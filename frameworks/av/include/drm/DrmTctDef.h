#ifndef __DRM_TCT_DEF_H__
#define __DRM_TCT_DEF_H__

#include <utils/String8.h>
#define NTP_SERVER_CNT 5

namespace android {

const unsigned int OMADrmFlag = 0x80; // 128

class DrmTrustedClient {
private:
    DrmTrustedClient();

public:
    static bool IsDrmTrustedClient(const String8& procName);

public:
    static const int TRUSTED_PROC_CNT = 13 + 1;
    static const char* TRUSTED_PROC[TRUSTED_PROC_CNT];
};

class DrmTrustedApp {
private:
    DrmTrustedApp();

public:
    static bool IsDrmTrustedApp(const String8& procName);

public:
    static const int TRUSTED_APP_CNT = 14 + 1;
    static const char* TRUSTED_APP[TRUSTED_APP_CNT];
};

class DrmTrustedVideoApp {
private:
    DrmTrustedVideoApp();

public:
    static bool IsDrmTrustedVideoApp(const String8& procName);

public:
    static const int TRUSTED_VIDEO_APP_CNT = 4;
    static const char* TRUSTED_VIDEO_APP[TRUSTED_VIDEO_APP_CNT];
};

class DrmMetaKey {
private:
    DrmMetaKey();

public:
    static const char* META_KEY_IS_DRM;
    static const char* META_KEY_CONTENT_URI;
    static const char* META_KEY_OFFSET;
    static const char* META_KEY_DATALEN;
    static const char* META_KEY_RIGHTS_ISSUER;
    static const char* META_KEY_CONTENT_NAME;
    static const char* META_KEY_CONTENT_DESCRIPTION;
    static const char* META_KEY_CONTENT_VENDOR;
    static const char* META_KEY_ICON_URI;
    static const char* META_KEY_METHOD;
    static const char* META_KEY_MIME;
};

}

#endif // __DRM_TCT_DEF_H__
