/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_NDEBUG 1
#define LOG_TAG "DrmTctUtil/WhiteList"
#include <utils/Log.h>

#include <drm/DrmTctDef.h>
#include <utils/String8.h>

using namespace android;

const int DrmTrustedApp::TRUSTED_APP_CNT;
const char* DrmTrustedApp::TRUSTED_APP[TRUSTED_APP_CNT] = {
    "com.android.gallery3d",
    "com.android.music",
    "com.jrdcom.music",
    "com.android.phone",
    "com.android.settings",
    "android.process.media",
    "com.android.systemui",
    "com.google.android.xts.media",
    "com.widevine.demo",
    "com.android.mms",
};

bool DrmTrustedApp::IsDrmTrustedApp(const String8& procName) {
    bool result = false;
    for (int i = 0; i < DrmTrustedApp::TRUSTED_APP_CNT; i++) {
        ALOGD("IsDrmTrustedApp: compare [%s] with [%s].",
                DrmTrustedApp::TRUSTED_APP[i],
                procName.string());

        if (0 == strcmp(DrmTrustedApp::TRUSTED_APP[i], procName.string())) {
            ALOGD("IsDrmTrustedApp: accepted.");
            result = true;
            break;
        }
    }
    return result;
}

const int DrmTrustedClient::TRUSTED_PROC_CNT;
const char* DrmTrustedClient::TRUSTED_PROC[TRUSTED_PROC_CNT] = {
    "com.android.gallery", // gallery 2d
    "com.android.gallery:CropImage",
    "com.cooliris.media",  // gallery 3d (2.3)
    "android.process.media",
    "com.android.phone",   // ringtone playing
    "com.android.gallery3d", // gallery (4.0)
    "com.android.gallery3d:crop",
    "com.jrdcom.android.gallery3d:crop",
    "com.jrdcom.android.gallery3d",
    "com.google.android.xts.media",
    "com.widevine.demo",
    "com.android.mms",
};

bool DrmTrustedClient::IsDrmTrustedClient(const String8& procName) {
    bool result = false;
    for (int i = 0; i < DrmTrustedClient::TRUSTED_PROC_CNT; i++) {
        ALOGD("IsDrmTrustedClient: compare [%s] with [%s].",
                DrmTrustedClient::TRUSTED_PROC[i],
                procName.string());

        if (0 == strcmp(DrmTrustedClient::TRUSTED_PROC[i], procName.string())) {
            ALOGD("IsDrmTrustedClient: accepted.");
            result = true;
            break;
        }
    }
    return result;
}

const int DrmTrustedVideoApp::TRUSTED_VIDEO_APP_CNT;
const char* DrmTrustedVideoApp::TRUSTED_VIDEO_APP[TRUSTED_VIDEO_APP_CNT] = {
    "com.android.gallery3d",
    "com.jrdcom.android.gallery3d",
    "com.mediatek.videoplayer",
    "com.mediatek.videoplayer2"
};

bool DrmTrustedVideoApp::IsDrmTrustedVideoApp(const String8& procName) {
    bool result = false;
    for (int i = 0; i < DrmTrustedVideoApp::TRUSTED_VIDEO_APP_CNT; i++) {
        ALOGD("IsDrmTrustedVideoApp: compare [%s] with [%s].",
                DrmTrustedVideoApp::TRUSTED_VIDEO_APP[i],
                procName.string());

        if (0 == strcmp(DrmTrustedVideoApp::TRUSTED_VIDEO_APP[i], procName.string())) {
            ALOGD("DrmTrustedVideoApp: accepted.");
            result = true;
            break;
        }
    }
    return result;
}
