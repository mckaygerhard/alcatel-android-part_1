#define LOG_NDEBUG 0
#define LOG_TAG "DrmWrapper"
#include "drm/DrmClientWrapper.h"
#include <DrmManagerClientImpl.h>
#include <string.h>
#include <utils/String8.h>
#include "utils/Log.h"
#include "drm/drm_framework_common.h"
#include <fcntl.h>
#include <sys/stat.h>

using namespace android;
 bool isDrm(const char* path){
    bool isDrm = false;
    DrmManagerClient client;
    if (client.canHandle(String8(path), String8(""))){
        isDrm = true;
    }
    return isDrm;
}

 bool isDrmFd(int fd){
    bool isDrm = false;
    DrmManagerClient client;
    if (client.canHandleFd(fd)){
        isDrm = true;
    }
    return isDrm;
}

 bool openDecryptDrmSession(DrmWrapperRes* wrapperRes, long dataSize, int descripter){
    if ( descripter < 0){
        return false;
    }
    if (dataSize <= 0){
        return false;
    }
    DrmManagerClient* client = new DrmManagerClient();
    sp<DecryptHandle>* mHandle = new sp<DecryptHandle>();
    if (client){
        *mHandle = client->openDecryptSession(descripter, 0, dataSize,NULL);
        if (NULL == mHandle){
            delete client;
            client = NULL;
            return false;
        }
        wrapperRes->mClient = client;
        wrapperRes->mHandle = mHandle;
        memset(wrapperRes->mDrmBuf, 0, sizeof(wrapperRes->mDrmBuf)/sizeof(char));
        wrapperRes->moffset= -1;
        wrapperRes->mBufSize = 0;
    }
    return true;
}

 /*long decryptData(DrmWrapperRes* wrapperRes, void* buffer, long dataSize, int offset){
    if (wrapperRes->mClient == NULL || wrapperRes->mHandle == NULL || buffer == NULL){
        //TCTALOGD("decryptData wrapperRes.mClient == NULL || wrapperRes.mHandle == NULL || buffer == NULL...");
        return OPENSESSION_FAILER;
    }
    long data_size = -1;
    long leftSize = dataSize;
    sp<DecryptHandle>* handle = (sp<DecryptHandle>*)wrapperRes->mHandle;
    DrmManagerClient* client = (DrmManagerClient*)wrapperRes->mClient;
    long location = 0;
    long offsetTmp = 0;
    location = wrapperRes->moffset + wrapperRes->mBufSize;
    if (wrapperRes->mBufSize > 0
        && offset >= wrapperRes->moffset && offset < location){
         if ((offset + dataSize) <= location){
        memcpy(buffer, &wrapperRes->mDrmBuf[offset - wrapperRes->moffset], dataSize);
        leftSize = 0;
        return dataSize;
         } else {
        offsetTmp = location - offset;
        leftSize = dataSize - offsetTmp;
        memcpy(buffer, &wrapperRes->mDrmBuf[offset - wrapperRes->moffset], offsetTmp);
        }
    } else {
        location = offset;
    }

    if (leftSize <= MAX_CASHE_LENGTH){
        memset(wrapperRes->mDrmBuf, 0, sizeof(wrapperRes->mDrmBuf)/sizeof(char));
        wrapperRes->moffset = location;
        wrapperRes->mBufSize = client->pread( *handle, wrapperRes->mDrmBuf, MAX_CASHE_LENGTH, (off64_t)location);
        if (wrapperRes->mBufSize > 0){
            leftSize = leftSize >= wrapperRes->mBufSize ? wrapperRes->mBufSize : leftSize;
            memcpy(buffer + offsetTmp, wrapperRes->mDrmBuf, leftSize);
            return (offsetTmp + leftSize);
        } else {
            return offsetTmp;
        }
    } else {
        data_size = client->pread( *handle, (void*)(buffer + offsetTmp), leftSize, (off64_t)location);
        if (data_size <= 0){
            data_size = offsetTmp;
        } else {
            data_size += offsetTmp;
        }
    }
    return data_size;

}*/

 void closeSession(DrmWrapperRes* wrapperRes){
    DrmManagerClient* client =  (DrmManagerClient*)wrapperRes->mClient;
    sp<DecryptHandle>* handle = (sp<DecryptHandle>*)wrapperRes->mHandle;
    if (NULL != handle){
        status_t status = client->closeDecryptSession(*handle);
        delete handle;
        handle = NULL;
    }
    if (client){
        delete client;
        client = NULL;
    }
    memset(wrapperRes->mDrmBuf, 0, sizeof(wrapperRes->mDrmBuf)/sizeof(char));
    wrapperRes->moffset = -1;
    wrapperRes->mBufSize= 0;
}
