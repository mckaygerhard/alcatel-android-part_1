/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file
 * DRM 1.0 Reference Port: linux implementation of drm_time.c.
 */

#define LOG_NDEBUG 0
#define LOG_TAG "drm_time"
#define TCT_DEBUG_LOG 0
#ifdef TCT_DEBUG_LOG
#define TCTALOGE ALOGE
#define TCTALOGD ALOGD
#define TCTALOGW ALOGW
#else
#define TCTALOGE(a...) do { } while(0)
#define TCTALOGD(a...) do { } while(0)
#define TCTALOGW(a...) do { } while(0)
#endif
#include <utils/Log.h>

#include "drm_time.h"
#include <unistd.h>

/* See drm_time.h */
uint32_t DRM_time_getElapsedSecondsFrom1970(void)
{
    return time(NULL);
}

/* See drm_time.h */
void DRM_time_sleep(uint32_t ms)
{
    usleep(ms * 1000);
}

//read three values from one txt file
int readSecureTimeFromTXT(int *flag, int64_t *delta){
    FILE *fp;
    int length = 1024;
    char *buf = (char*)malloc(length);
    if (NULL == buf){
        return -1;
    }
    fp = fopen(SECURE_TIME_PATH, "r");
    if (!fp){
        TCTALOGE("open file failed");
        free(buf);
        return -1;
    }
    int i = 0;
    while (!feof(fp)){
        fgets(buf, length, fp);
        if (0 == strlen(buf)){
            continue;
        }
        i++;
        if (1 == i){
            *flag = atoi(buf);
        } else if (2 == i){
            *delta = atoll(buf);
            break;
        }
    }
    fclose(fp);
    free(buf);
    return 0;
}

static int64_t changeMilltoSecond(const int64_t delta){
    int64_t delta_in_second = delta / 1000;
    int remainder = delta % 1000;
    if (remainder != 0) {
        if (delta > 0) {
            if (remainder >= 500) {
                delta_in_second += 1;
            }
        } else {
            if (remainder <= (-500)) {
                delta_in_second += -1;
            }
        }
    }
    return delta_in_second;
}
/* See drm_time.h */
void DRM_time_getSysTime(T_DB_TIME_SysTime *time_ptr)
{
    TCTALOGD("DRM_time_getSysTime enter.... ");
    //[drm]-Jan 17,2012-yanlong.li@jrdcom.com
    int flag=0;
    int64_t delta=-1;
    if(readSecureTimeFromTXT(&flag, &delta)!=-1){
        TCTALOGD("readSecureTimeFromTXT enter true.... ");
        //successfully read from file system
        if(flag==0){
            //be seen as no trust time
        }else if(flag==1){
            time_t T = time(NULL);
            int64_t current_time = T;
            int64_t delta_in_second = 0;
            if (delta != 0) {
                delta_in_second = changeMilltoSecond(delta);
            }
            TCTALOGD("delta_in_second = %lld,delta=%lld",delta_in_second,delta);
            int64_t trusttime = current_time + delta_in_second;
            struct tm* UTCTime = gmtime(&trusttime);
                TCTALOGD("GMT is: %s", asctime(UTCTime));
                TCTALOGD("GMT is: year = %d, month = %d, day = %d, hour = %d, min = %d, sec = %d", UTCTime->tm_year, UTCTime->tm_mon, UTCTime->tm_mday,
                UTCTime->tm_hour, UTCTime->tm_min, UTCTime->tm_sec);
                time_ptr->year = UTCTime->tm_year + 1900;
                time_ptr->month = UTCTime->tm_mon + 1;
                time_ptr->day = UTCTime->tm_mday;
                time_ptr->hour = UTCTime->tm_hour;
                time_ptr->min = UTCTime->tm_min;
                time_ptr->sec = UTCTime->tm_sec;
        }
    }

}

//xiaoqin.zhou@jrdcom.com fr270238
static int64_t mkgmtime(
        uint32_t year, uint32_t month, uint32_t day,
        uint32_t hour, uint32_t minute, uint32_t second)
{
    int64_t result;

    /*
     * FIXME: It does not check whether the specified days
     *        is valid based on the specified months.
     */
    assert(year >= 1970
            && month > 0 && month <= 12
            && day > 0 && day <= 31
            && hour < 24 && minute < 60
            && second < 60);

    /* Set 'day' to the number of days into the year. */
    day += ydays[month - 1] + (month > 2 && leap (year)) - 1;

    /* Now calculate 'day' to the number of days since Jan 1, 1970. */
    day = day + 365 * (year - 1970) + nleap(year);

    int64_mul(result, int64_const(day), int64_const(SECONDS_PER_DAY));
    int64_add(result, result, int64_const(
        SECONDS_PER_HOUR * hour + SECONDS_PER_MINUTE * minute + second));

    return result;
}


void changeTime(T_DB_TIME_SysTime* timeTm){
    if (NULL == timeTm){
        return;
    }
    int64_t time = mkgmtime(timeTm->year, timeTm->month, timeTm->day, timeTm->hour, timeTm->min, timeTm->sec);
    int bflag=0;
    int64_t delta=-1;
    if(readSecureTimeFromTXT(&bflag, &delta)!=-1){
        if (bflag == 1){
            int64_t delta_in_second = 0;
            if (delta != 0) {
                delta_in_second = changeMilltoSecond(delta);
            }
            int64_t structtime = time - delta_in_second;
            struct tm* pm = localtime(&structtime);
            timeTm->year = pm->tm_year + 1900;
            timeTm->month = pm->tm_mon + 1;
            timeTm->day = pm->tm_mday;
            timeTm->hour = pm->tm_hour;
            timeTm->min = pm->tm_min;
            timeTm->sec = pm->tm_sec;
        }
    }
}

int checkEndTimeExpired(int32_t checkdate, int32_t checktime){
    int32_t systemdate, systemtime;
    T_DB_TIME_SysTime systemTime;

    DRM_time_getSysTime(&systemTime);

    YMD_HMS_2_INT(systemTime.year, systemTime.month, systemTime.day, systemdate, systemTime.hour, systemTime.min,
                                       systemTime.sec, systemtime);
    if(checkdate > systemdate){
        return DRM_TIME_VAILD;
    }
    if(checkdate == systemdate && checktime > systemtime){
        return DRM_TIME_VAILD;
    }
    return DRM_TIME_INVAILD;
}
