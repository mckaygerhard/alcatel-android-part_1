/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |    author     |      Key      |           comment              */
/* ----------|---------------|---------------|--------------------------------*/
/* 12/20/2012| Mingchun.Li   | PR-373549     | Cannot consume datetime rights */
/*           |               |               | when use a multiple constraint */
/*           |               |               | rights                         */
/* ----------|---------------|---------------|--------------------------------*/
/* 12/25/2012| Peng.Cao      | PR-363704     | Cannot play the SD SP-MIDI file*/
/*           |               |               | when the count rights of SD    */
/*           |               |               | file is 1 count                */
/* ----------|---------------|---------------|--------------------------------*/
/* 12/19/2013| Peng.Cao      | PR-561836     | Sometime roAmount = 0          */
/* ----------|---------------|---------------|--------------------------------*/
/******************************************************************************/
#include <DrmRightsManager.h>
#include <drm_inner.h>
#include <drm_file.h>
#include <drm_i18n.h>
#include <drm_time.h>
#include <svc_drm.h>
#include <utils/Log.h>
#include <ctype.h>
#define LOG_NDEBUG 0
#define LOG_TAG "DrmRightsManager(c)"



static int32_t drm_getString(uint8_t* string, int32_t len, int32_t handle)
{
    int32_t i;

    for (i = 0; i < len; i++) {
        if (DRM_FILE_FAILURE == DRM_file_read(handle, &string[i], 1))
            return FALSE;
        if (string[i] == '\n') {
            string[i + 1] = '\0';
            break;
        }
    }
    return TRUE;
}

static int32_t drm_putString(uint8_t* string, int32_t handle)
{
    int32_t i = 0;

    for (i = 0;; i++) {
        if (string[i] == '\0')
            break;
        if (DRM_FILE_FAILURE == DRM_file_write(handle, &string[i], 1))
            return FALSE;
    }
    return TRUE;
}

static int32_t drm_writeToUidTxt(uint8_t* Uid, int32_t* id)
{
    int32_t length;
    int32_t i;
    uint8_t idStr[8];
    int32_t idMax;
    uint8_t(*uidStr)[256];
    uint16_t nameUcs2[MAX_FILENAME_LEN];
    int32_t nameLen;
    int32_t bytesConsumed;
    int32_t handle;
    int32_t fileRes;

    if (*id < 1)
        return FALSE;

    /* convert in ucs2 */
    nameLen = strlen(DRM_UID_FILE_PATH);
    nameLen = DRM_i18n_mbsToWcs(DRM_CHARSET_UTF8,
                        (uint8_t *)DRM_UID_FILE_PATH,
                        nameLen,
                        nameUcs2,
                        MAX_FILENAME_LEN,
                        &bytesConsumed);
    fileRes = DRM_file_open(nameUcs2,
                        nameLen,
                        DRM_FILE_MODE_READ,
                        &handle);
    if (DRM_FILE_SUCCESS != fileRes) {
        if(CreateDirectoriesByAscii(nameUcs2, nameLen) != TRUE) {
               TCTALOGE("failed!: auto create directory");
               return FALSE;
           }
        DRM_file_open(nameUcs2,
                        nameLen,
                        DRM_FILE_MODE_WRITE,
                        &handle);
        DRM_file_write(handle, (uint8_t *)"0\n", 2);
        DRM_file_close(handle);
        DRM_file_open(nameUcs2,
                        nameLen,
                        DRM_FILE_MODE_READ,
                        &handle);
    }

    if (!drm_getString(idStr, 8, handle)) {
        DRM_file_close(handle);
        return FALSE;
    }
    idMax = atoi((char *)idStr);

    if (idMax < *id)
        uidStr = malloc((idMax + 1) * 256);
    else
        uidStr = malloc(idMax * 256);

    for (i = 0; i < idMax; i++) {
        if (!drm_getString(uidStr[i], 256, handle)) {
            DRM_file_close(handle);
            free(uidStr);
            return FALSE;
        }
    }
    //[BUGFIX]-ADD-BEGIN by TCTNB.(Peng.Cao), 2013/09/24,PR-524483
    for (i=0; Uid[i]!='\0'; i++) {
        Uid[i] = tolower(Uid[i]);
    }
    //[BUGFIX]-ADD-END by TCTNB.(Peng.Cao)
    length = strlen((char *)Uid);
    strcpy((char *)uidStr[*id - 1], (char *)Uid);
    uidStr[*id - 1][length] = '\n';
    uidStr[*id - 1][length + 1] = '\0';
    if (idMax < (*id))
        idMax++;
    DRM_file_close(handle);

    DRM_file_open(nameUcs2,
                    nameLen,
                    DRM_FILE_MODE_WRITE,
                    &handle);
    sprintf((char *)idStr, "%d", idMax);

    if (!drm_putString(idStr, handle)) {
        DRM_file_close(handle);
        free(uidStr);
        return FALSE;
    }
    if (DRM_FILE_FAILURE == DRM_file_write(handle, (uint8_t *)"\n", 1)) {
        DRM_file_close(handle);
        free(uidStr);
        return FALSE;
    }
    for (i = 0; i < idMax; i++) {
        if (!drm_putString(uidStr[i], handle)) {
            DRM_file_close(handle);
            free(uidStr);
            return FALSE;
        }
    }
    if (DRM_FILE_FAILURE == DRM_file_write(handle, (uint8_t *)"\n", 1)) {
        DRM_file_close(handle);
        free(uidStr);
        return FALSE;
    }
    DRM_file_close(handle);
    free(uidStr);
    return TRUE;
}

/* See objmng_files.h */
int32_t drm_readFromUidTxt(uint8_t* Uid, int32_t* id, int32_t option)
{
    int32_t i;
    uint8_t p[256] = { 0 };
    uint8_t idStr[8];
    int32_t idMax = 0;
    uint16_t nameUcs2[MAX_FILENAME_LEN];
    int32_t nameLen = 0;
    int32_t bytesConsumed;
    int32_t handle;
    int32_t fileRes;
    uint8_t otherUid[256]={ 0 };//[PLATFORM]-Add by TCTNB.(QiuRuifeng), 2013/09/23 PR-504324,

    if (NULL == id || NULL == Uid)
        return FALSE;
    DRM_file_startup();

    /* convert in ucs2 */

    nameLen = strlen(DRM_UID_FILE_PATH);
    nameLen = DRM_i18n_mbsToWcs(DRM_CHARSET_UTF8,
                        (uint8_t *)DRM_UID_FILE_PATH,
                        nameLen,
                        nameUcs2,
                        MAX_FILENAME_LEN,
                        &bytesConsumed);
    fileRes = DRM_file_open(nameUcs2,
                        nameLen,
                        DRM_FILE_MODE_READ,
                        &handle);
    if (DRM_FILE_SUCCESS != fileRes) {
        if(CreateDirectoriesByAscii(nameUcs2, nameLen) != TRUE) {
               TCTALOGE("failed!: auto create directory");
               return FALSE;
           }
        DRM_file_open(nameUcs2,
                        nameLen,
                        DRM_FILE_MODE_WRITE,
                        &handle);
        DRM_file_write(handle, (uint8_t *)"0\n", 2);
        DRM_file_close(handle);
        DRM_file_open(nameUcs2,
                        nameLen,
                        DRM_FILE_MODE_READ,
                        &handle);
    }

    if (!drm_getString(idStr, 8, handle)) {
        DRM_file_close(handle);
        return FALSE;
    }
    idMax = atoi((char *)idStr);

    if (option == GET_UID) {
        if (*id < 1 || *id > idMax) {
            DRM_file_close(handle);
            return FALSE;
        }
        for (i = 1; i <= *id; i++) {
            if (!drm_getString(Uid, 256, handle)) {
                DRM_file_close(handle);
                return FALSE;
            }
        }
        DRM_file_close(handle);
        return TRUE;
    }
    if (option == GET_ID) {
        *id = -1;
        for (i = 1; i <= idMax; i++) {
            if (!drm_getString(p, 256, handle)) {
                DRM_file_close(handle);
                return FALSE;
            }
//[PLATFORM]-Add-BEGIN by TCTNB.(QiuRuifeng), 2013/09/23 PR-504324, reason [ID card][OMA DRM]Need support OMA DRM 1.0 (FW Lock + Separate Delivery)
         int j=0;
         for(j=0;Uid[j]!='\0';j++){
             otherUid[j]=tolower(Uid[j]);
           }
          otherUid[j]='\0';
//[PLATFORM]-Add-END by TCTNB.(QiuRuifeng)
//[PLATFORM]-Mod by TCTNB.(QiuRuifeng), 2013/09/23 PR-504324, reason [ID card][OMA DRM]Need support OMA DRM 1.0 (FW Lock + Separate Delivery)
            if (strstr((char *)p, (char *)Uid) != NULL || strstr((char *)p, (char *)otherUid) != NULL) {
                //&& strlen((char *)p) == strlen((char *)Uid) + 1) {
                *id = i;
                DRM_file_close(handle);
                return TRUE;
            }
            if ((*id == -1) && (strlen((char *)p) < 3))
                *id = i;
        }
        if (*id != -1) {
            DRM_file_close(handle);
            return FALSE;
        }
        *id = idMax + 1;
        DRM_file_close(handle);
        return FALSE;
    }
    DRM_file_close(handle);
    return FALSE;
}

static int32_t drm_acquireId(uint8_t* uid, int32_t* id)
{
    if (TRUE == drm_readFromUidTxt(uid, id, GET_ID))
        return TRUE;

    if(drm_writeToUidTxt(uid, id)){
        TCTALOGD("uid not exists write uid OK! uid=[%s] id=[%d]",uid,id);
        }else{
        TCTALOGD("uid not exists write uid FAILED! uid=[%s] id=[%d]",uid,id);
        }
    return FALSE; /* The Uid is not exit, then return FALSE indicate it */
}

int32_t drm_writeOrReadInfo(int32_t id, T_DRM_Rights* Ro, int32_t* RoAmount, int32_t option)
{
    TCTALOGD("drm_writeOrReadInfo -> id=[%d]  option=[%d]",id,option );
    uint8_t fullname[MAX_FILENAME_LEN] = {0};
    int32_t tmpRoAmount;
    uint16_t nameUcs2[MAX_FILENAME_LEN];
    int32_t nameLen = 0;
    int32_t bytesConsumed;
    int32_t handle;
    int32_t fileRes;

    sprintf((char *)fullname, ANDROID_DRM_CORE_PATH"%d"EXTENSION_NAME_INFO, id);

    /* convert in ucs2 */
    nameLen = strlen((char *)fullname);
    nameLen = DRM_i18n_mbsToWcs(DRM_CHARSET_UTF8,
                        fullname,
                        nameLen,
                        nameUcs2,
                        MAX_FILENAME_LEN,
                        &bytesConsumed);
    fileRes = DRM_file_open(nameUcs2,
                            nameLen,
                            DRM_FILE_MODE_READ,
                            &handle);
    if (DRM_FILE_SUCCESS != fileRes) {
        if (GET_ALL_RO == option || GET_A_RO == option)
            return FALSE;

        if (GET_ROAMOUNT == option) {
            *RoAmount = -1;
            return TRUE;
        }
    }

    DRM_file_close(handle);
    DRM_file_open(nameUcs2,
                nameLen,
                DRM_FILE_MODE_READ | DRM_FILE_MODE_WRITE,
                &handle);

    switch(option) {
    case GET_ROAMOUNT:
        if (DRM_FILE_FAILURE == DRM_file_read(handle, (uint8_t*)RoAmount, sizeof(int32_t))) {
            TCTALOGW("drm_writeOrReadInfo -> GET_ROAMOUNT FAILURE, *RoAmount=[%d]", *RoAmount);
            DRM_file_close(handle);
            return FALSE;
        }
        TCTALOGD("drm_writeOrReadInfo -> GET_ROAMOUNT *RoAmount=[%d]", *RoAmount);
        //[BUGFIX]-ADD-BEGIN TCTNB.Peng.Cao 12/19/2013 ,PR-561836
        if(*RoAmount == 0)
            (*RoAmount)++;//PR993337-Li-Zhao
        //[BUGFIX]-ADD-END TCTNB.Peng.Cao 12/19/2013
        break;
    case GET_ALL_RO:
        DRM_file_setPosition(handle, sizeof(int32_t));

        if (DRM_FILE_FAILURE == DRM_file_read(handle, (uint8_t*)Ro, (*RoAmount) * sizeof(T_DRM_Rights))) {
        	TCTALOGW("drm_writeOrReadInfo -> GET_ALL_RO FAILURE, *Ro=[%d]", *Ro);
            DRM_file_close(handle);
            return FALSE;
        }
        TCTALOGD("drm_writeOrReadInfo -> GET_ALL_RO *Ro=[%d]", *Ro);
        break;
    case SAVE_ALL_RO:
        if (DRM_FILE_FAILURE == DRM_file_write(handle, (uint8_t*)RoAmount, sizeof(int32_t))) {
            DRM_file_close(handle);
            return FALSE;
        }

        if (NULL != Ro && *RoAmount >= 1) {
            if (DRM_FILE_FAILURE == DRM_file_write(handle, (uint8_t*) Ro, (*RoAmount) * sizeof(T_DRM_Rights))) {
                DRM_file_close(handle);
                return FALSE;
            }
        }
        break;
    case GET_A_RO:
        DRM_file_setPosition(handle, sizeof(int32_t) + (*RoAmount - 1) * sizeof(T_DRM_Rights));

        if (DRM_FILE_FAILURE == DRM_file_read(handle, (uint8_t*)Ro, sizeof(T_DRM_Rights))) {
            DRM_file_close(handle);
            return FALSE;
        }
        break;
    case SAVE_A_RO:
        DRM_file_setPosition(handle, sizeof(int32_t) + (*RoAmount - 1) * sizeof(T_DRM_Rights));

        if (DRM_FILE_FAILURE == DRM_file_write(handle, (uint8_t*)Ro, sizeof(T_DRM_Rights))) {
            DRM_file_close(handle);
            return FALSE;
        }

        DRM_file_setPosition(handle, 0);
        if (DRM_FILE_FAILURE == DRM_file_read(handle, (uint8_t*)&tmpRoAmount, sizeof(int32_t))) {
            DRM_file_close(handle);
            return FALSE;
        }
        if (tmpRoAmount < *RoAmount) {
            DRM_file_setPosition(handle, 0);
            DRM_file_write(handle, (uint8_t*)RoAmount, sizeof(int32_t));
        }
        break;
    default:
        DRM_file_close(handle);
        return FALSE;
    }

    DRM_file_close(handle);
    return TRUE;
}

int32_t drm_appendRightsInfo(T_DRM_Rights* rights)
{
    int32_t id;
    int32_t roAmount;

    if (NULL == rights)
        return FALSE;
    TCTALOGD("DrmRightsManager:drm_appendRightsInfo-> enter!");
    drm_acquireId(rights->uid, &id);
    TCTALOGD("DrmRightsManager:drm_appendRightsInfo->drm_acquireId-(uid=%d,id=%d)",rights->uid,id);

    if (FALSE == drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT))
        return FALSE;
    TCTALOGD("drm_writeOrReadInfo-->Get RoAmount=%d,",roAmount);

    if (-1 == roAmount)
        roAmount = 0;

    /* The RO amount increase */
    roAmount++;

    /* Save the rights information */
    if (FALSE == drm_writeOrReadInfo(id, rights, &roAmount, SAVE_A_RO)){
        TCTALOGD("DrmRightsManager:drm_appendRightsInfo-> drm_writeOrReadInfo SAVE-RO! id=%d,roAmount=%d",id,roAmount);
        return FALSE;
    }
    TCTALOGD("DrmRightsManager:drm_appendRightsInfo-> exit!");
    return TRUE;
}

int32_t drm_getMaxIdFromUidTxt()
{
    uint8_t idStr[8];
    int32_t idMax = 0;
    uint16_t nameUcs2[MAX_FILENAME_LEN] = {0};
    int32_t nameLen = 0;
    int32_t bytesConsumed;
    int32_t handle;
    int32_t fileRes;

    /* convert in ucs2 */
    nameLen = strlen(DRM_UID_FILE_PATH);
    nameLen = DRM_i18n_mbsToWcs(DRM_CHARSET_UTF8,
                        (uint8_t *)DRM_UID_FILE_PATH,
                        nameLen,
                        nameUcs2,
                        MAX_FILENAME_LEN,
                        &bytesConsumed);
    fileRes = DRM_file_open(nameUcs2,
                        nameLen,
                        DRM_FILE_MODE_READ,
                        &handle);

    /* this means the uid.txt file is not exist, so there is not any DRM object */
    if (DRM_FILE_SUCCESS != fileRes)
        return 0;

    if (!drm_getString(idStr, 8, handle)) {
        DRM_file_close(handle);
        return -1;
    }
    DRM_file_close(handle);

    idMax = atoi((char *)idStr);
    return idMax;
}

int32_t drm_removeIdInfoFile(int32_t id)
{
    uint8_t filename[MAX_FILENAME_LEN] = {0};
    uint16_t nameUcs2[MAX_FILENAME_LEN];
    int32_t nameLen = 0;
    int32_t bytesConsumed;

    if (id <= 0)
        return FALSE;

    sprintf((char *)filename, ANDROID_DRM_CORE_PATH"%d"EXTENSION_NAME_INFO, id);

    /* convert in ucs2 */
    nameLen = strlen((char *)filename);
    nameLen = DRM_i18n_mbsToWcs(DRM_CHARSET_UTF8,
                        filename,
                        nameLen,
                        nameUcs2,
                        MAX_FILENAME_LEN,
                        &bytesConsumed);
    if (DRM_FILE_SUCCESS != DRM_file_delete(nameUcs2, nameLen))
        return FALSE;

    return TRUE;
}

int32_t drm_updateUidTxtWhenDelete(int32_t id)
{
    uint16_t nameUcs2[MAX_FILENAME_LEN];
    int32_t nameLen = 0;
    int32_t bytesConsumed;
    int32_t handle;
    int32_t fileRes;
    int32_t bufferLen;
    uint8_t *buffer;
    uint8_t idStr[8];
    int32_t idMax;

    if (id <= 0)
        return FALSE;

    nameLen = strlen(DRM_UID_FILE_PATH);
    nameLen = DRM_i18n_mbsToWcs(DRM_CHARSET_UTF8,
                        (uint8_t *)DRM_UID_FILE_PATH,
                        nameLen,
                        nameUcs2,
                        MAX_FILENAME_LEN,
                        &bytesConsumed);
    bufferLen = DRM_file_getFileLength(nameUcs2, nameLen);
    if (bufferLen <= 0)
        return FALSE;

    buffer = (uint8_t *)malloc(bufferLen);
    if (NULL == buffer)
        return FALSE;

    fileRes = DRM_file_open(nameUcs2,
                            nameLen,
                            DRM_FILE_MODE_READ,
                            &handle);
    if (DRM_FILE_SUCCESS != fileRes) {
        free(buffer);
        return FALSE;
    }

    drm_getString(idStr, 8, handle);
    idMax = atoi((char *)idStr);

    bufferLen -= strlen((char *)idStr);
    fileRes = DRM_file_read(handle, buffer, bufferLen);
    buffer[bufferLen] = '\0';
    DRM_file_close(handle);

    /* handle this buffer */
    {
        uint8_t *pStart, *pEnd;
        int32_t i, movLen;

        pStart = buffer;
        pEnd = pStart;
        for (i = 0; i < id; i++) {
            if (pEnd != pStart)
                pStart = ++pEnd;
            while ('\n' != *pEnd)
                pEnd++;
            if (pStart == pEnd)
                pStart--;
        }
        movLen = bufferLen - (pEnd - buffer);
        memmove(pStart, pEnd, movLen);
        bufferLen -= (pEnd - pStart);
    }

    if (DRM_FILE_SUCCESS != DRM_file_delete(nameUcs2, nameLen)) {
        free(buffer);
        return FALSE;
    }

    fileRes = DRM_file_open(nameUcs2,
        nameLen,
        DRM_FILE_MODE_WRITE,
        &handle);
    if (DRM_FILE_SUCCESS != fileRes) {
        free(buffer);
        return FALSE;
    }
    sprintf((char *)idStr, "%d", idMax);
    drm_putString(idStr, handle);
    DRM_file_write(handle, (uint8_t*)"\n", 1);
    DRM_file_write(handle, buffer, bufferLen);
    free(buffer);
    DRM_file_close(handle);
    return TRUE;
}

int32_t drm_getKey(uint8_t* uid, uint8_t* KeyValue)
{
    T_DRM_Rights ro;
    int32_t id, roAmount;

    if (NULL == uid || NULL == KeyValue)
        return FALSE;

    if (FALSE == drm_readFromUidTxt(uid, &id, GET_ID))
        return FALSE;

    if (FALSE == drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT))
        return FALSE;

    if (roAmount <= 0)
        return FALSE;

    memset(&ro, 0, sizeof(T_DRM_Rights));
    roAmount = 1;
    if (FALSE == drm_writeOrReadInfo(id, &ro, &roAmount, GET_A_RO))
        return FALSE;

    memcpy(KeyValue, ro.KeyValue, DRM_KEY_LEN);
    return TRUE;
}

int32_t drm_startCheckRights(int32_t * bIsXXable,
                                    T_DRM_Rights_Constraint * XXConstraint)
{
    T_DB_TIME_SysTime curDateTime;
    T_DRM_DATETIME CurrentTime;

    memset(&CurrentTime, 0, sizeof(T_DRM_DATETIME));

    if (NULL == bIsXXable || 0 == *bIsXXable || NULL == XXConstraint){
    	TCTALOGD("drm_startCheckRights    --------- input param are NULL! ---- bIsXXable=[%d]---*bIsXXable=[%d]---XXConstraint=[%d]",NULL == bIsXXable,*bIsXXable,NULL == XXConstraint);
        return DRM_FAILURE;
        }

    if (0 != (uint8_t)(XXConstraint->Indicator & DRM_NO_CONSTRAINT)) /* Have utter right? */{
        TCTALOGD("drm_startCheckRights    --------- DRM_NO_CONSTRAINT return  NULL!");
        return DRM_SUCCESS;
        }

    *bIsXXable = 0; /* Assume have invalid rights at first */

    if (0 != (XXConstraint->Indicator & (DRM_START_TIME_CONSTRAINT | DRM_END_TIME_CONSTRAINT))) {
        DRM_time_getSysTime(&curDateTime);
        TCTALOGD("drm_startCheckRights    --------- DRM_START_TIME_CONSTRAINT --- DRM_END_TIME_CONSTRAINT!");

        if (-1 == drm_checkDate(curDateTime.year, curDateTime.month, curDateTime.day,
                                curDateTime.hour, curDateTime.min, curDateTime.sec)){
   TCTALOGD("drm_checkDate    ---------FAILED!");
            return DRM_FAILURE;
            }

        YMD_HMS_2_INT(curDateTime.year, curDateTime.month, curDateTime.day,
                      CurrentTime.date, curDateTime.hour, curDateTime.min,
                      curDateTime.sec, CurrentTime.time);
        TCTALOGD("drm_checkDate   ---YMD_HMS_2_INT()   DONE! CurrentTime.date=%d   CurrentTime.time=%d ",CurrentTime.date,CurrentTime.time);
        TCTALOGD("drm_checkDate  curDateTime.year=[%d],   curDateTime.month=[%d],  curDateTime.day=[%d],  curDateTime.hour=[%d],  curDateTime.min=[%d],  curDateTime.sec=[%d] ",
        		curDateTime.year,curDateTime.month,curDateTime.day,curDateTime.hour,curDateTime.min,curDateTime.sec);
    }

    if (0 != (uint8_t)(XXConstraint->Indicator & DRM_COUNT_CONSTRAINT)) { /* Have count restrict? */
        if (XXConstraint->Count <= 0) {
            XXConstraint->Indicator &= ~DRM_COUNT_CONSTRAINT;
            return DRM_RIGHTS_EXPIRED;
        }
    }

    if (0 != (uint8_t)(XXConstraint->Indicator & DRM_START_TIME_CONSTRAINT)) {
        TCTALOGD("drm_startCheckRights    --------- DRM_START_TIME_CONSTRAINT !");
        TCTALOGE("drm_startCheckRights   ---------- XXConstraint->StartTime.date : [%d] XXConstraint->StartTime.time:[%d]" ,XXConstraint->StartTime.date,XXConstraint->StartTime.time);
        //TCTALOGE("XXConstraint->StartTime.time : [%d] CurrentTime.time:[%d]" ,XXConstraint->StartTime.time,CurrentTime.time);
        if (XXConstraint->StartTime.date > CurrentTime.date ||
            (XXConstraint->StartTime.date == CurrentTime.date &&
            /**[pengfei.zhong@jrdcom.com-PR309407] */
             XXConstraint->StartTime.time > CurrentTime.time)) {
            *bIsXXable = 1;
            return DRM_RIGHTS_PENDING;
        }
    }

    if (0 != (uint8_t)(XXConstraint->Indicator & DRM_END_TIME_CONSTRAINT)) { /* Have end time restrict? */
        TCTALOGD("drm_startCheckRights    --------- DRM_END_TIME_CONSTRAINT !");
        TCTALOGE("drm_startCheckRights    --------- XXConstraint->EndTime.date : [%d] XXConstraint->EndTime.time:[%d]" ,XXConstraint->EndTime.date,XXConstraint->EndTime.time);
         //TCTALOGE("XXConstraint->EndTime.time : [%d] CurrentTime.time:[%d]" ,XXConstraint->EndTime.time,CurrentTime.time);
        if (XXConstraint->EndTime.date < CurrentTime.date ||
            (XXConstraint->EndTime.date == CurrentTime.date &&
             XXConstraint->EndTime.time <= CurrentTime.time)) {
            XXConstraint->Indicator &= ~DRM_END_TIME_CONSTRAINT;
            return DRM_RIGHTS_EXPIRED;
        }
    }

    if (0 != (uint8_t)(XXConstraint->Indicator & DRM_INTERVAL_CONSTRAINT)) { /* Have interval time restrict? */
        TCTALOGD("drm_startCheckRights    --------- DRM_INTERVAL_CONSTRAINT !");
        TCTALOGE("drm_startCheckRights    --------- XXConstraint->Interval.date : [%d] XXConstraint->Interval.time:[%d]" ,XXConstraint->Interval.date,XXConstraint->Interval.time);
        if (XXConstraint->Interval.date == 0 && XXConstraint->Interval.time == 0) {
            XXConstraint->Indicator &= ~DRM_INTERVAL_CONSTRAINT;
            return DRM_RIGHTS_EXPIRED;
        }
    }

    *bIsXXable = 1;
    TCTALOGD("drm_startCheckRights   END --------- Successfult! !");
    return DRM_SUCCESS;
}

int32_t SVC_drm_deleteRights(uint8_t* roId)
{
    int32_t maxId, id, roAmount, j;
    T_DRM_Rights rights;

    memset(&rights, 0, sizeof(T_DRM_Rights));

    if (NULL == roId)
        return DRM_FAILURE;

    maxId = drm_getMaxIdFromUidTxt();
    if (-1 == maxId)
        return DRM_NO_RIGHTS;

    for (id = 1; id <= maxId; id++) {
        drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT);
        if (roAmount <= 0) /* this means there is not any rights */
            continue;

        for (j = 1; j <= roAmount; j++) {
            if (FALSE == drm_writeOrReadInfo(id, &rights, &j, GET_A_RO))
                continue;

            /* here find the RO which will be deleted */
            if (0 == strcmp((char *)rights.uid, (char *)roId)) {
                T_DRM_Rights *pAllRights;

                pAllRights = (T_DRM_Rights *)malloc(roAmount * sizeof(T_DRM_Rights));
                if (NULL == pAllRights)
                    return DRM_FAILURE;

                drm_writeOrReadInfo(id, pAllRights, &roAmount, GET_ALL_RO);
                roAmount--;
                if (0 == roAmount) { /* this means it is the last one rights */
                    drm_removeIdInfoFile(id); /* delete the id.info file first */
                    drm_updateUidTxtWhenDelete(id); /* update uid.txt file */
                    free(pAllRights);
                    return DRM_SUCCESS;
                } else /* using the last one rights instead of the deleted one */
                    memcpy(pAllRights + (j - 1), pAllRights + roAmount, sizeof(T_DRM_Rights));

                /* delete the id.info file first */
//                drm_removeIdInfoFile(id);

                if (FALSE == drm_writeOrReadInfo(id, pAllRights, &roAmount, SAVE_ALL_RO)) {
                    free(pAllRights);
                    return DRM_FAILURE;
                }

                free(pAllRights);
                return DRM_SUCCESS;
            }
        }
    }

    return DRM_FAILURE;
}


int32_t drm_checkRoAndUpdate(int32_t id, int32_t permission)
{
    int32_t writeFlag = 0;
    int32_t roAmount;
    int32_t validRoAmount = 0;
    int32_t flag = DRM_FAILURE;
    int32_t i, j;
    T_DRM_Rights *pRo;
    T_DRM_Rights *pCurRo;
    int32_t * pNumOfPriority;
    int32_t iNum;
    T_DRM_Rights_Constraint * pCurConstraint;
    T_DRM_Rights_Constraint * pCompareConstraint;
    int priority[8] = {1, 2, 4, 3, 8, 6, 7, 5};

    if (FALSE == drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT))
        return DRM_FAILURE;

    validRoAmount = roAmount;
    if (roAmount < 1)
        return DRM_NO_RIGHTS;

    pRo = malloc(roAmount * sizeof(T_DRM_Rights));
    pCurRo = pRo;
    if (NULL == pRo)
        return DRM_FAILURE;

    if (FALSE == drm_writeOrReadInfo(id, pRo, &roAmount, GET_ALL_RO)) {
        free(pRo);
        return DRM_FAILURE;
    }

    /** check the right priority */
    pNumOfPriority = malloc(sizeof(int32_t) * roAmount);
    for(i = 0; i < roAmount; i++) {
        iNum = roAmount - 1;
        for(j = 0; j < roAmount; j++) {
            if(i == j)
                continue;
            switch(permission) {
            case DRM_PERMISSION_PLAY:
                pCurConstraint = &pRo[i].PlayConstraint;
                pCompareConstraint = &pRo[j].PlayConstraint;
                break;
            case DRM_PERMISSION_DISPLAY:
                pCurConstraint = &pRo[i].DisplayConstraint;
                pCompareConstraint = &pRo[j].DisplayConstraint;
                break;
            case DRM_PERMISSION_EXECUTE:
                pCurConstraint = &pRo[i].ExecuteConstraint;
                pCompareConstraint = &pRo[j].ExecuteConstraint;
                break;
            case DRM_PERMISSION_PRINT:
                pCurConstraint = &pRo[i].PrintConstraint;
                pCompareConstraint = &pRo[j].PrintConstraint;
                break;
            default:
                free(pRo);
                free(pNumOfPriority);
                return DRM_FAILURE;
            }

            /**get priority by Indicator*/
            if(0 == (pCurConstraint->Indicator & DRM_NO_CONSTRAINT) &&
                0 == (pCompareConstraint->Indicator & DRM_NO_CONSTRAINT)) {
                    int num1, num2;
                    num1 = (pCurConstraint->Indicator & 0x0e) >> 1;
                    num2 = (pCompareConstraint->Indicator & 0x0e) >> 1;
                    if(priority[num1] > priority[num2]) {
                        iNum--;
                        continue;
                    } else if(priority[pCurConstraint->Indicator] < priority[pCompareConstraint->Indicator])
                        continue;
            } else if(pCurConstraint->Indicator > pCompareConstraint->Indicator) {
                iNum--;
                continue;
            } else if(pCurConstraint->Indicator < pCompareConstraint->Indicator)
                continue;

            if(0 != (pCurConstraint->Indicator & DRM_END_TIME_CONSTRAINT)) {
                if(pCurConstraint->EndTime.date < pCompareConstraint->EndTime.date) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->EndTime.date > pCompareConstraint->EndTime.date)
                    continue;

                if(pCurConstraint->EndTime.time < pCompareConstraint->EndTime.time) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->EndTime.date > pCompareConstraint->EndTime.date)
                    continue;
            }

            if(0 != (pCurConstraint->Indicator & DRM_INTERVAL_CONSTRAINT)) {
                if(pCurConstraint->Interval.date < pCompareConstraint->Interval.date) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->Interval.date > pCompareConstraint->Interval.date)
                    continue;

                if(pCurConstraint->Interval.time < pCompareConstraint->Interval.time) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->Interval.time > pCompareConstraint->Interval.time)
                    continue;
            }

            if(0 != (pCurConstraint->Indicator & DRM_COUNT_CONSTRAINT)) {
                if(pCurConstraint->Count < pCompareConstraint->Count) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->Count > pCompareConstraint->Count)
                    continue;
            }

            if(i < j)
                iNum--;
        }
        pNumOfPriority[iNum] = i;
    }

    for (i = 0; i < validRoAmount; i++) {
        /** check the right priority */
        if (pNumOfPriority[i] >= validRoAmount)
            break;

        pCurRo = pRo + pNumOfPriority[i];

        switch (permission) {
        case DRM_PERMISSION_PLAY:
            flag =
                drm_startConsumeRights(&pCurRo->bIsPlayable,
                                       &pCurRo->PlayConstraint, &writeFlag);
            break;
        case DRM_PERMISSION_DISPLAY:
            flag =
                drm_startConsumeRights(&pCurRo->bIsDisplayable,
                                       &pCurRo->DisplayConstraint,
                                       &writeFlag);
            break;
        case DRM_PERMISSION_EXECUTE:
            flag =
                drm_startConsumeRights(&pCurRo->bIsExecuteable,
                                       &pCurRo->ExecuteConstraint,
                                       &writeFlag);
            break;
        case DRM_PERMISSION_PRINT:
            flag =
                drm_startConsumeRights(&pCurRo->bIsPrintable,
                                       &pCurRo->PrintConstraint, &writeFlag);
            break;
        default:
            free(pNumOfPriority);
            free(pRo);
            return DRM_FAILURE;
        }

        /* Here confirm the valid RO amount and set the writeFlag */
        if (0 == pCurRo->bIsPlayable && 0 == pCurRo->bIsDisplayable &&
            0 == pCurRo->bIsExecuteable && 0 == pCurRo->bIsPrintable) {
            int32_t iCurPri;

            /** refresh the right priority */
            iCurPri = pNumOfPriority[i];
            for(j = i; j < validRoAmount - 1; j++)
                pNumOfPriority[j] = pNumOfPriority[j + 1];

            if(iCurPri != validRoAmount - 1) {
                memcpy(pCurRo, pRo + validRoAmount - 1,
                    sizeof(T_DRM_Rights));
                for(j = 0; j < validRoAmount -1; j++) {
                    if(validRoAmount - 1 == pNumOfPriority[j])
                        pNumOfPriority[j] = iCurPri;
                }
            }

            /* Here means it is not the last one RO, so the invalid RO should be deleted */
            writeFlag = 1;
            validRoAmount--; /* If current right is invalid */
            i--;
        }

        /* If the flag is TRUE, this means: we have found a valid RO, so break, no need to check other RO */
        if (DRM_SUCCESS == flag)
            break;
    }

    if (1 == writeFlag) {
        /* Delete the *.info first */
        drm_removeIdInfoFile(id);

        if (FALSE == drm_writeOrReadInfo(id, pRo, &validRoAmount, SAVE_ALL_RO))
            flag = DRM_FAILURE;
    }

    free(pNumOfPriority);
    free(pRo);
    return flag;
}

int32_t drm_startConsumeRights(int32_t * bIsXXable,
                                      T_DRM_Rights_Constraint * XXConstraint,
                                      int32_t * writeFlag)
{
	TCTALOGD("drm_startConsumeRights   ----start");
    T_DB_TIME_SysTime curDateTime;
    T_DRM_DATETIME CurrentTime;
    uint8_t countFlag = 0;
    memset(&CurrentTime, 0, sizeof(T_DRM_DATETIME));
    TCTALOGD("drm_startConsumeRights   ----check input parma ");
    if (NULL == bIsXXable || 0 == *bIsXXable || NULL == XXConstraint || NULL == writeFlag){
    	 TCTALOGD("drm_startConsumeRights   ----input parma = NULL");
        return DRM_FAILURE;
    }
    TCTALOGD("drm_startConsumeRights   ----check DRM_CONSTRAINT");
    if (0 != (uint8_t)(XXConstraint->Indicator & DRM_NO_CONSTRAINT)) /* Have utter right? */{
    	 TCTALOGD("drm_startConsumeRights   ----DRM_NO_CONSTRAINT");
        return DRM_SUCCESS;
    }

    *bIsXXable = 0; /* Assume have invalid rights at first */
    *writeFlag = 0;

    if (0 != (XXConstraint->Indicator & (DRM_START_TIME_CONSTRAINT | DRM_END_TIME_CONSTRAINT | DRM_INTERVAL_CONSTRAINT))) {
        DRM_time_getSysTime(&curDateTime);

        if (-1 == drm_checkDate(curDateTime.year, curDateTime.month, curDateTime.day,
                                curDateTime.hour, curDateTime.min, curDateTime.sec)){
        	TCTALOGD("drm_startConsumeRights   ----drm_checkDate fail");
            return DRM_FAILURE;
        }

        YMD_HMS_2_INT(curDateTime.year, curDateTime.month, curDateTime.day,
                      CurrentTime.date, curDateTime.hour, curDateTime.min,
                      curDateTime.sec, CurrentTime.time);
        TCTALOGD("drm_startConsumeRights   ----YMD_HMS_2_INT DONE  CurrentTime.date=[%d] , CurrentTime.time=[%d] ",CurrentTime.date,CurrentTime.time);
    }

    if (0 != (uint8_t)(XXConstraint->Indicator & DRM_COUNT_CONSTRAINT)) { /* Have count restrict? */
    	TCTALOGD("drm_startConsumeRights   ----DRM_COUNT_CONSTRAINT");
        *writeFlag = 1;
        /* If it has only one time for use, after use this function, we will delete this rights */
        if (XXConstraint->Count <= 0) {
            XXConstraint->Indicator &= ~DRM_COUNT_CONSTRAINT;
            TCTALOGD("drm_startConsumeRights   ----DRM_RIGHTS_EXPIRED--0");
            return DRM_RIGHTS_EXPIRED;
        }
        //[BUGFIX]-ADD-BEGIN by TCTNB.(Peng.Cao), 2012/12/25,PR-363704,
        //Cannot play the SD SP-MIDI file when the count rights of SD file is 1 count
        if ((XXConstraint->Count--) < 1) {
        //[BUGFIX]-ADD-END by TCTNB.(Peng.Cao), 2012/12/25
            XXConstraint->Indicator &= ~DRM_COUNT_CONSTRAINT;
            countFlag = 1;
        }
    }

    if (0 != (uint8_t)(XXConstraint->Indicator & DRM_START_TIME_CONSTRAINT)) {
    	TCTALOGD("drm_startConsumeRights   ----DRM_START_TIME_CONSTRAINT");
        if (XXConstraint->StartTime.date > CurrentTime.date ||
            (XXConstraint->StartTime.date == CurrentTime.date &&
             XXConstraint->StartTime.time >= CurrentTime.time)) {
            *bIsXXable = 1;
            TCTALOGD("drm_startConsumeRights   ----DRM_RIGHTS_PENDING");
            return DRM_RIGHTS_PENDING;
        }
    }

    if (0 != (uint8_t)(XXConstraint->Indicator & DRM_END_TIME_CONSTRAINT)) { /* Have end time restrict? */
    	TCTALOGD("drm_startConsumeRights   ----DRM_END_TIME_CONSTRAINT");
        if (XXConstraint->EndTime.date < CurrentTime.date ||
            (XXConstraint->EndTime.date == CurrentTime.date &&
             XXConstraint->EndTime.time <= CurrentTime.time)) {
            *writeFlag = 1;
            XXConstraint->Indicator &= ~DRM_END_TIME_CONSTRAINT;
            TCTALOGD("drm_startConsumeRights   ----DRM_RIGHTS_EXPIRED--1");
            return DRM_RIGHTS_EXPIRED;
        }
    }

    if (0 != (uint8_t)(XXConstraint->Indicator & DRM_INTERVAL_CONSTRAINT)) { /* Have interval time restrict? */
    	TCTALOGD("drm_startConsumeRights   ----DRM_INTERVAL_CONSTRAINT");
        int32_t year, mon, day, hour, min, sec, date, time;
        int32_t intervalYear, intervalMon, intervalDay, intervalHour, intervalMin, intervalSec;
        int32_t ret;
        //[BUGFIX]-ADD-BEGIN by TCTNB.(Mingchun.Li), 2012/12/20,PR-373549,Cannot consume datetime rights when use a multiple constraint rights
        int32_t end_date=0;
        int32_t end_time=0;
        int32_t exist_end_right = 0;

        if (0 != (uint8_t)(XXConstraint->Indicator & DRM_END_TIME_CONSTRAINT)) {
            end_date = XXConstraint->EndTime.date;
            end_time = XXConstraint->EndTime.time;
            exist_end_right = 1;
        }
        //[BUGFIX]-ADD-END by TCTNB.(Mingchun.Li), 2012/12/20,PR-373549
        XXConstraint->Indicator |= DRM_END_TIME_CONSTRAINT |DRM_START_TIME_CONSTRAINT;
        XXConstraint->Indicator &= ~DRM_INTERVAL_CONSTRAINT; /* Write off interval right */
        *writeFlag = 1;
        YMD_HMS_2_INT(curDateTime.year, curDateTime.month, curDateTime.day, XXConstraint->StartTime.date, curDateTime.hour,
                      curDateTime.min, curDateTime.sec, XXConstraint->StartTime.time);
        if (XXConstraint->Interval.date == 0
            && XXConstraint->Interval.time == 0) {
        	TCTALOGD("drm_startConsumeRights   ----DRM_RIGHTS_EXPIRED--2");
            return DRM_RIGHTS_EXPIRED;
        }
        //date = CurrentTime.date + XXConstraint->Interval.date;
        //time = CurrentTime.time + XXConstraint->Interval.time;
        INT_2_YMD_HMS(year, mon, day, CurrentTime.date, hour, min, sec, CurrentTime.time);
        INT_2_YMD_HMS(intervalYear, intervalMon, intervalDay, XXConstraint->Interval.date,
                                              intervalHour, intervalMin, intervalSec, XXConstraint->Interval.time);

        sec += intervalSec;
        if (sec > 59) {
            min += sec / 60;
            sec %= 60;
        }
        min += intervalMin;
        if (min > 59) {
            hour += min / 60;
            min %= 60;
        }
        hour += intervalHour;
        if (hour > 23) {
            day += hour / 24;
            hour %= 24;
        }
        day += intervalDay;
        if (day > 31) {
            mon += day / 31;
            day %= 31;
        }
        mon += intervalMon;
        if (mon > 12) {
            year += mon / 12;
            mon %= 12;
        }
        year += intervalYear;
        if (day > (ret = drm_monthDays(year, mon))) {
            day -= ret;
            mon++;
            if (mon > 12) {
                mon -= 12;
                year++;
            }
        }
        YMD_HMS_2_INT(year, mon, day, XXConstraint->EndTime.date, hour,
                      min, sec, XXConstraint->EndTime.time);
        TCTALOGD("end date = %d, time = %d", XXConstraint->EndTime.date, XXConstraint->EndTime.time);
        //[BUGFIX]-ADD-BEGIN by TCTNB.(Mingchun.Li), 2012/12/20,PR-373549,Cannot consume datetime rights when use a multiple constraint rights
        if(exist_end_right) {
            if (XXConstraint->EndTime.date > end_date
                ||(XXConstraint->EndTime.date == end_date && XXConstraint->EndTime.time > end_time)) {
                XXConstraint->EndTime.date = end_date;
                XXConstraint->EndTime.time = end_time;
            }
        }
        //[BUGFIX]-ADD-END by TCTNB.(Mingchun.Li), 2012/12/20,PR-373549
    }
    TCTALOGD("drm_startConsumeRights   ----end");
    if (1 != countFlag)
        *bIsXXable = 1; /* Can go here ,so  right must be valid */
    return DRM_SUCCESS;
}
