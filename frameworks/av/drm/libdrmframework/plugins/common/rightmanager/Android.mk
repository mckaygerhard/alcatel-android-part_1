#
# Copyright (C) 2010 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
    src/ParserRight.c \
    src/DrmRightsManager.c \
    src/drm_file.c \
    src/drm_i18n.c \
    src/drm_time.c \

LOCAL_MODULE := libdrmrightmanager
LOCAL_32_BIT_ONLY := true
LOCAL_WHOLE_STATIC_LIBRARIES := \
  libdrmframeworkcommon \
  libdrmxmlparser \
  libdrmdecoder \
  libdrmparserdm

LOCAL_SHARED_LIBRARIES := \
    libutils \
    libdl \
    libcrypto

LOCAL_C_INCLUDES += \
    $(TOP)/frameworks/av/drm/libdrmframework/plugins/common/rightmanager/include \
    $(TOP)/frameworks/av/drm/libdrmframework/plugins/common/include \
    $(TOP)/frameworks/av/drm/libdrmframework/plugins/common/xml/include \
    $(TOP)/frameworks/av/drm/libdrmframework/plugins/common/parserdm/include \
    $(TOP)/frameworks/av/drm/libdrmframework/plugins/common/decode/include \
    external/openssl/include \

LOCAL_MODULE_TAGS := optional

include $(BUILD_STATIC_LIBRARY)
