/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/06/2012|Peng.Cao              |                      |support drm       */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

#define LOG_NDEBUG 0
#define LOG_TAG "SeparateEngine"

#include <utils/Log.h>
#include <unistd.h>
#include <openssl/aes.h>
#include <openssl/hmac.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <utils/String8.h>
#include <DrmMetadata.h>
#include <DrmConvertedStatus.h>
#include "dlfcn.h"
#include "MimeTypeUtil.h"
#include "SDFileDecrypt.h"
#include "parser_dcf.h"
#include "DrmRightsManager.h"
#include "ParserRight.h"
#include <drm_time.h>

#include "SeparateEngine.h"
#include "SeparateEngineConst.h"
#include "drm_framework_common.h"
#include "drm_sd_api.h"

#include <DrmConstraints.h>
using namespace android;
#define FILEHEADER  (DRM_UID_LEN * 2)

int32_t  getInputDataLength(int32_t inputHandle)
{
   lseek(inputHandle, 0, SEEK_SET);
   int newpos = lseek(inputHandle, 0, SEEK_END);
   lseek(inputHandle, 0, SEEK_SET);
   return newpos;
}

int32_t  readInputData(int32_t inputHandle, uint8_t* buf, int32_t bufLen)
{
   int size=read(inputHandle,buf,bufLen);
   return size;
}

int32_t  seekInputData(int32_t inputHandle, int32_t offset)
{
   int newpos=lseek(inputHandle,offset,SEEK_CUR);
   return newpos;
}

// This extern "C" is mandatory to be managed by TPlugInManager
extern "C" IDrmEngine* create() {
	TCTALOGE("LZH IDrmEngine* create()");

    return new SeparateEngine();
}

// This extern "C" is mandatory to be managed by TPlugInManager
extern "C" void destroy(IDrmEngine* plugIn) {
    delete plugIn;
}

const String8 SeparateEngine::CONTENT_TYPE("content_type");
const String8 SeparateEngine::CONTENT_UID("content_uid");
const String8 SeparateEngine::ENCRYPTION_METHOD("encrption_method");
const String8 SeparateEngine::CONTENT_NAME("content_name");
const String8 SeparateEngine::RIGHTS_ISSUER("rights_issuer");
const String8 SeparateEngine::CONTENT_DESCRIPTION("content_description");
const String8 SeparateEngine::CONTENT_VENDOR("content_vendor");
const String8 SeparateEngine::ICON_URI("icon_uri");
static bool getUidFromDrmFile(const int fd, char *uid, int32_t *uidLength);

SeparateEngine::SeparateEngine() {
}

SeparateEngine::~SeparateEngine() {
    int size = decodeSessionMap.getSize();

    for (int i = 0; i < size; i++) {
        DecodeSession *session = decodeSessionMap.getValueAt(i);
        close(session->mFd);
    }
}

int SeparateEngine::getConvertedStatus(SeparateConv_Status_t status) {
    int retStatus = DrmConvertedStatus::STATUS_ERROR;

    switch(status) {
        case SeparateConv_Status_OK:
            retStatus = DrmConvertedStatus::STATUS_OK;
            break;
        case SeparateConv_Status_SyntaxError:
        case SeparateConv_Status_InvalidArgument:
        case SeparateConv_Status_UnsupportedFileFormat:
        case SeparateConv_Status_UnsupportedContentTransferEncoding:
            TCTALOGD("SeparateEngine getConvertedStatus: file conversion Error %d. "
                  "Returning STATUS_INPUTDATA_ERROR", status);
            retStatus = DrmConvertedStatus::STATUS_INPUTDATA_ERROR;
            break;
        default:
            TCTALOGD("SeparateEngine getConvertedStatus: file conversion Error %d. "
                  "Returning STATUS_ERROR", status);
            retStatus = DrmConvertedStatus::STATUS_ERROR;
            break;
    }

    return retStatus;
}

/**
 * @auther feng.wang@jrdcom.com
 */
static int getConstraintFromRights(T_DRM_Rights *rights, int *type,
        T_DRM_Rights_Constraint **constraint) {

    if (NULL == rights || NULL == type || NULL == constraint) {
        return FALSE;
    }

    if (rights->bIsPlayable) {
        *type = Action::PLAY;
        *constraint = &rights->PlayConstraint;
    } else if (rights->bIsDisplayable) {
        *type = Action::DISPLAY;
        *constraint = &rights->DisplayConstraint;
    } else if (rights->bIsExecuteable) {
        *type = Action::PLAY;
        *constraint = &rights->ExecuteConstraint;
    } else if (rights->bIsPrintable) {
        *type = Action::PRINT;
        *constraint = &rights->PrintConstraint;
    }
    return TRUE;
}

/**
 * @auther jian.gu@tct-nj.com
 */
static int getUidFromDrmFile(const char *path, char *uid) {
    int result = FALSE;
    int32_t typeLength = 0;
    int32_t uidLength = 0;
    FILE *fp = NULL;
    char *head = (char *) malloc(MAX_CONTENT_ID * 2);

    if ((fp = fopen(path, "rb")) != NULL) {
        if (fread(head, 1, FILEHEADER, fp) >= 3) {
            typeLength = head[1];
            uidLength = head[2];
            if (uidLength < MAX_CONTENT_ID) {
                memcpy(uid, head + typeLength + 3, uidLength);
                uid[uidLength] = '\0';
                result = TRUE;
            } else {
            }
        }
    }
    free(head);
    fclose(fp);
    return result;
}

// [drm] jian.gu@tct-nj.com
bool SeparateEngine::onIsCountOrIntervalConstraint(const String8& path) {
    char uid[MAX_CONTENT_ID];
    int32_t id = -1;
    if (!getUidFromDrmFile(path.string(), uid)) {
        return false;
    }
    if (!drm_readFromUidTxt((uint8_t*) uid, &id, GET_ID)) {
        return false;
    }

    int32_t roAmount = -1;
    if (!drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT) || roAmount <= 0) {
        return false;
    }

    T_DRM_Rights rights;
    memset(&rights, 0, sizeof(T_DRM_Rights));
    if (!drm_writeOrReadInfo(id, &rights, &roAmount, GET_A_RO)) {
        return false;
    }
    /** action, constraint */
    int drmAction = Action::DEFAULT;
    T_DRM_Rights_Constraint *constraint = NULL;
    if (TRUE == getConstraintFromRights(&rights, &drmAction, &constraint)) {
        //PR982395-Li-Zhao begin
        if (constraint == NULL) {
            return false;
        }
        //PR982395-Li-Zhao end
        if (0 != (uint8_t) (constraint->Indicator & DRM_COUNT_CONSTRAINT) ||
                (0 != (uint8_t) (constraint->Indicator & DRM_INTERVAL_CONSTRAINT) &&
                        0 == (uint8_t) (constraint->Indicator & DRM_END_TIME_CONSTRAINT))) {
            return true;
        }
    }
    return false;
}

//xiaoqin.zhou@jrdcom.com to change time to display the time
void SeparateEngine::changeDisplayTime(int date, int time, bool isChange, char* sOutput){
    if (NULL == sOutput){
        return;
    }
    int year = 0, mon = 0, day = 0, hour = 0, min = 0, sec = 0;
    INT_2_YMD_HMS(year, mon, day, date, hour, min, sec,time);
    TCTALOGD("LZH CombinedEngine::onGetConstraints1 year = %d, mon = %d, day = %d, hour = %d, min = %d, sec = %d",
        year, mon, day, hour, min, sec);
    if (isChange){
        T_DB_TIME_SysTime rightDateTime;
        rightDateTime.year = year;
        rightDateTime.month = mon;
        rightDateTime.day = day;
        rightDateTime.hour = hour;
        rightDateTime.min = min;
        rightDateTime.sec= sec;
        changeTime(&rightDateTime);
        TCTALOGD("LZH CombinedEngine::onGetConstraints1 changed year = %d, mon = %d, day = %d, hour = %d, min = %d, sec = %d",
                rightDateTime.year, rightDateTime.month, rightDateTime.day, rightDateTime.hour, rightDateTime.min, rightDateTime.sec);
        sprintf(sOutput, "%04d-%02d-%02d %02d:%02d:%02d", rightDateTime.year, rightDateTime.month, rightDateTime.day, rightDateTime.hour, rightDateTime.min, rightDateTime.sec);
    } else {
        sprintf(sOutput, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);
    }
}

DrmConstraints* SeparateEngine::onGetConstraints( int uniqueId, const String8* path, int action){
    //FILE *fp;
    if (NULL == path ){
        return NULL;
    }

    int fd = open((*path).string(), O_RDONLY);
    if (fd < 0){
        return NULL;
    }
    DrmConstraints* drmConstraints = onGetConstraintsByFd(uniqueId, fd, action);
    close(fd);
    return drmConstraints;
}

DrmConstraints* SeparateEngine::onGetConstraintsByFd(
        int uniqueId, int fd, int action){
  //  char* fileheader=(char *)malloc(FILEHEADER);
    char uid[MAX_CONTENT_ID];
    int  typeLength,uidLength;
    int32_t id;
    int32_t roAmount;
    const char *errmsg;
    int32_t validRoAmount = 0;
    int32_t flag = DRM_FAILURE;
    int32_t i, j;
    T_DRM_Rights *pRo;
    T_DRM_Rights *pCurRo;
    int32_t * pNumOfPriority;
    int32_t iNum;
    T_DRM_Rights_Constraint * pCurConstraint;
    T_DRM_Rights_Constraint * pCompareConstraint;
    T_DRM_Rights_Constraint  validConstraint;
    int priority[8] = {1, 2, 4, 3, 8, 5, 7, 6};

    int des = dup(fd);
    if (des < 0){
     //   free(fileheader);
        return NULL;
    }
    if (-1 == lseek64(des, 0, SEEK_SET)){
//        free(fileheader);
        close(des);
        return NULL;
    }
       //pengfei.zhong@jrdcom.com-PR300460 --- if action is default choose an action for user. start
     if(action == Action::DEFAULT){
                String8 m = onGetOriginalMimeType(uniqueId, des);
                 action = MimeTypeUtil::getAction(m);
   }
    //pengfei.zhong@jrdcom.com-PR300460 ---end
    if (!getUidFromDrmFile(des, uid, &uidLength)){
  //      free(fileheader);
        close(des);
        return NULL;
    }
    close(des);

    if (0 == drm_readFromUidTxt((uint8_t*)uid, &id, GET_ID)){
  //      free(fileheader);
        return NULL;
    }

//    free(fileheader);
    if (0 == drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT)){
        return NULL;
    }

    validRoAmount = roAmount;
    if (roAmount < 1){
        return NULL;
    }

    pRo = (T_DRM_Rights*)malloc(roAmount * sizeof(T_DRM_Rights));
    pCurRo = pRo;
    if (NULL == pRo){
        return NULL;
    }

    if (0 == drm_writeOrReadInfo(id, pRo, &roAmount, GET_ALL_RO)) {
        free(pRo);
        return NULL;
    }

    /** check the right priority */
    pNumOfPriority = (int32_t*) malloc(sizeof(int32_t) * roAmount);
    for(i = 0; i < roAmount; i++) {
        iNum = roAmount - 1;
        for(j = 0; j < roAmount; j++) {
            if(i == j)
                continue;
            switch(action) {
            case Action::PLAY:
                pCurConstraint = &pRo[i].PlayConstraint;
                pCompareConstraint = &pRo[j].PlayConstraint;
                break;
            case Action::DISPLAY:
                pCurConstraint = &pRo[i].DisplayConstraint;
                pCompareConstraint = &pRo[j].DisplayConstraint;
                break;
            case Action::EXECUTE:
                pCurConstraint = &pRo[i].ExecuteConstraint;
                pCompareConstraint = &pRo[j].ExecuteConstraint;
                break;
            case Action::PRINT:
                pCurConstraint = &pRo[i].PrintConstraint;
                pCompareConstraint = &pRo[j].PrintConstraint;
                break;
            default:
                free(pRo);
                free(pNumOfPriority);
                return NULL;
            }

            /**get priority by Indicator*/
            if(0 == (pCurConstraint->Indicator & DRM_NO_CONSTRAINT) &&
                0 == (pCompareConstraint->Indicator & DRM_NO_CONSTRAINT)) {
                    int num1, num2;
                    num1 = (pCurConstraint->Indicator & 0x0e) >> 1;
                    num2 = (pCompareConstraint->Indicator & 0x0e) >> 1;
                    if(priority[num1] > priority[num2]) {
                        iNum--;
                        continue;
                     /* } else if(priority[pCurConstraint->Indicator] < priority[pCompareConstraint->Indicator])   */
                    } else if(priority[num1] < priority[num2])
                        continue;
            } else if(pCurConstraint->Indicator > pCompareConstraint->Indicator) {
                iNum--;
                continue;
            } else if(pCurConstraint->Indicator < pCompareConstraint->Indicator)
                continue;

            if(0 != (pCurConstraint->Indicator & DRM_END_TIME_CONSTRAINT)) {
                if(pCurConstraint->EndTime.date < pCompareConstraint->EndTime.date) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->EndTime.date > pCompareConstraint->EndTime.date)
                    continue;

                if(pCurConstraint->EndTime.time < pCompareConstraint->EndTime.time) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->EndTime.date > pCompareConstraint->EndTime.date)
                    continue;
            }

            if(0 != (pCurConstraint->Indicator & DRM_INTERVAL_CONSTRAINT)) {
                if(pCurConstraint->Interval.date < pCompareConstraint->Interval.date) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->Interval.date > pCompareConstraint->Interval.date)
                    continue;

                if(pCurConstraint->Interval.time < pCompareConstraint->Interval.time) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->Interval.time > pCompareConstraint->Interval.time)
                    continue;
            }

            if(0 != (pCurConstraint->Indicator & DRM_COUNT_CONSTRAINT)) {
                if(pCurConstraint->Count < pCompareConstraint->Count) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->Count > pCompareConstraint->Count)
                    continue;
            }

            if(i < j)
                iNum--;
        }
        pNumOfPriority[iNum] = i;
    }

    for (i = 0; i < validRoAmount; i++) {
        /** check the right priority */
        if (pNumOfPriority[i] >= validRoAmount)
            break;

        pCurRo = pRo + pNumOfPriority[i];
        switch (action) {
        case Action::PLAY:
            flag =
                drm_startCheckRights(&pCurRo->bIsPlayable,
                                       &pCurRo->PlayConstraint);
            break;
        case Action::DISPLAY:
            flag =
                drm_startCheckRights(&pCurRo->bIsDisplayable,
                                       &pCurRo->DisplayConstraint);
            break;
        case Action::EXECUTE:
            flag =
                drm_startCheckRights(&pCurRo->bIsExecuteable,
                                       &pCurRo->ExecuteConstraint);
            break;
        case DRM_PERMISSION_PRINT:
            flag =
                drm_startCheckRights(&pCurRo->bIsPrintable,
                                       &pCurRo->PrintConstraint);
            break;
        default:
            free(pNumOfPriority);
            free(pRo);
            return NULL;
        }

        /* If the flag is TRUE, this means: we have found a valid RO, so break, no need to check other RO */
        if (DRM_SUCCESS == flag)
            break;
    }

    if (i==validRoAmount)
    {
        free(pNumOfPriority);
        free(pRo);
        return NULL;
    }
    free(pNumOfPriority);


    switch (action) {
    case Action::PLAY:
        validConstraint=pCurRo->PlayConstraint;
        break;
    case Action::DISPLAY:
        validConstraint=pCurRo->DisplayConstraint;
        break;
    case Action::EXECUTE:
        validConstraint=pCurRo->ExecuteConstraint;
        break;
    case Action::PRINT:
        validConstraint=pCurRo->PrintConstraint;
        break;
    default:
        free(pRo);
        return NULL;
    }
    char sOutput[64];
    DrmConstraints* drmConstraints =  new DrmConstraints();
    if ( drmConstraints==NULL)
    {
        free(pRo);
        return NULL;
    }
    /** content id */
    drmConstraints->put(&DrmConstraints::CONTENT_ID, uid);

    if (0 != (uint8_t)(validConstraint.Indicator & DRM_NO_CONSTRAINT)) /* Have utter right? */
    {
        free(pRo);
        return drmConstraints;
     }
    //int year = 0, mon = 0, day = 0, hour = 0, min = 0, sec = 0;
    if (0 != (uint8_t)(validConstraint.Indicator & DRM_COUNT_CONSTRAINT)) { /* Have count restrict? */
       sprintf(sOutput, "%d", validConstraint.Count);
       drmConstraints->put(&DrmConstraints::REMAINING_REPEAT_COUNT, sOutput);
       drmConstraints->put(&DrmConstraints::TYPE, "count");
    }

    if (0 != (uint8_t)(validConstraint.Indicator & DRM_START_TIME_CONSTRAINT)) {
       changeDisplayTime(validConstraint.StartTime.date, validConstraint.StartTime.time, true, sOutput);
       /*INT_2_YMD_HMS(year, mon, day, validConstraint.StartTime.date, hour, min, sec,
               validConstraint.StartTime.time);
            sprintf(sOutput, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
            drmConstraints->put(&DrmConstraints::LICENSE_START_TIME, sOutput);
            drmConstraints->put(&DrmConstraints::TYPE, "datetime");
    }

    if (0 != (uint8_t)(validConstraint.Indicator & DRM_END_TIME_CONSTRAINT)) { /* Have end time restrict? */
       changeDisplayTime(validConstraint.EndTime.date, validConstraint.EndTime.time, true, sOutput);
       /*INT_2_YMD_HMS(year, mon, day, validConstraint.EndTime.date, hour, min, sec,
                validConstraint.EndTime.time);
            sprintf(sOutput, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
            drmConstraints->put(&DrmConstraints::LICENSE_EXPIRY_TIME, sOutput);
            drmConstraints->put(&DrmConstraints::TYPE, "datetime");
    }

    if (0 != (uint8_t)(validConstraint.Indicator & DRM_INTERVAL_CONSTRAINT)) { /* Have interval time restrict? */
        changeDisplayTime(validConstraint.Interval.date, validConstraint.Interval.time, false, sOutput);
        /*INT_2_YMD_HMS(year, mon, day, validConstraint.Interval.date, hour, min, sec,
                validConstraint.Interval.time);
            sprintf(sOutput, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
            drmConstraints->put(&DrmConstraints::LICENSE_AVAILABLE_TIME, sOutput);
            drmConstraints->put(&DrmConstraints::TYPE, "interval");
    }
    free(pRo);
    return drmConstraints;
}


List<DrmConstraints*>* SeparateEngine::onGetConstraintsList(
        int uniqueId, const String8* path, int action){
    char uid[MAX_CONTENT_ID];
    int  typeLength,uidLength;
    int32_t id;
    int32_t roAmount;
    int32_t flag = DRM_FAILURE;
    int32_t i;
    T_DRM_Rights *pRo;
    int32_t iNum;
    T_DRM_Rights_Constraint * pCurConstraint;
    T_DRM_Rights_Constraint  validConstraint;
    int priority[8] = {1, 2, 4, 3, 8, 5, 7, 6};


    int des = open((*path).string(), O_RDONLY);
    if (-1 == des){
        TCTALOGW("LZH Open file failed ! [path=%s] ", (*path).string());
        return NULL;;
    }
     if (des < 0){
         return NULL;
     }
     if (-1 == lseek64(des, 0, SEEK_SET)){
         close(des);
         return NULL;
     }
        //if action is default choose an action for user. start
      if(action == Action::DEFAULT){
        String8 m = onGetOriginalMimeType(uniqueId, des);
                  action = MimeTypeUtil::getAction(m);
    }
     if (!getUidFromDrmFile(des, uid, &uidLength)){
         close(des);
         return NULL;
     }
     close(des);

    if (0 == drm_readFromUidTxt((uint8_t*)uid, &id, GET_ID)){
        return NULL;
    }

    if (0 == drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT)){
        return NULL;
    }

    if (roAmount < 1){
        return NULL;
    }

    pRo = (T_DRM_Rights*)malloc(roAmount * sizeof(T_DRM_Rights));
    if (NULL == pRo){
        return NULL;
    }

    if (0 == drm_writeOrReadInfo(id, pRo, &roAmount, GET_ALL_RO)) {
        free(pRo);
        return NULL;
    }

    /** check the right priority */
    List<DrmConstraints*>* drmConstraintsList = new List<DrmConstraints*>();
    for(i = 0; i < roAmount; i++) {
        char sOutputs[64];
        DrmConstraints* tmdrmConstraints =  new DrmConstraints();
        switch(action) {
            case Action::PLAY:
                pCurConstraint = &pRo[i].PlayConstraint;
                break;
            case Action::DISPLAY:
                pCurConstraint = &pRo[i].DisplayConstraint;
                break;
            case Action::EXECUTE:
                pCurConstraint = &pRo[i].ExecuteConstraint;
                break;
            case Action::PRINT:
                pCurConstraint = &pRo[i].PrintConstraint;
                break;
            default:
               free(pRo);
              drmConstraintsList->clear();
               delete tmdrmConstraints;
               delete drmConstraintsList;
               return NULL;
            }
        sprintf(sOutputs, "%s", (char*) pRo->Version);
        tmdrmConstraints->put(&DrmConstraints::VERSION, sOutputs);
        tmdrmConstraints->put(&DrmConstraints::CONTENT_ID, uid);
        sprintf(sOutputs, "%d", action);
        tmdrmConstraints->put(&DrmConstraints::ACTION, sOutputs);
        if (0 != (uint8_t)(pCurConstraint->Indicator & DRM_NO_CONSTRAINT)) /* Have utter right? */
        {
            TCTALOGD("DRM_NO_CONSTRAINT!");
            delete tmdrmConstraints; tmdrmConstraints = NULL;
            continue;
        }
        //int year = 0, mon = 0, day = 0, hour = 0, min = 0, sec = 0;
        if (0 != (uint8_t)(pCurConstraint->Indicator & DRM_COUNT_CONSTRAINT)) { /* Have count restrict? */
           if (pCurConstraint->Count <= 0) {
               delete tmdrmConstraints; tmdrmConstraints = NULL;
               continue;
           }
           sprintf(sOutputs, "%04d", pCurConstraint->Count);
           tmdrmConstraints->put(&DrmConstraints::REMAINING_REPEAT_COUNT, sOutputs);
           tmdrmConstraints->put(&DrmConstraints::TYPE, "count");
        }

        if (0 != (uint8_t)(pCurConstraint->Indicator & DRM_START_TIME_CONSTRAINT)) {
            changeDisplayTime(pCurConstraint->StartTime.date, pCurConstraint->StartTime.time, true, sOutputs);
            /*INT_2_YMD_HMS(year, mon, day, pCurConstraint->StartTime.date, hour, min, sec,
               pCurConstraint->StartTime.time);
            sprintf(sOutputs, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
            tmdrmConstraints->put(&DrmConstraints::LICENSE_START_TIME, sOutputs);
            tmdrmConstraints->put(&DrmConstraints::TYPE, "datetime");
        }

        if (0 != (uint8_t)(pCurConstraint->Indicator & DRM_END_TIME_CONSTRAINT)) { /* Have end time restrict? */
            if(DRM_TIME_INVAILD == checkEndTimeExpired(pCurConstraint->EndTime.date, pCurConstraint->EndTime.time)){
                delete tmdrmConstraints; tmdrmConstraints = NULL;
                continue;
            }
            changeDisplayTime(pCurConstraint->EndTime.date, pCurConstraint->EndTime.time, true, sOutputs);
            /*INT_2_YMD_HMS(year, mon, day, pCurConstraint->EndTime.date, hour, min, sec,
                pCurConstraint->EndTime.time);
            sprintf(sOutputs, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
            tmdrmConstraints->put(&DrmConstraints::LICENSE_EXPIRY_TIME, sOutputs);
            tmdrmConstraints->put(&DrmConstraints::TYPE, "datetime");
        }

        if (0 != (uint8_t)(pCurConstraint->Indicator& DRM_INTERVAL_CONSTRAINT)) { /* Have interval time restrict? */
            changeDisplayTime(pCurConstraint->Interval.date, pCurConstraint->Interval.time, false, sOutputs);
            /*INT_2_YMD_HMS(year, mon, day, pCurConstraint->Interval.date, hour, min, sec,
                pCurConstraint->Interval.time);
              sprintf(sOutputs, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
            tmdrmConstraints->put(&DrmConstraints::LICENSE_AVAILABLE_TIME, sOutputs);
            tmdrmConstraints->put(&DrmConstraints::TYPE, "interval");
        }
        drmConstraintsList->push_back(tmdrmConstraints);
    }
    free(pRo);
    return drmConstraintsList;
}

DrmMetadata* SeparateEngine::onGetMetadata(int uniqueId, const String8* path){
    DrmMetadata* drmMetadata = NULL;
    int mFd=0;
    unsigned char* rawContent;
    int rawContentLen;
    T_DRM_DCF_Info dcfInfo;
    unsigned char* pEncData = NULL;
    const char *errmsg;


    if (NULL == path) {
            // Returns empty metadata to show no error condition.
        return NULL;
    }
    mFd = open((*path).string(), O_RDONLY);
    rawContentLen = getInputDataLength(mFd);
    if (rawContentLen < 0){
        close(mFd);
        return NULL;
        }
    if (rawContentLen > DRM_MAX_MALLOC_LEN)
       rawContentLen = DRM_MAX_MALLOC_LEN;


    rawContent = (uint8_t *)malloc(rawContentLen);
    if (NULL == rawContent){
       close(mFd);
       return NULL;
        }

    /* Read input data to buffer */
    if (0 >= readInputData(mFd, rawContent, rawContentLen)) {
        free(rawContent);
        close(mFd);
       return NULL;
    }
            close(mFd);

    memset(&dcfInfo, 0, sizeof(T_DRM_DCF_Info));
    if (0 == drm_dcfParser(rawContent, rawContentLen, &dcfInfo, &pEncData)) {
        free(rawContent);
       return NULL;
    }
            free(rawContent);

    drmMetadata = new DrmMetadata();
    if (drmMetadata ==NULL)
       return NULL;

    drmMetadata ->put(&SeparateEngine::CONTENT_TYPE, (char *)dcfInfo.ContentType);
    drmMetadata ->put(&SeparateEngine::CONTENT_UID, (char *)dcfInfo.ContentURI);
    drmMetadata ->put(&SeparateEngine::ENCRYPTION_METHOD, (char *)dcfInfo.Encryption_Method);
    drmMetadata ->put(&SeparateEngine::CONTENT_NAME, (char *)dcfInfo.Content_Name);
    drmMetadata ->put(&SeparateEngine::RIGHTS_ISSUER, (char *)dcfInfo.Rights_Issuer);
    drmMetadata ->put(&SeparateEngine::CONTENT_DESCRIPTION, (char *)dcfInfo.ContentDescription);
    drmMetadata ->put(&SeparateEngine::CONTENT_VENDOR, (char *)dcfInfo.ContentVendor);
    drmMetadata ->put(&SeparateEngine:: ICON_URI, (char *)dcfInfo.Icon_URI);
                      /**< Icon URI */
    return drmMetadata;
}

status_t SeparateEngine::onInitialize(int uniqueId) {

    return DRM_NO_ERROR;
}

status_t SeparateEngine::onSetOnInfoListener(
        int uniqueId, const IDrmEngine::OnInfoListener* infoListener){
    return DRM_NO_ERROR;
}

status_t SeparateEngine::onTerminate(int uniqueId){

    return DRM_NO_ERROR;
}

bool SeparateEngine::onCanHandle(int uniqueId, const String8& path){
    bool result = false;
    //because dowload file's extension name has been modified, so this is invalid
    //the Extension name of fl or sd or cd maybe is the same, so this is valid

        int mFd = open(path.string(), O_RDONLY);
        if (mFd < 0){
            return result;
        }
        result = onCanHandle(uniqueId, mFd);
        close(mFd);
    return result;
}

bool SeparateEngine::onCanHandle(int uniqueId, int fd){
    bool result = false;
    int des = dup(fd);
    if (des < 0){
        return result;
    }
    if (-1 == lseek64(des, 0, SEEK_SET)){
        close(des);
        return result;
    }
    //TODO should delete this code!
    //lseek(des, 0, SEEK_SET);
    int32_t rawContentLen = getInputDataLength(des);
    if (rawContentLen < 0){
        close(des);
        return result;
    }
    if (rawContentLen > DRM_MAX_MALLOC_LEN/10)
         rawContentLen = DRM_MAX_MALLOC_LEN/10;
    unsigned char *rawContent = (uint8_t *)malloc(rawContentLen);
    if (NULL == rawContent){
        close(des);
        return result;
    }
    //TODO here  we just need check the file are SD file or not,did we need the whole file's data?
    /* Read input data to buffer */
    if (0 >= readInputData(des, rawContent, rawContentLen)) {
        free(rawContent);
        close(des);
        return result;
    }
    result = drm_isSdFile(rawContent, rawContentLen);
    if (!result){
    }
    //TODO here  we just need check the file are SD file or not,did we need the whole file's data?
    free(rawContent);
    close(des);
    return result;
}

DrmInfoStatus* SeparateEngine::onProcessDrmInfo(int uniqueId, const DrmInfo* drmInfo){
    DrmInfoStatus *drmInfoStatus = NULL;

    // Nothing to process

    drmInfoStatus = new DrmInfoStatus((int)DrmInfoStatus::STATUS_OK, 0, NULL, String8(""));

    return drmInfoStatus;
}

status_t SeparateEngine::onSaveRights(int uniqueId, const DrmRights& drmRights,
        const String8& rightspath, const String8& contentPath){
    const char *errmsg;
    T_DRM_Rights rights;
    char * data;
    int length;

    data=drmRights.getData().data;
    length = drmRights.getData().length;
    memset(&rights, 0, sizeof(T_DRM_Rights));

    if(!((data[0] == 0x03) && (data[1] == 0x0E) && (data[2] == 0x6A))) {
          if (!drm_rightParser((uint8_t*)(data), length, TYPE_DRM_RIGHTS_XML, &rights)) {
           return DRM_FAILURE;
        }
    }
    else
        if (!drm_rightParser((uint8_t*)(data), length, TYPE_DRM_RIGHTS_WBXML, &rights)) {
           TCTALOGE("parser right wbxml: failed");
           return DRM_FAILURE;
        }



    if (rights.bIsPlayable) {
        printConstraintInfo(&rights.PlayConstraint);
    }
    if (rights.bIsDisplayable) {
        printConstraintInfo(&rights.DisplayConstraint);
    }
    if (rights.bIsExecuteable) {
        printConstraintInfo(&rights.ExecuteConstraint);
    }
    if (rights.bIsPrintable) {
        printConstraintInfo(&rights.PrintConstraint);
    }




    if (! drm_appendRightsInfo(&rights)) {
        return DRM_FAILURE;
    }
    return DRM_SUCCESS;
}

/**
 * @auther feng.wang@jrdcom.com
 */
void SeparateEngine::printConstraintInfo(T_DRM_Rights_Constraint *constraint) {

    if (NULL == constraint) {
        return;
    }

    int year = 0, month = 0, day = 0, hour = 0, min = 0, sec = 0;

    if (constraint->StartTime.date > 0 || constraint->StartTime.time > 0) {
        getDatatime(&constraint->StartTime, &year, &month, &day, &hour, &min, &sec);
    }

    if (constraint->EndTime.date > 0 || constraint->EndTime.time > 0) {
        getDatatime(&constraint->EndTime, &year, &month, &day, &hour, &min, &sec);
    }

    if (constraint->Interval.date > 0 || constraint->Interval.time > 0) {
        getDatatime(&constraint->Interval, &year, &month, &day, &hour, &min, &sec);
    }
}

/**
 * @auther feng.wang@jrdcom.com
 */
int SeparateEngine::getDatatime(T_DRM_DATETIME *dateTime, int *year, int *month, int *day,
        int *hour, int *min, int *sec) {

    if (NULL == dateTime || NULL == year || NULL == month || NULL == hour || NULL == min
            || NULL == sec) {
        return FALSE;
    }

    *year = dateTime->date / 10000;
    *month = (dateTime->date - *year * 10000) / 100;
    *day = dateTime->date % 100;

    *hour = dateTime->time / 10000;
    *min = (dateTime->time - *hour * 10000)/100;
    *sec = dateTime->time % 100;

    return TRUE;
}





DrmInfo* SeparateEngine::onAcquireDrmInfo(int uniqueId, const DrmInfoRequest* drmInforequest){
    return NULL;
}

String8 SeparateEngine::onGetOriginalMimeType(int uniqueId, const String8& path){
    String8 mimeString = String8("");
    //modify the code  according to CombianedEngine
TCTALOGE("LZH  onGetOriginalMimeType path = %s",path.string());
        int fd = open(path.string(), O_RDONLY);
		TCTALOGE("LZH fd = %d",fd);
    if (-1 != fd){
     mimeString =  onGetOriginalMimeType( uniqueId, fd);
     close(fd);
    }
    return mimeString;
}

String8 SeparateEngine::onGetOriginalMimeType(int uniqueId, const String8& path, int fileDesc) {
    TCTALOGE("LZH SeparateEngine::onGetOriginalMimeType");
    String8 mimeString = String8("");
    //int fileDesc = dup(fd);

    int fd = open(path.string(), O_RDONLY);
    if(-1 != fd){
        TCTALOGE("LZH onGetOriginalMimeType path = %s,fd = %d",path.string(),fd);
        mimeString = onGetOriginalMimeType( uniqueId, fd);
        TCTALOGE("LZH mimeString = %s",mimeString.string());
    }

    if(NULL != path){
        TCTALOGE("LZH onGetOriginalMimeType null!= path =%s",path.string());
        mimeString = onGetOriginalMimeType(uniqueId,path);
        TCTALOGE("LZH mimeString = %s",mimeString.string());
    }

    close(fd);
    return mimeString;
}

String8 SeparateEngine::onGetOriginalMimeType(int uniqueId, int fd){
    off64_t location = lseek64(fd, 0, SEEK_CUR);
    String8 mimeString = String8("");
    char *contentType = NULL;
    char fileHeader[3];
    if(read(fd,fileHeader,3) == 3){
        int contentTypeLen = fileHeader[1];
        //TODO check this resource can recycle.
        char * contentType = (char *)malloc( sizeof(char) * (contentTypeLen +1));
        if (NULL != contentType){
            if(read(fd,contentType,contentTypeLen) == contentTypeLen){
                contentType[contentTypeLen] = '\0';
                String8 srcMimeType = String8(contentType);
                mimeString = MimeTypeUtil::convertMimeType(srcMimeType);
            }
            free(contentType);
        }
    }
    if (location >= 0){
        lseek64(fd, location, SEEK_SET);
    }
 return mimeString;
}


int SeparateEngine::onGetDrmObjectType(
            int uniqueId, const String8& path, const String8& mimeType){
    String8 mimeStr = String8(mimeType);

    mimeStr.toLower();

    /* Checks whether
    * 1. path and mime type both are not empty strings (meaning unavailable) else content is unknown
    * 2. if one of them is empty stonGetSupportInforing and if other is known then its a DRM Content Object.
    * 3. if both of them are available, then both may be of known type
    *    (regardless of the relation between them to make it compatible with other DRM Engines)
    */
    if (((0 == path.length()) || onCanHandle(uniqueId, path)) &&
        ((0 == mimeType.length()) || (mimeStr == String8(SEPERATE_MIMETYPE_SD))) && (mimeType != path) ) {
            return DrmObjectType::CONTENT;
    }

    return DrmObjectType::UNKNOWN;
}

/**
 * @auther jian.gu@tct-nj.com
 //TODO maybe this method should return the UID or NULL
 */
static bool getUidFromDrmFile(const int fd, char *uid, int32_t *uidLength) {
    int result = FALSE;
    char *head = (char *) malloc(MAX_CONTENT_ID * 2);
    if(head == NULL){
     return result;
    }
    //int32_t uidLength = 0;
    //head[0] are version code = 1;
    int32_t typeLength = 0;
    if (read(fd, head, FILEHEADER) == FILEHEADER) {
        typeLength = head[1];
        *uidLength = head[2];
        if (*uidLength < MAX_CONTENT_ID) {
            memcpy(uid, &head[3 + typeLength], *uidLength);
            uid[*uidLength] = '\0';
            result = TRUE;
        } else {
        }
    }else {
    }
    free(head);
    return result;
}


int SeparateEngine::onCheckRightsStatus(int uniqueId, const String8& path, int action){
     int32_t res = RightsStatus::RIGHTS_INVALID;



    int fd = open(path.string(), O_RDONLY);
    if (-1 == fd){
        TCTALOGW("LZH Open file failed ! [path=%s] ", path.string());
        return res;
    }

    res = onCheckRightsStatus(uniqueId, fd, action);
    close(fd);
    return res;
}

int SeparateEngine::onCheckRightsStatus(int uniqueId,const int fd,int action){
//TODO maybe we just think the file that the fd point are drm file.
//the caller that call this mehod should make check the file are drm or not
    int32_t res = RightsStatus::RIGHTS_INVALID;
    if (!onCanHandle(uniqueId,fd)){
        return RightsStatus::RIGHTS_VALID;
    }

    //fix sd file can't transfer
    if(action ==Action::TRANSFER){
      return RightsStatus::RIGHTS_VALID;
    }

    char uid[MAX_CONTENT_ID];
    int  typeLength, uidLength;
    int32_t id;
    T_DRM_Rights *pRo, *pCurRo;
    int32_t roAmount;
    int32_t i;
    const char *errmsg;
    int peddingFlag=0;
    int expireFlag=0;
    int des = dup(fd);
    if (des < 0){
        TCTALOGW("onCheckRightsStatus() dup failed!");
        return res;
    }
    if (-1 == lseek64(des, 0, SEEK_SET)){
        close(des);
        TCTALOGW("onCheckRightsStatus() lseek64 failed!");
        return res;
    }
    //pengfei.zhong@jrdcom.com-PR300460 --- if action is default choose an action for user. start
     if(action == Action::DEFAULT){
        String8 m = onGetOriginalMimeType(uniqueId, des);
                 action = MimeTypeUtil::getAction(m);
   }
    //pengfei.zhong@jrdcom.com-PR300460 ---end

  if (!getUidFromDrmFile(des, uid, &uidLength)){
        close(des);
        TCTALOGW("onCheckRightsStatus() getUidFromDrmFile failed!");
        return RightsStatus::RIGHTS_INVALID;
    }
    close(des);
    /* The following will SD other permissions */
    if (!drm_readFromUidTxt((uint8_t*)uid, &id, GET_ID))
    {
        TCTALOGW("LZH SeparateEngine.cpp Get drm uid from txt fail for file %d", fd);
        return RightsStatus::RIGHTS_INVALID;
    }
    drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT);
    TCTALOGD("onCheckRightsStatus() roAmount=[%d]", roAmount);
    if (roAmount <= 0)
    {
        return RightsStatus::RIGHTS_NOT_ACQUIRED;
    }

    pRo = (T_DRM_Rights *)malloc(roAmount * sizeof(T_DRM_Rights));
    if (NULL == pRo)
    {
        TCTALOGW("onCheckRightsStatus() malloc pRo failed!");
        return RightsStatus::FAILURE;
    }
    drm_writeOrReadInfo(id, pRo, &roAmount, GET_ALL_RO);

    pCurRo = pRo;

    for (i = 0; i < roAmount; i++) {
        switch (action) {
            case Action::PLAY:
                res = drm_startCheckRights(&(pCurRo->bIsPlayable), &(pCurRo->PlayConstraint));
                   break;
            case Action::DISPLAY:
                res = drm_startCheckRights(&(pCurRo->bIsDisplayable), &(pCurRo->DisplayConstraint));
                break;
            case Action::EXECUTE:
                res = drm_startCheckRights(&(pCurRo->bIsExecuteable), &(pCurRo->ExecuteConstraint));
                break;
            case Action::PRINT:
                res = drm_startCheckRights(&(pCurRo->bIsPrintable), &(pCurRo->PrintConstraint));
                break;
            default:
                free(pRo);
                return RightsStatus::FAILURE;
        }
        if(res==DRM_SUCCESS){
            free(pRo);
            return RightsStatus::RIGHTS_VALID;
        }
        else if (res==DRM_RIGHTS_PENDING){
            peddingFlag=1;
        }
        else if (res==DRM_RIGHTS_EXPIRED){
            expireFlag=1;
        }
        pCurRo++;
    }

    free(pRo);
    if(peddingFlag==1){
        return RightsStatus::RIGHTS_PENDING;
    }
    else if(expireFlag==1){
        return RightsStatus::RIGHTS_EXPIRED;
    }
     else{
        return RightsStatus::FAILURE;
    }
}

status_t SeparateEngine::onConsumeRights(int uniqueId, DecryptHandle* decryptHandle,
                int action, bool reserve){
    int ret = DRM_FAILURE;
    // [drm] jian.gu@tct-nj.com
    if (NULL != decryptHandle && decodeSessionMap.isCreated(decryptHandle->decryptId)){
        DecodeSession *session = decodeSessionMap.getValue(decryptHandle->decryptId);
        if (session) {
            //  ret = SVC_drm_consumeRights(session->mSessionID, action);
            T_DRM_Session_Node* s;
            int32_t id;
            if (session < 0){
                return DRM_FAILURE;
            }
            s = getSession(session->mSessionID);
            if (NULL == s){
                return DRM_SESSION_NOT_OPENED;
            }
            if (DRM_PERMISSION_FORWARD == action) {
                if (SEPARATE_DELIVERY == s->deliveryMethod){
                    return DRM_SUCCESS;
                }
                return DRM_FAILURE;
            }
            if (FORWARD_LOCK == s->deliveryMethod) /* Forwardlock type have utter rights */
                return DRM_SUCCESS;
            if (FALSE == drm_readFromUidTxt(s->contentID, &id, GET_ID))
                return DRM_FAILURE;

            ret = drm_checkRoAndUpdate(id, action);
        }
    }
    return ret;
}

#ifdef USE_64BIT_DRM_API
status_t SeparateEngine::onSetPlaybackStatus(int uniqueId,
                             DecryptHandle* decryptHandle,
                             int playbackStatus,
                             int64_t position){
#else
status_t SeparateEngine::onSetPlaybackStatus(int uniqueId,
                             DecryptHandle* decryptHandle,
                             int playbackStatus,
                             int position){
#endif
    return DRM_NO_ERROR;
}

bool SeparateEngine::onValidateAction(int uniqueId, const String8& path,
            int action, const ActionDescription& description){

    // For the forwardlock engine checkRights and ValidateAction are the same.
    return (onCheckRightsStatus(uniqueId, path, action) == RightsStatus::RIGHTS_VALID);
}

status_t SeparateEngine::onRemoveRights(int uniqueId, const String8& path){
    // No Rights to remove
    char* fileheader=(char *)malloc(FILEHEADER);
    char *uid;
    int  typeLength,uidLength;
    const char *errmsg;

    FILE *fp;
    if ((fp=fopen(path,"rb"))!=NULL)
    {
        if (fread(fileheader, 1, FILEHEADER, fp) >= 3){
            typeLength=fileheader[1];
            uidLength=fileheader[2];
            uid=&fileheader[3+typeLength];
            uid[uidLength]='\0';
        }
        else
           {
            free(fileheader);
            fclose(fp);
            return DRM_FAILURE;
        }
    }
    else
    {
        free(fileheader);
        return DRM_FAILURE;
    }
    fclose(fp);

    if (!SVC_drm_deleteRights((uint8_t*)uid)){
        free(fileheader);
        return DRM_FAILURE;
    }

    free(fileheader);
    return DRM_SUCCESS;
}

status_t SeparateEngine::onRemoveAllRights(int uniqueId){
     // No rights to remove
     int id,maxId;
     const char *errmsg;

     maxId = drm_getMaxIdFromUidTxt();
     if (-1 == maxId)
     {
        return DRM_SUCCESS;
     }

     for (id = 1; id <= maxId; id++) {
          drm_removeIdInfoFile(id);
          drm_updateUidTxtWhenDelete(id);
     }
     return DRM_SUCCESS;
}

status_t SeparateEngine::onOpenConvertSession(int uniqueId, int convertId){

    return DRM_NO_ERROR;
}

DrmConvertedStatus* SeparateEngine::onConvertData(
            int uniqueId, int convertId, const DrmBuffer* inputData){
    SeparateConv_Status_t retStatus = SeparateConv_Status_InvalidArgument;
    DrmBuffer *convResult = new DrmBuffer(NULL, 0);
    int offset = -1;
    return new DrmConvertedStatus(getConvertedStatus(retStatus), convResult, offset);
}

DrmConvertedStatus* SeparateEngine::onCloseConvertSession(int uniqueId, int convertId){
    SeparateConv_Status_t retStatus = SeparateConv_Status_InvalidArgument;
    DrmBuffer *convResult = new DrmBuffer(NULL, 0);
    int offset = -1;
    return new DrmConvertedStatus(getConvertedStatus(retStatus), convResult, offset);
}

DrmSupportInfo* SeparateEngine::onGetSupportInfo(int uniqueId){
    DrmSupportInfo* pSupportInfo = new DrmSupportInfo();

    if (NULL != pSupportInfo) {
        pSupportInfo->addFileSuffix(String8(SEPERATE_DOTEXTENSION_DCF));
        pSupportInfo->addMimeType(String8(SEPERATE_DRM_TYPE ));
        pSupportInfo->addMimeType(String8(SEPERATE_RIGHT_MIME_TYPE_1));
        pSupportInfo->addMimeType(String8(SEPERATE_RIGHT_MIME_TYPE_2));
        pSupportInfo->addMimeType(String8(SEPERATE_CONTENT_MIMETYPE));

        pSupportInfo->setDescription(String8(SEPERATE_DESCRIPTION));
    }

    return pSupportInfo;
}

#ifdef USE_64BIT_DRM_API
status_t SeparateEngine::onOpenDecryptSession(int uniqueId,
                              DecryptHandle* decryptHandle,
                              int fd, off64_t offset, off64_t length){
#else
status_t SeparateEngine::onOpenDecryptSession(int uniqueId,
                              DecryptHandle* decryptHandle,
                              int fd, int offset, int length){
#endif
    status_t result = DRM_ERROR_CANNOT_HANDLE;
    int fileDesc = -1;
    T_DRM_Input_Data data;

    if ((-1 < fd) && (NULL != decryptHandle) &&
            !decodeSessionMap.isCreated(decryptHandle->decryptId)){
        fileDesc = dup(fd);
    } else {
        return result;
    }

    data.inputHandle = fileDesc;
    data.mimeType = TYPE_DRM_CONTENT;
    data.getInputDataLength = getInputDataLength;
    data.readInputData=readInputData;
    data.seekInputData=seekInputData;


    if (-1 < fileDesc &&
        -1 < ::lseek(fileDesc, offset, SEEK_SET)) {

        decryptHandle->mimeType = onGetOriginalMimeType(uniqueId, fileDesc);
        ::lseek(fileDesc, 0, SEEK_SET);

        int session = SVC_drm_openSession(data);
        if (session < 0) {
            result = session;
        } else {
            DecodeSession* decodesession = new DecodeSession(session, fileDesc);
            if (decodesession) {
                decodeSessionMap.addValue(decryptHandle->decryptId, decodesession);
                decryptHandle->decryptApiType = DecryptApiType::CONTAINER_BASED;
                decryptHandle->status = RightsStatus::RIGHTS_VALID;
                decryptHandle->decryptInfo = NULL;
                result = DRM_NO_ERROR;
            }
        }
    }
    if (DRM_NO_ERROR != result && -1 < fileDesc) {
        ::close(fileDesc);
    }


    return result;
}

status_t SeparateEngine::onOpenDecryptSession(
            int uniqueId, DecryptHandle* decryptHandle, const char* uri){
    status_t result = DRM_ERROR_CANNOT_HANDLE;
    // we already convert uri to filepath in DrmManagerClient.cpp
    if (NULL != decryptHandle && NULL != uri && onCanHandle(uniqueId, String8(uri))) {
        int fd = open(uri, O_RDONLY);
        if (-1 < fd) {
            // offset is always 0 and length is not used. so any positive size.
            result = onOpenDecryptSession(uniqueId, decryptHandle, fd, 0, 1);
            // fd is duplicated already if success. closing the file
            close(fd);
        }
    }
    return result;
}

status_t SeparateEngine::onCloseDecryptSession(int uniqueId, DecryptHandle* decryptHandle){
    status_t result = DRM_FAILURE;
    if (NULL != decryptHandle && decodeSessionMap.isCreated(decryptHandle->decryptId)){
        DecodeSession* session = decodeSessionMap.getValue(decryptHandle->decryptId);
        if (session) {
            result = SVC_drm_closeSession(session->mSessionID);
            if (session->mFd > 0) {
                close(session->mFd);
            }
            decodeSessionMap.removeValue(decryptHandle->decryptId);
        }
    }

    if (NULL != decryptHandle) {
        if (NULL != decryptHandle->decryptInfo) {
            delete decryptHandle->decryptInfo;
            decryptHandle->decryptInfo = NULL;
        }

        decryptHandle->copyControlVector.clear();
        decryptHandle->extendedData.clear();

        delete decryptHandle;
        decryptHandle = NULL;
    }

    return result;
}

status_t SeparateEngine::onInitializeDecryptUnit(int uniqueId, DecryptHandle* decryptHandle,
            int decryptUnitId, const DrmBuffer* headerInfo){
    return DRM_NO_ERROR;
}

status_t SeparateEngine::onDecrypt(int uniqueId, DecryptHandle* decryptHandle, int decryptUnitId,
            const DrmBuffer* encBuffer, DrmBuffer** decBuffer, DrmBuffer* IV){
    return DRM_NO_ERROR;
}

status_t SeparateEngine::onFinalizeDecryptUnit(
            int uniqueId, DecryptHandle* decryptHandle, int decryptUnitId){
    return DRM_NO_ERROR;
}

#ifdef USE_64BIT_DRM_API
ssize_t SeparateEngine::onPread(int uniqueId,
                DecryptHandle* decryptHandle,
                void* buffer,
                ssize_t numBytes,
                off64_t offset){
#else
ssize_t SeparateEngine::onPread(int uniqueId,
                DecryptHandle* decryptHandle,
                void* buffer,
                ssize_t numBytes,
                off_t offset){
#endif
    ssize_t bytesRead = -1;
    if (NULL == decryptHandle || !decodeSessionMap.isCreated(decryptHandle->decryptId)) {
        return DRM_FAILURE;
    }

    DecodeSession* session = decodeSessionMap.getValue(decryptHandle->decryptId);
    if (session) {
        bytesRead = SVC_drm_getContent(session->mSessionID, offset, (uint8_t*) buffer, numBytes);

        if (bytesRead == DRM_MEDIA_EOF){
            bytesRead = 0;
        }
    }
    TCTALOGD("the read bytes to decrypt are ,the decrypted bytes are ");
    return bytesRead;
}
