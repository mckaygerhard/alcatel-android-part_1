/*************************************************************************/
/*                                                               Date:11/2012                                               */
/*                                PRESENTATION                                                                            */
/*                                                                                                                                 */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.                                    */
/*                                                                                                                                 */
/* This material is company confidential, cannot be reproduced in any form                                */
/* without the written permission of TCL Communication Technology Holdings                              */
/* Limited.                                                                                                                       */
/*                                                                                                                                  */
/* ---------------------------------------------------------------------------------*/
/*  Author :  Zonghui.Li                                                                                                       */
/*  Email  :  Zonghui.Li@tcl-mobile.com                                                                                  */
/*  Role   :                                                                                                                        */
/*  Reference documents :                                                                                                   */
/* ----------------------------------------------------------------------------------*/
/*  Comments :                                                                                                                   */
/*  File     :                                                                                                                        */
/*  Labels   :                                                                                                                      */
/* ----------------------------------------------------------------------------------*/
/* =========================================================================*/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

#include "drm_sd_api.h"


/**
 * Current id.
 */
static int32_t curID = 0;
/**
 * The header pointer for the session list.
 */
static T_DRM_Session_Node* sessionTable = NULL;

/**
 * New a session.
 */
T_DRM_Session_Node* newSession(T_DRM_Input_Data data)
{
    T_DRM_Session_Node* s = (T_DRM_Session_Node *)malloc(sizeof(T_DRM_Session_Node));

    if (NULL != s) {
        memset(s, 0, sizeof(T_DRM_Session_Node));

        s->sessionId = curID++;
        s->inputHandle = data.inputHandle;
        s->mimeType = data.mimeType;
        s->getInputDataLengthFunc = data.getInputDataLength;
        s->readInputDataFunc = data.readInputData;
        s->seekInputDataFunc = data.seekInputData;
    }

    return s;
}

/**
 * Free a session.
 */
void freeSession(T_DRM_Session_Node* s)
{
    if (NULL == s)
        return;

    if (NULL != s->rawContent)
        free(s->rawContent);

    if (NULL != s->readBuf)
        free(s->readBuf);

    if (NULL != s->infoStruct)
        free(s->infoStruct);

    free(s);
}

/**
 * Add a session to list.
 */
int32_t addSession(T_DRM_Session_Node* s)
{
    if (NULL == s)
        return -1;

    s->next = sessionTable;
    sessionTable = s;

    return s->sessionId;
}

/**
 * Get a session from the list.
 */
T_DRM_Session_Node* getSession(int32_t sessionId)
{
    T_DRM_Session_Node* s;

    if (sessionId < 0 || NULL == sessionTable)
        return NULL;

    for (s = sessionTable; s != NULL; s = s->next) {
        if (sessionId == s->sessionId)
            return s;
    }

    return NULL;
}

/**
 * Remove a session from the list.
 */
void removeSession(int32_t sessionId)
{
    T_DRM_Session_Node *curS, *preS;

    if (sessionId < 0 || NULL == sessionTable)
        return;

    if (sessionId == sessionTable->sessionId) {
        curS = sessionTable;
        sessionTable = curS->next;
        freeSession(curS);
        return;
    }

    for (preS = sessionTable; preS->next != NULL; preS = preS->next) {
        if (preS->next->sessionId == sessionId)
            curS = preS->next;
    }

    if (NULL == preS->next)
        return;

    preS->next = curS->next;
    freeSession(curS);
}

/**
 * Try to identify the mimetype according the input DRM data.
 */
int32_t getMimeType(const uint8_t *buf, int32_t bufLen)
{
    const uint8_t *p;

    if (NULL == buf || bufLen <= 0)
        return TYPE_DRM_UNKNOWN;

    p = buf;

    /* check if it is DRM Content Format, only check the first field of Version, it must be "0x01" */
    if (0x01 == *p)
        return TYPE_DRM_CONTENT;

    /* check if it is DRM Message, only check the first two bytes, it must be the start flag of boundary: "--" */
    if (bufLen >= 2 && '-' == *p && '-' == *(p + 1))
        return TYPE_DRM_MESSAGE;

    /* check if it is DRM Rights XML format, only check the first several bytes, it must be: "<o-ex:rights" */
    if (bufLen >= 12 && 0 == strncmp("<o-ex:rights", (char *)p, 12))
        return TYPE_DRM_RIGHTS_XML;

    /* check if it is DRM Rights WBXML format, only check the first two bytes, it must be: 0x03, 0x0e */
    if (bufLen >= 2 && 0x03 == *p && 0x0e == *(p + 1))
        return TYPE_DRM_RIGHTS_WBXML;

    return TYPE_DRM_UNKNOWN;
}


