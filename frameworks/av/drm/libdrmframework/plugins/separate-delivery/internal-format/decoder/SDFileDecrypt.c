/*************************************************************************/
/*                                                               Date:11/2012                                               */
/*                                PRESENTATION                                                                            */
/*                                                                                                                                 */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.                                    */
/*                                                                                                                                 */
/* This material is company confidential, cannot be reproduced in any form                                */
/* without the written permission of TCL Communication Technology Holdings                              */
/* Limited.                                                                                                                       */
/*                                                                                                                                  */
/* ---------------------------------------------------------------------------------*/
/*  Author :  Zonghui.Li                                                                                                       */
/*  Email  :  Zonghui.Li@tcl-mobile.com                                                                                  */
/*  Role   :                                                                                                                        */
/*  Reference documents :                                                                                                   */
/* ----------------------------------------------------------------------------------*/
/*  Comments :                                                                                                                   */
/*  File     :                                                                                                                        */
/*  Labels   :                                                                                                                      */
/* ----------------------------------------------------------------------------------*/
/* =========================================================================*/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

#include "SDFileDecrypt.h"
#include "parser_dcf.h"
#include "drm_sd_api.h"
#define TCT_DEBUG_LOG 0
#ifdef TCT_DEBUG_LOG
#define TCTALOGE ALOGE
#define TCTALOGD ALOGD
#define TCTALOGW ALOGW
#else
#define TCTALOGE(a...) do { } while(0)
#define TCTALOGD(a...) do { } while(0)
#define TCTALOGW(a...) do { } while(0)
#endif

#define LOG_NDEBUG 0
#define LOG_TAG "SDFileDecrypt"

#include <utils/Log.h>

void drm_discardPaddingByte(uint8_t *decryptedBuf, int32_t *decryptedBufLen)
{
    int32_t tmpLen = *decryptedBufLen;
    int32_t i;

    if (NULL == decryptedBuf || *decryptedBufLen < 0)
        return;

    /* Check whether the last several bytes are padding or not */
    for (i = 1; i < decryptedBuf[tmpLen - 1]; i++) {
        if (decryptedBuf[tmpLen - 1 - i] != decryptedBuf[tmpLen - 1])
            break; /* Not the padding bytes */
    }
    if (i == decryptedBuf[tmpLen - 1]) {/* They are padding bytes */
        *decryptedBufLen = tmpLen - i;
        //TCTALOGD("drm_discardPaddingByte i = %d", i);
    }
    return;
}

int32_t drm_aesDecBuffer(uint8_t * Buffer, int32_t * BufferLen, AES_KEY *key)
{
    uint8_t dbuf[3 * DRM_ONE_AES_BLOCK_LEN], buf[DRM_ONE_AES_BLOCK_LEN];
    uint64_t i, len, wlen = DRM_ONE_AES_BLOCK_LEN, curLen, restLen;
    uint8_t *pTarget, *pTargetHead;

    pTargetHead = Buffer;
    pTarget = Buffer;
    curLen = 0;
    restLen = *BufferLen;

    if (restLen > 2 * DRM_ONE_AES_BLOCK_LEN) {
        len = 2 * DRM_ONE_AES_BLOCK_LEN;
    } else {
        len = restLen;
    }
    memcpy(dbuf, Buffer, (size_t)len);
    restLen -= len;
    Buffer += len;

    if (len < 2 * DRM_ONE_AES_BLOCK_LEN) { /* The original file is less than one block in length */
        len -= DRM_ONE_AES_BLOCK_LEN;
        /* Decrypt from position len to position len + DRM_ONE_AES_BLOCK_LEN */
        AES_decrypt((dbuf + len), (dbuf + len), key);

        /* Undo the CBC chaining */
        for (i = 0; i < len; ++i)
            dbuf[i] ^= dbuf[i + DRM_ONE_AES_BLOCK_LEN];

        /* Output the decrypted bytes */
        memcpy(pTarget, dbuf, (size_t)len);
        pTarget += len;
    } else {
        uint8_t *b1 = dbuf, *b2 = b1 + DRM_ONE_AES_BLOCK_LEN, *b3 = b2 + DRM_ONE_AES_BLOCK_LEN, *bt;

        for (;;) { /* While some ciphertext remains, prepare to decrypt block b2 */
            /* Read in the next block to see if ciphertext stealing is needed */
            b3 = Buffer;
            if (restLen > DRM_ONE_AES_BLOCK_LEN) {
                len = DRM_ONE_AES_BLOCK_LEN;
            } else {
                len = restLen;
            }
            restLen -= len;
            Buffer += len;

            /* Decrypt the b2 block */
            AES_decrypt((uint8_t *)b2, buf, key);

            if (len == 0 || len == DRM_ONE_AES_BLOCK_LEN) { /* No ciphertext stealing */
                /* Unchain CBC using the previous ciphertext block in b1 */
                for (i = 0; i < DRM_ONE_AES_BLOCK_LEN; ++i)
                    buf[i] ^= b1[i];
            } else { /* Partial last block - use ciphertext stealing */
                wlen = len;
                /* Produce last 'len' bytes of plaintext by xoring with */
                /* The lowest 'len' bytes of next block b3 - C[N-1] */
                for (i = 0; i < len; ++i)
                    buf[i] ^= b3[i];

                /* Reconstruct the C[N-1] block in b3 by adding in the */
                /* Last (DRM_ONE_AES_BLOCK_LEN - len) bytes of C[N-2] in b2 */
                for (i = len; i < DRM_ONE_AES_BLOCK_LEN; ++i)
                    b3[i] = buf[i];

                /* Decrypt the C[N-1] block in b3 */
                AES_decrypt((uint8_t *)b3, (uint8_t *)b3, key);

                /* Produce the last but one plaintext block by xoring with */
                /* The last but two ciphertext block */
                for (i = 0; i < DRM_ONE_AES_BLOCK_LEN; ++i)
                    b3[i] ^= b1[i];

                /* Write decrypted plaintext blocks */
                memcpy(pTarget, b3, DRM_ONE_AES_BLOCK_LEN);
                pTarget += DRM_ONE_AES_BLOCK_LEN;
            }

            /* Write the decrypted plaintext block */
            memcpy(pTarget, buf, (size_t)wlen);
            pTarget += wlen;

            if (len != DRM_ONE_AES_BLOCK_LEN) {
                *BufferLen = pTarget - pTargetHead;
                return 0;
            }

            /* Advance the buffer pointers */
            bt = b1, b1 = b2, b2 = b3, b3 = bt;
        }
    }
    return 0;
}


int32_t drm_updateDcfDataLen(uint8_t* pDcfLastData, uint8_t* keyValue, int32_t* moreBytes)
{
    AES_KEY key;
    int32_t len = DRM_TWO_AES_BLOCK_LEN;

    if (NULL == pDcfLastData || NULL == keyValue)
        return FALSE;

    AES_set_decrypt_key(keyValue, DRM_KEY_LEN * 8, &key);

    if (drm_aesDecBuffer(pDcfLastData, &len, &key) < 0)
        return FALSE;

    drm_discardPaddingByte(pDcfLastData, &len);

    *moreBytes = DRM_TWO_AES_BLOCK_LEN - len;

    return TRUE;
}


int32_t SVC_drm_openSession(T_DRM_Input_Data data)
{
    int32_t session;
    int32_t dataLen;
    T_DRM_Session_Node* s;

    if (0 == data.inputHandle)
        return DRM_MEDIA_DATA_INVALID;

    /* Get input data length */
    dataLen = data.getInputDataLength(data.inputHandle);
    if (dataLen <= 0)
        return DRM_MEDIA_DATA_INVALID;

    s = newSession(data);
    if (NULL == s)
        return DRM_FAILURE;

    /* Check if the length is larger than DRM max malloc length */
    if (dataLen > DRM_MAX_MALLOC_LEN)
        s->rawContentLen = DRM_MAX_MALLOC_LEN;
    else
        s->rawContentLen = dataLen;

    s->rawContent = (uint8_t *)malloc(s->rawContentLen);
    if (NULL == s->rawContent) {
        freeSession(s);
        return DRM_FAILURE;
    }

    /* Read input data to buffer */
    if (0 >= data.readInputData(data.inputHandle, s->rawContent, s->rawContentLen)) {
        freeSession(s);
        return DRM_MEDIA_DATA_INVALID;
    }

    /* if the input mime type is unknown, DRM engine will try to recognize it. */
    if (TYPE_DRM_UNKNOWN == data.mimeType)
        data.mimeType = getMimeType(s->rawContent, s->rawContentLen);

    if (TYPE_DRM_CONTENT == data.mimeType){
        T_DRM_DCF_Info dcfInfo;
        uint8_t* pEncData = NULL;

        memset(&dcfInfo, 0, sizeof(T_DRM_DCF_Info));
        if (FALSE == drm_dcfParser(s->rawContent, s->rawContentLen, &dcfInfo, &pEncData)) {
            freeSession(s);
            return DRM_MEDIA_DATA_INVALID;
        }

        s->infoStruct = (T_DRM_Dcf_Node *)malloc(sizeof(T_DRM_Dcf_Node));
        if (NULL == s->infoStruct) {
            freeSession(s);
            return DRM_FAILURE;
        }

        memset(s->infoStruct, 0, sizeof(T_DRM_Dcf_Node));

        s->deliveryMethod = SEPARATE_DELIVERY;
        s->contentLength = dcfInfo.DecryptedDataLen;
        ((T_DRM_Dcf_Node *)(s->infoStruct))->encContentLength = dcfInfo.EncryptedDataLen;
        s->contentOffset = pEncData - s->rawContent;
        strcpy((char *)s->contentType, (char *)dcfInfo.ContentType);
        strcpy((char *)s->contentID, (char *)dcfInfo.ContentURI);
        strcpy((char *)((T_DRM_Dcf_Node *)(s->infoStruct))->rightsIssuer, (char *)dcfInfo.Rights_Issuer);

    } else {
       freeSession(s);
       return DRM_MEDIA_DATA_INVALID;
    }

    if ((/*SEPARATE_DELIVERY_FL == s->deliveryMethod || */SEPARATE_DELIVERY == s->deliveryMethod) &&
        s->contentOffset + ((T_DRM_Dcf_Node *)(s->infoStruct))->encContentLength <= DRM_MAX_MALLOC_LEN) {
        uint8_t keyValue[DRM_KEY_LEN];
        uint8_t lastDcfBuf[DRM_TWO_AES_BLOCK_LEN];
        int32_t seekPos, moreBytes;
        //xiaoqin.zhou@jrdcom.com fr303132 because of moving code it is caused.
        if (TRUE == drm_getKey(s->contentID, keyValue)) {
            seekPos = s->contentOffset + ((T_DRM_Dcf_Node *)(s->infoStruct))->encContentLength - DRM_TWO_AES_BLOCK_LEN;
            memcpy(lastDcfBuf, s->rawContent + seekPos, DRM_TWO_AES_BLOCK_LEN);

            if (TRUE == drm_updateDcfDataLen(lastDcfBuf, keyValue, &moreBytes)) {
                s->contentLength = ((T_DRM_Dcf_Node *)(s->infoStruct))->encContentLength;
                s->contentLength -= moreBytes;
            }
        }
    }

    session = addSession(s);
    if (-1 == session) {
        freeSession(s);
        return DRM_FAILURE;
    }

    return session;
}

int32_t SVC_drm_closeSession(int32_t session)
{
    if (session < 0)
        return DRM_FAILURE;

    if (NULL == getSession(session))
        return DRM_SESSION_NOT_OPENED;

    removeSession(session);

    return DRM_SUCCESS;
}

int32_t SVC_drm_getContent(int32_t session, int32_t offset, uint8_t* mediaBuf, int32_t mediaBufLen)
{
    T_DRM_Session_Node* s;
    int32_t readBytes;

    if (session < 0 || offset < 0 || NULL == mediaBuf || mediaBufLen <= 0)
        return DRM_FAILURE;

    s = getSession(session);
    if (NULL == s)
        return DRM_SESSION_NOT_OPENED;

    if (0 >= s->getInputDataLengthFunc(s->inputHandle))
        return DRM_MEDIA_DATA_INVALID;

    if (s->deliveryMethod == SEPARATE_DELIVERY){
        readBytes = drm_readAesContent(s, offset, mediaBuf, mediaBufLen);
    } else {
        return DRM_FAILURE;
    }

    return readBytes;
}

int32_t drm_readAesContent(T_DRM_Session_Node* s, int32_t offset, uint8_t* mediaBuf, int32_t mediaBufLen)
{
    uint8_t keyValue[DRM_KEY_LEN] = {0};
    uint8_t buf[DRM_TWO_AES_BLOCK_LEN];
    int32_t readBytes = 0;
    int32_t bufLen, piece, i, copyBytes, leftBytes;
    int32_t aesStart, mediaStart, mediaBufOff;
    AES_KEY key;

    if(!memcmp ( s->keyValue, keyValue, DRM_KEY_LEN)){
        if (FALSE == drm_getKey(s->contentID, s->keyValue))
            return DRM_NO_RIGHTS;
    }
    memcpy(keyValue , s->keyValue, DRM_KEY_LEN);

    /* when the content length has been well-known */
    if (s->contentLength > 0) {
        if (offset > s->contentLength)
            return DRM_FAILURE;

        if (offset == s->contentLength)
            return DRM_MEDIA_EOF;

        if (offset + mediaBufLen > s->contentLength)
            readBytes = s->contentLength - offset;
        else
            readBytes = mediaBufLen;

        aesStart = s->contentOffset + (offset / DRM_ONE_AES_BLOCK_LEN * DRM_ONE_AES_BLOCK_LEN);
        piece = (offset + readBytes - 1) / DRM_ONE_AES_BLOCK_LEN - offset / DRM_ONE_AES_BLOCK_LEN + 2;
        mediaStart = offset % DRM_ONE_AES_BLOCK_LEN;

        AES_set_decrypt_key(keyValue, DRM_KEY_LEN * 8, &key);
        mediaBufOff = 0;
        leftBytes = readBytes;
        for (i = 0; i < piece - 1; i++) {
            memcpy(buf, s->rawContent + aesStart + i * DRM_ONE_AES_BLOCK_LEN, DRM_TWO_AES_BLOCK_LEN);
            bufLen = DRM_TWO_AES_BLOCK_LEN;

            if (drm_aesDecBuffer(buf, &bufLen, &key) < 0)
                return DRM_MEDIA_DATA_INVALID;
            if (0 != i)
                mediaStart = 0;

            if (bufLen - mediaStart <= leftBytes)
                copyBytes = bufLen - mediaStart;
            else
                copyBytes = leftBytes;

            memcpy(mediaBuf + mediaBufOff, buf + mediaStart, copyBytes);
            leftBytes -= copyBytes;
            mediaBufOff += copyBytes;
        }
    } else {
        int32_t res;

        if (s->bEndData)
            return DRM_MEDIA_EOF;
        TCTALOGD("aesDecDataLen = %d aesDecDataOff = %d", ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataLen, ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataOff);

        //xiaoqin.zhou@jrdcom.com fr303097

        /*if (((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataLen > ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataOff) {
            if (mediaBufLen < ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataLen - ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataOff)
                copyBytes = mediaBufLen;
            else
                copyBytes = ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataLen - ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataOff;

            memcpy(mediaBuf, ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecData + ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataOff, copyBytes);
            TCTALOGE("mediaBuf = %s copyBytes = %d", mediaBuf, copyBytes);
            //((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataOff += copyBytes;
            readBytes += copyBytes;
        }*/
         leftBytes = mediaBufLen;
        if (0 == leftBytes)
            return readBytes;
        if (leftBytes < 0)
            return DRM_FAILURE;

        //offset += readBytes;
        //to do when offset + mediaBufLen > ((T_DRM_Dcf_Node *)(s->infoStruct))->encContentLength modify the leftBytes
        aesStart = s->contentOffset + (offset / DRM_ONE_AES_BLOCK_LEN * DRM_ONE_AES_BLOCK_LEN);
        piece = (offset + leftBytes - 1) / DRM_ONE_AES_BLOCK_LEN - offset / DRM_ONE_AES_BLOCK_LEN + 2;
        mediaBufOff = readBytes;
        mediaStart = offset % DRM_ONE_AES_BLOCK_LEN;
        AES_set_decrypt_key(keyValue, DRM_KEY_LEN * 8, &key);

        ((T_DRM_Dcf_Node *)(s->infoStruct))->bAesBackupBuf = FALSE;
        for (i = 0; i < piece - 1; i++) {
            if (-1 == (res = drm_readAesData(buf, s, aesStart, DRM_TWO_AES_BLOCK_LEN))){
                return DRM_MEDIA_DATA_INVALID;
            }
            if (-2 == res){
                break;
            }

            bufLen = DRM_TWO_AES_BLOCK_LEN;
            aesStart += DRM_ONE_AES_BLOCK_LEN;

            if (drm_aesDecBuffer(buf, &bufLen, &key) < 0)
                return DRM_MEDIA_DATA_INVALID;

            //LOGV("aesStart = %d", aesStart);
            //drm_discardPaddingByte should be the last
            if (aesStart - s->contentOffset > ((T_DRM_Dcf_Node *)(s->infoStruct))->encContentLength){
                drm_discardPaddingByte(buf, &bufLen);
            }

            if (0 != i)
                mediaStart = 0;

            if ((bufLen - mediaStart)<= leftBytes)
                copyBytes = bufLen - mediaStart;
            else
                copyBytes = leftBytes;

            memcpy(mediaBuf + mediaBufOff, buf + mediaStart , copyBytes);
            leftBytes -= copyBytes;
            mediaBufOff += copyBytes;
            readBytes += copyBytes;
        }

        memcpy(((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecData, buf, DRM_ONE_AES_BLOCK_LEN);
        ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataLen = bufLen;
        ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataOff = copyBytes;

        if (aesStart - s->contentOffset > ((T_DRM_Dcf_Node *)(s->infoStruct))->encContentLength && ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataOff == ((T_DRM_Dcf_Node *)(s->infoStruct))->aesDecDataLen) {
            s->bEndData = TRUE;
            if (0 == readBytes)
                return DRM_MEDIA_EOF;
        }
    }

    return readBytes;
}

int32_t drm_readAesData(uint8_t* buf, T_DRM_Session_Node* s, int32_t aesStart, int32_t bufLen)
{
    if (NULL == buf || NULL == s || aesStart < 0 || bufLen < 0)
        return -1;

    if (aesStart - s->contentOffset  + bufLen > ((T_DRM_Dcf_Node *)(s->infoStruct))->encContentLength){
        return -2;
    }

    if (aesStart < DRM_MAX_MALLOC_LEN) {
        if (aesStart + bufLen <= DRM_MAX_MALLOC_LEN) { /* read from buffer */
            memcpy(buf, s->rawContent + aesStart, bufLen);
            return bufLen;
        } else { /* first read from buffer and then from InputStream */
            int32_t point = DRM_MAX_MALLOC_LEN - aesStart;
            int32_t res;
            if (((T_DRM_Dcf_Node *)(s->infoStruct))->bAesBackupBuf) {
                memcpy(buf, ((T_DRM_Dcf_Node *)(s->infoStruct))->aesBackupBuf, DRM_ONE_AES_BLOCK_LEN);
                res = s->readInputDataFunc(s->inputHandle, buf + DRM_ONE_AES_BLOCK_LEN, DRM_ONE_AES_BLOCK_LEN);
                if (0 == res || -1 == res)
                    return -1;

                res += DRM_ONE_AES_BLOCK_LEN;
            } else {
                memcpy(buf, s->rawContent + aesStart, point);
                //the file location maybe be modified, so need reseek
                lseek64(s->inputHandle, DRM_MAX_MALLOC_LEN, SEEK_SET);
                res = s->readInputDataFunc(s->inputHandle, buf + point, bufLen - point);
                if (0 == res || -1 == res)
                    return -1;
                res += point;
            }

            memcpy(((T_DRM_Dcf_Node *)(s->infoStruct))->aesBackupBuf, buf + DRM_ONE_AES_BLOCK_LEN, DRM_ONE_AES_BLOCK_LEN);
            ((T_DRM_Dcf_Node *)(s->infoStruct))->bAesBackupBuf = TRUE;

            return res;
        }
    } else { /* read from InputStream */
        int32_t res;
        if (!((T_DRM_Dcf_Node *)(s->infoStruct))->bAesBackupBuf){
            lseek64(s->inputHandle, aesStart, SEEK_SET);
            res = s->readInputDataFunc(s->inputHandle, buf, bufLen);
        } else {
            memcpy(buf, ((T_DRM_Dcf_Node *)(s->infoStruct))->aesBackupBuf, DRM_ONE_AES_BLOCK_LEN);
            res = s->readInputDataFunc(s->inputHandle, buf + DRM_ONE_AES_BLOCK_LEN, DRM_ONE_AES_BLOCK_LEN);
        }

        if (0 == res || -1 == res)
            return -1;
        memcpy(((T_DRM_Dcf_Node *)(s->infoStruct))->aesBackupBuf, buf + DRM_ONE_AES_BLOCK_LEN, DRM_ONE_AES_BLOCK_LEN);

        return DRM_ONE_AES_BLOCK_LEN + res;
    }
}
