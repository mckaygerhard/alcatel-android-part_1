/*************************************************************************/
/*                                                               Date:11/2012                                               */
/*                                PRESENTATION                                                                            */
/*                                                                                                                                 */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.                                    */
/*                                                                                                                                 */
/* This material is company confidential, cannot be reproduced in any form                                */
/* without the written permission of TCL Communication Technology Holdings                              */
/* Limited.                                                                                                                       */
/*                                                                                                                                  */
/* ---------------------------------------------------------------------------------*/
/*  Author :  Zonghui.Li                                                                                                       */
/*  Email  :  Zonghui.Li@tcl-mobile.com                                                                                  */
/*  Role   :                                                                                                                        */
/*  Reference documents :                                                                                                   */
/* ----------------------------------------------------------------------------------*/
/*  Comments :                                                                                                                   */
/*  File     :                                                                                                                        */
/*  Labels   :                                                                                                                      */
/* ----------------------------------------------------------------------------------*/
/* =========================================================================*/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

#ifndef __SDFILEDECRYPT_H__
#define __SDFILEDECRYPT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <openssl/aes.h>
#include "drm_sd_api.h"

#define DRM_MAX_MALLOC_LEN          (50 * 1024) /* 50K */

#define DRM_ONE_AES_BLOCK_LEN       16
#define DRM_TWO_AES_BLOCK_LEN       32


typedef struct _T_DRM_Dcf_Node{
    uint8_t rightsIssuer[256];
    int32_t encContentLength;
    uint8_t aesDecData[16];
    int32_t aesDecDataLen;
    int32_t aesDecDataOff;
    uint8_t aesBackupBuf[16];
    int32_t bAesBackupBuf;
}T_DRM_Dcf_Node;

/**
 * Discard the padding bytes in DCF decrypted data.
 *
 * @param decryptedBuf      The aes decrypted data buffer to be scanned.
 * @param decryptedBufLen   The length of the buffer. And save the output result.
 *
 * @return
 *      -0
 */
void drm_discardPaddingByte(uint8_t *decryptedBuf, int32_t *decryptedBufLen);

/**
 * Decrypt the media data according the CEK.
 *
 * @param Buffer    The buffer to decrypted and also used to save the output data.
 * @param BufferLen The length of the buffer data and#endif // __FWDLOCKFILE_H__ also save the output data length.
 * @param key       The structure of the CEK.
 *
 * @return
 *      -0
 */
int32_t drm_aesDecBuffer(uint8_t * Buffer, int32_t * BufferLen, AES_KEY *key);

/**
 * Update the DCF data length according the CEK.
 *
 * @param pDcfLastData  The last several byte for the DCF.
 * @param keyValue  The CEK of the DRM content.
 * @param moreBytes Output the more bytes for discarded.
 *
 * @return
 *      -TRUE, if the operation successfully.
 *      -FALSE, if the operation failed.
 */
int32_t drm_updateDcfDataLen(uint8_t* pDcfLastData, uint8_t* keyValue, int32_t* moreBytes);

/**
 * Open a session for a special DRM object, it will parse the input DRM data, and then user
 * can try to get information for this DRM object, or try to use it if the rights is valid.
 *
 * @param data      The DRM object data, DM or DCF.
 *
 * @return
 *      -A handle for this opened DRM object session.
 *      -DRM_MEDIA_DATA_INVALID, when the input DRM object data is invalid.
 *      -DRM_FAILURE, when some other error occurred.
 */
int32_t SVC_drm_openSession(T_DRM_Input_Data data);

/**
 * Close the opened session, after closed, the handle become invalid.
 *
 * @param session   The handle for this DRM object session.
 *
 * @return
 *      -DRM_SUCCESS, when close operation success.
 *      -DRM_SESSION_NOT_OPENED, when the session is not opened or has been closed.
 *      -DRM_FAILURE, when some other error occured.
 */
int32_t SVC_drm_closeSession(int32_t session);

/**
 * Get DRM media object content data. Support get the data piece by piece if the content is too large.
 *
 * @param session   The handle for this DRM object session.
 * @param offset    The offset to start to get content.
 * @param mediaBuf  The buffer to save media object data.
 * @param mediaBufLen   The length of the buffer.
 *
 * @return
 *      -A positive integer indicate the actually length of the data has been got.
 *      -DRM_SESSION_NOT_OPENED, when the session is not opened or has been closed.
 *      -DRM_NO_RIGHTS, when the rights object is not existed.
 *      -DRM_MEDIA_EOF, when reach to the end of the media data.
 *      -DRM_FAILURE, when some other error occured.
 */
int32_t SVC_drm_getContent(int32_t session, int32_t offset, uint8_t* mediaBuf, int32_t mediaBufLen);

/**
 * Get DRM media object decryption content data. Support get the data piece by piece if the content is too large.
 *
 * @param data    The DRM object data  DCF
 * @param offset    The offset to start to get content.
 * @param mediaBuf  The buffer to save media object data.
 * @param mediaBufLen   The length of the buffer.
 *
 * @return
 *      -A positive integer indicate the actually length of the data has been got.
 *      -DRM_SESSION_NOT_OPENED, when the session is not opened or has been closed.
 *      -DRM_NO_RIGHTS, when the rights object is not existed.
 *      -DRM_MEDIA_EOF, when reach to the end of the media data.
 *      -DRM_FAILURE, when some other error occured.
 */
int32_t drm_readAesContent(T_DRM_Session_Node* s, int32_t offset, uint8_t* mediaBuf, int32_t mediaBufLen);

int32_t drm_readAesData(uint8_t* buf, T_DRM_Session_Node* s, int32_t aesStart, int32_t bufLen);

#ifdef __cplusplus
}
#endif

#endif // __SDFILEDECRYPT_H__
