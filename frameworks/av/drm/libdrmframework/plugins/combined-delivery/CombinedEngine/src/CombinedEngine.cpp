/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_NDEBUG 0
#define TCT_DEBUG 1
#define LOG_TAG "CombinedEngine"
#include <utils/Log.h>

#include "SessionMap.h"
#include "CombinedEngine.h"
#include <unistd.h>
#include <openssl/aes.h>
#include <openssl/hmac.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include "drm_framework_common.h"
#include <fcntl.h>
#include <limits.h>
#include <DrmRights.h>
#include <DrmConstraints.h>
#include <DrmMetadata.h>
#include <DrmInfo.h>
#include <DrmInfoStatus.h>
#include <DrmInfoRequest.h>
#include <DrmSupportInfo.h>
#include <DrmConvertedStatus.h>
#include <utils/String8.h>
#include "CombinedConv.h"
#include "CombinedFile.h"
#include "CombinedGlue.h"
#include "CombinedEngineConst.h"
#include "MimeTypeUtil.h"
 #include "dlfcn.h"
 #include "parser_dm.h"
#include <drm_time.h>



using namespace android;
// This extern "C" is mandatory to be managed by TPlugInManager
#define FILEHEADER  100
#define INT_2_YMD_HMS(year, mon, day, date, hour, min, sec, time) do{\
    year = date / 10000;\
    mon = date % 10000 / 100;\
    day = date %100;\
    hour = time / 10000;\
    min = time % 10000 / 100;\
    sec = time % 100;\
}while(0)

extern "C" IDrmEngine* create() {
    if(TCT_DEBUG)
        TCTALOGE("LZH IDrmEngine* create()");
    return new CombinedEngine();
}

// This extern "C" is mandatory to be managed by TPlugInManager
extern "C" void destroy(IDrmEngine* plugIn) {
    delete plugIn;
}

CombinedEngine::CombinedEngine() {
}

CombinedEngine::~CombinedEngine() {

    int size = decodeSessionMap.getSize();

    for (int i = 0; i < size; i++) {
        DecodeSession *session = (DecodeSession*) decodeSessionMap.getValueAt(i);
        CombinedFile_detach(session->fileDesc);
        ::close(session->fileDesc);
    }

    size = convertSessionMap.getSize();
    for (int i = 0; i < size; i++) {
        ConvertSession *convSession = (ConvertSession*) convertSessionMap.getValueAt(i);
        CombinedConv_CloseSession(convSession->uniqueId, &(convSession->output));
    }
}

int CombinedEngine::getConvertedStatus(CombinedConv_Status_t status) {
    int retStatus = DrmConvertedStatus::STATUS_ERROR;

    switch(status) {
        case CombinedConv_Status_OK:
            retStatus = DrmConvertedStatus::STATUS_OK;
            break;
        case CombinedConv_Status_SyntaxError:
        case CombinedConv_Status_InvalidArgument:
        case CombinedConv_Status_UnsupportedFileFormat:
        case CombinedConv_Status_UnsupportedContentTransferEncoding:
            TCTALOGD("CombinedEngine.cpp CombinedEngine getConvertedStatus: file conversion Error %d. "
                  "Returning STATUS_INPUTDATA_ERROR", status);
            retStatus = DrmConvertedStatus::STATUS_INPUTDATA_ERROR;
            break;
        default:
            TCTALOGD("CombinedEngine.cpp CombinedEngine getConvertedStatus: file conversion Error %d. "
                  "Returning STATUS_ERROR", status);
            retStatus = DrmConvertedStatus::STATUS_ERROR;
            break;
    }

    return retStatus;
}



static bool getUidFromDrmFile(const int fd, char *uid, int32_t *uidLength) {
    int result = FALSE;
    char *head = (char *) malloc(MAX_CONTENT_ID * 2);
    //int32_t uidLength = 0;
    int32_t typeLength = 0;
    if (read(fd, head, FILEHEADER) == FILEHEADER) {
        typeLength = head[7];
        *uidLength = head[8 + typeLength];
        if (*uidLength < MAX_CONTENT_ID) {
            memcpy(uid, &head[9 + typeLength], *uidLength);
            uid[*uidLength] = '\0';
            result = TRUE;
        } else {
        }
    }
    free(head);
    return result;
}

// to change time to display the time
void CombinedEngine::changeDisplayTime(int date, int time, bool isChange, char* sOutput){
    if (NULL == sOutput){
        return;
    }
    int year = 0, mon = 0, day = 0, hour = 0, min = 0, sec = 0;
    INT_2_YMD_HMS(year, mon, day, date, hour, min, sec,
       time);
    TCTALOGD("LZH CombinedEngine.cpp CombinedEngine::onGetConstraints1 year = %d, mon = %d, day = %d, hour = %d, min = %d, sec = %d",
        year, mon, day, hour, min, sec);
    if (isChange){
        T_DB_TIME_SysTime rightDateTime;
        rightDateTime.year = year;
        rightDateTime.month = mon;
        rightDateTime.day = day;
        rightDateTime.hour = hour;
        rightDateTime.min = min;
        rightDateTime.sec= sec;
        changeTime(&rightDateTime);
        TCTALOGD("LZH CombinedEngine.cpp CombinedEngine::onGetConstraints1 chenged year = %d, mon = %d, day = %d, hour = %d, min = %d, sec = %d",
                rightDateTime.year, rightDateTime.month, rightDateTime.day, rightDateTime.hour, rightDateTime.min, rightDateTime.sec);
        sprintf(sOutput, "%04d-%02d-%02d %02d:%02d:%02d", rightDateTime.year, rightDateTime.month, rightDateTime.day, rightDateTime.hour, rightDateTime.min, rightDateTime.sec);
    } else {
        sprintf(sOutput, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);
    }
}


DrmConstraints* CombinedEngine::onGetConstraints(int uniqueId, const String8* path, int action){
    if (NULL == path){
        return NULL;
    }
    int fd = open((*path).string(), O_RDONLY);
    if (fd < 0){
        return NULL;
    }

    DrmConstraints* drmConstraints = onGetConstraintsByFd(uniqueId, fd, action);
    close(fd);
    return drmConstraints;
}

//TODO wrap an method for get uid from file and reset action[pengfei.zhong@jrdcom.com-2012-07-10]
DrmConstraints* CombinedEngine::onGetConstraintsByFd(int uniqueId, int fd, int action) {
    char uid[MAX_CONTENT_ID];
    int  typeLength,uidLength;
    int32_t id;
    int32_t roAmount;
    int32_t validRoAmount = 0;
    int32_t flag = DRM_FAILURE;
    int32_t i, j;
    T_DRM_Rights *pRo;
    T_DRM_Rights *pCurRo;
    int32_t * pNumOfPriority;
    int32_t iNum;
    T_DRM_Rights_Constraint * pCurConstraint;
    T_DRM_Rights_Constraint * pCompareConstraint;
    T_DRM_Rights_Constraint  validConstraint;
    int priority[8] = {1, 2, 4, 3, 8, 5, 7, 6};

    int des = dup(fd);
    if (des < 0){
        return NULL;
    }
    if (-1 == lseek64(des, 0, SEEK_SET)){
        close(des);
        return NULL;
    }
   //if action is default choose an action for user. start
     if(action == Action::DEFAULT){
             if(CombinedFile_attach(des) < 0){
                TCTALOGW("LZH CombinedEngine.cpp CombinedFile_attach attach failed!");
                close(des);
                 return NULL;
                }else{
                String8 m = onGetOriginalMimeType(uniqueId, des);
                 action = MimeTypeUtil::getAction(m);
                  const   char* fileContentId =  CombinedFile_GetContentId(des);
                 int fileContentIdLen = strlen(fileContentId);
                 memcpy(uid, fileContentId, fileContentIdLen);
                 uid[fileContentIdLen] = '\0';
                 CombinedFile_close(des);
               }
   }else  if (!getUidFromDrmFile(des, uid, &uidLength)){
        close(des);
        return NULL;
    }
   close(des);



    if (0 == drm_readFromUidTxt((uint8_t*)uid, &id, GET_ID)){
        return NULL;
     }

    if (0 == drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT)){
         return NULL;
    }

    validRoAmount = roAmount;
    if (roAmount < 1){
        return NULL;
    }

    pRo = (T_DRM_Rights*)malloc(roAmount * sizeof(T_DRM_Rights));
    pCurRo = pRo;
    if (NULL == pRo){
        return NULL;
    }

    if (0 == drm_writeOrReadInfo(id, pRo, &roAmount, GET_ALL_RO)) {
        free(pRo);
        return NULL;
    }
    /** check the right priority */
    pNumOfPriority = (int32_t*) malloc(sizeof(int32_t) * roAmount);
    for(i = 0; i < roAmount; i++) {
        iNum = roAmount - 1;
        for(j = 0; j < roAmount; j++) {
            if(i == j)
                continue;
            switch(action) {
            case Action::PLAY:
                pCurConstraint = &pRo[i].PlayConstraint;
                pCompareConstraint = &pRo[j].PlayConstraint;
                break;
            case Action::DISPLAY:
                pCurConstraint = &pRo[i].DisplayConstraint;
                pCompareConstraint = &pRo[j].DisplayConstraint;
                break;
            case Action::EXECUTE:
                pCurConstraint = &pRo[i].ExecuteConstraint;
                pCompareConstraint = &pRo[j].ExecuteConstraint;
                break;
            case Action::PRINT:
                pCurConstraint = &pRo[i].PrintConstraint;
                pCompareConstraint = &pRo[j].PrintConstraint;
                break;
            default:
               free(pRo);
               free(pNumOfPriority);
                return NULL;
            }

            /**get priority by Indicator*/
            if(0 == (pCurConstraint->Indicator & DRM_NO_CONSTRAINT) &&
                0 == (pCompareConstraint->Indicator & DRM_NO_CONSTRAINT)) {
                    int num1, num2;
                    num1 = (pCurConstraint->Indicator & 0x0e) >> 1;
                    num2 = (pCompareConstraint->Indicator & 0x0e) >> 1;
                    if(priority[num1] > priority[num2]) {
                        iNum--;
                        continue;
                     /* } else if(priority[pCurConstraint->Indicator] < priority[pCompareConstraint->Indicator])   */
                    } else if(priority[num1] < priority[num2])
                        continue;
            } else if(pCurConstraint->Indicator > pCompareConstraint->Indicator) {
                iNum--;
                continue;
            } else if(pCurConstraint->Indicator < pCompareConstraint->Indicator)
                continue;

            if(0 != (pCurConstraint->Indicator & DRM_END_TIME_CONSTRAINT)) {
                if(pCurConstraint->EndTime.date < pCompareConstraint->EndTime.date) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->EndTime.date > pCompareConstraint->EndTime.date)
                    continue;

                if(pCurConstraint->EndTime.time < pCompareConstraint->EndTime.time) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->EndTime.date > pCompareConstraint->EndTime.date)
                    continue;
            }

            if(0 != (pCurConstraint->Indicator & DRM_INTERVAL_CONSTRAINT)) {
                if(pCurConstraint->Interval.date < pCompareConstraint->Interval.date) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->Interval.date > pCompareConstraint->Interval.date)
                    continue;

                if(pCurConstraint->Interval.time < pCompareConstraint->Interval.time) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->Interval.time > pCompareConstraint->Interval.time)
                    continue;
            }

            if(0 != (pCurConstraint->Indicator & DRM_COUNT_CONSTRAINT)) {
                if(pCurConstraint->Count < pCompareConstraint->Count) {
                    iNum--;
                    continue;
                } else if(pCurConstraint->Count > pCompareConstraint->Count)
                    continue;
            }

            if(i < j)
                iNum--;
        }
        pNumOfPriority[iNum] = i;
    }

    for (i = 0; i < validRoAmount; i++) {
        /** check the right priority */
        if (pNumOfPriority[i] >= validRoAmount)
            break;

        pCurRo = pRo + pNumOfPriority[i];
        switch (action) {
        case Action::PLAY:
            flag =
                drm_startCheckRights(&pCurRo->bIsPlayable,
                                       &pCurRo->PlayConstraint);
            break;
        case Action::DISPLAY:
            flag =
                drm_startCheckRights(&pCurRo->bIsDisplayable,
                                       &pCurRo->DisplayConstraint);
            break;
        case Action::EXECUTE:
            flag =
                drm_startCheckRights(&pCurRo->bIsExecuteable,
                                       &pCurRo->ExecuteConstraint);
            break;
        case DRM_PERMISSION_PRINT:
            flag =
                drm_startCheckRights(&pCurRo->bIsPrintable,
                                       &pCurRo->PrintConstraint);
            break;
        default:
            free(pNumOfPriority);
            free(pRo);
            return NULL;
        }

        /* If the flag is TRUE, this means: we have found a valid RO, so break, no need to check other RO */
        if (DRM_SUCCESS == flag)
            break;
    }

    if (i==validRoAmount)
    {
            free(pNumOfPriority);
            free(pRo);
            return NULL;
    }
    free(pNumOfPriority);

    switch (action) {
    case Action::PLAY:
        validConstraint=pCurRo->PlayConstraint;
        break;
    case Action::DISPLAY:
        validConstraint=pCurRo->DisplayConstraint;
        break;
    case Action::EXECUTE:
        validConstraint=pCurRo->ExecuteConstraint;
        break;
    case Action::PRINT:
        validConstraint=pCurRo->PrintConstraint;
        break;
    default:
        free(pRo);
        return NULL;
    }
    char sOutput[64];
    DrmConstraints* drmConstraints =  new DrmConstraints();
    if ( drmConstraints==NULL)
    {
        free(pRo);
        return NULL;
    }


    sprintf(sOutput, "%s", (char*) pCurRo->Version);
    drmConstraints->put(&DrmConstraints::VERSION, sOutput);
    drmConstraints->put(&DrmConstraints::CONTENT_ID, uid);
    sprintf(sOutput, "%d", action);
    drmConstraints->put(&DrmConstraints::ACTION, sOutput);
    if (0 != (uint8_t)(validConstraint.Indicator & DRM_NO_CONSTRAINT)) /* Have utter right? */
    {
        free(pRo);
        return drmConstraints;
     }
    //int year = 0, mon = 0, day = 0, hour = 0, min = 0, sec = 0;
    if (0 != (uint8_t)(validConstraint.Indicator & DRM_COUNT_CONSTRAINT)) { /* Have count restrict? */
       sprintf(sOutput, "%04d", validConstraint.Count);
       drmConstraints->put(&DrmConstraints::REMAINING_REPEAT_COUNT, sOutput);
       drmConstraints->put(&DrmConstraints::TYPE, "count");
    }
    if (0 != (uint8_t)(validConstraint.Indicator & DRM_START_TIME_CONSTRAINT)) {
       changeDisplayTime(validConstraint.StartTime.date, validConstraint.StartTime.time, true, sOutput);
       /*INT_2_YMD_HMS(year, mon, day, validConstraint.StartTime.date, hour, min, sec,
               validConstraint.StartTime.time);
            sprintf(sOutput, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
         drmConstraints->put(&DrmConstraints::LICENSE_START_TIME, sOutput);
         drmConstraints->put(&DrmConstraints::TYPE, "datetime");
    }

    if (0 != (uint8_t)(validConstraint.Indicator & DRM_END_TIME_CONSTRAINT)) { /* Have end time restrict? */
        changeDisplayTime(validConstraint.EndTime.date, validConstraint.EndTime.time, true, sOutput);
       /*INT_2_YMD_HMS(year, mon, day, validConstraint.EndTime.date, hour, min, sec,
                validConstraint.EndTime.time);
            sprintf(sOutput, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
         drmConstraints->put(&DrmConstraints::LICENSE_EXPIRY_TIME, sOutput);
         drmConstraints->put(&DrmConstraints::TYPE, "datetime");
    }

    if (0 != (uint8_t)(validConstraint.Indicator& DRM_INTERVAL_CONSTRAINT)) { /* Have interval time restrict? */
        changeDisplayTime(validConstraint.Interval.date, validConstraint.Interval.time, false, sOutput);
       /*INT_2_YMD_HMS(year, mon, day, validConstraint.Interval.date, hour, min, sec,
                validConstraint.Interval.time);
            sprintf(sOutput, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
            drmConstraints->put(&DrmConstraints::LICENSE_AVAILABLE_TIME, sOutput);
         drmConstraints->put(&DrmConstraints::TYPE, "interval");

    }
    free(pRo);
    return drmConstraints;
}


List<DrmConstraints*>* CombinedEngine::onGetConstraintsList(int uniqueId, const String8* path, int action) {
 //   char* fileheader=(char *)malloc(FILEHEADER);
    char uid[MAX_CONTENT_ID];
    int  typeLength,uidLength;
    int32_t id;
    int32_t roAmount;
    int32_t validRoAmount = 0;
    int32_t flag = DRM_FAILURE;
    int32_t i;
    T_DRM_Rights *pRo;
    int32_t iNum;
    T_DRM_Rights_Constraint * pCurConstraint;
    T_DRM_Rights_Constraint  validConstraint;
    List<DrmConstraints*>* drmConstraintsList = NULL;
    int priority[8] = {1, 2, 4, 3, 8, 5, 7, 6};
   //pengfei.zhong@jrdcom.com-PR300460 --- if action is default choose an action for user. start
     int des = open((*path).string(), O_RDONLY);
      if (des < 0){
        return NULL;
       }
       if (-1 == lseek64(des, 0, SEEK_SET)){
          close(des);
          return NULL;
      }
     if(action == Action::DEFAULT){
             if(CombinedFile_attach(des) < 0){
                TCTALOGW("LZH CombinedEngine.cpp CombinedFile_attach attach failed!");
                close(des);
                 return NULL;
                }else{
                String8 m = onGetOriginalMimeType(uniqueId, des);
                 action = MimeTypeUtil::getAction(m);
                const  char* fileContentId =  CombinedFile_GetContentId( des);
                 int fileContentIdLen = strlen(fileContentId);
                 memcpy(uid, fileContentId, fileContentIdLen);
                 uid[fileContentIdLen] = '\0';
                 CombinedFile_close(des);
               }
   }else  if (!getUidFromDrmFile(des, uid, &uidLength)){
        close(des);
        return NULL;
    }
           close(des);
    if (0 == drm_readFromUidTxt((uint8_t*)uid, &id, GET_ID)){
        return NULL;
     }

    if (0 == drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT)){
         return NULL;
    }
    validRoAmount = roAmount;
    if (roAmount < 1){
        return NULL;
    }

    pRo = (T_DRM_Rights*)malloc(roAmount * sizeof(T_DRM_Rights));
    if (NULL == pRo){
        return NULL;
    }

    if (0 == drm_writeOrReadInfo(id, pRo, &roAmount, GET_ALL_RO)) {
        free(pRo);
        return NULL;
    }
    drmConstraintsList = new List<DrmConstraints*>();
    for(i = 0; i < roAmount; i++) {
        char sOutputs[64];
        DrmConstraints* tmdrmConstraints =  new DrmConstraints();
        switch(action) {
            case Action::PLAY:
                pCurConstraint = &pRo[i].PlayConstraint;
                break;
            case Action::DISPLAY:
                pCurConstraint = &pRo[i].DisplayConstraint;
                break;
            case Action::EXECUTE:
                pCurConstraint = &pRo[i].ExecuteConstraint;
                break;
            case Action::PRINT:
                pCurConstraint = &pRo[i].PrintConstraint;
                break;
            default:
               free(pRo);
               drmConstraintsList->clear();
               delete drmConstraintsList;
               delete tmdrmConstraints;
                return NULL;
            }
        sprintf(sOutputs, "%s", (char*) pRo->Version);
        tmdrmConstraints->put(&DrmConstraints::VERSION, sOutputs);
        tmdrmConstraints->put(&DrmConstraints::CONTENT_ID, uid);
        sprintf(sOutputs, "%d", action);
        tmdrmConstraints->put(&DrmConstraints::ACTION, sOutputs);
        if (0 != (uint8_t)(pCurConstraint->Indicator & DRM_NO_CONSTRAINT)) /* Have utter right? */
        {
            delete tmdrmConstraints; tmdrmConstraints = NULL;
            continue;
        }
        //int year = 0, mon = 0, day = 0, hour = 0, min = 0, sec = 0;
        if (0 != (uint8_t)(pCurConstraint->Indicator & DRM_COUNT_CONSTRAINT)) { /* Have count restrict? */
            if (pCurConstraint->Count <= 0) {
                delete tmdrmConstraints; tmdrmConstraints = NULL;
                continue;
            }
           sprintf(sOutputs, "%04d", pCurConstraint->Count);
           tmdrmConstraints->put(&DrmConstraints::REMAINING_REPEAT_COUNT, sOutputs);
           tmdrmConstraints->put(&DrmConstraints::TYPE, "count");
        }

        if (0 != (uint8_t)(pCurConstraint->Indicator & DRM_START_TIME_CONSTRAINT)) {
            changeDisplayTime(pCurConstraint->StartTime.date, pCurConstraint->StartTime.time, true, sOutputs);
            /*INT_2_YMD_HMS(year, mon, day, pCurConstraint->StartTime.date, hour, min, sec,
               pCurConstraint->StartTime.time);
            sprintf(sOutputs, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
            tmdrmConstraints->put(&DrmConstraints::LICENSE_START_TIME, sOutputs);
            tmdrmConstraints->put(&DrmConstraints::TYPE, "datetime");
        }
        T_DB_TIME_SysTime rightDateTime;

        if (0 != (uint8_t)(pCurConstraint->Indicator & DRM_END_TIME_CONSTRAINT)) { /* Have end time restrict? */
            if(DRM_TIME_INVAILD == checkEndTimeExpired(pCurConstraint->EndTime.date, pCurConstraint->EndTime.time)){
                delete tmdrmConstraints; tmdrmConstraints = NULL;
                continue;
            }
            changeDisplayTime(pCurConstraint->EndTime.date, pCurConstraint->EndTime.time, true, sOutputs);
            /*INT_2_YMD_HMS(year, mon, day, pCurConstraint->EndTime.date, hour, min, sec,
                pCurConstraint->EndTime.time);
            sprintf(sOutputs, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
            tmdrmConstraints->put(&DrmConstraints::LICENSE_EXPIRY_TIME, sOutputs);
            tmdrmConstraints->put(&DrmConstraints::TYPE, "datetime");
        }
        if (0 != (uint8_t)(pCurConstraint->Indicator& DRM_INTERVAL_CONSTRAINT)) { /* Have interval time restrict? */
            changeDisplayTime(pCurConstraint->Interval.date, pCurConstraint->Interval.time, false, sOutputs);
              /*INT_2_YMD_HMS(year, mon, day, pCurConstraint->Interval.date, hour, min, sec,
                pCurConstraint->Interval.time);
              sprintf(sOutputs, "%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, hour, min, sec);*/
              tmdrmConstraints->put(&DrmConstraints::LICENSE_AVAILABLE_TIME, sOutputs);
              tmdrmConstraints->put(&DrmConstraints::TYPE, "interval");
         }
         drmConstraintsList->push_back(tmdrmConstraints);
    }
    free(pRo);
    return drmConstraintsList;
}

// [drm] jian.gu@tct-nj.com
bool CombinedEngine::onIsCountOrIntervalConstraint(const String8& path) {
    char uid[MAX_CONTENT_ID] = "\0";
    int32_t uidLength = 0;
    int32_t id = -1;
       int fd = open((path).string(), O_RDONLY);
    if (fd < 0){
        return NULL;
    }
    if (!getUidFromDrmFile(fd, uid, &uidLength)) {
        close(fd);
        return false;
    }
            close(fd);

    if (!drm_readFromUidTxt((uint8_t*) uid, &id, GET_ID)) {
        return false;
    }

    int32_t roAmount = -1;
    if (!drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT) || roAmount <= 0) {
        return false;
    }

    T_DRM_Rights rights;
    memset(&rights, 0, sizeof(T_DRM_Rights));
    if (!drm_writeOrReadInfo(id, &rights, &roAmount, GET_A_RO)) {
        return false;
    }
    /** action, constraint */
    int drmAction = Action::DEFAULT;
    T_DRM_Rights_Constraint *constraint = NULL;
    if (TRUE == getConstraintFromRights(&rights, &drmAction, &constraint)) {
        //PR982395-Li-Zhao begin
        if (constraint == NULL) {
            return false;
        }
        //PR982395-Li-Zhao end
        if (0 != (uint8_t) (constraint->Indicator & DRM_COUNT_CONSTRAINT) ||
                (0 != (uint8_t) (constraint->Indicator & DRM_INTERVAL_CONSTRAINT) &&
                        0 == (uint8_t) (constraint->Indicator & DRM_END_TIME_CONSTRAINT))) {
            return true;
        }
    }
    return false;
}

/**
 * @auther feng.wang@jrdcom.com
 */
int CombinedEngine::getConstraintFromRights(T_DRM_Rights *rights, int *type,
        T_DRM_Rights_Constraint **constraint) {

    if (NULL == rights || NULL == type || NULL == constraint) {
        return FALSE;
    }

    if (rights->bIsPlayable) {
        *type = Action::PLAY;
        *constraint = &rights->PlayConstraint;
    } else if (rights->bIsDisplayable) {
        *type = Action::DISPLAY;
        *constraint = &rights->DisplayConstraint;
    } else if (rights->bIsExecuteable) {
        *type = Action::PLAY;
        *constraint = &rights->ExecuteConstraint;
    } else if (rights->bIsPrintable) {
        *type = Action::PRINT;
        *constraint = &rights->PrintConstraint;
    }
    return TRUE;
}

/**
 * @auther feng.wang@jrdcom.com
 */
void CombinedEngine::printRightInfo(T_DRM_Rights *rights) {

    if (NULL == rights) {
        return;
    }


    if (rights->bIsPlayable) {
        printConstraintInfo(&rights->PlayConstraint);
    }
    if (rights->bIsDisplayable) {
        printConstraintInfo(&rights->DisplayConstraint);
    }
    if (rights->bIsExecuteable) {
        printConstraintInfo(&rights->ExecuteConstraint);
    }
    if (rights->bIsPrintable) {
        printConstraintInfo(&rights->PrintConstraint);
    }
}

/**
 * @auther feng.wang@jrdcom.com
 */
void CombinedEngine::printConstraintInfo(T_DRM_Rights_Constraint *constraint) {

    if (NULL == constraint) {
        return;
    }

    int year = 0, month = 0, day = 0, hour = 0, min = 0, sec = 0;

    if (constraint->StartTime.date > 0 || constraint->StartTime.time > 0) {
        getDatatime(&constraint->StartTime, &year, &month, &day, &hour, &min, &sec);
    }

    if (constraint->EndTime.date > 0 || constraint->EndTime.time > 0) {
        getDatatime(&constraint->EndTime, &year, &month, &day, &hour, &min, &sec);
    }

    if (constraint->Interval.date > 0 || constraint->Interval.time > 0) {
        getDatatime(&constraint->Interval, &year, &month, &day, &hour, &min, &sec);
    }
}

/**
 * @auther feng.wang@jrdcom.com
 */
int CombinedEngine::getDatatime(T_DRM_DATETIME *dateTime, int *year, int *month, int *day,
        int *hour, int *min, int *sec) {

    if (NULL == dateTime || NULL == year || NULL == month || NULL == hour || NULL == min
            || NULL == sec) {
        return FALSE;
    }

    *year = dateTime->date / 10000;
    *month = (dateTime->date - *year * 10000) / 100;
    *day = dateTime->date % 100;

    *hour = dateTime->time / 10000;
    *min = (dateTime->time - *hour * 10000)/100;
    *sec = dateTime->time % 100;

    return TRUE;
}


DrmMetadata* CombinedEngine::onGetMetadata(int uniqueId, const String8* path) {
    DrmMetadata* drmMetadata = NULL;


    if (NULL != path) {
        // Returns empty metadata to show no error condition.
        drmMetadata = new DrmMetadata();
    }

    return drmMetadata;
}

android::status_t CombinedEngine::onInitialize(int uniqueId) {

    if (CombinedGlue_InitializeKeyEncryption()) {
        TCTALOGD("CombinedEngine.cpp CombinedEngine::onInitialize -- CombinedGlue_InitializeKeyEncryption succeeded");
    } else {
        TCTALOGE("CombinedEngine.cpp CombinedEngine::onInitialize -- CombinedGlue_InitializeKeyEncryption failed:"
             "errno = %d", errno);
    }

    return DRM_NO_ERROR;
}

android::status_t
CombinedEngine::onSetOnInfoListener(int uniqueId, const IDrmEngine::OnInfoListener* infoListener) {
    // Not used

    return DRM_NO_ERROR;
}

android::status_t CombinedEngine::onTerminate(int uniqueId) {

    return DRM_NO_ERROR;
}

DrmSupportInfo* CombinedEngine::onGetSupportInfo(int uniqueId) {
    DrmSupportInfo* pSupportInfo = new DrmSupportInfo();


    // fill all Combined Delivery  mimetypes and extensions
    if (NULL != pSupportInfo) {
        pSupportInfo->addMimeType(String8(COMBINED_DRM_TYPE ));
        pSupportInfo->addMimeType(String8(COMBINED_MIME_TYPE ));
        pSupportInfo->addFileSuffix(String8(COMBINED_DOTEXTENSION_DM));

        pSupportInfo->setDescription(String8(COMBINED_DESCRIPTION));
    }

    return pSupportInfo;
}

bool CombinedEngine::onCanHandle(int uniqueId, const String8& path) {
    /**
     *
     *  OnCanHandle is NOT needed because the plug in is selected by drm type string, such as
     *  "forward lock", or "combined delivery", or "separator delivery"
     */

    bool result = false;
    //because dowload file's extension name has been modified, so this is invalid
    //the Extension name of fl or sd or cd maybe is the same, so this is valid

    int fileDesc = open(path.string(), O_RDONLY);
    if(TCT_DEBUG)
        TCTALOGE("LZH :onCanHandle path =%s and fileDesc = %d",path.string(),fileDesc);
    if (fileDesc < 0) {
        return result;
    }
    result = onCanHandle(uniqueId, fileDesc);
    if (!result){
    }
    close(fileDesc);
    return result;
}

bool CombinedEngine::onCanHandle(int uniqueId, int fd){
    bool result = false;
    if (fd < 0){
        return result;
    }
    result = CombinedFile_IsCdFile(fd);
    if (!result){
    }
    return result;
}


DrmInfoStatus* CombinedEngine::onProcessDrmInfo(int uniqueId, const DrmInfo* drmInfo) {
    DrmInfoStatus *drmInfoStatus = NULL;

    // Nothing to process

    drmInfoStatus = new DrmInfoStatus((int)DrmInfoStatus::STATUS_OK, 0, NULL, String8(""));


    return drmInfoStatus;
}

status_t CombinedEngine::onSaveRights(int uniqueId, const DrmRights& drmRights,
        const String8& rightsPath, const String8& contentPath) {


    char *data = drmRights.getData().data;
    int length = drmRights.getData().length;
    T_DRM_DM_Info dmInfo;
    memset(&dmInfo, 0, sizeof(T_DRM_DM_Info));
    if (!drm_parseDMPart((uint8_t*) data, length, &dmInfo)) {
        return DRM_FAILURE;
    }

    T_DRM_Rights rights;
    memset(&rights, 0, sizeof(T_DRM_Rights));
    if (!drm_rightParser((uint8_t*) (data + dmInfo.rightsOffset), dmInfo.rightsLen,
            TYPE_DRM_RIGHTS_XML, &rights)) {
        return DRM_FAILURE;
    }

    if (!drm_appendRightsInfo(&rights)) {
        return DRM_FAILURE;
    }
    return DRM_SUCCESS;
}

DrmInfo* CombinedEngine::onAcquireDrmInfo(int uniqueId, const DrmInfoRequest* drmInfoRequest) {
    DrmInfo* drmInfo = NULL;

    // Nothing to be done for Forward Lock file

    return drmInfo;
}

int CombinedEngine::onCheckRightsStatus(int uniqueId,
                                       const String8& path,
                                       int action) {

    int32_t result = RightsStatus::RIGHTS_INVALID;

    /*if (!onCanHandle(uniqueId, path)) {
        return result;
    }
    if (!getUidFromDrmFile(path, uid, &uidLength)) {
        TCTALOGW("Cannot get drm uid of file %s", path.string());
        return RightsStatus::RIGHTS_INVALID;
    }*/
    int fd = open(path.string(), O_RDONLY);
    if (-1 == fd){
        TCTALOGW("LZH CombinedEngine.cpp Cannot open file = [%s]", path.string());
        return result;
    }
    result = onCheckRightsStatus(uniqueId, fd, action);
    close(fd);
    return result;
}

int CombinedEngine::onCheckRightsStatus(int uniqueId,
                        const int fd,
                        int action){
    int32_t result = RightsStatus::RIGHTS_INVALID;
    if (!onCanHandle(uniqueId, fd)) {
        return RightsStatus::RIGHTS_VALID;
    }
    char uid[MAX_CONTENT_ID];
    int uidLength = 0;
    int expireFlag=0,peddingFlag=0;
    int des = dup(fd);
    if (des < 0){
        TCTALOGW("onCheckRightsStatus() dup failed!");
        return result;
    }
    if (-1 == lseek64(des, 0, SEEK_SET)){
        close(des);
        TCTALOGW("onCheckRightsStatus() lseek64 failed!");
        return result;
    }
    //if action is default choose an action for user. start
     if(action == Action::DEFAULT){
             if(CombinedFile_attach(des) < 0){
                TCTALOGW("LZH CombinedEngine.cpp CombinedFile_attach attach failed!");
                close(des);
                 return result;
                }else{
                String8 m = onGetOriginalMimeType(uniqueId, des);
                 action = MimeTypeUtil::getAction(m);
               const  char* fileContentId =  CombinedFile_GetContentId( des);
                 int fileContentIdLen = strlen(fileContentId);
                 memcpy(uid, fileContentId, fileContentIdLen);
                 uid[fileContentIdLen] = '\0';
                 CombinedFile_close(des);
               }
   }else  if (!getUidFromDrmFile(des, uid, &uidLength)){
        close(des);
        TCTALOGW("onCheckRightsStatus() getUidFromDrmFile failed!");
        return RightsStatus::RIGHTS_INVALID;
    }
   close(des);


    int32_t id = 0;
    if (!drm_readFromUidTxt((uint8_t*) uid, &id, GET_ID)) {
        TCTALOGW("LZH CombinedEngine.cpp Get drm uid from txt fail for file %d", fd);
        return RightsStatus::RIGHTS_INVALID;
    }
    int32_t roAmount = -1;
    if (!drm_writeOrReadInfo(id, NULL, &roAmount, GET_ROAMOUNT) || roAmount <= 0) {
        TCTALOGW("LZH CombinedEngine.cpp Get ro amount fail, or roAmount < 0 for file %d", fd);
        return RightsStatus::RIGHTS_INVALID;
    }
    TCTALOGD("onCheckRightsStatus() roAmount=[%d]", roAmount);

    T_DRM_Rights *pRo = NULL, *pCurRo = NULL;
    pRo = (T_DRM_Rights *) malloc(roAmount * sizeof(T_DRM_Rights));
    if (NULL == pRo) {
        TCTALOGW("onCheckRightsStatus() malloc pRo failed!");
        return RightsStatus::RIGHTS_INVALID;
    }
    drm_writeOrReadInfo(id, pRo, &roAmount, GET_ALL_RO);
    pCurRo = pRo;
    TCTALOGD("onCheckRightsStatus() *pCurRo=[%d]", *pCurRo);
      //default action, just check all the action
      //TODO should optimize this code,so desultorily
    for (int32_t i = 0; i < roAmount; i++) {
         switch (action) {
             case Action::PLAY:
            	 TCTALOGD("onCheckRightsStatus() PLAY");
                 result = drm_startCheckRights(&(pCurRo->bIsPlayable), &(pCurRo->PlayConstraint));
                 break;
             case Action::DISPLAY:
            	 TCTALOGD("onCheckRightsStatus() DISPLAY");
                 result = drm_startCheckRights(&(pCurRo->bIsDisplayable), &(pCurRo->DisplayConstraint));
                 break;
             case Action::EXECUTE:
            	 TCTALOGD("onCheckRightsStatus() EXECUTE");
                 result = drm_startCheckRights(&(pCurRo->bIsExecuteable), &(pCurRo->ExecuteConstraint));
                 break;
             case Action::PRINT:
            	 TCTALOGD("onCheckRightsStatus() PRINT");
                 result = drm_startCheckRights(&(pCurRo->bIsPrintable), &(pCurRo->PrintConstraint));
                 break;
             default:
            	 TCTALOGD("onCheckRightsStatus() free");
                 free(pRo);
                 return RightsStatus::RIGHTS_INVALID;
             }
        if (result==DRM_SUCCESS){
            free(pRo);
            return RightsStatus::RIGHTS_VALID;
        }
        else if (result==DRM_RIGHTS_PENDING){
           peddingFlag=1;
        }
        else if (result==DRM_RIGHTS_EXPIRED){
          expireFlag=1;
        }
        pCurRo++;
    }
    free(pRo);
    if (peddingFlag==1){
        //return RightsStatus::RIGHTS_PENDING; TODO
        return RightsStatus::RIGHTS_EXPIRED;
    }
    else if(expireFlag==1){
        return RightsStatus::RIGHTS_EXPIRED;
    }
    else{
        //return RightsStatus::FAILURE;   TODO
        return RightsStatus::RIGHTS_EXPIRED;
    }
}

status_t CombinedEngine::onConsumeRights(int uniqueId,
                                        DecryptHandle* decryptHandle,
                                        int action,
                                        bool reserve) {

     char* fileheader=(char *)malloc(FILEHEADER);
     char *uid=NULL;
     int  typeLength,uidLength,id;
     int ret=DRM_FAILURE;

    if (NULL != decryptHandle &&
       decodeSessionMap.isCreated(decryptHandle->decryptId) ) {
        DecodeSession* session = decodeSessionMap.getValue(decryptHandle->decryptId);
        if (NULL != session && session->fileDesc > -1) {
           lseek64(session->fileDesc , 0, SEEK_SET);
        if(read(session->fileDesc, fileheader, FILEHEADER)){
            typeLength=fileheader[7];
            uidLength=fileheader[8+typeLength];
            uid=&fileheader[9+typeLength];
            uid[uidLength]='\0';
         }
        if (!drm_readFromUidTxt((uint8_t*)uid, &id, GET_ID)){
            free(fileheader);
            return DRM_FAILURE;
            }
    switch (action) {
    case Action::PLAY:
        action = DRM_PERMISSION_PLAY;
        break;
    case Action::DISPLAY:
        action = DRM_PERMISSION_DISPLAY;
        break;
    case Action::EXECUTE:
       action = DRM_PERMISSION_EXECUTE;
        break;
    case Action::PRINT:
        action = DRM_PERMISSION_PRINT;
        break;
    default:
        break;
    }
        ret = drm_checkRoAndUpdate(id, action);
        free(fileheader);
        return ret;
     }
    }
    free(fileheader);
    return ret;
}

bool CombinedEngine::onValidateAction(int uniqueId,
                                     const String8& path,
                                     int action,
                                     const ActionDescription& description) {

    // For the forwardlock engine checkRights and ValidateAction are the same.
    return (onCheckRightsStatus(uniqueId, path, action) == RightsStatus::RIGHTS_VALID);
}

String8 CombinedEngine::onGetOriginalMimeType(int uniqueId, const String8& path) {
    String8 mimeString = String8("");
    if(TCT_DEBUG)
        TCTALOGE("LZH 111 onGetOriginalMimeType = %s",path.string());
    int fileDesc = CombinedFile_open(path.string());
    if (-1 < fileDesc) {
        //warp src's code to an method.
     mimeString = onGetOriginalMimeType( uniqueId, fileDesc);
        CombinedFile_close(fileDesc);
    }
    return mimeString;
}

/**[pengfei.zhong@jrdcom.com]
    NOTCIE!!!! make sure before call this method,has been called  CombinedFile_attach(fileDesc)
    and make sure the the two methods's param fileDesc are the same*/
String8 CombinedEngine::onGetOriginalMimeType(int uniqueId, const int  fileDesc) {
    if(TCT_DEBUG)
        TCTALOGE("LZH onGetOriginalMimeType uniqueId = %d,fileDesc = %d",uniqueId,fileDesc);
    String8 mimeString = String8("");
    const char* pMimeType = CombinedFile_GetContentType(fileDesc);
    if(TCT_DEBUG)
        TCTALOGE("LZH onGetOriginalMimeType =%s",pMimeType);
    if (NULL != pMimeType) {
        String8 contentType = String8(pMimeType);
        contentType.toLower();
        mimeString = MimeTypeUtil::convertMimeType(contentType);
    }
    return mimeString;
}

String8 CombinedEngine::onGetOriginalMimeType(int uniqueId, const String8& path, int fd) {
    if(TCT_DEBUG)
        TCTALOGE("LZH CombinedEngine::onGetOriginalMimeType path = %s",path.string());
    String8 mimeString = String8("");
    //int fileDesc = dup(fd);
    int fileDesc = open(path.string(), O_RDONLY);
    if(TCT_DEBUG)
        TCTALOGE("LZH int fileDesc = %d",fileDesc);
    if (-1 < fileDesc) {
        if (CombinedFile_attach(fileDesc) < 0) {
            TCTALOGE("LZH return empty string ...");
            close(fileDesc);
            return mimeString;
        }
        const char* pMimeType = CombinedFile_GetContentType(fileDesc);

        if (NULL != pMimeType) {
            String8 contentType = String8(pMimeType);
            contentType.toLower();
            mimeString = MimeTypeUtil::convertMimeType(contentType);
        }

        CombinedFile_close(fileDesc);
    }

    return mimeString;
}

int CombinedEngine::onGetDrmObjectType(int uniqueId,
                                      const String8& path,
                                      const String8& mimeType) {
    String8 mimeStr = String8(mimeType);


    mimeStr.toLower();

    /* Checks whether
    * 1. path and mime type both are not empty strings (meaning unavailable) else content is unknown
    * 2. if one of them is empty string and if other is known then its a DRM Content Object.
    * 3. if both of them are available, then both may be of known type
    *    (regardless of the relation between them to make it compatible with other DRM Engines)
    */
    if (((0 == path.length()) || onCanHandle(uniqueId, path)) &&
        ((0 == mimeType.length()) || (mimeStr == String8(COMBINED_MIMETYPE_DM))) && (mimeType != path) ) {
            return DrmObjectType::CONTENT;
    }

    return DrmObjectType::UNKNOWN;
}

status_t CombinedEngine::onRemoveRights(int uniqueId, const String8& path) {
    // No Rights to remove
    char* fileheader=(char *)malloc(FILEHEADER);
    char *uid;
    int  typeLength,uidLength;

    FILE *fp;
    if((fp=fopen(path,"rb"))!=NULL){
        if(fread(fileheader,FILEHEADER,1 ,fp)==1){
            typeLength=fileheader[7];
            uidLength=fileheader[8+typeLength];
            uid=&fileheader[9+typeLength];
            uid[uidLength]='\0';
         }
        else{
           free(fileheader);
           fclose(fp);
           return DRM_FAILURE;
         }
    }
    else{
         free(fileheader);
         return DRM_FAILURE;
    }
    fclose(fp);

    if (!SVC_drm_deleteRights((uint8_t*)uid)){
        return DRM_FAILURE;
    }
       return DRM_SUCCESS;
}

status_t CombinedEngine::onRemoveAllRights(int uniqueId) {
    // No rights to remove
   int id,maxId;
     maxId = drm_getMaxIdFromUidTxt();
     if (-1 == maxId)
     {
          return DRM_SUCCESS;
     }

    for (id = 1; id <= maxId; id++) {
      drm_removeIdInfoFile(id);
      drm_updateUidTxtWhenDelete(id);
    }
    return DRM_SUCCESS;
}

#ifdef USE_64BIT_DRM_API
status_t CombinedEngine::onSetPlaybackStatus(int uniqueId, DecryptHandle* decryptHandle,
                                            int playbackStatus, int64_t position) {
#else
status_t CombinedEngine::onSetPlaybackStatus(int uniqueId, DecryptHandle* decryptHandle,
                                            int playbackStatus, int position) {
#endif
    // Not used
    return DRM_NO_ERROR;
}

status_t CombinedEngine::onOpenConvertSession(int uniqueId,
                                         int convertId) {
    status_t result = DRM_ERROR_UNKNOWN;
    if (!convertSessionMap.isCreated(convertId)) {
        ConvertSession *newSession = new ConvertSession();
        if (CombinedConv_Status_OK ==
            CombinedConv_OpenSession(&(newSession->uniqueId), &(newSession->output))) {
            convertSessionMap.addValue(convertId, newSession);
            result = DRM_NO_ERROR;
        } else {
            delete newSession;
        }
    }
    return result;
}

DrmConvertedStatus* CombinedEngine::onConvertData(int uniqueId,
                                                 int convertId,
                                                 const DrmBuffer* inputData) {
    CombinedConv_Status_t retStatus = CombinedConv_Status_InvalidArgument;
    DrmBuffer *convResult = new DrmBuffer(NULL, 0);
    int offset = -1;

    if (NULL != inputData && convertSessionMap.isCreated(convertId)) {
        ConvertSession *convSession = convertSessionMap.getValue(convertId);

        if (NULL != convSession) {
            retStatus = CombinedConv_ConvertData(convSession->uniqueId,
                                                inputData->data,
                                                inputData->length,
                                                &(convSession->output));

            if (CombinedConv_Status_OK == retStatus) {
                // return bytes from conversion if available
                if (convSession->output.fromConvertData.numBytes > 0) {
                    convResult->data = new char[convSession->output.fromConvertData.numBytes];

                    if (NULL != convResult->data) {
                        convResult->length = convSession->output.fromConvertData.numBytes;
                        memcpy(convResult->data,
                               (char *)convSession->output.fromConvertData.pBuffer,
                               convResult->length);
                    }
                }
            } else {
                offset = convSession->output.fromConvertData.errorPos;
            }
        }
    }
    return new DrmConvertedStatus(getConvertedStatus(retStatus), convResult, offset);
}

DrmConvertedStatus* CombinedEngine::onCloseConvertSession(int uniqueId,
                                                         int convertId) {
    CombinedConv_Status_t retStatus = CombinedConv_Status_InvalidArgument;
    DrmBuffer *convResult = new DrmBuffer(NULL, 0);
    int offset = -1;


    if (convertSessionMap.isCreated(convertId)) {
        ConvertSession *convSession = convertSessionMap.getValue(convertId);

        if (NULL != convSession) {
            retStatus = CombinedConv_CloseSession(convSession->uniqueId, &(convSession->output));

            if (CombinedConv_Status_OK == retStatus) {
                offset = convSession->output.fromCloseSession.fileOffset;
                convResult->data = new char[COMBINED_DELIVERY_SIGNATURES_SIZE];

                if (NULL != convResult->data) {
                      convResult->length = COMBINED_DELIVERY_SIGNATURES_SIZE;
                      memcpy(convResult->data,
                             (char *)convSession->output.fromCloseSession.signatures,
                             convResult->length);
                }
            }
        }
        convertSessionMap.removeValue(convertId);
    }
    return new DrmConvertedStatus(getConvertedStatus(retStatus), convResult, offset);
}

#ifdef USE_64BIT_DRM_API
status_t CombinedEngine::onOpenDecryptSession(int uniqueId,
                                             DecryptHandle* decryptHandle,
                                             int fd,
                                             off64_t offset,
                                             off64_t length) {
#else
status_t CombinedEngine::onOpenDecryptSession(int uniqueId,
                                             DecryptHandle* decryptHandle,
                                             int fd,
                                             int offset,
                                             int length) {
#endif
    status_t result = DRM_ERROR_CANNOT_HANDLE;
    int fileDesc = -1;


    if ((-1 < fd) &&
        (NULL != decryptHandle) &&
        (!decodeSessionMap.isCreated(decryptHandle->decryptId))) {
        fileDesc = dup(fd);
    } else {
        return result;
    }

    if (-1 < fileDesc &&
        -1 < ::lseek(fileDesc, offset, SEEK_SET) &&
        -1 < CombinedFile_attach(fileDesc)) {
        // check for file integrity. This must be done to protect the content mangling.
        int retVal = CombinedFile_CheckHeaderIntegrity(fileDesc);
        DecodeSession* decodeSession = new DecodeSession(fileDesc);

        if (retVal && NULL != decodeSession) {
            decodeSessionMap.addValue(decryptHandle->decryptId, decodeSession);
            const char *pmime= CombinedFile_GetContentType(fileDesc);
            String8 contentType = String8(pmime == NULL ? "" : pmime);
            contentType.toLower();
            decryptHandle->mimeType = MimeTypeUtil::convertMimeType(contentType);
            decryptHandle->decryptApiType = DecryptApiType::CONTAINER_BASED;
            decryptHandle->status = RightsStatus::RIGHTS_VALID;
            decryptHandle->decryptInfo = NULL;
            result = DRM_NO_ERROR;
        } else {
            CombinedFile_detach(fileDesc);
            delete decodeSession;
        }
    }

    if (DRM_NO_ERROR != result && -1 < fileDesc) {
        ::close(fileDesc);
    }


    return result;


}

status_t CombinedEngine::onOpenDecryptSession(int uniqueId,
                                             DecryptHandle* decryptHandle,
                                             const char* uri) {
    status_t result = DRM_ERROR_CANNOT_HANDLE;
    // [drm] jian.gu@tct-nj.com
    // we already convert uri to filepath in DrmManagerClient.cpp
    if (NULL != decryptHandle && NULL != uri && onCanHandle(uniqueId, String8(uri))) {
        int fd = open(uri, O_RDONLY);
        if (-1 < fd) {
            // offset is always 0 and length is not used. so any positive size.
            result = onOpenDecryptSession(uniqueId, decryptHandle, fd, 0, 1);
            // fd is duplicated already if success. closing the file
            close(fd);
        }
    }

    return result;
}

status_t CombinedEngine::onCloseDecryptSession(int uniqueId,
                                              DecryptHandle* decryptHandle) {
    status_t result = DRM_ERROR_UNKNOWN;

    if (NULL != decryptHandle && decodeSessionMap.isCreated(decryptHandle->decryptId)) {
        DecodeSession* session = decodeSessionMap.getValue(decryptHandle->decryptId);
        if (NULL != session && session->fileDesc > -1) {
            CombinedFile_detach(session->fileDesc);
            ::close(session->fileDesc);
            decodeSessionMap.removeValue(decryptHandle->decryptId);
            result = DRM_NO_ERROR;
        }
    }

    if (NULL != decryptHandle) {
        if (NULL != decryptHandle->decryptInfo) {
            delete decryptHandle->decryptInfo;
            decryptHandle->decryptInfo = NULL;
        }

        decryptHandle->copyControlVector.clear();
        decryptHandle->extendedData.clear();

        delete decryptHandle;
        decryptHandle = NULL;
    }

    return result;
}

status_t CombinedEngine::onInitializeDecryptUnit(int uniqueId,
                                                DecryptHandle* decryptHandle,
                                                int decryptUnitId,
                                                const DrmBuffer* headerInfo) {
    return DRM_ERROR_UNKNOWN;
}

status_t CombinedEngine::onDecrypt(int uniqueId, DecryptHandle* decryptHandle, int decryptUnitId,
            const DrmBuffer* encBuffer, DrmBuffer** decBuffer, DrmBuffer* IV) {
    return DRM_ERROR_UNKNOWN;
}

status_t CombinedEngine::onDecrypt(int uniqueId,
                                  DecryptHandle* decryptHandle,
                                  int decryptUnitId,
                                  const DrmBuffer* encBuffer,
                                  DrmBuffer** decBuffer) {
    return DRM_ERROR_UNKNOWN;
}

status_t CombinedEngine::onFinalizeDecryptUnit(int uniqueId,
                                              DecryptHandle* decryptHandle,
                                              int decryptUnitId) {
    return DRM_ERROR_UNKNOWN;
}

ssize_t CombinedEngine::onRead(int uniqueId,
                              DecryptHandle* decryptHandle,
                              void* buffer,
                              int numBytes) {
    ssize_t size = -1;

    if (NULL != decryptHandle &&
       decodeSessionMap.isCreated(decryptHandle->decryptId) &&
        NULL != buffer &&
        numBytes > -1) {
        DecodeSession* session = decodeSessionMap.getValue(decryptHandle->decryptId);
        if (NULL != session && session->fileDesc > -1) {
            size = CombinedFile_read(session->fileDesc, buffer, numBytes);

            if (0 > size) {
                session->offset = ((off_t)-1);
            } else {
                session->offset += size;
            }
        }
    }

    return size;
}

#ifdef USE_64BIT_DRM_API
off64_t CombinedEngine::onLseek(int uniqueId, DecryptHandle* decryptHandle,
                               off64_t offset, int whence) {
#else
off_t CombinedEngine::onLseek(int uniqueId, DecryptHandle* decryptHandle,
                             off_t offset, int whence) {
#endif
    off_t offval = -1;

    if (NULL != decryptHandle && decodeSessionMap.isCreated(decryptHandle->decryptId)) {
        DecodeSession* session = decodeSessionMap.getValue(decryptHandle->decryptId);
        if (NULL != session && session->fileDesc > -1) {
            offval = CombinedFile_lseek(session->fileDesc, offset, whence);
            session->offset = offval;
        }
    }

    return offval;
}

#ifdef USE_64BIT_DRM_API
ssize_t CombinedEngine::onPread(int uniqueId,
                               DecryptHandle* decryptHandle,
                               void* buffer,
                               ssize_t numBytes,
                               off64_t offset) {
#else
ssize_t CombinedEngine::onPread(int uniqueId,
                               DecryptHandle* decryptHandle,
                               void* buffer,
                               ssize_t numBytes,
                               off_t offset) {
#endif
    ssize_t bytesRead = -1;

    DecodeSession* decoderSession = NULL;

    if ((NULL != decryptHandle) &&
        (NULL != (decoderSession = decodeSessionMap.getValue(decryptHandle->decryptId))) &&
        (NULL != buffer) &&
        (numBytes > -1) &&
        (offset > -1)) {
        if (offset != decoderSession->offset) {
            decoderSession->offset = onLseek(uniqueId, decryptHandle, offset, SEEK_SET);
        }

        if (((off_t)-1) != decoderSession->offset) {
            bytesRead = onRead(uniqueId, decryptHandle, buffer, numBytes);
            if (bytesRead < 0) {
            }
        }
    } else {
    }

    return bytesRead;
}
