/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************/
/*                                                               Date:11/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  LiuRuili                                                        */
/*  Email  :  ruili.liu@tcl.com                                               */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments : WPS Push Button                                                */
/*  File     :frameworks/base/wifi/java/android/net/wifi/TctSoftapMonitor.java*/
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/******************************************************************************/

package com.android.server.wifi;

import java.util.HashMap;

import android.util.Log;

import com.android.internal.util.Protocol;
import com.android.internal.util.StateMachine;

/**
 * Listens for events from the wpa_supplicant server, and passes them on to the
 * {@link StateMachine} for handling. Runs in its own thread.
 *
 * @hide
 */
public class TctSoftapMonitor {

    private static final String TAG = "TctSoftapMonitor";

    private static final int TERMINATING = 6;
    private static final int UNKNOWN = 9;

    /** All events coming from the supplicant start with this prefix */
    private static final String EVENT_PREFIX_STR = "CTRL-EVENT-";
    private static final int EVENT_PREFIX_LEN_STR = EVENT_PREFIX_STR.length();

    /** All WPA events coming from the supplicant start with this prefix */
    private static final String WPA_EVENT_PREFIX_STR = "WPA:";
    private static final String PASSWORD_MAY_BE_INCORRECT_STR =
       "pre-shared key may be incorrect";

    /* WPS events */
    private static final String WPS_OVERLAP_STR = "WPS-OVERLAP-DETECTED";
    /* WPS registration failed after M2/M2D */
    private static final String WPS_FAIL_STR = "WPS-FAIL ";
    /* WPS registration completed successfully */
    private static final String WPS_SUCCESS_STR = "WPS-SUCCESS ";
    /* WPS enrollment attempt timed out and was terminated */
    private static final String WPS_TIMEOUT_STR = "WPS-TIMEOUT ";

    /**
     * <pre>
     * CTRL-EVENT-TERMINATING - signal x
     * </pre>
     *
     * <code>x</code> is the signal that caused termination.
     */
    private static final String TERMINATING_STR = "TERMINATING";

    /* Supplicant events reported to a state machine */
    private static final int BASE = Protocol.BASE_WIFI_MONITOR;

    /* Connection to supplicant established */
    public static final int SUP_CONNECTION_EVENT                 = BASE + 1;
    /* Connection to supplicant lost */
    public static final int SUP_DISCONNECTION_EVENT              = BASE + 2;
    /* Network connection completed */
    public static final int NETWORK_CONNECTION_EVENT             = BASE + 3;
    /* Network disconnection completed */
    public static final int NETWORK_DISCONNECTION_EVENT          = BASE + 4;
    /* Scan results are available */
    public static final int SCAN_RESULTS_EVENT                   = BASE + 5;
    /* Supplicate state changed */
    public static final int SUPPLICANT_STATE_CHANGE_EVENT        = BASE + 6;
    /* Password failure and EAP authentication failure */
    public static final int AUTHENTICATION_FAILURE_EVENT         = BASE + 7;
    /* WPS success detected */
    public static final int WPS_SUCCESS_EVENT                    = BASE + 8;
    /* WPS failure detected */
    public static final int WPS_FAIL_EVENT                       = BASE + 9;
    /* WPS overlap detected */
    public static final int WPS_OVERLAP_EVENT                    = BASE + 10;
    /* WPS timeout detected */
    public static final int WPS_TIMEOUT_EVENT                    = BASE + 11;
    /* Driver was hung */
    public static final int DRIVER_HUNG_EVENT                    = BASE + 12;

    /**
     * This indicates the supplicant connection for the monitor is closed
     */
    private static final String MONITOR_SOCKET_CLOSED_STR = "connection closed";

    /**
     * This indicates a read error on the monitor socket conenction
     */
    private static final String WPA_RECV_ERROR_STR = "recv error";

    /**
     * Tracks consecutive receive errors
     */
    private int mRecvErrors = 0;

    /**
     * Max errors before we close supplicant connection
     */
    private static final int MAX_RECV_ERRORS    = 10;

    private final StateMachine mStateMachine;
    private final WifiNative mWifiNative;
    private final String mInterfaceName;
    private boolean mMonitoring;

    public TctSoftapMonitor(String iface, StateMachine wifiStateMachine) {
        mStateMachine = wifiStateMachine;
        mWifiNative = WifiNative.getWlanNativeInterface();
        mInterfaceName = iface;
        mMonitoring = false;

        TctSoftapMonitorSingleton.sInstance.registerInterfaceMonitor(mInterfaceName, this);
    }

    public void startMonitoring() {
        TctSoftapMonitorSingleton.sInstance.startMonitoring(mInterfaceName);
    }

    public void stopMonitoring() {
        TctSoftapMonitorSingleton.sInstance.stopMonitoring(mInterfaceName);
    }

    private static class TctSoftapMonitorSingleton {

        private static final TctSoftapMonitorSingleton sInstance = new TctSoftapMonitorSingleton();

        private final HashMap<String, TctSoftapMonitor> mIfaceMap = new HashMap<String, TctSoftapMonitor>();
        private boolean connected = false;
        private WifiNative wifiNative;

        public void registerInterfaceMonitor(String iface, TctSoftapMonitor m) {
            Log.d(TAG, "registerInterface(" + iface + ")");
            mIfaceMap.put(iface, m);
            if (wifiNative == null) {
                wifiNative = m.mWifiNative;
            }
        }

        public synchronized void startMonitoring(String iface) {
            TctSoftapMonitor m = mIfaceMap.get(iface);
            if (m == null) {
                Log.e(TAG, "startMonitor called with unknown iface=" + iface);
                return;
            }

            Log.d(TAG, "startMonitoring(" + iface + ") with mConnected = " + connected);

            if (connected) {
                m.mMonitoring = true;
            } else {
                int connectTries = 0;
                while (true) {
                    if (wifiNative.connectTosoftap()) {
                        m.mMonitoring = true;
                        connected = true;
                        new MonitorThread(wifiNative, this).start();
                        break;
                    }
                    if (connectTries++ < 5) {
                        try {
                            Thread.sleep(1 * 1000);
                        } catch (InterruptedException ignore) {
                        }
                    } else {
                        Log.d(TAG, "startMonitoring(" + iface + ") failed!");
                        break;
                    }
                }
            }
        }

        public synchronized void stopMonitoring(String iface) {
            TctSoftapMonitor m = mIfaceMap.get(iface);
            Log.d(TAG, "stopMonitoring(" + iface + ")");
            m.mMonitoring = false;
            connected = false;
            m.mWifiNative.closeSupplicantConnection();
        }

        public boolean dispatchEvent(String eventStr, String iface) {

            Log.d(TAG, "Dispatching event to interface: " + iface);

            TctSoftapMonitor m = mIfaceMap.get(iface);
            if (m != null) {
                if (m.mMonitoring) {
                    if (m.dispatchEvent(eventStr, iface)) {
                        connected = false;
                        return true;
                    }
                    return false;
                } else {
                    Log.d(TAG, "Dropping event because (" + iface + ") is stopped");
                    return false;
                }
            } else {
                Log.d(TAG, "Dropping event because there's no matching iface");
                return false;
            }
        }
    }

    private static class MonitorThread extends Thread {

        private final WifiNative mWifiNative;
        private final TctSoftapMonitorSingleton mTctSoftapMonitorSingleton;

        public MonitorThread(WifiNative wifiNative,
                TctSoftapMonitorSingleton tctSoftapMonitorSingleton) {
            super("TctSoftapMonitor");
            mWifiNative = wifiNative;
            mTctSoftapMonitorSingleton = tctSoftapMonitorSingleton;
        }

        public void run() {

            mWifiNative.startWpsPbcCommand();

            for (;;) {
                if (!mTctSoftapMonitorSingleton.connected) {
                    Log.d(TAG, "MonitorThread exit because connected is false");
                    break;
                }

                String eventStr = mWifiNative.waitForEvent();
                if (mTctSoftapMonitorSingleton.dispatchEvent(eventStr, mWifiNative.getInterfaceName())) {
                    Log.d(TAG, "Disconnecting from the supplicant, no more events");
                    break;
                }
            }
        }
    }

    public boolean dispatchEvent(String eventStr, String iface) {

        Log.d(TAG, "eventStr = " + eventStr + " iface = " + iface);

        if (!eventStr.startsWith(EVENT_PREFIX_STR)) {
            if (eventStr.startsWith(WPA_EVENT_PREFIX_STR)
                    && 0 < eventStr.indexOf(PASSWORD_MAY_BE_INCORRECT_STR)) {
            } else if (eventStr.startsWith(WPS_OVERLAP_STR)) {
                mStateMachine.sendMessage(WPS_OVERLAP_EVENT);
            } else if (eventStr.startsWith(WPS_FAIL_STR)) {
                mStateMachine.sendMessage(WPS_FAIL_EVENT);
            } else if (eventStr.startsWith(WPS_TIMEOUT_STR)) {
                mStateMachine.sendMessage(WPS_TIMEOUT_EVENT);
                stopMonitoring();
                return true;
            } else if (eventStr.startsWith(WPS_SUCCESS_STR)) {
                mStateMachine.sendMessage(WPS_SUCCESS_EVENT);
                stopMonitoring();
                return true;
            }
            return false;
        }

        String eventName = eventStr.substring(EVENT_PREFIX_LEN_STR);
        int nameEnd = eventName.indexOf(' ');
        if (nameEnd != -1)
            eventName = eventName.substring(0, nameEnd);
        if (eventName.length() == 0) {
            return false;
        }

        int event;
        if (eventName.equals(TERMINATING_STR))
            event = TERMINATING;
        else
            event = UNKNOWN;

        String eventData = eventStr;
        if (event == TERMINATING) {
            int ind = eventStr.indexOf(" - ");
            if (ind != -1) {
                eventData = eventStr.substring(ind + 3);
            }
        }

        if (event == TERMINATING) {
            if (eventData.startsWith(MONITOR_SOCKET_CLOSED_STR)) {
                return true;
            }
            if (eventData.startsWith(WPA_RECV_ERROR_STR)) {
                if (++mRecvErrors > MAX_RECV_ERRORS) {
                } else {
                    return false;
                }
            }
            return true;
        }
        mRecvErrors = 0;
        return false;
    }
}
