/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2013 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  jianglong pan                                                   */
/*  Email  :  jianglong.pan@tcl-mobile.com                                    */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : frameworks/tct-ext/                                            */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 07/30/2013|Jianglong Pan         |PR-493060             |CDMA operator name*/
/* ----------|----------------------|----------------------|----------------- */
/* 12/05/2013|huiyuan.wang          |PR-566061             |[Network][MMI]DUT */
/*           |                      |                      | shows "14174" on */
/*           |                      |                      | notification bar */
/*           |                      |                      | instead of "CHIN */
/*           |                      |                      |A TELECOM" after  */
/*           |                      |                      |turn off LTE      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

/******************************************************************************/

package com.android.internal.telephony.cdma;
import java.util.HashMap;

/**
 * {@hide}
 */
public class TctSidSpnMap {
    private static TctSidSpnMap sTctSidSpnMap;
    private HashMap<Integer, String[]> mSidToSpnMap;

    private TctSidSpnMap() {
        mSidToSpnMap = new HashMap<Integer, String[]>();
    }

    private void loadEntry(Integer sid, String[] spn) {
        if (!mSidToSpnMap.containsKey(sid)) {
            mSidToSpnMap.put(sid, spn);
        }
    }

    public String[] getSpnFromSid(int sid) {
        if (sid != 0) {
            return mSidToSpnMap.get(sid);
        }
        return null;
    }

    public boolean hasSid(int sid) {
        if (sid != 0) {
            return mSidToSpnMap.containsKey(sid);
        }
        return false;
    }

    public static TctSidSpnMap getSingleton(){
        if (sTctSidSpnMap == null) {
            sTctSidSpnMap = new TctSidSpnMap();
            // Revol Wireless 20
            sTctSidSpnMap.loadEntry(5406,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(4355,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(5197,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(6507,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(6506,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(6505,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(5945,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(5450,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(5117,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(5035,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(4391,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(4755,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(6504,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(5361,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(5358,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(4255,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(4847,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(6471,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(4271,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(5203,new String[]{"REVOL","REVOL"});
            sTctSidSpnMap.loadEntry(4225,new String[]{"REVOL","REVOL"});

            //Open Mobile
            sTctSidSpnMap.loadEntry(5205,new String[]{"Open Mobile","Open Mobile"});

            //Pioneer
            sTctSidSpnMap.loadEntry(324,new String[]{"Pioneer","Pioneer"});

            //Nex-Tech
            sTctSidSpnMap.loadEntry(6499,new String[]{"NTW","NTW"});
            sTctSidSpnMap.loadEntry(5216,new String[]{"NTW","NTW"});


            //05 Element Mobile
            sTctSidSpnMap.loadEntry(1816,new String[]{"Element Mobile","Element Mobile"});

            //Alaska Communications
            //05269/65535 ; 00234/65535; 05510/65535; 05513/65535; 05682/65535 ; 05685/65535; 01018/65535 ; 01022/65535 ; 02082/65535
            sTctSidSpnMap.loadEntry(5269,new String[]{"Alaska Communications","ACS"});
            sTctSidSpnMap.loadEntry(234,new String[]{"Alaska Communications","ACS"});
            sTctSidSpnMap.loadEntry(5510,new String[]{"Alaska Communications","ACS"});
            sTctSidSpnMap.loadEntry(5513,new String[]{"Alaska Communications","ACS"});
            sTctSidSpnMap.loadEntry(5682,new String[]{"Alaska Communications","ACS"});
            sTctSidSpnMap.loadEntry(5685,new String[]{"Alaska Communications","ACS"});
            sTctSidSpnMap.loadEntry(1018,new String[]{"Alaska Communications","ACS"});
            sTctSidSpnMap.loadEntry(1022,new String[]{"Alaska Communications","ACS"});
            sTctSidSpnMap.loadEntry(2082,new String[]{"Alaska Communications","ACS"});

            //Appalachian wireless
            sTctSidSpnMap.loadEntry(1290,new String[]{"Appalachian wireless","Appalachian wireless"});

            // Carolina West
            sTctSidSpnMap.loadEntry(1522,new String[]{"Carolina West","Carolina West"});

            // Golden State
            sTctSidSpnMap.loadEntry(1064,new String[]{"Golden State","GSC"});

            //Illinois Valley,1178/65535
            sTctSidSpnMap.loadEntry(1178,new String[]{"Illinois Valley","IVC"});

            //Inland Cellular,1788/65535(Carrier Customized),2004/65535(default)
            sTctSidSpnMap.loadEntry(1788,new String[]{"Inland Cellular","Inland"});
            sTctSidSpnMap.loadEntry(2004,new String[]{"Inland Cellular","Inland"});

            //Mobi PCS,4613/65535
            sTctSidSpnMap.loadEntry(4613, new String[]{"Mobi PCS","MPCS"});

            //Sagebrush(Nemont),31092/65535
            sTctSidSpnMap.loadEntry(31092, new String[]{"Nemont","Nemont"});

            //Strata,1858/65535, 6486/65535, 6007/65535
            sTctSidSpnMap.loadEntry(1858,new String[]{"Strata","Strata"});
            sTctSidSpnMap.loadEntry(6486,new String[]{"Strata","Strata"});
            sTctSidSpnMap.loadEntry(6007,new String[]{"Strata","Strata"});

            //Thumb Cellular,01350/65535
            sTctSidSpnMap.loadEntry(1350, new String[]{"Thumb Cellular","Thumb"});

            //Flat Wireless
            sTctSidSpnMap.loadEntry(21575, new String[]{"Flat Wireless","Flat"});
            sTctSidSpnMap.loadEntry(4565, new String[]{"Flat Wireless","Flat"});
            sTctSidSpnMap.loadEntry(4477, new String[]{"Flat Wireless","Flat"});
            sTctSidSpnMap.loadEntry(5210, new String[]{"Flat Wireless","Flat"});

            //Cross Wireless
            sTctSidSpnMap.loadEntry(21605, new String[]{"Sprocket","Sprocket"});
            //Clear Talk
            sTctSidSpnMap.loadEntry(21607, new String[]{"Clear Talk","Clear Talk"});
            sTctSidSpnMap.loadEntry(21608, new String[]{"Clear Talk","Clear Talk"});

            //MobileNation(SI)
            sTctSidSpnMap.loadEntry(4307, new String[]{"MobileNation","MobileNation"});

            //China Telecom
            sTctSidSpnMap.loadEntry(14124, new String[]{"China Telecom","China Telecom"});
            sTctSidSpnMap.loadEntry(13840, new String[]{"China Telecom","China Telecom"});
            sTctSidSpnMap.loadEntry(14174, new String[]{"China Telecom","China Telecom"});//[FEATURE]-Add-BEGIN by TCTNB.huiyuan.wang, 2013-12-05,PR566061

               //Mohave
            sTctSidSpnMap.loadEntry(1024,new String[]{"Mohave","Mohave"});

            //Panhandle
            sTctSidSpnMap.loadEntry(1580,new String[]{"PTCI","PTCI"});
            sTctSidSpnMap.loadEntry(50,new String[]{"PTCI","PTCI"});

            //NorthWest Mo Cell,1396/65535
            sTctSidSpnMap.loadEntry(1396, new String[]{"NorthWest Mo Cell","NWMC"});

            //James Valley Wireless ,5238/65535
            sTctSidSpnMap.loadEntry(5238, new String[]{"NVC","NVC"});
            //United
            sTctSidSpnMap.loadEntry(6498, new String[]{"United Wireless","United Wireless"});
            //SRT
            sTctSidSpnMap.loadEntry(5846, new String[]{"SRT","SRT"});
            //Bluegrass
            sTctSidSpnMap.loadEntry(1280, new String[]{"Bluegrass","Bluegrass"});
            sTctSidSpnMap.loadEntry(6490, new String[]{"Bluegrass","Bluegrass"});
            sTctSidSpnMap.loadEntry(6492, new String[]{"Bluegrass","Bluegrass"});
        }
        return sTctSidSpnMap;
    }
}
