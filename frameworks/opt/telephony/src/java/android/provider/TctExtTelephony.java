/*****************************************************************************/
/*                                                           Date:2015/01/17 */
/*                                PRESENTATION                               */
/*                                                                           */
/*       Copyright 2013 TCL Communication Technology Holdings Limited.       */
/*                                                                           */
/* This material is company confidential, cannot be reproduced in any form   */
/* without the written permission of TCL Communication Technology Holdings   */
/* Limited.                                                                  */
/*                                                                           */
/* ------------------------------------------------------------------------- */
/*  Author :                                                                 */
/*  Email  :                                                                 */
/*  Role   :                                                                 */
/*  Reference documents :                                                    */
/* ------------------------------------------------------------------------- */
/*  Comments :                                                               */
/*  File     :                                                               */
/*  Labels   :                                                               */
/* ------------------------------------------------------------------------- */
/* ========================================================================= */
/*     Modifications on Features list / Changes Request / Problems Report    */
/* ------------------------------------------------------------------------- */
/*   date    |   author      |   Key   |                comment              */
/* ----------|------------   |---------|-------------------------------------*/
/* 03/23/2015|   qingyi.he   |  946572 |[GCF][OMA-ETS-MMS-1.3]Delivery report*/
/*           |               |         |failed                               */
/* ----------|-----------    |---------|-------------------------------------*/
/*****************************************************************************/
package android.provider;

/**
 *@hide
 */
public class TctExtTelephony {

    public static final String DELIVERY_R =  "delivery_r";

    private Telephony mTelephony = null;//MODIFICATIONS FORBIDDEN
    TctExtTelephony(Telephony telephony){//MODIFICATIONS FORBIDDEN
        mTelephony = telephony;
    }
}