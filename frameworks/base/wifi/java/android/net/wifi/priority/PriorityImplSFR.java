/******************************************************************************/
/*                                                               Date:10/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Yu.Feng                                                         */
/*  Email  :  Yu.Feng@tcl-mobile.com                                          */
/*  Role   :                                                                  */
/*  Reference documents : SFR mobile technical requirements                   */
/* -------------------------------------------------------------------------- */
/*  Comments : Implements of wifi priority on SFR mobile, Private SSIDs (WEP/ */
/*             WPA-PSK/WPA2-PSK) have grade Three, SSIDs like EAP-SIM have gr */
/*             ade Two, Public SSIDs have grade one                           */
/*  File     :                                                                */
/*  Labels   : Wifi Priority                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */

package android.net.wifi.priority;

import java.util.List;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;

/**
 * @hide
 */
public class PriorityImplSFR implements IPriority{

    @Override
    public int getPriorityGrade(WifiConfiguration mConfig) {
        //private ssids
        if(mConfig.allowedKeyManagement.get(KeyMgmt.WPA_PSK) || mConfig.allowedKeyManagement.get(KeyMgmt.WPA2_PSK)){
            return IPriority.PRIORITY_GRADE_THREE;
        }
        //enterprise ssids
        if(mConfig.allowedKeyManagement.get(KeyMgmt.WPA_EAP) || mConfig.allowedKeyManagement.get(KeyMgmt.IEEE8021X)) {
            return IPriority.PRIORITY_GRADE_TWO;
        }
        //WEP ssids
        if(mConfig.wepKeys[0] != null) {
            return IPriority.PRIORITY_GRADE_THREE;
        }
        return IPriority.PRIORITY_GRADE_ONE;
    }
}
