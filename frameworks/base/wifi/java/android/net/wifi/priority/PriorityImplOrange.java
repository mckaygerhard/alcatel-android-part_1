/******************************************************************************/
/*                                                               Date:10/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Yu.Feng                                                         */
/*  Email  :  Yu.Feng@tcl-mobile.com                                          */
/*  Role   :                                                                  */
/*  Reference documents : Orange Requirements of Wifi                         */
/* -------------------------------------------------------------------------- */
/*  Comments : WIfi priority implements of Orange. Private SSIDs like WPA_PSK */
/*             ,WPA2_PSK have the highest priority of grade three, WEP and En */
/*             terprise SSIDs (IEEE8021X) like EAP-SIM have priority of grade */
/*              two, then come the public SSIDs                               */
/*  File     :                                                                */
/*  Labels   : Wifi Priority                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */

package android.net.wifi.priority;

import java.util.List;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;
/**
 * Wifi priority implements of Orange.
 * @author Yu.Feng 93421
 * @hide
 */
public class PriorityImplOrange implements IPriority{

    @Override
    public int getPriorityGrade(WifiConfiguration mConfig) {
        //private ssids
        if(mConfig.allowedKeyManagement.get(KeyMgmt.WPA_PSK) || mConfig.allowedKeyManagement.get(KeyMgmt.WPA2_PSK)){
            return IPriority.PRIORITY_GRADE_THREE;
        }
        //enterprise ssids
        if(mConfig.allowedKeyManagement.get(KeyMgmt.WPA_EAP) || mConfig.allowedKeyManagement.get(KeyMgmt.IEEE8021X)) {
            return IPriority.PRIORITY_GRADE_TWO;
        }
        //WEP ssids
        if(mConfig.wepKeys[0] != null) {
            return IPriority.PRIORITY_GRADE_TWO;
        }
        return IPriority.PRIORITY_GRADE_ONE;
    }

}
