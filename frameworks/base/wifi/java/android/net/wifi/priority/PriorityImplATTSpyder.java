/******************************************************************************/
/*                                                               Date:11/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Yu.Feng                                                         */
/*  Email  :  Yu.Feng@tcl-mobile.com                                          */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments : [18.11][CDR-LAN-1720]SSID Prioritization                       */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/13/2012|Yu.Feng               |                      |Add new wifi prio */
/*           |                      |                      |rity strategy to  */
/*           |                      |                      |support ATT EAP-S */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package android.net.wifi.priority;

import java.util.List;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;

/**
 * @hide
 */
public class PriorityImplATTSpyder implements IPriority{

    private static final String SSID_Z736563757265 = "Z736563757265";

    private static final String SSID_ATTWIFI = "attwifi";


    @Override
    public int getPriorityGrade(WifiConfiguration mConfig) {
        //private ssids
        if(mConfig.allowedKeyManagement.get(KeyMgmt.WPA_PSK) || mConfig.allowedKeyManagement.get(KeyMgmt.WPA2_PSK)){
            return IPriority.PRIORITY_GRADE_FOUR;
        }
        //preconfiged ssids (EAP-SIM EAP-APA)
        if(mConfig.allowedKeyManagement.get(KeyMgmt.WPA_EAP) || mConfig.allowedKeyManagement.get(KeyMgmt.IEEE8021X)) {
            if(mConfig.SSID.trim().equals(this.SSID_Z736563757265)) {
                return IPriority.PRIORITY_GRADE_THREE;
            }
            if(mConfig.SSID.trim().equals(this.SSID_ATTWIFI)) {
                return IPriority.PRIORITY_GRADE_TWO;
            }
        }
        return IPriority.PRIORITY_GRADE_ONE;
    }

}
