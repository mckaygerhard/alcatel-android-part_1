/******************************************************************************/
/*                                                               Date:10/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Yu.Feng                                                         */
/*  Email  :  Yu.Feng@tcl-mobile.com                                          */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments : Wifi priority interface, its implements makes different wifi p */
/*             riority strategies                                             */
/*  File     :                                                                */
/*  Labels   : Wifi Priority                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */

package android.net.wifi.priority;

import java.util.List;

import android.net.wifi.WifiConfiguration;

/**
 * @hide
 */
public interface IPriority {

    public static int PRIORITY_GRADE_ONE = 1;

    public static int PRIORITY_GRADE_TWO = 2;

    public static int PRIORITY_GRADE_THREE = 3;

    public static int PRIORITY_GRADE_FOUR = 4;

    public static int PRIORITY_GRADE_FIVE = 5;


    //public void updatePriority(WifiConfigStore wifiConfigStore,WifiConfiguration mConfig);

    //public void updateAllSavedPriority(WifiConfigStore wifiConfigStore,List<WifiConfiguration> configs);

    public int getPriorityGrade(WifiConfiguration mConfig);

}
