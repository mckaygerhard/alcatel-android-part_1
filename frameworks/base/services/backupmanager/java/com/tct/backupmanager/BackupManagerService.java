package com.tct.backupmanager;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.ServiceManager;
import android.util.Log;
import android.util.Slog;
import com.tct.backupmanager.IBackupManagerService;
import com.tct.backupmanager.IBackupManagerServiceCallback;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemProperties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BackupManagerService extends IBackupManagerService.Stub {
    private static final String TAG = "BackupManagerService";
    private boolean DBG = true;
    private Context mContext = null;
    private final BackupHandler mHandler;

    private static final String APP_DATA_PATH_CFG_FILE = "/data/.backup.cfg";
    private static final String APP_TRANS_FILE = "/data/.backupTrans.cfg";
    private static final String APP_CFG_FILE = "/data/.backupCfg.cfg";
    private static final String processUpdateAction = "com.android.processUpdate";
    private static final long UPDATE_INTERVAL = 1000l;
    private boolean isRunning = false;

    /**
     * Recover successfully with no error
     * @hide 
     */
    public static final int RECOVER_SUCCESS = 0;

    private static final int STATE_INIT = 1;
    private static final int STATE_PROCESS_BEGIN = 2;
    private static final int STATE_HAS_CHANGED = 3;
    private static final int STATE_PROCESS_END = 4;
    private static final int STATE_PROCESS_ERROR = 5;
    private static final int STATE_MONITOR = 6;

    final static RemoteCallbackList<IBackupManagerServiceCallback> mCallbacks = new RemoteCallbackList<IBackupManagerServiceCallback>();

    /**
     * Interface for modules need on-demand backup
     * @hide
     */
    public BackupManagerService (Context context) throws Exception {
        if (DBG) Slog.d(TAG,"init");

        try {
            mContext = context;
            HandlerThread thread = new HandlerThread("BackupManagerService");
            thread.start();
            mHandler = new BackupHandler(thread.getLooper());
        } catch (Exception e) {
            Slog.e(TAG, "BackupManagerService() failed", e);
            throw new Exception(e);
        }
    }

    public void registerCallback(IBackupManagerServiceCallback cb) throws RemoteException {
        if (DBG) Slog.d(TAG,"registerCallback");

        if (cb != null) {
            mCallbacks.register(cb);
        }
    }

    public void unregisterCallback(IBackupManagerServiceCallback cb) throws RemoteException {
        if (DBG) Slog.d(TAG,"unregisterCallback");

        if (cb != null) {
            mCallbacks.unregister(cb);
        }
    }

    public void start() {
        if (DBG) Slog.d(TAG,"start");

        mHandler.sendMessageDelayed(this.mHandler.obtainMessage(STATE_INIT), 500);
    }

    public boolean beginBack(String[] files, String dst, int type) {
        if (DBG) Slog.d(TAG, "beginBack");

        if (isRunning) {
            Slog.d(TAG, "service is running, please wait");
            return false;
        }

        if (files == null || files.length == 0 || dst == null) {
            Slog.e(TAG, "param is not right");
            return false;
        }

        isRunning = true;

        mHandler.sendMessage(mHandler.obtainMessage(STATE_PROCESS_BEGIN));

        FileOutputStream outPath = null;
        try {
            outPath = new FileOutputStream(new File(APP_DATA_PATH_CFG_FILE));

            for (String filename:files) {
                if (DBG) Slog.d(TAG, "filename: " + filename);
                
                outPath.write(filename.getBytes());
                outPath.write('\n');
            }
        } catch (IOException e) {
            Slog.e(TAG, "backup IOException" + e.getMessage());
            e.printStackTrace();
        } finally {
            if (null != outPath) {
                try {
                    outPath.flush();
                    outPath.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        FileOutputStream cfgPath = null;
        try {
            cfgPath = new FileOutputStream(new File(APP_CFG_FILE));

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("dst=" + dst + ",type=" + type);

            cfgPath.write(stringBuilder.toString().getBytes());
            cfgPath.write('\n');
        } catch (IOException e) {
            Slog.e(TAG, "backup cfg IOException" + e.getMessage());
            e.printStackTrace();
        } finally {
            if (null != cfgPath) {
                try {
                    cfgPath.flush();
                    cfgPath.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        File transFile = new File(APP_TRANS_FILE);
        if (transFile.exists()) {
            transFile.delete();
        }

        SystemProperties.set("ctl.start", "backupAppData");

        return true;
    }

    public boolean beginRestore(String[] files, String dst, int type) {
        if (DBG) Slog.d(TAG, "beginRestore");

        if (isRunning) {
            Slog.d(TAG, "service is running, please wait");
            return false;
        }

        if (files == null || files.length == 0 || dst == null) {
            Slog.e(TAG, "param is not right");
            return false;
        }

        isRunning = true;

        mHandler.sendMessage(mHandler.obtainMessage(STATE_PROCESS_BEGIN));

        FileOutputStream outPath = null;
        try {
            outPath = new FileOutputStream(new File(APP_DATA_PATH_CFG_FILE));

            for (String filename:files) {
                if (DBG) Slog.d(TAG, "filename: " + filename);
                
                outPath.write(filename.getBytes());
                outPath.write('\n');
            }
        } catch (IOException e) {
            Slog.e(TAG, "restore IOException" + e.getMessage());
            e.printStackTrace();
        } finally {
            if (null != outPath) {
                try {
                    outPath.flush();
                    outPath.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        FileOutputStream cfgPath = null;
        try {
            cfgPath = new FileOutputStream(new File(APP_CFG_FILE));

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("dst=" + dst + ",type=" + type);

            cfgPath.write(stringBuilder.toString().getBytes());
            cfgPath.write('\n');
        } catch (IOException e) {
            Slog.e(TAG, "restore cfg IOException" + e.getMessage());
            e.printStackTrace();
        } finally {
            if (null != cfgPath) {
                try {
                    cfgPath.flush();
                    cfgPath.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        File transFile = new File(APP_TRANS_FILE);
        if (transFile.exists()) {
            transFile.delete();
        }

        SystemProperties.set("ctl.start", "restoreAppData");

        return true;
    }

    public void pause() {
        if (DBG) Slog.d(TAG,"pause isRunning: " + isRunning);

        if (isRunning) {
            File transFile = new File(APP_TRANS_FILE);
            if (!transFile.exists()) {
                try {
                    transFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                    Slog.e(TAG, "create pause file fail: " + e.getMessage());
                }
            }
        }
    }

    private final Runnable mover = new Runnable() {
            @Override
            public void run() {
                int count = 0;
                File pipeFile = new File("/data/tmpfile");

                while (count < 30) {
                    if (pipeFile.exists()) {
                        break;
                    } else {
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException unused) {
                        }

                        count ++;
                    }
                }

                if (count >= 30) {
                    Message msg = mHandler.obtainMessage(STATE_PROCESS_ERROR);
                    msg.obj = "error1";
                    mHandler.sendMessage(msg);
                    Slog.e(TAG, "wait for pipe file timeout");
                    mHandler.sendEmptyMessageDelayed(STATE_PROCESS_END, 300);
                    return;
                }

                boolean result = true;

                FileInputStream inputStream = null;
                BufferedReader mReader = null;

                try {
                    inputStream = new FileInputStream("/data/tmpfile");
                    mReader = new BufferedReader(new InputStreamReader(inputStream));
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                    Slog.e(TAG, "open pipe file io exception: " + e1);

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException unused) {
                    }

                    try {
                        inputStream = new FileInputStream("/data/tmpfile");
                        mReader = new BufferedReader(new InputStreamReader(inputStream));
                    } catch (FileNotFoundException e2) {
                        e2.printStackTrace();
                        result = false;
                        Slog.e(TAG, "open pipe file second time io exception: " + e2);
                    }
                }

                if (result) {
                    String data = null;
                    long last = System.currentTimeMillis();

                    try {
                        while ( (data = mReader.readLine()) != null) {
                            long current = System.currentTimeMillis();

                            if (data.startsWith("=====")) {
                                Slog.d(TAG, "msg: " + data);
                                if (data.startsWith("=====Exit")) {
                                    Message msg = mHandler.obtainMessage(STATE_PROCESS_ERROR);
                                    msg.obj = data;
                                    mHandler.sendMessage(msg);
                                    mHandler.sendEmptyMessageDelayed(STATE_PROCESS_END, 300);
                                    return;
                                } else if (data.startsWith("=====Fail")) {
                                    Message msg = mHandler.obtainMessage(STATE_PROCESS_ERROR);
                                    msg.obj = data;
                                    mHandler.sendMessage(msg);
                                } else if(data.startsWith("=====Pause")) {
                                    mHandler.sendEmptyMessageDelayed(STATE_PROCESS_END, 300);
                                    return;
                                } else if(data.startsWith("=====Start:")) {
                                    Message msg = mHandler.obtainMessage(STATE_HAS_CHANGED);
                                    msg.obj = data.substring(5);
                                    mHandler.sendMessage(msg);
                                    last = current;
                                } else if(data.startsWith("=====End:")) {
                                    Message msg = mHandler.obtainMessage(STATE_HAS_CHANGED);
                                    msg.obj = data.substring(5);
                                    mHandler.sendMessage(msg);
                                    last = current;
                                }
                            } else {
                                if (current - last > UPDATE_INTERVAL) {
                                    Message msg = mHandler.obtainMessage(STATE_HAS_CHANGED);
                                    msg.obj = data;
                                    mHandler.sendMessage(msg);
                                    last = current;
                                }
                            }

                            if (DBG) Slog.d(TAG, "data: " + data);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        result = false;
                        Slog.e(TAG, "open pipe file io exception2: " + e);
                    } finally {
                        if (mReader != null) {
                            try {
                                mReader.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                if (result) {
                    mHandler.sendEmptyMessageDelayed(STATE_PROCESS_END, 300);
                } else {
                    Message msg = mHandler.obtainMessage(STATE_PROCESS_ERROR);
                    msg.obj = "error2";
                    mHandler.sendMessage(msg);
                    mHandler.sendEmptyMessageDelayed(STATE_PROCESS_END, 300);
                }

                if (DBG) Slog.d(TAG, "The end");
            }
        };

    class BackupHandler extends Handler {
        BackupHandler(Looper paramLooper) {
            super(paramLooper); 
        }

        public void handleMessage(Message msg) {
            try {
                doHandleMessage(msg);
            } finally {
                Process.setThreadPriority(10);
            }
        }

        void doHandleMessage(Message msg) {
            switch (msg.what) {
            case STATE_INIT:
                if (DBG) Slog.d(TAG,"STATE_INIT register receiver");
                break;
            case STATE_PROCESS_BEGIN:
                if (DBG) Slog.d(TAG,"STATE_PROCESS_BEGIN");
                notifyStart();
                break;
            case STATE_HAS_CHANGED:
                if (DBG) Slog.d(TAG,"STATE_HAS_CHANGED");
                if (msg.obj != null) {
                    notifyUpdate((String) msg.obj);
                }
                break;
            case STATE_PROCESS_END:
                if (DBG) Slog.d(TAG,"STATE_PROCESS_END");
                notifyEnd();
                break;
            case STATE_PROCESS_ERROR:
                if (DBG) Slog.d(TAG,"STATE_PROCESS_ERROR");
                if (msg.obj != null) {
                    notifyError((String) msg.obj);
                }
                break;
            case STATE_MONITOR:
                if (DBG) Slog.d(TAG,"STATE_MONITOR");

                Thread mThread = new Thread(mover);
                mThread.start();
                break;
            }
        }
    }

    private void notifyStart() {
        int N = mCallbacks.beginBroadcast();
        for (int i=0; i<N; i++) {
            try {
                mCallbacks.getBroadcastItem(i).onStart();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        mCallbacks.finishBroadcast();

//        mHandler.postDelayed(mover, 100);
        mHandler.sendEmptyMessageDelayed(STATE_MONITOR, 100);
    }

    private void notifyEnd() {
        int N = mCallbacks.beginBroadcast();
        for (int i=0; i<N; i++) {
            try {
                mCallbacks.getBroadcastItem(i).onComplete();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        mCallbacks.finishBroadcast();
        isRunning = false;
    }

    private void notifyUpdate(String info) {
        int N = mCallbacks.beginBroadcast();

        if (DBG) Slog.d(TAG, "notifyUpdate: " + N + ", info: " + info);

        for (int i=0; i<N; i++) {
            try {
                mCallbacks.getBroadcastItem(i).onUpdate(info);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        mCallbacks.finishBroadcast();
    }

    private void notifyError(String info) {
        int N = mCallbacks.beginBroadcast();

        if (DBG) Slog.d(TAG, "notifyError: " + N + ", info: " + info);

        for (int i=0; i<N; i++) {
            try {
                mCallbacks.getBroadcastItem(i).onError(info);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        mCallbacks.finishBroadcast();
    }

}
