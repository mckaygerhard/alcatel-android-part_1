
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import android.os.SystemProperties;
import android.util.TctLog;

class CommonSIMCardStore implements ISIMCardStore {
    private static final String TAG = "SSV_CommonSIMCardStore";
    private static final String OPERATOR_KEY = "ro.ssv.operator.choose";
    private static final boolean DEBUG_SSV = SsvUtil.DEBUG_SSV;
    private List<SIMCardInfo> SIMCARD_ITEMS = null;
    String operator = "AMV";

    public CommonSIMCardStore() {
        init();
    }

    private void init() {

        operator = SystemProperties.get(OPERATOR_KEY, "AMV");
        MncMappedXmlParser parser = new MncMappedXmlParser();
        try {
            SIMCARD_ITEMS = parser.parse(operator);
            TctLog.d(TAG,"init simcard info from xml : size = " + SIMCARD_ITEMS.size());
        } catch (Exception e) {
            TctLog.d(TAG, "parse xml exception : " + e.getMessage());
        }
    }

    //@override
    public boolean compareOperatorSIMCard(String mappedlhs, String mappedrhs) {
        if(mappedlhs.equals(mappedrhs)) {
            return true;
        } else {
            String mccl = (mappedlhs.length() >= 3 ? mappedlhs.substring(0, 3) : mappedlhs);
            String mccr = (mappedrhs.length() >= 3 ? mappedrhs.substring(0, 3) : mappedrhs);
            if(mccl.equals(mccr) && SIMCARD_ITEMS.contains(mccr)) return true; // ro.ssv.mccmnclist may set to be "675;779" which means same mcc, differnet can be OK
            return false;
        }
    }

    //@override
    public boolean isValidOperatorSIMCard(String mapped) {
        if(!SsvUtil.isValidMccMnc(mapped)){
            return false;
        }

        for (int i = 0; i < SIMCARD_ITEMS.size(); i++) {
            if (mapped.equals(SIMCARD_ITEMS.get(i).getMccmnc())) {
                return true;
            }
        }
        return false;
    }

    //@override
    public String getMappedMccMnc(String mccmnc, String spn) {
        if (SsvUtil.isValidMccMnc(mccmnc))
            return mccmnc;

        return mccmnc;
    }

    //@override
    public String toString() {
        Iterator<SIMCardInfo> itr = SIMCARD_ITEMS.iterator();
        StringBuilder builder = new StringBuilder();
        int index = 0;
        builder.append(" SSV_CommonSIMCardStore \n");
        while (itr.hasNext()) {
            SIMCardInfo item = itr.next();
            builder.append("Item").append(String.valueOf(index)).append("->: ");
            builder.append(item.toString());
            builder.append("\n");
            index++;
        }
        return builder.toString();
    }
}
