package com.android.server;

import java.lang.String;
import android.util.TctLog;

/**
 * Created by wei.huang on 9/9/15.
 */
public class SIMCardInfo {
    private static final String TAG = "SIMCardInfo";
    private String mcc;
    private String mnc;
    private String spn;
    private String gid;
    private String mapped;
    private String type;
    private String desc;
    private String operator;
    private String imsi;

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getSpn() {
        return spn;
    }

    public void setSpn(String spn) {
        this.spn = spn;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMnc() {
        return mnc;
    }

    public void setMnc(String mnc) {
        this.mnc = mnc;
    }

    public String getMapped() {
        return mapped;
    }

    public void setMapped(String mapped) {
        this.mapped = mapped;
    }

    public String getMappedMccMnc() {
        String mappedmccmnc = mcc + mapped;
        TctLog.d(TAG, "getMappedMccMnc = " + mappedmccmnc);
        return mappedmccmnc;
    }

    public String getMccMncSpn() {
        return mcc + mnc + (spn == null ? "" : spn);
    }

    public String getMccMncGid() {
        return mcc + mnc + gid;
    }

    public String getMccmnc(){
        return mcc + mnc;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SIMCardInfo : mcc = ").append(mcc).append(" mnc = ").append(mnc).append(" spn = ").append(spn).append(" operator = ").append(operator)
                .append(" gid = ").append(gid).append(" mapped = ").append(mapped).append(" type = ").append(type).append(" desc = ")
                .append(desc).append(" mccmncspn = ").append(getMappedMccMnc());
        return builder.toString();
    }
}
