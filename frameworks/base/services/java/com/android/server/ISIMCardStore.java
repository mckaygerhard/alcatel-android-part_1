
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

public interface ISIMCardStore {
    public String getMappedMccMnc(String mccmnc, String spn);
    public boolean isValidOperatorSIMCard(String mapped);
    public boolean compareOperatorSIMCard(String mappedlhs, String mappedrhs);
}
