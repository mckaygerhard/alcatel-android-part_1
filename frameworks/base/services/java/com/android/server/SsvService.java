
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.HashMap;

import android.content.res.Resources;
import android.telephony.TelephonyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.ISsvService;
import android.os.Message;
import android.os.SsvManager;
import android.os.SystemProperties;
import android.util.TctLog;
import android.content.ContentValues;
import android.content.ContentResolver;

import com.android.internal.telephony.MccTable;

import android.net.Uri;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * A service to maintain the SIM card state and transitions.
 * <p/>
 * this is a very early service, many components are may not ready to be used.
 * just get mccmnc from rild and do some calculating.
 *
 * @hide
 */
public class SsvService extends ISsvService.Stub {
    private static final String TAG = "SsvService";

    private SsvSettings mSettings;
    private String mSettingsFile = "/data/system/ssv.xml";
    private String[] mMccMncList; // valid OP mccmnc list
    private String[] mSpnList; // valid OP spn list
    private static final String MCCMNC_KEY = "persist.sys.mcc.mnc";
    private static final String SPN_KEY = "persist.sys.sim.spn";
    private static final String MCCMNC_LIST_KEY1 = "ro.ssv.mccmncspnlist1"; // from perso
    private static final String MCCMNC_LIST_KEY2 = "ro.ssv.mccmncspnlist2"; // from perso
    private static final String PRE_KEY = "persist.sys.pre.mcccmncspn"; // from reboot
    private static final String IMSI_KEY = "gsm.operator.numeric1";//from rild
    private static final String SPN_STRING = "gsm.operator.numeric2";//from rild
    private static final String SPNALIAS = "persist.sys.special.mcccmncspn";//from rild
    private static final String OPERATOR_SIM = "ro.ssv.operator.choose";
    private int mTransition = SsvManager.TRANSITION_NONE;
    private boolean mIsSimLock = false;
    private boolean mHasPinLock = false;
    private boolean mSwitchEnable = false;
    private String mSimLockMccMnc = null;
    private String mSimLockGid = null;
    private String mSimLockSpn = null;
    private boolean mKeyGuardState = false;
    private String mOperatorSim = "";
    private String mValidateSimInfoString = "";
    private String mMappedCurMccMnc = "";
    private Context mContext = null;
    Timer timer = new Timer();
    private int mTimerCount = 0;
    private final int STARTSSVACTIVITY = 1;
    private IStateTransition mStateTransition = null;

    public static final String OPERATOR_TEF = "TEF";
    public static final String OPERATOR_AMV = "AMV";
    public static final String OPERATOR_TMO = "TMO";
    public static final String OPERATOR_FOC = "FOC";
    public static final String OPERATOR_GSM = "GSM";

    public SsvService(Context context) {
        super();
        mContext = context;
        mSettings = new SsvSettings(mSettingsFile);
        mOperatorSim = SystemProperties.get(OPERATOR_SIM, "TMO");
        getMccMncFromImsi();

        if (mOperatorSim.equals(OPERATOR_TEF)) {
            initTefMccMncList();
        } else if (mOperatorSim.equals(OPERATOR_AMV)) {
            initAMVMccMncList();
        } else if (mOperatorSim.equals(OPERATOR_TMO)) {
            initMccMncList();
        } else if (mOperatorSim.equals(OPERATOR_FOC)) {
            initFOCMccMncList();
        }
        // initTransition();
        mSwitchEnable = Boolean.parseBoolean(mSettings.getValue(
                SsvSettings.KEY_SWITCH_ENABLE, "false"));
        ;
        TctLog.d(TAG, "SsvService created.");
    }

    /**
     * get the MccMnc of the current SIM Card from RIL, and set it to the SystemProperty "persist.sys.mcc.mnc"
     */
    private void getMccMncFromImsi() {
        String imsi = "";
        int mcc = 0;
        String mnc = "";
        int mnclenth = 0;
        String mccmnc = "";
        imsi = SystemProperties.get(IMSI_KEY, "");
        if (imsi.length() > 6) {
            if (imsi.equals("sim_lock")) {
                mccmnc = "sim_lock";
                SystemProperties.set(MCCMNC_KEY, mccmnc);
                mIsSimLock = true;
                TctLog.d(TAG, "is SIM card locked: " + mIsSimLock);
                return;
            }
            mcc = Integer.parseInt(imsi.substring(0, 3));
            mnclenth = MccTable.smallestDigitsMccForMnc(mcc);
            mnc = imsi.substring(3, 3 + mnclenth);
            mccmnc = mcc + "" + mnc;
            TctLog.d(TAG, " getMccMncFromImsi mccmnc = " + mccmnc);
            SystemProperties.set(MCCMNC_KEY, mccmnc);
        } else if (imsi.length() < 7 && imsi.length() > 0) {
            mccmnc = imsi;
            SystemProperties.set(MCCMNC_KEY, imsi);
        } else if (imsi.length() == 0) {
            SystemProperties.set(MCCMNC_KEY, "");
        }

        if (!mIsSimLock && !mccmnc.equals("") && mOperatorSim.equals("TMO")) {
            getSpnAlias(mccmnc);
        }
    }


    /**
     * get the alias of the spn, for example : 46000-SPN-999,the result is 999
     * the method of the role in the new SSV framework is to determined
     *
     * @param mccmnc
     */
    private String getSpnAlias(String mccmnc) {
        String spn = "";
        String spnalias = "";

        spn = SystemProperties.get(SPN_STRING, "");

        mMappedCurMccMnc = getMappedMccMnc(mccmnc, spn);
        spnalias = mMappedCurMccMnc;
        TctLog.d(TAG, "getTheMapped MCCMNC and save in the property spn == " + spn + " getSpnAlias = " + spnalias);
        SystemProperties.set(SPN_KEY, spnalias.substring(3));
        return spnalias;
    }

    /**
     * get the mapped mccmnc for FOC
     *
     * @return
     */
    public String getCurrentFocMccAlias() {
        String mAlias = "";
        String mccmnc = "";
        mccmnc = SystemProperties.get(IMSI_KEY, "");
        mMappedCurMccMnc = getMappedMccMnc(mccmnc, getSimGid());
        mAlias = mMappedCurMccMnc;
        TctLog.d(TAG, "getCurrentFocAlias = " + mAlias);
        return mAlias;
    }

    private void initMccMncList() {
        /**
         * ro.ssv.mccmnclist should look like this:
         * "46000-007,46001-06,46002-005" includes MCCMNC and SPN, connect with
         * a dash
         */

        // because perso can only set 91 for string,so use three perso.
        String mccMncListString1 = SystemProperties.get(MCCMNC_LIST_KEY1, "");
        String mccMncListString2 = SystemProperties.get(MCCMNC_LIST_KEY2, "");
        String mccMncListString = mccMncListString1 + "," + mccMncListString2;
        String[] mccMncList = mccMncListString.split(",");
        TctLog.d(TAG, "main:  mccMncList.length" + mccMncList.length);
        mMccMncList = new String[mccMncList.length];
        mSpnList = new String[mccMncList.length];

        String logMsg = "init mccmnc list:";
        for (int i = 0; i < mMccMncList.length; i++) {
            // just get mccmnc and spn
            TctLog.d(TAG, "details:  mMccMncList.i = " + i + "      " + mccMncList[i]);
            mMccMncList[i] = mccMncList[i].split("-")[0].trim();
            mSpnList[i] = mccMncList[i].split("-")[1].trim();
            logMsg += " " + mMccMncList[i] + "-" + mSpnList[i];
        }
        TctLog.d(TAG, logMsg);
    }

    /**
     * Get the mccmnclist of the Telefonica
     */
    private void initTefMccMncList() {
        // because perso can only set 91 for string,so use three perso.
        String MinmatchLen1 = SystemProperties.get("phone.num.tel.ssv.sign.len1", "");
        String MinmatchLen2 = SystemProperties.get("phone.num.tel.ssv.sign.len2", "");
        String MinmatchLen3 = SystemProperties.get("phone.num.tel.ssv.sign.len3", "");
        String MinmatchLen4 = SystemProperties.get("phone.num.tel.ssv.sign.len4", "");
        String MinmatchLen = MinmatchLen1 + "," + MinmatchLen2 + "," + MinmatchLen3 + "," + MinmatchLen4;
        TctLog.d(TAG, "Minmatchnumbermccmnc ====  " + MinmatchLen);
        String[] Minmatchnumbermccmnc = MinmatchLen.split(",");
        mMccMncList = new String[Minmatchnumbermccmnc.length / 2];
        String logMsg = "init initTefMccMncList:";
        TctLog.d(TAG, logMsg);
        if (Minmatchnumbermccmnc != null && Minmatchnumbermccmnc.length != 0
                && Minmatchnumbermccmnc.length % 2 == 0) {
            int j = 0;
            for (int i = 0; i < Minmatchnumbermccmnc.length; ) {
                mMccMncList[j] = Minmatchnumbermccmnc[i];
                TctLog.d(TAG, "mMccMncList.j = " + j + "      " + mMccMncList[j]);
                i += 2;
                j = i / 2;
            }
        }
    }

    /**
     * Get the mccmnclist of the Telefonica
     */
    private void initAMVMccMncList() {
        // because perso can only set 91 for string,so use three perso.
        String MinmatchLen1 = SystemProperties.get("phone.num.amv.ssv.mccmnclist1", "");
        String MinmatchLen2 = SystemProperties.get("phone.num.amv.ssv.mccmnclist2", "");
        String MinmatchLen = MinmatchLen1 + "," + MinmatchLen2;
        mMccMncList = MinmatchLen.split(",");
        String logMsg = "init initAMVMccMncList:";
        {
            for (int i = 0; i < mMccMncList.length; i++) {
                TctLog.d(TAG, "mMccMncList.i = " + i + "      " + mMccMncList[i]);
            }
        }
    }

    private void initButtonNoState() {
        String prevLangMccMnc = getLangMccMnc();
        String simMccMnc = getSimMccMnc();
        if (simMccMnc.equals(prevLangMccMnc)) {
            setButtonNoNum(mSettings.VALUE_BTN_CLICK_NONE);
        }
    }

    /**
     * Determine the transition between states via the SIM card changing
     */
    public int getTransition() {
        TctLog.d(TAG, "...get transition=" + mTransition);
        return mTransition;
    }

    private void initTransition() {
        TctLog.d(TAG, "...init transition...");
        mTransition = SsvManager.TRANSITION_NONE;
        int currentState = getCurrentState();
        int newState = getNewState();
        mStateTransition = StateTransitionFactory.createStateTransition(getOperator(), currentState);
        mStateTransition.handleNewState(newState, isOperatorSimCardChanged());
        mTransition = mStateTransition.getTransition();
        TctLog.d(TAG, "...transition=" + mTransition);

    }

    /**
     * Get mccmnc of current SIM card
     */
    public String getSimMccMnc() {
        String mccmnc;
        if (mSimLockMccMnc == null) {
            mccmnc = SystemProperties.get(MCCMNC_KEY, "");
        } else {
            mccmnc = mSimLockMccMnc;
        }

        if (mccmnc.equals("sim_lock")) {
            return mccmnc;
        } else {
            if (mOperatorSim.equals("TMO") && mSimLockMccMnc != null) {
                return getSpnAlias(mccmnc);
            } else if (mOperatorSim.equals("FOC") && mSimLockMccMnc != null) {
                return getCurrentFocMccAlias();
            }
        }

        try {
            Integer.parseInt(mccmnc);
        } catch (NumberFormatException e) {
            // not a standard mccmnc string, may be 'sim_absent', 'sim_lock'
            // etc.
            TctLog.d(TAG, "main:  not a standard mccmnc string, return an empty String");
            return "";
        }
        return mccmnc;
    }

    private boolean compareOperatorSIMCard(String mappedlhs, String mappedrhs) {
        return OperatorSIMCardManager.instance().getSIMCardStore().compareOperatorSIMCard(mappedlhs, mappedrhs);
    }

    private String get32BitsSpn() {
        String spn = SystemProperties.get(SPN_STRING, "");
        if ((spn != null) && spn.length() > 32) {
            spn = spn.substring(2);
            TctLog.d(TAG, "main:  getSpnAliasTest spn == " + spn);
        }
        return spn;
    }

    /**
     * Get spn of current SIM card
     */
    public String getSimSpn() {
        String spn;
        spn = SystemProperties.get(SPN_KEY, "");
        try {
            Integer.parseInt(spn);
        } catch (NumberFormatException e) {
            // not a standard mccmnc string, may be 'sim_absent', 'sim_lock'
            // etc.
            TctLog.d(TAG, "not a standard spn string, return an empty String");
            return "";
        }

        return spn;
    }

    /**
     * Get gid of current SIM card
     */
    public String getSimGid() {
        String gid = "ff";
        gid = TelephonyManager.getDefault().getGroupIdLevel1();
        if (gid == null || gid.equals("")) {
            gid = "ff";
        } else if (gid.length() > 2) {
            gid = gid.substring(0, 2);
        }
        return gid;
    }

    /**
     * Get spn of previous SIM card
     */
    public String getPreSimSpn() {
        TctLog.d(TAG, "getPrevMccMnc()");
        return mSettings.getValue(SsvSettings.KEY_PREV_SPN, "");
    }

    /**
     * Set spn of previous SIM card
     */
    public void setPreSimSpn(String spn) {
        TctLog.d(TAG, "setPrevMccMnc()");
        mSettings.setValue(SsvSettings.KEY_PREV_SPN, spn);
    }

    public boolean getIsFirstRun() {
        TctLog.d(TAG, "getIsFirstRun()");
        return Boolean.parseBoolean(mSettings.getValue(SsvSettings.KEY_FIRST_BOOT, "false"));
    }

    public void setIsFirstRun(boolean b) {
        TctLog.d(TAG, "setIsFirstRun()");
        mSettings.setValue(SsvSettings.KEY_FIRST_BOOT, String.valueOf(b));
    }

    /**
     * Get mccmnc of previous (valid) SIM card
     */
    public String getPrevMccMnc() {
        TctLog.d(TAG, "getPrevMccMnc()");
        return mSettings.getValue(SsvSettings.KEY_PREV_MCCMNC, "");
    }

    /**
     * Get mccmnc of every previous (valid) SIM card
     */
    public String getEveryPrevMccMnc() {
        TctLog.d(TAG, "getEveryPrevMccMnc()");
        return mSettings.getValue(SsvSettings.KEY_EVERY_MCCMNC, "");
    }

    /**
     * Get mccmnc for language customization. if SIM card changed from OP1 to
     * OP2, SSV may allow user to change language but not settings/data. so we
     * need a new mccmnc value related to the language.
     */
    public String getLangMccMnc() {
        return mSettings.getValue(SsvSettings.KEY_LANG_MCCMNC, "");
    }

    /**
     * set mccmnc of previous SIM card
     *
     * @param mccmnc
     */
    public void setPrevMccMnc(String mccmnc) {
        TctLog.d(TAG, "setPrevMccMnc(): " + mccmnc);
        mSettings.setValue(SsvSettings.KEY_PREV_MCCMNC, mccmnc);
    }

    /**
     * set mccmnc of every previous SIM card
     *
     * @param mccmnc
     */
    public void setEveryPrevMccMnc(String mccmnc) {
        TctLog.d(TAG, "setEveryPrevMccMnc(): " + mccmnc);
        mSettings.setValue(SsvSettings.KEY_EVERY_MCCMNC, mccmnc);
    }

    /**
     * set mccmnc for language customization
     *
     * @param mccmnc
     */
    public void setLangMccMnc(String mccmnc) {
        mSettings.setValue(SsvSettings.KEY_LANG_MCCMNC, mccmnc);
    }

    public String getButtonNoNum() {
        return mSettings.getValue(SsvSettings.KEY_BUTTON_NO_NUM, SsvSettings.VALUE_BTN_CLICK_NONE);
    }

    public void setButtonNoNum(String btnnonum) {
        mSettings.setValue(SsvSettings.KEY_BUTTON_NO_NUM, btnnonum);
    }

    public void initSsvSettings() {
        mSettings.initSsvSettings();
    }

    /**
     * set current SIM state
     *
     * @param state
     */
    public void setCurrentState(int state) {
        TctLog.d(TAG, "setCurrentState(): " + state);
        mSettings.setValue(SsvSettings.KEY_CURRENT_STATE,
                Integer.toString(state));
    }

    /**
     * determine that if current SIM card belongs to Operator
     */
    public boolean isOpSim() {
        return isMccMncValid(getSimMccMnc());
    }

    /**
     * Checking Ssv is enable or not.
     */
    public boolean isSsvEnable() {
        String ssvStatus = SystemProperties.get("ro.ssv.enabled", "false");
        return "true".equals(ssvStatus);
    }

    /**
     * active reboot and get new state
     */
    public int getPreState() {
        String[] arraySpn = SystemProperties.get(PRE_KEY, "00 00 00")
                .split(" ");
        if (arraySpn.length == 3) {
            String mccMnc = arraySpn[0] + arraySpn[1];
            String spn = arraySpn[2];
            if (isOperatorSIMCardValid(mccMnc)) {
                if (isMainOperator(spn)) {
                    return SsvManager.STATE_OP;
                } else {
                    return SsvManager.STATE_SUB;
                }
            }
        }
        return SsvManager.STATE_DEF;
    }

    /**
     * determine that if apps need to change their UI or not
     */
    public boolean isAppNeedChange(String app) {
        boolean ret = false;
        int appState = Integer.parseInt(mSettings.getValue(
                SsvSettings.KEY_APP_STATE + app,
                Integer.toString(SsvManager.STATE_ORIG)));
        int sysState = this.getPreState();
        TctLog.d(TAG, "isAppNeedChange app string == " + app);
        TctLog.d(TAG, "get the switch enable " + String.valueOf(mSwitchEnable));
        TctLog.d(TAG, "isAppNeedChange,sysState ==  " + sysState + "   appState == " + appState);
        if (mOperatorSim.equals("TEF") || mOperatorSim.equals("AMV")) {
            if (mSwitchEnable) {
                switch (appState) {
                    case SsvManager.STATE_ORIG:
                        ret = true;
                        break;
                    case SsvManager.STATE_DEF:
                        ret = false;
                        break;
                }
            }
        } else if (mOperatorSim.equals("TMO")) {
            if (mSwitchEnable) {
                switch (appState) {
                    case SsvManager.STATE_ORIG:
                        if (sysState != SsvManager.STATE_DEF) {
                            ret = true;
                        }

                    case SsvManager.STATE_DEF:
                        if (sysState == SsvManager.STATE_OP
                                || sysState == SsvManager.STATE_SUB) {
                            ret = true;
                        }
                }
            }
        }
        return ret;
    }

    /**
     * determine that if the app can switch the resource
     */
    public void setSwitchEnable(boolean enable) {
        mSettings.setValue(SsvSettings.KEY_SWITCH_ENABLE,
                String.valueOf(enable));
    }

    /**
     * app configure complete, update app state
     */
    public void appChangeComplete(String app) {
        if (!mSwitchEnable) {
            mSettings.setValue(SsvSettings.KEY_APP_STATE + app,
                    Integer.toString(SsvManager.STATE_DEF));
        } else {
            mSettings.setValue(SsvSettings.KEY_APP_STATE + app,
                    Integer.toString(getCurrentState()));
        }
    }

    /**
     * check if SIM card is locked
     */
    public boolean isSimLock() {
        return mIsSimLock;
    }

    /**
     * locked SIM card is unlocked, update mccmnc and transition
     */
    public void unlock(String mccmnc, String spn) {
        mSimLockMccMnc = mccmnc;
        mSimLockSpn = spn;
        mIsSimLock = false;
        if (mOperatorSim.equals("TMO")) {
            initTransition();
        }
    }

    public void unLock(String mccmnc, String spn, String gid) {
        TctLog.d(TAG, "details:  mccmnc=" + mccmnc + "    spn=" + spn + "    gid=" + gid);
        mSimLockGid = gid;
        unlock(mccmnc, spn);
    }

    /**
     * check if SIM card is locked
     */

    public boolean hasPinLock() {
        return mHasPinLock;
    }

    public int getCurrentState() {
        String state = mSettings.getValue(SsvSettings.KEY_CURRENT_STATE,
                Integer.toString(SsvManager.STATE_ORIG));
        return Integer.parseInt(state);
    }

    public String getOperator() {
        return OperatorSIMCardManager.instance().getOperator();
    }

    public String getMappedMccMnc(String realMccMnc, String spn) {
        return OperatorSIMCardManager.instance().getSIMCardStore().getMappedMccMnc(realMccMnc, spn);
    }

    public boolean isOperatorSimCardChanged() {
        return !compareOperatorSIMCard(mMappedCurMccMnc, getPrevMccMnc());
    }

    private int getNewState() {
        String mccMnc = getSimMccMnc();
        String spn = getSimSpn();

        if (isOperatorSIMCardValid(mccMnc)) {
            if (mccMnc.equals("sim_lock")) {
                return SsvManager.STATE_DEF;
            }
            if (isMainOperator(spn)) {
                return SsvManager.STATE_OP;
            } else {
                return SsvManager.STATE_SUB;
            }
        }
        return SsvManager.STATE_DEF;
    }

    private boolean isMainOperator(String spn) {
        boolean ret = true;
        if (spn == null || spn.equals("")) {
            return ret;
        }
        for (int i = 0; i < mSpnList.length; i++) {
            if (mSpnList[i].contains(spn)) {
                ret = true;
            }
        }
        return ret;
    }

    public boolean isOperatorSIMCardValid(String mapped) {
        return OperatorSIMCardManager.instance().getSIMCardStore().isValidOperatorSIMCard(mapped);
    }

    private boolean isMccMncValid(String mccMnc) {
        boolean ret = false;
        if (mccMnc == null || mccMnc.equals("") || mMccMncList == null) {
            TctLog.d(TAG, "main:  returned because mccmnc or mMccMncList error.");
            return false;
        }

        if (mccMnc.equals("sim_lock")) {
            TctLog.d(TAG, "main:  returned because mccMnc = sim_lock");
            return true;
        }

        if ("GMS".equals(mOperatorSim)) {
            return true;
        }

        String spnalias = getSimSpn();
        TctLog.d(TAG, "details: isMccMncValid() spnalias=" + spnalias + " mccMnc = " + mccMnc);

        for (int i = 0; i < mMccMncList.length; i++) {
            if (mccMnc.equals(mMccMncList[i])) {
                if (mOperatorSim.equals("TMO") && spnalias.equals("")) {
                    TctLog.d(TAG, "main:  returned the value mccMnc=false");
                    return false;
                }
                ret = true;
                break;
            }
        }
        TctLog.d(TAG, "main:  isMccMncValid() return mccMnc=" + ret);
        return ret;

    }

//    private boolean isMccMncValid(String mccmnc, String spn, String gid) {
//        TctLog.d(TAG, "details:  isMccMncValid()-----> mccmnc=" + mccmnc + "    spn=" + spn + "    gid=" + gid);
//        String s = mContext.getResources().getString(com.android.internal.R.string.feature_foc_match_method);
//        String tmp_mccmnc = mccmnc + ",";
//        String tmp_spn = "," + spn + ",";
//        String tmp_gid = "," + gid + ",";
//        try {
//            TctLog.d(TAG, "details:    the SDM value is:" + s);
//            Integer.parseInt(s);
//        } catch (NumberFormatException e) {
//            TctLog.d(TAG, "main:  not a standard match method param string, return an empty String, chang the default value to:1");
//            s = "1";
//        }
//        boolean result = false;
//        switch (Integer.parseInt(s)) {
//            case 1://"1": mccmnc+gid
//                if (mccmnc == null || "".equalsIgnoreCase(mccmnc) || gid == null || "".equalsIgnoreCase(gid)) {
//                    result = false;TctLog.d(TAG, "=" + result);
//                } else {
//                    result = isMccMncAvailability(tmp_mccmnc, tmp_gid);
//                }
//                break;
//            case 2://"2": mccmnc+spn;
//                if (mccmnc == null || "".equalsIgnoreCase(mccmnc) || spn == null || "".equalsIgnoreCase(spn)) {
//                    result = false;TctLog.d(TAG, "=" + result);
//                } else {
//                    result = isMccMncAvailability(tmp_mccmnc, tmp_spn);
//                }
//                break;
//            case 3://"3": mccmnc+spn+gid;
//                if (mccmnc == null || "".equalsIgnoreCase(mccmnc) || spn == null || "".equalsIgnoreCase(spn) || gid == null || "".equalsIgnoreCase(gid)) {
//                    result = false;TctLog.d(TAG, "=" + result);
//                } else {
//                    result = isMccMncAvailability(tmp_mccmnc, tmp_spn, tmp_gid);
//                }
//                break;
//            default:
//                TctLog.d(TAG, "main:  isMccMncValid(3params)-->The SDM value error :" + result);
//                result = false;
//        }
//        TctLog.d(TAG, "main:  isMccMncValid()-----> result=" + result);
//        return result;
//    }
//
//    private boolean isMccMncAvailability(String... args) {
//        TctLog.d(TAG, "details:  isMccMncAvailability()----->    arg.length=" + args.length + "    arg[0]=" + args[0] + "    arg[1]=" + args[1] + "    args[2]=" + (args.length > 2 ? args[2] : "null") + "    mMccMncList.length=" + (mMccMncList == null ? "null" : mMccMncList.length));
//        if (args.length < 2 || args.length > 3 || mMccMncList == null || mMccMncList.length <= 0) {
//            TctLog.d(TAG, "main:  isMccMncAvailability()----->need to return, args.length=" + args.length + "     |mMccMncList=" + mMccMncList);
//            return false;
//        }
//        boolean result = false;
//        for (String s: mMccMncList) {
//            if (s == null || "".equalsIgnoreCase(s)) {
//                continue;
//            }
//            if (args.length == 2) {
//                result = s.contains(args[0]) && s.contains(args[1]);
//            } else {
//                result = s.contains(args[0]) && s.contains(args[1]) && s.contains(args[2]);
//            }
//            if (result) {
//                mValidateSimInfoString = s;
//                break;
//            }
//        }
//        TctLog.d(TAG, "main:  isMccMncAvailability()----->" + result + "     mValidateSimInfoString=" + mValidateSimInfoString);
//        return result;
//    }

    public boolean saveFocSpnAlias() {
        if ("".equalsIgnoreCase(mValidateSimInfoString) || mValidateSimInfoString.split(",").length != 4) {
            TctLog.d(TAG, "main:  saveFocSpnAlias()----->  mValidateSimInfoString=" + mValidateSimInfoString + "    return false");
            return false;
        }
        String mAlias = mValidateSimInfoString.split(",")[3].trim();
        if (!mAlias.equalsIgnoreCase(getSimSpn())) {
            SystemProperties.set(SPN_KEY, mAlias);
            TctLog.d(TAG, "main:  saveFocSpnAlias()----->  saved mAlias=" + mAlias + "     return true");
        }
        TctLog.d(TAG, "detail:  saveFocSpnAlias()----->  excute finish");
        return true;
    }

    /**
     * Get the mccmnclist of the FOC
     */
    private void initFOCMccMncList() {
        // because perso can only set 91 for string,so use three perso.
//        String MinmatchLen1 = SystemProperties.get( "phone.num.foc.ssv.mccmnclist", "");
        //String MinmatchLen1 = mContext.getResources().getString(com.android.internal.R.string.def_mccmncspngid_list_ssv);
        String MinmatchLen1 = "";
        TctLog.d(TAG, "details:  the perso def_mccmncspngid_list_ssv config is--->" + MinmatchLen1);
        mMccMncList = MinmatchLen1.split(";");
        for (int i = 0; i < mMccMncList.length; i++) {
            TctLog.d(TAG, "details:  mMccMncList[ = " + i + "]=" + mMccMncList[i]);
        }
    }

    boolean mValidMccmnc = false;

    /**
     * Determine whether the SIM card is changed and whether you need to reload the resource
     *
     * @return
     */
    public boolean isMccMncChanged() {
        /**
         * this method has two point:
         * 1.Determine whether the current SIM Card need SSV
         * 2.if needed:compare the SIM Card info, if the SIM Card is changed, reboot Device and active  the SSV
         */
        String prevLangMccMnc = getLangMccMnc();
        String simMccMnc = getSimMccMnc();
        String everyMccMnc = getEveryPrevMccMnc();
        String buttonnostate = "";
        mValidMccmnc = false;
        /**
         * step 1: determine whether the SIM Card need SSV
         * the SIM Card which need SSV is defined in the simcardinfo.xml
         */
        if (OPERATOR_FOC.equalsIgnoreCase(mOperatorSim) || OPERATOR_TMO.equalsIgnoreCase(mOperatorSim) || OPERATOR_AMV.equalsIgnoreCase(mOperatorSim) || OPERATOR_TEF.equalsIgnoreCase(mOperatorSim)) {
            mValidMccmnc = isOperatorSIMCardValid(simMccMnc);
            //mValidMccmnc = isMccMncValid( simMccMnc, get32BitsSpn()/*mSimLockSpn*//*getSimSpn()*/, getSimGid());
        } else {
            mValidMccmnc = isMccMncValid(simMccMnc);
        }
        TctLog.d(TAG, "details -- isMccMncChanged(): mValidMccmnc=" + mValidMccmnc + " mOperatorSim=" + mOperatorSim + " prevLangMccMnc == " + prevLangMccMnc + " simMccMnc == " + simMccMnc);
        if (!mValidMccmnc) {
            return false;
        }

        boolean isneedchange = false;
        TctLog.d(TAG, "! The current SIMCard need SSV !");
        buttonnostate = mSettings.getValue(SsvSettings.KEY_BUTTON_NO_NUM, SsvSettings.VALUE_BTN_CLICK_NONE);
        /**
         * step 2: compares SIM Card information to determine if the SIM Card has changed
         * For different Operators have different judgment logic
         */
        if (mOperatorSim.equals(OPERATOR_TMO) || mOperatorSim.equals(OPERATOR_TEF) || mOperatorSim.equals(OPERATOR_AMV)) {
            /**
             * step 2-1: for operators TMO,TEF,AMV,just need to compare old mccmnc and new mccmnc
             */
            isneedchange = !prevLangMccMnc.equals(simMccMnc);
        } else if (mOperatorSim.equals(OPERATOR_GSM)) {
            /**
             * step 2-2: for GSM operator, in this Logic
             */
            String mMccMncListStrings = "";//mContext.getResources().getString(com.android.internal.R.string.def_ssv_mccmnc_apks1);
            //mMccMncListStrings += mContext.getResources().getString(com.android.internal.R.string.def_ssv_mccmnc_apks2);
            //mMccMncListStrings += mContext.getResources().getString(com.android.internal.R.string.def_ssv_mccmnc_apks3);
            String mMccMncFiltList = "";//mContext.getResources().getString(com.android.internal.R.string.def_ssv_mccmnc_filtrate_apks);
            TctLog.d(TAG, "details -- isMccMncChanged() step 2-2: mMccMncListStrings=" + mMccMncListStrings + " simMccMnc=" + simMccMnc + " prevLangMccMnc=" + prevLangMccMnc);
            isneedchange = !prevLangMccMnc.equals(simMccMnc);
            if (mMccMncListStrings == null || ("".equals(mMccMncListStrings) && "".equals(mMccMncFiltList)) || simMccMnc == null) {
                TctLog.d(TAG, "details -- isMccMncChanged() step 2-2: error ");
                isneedchange = false;
            }
            if (isneedchange && !"".equals(prevLangMccMnc)) {
                String mStringKey = findStringKey(mMccMncListStrings, prevLangMccMnc);
                if (mStringKey.length() > 3 || (!mStringKey.substring(0, 3).equals(simMccMnc.substring(0, 3)))) {
                    isneedchange = true;
                } else {
                    String[] mSplitArr = mMccMncListStrings.split(";");
                    for (int i = 0; i < mSplitArr.length; i++) {
                        String mm = mSplitArr[i].split(":")[0];
                        TctLog.d(TAG, "details -- isMccMncChanged() step 2-2: compare object mSplitArr=" + mm + " with simMccMnc=" + simMccMnc + "   mStringKey=" + mStringKey);
                        if (mm.equals(mStringKey)) {
                            isneedchange = false;
                            break;
                        } else {
                            isneedchange = true;
                        }
                    }
                }
            } else if (isneedchange && "".equals(prevLangMccMnc) && simMccMnc != null && !"".equals(simMccMnc)) {
                String[] mSplitArr = mMccMncListStrings.split(";");
                HashMap<String, String> maps = new HashMap<String, String>();
                for (String k : mSplitArr) {
                    String[] one = k.split(":");
                    if (one.length == 2) {
                        maps.put(one[0], one[1]);
                    }
                }
                if (maps.containsKey(simMccMnc + ":") || maps.containsKey(simMccMnc.substring(0, 3) + ":")) {
                    isneedchange = true;
                }
            }
        } else if (OPERATOR_FOC.equalsIgnoreCase(mOperatorSim)) {
            /**
             * step 2-3: for FOC operator
             * 1.compare the old mccmnc to the new mccmnc
             * 2.if the old mccmnc == "", the SIMCard is changed too
             */
            boolean b1 = !prevLangMccMnc.equals(simMccMnc);
            boolean b2 = "".equals(prevLangMccMnc);
            isneedchange = b1 || b2;
            TctLog.d(TAG, "details -- isMccMncChanged() step 2-3: b1=" + b1 + " b2=" + b2 + " isneedchange=" + isneedchange);
            saveFocSpnAlias();
            TctLog.d(TAG, "details -- isMccMncChanged() step 2-3: prevLangMccMnc=" + prevLangMccMnc + " getCurrentFocMccAlias()=" + simMccMnc + " isneedchange=" + isneedchange);
        }
        if (isneedchange) {
            if (!everyMccMnc.equals(simMccMnc)) {
                TctLog.d(TAG, "return true");
                return true;
            }
        }
        TctLog.d(TAG, "main:  return isMccMncChanged =" + isneedchange);
        return isneedchange;
    }

    private String findStringKey(String str, String perLang) {
        if ("".equals(perLang))
            return str;
        if (str.contains(perLang + ":"))
            return perLang;
        return perLang.substring(0, 3);
    }

    public void setSsvKeyGuardState(boolean state) {
        TctLog.d(TAG, "details:  setSsvKeyGuardState ==  " + state);
        mKeyGuardState = state;
    }

    public boolean getSsvKeyGuardState() {
        return mKeyGuardState;
    }

    public void updateMinMatchField() {
        if (null != mContext) {
            ContentResolver contentResolver = mContext.getContentResolver();
            Uri uri = Uri.parse("content://com.android.contacts/phone_lookup_minmatch");
            contentResolver.update(uri, new ContentValues(), null, null);
            TctLog.d("SsvService", " minmatch changed successfully ");
        }
    }

    //[FEATURE]-ADD-BEGIN by TSNJ.xiaowei.yang-nb,PR-1067379,09/09/2015
    public void updateLogo() {
        String mccmnc = SystemProperties.get(MCCMNC_KEY, "");
        String path = "/system/custpack/" + mccmnc + ".raw";
        TctLog.d(TAG, "update Logo path=" + path);
        RewriteSplash(path);
    }

    public void RewriteSplash(String path) {

        InputStream mIn = null;
        OutputStream mOut = null;
        LocalSocket mSocket = null;

        mSocket = new LocalSocket();
        try {
            LocalSocketAddress address = new LocalSocketAddress(
                    "tctd", LocalSocketAddress.Namespace.RESERVED);
            mSocket.connect(address);
            mIn = mSocket.getInputStream();
            mOut = mSocket.getOutputStream();

            //send command
            StringBuilder rawBuilder = new StringBuilder("0 privilege rewrite_splash ");
            rawBuilder.append(path);
            rawBuilder.append('\0');

            try {
                mOut.write(rawBuilder.toString().getBytes(StandardCharsets.UTF_8));

                //read result
                try {
                    int count = 0;
                    byte[] buffer = new byte[4096];
                    count = mIn.read(buffer, 0, 4096);
                    if (count > 0) {
                        buffer[count] = 0;
                        String rawEvent = new String(buffer, 0, count, StandardCharsets.UTF_8);
                        if (rawEvent.startsWith("200 0")) {
                            TctLog.d(TAG, "Succeeded");
                        } else {
                            TctLog.d(TAG, "Failed, please retry");
                        }
                    }
                } catch (IOException ex) {
                    TctLog.d(TAG, "read fail");
                }
            } catch (IOException ex) {
                TctLog.d(TAG, "send command fail");
            }
        } catch (IOException ex) {
            TctLog.d(TAG, "connect fail");
        } finally {
            try {
                if (mSocket != null) mSocket.close();
            } catch (IOException ex) {
            }
            try {
                if (mIn != null) mIn.close();
            } catch (IOException ex) {
            }
            try {
                if (mOut != null) mOut.close();
            } catch (IOException ex) {
            }
        }
    }

    //[FEATURE]-ADD-END by TSNJ.xiaowei.yang-nb,PR-1067379,09/09/2015
    private class SsvSettings {
        private Properties mProps;
        private File mFile;
        public static final String KEY_CURRENT_STATE = "current_state";
        public static final String KEY_PREV_MCCMNC = "prev_mccmnc";
        public static final String KEY_LANG_MCCMNC = "lang_mccmnc";
        public static final String KEY_APP_STATE = "app_state_";
        public static final String KEY_EVERY_MCCMNC = "every_mccmnc";
        public static final String KEY_PREV_SPN = "pre_spn";
        public static final String KEY_SWITCH_ENABLE = "switch_enable";
        public static final String KEY_BUTTON_NO_NUM = "button_no_num";
        public static final String VALUE_BTN_CLICK_NONE = "nobuttonclick_none";
        public static final String VALUE_BTN_CLICK_FRIST = "nobuttonclick_frist";
        public static final String VALUE_BTN_CLICK_SECOND = "nobuttonclick_second";
        public static final String KEY_FIRST_BOOT = "first_boot";

        public SsvSettings(String xmlPath) {
            mProps = new Properties();
            mFile = new File(xmlPath);
            if (mFile.exists()) {
                loadSettings();
            } else {
                try {
                    mFile.createNewFile();
                } catch (Exception e) {

                }

            }
        }

        public void initSsvSettings() {
            if (mFile.exists()) {
                mFile.delete();
            }
        }

        public String getValue(String key, String defValue) {
            return mProps.getProperty(key, defValue);
        }

        public void setValue(String key, String value) {
            if (key == null || value == null) {
                return;
            }
            mProps.put(key, value);
            saveSettings();
        }

        private void loadSettings() {
            mProps.clear();
            try {
                FileInputStream in = new FileInputStream(mFile);
                mProps.loadFromXML(in);
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void saveSettings() {
            try {
                FileOutputStream out = new FileOutputStream(mFile);
                mProps.storeToXML(out, null);
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
