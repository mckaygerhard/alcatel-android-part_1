
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

import android.os.SystemProperties;

class SsvUtil {
    public static final String TAG = "Ssv";
    public static final String OPERATOR_TMO = "TMO"; //Tmobile
    public static final String OPERATOR_TEF = "TEF"; //Telefonica
    public static final String OPERATOR_AMV = "AMV"; //America Movil(Telecel&Claro)
    public static final String OPERATOR_FOC = "FOC";
    public static final boolean DEBUG_SSV = SystemProperties.getBoolean("persist.ssv.debug",false) || SystemProperties.get("ro.build.type", "").equals("eng");
    public static boolean isValidMccMnc(String noamalMccMnc) {
        if(noamalMccMnc == null || noamalMccMnc.length() < 3 || noamalMccMnc.length() > 6) return false;
        if(noamalMccMnc.contains("*") || noamalMccMnc.contains("-") || noamalMccMnc.contains(" ")) return false;
        final Integer val = Integer.valueOf(noamalMccMnc);
        if(val != null)
            return true;
        return false;
    }
}