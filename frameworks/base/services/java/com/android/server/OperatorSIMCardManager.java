
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

import android.os.SystemProperties;
import android.content.Context;

public class OperatorSIMCardManager {
    //static enum Operator { TMO, TEF, AMV, CLARO, OPTR_NUMS };
    private static final String OPERATOR_KEY = "ro.ssv.operator.choose";

    private static OperatorSIMCardManager sManager = null;
    private boolean mSpnMapped = false;
    private ISIMCardStore store = null;
    private String operator = null;

    public static synchronized OperatorSIMCardManager instance() {
        if(sManager == null) {
            sManager = new OperatorSIMCardManager();
        }
        return sManager;
    }

    private OperatorSIMCardManager() {
        operator = SystemProperties.get(OPERATOR_KEY, "");
        mSpnMapped = operator.equals(SsvUtil.OPERATOR_TMO);
        if(mSpnMapped) {
            store = new TmobileSIMCardStore();
        } else if(operator.equals(SsvUtil.OPERATOR_TEF) || operator.equals(SsvUtil.OPERATOR_AMV)){
            store = new CommonSIMCardStore();
        } else if(operator.equals(SsvUtil.OPERATOR_FOC)){
            store = new FocSIMCardStore();
        } else {
            store = new DefaultSIMCardStore();
        }
    }

    public boolean isSpnMapped() {
        return mSpnMapped;
    }

    public ISIMCardStore getSIMCardStore() {
        return store;
    }

    public String getOperator() {
        return operator;
    }
}
