
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

public class StateTransitionFactory {
    public static IStateTransition createStateTransition(String operator, int origState) {
        if(operator.equals(SsvUtil.OPERATOR_TEF) || operator.equals(SsvUtil.OPERATOR_AMV))
            return new CommonStateTransition(origState);
        else if(operator.equals(SsvUtil.OPERATOR_TMO))
            return new TmobileStateTransition(origState);
        else
            return new DefaultStateTransition(origState);
    }
}
