
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

import android.os.SsvManager;

public class DefaultStateTransition implements IStateTransition {
    public DefaultStateTransition(int state) {
    }

    public void handleNewState(int newState, boolean simcardChanged) {
    }

    public int getTransition() {
        return SsvManager.TRANSITION_NONE;
    }
}