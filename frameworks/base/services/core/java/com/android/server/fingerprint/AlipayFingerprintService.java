/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.server.fingerprint;

import android.content.Context;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

import org.ifaa.android.manager.IAlipayFingerprintService;
/**
 * Created by sunchunzhi on 11/4/16.
 */
public class AlipayFingerprintService extends IAlipayFingerprintService.Stub{
    private Context mContext;
    private static String TAG = "AlipayFingerprintService";
    private static final boolean debug = true;

    /**
     * @hide
     */
    public AlipayFingerprintService(Context context) {
        mContext = context;
    }

    public byte[] processCmd(byte[] param) throws RemoteException {
        if (debug)
            Log.d(TAG, " paramlength:" + param.length);
        Parcel paramParcel = Parcel.obtain();
                Parcel replyParcel = Parcel.obtain();
        try {
            IBinder binder = ServiceManager.getService("tcl.alipay");
            if (binder == null) {
                Log.e(TAG, "getService failed");
            }
            paramParcel.writeByteArray(param);
            binder.transact(3, paramParcel, replyParcel, 0);
            int reint = replyParcel.readInt();
            if (reint == 0) {
                int relen = replyParcel.readInt();
                byte[] val = new byte[relen];
                if (debug)
                    Log.d(TAG, "invokeTA:relen:" + relen + " length:"
                            + val.length);
                replyParcel.readByteArray(val);
                return val;
            } else {
                Log.e(TAG, "replyParcel failed code:" + reint);
                return null;
            }
        } catch (Exception e) {
            Log.e(TAG, "invokeTACmdfailed");
            e.printStackTrace();
            return null;
        } finally {
            paramParcel.recycle();
            replyParcel.recycle();
        }
    }
}
