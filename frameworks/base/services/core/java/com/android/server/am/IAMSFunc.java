/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.server.am;

import android.content.Intent;
import android.os.Bundle;

import java.util.List;
import java.util.Set;

public interface IAMSFunc {
    int processReceiverComponents(Set<Integer> boxesToReceive, List receivers, String callerPackage, int userId);
    int processRegisterReceivers(List<BroadcastFilter> registeredReceivers , Set<Integer> boxesToReceive,
                                 Intent intent, String resolvedType, int userId, int alternativeBoxId, int callingUid);
    void queueNOrderedRegisteredBroadcastForClone(List<BroadcastFilter> registeredReceivers, BroadcastRecord temp, Set<Integer> boxesToReceive,
                                Intent intent, boolean replacePending);
    void queueFinalBroadcastForClone(List receivers , BroadcastRecord temp, Set<Integer> boxesToReceive, Intent intent, int userId, boolean replacePending);
    ContentProviderRecord getCloneCpr(String name, Bundle b);
    void checkStartActivityLockedForBox(ProcessRecord caller, ActivityRecord r);

    int processSpecialIntent(Intent intent,int callingUid, int userId);
}
