/*
 * Copyright (C) 2007-2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server.storage;

import com.android.server.EventLogTags;
import com.android.server.SystemService;
import com.android.server.pm.InstructionSets;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Environment;
import android.os.FileObserver;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.StatFs;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.storage.StorageManager;
import android.provider.Settings;
import android.text.format.Formatter;
import android.util.EventLog;
import android.util.Slog;
import android.util.TimeUtils;

import java.io.File;
import java.io.FileDescriptor;
import java.io.PrintWriter;

import dalvik.system.VMRuntime;

//[FEATURE]-Add-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PackageStats;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View ;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Iterator;
import android.content.ActivityNotFoundException;
//[FEATURE]-Add-END by zhengyu.hu for task-2694357 Low storage mechanism

/**
 * This class implements a service to monitor the amount of disk
 * storage space on the device.  If the free storage on device is less
 * than a tunable threshold value (a secure settings parameter;
 * default 10%) a low memory notification is displayed to alert the
 * user. If the user clicks on the low memory notification the
 * Application Manager application gets launched to let the user free
 * storage space.
 *
 * Event log events: A low memory event with the free storage on
 * device in bytes is logged to the event log when the device goes low
 * on storage space.  The amount of free storage on the device is
 * periodically logged to the event log. The log interval is a secure
 * settings parameter with a default value of 12 hours.  When the free
 * storage differential goes below a threshold (again a secure
 * settings parameter with a default value of 2MB), the free memory is
 * logged to the event log.
 */
public class DeviceStorageMonitorService extends SystemService {
    static final String TAG = "DeviceStorageMonitorService";

    // TODO: extend to watch and manage caches on all private volumes

    static final boolean DEBUG = true;
    static final boolean localLOGV = true;

    static final int DEVICE_MEMORY_WHAT = 1;
    private static final int MONITOR_INTERVAL = 1; //in minutes
    private static final int LOW_MEMORY_NOTIFICATION_ID = 1;

    private static final int DEFAULT_FREE_STORAGE_LOG_INTERVAL_IN_MINUTES = 12*60; //in minutes
    private static final long DEFAULT_DISK_FREE_CHANGE_REPORTING_THRESHOLD = 2 * 1024 * 1024; // 2MB
    private static final long DEFAULT_CHECK_INTERVAL = MONITOR_INTERVAL*60*1000;

    private long mFreeMem;  // on /data
    private long mFreeMemAfterLastCacheClear;  // on /data
    private long mLastReportedFreeMem;
    private long mLastReportedFreeMemTime;
    boolean mLowMemFlag=false;
    private boolean mMemFullFlag=false;
    private final boolean mIsBootImageOnDisk;
    private final ContentResolver mResolver;
    private final long mTotalMemory;  // on /data
    private final StatFs mDataFileStats;
    private final StatFs mSystemFileStats;
    private final StatFs mCacheFileStats;

    private static final File DATA_PATH = Environment.getDataDirectory();
    private static final File SYSTEM_PATH = Environment.getRootDirectory();
    private static final File CACHE_PATH = Environment.getDownloadCacheDirectory();

    private long mThreadStartTime = -1;
    boolean mClearSucceeded = false;
    boolean mClearingCache;
    private final Intent mStorageLowIntent;
    private final Intent mStorageOkIntent;
    private final Intent mStorageFullIntent;
    private final Intent mStorageNotFullIntent;
    private CachePackageDataObserver mClearCacheObserver;
    private CacheFileDeletedObserver mCacheFileDeletedObserver;
    private static final int _TRUE = 1;
    private static final int _FALSE = 0;
    // This is the raw threshold that has been set at which we consider
    // storage to be low.
    long mMemLowThreshold;
    // This is the threshold at which we start trying to flush caches
    // to get below the low threshold limit.  It is less than the low
    // threshold; we will allow storage to get a bit beyond the limit
    // before flushing and checking if we are actually low.
    private long mMemCacheStartTrimThreshold;
    // This is the threshold that we try to get to when deleting cache
    // files.  This is greater than the low threshold so that we will flush
    // more files than absolutely needed, to reduce the frequency that
    // flushing takes place.
    private long mMemCacheTrimToThreshold;
    private long mMemFullThreshold;

    /**
     * This string is used for ServiceManager access to this class.
     */
    static final String SERVICE = "devicestoragemonitor";

    /**
    * Handler that checks the amount of disk space on the device and sends a
    * notification if the device runs low on disk space
    */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            //[FEATURE]-Add-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
            if (isEnableLowMemoryPolicy(mContext)) {
                handleLowMemory(mContext,msg);
            }
            //[FEATURE]-Add-END by zhengyu.hu for task-2694357

            //don't handle an invalid message
            if (msg.what != DEVICE_MEMORY_WHAT) {
                Slog.e(TAG, "Will not process invalid message");
                return;
            }
            checkMemory(msg.arg1 == _TRUE);
        }
    };

    private class CachePackageDataObserver extends IPackageDataObserver.Stub {
        public void onRemoveCompleted(String packageName, boolean succeeded) {
            mClearSucceeded = succeeded;
            mClearingCache = false;
            if(localLOGV) Slog.i(TAG, " Clear succeeded:"+mClearSucceeded
                    +", mClearingCache:"+mClearingCache+" Forcing memory check");
            postCheckMemoryMsg(false, 0);
        }
    }

    private void restatDataDir() {
        try {
            mDataFileStats.restat(DATA_PATH.getAbsolutePath());
             //[FEATURE]-Add-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
            if (isEnableLowMemoryPolicy(mContext)) {
                mFreeMem = (long) mDataFileStats.getAvailableBlocks() *
                      mDataFileStats.getBlockSize()-(long)(TEST);
            }else{
                mFreeMem = (long) mDataFileStats.getAvailableBlocks() *
                      mDataFileStats.getBlockSize();
            }
            //[FEATURE]-Add-END by zhengyu.hu for task-2694357
        } catch (IllegalArgumentException e) {
            // use the old value of mFreeMem
        }
        // Allow freemem to be overridden by debug.freemem for testing
        String debugFreeMem = SystemProperties.get("debug.freemem");
        if (!"".equals(debugFreeMem)) {
            mFreeMem = Long.parseLong(debugFreeMem);
        }
        // Read the log interval from secure settings
        long freeMemLogInterval = Settings.Global.getLong(mResolver,
                Settings.Global.SYS_FREE_STORAGE_LOG_INTERVAL,
                DEFAULT_FREE_STORAGE_LOG_INTERVAL_IN_MINUTES)*60*1000;
        //log the amount of free memory in event log
        long currTime = SystemClock.elapsedRealtime();
        if((mLastReportedFreeMemTime == 0) ||
           (currTime-mLastReportedFreeMemTime) >= freeMemLogInterval) {
            mLastReportedFreeMemTime = currTime;
            long mFreeSystem = -1, mFreeCache = -1;
            try {
                mSystemFileStats.restat(SYSTEM_PATH.getAbsolutePath());
                mFreeSystem = (long) mSystemFileStats.getAvailableBlocks() *
                    mSystemFileStats.getBlockSize();
            } catch (IllegalArgumentException e) {
                // ignore; report -1
            }
            try {
                mCacheFileStats.restat(CACHE_PATH.getAbsolutePath());
                mFreeCache = (long) mCacheFileStats.getAvailableBlocks() *
                    mCacheFileStats.getBlockSize();
            } catch (IllegalArgumentException e) {
                // ignore; report -1
            }
            EventLog.writeEvent(EventLogTags.FREE_STORAGE_LEFT,
                                mFreeMem, mFreeSystem, mFreeCache);
        }
        // Read the reporting threshold from secure settings
        long threshold = Settings.Global.getLong(mResolver,
                Settings.Global.DISK_FREE_CHANGE_REPORTING_THRESHOLD,
                DEFAULT_DISK_FREE_CHANGE_REPORTING_THRESHOLD);
        // If mFree changed significantly log the new value
        long delta = mFreeMem - mLastReportedFreeMem;
        if (delta > threshold || delta < -threshold) {
            mLastReportedFreeMem = mFreeMem;
            EventLog.writeEvent(EventLogTags.FREE_STORAGE_CHANGED, mFreeMem);
        }
    }

    private void clearCache() {
        if (mClearCacheObserver == null) {
            // Lazy instantiation
            mClearCacheObserver = new CachePackageDataObserver();
        }
        mClearingCache = true;
        try {
            if (localLOGV) Slog.i(TAG, "Clearing cache");
            IPackageManager.Stub.asInterface(ServiceManager.getService("package")).
                    freeStorageAndNotify(null, mMemCacheTrimToThreshold, mClearCacheObserver);
        } catch (RemoteException e) {
            Slog.w(TAG, "Failed to get handle for PackageManger Exception: "+e);
            mClearingCache = false;
            mClearSucceeded = false;
        }
    }

    void checkMemory(boolean checkCache) {
        //if the thread that was started to clear cache is still running do nothing till its
        //finished clearing cache. Ideally this flag could be modified by clearCache
        // and should be accessed via a lock but even if it does this test will fail now and
        //hopefully the next time this flag will be set to the correct value.
        if(mClearingCache) {
            if(localLOGV) Slog.i(TAG, "Thread already running just skip");
            //make sure the thread is not hung for too long
            long diffTime = System.currentTimeMillis() - mThreadStartTime;
            if(diffTime > (10*60*1000)) {
                Slog.w(TAG, "Thread that clears cache file seems to run for ever");
            }
        } else {
            restatDataDir();
            if (localLOGV)  Slog.v(TAG, "freeMemory="+mFreeMem + ", mMemLowThreshold: " + mMemLowThreshold);

            //post intent to NotificationManager to display icon if necessary
            if (mFreeMem < mMemLowThreshold) {
                //[FEATURE]-Add-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
                if (isEnableLowMemoryPolicy(mContext)) {
                    checkEmail();
                }
                //[FEATURE]-Add-END by zhengyu.hu for task-2694357

                if (checkCache) {

                    //[FEATURE]-Add-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
                    if (isEnableLowMemoryPolicy(mContext)) {
                        mThreadStartTime = System.currentTimeMillis();
                        mClearSucceeded = false;
                        clearCache();
                    }
                   //[FEATURE]-Add-END by zhengyu.hu for task-2694357

                    // We are allowed to clear cache files at this point to
                    // try to get down below the limit, because this is not
                    // the initial call after a cache clear has been attempted.
                    // In this case we will try a cache clear if our free
                    // space has gone below the cache clear limit.
                    else if (mFreeMem < mMemCacheStartTrimThreshold) {
                        // We only clear the cache if the free storage has changed
                        // a significant amount since the last time.
                        if ((mFreeMemAfterLastCacheClear-mFreeMem)
                                >= ((mMemLowThreshold-mMemCacheStartTrimThreshold)/4)) {
                            // See if clearing cache helps
                            // Note that clearing cache is asynchronous and so we do a
                            // memory check again once the cache has been cleared.
                            mThreadStartTime = System.currentTimeMillis();
                            mClearSucceeded = false;
                            clearCache();
                        }
                    }
                } else {
                    // This is a call from after clearing the cache.  Note
                    // the amount of free storage at this point.
                    mFreeMemAfterLastCacheClear = mFreeMem;
                    if (!mLowMemFlag) {
                        // We tried to clear the cache, but that didn't get us
                        // below the low storage limit.  Tell the user.
                        Slog.i(TAG, "Running low on memory. Sending notification");
                        sendNotification();
                        mLowMemFlag = true;
                    } else {
                        if (localLOGV) Slog.v(TAG, "Running low on memory " +
                                "notification already sent. do nothing");
                    }
                }
            } else {
                mFreeMemAfterLastCacheClear = mFreeMem;
                if (mLowMemFlag) {
                    //[FEATURE]-Add-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
                    if (isEnableLowMemoryPolicy(mContext)) {
                        mCheckAppSize = true;
                        mGetSize = false;
                    }
                    //[FEATURE]-Add-END by zhengyu.hu for task-2694357

                    Slog.i(TAG, "Memory available. Cancelling notification");
                    cancelNotification();
                    mLowMemFlag = false;
                }
            }
            if (!mLowMemFlag && !mIsBootImageOnDisk) {
                Slog.i(TAG, "No boot image on disk due to lack of space. Sending notification");
                sendNotification();
            }
            if (mFreeMem < mMemFullThreshold) {
                if (!mMemFullFlag) {
                    sendFullNotification();
                    mMemFullFlag = true;
                }
            } else {
                if (mMemFullFlag) {
                    cancelFullNotification();
                    mMemFullFlag = false;
                }
            }
        }

        //[FEATURE]-Add-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
        if (isEnableLowMemoryPolicy(mContext)) {
            checkLowMemory();
        }
        //[FEATURE]-Add-END by zhengyu.hu for task-2694357

        if(localLOGV) Slog.i(TAG, "Posting Message again");
        //keep posting messages to itself periodically
        postCheckMemoryMsg(true, DEFAULT_CHECK_INTERVAL);
    }

    void postCheckMemoryMsg(boolean clearCache, long delay) {
        // Remove queued messages
        mHandler.removeMessages(DEVICE_MEMORY_WHAT);
        mHandler.sendMessageDelayed(mHandler.obtainMessage(DEVICE_MEMORY_WHAT,
                clearCache ?_TRUE : _FALSE, 0),
                delay);
    }

    public DeviceStorageMonitorService(Context context) {
        super(context);
        mContext = context;
        mLastReportedFreeMemTime = 0;
        mResolver = context.getContentResolver();
        mIsBootImageOnDisk = isBootImageOnDisk();
        //create StatFs object
        mDataFileStats = new StatFs(DATA_PATH.getAbsolutePath());
        mSystemFileStats = new StatFs(SYSTEM_PATH.getAbsolutePath());
        mCacheFileStats = new StatFs(CACHE_PATH.getAbsolutePath());
        //initialize total storage on device
        mTotalMemory = (long)mDataFileStats.getBlockCount() *
                        mDataFileStats.getBlockSize();
        mStorageLowIntent = new Intent(Intent.ACTION_DEVICE_STORAGE_LOW);
        mStorageLowIntent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
        mStorageOkIntent = new Intent(Intent.ACTION_DEVICE_STORAGE_OK);
        mStorageOkIntent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
        mStorageFullIntent = new Intent(Intent.ACTION_DEVICE_STORAGE_FULL);
        mStorageFullIntent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
        mStorageNotFullIntent = new Intent(Intent.ACTION_DEVICE_STORAGE_NOT_FULL);
        mStorageNotFullIntent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);


        //[FEATURE]-Add-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
        /* MODIFIED-BEGIN by weifan.li, 2016-12-02,BUG-3478490*/
        if ((Settings.Global.getInt(mResolver, Settings.Global.LOW_STORAGE_MODE, 0)) != 0) {
            Settings.Global.putInt(mResolver, Settings.Global.LOW_STORAGE_MODE, 0);
        }
        /* MODIFIED-END by weifan.li,BUG-3478490*/
        if (isEnableLowMemoryPolicy(mContext)) {
            init(mContext);
        }
        //[FEATURE]-Mod-END by zhengyu.hu for task-2694357
    }

    private static boolean isBootImageOnDisk() {
        for (String instructionSet : InstructionSets.getAllDexCodeInstructionSets()) {
            if (!VMRuntime.isBootClassPathOnDisk(instructionSet)) {
                return false;
            }
        }
        return true;
    }

    /**
    * Initializes the disk space threshold value and posts an empty message to
    * kickstart the process.
    */
    @Override
    public void onStart() {
        // cache storage thresholds
        final StorageManager sm = StorageManager.from(getContext());
         //[FEATURE]-Mod-START by zhengyu.hu for task-2694357
        if (isEnableLowMemoryPolicy(mContext)) {
            int defaultvalue = mContext.getResources().getInteger(com.android.internal.R.integer.def_tctfw_low_memory_notification_mode_threshold);
            NOTIFICATION_MODE_THRESHOLD = defaultvalue*1024*1024;
            mMemLowThreshold = NOTIFICATION_MODE_THRESHOLD;
            mMemFullThreshold = EXTREMELY_MODE_THRESHOLD;//FULL_THRESHOLD_BYTES; // MODIFIED by cheng.zhao-nb, 2016-11-17,BUG-3476904

            if (localLOGV) Slog.v(TAG, "onStart EnableLowMemoryPolicy mMemLowThreshold: " + mMemLowThreshold);
        } else {
            mMemLowThreshold = sm.getStorageLowBytes(DATA_PATH);
            mMemFullThreshold = sm.getStorageFullBytes(DATA_PATH);

            if (localLOGV) Slog.v(TAG, "onStart not EnableLowMemoryPolicy mMemLowThreshold: " + mMemLowThreshold);
        }
        //[FEATURE]-Mod-END by zhengyu.hu for task-2694357

        mMemCacheStartTrimThreshold = ((mMemLowThreshold*3)+mMemFullThreshold)/4;
        mMemCacheTrimToThreshold = mMemLowThreshold
                + ((mMemLowThreshold-mMemCacheStartTrimThreshold)*2);
        mFreeMemAfterLastCacheClear = mTotalMemory;
        checkMemory(true);

        mCacheFileDeletedObserver = new CacheFileDeletedObserver();
        mCacheFileDeletedObserver.startWatching();

        publishBinderService(SERVICE, mRemoteService);
        publishLocalService(DeviceStorageMonitorInternal.class, mLocalService);
    }

    private final DeviceStorageMonitorInternal mLocalService = new DeviceStorageMonitorInternal() {
        @Override
        public void checkMemory() {
            // force an early check
            postCheckMemoryMsg(true, 0);
        }

        @Override
        public boolean isMemoryLow() {
           //[FEATURE]-Mod-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
           if (isEnableLowMemoryPolicy(mContext)) {
                return mLowmemoryModeFlag;
            }else{
              return mLowMemFlag || !mIsBootImageOnDisk;
            }
           //[FEATURE]-Mod-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
        }

        @Override
        public long getMemoryLowThreshold() {
            //[FEATURE]-ModAdd-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
            if (isEnableLowMemoryPolicy(mContext)) {
                return LOWMEMORY_MODE_THRESHOLD;
            }else{
                return mMemLowThreshold;
            }
            //[FEATURE]-Mod-END by zhengyu.hu
        }
    };

    private final IBinder mRemoteService = new Binder() {
        @Override
        protected void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
            if (getContext().checkCallingOrSelfPermission(android.Manifest.permission.DUMP)
                    != PackageManager.PERMISSION_GRANTED) {

                pw.println("Permission Denial: can't dump " + SERVICE + " from from pid="
                        + Binder.getCallingPid()
                        + ", uid=" + Binder.getCallingUid());
                return;
            }

            dumpImpl(pw);
        }
    };

    void dumpImpl(PrintWriter pw) {
        final Context context = getContext();

        pw.println("Current DeviceStorageMonitor state:");

        pw.print("  mFreeMem="); pw.print(Formatter.formatFileSize(context, mFreeMem));
        pw.print(" mTotalMemory=");
        pw.println(Formatter.formatFileSize(context, mTotalMemory));

        pw.print("  mFreeMemAfterLastCacheClear=");
        pw.println(Formatter.formatFileSize(context, mFreeMemAfterLastCacheClear));

        pw.print("  mLastReportedFreeMem=");
        pw.print(Formatter.formatFileSize(context, mLastReportedFreeMem));
        pw.print(" mLastReportedFreeMemTime=");
        TimeUtils.formatDuration(mLastReportedFreeMemTime, SystemClock.elapsedRealtime(), pw);
        pw.println();

        pw.print("  mLowMemFlag="); pw.print(mLowMemFlag);
        pw.print(" mMemFullFlag="); pw.println(mMemFullFlag);
        pw.print(" mIsBootImageOnDisk="); pw.print(mIsBootImageOnDisk);

        pw.print("  mClearSucceeded="); pw.print(mClearSucceeded);
        pw.print(" mClearingCache="); pw.println(mClearingCache);

        pw.print("  mMemLowThreshold=");
        pw.print(Formatter.formatFileSize(context, mMemLowThreshold));
        pw.print(" mMemFullThreshold=");
        pw.println(Formatter.formatFileSize(context, mMemFullThreshold));

        pw.print("  mMemCacheStartTrimThreshold=");
        pw.print(Formatter.formatFileSize(context, mMemCacheStartTrimThreshold));
        pw.print(" mMemCacheTrimToThreshold=");
        pw.println(Formatter.formatFileSize(context, mMemCacheTrimToThreshold));
    }

    /**
    * This method sends a notification to NotificationManager to display
    * an error dialog indicating low disk space and launch the Installer
    * application
    */
    private void sendNotification() {
        final Context context = getContext();
        if(localLOGV) Slog.i(TAG, "Sending low memory notification");

        //[BUGFIX]-Add-BEGIN by zhengyu.hu for task-2694357
        if (!isEnableLowMemoryPolicy(mContext)) {
            return;
        }
        //[BUGFIX]-MOD-END by zhengyu.hu for task-2694357

        //log the event to event log with the amount of free storage(in bytes) left on the device
        EventLog.writeEvent(EventLogTags.LOW_STORAGE, mFreeMem);

        //  Pack up the values and broadcast them to everyone
        Intent lowMemIntent = new Intent(Environment.isExternalStorageEmulated()
                ? Settings.ACTION_INTERNAL_STORAGE_SETTINGS
                : Intent.ACTION_MANAGE_PACKAGE_STORAGE);

        lowMemIntent.putExtra("memory", mFreeMem);
        lowMemIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        NotificationManager mNotificationMgr =
                (NotificationManager)context.getSystemService(
                        Context.NOTIFICATION_SERVICE);
        CharSequence title = context.getText(
                com.android.internal.R.string.low_internal_storage_view_title);
        CharSequence details = context.getText(mIsBootImageOnDisk
                ? com.android.internal.R.string.low_internal_storage_view_text
                : com.android.internal.R.string.low_internal_storage_view_text_no_boot);
        PendingIntent intent = PendingIntent.getActivityAsUser(context, 0,  lowMemIntent, 0,
                null, UserHandle.CURRENT);
        Notification notification = new Notification.Builder(context)
                .setSmallIcon(com.android.internal.R.drawable.stat_notify_disk_full)
                .setTicker(title)
                .setColor(context.getColor(
                    com.android.internal.R.color.system_notification_accent_color))
                .setContentTitle(title)
                .setContentText(details)
                .setContentIntent(intent)
                .setStyle(new Notification.BigTextStyle()
                      .bigText(details))
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setCategory(Notification.CATEGORY_SYSTEM)
                .build();
        notification.flags |= Notification.FLAG_NO_CLEAR;
        mNotificationMgr.notifyAsUser(null, LOW_MEMORY_NOTIFICATION_ID, notification,
                UserHandle.ALL);
        context.sendStickyBroadcastAsUser(mStorageLowIntent, UserHandle.ALL);
    }

    /**
     * Cancels low storage notification and sends OK intent.
     */
    private void cancelNotification() {
        final Context context = getContext();
        if(localLOGV) Slog.i(TAG, "Canceling low memory notification");
        NotificationManager mNotificationMgr =
                (NotificationManager)context.getSystemService(
                        Context.NOTIFICATION_SERVICE);
        //cancel notification since memory has been freed
        mNotificationMgr.cancelAsUser(null, LOW_MEMORY_NOTIFICATION_ID, UserHandle.ALL);

        context.removeStickyBroadcastAsUser(mStorageLowIntent, UserHandle.ALL);
        context.sendBroadcastAsUser(mStorageOkIntent, UserHandle.ALL);
    }

    /**
     * Send a notification when storage is full.
     */
    private void sendFullNotification() {
        if(localLOGV) Slog.i(TAG, "Sending memory full notification");
        getContext().sendStickyBroadcastAsUser(mStorageFullIntent, UserHandle.ALL);
    }

    /**
     * Cancels memory full notification and sends "not full" intent.
     */
    private void cancelFullNotification() {
        if(localLOGV) Slog.i(TAG, "Canceling memory full notification");
        getContext().removeStickyBroadcastAsUser(mStorageFullIntent, UserHandle.ALL);
        getContext().sendBroadcastAsUser(mStorageNotFullIntent, UserHandle.ALL);
    }

    private static class CacheFileDeletedObserver extends FileObserver {
        public CacheFileDeletedObserver() {
            super(Environment.getDownloadCacheDirectory().getAbsolutePath(), FileObserver.DELETE);
        }

        @Override
        public void onEvent(int event, String path) {
            EventLogTags.writeCacheFileDeleted(path);
        }
    }

    //[FEATURE]-Add-BEGIN by zhengyu.hu for task-2694357 Low storage mechanism
    private static final String SYSPROP_LOW_MEMORY_KEY = "sys.lowmemorymode.key";  //[BUGFIX]-Mod by TCTNB.anshu.zhou@tcl.com,12/25/2013,571852,when the phone in low memory or low power mode, disable the another mode
    /*
     1. Notification mode               500M    a, send a notification to statusbar ;
     2. Low memory mode                 200M    a, move applicaiton to sdcard ; b ,forbidden to uninstall the application / clear data
     3. Force mode                      100M    a, force user just can do 4 things  (Application Manger ,file manager ,Dial ,SMS);
     4. Extremely low memory mode       50MB    a, nofication sms disabled   b , can not save log
    */

    public double TEST = 0;//12.5*1024*1024*1024;
    public int NOTIFICATION_MODE_THRESHOLD = 500 * 1024 * 1024 ;
    public static final int LOWMEMORY_MODE_THRESHOLD =200 * 1024 * 1024 ;
    public static final int FORCE_MODE_THRESHOLD = 100 * 1024 * 1024 ;
    public static final int EXTREMELY_MODE_THRESHOLD = 50 * 1024 * 1024 ;

    private static final int DEVICE_MEMORY_CRITICAL_LOW = 2;     // For low storage force mode
    public static final int DEVICE_MEMORY_EXTREMELY_LOW = 3 ;    // Extremely low memory mode

    public boolean mLowmemoryModeFlag = false ;
    public boolean mForceModeFlag = false ;
    public boolean mExtremeModeFlag = false ;

    public Intent mExtremeModeIntent = null ;
    public static final int SMS_DISABLED_NOTIFICATION_ID = 2;
    public boolean mBootCompleted = false ;
    private Intent mDataConnectionDisabled = null;
    private Intent mDataConnectionEnabled = null;
    public AlertDialog mDialog = null;

    public static final int LOW_THRESHOLD_BYTES = 10 * 1024 * 1024; // 10MB
    public static final int FULL_THRESHOLD_BYTES = 5 * 1024 * 1024; // 5MB
    public static final int CRITICAL_LOW_THRESHOLD_BYTES = 4 * 1024 * 1024; // 4MB
    public static final int EXCEPTION_LOW_THRESHOLD_BYTES = 10 * 1024 * 1024; // 10MB
    public static final int EMAIL_CHECK_SIZE = 50 * 1024 * 1024; // 50MB
    public boolean mConfigChanged = false;
    public static final String IPO_POWER_ON = "android.intent.action.ACTION_BOOT_IPO"; // M:For IPO feature

    public boolean mIPOBootup = false; // /M:For IPO feature
    public boolean mCheckAppSize = true;
    public long mCacheSize = 0;
    public long mCodeSize = 0;
    public long mDataSize = 0;
    public long mTotalSize = 0;
    public String[] mStrings = null;
    public boolean mGetSize = false; // M

    private Context mContext;




   public void init(Context context){
        mDataConnectionDisabled = new Intent("storageforcemode.disable.dataconnection");
        mDataConnectionDisabled.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
        mDataConnectionEnabled = new Intent("storageforcemode.enabled.dataconnection");
        mDataConnectionEnabled.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
        IntentFilter filter = new IntentFilter();
        mContext = context;
        filter.addAction(Intent.ACTION_BOOT_COMPLETED);
        mContext.registerReceiver(mIntentReceiver, filter);
   }
    public void handleLowMemory(Context mContext,Message msg){
        //Del by fuyin.liu for Defect: 1073492 --begin
        // if (!checkPackageExist(mContext, "com.android.settings.LowMemoryStorage")) {
          //  return;
        //}
        //Del by fuyin.liu for Defect: 1073492 --end
        if (msg.what == DEVICE_MEMORY_CRITICAL_LOW&&!IfShowDialog("com.android.settings.LowMemoryStorage")) {
            Intent mIntent = new Intent();
            mIntent.setAction("com.android.settings.LowMemoryStorage");
            mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivityAsUser(mIntent, UserHandle.CURRENT_OR_SELF);
        }
     }

    public boolean isEnableLowMemoryPolicy(Context context){
       return context.getResources().getBoolean( com.android.internal.R.bool.feature_tctfw_lowmemory_policy_on) && "false".equals(SystemProperties.get("sys.supermode.key", "false"));
    }

    final IPackageStatsObserver.Stub mStatsObserver = new IPackageStatsObserver.Stub() {
        public void onGetStatsCompleted(PackageStats stats, boolean succeeded) {
            mCacheSize = stats.cacheSize;
            mCodeSize = stats.codeSize;
            mDataSize = stats.dataSize;
            mTotalSize = mCacheSize + mCodeSize + mDataSize;
            mGetSize = true;
            Log.i(TAG, "mStatsObserver  mCacheSize = " + mCacheSize + "mCodeSize = " + mCodeSize
                    + "mDataSize=" + mDataSize + "mTotalSize=" + mTotalSize);
        }
    };

    private void startMoveAppsApplication() {
        if (checkPackageExist(mContext, "com.jrdcom.app2sdcard")) {
            Intent  lowMemIntent = new Intent("com.jrdcom.app2sdcard.MOVE_APPS_TO_SDCARD");
            lowMemIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivityAsUser(lowMemIntent, UserHandle.CURRENT_OR_SELF);
        }
    }

     private void sendDataDisabledNotification() {
        Toast mToast = Toast.makeText(mContext, com.android.internal.R.string.disable_dataconnection_text,Toast.LENGTH_LONG);
        mToast.show();
        Log.i(TAG," sendDataDisabledNotification " );
        mContext.sendStickyBroadcastAsUser(mDataConnectionDisabled, UserHandle.ALL);
        SystemProperties.set(SYSPROP_LOW_MEMORY_KEY, "true");
    }

    private void sendDataEnabledNotification() {
        Log.i(TAG," sendDataEnabledNotification  " );
        mContext.removeStickyBroadcastAsUser(mDataConnectionDisabled, UserHandle.ALL);
        mContext.sendBroadcastAsUser(mDataConnectionEnabled, UserHandle.ALL);
        SystemProperties.set(SYSPROP_LOW_MEMORY_KEY, "false");
    }

    private void sendSmsNotification(){
        Log.i(TAG," send  Sms  Notification ");
        NotificationManager mNotificationMgr =
                (NotificationManager)mContext.getSystemService(
                        Context.NOTIFICATION_SERVICE);
        CharSequence title = mContext.getText(
                 com.android.internal.R.string.low_internal_storage_disablesms_title);
        CharSequence details = mContext.getText(
                 com.android.internal.R.string.low_internal_storage_disablesms_text);
        Notification notification = new Notification();
        notification.icon = com.android.internal.R.drawable.stat_notify_disk_full;
        notification.tickerText = title;
        notification.flags |= Notification.FLAG_NO_CLEAR;
        notification.setLatestEventInfo(mContext, title, details, null);
        mNotificationMgr.notifyAsUser(null, SMS_DISABLED_NOTIFICATION_ID, notification,
                UserHandle.ALL);
    }

    private final void cancelSmsNotification() {
        Log.i(TAG, "cancel sms notification ");
        NotificationManager mNotificationMgr =
                (NotificationManager)mContext.getSystemService(
                        Context.NOTIFICATION_SERVICE);
        mNotificationMgr.cancelAsUser(null, SMS_DISABLED_NOTIFICATION_ID, UserHandle.ALL);
    }

    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(Intent.ACTION_BOOT_COMPLETED)){
                mBootCompleted = true ;
            }
        }
    };
    public boolean checkPackageExist(Context context, String packageName) {
        if (null == packageName || packageName.equals("")) {
            return false;
         }

        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(
                    packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }

    public  boolean IfShowDialog(String str) {
        ActivityManager am = (ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);
        Iterator<RunningTaskInfo> iterator = am.getRunningTasks(10).iterator();
        String topClassName = null;
        String baseClassName = null;
        String lowMemoryClassName = str;
        while (iterator.hasNext()) {
            RunningTaskInfo runningTaskInfo = iterator.next();
            ComponentName top = runningTaskInfo.topActivity;
            ComponentName base = runningTaskInfo.baseActivity;
            if(top != null){
                topClassName = top.getClassName();
            }
            if (base != null) {
                baseClassName = base.getClassName() ;
            }
            if ((lowMemoryClassName == topClassName)||(lowMemoryClassName == baseClassName)){
                return true ;
            }
        }
        return false;
     }

    private void leaveLowStorage(String taskName) {
        ActivityManager am = (ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);
        Iterator<RunningTaskInfo> iterator = am.getRunningTasks(10).iterator();
        while (iterator.hasNext()) {
            boolean isRunningTask = false;
            RunningTaskInfo runningTaskInfo = iterator.next();
            ComponentName top = runningTaskInfo.topActivity;
            ComponentName base = runningTaskInfo.baseActivity;
            if(top != null){
                if (top.getClassName().equals(taskName)) {
                    isRunningTask = true;
                }
            }
            if (!isRunningTask && base != null) {
                if (base.getClassName().equals(taskName)) {
                    isRunningTask = true;
                }
            }
            if (isRunningTask) {
                Log.i(TAG, "remove task " + taskName);
                am.removeTask(runningTaskInfo.id );
            }
        }
    }

    public void checkEmail(){
        if (mCheckAppSize) {
            mCheckAppSize = false;
            PackageManager pm = mContext.getPackageManager();
            pm.getPackageSizeInfo("com.android.email", mStatsObserver);
         }
    }


    public void checkLowMemory(){
        if (mFreeMem < LOWMEMORY_MODE_THRESHOLD) {
            if(!mLowmemoryModeFlag) {
                Log.i(TAG," startMoveAppsApplication() ");
                startMoveAppsApplication() ;
                mLowmemoryModeFlag = true ;
            }
        } else {
            if(mLowmemoryModeFlag){
                mLowmemoryModeFlag = false ;
            }
        }
        // /M:Add for low storage, if storage <4M,show the wanring
        // message,@{
        if ( mFreeMem < FORCE_MODE_THRESHOLD/*CRITICAL_LOW_THRESHOLD_BYTES*/) {
            Log.i(TAG, "now device into < 15M storage");

            /* MODIFIED-BEGIN by weifan.li, 2016-12-02,BUG-3478490*/
            if ((Settings.Global.getInt(mResolver, Settings.Global.LOW_STORAGE_MODE, 0)) == 0) {
                Settings.Global.putInt(mResolver, Settings.Global.LOW_STORAGE_MODE, 1);
            }
            /* MODIFIED-END by weifan.li,BUG-3478490*/

            if (mGetSize) {
                mHandler.sendMessage(mHandler.obtainMessage(DEVICE_MEMORY_CRITICAL_LOW));
            }
            if(!mForceModeFlag){
                mForceModeFlag = true ;
                sendDataDisabledNotification();
            }
        } else {
            /* MODIFIED-BEGIN by weifan.li, 2016-12-02,BUG-3478490*/
            if ((Settings.Global.getInt(mResolver, Settings.Global.LOW_STORAGE_MODE, 0)) != 0) {
                Settings.Global.putInt(mResolver, Settings.Global.LOW_STORAGE_MODE, 0);
            }
            /* MODIFIED-END by weifan.li,BUG-3478490*/

            if(mForceModeFlag){
                mForceModeFlag = false ;
                sendDataEnabledNotification();
            }
        }// (endof mFreeMem < 4M )
        // /@}
        if(mFreeMem < EXTREMELY_MODE_THRESHOLD ){
           if(!mExtremeModeFlag) {
               if(mExtremeModeIntent == null ){
                    mExtremeModeIntent = new Intent("intent.action.EXTREMELY_MODE_THRESHOLD");
               }
               sendSmsNotification();
               mExtremeModeFlag = true ;
           }
        } else {
           if(mExtremeModeFlag){
               cancelSmsNotification();
               mExtremeModeFlag = false ;
           }
        }
    }
//[FEATURE]-Add-END by zhengyu.hu for task-2694357

}
