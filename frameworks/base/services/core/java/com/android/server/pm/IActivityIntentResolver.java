/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.server.pm;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageParser;
import android.content.pm.ResolveInfo;
import android.util.ArrayMap;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public interface IActivityIntentResolver {
    List<ResolveInfo> queryIntent(Intent intent, String resolvedType,
                                         boolean defaultOnly, int userId);
    List<ResolveInfo> queryIntent(Intent intent, String resolvedType, int flags,
                                  int userId);
    List<ResolveInfo> queryIntentForPackage(Intent intent, String resolvedType,
                                            int flags, ArrayList<PackageParser.Activity> packageActivities, int userId);
    void addActivity(PackageParser.Activity a, String type);
    void removeActivity(PackageParser.Activity a, String type);
    boolean dump(PrintWriter out, String title, String prefix, String packageName,
                 boolean printFilter, boolean collapseDuplicates);
    ArrayMap<ComponentName, PackageParser.Activity> getActivities();
}
