/* Copyright (C) 2016 Tcl Corporation Limited */
/* ***************************************************************************/
/*                                                       Date : Feb 2, 2015  */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2015 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Hanwu.xie (XHANWU)                                                    */
/*   Role :    Telecom Leader                                                */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : AttrNameService.java                                                */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*02/02/15 | hanwu.xie | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.android.server;

import android.app.StringMapParcelable;
import android.app.ValueItem;
import android.app.IAttrNameManager;
import android.os.SystemProperties;
import android.util.Slog;
import android.content.Context;
import android.widget.Toast;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.WindowManager;
import android.os.Handler;
import android.os.Looper;
import java.util.HashMap;
import java.util.Locale;

import com.android.server.SystemService;
import android.content.Context;

import android.os.Build;//Task-340952 zubai.li add 20150615
import android.content.pm.PackageInfo;

import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.PowerManager;

public final class AttrNameService extends SystemService {
    private static final String TAG = "AttrNameService";
    private final AttrNameManagerImpl mAttrNameManagerImpl;
    private Context mContext;
    private NotificationManager mNotificationManager;

    // private static final boolean IS_USER_BUILD = "user".equals(Build.TYPE) ||
    // "userdebug".equals(Build.TYPE);//Task-340952 zubai.li add 20150615 start

    public AttrNameService(Context context) {
        super(context);
        mContext = context;
        mAttrNameManagerImpl = new AttrNameManagerImpl();
    }

    @Override
    public void onStart() {
        Slog.i(TAG, "Registering AttrNameService ");
        publishBinderService(Context.JRD_ATTR_NAME_SERVICE, mAttrNameManagerImpl);
    }

    class AttrNameManagerImpl extends IAttrNameManager.Stub {

        private HashMap<String, ValueItem> stringInfoMap = new HashMap<String, ValueItem>();
        private boolean mIsShowId = false;
        private boolean mIsShowStringAttribute = false;
        private boolean mIsStart = false;
        private final Object setSync = new Object();

        public AttrNameManagerImpl() {
            Slog.i(TAG, "<<<<<<<<<Create AttrNameManagerImpl object");
            mIsStart = SystemProperties.getBoolean("persist.sys.isToolStart", false);
            mIsShowId = SystemProperties.getBoolean("persist.sys.showId", false);
            mIsShowStringAttribute = SystemProperties.getBoolean("persist.sys.showZoneType", false);
        }

        public void clearAllCache() {
            if (stringInfoMap != null) {
                stringInfoMap.clear();
            }
        }

        public boolean isAttrNameServiceStart() {
            // Task-340952 zubai.li add 20150615 start
            // if(IS_USER_BUILD){
            // return false;
            // }
            // Task-340952 zubai.li add 20150615 end
            // Slog.i(TAG, "<<<<<<<<<isAttrNameServiceStart = " + isStart);
            return mIsStart;
        }

        public void setAttrNameServiceStart(boolean toStart) {
            Slog.i(TAG, "<<<<<<<<<setAttrNameServiceStart = " + toStart);
            synchronized (setSync) {
                mIsStart = toStart;
            }

            // update notification information
            /*
             * if (mIsStart) { CharSequence title = "AttrName APP Setting";
             * CharSequence message =
             * "show strId, string attribute, export to excel,...";
             * mNotificationManager = (NotificationManager)
             * mContext.getSystemService(Context.NOTIFICATION_SERVICE);
             * Notification notification = new Notification(); notification.icon
             * = com.android.internal.R.drawable.stat_sys_adb; notification.when
             * = 0; notification.flags = Notification.FLAG_ONGOING_EVENT;
             * notification.tickerText = title; notification.defaults = 0; //
             * please be quiet notification.sound = null; notification.vibrate =
             * null; notification.priority = Notification.PRIORITY_DEFAULT;
             * Intent intent = new Intent();
             * intent.setClassName("com.android.server",
             * "com.android.systemui.jrdRefBuilder.AttrNameSettingActivity");
             * PendingIntent pendingIntent=PendingIntent.getActivity(mContext,
             * 0, intent, 0); notification.setLatestEventInfo(mContext, title,
             * message, pendingIntent); mNotificationManager.notify(0,
             * notification); }
             */
            SystemProperties.set("persist.sys.isToolStart", toStart ? "true" : "false");
            pokeSystemProperties();
        }

        public boolean isShowId() {
            Slog.i(TAG, "<<<<<<<<<mIsShowId = " + mIsShowId);
            return (isAttrNameServiceStart() && mIsShowId);
        }

        public void setShowId(boolean isShow) {
            synchronized (setSync) {
                mIsShowId = isShow;
                SystemProperties.set("persist.sys.showId", isShow ? "true" : "false");
                pokeSystemProperties();
            }
        }

        public boolean isShowStringAttribute() {
            return (isAttrNameServiceStart() && mIsShowStringAttribute);
        }

        public void setShowStringAttribute(boolean isShow) {
            synchronized (setSync) {
                mIsShowStringAttribute = isShow;
                SystemProperties.set("persist.sys.showZoneType", isShow ? "true" : "false");
                pokeSystemProperties();
            }
        }

        public StringMapParcelable getStingMap() {
            StringMapParcelable strMap = new StringMapParcelable();
            strMap.setStringMap(stringInfoMap);

            return strMap;
        }

        public void updateStringInfo(String key, String attrName, String attrValue,
                String packName, int zoneWidth, int zoneHeight, int textSize, int bold, int italic,
                int isEllipse, String currLang) {

            if (key != null && key.length() > 0 && attrValue != null && attrValue.length() > 0) {
                // String strKey = getMapKey(key);
                ValueItem v = stringInfoMap.get(key);
                if (v == null) {
                    v = new ValueItem();
                    v.setZoneWidth(zoneWidth);
                    v.setZoneHeight(zoneHeight);
                } else {
                    if (v.getZoneWidth() == 0 || zoneWidth < v.getZoneWidth()) {
                        v.setZoneWidth(zoneWidth);
                    }
                    if (v.getZoneHeight() == 0 || zoneHeight < v.getZoneHeight()) {
                        v.setZoneHeight(zoneHeight);
                    }
                }
                v.setAttrName(attrName);
                v.setAttrValue(attrValue);
                v.setPackageName(packName);
                v.setTextSize(textSize);
                v.setTextBold(bold);
                v.setTextItalic(italic);
                v.setEllipse(isEllipse);
                v.setCurrentLanguage(currLang);

                stringInfoMap.put(key, v);

                Slog.i(TAG, "<<<<<<<<<updateStringInfo attrName =" + v.getAttrName()
                        + ", packageName=" + v.getPackageName() + ", zoneWidth=" + v.getZoneWidth()
                        + ", zoneHeight=" + v.getZoneHeight() + ", textSize=" + v.getTextSize()
                        + ", value=" + v.getAttrValue());

            }
        }

        public void showStringAttributeInfo(String attrName, String attrValue, String packName,
                int zoneWidth, int zoneHeight, int textSize, int bold, int italic) {
            String message = "id: " + attrName + "\nWidth: " + zoneWidth + "\nHeight: "
                    + zoneHeight + "\nText Size: " + textSize;

            final CharSequence charMessage = message;
            final CharSequence charTitle = (attrValue != null) ? attrValue : "?";

            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                public void run() {
                    // Toast.makeText(getContext(),"display string information",
                    // Toast.LENGTH_LONG).show();

                    AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                            .setTitle(charTitle).setMessage(charMessage)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                }
                            }).create();
                    alertDialog.getWindow().setType(
                            WindowManager.LayoutParams.TYPE_STATUS_BAR_PANEL);
                    alertDialog.show();

                }
            });
        }

        private void pokeSystemProperties() {
            (new SystemPropPoker()).execute();
        }

        private class SystemPropPoker extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... params) {
                String[] services;
                try {
                    services = ServiceManager.listServices();
                } catch (Exception e) {
                    return null;
                }
                for (String service : services) {
                    IBinder obj = ServiceManager.checkService(service);
                    if (obj != null) {
                        Parcel data = Parcel.obtain();
                        try {
                            obj.transact(IBinder.SYSPROPS_TRANSACTION, data, null, 0);
                        } catch (RemoteException e) {
                        } catch (Exception e) {
                            Slog.i(TAG, "Somone wrote a bad service '" + service
                                    + "' that doesn't like to be poked: " + e);
                        }
                        data.recycle();
                    }
                }
                return null;
            }
        }

        public void rebootPhone() {
            Slog.i(TAG, "<<<<<<<<<rebootPhone");
            PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
            pm.reboot(null);

            /*
             * TODO:above API will cause: java.lang.SecurityException: Neither
             * user 10037 nor current process has android.permission.REBOOT. How
             * to FIX IT??
             */
        }

        /**
         * @hide
         */
        /*
         * private static void setAllPackageServiceState(boolean state){ final
         * PackageManager mPm = getContext().getPackageManager(); if(mPm ==
         * null){ Slog.i(TAG,
         * "isPackageRuning match package name exception, mPm is null"); return;
         * } List<PackageInfo> packages = null; try{ packages =
         * mPm.getInstalledPackages(0,0).getList(); if(packages != null){
         * for(PackageInfo p : packages){ Slog.i(TAG,
         * "isPackageRuning p.packageName:" + p.packageName); if(p.packageName
         * != null){ Resources resources =
         * mPm.getResourcesForApplication(p.packageName); if (resources != null)
         * { resources.setAttrNameServiceState(state); } } } } } catch(Exception
         * e){ Slog.i(TAG, e.toString()); } return false; }
         */

        /*
         * public void addTypedArrayStringInfo(int indexOfTypedArray){ } private
         * String getMapKey(String attrValue) { String currLang =
         * Locale.getDefault().getLanguage(); String strKey = null; if
         * (attrValue != null && attrValue.length() > 0) { strKey = attrValue +
         * "-" + currLang; } return strKey; } public void addStringInfo(String
         * packageName, String attrName, String attrValue){ String strKey =
         * getMapKey(attrValue); if (strKey != null) { ValueItem v =
         * stringInfoMap.get(strKey); if (v == null) { v = new ValueItem();
         * v.setPackageName(packageName); v.setAttrName(attrName);
         * v.setAttrValue(attrValue); stringInfoMap.put(strKey, v); Slog.i(TAG,
         * "<<<<<<<<<addStringInfo packageName=" + packageName + ";attrName="+
         * attrName + ";attrValue="+ attrValue); } else { //The duplicate
         * attrValue or the same attrValue with different attrName //To do
         * list... } } }
         */

        /*
         * public String getStringInfo(String attrValue) { String attrName =
         * null; if (attrValue != null) { String strKey = getMapKey(attrValue);
         * ValueItem v = stringInfoMap.get(strKey); if (v != null) { attrName =
         * v.getAttrName(); } } Slog.i(TAG, "<<<<<<<<<getStringInfo attrValue="
         * + attrValue + ";attrName=" +attrName); return attrName; }
         */

    }

}
