/*willab*/
//#define LOG_NDEBUG 0
#define LOG_TAG "FileLock-JNI"
#include <utils/Log.h>

#include "jni.h"
#include "JNIHelp.h"
#include "core_jni_helpers.h"

#include <ScopedUtfChars.h>
#include <cutils/properties.h>
#include <utils/Vector.h>
#include <utils/Errors.h>

#include <binder/IMemory.h>
#include <gui/FileLockManager.h>

using namespace android;
using android::FileLockManager;
using android::String8;
android::Mutex android::FileLockManager::sLock;
android::FileLockManager* android::FileLockManager::mFileLockManager;

static jint android_hardware_FileLock_lockFile(JNIEnv *env, jobject thiz, jstring fileName) {
	ALOGE("hello android_hardware_FileLock_lockFile...");
	ScopedUtfChars fileNameUtf(env, fileName);
	int ret = FileLockManager::getInstanceForFileLock().lockFile((const String8)(fileNameUtf.c_str()));
	return ret;
}

static jint android_hardware_FileLock_unLockFile(JNIEnv *env, jobject thiz, jstring fileName) {
	ALOGE("hello android_hardware_FileLock_unLockFile");
	ScopedUtfChars fileNameUtf(env, fileName);
	int ret = FileLockManager::getInstanceForFileLock().unLockFile((const String8)(fileNameUtf.c_str()));
	return ret;
}

static jint android_hardware_FileLock_hideLockFile(JNIEnv *env, jobject thiz) {
	ALOGE("hello android_hardware_FileLock_hideLockFile");
	int ret = FileLockManager::getInstanceForFileLock().hideLockFile();
	return ret;
}

static jint android_hardware_FileLock_unHideAnyFile(JNIEnv *env, jobject thiz) {
	ALOGE("hello android_hardware_FileLock_unHideAnyFile");
	int ret = FileLockManager::getInstanceForFileLock().unHideAnyFile();
	return ret;
}

static jint android_hardware_FileLock_hideUnLockFile(JNIEnv *env, jobject thiz) {
	ALOGE("hello android_hardware_FileLock_hideUnLockFile");
	int ret = FileLockManager::getInstanceForFileLock().hideUnLockFile();
	return ret;
}

static void android_hardware_FileLock_checkFileState(JNIEnv *env, jobject thiz, jstring fileName) {
	ALOGE("hello android_hardware_FileLock_checkFileState");
	int state = -1;
	ScopedUtfChars fileNameUtf(env, fileName);
	int ret = FileLockManager::getInstanceForFileLock().checkFileState((const String8)(fileNameUtf.c_str()), state);

	jclass cls = env->GetObjectClass(thiz);   
    jmethodID callback = env->GetMethodID(cls,"callbackForCheckState","(II)V");  
    env->CallVoidMethod(thiz,callback,ret,state);
	ALOGE("hello checkFileState, ret is %d, state is %d", ret, state);

}

static jint android_hardware_FileLock_checkFileLockService(JNIEnv *env, jobject thiz) {
	ALOGE("hello android_hardware_FileLock_checkFileLockService");
	
	return FileLockManager::checkFileLockService();
}

//-------------------------------------------------
static JNINativeMethod fileLockMethods[] = {
  { "native_lockFile",
	"(Ljava/lang/String;)I",
	(void *)android_hardware_FileLock_lockFile },
  { "native_unLockFile",
	"(Ljava/lang/String;)I",
	(void *)android_hardware_FileLock_unLockFile },
  { "native_hideLockFile",
	"()I",
	(void *)android_hardware_FileLock_hideLockFile },
  { "native_unHideAnyFile",
	"()I",
	(void *)android_hardware_FileLock_unHideAnyFile },
  { "native_hideUnLockFile",
	"()I",
	(void *)android_hardware_FileLock_hideUnLockFile },
  { "native_checkFileState",
	"(Ljava/lang/String;)V",
	(void *)android_hardware_FileLock_checkFileState },
  { "native_checkFileLockService",
	"()I",
	(void *)android_hardware_FileLock_checkFileLockService },
};

// Get all the required offsets in java class and register native functions
int register_android_hardware_FileLock(JNIEnv *env)
{
    // Register native functions
    return RegisterMethodsOrDie(env, "android/hardware/FileLock", fileLockMethods, NELEM(fileLockMethods));
}

