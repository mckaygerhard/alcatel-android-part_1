#include "CreateJavaOutputStreamAdaptor.h"
#include "GraphicsJNI.h"
#include "ScopedLocalRef.h"
#include "SkFrontBufferedStream.h"
#include "SkMovie.h"
#include "SkStream.h"
#include "SkUtils.h"
#include "Utils.h"
#include "core_jni_helpers.h"

#include <androidfw/Asset.h>
#include <androidfw/ResourceTypes.h>
#include <hwui/Canvas.h>
#include <hwui/Paint.h>
#include <jni.h>
#include <netinet/in.h>

//FEATURE]-Add-BEGIN by TCTNB.Yang.Hu,2015/9/30, task-572426 TCT DRM solution
#include "DrmManagerClient.h"
#include <utils/String8.h>
#include <errno.h>
#include "JNIHelp.h"
#include <fcntl.h>

using namespace android;
//[FEATURE]-Add-END by TCTNB.Yang.Hu TCT DRM solution

static jclass       gMovie_class;
static jmethodID    gMovie_constructorMethodID;
static jfieldID     gMovie_nativeInstanceID;

jobject create_jmovie(JNIEnv* env, SkMovie* moov) {
    if (NULL == moov) {
        return NULL;
    }
    return env->NewObject(gMovie_class, gMovie_constructorMethodID,
            static_cast<jlong>(reinterpret_cast<uintptr_t>(moov)));
}

static SkMovie* J2Movie(JNIEnv* env, jobject movie) {
    SkASSERT(env);
    SkASSERT(movie);
    SkASSERT(env->IsInstanceOf(movie, gMovie_class));
    SkMovie* m = (SkMovie*)env->GetLongField(movie, gMovie_nativeInstanceID);
    SkASSERT(m);
    return m;
}

///////////////////////////////////////////////////////////////////////////////

static jint movie_width(JNIEnv* env, jobject movie) {
    NPE_CHECK_RETURN_ZERO(env, movie);
    return static_cast<jint>(J2Movie(env, movie)->width());
}

static jint movie_height(JNIEnv* env, jobject movie) {
    NPE_CHECK_RETURN_ZERO(env, movie);
    return static_cast<jint>(J2Movie(env, movie)->height());
}

static jboolean movie_isOpaque(JNIEnv* env, jobject movie) {
    NPE_CHECK_RETURN_ZERO(env, movie);
    return J2Movie(env, movie)->isOpaque() ? JNI_TRUE : JNI_FALSE;
}

static jint movie_duration(JNIEnv* env, jobject movie) {
    NPE_CHECK_RETURN_ZERO(env, movie);
    return static_cast<jint>(J2Movie(env, movie)->duration());
}

static jboolean movie_setTime(JNIEnv* env, jobject movie, jint ms) {
    NPE_CHECK_RETURN_ZERO(env, movie);
    return J2Movie(env, movie)->setTime(ms) ? JNI_TRUE : JNI_FALSE;
}

static void movie_draw(JNIEnv* env, jobject movie, jlong canvasHandle,
                       jfloat fx, jfloat fy, jlong paintHandle) {
    NPE_CHECK_RETURN_VOID(env, movie);

    android::Canvas* c = reinterpret_cast<android::Canvas*>(canvasHandle);
    const android::Paint* p = reinterpret_cast<android::Paint*>(paintHandle);

    // Canvas should never be NULL. However paint is an optional parameter and
    // therefore may be NULL.
    SkASSERT(c != NULL);

    SkMovie* m = J2Movie(env, movie);
    const SkBitmap& b = m->bitmap();

    c->drawBitmap(b, fx, fy, p);
}

static jobject movie_decodeAsset(JNIEnv* env, jobject clazz, jlong native_asset) {
    android::Asset* asset = reinterpret_cast<android::Asset*>(native_asset);
    if (asset == NULL) return NULL;
    android::AssetStreamAdaptor stream(asset);
    SkMovie* moov = SkMovie::DecodeStream(&stream);
    return create_jmovie(env, moov);
}

static jobject movie_decodeStream(JNIEnv* env, jobject clazz, jobject istream) {

    NPE_CHECK_RETURN_ZERO(env, istream);

    jbyteArray byteArray = env->NewByteArray(16*1024);
    ScopedLocalRef<jbyteArray> scoper(env, byteArray);
    SkStream* strm = CreateJavaInputStreamAdaptor(env, istream, byteArray);
    if (NULL == strm) {
        return 0;
    }

    // Need to buffer enough input to be able to rewind as much as might be read by a decoder
    // trying to determine the stream's format. The only decoder for movies is GIF, which
    // will only read 6.
    // FIXME: Get this number from SkImageDecoder
    // bufferedStream takes ownership of strm
    std::unique_ptr<SkStreamRewindable> bufferedStream(SkFrontBufferedStream::Create(strm, 6));
    SkASSERT(bufferedStream.get() != NULL);

    SkMovie* moov = SkMovie::DecodeStream(bufferedStream.get());
    return create_jmovie(env, moov);
}

//FEATURE]-Add-BEGIN by TCTNB.Yang.Hu,2015/9/30, task-572426 TCT DRM solution
static jobject movie_mDrmdecodeFile(JNIEnv* env, jobject clazz, jstring filePath) {
    NPE_CHECK_RETURN_ZERO(env, filePath);
    String8 dataString("");//the file path string8
    if (NULL != filePath && filePath != env->NewStringUTF("")) {
        char* bytes = const_cast< char* > (env->GetStringUTFChars(filePath, NULL));
        const int length = strlen(bytes) + 1;
        char *data = new char[length];
        strncpy(data, bytes, length);
        dataString = String8(data);
        env->ReleaseStringUTFChars(filePath, bytes);
        delete [] data; data = NULL;
    } else {
        return 0;
    }
    // get the file length
    int fd = open(dataString.string(), O_RDONLY);
    if (fd <0) {
        return 0;
    }

    int fileLength = lseek64(fd, 0, SEEK_END);
    if (fileLength <= 0) {
        close(fd);
        return 0;
    }

    unsigned char* fileData = new unsigned char[fileLength];
    DrmManagerClient  drmClient;
    SkStream* strm = NULL;
    bool canHandle = drmClient.canHandle(dataString,String8(""));
    bool isDrm = drmClient.isDrm(dataString);
    if (canHandle || isDrm) {
        strm  = CreateDrmInputStreamAdaptor(fd);
        if (strm == NULL) {
            close(fd);
            delete[] fileData;
            fileData = NULL;
            return 0;
        }
        int count =  strm->read(fileData,fileLength);
        if (count <= 0) {
            delete[] fileData;
            fileData = NULL;
            //strm->unref();
            strm = NULL;
            close(fd);
            return 0;
        }
    } else {
        lseek64(fd, 0, SEEK_SET);
        int count =  read(fd, fileData, fileLength);
        if (count <= 0) {
            close(fd);
            delete[] fileData;fileData = NULL;
            return 0;
        }
    }

    close(fd);
    jbyteArray jBuffer = env->NewByteArray(fileLength);
    env->SetByteArrayRegion(jBuffer,0,fileLength,(jbyte*)fileData);
    delete[] fileData;fileData = NULL;
    AutoJavaByteArray   ar(env, jBuffer);
    SkMovie* moov = SkMovie::DecodeMemory(ar.ptr() , fileLength);
    if (NULL == moov) {
        if (strm != NULL) {
            //strm->unref();
            strm = NULL;
        }
        return 0;
    }
    jobject  jMovie =  create_jmovie(env, moov);
    if(jMovie != NULL && strm != NULL){
        strm->consumeRight();
        //strm->unref();
        strm = NULL;
    }

    return jMovie;
}

static jobject movie_mDrmdecodeFileDescriptor(JNIEnv* env, jobject clazz,jobject fdobj) {
    NPE_CHECK_RETURN_ZERO(env, fdobj);
    jint descriptor = jniGetFDFromFileDescriptor(env, fdobj);
    int fd = dup(descriptor);
    if (fd == -1) {
        return 0;
    }

    int fileLength = lseek64(fd, 0, SEEK_END);
    unsigned char* fileData = new unsigned char[fileLength];//for store the all gif data.
    DrmManagerClient drmClient;
    SkStream* strm = NULL; //for drm file use.
    if (drmClient.canHandleFd(fd)) {
        strm  = CreateDrmInputStreamAdaptor(fd);
        if (strm == NULL) {
            close(fd);
            delete[] fileData;fileData = NULL;
            return 0;
        }
        int count =  strm->read(fileData,fileLength);
        if (count <= 0) {
            delete[] fileData;fileData = NULL;
            //strm->unref();
            strm = NULL;
            close(fd);
            return 0;
        }
    } else {
        if( lseek64(fd, 0, SEEK_SET) == -1){//come the file header
            delete[] fileData;fileData = NULL;
            close(fd);
            return 0;
        }
        int count =  read(fd, fileData, fileLength);
        if(count <= 0){
            close(fd);
            delete[] fileData;fileData = NULL;
            return 0;
        }
    }

    close(fd);//now don't need this file again
    jbyteArray jBuffer = env->NewByteArray(fileLength);
    env->SetByteArrayRegion(jBuffer,0,fileLength,(jbyte*)fileData);
    delete[] fileData;fileData = NULL;
    AutoJavaByteArray   ar(env, jBuffer);
    SkMovie* moov = SkMovie::DecodeMemory(ar.ptr(), fileLength);

    if (NULL == moov) {
        if(strm != NULL){
            //strm->unref();
            strm = NULL;
        }
        return 0;
    }
    jobject  jMovie =  create_jmovie(env, moov);
    if (jMovie != NULL && strm != NULL) {
        strm->consumeRight();
        //strm->unref();
        strm = NULL;
    }

    return jMovie;
}
//[FEATURE]-Add-END by TCTNB.Yang.Hu TCT DRM solution

static jobject movie_decodeByteArray(JNIEnv* env, jobject clazz,
                                     jbyteArray byteArray,
                                     jint offset, jint length) {

    NPE_CHECK_RETURN_ZERO(env, byteArray);

    int totalLength = env->GetArrayLength(byteArray);
    if ((offset | length) < 0 || offset + length > totalLength) {
        doThrowAIOOBE(env);
        return 0;
    }

    AutoJavaByteArray   ar(env, byteArray);
    SkMovie* moov = SkMovie::DecodeMemory(ar.ptr() + offset, length);
    return create_jmovie(env, moov);
}

static void movie_destructor(JNIEnv* env, jobject, jlong movieHandle) {
    SkMovie* movie = (SkMovie*) movieHandle;
    delete movie;
}

//////////////////////////////////////////////////////////////////////////////////////////////

static const JNINativeMethod gMethods[] = {
    {   "width",    "()I",  (void*)movie_width  },
    {   "height",   "()I",  (void*)movie_height  },
    {   "isOpaque", "()Z",  (void*)movie_isOpaque  },
    {   "duration", "()I",  (void*)movie_duration  },
    {   "setTime",  "(I)Z", (void*)movie_setTime  },
    {   "nDraw",    "(JFFJ)V",
                            (void*)movie_draw  },
    { "nativeDecodeAsset", "(J)Landroid/graphics/Movie;",
                            (void*)movie_decodeAsset },
    { "nativeDecodeStream", "(Ljava/io/InputStream;)Landroid/graphics/Movie;",
                            (void*)movie_decodeStream },
    { "nativeDestructor","(J)V", (void*)movie_destructor },
    { "decodeByteArray", "([BII)Landroid/graphics/Movie;",
                            (void*)movie_decodeByteArray },
    //FEATURE]-Add-BEGIN by TCTNB.Yang.Hu,2015/9/30, task-572426 TCT DRM solution
    { "mDrmdecodeFile","(Ljava/lang/String;)Landroid/graphics/Movie;",
                            (void*)movie_mDrmdecodeFile },
    { "mDrmdecodeFileDescriptor","(Ljava/io/FileDescriptor;)Landroid/graphics/Movie;",
                            (void*)movie_mDrmdecodeFileDescriptor },
    //[FEATURE]-Add-END by TCTNB.Yang.Hu TCT DRM solution
};

int register_android_graphics_Movie(JNIEnv* env)
{
    gMovie_class = android::FindClassOrDie(env, "android/graphics/Movie");
    gMovie_class = android::MakeGlobalRefOrDie(env, gMovie_class);

    gMovie_constructorMethodID = android::GetMethodIDOrDie(env, gMovie_class, "<init>", "(J)V");

    gMovie_nativeInstanceID = android::GetFieldIDOrDie(env, gMovie_class, "mNativeMovie", "J");

    return android::RegisterMethodsOrDie(env, "android/graphics/Movie", gMethods, NELEM(gMethods));
}
