package org.ifaa.android.manager;

import android.content.Context;
/**
 * Communication channel from client to the alipay fingerprint service.
 * @hide
 */
 interface IAlipayFingerprintService {
    byte[] processCmd(in byte [] param);
 }