/* Copyright (C) 2016 Tcl Corporation Limited */
package org.ifaa.android.manager;

import org.ifaa.android.manager.IFAAManagerImpl;
import org.ifaa.android.manager.IFAAManager;
import android.content.Context;
import android.util.Log;

/**
 * Created by sunchunzhi on 11/2/16.
 */
public class IFAAManagerFactory {
    protected  static final String TAG = "IFAAManagerFactory";

    public static IFAAManager getIFAAManager(Context context, int authType){
        Log.i(TAG,"getIFAAManager...");
        if (IFAAManager.AUTH_TYPE_FINGERPRINT == authType) {
            Log.d(TAG,"authType is AUTH_TYPE_FINGERPRINT");
            return new IFAAManagerImpl(context);
        } else if (IFAAManager.AUTH_TYPE_IRIS == authType) {
            Log.d(TAG,"auth type is AUTH_TYPE_IRIS");
            return null;
        } else {
            Log.d(TAG,"auth type is null");
            return null;
        }
    }
}
