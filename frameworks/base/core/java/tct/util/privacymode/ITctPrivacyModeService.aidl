
package tct.util.privacymode;

import android.os.IBinder;

/**
 * interface that enter privacy mode or owner mode
 *
 * {@hide}
 */
interface ITctPrivacyModeService
{
    void enterPrivacyMode(boolean isPrivacy, IBinder resultListener);
}

