/******************************************************************************/
/*                                                               Date:09/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  caixia.chen                                                     */
/*  Email  :                                                                  */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/07/2016|     caixia.chen      |     task 2854067     |Private mode      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package tct.util.privacymode;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.hardware.FileLock;
import android.hardware.FlOperationResult;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;

/**
 *
 * @author chencaixia
 * @hide
 */
public class TctPrivacyModeHelper {
    private static final String PACKAGE_NAME = "package_name";
    private static final String TCT_IS_PRIVATE = "tct_is_private";
    private static final Uri PRIVATE_APP_CONTENT_URI = Uri.parse("content://com.tct.privacymode.provider/privacy_apps");
    private static final Uri FILE_URI = MediaStore.Files.getContentUri("external");

    private Context mContext;

    public interface OnResultListener {
        void onResult(int result);
    }

    public static TctPrivacyModeHelper createHelper(Context context) {
        return new TctPrivacyModeHelper(context);
    }

    private TctPrivacyModeHelper(Context context) {
        mContext = context;
    }

    public void enterPrivacyMode(boolean isPrivacy, OnResultListener resultListener) {
        if (!isPrivacyModeEnable()) {
            return;
        }
        TctPrivacyModeHelperImpl impl = new TctPrivacyModeHelperImpl(mContext);
        impl.enterPrivacyMode(isPrivacy, resultListener);
    }

    public String getPrivacyPassword() {
        if (isPrivacyModeEnable()) {
            return Settings.System.getString(mContext.getContentResolver(), Settings.System.PRIVACY_MODE_PASSWORD);
        } else {
            return "";
        }
    }

    public void setPrivacyPassword(String password) {
        Settings.System.putString(mContext.getContentResolver(), Settings.System.PRIVACY_MODE_PASSWORD, password);
    }

    public String getNormalPassword() {
        return Settings.System.getString(mContext.getContentResolver(), Settings.System.NORMAL_MODE_PASSWORD);
    }

    public void saveNormalPassword(String password) {
        Settings.System.putString(mContext.getContentResolver(), Settings.System.NORMAL_MODE_PASSWORD, password);
    }

    public boolean isInPrivacyMode() {
        int mode = Settings.System.getInt(mContext.getContentResolver(), Settings.System.CURRENT_PHONE_MODE, 0);
        return (mode == 1);
    }

    public boolean isPrivacyModeEnable() {
        int mode = Settings.System.getInt(mContext.getContentResolver(), Settings.System.PRIVACY_MODE_ON, 0);
        return (mode == 1);
    }

    public boolean shouldHidePrivateContents() {
        return (isPrivacyModeEnable() && !isInPrivacyMode());
    }

    public boolean enterPrivacyMode(boolean isPrivacy) {
        if (!isPrivacyModeEnable()) {
            return false;
        }
        TctPrivacyModeHelperImpl impl = new TctPrivacyModeHelperImpl(mContext);
        impl.enterPrivacyMode(isPrivacy, null);
        return true;
    }

    public long getSwitchToNormalModeTimestamp() {
        return Settings.System.getLong(mContext.getContentResolver(), Settings.System.SWITCH_TO_NORMAL_MODE_TIMESTAMP, 0);
    }

    public boolean isPrivateApp(String packageName) {
        boolean isPrivate = false;
        ContentResolver cr = mContext.getContentResolver();
        Cursor c = cr.query(PRIVATE_APP_CONTENT_URI, null, PACKAGE_NAME + "=?", new String[]{packageName}, null);
        if (c != null) {
            try {
                isPrivate = (c.getCount() > 0) ? true : false;
            } finally {
                c.close();
            }
        }
        return isPrivate;
    }

    public void setAppPrivateFlag(String packageName, boolean isPrivate) {
        ContentResolver cr = mContext.getContentResolver();
        if (isPrivate) {
            ContentValues values = new ContentValues(1);
            values.put(PACKAGE_NAME, packageName);
            cr.insert(PRIVATE_APP_CONTENT_URI, values);
        } else {
            cr.delete(PRIVATE_APP_CONTENT_URI, PACKAGE_NAME + "=?", new String[]{packageName});
        }
    }

    public ArrayList<String> getAllPrivateApp() {
        ArrayList<String> packageNameList = new ArrayList<String>();
        ContentResolver cr = mContext.getContentResolver();
        Cursor c = cr.query(PRIVATE_APP_CONTENT_URI, new String[] {PACKAGE_NAME}, null, null, null);
        if (c != null) {
            try {
                while (c.moveToNext()) {
                    packageNameList.add(c.getString(0));
                }
            } finally {
                c.close();
            }
        }
        return packageNameList;
    }

    public void setFilePrivateFlag(String packageName, ArrayList<String> data, boolean isPrivate) {
        if (data.size() < 1) {
            return;
        }
        FileLock mFileLock = new FileLock();
        ContentResolver cr = mContext.getContentResolver();
        ContentValues values = new ContentValues(1);
        values.put(TCT_IS_PRIVATE, isPrivate ? 1 : 0);

        int MAX_OPS = 200;
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        for (int i = 0; i < data.size(); i++) {
            String path = data.get(i);
            try {
                if (isPrivate) {
                    mFileLock.lockFile(path);
                } else {
                    mFileLock.unLockFile(path);
                }
            } catch (Exception e) {
                Log.i("ccxccx","hide/show file = " + path + ", isPrivate = " + isPrivate + ", " + e);
            }
            ops.add(ContentProviderOperation.newUpdate(FILE_URI)
                    .withSelection(MediaStore.MediaColumns.DATA + "=?", new String[]{path})
                    .withValues(values)
                    .build());
            if (ops.size() > MAX_OPS) {
                try {
                    cr.applyBatch(MediaStore.AUTHORITY, ops);
                } catch (Exception e) {
                    Log.i("ccxccx","update db isPrivate1 = " + isPrivate + ", error = " + e);
                }
                ops.clear();
            }
        }
        if (ops.size() > 0) {
            try {
                cr.applyBatch(MediaStore.AUTHORITY, ops);
            } catch (Exception e) {
                Log.i("ccxccx","update db isPrivate2 = " + isPrivate + ", error = " + e);
            }
        }
    }

    public boolean isPrivateFile(String packageName, String data) {
        boolean isPrivate = false;
        FileLock mFileLock = new FileLock();
        FlOperationResult flor = mFileLock.checkFileState(data);
        if (flor != null && (flor.getExecResult() == 0)) {
            isPrivate = flor.isFileLocked();
        }
        return isPrivate;
    }

    public boolean isPrivateModeFeatureOn() {
        return mContext.getResources().getBoolean(com.android.internal.R.bool.feature_tctfw_privatemode_on);
    }

    public boolean autoExitPrivateMode() {
        return (Settings.System.getInt(mContext.getContentResolver(), Settings.System.AUTO_EXIT_PRIVATE_MODE, 0) == 1);
    }
}
