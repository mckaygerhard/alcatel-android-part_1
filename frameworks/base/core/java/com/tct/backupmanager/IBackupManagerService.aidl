/* Copyright Statement:
 *
 */

/**
 * aidl file : IBackupManagerService.aidl
 * This file contains definitions of functions which are exposed by service
 */
package com.tct.backupmanager;

import com.tct.backupmanager.IBackupManagerServiceCallback;

interface IBackupManagerService {
    void start();
    boolean beginBack(in String[] files, String dst, int type);
    boolean beginRestore(in String[] files, String dst, int type);
    void pause();
    void registerCallback(IBackupManagerServiceCallback cb);
    void unregisterCallback(IBackupManagerServiceCallback cb);
}
