package com.tct.rom;

import android.content.Context;
import android.os.SystemProperties;

/**
 * Utils for rom
 */
public class RomUtils {

    /**
     * Check whether rom is enabled.<br>
     * @param context context to get resource. If you pass null, please make sure it is not the first time it called in the process.
     * @return true: enabled. false: disabled
     */
    public static boolean isRomEnabled(Context context) {

        return isRomEnabled();
    }
    /**
     * Check whether rom is enabled.<br>
     * NOTE: This method should not be used if {@link #isRomEnabled(Context)} is not called before in this process.
     * @return true: enabled. false: disabled
     */
    public static boolean isRomEnabled() {
        return SystemProperties.getBoolean("ro.tctfw.rom.enabled",false);
    }
}
