package com.tcl.enc;

import java.io.UnsupportedEncodingException;

public class TclEncrypt {

    // TODO : remember to protect this KEY !!! *IMPORTANT* !
    private static final String CRYPTOGRAPHY_BLOWFISH_INIT_KEY = "2@c%er!ks980%s^by20oiusc!o=2mh@3^osis22loip#21nch%6!23hh";

    // Wechat multi-notification prefix
    public static final String MULTI_NOTIFICATION_PREFIX_WECHAT = ": ";

    // You can also define the other cryptography type
    public static final int CRYPTOGRAPHY_TYPE_BLOWFISH = 0x1000;

    private static TclEncrypt mEnc = null;

    private Blowfish mFish = null;

    private TclEncrypt() {

    }

    public static TclEncrypt getInstance() {
        if (null == mEnc) {
            synchronized (TclEncrypt.class) {
                if (null == mEnc) {
                    mEnc = new TclEncrypt();
                }
            }
        }

        return mEnc;
    }

    /*
     * To encrypt text information
     */
    public String encrypt(int type, String plaintext) {
        if (null == plaintext || plaintext.equals("")) {
            System.out.println("TCL, Invalid content to encrypt.");
            return "";
        }

        if (plaintext.endsWith(Blowfish.SIGN)) {
            if (!plaintext.startsWith(Blowfish.SIGN)) {
                plaintext = decrypt(CRYPTOGRAPHY_TYPE_BLOWFISH, plaintext);
                System.out.println("Mixin, decrypt before encyrpt, plaintext : " + plaintext);
            } else {
                plaintext = decrypt(CRYPTOGRAPHY_TYPE_BLOWFISH, plaintext);
                if (plaintext.contains(Blowfish.SIGN)) {
                    System.out.println("Mixin, error, can not dec, txt:" + plaintext);
                    return plaintext;
                }
            }
        }

        String res = "";
        if (type == CRYPTOGRAPHY_TYPE_BLOWFISH) {
            if (null == mFish) {
                mFish = new Blowfish(CRYPTOGRAPHY_BLOWFISH_INIT_KEY);
            }

            try {
                res = mFish.encrypt(plaintext);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("TCL, ERROR in encrypt text");
                res = plaintext;
            } finally {
                return res;
            }
        }

        System.out.println("TCL, Invalid cryptography type to encrypt.");
        return res;
    }

    /*
     * To decrypt text information
     */
    public String decrypt(int type, String cyphertext) {
        if (null == cyphertext || cyphertext.equals("")) {
            System.out.println("TCL, Invalid content to decrypt.");
            return "";
        }

        cyphertext = correctString(cyphertext);
        String backup = cyphertext;

        if (cyphertext.contains(MULTI_NOTIFICATION_PREFIX_WECHAT)) {
            String[] cyphers = cyphertext.split(MULTI_NOTIFICATION_PREFIX_WECHAT);
            cyphertext = cyphers[cyphers.length - 1];
        }

        String plaintext_prefix = "";
        String plaintext_suffix = "";
        if (!cyphertext.startsWith(Blowfish.SIGN)) {

            String[] ss = cyphertext.split(Blowfish.SIGN);
            if (cyphertext.endsWith(Blowfish.SIGN)) {
                if (ss.length == 2) {
                    cyphertext = ss[1];
                    plaintext_prefix = ss[0];
                } else {
                    return backup;
                }
            } else {

                if (ss.length == 3){
                    cyphertext = ss[1];
                    plaintext_prefix = ss[0];
                    plaintext_suffix = ss[2];
                } else {
                    return backup;
                }
            }
        } else if (cyphertext.startsWith(Blowfish.SIGN) && !cyphertext.endsWith(Blowfish.SIGN)) {
            String[] ss = cyphertext.split(Blowfish.SIGN);
            if (ss.length == 3) {
                if (("").equals(ss[0])) {
                    cyphertext = ss[1];
                    plaintext_suffix = ss[2];
                }
            }
        }

        if (cyphertext.equals("")) {
            return backup;
        }

        String res = "";
        if (type == CRYPTOGRAPHY_TYPE_BLOWFISH) {
            if (null == mFish) {
                mFish = new Blowfish(CRYPTOGRAPHY_BLOWFISH_INIT_KEY);
            }
            System.out.println("Mixin, plaintext_prefix : " + plaintext_prefix + " , cyphertext : " + cyphertext);

            try {
                if (cyphertext.contains("\n")) {
                    String[] txts = cyphertext.split("\n");
                    for (int i=0; i<txts.length; i++) {
                        if (null != txts[i] && !(txts[i]).equals("")) {
                            res += mFish.decrypt(txts[i]);

                            if (i < (txts.length - 1)) {
                                res += "\n";
                            }
                        }
                    }
                } else {
                    res = mFish.decrypt(cyphertext);
                }

                System.out.println("Mixin, AD, plaintext_prefix : " + plaintext_prefix + " , res : " + res);

                if (!plaintext_prefix.equals("")) {
                    res = plaintext_prefix + res;
                }
                if (!plaintext_suffix.equals("")) {
                    res = res + plaintext_suffix;
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Mixin, ERROR in decrypt text");
                res = "Error";
            } finally {
                return res;
            }
        }

        System.out.println("Mixin, Invalid cryptography type to decrypt.");
        return res;
    }

    /*
     * To take a judgement for whether be encrypted, for text
     */
    public boolean isEncrypted(String text, boolean inLooseMode, boolean isEditing) {
        if (null == text || text.equals("")) {
            return false;
        }

        if (!text.contains(Blowfish.SIGN)) {
            return false;
        }

        if (inLooseMode) {
            if (text.startsWith(Blowfish.SIGN)) {
                return true;
            }
        }

        if (isEditing) {
            if (!text.startsWith(Blowfish.SIGN) && !text.endsWith(Blowfish.SIGN)) {
                if (text.contains(Blowfish.SIGN)) {
                    return true;
                }
            }
        }

        if (text.endsWith(Blowfish.SIGN)) {

            /*
            if (!text.startsWith(Blowfish.SIGN)) {
                if (isEditing) {
                    return false;
                } else {
                    return true;
                }
            }*/

            return true;
        }

        // default
        return false;
    }

    /*
     * To encrypt binary information
     */
    public byte[] encrypt(int type, byte[] palintcontent) {
        // NOT BE IMPLEMENTED NOW
        return null;
    }

    /*
     * To decrypt binary information
     */
    public byte[] decrypt(int type, byte[] cyphercontent) {
        // NOT BE IMPLEMENTED NOW
        return null;
    }

    /*
     * To take a judgement for whether be encrypted, for binary
     */
    public boolean isEncrypted(byte[] content) {
        // NOT BE IMPLEMENTED NOW
        // default
        return false;
    }

    // can not support too many char sets, performance is bad.
    private final static String[] CHARSETS = {
        "iso-8859-1",
        "utf-8",
        "big5",
        "us-ascii",
    };

    public String correctString(String str) {
        if (str.contains(Blowfish.SIGN)) {
            return str;
        }

        String set = "null";
        String res = str;
        int index = 0;

        do {
            try {
                String tmp = new String(str.getBytes(CHARSETS[index]));
                if (null != tmp && tmp.contains(Blowfish.SIGN)) {
                    set = CHARSETS[index];
                    res = tmp;
                    break;
                }

                index++;
            } catch (UnsupportedEncodingException e) {
                // do nothing
            }
        } while (index < CHARSETS.length);

        System.out.println("Mixin, CHARSET:" + set);

        return res;       
    }

}
