package com.cmx.cmplus;

import com.cmx.cmplus.ContainerInfo;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.content.pm.ApplicationInfo;
import android.content.Intent;

/**
 *
 *  {@hide}
 */

interface ISmartContainerManager {
    ContainerInfo createVirtualBox(String name, int type);
    ContainerInfo getContainerInfo(int id);

    List<ContainerInfo> getAllVirtualBoxes();

    boolean removeVirtualBox(int id);

    void setIcon(int id, in Bitmap bmp);
    Bitmap getIcon(int id);
    void setContainerName(int id, String name);
    void setAppEnabled(int id, String packageName, boolean enabled);

    int getBoxIdForPackage(String pkg);
    int getForwardUserIdForPackage(int userId, String pkg, int callingUid);
    boolean isPackageInBox(int userId, String pkg);

    boolean canAddPackageToBox(int boxid, String pkg);

    boolean enableShareAcrossBoxWithUserId(String path, int srcUserId, int targetUserId);
    String enableShareAcrossBox(String path);
    boolean disableShareAcrossBox(String path);
    int checkActivityIntentResult(inout Intent intent);
    boolean preparePathForCrossBox(String filePath, int srcUserId, int targetUserId);

    //For PMS permission check only
    int checkPermission(String permName, String pkgName, int userId);
    int checkUidPermission(String permName, int uid) ;

    int getCloneUserIdForPackage(int userId, String pkg);
    boolean isPackageCloned(String pkg);
    void createCloneForPackage(String pkg);
    void deleteCloneForPackage(String pkg);
    boolean canPackageBeCloned(String pkg);

    boolean isBoxVisible(int id1, int id2);

    void setFeatureStatus(String feature, boolean enable);

    CharSequence loadLabel(String packageName, in ApplicationInfo info, CharSequence label);

    //For ResolverActivity
    void processIntentForClone(inout Intent intent);
    void remarkIntentForClone(inout Intent intent);

    void markIntentRestricted(inout Intent intent, boolean restricted);

    boolean isCloneEnabling();

    List<String> getDefaultCloneablePackages();
    List<String> getOEMCloneablePackages();
    List<String> getIntentRestrictedPackages();

    void setCurrUid(int uid);
    int getOldUid();
}
