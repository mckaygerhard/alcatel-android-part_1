/* Copyright (C) 2016 Tcl Corporation Limited */
package com.cmx.cmplus;

import android.os.SystemProperties;
import android.util.Log;

import java.util.HashSet;

/**
 * @hide
 */
public class SmartContainerConfig {
    private static String FEATURE = "";
    private static String TAG = "SmartContainerConfig";

    public static boolean WITH_OUT_ALL;
    public static boolean WITH_OUT_VIRTUAL_BOX;

    public static boolean WITH_OUT_CROSS_BOX_SHARE;
    public static boolean WITH_OUT_APP_CLONE;

    public static String CONFIG_PROPERTY = "persist.sys.cmplus.disabled";

    static  {
        init();
    }

    public static void init(){
        FEATURE = SystemProperties.get(CONFIG_PROPERTY, "");
        Log.d(TAG, "Configured features : " + FEATURE);
        WITH_OUT_ALL = FEATURE.contains("all");
        WITH_OUT_VIRTUAL_BOX = WITH_OUT_ALL || FEATURE.contains("virtualbox");

        WITH_OUT_APP_CLONE = WITH_OUT_VIRTUAL_BOX || FEATURE.contains("clone");
        WITH_OUT_CROSS_BOX_SHARE = WITH_OUT_VIRTUAL_BOX || FEATURE.contains("cross-box-share");
    }

    public static void turnOff() {
        WITH_OUT_ALL = true;
        WITH_OUT_VIRTUAL_BOX = true;
        WITH_OUT_CROSS_BOX_SHARE = true;
        WITH_OUT_APP_CLONE = true;
    }
}
