/* Copyright (C) 2016 Tcl Corporation Limited */
package com.cmx.cmplus;

import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;
import android.util.Singleton;

/**
 * Quick interface to get ISmartManagerService
 */
public class SmartContainerManagerNative {
    private static String TAG = "SmartContainerManagerNative";
    private static String SMART_CONTAINER_SERVICE = "smart_container";

    private static final Singleton<ISmartContainerManager> gDefault = new Singleton<ISmartContainerManager>() {
        protected ISmartContainerManager create() {
            ISmartContainerManager i = null;
            try {
                i = ISmartContainerManager.Stub.asInterface(
                        ServiceManager.getService(SMART_CONTAINER_SERVICE));
            } catch (Exception e){
                if (SystemProperties.getInt("sys.boot_completed", 0) == 1) {
                    SmartContainerConfig.turnOff();
                }
                Log.e(TAG, "Error to get service " + SMART_CONTAINER_SERVICE);
            }
            return i;
        }
    };
    public static ISmartContainerManager get(){
        return gDefault.get();
    }
}
