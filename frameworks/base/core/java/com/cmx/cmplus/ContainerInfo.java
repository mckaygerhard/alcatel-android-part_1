/* Copyright (C) 2016 Tcl Corporation Limited */
package com.cmx.cmplus;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Info of Container
 */
public class ContainerInfo implements Parcelable{


    /**
     * Sent when a container was created
     * Please use CloneHelper.ACTION_CONTAINER_CREATED instead
     */
    public static final String ACTION_CONTAINER_CREATED = "com.cmx.cmplus.ACTION_CONTAINER_CREATED";
    /**
     * Extra container info
     * Please use CloneHelper.EXTRA_CONTAINER_INFO instead
     */
    public static final String EXTRA_CONTAINER_INFO = "com.cmx.cmplus.EXTRA_CONTAINER_INFO";

    /**
     * CONTAINER TYPE
     */
    public static final int TYPE_VIRTUAL_BOX = 0;


    public static final int TYPE_VIRTUAL_BOX_MAX = 1000;

    /* Additional user info flag mask */
    public static final int CI_FLAG_VIRTUAL_BOX = 0x40000000;

    /**
     * MAX number of Virtual box
     */
    public static final int MAX_VIRTUAL_BOX_NUM = 5;

    public int id;
    public int type;
    public String name;

    //extra info for box, space ...
    public Bundle extra;

    public ContainerInfo(Parcel parcel){
        id = parcel.readInt();
        type = parcel.readInt();
        name = parcel.readString();
        extra = parcel.readBundle();
    }

    public ContainerInfo(int id, int type, String name){
        this.id = id;
        this.type = type;
        this.name = name;
        this.extra = new Bundle();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(type);
        parcel.writeString(name);
        parcel.writeBundle(extra);
    }

    public static final Parcelable.Creator<ContainerInfo> CREATOR
            = new Parcelable.Creator<ContainerInfo>() {
        public ContainerInfo createFromParcel(Parcel source) {
            return new ContainerInfo(source);
        }
        public ContainerInfo[] newArray(int size) {
            return new ContainerInfo[size];
        }
    };

    public static boolean isVirtualBox(int type) {
        return type >= ContainerInfo.TYPE_VIRTUAL_BOX && type < ContainerInfo.TYPE_VIRTUAL_BOX_MAX ;
    }

    public boolean isVirtualBox() {
        return type >= ContainerInfo.TYPE_VIRTUAL_BOX && type < ContainerInfo.TYPE_VIRTUAL_BOX_MAX ;
    }
}
