/*willab*/
package android.hardware;

/**
 * @hide
 */
public class FlOperationResult {
    int retVal; //Used to record operation return value
    boolean isLocked; //Used to record flag of file status, locked or not

	public boolean isFileLocked() {
        return isLocked;
    }

    public int getExecResult() {
        return retVal;
    }
}
