/* Copyright (C) 2016 Tcl Corporation Limited */
/* ***************************************************************************/
/*                                                       Date : Feb 2, 2015  */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2015 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Hanwu.xie (XHANWU)                                                    */
/*   Role :    Telecom Leader                                                */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : AttrNameManager.java                                                */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*02/02/15 | hanwu.xie | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package android.app;

import android.os.RemoteException;
import android.util.Slog;
import android.os.ServiceManager;
import android.content.Context;
import android.os.IBinder;

/**
 * System private API for talking with the stringID manager service. {@hide
 * }
 */
public class AttrNameManager {

    private static final String TAG = "AttrNameManager";
    private final IAttrNameManager mService;

    static final Object sInstanceSync = new Object();
    private static AttrNameManager sInstance;

    /**
     * Get an AttrNameManager instance (create one if necessary).
     *
     * @param context Context in which this manager operates.
     * @hide
     */
    public static AttrNameManager getInstance(Context context) {
        synchronized (sInstanceSync) {
            if (sInstance == null) {
                sInstance = (AttrNameManager) context
                        .getSystemService(Context.JRD_ATTR_NAME_SERVICE);
            }
        }
        return sInstance;
    }

    public AttrNameManager(IAttrNameManager service) {
        mService = service;
        Slog.i(TAG, "<<<<<<<<<mService = " + mService);
    }

    /*
     * public void addTypedArrayStringInfo(int indexOfTypedArray){ try {
     * if(mService != null) {
     * mService.addTypedArrayStringInfo(indexOfTypedArray); } } catch (Exception
     * e) { // TODO: handle exception } } public void addStringInfo(String
     * packageName, String attrName, String attrValue){ try { if(mService !=
     * null) { mService.addStringInfo(packageName,attrName,attrValue); } } catch
     * (Exception e) { // TODO: handle exception } }
     */
    public void updateStringInfo(String key, String attrName, String attrValue, String packName,
            int zoneWidth, int zoneHeight, int textSize, int bold, int italic, int isEllipse,
            String currLang) {
        try {
            if (mService != null) {
                mService.updateStringInfo(key, attrName, attrValue, packName, zoneWidth,
                        zoneHeight, textSize, bold, italic, isEllipse, currLang);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /*
     * public String getStringInfo(String attrValue) { String attrName = null;
     * try { if(mService != null) { attrName =
     * mService.getStringInfo(attrValue); } } catch (Exception e) { // TODO:
     * handle exception } return attrName; }
     */
    public void clearAllCache() {
        try {
            if (mService != null) {
                mService.clearAllCache();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public boolean isAttrNameServiceStart() {
        boolean isStart = false;
        try {
            if (mService != null) {
                isStart = mService.isAttrNameServiceStart();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return isStart;
    }

    public void setAttrNameServiceStart(boolean flag) {
        try {
            if (mService != null) {
                mService.setAttrNameServiceStart(flag);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public boolean isShowId() {
        boolean isShowId = false;
        try {
            if (mService != null) {
                isShowId = mService.isShowId();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return isShowId;
    }

    public void setShowId(boolean isShow) {
        try {
            if (mService != null) {
                mService.setShowId(isShow);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public boolean isShowStringAttribute() {
        boolean isShowAttribute = false;
        try {
            if (mService != null) {
                isShowAttribute = mService.isShowStringAttribute();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return isShowAttribute;
    }

    public void setShowStringAttribute(boolean isShow) {
        try {
            if (mService != null) {
                mService.setShowStringAttribute(isShow);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public StringMapParcelable getStingMap() {

        StringMapParcelable strMap = null;
        try {
            if (mService != null) {
                strMap = mService.getStingMap();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

        return strMap;
    }

    public void showStringAttributeInfo(String attrName, String attrValue, String packName,
            int zoneWidth, int zoneHeight, int textSize, int bold, int italic) {
        try {
            if (mService != null) {
                mService.showStringAttributeInfo(attrName, attrValue, packName, zoneWidth,
                        zoneHeight, textSize, bold, italic);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void rebootPhone() {
        try {
            if (mService != null) {
                mService.rebootPhone();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
