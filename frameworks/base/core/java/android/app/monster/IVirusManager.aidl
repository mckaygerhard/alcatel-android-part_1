package android.app.monster;
  
/**
 * {@hide}
 */
interface IVirusManager  
{  
    String getValue(String name);  
} 