/* ***************************************************************************/
/*                                                       Date : Feb 2, 2015  */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2015 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Hanwu.xie (XHANWU)                                            */
/*   Role :    Telecom Leader                                                */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : IAttrNameManager.java                                        */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*02/02/15 | hanwu.xie | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/
package android.app;

import android.app.StringMapParcelable;

/**
 * System private API for talking with the stringID manager service.
 *
 * {@hide}
 */
interface IAttrNameManager {
     //void addTypedArrayStringInfo(int indexOfTypedArray);
     //void addStringInfo(String packageName, String attrName, String attrValue);
     void updateStringInfo(String key, String attrName, String attrValue, String packName,
     int zoneWidth,int zoneHeight,int textSize,int bold, int italic, int isEllipse, String currLang);
     //String getStringInfo(String attrValue);
     void clearAllCache();
     boolean isAttrNameServiceStart();
     void setAttrNameServiceStart(boolean flag);
     boolean isShowId();
     void setShowId(boolean isShow);
     boolean isShowStringAttribute();
     void setShowStringAttribute(boolean isShow);
     StringMapParcelable getStingMap();
     void showStringAttributeInfo(String attrName, String attrValue,String packName,
     int zoneWidth,int zoneHeight,int textSize,int bold, int italic);
     void rebootPhone();
}
