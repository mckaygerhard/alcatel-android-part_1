package android.os;

/**
 * @hide
 */
interface ISsvService {
    String getCurrentFocMccAlias();
    String getSimGid();
    void unLock(String mccmnc, String spn, String gid);
    int getTransition();
    String getSimMccMnc();
    String getSimSpn();
    String getPreSimSpn();
    void setPreSimSpn(String spn);
    String getPrevMccMnc();
    String getEveryPrevMccMnc();
    String getLangMccMnc();
    void setPrevMccMnc(String mccmnc);
    void setEveryPrevMccMnc(String mccmnc);
    void setLangMccMnc(String mccmnc);
    void setCurrentState(int state);
    void setButtonNoNum(String btnnonum);
    String getButtonNoNum();
    int getCurrentState();
    boolean isOpSim();
    boolean isMccMncChanged();
    boolean isAppNeedChange(String app);
    void setSwitchEnable(boolean enable);
    void appChangeComplete(String app);
    boolean isSimLock();
    void unlock(String mccmnc, String spn);
    boolean hasPinLock();
    boolean getSsvKeyGuardState();
    boolean isSsvEnable();
    void setSsvKeyGuardState(boolean state);
    void initSsvSettings();
    void updateMinMatchField(); //[FEATURE]-Add-by TSCD.Xian.Wang,CR-905066,03/01/2015
    boolean getIsFirstRun();
    void setIsFirstRun(boolean b);
    void updateLogo();
}
