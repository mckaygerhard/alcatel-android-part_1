// Architecture team modificaiton only
// ArchiNO:(011.tct_log)

package android.util;

/**
 * @hide
 */
public final class TctSLog {
    private TctSLog(){}

    public static int d(String tag, String msg) {
        if (TctLog.debugEnable) {
            return Log.println_native(Log.LOG_ID_SYSTEM, Log.DEBUG, tag, msg);
        }
        return  0;
    }

    public static int i(String tag, String msg) {
        if (TctLog.infoEnable) {
            return Log.println_native(Log.LOG_ID_SYSTEM, Log.INFO, tag, msg);
        }
        return  0;
    }

    public static int w(String tag, String msg) {
        if (TctLog.warnEnable) {
            return Log.println_native(Log.LOG_ID_SYSTEM, Log.WARN, tag, msg);
        }
        return  0;
    }

    public static int e(String tag, String msg) {
        if (TctLog.errorEnable) {
            return Log.println_native(Log.LOG_ID_SYSTEM, Log.ERROR, tag, msg);
        }
        return  0;
    }


    public static int d(String tag, String msg, Throwable tr) {
        return d(tag, msg + '\n' + Log.getStackTraceString(tr));
    }

    public static int i(String tag, String msg, Throwable tr) {
        return i(tag, msg + '\n' + Log.getStackTraceString(tr));
    }

    public static int w(String tag, String msg, Throwable tr) {
        return w(tag, msg + '\n' + Log.getStackTraceString(tr));
    }

    public static int w(String tag, Throwable tr) {
        return w(tag, Log.getStackTraceString(tr));
    }

    public static int e(String tag, String msg, Throwable tr) {
        return e(tag, msg + '\n' + Log.getStackTraceString(tr));
    }
}
