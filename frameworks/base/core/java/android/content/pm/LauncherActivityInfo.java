/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.content.pm;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;
import android.os.UserManager;
import android.util.DisplayMetrics;
import android.util.Log;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2521558
//Porting STK app:528162
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.util.TctLog;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

/**
 * A representation of an activity that can belong to this user or a managed
 * profile associated with this user. It can be used to query the label, icon
 * and badged icon for the activity.
 */
public class LauncherActivityInfo {
    private static final String TAG = "LauncherActivityInfo";

    private final PackageManager mPm;

    private ActivityInfo mActivityInfo;
    private ComponentName mComponentName;
    private UserHandle mUser;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2521558
//Porting STK app:528460
    private Context mContext;

    /**
     * The Vivo Mcc&Mnc table
     *
     * @hide
     */
    private static final String[] mVivoSimMccMnc = new String[] {
        "72406",
        "72410",
        "72411",
        "72423"
    };

    /**
     * The IDEA Mcc&Mnc table
     *
     * @hide
     */
    private static final String[] mIdeaSimMccMnc = new String[] {
        "40407", "40404", "40424", "40412", "40482", "40419", "40478",
        "40422", "405799", "40487", "40489", "40456", "40570", "405852", "405850", "40444", "40414",
        "405848", "405853", "405845", "405849", "405846", "40411", "40430", "40443", "40484", "40492",
        "40552", "40416", "40556", "40470", "40445", "40551"
    };

    /**
     * The Tim Mcc&Mnc table
     *
     * @hide
     */
    private static final String[] mTimSimMccMnc = new String[] {
        "72402",
        "72403",
        "72404"
    };

    /**
     * The OI Mcc&Mnc table
     *
     * @hide
     */
    private static final String[] mOiSimMccMnc = new String[] {
        "72416",
        "72424",
        "72431"
    };

    private static final String[] mClaroSimMccMnc = new String[] {
        "72405"
    };

    /**
     * whether insert Vivo SIM
     *
     * @hide
     */
    public boolean isVivoSim(String mccmnc) {
        for (String mMncMcc:mVivoSimMccMnc)
            if (mccmnc != null && mccmnc.equals(mMncMcc)){
            TctLog.d(TAG, "Vivo sim card");
            return true;
        }
        return false;
    }

    /**
     * whether insert Tim SIM
     *
     * @hide
     */
    public boolean isTimSim(String mccmnc) {
        for (String mMncMcc : mTimSimMccMnc)
            if (mccmnc != null && mccmnc.equals(mMncMcc)) {
                TctLog.d(TAG, "Tim sim card");
                return true;
            }
        return false;
    }

    /**
     * whether insert OI SIM
     *
     * @hide
     */
    public boolean isOiSim(String mccmnc) {
        for (String mMncMcc : mOiSimMccMnc)
            if (mccmnc != null && mccmnc.equals(mMncMcc)) {
                TctLog.d(TAG, "OI sim card");
                return true;
            }
        return false;
    }

    /**
     * whether insert Vivo SIM
     *
     * @hide
     */
    public boolean isIdeaSim(String mccmnc) {
        for (String mMncMcc:mIdeaSimMccMnc)
            if (mccmnc != null && mccmnc.equals(mMncMcc)){
            Log.i(TAG, "Idea sim card");
            return true;
        }
        return false;
    }

    /**
     * whether insert Claro SIM
     *
     * @hide
     */
    public boolean isClaroSim(String mccmnc) {
        for (String mMncMcc:mClaroSimMccMnc)
            if (mccmnc != null && mccmnc.equals(mMncMcc)){
                TctLog.d(TAG, "Claro sim card");
                return true;
        }
        return false;
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    /**
     * Create a launchable activity object for a given ResolveInfo and user.
     *
     * @param context The context for fetching resources.
     * @param info ResolveInfo from which to create the LauncherActivityInfo.
     * @param user The UserHandle of the profile to which this activity belongs.
     */
    LauncherActivityInfo(Context context, ActivityInfo info, UserHandle user) {
        this(context);
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2521558
//Porting STK app:528460
        mContext = context;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        mActivityInfo = info;
        mComponentName =  new ComponentName(info.packageName, info.name);
        mUser = user;
    }

    LauncherActivityInfo(Context context) {
        mPm = context.getPackageManager();
    }

    /**
     * Returns the component name of this activity.
     *
     * @return ComponentName of the activity
     */
    public ComponentName getComponentName() {
        return mComponentName;
    }

    /**
     * Returns the user handle of the user profile that this activity belongs to. In order to
     * persist the identity of the profile, do not store the UserHandle. Instead retrieve its
     * serial number from UserManager. You can convert the serial number back to a UserHandle
     * for later use.
     *
     * @see UserManager#getSerialNumberForUser(UserHandle)
     * @see UserManager#getUserForSerialNumber(long)
     *
     * @return The UserHandle of the profile.
     */
    public UserHandle getUser() {
        return mUser;
    }

    /**
     * Retrieves the label for the activity.
     *
     * @return The label for the activity.
     */
    public CharSequence getLabel() {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2521558
//Porting STK app:528460
        if ("com.android.stk".equals(mComponentName.getPackageName()) && "true".equalsIgnoreCase
                (SystemProperties.get("def.stk.show.double.icon", "false"))){
            TctLog.d(TAG,"def.stk.show.double.icon:"+"true".equalsIgnoreCase
                    (SystemProperties.get("def.stk.show.double.icon", "false")));
            int stkIcon = Resources.getSystem()
                    .getInteger(com.android.internal.R.integer.def_tctfw_show_sim_icon);
            String sume = "";
            String MccMnc = null;
            if ("com.android.stk.StkLauncherActivity0".equals(mComponentName.getClassName())) {
                sume = android.provider.Settings.Global.getString(mContext.getContentResolver(), "stk_sume_0");
                MccMnc = android.provider.Settings.Global.getString(mContext.getContentResolver(), "mcc_mnc_0");
            } else if("com.android.stk.StkLauncherActivity1".equals(mComponentName.getClassName())) {
                sume = android.provider.Settings.Global.getString(mContext.getContentResolver(),"stk_sume_1");
                MccMnc = android.provider.Settings.Global.getString(mContext.getContentResolver(),"mcc_mnc_1");
            }
            Log.i(TAG, "getLabel sume: " + sume + " MccMnc: " + MccMnc + "stkicon: " + stkIcon);
            if ((2 == stkIcon)) {
                // for TIM brazil
                String tim_stk_name = mContext.getResources().getString(
                        com.android.internal.R.string.tim_brazil_stk_app_name);
                if ((tim_stk_name != null) && (MccMnc != null) && isTimSim(MccMnc))
                    return tim_stk_name;
            } else if ((5 == stkIcon)) {
                // for IDEA
                String idea_stk_name = mContext.getResources().getString(
                        com.android.internal.R.string.idea_stk_app_name);
                if ((idea_stk_name != null) && (MccMnc != null) && isIdeaSim(MccMnc))
                    return idea_stk_name;
            } else if ((4 == stkIcon) && (MccMnc != null) && (isVivoSim(MccMnc))) {
            //for vivo;show Vivo label "Vivo Chip" with a Vivo SIM card;
            //When the SIM card that is not from Vivo carrier,gets stk app name from sim card;
                    return sume;
            } else if (1 == stkIcon) {
                if ((MccMnc != null) && (isClaroSim(MccMnc))) {
                    // for claro operators
                    return sume;
                }
            } else {
                // other situations, if sume has real value, return it.
                if (sume != null && sume.length() > 0) {
                    return sume;
                }
            }
        } else if ("com.android.stk".equals(mComponentName.getPackageName())) {
            int stkIcon = Resources.getSystem()
                    .getInteger(com.android.internal.R.integer.def_tctfw_show_sim_icon);
            //[BUGFIX]-Mod-BEGIN by TCTNB.changwei.chi,01/19/2016,defect-1210620,
            //Modify the logic of the SDM def_tctfw_show_sim_icon.
            // Only when one SIM card inserted, the STK icon and name will be customized.
            boolean shouldShowCustomSimIcon = false;
            String MccMnc = null;
            String sume = null;
            int slotId = -1;
            TelephonyManager tm = TelephonyManager.getDefault();
            if (tm.isMultiSimEnabled() && (!tm.hasIccCard(0)) && tm.hasIccCard(1)) {
                shouldShowCustomSimIcon = true;
                MccMnc = android.provider.Settings.Global.getString(
                        mContext.getContentResolver(), "mcc_mnc_1");
                sume = android.provider.Settings.Global.getString(
                        mContext.getContentResolver(), "stk_sume_1");
                slotId = 1;
            } else if ((!tm.isMultiSimEnabled()) || (tm.hasIccCard(0) && !tm.hasIccCard(1))) {
                shouldShowCustomSimIcon = true;
                MccMnc = android.provider.Settings.Global.getString(
                        mContext.getContentResolver(), "mcc_mnc_0");
                sume = android.provider.Settings.Global.getString(
                        mContext.getContentResolver(), "stk_sume_0");
                slotId = 0;
            }
            TctLog.d(TAG, "getLabel-shouldShowCustomSimIcon is " + shouldShowCustomSimIcon
                    + " MccMnc:" + MccMnc + " sume:" + sume + " stkIcon: " + stkIcon + " slotId:"
                    + slotId);
            if (shouldShowCustomSimIcon) {
                if ((2 == stkIcon)) {
                    // for TIM brazil
                    String tim_stk_name = mContext.getResources().getString(
                            com.android.internal.R.string.tim_brazil_stk_app_name);
                    if ((tim_stk_name != null) && (MccMnc != null) && isTimSim(MccMnc))
                        return tim_stk_name;
                } else if ((5 == stkIcon)) {
                    // for IDEA
                    String idea_stk_name = mContext.getResources().getString(
                            com.android.internal.R.string.idea_stk_app_name);
                    if ((idea_stk_name != null) && (MccMnc != null) && isIdeaSim(MccMnc))
                        return idea_stk_name;
                } else if ("true".equalsIgnoreCase(SystemProperties.get("def.sys.stk.sume.name",
                        "false"))) {
                    if (1 == stkIcon) {
                        if ((MccMnc != null) && (isClaroSim(MccMnc))) {
                            // for claro operators
                            return sume;
                        }
                    } else if (4 == stkIcon) {
                        if ((MccMnc != null) && (isVivoSim(MccMnc))) {
                            return sume;
                        }
                    } else {
                        // other situations, if sume has real value, return it.
                        if (sume != null && sume.length() > 0) {
                            return sume;
                        }
                    }
                }
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        }
        return mActivityInfo.loadLabel(mPm);
    }

    /**
     * Returns the icon for this activity, without any badging for the profile.
     * @param density The preferred density of the icon, zero for default density. Use
     * density DPI values from {@link DisplayMetrics}.
     * @see #getBadgedIcon(int)
     * @see DisplayMetrics
     * @return The drawable associated with the activity.
     */
    public Drawable getIcon(int density) {
        final int iconRes = mActivityInfo.getIconResource();
        Drawable icon = null;
        // Get the preferred density icon from the app's resources
        if (density != 0 && iconRes != 0) {
            try {
                final Resources resources
                        = mPm.getResourcesForApplication(mActivityInfo.applicationInfo);
                icon = resources.getDrawableForDensity(iconRes, density);
            } catch (NameNotFoundException | Resources.NotFoundException exc) {
            }
        }
        // Get the default density icon
        if (icon == null) {
            icon = mActivityInfo.loadIcon(mPm);
        }
        return icon;
    }

    /**
     * Returns the application flags from the ApplicationInfo of the activity.
     *
     * @return Application flags
     * @hide remove before shipping
     */
    public int getApplicationFlags() {
        return mActivityInfo.applicationInfo.flags;
    }

    /**
     * Returns the application info for the appliction this activity belongs to.
     * @return
     */
    public ApplicationInfo getApplicationInfo() {
        return mActivityInfo.applicationInfo;
    }

    /**
     * Returns the time at which the package was first installed.
     *
     * @return The time of installation of the package, in milliseconds.
     */
    public long getFirstInstallTime() {
        try {
            return mPm.getPackageInfo(mActivityInfo.packageName,
                    PackageManager.GET_UNINSTALLED_PACKAGES).firstInstallTime;
        } catch (NameNotFoundException nnfe) {
            // Sorry, can't find package
            return 0;
        }
    }

    /**
     * Returns the name for the acitivty from  android:name in the manifest.
     * @return the name from android:name for the acitivity.
     */
    public String getName() {
        return mActivityInfo.name;
    }

    /**
     * Returns the activity icon with badging appropriate for the profile.
     * @param density Optional density for the icon, or 0 to use the default density. Use
     * {@link DisplayMetrics} for DPI values.
     * @see DisplayMetrics
     * @return A badged icon for the activity.
     */
    public Drawable getBadgedIcon(int density) {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2521558
//Porting STK app:528460
        if ("com.android.stk".equals(mComponentName.getPackageName()) && "true".equalsIgnoreCase
                (SystemProperties.get("def.stk.show.double.icon", "false"))) {
            TctLog.d(TAG,"def.stk.show.double.icon:"+"true".equalsIgnoreCase
                    (SystemProperties.get("def.stk.show.double.icon", "false")));
            int stkIcon = Resources.getSystem().getInteger(
                    com.android.internal.R.integer.def_tctfw_show_sim_icon);
            String MccMnc = null;
            if ("com.android.stk.StkLauncherActivity0".equals(mComponentName.getClassName())) {
                MccMnc = android.provider.Settings.Global.getString(mContext.getContentResolver(), "mcc_mnc_0");
            } else if("com.android.stk.StkLauncherActivity1".equals(mComponentName.getClassName())) {
                MccMnc = android.provider.Settings.Global.getString(mContext.getContentResolver(),"mcc_mnc_1");
            }
            Log.i(TAG, " MccMnc: " + MccMnc);
            if ((1 == stkIcon) && (MccMnc != null) && isClaroSim(MccMnc))
                return mContext.getResources().getDrawable(
                        com.android.internal.R.drawable.claro_sim_toolkit_icon);
            else if ((2 == stkIcon) && (MccMnc != null) && isTimSim(MccMnc))
                return mContext.getResources().getDrawable(
                        com.android.internal.R.drawable.tim_sim_toolkit_icon);
            else if ((3 == stkIcon) && (MccMnc != null) && isOiSim(MccMnc))
                return mContext.getResources().getDrawable(
                        com.android.internal.R.drawable.oi_sim_toolkit_icon);
            else if ((4 == stkIcon) && (MccMnc != null) && isVivoSim(MccMnc))
                return mContext.getResources().getDrawable(
                        com.android.internal.R.drawable.vivo_sim_toolkit_icon);
            else if ((5 == stkIcon) && (MccMnc != null) && isIdeaSim(MccMnc))
                return mContext.getResources().getDrawable(
                        com.android.internal.R.drawable.idea_sim_toolkit_icon);
            else if (6 == stkIcon)
                return mContext.getResources().getDrawable(
                        com.android.internal.R.drawable.telcel_sim_toolkit_icon);
        } else if ("com.android.stk".equals(mComponentName.getPackageName())) {
            //Modify the logic of the SDM def_tctfw_show_sim_icon.
            // Only when one SIM card inserted, the STK icon and name will be customized.
            int stkIcon = Resources.getSystem().getInteger(
                    com.android.internal.R.integer.def_tctfw_show_sim_icon);
            boolean shouldShowCustomSimIcon = false;
            String MccMnc = null;
            TelephonyManager tm = TelephonyManager.getDefault();
            if (tm.isMultiSimEnabled() && (!tm.hasIccCard(0)) && tm.hasIccCard(1)) {
                shouldShowCustomSimIcon = true;
                MccMnc = android.provider.Settings.Global.getString(
                        mContext.getContentResolver(), "mcc_mnc_1");
            } else if ((!tm.isMultiSimEnabled()) || (tm.hasIccCard(0) && !tm.hasIccCard(1))) {
                shouldShowCustomSimIcon = true;
                MccMnc = android.provider.Settings.Global.getString(
                        mContext.getContentResolver(), "mcc_mnc_0");
            }
            TctLog.d(TAG, "getBadgedIcon-shouldShowCustomSimIcon:" + shouldShowCustomSimIcon
                    + " MccMnc:" + MccMnc + " show sim icon: " + stkIcon);
            if (shouldShowCustomSimIcon) {
                if ((1 == stkIcon) && (MccMnc != null) && isClaroSim(MccMnc))
                    return mContext.getResources().getDrawable(
                            com.android.internal.R.drawable.claro_sim_toolkit_icon);
                else if ((2 == stkIcon) && (MccMnc != null) && isTimSim(MccMnc))
                    return mContext.getResources().getDrawable(
                            com.android.internal.R.drawable.tim_sim_toolkit_icon);
                else if ((3 == stkIcon) && (MccMnc != null) && isOiSim(MccMnc))
                    return mContext.getResources().getDrawable(
                            com.android.internal.R.drawable.oi_sim_toolkit_icon);
                else if ((4 == stkIcon) && (MccMnc != null) && isVivoSim(MccMnc))
                    return mContext.getResources().getDrawable(
                            com.android.internal.R.drawable.vivo_sim_toolkit_icon);
                else if ((5 == stkIcon) && (MccMnc != null) && isIdeaSim(MccMnc))
                    return mContext.getResources().getDrawable(
                            com.android.internal.R.drawable.idea_sim_toolkit_icon);
                else if (6 == stkIcon)
                    return mContext.getResources().getDrawable(
                            com.android.internal.R.drawable.telcel_sim_toolkit_icon);
            }
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        Drawable originalIcon = getIcon(density);
        if (originalIcon instanceof BitmapDrawable) {
            return mPm.getUserBadgedIcon(originalIcon, mUser);
        } else {
            Log.e(TAG, "Unable to create badged icon for " + mActivityInfo);
        }
        return originalIcon;
    }
}
