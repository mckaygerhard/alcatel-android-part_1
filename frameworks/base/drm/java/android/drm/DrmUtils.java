/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.drm;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;

//FEATURE]-Add-BEGIN by TCTNB.Yang.Hu,2015/9/30, task-572426 TCT DRM solution
import android.util.Log;
import android.webkit.MimeTypeMap;
//[FEATURE]-Add-END by TCTNB.Yang.Hu TCT DRM solution

/**
 * A utility class that provides operations for parsing extended metadata embedded in
 * DRM constraint information. If a DRM scheme has specific constraints beyond the standard
 * constraints, the constraints will show up in the
 * {@link DrmStore.ConstraintsColumns#EXTENDED_METADATA} key. You can use
 * {@link DrmUtils.ExtendedMetadataParser} to iterate over those values.
 */
public class DrmUtils {
    /* Should be used when we need to read from local file */
    /* package */ static byte[] readBytes(String path) throws IOException {
        File file = new File(path);
        return readBytes(file);
    }

    /* Should be used when we need to read from local file */
    /* package */ static byte[] readBytes(File file) throws IOException {
        FileInputStream inputStream = new FileInputStream(file);
        BufferedInputStream bufferedStream = new BufferedInputStream(inputStream);
        byte[] data = null;

        try {
            int length = bufferedStream.available();
            if (length > 0) {
                data = new byte[length];
                // read the entire data
                bufferedStream.read(data);
             }
        } finally {
            quietlyDispose(bufferedStream);
            quietlyDispose(inputStream);
        }
        return data;
    }

    /* package */ static void writeToFile(final String path, byte[] data) throws IOException {
        /* check for invalid inputs */
        FileOutputStream outputStream = null;

        if (null != path && null != data) {
            try {
                outputStream = new FileOutputStream(path);
                outputStream.write(data);
            } finally {
                quietlyDispose(outputStream);
            }
        }
    }

    /* package */ static void removeFile(String path) throws IOException {
        File file = new File(path);
        file.delete();
    }

    private static void quietlyDispose(InputStream stream) {
        try {
            if (null != stream) {
                stream.close();
            }
        } catch (IOException e) {
            // no need to care, at least as of now
        }
    }

    private static void quietlyDispose(OutputStream stream) {
        try {
            if (null != stream) {
                stream.close();
            }
        } catch (IOException e) {
            // no need to care
        }
    }

    /**
     * Gets an instance of {@link DrmUtils.ExtendedMetadataParser}, which can be used to parse
     * extended metadata embedded in DRM constraint information.
     *
     * @param extendedMetadata Object in which key-value pairs of extended metadata are embedded.
     *
     */
    public static ExtendedMetadataParser getExtendedMetadataParser(byte[] extendedMetadata) {
        return new ExtendedMetadataParser(extendedMetadata);
    }

    //FEATURE]-Add-BEGIN by TCTNB.Yang.Hu,2015/9/30, task-572426 TCT DRM solution
    /**
     * {@hide}
     */
    public static void tctRemoveFile(String path) throws IOException {
        removeFile(path);
    }
    //[FEATURE]-Add-END by TCTNB.Yang.Hu TCT DRM solution

    /**
     * Utility that parses extended metadata embedded in DRM constraint information.
     *<p>
     * Usage example:
     *<p>
     * byte[] extendedMetadata<br>
     * &nbsp;&nbsp;&nbsp;&nbsp; =
     *         constraints.getAsByteArray(DrmStore.ConstraintsColumns.EXTENDED_METADATA);<br>
     * ExtendedMetadataParser parser = getExtendedMetadataParser(extendedMetadata);<br>
     * Iterator keyIterator = parser.keyIterator();<br>
     * while (keyIterator.hasNext()) {<br>
     *     &nbsp;&nbsp;&nbsp;&nbsp;String extendedMetadataKey = keyIterator.next();<br>
     *     &nbsp;&nbsp;&nbsp;&nbsp;String extendedMetadataValue =
     *             parser.get(extendedMetadataKey);<br>
     * }
     */
    public static class ExtendedMetadataParser {
        HashMap<String, String> mMap = new HashMap<String, String>();

        private int readByte(byte[] constraintData, int arrayIndex) {
            //Convert byte[] into int.
            return (int)constraintData[arrayIndex];
        }

        private String readMultipleBytes(
                byte[] constraintData, int numberOfBytes, int arrayIndex) {
            byte[] returnBytes = new byte[numberOfBytes];
            for (int j = arrayIndex, i = 0; j < arrayIndex + numberOfBytes; j++,i++) {
                returnBytes[i] = constraintData[j];
            }
            return new String(returnBytes);
        }

        /*
         * This will parse the following format
         * KeyLengthValueLengthKeyValueKeyLength1ValueLength1Key1Value1..\0
         */
        private ExtendedMetadataParser(byte[] constraintData) {
            //Extract KeyValue Pair Info, till terminator occurs.
            int index = 0;

            while (index < constraintData.length) {
                //Parse Key Length
                int keyLength = readByte(constraintData, index);
                index++;

                //Parse Value Length
                int valueLength = readByte(constraintData, index);
                index++;

                //Fetch key
                String strKey = readMultipleBytes(constraintData, keyLength, index);
                index += keyLength;

                //Fetch Value
                String strValue = readMultipleBytes(constraintData, valueLength, index);
                if (strValue.equals(" ")) {
                    strValue = "";
                }
                index += valueLength;
                mMap.put(strKey, strValue);
            }
        }

        /**
         * This method returns an iterator object that can be used to iterate over
         * all values of the metadata.
         *
         * @return The iterator object.
         */
        public Iterator<String> iterator() {
            return mMap.values().iterator();
        }

        /**
         * This method returns an iterator object that can be used to iterate over
         * all keys of the metadata.
         *
         * @return The iterator object.
         */
        public Iterator<String> keyIterator() {
            return mMap.keySet().iterator();
        }

        /**
         * This method retrieves the metadata value associated with a given key.
         *
         * @param key The key whose value is being retrieved.
         *
         * @return The metadata value associated with the given key. Returns null
         * if the key is not found.
         */
        public String get(String key) {
            return mMap.get(key);
        }
    }

    //FEATURE]-Add-BEGIN by TCTNB.Yang.Hu,2015/9/30, task-572426 TCT DRM solution
    private static final String TAG = "DrmUtils";
    /**
     * from the type, we can get the extension. TODO use and simple method
     *
     * @param strDate string type
     * @return String extension.
     * @hide
     */
    public static String getExtensionFromMime(String type) {
        if (type == null) {
            Log.e(TAG, "the mimeType should not be null");
            return null;
        }
        String extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(type);

        if (type.startsWith("audio/mp3") || type.startsWith("video/mp3")
                || type.startsWith("audio/mpeg")) {
            extension = new String("mp3");
        }
        if (type.startsWith("audio/mp4") || type.startsWith("video/mp4")) {
            extension = new String("mp4");
        }

        if (type.startsWith("audio/3gpp") || type.startsWith("video/3gpp")) {
            extension = new String("3gp");
        }

        if (type.startsWith("audio/aac") || type.startsWith("video/aac")) {
            extension = new String("aac");
        }
        if (type.startsWith("audio/mp4a-latm")
                || type.startsWith("video/mp4a-latm")) {
            extension = new String("aac");
        }
        if (type.startsWith("audio/amr-wb")) {
            extension = new String("amr");
        }
        if (type.startsWith("video/mpeg4")) {
            extension = new String("mp4");
        }
        if (type.startsWith("image/jpeg")) {
            extension = new String("jpg");
        }
        return extension;
    }

    /**
     * convert interval date. TODO use and simple method
     *
     * @param strDate
     *            string date
     * @return int date.
     * @hide
     */
    public static long convertStrDateToIntDate(String strDate) {
        final int YEAR = 365 * 30 * 24 * 60 * 60;
        final int MONTH = 30 * 24 * 60 * 60;
        final int DAY = 24 * 60 * 60;
        final int HOUR = 60 * 60;
        final int MINUTE = 60;
        final int SECOND = 1;
        final int OFFSET = 1;
        final int DATE_LENGTH = 19;
        long integerDate = 0;

        if (strDate == null) {
            return 0;
        }
        if (strDate.indexOf('-') == -1) {
            try {
                integerDate = Long.parseLong(strDate);
                return integerDate;
            } catch (NumberFormatException e) {
                e.printStackTrace();
                integerDate = 0;
            }
        }
        if (strDate.length() != DATE_LENGTH) {
            return 0;
        }
        int countNumber = 6;
        long[] date = new long[countNumber];
        int head = 0; // because String format is 0000-00-00 00:00:00(Y-M-D
                      // H:M:S)
        int tail = 4;
        for (int i = 0; i < countNumber; i++) {
            String subStr = strDate.substring(head, tail);
            try {
                date[i] = Integer.parseInt(subStr);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return 0;
            }
            head = tail + OFFSET;
            tail = tail + OFFSET + OFFSET + OFFSET; // as string format, do this
                                                    // operator.
        }
        Log.d("drmmediaplay", "year:" + date[0] + ", month:" + date[1]
                + ", day:" + date[2] + ", hour:" + date[3] + ", minute:"
                + date[4] + ", second:" + date[5]);

        integerDate = date[0] * YEAR + date[1] * MONTH + date[2] * DAY
                + date[3] * HOUR + date[4] * MINUTE + date[5] * SECOND;
        return integerDate;
    }
    
    
    /**
     * Defines actions that can be performed on rights-protected content.
     * @hide
     */
    public static class Action {
        /**
         * The default action.
         */
        public static final int DEFAULT = 0x00;
        /**
         * The rights-protected content can be played.
         */
        public static final int PLAY = 0x01;
        /**
         * The rights-protected content can be set as a ringtone.
         */
        public static final int RINGTONE = 0x02;
        /**
         * The rights-protected content can be transferred.
         */
        public static final int TRANSFER = 0x03;
        /**
         * The rights-protected content can be set as output.
         */
        public static final int OUTPUT = 0x04;
        /**
         * The rights-protected content can be previewed.
         */
        public static final int PREVIEW = 0x05;
        /**
         * The rights-protected content can be executed.
         */
        public static final int EXECUTE = 0x06;
        /**
         * The rights-protected content can be displayed.
         */
        public static final int DISPLAY = 0x07;

        public static boolean isValid(int action) {
            boolean isValid = false;

            switch (action) {
                case DEFAULT:
                case PLAY:
                case RINGTONE:
                case TRANSFER:
                case OUTPUT:
                case PREVIEW:
                case EXECUTE:
                case DISPLAY:
                    isValid = true;
            }
            return isValid;
        }
    }
    //[FEATURE]-Add-END by TCTNB.Yang.Hu TCT DRM solution
}

