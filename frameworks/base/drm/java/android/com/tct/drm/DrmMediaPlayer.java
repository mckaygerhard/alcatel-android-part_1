package com.tct.drm;

import java.lang.ref.WeakReference;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SqliteWrapper;
import android.drm.DrmManagerClient;
import android.drm.DrmUtils;
import android.drm.DrmStore;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.Parcelable;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;

import com.tct.drm.DrmApp;
import com.tct.drm.TctDrmStore;
import com.tct.drm.TctDrmUtils;

/**
 * {@hide}
 */
public class DrmMediaPlayer {

    private final static String TAG = "DrmMediaPlayer";
    private static final boolean TCT_DEBUG = false;
    /*
     * Key used in setParameter method. Indicates the index of the timed text
     * track to be enabled/disabled. The index includes both the in-band and
     * out-of-band timed text. The index should start from in-band text if any.
     * Application can retrieve the number of in-band text tracks by using
     * MediaMetadataRetriever::extractMetadata(). Note it might take a few
     * hundred ms to scan an out-of-band text file before displaying it.
     */
    private static final int KEY_PARAMETER_TIMED_TEXT_TRACK_INDEX = 1000;
    /*
     * Key used in setParameter method. Used to add out-of-band timed text
     * source path. Application can add multiple text sources by calling
     * setParameter() with KEY_PARAMETER_TIMED_TEXT_ADD_OUT_OF_BAND_SOURCE
     * multiple times.
     */
    private static final int KEY_PARAMETER_TIMED_TEXT_ADD_OUT_OF_BAND_SOURCE = 1001;
    private static DrmMediaPlayer mDrmMediaPlayer = null;
    private static final String VIDEO_PACKAGE = "com.tct.gallery";
    private static final String AUDIO_PACKAGE = "com.google.android.music";
    private static final String SYSTEM_PACKAGE = "android.uid.system"; // MODIFIED by hu.yang, 2016-11-03,BUG-3309643
    private static final String MESSAGE_PACKAGE = "com.android.mms"; // MODIFIED by hu.yang, 2016-11-03,BUG-3376609
    private static final String MEDIA_PACKAGE = "android.media";

    private Uri mUri = null;

    private boolean mDrmConsumed = false;
    private boolean mRingTone = false;

    private final int FILE_INIT = -1;
    private final int DRM_FILE = 1;
    private final int NOT_DRM_FILE = 0;
    private final long MIN_DELAY_TIME = 0;
    private final long MAX_DELAY_TIME = 86400000;
    private int mFileType = FILE_INIT;
    private String mFilePath = null;
    private ParcelFileDescriptor mFd = null;
    private Handler mMediaListener;
    private MediaPlayIntervalCallback mDrmTimeListener = null;
    private MediaPlayer mMediaPlayer = null;
    private DrmTime mDrmTime;

    /**
     * @hide
     */
    public void setmFileType(int type) {
        mFileType = type;
    }

    /**
     * @hide
     */
    public void setmFilePath(String path) {
        mFilePath = path;
    }

    /**
     * @hide
     */
    public void setmFd(ParcelFileDescriptor Fd) {
        mFd = Fd;
    }

    /**
     * @hide
     */
    public void setmDrmConsumed(boolean mark) {
        mDrmConsumed = mark;
    }

    /**
     * @hide
     */
    public void setmRingTone(boolean mark) {
        mRingTone = mark;
    }

    /**
     * @hide
     */
    public int getDRM_FILE() {
        return DRM_FILE;
    }

    /**
     * @hide
     */
    public int getmFileType() {
        return mFileType;
    }

    private class DrmTime {
        static final int DRM_TIME_TYPE_INERVAL = 0;
        static final int DRM_TIME_TYPE_STAT_END = 1;
        int type;
        long mIntervalDate;
    }

    /**
     * @hide
     */
    public synchronized static DrmMediaPlayer getInstance() {
        if (mDrmMediaPlayer == null) {
            mDrmMediaPlayer = new DrmMediaPlayer();
        }

        return mDrmMediaPlayer;
    }

    /**
     * @hide
     */
    public DrmMediaPlayer() {
        mFileType = FILE_INIT;
        mDrmTime = new DrmTime();
    }

    /**
     * @hide
     */
    public DrmMediaPlayer(boolean isRingTone) {
        this();
        mRingTone = isRingTone;
    }

    /**
     * @hide
     */
    public boolean isDrmEnable(Context mContext, Uri uri) {
        boolean result = true;

        //[BUGFIX]-Mod-BEGIN by TCTNB.Yang.Hu 2016/2/29 PR-1692551
        try {
            if (uri != null && uri.toString().startsWith("content://settings/system")) {
                Uri seturi = RingtoneManager.getActualDefaultRingtoneUri(mContext, 1);// 1 stands for ringtone
                uri = seturi;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //[BUGFIX]-Mod-END by TCTNB.Yang.Hu 2016/2/29 PR-1692551

        //[BUGFIX]Add-BEGIN by TCTNB.Rongdi.Wang 2016/1/8 PR 1273358
        if (TctDrmManagerClient.isDrmEnabled() && mContext != null && uri != null) {
            //[BUGFIX]Add-BEGIN by TCTNB.Rongdi.Wang 2016/1/20 PR 1453309
            Log.d(TAG, "isDrmEnable uri = " + uri);
            if (uri.toString().startsWith("content:")) {
                ContentResolver mCr = mContext.getContentResolver();
                try {
                    mFd = mCr.openFileDescriptor(uri, "r");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (mFd != null) {
                    FileDescriptor fd = mFd.getFileDescriptor();
                    if (TctDrmManagerClient.getInstance(mContext).isDrm(fd)) {
                        if (uri.toString().startsWith("content://gmail-ls/")) {
                            closeMFd();
                            new DrmApp(mContext).showToast(com.android.internal.R.string.drm_not_support_Gmail, null);
                            return true;
                        }

                        if (uri.toString().startsWith("content://com.google.android.apps")) {
                            closeMFd();
                            new DrmApp(mContext).showToast(com.android.internal.R.string.drm_not_support, null);
                            return true;
                        }

                        int uid = Binder.getCallingUid();
                        PackageManager pm = mContext.getPackageManager();
                        if (pm != null) {
                            String name = pm.getNameForUid(uid);
                            Log.d(TAG, "calling drm package name1 = " + name);
                            /* MODIFIED-BEGIN by hu.yang, 2016-11-03,BUG-3312266*/
                            if (name == null) {
                                Log.d(TAG, "calling drm package name1 is null");
                                closeMFd();
                                return true;
                            }
                            /* MODIFIED-END by hu.yang,BUG-3312266*/

                            if (!(name != null && (name.startsWith(AUDIO_PACKAGE)
                                    || name.startsWith(VIDEO_PACKAGE)
                                    || name.startsWith(SYSTEM_PACKAGE)
                                    || name.startsWith(MESSAGE_PACKAGE)
                                    || name.startsWith(MEDIA_PACKAGE)))) { // MODIFIED by hu.yang, 2016-11-03,BUG-3309643
                                Log.d(TAG, "DRM file is not allowed in 3rd app");
                                closeMFd();
                                new DrmApp(mContext).showToast(com.android.internal.R.string.drm_not_support, null);
                                return true;
                            }
                        }

                        if (!TctDrmManagerClient.doWithWifidisplay(null, fd, null)) {
                            Log.d(TAG, "DRM file is not allowed in wifidisplay");
                            closeMFd();
                            new DrmApp(mContext).showToast(com.android.internal.R.string.drm_not_support_wifidisplay, null);
                            return true;
                        }

                        if (!processDrmFile(mContext, uri)) {
                            closeMFd();
                            return result;
                        }
                    }
                }
                closeMFd();
            } else {
                if (TctDrmManagerClient.getInstance(mContext).isDrm(uri)) {
                    int uid = Binder.getCallingUid();
                    PackageManager pm = mContext.getPackageManager();
                    if (pm != null) {
                        String name = pm.getNameForUid(uid);
                        Log.d(TAG, "calling drm package name2 = " + name);
                        /* MODIFIED-BEGIN by hu.yang, 2016-11-03,BUG-3312266*/
                        if (name == null) {
                            Log.d(TAG, "calling drm package name2 is null");
                            return true;
                        }
                        /* MODIFIED-END by hu.yang,BUG-3312266*/

                        if (!(name != null && (name.startsWith(AUDIO_PACKAGE)
                                || name.startsWith(VIDEO_PACKAGE)
                                || name.startsWith(SYSTEM_PACKAGE)
                                || name.startsWith(MESSAGE_PACKAGE)
                                || name.startsWith(MEDIA_PACKAGE)))) { // MODIFIED by hu.yang, 2016-11-03,BUG-3309643
                            Log.d(TAG, "DRM file is not allowed in 3rd app");
                            new DrmApp(mContext).showToast(com.android.internal.R.string.drm_not_support, null);
                            return true;
                        }
                    }

                    if (!TctDrmManagerClient.doWithWifidisplay(null, null, uri)) {
                        Log.d(TAG, "DRM file is not allowed in wifidisplay");
                        new DrmApp(mContext).showToast(com.android.internal.R.string.drm_not_support_wifidisplay, null);
                        return true;
                    }

                    if (!processDrmFile(mContext, uri)) {
                        return result;
                    }
                }
            }
            //[BUGFIX]Add-End by TCTNB.Rongdi.Wang
        }
        //[BUGFIX]Add-BEGIN by TCTNB.Rongdi.Wang 2016/1/8 PR 1273358
        return false;
    }

    /**
     * @hide
     */
    public void mMediaPlayerSeek(MediaPlayer mMediaPlayer) {
        if ((null != mUri || (null != mFilePath && !mFilePath.equals("")) ||
                (null != mFd && mFd.getFileDescriptor().valid())) && (!mMediaPlayer.isLooping())) {
            mMediaPlayer.seekTo(0);
        }
    }

    /**
     * @hide
     */
    public void DrmShowToast(String path, boolean mark, Context mContext) {
        if (TctDrmManagerClient.isDrmEnabled()) {
            if (TctDrmManagerClient.getInstance(mContext).isDrm(path)) {

                if (new TctDrmManagerClient(mContext, false).isRightValid(path)) {
                } else {
                    new DrmApp(mContext).showToast(com.android.internal.R.string.drm_no_valid_right, null);
                }
            }
        }
    }

    /**
     * check Drm file right. return true if the file right is viable, otherwise
     * return false.
     *
     * @param target the media file is checked.
     * @param mark
     * @return result by check right.
     * @hide
     */
    public boolean IsDrmCanhandle(Object target, boolean mark, Context mContext) {
        boolean result = false;
        try {
            if (mark && null != mFd) {
                mFd.close();
                mFd = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!TctDrmManagerClient.isDrmEnabled()) {
            return result;
        }
        if (target instanceof String) {
            String path = (String) target;
            if (path.equals("")) {
                return result;
            }
            result = checkDrmCanHandlePath(path, mContext);
        } else if (target instanceof Uri) {
            Uri uri = (Uri) target;
            if (Uri.EMPTY == uri) {
                return result;
            }
            result = checkDrmCanHandleUri(uri, mContext);
        } else if (target instanceof ParcelFileDescriptor) {
            ParcelFileDescriptor pfd = (ParcelFileDescriptor) target;
            if (!(pfd.getFileDescriptor()).valid()) {
                return result;
            }
            result = checkDrmCanHandleFileDescriptor(pfd, mContext);
        }
        return result;
    }

    /**
     * @param value
     */
    private void getIntervalTime(ContentValues value, TctDrmManagerClient drmClient) {
        String date;
        mDrmTime.mIntervalDate = 0;
        mDrmTime.type = DrmTime.DRM_TIME_TYPE_STAT_END;
        long[] secureTime = new long[2];
        if (value != null) {
            if (value.get(DrmStore.ConstraintsColumns.LICENSE_AVAILABLE_TIME) != null) {
                date = value.get(DrmStore.ConstraintsColumns.LICENSE_AVAILABLE_TIME).toString();
                Log.d(TAG, "Interval Time date:" + date);
                mDrmTime.mIntervalDate = TctDrmUtils.convertStrDateToIntDate(date) * TctDrmUtils.MILLI_SECOND;
                mDrmTime.type = DrmTime.DRM_TIME_TYPE_INERVAL;
            } else if (value.get(DrmStore.ConstraintsColumns.LICENSE_EXPIRY_TIME) != null) {
                date = value.get(DrmStore.ConstraintsColumns.LICENSE_EXPIRY_TIME).toString();
                Log.d(TAG, "End Time date:" + date);
                mDrmTime.mIntervalDate = TctDrmUtils.convertStrDateToIntDate(date) * TctDrmUtils.MILLI_SECOND;
                mDrmTime.type = DrmTime.DRM_TIME_TYPE_STAT_END;
                drmClient.readSecureTimeFromFile(secureTime);
                String timeFormat = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat sdf = new SimpleDateFormat(timeFormat, Locale.US);
                Calendar c = Calendar.getInstance();
                try {
                    Date dt = sdf.parse(date);
                    c.setTime(dt);
                    if (secureTime[0] == 1) {
                        mDrmTime.mIntervalDate = c.getTimeInMillis() - (System.currentTimeMillis() + secureTime[1]);
                    } else {
                        mDrmTime.mIntervalDate = 0;
                    }
                } catch (ParseException e1) {
                    e1.printStackTrace();
                    mDrmTime.mIntervalDate = 0;
                } finally {
                    Log.d(TAG, "getIntervalTime interval date:" + mDrmTime.mIntervalDate);
                }
            }
        }
    }

    /**
     * check right of file by path.
     *
     * @param path
     * @return
     */
    private boolean checkDrmCanHandlePath(String path, Context mContext) {
        boolean result = false;
        DrmManagerClient drmClient = new DrmManagerClient(mContext);
        TctDrmManagerClient mDrmClient = TctDrmManagerClient.getInstance(mContext);
        mFilePath = path;
        mFileType = NOT_DRM_FILE;
        if (drmClient != null && drmClient.canHandle(path, "")) {
            mFileType = DRM_FILE;
            if (drmClient.checkRightsStatus(path) != DrmStore.RightsStatus.RIGHTS_VALID) {
                new DrmApp(mContext).showToast(com.android.internal.R.string.drm_no_valid_right, null);
                Log.d(TAG, "right is invalid");
                drmClient.release();
                return result;
            }
            ContentValues value = drmClient.getConstraints(path, DrmStore.Action.PLAY);
            getIntervalTime(value, mDrmClient);
            result = true;
        }
        if(drmClient != null){
            drmClient.release();
        }
        return result;
    }

    /**
     * check right of file by uri.
     *
     * @param path
     * @return
     */
    private boolean checkDrmCanHandleUri(Uri uri, Context mContext) {
        boolean result = false;
        mUri = uri;
        mFileType = NOT_DRM_FILE;
        DrmManagerClient drmClient = new DrmManagerClient(mContext);
        TctDrmManagerClient mDrmClient = TctDrmManagerClient.getInstance(mContext);
        if (drmClient.canHandle(uri, "")) {
            mFileType = DRM_FILE;
            if (drmClient.checkRightsStatus(uri) != DrmStore.RightsStatus.RIGHTS_VALID) {
                new DrmApp(mContext).showToast(com.android.internal.R.string.drm_no_valid_right,null);
                return result;
            }
            ContentValues value = drmClient.getConstraints(uri, DrmStore.Action.PLAY);
            getIntervalTime(value, mDrmClient);
            result = true;
        }

        return result;
    }

    /**
     * check right of file by File Descriptor.
     *
     * @param path
     * @return
     */
    private boolean checkDrmCanHandleFileDescriptor(ParcelFileDescriptor pfd, Context mContext) {
        boolean result = false;
        mFd = pfd;
        mFileType = NOT_DRM_FILE;
        FileDescriptor fd = pfd.getFileDescriptor();
        TctDrmManagerClient mDrmClient = TctDrmManagerClient.getInstance(mContext);
        if (mDrmClient.canHandle(fd)) {
            mFileType = DRM_FILE;
            if (mDrmClient.checkRightsStatus(fd) != DrmStore.RightsStatus.RIGHTS_VALID) {
                new DrmApp(mContext).showToast(com.android.internal.R.string.drm_no_valid_right, null);
                try {
                    pfd.close();
                    pfd = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return result;
            }
            ContentValues value = mDrmClient.getConstraints(fd, DrmStore.Action.PLAY);
            getIntervalTime(value, mDrmClient);
            result = true;
        }

        return result;
    }

    private boolean processDrmFile(Context mContext, Uri uri) {
        mUri = uri;

        boolean result = false;

        if (uri != null) {
            mFilePath = convertUriToPath(uri,mContext);
        }

        if (!mDrmConsumed) {
            if (!result) {
                result = IsDrmCanhandle(mFilePath, false, mContext);
            }
            if (!result) {
                result = IsDrmCanhandle(mFd, false, mContext);
            }
            if (!result) {
                result = IsDrmCanhandle(uri, false, mContext);
            }
        } else {
            result = true;
        }

        if ((DRM_FILE == mFileType) && result && !mDrmConsumed) {
            if (mDrmTime.mIntervalDate > MIN_DELAY_TIME && mDrmTime.mIntervalDate < MAX_DELAY_TIME) {
                if (mMediaListener == null) {
                    mMediaListener = new Handler();
                }
                if (mDrmTimeListener != null) {
                    mMediaListener.removeCallbacks(mDrmTimeListener);
                    mDrmTimeListener = null;
                }
                mDrmTimeListener = new MediaPlayIntervalCallback(mMediaPlayer, mContext);
                mMediaListener.postDelayed(mDrmTimeListener, mDrmTime.mIntervalDate);
                Log.d(TAG, "postDelayed drm listener:" + mDrmTime.mIntervalDate);
            }
            TctDrmManagerClient mDrmClient = TctDrmManagerClient.getInstance(mContext);
            if (null != mFilePath && !mFilePath.equals("") && !mFilePath.startsWith("content://gmail-ls/")) {
                mDrmClient.consumeRights(mFilePath);
            } else if (null != mUri && Uri.EMPTY != mUri && !(mUri.toString().startsWith("content://gmail-ls/"))) {
                mDrmClient.consumeRights(mUri, DrmStore.Action.PLAY);
            } else {
                mDrmClient.consumeRights(mFd.getFileDescriptor(), DrmStore.Action.PLAY);
            }
            if (null != mUri && Uri.EMPTY != mUri && mDrmTime.type == DrmTime.DRM_TIME_TYPE_INERVAL) {
                String playUriStr = mUri.toString();
                if (playUriStr.startsWith("content://settings/system/")) {
                    if (Settings.System.DEFAULT_NOTIFICATION_URI.equals(mUri)) {
                        sendDrmIntent(mContext,
                                Settings.System.NOTIFICATION_SOUND,
                                Settings.System.getString(
                                        mContext.getContentResolver(),
                                        Settings.System.NOTIFICATION_SOUND), true);
                    } else if (Settings.System.DEFAULT_RINGTONE_URI.equals(mUri)) {
                        sendDrmIntent(mContext, Settings.System.RINGTONE,
                                Settings.System.getString(
                                        mContext.getContentResolver(),
                                        Settings.System.RINGTONE), true);
                    } else if (Settings.System.DEFAULT_ALARM_ALERT_URI.equals(mUri)) {
                        sendDrmIntent(mContext, Settings.System.ALARM_ALERT,
                                Settings.System.getString(
                                        mContext.getContentResolver(),
                                        Settings.System.ALARM_ALERT), true);
                    }
                } else {
                    sendDrmIntent(mContext, Settings.System.RINGTONE, mUri.toString(), false);
                    sendDrmIntent(mContext, Settings.System.NOTIFICATION_SOUND, mUri.toString(), false);
                    sendDrmIntent(mContext, Settings.System.ALARM_ALERT, mUri.toString(), false);
                }
            }
            mDrmConsumed = true;
        } else if ((DRM_FILE == mFileType) && !result && !mRingTone) {
            String info = null;
            if (mFilePath != null) {
                info = mFilePath;
            } else if (mUri != null && mUri.getScheme().contains("file")) {
                info = mUri.getPath();
            }
            new DrmApp(mContext).showToast(com.android.internal.R.string.drm_no_valid_right, null);
            return false;
        }
        return result;
    }

    /***
     * @param context
     * @param uri
     * @return
     */
    private String getDataByUri(Context context, Uri uri) {
        if (null == uri) {
            return null;
        }
        String filePath = null;
        if (context != null) {
             Log.v(TAG, "getActualUri:get  uri from DocumentUri " + uri);
             Cursor c = SqliteWrapper.query(context, context.getContentResolver(), uri, null, null, null, null);
             if (c != null && c.moveToFirst()) {
                 filePath = c.getString(c.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA));
             }
             if (c != null) c.close();
         }
        if (context != null && uri.getScheme() != null && uri.getScheme().endsWith("file")) {
            Log.v(TAG, "Get uri from Storage " + uri);
            filePath = uri.getPath();
        }
        if (filePath != null) {
            Cursor cursor = context.getContentResolver().query(
                       MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                       new String[] { MediaStore.Audio.Media._ID },
                       "_data = ?",
                       new String[] { filePath },
                       MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
            if (cursor != null && cursor.getCount() > 0) {cursor.moveToFirst();
                 uri = Uri.parse(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "/"
                 + cursor.getString(cursor.getColumnIndex("_id")));
             }
            cursor.close();
        }
        return uri.toString();
    }

    /**
     * @hide
     */
    public void cancelDrmListener() {
        if (mMediaListener != null && mDrmTimeListener != null) {
            mMediaListener.removeCallbacks(mDrmTimeListener);
        }
        mMediaListener = null;
        mDrmTimeListener = null;
    }

    private void sendDrmIntent(Context mContext, String ringType, String uri, boolean send) {
        String ringUri = Settings.System.getString(mContext.getContentResolver(), ringType);
        String strCurdata = uri;
        if (!uri.startsWith("content://media/audio")) {
            strCurdata = getDataByUri(mContext, Uri.parse(uri));
        }

        if (send || (ringUri != null && ringUri.equals(strCurdata))) {
            Intent drmIntent = new Intent(TctDrmStore.ACITON_DRM_RINGTONE_SET);
            drmIntent.putExtra("ringtype", ringType);
            drmIntent.putExtra(ringType, strCurdata.toString());
            mContext.sendBroadcast(drmIntent);
        }
    }

    /**
     * @hide
     */
    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        mMediaPlayer = mediaPlayer;
    }

    /**
     * @hide
     */
    public void closeMFd() {
        try {
            if (null != mFd) {
                mFd.close();
                mFd = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tell mediaplayer that the rights of the source have been consumed once
     *
     * @hide
     */
    public void drmAlreadyConsumed() {
        mDrmConsumed = true;
    }

    /**
     * true if drm rights have been consumed successfully.
     *
     * @hide
     */
    public boolean drmConsumed() {
        return mDrmConsumed;
    }

    /**
     * listener of playing drm media as interval constraint.
     *
     * @author
     */
    private class MediaPlayIntervalCallback implements Runnable {
        private MediaPlayer mPlayer;
        private Context mContext;

        public MediaPlayIntervalCallback(MediaPlayer mediaPlayer,
                Context context) {
            mPlayer = new WeakReference<MediaPlayer>(mediaPlayer).get();
            mContext = context;
        }

        @Override
        public void run() {
            if (mMediaPlayer == null || mPlayer == null || !mDrmMediaPlayer.mDrmConsumed) {
                return;
            }
            mDrmMediaPlayer.mDrmConsumed = false;
            //[BUGFIX]-Mod-BEGIN by TCTNB.Minjie.Cai 2016/3/4 PR-1706899
            try {
                if (mMediaPlayer != null && mPlayer !=null && mPlayer.isPlaying()&& !mDrmMediaPlayer.mRingTone) {
                mPlayer.stop();
                Intent drmTimeOutIntent = new Intent(TctDrmStore.DrmBroadcastAction.DRM_TIME_OUT_ACTION);
                drmTimeOutIntent.putExtra("file_path", mDrmMediaPlayer.mFilePath);
                mContext.sendBroadcast(drmTimeOutIntent);
                Log.d(TAG, "stop listener:" + mDrmMediaPlayer.mDrmTime.mIntervalDate);
               }
            } catch (IllegalStateException e) {
                mPlayer = null;
                e.printStackTrace();
            }
            //[BUGFIX]-Mod-END by TCTNB.Minjie.Cai 2016/3/4 PR-1706899
        }
    }

    /**
     * @hide
     */
    public static String convertUriToPath(Uri uri, Context mContext) {
         /*MODIFIED-BEGIN by rongdi.wang, 2016-03-30,BUG-1658013*/
        String path = null;
        String realpath = null;
        if (uri != null && uri.toString().startsWith("content://downloads/my_downloads/")){
            path = uri.toString();
            realpath = path.replaceAll("my","all");
            uri = Uri.parse(realpath);
        }
         /*MODIFIED-END by rongdi.wang,BUG-1658013*/
        if (TctDrmManagerClient.getInstance(mContext).isDrm(uri)) {
            if (null != uri) {
                String scheme = uri.getScheme();
                if (null == scheme || scheme.equals("") || scheme.equals(ContentResolver.SCHEME_FILE)) {
                    path = uri.getPath();
                } else if (scheme.equals("http")) {
                    path = uri.toString();
                } else if (scheme.equals(ContentResolver.SCHEME_CONTENT)) {
                    String[] projection = new String[] { MediaStore.MediaColumns.DATA };
                    Cursor cursor = null;
                    try {
                        cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
                        if (null == cursor || 0 == cursor.getCount() || !cursor.moveToFirst()) {
                            throw new IllegalArgumentException( "Given Uri could not be found" + "in media store");
                        }
                        int pathIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                        path = cursor.getString(pathIndex);
                    } catch (SQLiteException e) {
                        throw new IllegalArgumentException("Given Uri is not formatted in a way "
                                + "so that it can be found in media store.");
                    } finally {
                        if (null != cursor) {
                            cursor.close();
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Given Uri scheme is not supported");
                }
            }
            return path;
        } else {
            return uri.toString();
        }

    }
}
