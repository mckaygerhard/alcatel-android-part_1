package com.tct.drm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import android.app.WallpaperManager;
import android.app.IWallpaperManagerCallback;
import android.app.IWallpaperManager;
import android.content.Context;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.drm.DrmManagerClient;
import android.drm.DrmStore;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.ServiceManager;
import android.os.Message;
import android.os.Bundle;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.TctLog;

import com.tct.drm.TctDrmStore;

/**
 * {@hide}
 */
public final class TctWallpaperManager {
    private static String TAG = "TctWallpaperManager";
    //IAccesser mAccesser = null;

    public TctWallpaperManager() {
        //mAccesser = a;
    }

    public void willSendBroadcat(Context mContext) {
        if (TctDrmManagerClient.isDrmEnabled()) {
            ContentResolver cr = mContext.getContentResolver();
            Intent drmIntent = new Intent(TctDrmStore.ACITON_DRM_WALLPAPER_SET);
            String drmPath = Settings.System.getString(cr, TctDrmStore.NEW_WALLPAPER_DRMPATH);
            TctLog.d(TAG, "drmPath: " + drmPath);
            if (drmPath != null && drmPath.length() != 0) {
                drmIntent.putExtra("drmpath", drmPath);
            }
            mContext.sendBroadcast(drmIntent);
            TctSettings.putString(cr, TctDrmStore.NEW_WALLPAPER_DRMPATH, "", false, UserHandle.myUserId());
        }
    }

    public void ifSendBroadcat(Context mContext) {
        if (TctDrmManagerClient.isDrmEnabled()) {
            mContext.sendBroadcast(new Intent(TctDrmStore.ACITON_DRM_WALLPAPER_SET));
        }
    }

    public void setResource(int resid, boolean sendDrmBroadcat, Context mContext)
            throws IOException {
        try {
            Resources resources = mContext.getResources();
            ParcelFileDescriptor fd = setWallpaper("res:" + resources.getResourceName(resid));
            if (fd != null) {
                FileOutputStream fos = null;
                try {
                    fos = new ParcelFileDescriptor.AutoCloseOutputStream(fd);
                    setWallpaper(resources.openRawResource(resid), fos);
                    if (sendDrmBroadcat && TctDrmManagerClient.isDrmEnabled()) {
                        mContext.sendBroadcast(new Intent(TctDrmStore.ACITON_DRM_WALLPAPER_SET));
                    }
                } finally {
                    if (fos != null) {
                        fos.close();
                    }
                }
            }
        } finally {
        }
    }

    private ParcelFileDescriptor setWallpaper(String name) {
        IWallpaperManager mService;
        ParcelFileDescriptor fd = null;
        IBinder b = ServiceManager.getService(Context.WALLPAPER_SERVICE);
        mService = IWallpaperManager.Stub.asInterface(b);
        try {
            final Bundle result = new Bundle();
            final WallpaperSetCompletion completion = new WallpaperSetCompletion();
            fd = mService.setWallpaper(name, null, null, false, result, 1, completion);
        } catch (RemoteException e) {
        }
        return fd;
    }

	private void setWallpaper(InputStream data, FileOutputStream fos)
            throws IOException {
	    byte[] buffer = new byte[32768];
	    int amt;
	    while ((amt=data.read(buffer)) > 0) {
	        fos.write(buffer, 0, amt);
	 	}
	}

    private class WallpaperSetCompletion extends IWallpaperManagerCallback.Stub {
        final CountDownLatch mLatch;

        public WallpaperSetCompletion() {
            mLatch = new CountDownLatch(1);
        }

        public void waitForCompletion() {
            try {
                mLatch.await(30, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                // This might be legit: the crop may take a very long time. Don't sweat
                // it in that case; we are okay with display lagging behind in order to
                // keep the caller from locking up indeterminately.
            }
        }

        @Override
        public void onWallpaperChanged() throws RemoteException {
            mLatch.countDown();
        }
    }
}
