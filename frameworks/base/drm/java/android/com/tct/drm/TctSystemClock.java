package com.tct.drm;

import com.tct.drm.TctDrmManagerClient;

import android.drm.DrmManagerClient;
import android.os.SystemClock;
import android.text.format.Time;
import android.util.Log;

/**
 * {@hide}
 */
public final class TctSystemClock {
    public TctSystemClock() {
    }

    public boolean setCurrentTimeMillis(long millis) {
        long[] attr = { 0, 0 };
        TctDrmManagerClient pdmc = TctDrmManagerClient.getInstance(null);
        boolean result = pdmc.readSecureTimeFromFile(attr);
        Time time = new Time();
        time.setToNow();
        long currenttime = time.toMillis(false);
        if (result && (attr.length == 2) && (1 == attr[0])) {
            Log.d("Tct-SystemClock", "setCurrentTimeMillis flag = " + attr[0]
                    + " time = " + attr[1] + " timeInterval = "
                    + (millis - currenttime));
            //pdmc.writeSecureTimeToFile(1, (attr[1] - (millis - currenttime)));
        }

        return true;
    }

    public boolean setCurrentTimeMillis(long currentmillis,long diffMillis){
        long[] attr = { 0, 0 };
        TctDrmManagerClient pdmc = TctDrmManagerClient.getInstance(null);
        boolean result = pdmc.readSecureTimeFromFile(attr);
        if (result && (attr.length == 2) && (1 == attr[0])) {
            Log.d("TctSystemClock", "002 setCurrentTimeMillis flag = " + attr[0]
                    + " time = " + attr[1] + " timeInterval = "
                    + diffMillis);
            pdmc.writeSecureTimeToFile(1, (attr[1] - diffMillis));
        }

        return true;
    }

    public boolean setCurrentTimeMillis(long millis, boolean isNitzOrNet) {
        if (isNitzOrNet) {
            TctDrmManagerClient pdmc = TctDrmManagerClient.getInstance(null);
            pdmc.writeSecureTimeToFile(1, 0);
            return false;
        } else {
            return setCurrentTimeMillis(millis);
        }
    }

}
