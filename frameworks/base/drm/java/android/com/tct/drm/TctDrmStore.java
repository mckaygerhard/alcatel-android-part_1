package com.tct.drm;

/**
 * {@hide}
 */
public class TctDrmStore {

    /**
     * for store the drm wallpaper's path.
     * 
     * @hide
     */
    public static final String NEW_WALLPAPER_DRMPATH = "new_wallpaper_drmpath";
    /**
     * @hide
     */
    public static final String CURR_WALLPAPER_DRMPATH = "curr_wallpaper_drmpath";
    /**
     * @hide
     */
    public static final String DRMFILE_LOCKSCREEN = "drm_lockscreen";
    /**
     * @hide
     */
    public static final String DRMFILE_CLICKBOTH = "drm_clickboth";

    /**
     * M: Columns for Drm media files. @{
     * 
     * @hide
     */
    public interface DrmColumns {
        /**
         * @hide
         */
        public static final String TCT_IS_DRM = "tct_is_drm";
        /**
         * @hide
         */
        public static final String TCT_DRM_COUNT = "tct_drm_count";
        /**
         * @hide
         */
        public static final String TCT_DRM_TIME = "tct_drm_time";
        /**
         * @hide
         */
        public static final String TCT_DRM_INTERNAL = "tct_drm_internal";
        /**
         * @hide
         */
        public static final String TCT_DRM_RIGHT_TYPE = "tct_drm_right_type";
        /**
         * @hide
         */
        public static final String TCT_DRM_TYPE = "tct_drm_type";
        /**
         * Non-zero if the media file is drm-protected,and right valid
         * <P>
         * Type: INTEGER (boolean)
         * </P>
         * 
         * @hide
         */
        public static final String TCT_DRM_VALID = "tct_drm_valid";
        /**
         * @hide
         */
        public static final String TCT_DRM_RIGHT_ISSUER = "rights_issuer";

    }

    /**
     * @hide
     */
    public interface ConstraintsColumns {

        /**
         * content id of drm.
         * <p>
         * Type: TEXT
         * 
         * @hide
         */
        public static final String CONTENT_ID = "content_id";
        /**
         * Version of OMA drm, it is always 1.0 now
         * 
         * @hide
         */
        public static final String VERSION = "version";
        /**
         * DRM action, such as Action::PLAY, Action::DISPLAY, Action::EXECUTE
         * 
         * @hide
         */
        public static final String ACTION = "action";
        /**
         * DRM constraint type, such as count, datetime, interval
         * 
         * @hide
         */
        public static final String CONSTRAINT_TYPE = "constraint_type";

    }

    /**
     * Interface definition for the columns that represent DRM properties.
     * 
     * @hide
     */
    public interface PropertiesColumns {
        public static final String RIGHTS_ISSUER = "rights_issuer";
        public static final String CONTENT_VENDOR = "content_vendor";
        public static final String CONTENT_TYPE = "content_type";
        public static final String ENCRYPTION_METHOD = "encrption_method";
        public static final String CONTENT_NAME = "content_name";
        public static final String ICON_URI = "icon_uri";
    }

    /**
     * Defines status notifications for digital rights.
     * @hide
     */
    public static class RightsStatus {
        /**
         * native failure status Constant field signifies that the operation is
         * failed
         * 
         * @hide
         */
        public static final int FAILURE = -1;
        /**
         * add one more status, in the case of starting date is set as a time
         * pointer in future Constant field signifies that the rights will be
         * valid in a future time point
         * 
         * @hide
         */
        public static final int RIGHTS_PENDING = 0x04;
    }

    /**
     * Defines Mimetype for drm content e.g."application/vnd.oma.drm.message".
     * 
     * @hide
     */
    public static class MimeType {
        /** The "application/vnd.oma.drm.message" MIME type. */
        public static final String DRM_MIMETYPE_MESSAGE_STRING = "application/vnd.oma.drm.message";

        /** The "application/vnd.oma.drm.rights+xml" MIME type. */
        public static final String DRM_MIMETYPE_RIGHTS_XML_STRING = "application/vnd.oma.drm.rights+xml";

        /** The "application/vnd.oma.drm.rights+wbxml" MIME type. */
        public static final String DRM_MIMETYPE_RIGHTS_WBXML_STRING = "application/vnd.oma.drm.rights+wbxml";

        /** The "application/vnd.oma.drm.content" MIME type. */
        public static final String DRM_MIMETYPE_CONTENT_STRING = "application/vnd.oma.drm.content";

        /** The "application/vnd.oma.drm.dcf" MIME type. */
        public static final String DRM_MIMETYPE_DCF_STRING = "application/vnd.oma.drm.dcf";

        /** The "application/vnd.oma.dd+xml" MIME type. */
        public static final String DRM_MIMETYPE_DD_STRING = "application/vnd.oma.dd+xml";

        /** Three kind of engine. */
        public static final String DRM_MIMETYPE_FL_STRING = "forward-lock";
        public static final String DRM_MIMETYPE_CD_STRING = "combined-delivery";
        public static final String DRM_MIMETYPE_SD_STRING = "separate-delivery";
    }

    /**
     * @hide
     */
    public static class Scheme {
        /** The ID to designate non DRM-protected content. */
        public final static int DRM_SCHEME_NOT_PROTECTED = 0;

        /** The ID to designate OMA1 Forward Locked DRM content. */
        public final static int DRM_SCHEME_OMA1_FL = 1;

        /** The ID to designate OMA1 Combined Delivery DRM content. */
        public final static int DRM_SCHEME_OMA1_CD = 2;

        /** The ID to designate OMA1 Separate Delivery DRM content. */
        public final static int DRM_SCHEME_OMA1_SD = 3;
    }

    /**
     * @hide
     */
    public final static String[] sDrmType = {
            "", "forward-lock",
            "combined-delivery", "separate-delivery",
    };

    /**
     * @hide
     */
    public static class DrmBroadcastAction {
        public final static String DRM_TIME_OUT_ACTION = "drm_time_out_action";
        public final static String DRM_RECEIVE_RIGHT_ACTION = "drm_receive_right_action";
    }

    /**
     * @hide
     */
    public static final String ACITON_DRM_RINGTONE_SET ="jrdcom.intent.action.ACTION_DRM_RINGTONE_SET";

     /**
     * @hide
     */
    public static final String ACITON_DRM_MEDIA_HAS_EXPIRE_TIME ="jrdcom.intent.action.ACITON_DRM_MEDIA_HAS_EXPIRE_TIME";
    /**
    * @hide
    */
    public static final String ACITON_DRM_WALLPAPER_SET = "jrdcom.intent.action.ACTION_DRM_WALLPAPER_SET";

}
