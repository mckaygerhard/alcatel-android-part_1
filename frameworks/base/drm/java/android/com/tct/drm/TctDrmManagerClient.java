package com.tct.drm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.NullPointerException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import android.app.WallpaperManager;
import android.content.Context;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.drm.DrmConvertedStatus;
import android.drm.DrmErrorEvent;
import android.drm.DrmEvent;
import android.drm.DrmInfo;
import android.drm.DrmInfoEvent;
import android.drm.DrmInfoRequest;
import android.drm.DrmInfoStatus;
import android.drm.DrmManagerClient;
import android.drm.DrmRights;
import android.drm.DrmStore;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.hardware.display.DisplayManager;
import android.hardware.display.WifiDisplayStatus;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.SystemProperties;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.MediaStore.TctDrmMediaColumns;
import android.provider.Settings;
import android.view.Surface;
import android.widget.Toast;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.tct.drm.DrmUri;
import com.tct.drm.TctDrmLog;

/**
 * The main programming interface for the TCT-DRM framework. An application must
 * instantiate this class to access DRM agents through the TCT-DRM framework.
 * {@hide}
 */
public class TctDrmManagerClient {
    /**
     * @hide
     */
    public static final int ERROR_NONE = 0;
    /**
     * @hide
     */
    public static final int ERROR_UNKNOWN = -2000;

    private static final String TAG = "TctDrmManagerClient";
    private static final int GALLERY_DRM_SIZE = 108;
    private static final int FILEMANAGER_DRM_SIZE = 48;
    private static final int ACTION_REMOVE_ALL_RIGHTS = 1001;
    private static final int ACTION_PROCESS_DRM_INFO = 1002;
    private static int mUniqueId;
    private static boolean isTctDrmEnable = false;
    private static Context mContext = null;
    private static DisplayManager mDisplayManager;
    private static ContentResolver mCr;
    private static DrmManagerClient mDrmClient = null;
    private static TctDrmManagerClient pDrmClient = null;

    private int mNativeContext;
    private boolean mReleased;
    private OnInfoListener mOnInfoListener;
    private OnEventListener mOnEventListener;
    private OnErrorListener mOnErrorListener;
    private InfoHandler mInfoHandler;
    private EventHandler mEventHandler;
    private String mUri;
    private String mFilePath;

    HandlerThread mInfoThread;
    HandlerThread mEventThread;

    static{
        if (SystemProperties.get("feature.tct.drm.enabled", "false")
                .equalsIgnoreCase("true") || SystemProperties.get("feature.tct.drm.enabled", "0")
                .equalsIgnoreCase("1")) {
            isTctDrmEnable = true;
        }
    }

    /**
     * Interface definition for a callback that receives status messages and
     * warnings during registration and rights acquisition.
     * @hide
     */
    public interface OnInfoListener {
        /**
         * Called when the DRM framework sends status or warning information
         * during registration and rights acquisition.
         *
         * @param client The <code>DrmManagerClient</code> instance.
         * @param event The {@link DrmInfoEvent} instance that wraps the status information or warnings.
         */
        public void onInfo(TctDrmManagerClient client, DrmInfoEvent event);
    }

    /**
     * Interface definition for a callback that receives information about DRM
     * processing events.
     * @hide
     */
    public interface OnEventListener {
        /**
         * Called when the DRM framework sends information about a DRM
         * processing request.
         *
         * @param client The <code>DrmManagerClient</code> instance.
         * @param event The {@link DrmEvent} instance that wraps the information
         *            being conveyed, such as the information type and message.
         */
        public void onEvent(TctDrmManagerClient client, DrmEvent event);
    }

    /**
     * Interface definition for a callback that receives information about DRM
     * framework errors.
     * @hide
     */
    public interface OnErrorListener {
        /**
         * Called when the DRM framework sends error information.
         *
         * @param client The <code>DrmManagerClient</code> instance.
         * @param event The {@link DrmErrorEvent} instance that wraps the error
         *            type and message.
         */
        public void onError(TctDrmManagerClient client, DrmErrorEvent event);
    }

    private class EventHandler extends Handler {

        public EventHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            DrmEvent event = null;
            DrmErrorEvent error = null;
            HashMap<String, Object> attributes = new HashMap<String, Object>();

            switch (msg.what) {
            case ACTION_PROCESS_DRM_INFO: {
                final DrmInfo drmInfo = (DrmInfo) msg.obj;
                DrmInfoStatus status = mDrmClient._processDrmInfo(mUniqueId,drmInfo);
                attributes.put(DrmEvent.DRM_INFO_STATUS_OBJECT, status);
                attributes.put(DrmEvent.DRM_INFO_OBJECT, drmInfo);

                if (null != status && DrmInfoStatus.STATUS_OK == status.statusCode) {
                    event = DrmEvent.getDrmEvent(mUniqueId, getEventType(status.infoType), null, attributes);
                } else {
                    int infoType = (null != status) ? status.infoType : drmInfo.getInfoType();
                    error = new DrmErrorEvent(mUniqueId, getErrorType(infoType), null, attributes);
                }
                break;
            }
            case ACTION_REMOVE_ALL_RIGHTS: {
                if (ERROR_NONE == mDrmClient._removeAllRights(mUniqueId)) {
                    event = DrmEvent.getDrmEvent(mUniqueId, DrmEvent.TYPE_ALL_RIGHTS_REMOVED, null);
                } else {
                    error = new DrmErrorEvent(mUniqueId, DrmErrorEvent.TYPE_REMOVE_ALL_RIGHTS_FAILED, null);
                }
                break;
            }
            default:
                TctDrmLog.e(TAG, "Unknown message type " + msg.what);
                return;
            }
            if (null != mOnEventListener && null != event) {
                mOnEventListener.onEvent(TctDrmManagerClient.this, event);
            }
            if (null != mOnErrorListener && null != error) {
                mOnErrorListener.onError(TctDrmManagerClient.this, error);
            }
        }
    }

    private class InfoHandler extends Handler {
        public static final int INFO_EVENT_TYPE = 1;

        public InfoHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            DrmInfoEvent info = null;
            DrmErrorEvent error = null;

            switch (msg.what) {
            case InfoHandler.INFO_EVENT_TYPE:
                int uniqueId = msg.arg1;
                int infoType = msg.arg2;
                String message = msg.obj.toString();

                switch (infoType) {
                case DrmInfoEvent.TYPE_REMOVE_RIGHTS: {
                    try {
                        TctDrmUtils.removeFile(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    info = new DrmInfoEvent(uniqueId, infoType, message);
                    break;
                }
                case DrmInfoEvent.TYPE_ALREADY_REGISTERED_BY_ANOTHER_ACCOUNT:
                case DrmInfoEvent.TYPE_RIGHTS_INSTALLED:
                case DrmInfoEvent.TYPE_WAIT_FOR_RIGHTS:
                case DrmInfoEvent.TYPE_ACCOUNT_ALREADY_REGISTERED:
                case DrmInfoEvent.TYPE_RIGHTS_REMOVED: {
                    info = new DrmInfoEvent(uniqueId, infoType, message);
                    break;
                }
                default:
                    error = new DrmErrorEvent(uniqueId, infoType, message);
                    break;
                }

                if (null != mOnInfoListener && null != info) {
                    mOnInfoListener.onInfo(TctDrmManagerClient.this, info);
                }
                if (null != mOnErrorListener && null != error) {
                    mOnErrorListener.onError(TctDrmManagerClient.this, error);
                }
                return;
            default:
                TctDrmLog.e(TAG, "Unknown message type " + msg.what);
                return;
            }
        }
    }

    /**
     * App should get TctDrmManagerClient with this API instead of create
     * TctDrmManagerClient everywhere
     *
     * @param context
     * @modify
     *         synchronized for single instance.for online drm media files,can't
     *         call this mehod.because for single instance use this mehod
     *         {@link #setOnEventListener(OnEventListener)}
     *         {@link #setOnErrorListener(OnErrorListener)}
     *         {@link #setOnInfoListener(OnInfoListener)}will cause problem.
     *
     * @hide
     */
    public synchronized static TctDrmManagerClient getInstance(Context context) {
        if (pDrmClient == null) {
            pDrmClient = new TctDrmManagerClient(context, false);
        }
        if (pDrmClient.mContext == null && null != context) {
            pDrmClient.setContext(context);
        }
        return pDrmClient;
    }

    /**
     * for more effect. don't start the thread. now,we know only online drm
     * stream will use the Thread. also this method is hide, no one know that
     * except ourself.where the method use should make sure it's not for check
     * online drm media.
     *
     * @param context
     * @param needThreads true for start the threads. false not start.
     * @hide
     */
    public TctDrmManagerClient(Context context, boolean needThreads) {
        TctDrmLog.v(TAG, "enter TctDrmManagerClient");
        if (needThreads) {
            HandlerThread infoThread = new HandlerThread("DrmManagerClient.InfoHandler");
            infoThread.start();
            mInfoHandler = new InfoHandler(infoThread.getLooper());
            HandlerThread eventThread = new HandlerThread("DrmManagerClient.EventHandler");
            eventThread.start();
            mEventHandler = new EventHandler(eventThread.getLooper());
        }

        if (null != context) {
            setContext(context);
        }

        if (mDrmClient == null) {
            mDrmClient = new DrmManagerClient(mContext);
        }

        mUniqueId = mDrmClient.tct_initialize(new WeakReference<DrmManagerClient>(mDrmClient));
    }

    /**
     * Checks the fd is drm protected or not.
     * @param fd which we want to check the protected content's file descriptor.
     * @return True if the content is DRM protected and false otherwise.
     * @hide
     */
    public boolean isDrm(FileDescriptor fd) {
        if (!isTctDrmEnable || null == fd) {
            return false;
        }
        return canHandle(fd);
    }

    /**
     * Checks whether the contents in the given buffer is DRM protected.
     *
     * @param b The buffer containing the data.
     * @param off The start offset of the data within the buffer.
     * @param len The total number of bytes in the buffer.
     * @return True if the content is DRM protected and false otherwise.
     * @throws NullPointerException, ArrayIndexOutOfBoundsException if an error occurs within the native DRM Agent.
     * @hide
     */
    public boolean isDrm(byte[] b, int off, int len) throws NullPointerException, ArrayIndexOutOfBoundsException {
        boolean result = false;
        String header = new String(b, 0, 4);
        TctDrmLog.d(TAG, "is_Drm E header:" + header);
        if (header != null && header.equals("FWLK")) {
            TctDrmLog.d(TAG, "is_Drm E FL or CD");
            result = true;
        } else {
            /**
             * SD raw content is same as non-raw content, the following codes
             * can handle all types of sd drm content
             */
            int scheme = getRawDrmScheme(b, off, len);
            TctDrmLog.d(TAG, "is_Drm SD  scheme:" + scheme);
            if (scheme != TctDrmMediaColumns.DrmScheme.DRM_SCHEME_NOT_PROTECTED) {
                result = true;
            }
        }

        TctDrmLog.d(TAG, "is_Drm X result:" + result);
        return result;
    }

    /**
     * Checks the content is drm protected or not.
     *
     * @param filePath the protected content's path
     * @return True if the content is DRM protected and false otherwise.
     * @hide
     */
    public boolean isDrm(String filePath) {
        boolean result = false;
        if (!isTctDrmEnable || TextUtils.isEmpty(filePath)) {
            return result;
        }

        if (filePath.startsWith("http://") || filePath.startsWith("https://")
                || filePath.startsWith("rtsp://")) {
            return result;
        }

        filePath = checkFilePathAndStandardized(filePath);
        FileInputStream fis = null;

        try{
            fis = new FileInputStream(filePath);
            byte[] buffer = new byte[1000];
            int count = fis.read(buffer);

            for (int i = 0; i < count - 4; i++) {
                if (((buffer[i] ^ 0x46) == 0) && ((buffer[i + 1] ^ 0x57) == 0)
                        && ((buffer[i + 2] ^ 0x4c) == 0) && ((buffer[i + 3] ^ 0x4b) == 0)) {
                    TctDrmLog.d(TAG, "This is an FL or CD content path=" + filePath);
                    result = true;
                }
            }

            if (TctDrmMediaColumns.DrmScheme.DRM_SCHEME_NOT_PROTECTED != getRawDrmScheme(buffer, 0, count)) {
                TctDrmLog.d(TAG, "in isDrm(String), SD type be return");
                result = true;
            }

        } catch(Exception e) {
            TctDrmLog.e(TAG,"Exception = " + e);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;
        }
    }

    /**
     * Checks the content is drm protected or not.
     *
     * @param uri the protected content's path
     * @return True if the content is DRM protected and false otherwise.
     * @hide
     */
    public boolean isDrm(Uri uri) {
      //TODO: change the method from C/C++ to java
        boolean result = false;
        String filePath = null;
        if (!isTctDrmEnable || null == uri || Uri.EMPTY == uri) {
            return result;
        }

        try {
            filePath = convertUriToPath(uri);
        } catch(IllegalArgumentException e){
            TctDrmLog.d(TAG,"Given Uri scheme is not supported IllegalArgumentException");
        } catch (UnsupportedOperationException e) {
            TctDrmLog.d(TAG,"Given Uri scheme is not supported UnsupportedOperationException");
            return false;
        }

        if (!isTctDrmEnable || null == filePath || TextUtils.isEmpty(filePath)) {
            return result;
        }

        return isDrm(filePath);
    }

    /**
     * check the path,fd or uri can be supported WIFI display or not .
     * one of the path fd or uri was valid,this method be used normal
     * @param path the protected content's path
     * @param fd the protected contents's file descriptor
     * @param uri the protected contents's uri
     * @return true for support WIFI display false for NOT support WIFI display
     * @hide
     */
    public static boolean doWithWifidisplay(String path, FileDescriptor fd, Uri uri) {
        if ((null == mContext) || !isTctDrmEnable) {
            return true;
        }

        if (pDrmClient == null) {
            pDrmClient = new TctDrmManagerClient(mContext, false);
        }

        if (pDrmClient.isDrm(path) || pDrmClient.isDrm(fd) || pDrmClient.isDrm(uri)) {
            mDisplayManager = (DisplayManager) mContext.getSystemService(Context.DISPLAY_SERVICE);
            WifiDisplayStatus mWifiDisplayStatus = mDisplayManager.getWifiDisplayStatus();

            final ContentResolver resolver = mContext.getContentResolver();
            //[FEATURE]Add-BEGIN by TCTNB.Rongdi.Wang 2015/12/1,Task 1004317
            int HDCP_ENABLE = Settings.Global.getInt(resolver,Settings.Global.TCT_HDCP_DRM_NOTIFY,0);
            //int HDCP_ENABLE = 0;
            //[FEATURE]Add-END by TCTNB.Rongdi.Wang 2015/12/1,Task 1004317
            if ((mWifiDisplayStatus.getActiveDisplayState() == mWifiDisplayStatus.DISPLAY_STATE_CONNECTED) && (0 == HDCP_ENABLE)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Sometime ,this.mContext may null;
     * add this method for the glue from
     * DrmManagerClient.java to TctDrmManagerClient.java
     * @hide
     * @param context
     * @deprecated
     */
    public static void setmContext(Context context) {
        if (context != null) {
            mContext = context;
        }

        if (mDrmClient == null) {
            mDrmClient = new DrmManagerClient(mContext);
        }

        mCr = mContext.getContentResolver();
    }

    /**
     * Sometime ,this.mContext may null;
     * @param context context
     * @hide
     */
    public void setContext(Context context) {
        mContext = context;
        mCr = mContext.getContentResolver();
    }

    /**
     * Retrieves list of constraint information for rights-protected content.
     *
     * @param path Path to the content from which you are retrieving DRM constraints.
     * @param action Action defined in {@link DrmStore.Action}.
     * @return A {@link android.content.ContentValues} instance that list of
     *         contains key-value pairs representing the constraints. Null in
     *         case of failure.
     * @hide
     */
    public ArrayList<ContentValues> getConstraintsList(String path, int action) {
        if (null == path || path.equals("") || !TctDrmUtils.Action.isValid(action)) {
            throw new IllegalArgumentException("Given usage or path is invalid/null");
        }
        TctDrmLog.d(TAG, "getConstraintsList ---path:" + path + "|action:" + action);

        return mDrmClient.tct_getConstraintsList(mUniqueId, path, action);
    }

    /**
     * Retrieves list of constraint information for rights-protected content.
     *
     * @param uri URI for the content from which you are retrieving DRM constraints.
     * @param action Action defined in {@link DrmStore.Action}.
     * @return A {@link android.content.ContentValues} instance that list of
     *         contains key-value pairs representing the constraints. Null in
     *         case of failure.
     * @hide
     */
    public ArrayList<ContentValues> getConstraintsList(Uri uri, int action) {
        if (null == uri || Uri.EMPTY == uri) {
            throw new IllegalArgumentException("Uri should be non null");
        }

        TctDrmLog.d(TAG, "getConstraintsList ---uri:" + uri + "|action:" + action);
        return getConstraintsList(convertUriToPath(uri), action);
    }

    /**
     * Retrieves constraint information for rights-protected content.
     *
     * @param uri URI for the content from which you are retrieving DRM
     *            constraints.
     * @param action Action defined in {@link DrmStore.Action}.
     *
     * @return A {@link android.content.ContentValues} instance that contains
     *         key-value pairs representing the constraints. Null in case of
     *         failure.
     * @hide
     */
    public static ContentValues getConstraints(Uri uri, int action) {
        ContentValues result = null;
        ParcelFileDescriptor pfd = null;
        FileDescriptor fd = null;
        if (mCr == null) {
            return result;
        }
        if(uri.toString().startsWith("http://")){
            return result;
        }
        if (isTctDrmEnable) {
            try {
                pfd = mCr.openFileDescriptor(uri, "r");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return result;
            }
            fd = pfd.getFileDescriptor();
            if (fd == null) {
                TctDrmLog.e(TAG, "LZH pfd.getFileDescriptor() == NULL!");
            } else {
                TctDrmLog.d(TAG,"fd valid =" + fd.valid() + ",fd.getInt = "+ fd.toString());
                result = mDrmClient.tct_getConstraintsByFd(mUniqueId, fd, action);
            }
            try {
                pfd.close();
                pfd = null;
                fd = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            result = mDrmClient.getConstraints(convertUriToPath(uri), action);
        }
        return result;
    }

    /**
     * Retrieves constraint information for rights-protected content.
     * this method wrapper for {@link DrmManagerClient#getConstraints(String, int)}
     * @hide
     * @deprecated
     */
    public ContentValues getConstraints(String path, int action){
        return mDrmClient.getConstraints(path,action);
    }

    /**
     * Saves rights to a specified path and associates that path with the content path.
     * this method wrapper for {@link DrmManagerClient#saveRights(DrmRights, String, String)}
     * @hide
     */
    public int saveRights(DrmRights drmRights, String rightsPath, String contentPath) throws IOException {
        return mDrmClient.saveRights(drmRights,rightsPath,contentPath);
    }

   /**
    * Retrieves metadata information for rights-protected content.
    * this method wrapper for {@link DrmManagerClient#getMetadata(String)}
    * @hide
    * @deprecated
    */
    public ContentValues getMetadata(String path){
        return mDrmClient.getMetadata(path);
    }

    /**
     * Retrieves constraint information for rights-protected content.
     *
     * @param fd file descriptor for the content from which you are retrieving
     *            DRM constraints.
     * @param action Action defined in {@link DrmStore.Action}.
     * @hide
     * @return A {@link android.content.ContentValues} instance that contains
     *         key-value pairs representing the constraints. Null in case of
     *         failure.
     */
    public ContentValues getConstraints(FileDescriptor fd, int action) {
        ContentValues result = null;
        if (!fd.valid() || !isTctDrmEnable) {
            TctDrmLog.e(TAG, "pfd.getFileDescriptor() == NULL! or not suport drm");
            return result;
        }
        result = mDrmClient.tct_getConstraintsByFd(mUniqueId, fd, action);

        return result;
    }

    /**
     * Checks whether the given MIME type or URI can be handled.
     * this method wrapper for DrmManagerClient#canHandle(String,String)
     * @hide
     */
    public static boolean canHandle(String path, String mimeType) {
        return mDrmClient.canHandle(path,mimeType);
    }

    /**
     * check content datas can handled. if the content data are from drm file
     * return the file's mimetype that is used for
     * DrmManagerClient#openConvertSession() or return NULL
     *
     * @param data check data to handled.
     * @return check data to return mime type of data String or NULL
     * @hide
     */
    public String canHandle(byte[] data, int datalength) {
        if (!isTctDrmEnable) {
            return null;
        }
        if (data == null || datalength == 0 || data.length < datalength) {
            return null;
        }
        byte[] input;
        if (data.length != datalength) {
            input = new byte[datalength];
            System.arraycopy(data, 0, input, 0, datalength);
        } else {
            input = data;
        }

        return mDrmClient.tct_canHandleData(mUniqueId,input);
    }

    /**
     * Checks whether the given MIME type or URI can be handled.
     *
     * @param uri URI for the content to be handled.
     * @param mimeType MIME type of the object to be handled
     *
     * @return True if the given MIME type or URI can be handled; false if they
     *         cannot be handled.
     * @hide
     */
    public static boolean canHandle(Uri uri, String mimeType) {
        if ((null == uri || Uri.EMPTY == uri)
                && (null == mimeType || mimeType.equals(""))) {
            throw new IllegalArgumentException(
                    "Uri or the mimetype should be non null");
        }
        if (uri == null || uri == Uri.EMPTY) {
            return mDrmClient._canHandle(mUniqueId, null, mimeType);
        }
        if (!isTctDrmEnable) {
            return mDrmClient.canHandle(convertUriToPath(uri), mimeType);
        }
        if (mCr == null) {
            return false;
        }
        ParcelFileDescriptor pfd = null;
        FileDescriptor fd = null;
        boolean result = false;

        if (uri.getScheme() != null) {
            if (uri.getScheme().equals("content") || uri.getScheme().equals("file")) {
                try {
                    TctDrmLog.e(TAG, "canHandle uri=" + uri);
                    pfd = mCr.openFileDescriptor(uri, "r");
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }

        if (pfd != null) {
            fd = pfd.getFileDescriptor();
            if (fd == null) {
                TctDrmLog.d(TAG, "pfd.getFileDescriptor() == NULL!");
            } else {
                TctDrmLog.d(TAG,"fd valid =" + fd.valid() + ",fd.getInt="+ fd.toString());
                result = mDrmClient.tct_canHandleFd(mUniqueId,fd);
            }
            try {
                pfd.close();
                pfd = null;
                fd = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    /**
     * Checks whether the fd can be handled.
     *
     * @param fd file descriptor for the content to be handled.
     * @return True if the given MIME type or URI can be handled; false if they
     *         cannot be handled.
     * @hide
     */
    public boolean canHandle(FileDescriptor fd) {
        if (!fd.valid() || !isTctDrmEnable) {
            TctDrmLog.d(TAG, "pfd.getFileDescriptor() is invalid!");
            return false;
        }
        return mDrmClient.tct_canHandleFd(mUniqueId,fd);
    }

    /**
     * Check whether the given content has valid rights.
     *
     * @param uri URI of the rights-protected content.
     *
     * @return An <code>int</code> representing the
     *         {@link DrmStore.RightsStatus} of the content.
     * @hide
     */
    public static int checkRightsStatus(Uri uri) {
        if (null == uri || Uri.EMPTY == uri) {
            throw new IllegalArgumentException("Given uri is not valid");
        }
        if(uri.toString().startsWith("http://")){
            return DrmStore.RightsStatus.RIGHTS_VALID;
        }
        return checkRightsStatus(uri, DrmStore.Action.DEFAULT);
    }

    /**
     * Check whether the given content has valid rights.
     *
     * @param fd file descriptor of the rights-protected content.
     * @hide
     * @return An <code>int</code> representing the
     *         {@link DrmStore.RightsStatus} of the content.
     */
    public int checkRightsStatus(FileDescriptor fd) {
        if (!fd.valid() || !isTctDrmEnable) {
            return DrmStore.RightsStatus.RIGHTS_INVALID;
        }

        return mDrmClient.tct_checkRightsStatusFd(mUniqueId, fd, DrmStore.Action.DEFAULT);
    }

    /**
     * Checks whether the given rights-protected content has valid rights for
     * the specified {@link DrmStore.Action}.
     *
     * @param uri URI for the rights-protected content.
     * @param action The {@link DrmStore.Action} to perform.
     *
     * @return An <code>int</code> representing the
     *         {@link DrmStore.RightsStatus} of the content.
     * @hide
     */
    public static int checkRightsStatus(Uri uri, int action) {
        if (null == uri || Uri.EMPTY == uri) {
            throw new IllegalArgumentException("Given uri is not valid");
        }

        if(uri.toString().startsWith("http://")){
            return DrmStore.RightsStatus.RIGHTS_VALID;
        }

        if (!isTctDrmEnable) {
            return mDrmClient.checkRightsStatus(convertUriToPath(uri), action);
        }

        ParcelFileDescriptor pfd = null;
        if (mCr == null) {
            return DrmStore.RightsStatus.RIGHTS_INVALID;
        }

        try {
            pfd = mCr.openFileDescriptor(uri, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return DrmStore.RightsStatus.RIGHTS_INVALID;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return DrmStore.RightsStatus.RIGHTS_INVALID;
        }

        int result = DrmStore.RightsStatus.RIGHTS_INVALID;
        if (pfd != null) {
            FileDescriptor fd = pfd.getFileDescriptor();
            if (null != fd) {
                result = mDrmClient.tct_checkRightsStatusFd(mUniqueId, fd, action);
            }

            try {
                pfd.close();
                pfd = null;
                fd = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    /**
     * @hide
     */
    public static DrmConvertedStatus convertData(int convertId,
            byte[] inputData, Context context) {
        if (mDrmClient == null) {
            mDrmClient = new DrmManagerClient(context);
        }

        if (null == inputData || 0 >= inputData.length) {
            throw new IllegalArgumentException("Given inputData should be non null");
        }
        int scheme = getRawDrmScheme(inputData, 0, inputData.length);
        if (scheme == TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_CD) {
            DrmRights dr = new DrmRights(inputData,
                    TctDrmMediaColumns.DrmMimeType.DRM_MIMETYPE_RIGHTS_XML_STRING);
            try {
                if (mDrmClient.saveRights(dr, null, null) < 0) {
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return mDrmClient._convertData(mUniqueId, convertId, inputData);
    }

    /**
     * This method expects uri in the following format
     * content://media/<table_name>/<row_index> (or) file://sdcard/test.mp4
     * http://test.com/test.mp4
     *  use {@link Uri#convertUriToPath(Uri, Context)} Here <table_name>
     *             shall be "video" or "audio" or "images" <row_index> the index
     *             of the content in given table
     * @hide
     */
    public static String convertUriToPath(Uri uri) {
        String path = null;
        if (null != uri) {
            String scheme = uri.getScheme();
            if (null == scheme || scheme.equals("")
                    || scheme.equals(ContentResolver.SCHEME_FILE)) {
                path = uri.getPath();
            } else if (scheme.equals("http") || scheme.equals("https") ||scheme.equals("rtsp")) {
                path = uri.toString();
            } else if (DocumentsContract.isDocumentUri(mContext, uri)) {
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                } else if (isDownloadsDocument(uri)) {
                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                    return getDataColumn(mContext, contentUri, null, null);
                } else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                    if (contentUri != null) {
                        contentUri = contentUri.withAppendedPath(contentUri, split[1]);
                        return getDataColumn(mContext, contentUri, null, null);
                    } else {
                        path = uri.getPath();
                    }
                }
            } else if (scheme.equals(ContentResolver.SCHEME_CONTENT)) {
                String[] projection = new String[] { MediaStore.MediaColumns.DATA };
                Cursor cursor = null;
                try {
                    cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
                    if (null == cursor || 0 == cursor.getCount() || !cursor.moveToFirst()) {
                        TctDrmLog.d(TAG, "Given Uri could not be found in media store");
                        return null;
                    }
                    int pathIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                    path = cursor.getString(pathIndex);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (null != cursor) {
                        cursor.close();
                    }
                }
            } else {
                path = uri.getPath();
            }
        }
        return path;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     * @hide
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
            String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
            column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                String path = cursor.getString(index);
                if (path != null && path.endsWith("RAW")) {
                    List<String> segments = uri.getPathSegments();
                    String dbName = segments.get(0);
                    String id = segments.get(1);
                    path = mContext.getDatabasePath(dbName + "_att") + "/" + id;
                }
                return path;
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     * @hide
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     * @hide
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     * @hide
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * Get file name when download drm content; if file already existed, add
     * index number to end of file base name;
     */
    private String getNewFileName(String oldName, String extension) {
        int index = oldName.lastIndexOf(".");
        String surfix = null;
        String newFile = null;
        if (index != -1) {
            surfix = oldName.substring(0, index);
        } else {
            surfix = oldName;
        }
        newFile = surfix + extension;
        if (new File(newFile).exists()) {
            int seq = 0;
            do {
                seq++;
                newFile = surfix + "_" + seq + extension;
            } while (new File(newFile).exists());
        }
        return newFile;
    }

    /**
     * return converted file name if convert successfully and return null .
     * otherwise this method can convert normal drm media to encyped drm format.
     *
     * @hide
     */
    public String doRawDrmConvert(String path) {
        if (!isRawDrmContent(path))
            return null;
        String mimeType = getRawMimeType(path);
        TctDrmLog.d(TAG, " filepath=: " + path + ", mimeType = " + mimeType);
        String extension = TctDrmUtils.getExtensionFromMime(mimeType);
        TctDrmLog.d(TAG, "filepath=: " + path + ", extension = " + extension);
        if (null == extension)
            return null;
        String newFileName = getNewFileName(path, "." + extension);
        TctDrmLog.d(TAG, "filepath=: " + path + ", newFileName = " + newFileName);
        int drmScheme = getDrmScheme(path);
        if (drmScheme == TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_SD) {
            if (new File(path).renameTo(new File(newFileName)))
                return newFileName;
        }
        int convertId = 0;
        try {
            convertId = mDrmClient.openConvertSession(TctDrmMediaColumns.DrmType[drmScheme]);
            if (convertId < 0) {
                TctDrmLog.d(TAG, "open convert session failed");
                return null;
            }
        } catch (Exception e) {
            TctDrmLog.d(TAG,"exception happen when open convert session:"+ e.getMessage());
            return null;
        }

        FileInputStream fis = null;
        FileOutputStream fos = null;
        DrmConvertedStatus Status = null;
        boolean isConvertSuccess = false;
        try {
            fis = new FileInputStream(path);
            fos = new FileOutputStream(newFileName);
            int len = 0;
            int BUFFER_SIZE = 8192;
            boolean isEnd = false;
            do {
                byte[] buffer = new byte[BUFFER_SIZE];
                Status = null;
                len = 0;
                len = fis.read(buffer);
                if (len > 0 && len == BUFFER_SIZE) {
                    Status = convertData(convertId, buffer, mContext);
                } else if (len > 0 && len < BUFFER_SIZE) {
                    byte[] inBuffer = new byte[len];
                    System.arraycopy(buffer, 0, inBuffer, 0, len);
                    Status = convertData(convertId, inBuffer, mContext);
                } else if (len == -1) {
                    isEnd = true;
                    Status = mDrmClient.closeConvertSession(convertId);
                }
                if (Status != null
                        && Status.statusCode == DrmConvertedStatus.STATUS_OK
                        && Status.convertedData != null) {
                    byte[] b = Status.convertedData;
                    if (isEnd) {
                        RandomAccessFile rndAccessFile = null;
                        try {
                            rndAccessFile = new RandomAccessFile(newFileName,"rw");
                            rndAccessFile.seek(Status.offset);
                            rndAccessFile.write(b);
                            break;
                        } catch (Exception e) {
                            throw e;
                        } finally {
                            if (null != rndAccessFile) {
                                try {
                                    rndAccessFile.close();
                                } catch (Exception e) {
                                    TctDrmLog.e(TAG, "first ," + e);
                                }
                            }
                        }
                    } else {
                        fos.write(b);
                    }
                } else {
                    throw new Exception("convert failed");
                }
            } while (len > 0 && !isEnd);
            isConvertSuccess = true;
        } catch (Exception e) {
            TctDrmLog.e(TAG, "second," + e);
        } finally {
            try {
                if (null != fos)
                    fos.close();
                if (null != fis)
                    fis.close();
            } catch (IOException e) {
                TctDrmLog.e(TAG, "third," + e);
            }
        }
        if (!isConvertSuccess)
            new File(newFileName).delete();
        return isConvertSuccess ? newFileName : null;
    }

    /**
     * get raw mimeType from the file
     *
     * @hide
     * */
    private String getRawMimeType(String path) {
        String mimetype = null;
        if (isRawDrmContent(path)) {
            byte[] buffer = new byte[1024];
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(path);
                fis.read(buffer);
                if (isSdType(path)) {
                    int contenttype_index = 3;
                    byte sdtype_length_byte = buffer[1];
                    int ctype_length = (sdtype_length_byte & 0xFF);
                    TctDrmLog.i(TAG, "sdtype_length  " + ctype_length);
                    if (ctype_length > 0) {
                        byte[] ctype_byte_array = new byte[ctype_length];
                        for (int i = 0; i < ctype_length; i++) {
                            ctype_byte_array[i] = buffer[contenttype_index + i];
                        }
                        String content_type = new String(ctype_byte_array);
                        TctDrmLog.i(TAG, "content type for " + path+ " content type:= " + content_type);
                        if (content_type != null)
                            return content_type;
                    }
                }

                String data = (new String(buffer)).toLowerCase();
                String str_content = "content-type: ";
                String[] strs_media = { "audio/", "video/", "image/" };
                int i = 0;
                while (i++ != strs_media.length && -1 == data.indexOf(str_content + strs_media[i - 1]))
                    ;

                if (i != strs_media.length + 1) {
                    int index_begin = data.indexOf(str_content
                            + strs_media[i - 1])
                            + (str_content + strs_media[i - 1]).length();
                    int index_end = index_begin;
                    while (data.charAt(index_end) != ';'
                            && data.charAt(index_end) != '\r'
                            && data.charAt(index_end) != '\n'
                            && ++index_end < data.length())
                        ;

                    if (index_end < data.length()) {
                        StringBuffer sb = new StringBuffer();
                        sb.append(strs_media[i - 1]);
                        sb.append(data.substring(index_begin, index_end));
                        mimetype = sb.toString().toLowerCase();
                    }
                }
            } catch (FileNotFoundException e) {
                TctDrmLog.e(TAG, "" + e);
            } catch (IOException e) {
                TctDrmLog.e(TAG, "" + e);
            } finally {
                if (null != fis) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        TctDrmLog.e(TAG, "" + e);
                    }
                }
            }
        }
        TctDrmLog.d(TAG, "mimetype = " + mimetype);
        return mimetype;
    }

    /**
     * Judge whether a drm content is un-processed or not
     *
     * @hide
     */
    private static boolean isRawDrmContent(String filePath) {
        boolean result = false;

        if (TextUtils.isEmpty(filePath)) {
            return false;
        }

        int index = filePath.lastIndexOf(".");
        if (index >= 0) {
            String ext = filePath.substring(index + 1);
            if (ext.equalsIgnoreCase("dm") || ext.equalsIgnoreCase("dcf")
                    || ext.equalsIgnoreCase("fl")) {
                result = true;
            }
        }
        return result;
    }

    /**
     * retrieve drm content scheme
     *
     * @return responding scheme
     * {@link TctDrmMediaColumns.DrmScheme};
     * @hide
     */
    public static int getDrmScheme(byte[] data, int offset, int length) {
        int scheme = TctDrmMediaColumns.DrmScheme.DRM_SCHEME_NOT_PROTECTED;
        byte firstByte = data[5];
        int sixth_byte = (firstByte & 0xFF);
        if (sixth_byte == 1) {
            scheme = TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_CD;
        } else if (sixth_byte == 0) {
            scheme = TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_FL;
        } else {
            scheme = getSDRawDrmScheme(data, offset, length);
        }

        return scheme;
    }

    /**get TCT-DRM SCheme
     * More info {@link TctDrmMediaColumns.DrmScheme};
     * @hide
     */
    private static int getSDRawDrmScheme(byte[] data, int offset, int length) {
        int scheme = TctDrmMediaColumns.DrmScheme.DRM_SCHEME_NOT_PROTECTED;
        String str = new String(data);
        int start_index = 0;
        byte second_byte = data[1];
        byte third_byte = data[2];
        int cid_length = (third_byte & 0xFF);
        int ctype_length = (second_byte & 0xFF);
        TctDrmLog.i(TAG, "cid_length  " + cid_length);
        if (cid_length > 0 && ctype_length > 0) {
            start_index = ctype_length + 2;
        }
        if (start_index != 0 && start_index + 3 < length) {
            byte[] cid_byte_array = { data[start_index + 1],
                    data[start_index + 2], data[start_index + 3]};
            String cid_str = new String(cid_byte_array);
            TctDrmLog.i(TAG, "cid_Str: " + cid_str);
            if (cid_str.equals("cid"))
                if (str.indexOf("Rights-Issuer") != -1 || str.indexOf("Encryption-Method") > 0) {
                    scheme = TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_SD;
                }
        }
        return scheme;

    }

    /**
     * SD raw content is same as non-raw content, the following codes
     * can handle all types of sd drm content
     *
     */
    private static int getRawDrmScheme(byte[] data, int offset, int length) {
        int scheme = TctDrmMediaColumns.DrmScheme.DRM_SCHEME_NOT_PROTECTED;
        String head = new String(data).toLowerCase();
        boolean isDrm = ( (data[0] ^ 0x46) == 0) && ((data[1] ^ 0x57) == 0) && ((data[2] ^ 0x4c) == 0) && ((data[3] ^ 0x4b) == 0);
        boolean hasFirstByte = false;
        if(length > 5){
            hasFirstByte = true;
        }
        byte firstByte = 0;
        int sixth_byte = 0;
        if (hasFirstByte) {
            firstByte = data[5];
            sixth_byte = (firstByte & 0xFF);
        }

        if (head.indexOf("content-type:") >= 0 && head.indexOf("content-transfer-encoding:") >= 0) {
            if (head.indexOf(TctDrmMediaColumns.DrmMimeType.DRM_MIMETYPE_RIGHTS_XML_STRING) >= 0) {
                scheme = TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_CD;
            } else if (head.indexOf(TctDrmMediaColumns.DrmMimeType.DRM_MIMETYPE_MESSAGE_STRING) >= 0) { //MODIFIED by hu.yang, 2016-04-15,BUG-1866340
                scheme = TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_FL;
            }
        } else if(isDrm && hasFirstByte && (sixth_byte == 1)){
            scheme = TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_CD;
        }else if(isDrm && hasFirstByte && (sixth_byte == 0)){
            scheme = TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_FL;
        } else {
            scheme = getSDRawDrmScheme(data, offset, length);
        }
        TctDrmLog.d(TAG, "getRawDrmSchem E, raw drm scheme = " + scheme);
        return scheme;
    }

    /**get TCT-DRM SCheme
     * More info {@link TctDrmMediaColumns.DrmScheme};
     * @hide
     */
    public int getDrmScheme(String filePath) {
        TctDrmLog.d(TAG, "get_DrmScheme E filePath:" + filePath);
        if (TextUtils.isEmpty(filePath)) {
            TctDrmLog.v(TAG, "get_DrmScheme Eempty return");
            return TctDrmMediaColumns.DrmScheme.DRM_SCHEME_NOT_PROTECTED;
        }

        int scheme = TctDrmMediaColumns.DrmScheme.DRM_SCHEME_NOT_PROTECTED;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filePath);
            byte[] buffer = new byte[1000];
            int count = fis.read(buffer);
            if (!isRawDrmContent(filePath)) {
                scheme = getDrmScheme(buffer, 0, count);
            } else {
                scheme = getRawDrmScheme(buffer, 0, count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return scheme;
    }

    /**
     * check the drm right is allowed to send out or not with file path when
     * click send button in email, or in mms with drm attachment
     *
     * @hide
     * @deprecated
     */
    public boolean isAllowForward(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            TctDrmLog.v(TAG, "file path is empty");
            return false;
        }
        if (filePath.startsWith("content://")) {
            throw new IllegalArgumentException(
                    "filePath is a Uri.toString(),Please invoke isAllowForward(Uri uri)!");
        } else if (filePath.startsWith("file://")) {
            int index = "file://".length();
            filePath = filePath.substring(index + 1);
        }

        File file = new File(filePath);
        if (file == null || !file.exists()) {
            TctDrmLog.v(TAG, String.format("file %s not exist", filePath));
            return false;
        }

        if (!isDrm(filePath)) {
            TctDrmLog.d(TAG, String.format("not a drm content file %s", filePath));
            return true;
        }

        boolean forward = false;
        int action = DrmStore.Action.TRANSFER;
        int status = mDrmClient.checkRightsStatus(filePath, action);
        if (status == DrmStore.RightsStatus.RIGHTS_VALID) {
            forward = true;
        }

        return forward;
    }

    /**
     * check the drm right is allowed to send out or not with Uri when click
     * send button in email, or in mms with drm attachment
     *
     * @hide
     * @deprecated
     */
    public boolean isAllowForward(Uri uri) {
        if (null == uri || Uri.EMPTY == uri) {
            TctDrmLog.v(TAG, "uri is null or empty");
            return false;
        }

        if (!isDrm(uri)) {
            TctDrmLog.d(TAG, String.format("not a drm content file %s", uri.toString()));
            return true;
        }

        boolean forward = false;
        int action = DrmStore.Action.TRANSFER;
        int status = checkRightsStatus(uri, action);
        if (status == DrmStore.RightsStatus.RIGHTS_VALID) {
            forward = true;
        }

        return forward;
    }

    /**
     * Get system property which indicates drm features enabled or not
     *
     * @hide
     */
    public static boolean isDrmEnabled() {
        TctDrmLog.d(TAG, "check TCT-DRM sulution enable or not = "+isTctDrmEnable);
        return isTctDrmEnable;
    }

    /**
     * For all of the filePath, if begin with content://, or file://, or
     * http://, it should be converted to file path firstly;
     * this flag was stored in Database;you can qury it from MediaProvider
     * @hide
     * @deprecated
     */
    public boolean isSdType(String filePath) {
        boolean result = false;
        if (!TextUtils.isEmpty(filePath) || isTctDrmEnable) {
            if (filePath.startsWith("content://")) {
                filePath = convertUriToPath(Uri.parse(filePath));
            } else if (filePath.startsWith("file://")) {
                int start = "file://".length();
                filePath = filePath.substring(start + 1);
            }
            if (TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_SD == getDrmScheme(filePath)) {
                result = true;
            }
        }

        TctDrmLog.v(TAG, filePath + ", separate delivery status = " + result);
        return result;
    }

    /**
     * check protected content is FL type or not with Uri .
     * this flag was stored in Database;you can qury it from MediaProvider
     * @hide
     * @deprecated
     */
    public boolean isFLType(Object target) {
        boolean result = false;
        if (target instanceof Uri) {
            Uri mUri = (Uri) target;
            if ((null == mUri || Uri.EMPTY == mUri)) {
                throw new IllegalArgumentException("Uri should be non null");
            }
            String filePath = convertUriToPath(mUri);
            if (TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_FL == getDrmScheme(filePath)) {
                result = true;
            }
        } else if (target instanceof String) {
            String filePath = (String) target;
            if (null == filePath)
                throw new IllegalArgumentException("Uri should be non null");

            if (TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_FL == getDrmScheme(filePath)) {
                result = true;
            }
        }
        TctDrmLog.v(TAG, "Foward Lock status = " + result);
        return result;
    }

    /**
     * get Sd Rights Issuer,
     * make sure input parameter is sd drm content
     *
     * @hide
     */
    public String getSdRightsIssuer(String filePath) {
        String riIssuer = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filePath);
            byte[] buffer = new byte[1024];
            int count = fis.read(buffer);
            String head = new String(buffer).toLowerCase();
            int start = head.indexOf("rights-issuer:");
            int end = head.indexOf("\n", start);
            if (start >= 0 && end >= 0 && end > start) {
                riIssuer = head.substring(start + "Rights-Issuer:".length(),end - 1);
                TctDrmLog.v(TAG, "right issuer url:= " + riIssuer);
            }
        } catch (Exception e) {
            TctDrmLog.d(TAG, "fail to get right issuer");
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return riIssuer.trim();
    }

    /**
     * get Sd Rights Issuer,
     * make sure input parameter is sd drm content
     *
     * @hide
     */
    public String getSdRightsIssuer(Uri uri) {
        if ((null == uri || Uri.EMPTY == uri)) {
            throw new IllegalArgumentException("Uri should be non null");
        }

        String filePath = convertUriToPath(uri);
        if (null == filePath || TextUtils.isEmpty(filePath)) {
            throw new IllegalArgumentException("convertUriToPath and filePath should be non null");
        }

        if (!isSdType(filePath)) {
            throw new IllegalArgumentException("convertUriToPath filePath should be Sd uri");
        }

        return getSdRightsIssuer(filePath);
    }

    /**
     * if file has count constraint return true, else return false .
     * Please qury database to check whether protected content has count constraint
     * @deprecated
     * @hide
     */
    public boolean hasCountConstraint(String filePath) {
        filePath = checkFilePathAndStandardized(filePath);
        ContentValues cv = getConstraintFromFilePath(filePath);
        if (cv != null) {
            return cv.containsKey(DrmStore.ConstraintsColumns.REMAINING_REPEAT_COUNT);
        }
        return false;
    }

    /**
     * if file has count constraint return true, else return false .
     * NOTE:
     * if you want to get this flag,Please qury it from MediaProvider
     * More info:MediaStore.TctDrmMediaColumns.DrmColumns
     * @hide
     * @deprecated
     */
    public boolean hasCountConstraint(Uri uri) {
        if (null == uri || Uri.EMPTY == uri) {
            throw new IllegalArgumentException("Uri should be non null");
        }
        ContentValues cv = getConstraintFromFilePath(uri);
        if (cv != null) {
            return cv.containsKey(DrmStore.ConstraintsColumns.REMAINING_REPEAT_COUNT);
        }
        return false;
    }

    /**
     * if file has interval constraint return true, else return false .
     * Please qury database to check whether protected content has interval constraint
     * @deprecated
     * @hide
     */
    public boolean hasIntervalConstraint(String filePath) {
        filePath = checkFilePathAndStandardized(filePath);
        ContentValues cv = getConstraintFromFilePath(filePath);
        if (cv != null) {
            return cv.containsKey(DrmStore.ConstraintsColumns.LICENSE_AVAILABLE_TIME);
        }
        return false;
    }

    /**
     * if file has interval return true, else return false .
     * NOTE:
     * if you want to get this flag,Please qury it from MediaProvider
     * More info:MediaStore.TctDrmMediaColumns.DrmColumns
     * @hide
     * @deprecated
     */
    public boolean hasIntervalConstraint(Uri uri) {
        if (null == uri || Uri.EMPTY == uri) {
            throw new IllegalArgumentException("Uri should be non null");
        }
        ContentValues cv = getConstraintFromFilePath(uri);
        if (cv != null) {
            return cv.containsKey(DrmStore.ConstraintsColumns.LICENSE_AVAILABLE_TIME);
        }
        return false;
    }

    /**
     * if file has time constraint return true, else return false .
     * Please qury database to check whether protected content has time constraint
     * @deprecated
     * @hide
     */
    public boolean hasTimeConstraint(String filePath) {
        filePath = checkFilePathAndStandardized(filePath);
        ContentValues cv = getConstraintFromFilePath(filePath);
        if (cv != null) {
            return cv.containsKey(DrmStore.ConstraintsColumns.LICENSE_START_TIME)
                    || cv.containsKey(DrmStore.ConstraintsColumns.LICENSE_EXPIRY_TIME);
        }
        return false;
    }

    /**
     * if file has time constraint return true, else return false .
     * NOTE:
     * if you want to get this flag,Please qury it from MediaProvider
     * More info:MediaStore.TctDrmMediaColumns.DrmColumns
     * @hide
     * @deprecated
     */
    public boolean hasTimeConstraint(Uri uri) {
        if (null == uri || Uri.EMPTY == uri) {
            throw new IllegalArgumentException("Uri should be non null");
        }
        ContentValues cv = getConstraintFromFilePath(uri);
        if (cv != null) {
            return cv.containsKey(DrmStore.ConstraintsColumns.LICENSE_START_TIME)
                    || cv.containsKey(DrmStore.ConstraintsColumns.LICENSE_EXPIRY_TIME);
        }
        return false;
    }

    /**
     * check the uri can set as ringtone return true if is drm file and don't
     * contain the count constraint false other
     *
     * @hide
     * */
    public boolean canSetasRingtone(Uri uri) {
        String filePath = DrmUri.convertUriToPath(uri, mContext);
        return canSetAsRingtone(filePath);
    }

    /**
     * check the uri can set as ringtone return true if is drm file and don't
     * contain the count constraint false other
     *
     * @hide
     * */
    public boolean canSetasWallpaper(Uri uri) {
        String filePath = DrmUri.convertUriToPath(uri, mContext);
        return canSetAsWallpaper(filePath);
    }

    /**
     * check the path can set as ringtone should use this method as:
     * DrmManagerClient drmClient = new DrmManagerClient(context);
     * if(drmClient.canHanlde(path)){ if(canSetAsRingtone(path)){ //can set ring
     * tone. } } return true if is drm file and don't contain the count
     * constraint false other
     *
     * @param filePath
     * @hide
     * @return
     */
    public boolean canSetAsRingtone(String filePath) {
        return canSetWallpaperOrRingtone(filePath, false);
    }

    /**
     * check the path can set as wallpaper should use this method as:
     * DrmManagerClient drmClient = new DrmManagerClient(context);
     * if(drmClient.canHanlde(path)){ if(canSetAsWallpaper(path)){ //can set
     * ring tone. } } return true if is drm file and don't contain the count
     * constraint false other
     *
     * @param filePath
     * @hide
     * @return
     */
    public boolean canSetAsWallpaper(String filePath) {
        return canSetWallpaperOrRingtone(filePath, true);
    }

    /**
     *
     * @param filePath
     * @param wallpaper
     * @return
     */
    private boolean canSetWallpaperOrRingtone(String filePath, boolean wallpaper) {
        if (filePath == null) {
            TctDrmLog.w(TAG, "convert URI to path failed!");
            return false;
        }
        ContentValues cv = mDrmClient.getConstraints(filePath,
                wallpaper ? DrmStore.Action.DISPLAY : DrmStore.Action.PLAY);
        if (cv == null) {
            TctDrmLog.d(TAG, (wallpaper ? "Display" : "Play") + " constraints are NULL");
            return false;
        }
        Integer count = cv.getAsInteger(DrmStore.ConstraintsColumns.REMAINING_REPEAT_COUNT);
        if (count != null) {
            TctDrmLog.w(TAG, "the file has count constraint so can't set as"
                    + (wallpaper ? "wallpaper" : "ringtone"));
            return false;
        }

        return true;
    }

    /**
     * get constraints value from filepath, values wrapped as content-value
     *
     * @hide
     */
    private ContentValues getConstraintFromFilePath(String filePath) {
        String original_mimetype = null;
        int action = DrmStore.Action.DEFAULT;
        filePath = checkFilePathAndStandardized(filePath);
        try {
            original_mimetype = mDrmClient.getOriginalMimeType(filePath);
        } catch (IllegalArgumentException iae) {
            TctDrmLog.i(TAG, "fail to get mimetype");
        }
        if (original_mimetype == null) {
            int index = filePath.lastIndexOf(".");
            String extension = filePath.substring(index + 1);
            original_mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        if (original_mimetype != null) {
            action = getActionFromMimeType(original_mimetype);
        }
        return mDrmClient.getConstraints(filePath, action);
    }

    /**get the protected content's time constraint
     * @hide
     */
    private long getTimeConstraint(String filepath) {
        long time = -1;
        filepath = checkFilePathAndStandardized(filepath);
        ContentValues cv = getConstraintFromFilePath(filepath);
        if (cv != null) {
            String timeConstraintStr = cv
                    .getAsString(DrmStore.ConstraintsColumns.LICENSE_EXPIRY_TIME);
            int line_index = timeConstraintStr.indexOf("-");
            String year_str = timeConstraintStr.substring(0, line_index);
            line_index = timeConstraintStr.indexOf("-", line_index + 1);
            String month_str = timeConstraintStr.substring(line_index - 2,
                    line_index);
            String day_str = timeConstraintStr.substring(line_index + 1,
                    line_index + 3);
            int colon_index = timeConstraintStr.indexOf(":");
            String hour_str = timeConstraintStr.substring(colon_index - 2,
                    colon_index);
            colon_index = timeConstraintStr.indexOf(":", colon_index + 1);
            String minute_str = timeConstraintStr.substring(colon_index - 2,
                    colon_index);
            String second_str = timeConstraintStr.substring(colon_index + 1,
                    colon_index + 3);
            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            c.clear();
            c.set(Calendar.DST_OFFSET, 0);
            int year = Integer.valueOf(year_str);
            c.set(Calendar.YEAR, year);
            TctDrmLog.i(TAG, "getTimeConstraint year:= " + year);
            // month is 0 based!
            int month = Integer.valueOf(month_str);
            c.set(Calendar.MONTH, (month - 1));
            TctDrmLog.i(TAG, "getTimeConstraint month:= " + month);

            int date = Integer.valueOf(day_str);
            c.set(Calendar.DATE, date);
            TctDrmLog.i(TAG, "getTimeConstraint date:= " + date);
            int hour = Integer.valueOf(hour_str);
            c.set(Calendar.HOUR, hour);
            TctDrmLog.i(TAG, "getTimeConstraint hour:= " + hour);
            int minute = Integer.valueOf(minute_str);
            c.set(Calendar.MINUTE, minute);
            TctDrmLog.i(TAG, "getTimeConstraint minute:= " + minute);
            int second = Integer.valueOf(second_str);
            c.set(Calendar.SECOND, second);
            TctDrmLog.i(TAG, "getTimeConstraint second:= " + second);
            time = c.getTimeInMillis();
            TctDrmLog.i(TAG, "getTimeConstraint getTimeInMillis:= " + time);
        }
        return time;
    }

    /**
     * check is SD type or not with Uri
     * NOTE:
     * if you want to get this flag,Please qury it from MediaProvider
     * More info:MediaStore.TctDrmMediaColumns.DrmColumns
     * @hide
     * @deprecated
     */
    public boolean isSdType(Uri uri) {
        boolean result = false;
        if ((null == uri || Uri.EMPTY == uri)) {
            throw new IllegalArgumentException("Uri should be non null");
        }
        if (isTctDrmEnable) {
            String filePath = convertUriToPath(uri);
            if (TctDrmMediaColumns.DrmScheme.DRM_SCHEME_OMA1_SD == getDrmScheme(filePath)) {
                result = true;
            }
        }
        TctDrmLog.v(TAG, uri.toString() + ", separate delivery status = " + result);
        return result;
    }

    /**
     * get Sd Vendor value
     * make sure input parameter is sd drm content
     * @hide
     */
    public String getSdVendorUrl(Uri uri) {
        if ((null == uri || Uri.EMPTY == uri)) {
            throw new IllegalArgumentException("getVendorUrl Uri should be non null");
        }
        if (!isSdType(uri)) {
            throw new IllegalArgumentException(" getVendorUrl Uri should be Sd uri");
        }
        String sdVendorUrl = "";
        ContentValues contentValue = mDrmClient.getMetadata(uri);
        if (contentValue != null) {
            sdVendorUrl = contentValue.getAsString(TctDrmMediaColumns.PropertiesColumns.CONTENT_VENDOR);
        }
        TctDrmLog.v(TAG, "getSdVendorUrl " + uri.toString() + ", vendor url = "+ sdVendorUrl);
        return sdVendorUrl.trim();
    }

    /**
     * get constraints value from uri, values wrapped as content-value
     * @param uri
     *     The protected content's uri
     * @hide
     */
    private ContentValues getConstraintFromFilePath(Uri uri) {
        if (null == uri || Uri.EMPTY == uri) {
            throw new IllegalArgumentException("Uri should be non null");
        }
        String original_mimetype = null;
        int action = DrmStore.Action.DEFAULT;
        try {
            original_mimetype = mDrmClient.getOriginalMimeType(uri);
        } catch (IllegalArgumentException iae) {
            TctDrmLog.i(TAG, "fail to get mimetype");
        }
        if (original_mimetype == null) {
            String filePath = convertUriToPath(uri);

            if (null != filePath) {
                int index = filePath.lastIndexOf(".");
                String extension = filePath.substring(index + 1);
                original_mimetype = MimeTypeMap.getSingleton()
                        .getMimeTypeFromExtension(extension);
            }

        }
        if (original_mimetype != null) {
            action = getActionFromMimeType(original_mimetype);
        }
        return getConstraints(uri, action);
    }

    /**
     * get time constraints value from uri
     * @param uri
     *     The protected content's uri
     * @hide
     */
    public long getTimeConstraint(Uri uri) {
        if (null == uri || Uri.EMPTY == uri) {
            throw new IllegalArgumentException("Uri should be non null");
        }
        long time = -1;
        ContentValues cv = getConstraintFromFilePath(uri);
        if (cv != null) {
            String timeConstraintStr = cv
                    .getAsString(DrmStore.ConstraintsColumns.LICENSE_EXPIRY_TIME);
            int line_index = timeConstraintStr.indexOf("-");
            String year_str = timeConstraintStr.substring(0, line_index);
            line_index = timeConstraintStr.indexOf("-", line_index + 1);
            String month_str = timeConstraintStr.substring(line_index - 2,
                    line_index);
            String day_str = timeConstraintStr.substring(line_index + 1,
                    line_index + 3);
            int colon_index = timeConstraintStr.indexOf(":");
            String hour_str = timeConstraintStr.substring(colon_index - 2,
                    colon_index);
            colon_index = timeConstraintStr.indexOf(":", colon_index + 1);
            String minute_str = timeConstraintStr.substring(colon_index - 2,
                    colon_index);
            String second_str = timeConstraintStr.substring(colon_index + 1,
                    colon_index + 3);

            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            c.clear();
            c.set(Calendar.DST_OFFSET, 0);

            int year = Integer.valueOf(year_str);
            c.set(Calendar.YEAR, year);
            TctDrmLog.i(TAG, "getTimeConstraint year:= " + year);
            // month is 0 based!
            int month = Integer.valueOf(month_str);
            c.set(Calendar.MONTH, (month - 1));
            TctDrmLog.i(TAG, "getTimeConstraint month:= " + month);
            int date = Integer.valueOf(day_str);
            c.set(Calendar.DATE, date);
            TctDrmLog.i(TAG, "getTimeConstraint date:= " + date);
            int hour = Integer.valueOf(hour_str);
            c.set(Calendar.HOUR, hour);
            TctDrmLog.i(TAG, "getTimeConstraint hour:= " + hour);
            int minute = Integer.valueOf(minute_str);
            c.set(Calendar.MINUTE, minute);
            TctDrmLog.i(TAG, "getTimeConstraint minute:= " + minute);
            int second = Integer.valueOf(second_str);
            c.set(Calendar.SECOND, second);
            TctDrmLog.i(TAG, "getTimeConstraint second:= " + second);
            time = c.getTimeInMillis();
            TctDrmLog.i(TAG, "getTimeConstraint getTimeInMillis:= " + time);
        }
        return time;
    }

    /**
     * Check the rights of DRM content with uri
     *
     * @return true: Normal file, or the DRM content with valid false: DRM
     *         content without valid rights, or file not exist
     * @hide
     */
    public boolean isRightValid(Uri uri) {
        if (null == uri || Uri.EMPTY == uri) {
            throw new IllegalArgumentException("Uri should be non null");
        }

        if (!isDrm(uri)) {
            return true;
        }

        boolean rightValid = false;
        try {
            String mimeType = mDrmClient.getOriginalMimeType(uri);
            int action = getActionFromMimeType(mimeType);
            int status = checkRightsStatus(uri, action);
            if (status == DrmStore.RightsStatus.RIGHTS_VALID) {
                rightValid = true;
            }
        } catch (Exception e) {
            TctDrmLog.w(TAG, "isRightValid(URI) exception:" + e);
        }

        return rightValid;
    }

    /**
     * Check the rights of DRM content
     *
     * @param the protected content path
     * @return true: Normal file, or the DRM content with valid false: DRM
     *         content without valid rights, or file not exist
     * @hide
     */
    public boolean isRightValid(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return false;
        }

        if (filePath.startsWith("content://")) {
            filePath = convertUriToPath(Uri.parse(filePath));
        } else if (filePath.startsWith("file://")) {
            int index = "file://".length();
            filePath = filePath.substring(index + 1);
        }

        File file = new File(filePath);
        if (file == null || !file.exists()) {
            return false;
        }

        if (!isDrm(filePath)) {
            return true;
        }

        boolean rightValid = false;
        String mimeType = mDrmClient.getOriginalMimeType(filePath);
        int action = getActionFromMimeType(mimeType);
        int status = mDrmClient.checkRightsStatus(filePath, action);
        if (status == DrmStore.RightsStatus.RIGHTS_VALID) {
            rightValid = true;
        }

        return rightValid;
    }

    /**
     * check the file path,if it is start with "file://",delete the part of
     * "file://"
     *
     * @hide
     */
    private String checkFilePathAndStandardized(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            throw new IllegalArgumentException("File path shuld be none null!");
        }
        if (filePath.startsWith("file://")) {
            int index = "file://".length();
            filePath = filePath.substring(index + 1);
        }
        TctDrmLog.v(TAG, "File path after standardized is: " + filePath);
        return filePath;
    }

    /**
     * get corresponding action from mimetype of drm content
     * @param mimeType
     * @return {@link DrmStore.Action};
     * @hide
     */
    private int getActionFromMimeType(String mimeType) {
        int action = DrmStore.Action.DEFAULT;
        if (!TextUtils.isEmpty(mimeType)) {
            if (mimeType.startsWith("image")) {
                action = DrmStore.Action.DISPLAY;
            } else if (mimeType.startsWith("audio") || mimeType.startsWith("video")) {
                action = DrmStore.Action.PLAY;
            }
        }

        return action;
    }

    /**
     * Get the email filepath from uri
     * use for Email.apk
     * @param uri
     *        the attached file's uri
     * @hide
     */
    public String getEmailFilePath(Uri uri) {
        String path = null;
        if (uri.getScheme().equals("file")) {
            path = uri.getSchemeSpecificPart();
        }
        else if (DocumentsContract.isDocumentUri(mContext, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));
                return getDataColumn(mContext, contentUri, null, null);
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                contentUri = contentUri.withAppendedPath(contentUri, split[1]);
                return getDataColumn(mContext, contentUri, null, null);
            }
        } else if (uri.getScheme().equals("content")) {
            String[] projection = new String[1];
            if (uri.getAuthority().indexOf("com.android.email.provider") != -1) {
                projection[0] = "contentUri";
            } else {
                projection[0] = "_data";
            }
            Cursor c;
            try {
                c = mContext.getContentResolver().query(uri, projection, null, null, null);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            if (c != null) {
                try {
                    if (c.moveToFirst()) {
                        path = c.getString(0);
                    }
                } finally {
                    c.close();
                }
            }

            if (path != null && path.endsWith("RAW")) {
                List<String> segments = uri.getPathSegments();
                String dbName = segments.get(0);
                String id = segments.get(1);
                path = mContext.getDatabasePath(dbName + "_att") + "/" + id;
            }

        }
        TctDrmLog.v(TAG, "get Email filepath : " + path);
        return path;
    }

    /** get the DRM real content thumbnail without consume and water mark a lock
    * @param filepath the DRM source file path
    * @param opts BitmapFactory.Options
    * @param size The thumbnail size
    * @return the Bitmap decode from path with water mark a lock
    * NOTE: difference project difference size value.
    * @hide
    */
    public Bitmap getDrmRealThumbnail(String filePath,BitmapFactory.Options options, int size) {
        Bitmap originBitMap = null;
        if (TextUtils.isEmpty(filePath)) {
            TctDrmLog.w(TAG, "File path shuld be none null!");
            return null;
        }
        filePath = checkFilePathAndStandardized(filePath);
        if (size <= 0) {
            TctDrmLog.w(TAG, "size < 0!");
            size = 48;
        }
        if (!isDrm(filePath)) {
            TctDrmLog.w(TAG, "filePaht is not drm");
            return null;
        }
        originBitMap = BitmapFactory.getDrmThumbnailNotConsume(filePath, options);
        if (originBitMap == null) {
            originBitMap = BitmapFactory.getDrmThumbnailNotConsume(filePath, options);
        }

        if (originBitMap != null) {
            return createWaterMark(originBitMap,filePath,size);
        } else {
            TctDrmLog.d(TAG, "originBitmap = null ,size = "+size);
            originBitMap = getDrmThumbnail(filePath,size);

        }
        return originBitMap;

    }


    /** get the DRM real content thumbnail without consume and water mark a lock
     *  this method used to decode DRM Video files.
    * @param bitmap The Bitmap which decode from MediaMetadataRetriever.retriever.getFrameAtTime(0)
    * @param filepath the DRM source file path
    * @param size The thumbnail size
    * @return the Bitmap decode from path with water mark a lock
    * NOTE: difference project difference size value.
    * @hide
    */
    public Bitmap getDrmVideoThumbnail(Bitmap bitmap,String filePath,int size){
        Bitmap mBitmap = bitmap;
        if (null == mBitmap) {
            TctDrmLog.e(TAG,"getThumbnail error bitmap == null");
            if (FILEMANAGER_DRM_SIZE == size) {
                return getDrmThumbnail(filePath,size);
            } else {
                mBitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            }
        } else {
            mBitmap = zoomDrmImage (bitmap, size, size);
        }

        return createWaterMark(mBitmap,filePath,size);

    }


    /**
     * get different DRM icon thumbnail with mimetype for filepath
     *
     * @hide
     * @filePath drm file path
     * @size drm icon thumbnail with,eg,filemanager is 48,gallery is 200
     * NOTE: difference project difference size value.
     * @deprecated :Please DO NOT usage this method to get DRM file's thumbnail any more
     * will change this method's from public to private on next version
     */
    public Bitmap getDrmThumbnail(String filePath, int size) {
        Bitmap bm = null;
        filePath = checkFilePathAndStandardized(filePath);
        boolean isMimeTypeOK = true;
        if (size <= 0) {
            TctDrmLog.w(TAG, "size < 0!");
            size = 48;
        }
        if (!isDrm(filePath)) {
            TctDrmLog.w(TAG, "filePaht is not drm");
            return null;
        }
        String mimeType = mDrmClient.getOriginalMimeType(filePath);

        if(mimeType == null || TextUtils.isEmpty(mimeType)){
            isMimeTypeOK = false;
        }

        if (isMimeTypeOK && mimeType.startsWith("image")) {
            bm = BitmapFactory.decodeResource(mContext.getResources(),
                    com.android.internal.R.drawable.ic_drm_default_thumbnail_picture_image);
        } else if (isMimeTypeOK && mimeType.startsWith("video")) {
            bm = BitmapFactory.decodeResource(mContext.getResources(),
                    com.android.internal.R.drawable.ic_drm_default_thumbnail_picture_video);
        } else if (isMimeTypeOK
                && mimeType.startsWith("audio")
                || mimeType.startsWith("application/ogg")) {
            bm = BitmapFactory.decodeResource(mContext.getResources(),
                    com.android.internal.R.drawable.ic_drm_default_thumbnail_picture_audio);
        } else {
            bm = BitmapFactory.decodeResource(mContext.getResources(),
                    com.android.internal.R.drawable.ic_drm_default_thumbnail_picture_missing);
        }
        if (bm != null) {
            return createWaterMark(bm,filePath,size);
        }else{
            return bm;
        }
    }

    /** get the DRM real content thumbnail without consume and water mark a lock .
     *
    * @param bitmap The Bitmap which we want to override a lock on it .
    * @param filepath the DRM source file path
    * @param size The thumbnail size
    * @return the Bitmap decode from path with water mark a lock
    * NOTE: difference project difference size value.
    * @hide
    */
    private Bitmap createWaterMark(Bitmap bitmap,String filePath,int size){
        Bitmap mBitmap = bitmap;
        Bitmap newb = Bitmap.createBitmap(size,size,Bitmap.Config.ARGB_8888);
        Canvas cv = new Canvas(newb);
        Bitmap waterMark = null;
        mBitmap = zoomDrmImage(mBitmap ,size ,size);

        if (isRightValid(filePath)){
             waterMark = BitmapFactory.decodeResource(mContext.getResources(), com.android.internal.R.drawable.drm_unlocked);
        }else{
            waterMark = BitmapFactory.decodeResource(mContext.getResources(), com.android.internal.R.drawable.drm_locked);
        }

        int w = mBitmap.getWidth();
        int h = mBitmap.getHeight();
        int ww = waterMark.getWidth();
        int wh = waterMark.getHeight();
        cv.drawBitmap(mBitmap, 0, 0, null);
        cv.drawBitmap(waterMark, w - ww+5, h - wh+5, null);
        cv.save(Canvas.ALL_SAVE_FLAG);
        cv.restore();
        newb = zoomDrmImage (newb, size, size);
        return newb;
    }


    /**
     * Zoom drm image and override the key bitmap to thumbnail of image
     *
     */
    private Bitmap zoomDrmImage(Bitmap srcBitmap, int newWidth, int newHeight) {
        if (srcBitmap == null) {
            return null;
        }

        int width = srcBitmap.getWidth();
        int height = srcBitmap.getHeight();

        Matrix matrix = new Matrix();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(srcBitmap, 0, 0, width, height,
                matrix, true);
        return bitmap;
    }

    /**
     * get file path from url string
     * {@link convertUriToPath}
     * @hide
     */
    public String getFilePath(Uri uri) {
        Uri thisUri = null;
        String path = "";
        ContentResolver cr = mContext.getContentResolver();
        if (uri == null) {
            return path;
        }
        thisUri = uri;

        try {
            String strUri = thisUri.toString();
            if (strUri.startsWith("content://settings/system")) {
                int start = strUri.lastIndexOf('/');
                String setting = strUri.substring(start + 1);
                String strUriSetting = Settings.System.getString(cr, setting);
                if (strUriSetting == null) {
                    return path;
                }
                thisUri = Uri.parse(strUriSetting);
            }

            String scheme = thisUri.getScheme();

            if (scheme == null) {
                path = thisUri.toString();
            } else if (scheme.equals("file")) {
                path = thisUri.getPath();
                path = changeDrmFileSuffix(path);
            } else if (DocumentsContract.isDocumentUri(mContext, uri)){
                //ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {
                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                    return getDataColumn(mContext, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                    contentUri = contentUri.withAppendedPath(contentUri, split[1]);
                    return getDataColumn(mContext, contentUri, null, null);
                }
            } else if (scheme.equals("content")) {
                String[] projection = { "_data" };
                Cursor c = cr.query(thisUri, projection, null, null, null);
                if (c != null) {
                    try {
                        if (c.moveToFirst()) {
                            path = c.getString(0);
                        }
                    } finally {
                        c.close();
                    }
                }

                if (path.endsWith("RAW")) {
                    List<String> segments = thisUri.getPathSegments();
                    String dbName = segments.get(0);
                    String id = segments.get(1);
                    path = mContext.getDatabasePath(dbName + "_att") + "/" + id;
                }

            }
        } catch (Exception e) {
            TctDrmLog.w(TAG, "get file path " + e.toString());
        }
        TctDrmLog.i(TAG, "get file path, file path = " + path);

        return path;
    }

    // OMA DRM_MMS
    //'filePath' startswith "/storage/emulated/0/"
    //but,sudo adb shell ,we cann't find the '0' directory in '/storage/emulate/'
    //so,we explicit to correct 'filePath'
    private String changeDrmFileSuffix(String filePath) {
        String constTmp = "/storage/sdcard0/Download/";
        String tmpEmulated = "/storage/emulated/0/Download/";
        if (null != filePath && filePath.startsWith("/storage/emulated/0/")) {
            String tmpStr = filePath.substring(tmpEmulated.length(), filePath.length());
            String tmp = constTmp + tmpStr;
            return tmp;
        }
        return filePath;
    }

    // OMA DRM_MMS
    /**
     * show DRM toast
     *
     * @hide
     */
    public void showToast(int resId, String filepath) {
        String message = String.format(mContext.getString(resId),
                new File(filepath).getName());
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    /**
     * get media uri from drm file path, such as /sdcard/FLmusic.jpg, to
     * content://media/internal/audio/media/42
     * use for mms.
     * {@link convertUriToPath}
     * @hide
     */
    public Uri getMediaUriFromFilePath(String filePath, String mimeType) {
        Uri mediaUri = null;
        if (TextUtils.isEmpty(filePath) || !isDrm(filePath)) {
            return null;
        }

        try {
            ContentResolver cr = mContext.getContentResolver();
            mediaUri = getMediaUriPrefix(mimeType);
            if (mediaUri != null) {
                Cursor c = cr.query(mediaUri, new String[] { "_id" }, "_data='"
                        + filePath + "'", null, null);
                if (c == null)
                    TctDrmLog.i(TAG, "not exist this file in the media store "
                            + filePath);
                if (c != null && c.moveToNext()) {
                    mediaUri = Uri.withAppendedPath(mediaUri,
                            String.valueOf(c.getInt(0)));
                }
                c.close();
            }
            TctDrmLog.d(TAG, "media uri is: " + mediaUri + " for file " + filePath);
        } catch (Exception e) {
            TctDrmLog.d(TAG, "get media uri failed from file path " + filePath);
        }
        return mediaUri;
    }

    /**
     * get media store uri from file path, to delete or rename it
     *
     * @hide
     */
    private Uri getMediaUriPrefix(String mimeType) {
        Uri mediaUri = null;
        if (mimeType != null) {
            if (mimeType.startsWith("image")) {
                mediaUri = Uri.parse("content://media/external/images/media");
            } else if (mimeType.startsWith("audio")) {
                mediaUri = Uri.parse("content://media/external/audio/media");
            } else if (mimeType.startsWith("video")) {
                mediaUri = Uri.parse("content://media/external/video/media");
            }
        }
        return mediaUri;
    }

    /**
     * consume protected content's right
     * @hide
     */
    public void consumeRights(String filePath) {
        if (!TextUtils.isEmpty(filePath) || isTctDrmEnable) {
            if (filePath.startsWith("content://")) {
                filePath = convertUriToPath(Uri.parse(filePath));
            } else if (filePath.startsWith("file://")) {
                int start_index = "file://".length();
                filePath = filePath.substring(start_index + 1);
            }
            String mimeType = mDrmClient.getOriginalMimeType(filePath);
            int action = DrmStore.Action.DEFAULT;
            if (!TextUtils.isEmpty(filePath)) {
                action = getActionFromMimeType(mimeType);
                TctDrmLog.i(TAG, "filepath :" + filePath + "action :" + action);
            }
            mDrmClient.tct_consumeRights(mUniqueId, filePath, action);
        }
    }

    /**
     * consume protected content's right
     * @param action @see DrmStore.Action
     * @hide
     */
    public void consumeRights(FileDescriptor fd, int action) {
        if (!fd.valid() || !isTctDrmEnable) {
            TctDrmLog.w(TAG, "FileDescriptor is invalid");
            return;
        }
        mDrmClient.tct_consumeRights(mUniqueId, fd, action);
    }

    /**
     * consume protected content's right
     * @hide
     */
    public void consumeRights(Uri uri, int action) {
        if (null == uri || Uri.EMPTY == uri || !isTctDrmEnable) {
            return;
        }
        ParcelFileDescriptor pfd = null;

        if (mCr == null) {
            return;
        }
        try {
            pfd = mCr.openFileDescriptor(uri, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        if (pfd != null) {
            FileDescriptor fd = pfd.getFileDescriptor();
            if (fd == null) {
                TctDrmLog.w(TAG, "pfd.getFileDescriptor() == NULL!");
            } else {
                TctDrmLog.w(TAG, "fd valid =" + fd.valid() + "fd.getInt="+ fd.toString());
                mDrmClient.tct_consumeRights(mUniqueId, fd, action);
            }
            try {
                pfd.close();
                pfd = null;
                fd = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * open rights-Issuer url through web browser .
     * activate protected content(Separate delivery only) .
     * we can obtain SD file's right .
     * we can get Right-Issuer from Separate delivery .
     * @see getSdRightsIssuer
     *
     * @param  filepath protected content
     * @hide
     */
    public void activateContent(Context context, String filepath) {
        filepath = checkFilePathAndStandardized(filepath);
        TctDrmLog.i(TAG, "active drm, file path: " + filepath);
        String rightUrl = null;
        if (isDrm(filepath)) {
            try {
                ContentValues value = mDrmClient.getMetadata(filepath);
                if (value != null) {
                    rightUrl = value.getAsString(TctDrmMediaColumns.PropertiesColumns.RIGHTS_ISSUER);
                    TctDrmLog.d(TAG, "get SD Rights ISSUER: " + rightUrl);
                }
            } catch (Exception ex) {
                TctDrmLog.w(TAG, "activate content exception " + ex.toString());
            } finally {
                if (rightUrl.isEmpty()) {
                    TctDrmLog.i(TAG, "fail to acquire right issuer");
                    new DrmApp(mContext).showToast(com.android.internal.R.string.drm_no_valid_right, null);
                    return;
                }
                TctDrmLog.i(TAG, "active drm, right url: " + rightUrl);
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(rightUrl));
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                context.startActivity(i);
            }
        }
    }

    /**
     * open rights-Issuer url through web browser .
     * activate protected content(Separate delivery only) .
     * we can obtain SD file's right .
     * we can get Right-Issuer from Separate delivery .
     * @see getSdRightsIssuer
     *
     * @param  uri protected content
     * @hide
     */
    public void activateContent(Context context, Uri uri) {
        if (null == uri || Uri.EMPTY == uri) {
            throw new IllegalArgumentException("Uri should be non null");
        }
        TctDrmLog.i(TAG, "active drm, file uri: " + uri.toString());

        String rightUrl = null;
        if (isDrm(uri)) {
            try {
                ContentValues value = mDrmClient.getMetadata(uri);
                if (value != null) {
                    rightUrl = value.getAsString(TctDrmMediaColumns.PropertiesColumns.RIGHTS_ISSUER);
                    TctDrmLog.d(TAG, "get SD Rights ISSUER: " + rightUrl);
                }
            } catch (Exception ex) {
                TctDrmLog.w(TAG, "activate content exception " + ex.toString());
            } finally {
                if (rightUrl.isEmpty()) {
                    TctDrmLog.i(TAG, "fail to acquire right issuer");
                    String filepath = convertUriToPath(uri);
                    new DrmApp(mContext).showToast(com.android.internal.R.string.drm_no_valid_right, null);
                    return;
                }
                TctDrmLog.i(TAG, "active drm, right url: " + rightUrl);
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(rightUrl));
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                context.startActivity(i);
            }
        }
    }

    /**
     * {@link #readSecureTimeFromFile(long[])}
     * @hide
     */
    public long[] readDeltaValueFromFile() {
        long[] array = new long[2];
        long pre_delta = 0;
        long pre_flag = 0;
        try {
            FileReader fr = new FileReader("/data/drm/native_securetime.txt");
            BufferedReader bufferedreader = new BufferedReader(fr);
            String instring;
            int line_count = 0;
            while ((instring = bufferedreader.readLine()) != null) {
                instring = instring.trim();
                if (0 != instring.length()) {
                    line_count++;
                    if (line_count == 1) {
                        pre_flag = Long.valueOf(instring);
                    }
                    if (line_count == 2) {
                        pre_delta = Long.valueOf(instring);
                    }
                }
            }
            fr.close();
            array[0] = pre_flag;
            array[1] = pre_delta;
        } catch (IOException ioe) {
            TctDrmLog.d(TAG, "fail to open file: /data/drm/native_securetime.txt ");
        }
        return array;
    }

    /**
     * write the drm secure time to file deltatime'units ms
     * @param flag
     *        flag ==0 or flag ==1 olny!!.
     *        if flag == 1,deltatime is secure time,and deltatime is valid.
     *        if flag == 0,deltatime isn't secure time,and deltatime is invalid
     * @param deltatime
     *        if deltatime == 0,current local device's time same as network's NITZ time.
     *        otherwise,deltatime equals network's time subtract current local device's time(or reverse)
     * @hide
     */
    public boolean writeSecureTimeToFile(int flag, long deltatime) {
        if (!isTctDrmEnable) {
            return false;
        }
        return mDrmClient.tct_writeSecureTimeToFile(flag, deltatime);
    }

    /**
     * read secure time from /data/drm/native_securetime.txt
     * @param valueArr
     *        secure time data was stored valueArr
     *        valueArr[0]: {@link #writeSecureTimeToFile(int, long)} first param
     *        valueArr[1]: {@link #writeSecureTimeToFile(int, long)} second param
     * @hide
     */
    public boolean readSecureTimeFromFile(long[] valueArr) {
        if (!isTctDrmEnable) {
            return false;
        }
        return mDrmClient.tct_readSecureTimeFromFile(valueArr);
    }

    private int getEventType(int infoType) {
        int eventType = -1;
        switch (infoType) {
        case DrmInfoRequest.TYPE_REGISTRATION_INFO:
        case DrmInfoRequest.TYPE_UNREGISTRATION_INFO:
        case DrmInfoRequest.TYPE_RIGHTS_ACQUISITION_INFO:
            eventType = DrmEvent.TYPE_DRM_INFO_PROCESSED;
            break;
        }
        return eventType;
    }

    private int getErrorType(int infoType) {
        int error = -1;
        switch (infoType) {
        case DrmInfoRequest.TYPE_REGISTRATION_INFO:
        case DrmInfoRequest.TYPE_UNREGISTRATION_INFO:
        case DrmInfoRequest.TYPE_RIGHTS_ACQUISITION_INFO:
            error = DrmErrorEvent.TYPE_PROCESS_DRM_INFO_FAILED;
            break;
        }
        return error;
    }

    private void createEventThreads() {
        if (mEventHandler == null && mInfoHandler == null) {
            mInfoThread = new HandlerThread("DrmManagerClient.InfoHandler");
            mInfoThread.start();
            mInfoHandler = new InfoHandler(mInfoThread.getLooper());
            mEventThread = new HandlerThread("DrmManagerClient.EventHandler");
            mEventThread.start();
            mEventHandler = new EventHandler(mEventThread.getLooper());
        }
    }

    private void createListeners() {
        mDrmClient._setListeners(mUniqueId, new WeakReference<TctDrmManagerClient>(this));
    }
}
