package com.tct.drm;

import android.content.Context;
import android.content.Intent;
import com.tct.drm.TctDrmStore;
import com.tct.drm.TctDrmManagerClient;

/**
 * {@hide}
 */
public final class TctWallpaperManagerService {
    public static void ifSendBroadcat(Context mContext) {
        if (TctDrmManagerClient.isDrmEnabled()) {
            mContext.sendBroadcast(new Intent(TctDrmStore.ACITON_DRM_WALLPAPER_SET));
        }
    }

}
