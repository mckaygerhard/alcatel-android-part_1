package com.tct.drm;

import android.util.Log;

/**
 * {@hide}
 */
public class DrmVideoView {
    private String TAG = "DrmVideoView";
    private boolean mDrmAlreadyConsumed = false;
    private static DrmMediaPlayer mDrmMediaPlayer = null;
    private static DrmVideoView mDrmVideoView = null;

    private DrmVideoView() {
        Log.d(TAG, "in DrmVideoView()");
    }

    /**
     * @hide
     */
    public synchronized static DrmVideoView getInstance() {
        if (null == mDrmMediaPlayer) {
            mDrmMediaPlayer = DrmMediaPlayer.getInstance();
        }
        if (null == mDrmVideoView) {
            mDrmVideoView = new DrmVideoView();
        }
        return mDrmVideoView;
    }

    /**
     * @hide
     */
    public boolean drmConsumed() {
        if (mDrmMediaPlayer != null) {
            return mDrmMediaPlayer.drmConsumed();
        }
        return false;
    }

    /**
     * Tell inner mediaplayer that this right has been consumed and don't
     * re-consume it when resuming the playback
     *
     * @hide
     */
    public void drmAlreadyConsumed() {
        mDrmAlreadyConsumed = true;
    }

    /**
     * @hide
     */
    public void setDrmMediaplayerConsumed() {
        if (mDrmAlreadyConsumed) {
            mDrmMediaPlayer.drmAlreadyConsumed();
        }
    }

    /**
     * @hide
     */
    public void setDrmAlreadyConSumed(boolean mark) {
        mDrmAlreadyConsumed = mark;
    }
}
