package com.android.ims.wfc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @hide
 * T949479 '''WIFI Calling''' bob.shen 2015-11-19
 * Check list for wifi calling.
 */
public interface WFC {
    String FR_TASK_949479 = "T949479 WIFI Calling";

    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE, ElementType.CONSTRUCTOR})
    @Retention(RetentionPolicy.SOURCE)
    @interface JrdFeature {
        String[] value();

        String description() default "WFC";
    }
}