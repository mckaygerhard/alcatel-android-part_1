/* Copyright (C) 2016 Tcl Corporation Limited */
/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | T949479              | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
/** @hide */
package com.android.ims;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.android.ims.JrdIMSSettings.IMS;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@IMS.JrdFeature({IMS.FR_TASK_949479})
public class JrdIMSSettings {
    private static String TAG = "JrdIMSSettings";

    public static final Uri CONTENT_URI = Uri.parse("content://com.jrdcom.jrdwfcmanager/settings");
    public static final String MIME_TYPE_SETTINGS_ITEM = "vnd.android.cursor.item/wfc_settings";

    public static final String NAME_CV_KEY = "item";
    public static final String NAME_CV_VALUE = "value";

    public static boolean put(Context context, String item, int value) {
        ContentResolver resolver = context.getContentResolver();
        return putInternal(resolver, item, value);
    }

    public static boolean put(Context context, String item, boolean value) {
        ContentResolver resolver = context.getContentResolver();
        return putInternal(resolver, item, value);
    }

    public static boolean put(Context context, String item, long value) {
        ContentResolver resolver = context.getContentResolver();
        return putInternal(resolver, item, value);
    }

    public static boolean put(Context context, String item, float value) {
        ContentResolver resolver = context.getContentResolver();
        return putInternal(resolver, item, value);
    }

    public static boolean put(Context context, String item, String value) {
        ContentResolver resolver = context.getContentResolver();
        return putInternal(resolver, item, value);
    }

    public static int getInt(Context context, String name, int defValue) {
        int value = defValue;
        try {
            ContentResolver resolver = context.getContentResolver();
            value = (Integer) getInternal(resolver, name, int.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static boolean getBoolean(Context context, String name, boolean defValue) {
        boolean value = defValue;
        try {
            ContentResolver resolver = context.getContentResolver();
            value = (Boolean) getInternal(resolver, name, boolean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static long getLong(Context context, String name, long defValue) {
        long value = defValue;
        try {
            ContentResolver resolver = context.getContentResolver();
            value = (Long) getInternal(resolver, name, long.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }


    public static float getFloat(Context context, String name, float defValue) {
        float value = defValue;
        try {
            ContentResolver resolver = context.getContentResolver();
            value = (Float) getInternal(resolver, name, float.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String getString(Context context, String name, String defValue) {
        String value = defValue;
        try {
            ContentResolver resolver = context.getContentResolver();
            String val = (String) getInternal(resolver, name, String.class);
            if (val != null) {
                value = val;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private static boolean putInternal(ContentResolver resolver, String item, Object value) {
        boolean success = false;
        try {
            ContentValues values = new ContentValues();
            values.put(NAME_CV_KEY, item);
            if (value instanceof Integer) {
                values.put(NAME_CV_VALUE, (Integer) value);
            } else if (value instanceof Boolean) {
                values.put(NAME_CV_VALUE, (Boolean) value);
            } else if (value instanceof Float) {
                values.put(NAME_CV_VALUE, (Float) value);
            } else if (value instanceof Long) {
                values.put(NAME_CV_VALUE, (Long) value);
            } else if (value instanceof String) {
                values.put(NAME_CV_VALUE, (String) value);
            }
            resolver.insert(CONTENT_URI, values);
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return success;
    }

    private static Object getInternal(ContentResolver resolver, String name, Class<?> clazz) {
        Object value = null;
        Cursor c = null;
        try {
            c = resolver.query(CONTENT_URI, null, null, new String[]{name}, null);
            if (c != null && c.getCount() > 0 && c.moveToFirst()) {
                int index = 0;
                if (clazz == int.class) {
                    value = c.getInt(index);
                } else if (clazz == boolean.class) {
                    value = c.getInt(index) == 1;
                } else if (clazz == long.class) {
                    value = c.getLong(index);
                } else if (clazz == float.class) {
                    value = c.getFloat(index);
                } else if (clazz == String.class) {
                    value = c.getString(index);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return value;
    }

    private static Object getInternal(ContentResolver resolver, String name[], Class<?> clazz) {
        Object value = null;
        Cursor c = null;
        try {
            c = resolver.query(CONTENT_URI, null, null, name, null);
            if (c != null && c.getCount() > 0 && c.moveToFirst()) {
                int index = 0;
                if (clazz == int.class) {
                    value = c.getInt(index);
                } else if (clazz == boolean.class) {
                    value = c.getInt(index) == 1;
                } else if (clazz == long.class) {
                    value = c.getLong(index);
                } else if (clazz == float.class) {
                    value = c.getFloat(index);
                } else if (clazz == String.class) {
                    value = c.getString(index);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return value;
    }

    /** feature options */
    public static boolean isFeatureOn(Context context, int resID, boolean... defValue) {
        boolean value = true;
        try {
            value = (defValue != null && defValue.length > 0) && defValue[0];
            value = context.getResources().getBoolean(resID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, "isFeatureOn(), feature: " + getFeatureName(resID) + ", enabled: " + value);
        return value;
    }

    private static String getFeatureName(int resID) {
        switch (resID) {
            case com.android.internal.R.bool.jrd_ims_feature_right_status_bar_icon:
                return "jrd_ims_feature_right_status_bar_icon";
            case com.android.internal.R.bool.jrd_ims_feature_left_status_bar_icon:
                return "jrd_ims_feature_left_status_bar_icon";
            case com.android.internal.R.bool.jrd_ims_feature_settings_item_ims:
                return "jrd_ims_feature_settings_item_ims";
            case com.android.internal.R.bool.jrd_ims_feature_settings_item_volte:
                return "jrd_ims_feature_settings_item_volte";
            case com.android.internal.R.bool.jrd_ims_feature_settings_item_vowifi:
                return "jrd_ims_feature_settings_item_vowifi";
            case com.android.internal.R.bool.jrd_ims_feature_function_disable_vowifi_on_roaming:
                return "jrd_ims_feature_function_disable_vowifi_on_roaming";
            case com.android.internal.R.bool.jrd_ims_feature_vowifi_popup:
                return "jrd_ims_feature_vowifi_popup";
        }
        return null;
    }

    /**
     * @hide
     * T949479 '''WIFI Calling''' bob.shen 2015-11-19
     * Check list for wifi calling.
     */
    public interface IMS {
        String FR_TASK_949479 = "T949479 WIFI Calling";

        @Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE, ElementType.CONSTRUCTOR})
        @Retention(RetentionPolicy.SOURCE)
        @interface JrdFeature {
            String[] value();

            String description() default "WFC";
        }
    }
}
