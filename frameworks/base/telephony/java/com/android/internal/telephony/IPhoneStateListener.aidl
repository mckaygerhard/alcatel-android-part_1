/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.telephony;

import android.os.Bundle;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.CellInfo;
import android.telephony.DataConnectionRealTimeInfo;
import android.telephony.PreciseCallState;
import android.telephony.PreciseDataConnectionState;
import android.telephony.VoLteServiceState;
import com.android.ims.ImsReasonInfo;

oneway interface IPhoneStateListener {
    void onServiceStateChanged(in ServiceState serviceState);
    void onSignalStrengthChanged(int asu);
    void onMessageWaitingIndicatorChanged(boolean mwi);
    void onCallForwardingIndicatorChanged(boolean cfi);

    // we use bundle here instead of CellLocation so it can get the right subclass
    void onCellLocationChanged(in Bundle location);
    void onCallStateChanged(int state, String incomingNumber);
    void onDataConnectionStateChanged(int state, int networkType);
    void onDataActivity(int direction);
    void onSignalStrengthsChanged(in SignalStrength signalStrength);
    void onOtaspChanged(in int otaspMode);
    void onCellInfoChanged(in List<CellInfo> cellInfo);
    void onPreciseCallStateChanged(in PreciseCallState callState);
    void onPreciseDataConnectionStateChanged(in PreciseDataConnectionState dataConnectionState);
    void onDataConnectionRealTimeInfoChanged(in DataConnectionRealTimeInfo dcRtInfo);
    void onVoLteServiceStateChanged(in VoLteServiceState lteState);
    void onOemHookRawEvent(in byte[] rawData);
    void onCarrierNetworkChange(in boolean active);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/19/2016, SOLUTION-2743874
//Porting WifiCall IR94 Interface which Gapp needeed
    // these interfaces are added for wifi calling
    // { T949479 '''WIFI Calling''' bob.shen 2015-11-19
    void onIMSRegisterStateChanged();
    void onIMSCallHandover(in boolean isHandoverSuccess, in int srcAccessTech, in int targetAccessTech, in ImsReasonInfo reasonInfo);
    // } T949479 bob.shen 2015-11-19

    //add by fzhang for 1278538
    void onVideoCapabilitiesChanged(boolean isVideoCapable);
    //add by fzhang for 1278538

    //Added by yuting-wang@tcl.com for defect 2226136 begin 2016-6-20
    void onIMSCallSrvccCompleted();
    //Added by yuting-wang@tcl.com for defect 2226136 end 2016-6-20

   //IR.94 fix for task 2127445, zhi-zhang@tcl.com, 2016/5/12
    void notifyForVideoCallingStateToHold(int state);
    void notifyForCallSessionSrvccChange(boolean isImsCall);
    //IR.94 fix for task 2127445, zhi-zhang@tcl.com, 2016/5/12
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
}

