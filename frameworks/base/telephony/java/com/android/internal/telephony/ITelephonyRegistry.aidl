/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.telephony;

import android.content.Intent;
import android.net.LinkProperties;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.telephony.CellInfo;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.CellInfo;
import android.telephony.VoLteServiceState;
import com.android.internal.telephony.IPhoneStateListener;
import com.android.internal.telephony.IOnSubscriptionsChangedListener;
import com.android.ims.ImsReasonInfo;

interface ITelephonyRegistry {
    void addOnSubscriptionsChangedListener(String pkg,
            IOnSubscriptionsChangedListener callback);
    void removeOnSubscriptionsChangedListener(String pkg,
            IOnSubscriptionsChangedListener callback);
    void listen(String pkg, IPhoneStateListener callback, int events, boolean notifyNow);
    void listenForSubscriber(in int subId, String pkg, IPhoneStateListener callback, int events,
            boolean notifyNow);
    void notifyCallState(int state, String incomingNumber);
    void notifyCallStateForPhoneId(in int phoneId, in int subId, int state, String incomingNumber);
    void notifyServiceStateForPhoneId(in int phoneId, in int subId, in ServiceState state);
    void notifySignalStrengthForPhoneId(in int phoneId, in int subId,
            in SignalStrength signalStrength);
    void notifyMessageWaitingChangedForPhoneId(in int phoneId, in int subId, in boolean mwi);
    void notifyCallForwardingChanged(boolean cfi);
    void notifyCallForwardingChangedForSubscriber(in int subId, boolean cfi);
    void notifyDataActivity(int state);
    void notifyDataActivityForSubscriber(in int subId, int state);
    void notifyDataConnection(int state, boolean isDataConnectivityPossible,
            String reason, String apn, String apnType, in LinkProperties linkProperties,
            in NetworkCapabilities networkCapabilities, int networkType, boolean roaming);
    void notifyDataConnectionForSubscriber(int subId, int state, boolean isDataConnectivityPossible,
            String reason, String apn, String apnType, in LinkProperties linkProperties,
            in NetworkCapabilities networkCapabilities, int networkType, boolean roaming);
    void notifyDataConnectionFailed(String reason, String apnType);
    void notifyDataConnectionFailedForSubscriber(int subId, String reason, String apnType);
    void notifyCellLocation(in Bundle cellLocation);
    void notifyCellLocationForSubscriber(in int subId, in Bundle cellLocation);
    void notifyOtaspChanged(in int otaspMode);
    void notifyCellInfo(in List<CellInfo> cellInfo);
    void notifyPreciseCallState(int ringingCallState, int foregroundCallState,
            int backgroundCallState);
    void notifyDisconnectCause(int disconnectCause, int preciseDisconnectCause);
    void notifyPreciseDataConnectionFailed(String reason, String apnType, String apn,
            String failCause);
    void notifyCellInfoForSubscriber(in int subId, in List<CellInfo> cellInfo);
    void notifyVoLteServiceStateChanged(in VoLteServiceState lteState);
    void notifyOemHookRawEventForSubscriber(in int subId, in byte[] rawData);
    void notifySubscriptionInfoChanged();
    void notifyCarrierNetworkChange(in boolean active);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/19/2016, SOLUTION-2743874
//Porting WifiCall IR94 Interface which Gapp needeed
    // { T949479 '''WIFI Calling''' bob.shen 2015-11-19
    void notifyIMSRegisterStateChanged();
    void notifyIMSCallHandover(in boolean isHandoverSuccess, in int srcAccessTech, in int targetAccessTech, in ImsReasonInfo reasonInfo);
    // } T949479 bob.shen 2015-11-19

    //add by fzhang for 1278538
    void notifyVideoCapabilitiesChanged(boolean isVideoCapable);
    //add by fzhang for 1278538

    //Added by yuting-wang@tcl.com for defect 2226136 begin 2016-6-20
    void notifyIMSCallSrvccCompleted();
    //Added by yuting-wang@tcl.com for defect 2226136 end 2016-6-20

    //IR.94 fix for task 2127445, zhi-zhang@tcl.com, 2016/5/12
    void notifyForVideoCallingStateToHold(int state);
    void notifyForCallSessionSrvccChange(boolean isImsCall);
    //IR.94 fix for task 2127445, zhi-zhang@tcl.com, 2016/5/12
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
}
