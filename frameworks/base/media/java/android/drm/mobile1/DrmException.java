package android.drm.mobile1;

import java.io.IOException;

/**
 * A DrmException is thrown to report errors specific to handle DRM content and rights.
 */
public class DrmException extends Exception {
    private DrmException() {}
    
    public DrmException(String message) {
        super(message);
    }
}
