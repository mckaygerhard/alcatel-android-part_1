
package com.android.systemui.tuner;

import com.android.systemui.statusbar.policy.NightModeController;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.preference.TwoStatePreference;
import android.support.v7.preference.PreferenceViewHolder;
import android.provider.Settings.Secure;
import android.util.AttributeSet;
import android.util.Log;
import android.util.MathUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Checkable;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.android.systemui.R;

public class RaidoPreferences extends TwoStatePreference {

    public RaidoPreferences(Context context, AttributeSet attrs, int defStyleAttr,
            int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        setWidgetLayoutResource(R.layout.preference_widget_radiobutton);
    }

    public RaidoPreferences(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public RaidoPreferences(Context context, AttributeSet attrs) {
        this(context, attrs, com.android.internal.R.attr.preferenceStyle);
    }

    public RaidoPreferences(Context context) {
        this(context, null);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder view)
    {
        super.onBindViewHolder(view);
        View mRadioButton = view.findViewById(R.id.radio);
        if (mRadioButton != null && mRadioButton instanceof Checkable) {
            ((Checkable) mRadioButton).setChecked(mChecked);
        }
    }

    @Override
    protected void onClick() {
        //do not change the check state.
    }
}
