/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************/
/*                                                               Date:12/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2014 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  yulong.zhang                                                    */
/*  Email  : yulong.zhang@jrdcom.com                                          */
/*  Role   :  SystemUI                                                        */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/*12/08/2014 |       yulong.zhang   |      FR-847639       |add a switch      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.systemui.statusbar.policy;

import java.util.ArrayList;

import com.android.systemui.qs.tiles.HotspotTile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.AsyncTask;
import android.util.Log;
// defect 1223477 begin  Author:yang wei  email:wei-yang@tcl.com  Date: 2015-01-01
import android.os.UserHandle;
// defect 1223477 end
public class NfcControllerImpl implements NfcController {

    private static final String TAG = "NfcController";
    private static final boolean DEBUG = true;

    private Context mContext;
    private NfcAdapter mNfcAdapter;
    private boolean mEnabled; //[BUGFIX]-ADD BEGIN by TCTNB.Ji.Chen,2016/05/14,PR2120562
    private final ArrayList<Callback> mCallbacks = new ArrayList<Callback>();
    private final Receiver mReceiver = new Receiver();

    public NfcControllerImpl(Context context){
        mContext=context;
        //added by ping.li for FR522968, begin.30/10/2015
        try {
            mNfcAdapter = NfcAdapter.getNfcAdapter(mContext);
        } catch (UnsupportedOperationException e) {
            mNfcAdapter = null;
            Log.e(TAG, "get NFC adapter error! (UnsupportedOperationException)");
        } catch (Exception e) {
            mNfcAdapter = null;
            System.gc();
            Log.e(TAG, e.getMessage());
        }
        //added by ping.li for FR522968, end.30/10/2015
    }

    public void addCallback(Callback callback) {
        if (callback == null || mCallbacks.contains(callback)) return;
        if (DEBUG) Log.d(TAG, "addCallback " + callback);
        mCallbacks.add(callback);
        mReceiver.setListening(!mCallbacks.isEmpty());
    }

    public void removeCallback(Callback callback) {
        if (callback == null) return;
        if (DEBUG) Log.d(TAG, "removeCallback " + callback);
        mCallbacks.remove(callback);
        mReceiver.setListening(!mCallbacks.isEmpty());
    }

    @Override
    public boolean isNfcEnabled() {
        //[BUGFIX]-ADD BEGIN by TCTNB.Ji.Chen,2016/05/14,PR2120562
       // return isNfcSupported() && mNfcAdapter.isEnabled();
       return isNfcSupported() && mEnabled;
    }

    @Override
    public boolean isNfcSupported() {
        try {
            mNfcAdapter = NfcAdapter.getNfcAdapter(mContext);
            if (mNfcAdapter == null) {
                return false;
            } else {
                return true;
            }
        } catch (UnsupportedOperationException e) {
            Log.e(TAG, "get NFC adapter error! (UnsupportedOperationException)");
            return false;
        }
    }

    @Override
    public void setNfcEnabled(boolean enabled) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... args) {
                try {
                    if (DEBUG) {
                        Log.d(TAG, "NFC onClick: mNfcAdapter.isEnabled()="
                                + (mNfcAdapter != null ? mNfcAdapter.isEnabled()
                                        : "NFC adapter doesn't start"));
                    }
                    if (mNfcAdapter.isEnabled()) {
                        mNfcAdapter.disable();
                    } else {
                        mNfcAdapter.enable();
                    }
                } catch (UnsupportedOperationException e) {
                    Log.e(TAG, "get NFC adapter error.", e.fillInStackTrace());
                }
                return null;
            }
        }.execute();
    }

    private void fireCallback(boolean isEnabled) {
        for (Callback callback : mCallbacks) {
            callback.onNfcChanged(isEnabled);
        }
    }

    private final class Receiver extends BroadcastReceiver {
        private boolean mRegistered;

        public void setListening(boolean listening) {
            if (listening && !mRegistered) {
                if (DEBUG) Log.d(TAG, "Registering receiver");
                final IntentFilter filter = new IntentFilter();
                filter.addAction(NfcAdapter.ACTION_ADAPTER_STATE_CHANGED);
// defect 1223477 begin  Author:yang wei  email:wei-yang@tcl.com  Date: 2015-01-01
                //mContext.registerReceiver(this, filter);
                mContext.registerReceiverAsUser(this, UserHandle.ALL, filter, null, null);
// end
                mRegistered = true;
            } else if (!listening && mRegistered) {
                if (DEBUG) Log.d(TAG, "Unregistering receiver");
                mContext.unregisterReceiver(this);
                mRegistered = false;
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (DEBUG) Log.d(TAG, "onReceive " + intent.getAction());
            if (DEBUG) {
                Log.d(TAG, "NFC intent=" + intent + " extras="
                        + intent.getExtras().getInt(NfcAdapter.EXTRA_ADAPTER_STATE));
            }
            if (isNfcSupported()) {
                //[BUGFIX]-ADD BEGIN by TCTNB.Ji.Chen,2016/05/14,PR2120562
                mEnabled = intent.getIntExtra(NfcAdapter.EXTRA_ADAPTER_STATE,
                        NfcAdapter.STATE_OFF) == NfcAdapter.STATE_ON;
                fireCallback(mEnabled);
                //[BUGFIX]-ADD END Ji.Chen
            }
        }
     }
}
