package com.android.systemui.qs.tiles;

import android.content.Context;
import android.media.AudioManager;
import android.provider.Settings.Global;
import android.util.Log;
import android.widget.Toast;
import android.content.Intent;

import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import android.provider.Settings;
import android.provider.Settings.Global;
import com.android.systemui.R;
import com.android.systemui.SysUIToast;
import com.android.systemui.qs.QSTile;
import com.android.systemui.qs.QSTile.BooleanState;
import com.android.systemui.statusbar.policy.RingController;
import com.android.systemui.statusbar.policy.ZenModeController;

public class RingTile extends QSTile<QSTile.BooleanState> {

    private final RingController mController;
    private final AudioManager mAudio;
    private final ZenModeController mDndController;
    private long mClickTime; //[BUGFIX]-Mod-BEGIN by TCTNB.yubin.ying,09/19/2016,Solution-2521300,
    public RingTile(Host host) {
       super(host);
       mDndController = host.getZenModeController();
       mController = host.getRingController();
       mAudio = (AudioManager)mContext.getSystemService(Context.AUDIO_SERVICE);
    }

    @Override
    public void setListening(boolean listening) {
        if (true) {
            mController.addStateChangedCallback(mCallback);
        } else {
            mController.removeStateChangedCallback(mCallback);
        }
    }

    @Override
    public BooleanState newTileState() {
        return new BooleanState();
    }

    @Override
    public CharSequence getTileLabel() {
        return mContext.getString(R.string.ring_mode_silent);
    }

    @Override
    public Intent getLongClickIntent() {
       return new Intent(Settings.ACTION_SOUND_SETTINGS);
    }

    @Override
    protected void handleClick() {
        //[BUGFIX]-Mod-BEGIN by TCTNB.yubin.ying,09/13/2016,Solution-2521300,
        final int zen = mDndController.getZen();
        if(zen == Global.ZEN_MODE_NO_INTERRUPTIONS || zen == Global.ZEN_MODE_IMPORTANT_INTERRUPTIONS ||
            zen ==	Global.ZEN_MODE_ALARMS){
            //[BUGFIX]-Mod-BEGIN by TCTNB.yubin.ying,09/19/2016,Solution-2521300,
            if(System.currentTimeMillis() - mClickTime <5000){
                mDndController.setZen(Global.ZEN_MODE_OFF, null, TAG);
                SysUIToast.makeText(mContext, mContext.getString(R.string.accessibility_quick_settings_dnd_changed_off),Toast.LENGTH_LONG).show();
                return;
            }
            //[BUGFIX]-Mod-END by TCTNB.yubin.ying
            mHost.collapsePanels();
            SysUIToast.makeText(mContext, mContext.getString(R.string.dnd_mode_on),Toast.LENGTH_LONG).show();
            mClickTime = System.currentTimeMillis();
            return;
        }else{
        int ringMode = mAudio.getRingerMode();
        Log.d("RingTileClick","ringMode is :  "+ringMode);
        //add by jiajunwu for Defect1019467 2015-12-03 begin
        if(ringMode == AudioManager.RINGER_MODE_SILENT){
           Log.d("RingTileClick","setRingMode:  "+AudioManager.RINGER_MODE_NORMAL);
           mController.setRingMode(AudioManager.RINGER_MODE_NORMAL);
        }else if(ringMode == AudioManager.RINGER_MODE_VIBRATE){
           Log.d("RingTileClick","setRingMode:  "+AudioManager.RINGER_MODE_SILENT);
           mController.setRingMode(AudioManager.RINGER_MODE_SILENT);
        }else if(ringMode == AudioManager.RINGER_MODE_NORMAL){
           Log.d("RingTileClick","setRingMode:  "+AudioManager.RINGER_MODE_VIBRATE);
           mController.setRingMode(AudioManager.RINGER_MODE_VIBRATE);
        }
        //[BUGFIX]-Mod-END by TCTNB.yubin.ying
      }
    }

    @Override
    protected void handleUpdateState(BooleanState state, Object arg) {
        //state.visible = true;
        state.value = mController.isSilentMode();
        if (mAudio.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
          state.icon = ResourceIcon.get(R.drawable.ic_qs_mute);
          state.label = mContext.getString(R.string.ring_mode_silent);
        } else if(mAudio.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE) {
          state.icon = ResourceIcon.get(R.drawable.ic_qs_vibrate);
          state.label = mContext.getString(R.string.ring_mode_vibrate);
        }else if(mAudio.getRingerMode() == AudioManager.RINGER_MODE_NORMAL){
          state.icon = ResourceIcon.get(R.drawable.ic_qs_sound);
          state.label = mContext.getString(R.string.ring_mode_normal);
        }
    }


     RingController.RingStateChangeCallback mCallback = new RingController.RingStateChangeCallback() {
        @Override
        public void onRingStateChanged() {
          refreshState();
        }
     };

	 @Override
     protected String composeChangeAnnouncement() {
        if (mController.whichRingmode() == AudioManager.RINGER_MODE_SILENT) {
          return mContext.getString(R.string.accessibility_quick_settings_ring_silent);
        } else if(mController.whichRingmode() == AudioManager.RINGER_MODE_VIBRATE) {
          return mContext.getString(R.string.accessibility_quick_settings_ring_vibrate);
        }else if(mController.whichRingmode() == AudioManager.RINGER_MODE_NORMAL){
          return mContext.getString(R.string.accessibility_quick_settings_ring_normal);
        }else return null;
     }

    @Override
    public int getMetricsCategory() {
         return  MetricsEvent.ACTION_RINGER_MODE;
    }
}
