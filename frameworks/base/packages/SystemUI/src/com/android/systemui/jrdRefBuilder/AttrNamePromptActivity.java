/* Copyright (C) 2016 Tcl Corporation Limited */
/* ***************************************************************************/
/*                                                       Date : Feb 2, 2015  */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2015 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Hanwu.xie (XHANWU)                                            */
/*   Role :    Telecom Leader                                                */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : AttrNamePromptActivity.java                                  */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*02/02/15 | hanwu.xie | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.android.systemui.jrdRefBuilder;

import com.android.systemui.R;

import android.app.AttrNameManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemProperties;
import android.util.Slog;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.app.AttrNameManager;
import android.view.WindowManager;
import android.view.Display;

public class AttrNamePromptActivity extends Activity {
    private static final String TAG = "AttrNamePromptActivity";
    private static final String NO_WAIT = "nowait";
    private static final String INTERVAL = "interval";
    private static final String WINDOW = "window";

    private static final int DIALOG_PROMPT = 1;
    private String promptMessage = "";
    private boolean toStartTool;

    private AttrNameManager attrNameManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        attrNameManager = (AttrNameManager) getSystemService(Context.JRD_ATTR_NAME_SERVICE);
        if (attrNameManager == null) {
            return;
        }

        toStartTool = getIntent().getBooleanExtra("ToStartTool", false);
        if (toStartTool) {
            promptMessage = "The MS need restart to enable to String detect functionality!";
        } else {
            promptMessage = "The MS need restart to disable to String detect functionality!\nPlease make sure you have found your wanted string information, or have export the information to file system!";
        }
        // Adjust the dialog size of the screen
        Dialog mDialog = createDialog();
        Display display = getWindowManager().getDefaultDisplay();
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.width = display.getWidth();
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mDialog.getWindow().setAttributes(lp);
        mDialog.show();
    }

    /**
     * Create managed dialog.
     *
     * @return Dialog The dialog we created.
     */
    private Dialog createDialog() {

        final CharSequence promt = promptMessage;
        return new AlertDialog.Builder(this)
                .setTitle("NOTE")
                .setMessage(promt)
                .setPositiveButton(this.getResources().getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                attrNameManager.setAttrNameServiceStart(toStartTool);

                                // Reboot to start the detect tool
                                Intent intentReboot = new Intent(Intent.ACTION_REBOOT);
                                intentReboot.putExtra(NO_WAIT, 1);
                                intentReboot.putExtra(INTERVAL, 1);
                                intentReboot.putExtra(WINDOW, 0);
                                sendBroadcast(intentReboot);
                                finish();

                            }
                        })
                .setNegativeButton(this.getResources().getString(android.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                finish(); // close current activity
                            }
                        }).create();
    }
}
