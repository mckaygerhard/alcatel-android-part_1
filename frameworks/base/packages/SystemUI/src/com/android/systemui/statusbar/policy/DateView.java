/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.statusbar.policy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.icu.text.DateFormat;
import android.icu.text.DisplayContext;
import android.util.AttributeSet;
import android.widget.TextView;
import android.provider.Settings;

import com.android.systemui.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
import android.content.res.Resources;
import android.provider.Settings;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

public class DateView extends TextView {
    private static final String TAG = "DateView";

    private final Date mCurrentTime = new Date();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
    //private DateFormat mDateFormat;
    private SimpleDateFormat mDateFormat;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    private String mLastText;
    private String mDatePattern;

    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (Intent.ACTION_TIME_TICK.equals(action)
                    || Intent.ACTION_TIME_CHANGED.equals(action)
                    || Intent.ACTION_TIMEZONE_CHANGED.equals(action)
                    || Intent.ACTION_LOCALE_CHANGED.equals(action)) {
                if (Intent.ACTION_LOCALE_CHANGED.equals(action)
                        || Intent.ACTION_TIMEZONE_CHANGED.equals(action)) {
                    // need to get a fresh date format
                    mDateFormat = null;
                }
                updateClock();
            }
        }
    };

    public DateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.DateView,
                0, 0);

        try {
            mDatePattern = a.getString(R.styleable.DateView_datePattern);
        } finally {
            a.recycle();
        }
        if (mDatePattern == null) {
            mDatePattern = getContext().getString(R.string.system_ui_date_pattern);
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
        //Task528413
        if (Locale.getDefault().getLanguage().equals("ru")) {
            int start = mDatePattern.toUpperCase().indexOf("E");
            int last = mDatePattern.toUpperCase().lastIndexOf("E");
            String weekDay = "";
            if (start != -1) {
                weekDay = mDatePattern.substring(start, last + 1);
            }
            mDatePattern = weekDay + " dd.MM.yyyy";
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        filter.addAction(Intent.ACTION_LOCALE_CHANGED);
        getContext().registerReceiver(mIntentReceiver, filter, null, null);

        updateClock();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mDateFormat = null; // reload the locale next time
        getContext().unregisterReceiver(mIntentReceiver);
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
    public void updateClock() {
        if (mDateFormat == null) {
            final Locale l = Locale.getDefault();
            //DateFormat format = DateFormat.getInstanceForSkeleton(mDatePattern, l);
            //format.setContext(DisplayContext.CAPITALIZATION_FOR_STANDALONE);
            //mDateFormat = format;
            String fmt = android.text.format.DateFormat.getBestDateTimePattern(l, mDatePattern);
            if (l.getLanguage().equals("ru")) {
/* MODIFIED-BEGIN by wei.zhang-nb, 2016-10-20,BUG-3146359*/
/*
                String yearInRussian = " '?'.";
                fmt = fmt.replace(yearInRussian.toLowerCase(), "");
                fmt = fmt.replace(yearInRussian.toUpperCase(), "");
*/
                android.util.Log.d("DateView","fmt: " + fmt);
                int index = fmt.indexOf(" \'");
                if (index != -1) {
                    fmt = fmt.substring(0, index);
                }
                /* MODIFIED-END by wei.zhang-nb,BUG-3146359*/

            }


            mDateFormat = new SimpleDateFormat(fmt, l);
        }

        mCurrentTime.setTime(System.currentTimeMillis());

        final String text = getDateFormat();

        if (!text.equals(mLastText)) {
            setText(text);
            mLastText = text;
        }
    }

    //[Date format should be DD.MM.YYYY for Russian and CIS for task528426
    private String getDateFormat() {
        final Resources res = getContext().getResources();
        String result = "";
        if (null == res) {
            return result;
        }

        if (res.getBoolean(com.android.internal.R.bool.def_tctfw_systemUI_Dateformat_same_with_setting)) {
            String dateView = "";
            final String split = res.getString(com.android.internal.R.string.systemui_date_format_spilt);
            final String dateformat = Settings.System.getString(getContext().getContentResolver(),
                    Settings.System.DATE_FORMAT);
            if (null == dateformat || "".equals(dateformat)) {
                java.text.DateFormat format = null;
                format = java.text.DateFormat.getDateInstance(java.text.DateFormat.DEFAULT);
                if (null == format) {
                    return result;
                }
                String date = format.format(mCurrentTime);
                if (null != date) {
                    String orgial = "";
                    if (-1 != date.indexOf("/")) {
                        orgial = "/";
                    } else if (-1 != date.indexOf("-")) {
                        orgial = "-";
                    }

                    if (!orgial.isEmpty()) {
                        date = date.replaceAll(orgial, split);
                    }
                }
                String week = "";
                try {
                    week = new SimpleDateFormat("EEEE", Locale.getDefault()).format(mCurrentTime);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

                //Defect2475132
                boolean mJapanesePerso = getContext().getResources().getBoolean(com.android.internal.R.bool.feature_tctfw_Japanese_perso_on);
                if (mJapanesePerso) {
                    result = date + "  " + week;
                } else {
                    result = week + "," + " " + date;
                }

                return result;
            }

            dateView = dateformat;
            String orgial = "";
            if (-1 != dateformat.indexOf("/")) {
                orgial = "/";
            } else if (-1 != dateformat.indexOf("-")) {
                orgial = "-";
            }
            if (!orgial.isEmpty()) {
                dateView = dateformat.replaceAll(orgial, split);
            }
            //Defect2475132
            boolean mJapanesePerso = getContext().getResources().getBoolean(com.android.internal.R.bool.feature_tctfw_Japanese_perso_on);
            if (mJapanesePerso) {
                dateView = dateView  + "  " + "EEEE" ;
            } else {
                dateView = "EEEE" + "," + " " + dateView;
            }

            try {
                result = new SimpleDateFormat(dateView).format(mCurrentTime);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            return result;
        }  else {
            if (getContext().getResources().getBoolean(com.android.internal.R.bool.config_dateformat)) {
                String dateformat = Settings.System.getString(getContext().getContentResolver(),
                        Settings.System.DATE_FORMAT);
                return android.text.format.DateFormat.format(dateformat, mCurrentTime).toString();
            } else {
                return mDateFormat.format(mCurrentTime);
            }
        }
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
}
