/**
 * Copyright (c) 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.tuner;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.Nullable;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.app.UiModeManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.ContentObservable;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceClickListener;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.MathUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TimePicker;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.systemui.R;
import com.android.systemui.statusbar.policy.NightModeController;
import com.android.systemui.statusbar.policy.NightModeController.Listener;
import com.android.systemui.tuner.TunerService.Tunable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SchedulePlanFragment extends PreferenceFragment implements OnPreferenceClickListener,
        DialogCreatable {

    private static final String TAG = "SchedulePlanFragment";

    private static final String KEY_SUN = "sunset_to_sunrise";

    private static final String KEY_CUSTOM = "custom_schedule";

    private static final String KEY_TURN_ON = "turn_on";

    private static final String KEY_TURN_OFF = "turn_off";

    private static final int DIALOG_TIMEPICKER_ON = 0;

    private static final int DIALOG_TIMEPICKER_OFF = 1;

    private RaidoPreferences mSunPerf, mCustomPerf;

    private Preference mTurnOnPerf, mTurnOffPerf;

    private CustomeDialogFragment mDialogFragment;

    private ContentResolver resolver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        resolver = getActivity().getContentResolver();
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
    {
        Log.d(TAG, "onCreatePreferences");
        final Context context = getPreferenceManager().getContext();

        addPreferencesFromResource(R.xml.schedule_plan);
        mSunPerf = (RaidoPreferences) findPreference(KEY_SUN);
        mSunPerf.setOnPreferenceClickListener(this);
        mSunPerf.setChecked(false);
        mCustomPerf = (RaidoPreferences) findPreference(KEY_CUSTOM);
        mCustomPerf.setOnPreferenceClickListener(this);
        mCustomPerf.setChecked(false);

        String currentPlan = TunerService.get(getActivity()).getValue(
                Secure.NIGHT_MODE_CUSTOM_SCHEDULE_PLAN);
        if (currentPlan != null)
        {
            switch (currentPlan)
            {
                case "custom":
                    mCustomPerf.setChecked(true);
                    break;
                case "sun":
                    mSunPerf.setChecked(true);
                    break;
                default:
                    mCustomPerf.setChecked(true);
                    break;
            }
        }

        mTurnOnPerf = findPreference(KEY_TURN_ON);
        mTurnOnPerf.setEnabled(mCustomPerf.isChecked());
        mTurnOnPerf.setOnPreferenceClickListener(this);

        mTurnOffPerf = findPreference(KEY_TURN_OFF);
        // mTurnOffPerf.setSummary(DateFormat.getTimeFormat(getActivity()).format(now.getTime()));
        mTurnOffPerf.setEnabled(mCustomPerf.isChecked());
        mTurnOffPerf.setOnPreferenceClickListener(this);
    }

    private void updatePreferenceSummary()
    {
        String mTurnOnTime = TunerService.get(getActivity()).getValue(
                Secure.NIGHT_MODE_CUSTOM_SCHEDULE_ON);
        String mTurnOffTime = TunerService.get(getActivity()).getValue(
                Secure.NIGHT_MODE_CUSTOM_SCHEDULE_OFF);

        Calendar c = Calendar.getInstance();
        final SimpleDateFormat s = new SimpleDateFormat("HH:mm");

        // set mTurnOnPerf summary
        try
        {
            Date d = s.parse(mTurnOnTime);
            c.set(Calendar.HOUR_OF_DAY, d.getHours());
            c.set(Calendar.MINUTE, d.getMinutes());
        } catch (ParseException e)
        {
            e.printStackTrace();
        } catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        mTurnOnPerf.setSummary(DateFormat.getTimeFormat(getActivity()).format(c.getTime()));

        // set mTurnOffPerf summary
        c = Calendar.getInstance();
        try
        {
            Date d = s.parse(mTurnOffTime);
            c.set(Calendar.HOUR_OF_DAY, d.getHours());
            c.set(Calendar.MINUTE, d.getMinutes());
        } catch (ParseException e)
        {
            e.printStackTrace();
        } catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        mTurnOffPerf.setSummary(DateFormat.getTimeFormat(getActivity()).format(c.getTime()));
    }

    @Override
    public boolean onPreferenceClick(Preference preference)
    {
        if (preference == mCustomPerf)
        {
            final boolean currentValue = mCustomPerf.isChecked();
            if (!currentValue)
            {
                final boolean newValue = !currentValue;
                TunerService.get(getActivity()).setValue(Secure.NIGHT_MODE_CUSTOM_SCHEDULE_PLAN,
                        "custom");
                getView().post(new Runnable() {
                    @Override
                    public void run()
                    {
                        mCustomPerf.setChecked(newValue);
                        mSunPerf.setChecked(!newValue);
                        mTurnOnPerf.setEnabled(newValue);
                        mTurnOffPerf.setEnabled(newValue);
                    }
                });
            }
        } else if (preference == mSunPerf)
        {
            final boolean currentValue = mSunPerf.isChecked();
            if (!currentValue)
            {
                final boolean newValue = !currentValue;
                TunerService.get(getActivity()).setValue(Secure.NIGHT_MODE_CUSTOM_SCHEDULE_PLAN,
                        "sun");
                getView().post(new Runnable() {
                    @Override
                    public void run()
                    {
                        mSunPerf.setChecked(newValue);
                        mCustomPerf.setChecked(!newValue);
                        mTurnOnPerf.setEnabled(newValue);
                        mTurnOffPerf.setEnabled(newValue);
                    }
                });
            }
        } else if (preference == mTurnOnPerf)
        {
            removeDialog(DIALOG_TIMEPICKER_ON);
            showDialog(DIALOG_TIMEPICKER_ON);
        } else if (preference == mTurnOffPerf)
        {
            removeDialog(DIALOG_TIMEPICKER_OFF);
            showDialog(DIALOG_TIMEPICKER_OFF);
        }
        return true;
    }

    protected void showDialog(int dialogId)
    {
        if (mDialogFragment != null)
        {
            Log.e(TAG, "Old dialog fragment not null!");
        }
        mDialogFragment = new CustomeDialogFragment(this, dialogId);
        mDialogFragment.show(getChildFragmentManager(), Integer.toString(dialogId));
    }

    protected void removeDialog(int dialogId)
    {
        // mDialogFragment may not be visible yet in parent fragment's
        // onResume().
        // To be able to dismiss dialog at that time, don't check
        // mDialogFragment.isVisible().
        if (mDialogFragment != null && mDialogFragment.getDialogId() == dialogId)
        {
            mDialogFragment.dismiss();
        }
        mDialogFragment = null;
    }

    public static class CustomeDialogFragment extends DialogFragment {
        private static final String KEY_DIALOG_ID = "key_dialog_id";

        private static final String KEY_PARENT_FRAGMENT_ID = "key_parent_fragment_id";

        private int mDialogId;

        private Fragment mParentFragment;

        private DialogInterface.OnCancelListener mOnCancelListener;

        private DialogInterface.OnDismissListener mOnDismissListener;

        public CustomeDialogFragment() {
            /* do nothing */
        }

        public CustomeDialogFragment(DialogCreatable fragment, int dialogId) {
            mDialogId = dialogId;
            if (!(fragment instanceof Fragment))
            {
                throw new IllegalArgumentException("fragment argument must be an instance of "
                        + Fragment.class.getName());
            }
            mParentFragment = (Fragment) fragment;
        }

        @Override
        public void onSaveInstanceState(Bundle outState)
        {
            super.onSaveInstanceState(outState);
            if (mParentFragment != null)
            {
                outState.putInt(KEY_DIALOG_ID, mDialogId);
                outState.putInt(KEY_PARENT_FRAGMENT_ID, mParentFragment.getId());
            }
        }

        @Override
        public void onStart()
        {
            super.onStart();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState)
        {
            if (savedInstanceState != null)
            {
                mDialogId = savedInstanceState.getInt(KEY_DIALOG_ID, 0);
                mParentFragment = getParentFragment();
                int mParentFragmentId = savedInstanceState.getInt(KEY_PARENT_FRAGMENT_ID, -1);
                if (mParentFragment == null)
                {
                    mParentFragment = getFragmentManager().findFragmentById(mParentFragmentId);
                }
                if (!(mParentFragment instanceof DialogCreatable))
                {
                    throw new IllegalArgumentException((mParentFragment != null ? mParentFragment
                            .getClass().getName() : mParentFragmentId)
                            + " must implement "
                            + DialogCreatable.class.getName());
                }
                // This dialog fragment could be created from
                // non-SettingsPreferenceFragment
                if (mParentFragment instanceof SchedulePlanFragment)
                {
                    // restore mDialogFragment in mParentFragment
                    ((SchedulePlanFragment) mParentFragment).mDialogFragment = this;
                }
            }
            return ((DialogCreatable) mParentFragment).onCreateDialog(mDialogId);
        }

        @Override
        public void onCancel(DialogInterface dialog)
        {
            super.onCancel(dialog);
            if (mOnCancelListener != null)
            {
                mOnCancelListener.onCancel(dialog);
            }
        }

        @Override
        public void onDismiss(DialogInterface dialog)
        {
            super.onDismiss(dialog);
            if (mOnDismissListener != null)
            {
                mOnDismissListener.onDismiss(dialog);
            }
        }

        public int getDialogId()
        {
            return mDialogId;
        }

        @Override
        public void onDetach()
        {
            super.onDetach();

            // This dialog fragment could be created from
            // non-SettingsPreferenceFragment
            if (mParentFragment instanceof SchedulePlanFragment)
            {
                // in case the dialog is not explicitly removed by
                // removeDialog()
                if (((SchedulePlanFragment) mParentFragment).mDialogFragment == this)
                {
                    ((SchedulePlanFragment) mParentFragment).mDialogFragment = null;
                }
            }
        }
    }

    @Override
    public Dialog onCreateDialog(int id)
    {
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat s = new SimpleDateFormat("HH:mm");
        switch (id)
        {
            case DIALOG_TIMEPICKER_ON:
                String onTime = TunerService.get(getActivity()).getValue(
                        Secure.NIGHT_MODE_CUSTOM_SCHEDULE_ON);
                try
                {
                    Date d = s.parse(onTime);
                    calendar.set(Calendar.HOUR_OF_DAY, d.getHours());
                    calendar.set(Calendar.MINUTE, d.getMinutes());
                } catch (ParseException e)
                {
                    e.printStackTrace();
                } catch (NullPointerException e)
                {
                    e.printStackTrace();
                }
                return new TimePickerDialog(getActivity(), new OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute)
                    {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        c.set(Calendar.MINUTE, minute);
                        /* MODIFIED-BEGIN by yongliang.zhu, 2016-12-06,BUG-3649893*/
                        c.set(Calendar.SECOND,0);
                        c.set(Calendar.MILLISECOND,0);
                        /* MODIFIED-END by yongliang.zhu,BUG-3649893*/
                        SimpleDateFormat s = new SimpleDateFormat("HH:mm");
                        String time = s.format(c.getTime());

                        Log.d("John", "onTimeSet: " + time);
                        TunerService.get(getActivity()).setValue(
                                Secure.NIGHT_MODE_CUSTOM_SCHEDULE_ON, time);
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                        DateFormat.is24HourFormat(getActivity()));
            case DIALOG_TIMEPICKER_OFF:
                String offTime = TunerService.get(getActivity()).getValue(
                        Secure.NIGHT_MODE_CUSTOM_SCHEDULE_OFF);
                try
                {
                    Date d = s.parse(offTime);
                    calendar.set(Calendar.HOUR_OF_DAY, d.getHours());
                    calendar.set(Calendar.MINUTE, d.getMinutes());
                } catch (ParseException e)
                {
                    e.printStackTrace();
                } catch (NullPointerException e)
                {
                    e.printStackTrace();
                }
                return new TimePickerDialog(getActivity(), new OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute)
                    {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        c.set(Calendar.MINUTE, minute);
                        /* MODIFIED-BEGIN by yongliang.zhu, 2016-12-06,BUG-3649893*/
                        c.set(Calendar.SECOND,0);
                        c.set(Calendar.MILLISECOND,0);
                        /* MODIFIED-END by yongliang.zhu,BUG-3649893*/
                        SimpleDateFormat s = new SimpleDateFormat("HH:mm");
                        String time = s.format(c.getTime());
                        Log.d("John", "onTimeSet: " + time);
                        TunerService.get(getActivity()).setValue(
                                Secure.NIGHT_MODE_CUSTOM_SCHEDULE_OFF, time);
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                        DateFormat.is24HourFormat(getActivity()));
            default:
                throw new IllegalArgumentException();
        }
    }

    private final ContentObserver mContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange)
        {
            super.onChange(selfChange);
            updatePreferenceSummary();
        }
    };

    @Override
    public void onResume()
    {
        super.onResume();
        updatePreferenceSummary();
        resolver.registerContentObserver(
                Settings.Secure.getUriFor(Secure.NIGHT_MODE_CUSTOM_SCHEDULE_ON), false,
                mContentObserver);
        resolver.registerContentObserver(
                Settings.Secure.getUriFor(Secure.NIGHT_MODE_CUSTOM_SCHEDULE_OFF), false,
                mContentObserver);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        resolver.unregisterContentObserver(mContentObserver);
    }
}
