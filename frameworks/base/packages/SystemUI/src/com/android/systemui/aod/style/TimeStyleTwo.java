/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.systemui.aod.style;

import java.util.Calendar;
import java.util.TimeZone;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.systemui.R;

public class TimeStyleTwo extends LinearLayout {

    Context mContext;

    private String mHour;
    private String mMinute;

    private TextView mHourView;
    private TextView mMinuteView;

    private final int HOUR = 1;
    private final int MINUTE = 2;

    public TimeStyleTwo(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
    }

    @Override
    protected void onFinishInflate() {
        // TODO Auto-generated method stub
        super.onFinishInflate();
        prepare();
    }

    private void prepare() {
        // TODO Auto-generated method stub
        mHourView = (TextView) this.findViewById(R.id.time_hour);
        mHourView.setText(getData(HOUR));
        mMinuteView = (TextView) this.findViewById(R.id.time_min);
        mMinuteView.setText(getData(MINUTE));
    }

    private String getData(int data) {
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
        switch (data) {
        case (HOUR):
            /* MODIFIED-BEGIN by song.huan, 2016-10-28,BUG-3269387*/
            mHour = transferTimeFormat(String.valueOf(c.get(Calendar.HOUR_OF_DAY)));
            return mHour;
        case (MINUTE):
            mMinute = transferTimeFormat(String.valueOf(c.get(Calendar.MINUTE)));
            return mMinute;
        default:
            return "";
        }
    }

    private String transferTimeFormat(String time) {
        if(!time.equals("")) {
            if(time.length() == 1) {
                return "0" + time;
            }
        }
        return time;
    }
    /* MODIFIED-END by song.huan,BUG-3269387*/
}
