/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.systemui.aod.style;

import java.util.Calendar;
import java.util.TimeZone;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.systemui.R;


public class TimeStyleFour extends LinearLayout {

    Context mContext;

    private String mMonth;
    private String mDay;
    private String mWeek;

    private TextView mDataView;

    public TimeStyleFour(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
    }

    @Override
    protected void onFinishInflate() {
        // TODO Auto-generated method stub
        super.onFinishInflate();
        prepare();
    }

    private void prepare() {
        // TODO Auto-generated method stub
        mDataView = (TextView) this.findViewById(R.id.data_view);
        mDataView.setText(getData());
    }

    private String getData() {
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
        mMonth = String.valueOf(c.get(Calendar.MONTH) + 1);
        mDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
        mWeek = String.valueOf(c.get(Calendar.DAY_OF_WEEK));
        if ("1".equals(mWeek)) {
            mWeek = "SUN";
        } else if ("2".equals(mWeek)) {
            mWeek = "MON";
        } else if ("3".equals(mWeek)) {
            mWeek = "TUS";
        } else if ("4".equals(mWeek)) {
            mWeek = "WED";
        } else if ("5".equals(mWeek)) {
            mWeek = "THU";
        } else if ("6".equals(mWeek)) {
            mWeek = "FRI";
        } else if ("7".equals(mWeek)) {
            mWeek = "SAT";
        }

        if("1".equals(mMonth)){
            mMonth = "Jan";
        }else if("2".equals(mMonth)){
            mMonth = "Feb";
        }else if ("3".equals(mMonth)) {
            mMonth = "Mar";
        }else if("4".equals(mMonth)){
            mMonth = "Apr";
        }else if ("5".equals(mMonth)) {
            mMonth = "May";
        }else if ("6".equals(mMonth)) {
            mMonth = "Jun";
        }else if ("7".equals(mMonth)) {
            mMonth = "Jul";
        }else if ("8".equals(mMonth)) {
            mMonth = "Aug";
        }else if ("9".equals(mMonth)) {
            mMonth = "Sep";
        }else if ("10".equals(mMonth)) {
            mMonth = "Oct";
        }else if ("11".equals(mMonth)) {
            mMonth = "Nov";
        }else if ("12".equals(mMonth)) {
            mMonth = "Dec";
        }
        return mMonth + " " + mDay + ", " + mWeek;

    }
}
