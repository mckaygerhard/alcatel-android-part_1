/* Copyright (C) 2016 Tcl Corporation Limited */
/* **********************************************************************************/
/*                                                       Date : Feb 2, 2015 */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2015 JRD Communications, Inc.                  */
/* **********************************************************************************/
/*                                                                          */
/*    This material is company confidential, cannot be reproduced in any      */
/*    form without the written permission of JRD Communications, Inc.         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Hanwu.xie (XHANWU)                                            */
/*   Role :    Telecom Leader                                                 */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : PersoStringBuilderEx.java                                    */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*02/02/15 | hanwu.xie | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.android.systemui.jrdRefBuilder;

import com.android.systemui.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.AttrNameManager;
import android.app.Activity;
import android.app.StringMapParcelable;
import android.app.ValueItem;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemProperties;
import android.util.Slog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.AdapterView.OnItemClickListener;

import jxl.*;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import android.content.DialogInterface;
import android.app.AlertDialog;
import android.os.Handler;
import android.os.Looper;
import android.view.WindowManager;

public class AttrNameSettingActivity extends Activity implements OnClickListener {
    private static final String TAG = "AttrNameSettingActivity";

    CheckBox mCheckBoxOne;
    CheckBox mCheckBoxTwo;
    Button mExportToExcelBtn;
    Button mCloseAppBtn;
    ListView mList;
    ListAdapter mAdapter;
    LayoutInflater mInflater;

    AttrNameManager attrNameManager = null;
    private boolean isWritingExcel = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /* if user click from notification, enter setting screen directly */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attrname_setting);

        attrNameManager = (AttrNameManager) getSystemService(Context.JRD_ATTR_NAME_SERVICE);

        if (attrNameManager == null) {
            Slog.i(TAG, "<<<<<<<<< attrNameManager = " + attrNameManager);
            Toast.makeText(this, "getSystemService JRD_ATTR_NAME_SERVICE is null",
                    Toast.LENGTH_LONG).show();
            return;
        }

        mList = (ListView) findViewById(R.id.list);
        ArrayList<String> data = new ArrayList<String>();
        data.add(getString(R.string.show_string_id));
        data.add(getString(R.string.show_string_attribute));
        mAdapter = new ListAdapter(data);
        mList.setAdapter(mAdapter);
        mList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
            }
        });

        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mExportToExcelBtn = (Button) findViewById(R.id.btn_export_to_excel);
        mExportToExcelBtn.setOnClickListener(this);
        mCloseAppBtn = (Button) findViewById(R.id.btn_close_app);
        mCloseAppBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_export_to_excel:
                if (isWritingExcel) {
                    Toast.makeText(AttrNameSettingActivity.this,
                            "It is Writing Excel now, please wait...", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(AttrNameSettingActivity.this, "Write to Excel",
                            Toast.LENGTH_LONG).show();
                    writeToExcel(attrNameManager.getStingMap());
                }
                break;

            case R.id.btn_close_app:
                attrNameManager.setAttrNameServiceStart(false);
                attrNameManager.setShowId(false);
                attrNameManager.setShowStringAttribute(false);
                attrNameManager.clearAllCache();

                // restart to close the tool
                Intent activityIntent = new Intent();
                activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activityIntent.putExtra("ToStartTool", false);
                activityIntent.setClassName("com.android.systemui",
                        "com.android.systemui.jrdRefBuilder.AttrNamePromptActivity");
                startActivity(activityIntent);

                /*
                 * Intent service = new Intent();
                 * service.setClassName("com.android.systemui",
                 * "com.android.systemui.jrdRefBuilder.LoadAttrNameService");
                 * stopService(service); finish();
                 */

                break;
            default:
                break;
        }
    }

    class ListAdapter extends BaseAdapter {
        ArrayList<String> mDataAll;

        public ListAdapter(ArrayList<String> data) {
            mDataAll = data;
        }

        @Override
        public int getCount() {
            return mDataAll.size();
        }

        @Override
        public Object getItem(int index) {
            return mDataAll.get(index);
        }

        @Override
        public long getItemId(int index) {
            return mDataAll.get(index).hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View rootView = mInflater.inflate(R.layout.attrname_list_item, parent, false);
            final TextView text = (TextView) rootView.findViewById(R.id.textView);
            text.setText(mDataAll.get(position));
            CheckBox checkBox = (CheckBox) rootView.findViewById(R.id.checkBox);

            boolean checked = false;
            if ((position == 0 && attrNameManager.isShowId())
                    || (position == 1 && attrNameManager.isShowStringAttribute())) {
                checked = true;
            }
            checkBox.setChecked(checked);

            final int pos = position;
            checkBox.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (pos) {
                        case 0:// attrDisplay
                            if (attrNameManager.isShowId()) {
                                attrNameManager.setShowId(false);
                            } else {
                                attrNameManager.setShowId(true);
                            }
                            break;
                        case 1:// show attr(width/height/fontsize)
                            if (attrNameManager.isShowStringAttribute()) {
                                attrNameManager.setShowStringAttribute(false);
                            } else {
                                attrNameManager.setShowStringAttribute(true);
                            }
                            break;
                        default:
                            break;
                    }
                }
            });
            return rootView;

        }

    }

    public void writeToExcel(StringMapParcelable strMap) {

        isWritingExcel = true;
        Slog.i(TAG, "<<<<<<<<<WriteToExcel BEGIN");
        if (strMap == null) {
            return;
        }

        HashMap<String, ValueItem> stringInfoMap = strMap.getStringMap();

        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String date = sDateFormat.format(new java.util.Date()).replaceAll(" ", "_")
                .replaceAll(":", "-");
        String xlsPath = Environment.getExternalStorageDirectory().getPath();
        String outputFile = xlsPath + "/" + "outRefString-" + date + ".xls";

        Slog.i(TAG, "<<<<<<<<<output File:" + outputFile);

        WritableWorkbook wwb = null;
        try {
            wwb = Workbook.createWorkbook(new File(outputFile));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (wwb == null) {
            Slog.i(TAG, "create excel fial");
            Toast.makeText(this, "create excel fial", Toast.LENGTH_LONG).show();
            return;
        }

        WritableSheet ws = wwb.createSheet("MESSAGE", 0);

        // Write the topic to excel
        String[] topic = { /* "PackageName", */
                "Language", "Translation", "RefName", "zoneWidth", "zoneHeight", "fontSize",
                "Italic", "Bold", "Display Incomplete?"
        };
        for (int i = 0; i < topic.length; i++) {
            Label labelC = new Label(i, 0, topic[i]);
            try {
                ws.addCell(labelC);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // write the string information
        ArrayList<String> li;
        int rowN = 1;
        for (ValueItem v : stringInfoMap.values()) { // Get each stringItem
            // convert the map to Array list
            li = new ArrayList<String>();

            // li.add(v.getPackageName());
            li.add(v.getCurrentLanguage());
            li.add(v.getAttrValue());
            li.add(v.getAttrName());
            li.add(Integer.toString(v.getZoneWidth()));
            li.add(Integer.toString(v.getZoneHeight()));
            li.add(Integer.toString(v.getTextSize()));
            li.add(v.getTextItalic() == 1 ? "Yes" : "No");
            li.add(v.getTextBold() == 1 ? "Yes" : "No");
            li.add(v.getEllipse() == 1 ? "Yes" : "No");

            int columnN = 0;
            for (String l : li) {
                Label labelC = new Label(columnN, rowN, l);
                columnN++;
                try {
                    ws.addCell(labelC);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            li = null;
            rowN++;
        }

        try {
            wwb.write(); // write the excel buffer data to FIle SYstem
            wwb.close(); // clear the buffer
        } catch (Exception e) {
            e.printStackTrace();
        }
        wwb = null;

        /*-begin-20150914-zubin.chen-add-for-strApk-*/
        // refresh the file system
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri uri = Uri.fromFile(new File(xlsPath));
        intent.setData(uri);
        this.sendBroadcast(intent);
        /*-end-20150914-zubin.chen-add-for-strApk-*/

        Toast.makeText(this, "Export Excel to File System root directory Done!", Toast.LENGTH_LONG)
                .show();
        isWritingExcel = false;

        Slog.i(TAG, "<<<<<<<<< WriteToExcel END");

    }

}
