/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* ==================================================================================*/
/*     Modifications on Features list / Changes Request / Problems Report            */
/* ----------|----------------------|----------------------|-------------------------*/
/* 11/04/2014|nie.lei               |FR-821720             |[Ergo][TCT]Quick Settings*/
/* ----------|----------------------|----------------------|-------------------------*/
/* ================================================================================= */

package com.android.systemui.qs.tiles;

import android.content.Intent;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.systemui.R;
import com.android.systemui.qs.QSTile;
import android.provider.Settings;
import android.provider.Settings.Global;
import com.android.systemui.statusbar.policy.AutoBrightnessController;

/** Quick settings tile: AutoBrightness **/
public class AutoBrightnessTile extends QSTile<QSTile.BooleanState> {

    private final AutoBrightnessController mController;
    private final Callback mCallback = new Callback();

    public AutoBrightnessTile(Host host) {
        super(host);
        mController = host.getAutoBrightnessController();
    }

    @Override
    public BooleanState newTileState() {
        return new BooleanState();
    }

    @Override
    public void setListening(boolean listening) {
        if (listening) {
            mController.addSettingsChangedCallback(mCallback);
        } else {
            mController.removeSettingsChangedCallback(mCallback);
        }
    }

    @Override
    protected void handleClick() {
        final boolean wasEnabled = (Boolean) mState.value;
        mController.setAutoBrightnessEnabled(!wasEnabled);
    }


    @Override
    public CharSequence getTileLabel() {
        return mContext.getString(R.string.quick_settings_autobrightness_mode_label);
    }


    @Override
    public Intent getLongClickIntent() {
        return new Intent(Settings.ACTION_DISPLAY_SETTINGS);
    }

    @Override
    protected void handleUpdateState(BooleanState state, Object arg) {
        final boolean locationEnabled =  mController.isAutoBrightnessEnabled();
        //state.visible = true;
        state.value = locationEnabled;
        state.label = mContext.getString(R.string.quick_settings_autobrightness_mode_label);
        if (locationEnabled) {
            state.icon = ResourceIcon.get(R.drawable.ic_qs_brightness_on);
            state.contentDescription = mContext.getString(
                    R.string.accessibility_quick_settings_autobrightness_on);
        } else {
            state.icon = ResourceIcon.get(R.drawable.ic_qs_brightness_off);
            state.contentDescription = mContext.getString(
                    R.string.accessibility_quick_settings_autobrightness_off);
        }
    }

    private final class Callback implements AutoBrightnessController.AutoBrightnessChangeCallback {
        @Override
        public void onAutoBrightnessChanged(boolean AutoBrightnessEnabled) {
            refreshState();
        }
    }

    @Override
    public int getMetricsCategory() {
        // TODO Auto-generated method stub
        return MetricsEvent.ACTION_BRIGHTNESS_AUTO;
    };
}
