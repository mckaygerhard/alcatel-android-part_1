/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 12/25/2014|       xiao.jian      |       FR-862263      |USB OTG support - */
/*           |                      |                      |application part  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.systemui.usb;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.android.internal.app.AlertActivity;
import com.android.internal.app.AlertController;

public class UsbChargingActivity extends AlertActivity
        implements DialogInterface.OnClickListener {

    private static final String TAG = "UsbChargingActivity";

    private Context mContext;
    private CheckBox mAlwaysCheckBox;
    private String mDeviceName;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        mContext = this;
        Intent intent = getIntent();
        mDeviceName = (String)intent.getExtra("dName");

        final AlertController.AlertParams ap = mAlertParams;
        ap.mTitle = getString(com.android.systemui.R.string.otg_charging_dialog_title);
        ap.mMessage = getString(com.android.systemui.R.string.otg_charging_dialog_msg);
        ap.mPositiveButtonText = getString(com.android.internal.R.string.ok);
        ap.mPositiveButtonListener = this;

        // add "always" checkbox
        LayoutInflater inflater = (LayoutInflater)getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        ap.mView = inflater.inflate(com.android.internal.R.layout.always_use_checkbox, null);
        mAlwaysCheckBox = (CheckBox)ap.mView.findViewById(com.android.internal.R.id.alwaysUse);
        mAlwaysCheckBox.setText(com.android.systemui.R.string.otg_charging_dialog_show);
        TextView clearDefaultHint = (TextView)ap.mView.findViewById(com.android.internal.R.id.clearDefaultHint);
        clearDefaultHint.setVisibility(View.GONE);

        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(mUsbReceiver, filter);

        setupAlert();
    }

    @Override
    protected void onDestroy() {
        if (mUsbReceiver != null) {
            unregisterReceiver(mUsbReceiver);
        }
        super.onDestroy();
    }

    private void setDialogPreference() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (mAlwaysCheckBox.isChecked()) {
            editor.putBoolean("charging_dialog", true);
        } else {
            editor.putBoolean("charging_dialog", false);
        }
        editor.apply();
        finish();
    }

    public void onClick(DialogInterface dialog, int which) {
        if (which == AlertDialog.BUTTON_POSITIVE) {
            setDialogPreference();
        }
    }

    BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
            if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)
                && device != null && device.getDeviceName().equals(mDeviceName)) {
                setDialogPreference();
            }
        }
    };
}
