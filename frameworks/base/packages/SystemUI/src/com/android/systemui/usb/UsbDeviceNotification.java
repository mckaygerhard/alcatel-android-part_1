/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 12/25/2014|       xiao.jian      |       FR-862263      |USB OTG support - */
/*           |                      |                      |application part  */
/* ----------|----------------------|----------------------|----------------- */
/*           |                      |                      |                  */
/* 09/23/2015|      bingbing.xu     |      TASK-499637     |Modify mTick-     */
/*           |                      |                      |Receiver register */
/*           |                      |                      |location          */
/******************************************************************************/
package com.android.systemui.usb;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.SharedPreferences;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.storage.IMountService;
import android.os.UserHandle;
import android.preference.PreferenceManager;
import android.util.TctLog;

import com.android.systemui.SystemUI;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.ActivityNotFoundException;

public class UsbDeviceNotification extends SystemUI {
    private static final String TAG = "UsbDeviceNotification";

    private Notification mUsbDeviceNotification;
    private Handler mAsyncEventHandler;
    private List<String> mLauncherNames;
    private String mDeviceName;
    private int mDeviceClass;
    private int mBatteryLevel;
    private boolean mCharging;
    private long mChargingStartTime;
    private IntentFilter filter;

    private static final String ACTION_UNMOUNT_UMS = "action.unmount.ums";

    @Override
    public void start() {
        HandlerThread thr = new HandlerThread("SystemUI UsbDeviceNotification");
        thr.start();
        mAsyncEventHandler = new Handler(thr.getLooper());

        // listen for new devices
        filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        mContext.registerReceiver(mUsbReceiver, filter);

        // listen for battery status
        filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        mContext.registerReceiver(mBatteryReceiver, filter);

        // listen for unmount ums
        filter = new IntentFilter(ACTION_UNMOUNT_UMS);
        mContext.registerReceiver(mUnmountReceiver, filter);

    }

    /**
     * Sets the USB device notification.
     */
    private synchronized void setUsbDeviceNotification(String title, String message,
            int icon, boolean sound, PendingIntent pi) {
        NotificationManager notificationManager = (NotificationManager) mContext
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null) {
            return;
        }

        if (mUsbDeviceNotification == null) {
            mUsbDeviceNotification = new Notification();
            mUsbDeviceNotification.icon = icon;
            mUsbDeviceNotification.when = 0;
        }

        if (sound) {
            mUsbDeviceNotification.defaults |= Notification.DEFAULT_SOUND;
        } else {
            mUsbDeviceNotification.defaults &= ~Notification.DEFAULT_SOUND;
        }

        mUsbDeviceNotification.flags = Notification.FLAG_NO_CLEAR;
        mUsbDeviceNotification.tickerText = title;
        if (pi == null) {
            Intent intent = new Intent();
            pi = PendingIntent.getBroadcastAsUser(mContext, 0, intent, 0,
                    UserHandle.CURRENT);
        }
        mUsbDeviceNotification.color = mContext.getResources().getColor(
                com.android.internal.R.color.system_notification_accent_color);
        mUsbDeviceNotification.setLatestEventInfo(mContext, title, message, pi);
        mUsbDeviceNotification.visibility = Notification.VISIBILITY_PUBLIC;
        mUsbDeviceNotification.category = Notification.CATEGORY_SYSTEM;

        final int notificationId = mUsbDeviceNotification.icon;
        notificationManager.notifyAsUser(null, notificationId, mUsbDeviceNotification,
                UserHandle.ALL);
    }

   private void cancelUsbDeviceNotification(int notificationId) {
        NotificationManager notificationManager = (NotificationManager) mContext
                    .getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null || mUsbDeviceNotification == null) {
            return;
        }
        notificationManager.cancelAsUser(null, notificationId, UserHandle.ALL);
    }

    private int getLauncherNames() {
        mLauncherNames = new ArrayList();
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        List<ResolveInfo> resolveInfo = mContext.getPackageManager().queryIntentActivities(intent,
            PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo ri : resolveInfo) {
            mLauncherNames.add(ri.activityInfo.packageName);
        }
        return mLauncherNames.size();
    }

    private boolean isLauncher() {
        if (mLauncherNames == null) {
            if (getLauncherNames() < 1) {
                mLauncherNames = null;
                return false;
            }
        }
        ActivityManager am = (ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> rti = am.getRunningTasks(1);
        String topPackageName = rti.get(0).topActivity.getPackageName();
        for (int i = 0; i < mLauncherNames.size(); i++) {
            if (mLauncherNames.get(i) != null && mLauncherNames.get(i).equals(topPackageName)) {
                return true;
            }
        }
        return false;
    }

    private void startFileManager() {
        try {
             Intent intent = new Intent();
             intent.setComponent(new ComponentName("com.jrdcom.filemanager","com.jrdcom.filemanager.activity.FileBrowserActivity"));
             intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
             mContext.startActivity(intent);
        } catch (ActivityNotFoundException anfe) {
            TctLog.e(TAG, "unable to find explicit activity class: " + anfe);
        }
    }

    private void unmountUsbMassStorage() {
        IBinder service = ServiceManager.getService("mount");
        if (service != null) {
           IMountService mountService = IMountService.Stub.asInterface(service);
           try {
                mountService.unmountVolume("/storage/usbotg", true, false);
                TctLog.i(TAG, "unmount usb device: " + mDeviceName );
                cancelUsbDeviceNotification(com.android.systemui.R.drawable.ic_otg_storage);
            } catch (RemoteException re) {
                TctLog.e(TAG, "unmount usb mass storage failed: " + re);
            }
              catch (Exception ex) {
                TctLog.e(TAG, "unmount usb mass storage failed: " + ex);
            }
        }
        /*try {
            String[] commands = {"unmount " + mDeviceName};
            Runtime.getRuntime().exec(commands, null, null);
        } catch (IOException ie) {
            Log.e(TAG, "unmount usb mass storage failed: " + ie);
        }*/
    }

    private void startCharging() {
        // listen for time tick
        filter = new IntentFilter(Intent.ACTION_TIME_TICK);
        mContext.registerReceiver(mTickReceiver, filter);

        mChargingStartTime = SystemClock.elapsedRealtime();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        updateChargingState();
        if (!sharedPreferences.getBoolean("charging_dialog", false)) {
            Intent intent = new Intent(mContext, UsbChargingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("dName", mDeviceName);
            mContext.startActivity(intent);
        }
    }

    private void updateChargingState() {
        mAsyncEventHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mCharging) {
                    setUsbDeviceNotification(
                        formatElapsedTime(),
                        formatBatteryLevel(),
                        com.android.systemui.R.drawable.ic_otg_charging,
                        false, null);
                }
            }
        });
    }

    private String formatElapsedTime() {
        long hours = 0;
        long minutes = 0;
        long seconds = 0;
        long chargingSeconds = (SystemClock.elapsedRealtime() - mChargingStartTime) / 1000;
        if (chargingSeconds >= 3600) {
            hours = chargingSeconds / 3600;
            chargingSeconds -= hours * 3600;
        }
        if (chargingSeconds >= 60) {
            minutes = chargingSeconds / 60;
            chargingSeconds = minutes * 60;
        }
        seconds = chargingSeconds;
        return mContext.getString(com.android.systemui.R.string.otg_charging_detail,
            (hours < 10) ? String.format("%02d", hours) : String.valueOf(hours),
            (minutes < 10) ? String.format("%02d", minutes) : String.valueOf(minutes));
    }

    private String formatBatteryLevel() {
        return mContext.getString(com.android.systemui.R.string.otg_charging_battery, String.valueOf(mBatteryLevel)) + "%";
    }

    BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                final UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null) {
                    mDeviceName = device.getDeviceName();
                    mDeviceClass = device.getDeviceClass();
                    TctLog.i(TAG, "dName: " + mDeviceName + " dClass: " + mDeviceClass);
                    mAsyncEventHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mDeviceClass == UsbConstants.USB_CLASS_PER_INTERFACE) {
                                //use class information in the interface description
                                TctLog.i(TAG, "usb class per interface");
                                //PROC 1
                                /*UsbManager usbManager = (UsbManager)mContext.getSystemService(Context.USB_SERVICE);
                                HashMap<String, UsbDevice> map = usbManager.getDeviceList();
                                for (UsbDevice uDevice : map.values()) {
                                    String dName = uDevice.getDeviceName();
                                    if (dName != null && dName.equals(mDeviceName)) {
                                        mDeviceClass = uDevice.getDeviceClass();
                                        if (mDeviceClass == UsbConstants.USB_CLASS_PER_INTERFACE) {
                                            int count = uDevice.getInterfaceCount();
                                            for (int i = 0; i < count; i++) {
                                                UsbInterface ui = device.getInterface(i);
                                                if (ui != null) {
                                                    mDeviceClass = ui.getInterfaceClass();
                                                    if (mDeviceClass != UsbConstants.USB_CLASS_PER_INTERFACE) {
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }*/
                                //PROC 2
                                int count = device.getInterfaceCount();
                                for (int i = 0; i < count; i++) {
                                    UsbInterface ui = device.getInterface(i);
                                    if (ui != null) {
                                        mDeviceClass = ui.getInterfaceClass();
                                        if (mDeviceClass != UsbConstants.USB_CLASS_PER_INTERFACE) {
                                            break;
                                        }
                                    }
                                }
                            }
                            if (mDeviceClass == UsbConstants.USB_CLASS_COMM || mDeviceClass == UsbConstants.USB_CLASS_CDC_DATA) {
                                //communication devices
                                TctLog.i(TAG, "usb communication devices");
                                mCharging = true;
                                startCharging();
                                //showChargingDialog();
                            } else if (mDeviceClass == UsbConstants.USB_CLASS_HID) {
                                //keyboard & mouse
                                TctLog.i(TAG, "usb keyboard & mouse");
                            } else if (mDeviceClass == UsbConstants.USB_CLASS_STILL_IMAGE) {
                                //digital cameras
                                TctLog.i(TAG, "usb digital cameras");
                                setUsbDeviceNotification(
                                    mContext.getString(com.android.systemui.R.string.otg_camera_title),
                                    null,
                                    com.android.systemui.R.drawable.ic_otg_camera,
                                    false, null);
                            } else if (mDeviceClass == UsbConstants.USB_CLASS_MASS_STORAGE) {
                                //mass storage devices
                                TctLog.i(TAG, "usb mass storage devices");
                                Intent unmountIntent = new Intent(ACTION_UNMOUNT_UMS);
                                unmountIntent.setFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY);
                                PendingIntent pi = PendingIntent.getBroadcastAsUser(mContext, 0, unmountIntent, 0,
                                    UserHandle.CURRENT);
                                setUsbDeviceNotification(
                                    mContext.getString(com.android.systemui.R.string.otg_storage_title),
                                    mContext.getString(com.android.systemui.R.string.otg_storage_detail),
                                    com.android.systemui.R.drawable.ic_otg_storage,
                                    false, pi);
                                if (isLauncher()) {
                                    startFileManager();
                                }
                            } else {
                                TctLog.i(TAG, "Do not care of usb device which dClass is " + mDeviceClass);
                            }
                        }
                    });
                }
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                final UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null && device.getDeviceName().equals(mDeviceName)) {
                    mAsyncEventHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mDeviceClass == UsbConstants.USB_CLASS_COMM || mDeviceClass == UsbConstants.USB_CLASS_CDC_DATA) {
                                //communication devices
                                mCharging = false;
                                mContext.unregisterReceiver(mTickReceiver);
                                cancelUsbDeviceNotification(com.android.systemui.R.drawable.ic_otg_charging);
                            } else if (mDeviceClass == UsbConstants.USB_CLASS_STILL_IMAGE) {
                                cancelUsbDeviceNotification(com.android.systemui.R.drawable.ic_otg_camera);
                            } else if (mDeviceClass == UsbConstants.USB_CLASS_MASS_STORAGE) {
                                cancelUsbDeviceNotification(com.android.systemui.R.drawable.ic_otg_storage);
                            }
                        }
                    });
                }
            }
        }
    };

    BroadcastReceiver mBatteryReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                //int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
                //int voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0);
                if (mBatteryLevel != level) {
                    mBatteryLevel = level;
                    if (mCharging) {
                        updateChargingState();
                    }
                }
            }
        }
    };

    BroadcastReceiver mUnmountReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_UNMOUNT_UMS)) {
                mAsyncEventHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        unmountUsbMassStorage();
                    }
                });
            }
        }
    };

    BroadcastReceiver mTickReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
                if (mCharging) {
                    updateChargingState();
                }
            }
        }
    };
}
