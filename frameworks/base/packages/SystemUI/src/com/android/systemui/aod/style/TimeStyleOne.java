/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.systemui.aod.style;

import java.util.Calendar;
import java.util.TimeZone;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.systemui.R;

public class TimeStyleOne extends LinearLayout {

    Context mContext;

    private String mDay;
    private String mWeek;

    private TextView mWeekView;
    private TextView mDayView;

    private final int WEEK = 1;
    private final int DAY = 2;

    public TimeStyleOne(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
    }

    @Override
    protected void onFinishInflate() {
        // TODO Auto-generated method stub
        super.onFinishInflate();
        prepare();
    }

    private void prepare() {
        // TODO Auto-generated method stub
        mWeekView = (TextView) this.findViewById(R.id.time_week);
        mWeekView.setText(getData(WEEK));
        mDayView = (TextView) this.findViewById(R.id.time_day);
        mDayView.setText(getData(DAY));
    }

    private String getData(int data) {
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
        switch (data) {
        case (WEEK):
            mWeek = String.valueOf(c.get(Calendar.DAY_OF_WEEK));
            if ("1".equals(mWeek)) {
                mWeek = "SUN";
            } else if ("2".equals(mWeek)) {
                mWeek = "MON";
            } else if ("3".equals(mWeek)) {
                mWeek = "TUS";
            } else if ("4".equals(mWeek)) {
                mWeek = "WED";
            } else if ("5".equals(mWeek)) {
                mWeek = "THU";
            } else if ("6".equals(mWeek)) {
                mWeek = "FRI";
            } else if ("7".equals(mWeek)) {
                mWeek = "SAT";
            }
            return mWeek;
        case (DAY):
            mDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
            return mDay;
        default:
            return "";
        }
    }
}
