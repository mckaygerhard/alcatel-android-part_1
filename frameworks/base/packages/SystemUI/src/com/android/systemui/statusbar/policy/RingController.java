package com.android.systemui.statusbar.policy;

import java.util.ArrayList;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.util.Log;
public class RingController extends BroadcastReceiver{

	private static final String TAG = "RingController";
	private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG);
	private int mRingmode;
	private final AudioManager mAudio;
	private final ArrayList<RingStateChangeCallback> mChangeCallbacks = new ArrayList<>();

	@Override
	public void onReceive(Context context, Intent intent) {
	   final String action = intent.getAction();
	   if (action.equals(AudioManager.RINGER_MODE_CHANGED_ACTION)) {
		   //RingStateChanged();//remove by jiajunwu for Defect1825099 2016-03-24
		   updateRingmode();
	   }
	}

    public void addStateChangedCallback(RingStateChangeCallback cb) {
        mChangeCallbacks.add(cb);
        cb.onRingStateChanged();
    }

     public void removeStateChangedCallback(RingStateChangeCallback cb) {
        mChangeCallbacks.remove(cb);
     }


   private void RingStateChanged() {
        final int N = mChangeCallbacks.size();
        for (int i = 0; i < N; i++) {
            mChangeCallbacks.get(i).onRingStateChanged();
        }
    }
	public RingController(Context context) {
	     mAudio = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);   
	     IntentFilter filter = new IntentFilter();
	     filter.addAction(AudioManager.RINGER_MODE_CHANGED_ACTION);
	     context.registerReceiver(this, filter);
	     updateRingmode();
	 }

	 public int whichRingmode() {
	      return mRingmode;
	 }

	 public void updateRingmode() {
	     setRingMode(mAudio.getRingerMode());
	 }

	 public void setRingMode(int Ringmode) {
	     if (Ringmode == mRingmode) return;
	     mRingmode = Ringmode;
	     if (DEBUG) Log.d(TAG, "Ringmode is " +  Ringmode);
	     mAudio.setRingerMode(Ringmode);
	     RingStateChanged();
	  }

	 public boolean isValidRingMode() {
		return  AudioManager.isValidRingerMode(mAudio.getRingerMode());
     }

	 public boolean isSilentMode() {
	        return mAudio.isSilentMode();
	 }

	 public interface RingStateChangeCallback {
        void onRingStateChanged();
     }

}