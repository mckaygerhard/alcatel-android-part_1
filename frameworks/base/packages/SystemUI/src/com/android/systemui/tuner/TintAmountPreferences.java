
package com.android.systemui.tuner;

import com.android.systemui.statusbar.policy.NightModeController;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.provider.Settings.Secure;
import android.util.AttributeSet;
import android.util.Log;
import android.util.MathUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class TintAmountPreferences extends Preference{

    private int mProgress;

    private final int mMax = 50;

    private SeekBar mSeekBar;

    private OnSeekBarChangeListener mOnSeekBarChangeListener;

    public TintAmountPreferences(Context context, AttributeSet attrs, int defStyleAttr,
            int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        TypedArray a = context.obtainStyledAttributes(attrs,
                com.android.internal.R.styleable.SeekBarPreference, defStyleAttr, defStyleRes);
        final int layoutResId = a.getResourceId(
                com.android.internal.R.styleable.SeekBarPreference_layout,
                com.android.internal.R.layout.preference_widget_seekbar);
        a.recycle();

        setLayoutResource(layoutResId);
    }

    public TintAmountPreferences(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public TintAmountPreferences(Context context, AttributeSet attrs) {
        this(context, attrs, com.android.internal.R.attr.seekBarPreferenceStyle);
    }

    public TintAmountPreferences(Context context) {
        this(context, null);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder view)
    {
        super.onBindViewHolder(view);
        tintAmount2Progress();
        mSeekBar = (SeekBar) view.findViewById(com.android.internal.R.id.seekbar);
        mSeekBar.setOnSeekBarChangeListener(mOnSeekBarChangeListener);
        mSeekBar.setMax(mMax);
        mSeekBar.setProgress(mProgress);
    }

    public void setOnSeekBarChangeListener(OnSeekBarChangeListener listener)
    {
        mOnSeekBarChangeListener = listener;
        if(mSeekBar != null)
        {
            mSeekBar.setOnSeekBarChangeListener(mOnSeekBarChangeListener);
        }
    }

    private void tintAmount2Progress()
    {
        String amount = TunerService.get(getContext())
                .getValue(Secure.NIGHT_MODE_TINT_COLOR_AMOUNT);
        if (amount != null)
        {
            try
            {
                float mAmount = Float.parseFloat(amount);
                mProgress = (int) ((mAmount - 0.5) * 100);
            } catch (NumberFormatException e)
            {
                mProgress = mMax / 2;
            }
        } else
        {
            mProgress = mMax / 2;
        }
        if (mProgress > mMax)
        {
            mProgress = mMax;
        }
        if (mProgress < 0)
        {
            mProgress = 0;
        }
    }

    public void setProgress(int progress)
    {
        mSeekBar.setProgress(progress);
    }

    public int getProgress()
    {
        return mProgress;
    }

    @Override
    public CharSequence getSummary()
    {
        return null;
    }
}
