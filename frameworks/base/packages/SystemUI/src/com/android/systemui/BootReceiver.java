/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;
/* MODIFIED-BEGIN by jianguang.sun, 2016-09-08,BUG-2856089*/
//BEGIN-XHANWU-20150119-ADD-FOR-GET-INCOMPLETE-STRING-FEATURE
import android.os.SystemProperties;
//END-XHANWU-20150119-ADD-FOR-GET-INCOMPLETE-STRING-FEATURE
/* MODIFIED-END by jianguang.sun,BUG-2856089*/

/**
 * Performs a number of miscellaneous, non-system-critical actions
 * after the system has finished booting.
 */
public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = "SystemUIBootReceiver";

    @Override
    public void onReceive(final Context context, Intent intent) {
        try {
            // Start the load average overlay, if activated
            ContentResolver res = context.getContentResolver();
            if (Settings.Global.getInt(res, Settings.Global.SHOW_PROCESSES, 0) != 0) {
                Intent loadavg = new Intent(context, com.android.systemui.LoadAverageService.class);
                context.startService(loadavg);
            }

          /* MODIFIED-BEGIN by jianguang.sun, 2016-09-08,BUG-2856089*/
          //BEGIN-XHANWU-20150119-ADD-FOR-GET-INCOMPLETE-STRING-FEATURE
            /*
             * If the Detect tool already start, we need to load the attrName
             * service directly
             */
            boolean mIsDetectToolStart = SystemProperties.getBoolean("persist.sys.isToolStart",
                    false);
            if (mIsDetectToolStart) {
                Intent service = new Intent();
                service.setClassName("com.android.systemui",
                        "com.android.systemui.jrdRefBuilder.LoadAttrNameService");
                context.startService(service);
            }
            // END-XHANWU-20150119-ADD-FOR-GET-INCOMPLETE-STRING-FEATURE
            /* MODIFIED-END by jianguang.sun,BUG-2856089 */

        } catch (Exception e) {
            Log.e(TAG, "Can't start load average service", e);
        }
    }
}
