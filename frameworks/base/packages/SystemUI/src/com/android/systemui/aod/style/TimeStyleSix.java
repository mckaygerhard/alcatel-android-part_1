/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.systemui.aod.style;

import java.util.Calendar;
import java.util.TimeZone;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.systemui.R;

public class TimeStyleSix extends LinearLayout {

    Context mContext;

    private String mWeek;
    private String mHour;
    private String mMin;

    private TextView mWeekView;
    private TextView mHourView;
    private TextView mMinkView;

    private final int WEEK = 1;
    private final int HOUR = 2;
    private final int MIN = 3;

    public TimeStyleSix(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
    }

    @Override
    protected void onFinishInflate() {
        // TODO Auto-generated method stub
        super.onFinishInflate();
        prepare();
    }

    private void prepare() {
        // TODO Auto-generated method stub
        mWeekView = (TextView) this.findViewById(R.id.time_week);
        mWeekView.setText(getData(WEEK));
        mHourView = (TextView) this.findViewById(R.id.time_hour);
        mHourView.setText(getData(HOUR));
        mMinkView = (TextView) this.findViewById(R.id.time_min);
        mMinkView.setText(getData(MIN));
    }

    private String getData(int data) {
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
        switch (data) {
        case (WEEK):
            mWeek = String.valueOf(c.get(Calendar.DAY_OF_WEEK));
            if ("1".equals(mWeek)) {
                mWeek = "SUN";
            } else if ("2".equals(mWeek)) {
                mWeek = "MON";
            } else if ("3".equals(mWeek)) {
                mWeek = "TUS";
            } else if ("4".equals(mWeek)) {
                mWeek = "WED";
            } else if ("5".equals(mWeek)) {
                mWeek = "THU";
            } else if ("6".equals(mWeek)) {
                mWeek = "FRI";
            } else if ("7".equals(mWeek)) {
                mWeek = "SAT";
            }
            return mWeek;
        case (HOUR):
            /* MODIFIED-BEGIN by song.huan, 2016-10-28,BUG-3269387*/
            mHour = transferTimeFormat(String.valueOf(c.get(Calendar.HOUR_OF_DAY)));
            return mHour;
        case (MIN):
            mMin = transferTimeFormat(String.valueOf(c.get(Calendar.MINUTE)));
            return ":"+mMin;
        default:
            return "";
        }
    }

    private String transferTimeFormat(String time) {
        if(!time.equals("")) {
            if(time.length() == 1) {
                return "0" + time;
            }
        }
        return time;
    }
    /* MODIFIED-END by song.huan,BUG-3269387*/
}
