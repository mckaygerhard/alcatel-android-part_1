package com.android.systemui.statusbar.phone;

import android.content.Context;
import android.os.PowerManager;
import android.os.UserHandle;
import android.provider.Settings;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.android.systemui.R;

/**
 * Create by TCTNB(Guoqiang.Qiu), 2016-7-27, Solution-2519873
 */

public class DoubleTapHelper {
    private GestureDetector mDoubleTapGesture;
    private Context mContext;
    private final int STATUS_HEIGHT;
    private final boolean ENABLE_ON_KEYGUARD;
    private final boolean ENABLE_ON_STATUSBAR;
    private static DoubleTapHelper instance;

    private DoubleTapHelper(Context context) {
        mContext = context;
        STATUS_HEIGHT = context.getResources().getDimensionPixelSize(R.dimen.status_bar_height);
        ENABLE_ON_KEYGUARD = context.getResources().getBoolean(com.android.internal.R.bool.feature_keyguard_double_click_sleep_on);
        ENABLE_ON_STATUSBAR = context.getResources().getBoolean(R.bool.feature_statusbar_double_click_sleep_on);
        if (ENABLE_ON_KEYGUARD || ENABLE_ON_STATUSBAR) {
            mDoubleTapGesture = new GestureDetector(context, new DoubleTapGestureListener());
        }
    }

    public static DoubleTapHelper from(Context context) {
        if (instance == null) {
            instance = new DoubleTapHelper(context);
        }
        return instance;
    }

    private void handleDoubleTap(MotionEvent event) {
        boolean isDoubleClickEnable = Settings.System.getIntForUser(mContext.getContentResolver(),
                        Settings.System.TCT_DOUBLE_CLICK, 1, UserHandle.USER_CURRENT) == 1;
        if (isDoubleClickEnable && mDoubleTapGesture != null) {
            mDoubleTapGesture.onTouchEvent(event);
        }
    }

    public void handleDoubleTapOnStatusBar(MotionEvent event) {
        if (ENABLE_ON_STATUSBAR && event.getY() < STATUS_HEIGHT) {
            handleDoubleTap(event);
        }
    }

    public void handleDoubleTapOnKeyguard(MotionEvent event) {
        if (ENABLE_ON_KEYGUARD) {
            handleDoubleTap(event);
        }
    }

    private final class DoubleTapGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            ((PowerManager) mContext
                    .getSystemService(Context.POWER_SERVICE))
                    .goToSleep(e.getEventTime());
            return true;
        }
    }
}
