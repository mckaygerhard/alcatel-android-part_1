package com.vlife;

import android.content.Context;
import android.view.View;
import android.view.MotionEvent;

/**
 * @author Yanlei
 * create at 2016/8/10 14:47
 */
public interface ILockScreen {
	String packageName = "com.vlife.tcl.wallpaper";
	String className = "com.vlife.VlifeLockScreenForTcl";

	/**
	 * 初始化
	 * @param context
	 */
	void init(Context context);

	/**
	 * 获取锁屏view
	 * @param path 图片路径
	 * @param type 动效类型 water,particle,ball,sunshine,wipe,lightup
	 * @return
	 */
	View loadLockView(String path,String type);

	/**
	 * 显示锁屏界面时调用
	 * @return
	 */
	void onShow();
	
	/**
	 * 隐藏锁屏界面时调用，与onShow相对应
	 * 
	 * @return
	 */
	void onHide();
	
	/**
	 * 销毁时调用，释放所有资源
	 * 
	 * @return
	 */
	void onDestroy();

	/**
	 * 配置资源路径 及 动效类型
	 * @param path 静态图路径
	 * @param type 动效类型
	 */
	void setResourceInfo(String path,String type);
}
