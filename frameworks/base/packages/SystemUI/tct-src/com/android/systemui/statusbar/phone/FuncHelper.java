package com.android.systemui.statusbar.phone;

/* MODIFIED-BEGIN by junyong.sun, 2016-11-09,BUG-3319328*/
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
/* MODIFIED-END by junyong.sun,BUG-3319328*/
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.UserHandle;
import android.provider.Contacts;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.MotionEvent;
import android.view.View;
//[FEATURE]-Add-BEGIN by TCTNB.(Chuanjun Chen), 10/11/2016, FEATURE-2792219 And TASk-2989788.
import android.content.ContentValues;
import android.content.ContentProviderClient;
import android.os.RemoteException;
import android.util.Log;
//[FEATURE]-Add-END by TCTNB.(Chuanjun Chen).
import android.content.ComponentName; // MODIFIED by song.huan, 2016-10-31,BUG-3081621

import com.android.internal.R;
import com.android.internal.widget.LockPatternUtils;
import com.android.keyguard.KeyguardUpdateMonitor;
import com.android.systemui.assist.AssistManager;
import com.android.systemui.statusbar.KeyguardIndicationController;
import com.android.systemui.statusbar.policy.FlashlightController;

import java.util.List; // MODIFIED by junyong.sun, 2016-11-09,BUG-3319328

/**
 * Helper of Func to handle case
 * [SOLUTION]-ADD by TCTNB(Guoqiang.Qiu), 2016-8-8, Solution-2520273
 */

public final class FuncHelper implements FlashlightController.FlashlightListener {
    private Context mContext;
    private FuncLayout hostView;
    private FuncItemView mTargetedView;
    private long mPreviousTime, mCurrentTime;
    private boolean mVisible = true;

    private ActivityStarter mActivityStarter;
    private AssistManager mAssistManager;
    private FlashlightController mFlashlightController;
    private LockPatternUtils mLockPatternUtils;
    private KeyguardIndicationController mIndicationController;
    private KeyguardUpdateMonitor mUpdateMonitor;

    private static final int DOUBLE_TAP_TIMEOUT = 1200;

    private static final int NAVIGATE_TYPE_CAR = 1;
    private static final int NAVIGATE_TYPE_TRANSIT = 2;
    private static final int NAVIGATE_TYPE_WALK = 3;
    private static final int NAVIGATE_TYPE_BIKING = 4;
    public static final int PICK_CONTACT_REQUEST = 4; // MODIFIED by song.huan, 2016-10-18,BUG-3088432
    //[FEATURE]-Add-BEGIN by TCTNB.(Chuanjun Chen), 10/11/2016, FEATURE-2792219 And TASk-2989788.
    private static final String TAG = "FuncHelper";
    private static final String SCHEME = "content";
    private static final String AUTHORITY = "com.tct.diagnostics.provider.diagnosticsinfo";
    private static final String TABLE_NAME = "diagnostics";
    private static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY).path(TABLE_NAME).build();
    //[FEATURE]-Add-END by TCTNB.(Chuanjun Chen.
    /* MODIFIED-BEGIN by junyong.sun, 2016-11-09,BUG-3319328*/
    private boolean isOpneYahooSearch = false;

    public FuncHelper(Context context, FuncLayout view) {
        mContext = context;
        hostView = view;
        mLockPatternUtils = new LockPatternUtils(mContext);
        mUpdateMonitor = KeyguardUpdateMonitor.getInstance(mContext);
        registerKeyGuardGoneListener(mContext);
    }

    public void registerKeyGuardGoneListener(Context mContext){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_USER_PRESENT);
        intentFilter.addAction("com.action.KEYGUARD_BOUNCER_BACK_PRESSED");
        intentFilter.addAction("com.action.EMERGENCY_BUTTON_CLICK"); // MODIFIED by junyong.sun, 2016-11-10,BUG-3319328
        mContext.registerReceiver(new KeyguardGoneListener(),intentFilter);
        /* MODIFIED-END by junyong.sun,BUG-3319328*/
    }

    public void setFlashlightController(FlashlightController flashlightController) {
        mFlashlightController = flashlightController;
    }

    public void setActivityStarter(ActivityStarter activityStarter) {
        mActivityStarter = activityStarter;
    }

    public void setAssistManager(AssistManager assistManager) {
        mAssistManager = assistManager;
    }

    public void setKeyguardIndicationController(
            KeyguardIndicationController keyguardIndicationController) {
        mIndicationController = keyguardIndicationController;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (hostView.getVisibility() != View.VISIBLE) return false;
        int action = event.getActionMasked();
        final float y = event.getY();
        final float x = event.getX();

        if (action == MotionEvent.ACTION_DOWN) {
            FuncItemView targetView = hostView.getIconAtPosition(x, y);
            if (targetView == null) {
                return false;
            }
            mCurrentTime = System.currentTimeMillis();
            if (mCurrentTime - mPreviousTime < DOUBLE_TAP_TIMEOUT
                    && mTargetedView != null && mTargetedView == targetView) {
                handleEvent();
                mPreviousTime = 0L; // set previous time to zero in case tap many times
            } else {
                mTargetedView = targetView;
                mTargetedView.startHintAnimation();
                updateIndication();
                mPreviousTime = mCurrentTime;
            }
        }
        return true;
    }

    private void handleEvent() {
        int id = mTargetedView.getId();
        String mFuncName = "";
        if (id == R.id.func_torch) {
            mFuncName = "func_torch";
            mFlashlightController.setFlashlight(!mFlashlightController.isEnabled());
            return;
        }
        if (id == R.id.func_apps) {
            mFuncName = "func_apps";
            CharSequence pkgName = mTargetedView.getContentDescription();
            PackageManager pm = mContext.getPackageManager();
            // MOD-BEGIN by zhiqianghu, 11/15/2016, for 3402581
            //Intent targetIntent = pm.getLaunchIntentForPackage(pkgName.toString());
            String[] componentName = pkgName.toString().split("/");
            Intent targetIntent = null;
            if (componentName != null && componentName.length == 2) {
                ComponentName com = new ComponentName(componentName[0], componentName[1]);
                targetIntent = new Intent();
                targetIntent.setComponent(com);
            }
            // MOD-END by zhiqianghu, 11/15/2016, for 3402581
            if (mActivityStarter != null && targetIntent != null) {
                mActivityStarter.startActivity(targetIntent, false);
            }
            return;
        }
        Intent targetIntent = new Intent();
        boolean dismissShade = false;
        boolean canSkipBouncer = mUpdateMonitor.getUserCanSkipBouncer(
                KeyguardUpdateMonitor.getCurrentUser());
        boolean secure = mLockPatternUtils.isSecure(KeyguardUpdateMonitor.getCurrentUser());
        isOpneYahooSearch = false; // MODIFIED by junyong.sun, 2016-11-09,BUG-3319328
        switch (id) {
            case R.id.func_voice_search:
                if (secure && !canSkipBouncer) {
                    AsyncTask.execute(() -> {
                        mAssistManager.launchVoiceAssistFromKeyguard();
                        mActivityStarter.preventNextAnimation();
                    });
                    return;
                }
                targetIntent.setAction(Intent.ACTION_VOICE_ASSIST);
                mFuncName = "func_voice_search";
                break;
            case R.id.func_call:
                targetIntent.setAction(Intent.ACTION_DIAL);
                mFuncName = "func_call";
                break;
            case R.id.func_yahoo_search:
                /* MODIFIED-BEGIN by junyong.sun, 2016-11-09,BUG-3319328*/
                Log.d(TAG,"handle event func_yahoo_search");
                if(("com.android.chrome").equals(getDefaultBrowerappPackagename(mContext))){
                    Log.d(TAG,"handle event func_yahoo_search");
                    isOpneYahooSearch = true;
                }else{
                    String mYahooUrl = mContext.getResources().getString(com.android.systemui.R.string.def_jrdsystemui_yahoo_search_url_default);
                    targetIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mYahooUrl));
                    targetIntent.putExtra("isYahooSearch", true);
                }
                /* MODIFIED-END by junyong.sun,BUG-3319328*/
                mFuncName = "func_yahoo_search";
                break;
            case R.id.func_camera:
                dismissShade = secure && !canSkipBouncer;
                targetIntent = dismissShade ? KeyguardBottomAreaView.SECURE_CAMERA_INTENT : KeyguardBottomAreaView.INSECURE_CAMERA_INTENT;
                mFuncName = "func_camera";
                break;
            case R.id.func_recognise_song:
                targetIntent.setClassName("com.shazam.android",
                   "com.shazam.android.activities.MainActivity");
                targetIntent.setAction("com.shazam.android.intent.actions.START_TAGGING");
                mFuncName = "func_recognise_song";
                break;
            case R.id.func_timer:
                targetIntent.setAction("android.intent.action.SET_TIMER");
                // i.setPackage("com.android.deskclock");
                mFuncName = "func_timer";
                break;
            case R.id.func_magic_unlock:
                targetIntent.setAction("com.tct.magiclock.action.SETTINGS");
                mFuncName = "func_magic_unlock";
                break;
            case R.id.func_selfie:
                dismissShade = secure && !canSkipBouncer;
                if (dismissShade) {
                    targetIntent = KeyguardBottomAreaView.SECURE_CAMERA_INTENT;
                    targetIntent.setClassName("com.tct.camera", "com.android.camera.SecureCameraActivity");
                    targetIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    targetIntent.setAction(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA_SECURE);
                    targetIntent.putExtra("func_selfie",true);
                } else {
                    targetIntent.setAction("com.tct.camera.STARTFRONTCAMERA");
                }
                mFuncName = "func_selfie";
                break;
            case R.id.func_music:
                 /* MODIFIED-BEGIN by song.huan, 2016-10-31,BUG-3081621*/
                 /* MODIFIED-BEGIN by dongdong.li, 2016-11-09,BUG-3319297*/
                 dismissShade = secure && !canSkipBouncer;
                 ComponentName componet = ComponentName.unflattenFromString(
                        mContext.getResources().getString(com.android.systemui.R.string.func_music_component));
                 targetIntent.setAction("android.media.action.MEDIA_PLAY_FROM_SEARCH");
                 targetIntent.putExtra("showByLocked", true);
                 targetIntent.putExtra("IsSecure", dismissShade);
                 /* MODIFIED-END by dongdong.li,BUG-3319297*/
                 targetIntent.setComponent(componet);
                 /* MODIFIED-END by song.huan,BUG-3081621*/
                break;
            case R.id.func_message_new:
                Uri smsToUri = Uri.parse("smsto:");
                targetIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
                targetIntent.putExtra("sms_body", "");
                mFuncName = "func_message_new";
                break;
            case R.id.func_email_new:
                Uri uri = Uri.parse("mailto:");
                targetIntent = new Intent(Intent.ACTION_SENDTO, uri);
                mFuncName = "func_email_new";
                break;
            case R.id.func_contact_new:
                targetIntent.setAction(Contacts.Intents.Insert.ACTION);
                targetIntent.setType(Contacts.People.CONTENT_TYPE);
                mFuncName = "func_contact_new";
                break;
            case R.id.func_event_new:
                targetIntent.setAction("com.google.android.calendar.EVENT_EDIT");
                targetIntent.setClassName("com.google.android.calendar", "com.android.calendar.AllInOneActivity"); // MODIFIED by song.huan, 2016-11-01,BUG-3275288
                targetIntent.putExtra("createEditSource", "fab");
                targetIntent.putExtra("is_find_time_promo", false);
                break;
            case R.id.func_sound_record:
                targetIntent.setClassName("com.tct.soundrecorder", "com.tct.soundrecorder.SoundRecorder");
                targetIntent.putExtra("dowhat", "record");
                targetIntent.setAction(Intent.ACTION_MAIN);
                mFuncName = "func_sound_record";
                break;
            case R.id.func_navigate:
                targetIntent.setClassName("com.google.android.apps.maps",
                        "com.google.android.maps.driveabout.app.DestinationActivity");
                break;
            case R.id.func_alarm:
                targetIntent.setAction("android.intent.action.SET_ALARM");
                mFuncName = "func_alarm";
                break;
            case R.id.func_calculator:
                dismissShade = secure && !canSkipBouncer;
                targetIntent.setClassName("com.tct.calculator",
                        "com.tct.calculator.Calculator");
                targetIntent.putExtra("showByLocked", true);
                targetIntent.putExtra("IsSecure", dismissShade);
                mFuncName = "func_calculator";
                break;
            /* MODIFIED-BEGIN by song.huan, 2016-10-18,BUG-3088432*/
            case R.id.func_dial:
                String number = Settings.System.getString(
                        mContext.getContentResolver(), "contactNumber");
                if(number == null || number.equals("")) {
                    targetIntent.putExtra("isNullNumber", true);
                } else {
                    targetIntent.putExtra("isNullNumber", false);
                    targetIntent.putExtra("number", number);
                }
                targetIntent.setClassName("com.android.systemui",
                        "com.android.systemui.statusbar.phone.CallContactActivity");
                break;
                /* MODIFIED-END by song.huan,BUG-3088432*/
            case R.id.func_settings:
                mFuncName = "func_settings";
            default:
                targetIntent.setAction("android.settings.FUNC_SETTINGS");
                break;
        }
        saveEachFuncUsage(mFuncName);
        if (dismissShade) {
            mContext.startActivityAsUser(targetIntent, UserHandle.CURRENT);
        } else if (mActivityStarter != null) {
            mActivityStarter.startActivity(targetIntent, false);
        }
    }

    //[FEATURE]-Add-BEGIN by TCTNB.(Chuanjun Chen), 10/11/2016, FEATURE-2792219 And TASk-2989788.
    private void saveEachFuncUsage(String FuncName) {
        if (FuncName.length() <= 1) {
            return;
        }
        ContentValues values = new ContentValues();
        values.put("action","ADD");
        values.put(("FUNC_TOTAL_"+FuncName.replace(" ", "_")).toUpperCase(),1);
        ContentProviderClient provider   = mContext.getContentResolver().acquireUnstableContentProviderClient(AUTHORITY);
        try {
            if(provider!=null){
                provider.update(CONTENT_URI, values, null, null);
            }
        } catch (IllegalArgumentException e) {
            Log.d(TAG,"llegalArgumentException Exception: " + e);
        } catch (RemoteException e) {
            Log.d(TAG, "write2DB() RemoteException Exception: " + e);
            //Add by quan.zhai for Defect: 1816237 begin
        } catch (NullPointerException e){
            Log.d(TAG, "NullPointerException Exception: " + e);
            //Add by quan.zhai for Defect: 1816237 end
        } finally {
            if(provider!=null){
                provider.release();
            }
        }
    }
    //[FEATURE]-Add-END by TCTNB.(Chuanjun Chen).

    private void updateIndication() {
        if (mIndicationController == null) return;
        int id = mTargetedView.getId();
        String tip;
        if (id == R.id.func_torch) {
            tip = mContext.getString(mFlashlightController.isEnabled()
                    ? com.android.systemui.R.string.func_torch_off : com.android.systemui.R.string.func_torch);
        } else if (id == R.id.func_apps) {
            PackageManager pm = mContext.getPackageManager();
            String pkgName = mTargetedView.getContentDescription().toString();
            try {
                // MOD-BEGIN by zhiqianghu, 11/15/2016, for 3402581
                //ApplicationInfo info = pm.getApplicationInfo(pkgName, PackageManager.GET_META_DATA);
                //tip = mContext.getString(com.android.systemui.R.string.func, pm.getApplicationLabel(info));
                String[] componentName = pkgName.split("/");
                tip = pkgName;
                if (componentName != null && componentName.length == 2) {
                    ComponentName comName = new ComponentName(componentName[0], componentName[1]);
                    ActivityInfo activityInfo = pm.getActivityInfo(comName, PackageManager.GET_META_DATA);

                    final String label = activityInfo.loadLabel(pm).toString();
                    tip = mContext.getString(com.android.systemui.R.string.func, label);
                }
                // MOD-END by zhiqianghu, 11/15/2016, for 3402581
            } catch (Exception e) {
                tip = mContext.getString(com.android.systemui.R.string.func, pkgName);
            }
        } else {
            CharSequence item = mTargetedView.getContentDescription();
            Resources res = mContext.getResources();
            int descId = res.getIdentifier("string/" + item, null, "com.android.systemui");
            tip = descId != 0 ? res.getString(descId) : item.toString();
            /* MODIFIED-BEGIN by song.hua, 2016-12-03,BUG-3397221*/
            String name = Settings.System.getString(mContext.getContentResolver(), "name");
            if(name !=null && tip.equals(mContext.getResources().getString(com.android.systemui.R.string.func_dial))){
                tip = mContext.getResources().getString(com.android.systemui.R.string.func_contact_name)+" "+name;
            }
            /* MODIFIED-END by song.hua,BUG-3397221*/
        }
        mIndicationController.showTransientIndication(tip);
        mIndicationController.hideTransientIndicationDelayed(1200);
    }

    public void onVisibilityChanged(int visibility) {
        boolean vis = visibility == View.VISIBLE;
        if (vis && !mVisible) { // from invisible/gone to visible
            /* MODIFIED-BEGIN by wenguang.zhong, 2016-10-24,BUG-3155378*/
            if (mFlashlightController != null){
                if(mFlashlightController.isAvailable() == true ){
                    hostView.updateTorch(mFlashlightController.isEnabled());
                }
            }
            /* MODIFIED-BEGIN by wenguang.zhong, 2016-10-24,BUG-3155378*/
            updateFuncView();
            if (mFlashlightController != null) mFlashlightController.addListener(this);
        } else if (!vis && mVisible) { // from visible to invisible/gone
            if (mFlashlightController != null) mFlashlightController.removeListener(this);
        }
        mVisible = vis;
    }

    public void updateFuncView() {
        boolean on = isFuncVisible();
        if (on) {
            hostView.updateView();
            hostView.setVisibility(View.VISIBLE);
        } else {
            hostView.setVisibility(View.GONE);
        }
    }

    public boolean isFuncVisible() {
        return Settings.System.getIntForUser(mContext.getContentResolver(),
                Settings.System.TCT_FUNC, 1, UserHandle.USER_CURRENT) == 1;
    }

    @Override
    public void onFlashlightChanged(boolean enabled) {
        hostView.updateTorch(enabled);
    }

    @Override
    public void onFlashlightError() {}

    @Override
    public void onFlashlightAvailabilityChanged(boolean available) {}

    /* MODIFIED-BEGIN by junyong.sun, 2016-11-09,BUG-3319328*/
    public static String getDefaultBrowerappPackagename(Context context){
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addCategory("android.intent.category.BROWSABLE");
        Uri uri = Uri.parse("http://");
        intent.setDataAndType(uri, null);
        List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentActivities(intent,PackageManager.GET_INTENT_FILTERS);
        if(resolveInfoList.size() > 0){
            ActivityInfo activityInfo = resolveInfoList.get(0).activityInfo;
            return activityInfo.packageName;
        }else{
            return "";
        }
    }


    class KeyguardGoneListener extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG,"onReceive : keyguard gone");
            if(intent.getAction().equals(Intent.ACTION_USER_PRESENT)){
                if(isOpneYahooSearch){
                    Log.d(TAG,"onReceive : start brower to yahoo search");
                    isOpneYahooSearch = false;
                    String mYahooUrl = mContext.getResources().getString(com.android.systemui.R.string.def_jrdsystemui_yahoo_search_url_default);
                    Intent targetIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mYahooUrl));
                    targetIntent.putExtra("isYahooSearch", true);
                    mContext.startActivityAsUser(targetIntent, UserHandle.CURRENT);
                }
            }else if(intent.getAction().equals("com.action.KEYGUARD_BOUNCER_BACK_PRESSED")){
                Log.d(TAG,"onReceive : BOUNCER_BACK_PRESSED");
                isOpneYahooSearch = false;
            /* MODIFIED-BEGIN by junyong.sun, 2016-11-10,BUG-3319328*/
            }else if(intent.getAction().equals("com.action.EMERGENCY_BUTTON_CLICK")){
                Log.d(TAG,"onReceive : EMERGENCY_BUTTON_CLICK");
                isOpneYahooSearch = false;
                /* MODIFIED-END by junyong.sun,BUG-3319328*/
            }
        }
    }
    /* MODIFIED-END by junyong.sun,BUG-3319328*/
}
