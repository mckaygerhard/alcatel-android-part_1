package com.android.systemui.wallshuffle;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.android.systemui.R;
/**
 * [SOLUTION]-Created by TCTNB(Guoqiang.Qiu), 2016-8-25, Solution-2699694
 */

public class WallShuffleController {
    private static final String TAG = "WallShuffleController";
    private static WallShuffleController instance;
    private Context mContext;
    private Messenger messenger;
    private OnChangeListener mListener;

    private boolean flag = false;

    private static final int REQ_CONNECT = 10;
    private static final int RES_CONNECT = 11;
    private static final int REQ_UPDATE = 12;
    private static final int RES_UPDATE = 13;
    private static final int REQ_RECORD = 14;
    private static final int REQ_DELETE = 15;
    private static final int REQ_SENSOR = 16;

    private static final String KEY_NAME = "name";
    private static final String KEY_AUTHOR = "author";
    private static final String KEY_LIKE = "like";
    private static final String KEY_ANIM = "animation";

    static String path = "/storage/emulated/0/Android/data/com.tct.magiclock/files/.flickr_pic/";
    String mAnim = "particle";

    static boolean stateLike;
    static boolean statePin;

    private WallShuffleController(Context context) {
        mContext = context;
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            messenger = new Messenger(iBinder);
            Message msg = Message.obtain();
            msg.what = REQ_CONNECT;
            msg.replyTo = new Messenger(mHandler);
            try {
                messenger.send(msg);
            } catch (Exception e) {
                Log.d(TAG,"Message",e);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        }
    };

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == RES_UPDATE) {
                Bundle data = msg.getData();
                String name = data.getString(KEY_NAME);
                String author = data.getString(KEY_AUTHOR);
                stateLike = data.getBoolean(KEY_LIKE);
                mAnim = data.getString(KEY_ANIM);
                mListener.onChange(name, author);
            }
            super.handleMessage(msg);
        }
    };

    public static WallShuffleController getInstance(Context context) {
        if (instance == null) {
            instance = new WallShuffleController(context);
        }
        return instance;
    }

    void bindService() {
        final Intent intent = new Intent();
        ComponentName cname = ComponentName.unflattenFromString(
                mContext.getResources().getString(R.string.magic_service));
        intent.setComponent(cname);
        flag = mContext.bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    void unbindService() {
        if (flag && connection != null) {
            mContext.unbindService(connection);
            flag = false;
        }
    }

    /**
     * Set listener to observe whether wallpapaer has changed
     * @param listener Observer to observe wallpapaer changed
     */
    public void setOnChangeListener(OnChangeListener listener) {
        mListener = listener;
    }

    public void prepareWallpaper() {
        Message msg = Message.obtain();
        msg.what = REQ_UPDATE;
        try {
            messenger.send(msg);
        } catch (Exception e) {
            Log.d(TAG, "Message",e);
        }
    }

    public void registerSensor(boolean status) {
        Message msg = Message.obtain();
        msg.what = REQ_SENSOR;
        msg.arg1 = status ? 1 : 0;
        try {
            messenger.send(msg);
        } catch (Exception e) {
            Log.d(TAG, "Message",e);
        }
    }

    public static boolean likePicture() {
        stateLike = !stateLike;
        recordPinOrLike();
        return stateLike;
    }

    public static boolean pinPicture() {
        statePin = !statePin;
        recordPinOrLike();
        return statePin;
    }

    public static void dislikePicture() {
        Message msg = Message.obtain();
        msg.what = REQ_DELETE;
        try {
            instance.messenger.send(msg);
        } catch (Exception e) {
            Log.d(TAG, "dislikePicture",e);
        }
    }

    private static void recordPinOrLike() {
        Message msg = Message.obtain();
        msg.what = REQ_RECORD;
        msg.arg1 = stateLike ? 1 : 0;
        msg.arg2 = statePin ? 1 : 0;
        try {
            instance.messenger.send(msg);
        } catch (Exception e) {
            Log.d(TAG, "recordPinOrLike",e);
        }
    }

    public interface OnChangeListener {
        /**
         * Called when wallpaper changed
         */
        void onChange(String name, String author);
    }
}