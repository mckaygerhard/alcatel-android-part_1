/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.systemui.statusbar.phone;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ScrollView;
import com.android.systemui.R;
import android.content.BroadcastReceiver;
import android.database.Cursor;


public class CallContactActivity extends Activity {

    private static final String TAG = "CallContactActivityActivity";
    private static final String CONTACT_NUMBER = "contactNumber";
    private static final int PICK_CONTACT_REQUEST = 4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.func_call_contact);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Intent mIntent = getIntent();
        boolean isNullNumber = mIntent.getBooleanExtra("isNullNumber", true);
        if(isNullNumber){
            mIntent = new Intent(Intent.ACTION_PICK,
                    Uri.parse("content://contacts"));
            mIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(mIntent, PICK_CONTACT_REQUEST);
        } else {
            String number = mIntent.getStringExtra("number");
            mIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel://" + number));
            mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
            startActivity(mIntent);
            this.finish();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK
            && requestCode == PICK_CONTACT_REQUEST) {
            Uri contactUri = data.getData();
            String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};
            try (Cursor cursor = getContentResolver().query(contactUri, projection, null, null, null)){
                    /* MODIFIED-BEGIN by song.huan, 2016-10-28,BUG-3258502*/
                    String number = null;
              if(cursor != null && cursor.moveToFirst()) {
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                number = cursor.getString(column);
                Settings.System.putString(getContentResolver(), CONTACT_NUMBER, number);
              }
              if(number != null) {
                  Intent mIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel://" + number));
                  mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
                  startActivity(mIntent);
              }
              /* MODIFIED-END by song.huan,BUG-3258502*/
            } catch (Exception e) {
              e.printStackTrace();
            }
        }
        this.finish();
    }
}
