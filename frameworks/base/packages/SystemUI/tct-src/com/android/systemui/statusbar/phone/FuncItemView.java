package com.android.systemui.statusbar.phone;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.ImageView;

import com.android.systemui.Interpolators;
import com.android.systemui.R;

/**
 * ImageView to display Func icom
 * [SOLUTION]-ADD by TCTNB(Guoqiang.Qiu), 2016-8-8, Solution-2520273
 */

public class FuncItemView extends ImageView {

    private static final long NORMAL_ANIMATION_DURATION = 200;
    private static final float MIN_ICON_SCALE_AMOUNT = 0.8f;
    private static final long HINT_PHASE1_DURATION = 200;
    private static final long HINT_PHASE2_DURATION = 350;
    private static final int HINT_CIRCLE_OPEN_DURATION = 500;

    private final int mMaxBackgroundRadius;
    private final Paint mCirclePaint;
    private final int mInverseColor;
    private final int mNormalColor;
    private final ArgbEvaluator mColorInterpolator;
    private float mCircleRadius;
    private int mCenterX;
    private int mCenterY;
    private ValueAnimator mCircleAnimator;
    private ValueAnimator mScaleAnimator;
    private float mImageScale = 1f;
    private int mCircleColor;

    private AnimatorListenerAdapter mScaleEndListener = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
            mScaleAnimator = null;
        }
    };

    public FuncItemView(Context context) {
        this(context, null);
    }

    public FuncItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FuncItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public FuncItemView(Context context, AttributeSet attrs, int defStyleAttr,
            int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mCirclePaint = new Paint();
        mCirclePaint.setAntiAlias(true);
        mCircleColor = 0xffffffff;
        mCirclePaint.setColor(mCircleColor);

        mNormalColor = 0xffffffff;
        mInverseColor = 0xff000000;
        mMaxBackgroundRadius = mContext.getResources().getDimensionPixelSize(
                R.dimen.func_icon_bg_radius);
        mColorInterpolator = new ArgbEvaluator();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        mCenterX = getWidth() / 2;
        mCenterY = getHeight() / 2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawBackgroundCircle(canvas);
        canvas.save();
        canvas.scale(mImageScale, mImageScale, getWidth() / 2, getHeight() / 2);
        super.onDraw(canvas);
        canvas.restore();
    }

    public void setImageDrawable(Drawable drawable, final int size) {
        Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, size, size);
        drawable.draw(canvas);
        super.setImageDrawable(new BitmapDrawable(getResources(), bitmap));
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        updateIconColor();
        invalidate();
    }

    private void updateIconColor() {
        if (getId() == com.android.internal.R.id.func_apps) return;
        Drawable drawable = getDrawable().mutate();
        float alpha = mCircleRadius / mMaxBackgroundRadius;
        alpha = Math.min(1.0f, alpha);
        int color = (int) mColorInterpolator.evaluate(alpha, mNormalColor, mInverseColor);
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    }

    private void drawBackgroundCircle(Canvas canvas) {
        if (mCircleRadius > 0) {
            updateCircleColor();
            canvas.drawCircle(mCenterX, mCenterY, mCircleRadius, mCirclePaint);
        }
    }

    private void updateCircleColor() {
        double fraction = Math.abs(Math.sin(mCircleRadius * Math.PI / mMaxBackgroundRadius / 2));
        int color = Color.argb((int) (Color.alpha(mCircleColor) * fraction),
                Color.red(mCircleColor),
                Color.green(mCircleColor), Color.blue(mCircleColor));
        mCirclePaint.setColor(color);
    }

    private ValueAnimator getAnimatorToRadius(float circleRadius) {
        ValueAnimator animator = ValueAnimator.ofFloat(mCircleRadius, circleRadius);
        mCircleAnimator = animator;
        animator.addUpdateListener((animation) -> {
            mCircleRadius = (float) animation.getAnimatedValue();
            updateIconColor();
            invalidate();
        });
        return animator;
    }

    private void cancelAnimator(Animator animator) {
        if (animator != null) {
            animator.cancel();
        }
    }

    public void setImageScale(float imageScale, boolean animate) {
        setImageScale(imageScale, animate, -1, null);
    }

    /**
     * Sets the scale of the containing image
     *
     * @param imageScale The new Scale.
     * @param animate Should an animation be performed
     * @param duration If animate, whats the duration? When -1 we take the default duration
     * @param interpolator If animate, whats the interpolator? When null we take the default
     *                     interpolator.
     */
    public void setImageScale(float imageScale, boolean animate, long duration,
            Interpolator interpolator) {
        cancelAnimator(mScaleAnimator);
        if (!animate) {
            mImageScale = imageScale;
            invalidate();
        } else {
            ValueAnimator animator = ValueAnimator.ofFloat(mImageScale, imageScale);
            mScaleAnimator = animator;
            animator.addUpdateListener((animation) -> {
                mImageScale = (float) animation.getAnimatedValue();
                invalidate();
            });
            animator.addListener(mScaleEndListener);
            if (interpolator == null) {
                interpolator = imageScale == 0.0f
                        ? Interpolators.FAST_OUT_LINEAR_IN
                        : Interpolators.LINEAR_OUT_SLOW_IN;
            }
            animator.setInterpolator(interpolator);
            if (duration == -1) {
                float durationFactor = Math.abs(mImageScale - imageScale)
                        / (1.0f - MIN_ICON_SCALE_AMOUNT);
                durationFactor = Math.min(1.0f, durationFactor);
                duration = (long) (NORMAL_ANIMATION_DURATION * durationFactor);
            }
            animator.setDuration(duration);
            animator.start();
        }
    }

    /**
     * Start animation of Func icon
     */
    public void startHintAnimation() {
        setImageScale(1.2f, true);
        cancelAnimator(mCircleAnimator);
        ValueAnimator animator = getAnimatorToRadius(mMaxBackgroundRadius);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCircleAnimator = null;
                startUnlockHintAnimationPhase2(); //move back
            }
        });
        animator.setInterpolator(Interpolators.LINEAR_OUT_SLOW_IN);
        animator.setDuration(HINT_PHASE1_DURATION);
        animator.start();
    }

    /**
     * Phase 2: Move back.
     */
    private void startUnlockHintAnimationPhase2() {
        ValueAnimator animator = getAnimatorToRadius(0f);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                setImageScale(1f, true);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mCircleAnimator = null;
            }
        });
        animator.setInterpolator(Interpolators.FAST_OUT_LINEAR_IN);
        animator.setDuration(HINT_PHASE2_DURATION);
        animator.setStartDelay(HINT_CIRCLE_OPEN_DURATION);
        animator.start();
    }

}
