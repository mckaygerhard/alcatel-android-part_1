package com.android.settingslib.bluetooth;
/******************************************************************************/
/*                                                               Date:10/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  dongdong.gong                                                   */
/*  Email  :  dongdong.gong@tcl.com                                           */
/*  Role   :  engineer                                                        */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 10/10/2016|    dongdong.gong     |       3055667        |For Dual Bluetoo- */
/*           |                      |                      |th Dev            */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

import android.content.Context;
import android.support.v7.preference.Preference;

public class BluetoothPreferenceUp extends Preference {

	private notifyChange notifyChange;
	
	public BluetoothPreferenceUp(Context context) {
		super(context);
	}
	
	public void notifySummaryDiscAlways(){
		notifyChange.onSummaryChangedOne();
	}

	public void notifySummaryIsDiscover(String textTimeout){
		notifyChange.onSummaryChangedTwo(textTimeout);
	}

	public void registeNotify(notifyChange notify){
		this.notifyChange = notify;
	}

	public void unRegisteNotify(){
		this.notifyChange = null;
	}

	public interface notifyChange {
		void onSummaryChangedOne();
		void onSummaryChangedTwo(String textTimeout);
		void onSummaryChangedThree();
		void onSummaryChangedFour();
	}

	public void notifySummaryVisible2Paired() {
		notifyChange.onSummaryChangedThree();
	}

	public void notifySummaryInvisible2Other() {
		notifyChange.onSummaryChangedFour();
	}

}
