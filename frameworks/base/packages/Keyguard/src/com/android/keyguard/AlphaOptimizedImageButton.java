/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.keyguard;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520312
//[HOMO]Replace Enter icon by an Accept/OK text in PIN request screen, Discard text doesn't fit.
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.util.Log;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

/**
 * A frame layout which does not have overlapping renderings commands and therefore does not need a
 * layer when alpha is changed.
 */
public class AlphaOptimizedImageButton extends ImageButton {

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520312
//[HOMO]Replace Enter icon by an Accept/OK text in PIN request screen, Discard text doesn't fit.
    private String mtext = "";
    private String TAG = "";

    public void setText(String text){
        mtext = text;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float density = getContext().getResources().getDisplayMetrics().density;
        int textSize = (int)(20*density);
        Log.e(TAG, "AlphaOptimizedImageButton onDraw density = " + density + "  textSize = "+ textSize);
        Paint paint = new Paint();
        paint.setTextAlign(Align.CENTER);
        paint.setColor(Color.WHITE);
        paint.setTextSize(textSize);
        canvas.drawText(mtext, canvas.getWidth()/2, (canvas.getHeight()/2)+12, paint);
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    public AlphaOptimizedImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
