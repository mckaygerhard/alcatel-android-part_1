/*willab*/
#include <stdint.h>
#include <sys/types.h>

#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/Vector.h>
#include <utils/Timers.h>

#include <binder/Parcel.h>
#include <binder/IInterface.h>


#include <gui/IFileLockServer.h>

namespace android {
// ----------------------------------------------------------------------------

enum {
    FILE_LOCK,
	FILE_UNLOCK,
	FILE_HIDE_LOCK,
	FILE_UNHIDE_ANY_FILE,
	FILE_HIDE_UNLOCK,
	FILE_CHECK_STATE,
    END
};

class BpFileLockServer : public BpInterface<IFileLockServer>
{
public:
    BpFileLockServer(const sp<IBinder>& impl)
        : BpInterface<IFileLockServer>(impl)
    {
    }

    virtual ~BpFileLockServer();

	virtual int lockFile(const String8& path)
	{
        Parcel data, reply;
        data.writeInterfaceToken(IFileLockServer::getInterfaceDescriptor());
        data.writeString8(path);
        remote()->transact(FILE_LOCK, data, &reply);
        return reply.readInt32();
    }

	virtual int unLockFile(const String8& path)
	{
        Parcel data, reply;
        data.writeInterfaceToken(IFileLockServer::getInterfaceDescriptor());
        data.writeString8(path);
        remote()->transact(FILE_UNLOCK, data, &reply);
        return reply.readInt32();
    }

	virtual int hideLockFile(void)
	{
        Parcel data, reply;
        data.writeInterfaceToken(IFileLockServer::getInterfaceDescriptor());
        remote()->transact(FILE_HIDE_LOCK, data, &reply);
        return reply.readInt32();
    }

	virtual int unHideAnyFile(void)
	{
        Parcel data, reply;
        data.writeInterfaceToken(IFileLockServer::getInterfaceDescriptor());
        remote()->transact(FILE_UNHIDE_ANY_FILE, data, &reply);
        return reply.readInt32();
    }

	virtual int hideUnLockFile(void)
	{
        Parcel data, reply;
        data.writeInterfaceToken(IFileLockServer::getInterfaceDescriptor());
        remote()->transact(FILE_HIDE_UNLOCK, data, &reply);
        return reply.readInt32();
    }

	virtual int32_t checkFileState(const String8& path, int32_t& flag)
	{
		Parcel data, reply;
		data.writeInterfaceToken(IFileLockServer::getInterfaceDescriptor());
		data.writeString8(path);
		remote()->transact(FILE_CHECK_STATE, data, &reply);

        int32_t result = reply.readInt32();
        flag = reply.readInt32();
        return result;
	}

};

BpFileLockServer::~BpFileLockServer() {}

IMPLEMENT_META_INTERFACE(FileLockServer, "android.gui.FileLockServer");

// ----------------------------------------------------------------------

status_t BnFileLockServer::onTransact(
    uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags)
{
    switch(code) {
        case FILE_LOCK: {
            CHECK_INTERFACE(IFileLockServer, data, reply);
            String8 path = data.readString8();
			int32_t ret = lockFile(path);
            reply->writeInt32(static_cast<int32_t>(ret));
            return NO_ERROR;
        }

		case FILE_UNLOCK: {
            CHECK_INTERFACE(IFileLockServer, data, reply);
            String8 path = data.readString8();
			int32_t ret = unLockFile(path);
            reply->writeInt32(static_cast<int32_t>(ret));
            return NO_ERROR;
        }

		case FILE_HIDE_LOCK: {
            CHECK_INTERFACE(IFileLockServer, data, reply);
			int32_t ret = hideLockFile();
            reply->writeInt32(static_cast<int32_t>(ret));
            return NO_ERROR;
        }

		case FILE_UNHIDE_ANY_FILE: {
            CHECK_INTERFACE(IFileLockServer, data, reply);
			int32_t ret = unHideAnyFile();
            reply->writeInt32(static_cast<int32_t>(ret));
            return NO_ERROR;
        }

		case FILE_HIDE_UNLOCK: {
            CHECK_INTERFACE(IFileLockServer, data, reply);
			int32_t ret = hideUnLockFile();
            reply->writeInt32(static_cast<int32_t>(ret));
            return NO_ERROR;
        }

		case FILE_CHECK_STATE: {
            CHECK_INTERFACE(IFileLockServer, data, reply);
			String8 path = data.readString8();
			int32_t flag = -1;
			int32_t ret = checkFileState(path, flag);
            reply->writeInt32(static_cast<int32_t>(ret));
			reply->writeInt32(flag);
            return NO_ERROR;
        }
    }
    return BBinder::onTransact(code, data, reply, flags);
}

// ----------------------------------------------------------------------------
}; // namespace android

