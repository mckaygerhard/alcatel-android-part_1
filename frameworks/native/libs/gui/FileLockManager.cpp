/*
willab
 */
#define LOG_TAG "FileLockManager"

#include <stdint.h>
#include <sys/types.h>

#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/Singleton.h>

#include <binder/IBinder.h>
#include <binder/IServiceManager.h>

#include <gui/IFileLockServer.h>
#include <gui/FileLockManager.h>

// ----------------------------------------------------------------------------
namespace android {
// ----------------------------------------------------------------------------

android::Mutex android::FileLockManager::sLock;
android::FileLockManager* android::FileLockManager::mFileLockManager;

FileLockManager&  FileLockManager::getInstanceForFileLock() {
	Mutex::Autolock _l(sLock);
	FileLockManager* flm = mFileLockManager;
	if (flm == 0) {
		flm = new FileLockManager();
		mFileLockManager = flm;
	}
	return *flm;
}

FileLockManager::FileLockManager()
{
    // okay we're not locked here, but it's not needed during construction
    assertStateLocked();
}

FileLockManager::~FileLockManager()
{
}

status_t FileLockManager::checkFileLockService()
{
	const String16 name("FileLock");
	status_t err;
	
	sp<IBinder> binder = defaultServiceManager()->checkService(name);
	if (binder == NULL)
		err = NAME_NOT_FOUND;
	else
		err = NO_ERROR;

	return err;
}

void FileLockManager::fileLockManagerDied()
{
    Mutex::Autolock _l(mLock);
    mFileLockServer.clear();
	if (!mFileLockManager)
		delete mFileLockManager;
}

status_t FileLockManager::assertStateLocked() const {
    bool initFileLockManager = false;
    if (mFileLockServer == NULL) {
        initFileLockManager = true;
    } else {
        // Ping binder to check if sensorservice is alive.
        status_t err = IInterface::asBinder(mFileLockServer)->pingBinder();
        if (err != NO_ERROR) {
            initFileLockManager = true;
        }
    }
    if (initFileLockManager) {
        const String16 name("FileLock");
        for (int i = 0; i < 60; i++) {
            status_t err = getService(name, &mFileLockServer);
            if (err == NAME_NOT_FOUND) {
                sleep(1);
                continue;
            }
            if (err != NO_ERROR) {
                return err;
            }
            break;
        }

        class DeathObserver : public IBinder::DeathRecipient {
            FileLockManager& mFileLockManager;
            virtual void binderDied(const wp<IBinder>& who) {
                ALOGW("FileLock server died [%p]", who.unsafe_get());
                mFileLockManager.fileLockManagerDied();
            }
        public:
            DeathObserver(FileLockManager& mgr) : mFileLockManager(mgr) { }
        };

        LOG_ALWAYS_FATAL_IF(mFileLockServer.get() == NULL, "getService(FileLockServer) NULL");

        mDeathObserver = new DeathObserver(*const_cast<FileLockManager *>(this));
        IInterface::asBinder(mFileLockServer)->linkToDeath(mDeathObserver);

    }

    return NO_ERROR;
}

int FileLockManager::lockFile(const String8 path) {
	Mutex::Autolock _l(mLock);
	ALOGE("hello FileLockManager lockFile");
	return mFileLockServer->lockFile(path);

}

int FileLockManager::unLockFile(const String8 path) {
	Mutex::Autolock _l(mLock);
	ALOGE("hello FileLockManager unLockFile");
	return mFileLockServer->unLockFile(path);

}

int FileLockManager::hideLockFile() {
	Mutex::Autolock _l(mLock);
	ALOGE("hello FileLockManager hideLockFile");
	return mFileLockServer->hideLockFile();

}

int FileLockManager::unHideAnyFile() {
	Mutex::Autolock _l(mLock);
	ALOGE("hello FileLockManager unHideAnyFile");
	return mFileLockServer->unHideAnyFile();

}

int FileLockManager::hideUnLockFile() {
	Mutex::Autolock _l(mLock);
	ALOGE("hello FileLockManager hideUnLockFile");
	return mFileLockServer->hideUnLockFile();

}

int FileLockManager::checkFileState(const String8 path, int32_t& flag)
{
	Mutex::Autolock _l(mLock);
	ALOGE("hello FileLockManager checkFileState");
	return mFileLockServer->checkFileState(path, flag);
}


// ----------------------------------------------------------------------------
}; // namespace android


