/*willab*/
#include <inttypes.h>
#include <math.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <cutils/properties.h>

#include <utils/threads.h>
#include <utils/Atomic.h>
#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/Singleton.h>
#include <utils/String16.h>

#include <binder/BinderService.h>
#include <binder/IServiceManager.h>
#include <binder/PermissionCache.h>

#include <gui/IFileLockServer.h>
#include <hardware_legacy/power.h>



#include "FileLock.h"

#define WILLOGE ALOGE

namespace android {
// ---------------------------------------------------------------------------
char sdAliasPath[][32]
{
	"/sdcard",
	"/storage/sdcard0",
	"/storage/emulated/legacy",
	"/storage/emulated/0",
	"/mnt/sdcard",
	"/data/media/0"
};

const char sdMountPath[] = "/data/media/0";

FileLock::FileLock()
{
	mEventThread = new EventThread();
}

FileLock::~FileLock()
{
	mEventThread->waitForLastEventProcess();
	mEventThread->setEnable(true);
	
	mEventThread->requestExit();

	if (mEventThread.get()) {
		mEventThread.clear();
	}
}

void FileLock::onFirstRef()
{
	WILLOGE("hello FileLock::onFirstRef ...");
}

int FileLock::lockFile(const String8& path)
{
	WILLOGE("hello lockfile ...");

	mEventThread->onReceiveEvent(FILE_LOCK_CMD_FILE_LOCK, path.string(), FILE_LOCK_EN);
	return mEventThread->waitForResult();
}

int FileLock::unLockFile(const String8& path)
{
	WILLOGE("hello unlockfile ...");

	mEventThread->onReceiveEvent(FILE_LOCK_CMD_FILE_LOCK, path.string(), FILE_LOCK_DIS);
	return mEventThread->waitForResult();
}

int FileLock::hideLockFile(void)
{
	WILLOGE("hello hide lock file ...");

	mEventThread->onReceiveEvent(FILE_LOCK_CMD_HIDE_LOCK, NULL, HIDE_LOCK_FLIE);
	return mEventThread->waitForResult();
}

int FileLock::unHideAnyFile(void)
{
	WILLOGE("hello unhide any file ...");

	mEventThread->onReceiveEvent(FILE_LOCK_CMD_HIDE_LOCK, NULL, UNHIDE_ANY_FILE);
	return mEventThread->waitForResult();
}

int FileLock::hideUnLockFile(void)
{
	WILLOGE("hello unhide any file ...");

	mEventThread->onReceiveEvent(FILE_LOCK_CMD_HIDE_LOCK, NULL, HIDE_UNLOCK_FILE);
	return mEventThread->waitForResult();
}

int FileLock::checkFileState(const String8& path, int32_t& flag)
{
	WILLOGE("hello checkFileState ...");

	mEventThread->onReceiveEvent(FILE_LOCK_CMD_CHECK_STATE, path.string(), -1);
	return mEventThread->waitForResult(flag);
}

// ---------------------------------------------------------------------------
void EventThread::onFirstRef()
{
	run("FileLockControl", PRIORITY_URGENT_DISPLAY);
}

status_t EventThread::readyToRun()
{
	WILLOGE("hello readyToRun  ...");

	setEnable(false);
	
	while (mWaitResultCount) {
		WILLOGE("hello maybe the thread exited illegally last time  ...");
		mResult = FILE_LOCK_OPTION_FAILED;
		NotifyResult();
	}

	return NO_ERROR;
}

void EventThread::waitForLastEventProcess()
{
	sem_wait(&mEventSem);
}

void EventThread::setEnable(bool enable)
{
    if (mEnabled != enable) {
        mEnabled = enable;
        mCondition.signal();
    }
}

void EventThread::parsePath(char* srcPath, char* destPath)
{
	char* psrc = srcPath;
	char* pdest = destPath;
	
	for(int i=0; i<6; i++){
		char *p = NULL;
		char *tmp = NULL;
	
		size_t size = strlen(sdAliasPath[i]);
		if(strncmp(psrc, sdAliasPath[i], size) == 0){
			p = psrc + size;
		
			memcpy(pdest, sdMountPath, strlen(sdMountPath));
			memcpy(pdest+strlen(sdMountPath), p, strlen(psrc) - size + 1);
			break;
		}
	}
}

void EventThread::onReceiveEvent(int cmd, const char* filename, unsigned char flag)
{
	WILLOGE("hello onReceiveEvent wait to post event");
	//Mutex::Autolock _l(mLock);
	//mEventCount++;
	waitForLastEventProcess();
	
	memset(&mFileOpsEvent, 0, sizeof(struct FileOpsEvent));
	
	mFileOpsEvent.cmd = cmd;
	if(filename)
		parsePath((char *)filename, mFileOpsEvent.file_ctl.filename);
	mFileOpsEvent.file_ctl.flag = flag;

	mEventCount++;
	
	WILLOGE("hello onReceiveEvent post event, mEventCount:%d", mEventCount);
	mCondition.signal();
}

int EventThread::waitForResult()
{
	WILLOGE("hello wait for result....");
	mWaitResultCount++;
	sem_wait(&mResultSem);
	WILLOGE("hello return result to client");
	return mResult;
}

int EventThread::waitForResult(int& flag)
{
	WILLOGE("hello wait for result....");
	mWaitResultCount++;
	sem_wait(&mResultSem);
	flag = mFileOpsEvent.file_ctl.flag;
	WILLOGE("hello flag is %d", flag);
	WILLOGE("hello return result to client");
	return mResult;
}

void EventThread::NotifyResult()
{
	mWaitResultCount--;
	sem_post(&mResultSem);
}

unsigned long EventThread::calcMagicData(unsigned long arg) 
{
    unsigned long new_buf[8];
    unsigned long m=0;
    int i;

    for(i=0; i<8; i++) {
        new_buf[i] = mCodeKeyBuf[i] ^ arg;
        m = m + new_buf[i] / arg + new_buf[i] % arg; 
    }

    return m;
}

int EventThread::registerThreadToKmodule()
{
    unsigned long random_data = 0;
    struct registerPtheadInfo info;
    
    mFLModuleFd = open(FILE_LOCK_MODULE,O_RDWR);
    if (mFLModuleFd < 0)
    {
        return FILE_LOCK_OPTION_FAILED;
    }

    if (ioctl(mFLModuleFd, FILE_LOCK_CMD_GET_RANDOM, &random_data) < 0)
    {
        close(mFLModuleFd);
        return FILE_LOCK_OPTION_FAILED;
    }
     
    info.tid = syscall(SYS_gettid);
    info.magic_data = calcMagicData(random_data);

    if (ioctl(mFLModuleFd, FILE_LOCK_CMD_REGISTER_PID, &info) < 0)
    {
        close(mFLModuleFd);
        return FILE_LOCK_OPTION_FAILED;
    }

    return FILE_LOCK_OPTION_SUCCESS;
}

int EventThread::processCmd()
{
	int ret = FILE_LOCK_OPTION_FAILED;
	int isHideEnable;

	if (mFLModuleFd < 0) {
		ALOGE("hello module fd error");
		return ret;
	}
	
#if 0
	ALOGE("hello start test");
	 for (int k=1; k<=2000; k++) { 

            sprintf(mFileOpsEvent.file_ctl.filename, "/data/media/0/test/groupfiles/%d.txt", k); 

            //mFileOpsEvent.file_ctl.flag = FILE_LOCK_EN;
			mFileOpsEvent.file_ctl.flag = FILE_LOCK_DIS;
			
      	    if (ioctl(mFLModuleFd, FILE_LOCK_CMD_FILE_LOCK, &(mFileOpsEvent.file_ctl)) < 0) {

                ret = FILE_LOCK_OPTION_FAILED; 
				ALOGE("hello test failed");
				return ret;

			}

            else
                ret = FILE_LOCK_OPTION_SUCCESS;

	 }
	ALOGE("hello end test");
	return ret;

#endif

	switch (mFileOpsEvent.cmd) {
	case FILE_LOCK_CMD_FILE_LOCK:
	case FILE_LOCK_CMD_CHECK_STATE:
		if (ioctl(mFLModuleFd, mFileOpsEvent.cmd, &(mFileOpsEvent.file_ctl)) < 0)
		    ret = FILE_LOCK_OPTION_FAILED; 
		else
		    ret = FILE_LOCK_OPTION_SUCCESS;
		break;

	case FILE_LOCK_CMD_HIDE_LOCK:
		isHideEnable = (int)mFileOpsEvent.file_ctl.flag;
		if (ioctl(mFLModuleFd, FILE_LOCK_CMD_HIDE_LOCK, &isHideEnable) < 0)
            ret = FILE_LOCK_OPTION_FAILED;
        else
            ret = FILE_LOCK_OPTION_SUCCESS;
		break;

	default:
		break;
		
	}
	
	WILLOGE("hello processCmd result is %d", ret);

	return ret;
}

void EventThread::initiate()
{
	int register_count = 3;
	WILLOGE("hello EventThread::initiate");
	while(register_count--) {
        if (!registerThreadToKmodule())
            break;
    }

	/*register successful, so mFLModuleFd>0*/
    if (register_count) {
		//int isHideEnable = 0;
		//ioctl(mFLModuleFd, FILE_LOCK_CMD_HIDE_LOCK, &isHideEnable);
		mInitFlag = true;
	}
	else {
		mInitFlag = false;
	}
}

void EventThread::releaseCurrentEventProcess()
{
	sem_post(&mEventSem);
}
bool EventThread::threadLoop()
{
	if (!mInitFlag) 
		initiate();

	//Mutex::Autolock _l(mLock);
	if (mEventCount <= 0) {
#if 0		
		if (mCondition.waitRelative(mLock, milliseconds(50)) == TIMED_OUT) {
			return true;
		} 
#else	
		if (!mEnabled) {
			mCondition.wait(mLock);
		}
#endif
	}
	else 
		WILLOGE("hello has event to process not sleep...");

	if (mEventCount <= 0)
		return true;
	
	mEventCount--;
	WILLOGE("hello process event, cmd:%d, path:%s",mFileOpsEvent.cmd, mFileOpsEvent.file_ctl.filename);

	mResult = processCmd();
	//while(1) {sleep(1);}
	NotifyResult();

	releaseCurrentEventProcess();

	return true;
}

// ---------------------------------------------------------------------------
}; // namespace android


