LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	FileLock.cpp \

LOCAL_CFLAGS:= -DLOG_TAG=\"FileLock\"

LOCAL_CFLAGS += -fvisibility=hidden

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libhardware \
	libhardware_legacy \
	libutils \
	liblog \
	libbinder \
	libui \
	libgui

LOCAL_MODULE:= libfilelock

include $(BUILD_SHARED_LIBRARY)

#####################################################################
# build executable
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	main_filelock.cpp

LOCAL_SHARED_LIBRARIES := \
	libfilelock \
	libbinder \
	libutils

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= filelock
LOCAL_INIT_RC := FileLock.rc
include $(BUILD_EXECUTABLE)
