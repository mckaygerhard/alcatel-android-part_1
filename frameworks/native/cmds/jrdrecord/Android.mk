# Copyright (C) 2016 Tcl Corporation Limited
# [FEATURE]-ADD-BEGIN by TCTSH.(yanxi.liu), For TBR, Task-784014
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

#add by biao.wei@tcl.com add busybox for user version
#ifeq (,$(BUSYBOX))
#BUSYBOX := $(ANDROID_PRODUCT_OUT)/system/bin/busybox
#$(BUSYBOX) : $(LOCAL_PATH)/tools/busybox $(ACP)
#        -mkdir -p $(dir $@)
#        @$(ACP) $< $@
#endif

#LOCAL_ADDITIONAL_DEPENDENCIES := $(BUSYBOX)

LOCAL_SRC_FILES := main.cpp

LOCAL_MODULE := jrdrecord

LOCAL_SHARED_LIBRARIES := libcutils libz liblog
LOCAL_STATIC_LIBRARIES := libpng
LOCAL_C_INCLUDES += external/zlib

include $(BUILD_EXECUTABLE)
# [FEATURE]-ADD-END by TCTSH.(yanxi.liu)
