/******************************************************************************/
/*                                                            Date:27/09/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Long Na                                                         */
/*  Email  :  na.long@tcl-mobile.com                                          */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments : secure number receiver to open bluetooth test mode application */
/*  File     : development/apps/BluetoothTestMode/src/com/BluetoothTestMode/B */
/*             luetoothTestModeReceiver.java                                  */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */

package com.android.NfcTestMode;


import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import com.android.internal.telephony.TelephonyIntents;
import android.util.Log;

public class NfcTestModeReceiver extends BroadcastReceiver {
    private static final String TAG = "NfcTestModeReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive " + intent);

        // fetch up useful stuff
        String action = intent.getAction();
        String host = intent.getData() != null ? intent.getData().getHost() : null;

        // Nfc Test Mode, *#*#363363#*#*
        if (TelephonyIntents.SECRET_CODE_ACTION.equals(action) && "363363".equals(host)) {
            Intent i = new Intent(Intent.ACTION_MAIN);
            i.setClassName("com.android.NfcTestMode","com.android.NfcTestMode.NfcTestMode");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }
}
