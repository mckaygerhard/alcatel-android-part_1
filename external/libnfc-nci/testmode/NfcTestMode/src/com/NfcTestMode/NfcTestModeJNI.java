package com.android.NfcTestMode;

public class NfcTestModeJNI {

    static {
        // The runtime will add "lib" on the front and ".o" on the end of
        // the name supplied to loadLibrary.
        System.loadLibrary("nfctestjni");
    }

    public static native int testNfcSwp();

    public static native int testNfcRfOn();

    public static native int testNfcRfOff();

    public static native int testNfcReadA();
}
