LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

commonIncludes += $(LOCAL_PATH)/..
LOCAL_MODULE_TAGS:= eng debug

LOCAL_SRC_FILES += NfcTestJNI.cpp

#LOCAL_C_INCLUDES := \
#    $(LOCAL_PATH)/ \
#    $(LOCAL_PATH)/../testlib/inc/ \
# Also need the JNI headers.
LOCAL_C_INCLUDES += \
    $(JNI_H_INCLUDE) \
    $(LOCAL_PATH)/../../testlib/inc/ \
    $(commonIncludes)

LOCAL_SHARED_LIBRARIES := libnfctest \
              libcutils

LOCAL_MODULE:= libnfctestjni

include $(BUILD_SHARED_LIBRARY)

