#ifndef NFC_TEST_API_H
#define NFC_TEST_API_H

//#include "nfc_target.h"
//#include "nci_defs.h"
//#include "nfc_hal_api.h"
//#include "gki.h"

#ifdef __cplusplus
extern "C" {
#endif

//[FEATURE]-Add-BEGIN by TCTNB.LongNa,01/26/2016,1527651, Nfc Test Mode
/*******************************************************************************
**
** Function         testNfcSwp
**
** Description      This function test swp
**
** Returns          0 if swp ok, else -1
**
*******************************************************************************/
int testNfcSwp ();
/*******************************************************************************
**
** Function         testNfcRfOn
**
** Description      This function test rf on
**
** Returns          0 if rf on, else -1
**
*******************************************************************************/
int testNfcRfOn ();
/*******************************************************************************
**
** Function         testNfcRfOff
**
** Description      This function test rf off
**
** Returns          0 if rf off, else -1
**
*******************************************************************************/
int testNfcRfOff ();
/*******************************************************************************
**
** Function         testNfcReadA
**
** Description      This function test read type A card
**
** Returns          0 if read success, else -1
**
*******************************************************************************/
int testNfcReadA ();
//[FEATURE]-End-BEGIN by TCTNB.LongNa,01/26/2016,1527651, Nfc Test Mode

#ifdef __cplusplus
}
#endif

#endif /* NFC_TEST_API_H */
