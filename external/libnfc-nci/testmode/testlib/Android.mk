LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_PRELINK_MODULE := false
LOCAL_MODULE := libnfctest
LOCAL_MODULE_TAGS := optional

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/inc


LOCAL_SRC_FILES := \
    $(call all-c-files-under, src)

LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libc \

include $(BUILD_SHARED_LIBRARY)
