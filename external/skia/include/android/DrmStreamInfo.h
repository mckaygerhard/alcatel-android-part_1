/* Copyright (C) 2016 Tcl Corporation Limited */
#include "SkBitmap.h"

class DrmStreamInfo {
public:
       DrmStreamInfo(SkStream *stream, int fd, bool isDrm) {
               fStream = stream;
               mFd = fd;
               mIsDrm = isDrm;
       }

       DrmStreamInfo(SkStream *stream,SkStreamRewindable *streamr, int fd, bool isDrm) {
            fStream = stream;
            mFd = fd;
            mIsDrm = isDrm;
            fStreamRewindable = streamr;
       }

       SkStream* getStream() {return fStream;}
       SkStreamRewindable* getStreamR(){return fStreamRewindable;}
       int getFD() const { return mFd;}
       bool isDrm() {return mIsDrm;}

private:
    SkStream *fStream;
    SkStreamRewindable *fStreamRewindable;
    int mFd;
    bool mIsDrm;
};
