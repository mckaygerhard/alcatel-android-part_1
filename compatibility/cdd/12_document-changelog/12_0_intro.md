# 12\. Document Changelog

For a summary of changes to the Compatibility Definition in this release, refer
to the
[changelog](https://android.googlesource.com/platform/docs/source.android.com/+log/master/src/compatibility/android-cdd.html).
