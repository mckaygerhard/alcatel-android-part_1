## 5.7\. Network Protocols

Devices MUST support the [media network
protocols](http://developer.android.com/guide/appendix/media-formats.html) for
audio and video playback as specified in the Android SDK documentation.
Specifically, devices MUST support the following media network protocols:

*   RTSP (RTP, SDP)
*   HTTP(S) progressive streaming
*   HTTP(S) [Live Streaming draft protocol, Version
    3](http://tools.ietf.org/html/draft-pantos-http-live-streaming-03)
