/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (c) 2008 Travis Geiselbrecht
 *
 * Copyright (c) 2009-2014, The Linux Foundation. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __CONSOLE_H_
#define __CONSOLE_H_
struct console {
	char name[16];
	void (*write) (struct console *, const char *, unsigned);
	int (*read) (struct console *, char *, unsigned);
	int (*setup) (struct console *, char *);
	void *data;
	struct console *next;
	/* MODIFIED-BEGIN by wu.yan, 2016-06-20,BUG-2338999*/
	int level;
	int flags;
	/* MODIFIED-END by wu.yan,BUG-2338999*/
};

#define CON_ENABLED	(1)
#ifndef NULL
#define NULL		0
#endif
#define for_each_console(con) \
	for (con = console_drivers; con != NULL; con = con->next)

extern void register_console(struct console *);
extern int unregister_console(struct console *);
extern struct console *console_drivers;
extern void console_lock(void);
extern int console_trylock(void);
extern void console_unlock(void);
extern int is_console_locked(void);
extern void console_init_early(void);
extern void console_init(void);
#endif				/* __CONSOLE_H_ */
