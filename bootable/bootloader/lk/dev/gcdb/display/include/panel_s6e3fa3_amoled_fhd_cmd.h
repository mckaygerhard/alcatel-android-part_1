/* Copyright (c) 2015, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  * Neither the name of The Linux Foundation nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _PANEL_S6E3FA3_AMOLED_FHD_CMD_H_
#define _PANEL_S6E3FA3_AMOLED_FHD_CMD_H_
/*---------------------------------------------------------------------------*/
/* HEADER files                                                              */
/*---------------------------------------------------------------------------*/
#include "panel.h"

/*---------------------------------------------------------------------------*/
/* Panel configuration                                                       */
/*---------------------------------------------------------------------------*/
#if 0
static struct panel_config s6e3fa3_amoled_fhd_cmd_panel_data = {
	"qcom,mdss_dsi_s6e3fa3_amoled_fhd_cmd", "dsi:0:", "qcom,mdss-dsi-panel",
	10, 1, "DISPLAY_1", 0, 0, 60, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
	NULL
};
#endif

static struct panel_config s6e3fa3_amoled_fhd_cmd_panel_data_with_pmic = {
	"qcom,mdss_dsi_s6e3fa3_amoled_fhd_pmic_cmd", "dsi:0:", "qcom,mdss-dsi-panel",
	10, 1, "DISPLAY_1", 0, 0, 60, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
	NULL
};

/*---------------------------------------------------------------------------*/
/* Panel resolution                                                          */
/*---------------------------------------------------------------------------*/
/* [BUGFIX]-Mod-BEGIN by TCTNB.CY, FR-898333, 2015/11/11, optimize dsi clk configure*/
static struct panel_resolution s6e3fa3_amoled_fhd_cmd_panel_res = {
	1080, 1920, 80, 80, 40, 0, 40, 40, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
/* [PLATFORM]-Mod-END by TCTNB.CY, 2015/11/11*/
/*---------------------------------------------------------------------------*/
/* Panel color information                                                   */
/*---------------------------------------------------------------------------*/
static struct color_info s6e3fa3_amoled_fhd_cmd_color = {
	24, 0, 0xff, 0, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Panel on/off command information                                          */
/*---------------------------------------------------------------------------*/
static char s6e3fa3_amoled_fhd_cmd_on_cmd0[] = {
	0x11, 0x00, 0x05, 0x80
};

static char s6e3fa3_amoled_fhd_cmd_on_cmd1[] = {		//TE on
	0x35, 0x00, 0x15, 0x80
};

//common setting
static char s6e3fa3_amoled_fhd_cmd_on_cmd2[] = {
	0x03, 0x00, 0x39, 0xc0,
	0xfc, 0x5a, 0x5a, 0xff
};

static char s6e3fa3_amoled_fhd_cmd_on_cmd3[] = {
	0x02, 0x00, 0x39, 0xc0,
	0xb0, 0x1e, 0xff, 0xff
};

static char s6e3fa3_amoled_fhd_cmd_on_cmd4[] = {
	0x02, 0x00, 0x39, 0xc0,
	0xfd, 0x9e, 0xff, 0xff
};

static char s6e3fa3_amoled_fhd_cmd_on_cmd5[] = {
	0x03, 0x00, 0x39, 0xc0,
	0xfc, 0xa5, 0xa5, 0xff
};

/* [BUGFIX]-Mod-BEGIN by TCTNB.CY, task-929771, 2015/11/17, enable hsync for tp*/
static char s6e3fa3_amoled_fhd_cmd_on_cmd5_1[] = {
	0x03, 0x00, 0x39, 0xc0,
	0xf0, 0x5a, 0x5a, 0xff
};

static char s6e3fa3_amoled_fhd_cmd_on_cmd5_2[] = {
	0x03, 0x00, 0x39, 0xc0,
	0xbc, 0x11, 0x11, 0xff
};

static char s6e3fa3_amoled_fhd_cmd_on_cmd5_3[] = {
	0x03, 0x00, 0x39, 0xc0,
	0xf0, 0xa5, 0xa5, 0xff
};
/* [PLATFORM]-Mod-END by TCTNB.CY, 2015/11/17*/
/* [PLATFORM]-Mod-BEGIN by TCTNB.WPL, porting from task-1048316 to task-1629668, 2016/02/18, change brightness dimming mode when panel on */
//brightness setting
static char s6e3fa3_amoled_fhd_cmd_on_cmd6[] = {
	0x53, 0x20, 0x15, 0x80
};
/* [BUGFIX]-Mod-END by TCTNB.WPL, 2016/02/18*/

static char s6e3fa3_amoled_fhd_cmd_on_cmd7[] = {
	0x51, 0x7f, 0x15, 0x80
};

static char s6e3fa3_amoled_fhd_cmd_on_cmd8[] = {		//ACL off
	0x55, 0x00, 0x15, 0x80
};

static char s6e3fa3_amoled_fhd_cmd_on_cmd9[] = {
	0x2c, 0x00, 0x05, 0x80
};

static char s6e3fa3_amoled_fhd_cmd_on_cmd10[] = {
	0x29, 0x00, 0x05, 0x80
};

static struct mipi_dsi_cmd s6e3fa3_amoled_fhd_cmd_on_command[] = {
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd0), s6e3fa3_amoled_fhd_cmd_on_cmd0, 0x14},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd1), s6e3fa3_amoled_fhd_cmd_on_cmd1, 0x00},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd2), s6e3fa3_amoled_fhd_cmd_on_cmd2, 0x00},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd3), s6e3fa3_amoled_fhd_cmd_on_cmd3, 0x00},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd4), s6e3fa3_amoled_fhd_cmd_on_cmd4, 0x00},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd5), s6e3fa3_amoled_fhd_cmd_on_cmd5, 0x00},
/* [BUGFIX]-Mod-BEGIN by TCTNB.CY, task-929771, 2015/11/17, enable hsync for tp*/
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd5_1), s6e3fa3_amoled_fhd_cmd_on_cmd5_1, 0x00},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd5_2), s6e3fa3_amoled_fhd_cmd_on_cmd5_2, 0x00},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd5_3), s6e3fa3_amoled_fhd_cmd_on_cmd5_3, 0x00},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd6), s6e3fa3_amoled_fhd_cmd_on_cmd6, 0x00},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd7), s6e3fa3_amoled_fhd_cmd_on_cmd7, 0x00},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd8), s6e3fa3_amoled_fhd_cmd_on_cmd8, 0x96},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd9), s6e3fa3_amoled_fhd_cmd_on_cmd9, 0x00},
	{sizeof(s6e3fa3_amoled_fhd_cmd_on_cmd10), s6e3fa3_amoled_fhd_cmd_on_cmd10, 0x00},
	
};

#define S6E3FA3_AMOLED_FHD_CMD_ON_COMMAND 14
/* [PLATFORM]-Mod-END by TCTNB.CY, 2015/11/17*/

static char s6e3fa3_amoled_fhd_cmdoff_cmd0[] = {
	0x28, 0x00, 0x05, 0x80
};

static char s6e3fa3_amoled_fhd_cmdoff_cmd1[] = {
	0x10, 0x00, 0x05, 0x80
};

static struct mipi_dsi_cmd s6e3fa3_amoled_fhd_cmd_off_command[] = {
	{0x4, s6e3fa3_amoled_fhd_cmdoff_cmd0, 0x32},
	{0x4, s6e3fa3_amoled_fhd_cmdoff_cmd1, 0x78}
};

#define S6E3FA3_AMOLED_FHD_CMD_OFF_COMMAND 2

/* [BUGFIX]-Mod-BEGIN by TCTNB.CY, FR-989087, 2015/11/27, change send cmd mode to hs mode*/
static struct command_state s6e3fa3_amoled_fhd_cmd_state = {
	1, 1
};
/* [BUGFIX]-Mod-END by TCTNB.CY, 2015/11/27*/

/*---------------------------------------------------------------------------*/
/* Command mode panel information                                            */
/*---------------------------------------------------------------------------*/
static struct commandpanel_info s6e3fa3_amoled_fhd_cmd_command_panel = {
	1, 1, 1, 0, 0, 0x2c, 0, 0, 0, 1, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Video mode panel information                                              */
/*---------------------------------------------------------------------------*/
static struct videopanel_info s6e3fa3_amoled_fhd_cmd_video_panel = {
	0, 0, 0, 0, 1, 1, 1, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Lane configuration                                                        */
/*---------------------------------------------------------------------------*/
static struct lane_configuration s6e3fa3_amoled_fhd_cmd_lane_config = {
	4, 0, 1, 1, 1, 1, 0
};

/*---------------------------------------------------------------------------*/
/* Panel timing                                                              */
/*---------------------------------------------------------------------------*/
/* [BUGFIX]-Mod-BEGIN by TCTNB.CY, FR-898333, 2015/11/11, optimize dsi clk configure*/
static const uint32_t s6e3fa3_amoled_fhd_cmd_timings[] = {
	0xF2, 0x3A, 0x28, 0x00, 0x6C, 0x70, 0x2C, 0x3E, 0x2E, 0x03, 0x04, 0x00
};

static const uint32_t s6e3fa3_amoled_fhd_thulium_cmd_timings[] = {
	0x23, 0x1e, 0x7, 0x8, 0x5, 0x3, 0x4, 0xa0,
	0x23, 0x1e, 0x7, 0x8, 0x5, 0x3, 0x4, 0xa0,
	0x23, 0x1e, 0x7, 0x8, 0x5, 0x3, 0x4, 0xa0,
	0x23, 0x1e, 0x7, 0x8, 0x5, 0x3, 0x4, 0xa0,
	0x23, 0x18, 0x7, 0x8, 0x5, 0x3, 0x4, 0xa0,
};

static struct panel_timing s6e3fa3_amoled_fhd_cmd_timing_info = {
	0x0, 0x04, 0x02, 0x2D
};
/* [PLATFORM]-Mod-END by TCTNB.CY, 2015/11/11*/
/*---------------------------------------------------------------------------*/
/* Panel reset sequence                                                      */
/*---------------------------------------------------------------------------*/
static struct panel_reset_sequence s6e3fa3_amoled_fhd_cmd_reset_seq = {
	{1, 0, 1, }, {10, 10, 10, }, 2
};

/*---------------------------------------------------------------------------*/
/* Backlight setting                                                         */
/*---------------------------------------------------------------------------*/
static struct backlight s6e3fa3_amoled_fhd_cmd_backlight = {
	2, 1, 255, 1, 1, NULL	/* BL_DCS */
};
/* [PLATFORM]-Mod-END by TCTNB.WPL, 2015/12/16*/

/* [PLATFORM]-Mod-BEGIN by TCTNB.WPL, porting from task-1158877 to defect-1657510, 2016/02/25, add swire_control field, modify for configure lab register for display */
static struct labibb_desc s6e3fa3_amoled_fhd_cmd_labibb = {
       1, 1, 4000000, 4000000, 4600000, 4600000, 3, 3, 1, 1
};
/* [PLATFORM]-Mod-END by TCTNB.WPL, 2016/02/25*/
#define S6E3FA3_AMOLED_FHD_CMD_SIGNATURE  0xFFFF

#endif
