/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (c) 2008 Travis Geiselbrecht
 *
 * Copyright (c) 2009-2014, The Linux Foundation. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <kernel/console.h>
#include <dev/mmcon.h>
#include <partition_parser.h>
#include <debug.h>
#include <mmc.h>
#include <compiler.h>
#include <string.h>
#include <target.h>

#ifndef LK_LOG_BUF_SIZE
#define LK_LOG_BUF_SIZE    (4096)	/* align on 4k */
#define TRACEABILITY_AFTERSALE_SIZE  0x2000
#endif

struct mmcon_metadata {
	char ptn_name[32];
	uint64_t ptn;
	uint32_t offset;
	uint32_t len;
};

static const char *log_data; // MODIFIED by wu.yan, 2016-06-20,BUG-2338999
static struct mmcon_metadata emmcon_metadata = {
	.ptn_name = "traceability",
	.offset = 0xe8000,
	.len = LK_LOG_BUF_SIZE,
};

void emmcon_write(struct console *con __UNUSED, const char *s, unsigned l)
{
	/* MODIFIED-BEGIN by wu.yan, 2016-06-20,BUG-2338999*/
	uint64_t offset = emmcon_metadata.ptn + emmcon_metadata.offset;
	uint32_t len = emmcon_metadata.len;
	if (in_critical_section())
		return;

	mmc_write(offset, len, (void *)log_data);
}

int emmcon_read(struct console *con __UNUSED, char *d, unsigned l)
{
	uint64_t offset;
	uint32_t len;
	if (in_critical_section())
		return 0;
	offset = emmcon_metadata.ptn + emmcon_metadata.offset;
	len = emmcon_metadata.len;
	/* MODIFIED-END by wu.yan,BUG-2338999*/
	return mmc_read(offset,  (uint32_t *)d, len);
}

static inline int mmcon_fill_metadata(struct mmcon_metadata *md)
{
	int res = -1;
	int index = INVALID_PTN;
	char *ptn_name = md->ptn_name;
	uint64_t ptn;
	if (target_is_emmc_boot()) {
		index = partition_get_index(ptn_name);
		if (index == INVALID_PTN) {
			dprintf(CRITICAL, "No '%s' partition found\n",
				ptn_name);
			res = -2;
			goto out;
		}
		ptn = partition_get_offset(index);
		if (ptn == 0) {
			dprintf(CRITICAL, "partition %s doesn't exist\n",
				ptn_name);
			res = -3;
			goto out;
		}
		md->ptn = ptn;
		res = 0;
	}
 out:
	return res;
}

int emmcon_setup(struct console *con __UNUSED, char *s __UNUSED)
{
	int res;
	res = mmcon_fill_metadata(&emmcon_metadata);
	if (res)
		goto out;
	/* MODIFIED-BEGIN by wu.yan, 2016-06-20,BUG-2338999*/
	log_data = get_lk_log();
	res = (log_data == NULL);
 out:
	return res;
}

static struct console emmcon = {
	.name = "emmcon",
	.level = INFO,
	/* MODIFIED-END by wu.yan,BUG-2338999*/
	.write = emmcon_write,
	.read = emmcon_read,
	.setup = emmcon_setup,
};

void mmcon_init(uint32_t mode)
{
	emmcon.flags |= CON_ENABLED;
        if((mode & 0xF0000)>>16 )
            emmcon_metadata.offset=emmcon_metadata.offset+ TRACEABILITY_AFTERSALE_SIZE;
	register_console(&emmcon);
}

void mmcon_uninit()
{
	emmcon.flags &= ~(CON_ENABLED);
	unregister_console(&emmcon);
}
