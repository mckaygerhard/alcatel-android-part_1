#define DEFAULT_LANG (EN)
#define PAGE_COUNT (4)

static const char *EN_main_header[] = {
	"Volume up/down to move highlight;",
	"Enter button to select.",
	NULL
};

static const char *EN_main_menu[] = {
	"reboot system now",
	"reboot to bootloader",
	"apply update from ADB",
	"apply update from sdcard",
	"wipe data/factory reset",
	"wipe cache partition",
	"mount /system",
	"view recovery logs",
	"Run graphics test",
	"power off",
	"⇦",
	NULL
};

static const char *EN_wipe_data_header[] = {
	"Confirm to wipe all user data?",
	"  THIS CAN NOT BE UNDONE.",
	NULL
};

static const char *EN_wipe_data_menu[] = {
	"No",
	"Yes -- wipe all user data",
	NULL
};

static const char *EN_wipe_cache_header[] = {
	"Confirm to wipe cache data?",
	"  THIS CAN NOT BE UNDONE.",
	NULL
};

static const char *EN_wipe_cache_menu[] = {
	"No",
	"Yes -- wipe cache data",
	NULL
};

static const char *EN_updater_header[] = {
	"Choose a package to install:",
	NULL
};

static const char *EN_updater_menu[] = {
	NULL
};

static const char *ZH_CN_main_header[] = {
	"按音量+/-键移动高亮",
	"按电源键来选择",
	NULL
};

static const char *ZH_CN_main_menu[] = {
	"重新启动系统",
	"重启到引导加载器",
	"用ADB安装更新",
	"从SD卡安装更新",
	"擦除用户数据/恢复出厂设置",
	"擦除缓存区",
	"挂载/system分区",
	"查看恢复模式日志",
	"运行图形测试",
	"关机",
	"⇦",
	NULL
};

static const char *ZH_CN_wipe_data_header[] = {
	"是否擦除所有用户数据?",
	"  注意：擦除之后，用户数据不能复原!",
	NULL
};

static const char *ZH_CN_wipe_data_menu[] = {
	"否",
	"是 -- 擦除所有用户数据",
	NULL
};

static const char *ZH_CN_wipe_cache_header[] = {
	"是否擦除缓存分区?",
	"  注意：擦除之后，分区数据不能复原!",
	NULL
};

static const char *ZH_CN_wipe_cache_menu[] = {
	"否",
	"是 -- 擦除缓存分区数据",
	NULL
};

static const char *ZH_CN_updater_header[] = {
	"选择升级包:",
	NULL
};

static const char *ZH_CN_updater_menu[] = {
	NULL
};

static const char *JA_main_header[] = {
	"ボリュームダウン/ボリュームアップでハイライト部分に移行する",
	"電源キーで選択する.",
	NULL
};

static const char *JA_main_menu[] = {
	"システムをリブートする",
	"ブートローダーへリブートする",
	"ADB からアップグレードする",
	"sdカードからアップグレードする",
	"データ削除/工場出荷状態に初期化",
	"キャッシュパーティションを削除する",
	"マウント /システム",
	"ビューのリカバリー·ログ",
	"グラフィックテストを実行する",
	"シャットダウン",
	"⇦",
	NULL
};

static const char *JA_wipe_data_header[] = {
	"ユーザーデータを消去しますか?",
	"  この操作は撤回することができない.",
	NULL
};

static const char *JA_wipe_data_menu[] = {
	"いいえ",
	"はい--すべてのユーザーデータを消去します",
	NULL
};

static const char *JA_wipe_cache_header[] = {
	"キャッシュを消去しますか?",
	"  この操作は撤回することができない.",
	NULL
};

static const char *JA_wipe_cache_menu[] = {
	"いいえ",
	"はい--キャッシュを消去します",
	NULL
};

static const char *JA_updater_header[] = {
	"パッケージを選択し、インストールしてください：",
	NULL
};

static const char *JA_updater_menu[] = {
	NULL
};

static const char *DE_main_header[] = {
	"Lautstärketaste nach oben/unten drücken:\nMarkierung verschieben",
	"Zur Auswahl Eingabetaste drücken",
	NULL
};

static const char *DE_main_menu[] = {
	"System jetzt neustarten",
	"Neustart mit Bootloader",
	"Über ADB aktualisieren",
	"Update von SD-Karte installieren",
	"Daten löschen/Standardeinstellungen wiederherstellen",
	"Cache-Partition löschen",
	"/system mounten",
	"Wiederherstellungsprotokolle azeigen",
	"Graphik-Test ausführen",
	"Ausschalten",
	"⇦",
	NULL
};

static const char *DE_wipe_data_header[] = {
	"Alle Nutzerdaten löschen?",
	"DIESE AKTION KANN NICHT RÜCKGÄNGIG GEMACHT WERDEN.",
	NULL
};

static const char *DE_wipe_data_menu[] = {
	"Nein",
	"Ja - alle Benutzerdaten löschen",
	NULL
};

static const char *DE_wipe_cache_header[] = {
	"Cache löschen?",
	" DIESE AKTION KANN NICHT RÜCKGÄNGIG GEMACHT WERDEN.",
	NULL
};

static const char *DE_wipe_cache_menu[] = {
	"Nein",
	"Ja - Cache löschen",
	NULL
};

static const char *DE_updater_header[] = {
	"Zu installierendes Paket auswählen:",
	NULL
};

static const char *DE_updater_menu[] = {
	NULL
};

static const char *ES_main_header[] = {
	"Volumen arriba/abajo para mover la selección",
	"Pulse Intro para seleccionar",
	NULL
};

static const char *ES_main_menu[] = {
	"Reiniciar el sistema ahora",
	"Reinicio a modo bootloader",
	"Aplicar actualización desde ADB",
	"Aplicar actualización desde la tarjeta SD",
	"Borrar datos/reiniciar valores de fábrica",
	"Borrar partición de la caché",
	"montar /system",
	"Ver los registros de recuperación",
	"Ejecutar test de gráficos",
	"Apagar",
	"⇦",
	NULL
};

static const char *ES_wipe_data_header[] = {
	"¿Borrar todos los datos de usuario?",
	"  ESTA OPERACIÓN NO SE PUEDE DESHACER.",
	NULL
};

static const char *ES_wipe_data_menu[] = {
	"No",
	"Sí -- eliminar todos los datos del usuario.",
	NULL
};

static const char *ES_wipe_cache_header[] = {
	"¿Borrar el caché?",
	"  ESTA OPERACIÓN NO SE PUEDE DESHACER.",
	NULL
};

static const char *ES_wipe_cache_menu[] = {
	"No",
	"Sí",
	NULL
};

static const char *ES_updater_header[] = {
	"Elija el paquete para instalar:",
	NULL
};

static const char *ES_updater_menu[] = {
	NULL
};

static const char *FR_main_header[] = {
	"Volume haut/bas ppur bouger la sélection",
	"Entrée pour sélectionner",
	NULL
};

static const char *FR_main_menu[] = {
	"Redémarrer le système maintenant",
	"Redémarrer en mode bootloader",
	"Mettre à jour depuis ADB",
	"Appliquer la mise à jour à partir de la sdcard",
	"Effacer les données/Restaurer les valeurs d'usine",
	"Effacer la partition cache",
	"monter /system",
	"Afficher les journaux de récupération",
	"Executer tests graphiques",
	"Eteindre",
	"⇦",
	NULL
};

static const char *FR_wipe_data_header[] = {
	"Effacer toutes les données utilisateur?",
	"  ANNULATION IMPOSSIBLE.",
	NULL
};

static const char *FR_wipe_data_menu[] = {
	"Non",
	"Oui -- effacer toutes les données\n  utilisateur",
	NULL
};

static const char *FR_wipe_cache_header[] = {
	"Effacer le cache?",
	"  ANNULATION IMPOSSIBLE.",
	NULL
};

static const char *FR_wipe_cache_menu[] = {
	"Non",
	"Oui -- effacer le cache",
	NULL
};

static const char *FR_updater_header[] = {
	"Choisissez un package à installer:",
	NULL
};

static const char *FR_updater_menu[] = {
	NULL
};

static const char *PT_main_header[] = {
	"Aumentar/Diminuir volume para mover a selecção",
	"Carregue no botão Enter para seleccionar",
	NULL
};

static const char *PT_main_menu[] = {
	"reiniciar o sistema agora",
	"reiniciar para carregador de arranque",
	"aplicar actualização a partir de ADB",
	"aplicar actualização do sdcard",
	"apagar dados/reposição das configurações de fábrica",
	"apagar dados da partição da cache",
	"instalar /system",
	"logs de recuperação vista",
	"Executar teste de gráficos",
	"Desativar",
	"⇦",
	NULL
};

static const char *PT_wipe_data_header[] = {
	"Apagar todos os dados do utilizador?",
	"  ESTA ACÇÃO NÃO PODE SER ANULADA.",
	NULL
};

static const char *PT_wipe_data_menu[] = {
	"Não",
	"Sim -- eliminar todos os dados do utilizador",
	NULL
};

static const char *PT_wipe_cache_header[] = {
	"Apagar cache?",
	"  ESTA ACÇÃO NÃO PODE SER ANULADA.",
	NULL
};

static const char *PT_wipe_cache_menu[] = {
	"Não",
	"Sim -- eliminar cache",
	NULL
};

static const char *PT_updater_header[] = {
	"Seleccione um pacote para instalar:",
	NULL
};

static const char *PT_updater_menu[] = {
	NULL
};

static const char *RU_main_header[] = {
	"Используйте клавишу громкости для выделения",
	"Нажмите “Ввод” для выбора",
	NULL
};

static const char *RU_main_menu[] = {
	"перезагрузить систему",
	"перезагрузить в режим загрузчика",
	"применить обновление от ADB",
	"применить обновление с sd-карты",
	"стереть данные/восстановить заводские установки",
	"стереть раздел кэша",
	"подключить системный раздел /system",
	"журналы восстановления зрения",
	"Запустить графический тест",
	"Выключить",
	"⇦",
	NULL
};

static const char *RU_wipe_data_header[] = {
	"Удалить все пользовательские данные?",
	"  ЭТО ДЕЙСТВИЕ НЕВОЗМОЖНО ОТМЕНИТЬ.",
	NULL
};

static const char *RU_wipe_data_menu[] = {
	"Нет",
	"Да -- удалить все данные пользователя",
	NULL
};

static const char *RU_wipe_cache_header[] = {
	"Очистить кэш?",
	"  ЭТО ДЕЙСТВИЕ НЕВОЗМОЖНО ОТМЕНИТЬ.",
	NULL
};

static const char *RU_wipe_cache_menu[] = {
	"Нет",
	"Да -- Очистить кэш",
	NULL
};

static const char *RU_updater_header[] = {
	"Выбор программы для установки:",
	NULL
};

static const char *RU_updater_menu[] = {
	NULL
};

const struct ml_strings g_ml_str[] = {
{
	.lang = EN,
	.name = "main",
	.header = EN_main_header,
	.menu = EN_main_menu,
},
{
	.lang = EN,
	.name = "wipe_data",
	.header = EN_wipe_data_header,
	.menu = EN_wipe_data_menu,
},
{
	.lang = EN,
	.name = "wipe_cache",
	.header = EN_wipe_cache_header,
	.menu = EN_wipe_cache_menu,
},
{
	.lang = EN,
	.name = "updater",
	.header = EN_updater_header,
	.menu = EN_updater_menu,
},
{
	.lang = ZH_CN,
	.name = "main",
	.header = ZH_CN_main_header,
	.menu = ZH_CN_main_menu,
},
{
	.lang = ZH_CN,
	.name = "wipe_data",
	.header = ZH_CN_wipe_data_header,
	.menu = ZH_CN_wipe_data_menu,
},
{
	.lang = ZH_CN,
	.name = "wipe_cache",
	.header = ZH_CN_wipe_cache_header,
	.menu = ZH_CN_wipe_cache_menu,
},
{
	.lang = ZH_CN,
	.name = "updater",
	.header = ZH_CN_updater_header,
	.menu = ZH_CN_updater_menu,
},
{
	.lang = JA,
	.name = "main",
	.header = JA_main_header,
	.menu = JA_main_menu,
},
{
	.lang = JA,
	.name = "wipe_data",
	.header = JA_wipe_data_header,
	.menu = JA_wipe_data_menu,
},
{
	.lang = JA,
	.name = "wipe_cache",
	.header = JA_wipe_cache_header,
	.menu = JA_wipe_cache_menu,
},
{
	.lang = JA,
	.name = "updater",
	.header = JA_updater_header,
	.menu = JA_updater_menu,
},
{
	.lang = DE,
	.name = "main",
	.header = DE_main_header,
	.menu = DE_main_menu,
},
{
	.lang = DE,
	.name = "wipe_data",
	.header = DE_wipe_data_header,
	.menu = DE_wipe_data_menu,
},
{
	.lang = DE,
	.name = "wipe_cache",
	.header = DE_wipe_cache_header,
	.menu = DE_wipe_cache_menu,
},
{
	.lang = DE,
	.name = "updater",
	.header = DE_updater_header,
	.menu = DE_updater_menu,
},
{
	.lang = ES,
	.name = "main",
	.header = ES_main_header,
	.menu = ES_main_menu,
},
{
	.lang = ES,
	.name = "wipe_data",
	.header = ES_wipe_data_header,
	.menu = ES_wipe_data_menu,
},
{
	.lang = ES,
	.name = "wipe_cache",
	.header = ES_wipe_cache_header,
	.menu = ES_wipe_cache_menu,
},
{
	.lang = ES,
	.name = "updater",
	.header = ES_updater_header,
	.menu = ES_updater_menu,
},
{
	.lang = FR,
	.name = "main",
	.header = FR_main_header,
	.menu = FR_main_menu,
},
{
	.lang = FR,
	.name = "wipe_data",
	.header = FR_wipe_data_header,
	.menu = FR_wipe_data_menu,
},
{
	.lang = FR,
	.name = "wipe_cache",
	.header = FR_wipe_cache_header,
	.menu = FR_wipe_cache_menu,
},
{
	.lang = FR,
	.name = "updater",
	.header = FR_updater_header,
	.menu = FR_updater_menu,
},
{
	.lang = PT,
	.name = "main",
	.header = PT_main_header,
	.menu = PT_main_menu,
},
{
	.lang = PT,
	.name = "wipe_data",
	.header = PT_wipe_data_header,
	.menu = PT_wipe_data_menu,
},
{
	.lang = PT,
	.name = "wipe_cache",
	.header = PT_wipe_cache_header,
	.menu = PT_wipe_cache_menu,
},
{
	.lang = PT,
	.name = "updater",
	.header = PT_updater_header,
	.menu = PT_updater_menu,
},
{
	.lang = RU,
	.name = "main",
	.header = RU_main_header,
	.menu = RU_main_menu,
},
{
	.lang = RU,
	.name = "wipe_data",
	.header = RU_wipe_data_header,
	.menu = RU_wipe_data_menu,
},
{
	.lang = RU,
	.name = "wipe_cache",
	.header = RU_wipe_cache_header,
	.menu = RU_wipe_cache_menu,
},
{
	.lang = RU,
	.name = "updater",
	.header = RU_updater_header,
	.menu = RU_updater_menu,
},

};

static const int supported_lang[] = {
	EN,
	ZH_CN,
	JA,
	DE,
	ES,
	FR,
	PT,
	RU,

};
static const int supported_lang_cnt = (sizeof(supported_lang)/(sizeof(int)));

char *g_ml_str_hint[] = {
	"English",
	"中文",
	"日本語",
	"Deutsch",
	"Español",
	"Français",
	"Português",
	"Русский",

	NULL
};

