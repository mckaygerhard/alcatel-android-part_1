#!/bin/bash

if( test $# -le 0 ); then
  echo "please tell me the mbn list file path."
  echo "For example ./sign_mbn.sh  MBN_LIST_TO_SIGN.txt"
  exit 255
fi

    if [ -f "$1" ]; then
        cat "$1" | while read -r line
        do
            echo "mbn $line"
            ./st.sh ../ msm8996 common $line
        if test $? -ne 0 ; then
            echo "ERROR: efuse sign mbn $line error"
            exit 1
        fi
        done
    fi
