import os
import sys
import struct

def main():
    if len(sys.argv) < 2:
      raise RuntimeError, "Please input the file path"
    try:
       fp = open(sys.argv[1], 'rb')
    except:
       raise RuntimeError, "The file could not be opened: " + sys.argv[1]

    data = fp.read(4)
    elfmagic, = struct.unpack("I", data)
    if elfmagic != 0x464C457F:
      raise RuntimeError, "%s Not a ELF file" %(sys.argv[1])
    print sys.argv[1] + " is a ELF file"

if __name__ == "__main__":
  main()
