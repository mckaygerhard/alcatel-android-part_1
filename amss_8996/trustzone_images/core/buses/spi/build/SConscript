#===============================================================================
#
# GENERAL DESCRIPTION
#    Public build script for SPI BUS driver.
#
# Copyright (c) 2009-2014,2016 by QUALCOMM, Incorporated.
# All Rights Reserved.
# Qualcomm Confidential and Proprietary
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.tz/1.0.1/buses/spi/build/SConscript#2 $
#  $DateTime: 2016/06/17 15:11:15 $
#  $Author: pwbldsvc $
#  $Change: 10727967 $
#                      EDIT HISTORY FOR FILE
#                      
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 04/04/16   rc      To compile the xml files into devcfg binary.
# 08/13/14   ms      Compile SPI by default
# 05/30/14   ms      Added 8996 support.
# 06/12/12   ddk     Added requirements to compile for specific images.
# 06/03/12   ddk     Added 9625 support.
# 04/16/12   ddk     Added updates for dev config settings.
# 03/27/12   ddk     Added path for hwengines.
# 02/09/12   ag      Fixed the location where the object files are built.
# 01/21/12   ag      Initial release
#
#===============================================================================
Import('env')
#-------------------------------------------------------------------------------
# Load sub scripts
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()


#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
env = env.Clone()
#print env
# Additional defines
env.Append(CPPDEFINES = ["FEATURE_LIBRARY_ONLY"])   

SRCPATH = "../src"
IMAGES = []

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

CBSP_APIS = []
SPI_CONFIG_XML = []

#-------------------------------------------------------------------------------
# Publish Private APIs
#-------------------------------------------------------------------------------
env.PublishPrivateApi('BUSES_SPI_DEVICE', [
   '${INC_ROOT}/core/buses/spi/hw',
   '${INC_ROOT}/core/buses/spi/inc',
   '${INC_ROOT}/core/buses/spi/src/hal/inc',
   '${INC_ROOT}/core/buses/spi/src/logs/inc/',
   '${INC_ROOT}/core/buses/spi/src/device/inc',
   '${INC_ROOT}/core/buses/qup/hw',
   '${INC_ROOT}/core/buses/qup/inc',
   '${INC_ROOT}/core/buses/qup/src/hal/inc',   
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_APIS += [
   'BUSES',
   'DAL',
   'HAL',
   'SYSTEMDRIVERS',
   'KERNEL',   
   'SERVICES',   
   'HWENGINES',
   'SECUREMSM',
]

env.RequirePublicApi(CBSP_APIS)
env.RequireRestrictedApi(CBSP_APIS)
if 'USES_DEVCFG' in env:
   if env['MSM_ID'] in ['8974']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8974.xml')
   elif env['MSM_ID'] in ['8x26']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8626.xml')
   elif env['MSM_ID'] in ['8084']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8084.xml')
   elif env['MSM_ID'] in ['8962']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8962.xml')
   elif env['MSM_ID'] in ['8x10']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8x10.xml')
   elif env['MSM_ID'] in ['8092']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8084.xml')
   elif env['MSM_ID'] in ['8916']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8916.xml')
   elif env['MSM_ID'] in ['8909']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8916.xml')       
   elif env['MSM_ID'] in ['8936']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8936.xml')
   elif env['MSM_ID'] in ['8939']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8936.xml')      
   elif env['MSM_ID'] in ['8994']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8994.xml')     
   elif env['MSM_ID'] in ['8996']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8996.xml')       
   elif env['MSM_ID'] in ['8956']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8956.xml')       
   elif env['MSM_ID'] in ['8952']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8952.xml')       
   elif env['MSM_ID'] in ['8953']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8953.xml')
   elif env['MSM_ID'] in ['8937']:
      env.Replace(SPI_CONFIG_XML = 'spi_props_tz_8937.xml')	  
   else:
      Return();

   DEVCFG_IMG = ['DAL_DEVCFG_OEM_QSEE_IMG']
   env.AddDevCfgInfo(DEVCFG_IMG, 
   {
      'devcfg_xml'    : ['${BUILD_ROOT}/core/buses/spi/config/${SPI_CONFIG_XML}',]
   })

if env.has_key('ADSP_PROC'):
      IMAGES = ['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE']
elif env.has_key('ADSP_PROC'):
      IMAGES = ['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE']
elif env.has_key('MODEM_PROC'):
      IMAGES = ['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE']
elif env.has_key('TZOS_IMAGE'):
      IMAGES = ['TZOS_IMAGE']
else:
   Return();    
#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
SPI_DEVICE_GLOB_FILES = env.GlobFiles('../src/*/*.c', posix=True)

#GLOB returns the relative path name, it needs to replaced with correct build location
SPI_DEVICE_SOURCES = [path.replace(SRCPATH, '${BUILDPATH}') for path in SPI_DEVICE_GLOB_FILES]

#-------------------------------------------------------------------------------
# Add Libraries to image
# env.AddLibrary is a new API, only if the IMAGES is valid in the build env
# the objects will built and added to the image.
#-------------------------------------------------------------------------------
env.AddBinaryLibrary(IMAGES, '${BUILDPATH}/SpiDevice', SPI_DEVICE_SOURCES)
