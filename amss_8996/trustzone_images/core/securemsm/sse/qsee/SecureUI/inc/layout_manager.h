#ifndef LAYOUT_MGR_H_
#define LAYOUT_MGR_H_

/** @file layout_manager.h
  @brief
  This file contains the interfaces to the Secure UI Layout Manager.
*/

/*===========================================================================
 Copyright (c) 2013-2016 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Confidential and Proprietary - Qualcomm Technologies, Inc.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/22/16   sn      Added touched object structure.
04/10/16   sn      Refactoring of the API to allow custom layout usage.
12/10/15   kl      (Tech Pubs) Comment updates for 80-NJ546-1 Rev. J.
10/27/15   gs	   Add rendering of the layouts bg image to a secure buffer
09/06/15   ls      Add documentation to all the external functions
03/09/15   sn      Changed the API to get the stride in each rendering.
08/03/15   ls      Add one-time per device keypad randomization.
11/10/13   sn      Initial Version.

===========================================================================*/
#include "SecureTouchLayout.h"

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/** @addtogroup layout_manager
@{ */

/** Layout manager return codes. */
typedef enum {
	LAYOUT_MGR_SUCCESS                        = 0,
	/**< Success. */
	LAYOUT_MGR_GENERAL_ERROR                  = -1,
	/**< General error.  */
	LAYOUT_MGR_BAD_LAYOUT                     = -2,
	/**< Received an invalid layout structure. */
	LAYOUT_MGR_GOT_NULL_INPUT                 = -3,
	/**< Received a null parameter. */
	LAYOUT_MGR_UNSUPPORTED                    = -4,
	/**< Operation is not supported with the specified parameters. */
	LAYOUT_MGR_OBJECT_NOT_FOUND               = -5,
	/**< The specified object does not exist in the layout.  */
	LAYOUT_MGR_TEXT_EXCEED_BUFFER             = -6,
	/**< Received text exceeds the given buffer size. */
	LAYOUT_MGR_UNEXPECTED_TIMEOUT             = -7,
	/**< An unexpected timeout event occurred.  */
	LAYOUT_MGR_BUTTON_BUSY                    = -8,
	/**< The button is currently in the pressed state, so it cannot be disabled. */
	LAYOUT_MGR_FAILED_GEN_RAND                = -9,
	/**< Random number generation failed. */
	LAYOUT_MGR_INVALID_UTF8_STR               = -10,
	/**< Received an invalid UTF8 string. */
	LAYOUT_MGR_TEXT_EXCEED_OBJECT             = -11,
	/**< Text exceeds the object size limitations.  */
	LAYOUT_MGR_UNCHANGED                      = -12,
	/**< No change occurred in the layout. */
	LAYOUT_MGR_LAYOUT_NOT_LOADED              = -13,
	LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED         = -14,
	LAYOUT_MGR_FONT_MISSING_CHAR              = -15,
	LAYOUT_MGR_DYNAMIC_FONT_ERROR             = -16,
	LAYOUT_MGR_MEM_ALLOC_ERROR                = -17,
	LAYOUT_MGR_IMAGE_EXCEED_OBJECT            = -18,
	LAYOUT_MGR_INVALID_IMAGE                  = -19,

	LAYOUT_MGR_ENTER_PRESSED                  = 1,
	/**< Function button 'enter' was pressed. */
	LAYOUT_MGR_CANCEL_PRESSED                 = 2,
	/**< Function button 'cancel' was pressed. */
	LAYOUT_MGR_F1_PRESSED                     = 3,
	/**< Function button F1 was pressed. */
	LAYOUT_MGR_F2_PRESSED                     = 4,
	/**< Function button F2 was pressed. */
	LAYOUT_MGR_F3_PRESSED                     = 5,
	/**< Function button F3 was pressed  */
    /** @cond */
	LAYOUT_MGR_ERR_SIZE                       = 0x7FFFFFFF
	/**< @endcond */
} layout_mgr_err_t;

typedef enum {
	LAYOUT_MGR_EVENT_NONE                      = 0,
	LAYOUT_MGR_EVENT_UP                        = 1,
	LAYOUT_MGR_EVENT_DOWN                      = 2,
	LAYOUT_MGR_EVENT_MOVE                      = 3,
	LAYOUT_MGR_EVENT_SIZE                      = 0x7FFFFFFF
} layout_mgr_event_t;

typedef struct{
	const char* objectName;
	layout_mgr_event_t eventType;
} layout_mgr_touched_obj_t;


/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/


/**
   @brief
   External function that loads a layout struct and screen properties and checks its validity.
   Also, it sets some initial values.
   This function must be called before using any other API in this header.

   @param[in] layoutPage -  The TUI screen layout which represents the appearance of the screen statically.
   		             This layout  will be validated and loaded on the screen.
   @param[in] screenHeight - The device screen height in pixels.
   @param[in] screenWidth - The device screen width in pixels.

   @return
   	LAYOUT_MGR_SUCCESS
	LAYOUT_MGR_GOT_NULL_INPUT
	LAYOUT_MGR_BAD_LAYOUT
*/

layout_mgr_err_t layout_mgr_load_layout(LayoutPage_t* layoutPage, uint32_t screenHeight, uint32_t screenWidth);

/*****************************************************************************************************************/

/**
   @brief
   External function for setting the buffer of the label of some input object in the layout.
   Notice that this function doesn't set the text itself (curr_len is zeroed).

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[in] buffer - The buffer to be set for this object
   @param[in] len - The length of the buffer in bytes
   
   @return 
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
         LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.

*/
layout_mgr_err_t layout_mgr_set_buffer_for_input(const char* objectName, uint8_t* buffer, uint32_t len);

/*****************************************************************************************************************/

/**
   @brief
   External function for getting the buffer of an input object.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] buffer - The buffer that has been set for this object
   @param[out] len - The length of the buffer
   @return
         LAYOUT_MGR_SUCCESS
         LAYOUT_MGR_GOT_NULL_INPUT
         LAYOUT_MGR_OBJECT_NOT_FOUND
         LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
         LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_buffer_of_input(const char* objectName, const uint8_t** buffer, uint32_t* len);

/*****************************************************************************************************************/

/**
   @brief
   External function for setting the show flag of some object in the layout.
         
   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[in] show - show flag to be set
   
   @return 
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_set_show_flag(const char* objectName, uint32_t show);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the show flag value of some object.

   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[out] show - show flag.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
		LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_show_flag(const char* objectName, uint32_t* show);

/*****************************************************************************************************************/


/**
   @brief
   External function for setting focused input object in the layout.

   @param[in] objectName - Object name (as unique identifier in the layout)

   @return 
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_set_focus_input(const char* objectName);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting the image buffer and the alignment of some image object in the layout.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[in] imageBuffer - Buffer that contains the PNG representation of the required image
   @param[in] vAlign - Vertical alignment
   @param[in] hAlign - Horizontal alignment 

   @return 
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_IMAGE_EXCEED_OBJECT
        LAYOUT_MGR_INVALID_IMAGE - any unsupported format/corruption of the image
        LAYOUT_MGR_UNSUPPORTED - unsupported parameter values
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of image type
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_set_image(const char* objectName, const uint8_t* imageBuffer, VerticalAlignment_t vAlign,HorizontalAlignment_t hAlign);

/*****************************************************************************************************************/

/**
   @brief
   External function for setting the active keyboard in the layout.
   
   @param[in] keyboardName - Keyboard name to be set to active

   @return 
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_set_active_keyboard(const char* keyboardName);

/*****************************************************************************************************************/

/**
   @brief
   External function for keyboard randomization in the layout.

   @param[in] keyboardName - Keyboard name to be randomized
   @param[in] isOneTime - set to TRUE if randomization is to be one-time per device lifetime
   @return 
         LAYOUT_MGR_SUCCESS
         LAYOUT_MGR_GOT_NULL_INPUT
         LAYOUT_MGR_OBJECT_NOT_FOUND
         LAYOUT_MGR_FAILED_GEN_RAND
         LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_randomize_keyboard(const char* keyboardName, uint8_t isOneTime);

/*****************************************************************************************************************/

/**
   @brief
   External function for setting the length of the text label of some input object in the layout,
   to be lower or equal to the given maximum length. If longer, the text will be truncated to this length.
   The maximum length is in characters.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[in] max_chars_len - The maximum length in characters.
   
   @return 
         LAYOUT_MGR_SUCCESS
         LAYOUT_MGR_GOT_NULL_INPUT
         LAYOUT_MGR_OBJECT_NOT_FOUND
         LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type
         LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_truncate_input_string(const char* objectName, uint32_t max_chars_len);

/*****************************************************************************************************************/

/**
   @brief
   External function for deleting the last character of the focused input object.
   If the focused input is empty, nothing should happen.

    @return
         LAYOUT_MGR_SUCCESS
         LAYOUT_MGR_OBJECT_NOT_FOUND - in case of no focused input.
         LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/

layout_mgr_err_t layout_mgr_delete_input_last_character(void);

/*****************************************************************************************************************/

/**
   @brief
   External function for setting the buffer of an input object to zero.

   @param[in] objectName - Object name (as unique identifier in the layout)

   @return 
         LAYOUT_MGR_SUCCESS
         LAYOUT_MGR_GOT_NULL_INPUT
         LAYOUT_MGR_OBJECT_NOT_FOUND
         LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
         LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_clear_input_buffer(const char* objectName);

/*****************************************************************************************************************/

/**
   @brief
   External function for setting the hidden character of an input object.
   This character will be displayed instead of the clear string characters in case of hidden display mode.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[in] hidden_char - The hidden character in utf8 format
   @return
         LAYOUT_MGR_SUCCESS
         LAYOUT_MGR_GOT_NULL_INPUT
         LAYOUT_MGR_OBJECT_NOT_FOUND
         LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
         LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_set_input_hidden_char(const char* objectName, utf8_char_t hidden_char);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the hidden character of an input object.
   This character will be displayed instead of the clear string characters in case of hidden display mode.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] hidden_char - The hidden character in utf8 format
   @return
         LAYOUT_MGR_SUCCESS
         LAYOUT_MGR_GOT_NULL_INPUT
         LAYOUT_MGR_OBJECT_NOT_FOUND
         LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
         LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_input_hidden_char(const char* objectName, utf8_char_t* hidden_char);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting the display mode of some input object in the layout.
      
   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[in] mode - The display mode to be set
   
   @return 
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_TEXT_EXCEED_OBJECT
*/
layout_mgr_err_t layout_mgr_set_input_display_mode(const char* objectName, LayoutInputDisplayMode_t mode);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the display mode of some input object in the layout.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] mode - The display mode that has been set

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_input_display_mode(const char* objectName, LayoutInputDisplayMode_t* mode);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the status of some input object in the layout - focused/not-focused.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] input_status - The current status

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_input_status(const char* objectName, LayoutInputStatus_t* input_status);

/*****************************************************************************************************************/
/**
   @brief
   External function for checking that some text string is not exceeding object size.

   @param[in] objectName - Object name (as unique identifier in the layout) to check the text with.
   @param[in] text - Buffer containing the utf-8 representation of the string to be checked.
   @param[in] len - length of the string to be checked, in bytes.
   @param[out] fitting_len - length of the substring that can fit the object, in bytes.
   @param[out] char_length - length of the substring that can fit the object, in characters.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_TEXT_EXCEED_OBJECT
        LAYOUT_MGR_FONT_MISSING_CHAR
        LAYOUT_MGR_GENERAL_ERROR
        LAYOUT_MGR_INVALID_UTF8_STR
        LAYOUT_MGR_DYNAMIC_FONT_ERROR
*/
layout_mgr_err_t layout_mgr_check_exceeding_text(const char* objectName, const uint8_t* text, uint32_t len, uint32_t* fitting_len, uint32_t* char_length);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting some text string to some object.

   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[in] text - Buffer containing the utf-8 representation of the string to be set.
   @param[in] len - length of the string to be set, in bytes.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_TEXT_EXCEED_OBJECT
        LAYOUT_MGR_FONT_MISSING_CHAR
        LAYOUT_MGR_GENERAL_ERROR
        LAYOUT_MGR_INVALID_UTF8_STR
        LAYOUT_MGR_DYNAMIC_FONT_ERROR
*/
layout_mgr_err_t layout_mgr_set_text(const char* objectName, const uint8_t* text, uint32_t len);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the text string of some object.

   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[out] text - Buffer containing the utf-8 representation of the string.
   @param[out] len - length of the string, in bytes.
   @param[out] char_length - length of the substring that can fit the object, in characters.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_text(const char* objectName, const uint8_t** text, uint32_t* len, uint32_t* char_length);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting the text font of some object.
   This function is not supported for input type objects.

   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[in] font_path - absolute path to a font file.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_TEXT_EXCEED_OBJECT
        LAYOUT_MGR_FONT_MISSING_CHAR
        LAYOUT_MGR_DYNAMIC_FONT_ERROR
*/
layout_mgr_err_t layout_mgr_set_text_font(const char* objectName, const char* font_path);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the path for the dynamic font of some object.
   This function should return NULL for input type objects.

   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[out] font_path - font path ascii string.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_text_font(const char* objectName, const char** font_path);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting the text color of some object in the layout.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[in] color - The color to be set

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_set_text_color(const char* objectName, Color_t color);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the text color of some object in the layout.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] color - The color that has been set

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_text_color(const char* objectName, Color_t* color);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting the text alignment, horizontal and vertical, of some object in the layout.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[in] v_align - vertical alignment
   @param[in] h_align - horizontal alignment

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_set_text_alignment(const char* objectName, VerticalAlignment_t v_align, HorizontalAlignment_t h_align);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the text alignment, horizontal and vertical, of some object in the layout.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] v_align - vertical alignment
   @param[out] h_align - horizontal alignment

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_text_alignment(const char* objectName, VerticalAlignment_t* v_align, HorizontalAlignment_t* h_align);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting the flag to allow line breaking of text of some object.


   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[in] allow_line_breaking - TRUE - allow; False - not allow.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_TEXT_EXCEED_OBJECT
*/
layout_mgr_err_t layout_mgr_set_text_line_breaking(const char* objectName, uint32_t allow_line_breaking);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the flag to allow line breaking of text of some object.


   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[out] allow_line_breaking - allow/not allow.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_text_line_breaking(const char* objectName, uint32_t* allow_line_breaking);


/*****************************************************************************************************************/
/**
   @brief
   External function for checking that some text string is not exceeding object size, for hint label of input object.

   @param[in] objectName - Object name (as unique identifier in the layout) to check the text with.
   @param[in] text - Buffer containing the utf-8 representation of the string to be checked.
   @param[in] len - length of the string to be checked, in bytes.
   @param[out] fitting_len - length of the substring that can fit the object, in bytes.
   @param[out] char_length - length of the substring that can fit the object, in characters.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_TEXT_EXCEED_OBJECT
        LAYOUT_MGR_FONT_MISSING_CHAR
        LAYOUT_MGR_GENERAL_ERROR
        LAYOUT_MGR_INVALID_UTF8_STR
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
        LAYOUT_MGR_DYNAMIC_FONT_ERROR
*/
layout_mgr_err_t layout_mgr_check_exceeding_text_hint(const char* objectName, const uint8_t* text, uint32_t len, uint32_t* fitting_len, uint32_t* char_length);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting some text string, for hint label of input object.

   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[in] text - Buffer containing the utf-8 representation of the string to be set.
   @param[in] len - length of the string to be set, in bytes.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_TEXT_EXCEED_OBJECT
        LAYOUT_MGR_FONT_MISSING_CHAR
        LAYOUT_MGR_GENERAL_ERROR
        LAYOUT_MGR_INVALID_UTF8_STR
        LAYOUT_MGR_DYNAMIC_FONT_ERROR
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
*/
layout_mgr_err_t layout_mgr_set_text_hint(const char* objectName, const uint8_t* text, uint32_t len);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the text string, for hint label of input object.

   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[out] text - Buffer containing the utf-8 representation of the string.
   @param[out] len - length of the string, in bytes.
   @param[out] char_length - length of the substring that can fit the object, in characters.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
*/
layout_mgr_err_t layout_mgr_get_text_hint(const char* objectName, const uint8_t** text, uint32_t* len, uint32_t* char_length);


/*****************************************************************************************************************/
/**
   @brief
   External function for setting the text font, for hint label of input object.
   This function is not supported for input type objects.

   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[in] font_path - font path ascii string.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_TEXT_EXCEED_OBJECT
        LAYOUT_MGR_FONT_MISSING_CHAR
        LAYOUT_MGR_DYNAMIC_FONT_ERROR
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
*/
layout_mgr_err_t layout_mgr_set_text_font_hint(const char* objectName, const char* font_path);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the path for the dynamic font, for hint label of input object.
   This function should return NULL for input type objects.

   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[out] font_path - font path ascii string.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
*/
layout_mgr_err_t layout_mgr_get_text_font_hint(const char* objectName, const char** font_path);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting the text color, for hint label of input object.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[in] color - The color to be set

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
*/
layout_mgr_err_t layout_mgr_set_text_color_hint(const char* objectName, Color_t color);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the text color, for hint label of input object.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] color - The color that has been set

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
*/
layout_mgr_err_t layout_mgr_get_text_color_hint(const char* objectName, Color_t* color);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting the text alignment, horizontal and vertical, for hint label of input object.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[in] v_align - vertical alignment
   @param[in] h_align - horizontal alignment

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
*/
layout_mgr_err_t layout_mgr_set_text_alignment_hint(const char* objectName, VerticalAlignment_t v_align, HorizontalAlignment_t h_align);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the text alignment, horizontal and vertical, for hint label of input object.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] v_align - vertical alignment
   @param[out] h_align - horizontal alignment

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
*/
layout_mgr_err_t layout_mgr_get_text_alignment_hint(const char* objectName, VerticalAlignment_t* v_align, HorizontalAlignment_t* h_align);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting the flag to allow line breaking of text, for hint label of input object.


   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[in] allow_line_breaking - allow/not allow.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_TEXT_EXCEED_OBJECT
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
*/
layout_mgr_err_t layout_mgr_set_text_line_breaking_hint(const char* objectName, uint32_t allow_line_breaking);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the flag to allow line breaking of text, for hint label of input object.

   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[out] allow_line_breaking - allow/not allow.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_UNSUPPORTED
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of input type.
*/
layout_mgr_err_t layout_mgr_get_text_line_breaking_hint(const char* objectName, uint32_t* allow_line_breaking);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting layout flavor - pin/login/msg/custom.

   @param[out] flavor - layout flavor.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_layout_flavor(LayoutFlavor_t* flavor);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting layout size in pixels.

   @param[out] layout_height - layout height in pixels.
   @param[out] layout_width - layout width in pixels.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_layout_size(uint32_t* layout_height, uint32_t* layout_width);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the currently displayed keyboard.

   @param[out] keyboardName - keyboard name.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_active_keyboard(const char** keyboardName);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the currently selected input object.

   @param[out] focused_input_name - input object name.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_focus_input(const char** focused_input_name);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the size in pixels of some object.

   @param[in] objectName - Object name (as unique identifier in the layout).
   @param[out] object_height - object height in pixels.
   @param[out] object_width - object width in pixels.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
		LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_object_size(const char* objectName, uint32_t* object_height, uint32_t* object_width);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the type of some button object - data/function.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] type - button type

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of button type.
*/
layout_mgr_err_t layout_mgr_get_button_type(const char* objectName, LayoutButtonType_t* type);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the function of some function button object.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] function - button function

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of button type.
        LAYOUT_MGR_UNSUPPORTED - button is not of function type
*/
layout_mgr_err_t layout_mgr_get_button_function(const char* objectName, LayoutButtonFunctionType_t* function);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the data of some data button object.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] data - button data utf8 encoded


   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of button type.
        LAYOUT_MGR_UNSUPPORTED - button is not of data type
*/
layout_mgr_err_t layout_mgr_get_button_data(const char* objectName, utf8_char_t* data);

/*****************************************************************************************************************/
/**
   @brief
   External function for setting the state of some button object - released/pressed/disabled.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[in] state - button state

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of button type.
        LAYOUT_MGR_UNSUPPORTED
*/
layout_mgr_err_t layout_mgr_set_button_state(const char* objectName, LayoutButtonStatus_t state);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the state of some button object - released/pressed/disabled.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] state - button state

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of button type.
*/
layout_mgr_err_t layout_mgr_get_button_state(const char* objectName, LayoutButtonStatus_t* state);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the image buffer, size , and the alignment of some image object in the layout.

   @param[in] objectName - Object name (as unique identifier in the layout)
   @param[out] imageBuffer - Buffer that contains the PNG representation of the required image
   @param[out] vAlign - Vertical alignment
   @param[out] hAlign - Horizontal alignment
   @param[out] image_height - image height in pixels.
   @param[out] image_width - image width in pixels.

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
        LAYOUT_MGR_OBJECT_NOT_FOUND
        LAYOUT_MGR_OBJ_TYPE_NOT_SUPPORTED - object is not of image type
        LAYOUT_MGR_LAYOUT_NOT_LOADED - layout_mgr_load_layout must be called before calling this function.
*/
layout_mgr_err_t layout_mgr_get_image(const char* objectName, const uint8_t** image_buffer,
														VerticalAlignment_t* v_align,HorizontalAlignment_t* h_align,
														uint32_t* image_height, uint32_t* image_width);


/*****************************************************************************************************************/
/**
   @brief
   External function for setting the default timeout of the touch session (in milliseconds),
	i.e., the maximum time to wait for user input. 0 means return immediately, and -1 means no timeout (wait indefinitely).

   @param[in] timeout - Default timeout to be returned in layout_mgr_eval_event() in case the Layout manager
   		       itself does not request a different timeout for the operating the common behavior of the layout.
   		       (use NO_TIMEOUT if not applicable)

   @return
        LAYOUT_MGR_SUCCESS
*/
layout_mgr_err_t layout_mgr_set_default_timeout(int32_t timeout);

/*****************************************************************************************************************/
/**
   @brief
   External function for getting the default timeout of the touch session (in milliseconds),
	i.e., the maximum time to wait for user input. 0 means return immediately, and -1 means no timeout (wait indefinitely).

   @param[out] timeout - Default timeout to be returned in layout_mgr_eval_event() in case the Layout manager
   		       itself does not request a different timeout for the operating the common behavior of the layout.
   		       (use NO_TIMEOUT if not applicable)

   @return
        LAYOUT_MGR_SUCCESS
        LAYOUT_MGR_GOT_NULL_INPUT
*/
layout_mgr_err_t layout_mgr_get_default_timeout(int32_t* timeout);

/*****************************************************************************************************************/

/**
   @brief
   Cleanup of allocated buffers.
*/
void free_layout_buffers();

#endif /* LAYOUT_MGR_H_ */

