/********************************************************************
---------------------------------------------------------------------
Copyright (c) 2015 Qualcomm Technologies, Inc.
 All Rights Reserved. Qualcomm Technologies Proprietary and Confidential.
----------------------------------------------------------------------
*********************************************************************/

#ifndef __LAYOUT_H__
#define __LAYOUT_H__

#include "qsee_tui_dialogs.h"


LayoutPage_t* create_layout(LayoutFlavor_t layout_flavor);

LayoutPage_t* get_layout_by_name(const char * layout_name);


#endif
