#include <string.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>

#include "common.h"

#define LOG(fmt, ...) LOGD(fmt, ##__VA_ARGS__)
#define SEC_DUMP
#ifdef SEC_DUMP
void hexdump2(const char *hdr, const void *addr, uint32_t len)
{
    bool morelines = len > 16;
    uint32_t blocklen = 16;
    uint32_t offset = 0;
    uint32_t i = 0;
    unsigned char *p = (unsigned char *) addr;
    char buf[256];
    char *p_buf;

    if (hdr != NULL) {
       LOG("%s len=%d\n", hdr, len);
    }

    if (len == 0) {
        return;
    }

    buf[0] = '\0';
    p_buf = buf;
    do {
        if (len < 16) {
            blocklen = len;
        }

        p_buf += snprintf(p_buf, sizeof(buf)-strlen(buf), "%s%04x%s", "| ", offset, " | ");
        offset += 16;

        for (i = 0; i < blocklen; ++i) {
            p_buf += snprintf(p_buf, sizeof(buf)-strlen(buf), "%02x ", p[i]);
        }

        if (blocklen < 16 && morelines) {
            for (i = blocklen; i < 16; i++) {
                p_buf += snprintf(p_buf, sizeof(buf)-strlen(buf), "   ");
            }
        }

        p_buf += snprintf(p_buf, sizeof(buf)-strlen(buf), "| ");

        for (i = 0; i < blocklen; i++) {
            uint8_t c = p[i];
            if (c>=0x20 && c<0x7F) {
                if ( c == '%' )
                    p_buf += snprintf(p_buf, sizeof(buf)-strlen(buf), "%%");
                else
                    p_buf += snprintf(p_buf, sizeof(buf)-strlen(buf), "%c", c);
            } else {
                p_buf += snprintf(p_buf, sizeof(buf)-strlen(buf), ".");
            }
        }

        if (!morelines) {
            snprintf(p_buf, sizeof(buf)-strlen(buf), "\n");
            LOG("%s", buf);
            return;
        }

        if ((blocklen < 16) && morelines) {
            for (i = blocklen; i < 16; i++) {
                p_buf += snprintf(p_buf, sizeof(buf)-strlen(buf), " ");
            }
        }

        snprintf(p_buf, sizeof(buf)-strlen(buf), " |\n");
        LOG("%s", buf);
        buf[0] = '\0';
        p_buf = buf;
        len -= blocklen;
        p += blocklen;

    } while (len > 0);
}

#else
void hexdump2(const char *hdr, const void *addr, uint32_t len){}
#endif



