/*! @file vcsBase.h
*******************************************************************************
**  Synaptics Fingerprint SDK Base Definitions
**
**  This file defines the OS/Platform/Architecture specifics for the given
**  build target.
**
**  Copyright (c) 2006-2014 Synaptics Incorporated.  All rights reserved.
*/

#ifndef __vcsBase_h__
#define __vcsBase_h__

#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)
/* include standard integer data type definitions */
#include <stdint.h>
#endif

#if (defined(VCS_OS_LINUX) && !defined(__KERNEL__)) || defined(VCS_OS_MAC)
#  include <limits.h>
/* include standard integer data type definitions */
#  include <stdint.h>
#endif /* VCS_OS_LINUX */

#if defined(__APPLE__) && defined(__MACH__)
#  define VCS_OS_MACOSX
#endif /* __APPLE__ && __MACH__ */

#if defined(VCS_OS_WINDOWS)

#  include "vcsPushWarning.h"
#  ifndef WIN32_LEAN_AND_MEAN
#  define WIN32_LEAN_AND_MEAN          /* Exclude rarely-used stuff from Windows headers */
#  endif
#  include <windows.h>
#ifndef SKIP_TCHAR_FOR_LAB_WINDOWS
#  include <tchar.h>                   /* TCHAR and some _t routines */
#endif
#  include "vcsPopWarning.h"

/* warning C4152: '....' : nonstandard extension, function/data pointer conversion in expression */
#pragma warning(3:4152)

#elif defined (_PBA_)
#pragma data_seg( "_text" )
#pragma bss_seg( "_text" )
#pragma const_seg( "_text" )

#endif /* VCS_OS_WINDOWS */

/* include the feature definitions */
#if VCS_FEATURES_DEFINED
#include "vcsFeatures.h"
#endif

#endif /* __vcsBase_h__ */
