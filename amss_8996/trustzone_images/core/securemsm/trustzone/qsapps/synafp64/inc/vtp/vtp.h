/*! @file vtp.h
*******************************************************************************
**  VFM Transport Protocol Module Interface Definition.
**
**  Copyright (c) 2014 Synaptics Incorporated.  All rights reserved.
*/

#ifndef __vtp_h__
#define __vtp_h__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "vcsTypes.h"

/*!
*******************************************************************************
**  Open transport module and prepare transport handle for further session
**  establishment.
**
**  @param[out] phTp        Pointer to store opened transport module handle.
**  @param[in]  appId       Application ID, for which transport module should
**                          be opened. Should be 0 if only single application
**                          is supported by the transport.
**
**  @return                 0 if no error.
*/
extern vcsInt32_t VCS_API
vtpOpen(
    vcsHandle_t *   phTp,
    vcsUint32_t     appId);

/*!
*******************************************************************************
**  Close transport module. Session should be closed before closing transport
**  module.
**
**  @param[in]  hTp         Transport module handle to be closed.
*/
extern vcsInt32_t VCS_API
vtpClose(
    vcsHandle_t hTp);

/*!
*******************************************************************************
**  Establish session to communicate with remote side (partially can be a TEE).
**
**  @param[in]  hTp         Transport module handle which session to be
**                          established.
**
**  @return                 0 if no error.
*/
extern vcsInt32_t VCS_API
vtpSessionOpen(
    vcsHandle_t hTp);

/*!
*******************************************************************************
**  Close previously established session.
**
**  @param[in]  hTp         Transport module handle which session to be closed.
*/
extern vcsInt32_t VCS_API
vtpSessionClose(
    vcsHandle_t hTp);

/*!
*******************************************************************************
**  Make a remote call by specified transport handle. Session should be
**  established before this call.
**
**  @param[in]  hTp         Transport module handle.
**  @param[in]  pInput      Pointer to input data blob.
**  @param[out] pOutput     Pointer to output data blob.
**
**  @return                 0 if no error.
*/
extern vcsInt32_t VCS_API
vtpCall(
    vcsHandle_t                 hTp,
    const vcsConstBlobData_t *  pInput,
    vcsBlobData_t *             pOutput);

/*!
*******************************************************************************
**  Retrieve transport module properties by specified ID.
**
**  @param[in]  hTp         Transport module handle.
**  @param[in]  id          Property ID. One of VTP_PROP_ID_XXX
**  @param[out] pValue      Pointer where property value to be stored.
**
**  @return                 0 if no error.
*/

/** Transport property IDs (limits, configurations)
 */
/**< Limit for the entire data size which can be transferred by the transport
     module in a single call in either direction ('pInput' or 'pOutput' of
     vtpCall()).
     Expected 4-byte dword value returned in the memory buffer pointed to by
     pValue->pData.  The value should be set to 0xffff, if there are no limits.
 */
#define VTP_PROP_ID_TRANSFER_SIZE_LIMIT   ((vcsUint32_t) 1)

extern vcsInt32_t VCS_API
vtpPropertyGet(
    vcsHandle_t     hTp,
    vcsUint32_t     id,
    vcsBlobData_t * pValue);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __vtp_h__ */
