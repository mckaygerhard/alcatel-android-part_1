/** @addtogroup SECSPIDRV
 * @{
 * Basic QC TZ SPI Driver Wrapper functions.
 *
 * Copyright (c) 2013 Samsung Electronics Co., Ltd.
 */
 
extern char* pSFSRootPath;

typedef struct
{
 void* buf_addr;
 /**<buff address for read or write data*/
 uint32  buf_len; 
 /**<size in bytes*/
 uint32 total_bytes;
 /**<total bytes successfully transfered on SPI Bus*/
}qsee_wrapper_spi_transaction_info_t;

/**
 * Open the device, and perform some HW intialization \n
 *
 *
 * @return 0 on success, negative on failure
 */
extern int qsee_wrapper_spi_open(void);

/**
 * Reads data from SPI bus.
 *
 * @param[in] p_read_info Read buffer information
 *
 * @return 0 on success, negative on failure
 */
extern int qsee_wrapper_spi_read(
	qsee_wrapper_spi_transaction_info_t* p_read_info
);	

/**
 * Writes data on SPI bus.
 *
 * @param[in] p_write_info Write buffer information
 *
 * @return 0 on success, negative on failure
 */
extern int qsee_wrapper_spi_write(
	qsee_wrapper_spi_transaction_info_t* p_write_info
);

/**
 * Writes data on SPI bus.
 *
 * @param[in] p_write_info Write buffer information
 * @param[in] p_read_info Read buffer information
 *
 * @return 0 on success, negative on failure
 */
extern int qsee_wrapper_spi_full_duplex(
	qsee_wrapper_spi_transaction_info_t* p_write_info,
	qsee_wrapper_spi_transaction_info_t* p_read_info
);

/**
 * Close the client access to the spi device.
 *
 *
 *
 * @return 0 on success, negative on failure
 */

extern int qsee_wrapper_spi_close(void);
