/*
@file app_main.c
@brief App main entry point.
*/

#include <comdef.h>
//#include <rt_misc.h>
#include <string.h>
#include "qsee_log.h"
#include "qsee_heap.h"
#include "qsee_comstr.h"
#include "qsee_services.h"


#include "../inc/vtp/vtp.h"
/** 
 * app name
 */
char TZ_APP_NAME[] = {"fpctzappfingerprint"};

/**
  @brief 
    Add any app specific initialization code here
    QSEE will call this function after secure app is loaded and
    authenticated
*/
void tz_app_init(void)
{
  /*  App specific initialization code*/  
  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "SynaFp Initialization 2015_1113");
}


/**
  @brief 
    App specific commands should be handled in this
    function

  @param[in]      cmd         Requested command structure
  @param[in]      cmdlen      length of request commad struct
  @param[in/out]  rsp         Resposnse structure
  @param[in/out]  rsplen      length of response commad struct
*/
void tz_app_cmd_handler(void* pInData, uint32 pInDataLen, 
                        void* pOutData, uint32 pOutDataLen)
{

  /* Add code to process requests and set response (if any)*/
    vcsConstBlobData_t  inBlob    = {0};
    vcsBlobData_t       outBlob   = {0};
    vcsInt32_t  result = 0;
    
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "Entered tz_app_cmd_handler for fingerprint\n");
    
    if ((pInData == NULL) || (pOutData == NULL) || !pInDataLen || !pOutDataLen) 
    {
        QSEE_LOG(QSEE_LOG_MSG_DEBUG, "pInData=%x, pInDataLen=%d, pOutData=%x, pOutDataLen=%d\n", pInData, pInDataLen, pOutData, pOutDataLen);
        return; 
    }
    
    inBlob.length = pInDataLen;
    inBlob.pData = pInData;
    outBlob.length = pOutDataLen;
    outBlob.pData = pOutData;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "Executing vtpCall from inside tz command handler\n");
    result = vtpCall(VCS_NULL, &inBlob, &outBlob);
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "Exiting tz_app_cmd_handler for Synafingerprint result=%d\n",result);
}


/**
  @brief 
    App specific shutdown code
    App will be given a chance to shutdown gracefully
*/
void tz_app_shutdown(void)
{
  /* app specific shutdown code*/
  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "SynaFP shutdown");
  return;
}

