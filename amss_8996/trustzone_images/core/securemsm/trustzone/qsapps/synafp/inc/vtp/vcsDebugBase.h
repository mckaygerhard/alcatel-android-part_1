/*! @file vcsDebugBase.h
*******************************************************************************
**  Debug Interface Definition
**
**  This file defines a set of debug macros that can be used to instrument
**  C/C++ source code. The macros defined here are only compiled in the code
**  when compiler defines DEBUG, _DEBUG, or DBG != 0. This allows you to
**  add as much instrumentation to your code as you like, and then have it
**  all compiled out for the final release.
**
*/

#ifndef __vcsDebugBase_h__
#define __vcsDebugBase_h__

#include "vcsDebugBaseEx.h"
#include "vcsPalProdTrace.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/****************************************************************************/
#if !DBG /* When (DBG == 0) Event Tracing replaces some macroses. */
# ifdef DBG_ERROR
#  undef DBG_ERROR
# endif
# define DBG_ERROR(A,S)         PAL_PRODTRACE_ERROR()

# ifdef DBG_WARNING
#  undef DBG_WARNING
# endif
# define DBG_WARNING(A,S)       PAL_PRODTRACE_WARNING()
#ifdef VCS_OS_LINUX
# define DbgLastError(A)              "unavailable" /* causes problems under linux */
#endif

# ifdef DBG_STATUS
#  undef DBG_STATUS
# endif
# define DBG_STATUS(A,S)        PAL_PRODTRACE_INFO()

# ifdef DBG_NOTICE
#  undef DBG_NOTICE
# endif
# define DBG_NOTICE(A,S)        PAL_PRODTRACE_VERBOSE()

# ifdef DBG_SET_LEVEL
#  undef DBG_SET_LEVEL
# endif
# define DBG_SET_LEVEL(L)        PAL_PRODTRACE_SET_LEVEL(L)

/* define DBG_MINIMAL to enable only DBG_ERROR, DBG_WARNING and DBG_STATUS */
#ifndef DBG_MINIMAL

# ifdef VCSAPI_ENTER
#  undef VCSAPI_ENTER
# endif
# define VCSAPI_ENTER(A, S)        PAL_PRODTRACE_INFO()

# ifdef VCSAPI_LEAVE
#  undef VCSAPI_LEAVE
# endif
# define VCSAPI_LEAVE(A, S)        PAL_PRODTRACE_INFO()

#endif /* DBG_MINIMAL */

#endif /* !DBG */
/****************************************************************************/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __vcsDebugBase_h__ */
