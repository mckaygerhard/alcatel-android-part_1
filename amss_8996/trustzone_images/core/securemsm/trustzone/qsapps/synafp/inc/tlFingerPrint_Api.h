/*
 *
 */
#ifndef __TLFINGERPRINT_API_H__
#define __TLFINGERPRINT_API_H__

#include "tci.h"
#include "securefp_common.h"

#undef FEATURE_DEPEND_ON_DMA
//#undef FEATURE_DEVELOP_MODE

/* Command ID's for communication Trustlet Connector -> Trustlet. */
#define	CMD_FINGERPRINT_GETMODE									0x00000001
#define	CMD_FINGERPRINT_VTPCALL									0x00000002

/* TL Return codes */
#define	TL_FINGERPRINT_SUCCESS						            0x00000000
#define	TL_ERR_FINGERPRINT_GETMODE_FAILED						0x00000001
#define	TL_ERR_FINGERPRINT_VTPCALL_FAILED						0x00000002

/* Termination codes */
#define EXIT_ERROR                      ((uint32_t)(-1))


/* Trustlet UUID. */
#define TL_FINGERPRINT_UUID	{{0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, \
									0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09}}
// Secure SPI Driver UUID???
#define DR_SPI_UUID	{{0xff, 0xff, 0xff, 0xff, 0xd0, 0x00, 0x00, 0x00, \
									0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09}}

typedef struct {
    struct {
    	vfm_auth_req_t 	send_cmd;
		vfm_auth_rsp_t 	send_cmd_rsp;
	};
} tciMessage_t;

typedef struct {
	uint8_t 			*inp_buf;
	uint8_t				*inp_vaddr;
	uint32_t			inp_length;
	uint8_t				*out_buf;
	uint8_t				*out_vaddr;
	uint32_t			out_length;
} mcMap_addr_t;


#endif /* __TLFINGERPRINT_API_H__ */
