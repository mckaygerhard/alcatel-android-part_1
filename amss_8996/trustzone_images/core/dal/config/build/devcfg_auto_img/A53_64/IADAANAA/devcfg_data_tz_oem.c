#include "DALSysTypes.h" 

extern const DALSYSPropStructTblType DALPROP_StructPtrs_tz_oem[];

extern const uint32 DALPROP_PropBin_tz_oem[];

extern const StringDevice driver_list_tz_oem[];


const DALProps DALPROP_PropsInfo_tz_oem = {(const byte*)DALPROP_PropBin_tz_oem, DALPROP_StructPtrs_tz_oem, 76, driver_list_tz_oem};
