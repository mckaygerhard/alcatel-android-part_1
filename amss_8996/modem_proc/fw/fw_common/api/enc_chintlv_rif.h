/*
  @file
  enc_chintlv_rif.h

  @brief
  VPE API definitions.

  @detail
  Autogenerated by
  ../../common/drivers/vpe/scripts/parse_vpe_api.pl --input=thor_enc_chintlv_rif_fw.xml --output=enc_chintlv_rif.h

*/

/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================
   THIS FILE IS AUTOGENERATED FROM parse_vpe_api.pl. DO NOT EDIT.
===========================================================================*/

#ifndef ENC_CHINTLV_RIF_H
#define ENC_CHINTLV_RIF_H

typedef union {
  struct {
    uint32 ch_0                           : 16;  /* W[0] [15: 0] */
    uint32 ch_1                           : 16;  /* W[0] [31:16] */
  };

  uint32 word_arr[1];
  uint32 word_val;

} enc_tx_chintlv_num_sym_t;


typedef union {
  struct {
    uint32 r0_rri                         :  3;  /* W[0] [ 2: 0] */
    uint32 rab_rri_c0                     :  4;  /* W[0] [ 6: 3] */
    uint32 rab_rri_c1                     :  4;  /* W[0] [10: 7] */
    uint32 rab_rri_c2                     :  4;  /* W[0] [14:11] */
    uint32 reserved                       : 17;  /* W[0] [31:15] */
  };

  uint32 word_arr[1];
  uint32 word_val;

} enc_tx_chintlv_do_param_t;


typedef union {
  struct {
    enc_tx_chintlv_num_sym_t                                     num_sym[3];                    /* W[0]..W[2] */
    enc_tx_chintlv_do_param_t                                    param;                         /* W[3]..W[3] */
  };

  uint32 word_arr[4];

} enc_tx_chintlv_do_t;


typedef union {
  struct {
    uint32 empty_word0;                          /* W[0] */
    uint32 empty_word1;                          /* W[1] */
    uint32 tech_mode                      :  2;  /* W[2] [ 1: 0] */
#define ENC_TX_CHINTLV_PARAM_TECH_MODE_CDMA                          (0)
#define ENC_TX_CHINTLV_PARAM_TECH_MODE_UMTS                          (1)
#define ENC_TX_CHINTLV_PARAM_TECH_MODE_TDS                           (2)
#define ENC_TX_CHINTLV_PARAM_TECH_MODE_LTE                           (3)

    uint32 ch_type                        :  2;  /* W[2] [ 3: 2] */
#define ENC_TX_CHINTLV_PARAM_CH_TYPE_ONEX_RC_12                      (0)
#define ENC_TX_CHINTLV_PARAM_CH_TYPE_ONEX_RC_348                     (1)
#define ENC_TX_CHINTLV_PARAM_CH_TYPE_DO_REV0                         (2)
#define ENC_TX_CHINTLV_PARAM_CH_TYPE_DO_REVAB                        (3)

    uint32 ch_en                          : 20;  /* W[2] [23: 4] */
    uint32 ch_num                         :  3;  /* W[2] [26:24] */
    uint32 slot_num                       :  4;  /* W[2] [30:27] */
    uint32 reserved2                      :  1;  /* W[2] [31:31] */
  };

  uint32 word_arr[3];

} enc_tx_chintlv_param_t;


typedef union {
  struct {
    uint32 iram_word_addr                 : 14;  /* W[0] [13: 0] */
    uint32 reserved                       : 18;  /* W[0] [31:14] */
  };

  uint32 word_arr[1];
  uint32 word_val;

} enc_tx_chintlv_mem_iram_addr_t;


typedef union {
  struct {
    enc_tx_chintlv_mem_iram_addr_t                               mem_iram_addr[4];              /* W[0]..W[3] */
    enc_tx_chintlv_param_t                                       chintlv_param;                 /* W[4]..W[6] */
  };

  uint32 word_arr[7];

} enc_tx_chintlv_common_cfg_t;


typedef union {
  struct {
    uint32 ach_en                         :  1;  /* W[0] [ 0: 0] */
    uint32 fch_is95c                      :  1;  /* W[0] [ 1: 1] */
    uint32 fch_rate                       :  2;  /* W[0] [ 3: 2] */
    uint32 each_en                        :  1;  /* W[0] [ 4: 4] */
    uint32 sch_intlv_size                 :  2;  /* W[0] [ 6: 5] */
    uint32 reserved                       : 25;  /* W[0] [31: 7] */
  };

  uint32 word_arr[1];
  uint32 word_val;

} enc_tx_chintlv_1x_param_t;


typedef union {
  struct {
    enc_tx_chintlv_num_sym_t                                     num_sym[2];                    /* W[0]..W[1] */
    enc_tx_chintlv_1x_param_t                                    param;                         /* W[2]..W[2] */
  };

  uint32 word_arr[3];

} enc_tx_chintlv_1x_t;


typedef union {
  struct {
    uint32 r99_last_row                   :  9;  /* W[0] [ 8: 0] */
    uint32 r99_last_col                   :  5;  /* W[0] [13: 9] */
    uint32 reserved0                      : 18;  /* W[0] [31:14] */
    uint32 eul_en_16qam                   :  1;  /* W[1] [ 0: 0] */
    uint32 eul_area1_addr                 : 11;  /* W[1] [11: 1] */
    uint32 eul_area2_addr                 : 11;  /* W[1] [22:12] */
    uint32 reserved1                      :  9;  /* W[1] [31:23] */
    uint32 edpdch0_num_bits               : 15;  /* W[2] [14: 0] */
    uint32 edpdch1_num_bits               : 15;  /* W[2] [29:15] */
    uint32 reserved2                      :  2;  /* W[2] [31:30] */
    uint32 edpdch2_num_bits               : 15;  /* W[3] [14: 0] */
    uint32 edpdch3_num_bits               : 15;  /* W[3] [29:15] */
    uint32 reserved3                      :  2;  /* W[3] [31:30] */
  };

  uint32 word_arr[4];

} enc_tx_chintlv_umts_param_t;


typedef union {
  struct {
    enc_tx_chintlv_num_sym_t                                     num_sym[3];                    /* W[0]..W[2] */
    enc_tx_chintlv_umts_param_t                                  param;                         /* W[3]..W[6] */
  };

  uint32 word_arr[7];

} enc_tx_chintlv_umts_t;


typedef union {
  struct {
    uint32 cctrch0_intlv_type             :  2;  /* W[0] [ 1: 0] */
    uint32 cctrch0_iram_addr_offset       :  9;  /* W[0] [10: 2] */
    uint32 reserved0                      :  5;  /* W[0] [15:11] */
    uint32 cctrch1_intlv_type             :  2;  /* W[0] [17:16] */
    uint32 cctrch1_iram_addr_offset       :  9;  /* W[0] [26:18] */
    uint32 reserved1                      :  5;  /* W[0] [31:27] */
    uint32 eul_mod_mode                   :  1;  /* W[1] [ 0: 0] */
    uint32 eul_constellation_rearrange    :  2;  /* W[1] [ 2: 1] */
    uint32 reserved2                      : 29;  /* W[1] [31: 3] */
  };

  uint32 word_arr[2];

} enc_tx_chintlv_tds_param_t;


typedef union {
  struct {
    uint32 ch0_type                       :  2;  /* W[0] [ 1: 0] */
    uint32 ch0_sf                         :  3;  /* W[0] [ 4: 2] */
    uint32 ch0_cctrch_src                 :  1;  /* W[0] [ 5: 5] */
    uint32 ch0_cfg_src                    :  1;  /* W[0] [ 6: 6] */
    uint32 reserved0                      :  9;  /* W[0] [15: 7] */
    uint32 ch1_type                       :  2;  /* W[0] [17:16] */
    uint32 ch1_sf                         :  3;  /* W[0] [20:18] */
    uint32 ch1_cctrch_src                 :  1;  /* W[0] [21:21] */
    uint32 ch1_cfg_src                    :  1;  /* W[0] [22:22] */
    uint32 reserved1                      :  9;  /* W[0] [31:23] */
  };

  uint32 word_arr[1];
  uint32 word_val;

} enc_tx_chintlv_tds_ch_cfg_t;


typedef union {
  struct {
    enc_tx_chintlv_num_sym_t                                     num_sym[10];                   /* W[0]..W[9] */
    enc_tx_chintlv_tds_param_t                                   param;                         /* W[10]..W[11] */
    enc_tx_chintlv_tds_ch_cfg_t                                  ch_cfg[10];                    /* W[12]..W[21] */
  };

  uint32 word_arr[22];

} enc_tx_chintlv_tds_t;


typedef union {
  struct {
    uint32 acknak_scr_en                  :  1;  /* W[0] [ 0: 0] */
    uint32 acknak_scr_seq_idx             :  2;  /* W[0] [ 2: 1] */
    uint32 num_freq_car                   : 11;  /* W[0] [13: 3] */
    uint32 mod_type                       :  2;  /* W[0] [15:14] */
    uint32 num_symbols                    :  2;  /* W[0] [17:16] */
    uint32 cp_type                        :  1;  /* W[0] [18:18] */
    uint32 cqi_bc_mode                    :  1;  /* W[0] [19:19] */
    uint32 ri_in_type                     :  2;  /* W[0] [21:20] */
    uint32 acknak_in_type                 :  2;  /* W[0] [23:22] */
    uint32 reserved0                      :  8;  /* W[0] [31:24] */
    uint32 acknak_rm_sym0                 : 13;  /* W[1] [12: 0] */
    uint32 acknak_rm_sym1                 : 13;  /* W[1] [25:13] */
    uint32 reserved1                      :  6;  /* W[1] [31:26] */
    uint32 ri_rm_sym                      : 11;  /* W[2] [10: 0] */
    uint32 reserved2                      : 21;  /* W[2] [31:11] */
    uint32 cqi_rm_sym                     : 14;  /* W[3] [13: 0] */
    uint32 reserved3                      : 18;  /* W[3] [31:14] */
    uint32 scr_init                       : 31;  /* W[4] [30: 0] */
    uint32 reserved4                      :  1;  /* W[4] [31:31] */
  };

  uint32 word_arr[5];

} enc_tx_chintlv_lte_param_t;


typedef union {
  struct {
    enc_tx_chintlv_num_sym_t                                     num_sym;                       /* W[0]..W[0] */
    enc_tx_chintlv_lte_param_t                                   param;                         /* W[1]..W[5] */
  };

  uint32 word_arr[6];

} enc_tx_chintlv_lte_t;


typedef union {
  struct {
    enc_tx_chintlv_1x_t                                          cdma_1x;                       /* W[0]..W[2] */
  };

  struct {
    enc_tx_chintlv_do_t                                          cdma_do;                       /* W[0]..W[3] */
  };

  struct {
    enc_tx_chintlv_umts_t                                        umts;                          /* W[0]..W[6] */
  };

  struct {
    enc_tx_chintlv_tds_t                                         tds;                           /* W[0]..W[21] */
  };

  struct {
    enc_tx_chintlv_lte_t                                         lte;                           /* W[0]..W[5] */
  };

  uint32 word_arr[22];

} enc_tx_chintlv_union_t;


typedef union {
  struct {
    enc_tx_chintlv_common_cfg_t                                  common_cfg;                    /* W[0]..W[6] */
    enc_tx_chintlv_union_t                                       shared;                        /* W[7]..W[28] */
  };

  uint32 word_arr[29];

} enc_chintlv_mmap_t;


#endif  /* ENC_CHINTLV_RIF_H */

/* Parser warnings:
Overlap detected in struct ENC_TX_CHINTLV_UNION - converting to union*/
