/*!
  @file
  fw_decob_intf.h

  @brief
  Decoder output results buffer definitions

  @details
  Decoder output is written to the mempool unlike a separate DECOB buffer in
  HW like legacy. This file has details regarding information like
  start line, min bank, max_bank, vbuf_len (for all techs) to SW to use
  in reading out the results.
 
  L1 SW does not need to program any HW, it just needs to read out results from
  page 1 by setting the correct page
  To set page: HWIO_OUTI(MEM_PL_MEM_CHm_PAGE_NUM,1,offset>>14)
      To read: header = HWIO_IN(MEM_POOL_MEM_PAGE1_START+(offset&0x3FFF));
 
  A2 SW has a separate interface of its own and hence needs to have a separate
  VBUF length for every tech

*/

/*===========================================================================

  Copyright (c) 2012-2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
03/16/15   TC      Update DECOB MACROs to https://sharepoint.qualcomm.com/qct/DHW/Cores/Modem/MP/Thor/Documents/Microkernels/HLD/Memory_Map_Release/THOR-Memory-Map-R1.1.xlsx.
                   DECOB moved to cluster 0
09/19/14   TC      Update DECOB MACROs to https://sharepoint.qualcomm.com/qct/DHW/Cores/Modem/MP/Thor/Documents/Microkernels/HLD/Memory_Map_Release/THOR-Memory-Map-R0.2.xlsx.
                   Start line is relative to cluster 3 base line.
08/13/13   NB      For Bolt, DECOB lies at line 0 of banks 0-3 for INSTANCE 0
                   and line 0 of banks 4-7 for INSTANCE 1. SW can read using 
                   AXI interface and the register MEM_POOL_AXI_START 
06/10/12   NB      Initial DECOB interface 
 
===========================================================================*/

#ifndef FW_DECOB_INTF_H
#define FW_DECOB_INTF_H

#define FW_DECOB_INCLUDE_DEPRECATED

#ifdef FW_DECOB_INCLUDE_DEPRECATED
/*! @brief Mempool bridge ID to be used by SW */
#define FW_DECOB_OUT_MEMPL_BRDG_ID          1

/*! @brief DECOB base address for Instance 0 results (to be read from 
  banks 0 - 3 */
#define FW_DECOB_OUT_INST0_BANKS_0_TO_3_ADDR             (0xED900000)

/*! @brief DECOB base address for Instance 1 results (to be read from 
  banks 4 - 7 */
#define FW_DECOB_OUT_INST1_BANKS_4_TO_7_ADDR             (0xED970000)
#endif // FW_DECOB_INCLUDE_DEPRECATED

/*! @brief Init bank in mempool to start writing results */
#define FW_DECOB_OUT_MEMPL_INIT_BANK        0

/*! @brief Min bank in mempool to start writing results */
#define FW_DECOB_OUT_MEMPL_MIN_BANK         0

/*! @brief Max bank in mempool to start writing results */
#define FW_DECOB_OUT_MEMPL_MAX_BANK         7

#ifdef FW_DECOB_INCLUDE_DEPRECATED
/*! @brief Min bank in mempool to start writing results for instance 1 */
#define FW_DECOB_OUT_INST0_MEMPL_MIN_BANK         0

/*! @brief Max bank in mempool to start writing results  for instance 1 */
#define FW_DECOB_OUT_INST0_MEMPL_MAX_BANK         7

/*! @brief Min bank in mempool to start writing results for instance 1 */
#define FW_DECOB_OUT_INST1_MEMPL_MIN_BANK         0

/*! @brief Max bank in mempool to start writing results  for instance 1 */
#define FW_DECOB_OUT_INST1_MEMPL_MAX_BANK         7
#endif // FW_DECOB_INCLUDE_DEPRECATED

/*! @brief DECOB uses start line relative to cluster 3 base */
#define FW_DECOB_OUT_CLST3_BASE_START_LINE    (49152)

/*! @brief Default start line for DECOB results */
#define FW_DECOB_OUT_MEMPL_START_LINE         0

/*! @brief Start line in mempool for start of DECOB for LTE carrier 0 */
#define FW_DECOB_OUT_MEMPL_START_LINE_LTE     (3750)

#ifdef FW_DECOB_INCLUDE_DEPRECATED
/*! @brief Start line in mempool for start of DECOB for LTE carrier 1 */
#define FW_DECOB_OUT_MEMPL_START_LINE_LTE_C1  (49746 - FW_DECOB_OUT_CLST3_BASE_START_LINE)
#endif // FW_DECOB_INCLUDE_DEPRECATED

/*! @brief Start line in mempool for start of DECOB for WCDMA */
#define FW_DECOB_OUT_MEMPL_START_LINE_WCDMA   (5197)

/*! @brief Start line in mempool for start of DECOB for 1X */
#define FW_DECOB_OUT_MEMPL_START_LINE_1X      (7924)

/*! @brief Start line in mempool for start of DECOB for DO */
#define FW_DECOB_OUT_MEMPL_START_LINE_DO      (7352)

/*! @brief Start line in mempool for start of DECOB for TDSCDMA */
#define FW_DECOB_OUT_MEMPL_START_LINE_TDSCDMA (3750)

/* The VBUF length in words for all techs
*/ 

/*! @brief VBUF length for LTE */
#define FW_DECOB_OUT_MEMPL_VBUF_LEN_WORDS_LTE     (13600)

/*! @brief VBUF length for WCDMA */
#define FW_DECOB_OUT_MEMPL_VBUF_LEN_WORDS_WCDMA   (22335)

/*! @brief VBUF length for 1X */
#define FW_DECOB_OUT_MEMPL_VBUF_LEN_WORDS_1X      (194)

/*! @brief VBUF length for DO/HDR */
#define FW_DECOB_OUT_MEMPL_VBUF_LEN_WORDS_DO      (6240)

/*! @brief VBUF length for TDSCDMA */
#define FW_DECOB_OUT_MEMPL_VBUF_LEN_WORDS_TDSCDMA (2012)


#endif /* FW_DECOB_INTF_H */
