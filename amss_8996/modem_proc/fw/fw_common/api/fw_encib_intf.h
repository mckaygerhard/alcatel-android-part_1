/*!
  @file
  fw_encib_intf.h

  @brief
  Encoder input buffer definitions

  @details
  Encoder input is written to the LMEM0 unlike a separate ENCIB buffer in
  HW like legacy. This file has details regarding information like
  start the starting word offset (from LMEM0 base) and vbuf_len (for all techs)
  to SW to use in writing the encoder input buffer
 
  The offset and the length will be used by SW to make ensure that the
  amount of words written by SW is fitting the allocated memory

*/

/*===========================================================================

  Copyright (c) 2013-2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
03/16/15   TC      Updae ENCIB MACROs to https://sharepoint.qualcomm.com/qct/DHW/Cores/Modem/MP/Thor/Documents/Microkernels/HLD/Memory_Map_Release/THOR-Memory-Map-R1.1.xlsx.
                   ECNIB moved to cluster 0
09/19/14   TC      Updae ENCIB MACROs to https://sharepoint.qualcomm.com/qct/DHW/Cores/Modem/MP/Thor/Documents/Microkernels/HLD/Memory_Map_Release/THOR-Memory-Map-R0.2.xlsx.
                   Start line is relative to cluster 0 base line.
09/09/13   NB      Initial ENCIB interface
 
===========================================================================*/

#ifndef FW_ENCIB_INTF_H
#define FW_ENCIB_INTF_H

#define FW_ENCIB_INCLUDE_DEPRECATED

/*! @brief Enum to indicate different types of ENCIB buffers */
typedef enum {
  /*! LTE 20Mhz */
  FW_ENCIB_LTE_20MHZ,
  /*! LTE 10Mhz */
  FW_ENCIB_LTE_10MHZ,
  /*! WCDMA dualcarrier  */
  FW_ENCIB_WCDMA_DC,
  /*! WCDMA single carrier */
  FW_ENCIB_WCDMA_SC,
  /*! TDSCDMA */
  FW_ENCIB_TDSCDMA,
  /*! EVDO */
  FW_ENCIB_DO,
  /*! CDMA1x */
  FW_ENCIB_CDMA,
} fw_encib_type_e;

/*! @brief Enum to indicate Instance used (both Enc and DECOB use same instance) */
typedef enum {
  /*! Instance 0 for both Encoder input and decoder output */
  FW_INST0,
  /*! Instance 1 for both Encoder input and decoder output */
  FW_INST1,
  /*! Number of instances */
  FW_INST_NUM
} fw_inst_e;

#ifdef FW_ENCIB_INCLUDE_DEPRECATED
/* Start offsets from base on LMEM for the various ENC input buffer
   configurations */

/*! @brief LTE 20Mz ENCOB start line for instance 0 */
#define FW_ENCIB_START_WORD_OFFSET_LTE_20MHZ_INST_0 (3266*4*8)

/*! @brief LTE 10Mz ENCOB start line for instance 0 */
#define FW_ENCIB_START_WORD_OFFSET_LTE_10MHZ_INST_0 (2966*4*8)

/*! @brief WCDMA_DC ENCOB start line for instance 0 */
#define FW_ENCIB_START_WORD_OFFSET_WCDMA_DC_INST_0 (2674*4*8)

/*! @brief WCDMA_SC ENCOB start line for instance 0 */
#define FW_ENCIB_START_WORD_OFFSET_WCDMA_SC_INST_0 (2674*4*8)

/*! @brief TDSCDMA ENCOB start line for instance 0 */
#define FW_ENCIB_START_WORD_OFFSET_TDSCDMA_INST_0 (2671*4*8)

/*! @brief DO ENCOB start line for instance 0 */
#define FW_ENCIB_START_WORD_OFFSET_DO_INST_0 (2677*4*8)

/*! @brief CDMA ENCOB start line for instance 0 */
#define FW_ENCIB_START_WORD_OFFSET_CDMA_INST_0 (2670*4*8)

/*! @brief LTE 20Mz ENCOB start line for instance 1 */
#define FW_ENCIB_START_WORD_OFFSET_LTE_20MHZ_INST_1 (5056*4*8)

/*! @brief LTE 10Mz ENCOB start line for instance 1 */
#define FW_ENCIB_START_WORD_OFFSET_LTE_10MHZ_INST_1 (4643*4*8)

/*! @brief WCDMA_DC ENCOB start line for instance 1 */
#define FW_ENCIB_START_WORD_OFFSET_WCDMA_DC_INST_1 (4505*4*8)

/*! @brief WCDMA_SC ENCOB start line for instance 1 */
#define FW_ENCIB_START_WORD_OFFSET_WCDMA_SC_INST_1 (5234*4*8)

/*! @brief TDSCDMA ENCOB start line for instance 1 */
#define FW_ENCIB_START_WORD_OFFSET_TDSCDMA_INST_1 (4956*4*8)

/*! @brief DO ENCOB start line for instance 1 */
#define FW_ENCIB_START_WORD_OFFSET_DO_INST_1 (5052*4*8)

/*! @brief CDMA ENCOB start line for instance 1 */
#define FW_ENCIB_START_WORD_OFFSET_CDMA_INST_1 (5240*4*8)
#endif // FW_ENCIB_INCLUDE_DEPRECATED

/* Start offsets from base on cluster 0 for the various ENC input buffer
   configurations */
/*! @brief LTE ENCIB start word offset for carrier 0 */
#define FW_ENCIB_START_WORD_OFFSET_LTE_CC0         (2427*4*8)

/*! @brief LTE ENCIB start word offset for carrier 1 */
#define FW_ENCIB_START_WORD_OFFSET_LTE_CC1         (3135*4*8)

/*! @brief WCDMA ENCIB start word offset */
#define FW_ENCIB_START_WORD_OFFSET_WCDMA           (3455*4*8)

/*! @brief TDSCDMA ENCIB start word offset */
#define FW_ENCIB_START_WORD_OFFSET_TDSCDMA         (5659*4*8)

/*! @brief DO ENCIB start word offset */
#define FW_ENCIB_START_WORD_OFFSET_DO              (5133*4*8)

/*! @brief CDMA ENCIB start word offset */
#define FW_ENCIB_START_WORD_OFFSET_CDMA            (7512*4*8)

/* Maximum length of the encoder input buffer for
   various configurations */
#ifdef FW_ENCIB_INCLUDE_DEPRECATED
/*!@brief ENCIB LTE_20MHZ buffer max length in words */
#define FW_ENCIB_LTE_20MHZ_MAX_LENGTH_WORDS (100*4*8)

/*!@brief ENCIB LTE_10MHZ buffer max length in words */
#define FW_ENCIB_LTE_10MHZ_MAX_LENGTH_WORDS (100*4*8)

/*!@brief ENCIB WCDMA_DC buffer max length in words */
#define FW_ENCIB_WCDMA_DC_MAX_LENGTH_WORDS (104*4*8)

/*!@brief ENCIB WCDMA_SC buffer max length in words */
#define FW_ENCIB_WCDMA_SC_MAX_LENGTH_WORDS (14*4*8)
#endif // FW_ENCIB_INCLUDE_DEPRECATED

/*!@brief ENCIB LTE carrier 0 buffer max length in words */
#define FW_ENCIB_LTE_MAX_LENGTH_WORDS_CC0         (4714*4*8)

/*!@brief ENCIB LTE carrier 1 buffer max length in words */
#define FW_ENCIB_LTE_MAX_LENGTH_WORDS_CC1         (4714*4*8)

/*!@brief ENCIB WCDMA buffer max length in words */
#define FW_ENCIB_WCDMA_MAX_LENGTH_WORDS           (3320*4*8)

/*!@brief ENCIB TDSCDMA buffer max length in words */
#define FW_ENCIB_TDSCDMA_MAX_LENGTH_WORDS         (1142*4*8)

/*!@brief ENCIB DO buffer max length in words */
#define FW_ENCIB_DO_MAX_LENGTH_WORDS              (4608*4*8)

/*!@brief ENCIB CDMA buffer max length in words */
#define FW_ENCIB_CDMA_MAX_LENGTH_WORDS            (224*4*8)

#endif /* FW_ENCIB_INTF_H */

