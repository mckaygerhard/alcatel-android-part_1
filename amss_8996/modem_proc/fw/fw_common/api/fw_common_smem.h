/*!
  @file
  fw_common_smem.h

  @brief
  Common FW Shared Memory Definitions

*/

/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated. 
  All rights reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

#ifndef FW_COMMON_SMEM_H
#define FW_COMMON_SMEM_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/


/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/* Top-level Common FW SMEM interface, contains pointers to sub-sections */   
typedef struct
{
  struct fw_mvc_smem_t *fw_mvc_smem;  
} fw_common_smem_t;


/* Macros similar to memmap.h SW clients can use to point directly at the 
   subsection */
#define FW_SMEM_INTF_MVC_ADDR (((fw_common_smem_t*)FW_SMEM_COMMON_ADDR)->fw_mvc_smem)


#endif /* FW_COMMON_SMEM_H */


