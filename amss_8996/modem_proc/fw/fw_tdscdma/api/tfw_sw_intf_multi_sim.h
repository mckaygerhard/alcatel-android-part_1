/*!
  @file
  tfw_sw_intf_cxm.h

  @brief
  TD-SCDMA common FW-SW Interface Definitions 

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/modem/fw/cpl/thor/MPSS.TH.2.0.C1.9/image/fw_tdscdma/api/tfw_sw_intf_multi_sim.h#34 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------  
===========================================================================*/

#ifndef TFW_SW_INTF_MULTI_SIM_H
#define TFW_SW_INTF_MULTI_SIM_H


/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "tfw_const.h"
#include "msgr.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/
/**
    \defgroup cmdCommon Common Configuration Commands & Responses
    */
/*\{*/

  /*-----------------------------------------------------------------------*/
  /*     CONSTANTS                                                         */
  /*-----------------------------------------------------------------------*/

/* Interface provides passing parameters for 7 Regular + 2 Special slots   */
#define TFW_MULTI_SIM_SLOT_MAX                      9

#define TFW_MULTI_SIM_DWPTS_LOC                     7
#define TFW_MULTI_SIM_UPPTS_LOC                     8

#define TFW_MULTI_SIM_SFN_IMMEDIATE            0xFFFF

/* This is the max time from current Sfn multi sim message can be sent*/
#define TFW_MULTI_SIM_MAX_ACTION_SFN_LOOKAHEAD      8


/*! @brief Enumerated Gap Command types
 *
 *  TL1 will use this types to inform TFW whether to start the GAP or
 *  abort the current gap
 *
 */
typedef enum 
{
  TFW_DSDS_GAP_START = 1,
  TFW_DSDS_GAP_ABORT 
} tfw_dsds_gap_cmd_e;


/*! @brief Enumerated SIM Mode types
 *
 *  This enum lists all possible SIM Modes
 */
typedef enum 
{
  TFW_SINGLE_SIM_MODE = 0,
  TFW_MULTI_SIM_MODE_DSDS,
  TFW_MULTI_SIM_MODE_DSDA,
  /*! \brief This is the multi SIM mode of TFW in the TFW_STATE_STANDBY or
     TFW_STATE_INACTIVE states. 
     TL1 will configure a different multi SIM mode for changing TFW to a 
     different state. 
     IF TL1 places TFW to TFW_STATE_STANDBY or TFW_STATE_INACTIVE state
     TL1 sets state of Multi SIM Mode to TFW_SIM_MODE_NOT_CONFIGURED
  */
  TFW_SIM_MODE_NOT_CONFIGURED
} tfw_sim_mode_e;
/*\}*/

typedef struct
{
  /*! \brief TFW uses this priority to register activities with
     the coexistence manager
  */
  uint32  cxmPriority;

  /*! \brief TFW uses desense Id for register activities with
     the coexistence manager
  */
  uint32  cxmFreqId;


} tfw_cxm_parameters_t;

/**
   \defgroup cmdMultiSimConfig MULTI_SIM_CONFIG Command
   \ingroup cmdMultiSim
   @verbatim
   Usage:
          
    @endverbatim 
*/

typedef struct
{
  /*! \brief This is the Multi Sim Operation Mode*/
  tfw_sim_mode_e mode;

  /*! \brief This is the indication of subframe number in rxTime the MULTI_SIM_CONFIG
     message will be latched by the FW. For TFW updating CXM parameters immediately,
     pls. set it to 0xFFFF using the macro TFW_MULTI_SIM_SFN_IMMEDIATE define above.
  */
  uint16 actionSfn;

  /*! \brief flag to swap priority every frame(10 ms), for cell-DCH state, to protect DL
      TFCI decoding, need to use a high-high-low-low approach to protect whole TFCI
      and not block GSM activity. Refer to TFW design doc for details. 
      TRUE:  swap priority every 10ms ( For sfm_no % 4 = 0,1 params will be used )
                                      ( For sfm_no % 4 = 2,3 paramsSwap will be used )
      FALSE: do not swap priority     ( TFW will only use param buffer for all subframes)
  */
  boolean swapPriority;
  uint32                  :31;  

  /*! \brief DR-DSDS\STX-DSDA features require Channel ID to be dynamically allocated by RFSW -> TL1 -> TFW. 
      Currenlty we use only the channel ID information for the Primary Rx chain. 
      The fields for Diversity RX and TX are unpopulated by TL1 and hence need to be ignored. 
  */
  
  uint32 channelIdPRx;
  uint32 channelIdDRx;
  uint32 channelIdTx;


  /*! \brief For TFW states such as acquisition and IRAT, TFW will use the first
       one or two elements of the array for registration as no slot timings have been established
       yet. If needed, TL1 can set all array elements to the same values.
       Special slot dwpts uses array no 7 and Uppts uses array no 8. Regular slots use
       locations 0-7
  */
  tfw_cxm_parameters_t params[TFW_MULTI_SIM_SLOT_MAX];

  tfw_cxm_parameters_t paramsSwap[TFW_MULTI_SIM_SLOT_MAX];

} tfw_multi_sim_settings_t;



/*@{*/ 
typedef struct
{
  /*! \brief Message header */
  msgr_hdr_struct_type          hdr;
  /*! \brief Body of the message */
  tfw_multi_sim_settings_t      multiSimCfg;
  
} tfw_multi_sim_config_cmd_t;
/*@}*/



/*@}*/

  /*-----------------------------------------------------------------------*/
  /*      DATA STRUCTURES FOR DSDS_QTA_GAP COMMAND                         */
  /*-----------------------------------------------------------------------*/


/**
   \defgroup cmdmultiSimdsdsQtaGap MULTI_SIM_DSDS_QTA_GAP Command
   \ingroup cmdMultiSim
   @verbatim
   Usage:
     1) This command can only be sent to FW while FW is in DSDS mode.
     2) Each time TFW receives this message, it will start using CXM. It will 
        use CXM until it sends back the DSDS_CLEANUP indication to TL1. TL1 will 
        send this message for every QTA gap.
     3) If TFW receives this message in any other mode than DSDS, it will assert.
          
    @endverbatim 
*/

typedef struct 
{
  tfw_dsds_gap_cmd_e gapCmd;
  /*! \brief Cleanup RF buffer index (in SMEM space) */ 
  uint8 rfRxCleanupBufIndex;
  /*! \brief if TRUE => QBTA , FALSE => QTA */ 
  boolean isQbta;
} tfw_multi_sim_dsds_qta_gap_cfg_t;

/*@{*/
typedef struct 
{
  /*! \brief Message header */
  msgr_hdr_struct_type               hdr;
  /*! \brief IRAT T2X Cleanup measure config */
  tfw_multi_sim_dsds_qta_gap_cfg_t   dsdsGapCfg;
} tfw_multi_sim_dsds_qta_gap_cmd_t;
/*@}*/

/*! @brief DSDS_CLEANUP Indication
 *
 *   This is the message definition for DSDS_CLEANUP Indication
 *   sent to TL1 at the end of QTA gap.
 */

/*@{*/ 
typedef struct 
{

  /*! \brief Start time 
      @verbatim
      Resolution: cx8
	Upper 16bits represent sub-frame number and lower 16bits represent CX8 count that wraps around at 0xC800
	Note that this data type may represent walltime or RX time. Refer to comments in structures where this type
	is included
      @endverbatim
      **/
  uint32        cxmLockLostTime;
  /*! \brief End time
      @verbatim
      Resolution: cx8
	Upper 16bits represent sub-frame number and lower 16bits represent CX8 count that wraps around at 0xC800
	Note that this data type may represent walltime or RX time. Refer to comments in structures where this type
	is included
      @endverbatim
      **/
  uint32        cxmLockAcqTime;
 


} tfw_multi_sim_cxm_lock_lost_timings_t;


/*@{*/ 
typedef struct
{
  /*! \brief Message header */
  msgr_hdr_struct_type     hdr;
  /*! \brief Body of the message */
  tfw_multi_sim_cxm_lock_lost_timings_t  cxmLostTimingsInd;

} tfw_dsds_cleanup_ind_t;
/*@}*/
  
#endif /* TFW_SW_INTF_MULTI_SIM_H */
