/*!
  @file
  intf_prs_sm.h

  @brief
  REQUIRED brief one-sentence description of this C header file.

  @detail
  OPTIONAL detailed description of this C header file.
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2008 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
04/09/13   cj      Adding COM adjustment value to PRS smem interface. 
04/17/12   cj      Add PRS S/(S+N) data to sharedmem interface.
11/01/11   cj      Add params for PRS start index and prune window to per-cell 
                   structure in shared mem.  
05/02/11   cj      Added PRS message router interface.
===========================================================================*/

#ifndef INTF_PRS_SM_H
#define INTF_PRS_SM_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "intf_common.h"
#include "intf_prs.h"
#include "lte_LL1_log_prs.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

#ifndef PRS_SFINFO_DBG_LOGGING
#define PRS_SFINFO_DBG_LOGGING
#endif 

/*! @brief cell-specific PRS Meas descriptor.
*/
typedef struct
{
  /*! Physical Cell ID corresponding to the CER vector. 
      Range: 0~503
  */
  uint16  cer_cell_id;  
    
  /*! slot Offset */
  uint8 slot_offset;

  /*! Symbol Tap Offset */
  int8 symbol_offset;

  /*! Start Tap Index in Ts [0-2048] */
  uint16 start_idx; 

  /*! PRS Search Window Prune Size in Ts [0-2048]*/
  uint16 prune_size; 

  /*! Symbol Bitmap corresponding to CER */
  uint16 symbol_bitmap[LTE_LL1_LOG_PRS_NUMBER_OF_SUBFRAMES];

  /*! S/(S+N) data in Q31 format for Rx 0 */
  uint32 avg_s_sn_rx0;

  /*! S/(S+N) data in Q31 format for Rx 1 */
  uint32 avg_s_sn_rx1;
    
  /*! the number of the prs subframe per occasion */
  lte_LL1_prs_num_subframe_e n_prs;

  /*! Cell Center frequency */
  lte_earfcn_t    earfcn;

  /*! Memory offset for raw CER vectors relative to beginning of PRS CER buffer. */
   uint16    mem_offset;

  /*! Bias or scaling factor for PRS. Range [-15:13] */
  int8 agc_bias;

  /*! ECP/NCP indication: 0=NCP, 1=ECP */
  uint8 cp_mode;

  /*! CRS/PRS indication: 0=PRS, 1=CRS */
  uint8 search_mode;

  /*! eNB Tx Ant that was searched. Used only for CRS search */
  uint8 eNB_tx_ant_num;

  /*! Which Rx antenna was used to measure - LSB corresponds to Rx0, and so on. */
  uint8 ue_rx_ant_bitmask;

  /*! Rx0-Rx1 Combining mode (coherent, non-coherent). Applicable only when UE has 2 or more Rx chains, WBCRS only */
  lte_LL1_prs_rx_chain_comb_mode_e ue_rx_chain_comb_mode;

} lte_LL1_prs_cell_desc_s;

/*! @brief occasion-specific PRS Meas descriptor.
    This descriptor will be use for both messaging
    ML1, and to write to SharedMem.
*/
typedef struct
{
  /*! TimeStamp aligned to frame boundary of PRS Occasion,
      time read from 32-bit VSRC sample counter. 
      (alternatively 64-bit DL Link counter)
  */
  int64  timestamp; 

  /*! Accumulated TTL Adjustment */
  int16    ttl_adj[LTE_LL1_LOG_PRS_NUMBER_OF_SUBFRAMES];

  /*! Accumulated COM (Center-Of-Mass) Adjustment, in Ts units, Q6 format */
  int32    com_adj[LTE_LL1_LOG_PRS_NUMBER_OF_SUBFRAMES]; 

  /*! Accumulated MSTMR adjustment, in Ts units, Q0 format. */
  int64    mstmr_adj[LTE_LL1_LOG_PRS_NUMBER_OF_SUBFRAMES];
  int64    mstmr_adj_offset_logging[LTE_LL1_LOG_PRS_NUMBER_OF_SUBFRAMES];

  /*! cell ID of the serving sector in this PRS occasion */
  uint16  serv_cell_id; 
      
  /*! PRS Meas Result Sequence Number. This will be used 
    by GNSS SW to check if the data they read have been
    overwritten or not.
  */
  uint32  occasion_seq_num;

  /*! the number of the prs cell data in this prs occasion */
  uint8  num_cell; 
  
  /*! Flag indicating if FD DMA was newly done for this prs occasion or not */
  boolean fd_dma_is_new;

  /*! TDD_FDD mode: 0=FDD, 1=TDD */
  uint8 duplex_mode;

  /*! SF bitmask. LSB corresponds to first subframe */
  uint32 sf_bit_mask;

  boolean fd_buffer_logging;

  /*! SF Info Debug parameters */
  #ifdef PRS_SFINFO_DBG_LOGGING
  int64 dl_subframe_ref_time; ///< ref time at start of DL subframe
  int64 dl_subfn_ref_time_raw;///< ref time without mstmr correction
  int64 dl_subframe_ref_time_offline;
                              ///< ref time at start of DL subframe until
                              ///< sample recording is ON = dl_subframe_ref_time
                              ///< Once sample recording is frozen, 
                              ///< dl_subframe_ref_time_offline is also frozen.

  int64 dl_subframe_ext_mstmr;///< unwrapped MSTMR count at DL subframe
  int64 dl_subframe_rtc;      ///< ref time without TTL correction and mstmr correction
  uint32 dl_subframe_cnt;     ///< subframe counter
  int32 dl_subframe_mstmr;    ///< O_STMR MSTMR count at start of DL subframe
  uint32 dl_sf_univ_stmr_cnt; ///< UNIV_STMR MSTMR count at start of DL subframe
  int32 dl_subframe_sr_addr;  ///< SR addr at start of DL subframe
  int32 ul_subframe_mstmr;    ///< O_STMR MSTMR count at start of UL subframe
  uint32 ul_sf_univ_stmr_cnt; ///< UNIV_STMR MSTMR count at start of UL subframe
  int16 subframe_num;         ///< subframe number 
  int16 frame_num;            ///< radio frame number
  uint16 subframe_frame_num;  ///< bits 0~3  = subframe number
  #endif //PRS_SFINFO_DBG_LOGGING
  
  /*! Header containing relevant info for PRS measurement vector */
  lte_LL1_prs_cell_desc_s cell_info[LTE_LL1_PRS_NUM_CELL_MAX]; 
  
} lte_LL1_prs_occasion_desc_s;

/*! @brief cell-specific PRS Meas Data 
*/
typedef struct
{
  /*! Vector containing PRS measurement of a single cell, 
    consisting of 2048 array of 12-bit data.
  */
  uint32 cer_vector[LTE_LL1_PRS_CELL_SIZE_MAX]; 
  
} lte_LL1_prs_cell_data_s;

/*! @brief PRS Meas descriptor and data for the 
    whole PRS occasion. This will be written to
    SharedMem.
*/
typedef struct
{
  
  /*! Individual Cell Data, up to 3 cells */
//#ifndef THOR_TODO   
//*********** merging CR 705739, cl 6454998 bringing API changes only PRS capacity increase and dynamic gain control

  //uint32 cell_data[LTE_LL1_PRS_NUM_CELL_MAX][LTE_LL1_PRS_CELL_SIZE_MAX]; 
//#else
  uint32 cell_data[LTE_LL1_PRS_CER_BUF_SIZE_IN_WD]; //8*2048
//#endif

  /*! PRS Occasion Meas Descriptor */
  lte_LL1_prs_occasion_desc_s occasion_info;

} lte_LL1_prs_occasion_meas_s;

/*===========================================================================

                   OPCRS DEFINITIONS AND TYPES

===========================================================================*/
#define LTE_LL1_OPCRS_NUM_MEAS_SRV_CELL_MAX 3
#define LTE_LL1_OPCRS_NUM_MEAS_NBR_CELL_MAX 3

/*! @brief OpCRS Serving Cell measurement type.
*/
typedef struct
{
  uint32 config_mask;                  ///< Bitfield mask for indicator flags. Currently empty. 
  uint32 seq_num;                      ///< sequence number.
  int32  serving_cell_id;              ///< Range: 0...503
  int32  sub_frame_number;             ///< Sub-frame number (Range 0..9)
  int32  system_frame_number;          ///< System frame number (range 0..1023)
  int32  cer_size;                     ///< Range: 1..512
  uint32 earfcn;                       ///< lte_earfcn_t: Absolute cell's frequency Range: 0..4294967295
                                       ///< Defined in 36.1.1 Section 5.4.4.
  uint8  energy_type;                  ///< Encoded as:
                                       ///<   0x0: signal energy
                                       ///<   0x1: signal + noise energy
  uint8  carrier_index;                ///< 0-PCC, 1-SCC_1, 2-SCC_2     
  uint8  num_tx_ant;                  ///< 1, 2 or 4
  uint8  num_rx_ant;                  ///< Currently always 2. Field kept in API for future purpose. 
  uint8  cp_mode;                      ///< lte_l1_cp_e: 0-Normal CP, 1-Extended CP
  uint16 ue_proc_mode;                 ///< UE PROC MODE: 
                                       ///    LTE_LL1_UE_PROC_MODE_ON_LINE,                 // on-line: Process 1 symbol at a time
                                       ///<                                                    as they are stored in sample buffer
                                       ///<   LTE_LL1_UE_PROC_MODE_ON_LINE_INIT_ACQ_PBCH, //indicates UE is in init-acq state
                                       ///<   LTE_LL1_UE_PROC_MODE_ON_LINE_FSCAN,         //indicates UE is in freq scan state
                                       ///<   LTE_LL1_UE_PROC_MODE_ON_LINE_TRAFFIC,       //indicates UE is in connected state
                                       ///<   LTE_LL1_UE_PROC_MODE_ON_LINE_IRAT,          //indicates UE is in IRAT state                                                                                                                
                                       ///<   LTE_LL1_UE_PROC_MODE_OFF_LINE,              //off-line: Store ~1m of samples, then
                                       ///<                                                           process without collecting more samples

  uint16 ue_res_mode;                  ///< UE RES MODE : enum value 0=NO LTE, 1=IDLE, 2=IRAT, 3=SRCH ONLY, 4=FULL
  uint8  reserved;                     ///< Reserved.
  uint16 crs_ic_enable;                ///< 0 = false, 1 = enable
  uint16 crs_ic_cell_order;            ///< [0, 1, 2, 3]
  int32  ttl_adj;
  int32  ttl_adj_near[3];
  int32  com_adj;
  int64  mstmr_adj;
  int64  mstmr_adj_near[3];  
  uint32 bandwidth;
  int64  dl_subframe_ref_time;         ///< ref time at start of DL subframe
  int64  dl_subframe_ref_time_no_adj;  ///< DL ref time without any adjustments
  int64  dl_subfn_ref_time_raw;        ///< ref time without mstmr correction
  int64  dl_subframe_ref_time_offline; ///< ref time at start of DL subframe until
                                       ///< sample recording is ON = dl_subframe_ref_time
                                       ///< Once sample recording is frozen, 
                                       ///< dl_subframe_ref_time_offline is also frozen.
  uint32 dl_subframe_cnt;              ///< subframe counter
  int32  dl_subframe_mstmr;            ///< O_STMR MSTMR count at start of DL subframe
  //int32  dl_subframe_mstmr_no_adj;     ///< O_STMR MSTMR count for DL subframe without adjustments
  uint32 dl_sf_univ_stmr_cnt;          ///< UNIV_STMR MSTMR count at start of DL subframe
  //uint32 dl_sf_univ_stmr_cnt_no_adj;   ///< UNIV_STMR MSTMR count for DL subframe without adjustments
  int32  dl_subframe_sr_addr;          ///< SR addr at start of DL subframe
  int32  ul_subframe_mstmr;            ///< O_STMR MSTMR count at start of UL subframe
  uint32 ul_sf_univ_stmr_cnt;          ///< UNIV_STMR MSTMR count at start of UL subframe

  uint16 cer_per_rx_tx_pair[LTE_MAX_NUM_UE_RX_ANT][512];      ///< CER vector of maximum 512 taps. (#taps depend on LTE DL BW)
} lte_LL1_opcrs_srv_meas_s;

/*! @brief neighbor cell cer struct
 */
typedef struct
{
  uint16 per_tap_cer_for_rx0;         ///< Unitless energy metric. Range: 0-65535
  uint16 per_tap_cer_for_rx1;         ///< Unitless energy metric. Range: 0-65535
}lte_LL1_opcrs_ncell_cer_tap_s;

/*! @brief OpCRS Neighbor Cell measurement type.
*/
typedef struct
{
  uint32 config_mask;                 ///< Bitfield mask for indicator flags. Currently empty. 
  uint32 seq_num;                     ///< sequence number.
  int32  cell_id;                     ///< Range: 0...503
  int16  sub_frame_number;            ///< Sub-frame number (Range 0..9)
  int16  system_frame_number;         ///< System frame number (range 0..1023)
  uint32 earfcn;                      ///< lte_earfcn_t: Absolute cell's frequency Range: 0..4294967295
                                      ///< Defined in 36.1.1 Section 5.4.4.
  uint16 carrier_index;               ///< 0-PCC, 1-SCC_1, 2-SCC_2     
  uint8  num_tx_ant;                  ///< Encoded as:
                                      ///<   0x0: 1 antenna
                                      ///<   0x1: 2 antennas
                                      ///<   0x2: 4 antennas
  uint8  num_rx_ant;                  ///< Encoded as:
                                      ///<   0x0: 1 antenna
                                      ///<   0x1: 2 antennas 
  uint16 ue_proc_mode;                 ///< UE PROC MODE: 
                                       ///    LTE_LL1_UE_PROC_MODE_ON_LINE,                 // on-line: Process 1 symbol at a time
                                       ///<                                                    as they are stored in sample buffer
                                       ///<   LTE_LL1_UE_PROC_MODE_ON_LINE_INIT_ACQ_PBCH, //indicates UE is in init-acq state
                                       ///<   LTE_LL1_UE_PROC_MODE_ON_LINE_FSCAN,         //indicates UE is in freq scan state
                                       ///<   LTE_LL1_UE_PROC_MODE_ON_LINE_TRAFFIC,       //indicates UE is in connected state
                                       ///<   LTE_LL1_UE_PROC_MODE_ON_LINE_IRAT,          //indicates UE is in IRAT state                                                                                                                
                                       ///<   LTE_LL1_UE_PROC_MODE_OFF_LINE,              //off-line: Store ~1m of samples, then
                                       ///<                                                           process without collecting more samples

  uint16 ue_res_mode;                  ///< UE RES MODE : enum value 0=NO LTE, 1=IDLE, 2=IRAT, 3=SRCH ONLY, 4=FULL
  int64  scell_dl_subframe_ref_time;  ///< ref time at start of DL subframe
  int64  scell_dl_subframe_ref_time_no_adj;  ///< ref time at start of DL subframe w/o adjustments
  int64  scell_dl_subfn_ref_time_raw; ///< ref time without mstmr correction
  int64  scell_dl_subframe_ref_time_offline; ///< ref time at start of DL subframe until
                                             ///< sample recording is ON = dl_subframe_ref_time
                                             ///< Once sample recording is frozen, 
                                             ///< dl_subframe_ref_time_offline is also frozen.
  int64  scell_dl_subframe_ext_mstmr; ///< unwrapped MSTMR count at DL subframe
  uint32 scell_dl_subframe_cnt;       ///< subframe counter
  int32  scell_dl_subframe_mstmr;     ///< O_STMR MSTMR count at start of DL subframe
  //int32  scell_dl_subframe_mstmr_no_adj;     ///< O_STMR MSTMR count at start of DL subframe w/o adjustments
  uint32 scell_dl_sf_univ_stmr_cnt;   ///< UNIV_STMR MSTMR count at start of DL subframe
  //uint32 scell_dl_sf_univ_stmr_cnt_no_adj;   ///< UNIV_STMR MSTMR count at start of DL subframe w/o adjustments
  int32  scell_dl_subframe_sr_addr;   ///< SR addr at start of DL subframe
  int32  scell_ul_subframe_mstmr;     ///< O_STMR MSTMR count at start of UL subframe
  uint32 scell_ul_sf_univ_stmr_cnt;   ///< UNIV_STMR MSTMR count at start of UL subframe
  int16  scell_subframe_num;          ///< subframe number 
  int16  scell_frame_num;             ///< radio frame number
  int32  window_start_offset;         ///< NB sample offset at the start of subframe to schedule    
  uint32 frame_bndry_ref_time[2];     ///<  Neighbor cell frame boundary per Rx antenna as a modulo 10ms offset.
                                      ///<  Including neighbor cell TTL adjustment.
                                      ///<  <EM> Range: [0, 307199] </EM>
  int16  total_timing_adj_cir[2];     ///<  Total timing adjustment in CIR domain per Rx antenna to drive TTL
  int32  ttl_adj;
  int32  com_adj;
  int64  mstmr_adj;
             
  lte_LL1_opcrs_ncell_cer_tap_s ncell_cer[32];///< CER buffer storing rx0 and rx1 pairs. 
  uint32 raw_ee_rx[2];                ///< Raw Energy estimate value (Q31) per Rx */
  int32  lna_gain_rx[2];              ///< Adjustment for LNA state (Q24?) in dB per Rx */
  uint32 exp_rx[2];                   ///< NE minimum exponent per Rx antenna

} lte_LL1_opcrs_nbr_meas_s;

/*! @brief Shared memory buffer to store Opportunistic CRS measurements.
*/
typedef struct
{
  /*! Current array index for OpCRS serving cell measurement */
  uint8 srv_cell_idx;

  /*! Current array index for OpCRS neighbor cell measurement */
  uint8 nbr_cell_idx;

  /*! Array of OpCRS serving cell measurement */
  lte_LL1_opcrs_srv_meas_s srv_meas[LTE_LL1_OPCRS_NUM_MEAS_SRV_CELL_MAX];

  /*! Array of OpCRS neighbor cell measurement */
  lte_LL1_opcrs_nbr_meas_s nbr_meas[LTE_LL1_OPCRS_NUM_MEAS_NBR_CELL_MAX];
 
} lte_LL1_opcrs_meas_db_s;

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/


#endif /* INTF_PRS_SM_H */
