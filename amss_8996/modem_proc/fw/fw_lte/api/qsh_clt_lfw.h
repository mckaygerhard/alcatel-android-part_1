/*!
  @file
  qsh_clt_lfw.h

  @brief
  Header file for defining mini-set db

  @detail
  description of functions in this file
 
*/

/*===========================================================================

  Copyright (c) 2009 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/modem/fw/cpl/thor/MPSS.TH.2.0.C1.9/image/fw_lte/api/qsh_clt_lfw.h#34 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/06/15          Feature to enable mini dump 

===========================================================================*/

#ifndef QSH_CLT_LFW_H
#define QSH_CLT_LFW_H


/*===========================================================================

      Constants

===========================================================================*/
#define LFW_QSH_MAJOR_VER 1
#define LFW_QSH_MINOR_VER 0


// The enum lists the bit position in log_mask, to enable logging for each module 
typedef enum
{
  LFW_QSH_DUMP_TAG_TOP = 0,
  LFW_QSH_DUMP_TAG_RXAGC,
  LFW_QSH_DUMP_TAG_DBE,
  LFW_QSH_DUMP_TAG_RXFE,
  LFW_QSH_DUMP_TAG_IRAT,
  LFW_QSH_DUMP_TAG_DF,
  LFW_QSH_DUMP_TAG_MEAS,
  LFW_QSH_DUMP_TAG_UL,
  LFW_QSH_DUMP_TAG_FBRX,
  LFW_QSH_DUMP_TAG_NLIC,
  LFW_QSH_DUMP_TAG_QTA,
  LFW_QSH_DUMP_TAG_SRCH,
  LFW_QSH_DUMP_TAG_VPE,
  LFW_QSH_DUMP_TAG_OFFDRX,
  LFW_QSH_DUMP_TAG_MACROSLEEP,
  LFW_QSH_MAX_DUMP_TYPE 
}lfw_qsh_dump_tag_e;


/*===========================================================================

      Typedefs

===========================================================================*/


/* RxAGc database, common and carrier based */

typedef struct
{
  uint8      curr_rx_ant_bmask;
  uint32     num_one_rx_sfs;
  uint32     num_sfs_after_one_rx_mode_exit;
  int32      curr_agc_mode;
  boolean    update_LNA_state;

} lte_LL1_miniset_rxagc_carrier_db_t;

typedef struct
{
  boolean dummy;
} lte_LL1_miniset_rxagc_common_db_t;

/* Demback database - common and carrier based*/

typedef struct
{
  atomic_word_t pdcch_cmd_rsp_diff_cnt;

}lte_LL1_miniset_dbe_carrier_db_t;

typedef struct
{
  uint8   num_activated_carr;
  uint8   num_lte_dbe;

}lte_LL1_miniset_dbe_common_db_t;

/*  Command Processing internals  */
typedef struct
{
  uint32     sys_time_dl_sched_ref_time;
  uint16     sys_time_dl_sched_frame_num;
  uint16     sys_time_ud_offset_state;
  uint16     sys_time_ta_adj_state;
  uint8      sys_time_dl_sched_subframe_num;
  uint8      ca_db_rx_on;
  uint16     dl_config_enable_mask;
  uint16     dl_config_disable_mask;
  uint16     dl_config_resume_mask;
  uint16     dl_config_rxagc_start_mode; //lte_LL1_rxagc_mode_e Range 0 - 15
  uint16     ue_cfg_rx_enabled_mods;
  uint8      ue_cfg_rx_samp_rate_sel;
  uint8      sys_cnf_app_seq_id;

  boolean    ca_db_is_pending;
  boolean    ca_db_is_pending_irat_thread;
  boolean    dl_config_is_pending;
  boolean    dl_config_is_disable_pending;
  boolean    slam_rot_is_pending;
  boolean    slam_rot_is_pending_irat_thread;
  boolean    sys_cnf_app_is_pending;
  boolean    start_samp_rec_is_pending;
  boolean    start_samp_rec_is_pending_for_fscan;
  boolean    start_samp_rec_is_pending_irat_thread;
  uint8      start_samp_rec_curr_samp_buf; //lte_LL1_samp_serv_e Range 0-3
  boolean    stop_samp_rec_is_pending;
  boolean    stop_samp_rec_is_pending_for_fscan;
  boolean    stop_samp_rec_is_pending_delayed; // for 2nd stop
  boolean    rf_script_is_pending;
  boolean    rf_script_configure_rxfe_without_tune_is_pending;
  boolean    dl_schdr_config_is_pending;               /* RF wakeup mode setting is pending */
  boolean    dl_schdr_config_is_not_dl_sf;
  uint8      scell_db_stat_dl_cp_mode;
  uint8      scell_db_stat_duplexing_mode;
  uint8      scell_db_stat_dl_bandwidth;
  uint8      scell_db_stat_num_eNb_tx_ant;

  boolean    scell_db_semi_stat_tdd_is_valid;
  uint8      scell_db_semi_stat_tdd_ul_dl_cfg;
  uint8      scell_db_semi_stat_dl_tdd_ssf_cfg;

  uint8      scell_db_mbsfn_cfg_type;
  uint8      scell_db_mbsfn_is_enabled; 

} lte_LL1_miniset_sys_sched_carrier_db_t;


typedef struct
{
  uint32  cmd_proc_cmn_res_config_pending;
  uint32  cmd_proc_cmn_res_mode_pending;
  uint32  cmd_proc_cmn_proc_mode;
  uint8   cmd_proc_nb_switch_activate_deactivate_mask[2];
  uint8   cmd_proc_nb_switch_carrier[2];
  uint16   cmd_proc_nb_switch_operation_type[2];
  uint8   sleep_cmn_num_carriers;
  uint16   ue_cfg_wake_up_mode;
  uint8   ue_cfg_res_useage_mode;
  uint8   ue_cfg_proc_mode;
  uint8   ue_cfg_sys_mode;
  uint16  sleep_cmn_wakeup_subfn;
  uint16  sleep_cmn_wakeup_sfn;
  uint16  sleep_cmn_sleep_mode;
  boolean sleep_cmn_is_pending;
  boolean ue_cfg_is_conf_app_cnf_send;
} lte_LL1_miniset_sys_sched_common_db_t;

/* RxFE -  Front end internal state variables */
typedef union
{
  struct {
    uint8 nb_idx_0:4;
    uint8 nb_idx_1:4;
    uint8 nb_idx_2:4;
    uint8 nb_idx_3:4;
    uint8 wb_idx:4;
    uint8 adc_idx:4;
    uint8 rxch_idx:4;
    uint8 unused:4;
  };
  uint32 intval;
} lte_LL1_miniset_fw_rxlm_enabled_chain_s;

typedef struct
{
  boolean dummy;
}lte_LL1_miniset_rxfe_common_db_t;

typedef struct
{
  boolean rxfe_rxlm_params_chains_requested;
  boolean rxfe_rxlm_params_is_wb_vsrc_running;
  uint8   rxfe_rxlm_params_rxlm_buf_idx[2];
  lte_LL1_miniset_fw_rxlm_enabled_chain_s chain[2];

}lte_LL1_miniset_rxfe_carrier_db_t;

/* IRAT -  Gap management state for IRAT */
typedef struct
{

  uint8      irat_gap_type; // lte_LL1_gap_type_e : Range 0 - 5
  uint8      irat_rxagc_mode; // lte_LL1_rxagc_mode_e Range 0 - 15
  uint8      irat_ping_pong_sample_buff_sel; //lte_LL1_gap_sample_buff_select_e; Range 0-2
  boolean    irat_sample_server_stop_pending;
  uint32     irat_proc_done_callback_idx;
  uint32     irat_num_g3_cb_received;
  boolean    irat_lte_LL1_gap_manager_sync_pt_table[7];
  uint8      irat_gap_sf_counter[4];
  uint32     irat_rf_tune_irat_thread_start;
  uint32     irat_rf_tune_irat_thread_end;
  uint32     irat_rf_tune_tune_start;
  uint32     irat_rf_tune_tune_end;
  uint32     irat_rf_tune_vpe_image_load_start;
  uint32     irat_rf_tune_vpe_image_load_end;
  uint32     irat_rf_tune_actual_tune_time;

}lte_LL1_miniset_irat_common_db_t;


typedef struct
{
  boolean dummy; 
} lte_LL1_miniset_irat_carrier_db_t;


/* Demfront - CHEST parameters and state */

typedef struct
{
  boolean dummy;
} lte_LL1_miniset_demfront_common_db_t;

typedef struct
{
  boolean demfront_kalman_enable;
  boolean demfront_crsic_enable;
  uint8   demfront_num_cells;
  uint8   demfront_ping_pong_idx;
  boolean demfront_pbch_ic_buf_update_en;
  boolean demfront_rsrp_rssi_update_en;
  uint8   demfront_fd_sym_buf_idx;

} lte_LL1_miniset_demfront_carrier_db_t;


/* Measurement state, status */

typedef struct
{
  uint16     mtf_ncell_schd_param_dl_subframe_num;
  uint16     mtf_ncell_schd_param_dl_frame_num;         
  int64      mtf_ncell_schd_param_dl_subframe_ref_time; 
  uint16     mtf_ncell_schd_param_nb_smpl_offset;       
  uint16     mtf_ncell_schd_param_num_smpl_in_ss;       
  uint32     mtf_ncell_schd_param_scell_start_ustmer;   
} lte_LL1_miniset_meas_common_db_t;

typedef struct
{

  boolean    meas_ncell_enable;
  boolean    meas_ncell_vpe_type;          // type of VPE used for neighbor measurement, srcch/non srch
  boolean    meas_ncell_dyn_is_conn_mode;  ///< 0: DRX
                                                   ///< 1: Connected mode
  // From meas_ttl_ftl_ncell                       ///
  uint8      meas_dyn_msg_samp_rec_buf;            ///< To indicate which buffer is used for sample recording
  uint8      meas_dyn_msg_num_ncells;                  ///< Total number of neighbor cells that are measured and tracked
  uint8      meas_dyn_msg_duplexing_mode;          ///< duplexing mode Range 0 - 2
  uint8      meas_dyn_msg_carrier_idx;             ///< Enum for dl carriers in  CA
  uint8      meas_dyn_msg_nb_id;                   // Enum for NB ID ; Range 0 -2
  boolean    meas_dyn_msg_use_offline_samples;
  uint8      meas_first_sp_offset_due_to_gap;      ///< Due to an LTE gap, upt to 7ms of samples are streamed
                                                   ///< in the presence of an MBSFN schedule and in TDD before task
                                                   ///< scheduling for connected mode neighbor cell
                                                   ///< measurements.
  // From meas_ttl_ftl_ncell                      ///< Range: 0 ~ 7 (in ms)
  uint8      meas_conn_drx_un_conn_T_avail;        ///< ML1 specify directly; FW used to compute it for each SP based on gap pattern
  uint8      meas_conn_drx_un_drx_num_ms;          ///< Number of SFs averaged before computing RSRP/RSRQ for a neighbor cell
  uint8      meas_conn_drx_un_drx_meas_tx_mode;    ///< 0: average over 2 tx; 1 report tx0 2 report max tx0 and tx1
  uint8      meas_dyn_schedule_sf_schd_ofs;        ///< SF count in current SP (scheduling)
  uint8      meas_dyn_schedule_sf_proc_ofs;        ///< SF count in current SP (back-end processing)


  uint8      meas_scell_state;                                   ///lte_LL1_meas_scell_state_e Range 0 - 3
  uint8      meas_scell_dyn_meas_delay_sf_cnt;                   ///< SF counter for delaying measurement schedule
  int64      meas_scell_dyn_sched_subframe_ref_time;             ///< ref time at scheduled subframe
  int16      meas_scell_dyn_sched_frame_num;                     ///< frame # of scheduled subframe
  int8       meas_scell_dyn_sched_subframe_num;                  ///< subframe # of scheduled subframe

} lte_LL1_miniset_meas_carrier_db_t;


/* Tx - Uplink internal state variables */
typedef struct
{
#if 0 // Move to carr params for Thor 
  uint16                ul_ch_db_dyn_timing_info_sys_time_subframe_num;                  ///< 0 ~9
  uint16                ul_ch_db_dyn_timing_info_sys_time_frame_num;                    ///< radio frame number
#endif
  uint32                ul_fed_input_params_fed_call_start_mstmr_time[8];
  uint32                ul_fed_input_params_fed_call_return_mstmr_time[8];
} lte_LL1_miniset_ul_common_db_t;

typedef struct
{
  uint16    operating_state                : 16;  /* W[0] [15: 0] */
  uint16    ping_pong_state                : 16;  /* W[0] [31:16] */
  uint16    cmd_valid                      : 16;  /* W[1] [15: 0] */
  uint16    auto_sleep                     : 16;  /* W[1] [31:16] */
  uint32    stmr                           : 24;  /* W[2] [23: 0] */
} lte_LL1_miniset_vpe_lte_tx_syncp_t;

typedef struct
{
  uint32                                 vpe_ul_schdr_debug_tx1_done_ustmr_dump[10];
  uint32                                 vpe_ul_schdr_debug_cmd_valid_assertion_ustmr;
  lte_LL1_miniset_vpe_lte_tx_syncp_t     vpe_ul_schdr_debug_grp_sync_read_hist[10];
  lte_LL1_miniset_vpe_lte_tx_syncp_t     vpe_ul_schdr_debug_grp_sync_write_hist[10];
  uint32                                 vpe_ul_schdr_debug_syncp_write_times[10];
  uint8                                  ul_fed_input_params_ul_subframe_type; //rflm_lte_ul_chan_type_e Range 0 - 255
  boolean                                ul_fed_input_params_slot_active[4];
  uint8                                  ul_fed_input_params_sf_num;
  uint32                                 ul_fed_input_params_action_time[4];
  uint32                                 ul_fed_input_params_frame_num;
  boolean                                ul_fed_input_params_tx_dac_active;
  uint16                                 ul_ch_db_dyn_timing_info_sys_time_subframe_num;   ///< 0 ~9
  uint16                                 ul_ch_db_dyn_timing_info_sys_time_frame_num;      ///< radio frame number
} lte_LL1_miniset_ul_carrier_db_t;

/* FBRX */
typedef struct
{
  boolean dummy; 
}lte_LL1_miniset_fbrx_common_db_t;

typedef struct
{
  boolean             fbrx_config_en;
  uint32              fbrx_action_time_mstmr;
  uint16              fbrx_state_frame_num;
  uint8               fbrx_state_subframe_num;
}lte_LL1_miniset_fbrx_carrier_db_t;

/* NLIC  internal state */

typedef struct
{
  boolean dummy;
}lte_LL1_miniset_nlic_common_db_t;

typedef struct
{
  uint32     nlic_vu_state_vpe_nlic_schdr_state;
  boolean    nlic_vu_state_ping_pong_state;
  uint8      nlic_vu_state_cur_chan_type; // lte_LL1_ul_chan_type_enum Range 0 - 7
  uint32     nlic_vu_state_current_sf_nlic_en;
  uint8      rflm_lte_nlic_nlic_mode[8];
  uint8      rflm_lte_nlic_clock_mode[8];
  uint32     nlic_done_ustmr_dump[10];
  uint32     vpe_nlic_schdr_cmd_valid_assertion_ustmr;

}lte_LL1_miniset_nlic_carrier_db_t;

/* QTA  - Quick Tune Away start and end indications */
typedef struct
{
  uint16 conflict_detect;
  uint16 conflict_rat;
  uint32 conflict_start_time_in_sf;
  uint32 conflict_end_time_in_sf;
}lte_LL1_miniset_qta_conflict_t;

typedef struct
{
  boolean dummy;
}lte_LL1_miniset_qta_carrier_db_t;

typedef struct 
{
  lte_LL1_miniset_qta_conflict_t conflicts_db[10];
  boolean                        conflict_active;
  boolean                        lte_LL1_qta_dl_active;
  boolean                        tune_pending;
  boolean                        lte_LL1_blanking_en;
  uint16                         fw_sim_intf_prio;
  boolean                        qta_end;
  uint64                         conflict_start_sf_ref_time;
  uint64                         conflict_end_sf_ref_time;
}lte_LL1_miniset_qta_common_db_t;  

/* SRCH - Search internal state variables logged */

typedef struct 
{
  boolean dummy;
}lte_LL1_miniset_srch_common_db_t;  

typedef struct
{
  boolean     srch_state_abort_init_acq_abort_pending;
  boolean     srch_state_abort_ncell_abort_pending;
  boolean     srch_state_abort_freq_scan_abort_pending;
  uint8       srch_state_abort_req_seq_id;
  uint8       srch_state_abort_pending_cnf_status; //lte_LL1_conf_status_enum Range 0-3
  boolean     srch_state_ncell_srch_is_active;
  uint8       srch_state_ncell_srch_req_seq_id;
  uint8       srch_state_ncell_srch_pending_cnf_status; //lte_LL1_conf_status_enum Range 0-3
  boolean     srch_state_init_acq_is_active;
  uint8       srch_state_init_acq__req_seq_id;
  uint8       srch_state_init_aqc_pending_cnf_status; //lte_LL1_conf_status_enum Range 0 - 3
  uint8       srch_state_hw_schd;                     //lte_LL1_srch_hw_schedule_state_enum_t Range 0 - 3
  uint8       srch_state_fs_hw_schd;                  //lte_LL1_srch_fs_hw_schd_state_enum_t Range 0 - 2
  boolean     srch_state_full_freq_scan_is_active;
  uint8       srch_state_full_freq_scan_req_seq_id;
  uint8       srch_state_full_freq_scan_pending_cnf_status; //lte_LL1_conf_status_enum Range 0-3
  boolean     srch_state_list_freq_fscan_is_active;
  uint8       srch_state_list_freq_fscan_req_seq_id;
  uint8       srch_state_list_freq_fscan_pending_cnf_status; //lte_LL1_conf_status_enum Range 0 -3

}lte_LL1_miniset_srch_carrier_db_t;


/* VPE  - Processing state for underlying microkernel  */
typedef struct
{
  boolean   image_load_valid;        /*!< If type needs to be consumed*/
  uint8      state;                  /*!< State of image */ //Range 0 - 2
  uint8      type;                   /*!< Occaision of image loading */ // Range 0 - 8
  uint8      bandwidth;              /*!< Current bandwidth */ // Range 0 - 6
  boolean    unload_before_load;     /*!< Ignore for all VPEs but VPE0*/
  boolean    bw_changed;

} lte_LL1_miniset_vpe_image_proc_t;

typedef struct
{
  lte_LL1_miniset_vpe_image_proc_t lte_LL1_vpe_image_status[4];
  uint32 vpe_schdr_dbg_univ_stmr[4];
  uint8  vpe_schdr_dbg_curr_index;
  uint8  vpe_sched_fw_state;
  uint32 vpe_sched_syncPt_offset_ustrm;
}lte_LL1_miniset_vpe_common_db_t;

typedef struct
{
  uint32 vpe_sched_next_sf_start_mstmr;
  uint32 vpe_sched_curr_sf_start_ustmr;
  uint32 vpe_sched_next_sf_start_ustmr;

}lte_LL1_miniset_vpe_carrier_db_t;


/* OFFLINE - indication of offline processing state and req/cnf indication is logged */

typedef struct
{
  boolean dummy;
} lte_LL1_miniset_offline_drx_carrier_db_t;


typedef struct
{
  uint16 offline_drx_samp_rec_duration_ms;
  uint8  offline_drx_state_curr_proc_state;           /// Range 0 -9 , 255 ;  current offline processing state
  boolean offline_drx_state_is_active;                /// Offline scheduler is active, meaning there are
                                                      /// still processing/scheduling need to be done before
                                                      /// the offline thread goes back to sleep
  boolean offline_scell_meas_is_meas_in_progress;  /// Scell meas is scheduled
  boolean offline_scell_meas_is_meas_cnf_sent;     /// Flag to indicate the scell_meas_cnf
                                                   /// has been sent to ML
  boolean offline_ncell_req_received_meas_intra;
  boolean offline_ncell_req_received_meas_inter;
  boolean offline_ncell_req_received_srch_intra;
  boolean offline_ncell_req_received_srch_inter;      
  boolean offline_ncell_cnf_sent_meas_intra;
  boolean offline_ncell_cnf_sent_meas_inter;
  boolean offline_ncell_cnf_sent_srch_intra;
  boolean offline_ncell_cnf_sent_srch_inter;

  unsigned int sched_signal_signals;
  unsigned int sched_signal_waiting;
  unsigned int pdsch_task_prog_signal_signals;
  unsigned int pdsch_task_prog_signal_waiting;
  unsigned int allow_sleep_signal_signals;
  unsigned int allow_sleep_signal_waiting;
  unsigned int allow_ifreq_signal_signals;
  unsigned int allow_ifreq_signal_waiting;
} lte_LL1_miniset_offline_drx_common_db_t;

typedef struct
{
  boolean macrosleep_active;
  uint8  sleep_req_carrier_bmsk;
  boolean sched_sleep_from_dl_thread;
  uint16 sleep_frame_num;
  uint16 sleep_subframe_num;

  boolean wakeup_pending;
  uint8  wakeup_carrier_bmsk;
  uint16 wakeup_frame_num;
  uint16 wakeup_subframe_num;

} lte_LL1_miniset_macrosleep_common_db_t; 

typedef struct
{
  uint8 num_sf_to_monitor;
  boolean per_carrier_req_pending[2];
  uint8 subframe_grant_mask;
  boolean is_dl_dis_by_macrosleep;
  boolean is_dcc_accum_store_done;
  boolean is_macrosleep_en_bit_pgmed;
  boolean prev_sf_sched_from_pdcch_irq[2];
  boolean prev_sf_sched_samp_stop[2];
  uint16 enabled_mods;
  uint8 gain_state[2];
  uint8 agc_mode; // Range 0 - 15 
  boolean is_intra_band_ca;
  uint8 rxagc_share_carrier_lna;
  uint32 two_pwr_33_over_dl_freq;
  int32 start_vco_freq_corr;
  int32 start_rot_freq_corr;
} lte_LL1_miniset_macrosleep_carrier_db_t; 


/*  Contains database of all module params per carrier */

typedef struct
{
  lte_LL1_miniset_rxagc_carrier_db_t       rxagc_carrier_db;    // Logged under LFW_QSH_DUMP_TAG_RXAGC
  lte_LL1_miniset_dbe_carrier_db_t         dbe_carrier_db;      // logged under LFW_QSH_DUMP_TAG_DBE
  lte_LL1_miniset_sys_sched_carrier_db_t   sys_sched_carrier_db;// LFW_QSH_DUMP_TAG_TOP
  lte_LL1_miniset_rxfe_carrier_db_t        rxfe_carrier_db;     // LFW_QSH_DUMP_TAG_RXFE
  lte_LL1_miniset_irat_carrier_db_t        irat_carrier_db;     // LFW_QSH_DUMP_TAG_IRAT
  lte_LL1_miniset_demfront_carrier_db_t    demfront_carrier_db; // LFW_QSH_DUMP_TAG_DF
  lte_LL1_miniset_meas_carrier_db_t        meas_carrier_db;     // LFW_QSH_DUMP_TAG_MEAS
  lte_LL1_miniset_ul_carrier_db_t          ul_carrier_db;       // LFW_QSH_DUMP_TAG_UL
  lte_LL1_miniset_fbrx_carrier_db_t        fbrx_carrier_db;     // LFW_QSH_DUMP_TAG_FBRX
  lte_LL1_miniset_nlic_carrier_db_t        nlic_carrier_db;     // LFW_QSH_DUMP_TAG_NLIC
  lte_LL1_miniset_qta_carrier_db_t         qta_carrier_db;      // LFW_QSH_DUMP_TAG_QTA
  lte_LL1_miniset_srch_carrier_db_t        srch_carrier_db;     // LFW_QSH_DUMP_TAG_SRCH
  lte_LL1_miniset_vpe_carrier_db_t         vpe_carrier_db;      // LFW_QSH_DUMP_TAG_VPE
  lte_LL1_miniset_offline_drx_carrier_db_t offline_drx_carrier_db; //LFW_QSH_DUMP_TAG_OFFDRX
  lte_LL1_miniset_macrosleep_carrier_db_t macrosleep_carrier_db; //LFW_QSH_DUMP_TAG_MACROSLEEP

} lte_LL1_miniset_main_carrier_db_t;



/*  Contains database of all module params common to all carriers */

typedef struct
{
  lte_LL1_miniset_rxagc_common_db_t       rxagc_common_db;
  lte_LL1_miniset_dbe_common_db_t         dbe_common_db;
  lte_LL1_miniset_sys_sched_common_db_t   sys_sched_common_db;
  lte_LL1_miniset_rxfe_common_db_t        rxfe_common_db;
  lte_LL1_miniset_irat_common_db_t        irat_common_db;
  lte_LL1_miniset_demfront_common_db_t    demfront_common_db;
  lte_LL1_miniset_meas_common_db_t        meas_common_db;
  lte_LL1_miniset_ul_common_db_t          ul_common_db;
  lte_LL1_miniset_fbrx_common_db_t        fbrx_common_db;
  lte_LL1_miniset_nlic_common_db_t        nlic_common_db;
  lte_LL1_miniset_qta_common_db_t         qta_common_db;
  lte_LL1_miniset_srch_common_db_t        srch_common_db;
  lte_LL1_miniset_vpe_common_db_t         vpe_common_db;
  lte_LL1_miniset_offline_drx_common_db_t offline_drx_common_db;
  lte_LL1_miniset_macrosleep_common_db_t macrosleep_common_db;

} lte_LL1_miniset_main_common_db_t;

/* Contains header used to specify logging start/stop, logmask for modules to be logged and write Idx */
typedef struct
{
  boolean logging_flag;                 // start/stop logging
  uint16  per_module_log_mask;          // Specify the modules (1 bit for each of the above 14 modules) for which logging is enabled
  uint8   num_carrier;                  // Number of carriers being logged (do we need this ? )
  uint32  wrt_idx;                      // Wrt idx points to the start of the module that is being written into.
  uint16  log_version;                  // 

}lte_LL1_miniset_main_header_db_t;

typedef struct
{
  lte_LL1_miniset_main_header_db_t  mini_dump_header_db;
  lte_LL1_miniset_main_common_db_t  mini_dump_common_db;
  lte_LL1_miniset_main_carrier_db_t mini_dump_carrier_db[2];

} lte_LL1_miniset_main_db_t; 

extern void lte_LL1_qsh_clt_lfw_cb(int ); 
extern void lte_Ll1_qsh_clt_lfw_init(void);

#endif /* QSH_CLT_LFW_H */
