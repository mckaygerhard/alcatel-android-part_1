/*!
  @file
  lte_LL1_mvc.h

  @brief
  MVC clock interface for LTE LL1.

  @detail
  Constants and structures for LTE LL1 FW-SW MVC interface.

*/

/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/modem/fw/cpl/thor/MPSS.TH.2.0.C1.9/image/fw_lte/api/lte_LL1_mvc.h#34 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

#ifndef LTE_LL1_MVC_H
#define LTE_LL1_MVC_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "fw_mvc_intf.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*! @brief  Enum for number of Joint Demod cells enabled.
 */ 
typedef enum
{
  MVC_NUM_JOINT_DEMOD_CELLS_1,
  MVC_NUM_JOINT_DEMOD_CELLS_2,
  MVC_NUM_JOINT_DEMOD_CELLS_MAX
} lte_LL1_mvc_num_joint_demod_cells_e;

/*! @brief  Enum for number of CRS-IC cells enabled
 */
typedef enum
{
  MVC_NUM_CRS_IC_CELLS_1,
  MVC_NUM_CRS_IC_CELLS_2,
  MVC_NUM_CRS_IC_CELLS_3,
  MVC_NUM_CRS_IC_CELLS_4,
  MVC_NUM_CRS_IC_CELLS_MAX
} lte_LL1_mvc_num_crs_ic_cells_e;

/*! @brief  Enum for QICE mode carrier/bandwidth combinations.
            QICE supports up to 2CA and aggregate BW <= 20MHz.
 */
typedef enum
{
  MVC_QICE_5MHZ,
  MVC_QICE_10MHZ,
  MVC_QICE_15MHZ,
  MVC_QICE_20MHZ,
  MVC_QICE_5MHZ_5MHZ,
  MVC_QICE_5MHZ_10MHZ,
  MVC_QICE_10MHZ_10MHZ,
  MVC_QICE_5MHZ_15MHZ,
  MVC_QICE_MAX_BW,
} lte_LL1_mvc_qice_mode_bw_e;

/*! @brief  Enum for transmission mode range.
 */
typedef enum
{
  MVC_TM_1TO6,
  MVC_TM_7TO9,
  MVC_MAX_TM
} lte_LL1_mvc_tm_mode_type_e;

/*! @brief  Data-IC clock table entry.
            
    @description
    If the number of RBs within a PDSCH grant meets or exceeds the RB
    threshold, then the entry's clock level is required for data-IC.
    Otherwise, the default clock is sufficient (i.e. no MVC vote needed).
 */
typedef struct
{
  uint8           rb_threshold;
  fw_mvc_corner_e clock;
} lte_LL1_mvc_data_ic_clock_s;

/* @brief Table for LTE MVC clock regimes
 */
typedef struct
{
  /* Data-IC clock assumptions:
     a) This table is only valid when MVC is enabled
     b) This table is only consulted when at least one cell has
        joint demod (and CRS-IC, by extension) enabled by ML1
     c) The number of CRS-IC cells reflects the total number
        enabled, across all carriers
     d) The transmission mode selection reflects the max TM,
        among all carriers
   */
  lte_LL1_mvc_data_ic_clock_s data_ic[MVC_NUM_JOINT_DEMOD_CELLS_MAX]
                                     [MVC_NUM_CRS_IC_CELLS_MAX]
                                     [MVC_QICE_MAX_BW]
                                     [MVC_MAX_TM];
} lte_LL1_mvc_clock_table_s;

#endif /* LTE_LL1_MVC_H */

