#ifndef QFE4465FC_GSM_TYPEDEF_AG_H
#define QFE4465FC_GSM_TYPEDEF_AG_H


#include "comdef.h"

/*----------------------------------------------------------------------------*/
/*!
  It defines the QFE4465FC_GSM pa_port enum.
*/
typedef enum 
{
  QFE4465FC_GSM_GSM_BAND1800_PORT_ANT, 
  QFE4465FC_GSM_GSM_BAND1900_PORT_ANT, 
  QFE4465FC_GSM_PORT_NUM, 
  QFE4465FC_GSM_PORT_INVALID, 
} qfe4465fc_gsm_pa_port_data_type;


#endif