#ifndef QFE4465FC_TYPEDEF_AG_H
#define QFE4465FC_TYPEDEF_AG_H


#include "comdef.h"

/*----------------------------------------------------------------------------*/
/*!
  It defines the QFE4465FC pa_port enum.
*/
typedef enum 
{
  QFE4465FC_CDMA_BAND1_PORT_TX_INT_0, 
  QFE4465FC_CDMA_BAND6_PORT_TX_INT_1, 
  QFE4465FC_CDMA_BAND15_PORT_TX_INT_2, 
  QFE4465FC_CDMA_BAND15_PORT_TX_MB1_3, 
  QFE4465FC_WCDMA_BAND1_PORT_TX_INT_4, 
  QFE4465FC_WCDMA_BAND2_PORT_TX_INT_5, 
  QFE4465FC_WCDMA_BAND3_PORT_TX_INT_6, 
  QFE4465FC_WCDMA_BAND3_PORT_TX_MB1_9, 
  QFE4465FC_WCDMA_BAND9_PORT_TX_INT_6, 
  QFE4465FC_WCDMA_BAND9_PORT_TX_MB1_9, 
  QFE4465FC_WCDMA_BAND4_PORT_TX_INT_7, 
  QFE4465FC_WCDMA_BAND4_PORT_TX_MB1_10, 
  QFE4465FC_WCDMA_BAND25_PORT_TX_INT_8, 
  QFE4465FC_WCDMA_BAND25_PORT_TX_MB1_11, 
  QFE4465FC_WCDMA_BAND25_PORT_TX_MB2_12, 
  QFE4465FC_WCDMA_BAND10_PORT_TX_MB1_10, 
  QFE4465FC_LTE_BAND1_PORT_TX_INT_13, 
  QFE4465FC_LTE_BAND1_PORT_TX_INT_28, 
  QFE4465FC_LTE_BAND2_PORT_TX_INT_14, 
  QFE4465FC_LTE_BAND2_PORT_TX_INT_29, 
  QFE4465FC_LTE_BAND3_PORT_TX_INT_15, 
  QFE4465FC_LTE_BAND3_PORT_TX_MB1_18, 
  QFE4465FC_LTE_BAND3_PORT_TX_INT_30, 
  QFE4465FC_LTE_BAND3_PORT_TX_MB1_31, 
  QFE4465FC_LTE_BAND9_PORT_TX_INT_15, 
  QFE4465FC_LTE_BAND9_PORT_TX_MB1_18, 
  QFE4465FC_LTE_BAND4_PORT_TX_INT_16, 
  QFE4465FC_LTE_BAND4_PORT_TX_MB1_19, 
  QFE4465FC_LTE_BAND25_PORT_TX_INT_17, 
  QFE4465FC_LTE_BAND25_PORT_TX_MB1_20, 
  QFE4465FC_LTE_BAND25_PORT_TX_MB2_22, 
  QFE4465FC_LTE_BAND10_PORT_TX_MB1_19, 
  QFE4465FC_LTE_BAND34_PORT_TX_MB1_21, 
  QFE4465FC_LTE_BAND34_PORT_TX_MB2_23, 
  QFE4465FC_LTE_BAND39_PORT_TX_MB2_24, 
  QFE4465FC_LTE_BAND39_PORT_TX_MB2_32, 
  QFE4465FC_TDSCDMA_BAND34_PORT_TX_MB1_25, 
  QFE4465FC_TDSCDMA_BAND34_PORT_TX_MB2_26, 
  QFE4465FC_TDSCDMA_BAND39_PORT_TX_MB2_27, 
  QFE4465FC_PORT_NUM, 
  QFE4465FC_PORT_INVALID, 
} qfe4465fc_pa_port_data_type;


#endif