#ifndef RF_TDSCDMA_MSM_H
#define RF_TDSCDMA_MSM_H
/*!
  @file
  rf_tdscdma_msm.h

  @brief
  Provides TDSCDMA MSM interface functions.

  @details
  The function interface is defined in this file. This encompases functions
  for all the MSMs.

*/

/*=============================================================================

Copyright (c) 2011 - 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$DateTime: 2016/03/28 23:07:43 $ $Author: mplcsds1 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/hal/tdscdma/inc/rf_tdscdma_msm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/23/15   jak     Add parameters in rf_tdscdma_msm_update_txlm_buffer
05/28/15   jak     Pass PRX/DRX oprt mode to rf_tdscdma_msm_calc_iqmc_setting()
02/10/15   lwu     Created is_bho_update parameter in rf_tdscdma_msm_update_txlm_buffer()
09/18/14   jhe     Clean up obsolete api 
09/16/14   jyu     Support to pass WB ADC to FW
08/05/13   dej     Moved to Bolt DTR\RxLM types
07/22/13   kb      [XPT] Added API to return sampling rate.
05/28/13   ra      Added multiple API 
02/14/13   jyu     Added API to check if RX_ON/RF_ON is controlled by SSBI 
12/30/12   jyu     Added support for run-time select RX_ON control by SSBI or GRFC 
10/11/12   dej     Rename LM typedef ag header files 
08/20/12   jyu     Added support to set TX_ON 
08/08/12   jyu     Integrated with rfdevice_pa
07/31/12   qzh     Rfdevice interface change to use rfm_device instead of path
07/28/12   jyu     Fixed errors during merge 
07/10/12   kb/jyu  Remove MODEM specific setting from RF TDSCDMA MC/MEAS
06/05/12   jyu     Fixed compliation error for RxLM/TxLM
04/07/12   ycl     Added API to set DAC0_UPDATE GRFC for FTM.
08/23/11   jyu     Added API to allow to initialize TDS stmr in FTM mode 
08/09/11   jyu     Added support to control GRFCs before FW is ready 
07/08/11   jhe     Initial version.

============================================================================*/
#include "comdef.h"
#include "rfcom.h"
#include "rfnv_tdscdma.h"
#include "ftm.h"
#include "rf_tdscdma_msg.h"

//TODO
#define RF_TDSCDMA_ARM_TX_GENERAL_CTL_FTM      0x004
#define RF_TDSCDMA_ARM_TX_GENERAL_CTL_FTM_EUL  0x204
#define RF_TDSCDMA_ARM_TX_GENERAL_CTL_ONLINE   0x284



/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_update_static_rxlm_buffer(rfa_tdscdma_rxlm_mode_t mode,
                                              lm_handle_type rxlm_buf_idx);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_update_dynamic_rxlm_buffer
(
  rfm_device_enum_type device,
  uint16 channel,
  lm_handle_type rxlm_buf_idx,
  uint8* wb_adc_idx,
  rfdevice_rx_mode_type rfdevice_op_mode  
);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_update_rxlm_buffer
(
  rfm_device_enum_type device,
  uint16 channel,
  rfa_tdscdma_rxlm_mode_t mode,
  lm_handle_type rxlm_buf_idx,
  uint8* wb_adc_idx,
  rfdevice_rx_mode_type rfdevice_op_mode  
);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_update_txlm_buffer
(
  rfm_device_enum_type device,
  lm_handle_type txlm_buf_idx,
  rfcom_tdscdma_band_type band,
  boolean is_bho_update,
  uint8* dac_idx  
);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_init_lna_ctrl(void);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_set_pa_range(rfm_device_enum_type device, rfcom_tdscdma_band_type band, uint8 pa_range);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_set_pa_bypass(boolean state);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_set_smps_pa_bias_override(boolean flag);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_set_cw(boolean on_off,int32 freq_offset);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_set_rx_grfcs(rfcom_tdscdma_band_type band);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_set_tx_grfcs(rfcom_tdscdma_band_type band);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_set_tx_on(rfm_device_enum_type device, rfcom_tdscdma_band_type band, boolean tx_on);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_set_pa_on(rfm_device_enum_type device, rfcom_tdscdma_band_type band, boolean pa_on);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_set_rx_on(rfm_device_enum_type device, rfcom_tdscdma_band_type band, boolean rx_on);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_set_dac_update( void );

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_stmr_init(void);

/*--------------------------------------------------------------------------*/
boolean rf_tdscdma_msm_compare_is_same_rxlm_mode(rfa_tdscdma_rxlm_mode_t rxlm_mode_1, rfa_tdscdma_rxlm_mode_t rxlm_mode_2);

/*--------------------------------------------------------------------------*/
boolean rf_tdscdma_msm_is_rx_on_ssbi(rfm_device_enum_type  device, rfcom_tdscdma_band_type band);

/*--------------------------------------------------------------------------*/
boolean rf_tdscdma_msm_is_rf_on_ssbi(rfm_device_enum_type  device, rfcom_tdscdma_band_type band);

/*--------------------------------------------------------------------------*/
void rf_tdscdma_msm_clear_iqmc_rxlm( rfm_device_enum_type device,uint16 channel,lm_handle_type rxlm_buf_idx);

/*--------------------------------------------------------------------------*/
boolean rf_tdscdma_msm_update_dynamic_rxlm_buffer_xpt(uint32 rxlm_buf_idx, rfcom_device_enum_type device_fb, rfcom_tdscdma_band_type band);

/*--------------------------------------------------------------------------*/
uint32 rf_tdscdma_msm_get_default_txc_gain(lm_handle_type txlm_handle);

/*--------------------------------------------------------------------------*/
uint32 rf_tdscdma_msm_calc_iqmc_setting( rfm_device_enum_type device,uint16 channel,lm_handle_type rxlm_buf_idx, rfdevice_rx_mode_type rfdevice_op_mode);

/*--------------------------------------------------------------------------*/
boolean rf_tdscdma_msm_set_xpt_state_rxlm(uint32 rxlm_buf_idx);

/*--------------------------------------------------------------------------*/
uint32 rf_tdscdma_msm_get_samp_rate(lm_handle_type rx_lm_buf_index);

/*--------------------------------------------------------------------------*/
boolean rf_tdscdma_msm_disable_et_dac(uint32 txlm_buf_idx);

/*--------------------------------------------------------------------------*/

#endif /*RF_TDSCDMA_MSM_H*/
