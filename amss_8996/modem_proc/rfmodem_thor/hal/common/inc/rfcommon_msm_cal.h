#ifndef RFCOMMON_MSM_CAL_H
#define RFCOMMON_MSM_CAL_H

/*!
  @file rfcommon_msm_cal.h

  @brief
  This file contains APIs to support any common calibration routines specific
  to the current MSM.
*/

/*============================================================================== 
   
  Copyright (c) 2011 - 2013 Qualcomm Technologies Incorporated. All Rights Reserved 
   
  Qualcomm Proprietary 
   
  Export of this technology or software is regulated by the U.S. Government. 
  Diversion contrary to U.S. law prohibited. 
   
  All ideas, data and information contained in or disclosed by 
  this document are confidential and proprietary information of 
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved. 
  By accepting this material the recipient agrees that this material 
  and the information contained therein are held in confidence and in 
  trust and will not be used, copied, reproduced in whole or in part, 
  nor its contents revealed in any manner to others without the express 
  written permission of Qualcomm Technologies Incorporated. 
   
==============================================================================*/ 

/*==============================================================================
   
                        EDIT HISTORY FOR MODULE 
   
This section contains comments describing changes made to the module. 
Notice that changes are listed in reverse chronological order. 
 
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/hal/common/inc/rfcommon_msm_cal.h#1 $ 
   
when       who     what, where, why 
--------   ---     ------------------------------------------------------------- 
01/16/14   vv      Added support for ADC DC calibration
02/04/14   ljl     DAC cal mission mode initilization related APIs
11/27/13   ljl     DAC Cal result structure change for Bolt
08/22/13   dej     Remove references to FW TxLM files\structures 
01/15/13   aca     DIME DAC cal NV update
01/14/13   aca     DIME DAC cal mission mode config
01/10/12   aca     DIME Dac Nikel read method support
12/03/12   aca     DIME Dac cal
11/21/11   vb      Changes for DAC config based on the recommended Iref (which in 
                   turn is based on current device tx band port in use) 
11/02/11   vb      Support for performing DAC cal on dynamic number of Iref values
09/25/11   vb      Support for DAC cal using different Irefs
09/08/11   vb      Initial version
==============================================================================*/ 

#include "comdef.h"
#include "rfcom.h"
#include "txlm_intf.h"
#include "rfdevice_cmn_intf.h"
#include "fw_rf_common_intf.h" 
#include "rfcommon_msm.h"
#include "rflm_dtr_rx_typedef_ag.h"

/*Number of DACs supported for calibration*/
#define RFCOMMON_MAX_DAC_NUM             FW_RF_COMMON_NUM_TX_CHAINS

/*Results for each DAC*/
#define RFCOMMON_DAC_LUT_ENTRIES         FW_RF_COMMON_DAC_CAL_BUF_SIZE  

/* DAC cal results */
//#define RFCOMMON_NUM_DAC_RESULT_TYPE      2 /* Type1: MSB & DC Type2: ET */
#define RFCOMMON_NUM_TXDAC_NUM_MSB_RESULTS     36 
#define RFCOMMON_NUM_TXDAC_NUM_DC_RESULTS      1 
#define RFCOMMON_NUM_ETDAC_NUM_MSB_RESULTS     18 
#define RFCOMMON_NUM_ETDAC_NUM_RPOLY_RESULTS   1 

#define ENABLE_REF_DAC_CAL_MISSION_MODE_INIT

/*For ADC VCM calibration*/
typedef struct{
   int32  start_range;
   int32  stop_range;
   uint32 modem_reg;
}rfcommon_msm_adc_dc_cal_tbl_type; 

#if 0
typedef struct
{
  /* Iref value for which DAC config needs to be looked up */
  rfdevice_dac_iref_val iref_source;

  /* DAC Config value provided by DAC designers(refer TxLM spreadsheet for more info).
     This is the value that needs to be used for "iref_source" */
  rflm_dtr_tx_tx_dac_cfg_group_struct dac_cfg;

} rfcommon_msm_dac_cal_cfg_type;
#endif

/*----------------------------------------------------------------------------*/

typedef enum
{
	RFCOMMON_DAC_CAL_SAVE_NV,
		
	RFCOMMON_DAC_CAL_READ_NV,
	
	RFCOMMON_DAC_CAL_INVALID_CMD
	
}rfcommon_msm_dac_cal_data_cmd_enum_type;

/*----------------------------------------------------------------------------*/

typedef enum
{
	RFCOMMON_DAC_CAL_DAC0_IQ_MSB,
		
	RFCOMMON_DAC_CAL_DAC0_IQ_DC,
		
	RFCOMMON_DAC_CAL_DAC1_IQ_MSB,
		
	RFCOMMON_DAC_CAL_DAC1_IQ_DC,	
	
	RFCOMMON_DAC_CAL_DAC0_ET,
		
	RFCOMMON_DAC_CAL_DAC1_ET,

	RFCOMMON_DAC_CAL_DATA_ALL,
		
	RFCOMMON_DAC_CAL_INVALID
	
}rfcommon_msm_dac_cal_data_type;

/*----------------------------------------------------------------------------*/

typedef enum
{
  RFCOMMON_DAC_CAL_RD_DAC0_IQ_MSB,

  RFCOMMON_DAC_CAL_RD_DAC0_IQ_DC,

  RFCOMMON_DAC_CAL_RD_DAC1_IQ_MSB,

  RFCOMMON_DAC_CAL_RD_DAC1_IQ_DC,	

  RFCOMMON_DAC_CAL_RD_DAC0_ET_MSB,

  RFCOMMON_DAC_CAL_RD_DAC1_ET_MSB,

  RFCOMMON_DAC_CAL_RD_DAC0_ET_RPOLY,

  RFCOMMON_DAC_CAL_RD_DAC1_ET_RPOLY,

  RFCOMMON_DAC_CAL_RD_DATA_ALL,

  RFCOMMON_DAC_CAL_RD_INVALID

}rfcommon_msm_dac_cal_read_data_type;


typedef struct
{
  /* DAC cal results per chain 
        IQ DAC MSB cal - 36x24 reg array
        IQ DAC DC cal   - 14-bits
        ET DAC MSB cal - 18x24 reg array
        ET DAC Rpoly    -  8-bits */
  uint8  txdac_msb_result_valid;
  uint32 txdac_msb_results[RFCOMMON_NUM_TXDAC_NUM_MSB_RESULTS];
  uint8  txdac_dc_result_valid;
  uint32 txdac_dc_results[RFCOMMON_NUM_TXDAC_NUM_DC_RESULTS];
  uint8  etdac_msb_result_valid;
  uint32 etdac_msb_results[RFCOMMON_NUM_ETDAC_NUM_MSB_RESULTS];
  uint8  etdac_rpoly_result_valid;
  uint32 etdac_rpoly_results[RFCOMMON_NUM_ETDAC_NUM_RPOLY_RESULTS];
}rfcommon_msm_dac_cal_result_type;

typedef struct
{
  /* Flag to mention if the results from NV are valid  */
  boolean validity_flag;

  /*Both TxDAC and ETDAC results are defined in dac_cal_result_type*/
  rfcommon_msm_dac_cal_result_type dac_cal_results[TXLM_CHAIN_MAX];

} rfcommon_msm_cal_result_type;


/*----------------------------------------------------------------------------*/

boolean rfcommon_msm_dac_cal_nv_cfg
(
  rfcommon_msm_dac_cal_data_cmd_enum_type cmd,
  rfcommon_msm_dac_cal_read_data_type data_type,
  uint32* cal_results
);


/*--------------------------------------------------------------------------*/
boolean rfcommon_msm_dac_cfg_dynamic_update( txlm_chain_type txlm_chain,
                                             uint32 txlm_buf_idx,
                                             rfdevice_dac_iref_val iref_val );

/*--------------------------------------------------------------------------*/
boolean rfcommon_msm_cal_execute_dac_cal(rfcommon_msm_dac_cal_data_type dac_cal_type);

/*----------------------------------------------------------------------------*/
boolean rfcommon_msm_dac_cal_lut_dynamic_update( txlm_chain_type txlm_chain,
                                                 uint32 txlm_buf_idx,
                                                 boolean et_enabled );

/*For ADC VCM calibration*/
boolean rfcommon_msm_get_adc_cal_reg_setting(rxlm_state_cfg_params cfg,
                                             int32 adc_dc_cal_val,
                                             uint32 adc_config2_reg[],
                                             uint32 adc_config3_reg[]
                                             );

/*----------------------------------------------------------------------------*/
#ifdef ENABLE_REF_DAC_CAL_MISSION_MODE_INIT
void rfcommon_msm_cal_dac_init(void);
boolean rfcommon_msm_cal_dac_chk_execute(rfcommon_msm_cal_result_type *nv_dac_cal,
                                                   rfcommon_msm_dac_cal_read_data_type 
                                                   dac_cal_read_type);
void rfcommon_msm_dac_mission_mode_enter(void);
boolean rfcommon_msm_dac_cal_results_read(rfcommon_msm_cal_result_type *nv_dac_cal_results);
void rfcommon_msm_dac_bringup(rfcommon_msm_cal_result_type *nv_dac_cal_results);

#endif

#endif /*RFCOMMON_MSM_CAL_H*/
