#ifndef RFCOMMON_ANT_TUNER_H
#define RFCOMMON_ANT_TUNER_H

/*!
  @file rfcommon_ant_tuner.h

  @brief
  This file contains Common HAL for the target.
*/

/*============================================================================== 
   
  Copyright (c) 2011, 2012 Qualcomm Technologies Incorporated. All Rights Reserved 
   
  Qualcomm Proprietary 
   
  Export of this technology or software is regulated by the U.S. Government. 
  Diversion contrary to U.S. law prohibited. 
   
  All ideas, data and information contained in or disclosed by 
  this document are confidential and proprietary information of 
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved. 
  By accepting this material the recipient agrees that this material 
  and the information contained therein are held in confidence and in 
  trust and will not be used, copied, reproduced in whole or in part, 
  nor its contents revealed in any manner to others without the express 
  written permission of Qualcomm Technologies Incorporated. 
   
==============================================================================*/ 

/*==============================================================================
   
                        EDIT HISTORY FOR MODULE 
   
This section contains comments describing changes made to the module. 
Notice that changes are listed in reverse chronological order. 
 
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/hal/common/inc/rfcommon_ant_tuner.h#1 $ 
   
when       who     what, where, why 
--------   ---     -------------------------------------------------------------
06/06/12   as      Added time stamp API prototype
03/26/12   zc      Making all external APIs threadsafe
03/02/12   dyc     Make rfcommon_ant_tuner_protocol_para struct unpack type
10/14/11    gh     Add API for computing bin index
10/13/11   tnt     Add configurable falling/rising sampling edge support 
08/25/11   tnt     Update to accommodate 2 CS and dynamic NV size
07/21/11   tnt     Initial version
==============================================================================*/ 
#include "rfcom.h"
#include "comdef.h"
#include "rfcommon_nv.h"
#include "DalDevice.h"
#include "rfcommon_locks.h"
#include "rex.h"


/*! mutual exclusion and to provide atomic behavior */

/*
#define RFCOMMON_ANT_TUNER_LOCK(ant_mutex_ptr) rf_common_enter_critical_section((ant_mutex_ptr)); \
                                               REX_DISABLE_PREMPTION()
*/


											   
/*
#define RFCOMMON_ANT_TUNER_UNLOCK(ant_mutex_ptr) REX_ENABLE_PREMPTION(); \
                                               rf_common_leave_critical_section((ant_mutex_ptr))
*/
											   
											   
#define RFCOMMON_ANT_TUNER_LOCK(ant_mutex_ptr) rf_common_enter_critical_section((ant_mutex_ptr))
#define RFCOMMON_ANT_TUNER_UNLOCK(ant_mutex_ptr) rf_common_leave_critical_section((ant_mutex_ptr))

/*this struct will get the info from RFC*/
typedef struct
{
  uint8 protocol;             /* SPI = 1, RFFE = 2, 0 ow */
  uint8 clock_always_on;      /* 1 = clk to slave is always on */
  uint8 num_bits_per_trans;              
  uint8 deassert_wait_time;   /* clk cycles to deassert CS between transaction*/
  uint8 cs_active_hi;         /* 1= cs active hi, 0 = cs active low */
  uint8 core_index;           /* GSBI core id */   
  uint8 cs_1;                 /* first CS  */  
  uint8 cs_2;                 /* second CS  */  
  uint8 init_enable;          /* 1=send initialization codeword, 0 = o.w. */
  uint8 shutdown_enable;      /* 1=send shutdown codeword, 0 = o.w. */
  uint16 clock_freq_khz;   
  uint16 sampling_falling_edge;  /* 1 = MOSI data is sampled on falling edge, 0 = rising edge*/
  uint16 number_of_bins;      /*number of bins in a current band*/   
  uint32 init_tunecode;       /* initialization codeword*/
  uint32 shutdown_tunecode;   /* shutdown codeword*/
  uint32 tune_codewords[NV_CS_PER_CORE_MAX]; /*tune codeword, extract from NV */
  uint32 dalspi_device_id;
  DalDeviceHandle *hSPI[NV_CS_PER_CORE_MAX];  /* DAL device handle to SPI core
                                                 one for each CS*/  
} rfcommon_ant_tuner_protocol_para_type;

/*struct to store ant tuner codeword script to be sent to FW
 cs = NV_CS_PER_CORE_MAX when there are no codeword 
 associate with it*/
typedef struct
{
  uint8 cs_n[NV_CS_PER_CORE_MAX];             /* CS_N for each according codeword */
  uint32 tune_info[NV_CS_PER_CORE_MAX];       /* array of total max of 4 devices */
} rfcommon_ant_tuner_tune_script_struct_type;

/*----------------------------------------------------------------------------*/
void rfcommon_ant_tuner_init_mutex
(
 void
);

/*----------------------------------------------------------------------------*/
rfcommon_ant_tuner_protocol_para_type *rfcommon_ant_tuner_get_ptr
(
 void
);

/*----------------------------------------------------------------------------*/
boolean rfcommon_ant_tuner_is_enable
(
 void
);
/*----------------------------------------------------------------------------*/
boolean rfcommon_ant_tuner_multi_bins
(
  void
);

/*----------------------------------------------------------------------------*/
void rfcommon_ant_tuner_device_overwrite
(
 uint8 core_id,
 uint8 cs_n,   
 uint8 length,
 uint32* spi_command
);

/*----------------------------------------------------------------------------*/
void rfcommon_ant_tuner_device_init
(
 void
);

/*----------------------------------------------------------------------------*/
void rfcommon_ant_tuner_device_fake_init
(
 void
);

/*----------------------------------------------------------------------------*/
void rfcommon_ant_tuner_device_shutdown
(
 void
);

/*----------------------------------------------------------------------------*/
void rfcommon_ant_tuner_sends_and_exit
(
 rfcommon_nv_ant_tuner_type *ant_tuner_nv_ptr,
 uint16 rx_channel
);

/*----------------------------------------------------------------------------*/
void rfcommon_ant_tuner_build_scripts
(
 rfcommon_nv_ant_tuner_type *ant_tuner_nv_ptr,
 uint16 rx_channel,
 rfcommon_ant_tuner_tune_script_struct_type *rffw_codeword
);

/*----------------------------------------------------------------------------*/
boolean rfcommon_ant_tuner_get_bins_and_idx
(
 uint16  chan,
 uint16* chan_list,
 uint16*  ret_num_of_bins,
 uint8*  ret_bin_idx
);
/*----------------------------------------------------------------------------*/
uint32 rfcommon_atuner_get_ts
(
	void
);
#endif /*RFCOMMON_ANT_TUNER_H*/
