
#ifndef RFLM_IMD_TYPEDEF_AG_H
#define RFLM_IMD_TYPEDEF_AG_H


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated at:    Fri Jul 31 17:21:31 2015
Generated using: nlic_imd_autogen.pl
Generated from:  v0.0.38 of thor_NLICLM_settings.xlsx
*/

/*=============================================================================

              A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the modem register settings 
  configured by FW, provided by the rflm_imd.

Copyright (c) 2009, 2010, 2011, 2012, 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

$DateTime: 2016/03/28 23:07:43 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/nlic/inc/rflm_imd_typedef_ag.h#1 $

=============================================================================*/

/*=============================================================================
                           REVISION HISTORY
Version   Author   Date   
         Comments   
0.0.38   shwang   7/1/2015   
         1) clock mode for concurrency added. 2) B4+B252 H3 combo added.   
0.0.37   shwang   4/27/2015   
         B4+B5 interference mechanism corrected.   
0.0.36   shwang   4/16/2015   
         1) NOM clock for 20MHz PCC cases. 2) linear kernel reduction definition changed (0=no linear kernel, 1=k1, 2=k1*)   
0.0.35   shwang   3/20/2015   
         B8+B7, B3+B26, B3+B19, B4+B5 added   
0.0.34   shwang   3/6/2015   
         B28+B1 addition   
0.0.33   shwang   2/13/2015   
         B7+B7 addition   
0.0.32   shwang   2/3/2015   
         1) B8+B3 BW combos updated: 5+5, 10+5.  2) engaged_scc_index added. 3) B26+B41 added   
0.0.31   shwang   12/18/2014   
         1) B25+B25 added, 2) B8+B7 10+10 added, 3) uint8 actual_victim_index; added    
0.0.30   shwang   11/6/2014   
         kernel reduction feature updated for 20MHz victim case; q=-32768 for default NLIC disable. SINR TH lowered. B8+B3 f_edge fix: b0=4 --> b0=2.   
0.0.29   shwang   10/29/2014   
         addition of 5+20 and 10+20 for B12+B4    
0.0.28   shwang   10/14/2014   
         snirTH table expanded to 100RB (from 50RB)   
0.0.27   shwang   10/8/2014   
         clock mode change: NOM to SVS for b17b4 (b12b4) 5+5 and b4b2 (b3b3, b2b2)   
0.0.26   shwang   9/29/2014   
         removed 5+20 and 10+20 for B12+B4    
0.0.25   shwang   9/4/2014   
         f_offset_nom coefficients bug fix for 0.0.24   
0.0.24   shwang   9/4/2014   
         f_offset_nom coefficients (a and b) for B4+B2, B3+B3,  B2+B2 are fixed for new interf mechanism.   
0.0.23   shwang   8/19/2014   
         B4+B2, B3+B3, and B12+B4 (20MHz) added back. B2+B2 added. B3+B5 20+20 removed.   
0.0.22   shwang   8/14/2014   
         B8+B3 added   
0.0.21   shwang   8/12/2014   
         B4+B2, B3+B3, and B12+B4 (20MHz) addition reverted.   
0.0.20   shwang   8/11/2014   
         clock mode forced to NOM always for NLIC   
0.0.19   shwang   7/30/2014   
         B4+B2, B3+B3, and B12+B4 (20MHz) added   
0.0.18   shwang   7/25/2014   
         clock_mode type changed to an enum. Also names are shortened. added nb clock mode    
0.0.17   shwang   7/10/2014   
         clock mode added. 20MHz BW for B3B5 added. B12B4 is added   
0.0.16   shwang   6/30/2014   
         BW combinations 5+5 and 5+10 added   
0.0.15   shwang   6/25/2014   
         delay data type: uint8 -> int16   
0.0.14   shwang   6/24/2014   
         5+10 and 5+5 are not supported for 6/30 CS   
0.0.13   shwang   6/23/2014   
         delay data type uint16 -> uint8   
0.0.12   shwang   6/19/2014   
         removing 5+10, 5+5 from B3B5   
0.0.11   shwang   6/19/2014   
         adding clock_mode to API. adding 5+10, 5+5 to B17B4 and B3B5   
0.0.10   shwang   6/7/2014   
         adding time delay to API. adding 10+5 to B17B4 and B3B5   
0.0.9   shwang   6/6/2014   
         removed all band combinations other than B17B4 and B3B5: Unsupported use cases.   
0.0.8   shwang   4/11/2014   
         Diversity antenna also enabled for B17-B4, B12-B4, B8-B3   
0.0.7   shwang   4/1/2014   
         empty rows are filled for b3_b19 and later. nlic_mode added for wb+nb mode change   
0.0.6   shwang   3/31/2014   
         freature added: 1-reducing num_kernels. 2-selecting one antenna   
0.0.5   shwang   3/20/2014   
         FTL corrections. New equations to support PCC cancellation.   
0.0.4_B3B5   shwang   2/11/2014   
         B3-B3 FTL correction, NLIC enable always on for B3-B3, victim_carrier_id = 1, aggr_carrier_id = 0, f_offset_nom = 0   
0.0.4   shwang   12/9/2013   
         algorithm bug fix: kernel_term_extra_rxy_dly_acq = 2 for B8_B3 (was 1)   
0.0.3   shwang   11/25/2013   
         fix the algorithm: kernel_terms_cancel_mode_l0 = (1-k7_en)*kl0_on_off   
0.0.2   shwang   10/1/2013   
         added k7_en as static param   
0.0.1   shwang   9/27/2013   
         Initial Revision   

=============================================================================*/
/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "comdef.h" 



#define NUM_IMD_CASES_MAX 6

typedef enum 
{
	NLIC_IMD_STATE_B1_B3,
	NLIC_IMD_STATE_B3_B1,
	NLIC_IMD_STATE_B3_B7,
	NLIC_IMD_STATE_B7_B3,
	NLIC_IMD_STATE_INVALID,
}rflm_imd_backoff_state_enum_type;


typedef struct 
{
	uint8 victim_carrier_idx;
	uint8 victim_carrier_2_band;
	int8 m;
	int8 n;
	int8 p;
	uint8 max_desense;
	uint8 max_desense_NV_ptr;
}rflm_imd_case_type;

typedef struct 
{
	uint8 num_imd_cases;
	rflm_imd_case_type* imd_case[NUM_IMD_CASES_MAX];
}rflm_imd_backoff_static_config_type;



#ifdef __cplusplus
}
#endif



#endif


