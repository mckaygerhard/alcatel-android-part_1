
#ifndef RFLM_DTR_RX_TYPEDEF_AG_H
#define RFLM_DTR_RX_TYPEDEF_AG_H


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated at:    Tue Dec  1 12:40:26 2015
Generated using: lm_autogen.exe v4.0.51
Generated from:  v4.9.20 of Bolt_RXFE_register_settings.xlsx
*/

/*=============================================================================

           R X    A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the modem register settings 
  configured by FW, provided by the rflm_dtr_rx.

Copyright (c) 2009, 2010, 2011, 2012, 2013, 2014, 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.

$DateTime: 2016/03/28 23:07:43 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/lm/inc/rflm_dtr_rx_typedef_ag.h#1 $

=============================================================================*/

/*=============================================================================
                           REVISION HISTORY
Version    Author   Date   
         Comments   IR Number   Other POC (Comma Delimited)   Release Validation Notes   Dependencies   AG File Location   
4.9.20   ariyenga   11/18/2015   
         1. Updated LIF states for TDD-LTE HORxD      tvirk@qti.qualcomm.com, ashokm@qti.qualcomm.com   1. Verified with engg. build      http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E20   
4.9.19   gatesj   10/13/2015   
         1. Modified LTE15_SVS2 state to enable ADC Pack4 for HW constraint with SVS2 clocks      udayanb@qti.qualcomm.com   1. Verified by LTE team         
4.9.18   gatesj   9/30/2015   
         1. New state for GSM online power 2. Removed modes added in 4.9.17      christos@qti.qualcomm.com   1. Verified by GSM team         
4.9.17   gatesj   9/8/2015   
         1. Included SVS2 alternate mode to other LTE states that also support lower RxFE clock      djoffe@qti.qualcomm.com         http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E17   
4.9.16   gatesj   9/4/2015   
         1. New LTE20/15 states for SVS2      udayanb@qti.qualcomm.com   1. Verified by LTE team      http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E16   
4.9.15   gatesj   9/2/2015   
         1. Included new adc_hibernation_SW_Dynamic tab with ADC hibernation modes defined 2. Included new RxLM variable with corresponding hibernation mode               http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E15   
4.9.14   gatesj   7/28/2015   
         1. Updated the SOC_HW_VERSION override mask for Tesla and Istari pairings 2. Updated LTE20/15 WBDC K=3      djoffe@qti.qualcomm.com, kpatel@qti.qualcomm.com, ashokm@qti.qualcomm.com   1. SW Script changes validated by RFLM team 2. Validated by LTE team   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E14   
4.9.13   ariyenga   7/20/2015   
         1. Added mask for SOC_HW_VERSION based differentiation 2. Added LIF states for TH         1. SW Script changes validated by RFLM team 2. Validated in csim   AG Script update   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E13   
4.9.12   ariyenga   4/20/2015   
         1. Added recommended BBRx settings for Istari V2      wangyan@qti.qualcomm.com   1. Recommended settings from BBRx team. Validated by them   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E12   
4.9.11   ariyenga   4/7/2015   
         1. Added FTM support for single Rx CA      pphilip@qti.qualcomm.com, stiwana@qti.qualcomm.com   1. Validated in csim   1. LTE RFSW dependency to call SW dynamic update APIs for single Rx   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E11   
4.9.10   ariyenga   4/6/2015   
         1. Added SW dynamic settings for FBRx ADC register settings for WTR4905 pairing      kgandhi@qti.qualcomm.com, christos@qti.qualcomm.com, katkinso@qti.qualcomm.com   1. Recommended settings from BBRx team. Validated with engg. build   FBRx RFSW dependency for SW dynamic updates to ADC registers   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E10   
4.9.9   ariyenga   4/3/2015   
         1. Corrected RxFE scale factors for extra single-Rx CA modes         1. Validated in csim      http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E9   
4.9.8   ariyenga   3/20/2015   
         1. Corrected typo in ADC setting for FBRX mode 4 (DAC optimized) state 2. Corrected FBRx capture length for Mode 3      christos@qti.qualcomm.com   1. Recommended settings from BBRx team in use now, but no performance impact seen previously 2. Recommended by FBRx Systems team. Validated by them   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E8   
4.9.7   ariyenga   2/20/2015   
         1. Updated ADC register settings for Thor 2. Updated ICIFIR settings for CDMA with WTR 4905 pairing      yguo@qti.qualcomm.com, gakiran@qti.qualcomm.com, sekin@qti.qualcomm.com, ruhuah@qti.qualcomm.com   1. Recommended by BBRx Systems team. Validated by them. 2. Recommended by C2K Systems team. Validated through engg build   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E7   
4.9.6   ariyenga   2/4/2015   
         1. Split common Dec8 blocks into two separate blocks      djoffe@qti.qualcomm.com, udayanb@qti.qualcomm.com, hsinhaoc@qti.qualcomm.com   1. Required to handle common Dec8 configuration in CRAT and IRAT scenarios   RFLM script changes to handle Split Dec8 blocks Common FW API change to update RFLM database of Dec8   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E6   
4.9.5   ariyenga   2/4/2015   
         1. Increased FBRx mode1/2/4 capture size to 532      christos@qti.qualcomm.com, pvora@qti.qualcomm.com   1. Recommended by FBRx team. Validated by them.   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E5   
4.9.4   ariyenga   1/16/2015   
         1. Made DTR_RXFE_EN register directly writable 2. Set COMMON_DEC8 block-valid flags TRUE for LTE modes      vineethv@qti.qualcomm.com, udayanb@qti.qualcomm.com, yongl@qti.qualcomm.com   1. Required for ADC Vcm calibration implementation in SW 2. Needed for proper handling of single-Rx CA cases   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E4   
4.9.3   ariyenga   12/15/2014   
         1. Added changes for screening single vs. dual Rx for LTE CA 2. Copied NB settings into NB1 and NB2 blocks in SCDO buffer 3. Updated BBRx settings for Thor 4. Added demod NB decimator settings in single-Rx SW dynamic sheet      kbaskara@qti.qualcomm.com, udayanb@qti.qualcomm.com, apurvas@qti.qualcomm.com, srangana@qti.qualcomm.com, yguo@qti.qualcomm.com, gakiran@qti.qualcomm.com, pkotkar@qti.qualcomm.com   1. Needed for RFLM API to screen non-contiguous CA cases with single Rx support 2. Required by C2K team for SCDO state to handle drop of carriers from MCDO state--block valid flags for NB1 and NB2 buffers are left as FALSE so no additional NBs are allocated 3. Recommended by BBRx Systems team <https://sharepoint.qualcomm.com/qct/RFA/P/MS/TSMC_Tarvos2/Project Documents/Architecture/Tarvos2_BBRx_register_settings.xlsx> 4. Required for handling demod path processing for asymmetric BW cases   1. LTE RFSW to call RFLM API before configuring the Rx path--to force dual Rx when API returns invalid state 2. C2K RFSW will modify block-valid flags appropriately based on the carrier that is left active 4. LTE RFSW to add SW dynamic updates of demod path decimator settings in single Rx cases   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E3   
4.9.2   ariyenga   11/26/2014   
         1. Updated WBDC left shift for TDS TRK and IDLE states      sankarh@qti.qualcomm.com   1. Validated via on-target regression tests by TDS team   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E2   
4.9.1   ariyenga   11/20/2014   
         1. Corrected typos in single Rx CA RxLM variables      subhashs@qti.qualcomm.com   1. Corrected rotator values that were swapped between carriers   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E1   
4.9.0   ariyenga   11/5/2014   
         1. Added RxLM variable for LTE RFSW/FW usage in signal power computation      arshah@qti.qualcomm.com, vshirvan@qti.qualcomm.com   1. Fudge factor added to avoid re-cal. Validated through on target tests.   LTE RFSW to make use of Q24 variable in RF Cal codebase and RF Cal verification LTE FW to make use of the same Q24 variable in RxAGC computations   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E9%2E0   
4.8.9   ariyenga   10/15/2014   
         1. Corrected ADC register settings for LTE single Rx CA states, error canceller enable setting for 20 MHz BW modes 2. Added       arshah@qti.qualcomm.com   1. Used settings recommended by BBRx systems (as in    None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E8%2E9   
4.8.8   ariyenga   10/10/2014   
         1. Populated RxFE scale parameters for single Rx CA in SW Dynamic tab 2. Added LTE FTM states for NBPWR settings 3. Updated ADC settings for LTE-20 mode      vshirvan@qti.qualcomm.com, arshah@qti.qualcomm.com, yguo@qti.qualcomm.com, rkashyap@qti.qualcomm.com, llouie@qti.qualcomm.com   1. Validated in csim 2. RFSW will use RxLM settings in FTM for LNA Offset cal. 3. Power optimization feature. Recommended by BBRx Systems.   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E8%2E8   
4.8.7   ariyenga   9/24/2014   
         1. Added ICIFIR settings for 1x/SCDO for WTR4905 pairing 2. Added FBRx and XPT states for C/T/W for reduced DAC rate (230.4 or 115.2)      sekin@qti.qualcomm.com, ruhuah@qti.qualcomm.com, christos@qti.qualcomm.com   1. Recommended by C2K Systems based on WTR4905 O-specs from RFIC team. Validated by C2K Systems. 2. Required for supporting the power optimization feature to reduce the DAC rate   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E8%2E7   
4.8.6   ariyenga   9/16/2014   
         1. Added Tarvos2 ADC register settings for Thor   IR-028320   pdamera@qti.qualcomm.com, yguo@qti.qualcomm.com   1. Settings recommended by BBRx Systems team (https://sharepoint.qualcomm.com/qct/RFA/P/MS/TSMC_Tarvos2/Project%20Documents/Architecture/Tarvos2_BBRx_register_settings.xlsx) Validated by them   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E8%2E6   
4.8.5   ariyenga   9/12/2014   
         1. Increased FBRx mode1 and mode4 capture size from 206 to 256   IR-027755   bhaskarn@qti.qualcomm.com, katkinso@qti.qualcomm.com   1. Recommended by FBRx team. Validated by them.   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E8%2E5   
4.8.4   ariyenga   9/10/2014   
         1. Reverted GSM ICIFIR coefficients   IR-027271   bcanpola@qti.qualcomm.com   1. Required by RFSW to avoid RF Cal failures. Bahadir confirmed no impact to performance.   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E8%2E4   
4.8.3   ariyenga   9/5/2014   
         1. Removed notch filter thresholds from SW Dynamic tab   IR-026595      1. Not supported by autogen script presently. Will be added back if needed. No validation necessary.   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E8%2E3   
4.8.2   ariyenga   8/22/2014   
         1. Updated ideal ADC rate variables in U22 (unsigned, with 22 fractional bits) 2. Added SW Dynamic sheet for single Rx CA states with ICIFIR settings and RxLM variables (populated only for LTE20+20 presently) 3. Updated GSM ICIFIR coefficients 4. Populated    IR-024046   evanbuhl@qti.qualcomm.com, vshirvan@qti.qualcomm.com, subhashs@qti.qualcomm.com, bcanpola@qti.qualcomm.com,  ngottimu@qti.qualcomm.com, ahu@qti.qualcomm.com   1. Required for GNSS time transfer. To be validated by <evanbuhl> 2. Required for handling single Rx CA cases where multiple bandwidth combinations use the same RxLM state (mode). To be validated by LTE FW 3. Retuned coefficients for WTR3925 and made DC gain unity--for Bolt 2.0 PL only. To be validated by GSM team 4. To be used in programming notches for DR-DSDS. No validation necessary.   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E8%2E2   
4.8.1   ariyenga   7/18/2014   
         1. Updated ICIFIR settings in <cdma_icifir_SW_Dynamic> sheet 2. Made    IR-017836   ruhuah@qti.qualcomm.com, sekin@qti.qualcomm.com   1. Recommended settings from C2K team. Validated on target using engineering builds. 2. Required for single Rx LTE CA operation. 3. Changes made to allow operation with lower offline clocks. 4. Recommended by RFLM SW team   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E8%2E1   
4.8.0   ariyenga   7/9/2014   
         1. Doubled VSRC WA count for GSM BURST states 2. Made all IQMC registers directly writeable 3. Added notch sampling frequencies as RxLM variables 4. Specified signed variables to be of    IR-016205   bcanpola@qti.qualcomm.com, chunhaoh@qti.qualcomm.com, ahu@qti.qualcomm.com   1. Recommended by GSM Systems team. Validated by them 2. Dynamicity to be validated by RFLM SW team 3. To be used in notch filter implementation 4. Change made for consistency--no impact expected to functionality   FW interface change due to new RxLM variables   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E8%2E0   
4.7.3   ariyenga   6/26/2014   
         1. Updated ICIFIR settings in <cdma_icifir_SW_Dynamic> sheet   IR-014045   ruhuah@qti.qualcomm.com, sekin@qti.qualcomm.com   1. Recommended settings from C2K team. Validated on target using engineering builds.   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E7%2E3   
4.7.2   ariyenga   6/24/2014   
         1. Made IQMC_CFG1 registers    IR-013696      1. Dynamicity of register fields to be validated by RFSW team   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E7%2E2   
4.7.1   vnaware   6/6/2014   
         1. Updated GSM ICIFIR coefficients to improve GSM sensitivity   IR-011088   udaraf@qti.qualcomm.com, bcanpola@qti.qualcomm.com   Validation yet to happen.    None      
4.7.0   ariyenga/gatesj   5/22/2014   
         1. Updated VSRC wraparound value for the W DSR state 2.     requires an interface change 3.  RXFE_WB_VSRC_SCTR_WA_WBw moved outside of the VSRC subgroup    added to a new VSRC_WA_COUNT subgorup to allow for FW Export"   IR-009507   liuy@qti.qualcomm.com   1. Recommended by W team. Validated by them. 2/3. AG code validated by RFSW   
4.6.8   ariyenga   4/18/2014   
         1. Updated    IR-007338   ligao@qti.qualcomm.com   1. Affects csim only. 2. To be validated by Li Gao.   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E6%2E8   
4.6.7   ariyenga   4/17/2014   
         1. Changed    IR-007060      1. Validated in the AG code.   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E6%2E7   
4.6.6   ariyenga   4/15/2014   
         1. Corrected ADC register settings for XPT states, modified LTE-20 XPT state 2. Modified WBDC loop constant for the TDS_TRK_IRAT state 3. Corrected typo in RxFE scale factors   IR-007060   christos@qti.qualcomm.com, cirick@qti.qualcomm.com, sankarh@qti.qualcomm.com   1. Typo corrected, modified state to remove pack4-dec0 inconsistency. 2. Change made per request from TDS team to allow for substantial DC estimation within 800 Cx8 samples 3. Validated in csim   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E6%2E6   
4.6.5   ariyenga   4/3/2014   
         1. Added Modified GSM State 2. Updated ADC configurations   IR-005131   eekrem@qti.qualcomm.com, yanliu@qti.qualcomm.com, dimengw@qti.qualcomm.com, achakra@qti.qualcomm.com, yguo@qti.qualcomm.com   1. To be used for W ACQ (NBR). Will be validated by W FW, RF SW and L1 SW. 2. Recommended by BBRx team (https://sharepoint.qualcomm.com/qct/RFA/P/MS/20nmBBRx/Project Documents/Architecture/Tarvos_BBRx_register_settings.xlsx). Validated by them.   W FW, W RF SW and W L1 SW to port feature from Triton   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E6%2E5   
4.6.4   ariyenga   4/1/2014   
         1. Updated RxFE delay values   IR-005123   tcurtiss@qti.qualcomm.com, srangana@qti.qualcomm.com, sapte@qti.qualcomm.com, pingliu@qti.qualcomm.com    1. Changed format from Q24 to Q16 to support uint32 usage   1x RF SW, LTE FW changes to handle new format   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E6%2E4   
4.6.3   ariyenga   3/28/2014   
         1. Corrected RxCH Indices for FBRx modes 2. Corrected WBDC setting csim variable name   IR-005093      1. Required for RUMI only. 2. Affects csim only.   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E6%2E3   
4.6.2   ariyenga   3/21/2014   
         1. Updated format of ADC_INSERTION_LOSS_DELTA   IR-005035   pingliu@qti.qualcomm.com   1. RF SW assumes integer formats. Changed variables to Q24 format.   LTE FW change to handle new format   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E6%2E2   
4.6.1   ariyenga   3/19/2014   
         1. Updated ADC insertion loss values 2. Populated RxFE scale factors for TDS modes   IR-005006   yguo@qti.qualcomm.com, bhiremat@qti.qualcomm.com, udaraf@qti.qualcomm.com, kpatel@qti.qualcomm.com   1. Recommended by BBRx team. Validated by them. 2. Validated in csim   None   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E6%2E1   
4.6.0   ariyenga   3/14/2014   
         1. Populated RxFE delay variables 2. Corrected rotator values for LTE CA mode 12 3. Populated CSR_IDEAL values 4. Updated TRX state defining parameter to include WTR 4905 states 5. Added    IR-004999   tcurtiss@qti.qualcomm.com, srangana@qti.qualcomm.com, liuy@qti.qualcomm.com, yguo@qti.qualcomm.com, hsinhaoc@qti.qualcomm.com, evanbuhl@qti.qualcomm.com, pingliu@qti.qualcomm.com, yguo@qti.qualcomm.com   1. Verified in csim 2. Affects csim only. Validated through simulations. 3. Values verified in csim. To be validated by W FW. 4. WTR4905 and WTR3925 have the same filters and the states are shared between them. 5. Validated by RF SW team through updated Autogen. 6. To be used for 1x-GNSS sync. 7. To be used for dynamic notch filter programming algorithm 8. Modified format to suit LTE-FW usage. To be validated by LTE-FW. 9. Recommended settings from BBRx team for fuse-based RSB compensation. 10. WTR2605 states are the same as WTR3925 states presently. These are placeholders until the design is finalized.   FW interface change   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E6%2E0   
4.5.0   ariyenga   2/21/2014   
         1. Added NBPWR Dynamic settings tab 2. Added ZERO_DB_DIGITAL_GAIN as an RxLM variable 3. Added WTR state defining parameter to all the states 4. Added the WBDC_OFFSET RxLM variable 5. Renamed VSRC_MEMPOOL_DELAY variables as RXFE_DELAY   IR-004938   dimengw@qti.qualcomm.com, vbhide@qti.qualcomm.com, jiannant@qti.qualcomm.com, pingliu@qti.qualcomm.com, psriniva@qti.qualcomm.com   1. Needed for W FTM NBPWR Dynamic configuration. To be validated by W RFSW. 2. Recommended by W Systems team--to be used by W FW. See <https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/Bolt/Systems/Common/UFE%20Bolt%20(rxfe)/ZerodBDigGain_MaxSigDelLevel.docx> for definition. 3. Recommended by RF SW team for supporting WTR 2605 states. RF SW team to add support for this. 4. Validated by the RxFE Systems team. 5. Change made to follow common convention across modems. These variables contain the RxFE delays per NB chain in VSRC sample counter units in 32UR24 format.   FW interface change   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E5%2E0   
4.4.0   ariyenga   2/7/2014   
         1. Optimized UMTS DSR settings (ADC rate, decimator configurations, ICIFIR filter) 2. Updated NBPWR MASK for MCDO states 3. Made ADC_CFG block runtime dynamic   IR-004894   chunhaoh@qti.qualcomm.com, ruhuah@qti.qualcomm.com, sekin@qti.qualcomm.com, jpaul@qti.qualcomm.com, hsinhaoc@qti.qualcomm.com   1. Optimization for power savings: validated by RF Systems, BBRx  and RxFE systems 2. Recommended by C2K Systems team: to be validated by C2K Systems, RFSW and FW teams 3. Optimization to cut down on the RxLM config time. To be validated by common FW.   FW interface change   http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E4%2E0   
4.3.12   ariyenga   1/31/2014   
         1. Included spectral inversion by default for FBRx modes 2. Conjugated ICIFIR filters for MCDO states 3. Added xPT_RxFB_LTE20MHz state 4. Updated BBRx register settings   IR-004866   christos@qti.qualcomm.com, kaitu@qti.qualcomm.com, ruhuah@qti.qualcomm.com, sekin@qti.qualcomm.com, srangana@qti.qualcomm.com, yguo@qti.qualcomm.com   1. Recommended by FBRx team. To be validated by them. 2. Recommended by C2K team, validated by them. 3. Recommended by FBRx team. To be validated by them. 4. Recommended by BBRx team. Validated by them.      http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E3%2E12   
4.3.11   ariyenga   1/24/2014   
         1. Updated UMTS_VOICE_DSR state 2. Updated LTE RxFE scale factors 3. Updated ADC configurations   IR-004827   yguo@qti.qualcomm.com   1. ADC settings recommended by RF Systems. Validated through csim. 2. Recalculated WBPWR scale factors by using a wide-band signal. Validated through LTE AGC simulations. To be also validted on target 3. Recommended settings from BBRx team <http://sharepoint/qct/RFA/P/MS/20nmBBRx/Project Documents/Architecture/Tarvos_BBRx_register_settings.xlsx>. Validated by them.      http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E3%2E11   
4.3.10   ariyenga   1/17/2014   
         1. Made NB_WTR_CFG register SW Dynamic   IR-004810   christos@qti.qualcomm.com, jarirf@qti.qualcomm.com   1. Recommended by FBRx team to switch between FTM and online mode. To be validated by RF SW team      http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E3%2E10   
4.3.9   ariyenga   12/30/2013   
         1. Split common notch subgroups into two pairs   IR-004784      1. Recommended by RF SW team--necessary for independent writes, validated by them.      http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E3%2E9   
4.3.8   ariyenga   12/17/2013   
         1. Updated UMTS_Voice state with HS ICIFIR settings 2. Added Bolt+ FBRx NB indices 3. Readjusted UMTS 1C and UMTS 2C GDA settings : removed 4Cx8 delay in 1C state, added 8Cx8 delay in 2C state   IR-004769   chunhaoh@qti.qualcomm.com   1. Change required to achieve -40 dB ICI, validated by RxFE and UMTS Systems teams 2. FBRx NB index is 8 on Bolt, 10 on Bolt+. No impact to AG code, but used by csim. Validated in csim. 3. Change requested by W Systems, FW and SW teams. Validated on target by them.      http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E3%2E8   
4.3.7   ariyenga   12/13/2013   
         1. Enabled GDA blocks for UMTS_1C--adding 4Cx8 group delay to enable 1C-->2C transition 2. Changed NB usage for the multicarrier UMTS states (ICIFIR, CSR rotation settings updated) 3. Corrected order of ADC register programming--changed subgroups   IR-004748   chunhaoh@qti.qualcomm.com   1. Recommended by W Systems team. To be validated by them. 2. Required for compatibility with existing RF SW code. To be validated by W Systems/FW teams. 3. Required for maintaining the correct order of ADC register programming recommended by BBRx team. Validated by BBRx team      http://sharepoint/qct/Modem-Tech/Projects/Bolt/Systems/Forms/AllItems.aspx?RootFolder=%2Fqct%2FModem%2DTech%2FProjects%2FBolt%2FSystems%2FRF%20INTERFACES%2FAG%5FArchive%2F4%2E3%2E7&FolderCTID=0x0120000F76A8BE2E8085439EF60EA6CFA0C725&View={5301DE5C-52E4-4F7F-AE36-53BBDD07F434}   
4.3.6   ariyenga   12/4/2013   
         1. Updated buffer length of debug writers for y1y2-logging RxLM states 2. Corrected default Notch indices   IR-004726      1. Required to capture correct number of samples at LMEM. To be validated on target 2. Incorrect index values stored previously. No validation since there is no AG impact         
4.3.5   ariyenga   12/2/2013   
         1. Added csim tags for RxLM variables storing RxFE scale-factors 2. WBDC LSHIFT CMD bit set in GSM states   IR-004716   udaraf@qti.qualcomm.com, bcanpola@qti.qualcomm.com   1. Change required for csim only. No validation since no AG code impact. 2. Needs to be set from RxLM since GFW does not trigger this. Validated on target.         
4.3.4   ariyenga   11/20/2013   
         1. Added Debug logging RxLM states for ADC0-4 y1y2 logging 2. Added xPT state for TDS FTM   IR-004691   bolt.dtr.fw.poc@qti.qualcomm.com, cdave@qti.qualcomm.com, yiqiaopa@qti.qualcomm.com   1. New states for y1y2-logging; to be validated on RUMI, target 2. Settings recommended by Systems team, validated through simulations.         
4.3.3   ariyenga   11/18/2013   
         1. Formatting changes: State config parameters reverted to earlier format 2. Increased the number of samples captured in the FBRx ILPC states 3. Updated TDS DC configuration; disabled VSRC regulator for 3x MCDO state 4. Corecting WB/NB_TOP_CTL writes--removed    IR-004665      1. Formatting changes recommended by RFSW team, validated by them. 2. Increased samples recommended by Systems team, validated through simulations. 3. Config changes to accommodate VSRC regulator bypass; validated by Systems team. 4. Required for proper writes to register; validated by Systems and SW teams         
4.3.2   ariyenga   11/12/2013   
         1. Formatting changes: Replaced    IR-004642      1. Formatting changes recommended by RFSW team, validated by them.         
4.3.1   ariyenga   11/7/2013   
         1. Formatting changes: Replaced    IR-004610      1. Formatting changes recommended by RFSW team, validated by them.         
4.3.0   ariyenga   11/7/2013   
         1. Set NB_GATE_CMD registers to 0 for LTE search NBs 2. Updated FBRx RxLM states: enabled WBDC correction, IQMC, CSR freq-error correction 3. Making RXFE_TOP block Runtime-dynamic 4. Updating BBRx register settings   IR-004604      1. Requirement to handle LTE search NB-swapping. NB_GATE_CMD registers to be programmed by common FW based on input from L1 SW 2. Changes recommended by FBRx Systems team, to be validated by them. 3. Requirement to maintain the writing of DBG_CMD registers at the end. Validated by RFSW team. 4. Recommended by BBRx team, validated by them.         
4.2.3   ariyenga   11/4/2013   
         1. Correcting <TDS_DC> state settings 2. Updated XPT FTM states 3. Corrected debug muxing state 4. Corrected    IR-004587      1. Change required for HW functionality (Notch RSR constraint). csim and VI validated 2. Modes similar to FBRx ILPC modes but allow for longer sample capture at LMEM. Naming convention carried over from Dime 3. Default NB indices overwritten. Csim requirement. No change to AG code. 4. Typo corrected 5. Larger buffer size necessary to capture the required number of samples         
4.2.2   ariyenga   10/24/2013   
         1. Changes to <revision_history> sheet format 2. Changes to     ""num. of carriers""    and ""mode"" for each state. Added ""INVALID"" flag wherever not applicable 3. Corrected debug muxing state: setting ENABLE in NB_TOP_CTL 4. Updated XO CAL states"   IR-004530      1. Format changes recommended by <Excelerator> and SW teams 2. Changes recommended by SW team 3. ENABLE needs to be set for register programming 4. XO CAL states updated to match latest mission mode settings   
4.2.1   ariyenga   10/17/2013   
         1. Added Debug logging RxLM state                                                                                                                                                                                         2. Moved RXFE_TOP registers to the end to allow correct programming sequence for debug-muxing state                  
4.2.0   ariyenga   10/15/2013   
         Adding WBDC, WBPWR and NBPWR scale variables as RxLM Variables                  
4.1.6   ariyenga   10/11/2013   
         Inheriting Bolt Pack4 settings from Bolt+ settings                  
4.1.5   ariyenga   10/11/2013   
         Corrected DEC8_MODE subgroups (Bolt+ register)                  
4.1.4   ariyenga   10/11/2013   
         Copied NB0 settings into NB1 (except NB_GATE_IMM_TRIG) for UMTS_VOICE and UMTS_VOICE_DSR states for enabling NB-mirroring in FET                  
4.1.3   ariyenga   10/11/2013   
         Added missing                   
4.1.2   ariyenga   10/10/2013   
         Added                   
4.1.1   ariyenga   10/7/2013   
         Corrected calculation of VSRC_T2BYT1_IDEAL_SCALE                  
4.1.0   ariyenga   10/2/2013   
         Made DVGA block runtime dynamic, and DVGA registers SW dynamic                  
1.0.34   ariyenga   10/2/2013   
         Updated LTE-10 state to work with lower offline clock on Bolt+                  
1.0.33   ariyenga   9/25/2013   
         Removed the string                   
1.0.32   ariyenga   9/25/2013   
         Added default IQMC coefficients to each state                  
1.0.31   ariyenga   9/12/2013   
         Correcting PLL_CLK_EN programming sequence                  
1.0.30   ariyenga   9/11/2013   
         Updating VSRC WA value for LTE states                  
1.0.29   ariyenga   9/4/2013   
         Correcting WB/NB ENABLE/CLEAR subgroups                  
1.0.28   ariyenga   9/3/2013   
         Correcting SB SVDO RxLM state (ADC rate, VSRC settings, ICIFIR coefficients)                  
1.0.27   ariyenga   8/28/2013   
         Adding offline clock variable, VSRC regulator bypass, Pack4 registers for Bolt+                  
1.0.26   ariyenga   8/27/2013   
         Updating STMR frequency for C2K modes                  
1.0.25   ariyenga   8/23/2013   
         Correcting Subgroups of RxLM Variables                  
1.0.24   ariyenga   8/22/2013   
         Porting in Dime changes for WB/NB ENABLE register programming                  
1.0.23   ariyenga   8/19/2013   
         Correcting typo in 7xMCDO state ICIFIR coefficients                  
1.0.22   ariyenga   8/16/2013   
         Made URXFE_NB0-3 blocks runtime dynamic                  
1.0.21   ariyenga   8/15/2013   
         Updated ICIFIR coefficients, CSR frequency offsets for CDMA modes                  
1.0.20   ariyenga   8/9/2013   
         Ported in relevant changes from Dime RxLM                  
1.0.19   ariyenga   8/6/2013   
         Added <MASTER_CONFIG> column, blank row after <debug_pt> settings. Added Bolt+ Registers with <Bolt,Bolt+> flags                  
1.0.18   ariyenga   8/2/2013   
         Updated <START_MASK> register for WB PWR and NB PWR blocks for 1X mode                  
1.0.17   ariyenga   7/30/2013   
         Updated <BUFF_SIZE_P2> register settings for 1x/DO modes                  
1.0.16   ariyenga   7/16/2013   
         Added <CSIM_msm_register_settings> sheet which copies <msm_register_settings> sheet except for the CSR frequency offset settings which are zeroed-out in the <msm_register_settings> sheet.                  
   vnaware   7/16/2013   
         Programming correct ADC settings for all RxLM modes                  
      7/15/2013   
         Updated sub group comments to refelct correct WB/NB indices                  
1.0.15   vnaware   7/9/2013   
         Enabled PLL2 CLK for GSM modes. Was a bug.                  
1.0.14   vnaware   6/28/2013   
         Correct CSR_FREQ_OFFSET_NB2 to NB3, Add the chain3 RxLM variables and settings into RXLM_VARS block                   
1.0.13   vnaware   6/21/2013   
         Added <DTR_RXFE_EN> register  and a new DTR_RXFE_ENABLE block, updated RFIF_UPDATE_CMD field offset                  
1.0.12   ariyenga   6/12/2013   
         Added LTE CA mode 12, Corrected CA mode 1 settings. Removed <DTR_ENABLE> register, Corrected ADC <OUT_CLK_EN> register name                  
1.0.11   ariyenga   5/22/2013   
         Updated ADC Rates for 3 MHz modes, updated ICIFIR filters after analog pole update and highFreqNoise addition                  
1.0.10   ariyenga   5/17/2013   
         Added WB4_DEBUG_ENABLE register                  
1.0.9   ariyenga   5/6/2013   
         Updated Offline clock rates for LTE CA modes                  
1.0.8   vnaware   5/3/2013   
         Removing PLL_CFG block, modifying ADC register sequence per Tarvos data sheet, adding Tarvos specific settings in     adding rf intf ""Diff or SE"" selection in state cfg paramter list"               
1.0.7   ariyenga   5/3/2013   
         Updating VSRC Regulator settings                  
1.0.6   vnaware   4/26/2013   
         Adding LTE iRAT measurement state with 1 WB, 1NB and 10 PCFL. Settings from LTE1.4 search NB configuration                  
1.0.5   vnaware   4/26/2013   
         Added ADC and DTR trigger registers that are new for Bolt, removed RXFE_OFFLINE_CFG block                   
1.0.4   vikramr   4/16/2013   
         Block Valid Flag empty for Block: PLL_CFG and RXFE_OFFLINE_CFG empty for most FBRx states Rev History formatting update                  
                           
1.0.3   vnaware   4/9/2013   
         Adding FBRx modes 1-4                  
                           
1.0.2   vnaware   3/14/2012   
         Adding DEC8 filter setings to a common dec8 block according to SWI                  
         Adding start/stop immediate trigger for debug_CMD register                  
                           
1.0.1   vnaware   1/15/2012   
         Adding LTE CA states                  
                           
1.0.0   vnaware   12/14/2012   
         Initial release with single state to allow RxLM SW scripts to be exercised - this release is deleted from sharepoint subsequently                  

=============================================================================*/
/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "comdef.h" 





typedef enum
{
  RFLM_DTR_RX_STATE_UMTS_VOICE_NONET,
  RFLM_DTR_RX_STATE_UMTS_1C_NONET,
  RFLM_DTR_RX_STATE_UMTS_2C_NONET,
  RFLM_DTR_RX_STATE_UMTS_3C_NONET,
  RFLM_DTR_RX_STATE_UMTS_4C_NONET,
  RFLM_DTR_RX_STATE_UMTS_VOICE_DSR_NONET,
  RFLM_DTR_RX_STATE_UMTS_MODGSM_ACQ,
  RFLM_DTR_RX_STATE_GSM_BURST,
  RFLM_DTR_RX_STATE_GSM_EVWACI_BURST,
  RFLM_DTR_RX_STATE_GSM_BURST_WTR2605,
  RFLM_DTR_RX_STATE_GSM_EVWACI_BURST_WTR2605,
  RFLM_DTR_RX_STATE_LTE_1P4MHZ_IRAT,
  RFLM_DTR_RX_STATE_LTE_1P4MHZ,
  RFLM_DTR_RX_STATE_LTE_3MHZ,
  RFLM_DTR_RX_STATE_LTE_5MHZ,
  RFLM_DTR_RX_STATE_LTE_10MHZ,
  RFLM_DTR_RX_STATE_LTE_15MHZ,
  RFLM_DTR_RX_STATE_LTE_20MHZ,
  RFLM_DTR_RX_STATE_LTE_15MHZ_SVS2,
  RFLM_DTR_RX_STATE_LTE_20MHZ_SVS2,
  RFLM_DTR_RX_STATE_LTE_1P4MHZ_LIF,
  RFLM_DTR_RX_STATE_LTE_3MHZ_LIF,
  RFLM_DTR_RX_STATE_LTE_5MHZ_LIF,
  RFLM_DTR_RX_STATE_LTE_10MHZ_LIF,
  RFLM_DTR_RX_STATE_LTE_15MHZ_LIF,
  RFLM_DTR_RX_STATE_LTE_20MHZ_LIF,
  RFLM_DTR_RX_STATE_LTE_CA_MODE1,
  RFLM_DTR_RX_STATE_LTE_CA_MODE2,
  RFLM_DTR_RX_STATE_LTE_CA_MODE3,
  RFLM_DTR_RX_STATE_LTE_CA_MODE4,
  RFLM_DTR_RX_STATE_LTE_CA_MODE5,
  RFLM_DTR_RX_STATE_LTE_CA_MODE6,
  RFLM_DTR_RX_STATE_LTE_CA_MODE7,
  RFLM_DTR_RX_STATE_LTE_CA_MODE8,
  RFLM_DTR_RX_STATE_LTE_CA_MODE9,
  RFLM_DTR_RX_STATE_LTE_CA_MODE10,
  RFLM_DTR_RX_STATE_LTE_CA_MODE11,
  RFLM_DTR_RX_STATE_LTE_CA_MODE12,
  RFLM_DTR_RX_STATE_TDS_TRK,
  RFLM_DTR_RX_STATE_TDS_ACQ,
  RFLM_DTR_RX_STATE_TDS_TRK_IRAT,
  RFLM_DTR_RX_STATE_TDS_TRK_IDLE,
  RFLM_DTR_RX_STATE_TDS_DC,
  RFLM_DTR_RX_STATE_1X_NONET,
  RFLM_DTR_RX_STATE_HDR_1P48MHZ_NONET,
  RFLM_DTR_RX_STATE_HDR_4P05MHZ_NONET,
  RFLM_DTR_RX_STATE_HDR_6P63MHZ_NONET,
  RFLM_DTR_RX_STATE_HDR_9P21MHZ_NONET,
  RFLM_DTR_RX_STATE_SB_SVDO_NONET,
  RFLM_DTR_RX_STATE_1X_NONET_WTR2605,
  RFLM_DTR_RX_STATE_HDR_1P48MHZ_NONET_WTR2605,
  RFLM_DTR_RX_STATE_FBRX_MODE1,
  RFLM_DTR_RX_STATE_FBRX_MODE2,
  RFLM_DTR_RX_STATE_FBRX_MODE3,
  RFLM_DTR_RX_STATE_FBRX_MODE4,
  RFLM_DTR_RX_STATE_FBRX_MODE4_DAC230P4,
  RFLM_DTR_RX_STATE_SELF_TEST_MODE1,
  RFLM_DTR_RX_STATE_GSM_ONLINE_POWER,
  RFLM_DTR_RX_STATE_XPT_RXFB_UMTS_1C,
  RFLM_DTR_RX_STATE_XPT_RXFB_CDMA_1X,
  RFLM_DTR_RX_STATE_XPT_RXFB_LTE10MHZ,
  RFLM_DTR_RX_STATE_XPT_RXFB_TDS,
  RFLM_DTR_RX_STATE_XPT_RXFB_LTE20MHZ,
  RFLM_DTR_RX_STATE_XPT_RXFB_UMTS_1C_DAC230P4,
  RFLM_DTR_RX_STATE_XPT_RXFB_CDMA_1X_DAC230P4,
  RFLM_DTR_RX_STATE_XPT_RXFB_TDS_DAC230P4,
  RFLM_DTR_RX_STATE_UMTS_XO_CAL,
  RFLM_DTR_RX_STATE_CDMA_XO_CAL,
  RFLM_DTR_RX_STATE_LTE_XO_CAL,
  RFLM_DTR_RX_STATE_DEBUG_WB0_ADC_OUT,
  RFLM_DTR_RX_STATE_DEBUG_WB1_ADC_OUT,
  RFLM_DTR_RX_STATE_DEBUG_WB2_ADC_OUT,
  RFLM_DTR_RX_STATE_DEBUG_WB3_ADC_OUT,
  RFLM_DTR_RX_STATE_DEBUG_WB4_ADC_OUT,
  RFLM_DTR_RX_STATE_ADC_VCM_CAL,
  RFLM_DTR_RX_STATE_NUM,
  RFLM_DTR_RX_STATE_INVALID
}rflm_dtr_rx_state_enum_type;



typedef enum
{
  RFLM_DTR_RX_NUM_CARR_1, 
  RFLM_DTR_RX_NUM_CARR_2, 
  RFLM_DTR_RX_NUM_CARR_3, 
  RFLM_DTR_RX_NUM_CARR_4, 
  RFLM_DTR_RX_NUM_CARR_DEFAULT, 
  RFLM_DTR_RX_NUM_CARR_NUM,
  RFLM_DTR_RX_NUM_CARR_INVALID
}rflm_dtr_rx_enum_num_carr;



typedef enum
{
  RFLM_DTR_RX_BANDWIDTH_DEFAULT, 
  RFLM_DTR_RX_BANDWIDTH_1P4, 
  RFLM_DTR_RX_BANDWIDTH_3, 
  RFLM_DTR_RX_BANDWIDTH_5, 
  RFLM_DTR_RX_BANDWIDTH_10, 
  RFLM_DTR_RX_BANDWIDTH_15, 
  RFLM_DTR_RX_BANDWIDTH_20, 
  RFLM_DTR_RX_BANDWIDTH_NUM,
  RFLM_DTR_RX_BANDWIDTH_INVALID
}rflm_dtr_rx_enum_bandwidth;



typedef enum
{
  RFLM_DTR_RX_MODE_UMTS_VOICE_NONET, 
  RFLM_DTR_RX_MODE_UMTS_DATA_NONET, 
  RFLM_DTR_RX_MODE_UMTS_VOICE_DSR_NONET, 
  RFLM_DTR_RX_MODE_UMTS_MODGSM_ACQ, 
  RFLM_DTR_RX_MODE_GERAN_NONEV, 
  RFLM_DTR_RX_MODE_GERAN_EV, 
  RFLM_DTR_RX_MODE_IRAT, 
  RFLM_DTR_RX_MODE_DEFAULT, 
  RFLM_DTR_RX_MODE_SVS2, 
  RFLM_DTR_RX_MODE_LIF, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_5_0_5, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_10_0_10, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_15_0_15, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_20_0_20, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_15_0_20, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_20_0_15, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_15_5_15, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_15_10_15, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_5_0_10, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_10_0_5, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_5_0_15, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_15_0_5, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_5_0_20, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_20_0_5, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_10_0_15, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_15_0_10, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_10_0_20, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_20_0_10, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_5_10_5, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_10_5_10, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_10_10_10, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_5_5_10, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_10_5_5, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_5_10_10, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_10_10_5, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_5_15_10, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_10_15_5, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_5_20_10, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_10_20_5, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_5_25_10, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_10_25_5, 
  RFLM_DTR_RX_MODE_SINGLE_RX_CA_5_5_5, 
  RFLM_DTR_RX_MODE_TDS_TRK, 
  RFLM_DTR_RX_MODE_TDS_ACQ, 
  RFLM_DTR_RX_MODE_TDS_TRK_IRAT, 
  RFLM_DTR_RX_MODE_TDS_IDLE, 
  RFLM_DTR_RX_MODE_TDS_DC, 
  RFLM_DTR_RX_MODE_1X_NONET, 
  RFLM_DTR_RX_MODE_HDR_1P48MHZ, 
  RFLM_DTR_RX_MODE_HDR_4P05MHZ, 
  RFLM_DTR_RX_MODE_HDR_6P63MHZ, 
  RFLM_DTR_RX_MODE_HDR_9P21MHZ, 
  RFLM_DTR_RX_MODE_HDR_12P44MHZ, 
  RFLM_DTR_RX_MODE_FBRX_MODE1, 
  RFLM_DTR_RX_MODE_FBRX_MODE2, 
  RFLM_DTR_RX_MODE_FBRX_MODE3, 
  RFLM_DTR_RX_MODE_FBRX_MODE4, 
  RFLM_DTR_RX_MODE_FBRX_MODE4_DAC230P4, 
  RFLM_DTR_RX_MODE_SELF_TEST_MODE1, 
  RFLM_DTR_RX_MODE_GSM_ONLINE_POWER, 
  RFLM_DTR_RX_MODE_XPT_UMTS, 
  RFLM_DTR_RX_MODE_XPT_CDMA, 
  RFLM_DTR_RX_MODE_XPT_LTE10, 
  RFLM_DTR_RX_MODE_XPT_TDS, 
  RFLM_DTR_RX_MODE_XPT_LTE20, 
  RFLM_DTR_RX_MODE_XPT_UMTS_DAC230P4, 
  RFLM_DTR_RX_MODE_XPT_CDMA_DAC230P4, 
  RFLM_DTR_RX_MODE_XPT_TDS_DAC230P4, 
  RFLM_DTR_RX_MODE_UMTS_XO_CAL, 
  RFLM_DTR_RX_MODE_1X_XO_CAL, 
  RFLM_DTR_RX_MODE_LTE_XO_CAL, 
  RFLM_DTR_RX_MODE_ADC_DC_CAL, 
  RFLM_DTR_RX_MODE_NUM,
  RFLM_DTR_RX_MODE_INVALID
}rflm_dtr_rx_enum_mode;



typedef enum
{
  RFLM_DTR_RX_TRX_WTR3925, 
  RFLM_DTR_RX_TRX_WTR4905, 
  RFLM_DTR_RX_TRX_WTR2605, 
  RFLM_DTR_RX_TRX_NUM,
  RFLM_DTR_RX_TRX_INVALID
}rflm_dtr_rx_enum_trx;



typedef enum
{
  RFLM_DTR_RX_DEBUG_PT_WB0_ADC_OUT, 
  RFLM_DTR_RX_DEBUG_PT_WB1_ADC_OUT, 
  RFLM_DTR_RX_DEBUG_PT_WB2_ADC_OUT, 
  RFLM_DTR_RX_DEBUG_PT_WB3_ADC_OUT, 
  RFLM_DTR_RX_DEBUG_PT_WB4_ADC_OUT, 
  RFLM_DTR_RX_DEBUG_PT_NUM,
  RFLM_DTR_RX_DEBUG_PT_INVALID
}rflm_dtr_rx_enum_debug_pt;



typedef struct
{
  rflm_dtr_rx_enum_num_carr num_carr;
  rflm_dtr_rx_enum_bandwidth bandwidth;
  rflm_dtr_rx_enum_mode mode;
  rflm_dtr_rx_enum_trx trx;
  rflm_dtr_rx_enum_debug_pt debug_pt;
}rxlm_state_cfg_params;



typedef enum
{
  RFLM_DTR_RX_HEADER,	/* Use Struct: rflm_dtr_rx_header_struct */
  RXLM_GROUP_CSR_XO_VARS,	/* Use Struct: rflm_dtr_rx_csr_xo_vars_group_struct */
  RXLM_GROUP_XO_VARS,	/* Use Struct: rflm_dtr_rx_xo_vars_group_struct */
  RXLM_GROUP_VSRC_XO_VARS,	/* Use Struct: rflm_dtr_rx_vsrc_xo_vars_group_struct */
  RXLM_GROUP_STMR_XO_VARS,	/* Use Struct: rflm_dtr_rx_stmr_xo_vars_group_struct */
  RXLM_GROUP_DELAY_VARS,	/* Use Struct: rflm_dtr_rx_delay_vars_group_struct */
  RXLM_GROUP_DVGA_VARS,	/* Use Struct: rflm_dtr_rx_dvga_vars_group_struct */
  RXLM_GROUP_VSRC_VARS,	/* Use Struct: rflm_dtr_rx_vsrc_vars_group_struct */
  RXLM_GROUP_ADC_VARS,	/* Use Struct: rflm_dtr_rx_adc_vars_group_struct */
  RXLM_GROUP_ADC_HIBERNATION_VARS,	/* Use Struct: rflm_dtr_rx_adc_hibernation_vars_group_struct */
  RXLM_GROUP_CSR_PHASE_COMP_VARS,	/* Use Struct: rflm_dtr_rx_csr_phase_comp_vars_group_struct */
  RXLM_GROUP_SCALE_VARS,	/* Use Struct: rflm_dtr_rx_scale_vars_group_struct */
  RXLM_GROUP_NOTCH_VARS,	/* Use Struct: rflm_dtr_rx_notch_vars_group_struct */
  RXLM_GROUP_LTE_FTM_CAL_VARS,	/* Use Struct: rflm_dtr_rx_lte_ftm_cal_vars_group_struct */
  RXLM_GROUP_ADC_CONTROL_V0,	/* Use Struct: rflm_dtr_rx_adc_control_v0_group_struct */
  RXLM_GROUP_ADC_CONFIG,	/* Use Struct: rflm_dtr_rx_adc_config_group_struct */
  RXLM_GROUP_ADC_CONTROL_V1,	/* Use Struct: rflm_dtr_rx_adc_control_v1_group_struct */
  RXLM_GROUP_ADC_CONTROL_V2,	/* Use Struct: rflm_dtr_rx_adc_control_v1_group_struct */
  RXLM_GROUP_COMMON_NOTCH_01,	/* Use Struct: rflm_dtr_rx_common_notch_01_group_struct */
  RXLM_GROUP_COMMON_NOTCH_23,	/* Use Struct: rflm_dtr_rx_common_notch_23_group_struct */
  RXLM_GROUP_VSRC_WA_COUNT,	/* Use Struct: rflm_dtr_rx_vsrc_wa_count_group_struct */
  RXLM_GROUP_IQMC_A_B_COEFFS,	/* Use Struct: rflm_dtr_rx_iqmc_a_b_coeffs_group_struct */
  RXLM_GROUP_IQMC,	/* Use Struct: rflm_dtr_rx_iqmc_group_struct */
  RXLM_GROUP_NOTCH,	/* Use Struct: rflm_dtr_rx_notch_group_struct */
  RXLM_GROUP_NB_WBMUX_NB0,	/* Use Struct: rflm_dtr_rx_nb_wbmux_nb_group_struct */
  RXLM_GROUP_TOP_NB0_V0,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB0_V1,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB0_V2,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB0_V3,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_NB_GATE_NB0,	/* Use Struct: rflm_dtr_rx_nb_gate_nb_group_struct */
  RXLM_GROUP_NB_DEC_NB0,	/* Use Struct: rflm_dtr_rx_nb_dec_nb_group_struct */
  RXLM_GROUP_EQ_NB0,	/* Use Struct: rflm_dtr_rx_nb_dec_nb_group_struct */
  RXLM_GROUP_NB_DC_NB0,	/* Use Struct: rflm_dtr_rx_nb_dc_nb_group_struct */
  RXLM_GROUP_NB_GDA_NB0,	/* Use Struct: rflm_dtr_rx_nb_gda_nb_group_struct */
  RXLM_GROUP_NB_PWR_NB0,	/* Use Struct: rflm_dtr_rx_nb_pwr_nb_group_struct */
  RXLM_GROUP_FINAL_BITWIDTHS_NB0,	/* Use Struct: rflm_dtr_rx_final_bitwidths_nb_group_struct */
  RXLM_GROUP_WRITER_NB0,	/* Use Struct: rflm_dtr_rx_writer_nb_group_struct */
  RXLM_GROUP_DBG_NB0,	/* Use Struct: rflm_dtr_rx_dbg_nb_group_struct */
  RXLM_GROUP_CSR_NB0,	/* Use Struct: rflm_dtr_rx_csr_nb_group_struct */
  RXLM_GROUP_CSR_PH_OFFSET_NB0,	/* Use Struct: rflm_dtr_rx_csr_ph_offset_nb_group_struct */
  RXLM_GROUP_WTR_FUNC_CFG_NB0,	/* Use Struct: rflm_dtr_rx_wtr_func_cfg_nb_group_struct */
  RXLM_GROUP_CSR_FREQ_OFFSET_NB0,	/* Use Struct: rflm_dtr_rx_csr_freq_offset_nb_group_struct */
  RXLM_GROUP_DVGA_NB0,	/* Use Struct: rflm_dtr_rx_dvga_nb_group_struct */
  RXLM_GROUP_ICIFIR_NB0,	/* Use Struct: rflm_dtr_rx_icifir_nb_group_struct */
  RXLM_GROUP_NB_WBMUX_NB1,	/* Use Struct: rflm_dtr_rx_nb_wbmux_nb_group_struct */
  RXLM_GROUP_TOP_NB1_V0,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB1_V1,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB1_V2,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB1_V3,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_NB_GATE_NB1,	/* Use Struct: rflm_dtr_rx_nb_gate_nb_group_struct */
  RXLM_GROUP_NB_DEC_NB1,	/* Use Struct: rflm_dtr_rx_nb_dec_nb_group_struct */
  RXLM_GROUP_EQ_NB1,	/* Use Struct: rflm_dtr_rx_nb_dec_nb_group_struct */
  RXLM_GROUP_NB_DC_NB1,	/* Use Struct: rflm_dtr_rx_nb_dc_nb_group_struct */
  RXLM_GROUP_NB_GDA_NB1,	/* Use Struct: rflm_dtr_rx_nb_gda_nb_group_struct */
  RXLM_GROUP_NB_PWR_NB1,	/* Use Struct: rflm_dtr_rx_nb_pwr_nb_group_struct */
  RXLM_GROUP_FINAL_BITWIDTHS_NB1,	/* Use Struct: rflm_dtr_rx_final_bitwidths_nb_group_struct */
  RXLM_GROUP_WRITER_NB1,	/* Use Struct: rflm_dtr_rx_writer_nb_group_struct */
  RXLM_GROUP_DBG_NB1,	/* Use Struct: rflm_dtr_rx_dbg_nb_group_struct */
  RXLM_GROUP_CSR_NB1,	/* Use Struct: rflm_dtr_rx_csr_nb_group_struct */
  RXLM_GROUP_CSR_PH_OFFSET_NB1,	/* Use Struct: rflm_dtr_rx_csr_ph_offset_nb_group_struct */
  RXLM_GROUP_WTR_FUNC_CFG_NB1,	/* Use Struct: rflm_dtr_rx_wtr_func_cfg_nb_group_struct */
  RXLM_GROUP_CSR_FREQ_OFFSET_NB1,	/* Use Struct: rflm_dtr_rx_csr_freq_offset_nb_group_struct */
  RXLM_GROUP_DVGA_NB1,	/* Use Struct: rflm_dtr_rx_dvga_nb_group_struct */
  RXLM_GROUP_ICIFIR_NB1,	/* Use Struct: rflm_dtr_rx_icifir_nb_group_struct */
  RXLM_GROUP_NB_WBMUX_NB2,	/* Use Struct: rflm_dtr_rx_nb_wbmux_nb_group_struct */
  RXLM_GROUP_TOP_NB2_V0,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB2_V1,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB2_V2,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB2_V3,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_NB_GATE_NB2,	/* Use Struct: rflm_dtr_rx_nb_gate_nb_group_struct */
  RXLM_GROUP_NB_DEC_NB2,	/* Use Struct: rflm_dtr_rx_nb_dec_nb_group_struct */
  RXLM_GROUP_EQ_NB2,	/* Use Struct: rflm_dtr_rx_nb_dec_nb_group_struct */
  RXLM_GROUP_NB_DC_NB2,	/* Use Struct: rflm_dtr_rx_nb_dc_nb_group_struct */
  RXLM_GROUP_NB_GDA_NB2,	/* Use Struct: rflm_dtr_rx_nb_gda_nb_group_struct */
  RXLM_GROUP_NB_PWR_NB2,	/* Use Struct: rflm_dtr_rx_nb_pwr_nb_group_struct */
  RXLM_GROUP_FINAL_BITWIDTHS_NB2,	/* Use Struct: rflm_dtr_rx_final_bitwidths_nb_group_struct */
  RXLM_GROUP_WRITER_NB2,	/* Use Struct: rflm_dtr_rx_writer_nb_group_struct */
  RXLM_GROUP_DBG_NB2,	/* Use Struct: rflm_dtr_rx_dbg_nb_group_struct */
  RXLM_GROUP_CSR_NB2,	/* Use Struct: rflm_dtr_rx_csr_nb_group_struct */
  RXLM_GROUP_CSR_PH_OFFSET_NB2,	/* Use Struct: rflm_dtr_rx_csr_ph_offset_nb_group_struct */
  RXLM_GROUP_WTR_FUNC_CFG_NB2,	/* Use Struct: rflm_dtr_rx_wtr_func_cfg_nb_group_struct */
  RXLM_GROUP_CSR_FREQ_OFFSET_NB2,	/* Use Struct: rflm_dtr_rx_csr_freq_offset_nb_group_struct */
  RXLM_GROUP_DVGA_NB2,	/* Use Struct: rflm_dtr_rx_dvga_nb_group_struct */
  RXLM_GROUP_ICIFIR_NB2,	/* Use Struct: rflm_dtr_rx_icifir_nb_group_struct */
  RXLM_GROUP_NB_WBMUX_NB3,	/* Use Struct: rflm_dtr_rx_nb_wbmux_nb_group_struct */
  RXLM_GROUP_TOP_NB3_V0,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB3_V1,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB3_V2,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_TOP_NB3_V3,	/* Use Struct: rflm_dtr_rx_top_nb_v0_group_struct */
  RXLM_GROUP_NB_GATE_NB3,	/* Use Struct: rflm_dtr_rx_nb_gate_nb_group_struct */
  RXLM_GROUP_NB_DEC_NB3,	/* Use Struct: rflm_dtr_rx_nb_dec_nb_group_struct */
  RXLM_GROUP_EQ_NB3,	/* Use Struct: rflm_dtr_rx_nb_dec_nb_group_struct */
  RXLM_GROUP_NB_DC_NB3,	/* Use Struct: rflm_dtr_rx_nb_dc_nb_group_struct */
  RXLM_GROUP_NB_GDA_NB3,	/* Use Struct: rflm_dtr_rx_nb_gda_nb_group_struct */
  RXLM_GROUP_NB_PWR_NB3,	/* Use Struct: rflm_dtr_rx_nb_pwr_nb_group_struct */
  RXLM_GROUP_FINAL_BITWIDTHS_NB3,	/* Use Struct: rflm_dtr_rx_final_bitwidths_nb_group_struct */
  RXLM_GROUP_WRITER_NB3,	/* Use Struct: rflm_dtr_rx_writer_nb_group_struct */
  RXLM_GROUP_DBG_NB3,	/* Use Struct: rflm_dtr_rx_dbg_nb_group_struct */
  RXLM_GROUP_CSR_NB3,	/* Use Struct: rflm_dtr_rx_csr_nb_group_struct */
  RXLM_GROUP_CSR_PH_OFFSET_NB3,	/* Use Struct: rflm_dtr_rx_csr_ph_offset_nb_group_struct */
  RXLM_GROUP_WTR_FUNC_CFG_NB3,	/* Use Struct: rflm_dtr_rx_wtr_func_cfg_nb_group_struct */
  RXLM_GROUP_CSR_FREQ_OFFSET_NB3,	/* Use Struct: rflm_dtr_rx_csr_freq_offset_nb_group_struct */
  RXLM_GROUP_DVGA_NB3,	/* Use Struct: rflm_dtr_rx_dvga_nb_group_struct */
  RXLM_GROUP_ICIFIR_NB3,	/* Use Struct: rflm_dtr_rx_icifir_nb_group_struct */
  RXLM_GROUP_TOP_DBG,	/* Use Struct: rflm_dtr_rx_top_dbg_group_struct */
  RXLM_GROUP_PBS_CFG,	/* Use Struct: rflm_dtr_rx_pbs_cfg_group_struct */
  RXLM_GROUP_RFIF_UPDATED,	/* Use Struct: rflm_dtr_rx_rfif_updated_group_struct */
  RXLM_GROUP_COMMON_ACI,	/* Use Struct: rflm_dtr_rx_common_aci_group_struct */
  RFLM_DTR_RX_BLOCK_ENABLE_FLAGS,	/* Use Struct: rflm_dtr_rx_block_valid_flags_struct */
  RFLM_DTR_RX_INDICES,	/* Use Struct: rflm_dtr_rx_indices_struct */
  RXLM_GROUP_NUM,
  RXLM_GROUP_INVALID
}rxlm_dyn_group_type;

typedef enum
{
  RFLM_DTR_RX_HIBERNATION_MODE1,
  RFLM_DTR_RX_HIBERNATION_MODE2,
  RFLM_DTR_RX_HIBERNATION_MODE3,
  RFLM_DTR_RX_ADC_HIBERNATION_SW_DYNAMIC_SETTINGS_NUM,
  RFLM_DTR_RX_ADC_HIBERNATION_SW_DYNAMIC_SETTINGS_INVALID
}rflm_dtr_rx_adc_hibernation_sw_dynamic_settings_enum_type;

typedef enum
{
  RFLM_DTR_RX_CDMA_DEFAULT_1X,
  RFLM_DTR_RX_CDMA_DEFAULT_DORA,
  RFLM_DTR_RX_CDMA_DEFAULT_DORB_CARRIER_OFFSET_0,
  RFLM_DTR_RX_CDMA_DEFAULT_DORB_CARRIER_OFFSET_1p25,
  RFLM_DTR_RX_CDMA_DEFAULT_DORB_CARRIER_OFFSET_2p5,
  RFLM_DTR_RX_CDMA_DEFAULT_DORB_CARRIER_OFFSET_3p75,
  RFLM_DTR_RX_CDMA_WTR4905_1X,
  RFLM_DTR_RX_CDMA_WTR4905_DORA,
  RFLM_DTR_RX_CDMA_ICIFIR_SW_DYNAMIC_SETTINGS_NUM,
  RFLM_DTR_RX_CDMA_ICIFIR_SW_DYNAMIC_SETTINGS_INVALID
}rflm_dtr_rx_cdma_icifir_sw_dynamic_settings_enum_type;

typedef enum
{
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_5_0_5,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_10_0_10,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_15_0_15,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_20_0_20,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_20_0_15,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_15_0_20,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_15_5_15,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_15_10_15,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_10_0_5,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_5_0_10,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_15_0_5,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_5_0_15,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_20_0_5,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_5_0_20,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_15_0_10,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_10_0_15,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_20_0_10,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_10_0_20,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_5_10_5,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_10_5_10,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_10_10_10,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_10_5_5,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_5_5_10,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_10_10_5,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_5_10_10,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_10_15_5,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_5_15_10,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_10_20_5,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_5_20_10,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_10_25_5,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_5_25_10,
  RFLM_DTR_RX_LTE_SINGLE_RX_CA_5_5_5,
  RFLM_DTR_RX_SINGLE_RX_CA_SW_DYNAMIC_SETTINGS_NUM,
  RFLM_DTR_RX_SINGLE_RX_CA_SW_DYNAMIC_SETTINGS_INVALID
}rflm_dtr_rx_single_rx_ca_sw_dynamic_settings_enum_type;

typedef enum
{
  RFLM_DTR_RX_W_1C_DSR_FTM_NBPWR,
  RFLM_DTR_RX_W_1C_FTM_NBPWR,
  RFLM_DTR_RX_W_2C_FTM_NBPWR,
  RFLM_DTR_RX_W_3C_FTM_NBPWR,
  RFLM_DTR_RX_W_4C_FTM_NBPWR,
  RFLM_DTR_RX_LTE_1P4_FTM_NBPWR,
  RFLM_DTR_RX_LTE_3_FTM_NBPWR,
  RFLM_DTR_RX_LTE_5_FTM_NBPWR,
  RFLM_DTR_RX_LTE_10_FTM_NBPWR,
  RFLM_DTR_RX_LTE_15_FTM_NBPWR,
  RFLM_DTR_RX_LTE_20_FTM_NBPWR,
  RFLM_DTR_RX_SINGLE_RX_CA_5_0_5,
  RFLM_DTR_RX_SINGLE_RX_CA_10_0_10,
  RFLM_DTR_RX_SINGLE_RX_CA_15_0_15,
  RFLM_DTR_RX_SINGLE_RX_CA_20_0_20,
  RFLM_DTR_RX_SINGLE_RX_CA_20_0_15,
  RFLM_DTR_RX_SINGLE_RX_CA_15_0_20,
  RFLM_DTR_RX_SINGLE_RX_CA_15_5_15,
  RFLM_DTR_RX_SINGLE_RX_CA_15_10_15,
  RFLM_DTR_RX_SINGLE_RX_CA_10_0_5,
  RFLM_DTR_RX_SINGLE_RX_CA_5_0_10,
  RFLM_DTR_RX_SINGLE_RX_CA_15_0_5,
  RFLM_DTR_RX_SINGLE_RX_CA_5_0_15,
  RFLM_DTR_RX_SINGLE_RX_CA_20_0_5,
  RFLM_DTR_RX_SINGLE_RX_CA_5_0_20,
  RFLM_DTR_RX_SINGLE_RX_CA_15_0_10,
  RFLM_DTR_RX_SINGLE_RX_CA_10_0_15,
  RFLM_DTR_RX_SINGLE_RX_CA_20_0_10,
  RFLM_DTR_RX_SINGLE_RX_CA_10_0_20,
  RFLM_DTR_RX_SINGLE_RX_CA_5_10_5,
  RFLM_DTR_RX_SINGLE_RX_CA_10_5_10,
  RFLM_DTR_RX_SINGLE_RX_CA_10_10_10,
  RFLM_DTR_RX_SINGLE_RX_CA_10_5_5,
  RFLM_DTR_RX_SINGLE_RX_CA_5_5_10,
  RFLM_DTR_RX_SINGLE_RX_CA_10_10_5,
  RFLM_DTR_RX_SINGLE_RX_CA_5_10_10,
  RFLM_DTR_RX_SINGLE_RX_CA_10_15_5,
  RFLM_DTR_RX_SINGLE_RX_CA_5_15_10,
  RFLM_DTR_RX_SINGLE_RX_CA_10_20_5,
  RFLM_DTR_RX_SINGLE_RX_CA_5_20_10,
  RFLM_DTR_RX_SINGLE_RX_CA_10_25_5,
  RFLM_DTR_RX_SINGLE_RX_CA_5_25_10,
  RFLM_DTR_RX_SINGLE_RX_CA_5_5_5,
  RFLM_DTR_RX_NBPWR_SW_DYNAMIC_SETTINGS_NUM,
  RFLM_DTR_RX_NBPWR_SW_DYNAMIC_SETTINGS_INVALID
}rflm_dtr_rx_nbpwr_sw_dynamic_settings_enum_type;

typedef enum
{
  RFLM_DTR_RX_ADC_WTR4905,
  RFLM_DTR_RX_ADC_SETTINGS_WTR4905_SW_DYNAMIC_SETTINGS_NUM,
  RFLM_DTR_RX_ADC_SETTINGS_WTR4905_SW_DYNAMIC_SETTINGS_INVALID
}rflm_dtr_rx_adc_settings_wtr4905_sw_dynamic_settings_enum_type;

typedef enum
{
  RFLM_DTR_RX_NBPWR,
  RFLM_DTR_RX_SINGLE_RX_CA,
  RFLM_DTR_RX_CDMA_ICIFIR,
  RFLM_DTR_RX_ADC_SETTINGS_WTR4905,
  RFLM_DTR_RX_ADC_HIBERNATION,
  RFLM_DTR_RX_BBRX_FUSE,
  RFLM_DTR_RX_DYN_FUNCTIONALITY_NUM,
  RFLM_DTR_RX_DYN_FUNCTIONALITY_INVALID
}rflm_dtr_rx_dyn_functionality_type;

typedef struct {
  rflm_dtr_rx_nbpwr_sw_dynamic_settings_enum_type nbpwr_sw_dynamic;
  rflm_dtr_rx_enum_mode mode;
  rflm_dtr_rx_single_rx_ca_sw_dynamic_settings_enum_type single_rx_ca_sw_dynamic;
}rflm_dtr_rx_ca_mode_return_struct;


#ifdef __cplusplus
}
#endif



#endif


