/*! 
  @file
  rflm_dtr_pll_fw_intf.c
 
  @brief
  Implements the RFLM DTR PLL Interface Functions to FW
 
*/

/*==============================================================================

  Copyright (c) 2012 - 2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/target/msm8996/src/rflm_dtr_pll_fw_intf.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
09/29/15   dej     Revert settings to 9x45 settings
10/30/13   vrb     Sequence fixes to enable WB and NB activation for GSM DTR PLL
10/28/13   vrb     Sequence fixes to enable WB and NB activation
10/24/13   vrb     Include rflm header to account for FW standalone dependencies
10/24/13   vrb     Moving to RFLM HWINTF for HWIO definitions
10/24/13   vrb     Enabled SWI changes and "status" sequence changes for Bolt 
10/21/13   vrb     Fixes for SWI changes and temporarily disabled functionality
09/30/13   vrb     Initial version.
==============================================================================*/

#include "rflm.h"
#include "comdef.h"
#include "rflm_api_cmn.h"
#include "rflm_hwintf.h"
#include "rflm_features.h"
#include "rflm_time.h"

#define RFLM_DTR_PLL_STATUS_BITMASK 0x1

INLINE void rflm_dtr_configure_standard_pll(void)
{
   uint8 pll_divide_int = 43; // 19.2*43 = 825.6 MHz
   
   // Setup default register values
   HWIO_OUT(MSS_MODEM_DTR_PLL1_MODE, 0x0);
   HWIO_OUT(MSS_MODEM_DTR_PLL1_USER_CTL,0x0);
   HWIO_OUT(MSS_MODEM_DTR_PLL1_USER_CTL,0x8);
   HWIO_OUT(MSS_MODEM_DTR_PLL1_CONFIG_CTL,0x4309);
   HWIO_OUT(MSS_MODEM_DTR_PLL1_TEST_CTL,0x0);
   HWIO_OUT(MSS_MODEM_DTR_PLL1_L_VAL, pll_divide_int);
   HWIO_OUT(MSS_MODEM_DTR_PLL1_ALPHA_VAL_L, 0x0);
   HWIO_OUT(MSS_MODEM_DTR_PLL1_ALPHA_VAL_H, 0x0);
}


INLINE void rflm_dtr_configure_gsm_pll(void)
{   
   uint8 pll_divide_int = 48; // 19.2*48 = 921.6 MHz
   
   // Setup default register values
   HWIO_OUT(MSS_MODEM_DTR_PLL2_MODE, 0x0);
   HWIO_OUT(MSS_MODEM_DTR_PLL2_USER_CTL,0x0);
   HWIO_OUT(MSS_MODEM_DTR_PLL2_USER_CTL,0x8);
   HWIO_OUT(MSS_MODEM_DTR_PLL2_CONFIG_CTL,0x4309);
   HWIO_OUT(MSS_MODEM_DTR_PLL2_TEST_CTL,0x0);
   HWIO_OUT(MSS_MODEM_DTR_PLL2_L_VAL, pll_divide_int);
   HWIO_OUT(MSS_MODEM_DTR_PLL2_ALPHA_VAL_L, 0x0);
   HWIO_OUT(MSS_MODEM_DTR_PLL2_ALPHA_VAL_H, 0x0);
}


INLINE boolean rflm_dtr_get_standard_pll_on_status(void)
{
   boolean pll_enabled = FALSE;

   uint8 status = HWIO_IN(MSS_MODEM_DTR_PLL1_STATUS) & RFLM_DTR_PLL_STATUS_BITMASK;
   uint8 lock_detect = HWIO_INF(MSS_MODEM_DTR_PLL1_MODE, LOCK_DET);

   if ( (status) && (lock_detect) )
   {
      pll_enabled = TRUE;
   }
   return pll_enabled;
}

INLINE boolean rflm_dtr_get_gsm_pll_on_status(void)
{
   boolean pll_enabled = FALSE;

   uint8 status = HWIO_IN(MSS_MODEM_DTR_PLL2_STATUS) & RFLM_DTR_PLL_STATUS_BITMASK;
   uint8 lock_detect = HWIO_INF(MSS_MODEM_DTR_PLL1_MODE, LOCK_DET);

   if ( (status) && (lock_detect) )
   {
      pll_enabled = TRUE;
   }
   return pll_enabled;
}

INLINE boolean rflm_dtr_enable_standard_pll(void)
{
   if (!rflm_dtr_get_standard_pll_on_status()) 
   {
      // Begin PLL turn on      
      HWIO_OUT(MSS_MODEM_DTR_PLL1_MODE, HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,BYPASSNL));

      rflm_time_wait_us(5);

      HWIO_OUT(MSS_MODEM_DTR_PLL1_MODE, HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,RESET_N)|
                           HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,BYPASSNL));

      while (HWIO_INF(MSS_MODEM_DTR_PLL1_MODE, LOCK_DET) == 0) 
      {
        rflm_time_wait_us(1);
      }

      HWIO_OUT(MSS_MODEM_DTR_PLL1_MODE, HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,RESET_N)|
                           HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,BYPASSNL)|
                           HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,OUTCTRL));
   }
   return rflm_dtr_get_standard_pll_on_status();
}


INLINE boolean rflm_dtr_enable_gsm_pll(void)
{
   if (!rflm_dtr_get_gsm_pll_on_status()) 
   {
      // Begin PLL turn on      
      HWIO_OUT(MSS_MODEM_DTR_PLL2_MODE, HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,BYPASSNL));

      rflm_time_wait_us(5);

      HWIO_OUT(MSS_MODEM_DTR_PLL2_MODE, HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,RESET_N)|
                           HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,BYPASSNL));

      while (HWIO_INF(MSS_MODEM_DTR_PLL2_MODE, LOCK_DET) == 0) 
      {
        rflm_time_wait_us(1);
      }

      HWIO_OUT(MSS_MODEM_DTR_PLL2_MODE, HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,RESET_N)|
                           HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,BYPASSNL)|
                           HWIO_FMSK(MSS_MODEM_DTR_PLL1_MODE,OUTCTRL));
   }
   return rflm_dtr_get_gsm_pll_on_status();
}

INLINE boolean rflm_dtr_disable_standard_pll(void)
{
   HWIO_OUT(MSS_MODEM_DTR_PLL1_MODE, 0x0);
   return !rflm_dtr_get_standard_pll_on_status();
}

INLINE boolean rflm_dtr_disable_gsm_pll(void)
{
   HWIO_OUT(MSS_MODEM_DTR_PLL2_MODE, 0x0);
   return !rflm_dtr_get_gsm_pll_on_status();
}


INLINE rflm_err_t rflm_dtr_enable_both_plls( boolean vote_on )
{
   rflm_err_t ret_val = RFLM_ERR_API_FAILED;
   boolean status = TRUE;
   if (vote_on)
   {
      status &= rflm_dtr_enable_gsm_pll();
      status &= rflm_dtr_enable_standard_pll();      
   }
   else
   {
      status &= rflm_dtr_disable_gsm_pll();
      status &= rflm_dtr_disable_standard_pll();
   }

   if (status)
   {
      ret_val = RFLM_ERR_NONE;
   }
   return ret_val;
}

/*==============================================================================

FUNCTION:  rflm_dtr_configure_plls

==============================================================================*/
/*!
  @brief
  Configure DTR PLLs to the prescribed frequencies

  @detail
 
  @return
  rflm error code
*/
/*============================================================================*/
rflm_err_t rflm_dtr_configure_plls( void )
{
   rflm_dtr_configure_gsm_pll();
   rflm_dtr_configure_standard_pll();
   return RFLM_ERR_NONE;
}

/*==============================================================================*/
/*!
  @brief
  Vote DTR PLL on for the corresponding Rx Handle

  @detail
  The DTR PLL associated with the specified Rx handle will be voted on
 
  @return
  rflm error code
*/
/*============================================================================*/
rflm_err_t rflm_dtr_rx_vote_pll( rflm_handle_rx_t handle,
                                 boolean vote_on )
{   
   return rflm_dtr_enable_both_plls(vote_on);
}

/*==============================================================================*/
/*!
  @brief
  Vote DTR PLL on for the corresponding Tx Handle

  @detail
  The DTR PLL associated with the specified Tx handle will be voted on
 
  @return
  rflm error code
*/
/*============================================================================*/
rflm_err_t rflm_dtr_tx_vote_pll( rflm_handle_tx_t handle,
                                 boolean vote_on )
{
   return rflm_dtr_enable_both_plls(vote_on);
}




