
#ifndef RFLM_ADC_DC_CAL_H
#define RFLM_ADC_DC_CAL_H


#ifdef __cplusplus
extern "C" {
#endif


/*==============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/modem/rfmodem/bolt/main/latest/lm/inc/rflm_adc_dc_cal.h
$Author: mplcsds1 $ 

when       who     what, where, why
--------   ---     -------------------------------------------------------------
10/27/15   sar    Added VCM Cal support for Istari MSM8996. 
07/21/15   sar    Updated EFS Path macros. Updated sizes for V1 adn V2 LUT. 
01/13/15   vv     Initial version

==============================================================================*/
                              

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"
#include "rxlm_intf.h"
#include "rflm_dtr_rx_typedef_ag.h"

#define EFS_ADC_DC_CAL_PATH             "/selfcal/modem/adc.dat"
#define EFS_ADC_DC_CAL_LOG_PATH         "/selfcal/modem/adclog.dat"
#define ADC_DC_OFFSET_SPEC_THRESHOLD    400 /*400mv*/
#define MAX_ADC_DC_CAL_V1_ENTRIES       27
#define MAX_ADC_DC_CAL_V2_ENTRIES       27
#define ADC_DC_CAL_MAX_ITERATIONS       4
#define ADC_CONFIG2_REG_DEFAULT         0x00088000
#define ADC_CONFIG3_REG_DEFAULT         0x00000000
#define ADC_REG_INVALID                 0xFFFFFFFF
#define ADC_INDEX_INVALID               0xFFFFFFFF
#define ADC_DC_VAL_THRESHOLD            10     /*10 mv*/
#define MILLI_VOLT_SCALING_FACTOR       1290   /*note the value is modem specific. it is same for Bolt/Bolt+ and thor modems*/

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Register mask to write 1:3 and 10:15 bits to adc_config2_reg and adc_config2_reg
  registers, only applies to THOR.
*/
#define BBRX_ADC_CAL_CONFIG2_REG_MASK 0x00007C00
#define BBRX_ADC_CAL_CONFIG3_REG_MASK 0x00000000


typedef struct{
   int32  start_range;
   int32  stop_range;
   uint32 config2_register_value;
   uint32 config3_register_value;
}rfcommon_msm_adc_vcm_cal_tbl_type; 

typedef struct
{
   uint32 adc;
   uint32 config2_register_value;
   uint32 config3_register_value;
}efs_adc_vcm_cal_data_type;

typedef struct
{
   uint32 adc;
   uint32 run_index;
   int32  curr_i;
   int32  curr_q;
   int32  adc_value_mv;
}efs_adc_vcm_cal_log_data_type;

/*To detect ADC VCM Calibration status*/
boolean rfcommon_msm_adc_vcm_cal_done;

efs_adc_vcm_cal_data_type      efs_adc_vcm_cal_data[RXLM_CHAIN_MAX];     /*to be written to efs file to store the calibrated data*/
efs_adc_vcm_cal_log_data_type  efs_adc_vcm_cal_log_data[RXLM_CHAIN_MAX*ADC_DC_CAL_MAX_ITERATIONS]; /*to be written to efs file for debug*/

boolean get_rfcommon_msm_adc_vcm_cal_tbl(const rfcommon_msm_adc_vcm_cal_tbl_type **rfcommon_msm_adc_vcm_cal_tbl_p);

#ifdef __cplusplus
}
#endif



#endif


