/*! 
  @file
  rflm_dtr_tx_dac_ctl.c
 
  @brief
  Implements the RFLM DTR tx Interface Functions to FW
 
*/

/*==============================================================================

  Copyright (c) 2012 - 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/target/mdm9x55/src/rflm_dtr_tx_dac_ctl_target.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
05/29/14   cvd      Initial version.
==============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "rflm.h"
#include "txlm_intf.h"
#include "rflm_dtr_tx_dac_ctl.h"
#include "rflm_dtr_tx_struct_ag.h"
#include "rflm_dtr_tx_typedef_ag.h"
#include "rflm_dtr_rx_fw_intf_ag.h"

#include "rflm_hwintf.h"
#include "rflm_features.h"

#ifdef FEATURE_RFA_ATLAS_MODEM
   #include "rf_hal_ccs.h" 
   #include "rf_hal_sbi.h"
   #include "rf_hal_common.h"
#endif

#include "rflm_time.h"
#if (RFLM_FEATURE_BUILD_MODE!=RFLM_FEATURE_BUILD_FW_LIB)
#include "DALSys.h"

rflm_dtr_tx_dac_therm_info_t rflm_dtr_tx_dac_therm_info[] =
{
  {
  	0,
	0,
  	{
  	  -346,
	  20,
	  85,
	  130,
	  -50,
	  5
  	}, /* Thor - TSMC */
  },
  {
  	0,
	1,
  	{
  	  159,
	  20,
	  85,
	  130,
	  -50,
	  5
  	}, /* Thor - GF */
  }
};
#endif /* SKIP FOR FW STANDALONE BUILD */

void rflm_dtr_tx_read_qfuse(txlm_chain_type chain, uint32* qfuse_rd)
{
  if (TXLM_CHAIN_0 == chain)
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_BMSK
    *qfuse_rd = HWIO_INF(QFPROM_CORR_CALIB_ROW6_MSB, MODEM_TXDAC0_CALIB);
#endif
  }
  else if (TXLM_CHAIN_1 == chain) 
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_BMSK
    *qfuse_rd = HWIO_INF(QFPROM_CORR_CALIB_ROW6_MSB, MODEM_TXDAC1_CALIB);
#endif
  }
  else
  {
    /* Invalid Chain Information: Returning default value */
    *qfuse_rd = 128;
  }
}

void rflm_dtr_tx_read_0_1_fuseflag_bit(uint32* fuseflag_bit)
{
#ifdef HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC_0_1_FUSEFLAG_BMSK
  *fuseflag_bit =  HWIO_INF(QFPROM_CORR_CALIB_ROW6_MSB, MODEM_TXDAC_0_1_FUSEFLAG);
#endif
}

void rflm_dtr_tx_read_avg_error(txlm_chain_type chain, uint32* avg_error)
{
  if (TXLM_CHAIN_0 == chain)
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_AVG_ERROR_BMSK
    *avg_error = HWIO_INF(QFPROM_CORR_CALIB_ROW6_MSB, MODEM_TXDAC0_CALIB_AVG_ERROR);
#endif
  }
  else if (TXLM_CHAIN_1 == chain)  
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_AVG_ERROR_BMSK
    *avg_error = HWIO_INF(QFPROM_CORR_CALIB_ROW6_MSB, MODEM_TXDAC1_CALIB_AVG_ERROR);
#endif
  }
}

void rflm_dtr_tx_read_overflow_bit(txlm_chain_type chain, uint32* overflow_bit)
{
  if (TXLM_CHAIN_0 == chain)
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_OVERFLOW_BMSK
    *overflow_bit = HWIO_INF(QFPROM_CORR_CALIB_ROW6_MSB, MODEM_TXDAC0_CALIB_OVERFLOW);
#endif
  }
  else if (TXLM_CHAIN_1 == chain) 
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_OVERFLOW_BMSK
    *overflow_bit = HWIO_INF(QFPROM_CORR_CALIB_ROW6_MSB, MODEM_TXDAC1_CALIB_OVERFLOW);
#endif
  }
  else
  {
    /* Invalid Chain Information: Returning default value */
    *overflow_bit = 0;
  }
}

uint32 txlm_read_foundry_register()
{
    uint32 foundry_id = 0;
 #ifdef HWIO_QFPROM_CORR_PTE2_IN
    foundry_id = (HWIO_IN(QFPROM_CORR_PTE2) & 0x700) >> 8;
 #endif
    return foundry_id;
}

#ifdef FEATURE_RFA_ATLAS_MODEM
rflm_err_t rflm_dtr_rx_get_group_settings( rflm_handle_rx_t handle,
                                           rflm_dtr_rx_fw_export_group_id group_id,
                                           uint32 *addr )
{
return 0;
}

rflm_err_t rflm_dtr_tx_get_group_settings( rflm_handle_tx_t handle,
                                           rflm_dtr_tx_fw_export_group_id group_id,
                                           uint32 *addr )
{
return 0;
}

boolean rflm_dtr_tx_and_et_dac_reprogram_msbcal_code(txlm_chain_type chain, boolean etdac_setup)
{
  return TRUE;
}

void rflm_dtr_tx_txdac_power_down(txlm_chain_type chain)
{

}

void rflm_dtr_tx_txdac_reactivate(txlm_chain_type chain, boolean blocking)
{

}

rflm_err_t rflm_dtr_rx_configure_chain( rflm_handle_rx_t handle,
                                        rflm_dtr_rx_indices_struct *rx_indices,
                                        uint32 config_mask )
{
return 0;
}
rflm_err_t rflm_dtr_tx_configure_chain( rflm_handle_tx_t handle,
                                        rflm_dtr_tx_indices_struct *tx_indices,  /* IGNORED */                                      
                                        uint32 config_mask,
                                        /*! Pointer to block of data need to be DMA transferred */
  										rflm_dtr_tx_xfer_list_s * txfe_xfer_list_ptr )
{
return 0;
}

rflm_err_t rflm_dtr_rx_adc_settings_low_pwr (uint32 adc_idx,
                                  rflm_dtr_rx_adc_low_pwr_state_enum state_enum)
{
return 0;
}

rflm_err_t rflm_dtr_rx_adc_settings_restore (rflm_handle_rx_t rxlm_handle)
{
return 0;
}

rflm_err_t rflm_dtr_rx_configure_dyn_notch(  rflm_handle_rx_t handle,
                                             rflm_set_dyn_notch_in_t *notch_in,
                                             rflm_set_dyn_notch_out_t *result )
{
return 0;
}
void rflm_dtr_tx_set_lmem_start_addr(
	                              rflm_dtr_tx_indices_struct *tx_indices,
								  /*! Pointer to LMEM start physical address */
                                  uint32 * lmem0_start_addr)
{

}

void rflm_dtr_tx_set_lmem_drif3_start_addr (
	                              rflm_dtr_tx_indices_struct *tx_indices,
								  /*! Pointer to LMEM start physical address */
                                  uint32 * lmem0_start_addr)
{

}

boolean rf_hal_bus_enable
(
  boolean enable,
  rf_hal_bus_client_handle** handle,
  rf_hal_client_enum client_enum
)
{
  return TRUE;
}

rf_hal_rfcmd_tq_type rf_hal_reserve_rfcmd_tq_pair
(
  boolean enable,
  rf_hal_rfcmd_tq_enum tq_pair,
  uint32 client
)
{
  return rf_hal_bus_rfi_reserve_tq_pair(enable,tq_pair,client);

}

rf_hal_bus_result_type rfhal_rffe_read_bytes_ext
(
  rf_hal_bus_rffe_type* script_ptr,
  uint8 channel,
  uint32 count,
  rf_hal_bus_resource_script_settings_type* settings
)
{
  return 0;
}
rf_hal_bus_result_type rf_hal_issue_event_generic
(
  rf_hal_rfcmd_tq_enum which_pair, /*! TQ pair */
  boolean force_immediate_timing /*! Make every task in this event IMMEDIATE trigger */
)
{
  return 0;
}
#endif

