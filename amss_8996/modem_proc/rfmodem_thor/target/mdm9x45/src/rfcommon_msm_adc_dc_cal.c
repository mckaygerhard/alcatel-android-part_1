/*! 
  @file
  rfcommon_msm_bbrx_adc_dc_cal_tbl.c
 
  @brief
  captures the BBRx ADC DC cal table 
 
*/

/*==============================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //source/qcom/qct/modem/rfmodem/bolt/main/latest/target/mdm9x45/src/rfcommon_msm_bbrx_adc_dc_cal_tbl.c

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
07/21/15   sar     Ported VCM Cal Support for Tesla V2.
06/02/15   sar     Removed Diag MSG to fix Standalone FW error.
05/11/15   sar     Updated get_rfcommon_msm_adc_vcm_cal_tbl to read modem ver only once.
12/16/14   vv      Initial version.
==============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "rflm_adc_dc_cal.h"
#include "rxlm_rxf_hal.h"

static const rfcommon_msm_adc_vcm_cal_tbl_type rfcommon_msm_adc_vcm_cal_tbl_v1[MAX_ADC_DC_CAL_V1_ENTRIES] =
{

   /*-431mv <= DC <= -422mv*/
   {
        -431,               /*start range*/
        -422,               /*stop_range*/
         0x00002400,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/    
   },

   /*-422mv <= DC <= -413mv*/
   {
        -422,               /*start range*/
        -413,               /*stop_range*/
         0x00002800,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/    
   },

   /*413mv <= DC <= --403mv*/
   {
        -413,               /*start range*/
        -403,               /*stop_range*/
         0x00002000,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/    
   },

   /*-403mv <= DC <= -394mv*/
   {
        -403,               /*start range*/
        -394,               /*stop_range*/
         0x00002C00,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/    
   },

   /*-394mv <= DC <= -385mv*/
   {
        -394,               /*start range*/
        -385,               /*stop_range*/
         0x00003000,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/ 
   },

   /*-385mv <= DC <= -376mv*/
   {
        -385,               /*start range*/
        -376,               /*stop_range*/
         0x00003400,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/ 
   },

   /*-376mv <= DC <= -367mv*/
   {
        -376,               /*start range*/
        -367,               /*stop_range*/
         0x00003800,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },

   /*-367mv <= DC <= -363mv*/
   {
        -367,               /*start range*/
        -363,               /*stop_range*/
         0x00006400,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },

   /*-363mv <= DC <= -358mv*/
   {
        -363,               /*start range*/
        -358,               /*stop_range*/
         0x00003C00,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/                           
   },

   /*-358mv <= DC <= -349mv*/
   {
        -358,               /*start range*/
        -349,               /*stop_range*/
         0x00006800,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },

   /*-349mv <= DC <= -339mv*/
   {
        -349,               /*start range*/
        -339,               /*stop_range*/
         0x00006000,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },

   /*-339mv <= DC <= -330mv*/
   {
        -339,               /*start range*/
        -330,               /*stop_range*/
         0x00006C00,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },

   /*-330mv <= DC <= -320mv*/
   {
        -330,               /*start range*/
        -320,               /*stop_range*/
         0x00007000,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/                           
   },

   /*-320mv <= DC <= -310mv*/
   {
        -320,               /*start range*/
        -310,               /*stop_range*/
         0x00007400,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },

   /*-310mv <= DC <= -303mv*/
   {
        -310,               /*start range*/
        -303,               /*stop_range*/
         0x00007800,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },

   /*-303mv <= DC <= -298mv*/
   {
        -303,               /*start range*/
        -298,               /*stop_range*/
         0x00000400,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },

   /*-298mv <= DC <= -293mv*/
   {
        -298,               /*start range*/
        -293,               /*stop_range*/
         0x00007C00,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },

   /*-293mv <= DC <= -285mv*/
   {
        -293,               /*start range*/
        -285,               /*stop_range*/
         0x00000800,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/                           
   },

   /*-285mv <= DC <= -275mv*/
   {
        -285,               /*start range*/
        -275,               /*stop_range*/
         0x00000000,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },

   /*-275mv <= DC <= -273mv*/
   {
        -275,               /*start range*/
        -273,               /*stop_range*/
         0x00002800,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },

   /*-273mv <= DC <= -263mv*/
   {
        -273,               /*start range*/
        -263,               /*stop_range*/
         0x00002000,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },

   /*-263mv <= DC <= -254mv*/
   {
        -263,               /*start range*/
        -254,               /*stop_range*/
         0x00002C00,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                                
   },

   /*-254mv <= DC <= -245mv*/
   {
        -254,               /*start range*/
        -245,               /*stop_range*/
         0x00003000,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },

   /*-245mv <= DC <= -236mv*/
   {
        -245,               /*start range*/
        -239,               /*stop_range*/
         0x00003400,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/
   },

   /*-236mv <= DC <= -227mv*/
   {
        -236,               /*start range*/
        -227,               /*stop_range*/
         0x00003800,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },

   /*-227mv <= DC <= -223mv*/
   {
        -227,               /*start range*/
        -223,               /*stop_range*/
         0x00006400,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/           
   },

   /*-223mv <= DC <= -218mv*/
   {
        -223,               /*start range*/
        -218,               /*stop_range*/
         0x00003C00,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },

   /*-218mv <= DC <= -209mv*/
   {
        -218,               /*start range*/
        -209,               /*stop_range*/
         0x00006800,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },

   /*-209mv <= DC <= -199mv*/
   {
        -209,               /*start range*/
        -199,               /*stop_range*/
         0x00006000,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },

   /*-199mv <= DC <= -190mv*/
   {
        -199,               /*start range*/
        -190,               /*stop_range*/
         0x00006C00,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },

   /*-190mv <= DC <= -180mv*/
   {
        -190,               /*start range*/
        -180,               /*stop_range*/
         0x00007000,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/ 
   },

   /*-180mv <= DC <= -170mv*/
   {
        -180,               /*start range*/
        -170,               /*stop_range*/
         0x00007400,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/ 
   },

   /*-170mv <= DC <= -163mv*/
   {
        -170,               /*start range*/
        -163,               /*stop_range*/
         0x00007800,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/ 
   },

   /*-163mv <= DC <= -158mv*/
   {
        -163,               /*start range*/
        -158,               /*stop_range*/
         0x00000400,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/ 
   },

   /*-158mv <= DC <= -153mv*/
   {
        -158,               /*start range*/
        -153,               /*stop_range*/
         0x00007C00,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/ 
   },

   /*-153mv <= DC <= -145mv*/
   {
        -153,               /*start range*/
        -145,               /*stop_range*/
         0x00000800,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/ 
   },

   /*-145mv <= DC <= -135mv*/
   {
        -145,               /*start range*/
        -135,               /*stop_range*/
         0x00000000,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/ 
   },
//-------------------- Table 2 ------------------------------------------------
   /*-135mv <= DC <= -133mv*/
   {  
        -135,               /*start range*/ 
        -133,               /*stop_range*/                     
         0x00002800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          

   /*-133mv <= DC <= -123mv*/
   {                                                           
        -133,               /*start range*/ 
        -123,               /*stop_range*/                     
         0x00002000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-123mv <= DC <= -114mv*/
   {                                                           
        -123,               /*start range*/
        -114,               /*stop_range*/                     
         0x00002C00,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-114mv <= DC <= -105mv*/
   {                                                           
        -114,               /*start range*/
        -105,               /*stop_range*/                     
         0x00003000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-105mv <= DC <= -96mv*/
   {                                                           
       -105,                /*/*start range*/
        -96,                /*stop_range*/                     
         0x00003400,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-96mv <= DC <= -87mv*/
   {                                                           
        -96,                /*start range*/                    
        -87,                /*stop_range*/                     
         0x00003800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-87mv <= DC <= -83mv*/
   {                                                           
        -87,                /*start range*/                    
        -83,                /*stop_range*/                     
         0x00006400,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-83mv <= DC <= -78mv*/
   {                                                           
        -83,                /*start range*/                    
        -78,                /*stop_range*/                     
         0x00003C00,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-78mv <= DC <= -69mv*/
   {                                                           
        -78,                /*start range*/                    
        -69,                /*stop_range*/                     
         0x00006800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-69mv <= DC <= -59mv*/
   {                                                           
        -69,                /*start range*/                    
        -60,                /*stop_range*/                     
         0x00006000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-59mv <= DC <= -50mv*/
   {                                                           
        -59,                /*start range*/                    
        -50,                /*stop_range*/                     
         0x00006C00,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-50mv <= DC <=  -40mv*/
   {                                                           
        -50,                /*start range*/                    
        -40,                /*stop_range*/                     
         0x00007000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-40mv <= DC <= -30mv*/
   {                                                           
        -40,                /*start range*/                    
        -30,                /*stop_range*/                     
         0x00007400,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-30mv <= DC <= -23mv*/
   {                                                           
        -30,                /*start range*/                    
        -23,                /*stop_range*/                     
         0x00007800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-23mv <= DC <= -18mv*/
   {                                                           
        -23,                /*start range*/                    
        -18,                /*stop_range*/                     
         0x00000400,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-18mv <= DC <= -13mv*/
   {                                                           
        -18,                /*start range*/                    
        -13,                /*stop_range*/                     
         0x00007C00,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-13mv <= DC <= -5mv*/
   {                                                           
        -13,                /*start range*/                    
        -5,                 /*stop_range*/                     
         0x00000800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*-5mv <= DC <= 5mv*/
   {                                                           
         -5,                /*start range*/                    
          5,                /*stop_range*/                     
         0x00000000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*5mv <= DC <= 15mv*/
   {                                                           
          5,                /*start range*/                    
         15,                /*stop_range*/                     
         0x00000C00,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*15mv <= DC <= 25mv*/
   {                                                           
         15,                /*start range*/                    
         25,                /*stop_range*/                     
         0x00001000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*25mv <= DC <= 35mv*/  
   {                                                        
         25,                /*start range*/                    
         35,                /*stop_range*/                     
         0x00001400,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                        

   /*35mv <= DC <= 42mv*/
   {                                                        
         35,                /*start range*/                    
         42,                /*stop_range*/                     
         0x00001800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                        

   /*42mv <= DC <= 47mv*/
   {                                                        
         42,                /*start range*/                    
         47,                /*stop_range*/                     
         0x00004400,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                        

   /*47mv <= DC <=  52mv*/
   {                                                          
         47,                /*start range*/                    
         52,                /*stop_range*/                     
         0x00001C00,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          

   /*52mv <= DC <= 59mv*/
   {                                                          
         52,                /*start range*/                    
         59,                /*stop_range*/                     
         0x00004800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          

   /*59mv <= DC <= 69mv*/
   {                                                          
         59,                /*start range*/                    
         69,                /*stop_range*/                     
         0x00004000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          

   /*69mv <= DC <= 80mv*/
   {                                                          
        69,                /*start range*/                    
        80,                /*stop_range*/                     
        0x00004C00,        /*config2 reg DC value from modem*/
        0x0000007E         /*config3 reg DC value from modem*/
   },                                                          

   /*80mv <= DC <= 90mv*/
   {                                                          
         80,                /*start range*/                    
         90,                /*stop_range*/                     
         0x00005000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          

   /*90mv <= DC <= 100mv*/
   {                                                          
         90,                /*start range*/                    
        100,                /*stop_range*/                     
         0x00005400,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          

   /*100mv <= DC <= 111mv*/
   {                                                          
        100,                /*start range*/                    
        111,                /*stop_range*/                     
         0x00005800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*111mv <= DC <= 118mv*/
   {                                                           
        111,                /*start range*/                    
        118,                /*stop_range*/                     
         0x00005C00,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           

   /*118mv <= DC <= 122mv*/
   {                                                           
        118,                /*start range*/                    
        122,                /*stop_range*/                     
         0x00000400,        /*config2 reg DC value from modem*/
         0x0000005A         /*config3 reg DC value from modem*/
   },                                                         

   /*122mv <= DC <= 127mv*/
   {                                                         
        122,                /*start range*/                    
        127,                /*stop_range*/                     
         0x00007C00,        /*config2 reg DC value from modem*/
         0x0000005A         /*config3 reg DC value from modem*/
   },                                                         

   /*127mv <= DC <= 135mv*/
   {                                                         
        127,                /*start range*/                    
        135,                /*stop_range*/                     
         0x00000800,        /*config2 reg DC value from modem*/
         0x0000005A         /*config3 reg DC value from modem*/
   },                                                         

   /*135mv <= DC <= 145mv*/
   {                                                         
        135,                /*start range*/                    
        145,                /*stop_range*/                     
         0x00000000,        /*config2 reg DC value from modem*/
         0x0000005A         /*config3 reg DC value from modem*/
   },                                                         

   /*145mv <= DC <= 155v*/
   {                                                         
        145,                /*start range*/                    
        155,                /*stop_range*/                     
         0x00000C00,        /*config2 reg DC value from modem*/
         0x0000005A         /*config3 reg DC value from modem*/
   },                                                         

   /*155mv <= DC <= 165mv*/
   {                                                         
        155,                /*start range*/                    
        165,                /*stop_range*/                     
         0x00001000,        /*config2 reg DC value from modem*/
         0x0000005A         /*config3 reg DC value from modem*/
   },

   /*165mv <= DC <= 175mv*/
   {
        165,                /*start range*/                     
        175,                /*stop_range*/                      
         0x00001400,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },

   /*175mv <= DC <= 182mv*/
   {
        175,                /*start range*/                     
        182,                /*stop_range*/                      
         0x00001800,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },

   /*182mv <= DC <= 187mv*/
   {
        182,                /*start range*/                     
        187,                /*stop_range*/                      
         0x00004400,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },

   /*187mv <= DC <= 192mv*/
   {
        187,                /*start range*/                     
        192,                /*stop_range*/                      
         0x00001C00,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },

   /*192mv <= DC <= 199mv*/
   {
        192,                /*start range*/                     
        199,                /*stop_range*/                      
         0x00004800,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },

   /*199mv <= DC <= 209mv*/
   {
        199,                /*start range*/                     
        209,                /*stop_range*/                      
         0x00004000,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },

   /*209mv <= DC <= 220mv*/
   {
        209,                /*start range*/                     
        220,                /*stop_range*/                      
         0x00004C00,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },

   /*220mv <= DC <= 230mv*/
   {
        220,                /*start range*/                     
        230,                /*stop_range*/                      
         0x00005000,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },

   /*230mv <= DC <= 240mv*/
   {
        230,                /*start range*/                     
        240,                /*stop_range*/                      
         0x00005400,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },

   /*240mv <= DC <= 251mv*/
   {
        240,                /*start range*/                     
        251,                /*stop_range*/                      
         0x00005800,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },

   /*251mv <= DC <= 258mv*/
   {
        251,                /*start range*/                     
        258,                /*stop_range*/                      
         0x00005C00,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },

   /*258mv <= DC <= 262mv*/
   {
        258,                /*start range*/                     
        262,                /*stop_range*/                      
         0x00000400,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*262mv <= DC <= 267mv*/
   {
        262,                /*start range*/                     
        267,                /*stop_range*/                      
         0x00007C00,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*267mv <= DC <= 275mv*/
   {
        267,                /*start range*/                     
        275,                /*stop_range*/                      
         0x00000800,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*275mv <= DC <= 285mv*/
   {
        275,                /*start range*/                     
        285,                /*stop_range*/                      
         0x00000000,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*285mv <= DC <= 295mv*/
   {
        285,                /*start range*/                     
        295,                /*stop_range*/                      
         0x00000C00,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*295mv <= DC <= 305mv*/
   {
        295,                /*start range*/                     
        305,                /*stop_range*/                      
         0x00001000,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*305mv <= DC <= 315mv*/
   {
        305,                /*start range*/                     
        315,                /*stop_range*/                      
        0x00001400,         /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*315mv <= DC <= 321mv*/
   {
        315,                /*start range*/                     
        321,                /*stop_range*/                      
         0x00001800,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*321mv <= DC <= 327mv*/
   {
        321,                /*start range*/                     
        327,                /*stop_range*/                      
         0x00004400,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*327mv <= DC <= 332mv*/
   {
        327,                /*start range*/                     
        332,                /*stop_range*/                      
         0x00001C00,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*332mv <= DC <= 339mv*/
   {
        332,                /*start range*/                     
        339,                /*stop_range*/                      
         0x00004800,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*339mv <= DC <= 349mv*/
   {
        339,                /*start range*/                     
        349,                /*stop_range*/                      
         0x00004000,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*349mv <= DC <= 360mv*/
   {
        349,                /*start range*/                     
        360,                /*stop_range*/                      
         0x00004C00,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*360mv <= DC <= 370mv*/
   {
        360,                /*start range*/                     
        370,                /*stop_range*/                      
        0x00005000,         /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*370mv <= DC <= 380mv*/
   {
        370,                /*start range*/                     
        380,                /*stop_range*/                      
         0x00005400,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*380mv <= DC <= 391mv*/
   {
        380,                /*start range*/                     
        391,                /*stop_range*/                      
         0x00005800,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },

   /*391mv <= DC <= 401mv*/
   {
        391,                /*start range*/                     
        401,                /*stop_range*/                      
         0x00005C00,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   }


};

static const rfcommon_msm_adc_vcm_cal_tbl_type rfcommon_msm_adc_vcm_cal_tbl_v2[MAX_ADC_DC_CAL_V2_ENTRIES] =
{


   /*-569mv   <= DC <= -551mv*/
   {
        -569,               /*start range*/                    
        -551,               /*stop_range*/                     
         0x00002400,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/    
   },
   /*-551mv   <= DC <= -534mv*/
   {
        -551,               /*start range*/                    
        -534,               /*stop_range*/                     
        0x00002000,         /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/    
   },
   /*-534mv   <= DC <=  -517mv*/
   {
        -534,               /*start range*/                    
        -517,               /*stop_range*/                     
        0x00002800,         /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/    
   },
   /*-517mv   <= DC <=  -500mv*/
   {
        -517,               /*start range*/                    
        -500,               /*stop_range*/                     
         0x00002C00,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/    
   },
   /*-500mv   <= DC <=   -484mv*/
   {
        -500,               /*start range*/                    
        -484,               /*stop_range*/                     
         0x00003000,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/ 
   },
   /*-484mv   <= DC <=   -467mv*/
   {
        -484,               /*start range*/                     
        -467,               /*stop_range*/                      
         0x00003400,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/ 
   },
   /*-467mv   <= DC <=    -450mv*/
   {
        -467,               /*start range*/                     
        -450,               /*stop_range*/                      
         0x00003800,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },
   /*-450mv   <= DC <=    -436mv*/ 
   {
        -450,               /*start range*/                     
        -436,               /*stop_range*/                      
         0x00003C00,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/                           
   },
   /*-436mv   <= DC <=    -428mv*/
   {
        -436,               /*start range*/                     
        -428,               /*stop_range*/                      
        0x00006400,         /*config2 reg DC value from modem*/ 
         0x00000036         /*config3 reg DC value from modem*/
   },
   /*-428mv   <= DC <=    -423mv*/
   {
        -428,               /*start range*/                     
        -423,               /*stop_range*/                      
         0x00006000,        /*config2 reg DC value from modem*/
         0x00000036         /*config3 reg DC value from modem*/
   },
   /*-423mv   <= DC <=    -411mv*/
   {
        -423,               /*start range*/                     
        -411,               /*stop_range*/                      
        0x00002400,         /*config2 reg DC value from modem*/ 
        0x00000012          /*config3 reg DC value from modem*/ 
   },
   /*-411mv   <= DC <=    -394mv*/
   {
        -411,              /*start range*/                                   
        -394,              /*stop_range*/                      
        0x00002000,        /*config2 reg DC value from modem*/ 
        0x00000012         /*config3 reg DC value from modem*/ 
   },
   /*-394mv   <= DC <=    -377mv*/
   {
        -394,              /*start range*/                      
        -377,              /*stop_range*/                      
         0x00002800,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },
   /*-377mv   <= DC <=    -360mv*/
   {
        -377,             /*start range*/                     
        -360,             /*stop_range*/                      
         0x00002C00,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                                
   },
   /*-360mv   <= DC <=    -344mv*/
   {
        -360,             /*start range*/                     
        -344,             /*stop_range*/                      
         0x00003000,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },
   /*-344mv   <= DC <=    -327mv*/
   {
        -344,             /*start range*/                              
        -327,             /*stop_range*/                      
         0x00003400,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/
   },
   /*-327mv   <= DC <=    -310mv*/
   {
        -327,             /*start range*/                     
        -310,             /*stop_range*/                      
         0x00003800,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },
   /*-310mv   <= DC <=    -296mv*/
   {
        -310,             /*start range*/                     
        -296,             /*stop_range*/                      
         0x00003C00,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },
   /*-296mv   <= DC <=    -288mv*/
   {
        -296,             /*start range*/                     
        -288,             /*stop_range*/                      
        0x00006400,       /*config2 reg DC value from modem*/ 
         0x00000012         /*config3 reg DC value from modem*/                               
   },
   /*-288mv   <= DC <=  -283mv*/
   {
        -288,             /*start range*/                     
        -283,             /*stop_range*/                      
         0x00006000,        /*config2 reg DC value from modem*/
         0x00000012         /*config3 reg DC value from modem*/                               
   },
   /*-283mv   <= DC <=  -271mv*/
   {
        -283,             /*start range*/                     
        -271,             /*stop_range*/                      
        0x00002400,       /*config2 reg DC value from modem*/ 
        0x0000007E        /*config3 reg DC value from modem*/ 
   },
   /*-271mv   <= DC <=  -254mv*/
   {
        -271,             /*start range*/                     
        -254,             /*stop_range*/                      
        0x00002000,       /*config2 reg DC value from modem*/ 
        0x0000007E        /*config3 reg DC value from modem*/ 
   },
   /*-254mv   <= DC <=  -237mv*/
   {
        -254,             /*start range*/                     
        -237,             /*stop_range*/                      
         0x00002800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          
   /*-237mv   <= DC <=  -220mv*/
   {                                                           
        -237,             /*start range*/                     
        -220,             /*stop_range*/                      
         0x00002C00,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-220mv   <= DC <=  -204mv*/
   {                                                           
        -220,             /*start range*/                     
        -204,             /*stop_range*/                      
         0x00003000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-204mv   <= DC <=  -187mv*/
   {                                                           
        -204,             /*start range*/                     
        -187,             /*stop_range*/                      
         0x00003400,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-187mv   <= DC <=  -170mv*/
   {                                                           
        -187,             /*start range*/                     
        -170,             /*stop_range*/                      
         0x00003800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-170mv   <= DC <=  -156mv*/
   {                                                           
        -170,            /*start range*/                      
        -156,            /*stop_range*/                      
        0x00003C00,      /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-156mv   <= DC <=  -140mv*/
   {                                                           
        -156,            /*start range*/                      
        -140,            /*stop_range*/                      
        0x00006400,      /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-140mv   <= DC <=  -122mv*/
   {                                                           
        -140,            /*start range*/                      
        -122,            /*stop_range*/                      
        0x00006000,      /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-122mv   <= DC <=  -104mv*/
   {                                                           
        -122,            /*start range*/                      
        -104,            /*stop_range*/                      
        0x00006800,      /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-104mv   <= DC <=  -85mv*/
   {                                                           
        -104,            /*start range*/                      
        -85,             /*stop_range*/                      
         0x00006C00,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-85mv   <= DC <=  -67mv*/
   {                                                           
        -85,            /*start range*/                      
        -67,            /*stop_range*/                      
         0x00007000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-67mv   <= DC <=  -48mv*/
   {                                                           
        -67,            /*start range*/                      
        -48,            /*stop_range*/                      
         0x00007400,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-48mv   <= DC <=  -30mv*/
   {                                                           
        -48,            /*start range*/                      
        -30,            /*stop_range*/                      
         0x00007800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-30mv   <= DC <=  -20mv*/
   {                                                           
        -30,            /*start range*/                      
        -20,            /*stop_range*/                      
        0x00007C00,     /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-20mv   <= DC <=  -10mv*/ 
   {                                                           
        -20,            /*start range*/                     
        -10,            /*stop_range*/                      
        0x00000400,     /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*-10mv   <= DC <=  10mv*/
   {                                                           
        -10,            /*start range*/                      
         10,            /*stop_range*/                      
        0x00000000,     /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*10mv   <= DC <=  30mv*/
   {                                                           
        10,             /*start range*/                     
        30,             /*stop_range*/                      
        0x00000800,     /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*30mv   <= DC <=  50mv*/
   {                                                           
        30,             /*start range*/                     
        50,             /*stop_range*/                      
         0x00000C00,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*50mv   <= DC <=  70mv*/
   {                                                           
        50,            /*start range*/                      
        70,            /*stop_range*/                      
         0x00001000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*70mv   <= DC <=  90mv*/
   {                                                        
        70,            /*start range*/                      
        90,            /*stop_range*/                      
         0x00001400,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                        
   /*90mv   <= DC <=  110mv*/
   {                                                        
        90,            /*start range*/                      
        110,           /*stop_range*/                      
         0x00001800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                        
   /*110mv   <= DC <=  122mv*/
   {                                                        
        110,          /*start range*/                        
        122,          /*stop_range*/                      
        0x00001C00,   /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                        
   /*122mv   <= DC <=  128mv*/
   {                                                          
        122,          /*start range*/                        
        128,          /*stop_range*/                      
        0x00004400,   /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          
   /*128mv   <= DC <=  142mv*/
   {                                                          
        128,          /*start range*/                        
        142,          /*stop_range*/                      
        0x00004000,   /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          
   /*142mv   <= DC <=  164mv*/
   {                                                          
        142,         /*start range*/                         
        164,         /*stop_range*/                      
        0x00004800,  /*config2 reg DC value from modem*/ 
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          
   /*164mv   <= DC <=  185mv*/
   {                                                          
        16,          /*start range*/                         
        185,         /*stop_range*/                      
        0x00004C00,        /*config2 reg DC value from modem*/
        0x0000007E         /*config3 reg DC value from modem*/
   },                                                          
   /*185mv   <= DC <=  207mv*/
   {                                                          
        185,         /*start range*/                         
        207,         /*stop_range*/                      
         0x00005000,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          
   /*207mv   <= DC <=  228mv*/
   {                                                          
        207,         /*start range*/                          
        228,         /*stop_range*/                      
         0x00005400,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                          
   /*228mv   <= DC <=  250mv*/
   {                                                          
        228,         /*start range*/                         
        250,         /*stop_range*/                      
         0x00005800,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*250mv   <= DC <=  263mv*/
   {                                                           
        250,         /*start range*/                          
        263,         /*stop_range*/                      
         0x00005C00,        /*config2 reg DC value from modem*/
         0x0000007E         /*config3 reg DC value from modem*/
   },                                                           
   /*263mv   <= DC <=  268mv*/
   {                                                         
        263,        /*start range*/                          
        268,        /*stop_range*/                      
         0x00004400,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },
   /*268mv   <= DC <=  282mv*/
   {
        268,        /*start range*/                          
        282,        /*stop_range*/                      
        0x00004000,   /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },
   /*282mv   <= DC <=  304mv*/
   {
        282,       /*start range*/                           
        304,       /*stop_range*/                      
         0x00004800,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },
   /*304mv   <= DC <=  325mv*/
   {
        304,       /*start range*/                           
        325,       /*stop_range*/                      
         0x00004C00,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },
   /*325mv   <= DC <=  347mv*/
   {
        325,       /*start range*/                           
        347,       /*stop_range*/                      
         0x00005000,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },
   /*347mv   <= DC <=  368mv*/
   {
        347,       /*start range*/                           
        368,       /*stop_range*/                      
         0x00005400,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },
   /*368mv   <= DC <=  390mv*/
   {
        368,       /*start range*/                           
        390,       /*stop_range*/                      
         0x00005800,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },
   /*390mv   <= DC <=  403mv*/
   {
        390,       /*start range*/                           
        403,       /*stop_range*/                      
         0x00005C00,        /*config2 reg DC value from modem*/ 
         0x0000005A         /*config3 reg DC value from modem*/ 
   },
   /*403mv   <= DC <=  408mv*/
   {
        403,       /*start range*/                           
        408,       /*stop_range*/                      
         0x00004400,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },
   /*408mv   <= DC <=  422mv*/
   {
        408,       /*start range*/                           
        422,       /*stop_range*/                      
        0x00004000,  /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },
   /*422mv   <= DC <=  444mv*/
   {
        422,       /*start range*/                           
        444,       /*stop_range*/                      
         0x00004800,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },
   /*444mv   <= DC <=  465mv*/
   {
        444,       /*start range*/                           
        465,       /*stop_range*/                      
         0x00004C00,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },
   /*465mv   <= DC <=  487mv*/
   {
        465,       /*start range*/                           
        487,       /*stop_range*/                      
        0x00005000,         /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },
   /*487mv   <= DC <=  508mv*/
   {
        487,       /*start range*/                           
        508,       /*stop_range*/                      
         0x00005400,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },
   /*508mv   <= DC <=  530mv*/
   {
        508,       /*start range*/                           
        530,       /*stop_range*/                      
         0x00005800,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   },
   /*530mv   <= DC <=  551mv*/
   {
        530,       /*start range*/                           
        551,       /*stop_range*/                      
         0x00005C00,        /*config2 reg DC value from modem*/ 
         0x00000048         /*config3 reg DC value from modem*/ 
   }

};


boolean get_rfcommon_msm_adc_vcm_cal_tbl(const rfcommon_msm_adc_vcm_cal_tbl_type **rfcommon_msm_adc_vcm_cal_tbl_p)
{
    static uint32 modem_version = 0;
    static boolean modem_version_read = FALSE;

    if (FALSE == modem_version_read )
    { 
    modem_version = rxlm_hal_rxf_get_modem_version();
       modem_version_read = TRUE;
    }

    switch (modem_version & 0xffffff00) 
    {
    case 0x20020100: /* MDM9x45 (Tesla) v1*/
       *rfcommon_msm_adc_vcm_cal_tbl_p = (const rfcommon_msm_adc_vcm_cal_tbl_type*)rfcommon_msm_adc_vcm_cal_tbl_v1;
       break;

    case 0x20020200: /* MDM9x45 (Tesla) v2 */
       *rfcommon_msm_adc_vcm_cal_tbl_p = (const rfcommon_msm_adc_vcm_cal_tbl_type*)rfcommon_msm_adc_vcm_cal_tbl_v2;
       break;

    default:
       *rfcommon_msm_adc_vcm_cal_tbl_p =  NULL;
       return FALSE;
    }

   return TRUE;

}

uint32 get_rfcommon_msm_adc_cal_tbl_size(void)
{
    uint32 cal_tbl_size = 0;
    static uint32 modem_version = 0;
    static boolean modem_version_read = FALSE;

    if (FALSE == modem_version_read )
    { 
       modem_version = rxlm_hal_rxf_get_modem_version();
       modem_version_read = TRUE;
    }

    switch (modem_version & 0xffffff00) 
    {
    case 0x20020100: /* MDM9x45 (Tesla) v1*/
       cal_tbl_size = MAX_ADC_DC_CAL_V1_ENTRIES ;
       break;

    case 0x20020200: /* MDM9x45 (Tesla) v2 */
       cal_tbl_size = MAX_ADC_DC_CAL_V2_ENTRIES ;
       break;

    default:
       break;
    }

   return cal_tbl_size;

}
