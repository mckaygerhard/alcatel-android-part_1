#ifndef LOC_LTE_TDP_MEASPROC_H
#define LOC_LTE_TDP_MEASPROC_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                Location LTE TDP MEASPROC Module Header File

GENERAL DESCRIPTION
This file contains structure definitions and function prototypes for the 
LTE OMP TDP module.

===========================================================================

  Copyright (c) 2014 Qualcomm Atheros, Inc.
  All Rights Reserved. 
  Qualcomm Atheros Confidential and Proprietary.		

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================

Version Control

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/gps/gnss/wwanme/lte_tdp/1_0/src/loc_lte_tdp_measproc.h#1 $
$DateTime: 2016/03/28 23:03:26 $
$Author: mplcsds1 $

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*--------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/
#include "loc_lte_otdoa_common_defs.h"
#include "loc_lte_tdp_control.h"
#include "loc_lte_otdoa_measproc.h"

/*--------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

typedef struct 
{
  uint32 q_Flags;  /* Flag to indicate measurement type */

  uint32 q_SeqNum;  /* sequence number */

  uint16  w_PhysicalId; /* Range: 0...503 */

  uint32 q_Earfcn; /* Radio frequency channel no. */

  uint8 u_NumRxAntennas; /* Number of Rx antennas i.e. number of Cer
                           vectors pointed to by pw_Cer below */

  uint16 w_CerSize; /* Range: 1..512 */

  loc_lte_tdp_BandwidthType e_SysBw;
  
  uint8 u_SignalPower;  /* Range (-140 dBm to -44 dBm) with 1 dBm 
                           resolution. */

  uint8  u_SignalQuality; /* Reference signal received quality.
                           For PRS measurements, the signalQuality will 
                           be RSRQ as defined in LPP. Range (-19.5 dB 
                           to -3dB with 0.5 dB resolution. The same 
                           values and encoding will be used for CRS 
                           measurements. */

  int32 x_ServNgbrAdjTs[LOC_LTE_TDP_MAX_UE_RX_ANT]; /* System Frame Dl time Adj
                                                     to be used for Ngbr cell.
                                                     Will be zero for serv */

  uint16 **ppw_Cer; /* pointer to a max of 2 CER vectors of maximum 512 taps */

} llomp_tdp_EapInputStructType;


/*--------------------------------------------------------------------------
 * Function Definitions
 *-----------------------------------------------------------------------*/

/*
 ******************************************************************************
 * Function: loc_lte_tdp_ComputeEap
 *
 * Description: API called by OC task to send meas to OMP task for it to  
 *              compute Eap.
 *
 * Parameters: pz_TdpEapProc - Pointer to the Eap input structure
 *  
 * Dependencies:
 *  None
 *
 * Return value:
 *  None
 *
 ******************************************************************************
 */
void loc_lte_tdp_ComputeEap(llomp_tdp_EapInputStructType *pz_TdpEapInput);

/*
 ******************************************************************************
 * Function: loc_lte_otdoa_measproc_ProcCustomMsg
 *
 * Description: OMP task Message processor for processing TDP related msgs
 *
 * Parameters: p_Msg - Pointer to the IPC payload
 *  
 * Dependencies:
 *  None
 *
 * Return value:
 *  None
 *
 ******************************************************************************
 */
void loc_lte_tdp_measproc_ProcCustomMsg ( const os_IpcMsgType *const p_Msg );

#endif /* #ifndef LOC_LTE_TDP_MEASPROC_H */