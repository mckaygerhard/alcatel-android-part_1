/*!
  @file
  ds_3gpp_loopback_hdlr.c

  @brief
  REQUIRED brief one-sentence description of this C module.

  @detail
  OPTIONAL detailed description of this C module.
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2009-2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/3gpp/ps/src/ds_3gpp_loopback_hdlr.c#2 $ $DateTime: 2016/10/05 20:44:44 $ $Author: marylin $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/30/14   sb     Loopback modehandler using 3gppmodehandler wm

===========================================================================*/



#include "ds_3gpp_loopback_hdlr.h"
#include "ds_eps_bearer_context.h"
#include "ds_eps_pdn_context.h"
#include "ds_loopback_hdlr.h"
#include "modem_mem.h"
#include "lte_pdcp_msg.h"
#include "msgr.h"
#include "ds_um_lo.h"
#include "emm_ext_msg.h"
#include "comptask_api.h"
#include "comptask_v.h"
#include "pdcp.h"
#include "dsutil.h"
#include "ds3gcfgmgr.h"
#include "ds_3gpp_nv_manager.h"

#define LB_CALL_EVENT_MAX 5
#define LB_CALL_SS_EVENT_MAX 1
#define INIT_EPS_BEARER_ID 5
#define INIT_CALL_ID 0
#define ATTACH_APN_1 "vzwims"
#define DS_3GPP_LOOPBACK_CONFIG_FILE \
            "/nv/item_files/modem/data/3gpp/ps/loopback_config.txt"


static ds_3gpp_loopback_context_info_type ds_3gpp_loopback_context_info = {NULL};
static lb_call_info* lb_info_array[LB_CALL_EVENT_MAX] ={NULL};
static lb_call_ss_info*  lb_ss_info_array[LB_CALL_SS_EVENT_MAX] = {NULL};
static loopback_wm_info_type  loopback_wm_tbl[DS_3GPP_MAX_BEARER_CONTEXT] = {{NULL}};

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_GET_IPV4_ADDR

DESCRIPTION
  This function returns an IPv4 addr for the given call id
 
PARAMETERS
  call_id - call_id to be assigned an addr
 
DEPENDENCIES
  None.

RETURN VALUE
  A IPv4 addr
 
SIDE EFFECTS
  None.

===========================================================================*/
uint32 ds_3gpp_loopback_get_ipv4_addr
(
  uint8 call_id
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_CLIENT_CALL_REG

DESCRIPTION
  This function is used by ds3g to register for cm call events from the Loopback
  Handler.In this function the event type and function pointer are passed
  as parameters.
 
PARAMETERS
  call_event_func - function pointer
  event_reg_type - cm call event/cm ss event
  call_event - event for which client is registered

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
lb_client_status_e_type ds_3gpp_loopback_client_call_reg(
    cm_mm_call_event_f_type     *call_event_func,
        /* Pointer to a callback function to notify the client of call
        ** events */
    lb_client_event_reg_e_type  event_reg_type,
    cm_call_event_e_type        call_event
)
{
  uint8 index = 0;
  lb_client_status_e_type lb_status =  LB_CLIENT_ERR_EVENT_REG;
  if(event_reg_type == LB_CLIENT_EVENT_REG && 
     call_event_func != NULL)
  {
    while(index < LB_CALL_EVENT_MAX)
    {
      if(lb_info_array[index] == NULL)
      {
        lb_info_array[index] = modem_mem_alloc(sizeof(lb_call_info),
                                              MODEM_MEM_CLIENT_DATA_CRIT);
        lb_info_array[index]->cm_event_type = call_event;
        lb_info_array[index]->call_event_func = call_event_func;
        lb_status = LB_CLIENT_OK;
        break;
      }
      index++;

    }
  }
  return lb_status;
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_CLIENT_SS_CALL_REG

DESCRIPTION
  This function is used by ds3g to register for cm ss events from the Loopback
  Handler.In this function the event type and function pointer are passed
  as parameters.
 
PARAMETERS
  call_event_func - function pointer
  event_reg_type - cm call event/cm ss event
  call_event - event for which client is registered

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

lb_client_status_e_type ds_3gpp_loopback_client_ss_call_reg(
    cm_mm_msim_ss_event_f_type      *ss_event_func,
        /* Pointer to a callback function to notify the client of call
        ** events */
    lb_client_event_reg_e_type  event_reg_type,
    cm_ss_event_e_type          call_event
)
{
  uint8 index = 0;
  lb_client_status_e_type lb_status =  LB_CLIENT_ERR_EVENT_REG;

  if(event_reg_type == LB_CLIENT_SS_EVENT_REG && 
     ss_event_func != NULL)
  {
    while(index < LB_CALL_SS_EVENT_MAX)
    {
      if(lb_ss_info_array[index] == NULL)
      {
        lb_ss_info_array[index] = modem_mem_alloc(sizeof(lb_call_ss_info),
                                                  MODEM_MEM_CLIENT_DATA_CRIT);
        lb_ss_info_array[index]->cm_ss_event_type = call_event;
        lb_ss_info_array[index]->ss_event_func = ss_event_func;
        lb_status = LB_CLIENT_OK;
        break;
      }
      index++;
    }
  }
  return LB_CLIENT_OK;
}
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_SYS_MODE

DESCRIPTION
  This function is used to return the current loopback handler sys mode.
  This is read as part of efs configuration information
 
PARAMETERS
  None

DEPENDENCIES
  None.

RETURN VALUE
..loopback_sys_mode -current loopback sys mode
 
SIDE EFFECTS
  None.

===========================================================================*/
sys_sys_mode_e_type ds_3gpp_loopback_hdlr_sys_mode()
{
  DS_3GPP_MSG1_HIGH("DS 3GPP LOOPBACK SYS MODE : %d",ds_3gpp_loopback_context_info.loopback_sys_mode);
  return ds_3gpp_loopback_context_info.loopback_sys_mode;
}


/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_CHECK_TOKENID

DESCRIPTION
  This function is used to check the paramter read from the efs file against
  the preconfigured loopback parameters and approprioate token id is returned
 
PARAMETERS
  from-
  to-

 
 
DEPENDENCIES
  

RETURN VALUE
..ds_3gpp_loopback_hdlr_token - Token read from the efs

SIDE EFFECTS
  None.

===========================================================================*/

ds_3gpp_loopback_hdlr_token ds_3gpp_loopback_hdlr_check_token_id
(
  char *from,
  char *to
)
{
  ds_3gpp_loopback_hdlr_token ret_token = LOOPBACK_TOKEN_DEFAULT; /* return value */
  char comp_string_sys_mode[] = "SYS MODE";
  char comp_string_dl_mulf[]="DL_MULF";
  char comp_string_dl_dupf[]="DL_DUPF";
  char comp_string_ul_only[]="UL_ONLY";
  char comp_string_lb_enabled[] ="LOOPBACK_ENABLED";
  uint8 length = 0;

  if  ( from > to )  
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "Invalid Parameters");
    return ret_token;
  }

  /* Assumption that the token length will not be greater than 255 */
  length =(uint8)(to-from);

  if(strncasecmp(from,comp_string_lb_enabled,length) == 0)
  {
    ret_token = LOOPBACK_TOKEN_LOOPBACK_ENABLED;
  }

  if(strncasecmp(from,comp_string_sys_mode,length) == 0)
  {
    ret_token = LOOPBACK_TOKEN_SYS_MODE;
  }

  if(strncasecmp(from,comp_string_dl_mulf,length) == 0)
  {
    ret_token = LOOPBACK_TOKEN_DL_MULF;
  }

  if(strncasecmp(from,comp_string_dl_dupf,length) == 0)
  {
    ret_token = LOOPBACK_TOKEN_DL_DUPF;
  }
  
  if(strncasecmp(from,comp_string_ul_only,length) == 0)
  {
    ret_token = LOOPBACK_TOKEN_UL_ONLY;
  }

  return ret_token;

} 


/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_IS_ENABLED

DESCRIPTION
  This function is used to check whether Loopback is enabled in the current
  mode of operation.
 
PARAMETERS

DEPENDENCIES
  

RETURN VALUE
..boolean - TRUE/FALSE (indicating whether Loopback is enabled)

SIDE EFFECTS
  None.
===========================================================================*/
boolean ds_3gpp_loopback_is_enabled()
{
  if( ds_3gpp_loopback_context_info.ds_3gpp_loopback_mode == TRUE) 
  {
    DS_3GPP_MSG1_HIGH("DS 3GPP LOOPBACK ENABLED: %d",
                      ds_3gpp_loopback_context_info.ds_3gpp_loopback_mode);
    return TRUE;
  }
  return FALSE;
}


/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_READ_EFS

DESCRIPTION
  This function is used to read the efs during dstask initialization.This function
  will be used to read the following paramters:
  LOOPBACK_ENABLED:1
  SYS MODE:9
  DL_MULF:3
  DL_DUPF:0
  UL_ONLY:0
 
PARAMETERS

DEPENDENCIES
  

RETURN VALUE
..boolean - TRUE/FALSE (indicating whether Loopback is enabled)

SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_hdlr_read_efs()
{

  char              file_name[] = DS_3GPP_LOOPBACK_CONFIG_FILE; 
  ds3gcfgmgr_efs_token_type efs_db;  /* structure used for tokenizing the file*/
  char             *from    = NULL;    /* pointer to the End of the token*/
  char             *to      = NULL;      /* pointer to the start of the token */
  ds3gcfgmgr_efs_token_parse_status_type   ret_val = DS3GCFGMGR_EFS_TOKEN_PARSE_SUCCESS;
  boolean param_name = TRUE; 
  ds_3gpp_loopback_hdlr_token param_id = LOOPBACK_TOKEN_DEFAULT;
  uint8 length =0;
  int  atoi_result =0;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  DS_3GPP_MSG0_MED("Read Loopback config from EFS");
  
  if (ds_3gpp_nv_manager_get_data_shark() == TRUE)
  {
    DS_3GPP_MSG0_HIGH("DS_3GPP_DATA_SHARK_ENABLE NV is set. Ignore EFS settings");

    ds_3gpp_loopback_context_info.current_eps_id = INIT_EPS_BEARER_ID;
    ds_3gpp_loopback_context_info.current_call_id = INIT_CALL_ID;
    ds_3gpp_loopback_context_info.lb_dl_mulf =0;
    ds_3gpp_loopback_context_info.lb_dl_dupf = 0;
    ds_3gpp_loopback_context_info.lb_ul_only = 0;
    ds_3gpp_loopback_context_info.ds_3gpp_loopback_mode = TRUE;
    ds_3gpp_loopback_context_info.lb_rx_sig = (ps_sig_enum_type)((uint32)PS_3GPP_LB_SIGNAL_0);
    ds_3gpp_loopback_context_info.loopback_sys_mode = SYS_SYS_MODE_WCDMA;
    ds_3gpp_loopback_context_info.loopback_subs_id = SYS_MODEM_AS_ID_1;

    return;
  }
  else
  {
    ds_3gpp_loopback_context_info.current_eps_id = INIT_EPS_BEARER_ID;
    ds_3gpp_loopback_context_info.current_call_id = INIT_CALL_ID;
    ds_3gpp_loopback_context_info.lb_dl_mulf =0;
    ds_3gpp_loopback_context_info.lb_dl_dupf = 0;
    ds_3gpp_loopback_context_info.lb_ul_only = 0;
    ds_3gpp_loopback_context_info.ds_3gpp_loopback_mode = FALSE;
    ds_3gpp_loopback_context_info.lb_rx_sig = (ps_sig_enum_type)((uint32)PS_3GPP_LB_SIGNAL_0);
    ds_3gpp_loopback_context_info.loopback_sys_mode = SYS_SYS_MODE_WCDMA;
    ds_3gpp_loopback_context_info.loopback_subs_id = SYS_MODEM_AS_ID_1;  
  }
  
  
  /*--------------------------------------------------------------------------- 
    Open the file and Initialize the EFS State machine
  ---------------------------------------------------------------------------*/

  if(ds3gcfgmgr_efs_file_init(file_name, &efs_db) == -1)
  {
    DS_3GPP_MSG0_HIGH("Loopback config: Error reading list from EFS");
    return;
  }


  efs_db.seperator = ':';
  /*lint -save -esym(644,token_id) param_name boolean flag takes care of this */
  while (DS3GCFGMGR_EFS_TOKEN_PARSE_EOF 
          != (ret_val = ds3gcfgmgr_efs_tokenizer(&efs_db, &from, &to )))
  {
    if (DS3GCFGMGR_EFS_TOKEN_PARSE_FAILURE == ret_val)
    {
      DS_3GPP_MSG0_ERROR("Token Parse Failure");
      break;
    }
    /*------------------------------------------------------------------------
      Token being read. 'from' points to the beginning of the token and 
      'to' point to the end of the token.

      The tokenizer automatically skips blank lines and comments (lines 
      beginning with #, so no need to check for them here).
    ------------------------------------------------------------------------*/
    if((from == to) || (DS3GCFGMGR_EFS_TOKEN_PARSE_EOL == ret_val))
    {
      /*----------------------------------------------------------------------
        Skip empty tokens.
      ----------------------------------------------------------------------*/
      continue;
    }     
    else if(DS3GCFGMGR_EFS_TOKEN_PARSE_SUCCESS == ret_val)
    {
      /*--------------------------------------------------------------------- 
      check if we are looking for param name or param value 
      ---------------------------------------------------------------------*/
      if (param_name == TRUE)
      {
        /*------------------------------------------------------------------
        get the token identifier for this param name 
        ------------------------------------------------------------------*/
        if ((param_id = ds_3gpp_loopback_hdlr_check_token_id(from,to)) < LOOPBACK_TOKEN_DEFAULT)
        {
          /* This is an error scenario, Skip till the end of the line? */
          DATA_3GMGR_MSG0( MSG_LEGACY_ERROR, "Incorrect Param Name" );
          param_id = LOOPBACK_TOKEN_DEFAULT;
          return;
        }
        else
        {
          /*-----------------------------------------------------------------
          set param_name as FALSE This means the next token is a 
          param value
          -----------------------------------------------------------------*/
          param_name = FALSE;
          /* set the seperator as ; */
          efs_db.seperator = ';';
        }
        
      }
      /*---------------------------------------------------------------------
      This means that the token is a param value
      ---------------------------------------------------------------------*/
      else 
      {
        length =(uint8)(to-from);
        DSUTIL_ATOI(from,to,atoi_result);

        switch(param_id)
        {
        case LOOPBACK_TOKEN_LOOPBACK_ENABLED:
            ds_3gpp_loopback_context_info.ds_3gpp_loopback_mode = atoi_result;
            DS_3GPP_MSG1_HIGH("DS 3GPP LOOPBACK  SET ENABLED: %d",ds_3gpp_loopback_context_info.ds_3gpp_loopback_mode); 
            break;
          case LOOPBACK_TOKEN_SYS_MODE:
            ds_3gpp_loopback_context_info.loopback_sys_mode = atoi_result;
            DS_3GPP_MSG1_HIGH("DS 3GPP LOOPBACK  SET SYS MODE: %d",ds_3gpp_loopback_context_info.loopback_sys_mode); 
            break;
          case LOOPBACK_TOKEN_DL_MULF:
            ds_3gpp_loopback_context_info.lb_dl_mulf = atoi_result;
            DS_3GPP_MSG1_HIGH("DS 3GPP LOOPBACK  SET LOOPBACK DL MULF: %d",ds_3gpp_loopback_context_info.lb_dl_mulf); 
            break;
          case LOOPBACK_TOKEN_DL_DUPF:
            ds_3gpp_loopback_context_info.lb_dl_dupf = atoi_result;
            DS_3GPP_MSG1_HIGH("DS 3GPP LOOPBACK  SET LOOPBACK DL DUPF: %d",ds_3gpp_loopback_context_info.lb_dl_dupf); 
            break;
          case LOOPBACK_TOKEN_UL_ONLY:
            ds_3gpp_loopback_context_info.lb_ul_only = atoi_result;
            DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK  SET LOOPBACK UL ONLY: %d",ds_3gpp_loopback_context_info.lb_ul_only); 
            break;
          default:
            break;
        }

        /*-------------------------------------------------------------------
         set param_name as TRUE This means that next token is a param name
        -------------------------------------------------------------------*/
        param_name = TRUE;
        param_id = LOOPBACK_TOKEN_DEFAULT;
        /* Set the seperator as : */
        efs_db.seperator = ':';
      }
    }
  }
  ds3gcfgmgr_efs_file_close(&efs_db);
  return;
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_ATTACH

DESCRIPTION
  This function is used to attach to the sys mode read as part of configuration
  As part of the function CM SS Event callback is issued with the Loopback sys mode.
  In case of Lte; this function is responsible for sending Get Pdn connection
  Indication
 
PARAMETERS
.. 
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_hdlr_attach()
{
  ds_3gpp_loopback_hdlr_send_fullservice_ind();
  ds_loopback_hdlr_alloc_dyn_byte();
  ds_loopback_hdlr_alloc_dyn_unsigned_char();

  if(ds_3gpp_loopback_context_info.loopback_sys_mode == SYS_SYS_MODE_LTE )
  {
    ds_3gpp_loopback_send_pdnconnection_ind();
  }

  (void)ps_set_sig_handler(ds_3gpp_loopback_context_info.lb_rx_sig,
                           ds_3gpp_loopback_hdlr_process_data_pkt, 
                           NULL);

}
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_SEND_PDN_CONNECTION_INDICATION

DESCRIPTION
  This function is responsible for sending the CM_CALL_EVENT_GET_PDN_CONN_IND
  indication for LTE with sequence no:1. This function is invoked in case of
  LTE loopback mode as part of attach.
 
PARAMETERS
.. 
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_send_pdnconnection_ind()
{
  cm_mm_call_info_s_type *call_info_ptr = NULL;

  call_info_ptr	= (cm_mm_call_info_s_type *)modem_mem_alloc(
     sizeof(cm_mm_call_info_s_type),MODEM_MEM_CLIENT_DATA_CRIT);

  if(call_info_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("Memory Allocation Failed.");
    return;
  }

  memset(call_info_ptr, 0, sizeof(cm_mm_call_info_s_type));
  call_info_ptr->call_type = CM_CALL_TYPE_PS_DATA;
  call_info_ptr->sys_mode = ds_3gpp_loopback_context_info.loopback_sys_mode;
  call_info_ptr->asubs_id = ds_3gpp_loopback_context_info.loopback_subs_id;
  call_info_ptr->mode_info.info.lte_call.seq_num = 1;

  DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                     " SENDING GET PDN CONNECTION INDICATION SEQ NO: %d",call_info_ptr->mode_info.info.lte_call.seq_num); 
  ds_3gpp_loopback_hdlr_send_cm_call_back_event(CM_CALL_EVENT_GET_PDN_CONN_IND,call_info_ptr);


  modem_mem_free(call_info_ptr, MODEM_MEM_CLIENT_DATA);
  return;
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_SEND_CM_CALL_BACK_EVENT

DESCRIPTION
  This function is responsible for calling the appropriate cm call event function
  based on the event and call info pointer passed
 
PARAMETERS
..cm_event_type - CM Call Event Type 
  call_info_ptr - CM Call Info Pointer
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/


void ds_3gpp_loopback_hdlr_send_cm_call_back_event
(
   cm_call_event_e_type cm_event_type,
   cm_mm_call_info_s_type *call_info_ptr
)
{
  uint8 iter_index=0;

  if(call_info_ptr != NULL)
  {
     while(iter_index != LB_CALL_EVENT_MAX)
     {
       if((lb_info_array[iter_index] != NULL) && 
          (lb_info_array[iter_index]->cm_event_type == cm_event_type))
       {
         lb_info_array[iter_index]->call_event_func(cm_event_type,
                                                 call_info_ptr);
       }
       iter_index++;
     }
  }

  return;
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_GET_EPSID

DESCRIPTION
  This function is used to return the eps id to be used for the call.This is
  done by incrementing the current eps id evertime. This is intialized to the
  default eps id.
 
PARAMETERS

 
DEPENDENCIES
  

RETURN VALUE
..eps_id - eps id to be used for the call id 
 
SIDE EFFECTS
  None.

===========================================================================*/


uint8 ds_3gpp_loopback_hdlr_get_epsid()
{
  uint8 ret_eps_id;
  ret_eps_id = ds_3gpp_loopback_context_info.current_eps_id;
  ds_3gpp_loopback_context_info.current_eps_id++;
  return ret_eps_id;

}
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_GET_CALLID

DESCRIPTION
  This function is used to return the callid to be used for the call.This is
  done by incrementing the current eps id evertime. This is intialized to the
  default callid.
 
PARAMETERS

 
DEPENDENCIES
  

RETURN VALUE
..call_id - call id to be used for the call
 
SIDE EFFECTS
  None.

===========================================================================*/



uint8 ds_3gpp_loopback_hdlr_get_callid()
{
  uint8 ret_call_id;
  ret_call_id = ds_3gpp_loopback_context_info.current_call_id;
  ds_3gpp_loopback_context_info.current_call_id++;
  return ret_call_id;
}
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_SET_V4_PDN_ADDRESS

DESCRIPTION
  This function is responsible for setting the v4 pdn address; in case of LTE
  loopback.The ip address assigned is based on the default loopback address added
  with the current call id
 
PARAMETERS
..pdn_adde - pdn address pointer
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/


void ds_3gpp_loopback_setV4PdnAddress(pdn_address_T *pdn_addr)
{
  uint32 ipv4_addr = 0;
  if (pdn_addr != NULL)
  {
    pdn_addr->valid = TRUE;
    pdn_addr->pdn_addr_len = 4;
    pdn_addr->pdn_type_val = NAS_ESM_IPV4;
    ipv4_addr = ds_3gpp_loopback_get_ipv4_addr(ds_3gpp_loopback_context_info.current_call_id);
    memscpy(pdn_addr->address, sizeof(ipv4_addr), &ipv4_addr, sizeof(ipv4_addr));
  }
}
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_SET_V6_PDN_ADDRESS

DESCRIPTION
  This function is responsible for setting the v6 pdn address; in case of LTE
  loopback.The ip address assigned is based on the default loopback address added
  with the current call id
 
PARAMETERS
..pdn_adde - pdn address pointer
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_loopback_setV6PdnAddress(pdn_address_T *pdn_addr)
{
  uint64 iid = 0xB30A8C0AB30A8C0ULL;
  if (pdn_addr != NULL)
  {
    pdn_addr->valid = TRUE;
    pdn_addr->pdn_addr_len = 8;
    pdn_addr->pdn_type_val = NAS_ESM_IPV6;
    iid = iid + ds_3gpp_loopback_context_info.current_call_id -1;
    memscpy(pdn_addr->address, sizeof(uint64), &iid, sizeof(uint64));
  }
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_SET_V4V6_PDN_ADDRESS

DESCRIPTION
  This function is responsible for setting the V4,v6 pdn address; in case of LTE
  loopback.The ip address assigned is based on the default loopback address added
  with the current call id
 
PARAMETERS
..pdn_adde - pdn address pointer
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/


void ds_3gpp_loopback_setV4V6PdnAddress(pdn_address_T *pdn_addr)
{
  uint32 ipv4_addr = 0;
  uint64 iid = 0xB30A8C0AB30A8C0ULL;

  if (pdn_addr != NULL)
  {
    pdn_addr->valid = TRUE;
    pdn_addr->pdn_addr_len = 12;
    pdn_addr->pdn_type_val = NAS_ESM_IPV4V6;

    iid = iid + ds_3gpp_loopback_context_info.current_call_id -1;
    ipv4_addr = ds_3gpp_loopback_get_ipv4_addr(ds_3gpp_loopback_context_info.current_call_id);
    memscpy(pdn_addr->address, sizeof(uint64), &iid, sizeof(uint64));
    memscpy(&(pdn_addr->address[PDN_ADDRESS_IPV4V6_V4_OFFSET]), sizeof(ipv4_addr), &ipv4_addr, sizeof(ipv4_addr));
  }
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_SET_V4_PDP_ADDRESS

DESCRIPTION
  This function is responsible for setting the V4 pdp address; in case of GSM/WCDMA
  /LTE loopback mode.The ip address assigned is based on the default loopback address
  added with the current call id
 
PARAMETERS
..pdn_adde - pdn address pointer
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/



void ds_3gpp_loopback_setV4PdpAddress(pdp_address_T *pdp_addr)
{
  uint32 ipv4_addr = 0;
  if (pdp_addr != NULL)
  {
    pdp_addr->valid = TRUE;
    pdp_addr->pdp_addr_len = 4;
    pdp_addr->pdp_type_num = 0x21; //V4 address type
    pdp_addr->pdp_type_org = 1; //IETF organization
    ipv4_addr = ds_3gpp_loopback_get_ipv4_addr(ds_3gpp_loopback_context_info.current_call_id);
    memscpy(pdp_addr->address, sizeof(ipv4_addr), &ipv4_addr, sizeof(ipv4_addr));
  }

}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_SET_V6_PDP_ADDRESS

DESCRIPTION
  This function is responsible for setting the V6 pdp address; in case of GSM/WCDMA
  /TDS loopback mode.The ip address assigned is based on the default loopback address
  added with the current call id
 
PARAMETERS
..pdn_adde - pdn address pointer
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/


void ds_3gpp_loopback_setV6PdpAddress(pdp_address_T *pdp_addr)
{
  uint64 iid = 0xB30A8C0AB30A8C0ULL;
  if (pdp_addr != NULL)
  {
    pdp_addr->valid = TRUE;
    pdp_addr->pdp_addr_len = 16;
    pdp_addr->pdp_type_num = 0x57; //V6 address type
    pdp_addr->pdp_type_org = 1; //IETF organization
    iid = iid + ds_3gpp_loopback_context_info.current_call_id -1;
    memscpy(&(pdp_addr->address[8]), sizeof(uint64), &iid, sizeof(uint64));
  }
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_SET_V4V6_PDP_ADDRESS

DESCRIPTION
  This function is responsible for setting the V4V6 pdp address; in case of GSM/WCDMA
  /TDS loopback mode.The ip address assigned is based on the default loopback address
  added with the current call id
 
PARAMETERS
..pdn_adde - pdn address pointer
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/


void ds_3gpp_loopback_setV4V6PdpAddress(pdp_address_T *pdp_addr)
{
  uint32 ipv4_addr = 0;
  uint64 iid = 0xB30A8C0AB30A8C0ULL;
  if (pdp_addr != NULL)
  {
    pdp_addr->valid = TRUE;
    pdp_addr->pdp_addr_len = 18;
    pdp_addr->pdp_type_num = 0x8D; // v4v6 addr type
    pdp_addr->pdp_type_org = 1; //IETF organization
    iid = iid + ds_3gpp_loopback_context_info.current_call_id -1;
    memscpy(&(pdp_addr->address[12]), sizeof(uint64), &iid, sizeof(uint64));
    ipv4_addr = ds_3gpp_loopback_get_ipv4_addr(ds_3gpp_loopback_context_info.current_call_id);
    memscpy(pdp_addr->address, sizeof(ipv4_addr), &ipv4_addr, sizeof(ipv4_addr));
  }
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_PDN_CONNECTIVITY REQUEST

DESCRIPTION
  This function is responsible for handling pdn connectivty sent as part of
  Lte call bring up in case of Loopback configuration. This function is responsible
  for setting up the apn, ip address based on the pdp type and send an
  activate bearer indication This function is also responsible for setting up the
  loopback wm info table with the call id, eps id  and initialize the singal for
  further data processing.
 
PARAMETERS
  cm_call_cmd_cb_f_type  cmd_cb_func,
  void                   *data_block_ptr,
  const cm_pdn_connectivity_s_type   *pdn_connectivity_ptr
    pointer to the request specific data
 
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_loopback_pdn_connectivity_req(

  cm_call_cmd_cb_f_type  cmd_cb_func,
    /**< client callback function */
  void                   *data_block_ptr,
    /**< pointer to client callback data block */
  const cm_pdn_connectivity_s_type   *pdn_connectivity_ptr
    /**< pointer to the request specific data */
)
{
  cm_mm_call_info_s_type *call_info_ptr;
  cm_call_id_type call_id;
  cm_lte_call_info_s_type * lte_call_info_p;
  uint8 index = 0;
  boolean v4_call = FALSE;
  boolean v6_call = FALSE;

  call_info_ptr	= (cm_mm_call_info_s_type *)modem_mem_alloc(sizeof(cm_mm_call_info_s_type),MODEM_MEM_CLIENT_DATA_CRIT);

  if(call_info_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("Memory Allocation Failed.");
    return FALSE;
  }

  memset(call_info_ptr, 0, sizeof(cm_mm_call_info_s_type));
  call_id = call_info_ptr->call_id = ds_3gpp_loopback_hdlr_get_callid();
  call_info_ptr->call_type = CM_CALL_TYPE_PS_DATA;
  call_info_ptr->sys_mode = ds_3gpp_loopback_context_info.loopback_sys_mode;
  call_info_ptr->direction = CM_CALL_DIRECTION_MT;
  call_info_ptr->asubs_id =  ds_3gpp_loopback_context_info.loopback_subs_id;
  call_info_ptr->mode_info.info_type = CM_CALL_MODE_INFO_LTE;
  lte_call_info_p = &(call_info_ptr->mode_info.info.lte_call);
  lte_call_info_p->eps_bearer_id = ds_3gpp_loopback_hdlr_get_epsid();

  DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                     "Handling Pdn connectivity Request: %d",lte_call_info_p->eps_bearer_id); 

  switch(pdn_connectivity_ptr->pdn_data.pdn_type)
  {
    case NAS_ESM_IPV4:
      ds_3gpp_loopback_setV4PdnAddress(&(lte_call_info_p->pdn_addr));
      v4_call = TRUE;
      break;
    case NAS_ESM_IPV6:
      ds_3gpp_loopback_setV6PdnAddress(&(lte_call_info_p->pdn_addr));
      v6_call = TRUE;
      break;
    case NAS_ESM_IPV4V6:
      ds_3gpp_loopback_setV4V6PdnAddress(&(lte_call_info_p->pdn_addr));
      v4_call = TRUE;
      v6_call = TRUE;
      break;
  }

  if(pdn_connectivity_ptr->pdn_data.apn_name.valid == FALSE ||
     pdn_connectivity_ptr->pdn_data.apn_name.address[0] == 0)
  {
    memset(lte_call_info_p->apn_name.address, 0, MAX_APN_ADR_LEN);

    lte_call_info_p->apn_name.address[0]= strlen(ATTACH_APN_1);

    memscpy( lte_call_info_p->apn_name.address+1,
            strlen(ATTACH_APN_1), ATTACH_APN_1, strlen(ATTACH_APN_1) );

    lte_call_info_p->apn_name.address[strlen(ATTACH_APN_1)+1] = '\0';
    lte_call_info_p->apn_name.apn_addr_len = strlen(ATTACH_APN_1)+2;
    lte_call_info_p->apn_name.valid = TRUE;
  }
  else
  {
    memscpy(lte_call_info_p->apn_name.address, 
            pdn_connectivity_ptr->pdn_data.apn_name.apn_addr_len,
            pdn_connectivity_ptr->pdn_data.apn_name.address,
            pdn_connectivity_ptr->pdn_data.apn_name.apn_addr_len);

    lte_call_info_p->apn_name.apn_addr_len = 
      pdn_connectivity_ptr->pdn_data.apn_name.apn_addr_len;
    lte_call_info_p->apn_name.valid = TRUE;
  }

  lte_call_info_p->sdf_id = pdn_connectivity_ptr->sdf_id;
  lte_call_info_p->gsm_umts_connection_id.valid = TRUE;

  DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                     "Sending Activate Bearer Indication: %d",call_id); 
  ds_3gpp_loopback_hdlr_send_cm_call_back_event(CM_CALL_EVENT_ACT_BEARER_IND,call_info_ptr);

  for(index=0;index < DS_3GPP_MAX_BEARER_CONTEXT ; index++ )
  {
    if(loopback_wm_tbl[index].is_active == 0)
    {
      loopback_wm_tbl[index].is_active = 1;
      loopback_wm_tbl[index].loop_back_eps_id = lte_call_info_p->eps_bearer_id;
      loopback_wm_tbl[index].loop_back_call_id = call_id;
      loopback_wm_tbl[index].loop_back_rb_id = loopback_wm_tbl[index].loop_back_eps_id;
      loopback_wm_tbl[index].v4_call = v4_call;
      loopback_wm_tbl[index].v6_call = v6_call;
      break;
    }
  }
  modem_mem_free(call_info_ptr, MODEM_MEM_CLIENT_DATA);
  return TRUE;

}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_ACT_BEARER_RSP

DESCRIPTION
  This function is responsible for handling activate bearer response as part of
  Lte call bring up in case of Loopback configuration.This function is responsible
  for sending cm call connected event followed by attach complte incase of INITIAL
  EPS Bearer. This functions is also responsible for sending the Rab Reestabilishment
  Indication which will inturn set up the data path.
 
PARAMETERS
  call_id - call id passed during bearer activation
  act_bearer_rsp_ptr - activate bearer response pointer

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/


boolean ds_3gpp_loopback_act_bearer_rsp
(
   cm_call_id_type                  call_id,
   const cm_act_bearer_rsp_s_type   *act_bearer_rsp_ptr
)
{

  cm_call_id_type call_local_id;
  cm_mm_call_info_s_type *call_info_ptr;
  cm_lte_call_info_s_type *lte_call_info_p;
  uint8 index =0;
  msgr_umid_type              msgrtype;
  emm_attach_complete_ind_type attach_ind;

  memset(&attach_ind, 0x0, sizeof(emm_attach_complete_ind_type));

  call_info_ptr	= (cm_mm_call_info_s_type *)modem_mem_alloc(sizeof(cm_mm_call_info_s_type),MODEM_MEM_CLIENT_DATA_CRIT);

  if(call_info_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("Memory Allocation Failed.");
    return FALSE;
  }

  memset(call_info_ptr, 0, sizeof(cm_mm_call_info_s_type));

  /*
    Obtain the call Id from the CM Sim call params. This call params was 
    populated during cm_mm_call_cmd_orig
  */
  call_local_id = call_id;
  call_info_ptr->asubs_id = ds_3gpp_loopback_context_info.loopback_subs_id;
  call_info_ptr->call_type = CM_CALL_TYPE_PS_DATA;
  call_info_ptr->sys_mode = ds_3gpp_loopback_context_info.loopback_sys_mode;
 
  call_info_ptr->call_id = call_local_id;
  call_info_ptr->end_status = CM_CALL_END_NONE;


  call_info_ptr->mode_info.info_type = CM_CALL_MODE_INFO_LTE;
  lte_call_info_p = &(call_info_ptr->mode_info.info.lte_call);

   DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                      "Sending Call connected Indication: %d",call_id); 

	for(index=0;index < DS_3GPP_MAX_BEARER_CONTEXT ; index++ )
  {
    if(loopback_wm_tbl[index].is_active == 1 &&
       loopback_wm_tbl[index].loop_back_call_id == call_id)
    {
      lte_call_info_p->eps_bearer_id = loopback_wm_tbl[index].loop_back_eps_id;
    }
  }
  ds_3gpp_loopback_hdlr_send_cm_call_back_event(CM_CALL_EVENT_CONNECT,call_info_ptr);
  if(lte_call_info_p->eps_bearer_id == INIT_EPS_BEARER_ID)
  {
    msgrtype = NAS_EMM_ATTACH_COMPLETE_IND;
    attach_ind.msg_hdr.inst_id = SYS_AS_ID_TO_INST_ID(SYS_MODEM_AS_ID_1);
    ds_eps_pdn_cntx_attach_complete_ind_hdlr(msgrtype,&(attach_ind.msg_hdr));
  }


   DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                      "Sending Rab reestabilish Indication: %d",call_id); 
  ds_3gpp_loopback_hdlr_send_cm_call_back_event(CM_CALL_EVENT_RAB_REESTAB_IND,call_info_ptr);
  modem_mem_free(call_info_ptr, MODEM_MEM_CLIENT_DATA);

  return TRUE;
}

/*===========================================================================
FUNCTION  DS_3GPP_LOOPBACK_LTE_PDCPDL_RAB_REGISTER_REQ

DESCRIPTION
  This function is responsible for handling lte pdcp ul rab registration.
  This function stores the uplink wm pointer in the loopback_wm_table 
 
PARAMETERS
  eps_bearer_id      eps bearer id
  tx_wm_item         tx wm pointer
  uint8              inst_id

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_loopback_lte_pdcpul_rab_register_req
(
  uint8               eps_bearer_id,
  dsm_watermark_type* tx_wm_item,
  uint8               inst_id
)
{
  uint8 index =0;
  ds_cmd_type     *cmd_ptr;
  loopback_wm_reg_type   *wm_reg_info_ptr;


   DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                      "Handling Lte pdcp ul registration: %d",eps_bearer_id); 

  for(index=0;index < DS_3GPP_MAX_BEARER_CONTEXT ; index++ )
  {
    if(loopback_wm_tbl[index].is_active == 1 &&
       loopback_wm_tbl[index].loop_back_eps_id == eps_bearer_id)
    {
      loopback_wm_tbl[index].tx_wm_ptr = tx_wm_item;
    }
  }

  if( (cmd_ptr = ds_allocate_cmd_buf(sizeof(loopback_wm_reg_type)) ) == NULL ||
       cmd_ptr->cmd_payload_ptr == NULL)
  {
    ASSERT(0);
    return FALSE;
  }

  cmd_ptr->hdr.cmd_id = DS_CMD_3GPP_LB_UL_RAB_REG_CNF;
  DS_3GPP_MSG0_HIGH("Posting LB PDCP UL RAB REG CNF to DS task");	

  wm_reg_info_ptr = (loopback_wm_reg_type*)cmd_ptr->cmd_payload_ptr;
  wm_reg_info_ptr->eps_id = eps_bearer_id;
  wm_reg_info_ptr->inst_id = inst_id;
  ds_put_cmd( cmd_ptr );

  return TRUE;
}


/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_LTE_PDCPDL_RAB_REGISTER_REQ

DESCRIPTION
  This function is responsible for handling lte pdcp dl rab registration.
  This function stores the downlink wm pointer in the loopback_wm_table.
  This function is also reponsible for setting up the signal handler and
  register for nonempty callback function.
 
PARAMETERS
  eps_bearer_id      eps bearer id
  rx_wm_item         rx wm pointer
  inst_id

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_loopback_lte_pdcpdl_rab_register_req
(
   uint8 eps_bearer_id,
   dsm_watermark_type* rx_wm_item,
   uint8                  inst_id
)
{
  uint32 index =0;
  ds_cmd_type     *cmd_ptr;
  loopback_wm_reg_type   *wm_reg_info_ptr;


  DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                      "Handling Lte pdcp dl registration: %d",eps_bearer_id); 

  for(index=0;index < DS_3GPP_MAX_BEARER_CONTEXT ; index++ )
  {
    if(loopback_wm_tbl[index].is_active == 1 &&
       loopback_wm_tbl[index].loop_back_eps_id == eps_bearer_id)
    {
      loopback_wm_tbl[index].rx_wm_ptr = rx_wm_item;
      if(loopback_wm_tbl[index].tx_wm_ptr != NULL)
      {
        /*-------------------------------------------------------------------------  
          Enable it.
          -------------------------------------------------------------------------*/
        ps_enable_sig(ds_3gpp_loopback_context_info.lb_rx_sig);  
        loopback_wm_tbl[index].tx_wm_ptr->non_empty_func_ptr =
          (wm_cb_type)ds_3gpp_loopback_hdlr_tx_data;
        loopback_wm_tbl[index].tx_wm_ptr->non_empty_func_data = (void *)index; 
      }
    }
  }

  if( (cmd_ptr = ds_allocate_cmd_buf(sizeof(loopback_wm_reg_type)) ) == NULL ||
       cmd_ptr->cmd_payload_ptr == NULL)
  {
    ASSERT(0);
    return FALSE;
  }

  cmd_ptr->hdr.cmd_id = DS_CMD_3GPP_LB_DL_RAB_REG_CNF;
  DS_3GPP_MSG0_HIGH("Posting LB PDCP DL RAB REG CNF to DS task");	

  wm_reg_info_ptr = (loopback_wm_reg_type*)cmd_ptr->cmd_payload_ptr;
  wm_reg_info_ptr->eps_id = eps_bearer_id;
  wm_reg_info_ptr->inst_id = inst_id;
  ds_put_cmd( cmd_ptr );

  return TRUE;

}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_UL_RAB_REGISTER_CNF

DESCRIPTION
  This function is responsible for sending lte pdcp ul rab registration
  confirmation
 
PARAMETERS
  eps_id      eps bearer id
  inst_id     subscription id

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_loopback_ul_rab_register_cnf
(
   uint8 eps_id,
   uint8 inst_id
)
{
  lte_pdcpul_rab_register_cnf_msg_s rab_reg_cnf_msg;
  msgr_umid_type              msgrtype;
#ifdef FEATURE_DATA_LTE
  boolean ret_val = FALSE;
#endif /*FEATURE_DATA_LTE */
  const msgr_hdr_struct_type *dsmsg;

  rab_reg_cnf_msg.hdr.inst_id = inst_id;
  rab_reg_cnf_msg.eps_id = eps_id;
  rab_reg_cnf_msg.status = E_SUCCESS;
  msgrtype= LTE_PDCPUL_RAB_REGISTER_CNF;

  DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                      "Sending Lte pdcp ul registration confirmation: %d",eps_id); 
  dsmsg = (msgr_hdr_struct_type *)&rab_reg_cnf_msg;
#ifdef FEATURE_DATA_LTE
  ret_val = ds_eps_bearer_cntxt_pdcpul_rab_reg_cnf_msg_hdlr(msgrtype,dsmsg);
#endif /*FEATURE_DATA_LTE */
  return;

}
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_TX_DATA

DESCRIPTION
  This function is responsible for setting the ps signal.This is done as part of the
  uplink wm nonempty call back function.The callback data in this case is the index
  in the loopback wm info table.
 
PARAMETERS
  wm_p           wm pointer
  callback_data - index 

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_hdlr_tx_data
(
  dsm_watermark_type *wm_p,
  void               *call_back_data
)
{

  PS_SET_SIGNAL(ds_3gpp_loopback_context_info.lb_rx_sig);  
}
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_TX_DATA

DESCRIPTION
  This function is responsible for processing the signal set as part of uplink
  wm.As part of this function the packet is dequeued from the uplink wm and put
  on the downlink wm.Along with it the source address and destination address are
  swapped and other packet processing done as part.
 
PARAMETERS
  sig           - singal which was set
  user_data_p -   index 

DEPENDENCIES
  

RETURN VALUE
..TRUE/FALSE -if False;signal will be set again;  
 
 
SIDE EFFECTS
  None.

===========================================================================*/

unsigned char ds_3gpp_loopback_hdlr_process_data_pkt
( 
  ps_sig_enum_type sig, 
  void *user_data_p
)  
{
  uint32 index =0;
  dsm_item_type * dup_pkt_ptr;
  uint16 lpkt = 0;
  dsm_item_type  *item_ptr;
  int ndup = 0;
  static int num_pkt;
  boolean ret_val = TRUE;


  for(index=0;index < DS_3GPP_MAX_BEARER_CONTEXT ; index++ )
  {
    if(loopback_wm_tbl[index].is_active == 1 &&
       loopback_wm_tbl[index].tx_wm_ptr != NULL && 
       loopback_wm_tbl[index].rx_wm_ptr != NULL &&
       (!dsm_is_wm_empty(loopback_wm_tbl[index].tx_wm_ptr)))
    {
      item_ptr = (dsm_item_type *)dsm_dequeue(loopback_wm_tbl[index].tx_wm_ptr);
      if (item_ptr != NULL)
      {
        if (ds_3gpp_loopback_context_info.lb_ul_only) 
        {
          dsm_free_packet(&item_ptr);
          ret_val = FALSE;
          continue;
        }
        (void)dsumtsps_loopback_convert_packet(&item_ptr, NULL);
    
        /*-------------------------------------------------------------------------
        If DUP factor is non zero, then dup every n'th packet and enqueue it
        -------------------------------------------------------------------------*/

        if (ds_3gpp_loopback_context_info.lb_dl_dupf) 
        {
          if (++num_pkt == ds_3gpp_loopback_context_info.lb_dl_dupf) 
          {
            lpkt = (uint16)dsm_length_packet(item_ptr);
            ASSERT(lpkt == dsm_dup_packet(&dup_pkt_ptr, item_ptr, 0, lpkt));
            dsm_enqueue(loopback_wm_tbl[index].rx_wm_ptr,&dup_pkt_ptr);
            num_pkt = 0;
          }
        }  
    
        /*-------------------------------------------------------------------------
        If MUL factor is non zero, then make n copies and enqueue them
        -------------------------------------------------------------------------*/
    
        if (ds_3gpp_loopback_context_info.lb_dl_mulf) 
        {
          if (lpkt == 0) 
          {
            lpkt = (uint16)dsm_length_packet(item_ptr);
          }
    
          for (ndup = 0; ndup < ds_3gpp_loopback_context_info.lb_dl_mulf; ++ndup) 
          {
            ASSERT(lpkt == dsm_dup_packet(&dup_pkt_ptr, item_ptr, 0, lpkt));
            dsm_enqueue(loopback_wm_tbl[index].rx_wm_ptr,&dup_pkt_ptr);
          }
        }
        dsm_enqueue(loopback_wm_tbl[index].rx_wm_ptr,&item_ptr);
        ret_val = FALSE;
      }
    }
  }
  return ret_val;
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_DL_RAB_REGISTER_CNF

DESCRIPTION
  This function is responsible for sending lte pdcp dl rab registration
  confirmation
 
PARAMETERS
  eps_id      eps bearer id
  inst_id     subscription id

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_loopback_dl_rab_register_cnf
(
  uint8 eps_id,
  uint8 inst_id
)
{
  lte_pdcpdl_rab_register_cnf_msg_s rab_reg_cnf_msg;
  msgr_umid_type              msgrtype;
#ifdef FEATURE_DATA_LTE
  boolean ret_val = FALSE;
#endif
  const msgr_hdr_struct_type *dsmsg;

  rab_reg_cnf_msg.hdr.inst_id = inst_id;
  rab_reg_cnf_msg.eps_id = eps_id;
  rab_reg_cnf_msg.status = E_SUCCESS;
  msgrtype= LTE_PDCPDL_RAB_REGISTER_CNF;
    DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                      "Sending Lte pdcp dl registration confirmation: %d",eps_id); 
  dsmsg = (msgr_hdr_struct_type *)&rab_reg_cnf_msg;
#ifdef FEATURE_DATA_LTE
  ret_val = ds_eps_bearer_cntxt_pdcpdl_rab_reg_cnf_msg_hdlr(msgrtype,dsmsg);
#endif
  return;
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_PROCESS_CMDS

DESCRIPTION
  This function is responsible for processing the UL confirmation as well
  as DL confirmation.
 
PARAMETERS
  cmd_ptr

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_hdlr_process_cmds
(
  const ds_cmd_type      *cmd_ptr
)
{
  loopback_wm_reg_type *wm_reg_info_ptr = NULL;
  if(cmd_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("NULL ptr passed, return");
    return;
  }

  switch (cmd_ptr->hdr.cmd_id)
  {
    case DS_CMD_3GPP_LB_UL_RAB_REG_CNF:
    {
      if(cmd_ptr->cmd_payload_ptr == NULL)
      {
        DS_3GPP_MSG0_ERROR("NULL ptr passed, return");
        return;
      }
      wm_reg_info_ptr = (loopback_wm_reg_type*)(cmd_ptr->cmd_payload_ptr);
      ds_3gpp_loopback_ul_rab_register_cnf(wm_reg_info_ptr->eps_id,
                                           wm_reg_info_ptr->inst_id);
      break;
    }
    case DS_CMD_3GPP_LB_DL_RAB_REG_CNF:
    {
      if(cmd_ptr->cmd_payload_ptr == NULL)
      {
        DS_3GPP_MSG0_ERROR("NULL ptr passed, return");
        return;
	     }
      wm_reg_info_ptr = (loopback_wm_reg_type*)(cmd_ptr->cmd_payload_ptr);
      ds_3gpp_loopback_dl_rab_register_cnf(wm_reg_info_ptr->eps_id,
                                           wm_reg_info_ptr->inst_id);
      break;
    }
    default:
    {
      DS_3GPP_MSG1_ERROR("Unrecognized cmd: %d, ignoring",cmd_ptr->hdr.cmd_id);
      break;
    }
  }
  return;
}
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_PDN_DISCONNECT_REQ

DESCRIPTION
  This function is responsible for handling pdn disconnect request in case of
  LTE loopback mode.This function inturn will invoke CM_CALL_EVENT_END 
 
PARAMETERS
  eps_id  -eps bearer id
  pdn_disconnect_ptr - pdn disconnect ptr

DEPENDENCIES
  

RETURN VALUE
..TRUE/FALSE - indicating whether operation is succcesful 
 
SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_loopback_hdlr_pdn_disconnect_req
(
  uint8 eps_id,
  const cm_pdn_disconnect_s_type   *pdn_disconnect_ptr
)
{
  cm_mm_call_info_s_type *call_info_ptr;
  cm_lte_call_info_s_type * lte_call_info_p;
  uint8 index=0;
  call_info_ptr	= (cm_mm_call_info_s_type *)modem_mem_alloc(sizeof(cm_mm_call_info_s_type),MODEM_MEM_CLIENT_DATA_CRIT);

  if(call_info_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("Memory Allocation Failed.");
    return FALSE;
  }

  memset(call_info_ptr, 0, sizeof(cm_mm_call_info_s_type));
  call_info_ptr->call_type = CM_CALL_TYPE_PS_DATA;
  call_info_ptr->asubs_id = ds_3gpp_loopback_context_info.loopback_subs_id;
  call_info_ptr->sys_mode = ds_3gpp_loopback_context_info.loopback_sys_mode;


  for(index=0;index < DS_3GPP_MAX_BEARER_CONTEXT ; index++ )
  {
    if(loopback_wm_tbl[index].is_active == 1 &&
       loopback_wm_tbl[index].loop_back_eps_id == eps_id)
    {
      call_info_ptr->call_id = loopback_wm_tbl[index].loop_back_call_id;
      loopback_wm_tbl[index].tx_wm_ptr->non_empty_func_ptr = NULL;
      loopback_wm_tbl[index].tx_wm_ptr->non_empty_func_ptr = NULL;
      memset(&loopback_wm_tbl[index], 0, sizeof(loopback_wm_info_type));
    }
  }

  DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                      "Sending call end: %d",eps_id); 
  call_info_ptr->mode_info.info_type = CM_CALL_MODE_INFO_LTE;
  lte_call_info_p = &(call_info_ptr->mode_info.info.lte_call);
  lte_call_info_p->eps_bearer_id = eps_id;
  lte_call_info_p->sdf_id = pdn_disconnect_ptr->sdf_id;
  ds_3gpp_loopback_hdlr_send_cm_call_back_event(CM_CALL_EVENT_END,call_info_ptr);
  modem_mem_free(call_info_ptr, MODEM_MEM_CLIENT_DATA);

  return TRUE;


}


/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_SEND_FULL_SERVICE_IND

DESCRIPTION
  This function is responsible for sending full service indication This function
  is used to send  CM_SS_EVENT_SRV_CHANGED for the sys mode read as part of
  loopback efs configuration
 
PARAMETERS
  None

DEPENDENCIES
  None

RETURN VALUE
  None 
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_hdlr_send_fullservice_ind()
{
  cm_mm_msim_ss_info_s_type  *ss_msim_ifo_p = NULL;
  uint8 iter_index=0;
  ss_msim_ifo_p = (cm_mm_msim_ss_info_s_type *)modem_mem_alloc(sizeof(cm_mm_msim_ss_info_s_type),MODEM_MEM_CLIENT_DATA_CRIT);

  if(ss_msim_ifo_p == NULL)
  {
    DS_3GPP_MSG0_ERROR("Memory Allocation Failed.");
    return;
  }

  memset(ss_msim_ifo_p,0,sizeof(cm_mm_msim_ss_info_s_type));

  ss_msim_ifo_p->asubs_id = 0;
  ss_msim_ifo_p->number_of_stacks = 2;
  ss_msim_ifo_p->stack_info[0].is_operational = TRUE;
  ss_msim_ifo_p->stack_info[0].sys_mode = ds_3gpp_loopback_context_info.loopback_sys_mode;
  ss_msim_ifo_p->stack_info[0].srv_status = SYS_SRV_STATUS_SRV;
  ss_msim_ifo_p->stack_info[0].srv_domain = SYS_SRV_DOMAIN_CS_PS;
  ss_msim_ifo_p->stack_info[0].changed_fields |= CM_SS_EVT_SYS_MODE_MASK|CM_SS_EVT_SRV_STATUS_MASK|CM_SS_EVT_SRV_DOMAIN_MASK;

  DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK  SENDING FULL SERVICE INDICATION: %d",ds_3gpp_loopback_context_info.loopback_sys_mode); 
  while(iter_index != LB_CALL_SS_EVENT_MAX)
  {
    if((lb_ss_info_array[iter_index] != NULL) &&
       (lb_ss_info_array[iter_index]->cm_ss_event_type== CM_SS_EVENT_SRV_CHANGED))
    {
      lb_ss_info_array[iter_index]->ss_event_func(CM_SS_EVENT_SRV_CHANGED,
                                                  ss_msim_ifo_p);
    }
    iter_index++;
  }

  modem_mem_free(ss_msim_ifo_p, MODEM_MEM_CLIENT_DATA);
  return;
}
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_RES_ALLOC_REQ

DESCRIPTION
  This function is responsible for handling resource allocation request from the
  3GPP Modehandler in case of Loopback configuration. This inturn will send  activate
  bearer indication and intiate a network initiated bearer.
 
PARAMETERS
  bearer_alloc_params_ptr - bearer alloc paramaters pointer

DEPENDENCIES
  None

RETURN VALUE
  ..TRUE/FALSE - indicating whether operation is succcesful
 
SIDE EFFECTS
  None.

===========================================================================*/


boolean ds_3gpp_loopback_res_alloc_req
(
  cm_res_alloc_s_type *bearer_alloc_params_ptr
)
{
  cm_mm_call_info_s_type *call_info_ptr = NULL;
  cm_call_id_type call_id;
  cm_lte_call_info_s_type  *lte_call_info_ptr;
  uint8 index = 0;
  call_info_ptr	= (cm_mm_call_info_s_type *)modem_mem_alloc(sizeof(cm_mm_call_info_s_type),
                                                            MODEM_MEM_CLIENT_DATA_CRIT);

  if(call_info_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("Memory Allocation Failed.");
    return FALSE;
  }

  memset(call_info_ptr, 0, sizeof(cm_mm_call_info_s_type));
  call_id = call_info_ptr->call_id = ds_3gpp_loopback_hdlr_get_callid();
  call_info_ptr->call_type = CM_CALL_TYPE_PS_DATA;
  call_info_ptr->sys_mode = ds_3gpp_loopback_context_info.loopback_sys_mode;
  call_info_ptr->direction = CM_CALL_DIRECTION_MT;
  call_info_ptr->asubs_id = ds_3gpp_loopback_context_info.loopback_subs_id;
  call_info_ptr->mode_info.info_type = CM_CALL_MODE_INFO_LTE;
  lte_call_info_ptr = &(call_info_ptr->mode_info.info.lte_call);
  lte_call_info_ptr->eps_bearer_id = ds_3gpp_loopback_hdlr_get_epsid();;
  memscpy(&(lte_call_info_ptr->ul_dl_tft),sizeof(tft_type_T), 
          &(bearer_alloc_params_ptr->ul_dl_tft), sizeof(tft_type_T));
  memscpy(&(lte_call_info_ptr->sdf_qos),sizeof(sdf_qos_T), 
          &(bearer_alloc_params_ptr->sdf_qos), sizeof(sdf_qos_T));

  for (index = 0; index < lte_call_info_ptr->ul_dl_tft.num_filters; index ++)
 {
   lte_call_info_ptr->ul_dl_tft.filter_list[index].identifier  &= 0xF0;
   lte_call_info_ptr->ul_dl_tft.filter_list[index].identifier  += 
     (((byte) lte_call_info_ptr->ul_dl_tft.filter_list[index].identifier) & 0xF);
   // tft->filter_list[index].eval_precedence = (byte) nw_qos.filters[index].fi_preced;
 }

  lte_call_info_ptr->lbi = bearer_alloc_params_ptr->lbi;
  lte_call_info_ptr->gsm_umts_connection_id.valid = TRUE;
  lte_call_info_ptr->sdf_id = bearer_alloc_params_ptr->sdf_id;


  DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                      "Sending Activate bearer Indication dedicated bearer: %d",
                       lte_call_info_ptr->eps_bearer_id); 
  ds_3gpp_loopback_hdlr_send_cm_call_back_event(CM_CALL_EVENT_ACT_BEARER_IND,call_info_ptr);


  for(index=0;index < DS_3GPP_MAX_BEARER_CONTEXT ; index++ )
  {
    if(loopback_wm_tbl[index].is_active == 0) 
    {
      loopback_wm_tbl[index].is_active = 1;
      loopback_wm_tbl[index].loop_back_eps_id = lte_call_info_ptr->eps_bearer_id;
      loopback_wm_tbl[index].loop_back_call_id = call_id;
      loopback_wm_tbl[index].loop_back_rb_id = loopback_wm_tbl[index].loop_back_eps_id;
      break;
    }
  }
  modem_mem_free(call_info_ptr, MODEM_MEM_CLIENT_DATA);
  return TRUE;

}


/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_CALL_CMD_ORIG_CC_PER_SUBS

DESCRIPTION
  This function is responsible for handling call cmd origination for all non
  lte mode of operation in case of Loopback config
 
PARAMETERS
 gw_ps_orig_params_ptr    - gw ps origination paramaters 
 asubs_id,                - subs_id
	*return_call_id_ptr      - return call_id_ptr

DEPENDENCIES
  None

RETURN VALUE
  ..TRUE/FALSE - indicating whether operation is succcesful
 
SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_loopback_hdlr_call_cmd_orig_cc_per_subs
(
  cm_gw_ps_orig_params_s_type *gw_ps_orig_params_ptr,
	sys_modem_as_id_e_type      asubs_id,
	cm_call_id_type             *return_call_id_ptr
)
{
  cm_mm_call_info_s_type *call_info_ptr = NULL;
  cm_gw_ps_call_info_s_type     *gw_ps_call_ptr = NULL;
  uint8 index =0;
  comp_cmd_type* cmd_ptr = NULL;
  boolean v4_call = FALSE;
  boolean v6_call = FALSE;

  if(gw_ps_orig_params_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("Could not get gw ps origination paramaters.");
    return FALSE;
  }

  call_info_ptr	= (cm_mm_call_info_s_type *)modem_mem_alloc(sizeof(cm_mm_call_info_s_type),MODEM_MEM_CLIENT_DATA_CRIT);

  if(call_info_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("Memory Allocation Failed.");
    return FALSE;
  }

  memset(call_info_ptr, 0, sizeof(cm_mm_call_info_s_type));
  *return_call_id_ptr = ds_3gpp_loopback_hdlr_get_callid();
  call_info_ptr->asubs_id = ds_3gpp_loopback_context_info.loopback_subs_id;
  call_info_ptr->call_type = CM_CALL_TYPE_PS_DATA;
  call_info_ptr->sys_mode = ds_3gpp_loopback_context_info.loopback_sys_mode;

  call_info_ptr->mode_info.info_type = CM_CALL_MODE_INFO_GW_PS;
  gw_ps_call_ptr = &(call_info_ptr->mode_info.info.gw_ps_call);
  gw_ps_call_ptr->nsapi.nsapi = ds_3gpp_loopback_hdlr_get_epsid();
  gw_ps_call_ptr->nsapi.valid = TRUE;
  memscpy(gw_ps_call_ptr->apn_name.address, 
  gw_ps_orig_params_ptr->apn_name.apn_addr_len,
  gw_ps_orig_params_ptr->apn_name.address,
  gw_ps_orig_params_ptr->apn_name.apn_addr_len);
  gw_ps_call_ptr->apn_name.apn_addr_len = gw_ps_orig_params_ptr->apn_name.apn_addr_len;
  gw_ps_call_ptr->apn_name.valid = TRUE;

  if(gw_ps_orig_params_ptr->pdp_addr.pdp_type_num == 0x21 )
  {
     ds_3gpp_loopback_setV4PdpAddress(&(gw_ps_call_ptr->pdp_addr));
     v4_call = TRUE;
  }
  if(gw_ps_orig_params_ptr->pdp_addr.pdp_type_num == 0x57 )
  {
    ds_3gpp_loopback_setV6PdpAddress(&(gw_ps_call_ptr->pdp_addr));
    v6_call = TRUE;
  }
  if(gw_ps_orig_params_ptr->pdp_addr.pdp_type_num == 0x8D )
  {
    ds_3gpp_loopback_setV4V6PdpAddress(&(gw_ps_call_ptr->pdp_addr));
    v4_call = TRUE;
    v6_call = TRUE;
  }
  gw_ps_call_ptr->qos.valid = TRUE;
  call_info_ptr->call_id = *return_call_id_ptr;
  call_info_ptr->end_status = CM_CALL_END_NONE;

  if(ds_3gpp_loopback_context_info.loopback_sys_mode == SYS_SYS_MODE_WCDMA ||
     ds_3gpp_loopback_context_info.loopback_sys_mode == SYS_SYS_MODE_TDS)
  {
    cmd_ptr = comp_get_cmd_buf();
    if(cmd_ptr == NULL)
    {
      DATA_MSG0_HIGH("Could not get a buffer for PDCP command");
      ASSERT(0);
    }
    memset(cmd_ptr,0, sizeof(comp_cmd_type));
    cmd_ptr->hdr.subs_id = asubs_id;
    cmd_ptr->hdr.cmd_id = CPDCP_CONFIG_REQ;
    cmd_ptr->cmd.pdcp_cfg_req.action = SETUP_PDCP;
    /* 
      Set the PDCP INFO VALID flag to FALSE so that PDCP is in
      transparent mode
    */
    cmd_ptr->cmd.pdcp_cfg_req.pdcp_info.pdcp_info_valid = FALSE;
    cmd_ptr->cmd.pdcp_cfg_req.rb_id = gw_ps_call_ptr->nsapi.nsapi;
    /* 
      Fill the LC info. Only one LC is supported per RAB
      */
    cmd_ptr->cmd.pdcp_cfg_req.rlc_dl_id = 
      gw_ps_call_ptr->nsapi.nsapi;
    cmd_ptr->cmd.pdcp_cfg_req.rlc_ul_id = 
      gw_ps_call_ptr->nsapi.nsapi;
    /*Explicitly fill that this is not a multicast call*/
    cmd_ptr->cmd.pdcp_cfg_req.is_mcast_call = FALSE;
    /* Send command to COMP Task */
    comp_put_cmd(cmd_ptr);
  }

  DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                      "Sending Call connected umts mode: %d",gw_ps_call_ptr->nsapi.nsapi); 
  ds_3gpp_loopback_hdlr_send_cm_call_back_event(CM_CALL_EVENT_CONNECT,call_info_ptr);

  for(index=0;index < DS_3GPP_MAX_BEARER_CONTEXT ; index++ )
  {
    if(loopback_wm_tbl[index].is_active == 0)
    {
      loopback_wm_tbl[index].is_active = 1;
      loopback_wm_tbl[index].loop_back_eps_id = gw_ps_call_ptr->nsapi.nsapi;
      loopback_wm_tbl[index].loop_back_call_id = *return_call_id_ptr;
      loopback_wm_tbl[index].loop_back_rb_id = loopback_wm_tbl[index].loop_back_eps_id;
      loopback_wm_tbl[index].v4_call = v4_call;
      loopback_wm_tbl[index].v6_call = v6_call;
      break;
    }
  }
	modem_mem_free(call_info_ptr, MODEM_MEM_CLIENT_DATA);
	return TRUE;

}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_RRC_RETURN_LC_INFO_FOR_RAB

DESCRIPTION
  This function is responsible for filling the lc information pointer based
  on the rab id passed by the client.This function is invoked as part of WCDMA/TDS
  mode of operation
 
PARAMETERS
  lc_info_ptr  - Logical channel information pointer
 
DEPENDENCIES
  None

RETURN VALUE
  ..RRC_RAB_FOUND/RRC_RAB_NOT_FOUND - indicating whether operation is succcesful
 
SIDE EFFECTS
  None.

===========================================================================*/


rrc_rab_search_e_type ds_3gpp_loopback_hdlr_rrc_return_lc_info_for_rab
(
  rrc_user_plane_lc_info_type *lc_info_ptr
)
{
  if (lc_info_ptr == NULL)
  {
    return  RRC_RAB_NOT_FOUND;
  }
  /* Fill Ul lc id and dl lc id to the rab id and set no of lc ids to 1*/
  lc_info_ptr->num_dl_lc_ids_for_rab = 1;
  lc_info_ptr->num_ul_lc_ids_for_rab = 1;
  lc_info_ptr->ul_lc_id[0] = lc_info_ptr->rab_id;
  lc_info_ptr->dl_lc_id[0] = lc_info_ptr->rab_id;
  return RRC_RAB_FOUND;
}
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_L2_REG_UL_WM

DESCRIPTION
  This function is responsible for registering the uplink wm with the coresponding
  rb id passed .This function is invoked as part of all non-LTE mode of operation.

 
PARAMETERS
  rlc_ul_id - rlc id/nsapi in case of Wcdma/TDS/gsm
  tx_wm_ptr - tx wm pointer
 
DEPENDENCIES
  None

RETURN VALUE
  
 
SIDE EFFECTS
  None.

===========================================================================*/


void ds_3gpp_loopback_l2_reg_ul_wm
(
   uint8 rlc_ul_id,
   dsm_watermark_type* tx_wm_ptr
)
{
  uint8 index =0;
  for(index=0;index < DS_3GPP_MAX_BEARER_CONTEXT ; index++ )
  {
    if(loopback_wm_tbl[index].is_active == 1 &&
       loopback_wm_tbl[index].loop_back_eps_id == rlc_ul_id)
    {
      loopback_wm_tbl[index].tx_wm_ptr = tx_wm_ptr;
    }
  }
  return;
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_L2_REG_DL_WM

DESCRIPTION
  This function is responsible for registering the downlink wm with the coresponding
  rb id passed .This function is invoked as part of all non-LTE mode of operation.

 
PARAMETERS
  rlc_ul_id - rlc id/nsapi in case of Wcdma/TDS/gsm
  rx_wm_ptr - tx wm pointer
 
DEPENDENCIES
  None

RETURN VALUE
  
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_loopback_l2_reg_dl_wm
(
  uint8 rlc_dl_id,
  dsm_watermark_type* rx_wm_ptr
)
{
  uint32 index =0;

  for(index=0;index < DS_3GPP_MAX_BEARER_CONTEXT ; index++ )
  {
    if(loopback_wm_tbl[index].is_active == 1 &&
       loopback_wm_tbl[index].loop_back_eps_id == rlc_dl_id)
    {
      loopback_wm_tbl[index].rx_wm_ptr = rx_wm_ptr;
      if(loopback_wm_tbl[index].tx_wm_ptr != NULL)
      {
        /*-------------------------------------------------------------------------  
          Enable it.
          -------------------------------------------------------------------------*/
        ps_enable_sig(ds_3gpp_loopback_context_info.lb_rx_sig);  
        loopback_wm_tbl[index].tx_wm_ptr->non_empty_func_ptr =
          (wm_cb_type)ds_3gpp_loopback_hdlr_tx_data;
        loopback_wm_tbl[index].tx_wm_ptr->non_empty_func_data = (void *)index; 

      }
    }
  }
  return;
}
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_CALL_CMD_END

DESCRIPTION
  This function is responsible for handling call cmd end in case of Umts mode
  operation when Loopback is configured. 

 
PARAMETERS
  call_end_params - call end paramters pointer
 
DEPENDENCIES
  None

RETURN VALUE
  
 
SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_loopback_hdlr_call_cmd_end
(
  cm_end_params_s_type  *call_end_params
)
{
  cm_mm_call_info_s_type *call_info_ptr;
  cm_gw_ps_call_info_s_type* gw_ps_call_info_ptr;
  uint8 index=0;
  call_info_ptr	= (cm_mm_call_info_s_type *)modem_mem_alloc(sizeof(cm_mm_call_info_s_type),MODEM_MEM_CLIENT_DATA_CRIT);

  if(call_info_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("Memory Allocation Failed.");
    return FALSE;
  }

  memset(call_info_ptr, 0, sizeof(cm_mm_call_info_s_type));
  call_info_ptr->call_type = CM_CALL_TYPE_PS_DATA;
  call_info_ptr->asubs_id = ds_3gpp_loopback_context_info.loopback_subs_id;
  call_info_ptr->sys_mode = ds_3gpp_loopback_context_info.loopback_sys_mode;
  call_info_ptr->mode_info.info_type = CM_CALL_MODE_INFO_GW_PS;
  gw_ps_call_info_ptr = &(call_info_ptr->mode_info.info.gw_ps_call);

  
  for(index=0;index < DS_3GPP_MAX_BEARER_CONTEXT ; index++ ) 
  {
    if(loopback_wm_tbl[index].is_active == 1 && 
       loopback_wm_tbl[index].loop_back_call_id == call_end_params->call_id)
    {
      call_info_ptr->call_id = loopback_wm_tbl[index].loop_back_call_id;
      gw_ps_call_info_ptr->nsapi.nsapi = loopback_wm_tbl[index].loop_back_eps_id;
      gw_ps_call_info_ptr->nsapi.valid = TRUE;
      loopback_wm_tbl[index].tx_wm_ptr->non_empty_func_ptr = NULL;
      loopback_wm_tbl[index].tx_wm_ptr->non_empty_func_ptr = NULL;
      memset(&loopback_wm_tbl[index], 0, sizeof(loopback_wm_info_type));
    }
  }
  DS_3GPP_MSG1_HIGH ("DS 3GPP LOOPBACK "
                      "Sending Call end umts mode: %d",call_end_params->call_id); 
  ds_3gpp_loopback_hdlr_send_cm_call_back_event(CM_CALL_EVENT_END,call_info_ptr);
  modem_mem_free(call_info_ptr, MODEM_MEM_CLIENT_DATA);
  return TRUE;

}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_SNDCP_PDP_REQ

DESCRIPTION
  This function is responsible for sending sndcp pdcp reg confirmation
  in case of gsm mode of operation. This callback invokation is needed to
  configure the wm and registration

 
PARAMETERS
  nsapi,
  pdp_ul_suspend_fnc_ptr,
  pdp_ul_resume_fnc_ptr,
 ( *pdp_dl_fnc_ptr )( void* ds_context, dsm_item_type **npdu ),
 *ds_context,
 cipher,
( *pdp_reg_cnf_fnc_ptr )( void* ds_context, boolean success )
 
DEPENDENCIES
  None

RETURN VALUE
  
 
SIDE EFFECTS
  None.

===========================================================================*/

void  ds_3gpp_loopback_hdlr_sndcp_pdp_reg
(
  uint8        nsapi,
  wm_cb_type   pdp_ul_suspend_fnc_ptr,
  wm_cb_type   pdp_ul_resume_fnc_ptr,
  void         ( *pdp_dl_fnc_ptr )( void* ds_context, dsm_item_type **npdu ),
  void         *ds_context,
  boolean      cipher,
  void         ( *pdp_reg_cnf_fnc_ptr )( void* ds_context, boolean success )
)
{
  if(ds_context!= NULL && pdp_reg_cnf_fnc_ptr != NULL)
  {
      pdp_reg_cnf_fnc_ptr(ds_context, TRUE);
  }
  return;
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_PDP_DATA_CTRL_SETUP_CB

DESCRIPTION
  This function sets up the path to enqueue data between two bearers
 
PARAMETERS
  *phys_link_ptr - ptr to the phys link
  **item_ptr - ptr to items to be enqueued
  *meta_info_ptr - ptr to ps meta info
  *tx_info_ptr - ptr to bearer context
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_loopback_pdp_data_ctrl_setup_cb
(
  ps_phys_link_type *phys_link_ptr,
  dsm_item_type     **item_ptr,
  ps_meta_info_type *meta_info_ptr,
  void              *tx_info_ptr
)
{
  ds_bearer_context_s    *bearer_context_p = NULL;
  uint32                  index =0;
  dsm_watermark_type     *other_rx_watermark_ptr = NULL;
  boolean                 wait_for_v6_ra = FALSE;
  boolean                 other_bearer_found = FALSE;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  bearer_context_p = (ds_bearer_context_s*)tx_info_ptr;

  if (!ds_bearer_cntx_validate_bearer_context(bearer_context_p))
  {
    return;
  }

  for (index = 0; index < DS_3GPP_MAX_BEARER_CONTEXT; index++)
  {
    if ((loopback_wm_tbl[index].is_active == 1) &&
        (PS_PHYS_LINK_GET_BEARER_ID(&(bearer_context_p->
                                   ds_bearer_context_dyn_p->phys_link)) 
         == loopback_wm_tbl[index].loop_back_eps_id) && 
        (loopback_wm_tbl[index].v6_call == TRUE) &&
        (loopback_wm_tbl[index].ra_received == FALSE)
       )
    {
      wait_for_v6_ra = TRUE;
      break;
    }
  }

  /* Find the first different bearer's rx wm */
  for (index = 0; index < DS_3GPP_MAX_BEARER_CONTEXT; index++)
  {
    if ((loopback_wm_tbl[index].is_active == 1) && 
        (loopback_wm_tbl[index].tx_wm_ptr != NULL) &&
        (loopback_wm_tbl[index].rx_wm_ptr != NULL) &&
        (PS_PHYS_LINK_GET_BEARER_ID(&(bearer_context_p->
                                    ds_bearer_context_dyn_p->phys_link)) 
           != loopback_wm_tbl[index].loop_back_eps_id)
       )
    {
      other_rx_watermark_ptr = loopback_wm_tbl[index].rx_wm_ptr;
      other_bearer_found = TRUE;
      break;
    }
  }

  /* Enqueue into other bearer if found, else, use regular path */
  if (other_bearer_found && !wait_for_v6_ra)
  {
    DS_3GPP_MSG0_HIGH("Found other bearer. registering data cb");
    ps_phys_link_set_tx_function(&(bearer_context_p->ds_bearer_context_dyn_p->phys_link),
                                 ds_3gpp_loopback_pdp_ip_tx_um_data_cb,
                                 other_rx_watermark_ptr);
  }
  else
  {
    DS_3GPP_MSG0_HIGH("Only one bearer up. Enqueueing into regular path");
    dsm_enqueue(&(bearer_context_p->tx_wm.wm_item), item_ptr);
  }

  PS_META_INFO_FREE(&meta_info_ptr);

  return;
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_PDP_IP_TX_UM_DATA_CB

DESCRIPTION
  This function directly enqueues the data into the other bearer's rx wm
 
PARAMETERS
  *phys_link_ptr - ptr to the phys link
  **item_ptr - ptr to items to be enqueued
  *meta_info_ptr - ptr to ps meta info
  *tx_info_ptr - ptr to bearer context
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_loopback_pdp_ip_tx_um_data_cb
(
  ps_phys_link_type *phys_link_ptr,
  dsm_item_type     **item_ptr,
  ps_meta_info_type *meta_info_ptr,
  void              *tx_info_ptr
)
{
  dsm_watermark_type     *other_rx_watermark_ptr = NULL;
  uint8                   index = 0;
  ds_bearer_context_s    *bearer_context_p = NULL;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  other_rx_watermark_ptr = (dsm_watermark_type*)tx_info_ptr;

  if (other_rx_watermark_ptr != NULL)
  {
    dsm_enqueue(other_rx_watermark_ptr, item_ptr);
  }
  else
  {
    for (index = 0; index < DS_3GPP_MAX_BEARER_CONTEXT; index++)
    {
      bearer_context_p = ds_bearer_cntxt_get_instance_ptr(index);
      if (!ds_bearer_cntx_validate_bearer_context(bearer_context_p))
      {
        continue;
      }

      if (PS_PHYS_LINK_GET_BEARER_ID(&(bearer_context_p->ds_bearer_context_dyn_p->phys_link)) 
          == PS_PHYS_LINK_GET_BEARER_ID(phys_link_ptr))
      {
        dsm_enqueue(&(bearer_context_p->tx_wm.wm_item), item_ptr);
      }
    }
  }

  PS_META_INFO_FREE(&meta_info_ptr);

  return;
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_GET_IPV4_ADDR

DESCRIPTION
  This function returns an IPv4 addr for the given call id
 
PARAMETERS
  call_id - call_id to be assigned an addr
 
DEPENDENCIES
  None.

RETURN VALUE
  A IPv4 addr
 
SIDE EFFECTS
  None.

===========================================================================*/
uint32 ds_3gpp_loopback_get_ipv4_addr
(
  uint8 call_id
)
{
  uint32 ipv4_addr = 0xAB30A8C0;
  uint32 ipv4_addr_temp = 0xAB30A8C0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ipv4_addr_temp = ((ipv4_addr & 0xFF000000) >> 24);

  /* for ipv4, need to always increment by 2 for different ips */
  ipv4_addr_temp = (ipv4_addr_temp + (2 * (call_id-1)));
  ipv4_addr_temp = (ipv4_addr_temp << 24);
  
  ipv4_addr = (ipv4_addr & 0x00FFFFFF) | ipv4_addr_temp;

  DS_3GPP_MSG1_HIGH("Setting IP address to 0x%x", ipv4_addr);
  return ipv4_addr;
}

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_GET_IPV4_ADDR

DESCRIPTION
  This function sets the ra_received flag for a bearer to indicate that ipv6
  is complete
 
PARAMETERS
  bearer_id - bearer to set the flag
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_loopback_set_bearer_ra_received
(
  uint8 bearer_id
)
{
  uint32  index =0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  for (index = 0; index < DS_3GPP_MAX_BEARER_CONTEXT; index++)
  {
    if (bearer_id == loopback_wm_tbl[index].loop_back_eps_id)
    {
      loopback_wm_tbl[index].ra_received = TRUE;
      break;
    }
  }

  DS_3GPP_MSG1_HIGH("Received RA on bearer id %d", bearer_id);

  return;
}
