/*===========================================================================

                             D S M S G R R E C V . C
 
GENERAL DESCRIPTION
  Implementation of DS MSGR RECVer-side helper apis.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2008-2015 by Qualcomm Technologies, Incorporated.  
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/3gpp/msgr/src/dsmsgrrecv.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/19/14   vrk     Added support for POLICYMAN_CFG_UPDATE_MSIM_IND
09/25/12   ss      3GPP MH LTE-Off compilation fixes.
10/03/11   ua      Added support for LTE_CPHY_RFCHAIN_CNF to support DRX/PRX
                   in ATCoP. Currently, its only registration. 
11/18/10   sa      Added support of testing LTE_TLB_DS_CTL_READY_IND.
09/27/10   ss      DYNAMIC WDOG support for DATA tasks.
08/10/10   sa      Added support of LTE_TLB_LB_OPEN_IND.
04/23/10   vk      Use of MSGR for UL flow control event processing in LTE
08/09/09   vk      Moving DS to its own MSGR technology space
05/01/09   vk      Added support for test synchronization with DS task
12/22/08   vk      Initial version.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#include "err.h"


#ifdef TEST_FRAMEWORK
#error code not present
#endif


#include "amssassert.h"
#include "dsmsgr.h"
#include "dsmsgrsend.h"
#include "queue.h"
#include "pthread.h"
#include "msgr.h"
#include "appmgr.h"
#include "modem_mem.h"
#include "dsmsgrrecv.h"

/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

===========================================================================*/

#define ARRSIZ(a)	      (sizeof(a)/sizeof(a[0]))
#define DSMSGR_TASK_NAME_STR  "DSMSGR_RECV"

/*--------------------------------------------------------------------
  Data type for super command that contains received message info
----------------------------------------------------------------------*/
typedef union {
  msgr_hdr_s        hdr;
  struct 
  {
    dsmsgrrcv_msg_u msg;
  }ds_cmd;
} dsmsgrrcv_super_cmd_u;

/*-------------------------------------------------------
  Queue data block, containing registered clients info
--------------------------------------------------------*/
typedef struct 
{
  q_link_type                     link;
  msgr_umid_type                  msgrtype;
  dsmsgrrecv_cback_f_ptr   func_ptr;
  void                           *user_data;
} dsmsgrrcv_client_data_block;

/*-------------------------------------------------------
  Queue of registered clients
--------------------------------------------------------*/
static q_type dsmsgrrcv_reg_client_q = {{NULL}};

static pthread_mutex_t dsmsgrrcv_mutex;

static pthread_t       dsmsgrrcv_thread;

static msgr_client_t   dsmsgrrcv_msgr_client;
static msgr_id_t       dsmsgrrcv_msgr_q_id;

#define     DSMSGRRCV_MSGR_PRI          1
#define     DSMSGRRCV_MSGR_MAX_MSGS     16
#define     DSMSGRRCV_MSGR_MAX_MSG_SZ   sizeof(dsmsgrrcv_msg_u)
#define     DSMSGR_TASK_NAME_STR        "DSMSGR_RECV"

static dsmsgrrcv_super_cmd_u dsmsgrrcv_super_cmd;

/*===========================================================================

                      INTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
FUNCTION       DSMSGRRCV_GET_CLIENT_DATA_BLOCK

DESCRIPTION    Gets the block corresponding to a particular MSG from Registered 
               Clients queue.
 
PARAMETERS     msgrtype: UMID whose corresponding block is to be found
 
DEPENDENCIES   NONE

RETURN VALUE   Pointer to the message block

SIDE EFFECTS   NONE
===========================================================================*/
dsmsgrrcv_client_data_block* dsmsgrrcv_get_client_data_block
(
  msgr_umid_type              msgrtype
)
{
  dsmsgrrcv_client_data_block    *msgr_client_data_block_p = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  msgr_client_data_block_p = (dsmsgrrcv_client_data_block *)
                                            q_check(&dsmsgrrcv_reg_client_q);

  while(NULL != msgr_client_data_block_p)
  {
    if(msgr_client_data_block_p->msgrtype == msgrtype)
    {
      break;
    }

    msgr_client_data_block_p = (dsmsgrrcv_client_data_block *)q_next(
                                    &dsmsgrrcv_reg_client_q,
                                    &(msgr_client_data_block_p->link)
                                    );
  }

  return msgr_client_data_block_p;
}/*dsmsgrrcv_get_client_data_block*/
/*===========================================================================

                      EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
FUNCTION       DSMSGRRCV_REG_HDLR

DESCRIPTION    API invoked by DS clients to register a callback for a UMID 
 
PARAMETERS     MSGRTYPE: UMID for which cback is to be registered 
               common_cback_func_ptr: Cback to be invoked once message
                                      is sent for that UMID
               client_cback_func_ptr: void ptr containing client specific
                                      cback function (currently DS3G use it)
 
DEPENDENCIES   NONE

RETURN VALUE   NONE 

SIDE EFFECTS   NONE
===========================================================================*/
void dsmsgrrecv_reg_cb 
(
  msgr_umid_type                 msgrtype, 
  dsmsgrrecv_cback_f_ptr         cback_func_ptr,
  void                          *data_ptr
)
{
  errno_enum_type                errnum = E_FAILURE;
  dsmsgrrcv_client_data_block   *client_data_block_p = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if(NULL == cback_func_ptr)
  {
    DATA_MSG1_ERROR("Cannot register cback for MSG: %d; invalid cback "
                    "function ptr!!",msgrtype);
    return;
  }

  client_data_block_p = dsmsgrrcv_get_client_data_block(msgrtype);

  if(NULL != client_data_block_p)
  {
    client_data_block_p->func_ptr = cback_func_ptr;
    client_data_block_p->user_data = data_ptr;
  }
  else
  {
    client_data_block_p = (dsmsgrrcv_client_data_block *)modem_mem_alloc(
       sizeof(dsmsgrrcv_client_data_block), MODEM_MEM_CLIENT_DATA);

    if(NULL != client_data_block_p)
    {
      memset(client_data_block_p, 0, sizeof(dsmsgrrcv_client_data_block));

      client_data_block_p->func_ptr = cback_func_ptr;
      client_data_block_p->user_data = data_ptr;
      client_data_block_p->msgrtype = msgrtype;

      q_link(client_data_block_p, &(client_data_block_p->link));
      q_put(&dsmsgrrcv_reg_client_q, &(client_data_block_p->link));

      errnum = msgr_register
         (
           MSGR_DS_MSGRRECV, 
           &dsmsgrrcv_msgr_client,
           dsmsgrrcv_msgr_q_id,
           client_data_block_p->msgrtype
         );

      ASSERT(errnum == E_SUCCESS);
    }
  }
  return;
}/*dsmsgrrecv_reg_cb*/

/*===========================================================================
FUNCTION       DSMSGRRECV_TASK_TERMINATE

DESCRIPTION    This function is registered with RCINIT to receive 
               termination signal.
 
PARAMETERS     NONE
 
DEPENDENCIES   NONE

RETURN VALUE   NONE

SIDE EFFECTS   NONE
===========================================================================*/
void dsmsgrrecv_task_terminate( void )
{
  boolean               result;
  msgr_hdr_struct_type  msg;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /* Temporary fix: commenting out sending the message to kill the dsmsgrrecv
     task
     Note: This fix may cause a crash if clients pump in 16 messages between
     SSR and Q6 shutdown
     Long term fix needs to come from LTE team.
     */
  #if 0
  result = dsmsgrsnd_msg_send(DS_MSGRRECV_SPR_THREAD_KILL,
                              (msgr_hdr_struct_type*)&msg,
                              sizeof(msgr_hdr_struct_type));
  ASSERT(FALSE != result);
  #endif

  return;
} /* dsmsgrrecv_task_terminate */

/*===========================================================================
FUNCTION       DSMSGRRCV_DEREG_HDLR

DESCRIPTION    API invoked by DS clients to deregister a callback for a UMID 
 
PARAMETERS     MSGRTYPE: UMID for which cback is to be deregistered 
 
DEPENDENCIES   NONE

RETURN VALUE   NONE 

SIDE EFFECTS   NONE
===========================================================================*/
void dsmsgrrecv_dereg_cb 
(
   msgr_umid_type msgrtype
)
{
  errno_enum_type                errnum = E_FAILURE;
  dsmsgrrcv_client_data_block   *client_data_block_p = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  client_data_block_p = dsmsgrrcv_get_client_data_block(msgrtype);

  if(NULL != client_data_block_p)
  {
    errnum = msgr_deregister(MSGR_DS_MSGRRECV,
                             &dsmsgrrcv_msgr_client,
                             client_data_block_p->msgrtype);

    ASSERT(errnum == E_SUCCESS);
    q_delete(&dsmsgrrcv_reg_client_q, &(client_data_block_p->link));
    modem_mem_free(client_data_block_p, MODEM_MEM_CLIENT_DATA);
  }
  else
  {
    DATA_MSG1_HIGH("No Entry present for dereg for MSG: %d", msgrtype);
  }

  return;
}/*dsmsgrrecv_dereg_cb*/

/*===========================================================================
FUNCTION       DSMSGRRCV_INIT

DESCRIPTION    API invoked by pthread function during MSGR task initialization 
 
PARAMETERS     None
 
DEPENDENCIES   NONE

RETURN VALUE   NONE 

SIDE EFFECTS   NONE
===========================================================================*/
void dsmsgrrcv_init 
(
  void
)
{
  (void)q_init( &dsmsgrrcv_reg_client_q );
  (void)pthread_mutex_init(&dsmsgrrcv_mutex, NULL);

  return;
}/*dsmsgrrecv_init*/

/*===========================================================================
FUNCTION       DSMSGRRCV_REGISTER

DESCRIPTION    API invoked by pthread function during MSGR task initialization 
               This function creates the DS MSGR RECV client with MSGR, adds
               a queue for the client and registers few messages
 
PARAMETERS     None
 
DEPENDENCIES   NONE

RETURN VALUE   NONE 

SIDE EFFECTS   NONE
===========================================================================*/
void dsmsgrrcv_register 
(
  void
)
{
  errno_enum_type errnum;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  errnum = msgr_client_create(&dsmsgrrcv_msgr_client);
  ASSERT(errnum == E_SUCCESS);

  errnum = msgr_client_add_mq
           (
             "DS MSGR RECV",
             &dsmsgrrcv_msgr_client,
             DSMSGRRCV_MSGR_PRI,
             DSMSGRRCV_MSGR_MAX_MSGS,
             DSMSGRRCV_MSGR_MAX_MSG_SZ,
             &dsmsgrrcv_msgr_q_id
           );
  ASSERT(errnum == E_SUCCESS);

  /* 
  ** Now register for all messages
  */

  errnum = msgr_register
           (
             MSGR_DS_MSGRRECV,
             &dsmsgrrcv_msgr_client,
             dsmsgrrcv_msgr_q_id,
             DS_MSGRRECV_SPR_LOOPBACK
           );
  ASSERT(errnum == E_SUCCESS);

  errnum = msgr_register
           (
             MSGR_DS_MSGRRECV,
             &dsmsgrrcv_msgr_client,
             dsmsgrrcv_msgr_q_id,
             DS_MSGRRECV_SPR_THREAD_KILL
           );
  ASSERT(errnum == E_SUCCESS);

  return;
}/*dsmsgrrcv_register*/

/*===========================================================================
FUNCTION       DSMSGRRCV_TASK_EXIT

DESCRIPTION    API invoked when MSGR task is going down 
 
PARAMETERS     NONE
 
DEPENDENCIES   NONE

RETURN VALUE   NONE 

SIDE EFFECTS   NONE
===========================================================================*/
void dsmsgrrcv_task_exit (void)
{
  int rval = -1;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  // Release the mailbox
  (void) msgr_client_delete(&dsmsgrrcv_msgr_client);

  // release mutex
  rval = pthread_mutex_destroy(&dsmsgrrcv_mutex);

  if( rval != 0 )
  {
    ERR_FATAL("Failed dsmsgrrcv_mutex destroy",0,0,0);
  }

  rcinit_unregister_termfn_group(RCINIT_GROUP_3, dsmsgrrecv_task_terminate);
  rcinit_handshake_term();
  return;
}/*dsmsgrrcv_task_exit*/

/*===========================================================================
FUNCTION       DSMSGRRCV_THRD_MAIN

DESCRIPTION    This function is invoked when APP MGR spawns the thread during 
               MSGR initialization.
               It waits in an infinite loop to receive a message, once it ]
               receives it posts a cmd to DS task to invoke the cback
 
PARAMETERS     None
 
DEPENDENCIES   NONE

RETURN VALUE   NONE 

SIDE EFFECTS   NONE
===========================================================================*/
void * dsmsgrrcv_thrd_main 
(
   void * arg
)
{
  uint32                                 msglen = 0;
  errno_enum_type                        errnum = E_FAILURE;
  dsmsgrrcv_client_data_block           *msgr_client_data_block_p = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  
  rcinit_register_termfn_group(RCINIT_GROUP_3, dsmsgrrecv_task_terminate);
  rcinit_handshake_startup();
  
  dsmsgrrcv_init();
  dsmsgrrcv_register();

  //appmgr_thread_init_complete(MSGR_DS_MSGRRECV);

  for (;;)
  {
    errnum = msgr_receive
             (
               &dsmsgrrcv_msgr_client,
               (uint8 *)&dsmsgrrcv_super_cmd,
               DSMSGRRCV_MSGR_MAX_MSG_SZ,
               &msglen
             );
    ASSERT(errnum == E_SUCCESS);
    ASSERT(msglen <= DSMSGRRCV_MSGR_MAX_MSG_SZ);

    switch (dsmsgrrcv_super_cmd.hdr.id) 
    {
      case DS_MSGRRECV_SPR_LOOPBACK:
      {
        msgr_spr_loopback_struct_type loopback_msg;

        errnum = msgr_loopback_reply(&loopback_msg, MSGR_DS_MSGRRECV);
        ASSERT(errnum == E_SUCCESS);
        break;
      }
      case DS_MSGRRECV_SPR_THREAD_KILL:
      {
        // Thread is being killed, cleanup
        dsmsgrrcv_task_exit();
        break;
      }

      default:
      {
        /*policyman_ind may come even before DS task is ready, this check ensures
           not to post any command to DS till some handler is registered*/

        msgr_client_data_block_p = dsmsgrrcv_get_client_data_block(
                                        dsmsgrrcv_super_cmd.ds_cmd.msg.hdr.id);

        if ((msgr_client_data_block_p == NULL) ||
            (msgr_client_data_block_p->func_ptr == NULL))
        {
          break;
        }

        msgr_client_data_block_p->func_ptr((msgr_hdr_struct_type*)
                                         &dsmsgrrcv_super_cmd.ds_cmd.msg,
                                         msgr_client_data_block_p->user_data);
        break;
      }
    }
  } /* for(;;) */
}/*dsmsgrrcv_thrd_main*/

/*===========================================================================
FUNCTION       DSMSGRRECV_TASK

DESCRIPTION    This function is responsible for registering the cback for the 
               pthread that is spawned by APP MGR 
 
PARAMETERS     priority
 
DEPENDENCIES   NONE

RETURN VALUE   NONE 

SIDE EFFECTS   NONE
===========================================================================*/
pthread_t dsmsgrrecv_task 
(
  int priority
)
{
  int rval;
  pthread_attr_t attr;
  struct sched_param param;
#if defined(FEATURE_CENTRALIZED_THREAD_PRIORITY) && !defined(TEST_FRAMEWORK)
  RCINIT_INFO info_handle = NULL ;
  RCINIT_PRIO prio = 0;
  unsigned long stksz = 0;
#endif /*FEATURE_CENTRALIZED_THREAD_PRIORITY and TEST_FRAMEWORK */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  // Init thread attributes
  rval = pthread_attr_init(&attr);
  ASSERT(rval == 0);
  
#if defined(FEATURE_CENTRALIZED_THREAD_PRIORITY) && !defined(TEST_FRAMEWORK)
  /* Look up task info in rcinit */
  info_handle = rcinit_lookup(DSMSGR_TASK_NAME_STR);

  if (info_handle == NULL) 
  {
    ERR_FATAL("DSMSGR_RECV task info not found",0 , 0, 0);
  }
  else
  {
    prio = rcinit_lookup_prio_info(info_handle);
    stksz = rcinit_lookup_stksz_info(info_handle);
    if ((prio > 255) || (stksz == 0))
    {
      ERR_FATAL("Invalid Priority:%d or Stack Size: %d",prio, stksz, 0);
    }
  }
#endif /*FEATURE_CENTRALIZED_THREAD_PRIORITY and TEST_FRAMEWORK */
  memset(&param,0,sizeof(param));
  
#if defined(FEATURE_CENTRALIZED_THREAD_PRIORITY) && !defined(TEST_FRAMEWORK)
  param.sched_priority = prio;
  rval = pthread_attr_setstacksize(&attr, (size_t)stksz);
  rval = pthread_attr_setschedparam(&attr, &param);
#else
  // Set priority to what the app mgr passed in
  param.sched_priority = priority;
  rval = pthread_attr_setschedparam(&attr, &param);
#endif /*FEATURE_CENTRALIZED_THREAD_PRIORITY and TEST_FRAMEWORK */

#ifndef T_UNIX
  rval = pthread_attr_setthreadname(&attr,DSMSGR_TASK_NAME_STR);
  ASSERT(rval == 0);
#endif

  rval = pthread_create
         (
           &dsmsgrrcv_thread, 
           &attr,
           dsmsgrrcv_thrd_main,
           NULL
         );

  if (rval != 0) 
  {
    DATA_MSG0_ERROR("DSMSGR RECV thread creation failed!");
  }

  return dsmsgrrcv_thread;
}/*dsmsgrrecv_task*/

