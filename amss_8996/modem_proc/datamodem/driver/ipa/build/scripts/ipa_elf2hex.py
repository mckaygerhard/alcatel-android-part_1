# Copyright (c) 2013
# Qualcomm Technologies Incorporated
#!/usr/bin/python

import binascii
import sys
import os

# Image/header locations are fixed
mhi_elf_path_tn = "./../img/MHI"
mhi_hdr_path_tn = "./../src/ipa_mhi_img_tn.h"
mhi_elf_path_le = "./../img/MHI_LE"
mhi_hdr_path_le = "./../src/ipa_mhi_img_le.h"

def exists(env):
    return env.Detect('ipa_elf2hex_conv')

def generate(env):
    ipa_elf2hex_conv(mhi_elf_path_tn, mhi_hdr_path_tn)
    #ipa_elf2hex_conv(mhi_elf_path_le, mhi_hdr_path_le)

def f_open(file_name, mode):
    try:
        fp = open(file_name, mode)
    except IOError:
        PrintError "The file could not be opened: " + file_name
	return 0

    # File open has succeeded with the given mode, return the file object
    return fp

def ipa_elf2hex_conv(source, target):
    num = 12
    #if target file already exists, dont do this again.
    #Be aware the target file could be dummy if build
    # was compiled w/o MHI image.
    if os.path.exists(target):
        return

    #create output file always
    dst_fp = f_open(target, "w+b")
    PrintInfo("elf2hex_conv: dest file: "+ dst_fp.name)
    src_fp = f_open(source, "rb")
    if src_fp != 0:
        PrintInfo("elf2hex_conv: src file: "+ src_fp.name)
        in_str = src_fp.read()
        for c in range(len(in_str)):
            out_ch = binascii.hexlify(in_str[c])
            if c != 0 :
                if c % num == 0 :
                    dst_fp.write("\n")
            if c == len(in_str) - 1 :
                dst_fp.write("0x"+out_ch)
                break
	    dst_fp.write("0x"+out_ch+", ")
        src_fp.close()
    else:
        #write the output file so compilation doesnt fail
        dst_fp.write("0xDE, 0xAD, 0xC0, 0xFE")
    dst_fp.close()

