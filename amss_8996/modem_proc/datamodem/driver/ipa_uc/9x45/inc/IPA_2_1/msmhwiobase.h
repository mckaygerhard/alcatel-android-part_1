#ifndef __MSMHWIOBASE_H__
#define __MSMHWIOBASE_H__
/*
===========================================================================
*/
/**
  @file msmhwiobase.h
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/driver/ipa_uc/9x45/inc/IPA_2_1/msmhwiobase.h#1 $
  $Author: mplcsds1 $

  ===========================================================================
*/

/*------------------------------------------------------------------------------
 * MODULE: ipa_wrapper
 *------------------------------------------------------------------------------*/

#define IPA_WRAPPER_BASE                                                                         0xfd4c0000
#define IPA_WRAPPER_BASE_SIZE                                                                    0x00000000
#define IPA_WRAPPER_BASE_PHYS                                                                    0xfd4c0000

#endif /* __MSMHWIOBASE_H__ */
