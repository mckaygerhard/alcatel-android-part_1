/*=========================================================================*//**
    @file  ipa_hw_interface_mhi.h

    @brief Define the IPA HW to IPA driver MHI interface
*//****************************************************************************/
/*------------------------------------------------------------------------------
    Copyright (c) 2014 Qualcomm Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Confidential and Proprietary
 
    $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/driver/ipa_uc/9x45/inc/common/ipa_hw_interface_mhi.h#1 $
------------------------------------------------------------------------------*/

#ifndef IPA_HW_INTERFACE_MHI_H_
#define IPA_HW_INTERFACE_MHI_H_
#include "ipa_hw_interface.h"

#define IPA_HW_INTERFACE_MHI_VERSION            0x0006     /**< A7 to MHI engine interface version*/

#define IPA_HW_MAX_NUMBER_OF_CHANNELS           3
#define IPA_HW_MAX_NUMBER_OF_EVENTRINGS         3
#define IPA_HW_MAX_CHANNEL_HANDLE               (IPA_HW_MAX_NUMBER_OF_CHANNELS-1)
#define IPA_HW_MHI_MAX_CMD_SIZE                 sizeof(IpaHwMhiInitCmdData_t)

/**
 * @brief   Values that represent MHI channel state machine.
 * Values are according to MHI specification
*/
typedef enum
{
	IPA_HW_MHI_CHANNEL_STATE_DISABLE = 0,  /**< Channel is disabled and not processed by the host or device.  */
	IPA_HW_MHI_CHANNEL_STATE_ENABLE  = 1,  /**< A channel is enabled after being initialized and configured by host, including its channel context and associated transfer ring. While this state, the channel is not active and the device does not process transfer.  */
	IPA_HW_MHI_CHANNEL_STATE_RUN     = 2,  /**< The device processes transfers and doorbell for channels.  */
	IPA_HW_MHI_CHANNEL_STATE_SUSPEND = 3,  /**< Used to halt operations on the channel. The device does not process transfers for the channel in this state. This state is typically used to synchronize the transition to low power modes.  */
	IPA_HW_MHI_CHANNEL_STATE_STOP	 = 4,  /**< Used to halt operations on the channel. The device does not process transfers for the channel in this state. */
	IPA_HW_MHI_CHANNEL_STATE_ERROR   = 5,   /**< The device detected an error in an element from the transfer ring associated with the channel.  */
	IPA_HW_MHI_CHANNEL_STATE_INVALID = 0xFF /**< Invalid state. Shall not be in use in operational scenario  */
}IPA_HW_MHI_CHANNEL_STATES;

/**
 * @brief   Values that represent the MHI commands from CPU 
 *          to IPA HW.
*/
typedef enum
{
   IPA_CPU_2_HW_CMD_MHI_INIT                  = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 0),     /**< Initialize HW to be ready for MHI processing. Once operation was completed HW shall respond with IPA_HW_2_CPU_RESPONSE_CMD_COMPLETED */
   IPA_CPU_2_HW_CMD_MHI_INIT_CHANNEL          = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 1),     /**< Initialize specific channel to be ready to serve MHI transfers. Once initialization was completed HW shall respond with IPA_HW_2_CPU_RESPONSE_MHI_CHANGE_CHANNEL_STATE.IPA_HW_MHI_CHANNEL_STATE_ENABLE  */
   IPA_CPU_2_HW_CMD_MHI_UPDATE_MSI            = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 2),     /**< Update MHI MSI interrupts data. Once operation was completed HW shall respond with IPA_HW_2_CPU_RESPONSE_CMD_COMPLETED */
   IPA_CPU_2_HW_CMD_MHI_CHANGE_CHANNEL_STATE  = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 3),     /**< Change specific channel processing state following host request. Once operation was completed HW shall respond with IPA_HW_2_CPU_RESPONSE_MHI_CHANGE_CHANNEL_STATE */
   IPA_CPU_2_HW_CMD_MHI_DL_UL_SYNC_INFO       = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 4),     /**< Info related to DL UL syncronization */
   IPA_CPU_2_HW_CMD_MHI_STOP_EVENT_UPDATE     = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 5),     /**< Cmd to stop event ring processing */
}IPA_CPU_2_HW_MHI_COMMANDS;

/**
 * @brief   Values that represent MHI related HW responses to 
 *          CPU commands.
*/
typedef enum
{
   IPA_HW_2_CPU_RESPONSE_MHI_CHANGE_CHANNEL_STATE  = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 0),   /**< Response to IPA_CPU_2_HW_CMD_MHI_INIT_CHANNEL or IPA_CPU_2_HW_CMD_MHI_CHANGE_CHANNEL_STATE commands. */
}IPA_HW_2_CPU_MHI_RESPONSES;

/**
 * @brief   Values that represent MHI related HW event to be 
 *          sent to CPU.
*/
typedef enum
{
   IPA_HW_2_CPU_EVENT_MHI_CHANNEL_ERROR               = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 0),   /**< Event specify the device detected an error in an element from the transfer ring associated with the channel */
   IPA_HW_2_CPU_EVENT_MHI_CHANNEL_WAKE_UP_REQUEST     = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 1),   /**< Event specify a bam interrupt was asserted when MHI engine is suspended*/
}IPA_HW_2_CPU_MHI_EVENTS;

/**
 * @brief   Channel error types.
*/
typedef enum
{
   IPA_HW_CHANNEL_ERROR_NONE, 	          /**< No error persists.  */
   IPA_HW_CHANNEL_INVALID_RE_ERROR 	      /**< Invalid Ring Element was detected  */
}IPA_HW_CHANNEL_ERRORS;

/**
 * @brief   MHI error types.
*/
typedef enum
{
   IPA_HW_INVALID_MMIO_ERROR     = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 0),	 /**< Invalid data read from MMIO space */
   IPA_HW_INVALID_CHANNEL_ERROR  = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 1),	 /**< Invalid data read from channel context array */
   IPA_HW_INVALID_EVENT_ERROR    = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 2),	 /**< Invalid data read from event ring context array */
   IPA_HW_NO_ED_IN_RING_ERROR    = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 4),	 /**< No event descriptors are available to report on secondary event ring */
   IPA_HW_LINK_ERROR             = FEATURE_ENUM_VAL(IPA_HW_FEATURE_MHI, 5),  /**< Link error */
}IPA_HW_MHI_ERRORS;


/**
 * @brief   Structure referring to the common and MHI section of 128B shared memory located 
 *          in offset zero of SW Partition in IPA SRAM. 
 * @note    The shared memory is used for communication between IPA HW and CPU. 
 *                                                                                        
*/
typedef struct IpaHwSharedMemMhiMapping_t
{
   /*word 0 - word 9 */
   IpaHwSharedMemCommonMapping_t common;
   /*word 10 */
   uint16_t interfaceVersionMhi;   /**< The MHI interface version as reported by HW*/  	  
   uint8_t  mhiState;              /**< Overall MHI state */  	  
   uint8_t  reserved_2B;
   /*word 11 */
   uint8_t mhiCnl0State;      /**< State of MHI channel X. The state carries information regarding the error type. See IPA_HW_MHI_CHANNEL_STATES */
   uint8_t mhiCnl1State;
   uint8_t mhiCnl2State;
   uint8_t mhiCnl3State;
   /*word 12 */
   uint8_t mhiCnl4State;
   uint8_t mhiCnl5State;
   uint8_t mhiCnl6State;
   uint8_t mhiCnl7State; 
   /*word 13 */
   uint32_t reserved_37_34;
   /*word 14 */
   uint32_t reserved_3B_38;
   /*word 15 */
   uint32_t reserved_3F_3C;
}IpaHwSharedMemMhiMapping_t;


/*CPU->HW MHI commands*/

/**
 * @brief   Structure holding the parameters for IPA_CPU_2_HW_CMD_MHI_INIT command.
 *          Parameters are sent as pointer thus should be reside in address accessible to HW
*/
typedef struct IpaHwMhiInitCmdData_t
{
   uint32_t msiAddress;          	   /**< The MSI base (in device space) used for asserting the interrupt (MSI) associated with the event ring.*/
   uint32_t mmioBaseAddress;           /**< The address   (in device space) of MMIO structure in host space*/
   uint32_t deviceMhiCtrlBaseAddress;  /**< Base address of the memory region in the device address space where the MHI control data structures are allocated by the host, including channel context array, event context array, and rings. This value is used for host/device address translation */
   uint32_t deviceMhiDataBaseAddress;  /**< Base address of the memory region in the device address space where the MHI data buffers are allocated by the host. This value is used for host/device address translation */
   uint32_t firstChannelIndex;  	   /**< First channel ID. Doorbell 0 is mapped to this channel. */
   uint32_t firstEventRingIndex;  	   /**< First event ring ID. Doorbell 16 is mapped to this event ring. */
}IpaHwMhiInitCmdData_t;

/**
 * @brief   Structure holding the parameters for IPA_CPU_2_HW_CMD_MHI_INIT_CHANNEL command.
 *          Parameters are sent as 32b immediate parameters.
*/
typedef union IpaHwMhiInitChannelCmdData_t
{
   struct IpaHwMhiInitChannelCmdParams_t
   {
      uint32_t channelHandle     :8;   	/**< The channel identifier as allocated by driver. value is within the range 0 to IPA_HW_MAX_CHANNEL_HANDLE*/
      uint32_t contexArrayIndex  :8;      /**< Unique index for channels, between 0 and 255. The index is used as an index in channel context array structures. */
      uint32_t bamPipeId         :6;      /**< The BAM pipe number for pipe dedicated for this channel */
      uint32_t channelDirection  :2; 	   /**< The direction of the channel as defined in the channel type field (CHTYPE) in the channel context data structure.  */
      uint32_t reserved          :8;
   }params;
   uint32_t raw32b;
}IpaHwMhiInitChannelCmdData_t;

/**
 * @brief   Structure holding the parameters for IPA_CPU_2_HW_CMD_MHI_UPDATE_MSI command.
*/
typedef struct IpaHwMhiMsiCmdData_t
{
   uint32_t msiAddress_low;       	   /**< The MSI lower base addr (in device space) used for asserting the interrupt (MSI) associated with the event ring.*/
   uint32_t msiAddress_hi;       	   /**< The MSI higher base addr (in device space) used for asserting the interrupt (MSI) associated with the event ring.*/
   uint32_t msiMask;             	   /**< Mask indicating number of messages assigned by the host to device*/
   uint32_t msiData;             	   /**< Data Pattern to use when generating the MSI*/
}IpaHwMhiMsiCmdData_t;

/**
 * @brief   Structure holding the parameters for IPA_CPU_2_HW_CMD_MHI_CHANGE_CHANNEL_STATE command.
 *          Parameters are sent as 32b immediate parameters.
*/
typedef union IpaHwMhiChangeChannelStateCmdData_t
{
   struct IpaHwMhiChangeChannelStateCmdParams_t
   {
      uint32_t  requestedState        :8;  /**< The requested channel state as was indicated from Host. Use IPA_HW_MHI_CHANNEL_STATES to specify the requested state */
      uint32_t  channelHandle         :8;  /**< The channel identifier as allocated by driver. value is within the range 0 to IPA_HW_MAX_CHANNEL_HANDLE*/
      uint32_t  LPTransitionRejected  :8;  /**< Indication that low power state transition was rejected. */
      uint32_t  reserved              :8;
   }params;
   uint32_t raw32b;
}IpaHwMhiChangeChannelStateCmdData_t;

/**
 * @brief   Structure holding the parameters for 
 *          IPA_CPU_2_HW_CMD_MHI_DL_UL_SYNC_INFO command.
 *          Parameters are sent as 32b immediate parameters.
*/
typedef union IpaHwMhiDlUlSyncCmdData_t
{
   struct IpaHwMhiDlUlSyncCmdParams_t
   {
      uint32_t  isDlUlSyncEnabled   :8;  /**< Flag to indicate if DL UL Syncronization is enabled */
      uint32_t  UlAccmVal           :8;  /**< UL Timer Accumulation value (Period after which device will poll for UL data) */
      uint32_t  ulMsiEventThreshold :8;  /**< Threshold at which HW fires MSI to host for UL events */
      uint32_t  dlMsiEventThreshold :8;  /**< Threshold at which HW fires MSI to host for UL events */
   }params;
   uint32_t raw32b;
}IpaHwMhiDlUlSyncCmdData_t;

/*HW->Events MHI responses*/

/**
 * @brief   Structure holding the parameters for IPA_HW_2_CPU_RESPONSE_MHI_CHANGE_CHANNEL_STATE response.
 *          Parameters are sent as 32b immediate parameters.
*/
typedef union IpaHwMhiChangeChannelStateResponseData_t
{
   struct IpaHwMhiChangeChannelStateResponseParams_t
   {
      uint32_t     state             :8;  /**< The new channel state. In case state is not as requested this is error indication for the last command*/
      uint32_t     channelHandle     :8;  /**< The channel identifier */
      uint32_t     additonalParams   :16; /**< For stop: the number of pending bam descriptors currently queued */
   }params;
   uint32_t raw32b;
}IpaHwMhiChangeChannelStateResponseData_t;

/*HW->CPU MHI Events */

typedef union IpaHwMhiChannelErrorEventData_t
{
   struct IpaHwMhiChannelErrorEventParams_t
   {
      uint32_t     errorType      :8;      /**< Type of error - IPA_HW_CHANNEL_ERRORS*/
      uint32_t     channelHandle  :8;      /**< The channel identifier as allocated by driver. value is within the range 0 to IPA_HW_MAX_CHANNEL_HANDLE*/
      uint32_t     reserved       :16;
   }params;
   uint32_t raw32b;
}IpaHwMhiChannelErrorEventData_t;

typedef union IpaHwMhiChannelWakeupEventData_t
{
   struct IpaHwMhiChannelWakeupEventParams_t
   {
      uint32_t     channelHandle  :8;     /**< The channel identifier as allocated by driver. value is within the range 0 to IPA_HW_MAX_CHANNEL_HANDLE*/
      uint32_t     reserved       :24;
   }params;
   uint32_t raw32b;
}IpaHwMhiChannelWakeupEventData_t;

typedef IpaHwMhiChannelWakeupEventData_t IpaHwMhiStopEventUpdateData_t;

/******************************************************************************
               Data Types related to Statistics Collection 
******************************************************************************/
/**
 * @brief   Structure holding the MHI Common statistics
*/
typedef struct IpaHwStatsMhiCmnInfoData_t
{
  uint32_t numULDLSync;      /**< Number of times UL activity trigged due to DL activity */
  uint32_t numULTimerExpired;/**< Number of times UL Accm Timer expired */
  uint32_t numChEvCtxWpRead;
  uint32_t reserved;
}IpaHwStatsMhiCmnInfoData_t;

/**
 * @brief   Structure holding the MHI Channel statistics
*/
typedef struct IpaHwStatsMhiCnlInfoData_t
{
   uint32_t doorbellInt;      /**< The number of doorbell int */
   uint32_t reProccesed;      /**< The number of ring elements processed */
   uint32_t bamInt;           /**< Number of BAM Interrupts */
   uint32_t delayedMsi;       /**< Number of times device triggered MSI to host after Interrupt Moderation Timer expiry */
   uint32_t immediateMsi;     /**< Number of times device triggered MSI to host immediately */
   uint32_t thresholdMsi;     /**< Number of times device triggered MSI due to max pending events threshold reached */
   uint32_t numSuspend;       /**< Number of times channel was suspended */
   uint32_t numResume;        /**< Number of times channel was resumed */
   uint32_t num_OOB;          /**< Number of times we indicated that we are OOB */   
   uint32_t num_OOB_timer_expiry;
                              /**< Number of times we indicated that we are OOB after timer expiry */   
   uint32_t num_OOB_moderation_timer_start;
                                 /**< Number of times we started timer after sending OOB and hitting OOB again before we 
                                       processed threshold number of packets */
   uint32_t num_db_mode_evt;     /**< Number of times we indicated that we are in Doorbell mode */
   IpaHwBamStats_t  bamStats;    /**< Bam pipe statistics */
   IpaHwRingStats_t ringStats;   /**< Transfer Ring statistics */
}IpaHwStatsMhiCnlInfoData_t;

/**
 * @brief   Structure holding the MHI statistics
*/
typedef struct IpaHwStatsMhiInfoData_t
{
   IpaHwStatsMhiCmnInfoData_t mhiCmnStats;                                   /**< Stats pertaining to MHI */
   IpaHwStatsMhiCnlInfoData_t mhiCnlStats[IPA_HW_MAX_NUMBER_OF_CHANNELS];    /**< Stats pertaining to each channel */
}IpaHwStatsMhiInfoData_t;

/******************************************************************************
               Data Types related to Config Information
******************************************************************************/
/**
 * @brief   Structure holding the MHI Common Config info
*/
typedef struct IpaHwConfigMhiCmnInfoData_t
{
   uint8_t  isDlUlSyncEnabled;   /**< Flag to indicate if DL-UL synchronization is enabled */
   uint8_t  UlAccmVal;           /**< Out Channel(UL) accumulation time in ms when DL UL Sync is enabled */
   uint8_t  ulMsiEventThreshold; /**< Threshold at which HW fires MSI to host for UL events */
   uint8_t  dlMsiEventThreshold; /**< Threshold at which HW fires MSI to host for DL events */
}IpaHwConfigMhiCmnInfoData_t;

/**
 * @brief   Structure holding the MSI Config info
*/
typedef IpaHwMhiMsiCmdData_t IpaHwConfigMhiMsiInfoData_t;

/**
 * @brief   Structure holding the MHI Channel Config info
*/
typedef struct IpaHwConfigMhiCnlInfoData_t
{
   uint16_t transferRingSize;      /**< The Transfer Ring size in terms of Ring Elements */
   uint8_t  transferRingIndex;     /**< The Transfer Ring channel number as defined by host */
   uint8_t  eventRingIndex;        /**< The Event Ring Index associated with this Transfer Ring */
   uint8_t  bamPipeIndex;          /**< The BAM Pipe associated with this channel */
   uint8_t  isOutChannel;          /**< Indication for the direction of channel */
   uint8_t  reserved_0;            /** Reserved byte for maintaining 4byte alignment */
   uint8_t  reserved_1;            /** Reserved byte for maintaining 4byte alignment */
}IpaHwConfigMhiCnlInfoData_t;

/**
 * @brief   Structure holding the MHI Event Config info
*/
typedef struct IpaHwConfigMhiEventInfoData_t
{
   uint32_t msiVec;                /**< msi vector to invoke MSI interrupt */
   uint16_t intmodtValue;          /**< Interrupt moderation timer (in milliseconds) */
   uint16_t eventRingSize;         /**< The Event Ring size in terms of Ring Elements */
   uint8_t  eventRingIndex;        /**< The Event Ring number as defined by host */
   uint8_t  reserved_0;             /** Reserved byte to maintain 4byte alignment */
   uint8_t  reserved_1;             /** Reserved byte to maintain 4byte alignment */
   uint8_t  reserved_2;             /** Reserved byte to maintain 4byte alignment */
}IpaHwConfigMhiEventInfoData_t;

/**
 * @brief   Structure holding the MHI Config info
*/
typedef struct IpaHwConfigMhiInfoData_t
{
   IpaHwConfigMhiCmnInfoData_t    mhiCmnCfg;                                  /**< Common Config pertaining to MHI */ 
   IpaHwConfigMhiMsiInfoData_t    mhiMsiCfg;                                  /**< Config pertaining to MSI config */ 
   IpaHwConfigMhiCnlInfoData_t    mhiCnlCfg[IPA_HW_MAX_NUMBER_OF_CHANNELS];   /**< Config pertaining to each channel */
   IpaHwConfigMhiEventInfoData_t  mhiEvtCfg[IPA_HW_MAX_NUMBER_OF_EVENTRINGS]; /**< Config pertaining to each event Ring */
}IpaHwConfigMhiInfoData_t;


#endif /* IPA_HW_INTERFACE_MHI_H_ */
