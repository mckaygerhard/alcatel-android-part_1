#ifndef __MSMHWIOBASE_H__
#define __MSMHWIOBASE_H__
/* ===========================================================================

              H W I O   I N T E R F A C E   D E F I N I T I O N S

                     *** A U T O G E N E R A T E D ***

==============================================================================

 ADDRESS FILE VERSION: (unknown)

 Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved

==============================================================================
 $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/driver/ipa_uc/9x45/inc/IPA_2_0/msmhwiobase.h#1 $
=========================================================================== */

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_BASE                                                                            0x00000000
#define EBI1_MEM_BASE_SIZE                                                                       0x00000000
#define EBI1_MEM_BASE_PHYS                                                                       0x00000000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_end
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_END_BASE                                                                        0xf7ffffff
#define EBI1_MEM_END_BASE_SIZE                                                                   0x00000000
#define EBI1_MEM_END_BASE_PHYS                                                                   0xf7ffffff

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_size
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_SIZE_BASE                                                                       0xf8000000
#define EBI1_MEM_SIZE_BASE_SIZE                                                                  0x00000000
#define EBI1_MEM_SIZE_BASE_PHYS                                                                  0xf8000000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region0
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION0_BASE                                                                0x00000000
#define EBI1_MEM_RPM_REGION0_BASE_SIZE                                                           0x00000000
#define EBI1_MEM_RPM_REGION0_BASE_PHYS                                                           0x00000000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region0_end
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION0_END_BASE                                                            0x000fffff
#define EBI1_MEM_RPM_REGION0_END_BASE_SIZE                                                       0x00000000
#define EBI1_MEM_RPM_REGION0_END_BASE_PHYS                                                       0x000fffff

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region0_size
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION0_SIZE_BASE                                                           0x00100000
#define EBI1_MEM_RPM_REGION0_SIZE_BASE_SIZE                                                      0x00000000
#define EBI1_MEM_RPM_REGION0_SIZE_BASE_PHYS                                                      0x00100000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region1
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION1_BASE                                                                0x00100000
#define EBI1_MEM_RPM_REGION1_BASE_SIZE                                                           0x00000000
#define EBI1_MEM_RPM_REGION1_BASE_PHYS                                                           0x00100000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region1_end
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION1_END_BASE                                                            0x0017ffff
#define EBI1_MEM_RPM_REGION1_END_BASE_SIZE                                                       0x00000000
#define EBI1_MEM_RPM_REGION1_END_BASE_PHYS                                                       0x0017ffff

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region1_size
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION1_SIZE_BASE                                                           0x00080000
#define EBI1_MEM_RPM_REGION1_SIZE_BASE_SIZE                                                      0x00000000
#define EBI1_MEM_RPM_REGION1_SIZE_BASE_PHYS                                                      0x00080000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region2
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION2_BASE                                                                0x00180000
#define EBI1_MEM_RPM_REGION2_BASE_SIZE                                                           0x00000000
#define EBI1_MEM_RPM_REGION2_BASE_PHYS                                                           0x00180000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region2_end
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION2_END_BASE                                                            0x001fffff
#define EBI1_MEM_RPM_REGION2_END_BASE_SIZE                                                       0x00000000
#define EBI1_MEM_RPM_REGION2_END_BASE_PHYS                                                       0x001fffff

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region2_size
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION2_SIZE_BASE                                                           0x00080000
#define EBI1_MEM_RPM_REGION2_SIZE_BASE_SIZE                                                      0x00000000
#define EBI1_MEM_RPM_REGION2_SIZE_BASE_PHYS                                                      0x00080000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region3
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION3_BASE                                                                0x00200000
#define EBI1_MEM_RPM_REGION3_BASE_SIZE                                                           0x00000000
#define EBI1_MEM_RPM_REGION3_BASE_PHYS                                                           0x00200000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region3_end
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION3_END_BASE                                                            0xdfffffff
#define EBI1_MEM_RPM_REGION3_END_BASE_SIZE                                                       0x00000000
#define EBI1_MEM_RPM_REGION3_END_BASE_PHYS                                                       0xdfffffff

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region3_size
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION3_SIZE_BASE                                                           0xdfe00000
#define EBI1_MEM_RPM_REGION3_SIZE_BASE_SIZE                                                      0x00000000
#define EBI1_MEM_RPM_REGION3_SIZE_BASE_PHYS                                                      0xdfe00000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region4
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION4_BASE                                                                0xe0000000
#define EBI1_MEM_RPM_REGION4_BASE_SIZE                                                           0x00000000
#define EBI1_MEM_RPM_REGION4_BASE_PHYS                                                           0xe0000000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region4_end
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION4_END_BASE                                                            0xe00fffff
#define EBI1_MEM_RPM_REGION4_END_BASE_SIZE                                                       0x00000000
#define EBI1_MEM_RPM_REGION4_END_BASE_PHYS                                                       0xe00fffff

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region4_size
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION4_SIZE_BASE                                                           0x00100000
#define EBI1_MEM_RPM_REGION4_SIZE_BASE_SIZE                                                      0x00000000
#define EBI1_MEM_RPM_REGION4_SIZE_BASE_PHYS                                                      0x00100000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region5
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION5_BASE                                                                0xe0100000
#define EBI1_MEM_RPM_REGION5_BASE_SIZE                                                           0x00000000
#define EBI1_MEM_RPM_REGION5_BASE_PHYS                                                           0xe0100000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region5_end
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION5_END_BASE                                                            0xf7ffffff
#define EBI1_MEM_RPM_REGION5_END_BASE_SIZE                                                       0x00000000
#define EBI1_MEM_RPM_REGION5_END_BASE_PHYS                                                       0xf7ffffff

/*------------------------------------------------------------------------------
 * MODULE: ebi1_mem_rpm_region5_size
 *------------------------------------------------------------------------------*/

#define EBI1_MEM_RPM_REGION5_SIZE_BASE                                                           0x17f00000
#define EBI1_MEM_RPM_REGION5_SIZE_BASE_SIZE                                                      0x00000000
#define EBI1_MEM_RPM_REGION5_SIZE_BASE_PHYS                                                      0x17f00000

/*------------------------------------------------------------------------------
 * MODULE: system_imem
 *------------------------------------------------------------------------------*/

#define SYSTEM_IMEM_BASE                                                                         0xfe800000
#define SYSTEM_IMEM_BASE_SIZE                                                                    0x00000000
#define SYSTEM_IMEM_BASE_PHYS                                                                    0xfe800000

/*------------------------------------------------------------------------------
 * MODULE: system_imem_end
 *------------------------------------------------------------------------------*/

#define SYSTEM_IMEM_END_BASE                                                                     0xfe9fffff
#define SYSTEM_IMEM_END_BASE_SIZE                                                                0x00000000
#define SYSTEM_IMEM_END_BASE_PHYS                                                                0xfe9fffff

/*------------------------------------------------------------------------------
 * MODULE: system_imem_size
 *------------------------------------------------------------------------------*/

#define SYSTEM_IMEM_SIZE_BASE                                                                    0x00200000
#define SYSTEM_IMEM_SIZE_BASE_SIZE                                                               0x00000000
#define SYSTEM_IMEM_SIZE_BASE_PHYS                                                               0x00200000

/*------------------------------------------------------------------------------
 * MODULE: lpass_lpm
 *------------------------------------------------------------------------------*/

#define LPASS_LPM_BASE                                                                           0xfe090000
#define LPASS_LPM_BASE_SIZE                                                                      0x00000000
#define LPASS_LPM_BASE_PHYS                                                                      0xfe090000

/*------------------------------------------------------------------------------
 * MODULE: lpass_lpm_end
 *------------------------------------------------------------------------------*/

#define LPASS_LPM_END_BASE                                                                       0xfe09ffff
#define LPASS_LPM_END_BASE_SIZE                                                                  0x00000000
#define LPASS_LPM_END_BASE_PHYS                                                                  0xfe09ffff

/*------------------------------------------------------------------------------
 * MODULE: lpass_lpm_size
 *------------------------------------------------------------------------------*/

#define LPASS_LPM_SIZE_BASE                                                                      0x00010000
#define LPASS_LPM_SIZE_BASE_SIZE                                                                 0x00000000
#define LPASS_LPM_SIZE_BASE_PHYS                                                                 0x00010000

/*------------------------------------------------------------------------------
 * MODULE: mss_tcm
 *------------------------------------------------------------------------------*/

#define MSS_TCM_BASE                                                                             0xfcc00000
#define MSS_TCM_BASE_SIZE                                                                        0x00000000
#define MSS_TCM_BASE_PHYS                                                                        0xfcc00000

/*------------------------------------------------------------------------------
 * MODULE: mss_tcm_end
 *------------------------------------------------------------------------------*/

#define MSS_TCM_END_BASE                                                                         0xfcc9ffff
#define MSS_TCM_END_BASE_SIZE                                                                    0x00000000
#define MSS_TCM_END_BASE_PHYS                                                                    0xfcc9ffff

/*------------------------------------------------------------------------------
 * MODULE: mss_tcm_size
 *------------------------------------------------------------------------------*/

#define MSS_TCM_SIZE_BASE                                                                        0x000a0000
#define MSS_TCM_SIZE_BASE_SIZE                                                                   0x00000000
#define MSS_TCM_SIZE_BASE_PHYS                                                                   0x000a0000

/*------------------------------------------------------------------------------
 * MODULE: boot_rom_start_address
 *------------------------------------------------------------------------------*/

#define BOOT_ROM_START_ADDRESS_BASE                                                              0xfc000000
#define BOOT_ROM_START_ADDRESS_BASE_SIZE                                                         0x00000000
#define BOOT_ROM_START_ADDRESS_BASE_PHYS                                                         0xfc000000

/*------------------------------------------------------------------------------
 * MODULE: boot_rom_end_address
 *------------------------------------------------------------------------------*/

#define BOOT_ROM_END_ADDRESS_BASE                                                                0xfc02ffff
#define BOOT_ROM_END_ADDRESS_BASE_SIZE                                                           0x00000000
#define BOOT_ROM_END_ADDRESS_BASE_PHYS                                                           0xfc02ffff

/*------------------------------------------------------------------------------
 * MODULE: boot_rom_size
 *------------------------------------------------------------------------------*/

#define BOOT_ROM_SIZE_BASE                                                                       0x00030000
#define BOOT_ROM_SIZE_BASE_SIZE                                                                  0x00000000
#define BOOT_ROM_SIZE_BASE_PHYS                                                                  0x00030000

/*------------------------------------------------------------------------------
 * MODULE: rpm_code_ram_start_address
 *------------------------------------------------------------------------------*/

#define RPM_CODE_RAM_START_ADDRESS_BASE                                                          0xfc100000
#define RPM_CODE_RAM_START_ADDRESS_BASE_SIZE                                                     0x00000000
#define RPM_CODE_RAM_START_ADDRESS_BASE_PHYS                                                     0xfc100000

/*------------------------------------------------------------------------------
 * MODULE: rpm_code_ram_end_address
 *------------------------------------------------------------------------------*/

#define RPM_CODE_RAM_END_ADDRESS_BASE                                                            0xfc17ffff
#define RPM_CODE_RAM_END_ADDRESS_BASE_SIZE                                                       0x00000000
#define RPM_CODE_RAM_END_ADDRESS_BASE_PHYS                                                       0xfc17ffff

/*------------------------------------------------------------------------------
 * MODULE: rpm_code_ram_size
 *------------------------------------------------------------------------------*/

#define RPM_CODE_RAM_SIZE_BASE                                                                   0x00080000
#define RPM_CODE_RAM_SIZE_BASE_SIZE                                                              0x00000000
#define RPM_CODE_RAM_SIZE_BASE_PHYS                                                              0x00080000

/*------------------------------------------------------------------------------
 * MODULE: rpm_ss_msg_ram_start_address
 *------------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_START_ADDRESS_BASE                                                        0xfc428000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_SIZE                                                   0x00000000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_PHYS                                                   0xfc428000

/*------------------------------------------------------------------------------
 * MODULE: rpm_ss_msg_ram_end_address
 *------------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_END_ADDRESS_BASE                                                          0xfc42ffff
#define RPM_SS_MSG_RAM_END_ADDRESS_BASE_SIZE                                                     0x00000000
#define RPM_SS_MSG_RAM_END_ADDRESS_BASE_PHYS                                                     0xfc42ffff

/*------------------------------------------------------------------------------
 * MODULE: rpm_ss_msg_ram_size
 *------------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_SIZE_BASE                                                                 0x00008000
#define RPM_SS_MSG_RAM_SIZE_BASE_SIZE                                                            0x00000000
#define RPM_SS_MSG_RAM_SIZE_BASE_PHYS                                                            0x00008000

/*------------------------------------------------------------------------------
 * MODULE: boot_rom
 *------------------------------------------------------------------------------*/

#define BOOT_ROM_BASE                                                                            0xfc000000
#define BOOT_ROM_BASE_SIZE                                                                       0x00000000
#define BOOT_ROM_BASE_PHYS                                                                       0xfc000000

/*------------------------------------------------------------------------------
 * MODULE: bimc
 *------------------------------------------------------------------------------*/

#define BIMC_BASE                                                                                0xfc380000
#define BIMC_BASE_SIZE                                                                           0x00000000
#define BIMC_BASE_PHYS                                                                           0xfc380000

/*------------------------------------------------------------------------------
 * MODULE: clk_ctl
 *------------------------------------------------------------------------------*/

#define CLK_CTL_BASE                                                                             0xfc400000
#define CLK_CTL_BASE_SIZE                                                                        0x00000000
#define CLK_CTL_BASE_PHYS                                                                        0xfc400000

/*------------------------------------------------------------------------------
 * MODULE: core_top_csr
 *------------------------------------------------------------------------------*/

#define CORE_TOP_CSR_BASE                                                                        0xfd480000
#define CORE_TOP_CSR_BASE_SIZE                                                                   0x00000000
#define CORE_TOP_CSR_BASE_PHYS                                                                   0xfd480000

/*------------------------------------------------------------------------------
 * MODULE: crypto0_crypto_top
 *------------------------------------------------------------------------------*/

#define CRYPTO0_CRYPTO_TOP_BASE                                                                  0xfd400000
#define CRYPTO0_CRYPTO_TOP_BASE_SIZE                                                             0x00000000
#define CRYPTO0_CRYPTO_TOP_BASE_PHYS                                                             0xfd400000

/*------------------------------------------------------------------------------
 * MODULE: dehr_bimc_wrapper
 *------------------------------------------------------------------------------*/

#define DEHR_BIMC_WRAPPER_BASE                                                                   0xfc498000
#define DEHR_BIMC_WRAPPER_BASE_SIZE                                                              0x00000000
#define DEHR_BIMC_WRAPPER_BASE_PHYS                                                              0xfc498000

/*------------------------------------------------------------------------------
 * MODULE: ebi1_phy_cfg
 *------------------------------------------------------------------------------*/

#define EBI1_PHY_CFG_BASE                                                                        0xfc4e0000
#define EBI1_PHY_CFG_BASE_SIZE                                                                   0x00000000
#define EBI1_PHY_CFG_BASE_PHYS                                                                   0xfc4e0000

/*------------------------------------------------------------------------------
 * MODULE: torino_a7ss
 *------------------------------------------------------------------------------*/

#define TORINO_A7SS_BASE                                                                         0xf9000000
#define TORINO_A7SS_BASE_SIZE                                                                    0x00000000
#define TORINO_A7SS_BASE_PHYS                                                                    0xf9000000

/*------------------------------------------------------------------------------
 * MODULE: lpass
 *------------------------------------------------------------------------------*/

#define LPASS_BASE                                                                               0xfe000000
#define LPASS_BASE_SIZE                                                                          0x00000000
#define LPASS_BASE_PHYS                                                                          0xfe000000

/*------------------------------------------------------------------------------
 * MODULE: mpm2_mpm
 *------------------------------------------------------------------------------*/

#define MPM2_MPM_BASE                                                                            0xfc4a0000
#define MPM2_MPM_BASE_SIZE                                                                       0x00000000
#define MPM2_MPM_BASE_PHYS                                                                       0xfc4a0000

/*------------------------------------------------------------------------------
 * MODULE: modem_top
 *------------------------------------------------------------------------------*/

#define MODEM_TOP_BASE                                                                           0xfc800000
#define MODEM_TOP_BASE_SIZE                                                                      0x00000000
#define MODEM_TOP_BASE_PHYS                                                                      0xfc800000

/*------------------------------------------------------------------------------
 * MODULE: ocimem_wrapper_csr
 *------------------------------------------------------------------------------*/

#define OCIMEM_WRAPPER_CSR_BASE                                                                  0xfc488000
#define OCIMEM_WRAPPER_CSR_BASE_SIZE                                                             0x00000000
#define OCIMEM_WRAPPER_CSR_BASE_PHYS                                                             0xfc488000

/*------------------------------------------------------------------------------
 * MODULE: pcie_pcie20_wrapper_ahb
 *------------------------------------------------------------------------------*/

#define PCIE_PCIE20_WRAPPER_AHB_BASE                                                             0xfc520000
#define PCIE_PCIE20_WRAPPER_AHB_BASE_SIZE                                                        0x00000000
#define PCIE_PCIE20_WRAPPER_AHB_BASE_PHYS                                                        0xfc520000

/*------------------------------------------------------------------------------
 * MODULE: pcie_pcie20_wrapper_axi
 *------------------------------------------------------------------------------*/

#define PCIE_PCIE20_WRAPPER_AXI_BASE                                                             0xff800000
#define PCIE_PCIE20_WRAPPER_AXI_BASE_SIZE                                                        0x00000000
#define PCIE_PCIE20_WRAPPER_AXI_BASE_PHYS                                                        0xff800000

/*------------------------------------------------------------------------------
 * MODULE: periph_ss
 *------------------------------------------------------------------------------*/

#define PERIPH_SS_BASE                                                                           0xf9800000
#define PERIPH_SS_BASE_SIZE                                                                      0x00000000
#define PERIPH_SS_BASE_PHYS                                                                      0xf9800000

/*------------------------------------------------------------------------------
 * MODULE: pmic_arb
 *------------------------------------------------------------------------------*/

#define PMIC_ARB_BASE                                                                            0xfc4c0000
#define PMIC_ARB_BASE_SIZE                                                                       0x00000000
#define PMIC_ARB_BASE_PHYS                                                                       0xfc4c0000

/*------------------------------------------------------------------------------
 * MODULE: rpm
 *------------------------------------------------------------------------------*/

#define RPM_BASE                                                                                 0xfc100000
#define RPM_BASE_SIZE                                                                            0x00000000
#define RPM_BASE_PHYS                                                                            0xfc100000

/*------------------------------------------------------------------------------
 * MODULE: rpm_msg_ram
 *------------------------------------------------------------------------------*/

#define RPM_MSG_RAM_BASE                                                                         0xfc420000
#define RPM_MSG_RAM_BASE_SIZE                                                                    0x00000000
#define RPM_MSG_RAM_BASE_PHYS                                                                    0xfc420000

/*------------------------------------------------------------------------------
 * MODULE: qdss_qdss_ahb
 *------------------------------------------------------------------------------*/

#define QDSS_QDSS_AHB_BASE                                                                       0xfc368000
#define QDSS_QDSS_AHB_BASE_SIZE                                                                  0x00000000
#define QDSS_QDSS_AHB_BASE_PHYS                                                                  0xfc368000

/*------------------------------------------------------------------------------
 * MODULE: qdss_qdss_apb
 *------------------------------------------------------------------------------*/

#define QDSS_QDSS_APB_BASE                                                                       0xfc300000
#define QDSS_QDSS_APB_BASE_SIZE                                                                  0x00000000
#define QDSS_QDSS_APB_BASE_PHYS                                                                  0xfc300000

/*------------------------------------------------------------------------------
 * MODULE: security_control
 *------------------------------------------------------------------------------*/

#define SECURITY_CONTROL_BASE                                                                    0xfc4b8000
#define SECURITY_CONTROL_BASE_SIZE                                                               0x00000000
#define SECURITY_CONTROL_BASE_PHYS                                                               0xfc4b8000

/*------------------------------------------------------------------------------
 * MODULE: spdm_wrapper_top
 *------------------------------------------------------------------------------*/

#define SPDM_WRAPPER_TOP_BASE                                                                    0xfc4b0000
#define SPDM_WRAPPER_TOP_BASE_SIZE                                                               0x00000000
#define SPDM_WRAPPER_TOP_BASE_PHYS                                                               0xfc4b0000

/*------------------------------------------------------------------------------
 * MODULE: tlmm
 *------------------------------------------------------------------------------*/

#define TLMM_BASE                                                                                0xfd500000
#define TLMM_BASE_SIZE                                                                           0x00000000
#define TLMM_BASE_PHYS                                                                           0xfd500000

/*------------------------------------------------------------------------------
 * MODULE: usb30_wrapper
 *------------------------------------------------------------------------------*/

#define USB30_WRAPPER_BASE                                                                       0xf9200000
#define USB30_WRAPPER_BASE_SIZE                                                                  0x00000000
#define USB30_WRAPPER_BASE_PHYS                                                                  0xf9200000

/*------------------------------------------------------------------------------
 * MODULE: ipa_wrapper
 *------------------------------------------------------------------------------*/

#define IPA_WRAPPER_BASE                                                                         0xfd4c0000
#define IPA_WRAPPER_BASE_SIZE                                                                    0x00000000
#define IPA_WRAPPER_BASE_PHYS                                                                    0xfd4c0000

/*------------------------------------------------------------------------------
 * MODULE: cnoc_0_bus_timeout
 *------------------------------------------------------------------------------*/

#define CNOC_0_BUS_TIMEOUT_BASE                                                                  0xfc448000
#define CNOC_0_BUS_TIMEOUT_BASE_SIZE                                                             0x00000000
#define CNOC_0_BUS_TIMEOUT_BASE_PHYS                                                             0xfc448000

/*------------------------------------------------------------------------------
 * MODULE: cnoc_1_bus_timeout
 *------------------------------------------------------------------------------*/

#define CNOC_1_BUS_TIMEOUT_BASE                                                                  0xfc449000
#define CNOC_1_BUS_TIMEOUT_BASE_SIZE                                                             0x00000000
#define CNOC_1_BUS_TIMEOUT_BASE_PHYS                                                             0xfc449000

/*------------------------------------------------------------------------------
 * MODULE: cnoc_2_bus_timeout
 *------------------------------------------------------------------------------*/

#define CNOC_2_BUS_TIMEOUT_BASE                                                                  0xfc44a000
#define CNOC_2_BUS_TIMEOUT_BASE_SIZE                                                             0x00000000
#define CNOC_2_BUS_TIMEOUT_BASE_PHYS                                                             0xfc44a000

/*------------------------------------------------------------------------------
 * MODULE: cnoc_3_bus_timeout
 *------------------------------------------------------------------------------*/

#define CNOC_3_BUS_TIMEOUT_BASE                                                                  0xfc44b000
#define CNOC_3_BUS_TIMEOUT_BASE_SIZE                                                             0x00000000
#define CNOC_3_BUS_TIMEOUT_BASE_PHYS                                                             0xfc44b000

/*------------------------------------------------------------------------------
 * MODULE: cnoc_4_bus_timeout
 *------------------------------------------------------------------------------*/

#define CNOC_4_BUS_TIMEOUT_BASE                                                                  0xfc44c000
#define CNOC_4_BUS_TIMEOUT_BASE_SIZE                                                             0x00000000
#define CNOC_4_BUS_TIMEOUT_BASE_PHYS                                                             0xfc44c000

/*------------------------------------------------------------------------------
 * MODULE: cnoc_5_bus_timeout
 *------------------------------------------------------------------------------*/

#define CNOC_5_BUS_TIMEOUT_BASE                                                                  0xfc44d000
#define CNOC_5_BUS_TIMEOUT_BASE_SIZE                                                             0x00000000
#define CNOC_5_BUS_TIMEOUT_BASE_PHYS                                                             0xfc44d000

/*------------------------------------------------------------------------------
 * MODULE: cnoc_6_bus_timeout
 *------------------------------------------------------------------------------*/

#define CNOC_6_BUS_TIMEOUT_BASE                                                                  0xfc44e000
#define CNOC_6_BUS_TIMEOUT_BASE_SIZE                                                             0x00000000
#define CNOC_6_BUS_TIMEOUT_BASE_PHYS                                                             0xfc44e000

/*------------------------------------------------------------------------------
 * MODULE: snoc_0_bus_timeout
 *------------------------------------------------------------------------------*/

#define SNOC_0_BUS_TIMEOUT_BASE                                                                  0xfea00000
#define SNOC_0_BUS_TIMEOUT_BASE_SIZE                                                             0x00000000
#define SNOC_0_BUS_TIMEOUT_BASE_PHYS                                                             0xfea00000

/*------------------------------------------------------------------------------
 * MODULE: snoc_1_bus_timeout
 *------------------------------------------------------------------------------*/

#define SNOC_1_BUS_TIMEOUT_BASE                                                                  0xfea01000
#define SNOC_1_BUS_TIMEOUT_BASE_SIZE                                                             0x00000000
#define SNOC_1_BUS_TIMEOUT_BASE_PHYS                                                             0xfea01000

/*------------------------------------------------------------------------------
 * MODULE: config_noc
 *------------------------------------------------------------------------------*/

#define CONFIG_NOC_BASE                                                                          0xfc480000
#define CONFIG_NOC_BASE_SIZE                                                                     0x00000000
#define CONFIG_NOC_BASE_PHYS                                                                     0xfc480000

/*------------------------------------------------------------------------------
 * MODULE: periph_noc
 *------------------------------------------------------------------------------*/

#define PERIPH_NOC_BASE                                                                          0xfc468000
#define PERIPH_NOC_BASE_SIZE                                                                     0x00000000
#define PERIPH_NOC_BASE_PHYS                                                                     0xfc468000

/*------------------------------------------------------------------------------
 * MODULE: rbcpr_wrapper
 *------------------------------------------------------------------------------*/

#define RBCPR_WRAPPER_BASE                                                                       0xfc44f000
#define RBCPR_WRAPPER_BASE_SIZE                                                                  0x00000000
#define RBCPR_WRAPPER_BASE_PHYS                                                                  0xfc44f000

/*------------------------------------------------------------------------------
 * MODULE: system_noc
 *------------------------------------------------------------------------------*/

#define SYSTEM_NOC_BASE                                                                          0xfc460000
#define SYSTEM_NOC_BASE_SIZE                                                                     0x00000000
#define SYSTEM_NOC_BASE_PHYS                                                                     0xfc460000

/*------------------------------------------------------------------------------
 * MODULE: xpu_cfg_qdss_apu1132_2
 *------------------------------------------------------------------------------*/

#define XPU_CFG_QDSS_APU1132_2_BASE                                                              0xfc48d000
#define XPU_CFG_QDSS_APU1132_2_BASE_SIZE                                                         0x00000000
#define XPU_CFG_QDSS_APU1132_2_BASE_PHYS                                                         0xfc48d000

/*------------------------------------------------------------------------------
 * MODULE: xpu_cfg_ddr_phy_cfg_apu1132_2
 *------------------------------------------------------------------------------*/

#define XPU_CFG_DDR_PHY_CFG_APU1132_2_BASE                                                       0xfc501000
#define XPU_CFG_DDR_PHY_CFG_APU1132_2_BASE_SIZE                                                  0x00000000
#define XPU_CFG_DDR_PHY_CFG_APU1132_2_BASE_PHYS                                                  0xfc501000

/*------------------------------------------------------------------------------
 * MODULE: xpu_cfg_snoc_cfg_mpu1132_16_m15l7_ahb
 *------------------------------------------------------------------------------*/

#define XPU_CFG_SNOC_CFG_MPU1132_16_M15L7_AHB_BASE                                               0xfc48c000
#define XPU_CFG_SNOC_CFG_MPU1132_16_M15L7_AHB_BASE_SIZE                                          0x00000000
#define XPU_CFG_SNOC_CFG_MPU1132_16_M15L7_AHB_BASE_PHYS                                          0xfc48c000

#endif /* __MSMHWIOBASE_H__ */

