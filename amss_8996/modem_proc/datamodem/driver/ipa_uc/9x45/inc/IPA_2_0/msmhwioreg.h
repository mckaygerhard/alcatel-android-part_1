#ifndef __MSMHWIOREG_H__
#define __MSMHWIOREG_H__
/* ===========================================================================

              H W I O   I N T E R F A C E   D E F I N I T I O N S

                     *** A U T O G E N E R A T E D ***

==============================================================================

 ADDRESS FILE VERSION: (unknown)

 Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved

==============================================================================
 $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/driver/ipa_uc/9x45/inc/IPA_2_0/msmhwioreg.h#1 $
=========================================================================== */

#include "msmhwiobase.h"

/*------------------------------------------------------------------------------
 * MODULE: bam_ndp
 *------------------------------------------------------------------------------*/

#define BAM_NDP_REG_BASE                                                (IPA_WRAPPER_BASE + 0x00000000)
#define BAM_NDP_REG_BASE_PHYS                                           0x00000000

#define HWIO_BAM_CTRL_ADDR                                              (BAM_NDP_REG_BASE      + 0x00004000)
#define HWIO_BAM_CTRL_PHYS                                              (BAM_NDP_REG_BASE_PHYS + 0x00004000)
#define HWIO_BAM_CTRL_RMSK                                                0x1feff3
#define HWIO_BAM_CTRL_SHFT                                                       0
#define HWIO_BAM_CTRL_IN                                                \
        in_dword_masked(HWIO_BAM_CTRL_ADDR, HWIO_BAM_CTRL_RMSK)
#define HWIO_BAM_CTRL_INM(m)                                            \
        in_dword_masked(HWIO_BAM_CTRL_ADDR, m)
#define HWIO_BAM_CTRL_OUT(v)                                            \
        out_dword(HWIO_BAM_CTRL_ADDR,v)
#define HWIO_BAM_CTRL_OUTM(m,v)                                         \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_CTRL_ADDR,m,v,HWIO_BAM_CTRL_IN); \
        HWIO_INTFREE()
#define HWIO_BAM_CTRL_BAM_MESS_ONLY_CANCEL_WB_BMSK                        0x100000
#define HWIO_BAM_CTRL_BAM_MESS_ONLY_CANCEL_WB_SHFT                            0x14
#define HWIO_BAM_CTRL_CACHE_MISS_ERR_RESP_EN_BMSK                          0x80000
#define HWIO_BAM_CTRL_CACHE_MISS_ERR_RESP_EN_SHFT                             0x13
#define HWIO_BAM_CTRL_LOCAL_CLK_GATING_BMSK                                0x60000
#define HWIO_BAM_CTRL_LOCAL_CLK_GATING_SHFT                                   0x11
#define HWIO_BAM_CTRL_IBC_DISABLE_BMSK                                     0x10000
#define HWIO_BAM_CTRL_IBC_DISABLE_SHFT                                        0x10
#define HWIO_BAM_CTRL_BAM_CACHED_DESC_STORE_BMSK                            0x8000
#define HWIO_BAM_CTRL_BAM_CACHED_DESC_STORE_SHFT                               0xf
#define HWIO_BAM_CTRL_BAM_DESC_CACHE_SEL_BMSK                               0x6000
#define HWIO_BAM_CTRL_BAM_DESC_CACHE_SEL_SHFT                                  0xd
#define HWIO_BAM_CTRL_BAM_TESTBUS_SEL_BMSK                                   0xfe0
#define HWIO_BAM_CTRL_BAM_TESTBUS_SEL_SHFT                                     0x5
#define HWIO_BAM_CTRL_BAM_EN_ACCUM_BMSK                                       0x10
#define HWIO_BAM_CTRL_BAM_EN_ACCUM_SHFT                                        0x4
#define HWIO_BAM_CTRL_BAM_EN_BMSK                                              0x2
#define HWIO_BAM_CTRL_BAM_EN_SHFT                                              0x1
#define HWIO_BAM_CTRL_BAM_SW_RST_BMSK                                          0x1
#define HWIO_BAM_CTRL_BAM_SW_RST_SHFT                                            0

#define HWIO_BAM_REVISION_ADDR                                          (BAM_NDP_REG_BASE      + 0x00004004)
#define HWIO_BAM_REVISION_PHYS                                          (BAM_NDP_REG_BASE_PHYS + 0x00004004)
#define HWIO_BAM_REVISION_RMSK                                          0xffffffff
#define HWIO_BAM_REVISION_SHFT                                                   0
#define HWIO_BAM_REVISION_IN                                            \
        in_dword_masked(HWIO_BAM_REVISION_ADDR, HWIO_BAM_REVISION_RMSK)
#define HWIO_BAM_REVISION_INM(m)                                        \
        in_dword_masked(HWIO_BAM_REVISION_ADDR, m)
#define HWIO_BAM_REVISION_INACTIV_TMR_BASE_BMSK                         0xff000000
#define HWIO_BAM_REVISION_INACTIV_TMR_BASE_SHFT                               0x18
#define HWIO_BAM_REVISION_CMD_DESC_EN_BMSK                                0x800000
#define HWIO_BAM_REVISION_CMD_DESC_EN_SHFT                                    0x17
#define HWIO_BAM_REVISION_DESC_CACHE_DEPTH_BMSK                           0x600000
#define HWIO_BAM_REVISION_DESC_CACHE_DEPTH_SHFT                               0x15
#define HWIO_BAM_REVISION_NUM_INACTIV_TMRS_BMSK                           0x100000
#define HWIO_BAM_REVISION_NUM_INACTIV_TMRS_SHFT                               0x14
#define HWIO_BAM_REVISION_INACTIV_TMRS_EXST_BMSK                           0x80000
#define HWIO_BAM_REVISION_INACTIV_TMRS_EXST_SHFT                              0x13
#define HWIO_BAM_REVISION_HIGH_FREQUENCY_BAM_BMSK                          0x40000
#define HWIO_BAM_REVISION_HIGH_FREQUENCY_BAM_SHFT                             0x12
#define HWIO_BAM_REVISION_BAM_HAS_NO_BYPASS_BMSK                           0x20000
#define HWIO_BAM_REVISION_BAM_HAS_NO_BYPASS_SHFT                              0x11
#define HWIO_BAM_REVISION_SECURED_BMSK                                     0x10000
#define HWIO_BAM_REVISION_SECURED_SHFT                                        0x10
#define HWIO_BAM_REVISION_USE_VMIDMT_BMSK                                   0x8000
#define HWIO_BAM_REVISION_USE_VMIDMT_SHFT                                      0xf
#define HWIO_BAM_REVISION_AXI_ACTIVE_BMSK                                   0x4000
#define HWIO_BAM_REVISION_AXI_ACTIVE_SHFT                                      0xe
#define HWIO_BAM_REVISION_CE_BUFFER_SIZE_BMSK                               0x3000
#define HWIO_BAM_REVISION_CE_BUFFER_SIZE_SHFT                                  0xc
#define HWIO_BAM_REVISION_NUM_EES_BMSK                                       0xf00
#define HWIO_BAM_REVISION_NUM_EES_SHFT                                         0x8
#define HWIO_BAM_REVISION_REVISION_BMSK                                       0xff
#define HWIO_BAM_REVISION_REVISION_SHFT                                          0

#define HWIO_BAM_SW_VERSION_ADDR                                        (BAM_NDP_REG_BASE      + 0x00004080)
#define HWIO_BAM_SW_VERSION_PHYS                                        (BAM_NDP_REG_BASE_PHYS + 0x00004080)
#define HWIO_BAM_SW_VERSION_RMSK                                        0xffffffff
#define HWIO_BAM_SW_VERSION_SHFT                                                 0
#define HWIO_BAM_SW_VERSION_IN                                          \
        in_dword_masked(HWIO_BAM_SW_VERSION_ADDR, HWIO_BAM_SW_VERSION_RMSK)
#define HWIO_BAM_SW_VERSION_INM(m)                                      \
        in_dword_masked(HWIO_BAM_SW_VERSION_ADDR, m)
#define HWIO_BAM_SW_VERSION_MAJOR_BMSK                                  0xf0000000
#define HWIO_BAM_SW_VERSION_MAJOR_SHFT                                        0x1c
#define HWIO_BAM_SW_VERSION_MINOR_BMSK                                   0xfff0000
#define HWIO_BAM_SW_VERSION_MINOR_SHFT                                        0x10
#define HWIO_BAM_SW_VERSION_STEP_BMSK                                       0xffff
#define HWIO_BAM_SW_VERSION_STEP_SHFT                                            0

#define HWIO_BAM_NUM_PIPES_ADDR                                         (BAM_NDP_REG_BASE      + 0x0000403c)
#define HWIO_BAM_NUM_PIPES_PHYS                                         (BAM_NDP_REG_BASE_PHYS + 0x0000403c)
#define HWIO_BAM_NUM_PIPES_RMSK                                         0xffffc0ff
#define HWIO_BAM_NUM_PIPES_SHFT                                                  0
#define HWIO_BAM_NUM_PIPES_IN                                           \
        in_dword_masked(HWIO_BAM_NUM_PIPES_ADDR, HWIO_BAM_NUM_PIPES_RMSK)
#define HWIO_BAM_NUM_PIPES_INM(m)                                       \
        in_dword_masked(HWIO_BAM_NUM_PIPES_ADDR, m)
#define HWIO_BAM_NUM_PIPES_BAM_NON_PIPE_GRP_BMSK                        0xff000000
#define HWIO_BAM_NUM_PIPES_BAM_NON_PIPE_GRP_SHFT                              0x18
#define HWIO_BAM_NUM_PIPES_PERIPH_NON_PIPE_GRP_BMSK                       0xff0000
#define HWIO_BAM_NUM_PIPES_PERIPH_NON_PIPE_GRP_SHFT                           0x10
#define HWIO_BAM_NUM_PIPES_BAM_DATA_ADDR_BUS_WIDTH_BMSK                     0xc000
#define HWIO_BAM_NUM_PIPES_BAM_DATA_ADDR_BUS_WIDTH_SHFT                        0xe
#define HWIO_BAM_NUM_PIPES_BAM_NUM_PIPES_BMSK                                 0xff
#define HWIO_BAM_NUM_PIPES_BAM_NUM_PIPES_SHFT                                    0

#define HWIO_BAM_TIMER_ADDR                                             (BAM_NDP_REG_BASE      + 0x00004040)
#define HWIO_BAM_TIMER_PHYS                                             (BAM_NDP_REG_BASE_PHYS + 0x00004040)
#define HWIO_BAM_TIMER_RMSK                                                 0xffff
#define HWIO_BAM_TIMER_SHFT                                                      0
#define HWIO_BAM_TIMER_IN                                               \
        in_dword_masked(HWIO_BAM_TIMER_ADDR, HWIO_BAM_TIMER_RMSK)
#define HWIO_BAM_TIMER_INM(m)                                           \
        in_dword_masked(HWIO_BAM_TIMER_ADDR, m)
#define HWIO_BAM_TIMER_TIMER_BMSK                                           0xffff
#define HWIO_BAM_TIMER_TIMER_SHFT                                                0

#define HWIO_BAM_TIMER_CTRL_ADDR                                        (BAM_NDP_REG_BASE      + 0x00004044)
#define HWIO_BAM_TIMER_CTRL_PHYS                                        (BAM_NDP_REG_BASE_PHYS + 0x00004044)
#define HWIO_BAM_TIMER_CTRL_RMSK                                        0xe000ffff
#define HWIO_BAM_TIMER_CTRL_SHFT                                                 0
#define HWIO_BAM_TIMER_CTRL_IN                                          \
        in_dword_masked(HWIO_BAM_TIMER_CTRL_ADDR, HWIO_BAM_TIMER_CTRL_RMSK)
#define HWIO_BAM_TIMER_CTRL_INM(m)                                      \
        in_dword_masked(HWIO_BAM_TIMER_CTRL_ADDR, m)
#define HWIO_BAM_TIMER_CTRL_OUT(v)                                      \
        out_dword(HWIO_BAM_TIMER_CTRL_ADDR,v)
#define HWIO_BAM_TIMER_CTRL_OUTM(m,v)                                   \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_TIMER_CTRL_ADDR,m,v,HWIO_BAM_TIMER_CTRL_IN); \
        HWIO_INTFREE()
#define HWIO_BAM_TIMER_CTRL_TIMER_RST_BMSK                              0x80000000
#define HWIO_BAM_TIMER_CTRL_TIMER_RST_SHFT                                    0x1f
#define HWIO_BAM_TIMER_CTRL_TIMER_RUN_BMSK                              0x40000000
#define HWIO_BAM_TIMER_CTRL_TIMER_RUN_SHFT                                    0x1e
#define HWIO_BAM_TIMER_CTRL_TIMER_MODE_BMSK                             0x20000000
#define HWIO_BAM_TIMER_CTRL_TIMER_MODE_SHFT                                   0x1d
#define HWIO_BAM_TIMER_CTRL_TIMER_TRSHLD_BMSK                               0xffff
#define HWIO_BAM_TIMER_CTRL_TIMER_TRSHLD_SHFT                                    0

#define HWIO_BAM_DESC_CNT_TRSHLD_ADDR                                   (BAM_NDP_REG_BASE      + 0x00004008)
#define HWIO_BAM_DESC_CNT_TRSHLD_PHYS                                   (BAM_NDP_REG_BASE_PHYS + 0x00004008)
#define HWIO_BAM_DESC_CNT_TRSHLD_RMSK                                       0xffff
#define HWIO_BAM_DESC_CNT_TRSHLD_SHFT                                            0
#define HWIO_BAM_DESC_CNT_TRSHLD_IN                                     \
        in_dword_masked(HWIO_BAM_DESC_CNT_TRSHLD_ADDR, HWIO_BAM_DESC_CNT_TRSHLD_RMSK)
#define HWIO_BAM_DESC_CNT_TRSHLD_INM(m)                                 \
        in_dword_masked(HWIO_BAM_DESC_CNT_TRSHLD_ADDR, m)
#define HWIO_BAM_DESC_CNT_TRSHLD_OUT(v)                                 \
        out_dword(HWIO_BAM_DESC_CNT_TRSHLD_ADDR,v)
#define HWIO_BAM_DESC_CNT_TRSHLD_OUTM(m,v)                              \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_DESC_CNT_TRSHLD_ADDR,m,v,HWIO_BAM_DESC_CNT_TRSHLD_IN); \
        HWIO_INTFREE()
#define HWIO_BAM_DESC_CNT_TRSHLD_CNT_TRSHLD_BMSK                            0xffff
#define HWIO_BAM_DESC_CNT_TRSHLD_CNT_TRSHLD_SHFT                                 0

#define HWIO_BAM_IRQ_SRCS_ADDR                                          (BAM_NDP_REG_BASE      + 0x0000400c)
#define HWIO_BAM_IRQ_SRCS_PHYS                                          (BAM_NDP_REG_BASE_PHYS + 0x0000400c)
#define HWIO_BAM_IRQ_SRCS_RMSK                                          0xffffffff
#define HWIO_BAM_IRQ_SRCS_SHFT                                                   0
#define HWIO_BAM_IRQ_SRCS_IN                                            \
        in_dword_masked(HWIO_BAM_IRQ_SRCS_ADDR, HWIO_BAM_IRQ_SRCS_RMSK)
#define HWIO_BAM_IRQ_SRCS_INM(m)                                        \
        in_dword_masked(HWIO_BAM_IRQ_SRCS_ADDR, m)
#define HWIO_BAM_IRQ_SRCS_BAM_IRQ_BMSK                                  0x80000000
#define HWIO_BAM_IRQ_SRCS_BAM_IRQ_SHFT                                        0x1f
#define HWIO_BAM_IRQ_SRCS_P_IRQ_BMSK                                    0x7fffffff
#define HWIO_BAM_IRQ_SRCS_P_IRQ_SHFT                                             0

#define HWIO_BAM_IRQ_SRCS_MSK_ADDR                                      (BAM_NDP_REG_BASE      + 0x00004010)
#define HWIO_BAM_IRQ_SRCS_MSK_PHYS                                      (BAM_NDP_REG_BASE_PHYS + 0x00004010)
#define HWIO_BAM_IRQ_SRCS_MSK_RMSK                                      0xffffffff
#define HWIO_BAM_IRQ_SRCS_MSK_SHFT                                               0
#define HWIO_BAM_IRQ_SRCS_MSK_IN                                        \
        in_dword_masked(HWIO_BAM_IRQ_SRCS_MSK_ADDR, HWIO_BAM_IRQ_SRCS_MSK_RMSK)
#define HWIO_BAM_IRQ_SRCS_MSK_INM(m)                                    \
        in_dword_masked(HWIO_BAM_IRQ_SRCS_MSK_ADDR, m)
#define HWIO_BAM_IRQ_SRCS_MSK_OUT(v)                                    \
        out_dword(HWIO_BAM_IRQ_SRCS_MSK_ADDR,v)
#define HWIO_BAM_IRQ_SRCS_MSK_OUTM(m,v)                                 \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_IRQ_SRCS_MSK_ADDR,m,v,HWIO_BAM_IRQ_SRCS_MSK_IN); \
        HWIO_INTFREE()
#define HWIO_BAM_IRQ_SRCS_MSK_BAM_IRQ_MSK_BMSK                          0x80000000
#define HWIO_BAM_IRQ_SRCS_MSK_BAM_IRQ_MSK_SHFT                                0x1f
#define HWIO_BAM_IRQ_SRCS_MSK_P_IRQ_MSK_BMSK                            0x7fffffff
#define HWIO_BAM_IRQ_SRCS_MSK_P_IRQ_MSK_SHFT                                     0

#define HWIO_BAM_IRQ_SRCS_UNMASKED_ADDR                                 (BAM_NDP_REG_BASE      + 0x00004030)
#define HWIO_BAM_IRQ_SRCS_UNMASKED_PHYS                                 (BAM_NDP_REG_BASE_PHYS + 0x00004030)
#define HWIO_BAM_IRQ_SRCS_UNMASKED_RMSK                                 0xffffffff
#define HWIO_BAM_IRQ_SRCS_UNMASKED_SHFT                                          0
#define HWIO_BAM_IRQ_SRCS_UNMASKED_IN                                   \
        in_dword_masked(HWIO_BAM_IRQ_SRCS_UNMASKED_ADDR, HWIO_BAM_IRQ_SRCS_UNMASKED_RMSK)
#define HWIO_BAM_IRQ_SRCS_UNMASKED_INM(m)                               \
        in_dword_masked(HWIO_BAM_IRQ_SRCS_UNMASKED_ADDR, m)
#define HWIO_BAM_IRQ_SRCS_UNMASKED_BAM_IRQ_UNMASKED_BMSK                0x80000000
#define HWIO_BAM_IRQ_SRCS_UNMASKED_BAM_IRQ_UNMASKED_SHFT                      0x1f
#define HWIO_BAM_IRQ_SRCS_UNMASKED_P_IRQ_UNMASKED_BMSK                  0x7fffffff
#define HWIO_BAM_IRQ_SRCS_UNMASKED_P_IRQ_UNMASKED_SHFT                           0

#define HWIO_BAM_IRQ_STTS_ADDR                                          (BAM_NDP_REG_BASE      + 0x00004014)
#define HWIO_BAM_IRQ_STTS_PHYS                                          (BAM_NDP_REG_BASE_PHYS + 0x00004014)
#define HWIO_BAM_IRQ_STTS_RMSK                                                0x1e
#define HWIO_BAM_IRQ_STTS_SHFT                                                   0
#define HWIO_BAM_IRQ_STTS_IN                                            \
        in_dword_masked(HWIO_BAM_IRQ_STTS_ADDR, HWIO_BAM_IRQ_STTS_RMSK)
#define HWIO_BAM_IRQ_STTS_INM(m)                                        \
        in_dword_masked(HWIO_BAM_IRQ_STTS_ADDR, m)
#define HWIO_BAM_IRQ_STTS_BAM_TIMER_IRQ_BMSK                                  0x10
#define HWIO_BAM_IRQ_STTS_BAM_TIMER_IRQ_SHFT                                   0x4
#define HWIO_BAM_IRQ_STTS_BAM_EMPTY_IRQ_BMSK                                   0x8
#define HWIO_BAM_IRQ_STTS_BAM_EMPTY_IRQ_SHFT                                   0x3
#define HWIO_BAM_IRQ_STTS_BAM_ERROR_IRQ_BMSK                                   0x4
#define HWIO_BAM_IRQ_STTS_BAM_ERROR_IRQ_SHFT                                   0x2
#define HWIO_BAM_IRQ_STTS_BAM_HRESP_ERR_IRQ_BMSK                               0x2
#define HWIO_BAM_IRQ_STTS_BAM_HRESP_ERR_IRQ_SHFT                               0x1

#define HWIO_BAM_IRQ_CLR_ADDR                                           (BAM_NDP_REG_BASE      + 0x00004018)
#define HWIO_BAM_IRQ_CLR_PHYS                                           (BAM_NDP_REG_BASE_PHYS + 0x00004018)
#define HWIO_BAM_IRQ_CLR_RMSK                                                 0x1e
#define HWIO_BAM_IRQ_CLR_SHFT                                                    0
#define HWIO_BAM_IRQ_CLR_OUT(v)                                         \
        out_dword(HWIO_BAM_IRQ_CLR_ADDR,v)
#define HWIO_BAM_IRQ_CLR_BAM_TIMER_CLR_BMSK                                   0x10
#define HWIO_BAM_IRQ_CLR_BAM_TIMER_CLR_SHFT                                    0x4
#define HWIO_BAM_IRQ_CLR_BAM_EMPTY_CLR_BMSK                                    0x8
#define HWIO_BAM_IRQ_CLR_BAM_EMPTY_CLR_SHFT                                    0x3
#define HWIO_BAM_IRQ_CLR_BAM_ERROR_CLR_BMSK                                    0x4
#define HWIO_BAM_IRQ_CLR_BAM_ERROR_CLR_SHFT                                    0x2
#define HWIO_BAM_IRQ_CLR_BAM_HRESP_ERR_CLR_BMSK                                0x2
#define HWIO_BAM_IRQ_CLR_BAM_HRESP_ERR_CLR_SHFT                                0x1

#define HWIO_BAM_IRQ_EN_ADDR                                            (BAM_NDP_REG_BASE      + 0x0000401c)
#define HWIO_BAM_IRQ_EN_PHYS                                            (BAM_NDP_REG_BASE_PHYS + 0x0000401c)
#define HWIO_BAM_IRQ_EN_RMSK                                                  0x1e
#define HWIO_BAM_IRQ_EN_SHFT                                                     0
#define HWIO_BAM_IRQ_EN_IN                                              \
        in_dword_masked(HWIO_BAM_IRQ_EN_ADDR, HWIO_BAM_IRQ_EN_RMSK)
#define HWIO_BAM_IRQ_EN_INM(m)                                          \
        in_dword_masked(HWIO_BAM_IRQ_EN_ADDR, m)
#define HWIO_BAM_IRQ_EN_OUT(v)                                          \
        out_dword(HWIO_BAM_IRQ_EN_ADDR,v)
#define HWIO_BAM_IRQ_EN_OUTM(m,v)                                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_IRQ_EN_ADDR,m,v,HWIO_BAM_IRQ_EN_IN); \
        HWIO_INTFREE()
#define HWIO_BAM_IRQ_EN_BAM_TIMER_EN_BMSK                                     0x10
#define HWIO_BAM_IRQ_EN_BAM_TIMER_EN_SHFT                                      0x4
#define HWIO_BAM_IRQ_EN_BAM_EMPTY_EN_BMSK                                      0x8
#define HWIO_BAM_IRQ_EN_BAM_EMPTY_EN_SHFT                                      0x3
#define HWIO_BAM_IRQ_EN_BAM_ERROR_EN_BMSK                                      0x4
#define HWIO_BAM_IRQ_EN_BAM_ERROR_EN_SHFT                                      0x2
#define HWIO_BAM_IRQ_EN_BAM_HRESP_ERR_EN_BMSK                                  0x2
#define HWIO_BAM_IRQ_EN_BAM_HRESP_ERR_EN_SHFT                                  0x1

#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_ADDR                              (BAM_NDP_REG_BASE      + 0x00004024)
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_PHYS                              (BAM_NDP_REG_BASE_PHYS + 0x00004024)
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_RMSK                                0x7fffff
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_SHFT                                       0
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_IN                                \
        in_dword_masked(HWIO_BAM_AHB_MASTER_ERR_CTRLS_ADDR, HWIO_BAM_AHB_MASTER_ERR_CTRLS_RMSK)
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_INM(m)                            \
        in_dword_masked(HWIO_BAM_AHB_MASTER_ERR_CTRLS_ADDR, m)
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HVMID_BMSK                  0x7c0000
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HVMID_SHFT                      0x12
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_DIRECT_MODE_BMSK             0x20000
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_DIRECT_MODE_SHFT                0x11
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HCID_BMSK                    0x1f000
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HCID_SHFT                        0xc
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HPROT_BMSK                     0xf00
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HPROT_SHFT                       0x8
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HBURST_BMSK                     0xe0
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HBURST_SHFT                      0x5
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HSIZE_BMSK                      0x18
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HSIZE_SHFT                       0x3
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HWRITE_BMSK                      0x4
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HWRITE_SHFT                      0x2
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HTRANS_BMSK                      0x3
#define HWIO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HTRANS_SHFT                        0

#define HWIO_BAM_AHB_MASTER_ERR_ADDR_ADDR                               (BAM_NDP_REG_BASE      + 0x00004028)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_PHYS                               (BAM_NDP_REG_BASE_PHYS + 0x00004028)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_RMSK                               0xffffffff
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_SHFT                                        0
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_IN                                 \
        in_dword_masked(HWIO_BAM_AHB_MASTER_ERR_ADDR_ADDR, HWIO_BAM_AHB_MASTER_ERR_ADDR_RMSK)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_INM(m)                             \
        in_dword_masked(HWIO_BAM_AHB_MASTER_ERR_ADDR_ADDR, m)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_BAM_ERR_ADDR_BMSK                  0xffffffff
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_BAM_ERR_ADDR_SHFT                           0

#define HWIO_BAM_AHB_MASTER_ERR_ADDR_LSB_ADDR                           (BAM_NDP_REG_BASE      + 0x00004100)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_LSB_PHYS                           (BAM_NDP_REG_BASE_PHYS + 0x00004100)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_LSB_RMSK                           0xffffffff
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_LSB_SHFT                                    0
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_LSB_IN                             \
        in_dword_masked(HWIO_BAM_AHB_MASTER_ERR_ADDR_LSB_ADDR, HWIO_BAM_AHB_MASTER_ERR_ADDR_LSB_RMSK)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_LSB_INM(m)                         \
        in_dword_masked(HWIO_BAM_AHB_MASTER_ERR_ADDR_LSB_ADDR, m)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_LSB_BAM_ERR_ADDR_BMSK              0xffffffff
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_LSB_BAM_ERR_ADDR_SHFT                       0

#define HWIO_BAM_AHB_MASTER_ERR_ADDR_MSB_ADDR                           (BAM_NDP_REG_BASE      + 0x00004104)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_MSB_PHYS                           (BAM_NDP_REG_BASE_PHYS + 0x00004104)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_MSB_RMSK                                  0xf
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_MSB_SHFT                                    0
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_MSB_IN                             \
        in_dword_masked(HWIO_BAM_AHB_MASTER_ERR_ADDR_MSB_ADDR, HWIO_BAM_AHB_MASTER_ERR_ADDR_MSB_RMSK)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_MSB_INM(m)                         \
        in_dword_masked(HWIO_BAM_AHB_MASTER_ERR_ADDR_MSB_ADDR, m)
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_MSB_BAM_ERR_ADDR_BMSK                     0xf
#define HWIO_BAM_AHB_MASTER_ERR_ADDR_MSB_BAM_ERR_ADDR_SHFT                       0

#define HWIO_BAM_AHB_MASTER_ERR_DATA_ADDR                               (BAM_NDP_REG_BASE      + 0x0000402c)
#define HWIO_BAM_AHB_MASTER_ERR_DATA_PHYS                               (BAM_NDP_REG_BASE_PHYS + 0x0000402c)
#define HWIO_BAM_AHB_MASTER_ERR_DATA_RMSK                               0xffffffff
#define HWIO_BAM_AHB_MASTER_ERR_DATA_SHFT                                        0
#define HWIO_BAM_AHB_MASTER_ERR_DATA_IN                                 \
        in_dword_masked(HWIO_BAM_AHB_MASTER_ERR_DATA_ADDR, HWIO_BAM_AHB_MASTER_ERR_DATA_RMSK)
#define HWIO_BAM_AHB_MASTER_ERR_DATA_INM(m)                             \
        in_dword_masked(HWIO_BAM_AHB_MASTER_ERR_DATA_ADDR, m)
#define HWIO_BAM_AHB_MASTER_ERR_DATA_BAM_ERR_DATA_BMSK                  0xffffffff
#define HWIO_BAM_AHB_MASTER_ERR_DATA_BAM_ERR_DATA_SHFT                           0

#define HWIO_BAM_TRUST_REG_ADDR                                         (BAM_NDP_REG_BASE      + 0x00004070)
#define HWIO_BAM_TRUST_REG_PHYS                                         (BAM_NDP_REG_BASE_PHYS + 0x00004070)
#define HWIO_BAM_TRUST_REG_RMSK                                             0x3f87
#define HWIO_BAM_TRUST_REG_SHFT                                                  0
#define HWIO_BAM_TRUST_REG_IN                                           \
        in_dword_masked(HWIO_BAM_TRUST_REG_ADDR, HWIO_BAM_TRUST_REG_RMSK)
#define HWIO_BAM_TRUST_REG_INM(m)                                       \
        in_dword_masked(HWIO_BAM_TRUST_REG_ADDR, m)
#define HWIO_BAM_TRUST_REG_OUT(v)                                       \
        out_dword(HWIO_BAM_TRUST_REG_ADDR,v)
#define HWIO_BAM_TRUST_REG_OUTM(m,v)                                    \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_TRUST_REG_ADDR,m,v,HWIO_BAM_TRUST_REG_IN); \
        HWIO_INTFREE()
#define HWIO_BAM_TRUST_REG_LOCK_EE_CTRL_BMSK                                0x2000
#define HWIO_BAM_TRUST_REG_LOCK_EE_CTRL_SHFT                                   0xd
#define HWIO_BAM_TRUST_REG_BAM_VMID_BMSK                                    0x1f00
#define HWIO_BAM_TRUST_REG_BAM_VMID_SHFT                                       0x8
#define HWIO_BAM_TRUST_REG_BAM_RST_BLOCK_BMSK                                 0x80
#define HWIO_BAM_TRUST_REG_BAM_RST_BLOCK_SHFT                                  0x7
#define HWIO_BAM_TRUST_REG_BAM_EE_BMSK                                         0x7
#define HWIO_BAM_TRUST_REG_BAM_EE_SHFT                                           0

#define HWIO_BAM_TEST_BUS_SEL_ADDR                                      (BAM_NDP_REG_BASE      + 0x00004074)
#define HWIO_BAM_TEST_BUS_SEL_PHYS                                      (BAM_NDP_REG_BASE_PHYS + 0x00004074)
#define HWIO_BAM_TEST_BUS_SEL_RMSK                                        0x3f007f
#define HWIO_BAM_TEST_BUS_SEL_SHFT                                               0
#define HWIO_BAM_TEST_BUS_SEL_IN                                        \
        in_dword_masked(HWIO_BAM_TEST_BUS_SEL_ADDR, HWIO_BAM_TEST_BUS_SEL_RMSK)
#define HWIO_BAM_TEST_BUS_SEL_INM(m)                                    \
        in_dword_masked(HWIO_BAM_TEST_BUS_SEL_ADDR, m)
#define HWIO_BAM_TEST_BUS_SEL_OUT(v)                                    \
        out_dword(HWIO_BAM_TEST_BUS_SEL_ADDR,v)
#define HWIO_BAM_TEST_BUS_SEL_OUTM(m,v)                                 \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_TEST_BUS_SEL_ADDR,m,v,HWIO_BAM_TEST_BUS_SEL_IN); \
        HWIO_INTFREE()
#define HWIO_BAM_TEST_BUS_SEL_SW_EVENTS_ZERO_BMSK                         0x200000
#define HWIO_BAM_TEST_BUS_SEL_SW_EVENTS_ZERO_SHFT                             0x15
#define HWIO_BAM_TEST_BUS_SEL_SW_EVENTS_SEL_BMSK                          0x180000
#define HWIO_BAM_TEST_BUS_SEL_SW_EVENTS_SEL_SHFT                              0x13
#define HWIO_BAM_TEST_BUS_SEL_BAM_DATA_ERASE_BMSK                          0x40000
#define HWIO_BAM_TEST_BUS_SEL_BAM_DATA_ERASE_SHFT                             0x12
#define HWIO_BAM_TEST_BUS_SEL_BAM_DATA_FLUSH_BMSK                          0x20000
#define HWIO_BAM_TEST_BUS_SEL_BAM_DATA_FLUSH_SHFT                             0x11
#define HWIO_BAM_TEST_BUS_SEL_BAM_CLK_ALWAYS_ON_BMSK                       0x10000
#define HWIO_BAM_TEST_BUS_SEL_BAM_CLK_ALWAYS_ON_SHFT                          0x10
#define HWIO_BAM_TEST_BUS_SEL_BAM_TESTBUS_SEL_BMSK                            0x7f
#define HWIO_BAM_TEST_BUS_SEL_BAM_TESTBUS_SEL_SHFT                               0

#define HWIO_BAM_TEST_BUS_REG_ADDR                                      (BAM_NDP_REG_BASE      + 0x00004078)
#define HWIO_BAM_TEST_BUS_REG_PHYS                                      (BAM_NDP_REG_BASE_PHYS + 0x00004078)
#define HWIO_BAM_TEST_BUS_REG_RMSK                                      0xffffffff
#define HWIO_BAM_TEST_BUS_REG_SHFT                                               0
#define HWIO_BAM_TEST_BUS_REG_IN                                        \
        in_dword_masked(HWIO_BAM_TEST_BUS_REG_ADDR, HWIO_BAM_TEST_BUS_REG_RMSK)
#define HWIO_BAM_TEST_BUS_REG_INM(m)                                    \
        in_dword_masked(HWIO_BAM_TEST_BUS_REG_ADDR, m)
#define HWIO_BAM_TEST_BUS_REG_BAM_TESTBUS_REG_BMSK                      0xffffffff
#define HWIO_BAM_TEST_BUS_REG_BAM_TESTBUS_REG_SHFT                               0

#define HWIO_BAM_CNFG_BITS_ADDR                                         (BAM_NDP_REG_BASE      + 0x0000407c)
#define HWIO_BAM_CNFG_BITS_PHYS                                         (BAM_NDP_REG_BASE_PHYS + 0x0000407c)
#define HWIO_BAM_CNFG_BITS_RMSK                                         0xfffff80f
#define HWIO_BAM_CNFG_BITS_SHFT                                                  0
#define HWIO_BAM_CNFG_BITS_IN                                           \
        in_dword_masked(HWIO_BAM_CNFG_BITS_ADDR, HWIO_BAM_CNFG_BITS_RMSK)
#define HWIO_BAM_CNFG_BITS_INM(m)                                       \
        in_dword_masked(HWIO_BAM_CNFG_BITS_ADDR, m)
#define HWIO_BAM_CNFG_BITS_OUT(v)                                       \
        out_dword(HWIO_BAM_CNFG_BITS_ADDR,v)
#define HWIO_BAM_CNFG_BITS_OUTM(m,v)                                    \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_CNFG_BITS_ADDR,m,v,HWIO_BAM_CNFG_BITS_IN); \
        HWIO_INTFREE()
#define HWIO_BAM_CNFG_BITS_AOS_OVERFLOW_PRVNT_BMSK                      0x80000000
#define HWIO_BAM_CNFG_BITS_AOS_OVERFLOW_PRVNT_SHFT                            0x1f
#define HWIO_BAM_CNFG_BITS_MULTIPLE_EVENTS_DESC_AVAIL_EN_BMSK           0x40000000
#define HWIO_BAM_CNFG_BITS_MULTIPLE_EVENTS_DESC_AVAIL_EN_SHFT                 0x1e
#define HWIO_BAM_CNFG_BITS_MULTIPLE_EVENTS_SIZE_EN_BMSK                 0x20000000
#define HWIO_BAM_CNFG_BITS_MULTIPLE_EVENTS_SIZE_EN_SHFT                       0x1d
#define HWIO_BAM_CNFG_BITS_BAM_ZLT_W_CD_SUPPORT_BMSK                    0x10000000
#define HWIO_BAM_CNFG_BITS_BAM_ZLT_W_CD_SUPPORT_SHFT                          0x1c
#define HWIO_BAM_CNFG_BITS_BAM_CD_ENABLE_BMSK                            0x8000000
#define HWIO_BAM_CNFG_BITS_BAM_CD_ENABLE_SHFT                                 0x1b
#define HWIO_BAM_CNFG_BITS_BAM_AU_ACCUMED_BMSK                           0x4000000
#define HWIO_BAM_CNFG_BITS_BAM_AU_ACCUMED_SHFT                                0x1a
#define HWIO_BAM_CNFG_BITS_BAM_PSM_P_HD_DATA_BMSK                        0x2000000
#define HWIO_BAM_CNFG_BITS_BAM_PSM_P_HD_DATA_SHFT                             0x19
#define HWIO_BAM_CNFG_BITS_BAM_REG_P_EN_BMSK                             0x1000000
#define HWIO_BAM_CNFG_BITS_BAM_REG_P_EN_SHFT                                  0x18
#define HWIO_BAM_CNFG_BITS_BAM_WB_DSC_AVL_P_RST_BMSK                      0x800000
#define HWIO_BAM_CNFG_BITS_BAM_WB_DSC_AVL_P_RST_SHFT                          0x17
#define HWIO_BAM_CNFG_BITS_BAM_WB_RETR_SVPNT_BMSK                         0x400000
#define HWIO_BAM_CNFG_BITS_BAM_WB_RETR_SVPNT_SHFT                             0x16
#define HWIO_BAM_CNFG_BITS_BAM_WB_CSW_ACK_IDL_BMSK                        0x200000
#define HWIO_BAM_CNFG_BITS_BAM_WB_CSW_ACK_IDL_SHFT                            0x15
#define HWIO_BAM_CNFG_BITS_BAM_WB_BLK_CSW_BMSK                            0x100000
#define HWIO_BAM_CNFG_BITS_BAM_WB_BLK_CSW_SHFT                                0x14
#define HWIO_BAM_CNFG_BITS_BAM_WB_P_RES_BMSK                               0x80000
#define HWIO_BAM_CNFG_BITS_BAM_WB_P_RES_SHFT                                  0x13
#define HWIO_BAM_CNFG_BITS_BAM_SI_P_RES_BMSK                               0x40000
#define HWIO_BAM_CNFG_BITS_BAM_SI_P_RES_SHFT                                  0x12
#define HWIO_BAM_CNFG_BITS_BAM_AU_P_RES_BMSK                               0x20000
#define HWIO_BAM_CNFG_BITS_BAM_AU_P_RES_SHFT                                  0x11
#define HWIO_BAM_CNFG_BITS_BAM_PSM_P_RES_BMSK                              0x10000
#define HWIO_BAM_CNFG_BITS_BAM_PSM_P_RES_SHFT                                 0x10
#define HWIO_BAM_CNFG_BITS_BAM_PSM_CSW_REQ_BMSK                             0x8000
#define HWIO_BAM_CNFG_BITS_BAM_PSM_CSW_REQ_SHFT                                0xf
#define HWIO_BAM_CNFG_BITS_BAM_SB_CLK_REQ_BMSK                              0x4000
#define HWIO_BAM_CNFG_BITS_BAM_SB_CLK_REQ_SHFT                                 0xe
#define HWIO_BAM_CNFG_BITS_BAM_IBC_DISABLE_BMSK                             0x2000
#define HWIO_BAM_CNFG_BITS_BAM_IBC_DISABLE_SHFT                                0xd
#define HWIO_BAM_CNFG_BITS_BAM_NO_EXT_P_RST_BMSK                            0x1000
#define HWIO_BAM_CNFG_BITS_BAM_NO_EXT_P_RST_SHFT                               0xc
#define HWIO_BAM_CNFG_BITS_BAM_FULL_PIPE_BMSK                                0x800
#define HWIO_BAM_CNFG_BITS_BAM_FULL_PIPE_SHFT                                  0xb
#define HWIO_BAM_CNFG_BITS_BAM_ADML_SYNC_BRIDGE_BMSK                           0x8
#define HWIO_BAM_CNFG_BITS_BAM_ADML_SYNC_BRIDGE_SHFT                           0x3
#define HWIO_BAM_CNFG_BITS_BAM_PIPE_CNFG_BMSK                                  0x4
#define HWIO_BAM_CNFG_BITS_BAM_PIPE_CNFG_SHFT                                  0x2
#define HWIO_BAM_CNFG_BITS_BAM_ADML_DEEP_CONS_FIFO_BMSK                        0x2
#define HWIO_BAM_CNFG_BITS_BAM_ADML_DEEP_CONS_FIFO_SHFT                        0x1
#define HWIO_BAM_CNFG_BITS_BAM_ADML_INCR4_EN_N_BMSK                            0x1
#define HWIO_BAM_CNFG_BITS_BAM_ADML_INCR4_EN_N_SHFT                              0

#define HWIO_BAM_IRQ_SRCS_EEn_ADDR(n)                                   (BAM_NDP_REG_BASE      + 0x00004800 + 0x80 * (n))
#define HWIO_BAM_IRQ_SRCS_EEn_PHYS(n)                                   (BAM_NDP_REG_BASE_PHYS + 0x00004800 + 0x80 * (n))
#define HWIO_BAM_IRQ_SRCS_EEn_RMSK                                      0xffffffff
#define HWIO_BAM_IRQ_SRCS_EEn_SHFT                                               0
#define HWIO_BAM_IRQ_SRCS_EEn_MAXn                                             0x7
#define HWIO_BAM_IRQ_SRCS_EEn_INI(n) \
        in_dword(HWIO_BAM_IRQ_SRCS_EEn_ADDR(n))
#define HWIO_BAM_IRQ_SRCS_EEn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_IRQ_SRCS_EEn_ADDR(n), mask)
#define HWIO_BAM_IRQ_SRCS_EEn_OUTI(n,val) \
        out_dword(HWIO_BAM_IRQ_SRCS_EEn_ADDR(n),val)
#define HWIO_BAM_IRQ_SRCS_EEn_BAM_IRQ_BMSK                              0x80000000
#define HWIO_BAM_IRQ_SRCS_EEn_BAM_IRQ_SHFT                                    0x1f
#define HWIO_BAM_IRQ_SRCS_EEn_P_IRQ_BMSK                                0x7fffffff
#define HWIO_BAM_IRQ_SRCS_EEn_P_IRQ_SHFT                                         0

#define HWIO_BAM_IRQ_SRCS_MSK_EEn_ADDR(n)                               (BAM_NDP_REG_BASE      + 0x00004804 + 0x80 * (n))
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_PHYS(n)                               (BAM_NDP_REG_BASE_PHYS + 0x00004804 + 0x80 * (n))
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_RMSK                                  0xffffffff
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_SHFT                                           0
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_MAXn                                         0x7
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_INI(n) \
        in_dword(HWIO_BAM_IRQ_SRCS_MSK_EEn_ADDR(n))
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_IRQ_SRCS_MSK_EEn_ADDR(n), mask)
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_OUTI(n,val) \
        out_dword(HWIO_BAM_IRQ_SRCS_MSK_EEn_ADDR(n),val)
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_IRQ_SRCS_MSK_EEn_ADDR(n),mask,val,HWIO_BAM_IRQ_SRCS_MSK_EEn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_BAM_IRQ_MSK_BMSK                      0x80000000
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_BAM_IRQ_MSK_SHFT                            0x1f
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_P_IRQ_MSK_BMSK                        0x7fffffff
#define HWIO_BAM_IRQ_SRCS_MSK_EEn_P_IRQ_MSK_SHFT                                 0

#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_ADDR(n)                          (BAM_NDP_REG_BASE      + 0x00004808 + 0x80 * (n))
#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_PHYS(n)                          (BAM_NDP_REG_BASE_PHYS + 0x00004808 + 0x80 * (n))
#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_RMSK                             0xffffffff
#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_SHFT                                      0
#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_MAXn                                    0x7
#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_INI(n) \
        in_dword(HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_ADDR(n))
#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_ADDR(n), mask)
#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_OUTI(n,val) \
        out_dword(HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_ADDR(n),val)
#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_BAM_IRQ_UNMASKED_BMSK            0x80000000
#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_BAM_IRQ_UNMASKED_SHFT                  0x1f
#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_P_IRQ_UNMASKED_BMSK              0x7fffffff
#define HWIO_BAM_IRQ_SRCS_UNMASKED_EEn_P_IRQ_UNMASKED_SHFT                       0

#define HWIO_BAM_PIPE_ATTR_EEn_ADDR(n)                                  (BAM_NDP_REG_BASE      + 0x0000480c + 0x80 * (n))
#define HWIO_BAM_PIPE_ATTR_EEn_PHYS(n)                                  (BAM_NDP_REG_BASE_PHYS + 0x0000480c + 0x80 * (n))
#define HWIO_BAM_PIPE_ATTR_EEn_RMSK                                     0xffffffff
#define HWIO_BAM_PIPE_ATTR_EEn_SHFT                                              0
#define HWIO_BAM_PIPE_ATTR_EEn_MAXn                                            0x7
#define HWIO_BAM_PIPE_ATTR_EEn_INI(n) \
        in_dword(HWIO_BAM_PIPE_ATTR_EEn_ADDR(n))
#define HWIO_BAM_PIPE_ATTR_EEn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_PIPE_ATTR_EEn_ADDR(n), mask)
#define HWIO_BAM_PIPE_ATTR_EEn_OUTI(n,val) \
        out_dword(HWIO_BAM_PIPE_ATTR_EEn_ADDR(n),val)
#define HWIO_BAM_PIPE_ATTR_EEn_BAM_ENABLED_BMSK                         0x80000000
#define HWIO_BAM_PIPE_ATTR_EEn_BAM_ENABLED_SHFT                               0x1f
#define HWIO_BAM_PIPE_ATTR_EEn_P_ATTR_BMSK                              0x7fffffff
#define HWIO_BAM_PIPE_ATTR_EEn_P_ATTR_SHFT                                       0

#define HWIO_BAM_P_CTRLn_ADDR(n)                                        (BAM_NDP_REG_BASE      + 0x00005000 + 0x1000 * (n))
#define HWIO_BAM_P_CTRLn_PHYS(n)                                        (BAM_NDP_REG_BASE_PHYS + 0x00005000 + 0x1000 * (n))
#define HWIO_BAM_P_CTRLn_RMSK                                             0x1f0ffa
#define HWIO_BAM_P_CTRLn_SHFT                                                    0
#define HWIO_BAM_P_CTRLn_MAXn                                                 0x13
#define HWIO_BAM_P_CTRLn_INI(n) \
        in_dword(HWIO_BAM_P_CTRLn_ADDR(n))
#define HWIO_BAM_P_CTRLn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_CTRLn_ADDR(n), mask)
#define HWIO_BAM_P_CTRLn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_CTRLn_ADDR(n),val)
#define HWIO_BAM_P_CTRLn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_CTRLn_ADDR(n),mask,val,HWIO_BAM_P_CTRLn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_CTRLn_P_LOCK_GROUP_BMSK                                0x1f0000
#define HWIO_BAM_P_CTRLn_P_LOCK_GROUP_SHFT                                    0x10
#define HWIO_BAM_P_CTRLn_P_WRITE_NWD_BMSK                                    0x800
#define HWIO_BAM_P_CTRLn_P_WRITE_NWD_SHFT                                      0xb
#define HWIO_BAM_P_CTRLn_P_PREFETCH_LIMIT_BMSK                               0x600
#define HWIO_BAM_P_CTRLn_P_PREFETCH_LIMIT_SHFT                                 0x9
#define HWIO_BAM_P_CTRLn_P_AUTO_EOB_SEL_BMSK                                 0x180
#define HWIO_BAM_P_CTRLn_P_AUTO_EOB_SEL_SHFT                                   0x7
#define HWIO_BAM_P_CTRLn_P_AUTO_EOB_BMSK                                      0x40
#define HWIO_BAM_P_CTRLn_P_AUTO_EOB_SHFT                                       0x6
#define HWIO_BAM_P_CTRLn_P_SYS_MODE_BMSK                                      0x20
#define HWIO_BAM_P_CTRLn_P_SYS_MODE_SHFT                                       0x5
#define HWIO_BAM_P_CTRLn_P_SYS_STRM_BMSK                                      0x10
#define HWIO_BAM_P_CTRLn_P_SYS_STRM_SHFT                                       0x4
#define HWIO_BAM_P_CTRLn_P_DIRECTION_BMSK                                      0x8
#define HWIO_BAM_P_CTRLn_P_DIRECTION_SHFT                                      0x3
#define HWIO_BAM_P_CTRLn_P_EN_BMSK                                             0x2
#define HWIO_BAM_P_CTRLn_P_EN_SHFT                                             0x1

#define HWIO_BAM_P_RSTn_ADDR(n)                                         (BAM_NDP_REG_BASE      + 0x00005004 + 0x1000 * (n))
#define HWIO_BAM_P_RSTn_PHYS(n)                                         (BAM_NDP_REG_BASE_PHYS + 0x00005004 + 0x1000 * (n))
#define HWIO_BAM_P_RSTn_RMSK                                                   0x1
#define HWIO_BAM_P_RSTn_SHFT                                                     0
#define HWIO_BAM_P_RSTn_MAXn                                                  0x13
#define HWIO_BAM_P_RSTn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_RSTn_ADDR(n),val)
#define HWIO_BAM_P_RSTn_P_SW_RST_BMSK                                          0x1
#define HWIO_BAM_P_RSTn_P_SW_RST_SHFT                                            0

#define HWIO_BAM_P_HALTn_ADDR(n)                                        (BAM_NDP_REG_BASE      + 0x00005008 + 0x1000 * (n))
#define HWIO_BAM_P_HALTn_PHYS(n)                                        (BAM_NDP_REG_BASE_PHYS + 0x00005008 + 0x1000 * (n))
#define HWIO_BAM_P_HALTn_RMSK                                                  0x3
#define HWIO_BAM_P_HALTn_SHFT                                                    0
#define HWIO_BAM_P_HALTn_MAXn                                                 0x13
#define HWIO_BAM_P_HALTn_INI(n) \
        in_dword(HWIO_BAM_P_HALTn_ADDR(n))
#define HWIO_BAM_P_HALTn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_HALTn_ADDR(n), mask)
#define HWIO_BAM_P_HALTn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_HALTn_ADDR(n),val)
#define HWIO_BAM_P_HALTn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_HALTn_ADDR(n),mask,val,HWIO_BAM_P_HALTn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_HALTn_P_PROD_HALTED_BMSK                                    0x2
#define HWIO_BAM_P_HALTn_P_PROD_HALTED_SHFT                                    0x1
#define HWIO_BAM_P_HALTn_P_HALT_BMSK                                           0x1
#define HWIO_BAM_P_HALTn_P_HALT_SHFT                                             0

#define HWIO_BAM_P_TRUST_REGn_ADDR(n)                                   (BAM_NDP_REG_BASE      + 0x00005030 + 0x1000 * (n))
#define HWIO_BAM_P_TRUST_REGn_PHYS(n)                                   (BAM_NDP_REG_BASE_PHYS + 0x00005030 + 0x1000 * (n))
#define HWIO_BAM_P_TRUST_REGn_RMSK                                          0x1fff
#define HWIO_BAM_P_TRUST_REGn_SHFT                                               0
#define HWIO_BAM_P_TRUST_REGn_MAXn                                            0x13
#define HWIO_BAM_P_TRUST_REGn_INI(n) \
        in_dword(HWIO_BAM_P_TRUST_REGn_ADDR(n))
#define HWIO_BAM_P_TRUST_REGn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_TRUST_REGn_ADDR(n), mask)
#define HWIO_BAM_P_TRUST_REGn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_TRUST_REGn_ADDR(n),val)
#define HWIO_BAM_P_TRUST_REGn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_TRUST_REGn_ADDR(n),mask,val,HWIO_BAM_P_TRUST_REGn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_TRUST_REGn_BAM_P_VMID_BMSK                               0x1f00
#define HWIO_BAM_P_TRUST_REGn_BAM_P_VMID_SHFT                                  0x8
#define HWIO_BAM_P_TRUST_REGn_BAM_P_SUP_GROUP_BMSK                            0xf8
#define HWIO_BAM_P_TRUST_REGn_BAM_P_SUP_GROUP_SHFT                             0x3
#define HWIO_BAM_P_TRUST_REGn_BAM_P_EE_BMSK                                    0x7
#define HWIO_BAM_P_TRUST_REGn_BAM_P_EE_SHFT                                      0

#define HWIO_BAM_P_IRQ_STTSn_ADDR(n)                                    (BAM_NDP_REG_BASE      + 0x00005010 + 0x1000 * (n))
#define HWIO_BAM_P_IRQ_STTSn_PHYS(n)                                    (BAM_NDP_REG_BASE_PHYS + 0x00005010 + 0x1000 * (n))
#define HWIO_BAM_P_IRQ_STTSn_RMSK                                             0xff
#define HWIO_BAM_P_IRQ_STTSn_SHFT                                                0
#define HWIO_BAM_P_IRQ_STTSn_MAXn                                             0x13
#define HWIO_BAM_P_IRQ_STTSn_INI(n) \
        in_dword(HWIO_BAM_P_IRQ_STTSn_ADDR(n))
#define HWIO_BAM_P_IRQ_STTSn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_IRQ_STTSn_ADDR(n), mask)
#define HWIO_BAM_P_IRQ_STTSn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_IRQ_STTSn_ADDR(n),val)
#define HWIO_BAM_P_IRQ_STTSn_P_HRESP_ERR_IRQ_BMSK                             0x80
#define HWIO_BAM_P_IRQ_STTSn_P_HRESP_ERR_IRQ_SHFT                              0x7
#define HWIO_BAM_P_IRQ_STTSn_P_PIPE_RST_ERROR_IRQ_BMSK                        0x40
#define HWIO_BAM_P_IRQ_STTSn_P_PIPE_RST_ERROR_IRQ_SHFT                         0x6
#define HWIO_BAM_P_IRQ_STTSn_P_TRNSFR_END_IRQ_BMSK                            0x20
#define HWIO_BAM_P_IRQ_STTSn_P_TRNSFR_END_IRQ_SHFT                             0x5
#define HWIO_BAM_P_IRQ_STTSn_P_ERR_IRQ_BMSK                                   0x10
#define HWIO_BAM_P_IRQ_STTSn_P_ERR_IRQ_SHFT                                    0x4
#define HWIO_BAM_P_IRQ_STTSn_P_OUT_OF_DESC_IRQ_BMSK                            0x8
#define HWIO_BAM_P_IRQ_STTSn_P_OUT_OF_DESC_IRQ_SHFT                            0x3
#define HWIO_BAM_P_IRQ_STTSn_P_WAKE_IRQ_BMSK                                   0x4
#define HWIO_BAM_P_IRQ_STTSn_P_WAKE_IRQ_SHFT                                   0x2
#define HWIO_BAM_P_IRQ_STTSn_P_TIMER_IRQ_BMSK                                  0x2
#define HWIO_BAM_P_IRQ_STTSn_P_TIMER_IRQ_SHFT                                  0x1
#define HWIO_BAM_P_IRQ_STTSn_P_PRCSD_DESC_IRQ_BMSK                             0x1
#define HWIO_BAM_P_IRQ_STTSn_P_PRCSD_DESC_IRQ_SHFT                               0

#define HWIO_BAM_P_IRQ_CLRn_ADDR(n)                                     (BAM_NDP_REG_BASE      + 0x00005014 + 0x1000 * (n))
#define HWIO_BAM_P_IRQ_CLRn_PHYS(n)                                     (BAM_NDP_REG_BASE_PHYS + 0x00005014 + 0x1000 * (n))
#define HWIO_BAM_P_IRQ_CLRn_RMSK                                              0xff
#define HWIO_BAM_P_IRQ_CLRn_SHFT                                                 0
#define HWIO_BAM_P_IRQ_CLRn_MAXn                                              0x13
#define HWIO_BAM_P_IRQ_CLRn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_IRQ_CLRn_ADDR(n),val)
#define HWIO_BAM_P_IRQ_CLRn_P_HRESP_ERR_CLR_BMSK                              0x80
#define HWIO_BAM_P_IRQ_CLRn_P_HRESP_ERR_CLR_SHFT                               0x7
#define HWIO_BAM_P_IRQ_CLRn_P_PIPE_RST_ERROR_CLR_BMSK                         0x40
#define HWIO_BAM_P_IRQ_CLRn_P_PIPE_RST_ERROR_CLR_SHFT                          0x6
#define HWIO_BAM_P_IRQ_CLRn_P_TRNSFR_END_CLR_BMSK                             0x20
#define HWIO_BAM_P_IRQ_CLRn_P_TRNSFR_END_CLR_SHFT                              0x5
#define HWIO_BAM_P_IRQ_CLRn_P_ERR_CLR_BMSK                                    0x10
#define HWIO_BAM_P_IRQ_CLRn_P_ERR_CLR_SHFT                                     0x4
#define HWIO_BAM_P_IRQ_CLRn_P_OUT_OF_DESC_CLR_BMSK                             0x8
#define HWIO_BAM_P_IRQ_CLRn_P_OUT_OF_DESC_CLR_SHFT                             0x3
#define HWIO_BAM_P_IRQ_CLRn_P_WAKE_CLR_BMSK                                    0x4
#define HWIO_BAM_P_IRQ_CLRn_P_WAKE_CLR_SHFT                                    0x2
#define HWIO_BAM_P_IRQ_CLRn_P_TIMER_CLR_BMSK                                   0x2
#define HWIO_BAM_P_IRQ_CLRn_P_TIMER_CLR_SHFT                                   0x1
#define HWIO_BAM_P_IRQ_CLRn_P_PRCSD_DESC_CLR_BMSK                              0x1
#define HWIO_BAM_P_IRQ_CLRn_P_PRCSD_DESC_CLR_SHFT                                0

#define HWIO_BAM_P_IRQ_ENn_ADDR(n)                                      (BAM_NDP_REG_BASE      + 0x00005018 + 0x1000 * (n))
#define HWIO_BAM_P_IRQ_ENn_PHYS(n)                                      (BAM_NDP_REG_BASE_PHYS + 0x00005018 + 0x1000 * (n))
#define HWIO_BAM_P_IRQ_ENn_RMSK                                               0xff
#define HWIO_BAM_P_IRQ_ENn_SHFT                                                  0
#define HWIO_BAM_P_IRQ_ENn_MAXn                                               0x13
#define HWIO_BAM_P_IRQ_ENn_INI(n) \
        in_dword(HWIO_BAM_P_IRQ_ENn_ADDR(n))
#define HWIO_BAM_P_IRQ_ENn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_IRQ_ENn_ADDR(n), mask)
#define HWIO_BAM_P_IRQ_ENn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_IRQ_ENn_ADDR(n),val)
#define HWIO_BAM_P_IRQ_ENn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_IRQ_ENn_ADDR(n),mask,val,HWIO_BAM_P_IRQ_ENn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_IRQ_ENn_P_HRESP_ERR_EN_BMSK                                0x80
#define HWIO_BAM_P_IRQ_ENn_P_HRESP_ERR_EN_SHFT                                 0x7
#define HWIO_BAM_P_IRQ_ENn_P_PIPE_RST_ERROR_EN_BMSK                           0x40
#define HWIO_BAM_P_IRQ_ENn_P_PIPE_RST_ERROR_EN_SHFT                            0x6
#define HWIO_BAM_P_IRQ_ENn_P_TRNSFR_END_EN_BMSK                               0x20
#define HWIO_BAM_P_IRQ_ENn_P_TRNSFR_END_EN_SHFT                                0x5
#define HWIO_BAM_P_IRQ_ENn_P_ERR_EN_BMSK                                      0x10
#define HWIO_BAM_P_IRQ_ENn_P_ERR_EN_SHFT                                       0x4
#define HWIO_BAM_P_IRQ_ENn_P_OUT_OF_DESC_EN_BMSK                               0x8
#define HWIO_BAM_P_IRQ_ENn_P_OUT_OF_DESC_EN_SHFT                               0x3
#define HWIO_BAM_P_IRQ_ENn_P_WAKE_EN_BMSK                                      0x4
#define HWIO_BAM_P_IRQ_ENn_P_WAKE_EN_SHFT                                      0x2
#define HWIO_BAM_P_IRQ_ENn_P_TIMER_EN_BMSK                                     0x2
#define HWIO_BAM_P_IRQ_ENn_P_TIMER_EN_SHFT                                     0x1
#define HWIO_BAM_P_IRQ_ENn_P_PRCSD_DESC_EN_BMSK                                0x1
#define HWIO_BAM_P_IRQ_ENn_P_PRCSD_DESC_EN_SHFT                                  0

#define HWIO_BAM_P_TIMERn_ADDR(n)                                       (BAM_NDP_REG_BASE      + 0x0000501c + 0x1000 * (n))
#define HWIO_BAM_P_TIMERn_PHYS(n)                                       (BAM_NDP_REG_BASE_PHYS + 0x0000501c + 0x1000 * (n))
#define HWIO_BAM_P_TIMERn_RMSK                                              0xffff
#define HWIO_BAM_P_TIMERn_SHFT                                                   0
#define HWIO_BAM_P_TIMERn_MAXn                                                0x13
#define HWIO_BAM_P_TIMERn_INI(n) \
        in_dword(HWIO_BAM_P_TIMERn_ADDR(n))
#define HWIO_BAM_P_TIMERn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_TIMERn_ADDR(n), mask)
#define HWIO_BAM_P_TIMERn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_TIMERn_ADDR(n),val)
#define HWIO_BAM_P_TIMERn_P_TIMER_BMSK                                      0xffff
#define HWIO_BAM_P_TIMERn_P_TIMER_SHFT                                           0

#define HWIO_BAM_P_TIMER_CTRLn_ADDR(n)                                  (BAM_NDP_REG_BASE      + 0x00005020 + 0x1000 * (n))
#define HWIO_BAM_P_TIMER_CTRLn_PHYS(n)                                  (BAM_NDP_REG_BASE_PHYS + 0x00005020 + 0x1000 * (n))
#define HWIO_BAM_P_TIMER_CTRLn_RMSK                                     0xe000ffff
#define HWIO_BAM_P_TIMER_CTRLn_SHFT                                              0
#define HWIO_BAM_P_TIMER_CTRLn_MAXn                                           0x13
#define HWIO_BAM_P_TIMER_CTRLn_INI(n) \
        in_dword(HWIO_BAM_P_TIMER_CTRLn_ADDR(n))
#define HWIO_BAM_P_TIMER_CTRLn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_TIMER_CTRLn_ADDR(n), mask)
#define HWIO_BAM_P_TIMER_CTRLn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_TIMER_CTRLn_ADDR(n),val)
#define HWIO_BAM_P_TIMER_CTRLn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_TIMER_CTRLn_ADDR(n),mask,val,HWIO_BAM_P_TIMER_CTRLn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_TIMER_CTRLn_P_TIMER_RST_BMSK                         0x80000000
#define HWIO_BAM_P_TIMER_CTRLn_P_TIMER_RST_SHFT                               0x1f
#define HWIO_BAM_P_TIMER_CTRLn_P_TIMER_RUN_BMSK                         0x40000000
#define HWIO_BAM_P_TIMER_CTRLn_P_TIMER_RUN_SHFT                               0x1e
#define HWIO_BAM_P_TIMER_CTRLn_P_TIMER_MODE_BMSK                        0x20000000
#define HWIO_BAM_P_TIMER_CTRLn_P_TIMER_MODE_SHFT                              0x1d
#define HWIO_BAM_P_TIMER_CTRLn_P_TIMER_TRSHLD_BMSK                          0xffff
#define HWIO_BAM_P_TIMER_CTRLn_P_TIMER_TRSHLD_SHFT                               0

#define HWIO_BAM_P_PRDCR_SDBNDn_ADDR(n)                                 (BAM_NDP_REG_BASE      + 0x00005024 + 0x1000 * (n))
#define HWIO_BAM_P_PRDCR_SDBNDn_PHYS(n)                                 (BAM_NDP_REG_BASE_PHYS + 0x00005024 + 0x1000 * (n))
#define HWIO_BAM_P_PRDCR_SDBNDn_RMSK                                     0x11fffff
#define HWIO_BAM_P_PRDCR_SDBNDn_SHFT                                             0
#define HWIO_BAM_P_PRDCR_SDBNDn_MAXn                                          0x13
#define HWIO_BAM_P_PRDCR_SDBNDn_INI(n) \
        in_dword(HWIO_BAM_P_PRDCR_SDBNDn_ADDR(n))
#define HWIO_BAM_P_PRDCR_SDBNDn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_PRDCR_SDBNDn_ADDR(n), mask)
#define HWIO_BAM_P_PRDCR_SDBNDn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_PRDCR_SDBNDn_ADDR(n),val)
#define HWIO_BAM_P_PRDCR_SDBNDn_BAM_P_SB_UPDATED_BMSK                    0x1000000
#define HWIO_BAM_P_PRDCR_SDBNDn_BAM_P_SB_UPDATED_SHFT                         0x18
#define HWIO_BAM_P_PRDCR_SDBNDn_BAM_P_TOGGLE_BMSK                         0x100000
#define HWIO_BAM_P_PRDCR_SDBNDn_BAM_P_TOGGLE_SHFT                             0x14
#define HWIO_BAM_P_PRDCR_SDBNDn_BAM_P_CTRL_BMSK                            0xf0000
#define HWIO_BAM_P_PRDCR_SDBNDn_BAM_P_CTRL_SHFT                               0x10
#define HWIO_BAM_P_PRDCR_SDBNDn_BAM_P_BYTES_FREE_BMSK                       0xffff
#define HWIO_BAM_P_PRDCR_SDBNDn_BAM_P_BYTES_FREE_SHFT                            0

#define HWIO_BAM_P_CNSMR_SDBNDn_ADDR(n)                                 (BAM_NDP_REG_BASE      + 0x00005028 + 0x1000 * (n))
#define HWIO_BAM_P_CNSMR_SDBNDn_PHYS(n)                                 (BAM_NDP_REG_BASE_PHYS + 0x00005028 + 0x1000 * (n))
#define HWIO_BAM_P_CNSMR_SDBNDn_RMSK                                     0x1ffffff
#define HWIO_BAM_P_CNSMR_SDBNDn_SHFT                                             0
#define HWIO_BAM_P_CNSMR_SDBNDn_MAXn                                          0x13
#define HWIO_BAM_P_CNSMR_SDBNDn_INI(n) \
        in_dword(HWIO_BAM_P_CNSMR_SDBNDn_ADDR(n))
#define HWIO_BAM_P_CNSMR_SDBNDn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_CNSMR_SDBNDn_ADDR(n), mask)
#define HWIO_BAM_P_CNSMR_SDBNDn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_CNSMR_SDBNDn_ADDR(n),val)
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_SB_UPDATED_BMSK                    0x1000000
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_SB_UPDATED_SHFT                         0x18
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_WAIT_4_ACK_BMSK                     0x800000
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_WAIT_4_ACK_SHFT                         0x17
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_ACK_TOGGLE_BMSK                     0x400000
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_ACK_TOGGLE_SHFT                         0x16
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_ACK_TOGGLE_R_BMSK                   0x200000
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_ACK_TOGGLE_R_SHFT                       0x15
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_TOGGLE_BMSK                         0x100000
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_TOGGLE_SHFT                             0x14
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_CTRL_BMSK                            0xf0000
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_CTRL_SHFT                               0x10
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_BYTES_AVAIL_BMSK                      0xffff
#define HWIO_BAM_P_CNSMR_SDBNDn_BAM_P_BYTES_AVAIL_SHFT                           0

#define HWIO_BAM_P_EVNT_DEST_ADDRn_ADDR(n)                              (BAM_NDP_REG_BASE      + 0x0000582c + 0x1000 * (n))
#define HWIO_BAM_P_EVNT_DEST_ADDRn_PHYS(n)                              (BAM_NDP_REG_BASE_PHYS + 0x0000582c + 0x1000 * (n))
#define HWIO_BAM_P_EVNT_DEST_ADDRn_RMSK                                 0xffffffff
#define HWIO_BAM_P_EVNT_DEST_ADDRn_SHFT                                          0
#define HWIO_BAM_P_EVNT_DEST_ADDRn_MAXn                                       0x13
#define HWIO_BAM_P_EVNT_DEST_ADDRn_INI(n) \
        in_dword(HWIO_BAM_P_EVNT_DEST_ADDRn_ADDR(n))
#define HWIO_BAM_P_EVNT_DEST_ADDRn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_EVNT_DEST_ADDRn_ADDR(n), mask)
#define HWIO_BAM_P_EVNT_DEST_ADDRn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_EVNT_DEST_ADDRn_ADDR(n),val)
#define HWIO_BAM_P_EVNT_DEST_ADDRn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_EVNT_DEST_ADDRn_ADDR(n),mask,val,HWIO_BAM_P_EVNT_DEST_ADDRn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_EVNT_DEST_ADDRn_P_EVNT_DEST_ADDR_BMSK                0xffffffff
#define HWIO_BAM_P_EVNT_DEST_ADDRn_P_EVNT_DEST_ADDR_SHFT                         0

#define HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_ADDR(n)                          (BAM_NDP_REG_BASE      + 0x00005930 + 0x1000 * (n))
#define HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_PHYS(n)                          (BAM_NDP_REG_BASE_PHYS + 0x00005930 + 0x1000 * (n))
#define HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_RMSK                             0xffffffff
#define HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_SHFT                                      0
#define HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_MAXn                                   0x13
#define HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_INI(n) \
        in_dword(HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_ADDR(n))
#define HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_ADDR(n), mask)
#define HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_ADDR(n),val)
#define HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_ADDR(n),mask,val,HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_P_EVNT_DEST_ADDR_BMSK            0xffffffff
#define HWIO_BAM_P_EVNT_DEST_ADDR_LSBn_P_EVNT_DEST_ADDR_SHFT                     0

#define HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_ADDR(n)                          (BAM_NDP_REG_BASE      + 0x00005934 + 0x1000 * (n))
#define HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_PHYS(n)                          (BAM_NDP_REG_BASE_PHYS + 0x00005934 + 0x1000 * (n))
#define HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_RMSK                                    0xf
#define HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_SHFT                                      0
#define HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_MAXn                                   0x13
#define HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_INI(n) \
        in_dword(HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_ADDR(n))
#define HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_ADDR(n), mask)
#define HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_ADDR(n),val)
#define HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_ADDR(n),mask,val,HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_P_EVNT_DEST_ADDR_BMSK                   0xf
#define HWIO_BAM_P_EVNT_DEST_ADDR_MSBn_P_EVNT_DEST_ADDR_SHFT                     0

#define HWIO_BAM_P_EVNT_REGn_ADDR(n)                                    (BAM_NDP_REG_BASE      + 0x00005818 + 0x1000 * (n))
#define HWIO_BAM_P_EVNT_REGn_PHYS(n)                                    (BAM_NDP_REG_BASE_PHYS + 0x00005818 + 0x1000 * (n))
#define HWIO_BAM_P_EVNT_REGn_RMSK                                       0xffffffff
#define HWIO_BAM_P_EVNT_REGn_SHFT                                                0
#define HWIO_BAM_P_EVNT_REGn_MAXn                                             0x13
#define HWIO_BAM_P_EVNT_REGn_INI(n) \
        in_dword(HWIO_BAM_P_EVNT_REGn_ADDR(n))
#define HWIO_BAM_P_EVNT_REGn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_EVNT_REGn_ADDR(n), mask)
#define HWIO_BAM_P_EVNT_REGn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_EVNT_REGn_ADDR(n),val)
#define HWIO_BAM_P_EVNT_REGn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_EVNT_REGn_ADDR(n),mask,val,HWIO_BAM_P_EVNT_REGn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_EVNT_REGn_P_BYTES_CONSUMED_BMSK                      0xffff0000
#define HWIO_BAM_P_EVNT_REGn_P_BYTES_CONSUMED_SHFT                            0x10
#define HWIO_BAM_P_EVNT_REGn_P_DESC_FIFO_PEER_OFST_BMSK                     0xffff
#define HWIO_BAM_P_EVNT_REGn_P_DESC_FIFO_PEER_OFST_SHFT                          0

#define HWIO_BAM_P_SW_OFSTSn_ADDR(n)                                    (BAM_NDP_REG_BASE      + 0x00005800 + 0x1000 * (n))
#define HWIO_BAM_P_SW_OFSTSn_PHYS(n)                                    (BAM_NDP_REG_BASE_PHYS + 0x00005800 + 0x1000 * (n))
#define HWIO_BAM_P_SW_OFSTSn_RMSK                                       0xffffffff
#define HWIO_BAM_P_SW_OFSTSn_SHFT                                                0
#define HWIO_BAM_P_SW_OFSTSn_MAXn                                             0x13
#define HWIO_BAM_P_SW_OFSTSn_INI(n) \
        in_dword(HWIO_BAM_P_SW_OFSTSn_ADDR(n))
#define HWIO_BAM_P_SW_OFSTSn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_SW_OFSTSn_ADDR(n), mask)
#define HWIO_BAM_P_SW_OFSTSn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_SW_OFSTSn_ADDR(n),val)
#define HWIO_BAM_P_SW_OFSTSn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_SW_OFSTSn_ADDR(n),mask,val,HWIO_BAM_P_SW_OFSTSn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_SW_OFSTSn_SW_OFST_IN_DESC_BMSK                       0xffff0000
#define HWIO_BAM_P_SW_OFSTSn_SW_OFST_IN_DESC_SHFT                             0x10
#define HWIO_BAM_P_SW_OFSTSn_SW_DESC_OFST_BMSK                              0xffff
#define HWIO_BAM_P_SW_OFSTSn_SW_DESC_OFST_SHFT                                   0

#define HWIO_BAM_P_DATA_FIFO_ADDRn_ADDR(n)                              (BAM_NDP_REG_BASE      + 0x00005824 + 0x1000 * (n))
#define HWIO_BAM_P_DATA_FIFO_ADDRn_PHYS(n)                              (BAM_NDP_REG_BASE_PHYS + 0x00005824 + 0x1000 * (n))
#define HWIO_BAM_P_DATA_FIFO_ADDRn_RMSK                                 0xffffffff
#define HWIO_BAM_P_DATA_FIFO_ADDRn_SHFT                                          0
#define HWIO_BAM_P_DATA_FIFO_ADDRn_MAXn                                       0x13
#define HWIO_BAM_P_DATA_FIFO_ADDRn_INI(n) \
        in_dword(HWIO_BAM_P_DATA_FIFO_ADDRn_ADDR(n))
#define HWIO_BAM_P_DATA_FIFO_ADDRn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_DATA_FIFO_ADDRn_ADDR(n), mask)
#define HWIO_BAM_P_DATA_FIFO_ADDRn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_DATA_FIFO_ADDRn_ADDR(n),val)
#define HWIO_BAM_P_DATA_FIFO_ADDRn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_DATA_FIFO_ADDRn_ADDR(n),mask,val,HWIO_BAM_P_DATA_FIFO_ADDRn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_DATA_FIFO_ADDRn_P_DATA_FIFO_ADDR_BMSK                0xffffffff
#define HWIO_BAM_P_DATA_FIFO_ADDRn_P_DATA_FIFO_ADDR_SHFT                         0

#define HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_ADDR(n)                          (BAM_NDP_REG_BASE      + 0x00005920 + 0x1000 * (n))
#define HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_PHYS(n)                          (BAM_NDP_REG_BASE_PHYS + 0x00005920 + 0x1000 * (n))
#define HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_RMSK                             0xffffffff
#define HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_SHFT                                      0
#define HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_MAXn                                   0x13
#define HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_INI(n) \
        in_dword(HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_ADDR(n))
#define HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_ADDR(n), mask)
#define HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_ADDR(n),val)
#define HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_ADDR(n),mask,val,HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_P_DATA_FIFO_ADDR_BMSK            0xffffffff
#define HWIO_BAM_P_DATA_FIFO_ADDR_LSBn_P_DATA_FIFO_ADDR_SHFT                     0

#define HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_ADDR(n)                          (BAM_NDP_REG_BASE      + 0x00005924 + 0x1000 * (n))
#define HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_PHYS(n)                          (BAM_NDP_REG_BASE_PHYS + 0x00005924 + 0x1000 * (n))
#define HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_RMSK                                    0xf
#define HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_SHFT                                      0
#define HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_MAXn                                   0x13
#define HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_INI(n) \
        in_dword(HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_ADDR(n))
#define HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_ADDR(n), mask)
#define HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_ADDR(n),val)
#define HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_ADDR(n),mask,val,HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_P_DATA_FIFO_ADDR_BMSK                   0xf
#define HWIO_BAM_P_DATA_FIFO_ADDR_MSBn_P_DATA_FIFO_ADDR_SHFT                     0

#define HWIO_BAM_P_DESC_FIFO_ADDRn_ADDR(n)                              (BAM_NDP_REG_BASE      + 0x0000581c + 0x1000 * (n))
#define HWIO_BAM_P_DESC_FIFO_ADDRn_PHYS(n)                              (BAM_NDP_REG_BASE_PHYS + 0x0000581c + 0x1000 * (n))
#define HWIO_BAM_P_DESC_FIFO_ADDRn_RMSK                                 0xffffffff
#define HWIO_BAM_P_DESC_FIFO_ADDRn_SHFT                                          0
#define HWIO_BAM_P_DESC_FIFO_ADDRn_MAXn                                       0x13
#define HWIO_BAM_P_DESC_FIFO_ADDRn_INI(n) \
        in_dword(HWIO_BAM_P_DESC_FIFO_ADDRn_ADDR(n))
#define HWIO_BAM_P_DESC_FIFO_ADDRn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_DESC_FIFO_ADDRn_ADDR(n), mask)
#define HWIO_BAM_P_DESC_FIFO_ADDRn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_DESC_FIFO_ADDRn_ADDR(n),val)
#define HWIO_BAM_P_DESC_FIFO_ADDRn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_DESC_FIFO_ADDRn_ADDR(n),mask,val,HWIO_BAM_P_DESC_FIFO_ADDRn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_DESC_FIFO_ADDRn_P_DESC_FIFO_ADDR_BMSK                0xffffffff
#define HWIO_BAM_P_DESC_FIFO_ADDRn_P_DESC_FIFO_ADDR_SHFT                         0

#define HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_ADDR(n)                          (BAM_NDP_REG_BASE      + 0x00005910 + 0x1000 * (n))
#define HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_PHYS(n)                          (BAM_NDP_REG_BASE_PHYS + 0x00005910 + 0x1000 * (n))
#define HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_RMSK                             0xffffffff
#define HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_SHFT                                      0
#define HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_MAXn                                   0x13
#define HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_INI(n) \
        in_dword(HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_ADDR(n))
#define HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_ADDR(n), mask)
#define HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_ADDR(n),val)
#define HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_ADDR(n),mask,val,HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_P_DESC_FIFO_ADDR_BMSK            0xffffffff
#define HWIO_BAM_P_DESC_FIFO_ADDR_LSBn_P_DESC_FIFO_ADDR_SHFT                     0

#define HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_ADDR(n)                          (BAM_NDP_REG_BASE      + 0x00005914 + 0x1000 * (n))
#define HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_PHYS(n)                          (BAM_NDP_REG_BASE_PHYS + 0x00005914 + 0x1000 * (n))
#define HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_RMSK                                    0xf
#define HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_SHFT                                      0
#define HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_MAXn                                   0x13
#define HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_INI(n) \
        in_dword(HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_ADDR(n))
#define HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_ADDR(n), mask)
#define HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_ADDR(n),val)
#define HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_ADDR(n),mask,val,HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_P_DESC_FIFO_ADDR_BMSK                   0xf
#define HWIO_BAM_P_DESC_FIFO_ADDR_MSBn_P_DESC_FIFO_ADDR_SHFT                     0

#define HWIO_BAM_P_EVNT_GEN_TRSHLDn_ADDR(n)                             (BAM_NDP_REG_BASE      + 0x00005828 + 0x1000 * (n))
#define HWIO_BAM_P_EVNT_GEN_TRSHLDn_PHYS(n)                             (BAM_NDP_REG_BASE_PHYS + 0x00005828 + 0x1000 * (n))
#define HWIO_BAM_P_EVNT_GEN_TRSHLDn_RMSK                                    0xffff
#define HWIO_BAM_P_EVNT_GEN_TRSHLDn_SHFT                                         0
#define HWIO_BAM_P_EVNT_GEN_TRSHLDn_MAXn                                      0x13
#define HWIO_BAM_P_EVNT_GEN_TRSHLDn_INI(n) \
        in_dword(HWIO_BAM_P_EVNT_GEN_TRSHLDn_ADDR(n))
#define HWIO_BAM_P_EVNT_GEN_TRSHLDn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_EVNT_GEN_TRSHLDn_ADDR(n), mask)
#define HWIO_BAM_P_EVNT_GEN_TRSHLDn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_EVNT_GEN_TRSHLDn_ADDR(n),val)
#define HWIO_BAM_P_EVNT_GEN_TRSHLDn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_EVNT_GEN_TRSHLDn_ADDR(n),mask,val,HWIO_BAM_P_EVNT_GEN_TRSHLDn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_EVNT_GEN_TRSHLDn_P_TRSHLD_BMSK                           0xffff
#define HWIO_BAM_P_EVNT_GEN_TRSHLDn_P_TRSHLD_SHFT                                0

#define HWIO_BAM_P_FIFO_SIZESn_ADDR(n)                                  (BAM_NDP_REG_BASE      + 0x00005820 + 0x1000 * (n))
#define HWIO_BAM_P_FIFO_SIZESn_PHYS(n)                                  (BAM_NDP_REG_BASE_PHYS + 0x00005820 + 0x1000 * (n))
#define HWIO_BAM_P_FIFO_SIZESn_RMSK                                     0xffffffff
#define HWIO_BAM_P_FIFO_SIZESn_SHFT                                              0
#define HWIO_BAM_P_FIFO_SIZESn_MAXn                                           0x13
#define HWIO_BAM_P_FIFO_SIZESn_INI(n) \
        in_dword(HWIO_BAM_P_FIFO_SIZESn_ADDR(n))
#define HWIO_BAM_P_FIFO_SIZESn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_FIFO_SIZESn_ADDR(n), mask)
#define HWIO_BAM_P_FIFO_SIZESn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_FIFO_SIZESn_ADDR(n),val)
#define HWIO_BAM_P_FIFO_SIZESn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_FIFO_SIZESn_ADDR(n),mask,val,HWIO_BAM_P_FIFO_SIZESn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_FIFO_SIZESn_P_DATA_FIFO_SIZE_BMSK                    0xffff0000
#define HWIO_BAM_P_FIFO_SIZESn_P_DATA_FIFO_SIZE_SHFT                          0x10
#define HWIO_BAM_P_FIFO_SIZESn_P_DESC_FIFO_SIZE_BMSK                        0xffff
#define HWIO_BAM_P_FIFO_SIZESn_P_DESC_FIFO_SIZE_SHFT                             0

#define HWIO_BAM_P_RETR_CNTXT_n_ADDR(n)                                 (BAM_NDP_REG_BASE      + 0x00005834 + 0x1000 * (n))
#define HWIO_BAM_P_RETR_CNTXT_n_PHYS(n)                                 (BAM_NDP_REG_BASE_PHYS + 0x00005834 + 0x1000 * (n))
#define HWIO_BAM_P_RETR_CNTXT_n_RMSK                                    0xffffffff
#define HWIO_BAM_P_RETR_CNTXT_n_SHFT                                             0
#define HWIO_BAM_P_RETR_CNTXT_n_MAXn                                          0x13
#define HWIO_BAM_P_RETR_CNTXT_n_INI(n) \
        in_dword(HWIO_BAM_P_RETR_CNTXT_n_ADDR(n))
#define HWIO_BAM_P_RETR_CNTXT_n_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_RETR_CNTXT_n_ADDR(n), mask)
#define HWIO_BAM_P_RETR_CNTXT_n_OUTI(n,val) \
        out_dword(HWIO_BAM_P_RETR_CNTXT_n_ADDR(n),val)
#define HWIO_BAM_P_RETR_CNTXT_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_RETR_CNTXT_n_ADDR(n),mask,val,HWIO_BAM_P_RETR_CNTXT_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_RETR_CNTXT_n_RETR_DESC_OFST_BMSK                     0xffff0000
#define HWIO_BAM_P_RETR_CNTXT_n_RETR_DESC_OFST_SHFT                           0x10
#define HWIO_BAM_P_RETR_CNTXT_n_RETR_OFST_IN_DESC_BMSK                      0xffff
#define HWIO_BAM_P_RETR_CNTXT_n_RETR_OFST_IN_DESC_SHFT                           0

#define HWIO_BAM_P_SI_CNTXT_n_ADDR(n)                                   (BAM_NDP_REG_BASE      + 0x00005838 + 0x1000 * (n))
#define HWIO_BAM_P_SI_CNTXT_n_PHYS(n)                                   (BAM_NDP_REG_BASE_PHYS + 0x00005838 + 0x1000 * (n))
#define HWIO_BAM_P_SI_CNTXT_n_RMSK                                          0xffff
#define HWIO_BAM_P_SI_CNTXT_n_SHFT                                               0
#define HWIO_BAM_P_SI_CNTXT_n_MAXn                                            0x13
#define HWIO_BAM_P_SI_CNTXT_n_INI(n) \
        in_dword(HWIO_BAM_P_SI_CNTXT_n_ADDR(n))
#define HWIO_BAM_P_SI_CNTXT_n_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_SI_CNTXT_n_ADDR(n), mask)
#define HWIO_BAM_P_SI_CNTXT_n_OUTI(n,val) \
        out_dword(HWIO_BAM_P_SI_CNTXT_n_ADDR(n),val)
#define HWIO_BAM_P_SI_CNTXT_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_SI_CNTXT_n_ADDR(n),mask,val,HWIO_BAM_P_SI_CNTXT_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_SI_CNTXT_n_SI_DESC_OFST_BMSK                             0xffff
#define HWIO_BAM_P_SI_CNTXT_n_SI_DESC_OFST_SHFT                                  0

#define HWIO_BAM_P_DF_CNTXT_n_ADDR(n)                                   (BAM_NDP_REG_BASE      + 0x00005830 + 0x1000 * (n))
#define HWIO_BAM_P_DF_CNTXT_n_PHYS(n)                                   (BAM_NDP_REG_BASE_PHYS + 0x00005830 + 0x1000 * (n))
#define HWIO_BAM_P_DF_CNTXT_n_RMSK                                      0xffffffff
#define HWIO_BAM_P_DF_CNTXT_n_SHFT                                               0
#define HWIO_BAM_P_DF_CNTXT_n_MAXn                                            0x13
#define HWIO_BAM_P_DF_CNTXT_n_INI(n) \
        in_dword(HWIO_BAM_P_DF_CNTXT_n_ADDR(n))
#define HWIO_BAM_P_DF_CNTXT_n_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_DF_CNTXT_n_ADDR(n), mask)
#define HWIO_BAM_P_DF_CNTXT_n_OUTI(n,val) \
        out_dword(HWIO_BAM_P_DF_CNTXT_n_ADDR(n),val)
#define HWIO_BAM_P_DF_CNTXT_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_DF_CNTXT_n_ADDR(n),mask,val,HWIO_BAM_P_DF_CNTXT_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_DF_CNTXT_n_WB_ACCUMULATED_BMSK                       0xffff0000
#define HWIO_BAM_P_DF_CNTXT_n_WB_ACCUMULATED_SHFT                             0x10
#define HWIO_BAM_P_DF_CNTXT_n_DF_DESC_OFST_BMSK                             0xffff
#define HWIO_BAM_P_DF_CNTXT_n_DF_DESC_OFST_SHFT                                  0

#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_ADDR(n)                             (BAM_NDP_REG_BASE      + 0x00005804 + 0x1000 * (n))
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_PHYS(n)                             (BAM_NDP_REG_BASE_PHYS + 0x00005804 + 0x1000 * (n))
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_RMSK                                0xffffffff
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_SHFT                                         0
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_MAXn                                      0x13
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_INI(n) \
        in_dword(HWIO_BAM_P_AU_PSM_CNTXT_1_n_ADDR(n))
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_AU_PSM_CNTXT_1_n_ADDR(n), mask)
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_OUTI(n,val) \
        out_dword(HWIO_BAM_P_AU_PSM_CNTXT_1_n_ADDR(n),val)
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_AU_PSM_CNTXT_1_n_ADDR(n),mask,val,HWIO_BAM_P_AU_PSM_CNTXT_1_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_AU_PSM_ACCUMED_BMSK                 0xffff0000
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_AU_PSM_ACCUMED_SHFT                       0x10
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_AU_ACKED_BMSK                           0xffff
#define HWIO_BAM_P_AU_PSM_CNTXT_1_n_AU_ACKED_SHFT                                0

#define HWIO_BAM_P_PSM_CNTXT_2_n_ADDR(n)                                (BAM_NDP_REG_BASE      + 0x00005808 + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_2_n_PHYS(n)                                (BAM_NDP_REG_BASE_PHYS + 0x00005808 + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_2_n_RMSK                                   0xffffffff
#define HWIO_BAM_P_PSM_CNTXT_2_n_SHFT                                            0
#define HWIO_BAM_P_PSM_CNTXT_2_n_MAXn                                         0x13
#define HWIO_BAM_P_PSM_CNTXT_2_n_INI(n) \
        in_dword(HWIO_BAM_P_PSM_CNTXT_2_n_ADDR(n))
#define HWIO_BAM_P_PSM_CNTXT_2_n_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_PSM_CNTXT_2_n_ADDR(n), mask)
#define HWIO_BAM_P_PSM_CNTXT_2_n_OUTI(n,val) \
        out_dword(HWIO_BAM_P_PSM_CNTXT_2_n_ADDR(n),val)
#define HWIO_BAM_P_PSM_CNTXT_2_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_PSM_CNTXT_2_n_ADDR(n),mask,val,HWIO_BAM_P_PSM_CNTXT_2_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_VALID_BMSK                    0x80000000
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_VALID_SHFT                          0x1f
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_IRQ_BMSK                      0x40000000
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_IRQ_SHFT                            0x1e
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_IRQ_DONE_BMSK                 0x20000000
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_IRQ_DONE_SHFT                       0x1d
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_GENERAL_BITS_BMSK                  0x1e000000
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_GENERAL_BITS_SHFT                        0x19
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_CONS_STATE_BMSK                     0x1c00000
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_CONS_STATE_SHFT                          0x16
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_PROD_SYS_STATE_BMSK                  0x380000
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_PROD_SYS_STATE_SHFT                      0x13
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_PROD_B2B_STATE_BMSK                   0x70000
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_PROD_B2B_STATE_SHFT                      0x10
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_SIZE_BMSK                         0xffff
#define HWIO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_SIZE_SHFT                              0

#define HWIO_BAM_P_PSM_CNTXT_3_n_ADDR(n)                                (BAM_NDP_REG_BASE      + 0x0000580c + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_3_n_PHYS(n)                                (BAM_NDP_REG_BASE_PHYS + 0x0000580c + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_3_n_RMSK                                   0xffffffff
#define HWIO_BAM_P_PSM_CNTXT_3_n_SHFT                                            0
#define HWIO_BAM_P_PSM_CNTXT_3_n_MAXn                                         0x13
#define HWIO_BAM_P_PSM_CNTXT_3_n_INI(n) \
        in_dword(HWIO_BAM_P_PSM_CNTXT_3_n_ADDR(n))
#define HWIO_BAM_P_PSM_CNTXT_3_n_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_PSM_CNTXT_3_n_ADDR(n), mask)
#define HWIO_BAM_P_PSM_CNTXT_3_n_OUTI(n,val) \
        out_dword(HWIO_BAM_P_PSM_CNTXT_3_n_ADDR(n),val)
#define HWIO_BAM_P_PSM_CNTXT_3_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_PSM_CNTXT_3_n_ADDR(n),mask,val,HWIO_BAM_P_PSM_CNTXT_3_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_PSM_CNTXT_3_n_PSM_DESC_ADDR_BMSK                     0xffffffff
#define HWIO_BAM_P_PSM_CNTXT_3_n_PSM_DESC_ADDR_SHFT                              0

#define HWIO_BAM_P_PSM_CNTXT_3_LSBn_ADDR(n)                             (BAM_NDP_REG_BASE      + 0x00005900 + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_3_LSBn_PHYS(n)                             (BAM_NDP_REG_BASE_PHYS + 0x00005900 + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_3_LSBn_RMSK                                0xffffffff
#define HWIO_BAM_P_PSM_CNTXT_3_LSBn_SHFT                                         0
#define HWIO_BAM_P_PSM_CNTXT_3_LSBn_MAXn                                      0x13
#define HWIO_BAM_P_PSM_CNTXT_3_LSBn_INI(n) \
        in_dword(HWIO_BAM_P_PSM_CNTXT_3_LSBn_ADDR(n))
#define HWIO_BAM_P_PSM_CNTXT_3_LSBn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_PSM_CNTXT_3_LSBn_ADDR(n), mask)
#define HWIO_BAM_P_PSM_CNTXT_3_LSBn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_PSM_CNTXT_3_LSBn_ADDR(n),val)
#define HWIO_BAM_P_PSM_CNTXT_3_LSBn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_PSM_CNTXT_3_LSBn_ADDR(n),mask,val,HWIO_BAM_P_PSM_CNTXT_3_LSBn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_PSM_CNTXT_3_LSBn_PSM_DESC_ADDR_BMSK                  0xffffffff
#define HWIO_BAM_P_PSM_CNTXT_3_LSBn_PSM_DESC_ADDR_SHFT                           0

#define HWIO_BAM_P_PSM_CNTXT_3_MSBn_ADDR(n)                             (BAM_NDP_REG_BASE      + 0x00005904 + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_3_MSBn_PHYS(n)                             (BAM_NDP_REG_BASE_PHYS + 0x00005904 + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_3_MSBn_RMSK                                       0xf
#define HWIO_BAM_P_PSM_CNTXT_3_MSBn_SHFT                                         0
#define HWIO_BAM_P_PSM_CNTXT_3_MSBn_MAXn                                      0x13
#define HWIO_BAM_P_PSM_CNTXT_3_MSBn_INI(n) \
        in_dword(HWIO_BAM_P_PSM_CNTXT_3_MSBn_ADDR(n))
#define HWIO_BAM_P_PSM_CNTXT_3_MSBn_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_PSM_CNTXT_3_MSBn_ADDR(n), mask)
#define HWIO_BAM_P_PSM_CNTXT_3_MSBn_OUTI(n,val) \
        out_dword(HWIO_BAM_P_PSM_CNTXT_3_MSBn_ADDR(n),val)
#define HWIO_BAM_P_PSM_CNTXT_3_MSBn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_PSM_CNTXT_3_MSBn_ADDR(n),mask,val,HWIO_BAM_P_PSM_CNTXT_3_MSBn_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_PSM_CNTXT_3_MSBn_PSM_DESC_ADDR_BMSK                         0xf
#define HWIO_BAM_P_PSM_CNTXT_3_MSBn_PSM_DESC_ADDR_SHFT                           0

#define HWIO_BAM_P_PSM_CNTXT_4_n_ADDR(n)                                (BAM_NDP_REG_BASE      + 0x00005810 + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_4_n_PHYS(n)                                (BAM_NDP_REG_BASE_PHYS + 0x00005810 + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_4_n_RMSK                                   0xffffffff
#define HWIO_BAM_P_PSM_CNTXT_4_n_SHFT                                            0
#define HWIO_BAM_P_PSM_CNTXT_4_n_MAXn                                         0x13
#define HWIO_BAM_P_PSM_CNTXT_4_n_INI(n) \
        in_dword(HWIO_BAM_P_PSM_CNTXT_4_n_ADDR(n))
#define HWIO_BAM_P_PSM_CNTXT_4_n_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_PSM_CNTXT_4_n_ADDR(n), mask)
#define HWIO_BAM_P_PSM_CNTXT_4_n_OUTI(n,val) \
        out_dword(HWIO_BAM_P_PSM_CNTXT_4_n_ADDR(n),val)
#define HWIO_BAM_P_PSM_CNTXT_4_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_PSM_CNTXT_4_n_ADDR(n),mask,val,HWIO_BAM_P_PSM_CNTXT_4_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_PSM_CNTXT_4_n_PSM_DESC_OFST_BMSK                     0xffff0000
#define HWIO_BAM_P_PSM_CNTXT_4_n_PSM_DESC_OFST_SHFT                           0x10
#define HWIO_BAM_P_PSM_CNTXT_4_n_PSM_SAVED_ACCUMED_SIZE_BMSK                0xffff
#define HWIO_BAM_P_PSM_CNTXT_4_n_PSM_SAVED_ACCUMED_SIZE_SHFT                     0

#define HWIO_BAM_P_PSM_CNTXT_5_n_ADDR(n)                                (BAM_NDP_REG_BASE      + 0x00005814 + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_5_n_PHYS(n)                                (BAM_NDP_REG_BASE_PHYS + 0x00005814 + 0x1000 * (n))
#define HWIO_BAM_P_PSM_CNTXT_5_n_RMSK                                   0xffffffff
#define HWIO_BAM_P_PSM_CNTXT_5_n_SHFT                                            0
#define HWIO_BAM_P_PSM_CNTXT_5_n_MAXn                                         0x13
#define HWIO_BAM_P_PSM_CNTXT_5_n_INI(n) \
        in_dword(HWIO_BAM_P_PSM_CNTXT_5_n_ADDR(n))
#define HWIO_BAM_P_PSM_CNTXT_5_n_INMI(n,mask) \
        in_dword_masked(HWIO_BAM_P_PSM_CNTXT_5_n_ADDR(n), mask)
#define HWIO_BAM_P_PSM_CNTXT_5_n_OUTI(n,val) \
        out_dword(HWIO_BAM_P_PSM_CNTXT_5_n_ADDR(n),val)
#define HWIO_BAM_P_PSM_CNTXT_5_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_BAM_P_PSM_CNTXT_5_n_ADDR(n),mask,val,HWIO_BAM_P_PSM_CNTXT_5_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_BAM_P_PSM_CNTXT_5_n_PSM_BLOCK_BYTE_CNT_BMSK                0xffff0000
#define HWIO_BAM_P_PSM_CNTXT_5_n_PSM_BLOCK_BYTE_CNT_SHFT                      0x10
#define HWIO_BAM_P_PSM_CNTXT_5_n_PSM_OFST_IN_DESC_BMSK                      0xffff
#define HWIO_BAM_P_PSM_CNTXT_5_n_PSM_OFST_IN_DESC_SHFT                           0

#define HWIO_XPU_SCR_ADDR                                               (BAM_NDP_REG_BASE      + 0x00002000)
#define HWIO_XPU_SCR_PHYS                                               (BAM_NDP_REG_BASE_PHYS + 0x00002000)
#define HWIO_XPU_SCR_RMSK                                                    0x77f
#define HWIO_XPU_SCR_SHFT                                                        0
#define HWIO_XPU_SCR_IN                                                 \
        in_dword_masked(HWIO_XPU_SCR_ADDR, HWIO_XPU_SCR_RMSK)
#define HWIO_XPU_SCR_INM(m)                                             \
        in_dword_masked(HWIO_XPU_SCR_ADDR, m)
#define HWIO_XPU_SCR_OUT(v)                                             \
        out_dword(HWIO_XPU_SCR_ADDR,v)
#define HWIO_XPU_SCR_OUTM(m,v)                                          \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_SCR_ADDR,m,v,HWIO_XPU_SCR_IN); \
        HWIO_INTFREE()
#define HWIO_XPU_SCR_SCLEIE_BMSK                                             0x400
#define HWIO_XPU_SCR_SCLEIE_SHFT                                               0xa
#define HWIO_XPU_SCR_SCFGEIE_BMSK                                            0x200
#define HWIO_XPU_SCR_SCFGEIE_SHFT                                              0x9
#define HWIO_XPU_SCR_DYNAMIC_CLK_EN_BMSK                                     0x100
#define HWIO_XPU_SCR_DYNAMIC_CLK_EN_SHFT                                       0x8
#define HWIO_XPU_SCR_NSRGCLEE_BMSK                                            0x40
#define HWIO_XPU_SCR_NSRGCLEE_SHFT                                             0x6
#define HWIO_XPU_SCR_NSCFGE_BMSK                                              0x20
#define HWIO_XPU_SCR_NSCFGE_SHFT                                               0x5
#define HWIO_XPU_SCR_SDCDEE_BMSK                                              0x10
#define HWIO_XPU_SCR_SDCDEE_SHFT                                               0x4
#define HWIO_XPU_SCR_SEIE_BMSK                                                 0x8
#define HWIO_XPU_SCR_SEIE_SHFT                                                 0x3
#define HWIO_XPU_SCR_SCLERE_BMSK                                               0x4
#define HWIO_XPU_SCR_SCLERE_SHFT                                               0x2
#define HWIO_XPU_SCR_SCFGERE_BMSK                                              0x2
#define HWIO_XPU_SCR_SCFGERE_SHFT                                              0x1
#define HWIO_XPU_SCR_XPUNSE_BMSK                                               0x1
#define HWIO_XPU_SCR_XPUNSE_SHFT                                                 0

#define HWIO_XPU_SWDR_ADDR                                              (BAM_NDP_REG_BASE      + 0x00002004)
#define HWIO_XPU_SWDR_PHYS                                              (BAM_NDP_REG_BASE_PHYS + 0x00002004)
#define HWIO_XPU_SWDR_RMSK                                                     0x1
#define HWIO_XPU_SWDR_SHFT                                                       0
#define HWIO_XPU_SWDR_IN                                                \
        in_dword_masked(HWIO_XPU_SWDR_ADDR, HWIO_XPU_SWDR_RMSK)
#define HWIO_XPU_SWDR_INM(m)                                            \
        in_dword_masked(HWIO_XPU_SWDR_ADDR, m)
#define HWIO_XPU_SWDR_OUT(v)                                            \
        out_dword(HWIO_XPU_SWDR_ADDR,v)
#define HWIO_XPU_SWDR_OUTM(m,v)                                         \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_SWDR_ADDR,m,v,HWIO_XPU_SWDR_IN); \
        HWIO_INTFREE()
#define HWIO_XPU_SWDR_SCFGWD_BMSK                                              0x1
#define HWIO_XPU_SWDR_SCFGWD_SHFT                                                0

#define HWIO_XPU_SEAR0_ADDR                                             (BAM_NDP_REG_BASE      + 0x00002040)
#define HWIO_XPU_SEAR0_PHYS                                             (BAM_NDP_REG_BASE_PHYS + 0x00002040)
#define HWIO_XPU_SEAR0_RMSK                                             0xffffffff
#define HWIO_XPU_SEAR0_SHFT                                                      0
#define HWIO_XPU_SEAR0_IN                                               \
        in_dword_masked(HWIO_XPU_SEAR0_ADDR, HWIO_XPU_SEAR0_RMSK)
#define HWIO_XPU_SEAR0_INM(m)                                           \
        in_dword_masked(HWIO_XPU_SEAR0_ADDR, m)
#define HWIO_XPU_SEAR0_PA_BMSK                                          0xffffffff
#define HWIO_XPU_SEAR0_PA_SHFT                                                   0

#define HWIO_XPU_SESR_ADDR                                              (BAM_NDP_REG_BASE      + 0x00002048)
#define HWIO_XPU_SESR_PHYS                                              (BAM_NDP_REG_BASE_PHYS + 0x00002048)
#define HWIO_XPU_SESR_RMSK                                              0x8000000f
#define HWIO_XPU_SESR_SHFT                                                       0
#define HWIO_XPU_SESR_IN                                                \
        in_dword_masked(HWIO_XPU_SESR_ADDR, HWIO_XPU_SESR_RMSK)
#define HWIO_XPU_SESR_INM(m)                                            \
        in_dword_masked(HWIO_XPU_SESR_ADDR, m)
#define HWIO_XPU_SESR_OUT(v)                                            \
        out_dword(HWIO_XPU_SESR_ADDR,v)
#define HWIO_XPU_SESR_OUTM(m,v)                                         \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_SESR_ADDR,m,v,HWIO_XPU_SESR_IN); \
        HWIO_INTFREE()
#define HWIO_XPU_SESR_MULTI_BMSK                                        0x80000000
#define HWIO_XPU_SESR_MULTI_SHFT                                              0x1f
#define HWIO_XPU_SESR_CLMULTI_BMSK                                             0x8
#define HWIO_XPU_SESR_CLMULTI_SHFT                                             0x3
#define HWIO_XPU_SESR_CFGMULTI_BMSK                                            0x4
#define HWIO_XPU_SESR_CFGMULTI_SHFT                                            0x2
#define HWIO_XPU_SESR_CLIENT_BMSK                                              0x2
#define HWIO_XPU_SESR_CLIENT_SHFT                                              0x1
#define HWIO_XPU_SESR_CFG_BMSK                                                 0x1
#define HWIO_XPU_SESR_CFG_SHFT                                                   0

#define HWIO_XPU_SESRRESTORE_ADDR                                       (BAM_NDP_REG_BASE      + 0x0000204c)
#define HWIO_XPU_SESRRESTORE_PHYS                                       (BAM_NDP_REG_BASE_PHYS + 0x0000204c)
#define HWIO_XPU_SESRRESTORE_RMSK                                       0x8000000f
#define HWIO_XPU_SESRRESTORE_SHFT                                                0
#define HWIO_XPU_SESRRESTORE_IN                                         \
        in_dword_masked(HWIO_XPU_SESRRESTORE_ADDR, HWIO_XPU_SESRRESTORE_RMSK)
#define HWIO_XPU_SESRRESTORE_INM(m)                                     \
        in_dword_masked(HWIO_XPU_SESRRESTORE_ADDR, m)
#define HWIO_XPU_SESRRESTORE_OUT(v)                                     \
        out_dword(HWIO_XPU_SESRRESTORE_ADDR,v)
#define HWIO_XPU_SESRRESTORE_OUTM(m,v)                                  \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_SESRRESTORE_ADDR,m,v,HWIO_XPU_SESRRESTORE_IN); \
        HWIO_INTFREE()
#define HWIO_XPU_SESRRESTORE_MULTI_BMSK                                 0x80000000
#define HWIO_XPU_SESRRESTORE_MULTI_SHFT                                       0x1f
#define HWIO_XPU_SESRRESTORE_CLMULTI_BMSK                                      0x8
#define HWIO_XPU_SESRRESTORE_CLMULTI_SHFT                                      0x3
#define HWIO_XPU_SESRRESTORE_CFGMULTI_BMSK                                     0x4
#define HWIO_XPU_SESRRESTORE_CFGMULTI_SHFT                                     0x2
#define HWIO_XPU_SESRRESTORE_CLIENT_BMSK                                       0x2
#define HWIO_XPU_SESRRESTORE_CLIENT_SHFT                                       0x1
#define HWIO_XPU_SESRRESTORE_CFG_BMSK                                          0x1
#define HWIO_XPU_SESRRESTORE_CFG_SHFT                                            0

#define HWIO_XPU_SESYNR0_ADDR                                           (BAM_NDP_REG_BASE      + 0x00002050)
#define HWIO_XPU_SESYNR0_PHYS                                           (BAM_NDP_REG_BASE_PHYS + 0x00002050)
#define HWIO_XPU_SESYNR0_RMSK                                           0xffffffff
#define HWIO_XPU_SESYNR0_SHFT                                                    0
#define HWIO_XPU_SESYNR0_IN                                             \
        in_dword_masked(HWIO_XPU_SESYNR0_ADDR, HWIO_XPU_SESYNR0_RMSK)
#define HWIO_XPU_SESYNR0_INM(m)                                         \
        in_dword_masked(HWIO_XPU_SESYNR0_ADDR, m)
#define HWIO_XPU_SESYNR0_ATID_BMSK                                      0xff000000
#define HWIO_XPU_SESYNR0_ATID_SHFT                                            0x18
#define HWIO_XPU_SESYNR0_AVMID_BMSK                                       0xff0000
#define HWIO_XPU_SESYNR0_AVMID_SHFT                                           0x10
#define HWIO_XPU_SESYNR0_ABID_BMSK                                          0xe000
#define HWIO_XPU_SESYNR0_ABID_SHFT                                             0xd
#define HWIO_XPU_SESYNR0_APID_BMSK                                          0x1f00
#define HWIO_XPU_SESYNR0_APID_SHFT                                             0x8
#define HWIO_XPU_SESYNR0_AMID_BMSK                                            0xff
#define HWIO_XPU_SESYNR0_AMID_SHFT                                               0

#define HWIO_XPU_SESYNR1_ADDR                                           (BAM_NDP_REG_BASE      + 0x00002054)
#define HWIO_XPU_SESYNR1_PHYS                                           (BAM_NDP_REG_BASE_PHYS + 0x00002054)
#define HWIO_XPU_SESYNR1_RMSK                                           0xffffffff
#define HWIO_XPU_SESYNR1_SHFT                                                    0
#define HWIO_XPU_SESYNR1_IN                                             \
        in_dword_masked(HWIO_XPU_SESYNR1_ADDR, HWIO_XPU_SESYNR1_RMSK)
#define HWIO_XPU_SESYNR1_INM(m)                                         \
        in_dword_masked(HWIO_XPU_SESYNR1_ADDR, m)
#define HWIO_XPU_SESYNR1_DCD_BMSK                                       0x80000000
#define HWIO_XPU_SESYNR1_DCD_SHFT                                             0x1f
#define HWIO_XPU_SESYNR1_AC_BMSK                                        0x40000000
#define HWIO_XPU_SESYNR1_AC_SHFT                                              0x1e
#define HWIO_XPU_SESYNR1_BURSTLEN_BMSK                                  0x20000000
#define HWIO_XPU_SESYNR1_BURSTLEN_SHFT                                        0x1d
#define HWIO_XPU_SESYNR1_ARDALLOCATE_BMSK                               0x10000000
#define HWIO_XPU_SESYNR1_ARDALLOCATE_SHFT                                     0x1c
#define HWIO_XPU_SESYNR1_ABURST_BMSK                                     0x8000000
#define HWIO_XPU_SESYNR1_ABURST_SHFT                                          0x1b
#define HWIO_XPU_SESYNR1_AEXCLUSIVE_BMSK                                 0x4000000
#define HWIO_XPU_SESYNR1_AEXCLUSIVE_SHFT                                      0x1a
#define HWIO_XPU_SESYNR1_AWRITE_BMSK                                     0x2000000
#define HWIO_XPU_SESYNR1_AWRITE_SHFT                                          0x19
#define HWIO_XPU_SESYNR1_AFULL_BMSK                                      0x1000000
#define HWIO_XPU_SESYNR1_AFULL_SHFT                                           0x18
#define HWIO_XPU_SESYNR1_ARDBEADNDXEN_BMSK                                0x800000
#define HWIO_XPU_SESYNR1_ARDBEADNDXEN_SHFT                                    0x17
#define HWIO_XPU_SESYNR1_AOOO_BMSK                                        0x400000
#define HWIO_XPU_SESYNR1_AOOO_SHFT                                            0x16
#define HWIO_XPU_SESYNR1_APREQPRIORITY_BMSK                               0x380000
#define HWIO_XPU_SESYNR1_APREQPRIORITY_SHFT                                   0x13
#define HWIO_XPU_SESYNR1_ASIZE_BMSK                                        0x70000
#define HWIO_XPU_SESYNR1_ASIZE_SHFT                                           0x10
#define HWIO_XPU_SESYNR1_AMSSSELFAUTH_BMSK                                  0x8000
#define HWIO_XPU_SESYNR1_AMSSSELFAUTH_SHFT                                     0xf
#define HWIO_XPU_SESYNR1_ALEN_BMSK                                          0x7f00
#define HWIO_XPU_SESYNR1_ALEN_SHFT                                             0x8
#define HWIO_XPU_SESYNR1_AINST_BMSK                                           0x80
#define HWIO_XPU_SESYNR1_AINST_SHFT                                            0x7
#define HWIO_XPU_SESYNR1_APROTNS_BMSK                                         0x40
#define HWIO_XPU_SESYNR1_APROTNS_SHFT                                          0x6
#define HWIO_XPU_SESYNR1_APRIV_BMSK                                           0x20
#define HWIO_XPU_SESYNR1_APRIV_SHFT                                            0x5
#define HWIO_XPU_SESYNR1_AINNERSHARED_BMSK                                    0x10
#define HWIO_XPU_SESYNR1_AINNERSHARED_SHFT                                     0x4
#define HWIO_XPU_SESYNR1_ASHARED_BMSK                                          0x8
#define HWIO_XPU_SESYNR1_ASHARED_SHFT                                          0x3
#define HWIO_XPU_SESYNR1_AMEMTYPE_BMSK                                         0x7
#define HWIO_XPU_SESYNR1_AMEMTYPE_SHFT                                           0

#define HWIO_XPU_SESYNR2_ADDR                                           (BAM_NDP_REG_BASE      + 0x00002058)
#define HWIO_XPU_SESYNR2_PHYS                                           (BAM_NDP_REG_BASE_PHYS + 0x00002058)
#define HWIO_XPU_SESYNR2_RMSK                                                  0x7
#define HWIO_XPU_SESYNR2_SHFT                                                    0
#define HWIO_XPU_SESYNR2_IN                                             \
        in_dword_masked(HWIO_XPU_SESYNR2_ADDR, HWIO_XPU_SESYNR2_RMSK)
#define HWIO_XPU_SESYNR2_INM(m)                                         \
        in_dword_masked(HWIO_XPU_SESYNR2_ADDR, m)
#define HWIO_XPU_SESYNR2_MODEM_PRT_HIT_BMSK                                    0x4
#define HWIO_XPU_SESYNR2_MODEM_PRT_HIT_SHFT                                    0x2
#define HWIO_XPU_SESYNR2_SECURE_PRT_HIT_BMSK                                   0x2
#define HWIO_XPU_SESYNR2_SECURE_PRT_HIT_SHFT                                   0x1
#define HWIO_XPU_SESYNR2_NONSECURE_PRT_HIT_BMSK                                0x1
#define HWIO_XPU_SESYNR2_NONSECURE_PRT_HIT_SHFT                                  0

#define HWIO_XPU_MCR_ADDR                                               (BAM_NDP_REG_BASE      + 0x00002100)
#define HWIO_XPU_MCR_PHYS                                               (BAM_NDP_REG_BASE_PHYS + 0x00002100)
#define HWIO_XPU_MCR_RMSK                                                    0x71f
#define HWIO_XPU_MCR_SHFT                                                        0
#define HWIO_XPU_MCR_IN                                                 \
        in_dword_masked(HWIO_XPU_MCR_ADDR, HWIO_XPU_MCR_RMSK)
#define HWIO_XPU_MCR_INM(m)                                             \
        in_dword_masked(HWIO_XPU_MCR_ADDR, m)
#define HWIO_XPU_MCR_OUT(v)                                             \
        out_dword(HWIO_XPU_MCR_ADDR,v)
#define HWIO_XPU_MCR_OUTM(m,v)                                          \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_MCR_ADDR,m,v,HWIO_XPU_MCR_IN); \
        HWIO_INTFREE()
#define HWIO_XPU_MCR_CLEIE_BMSK                                              0x400
#define HWIO_XPU_MCR_CLEIE_SHFT                                                0xa
#define HWIO_XPU_MCR_CFGEIE_BMSK                                             0x200
#define HWIO_XPU_MCR_CFGEIE_SHFT                                               0x9
#define HWIO_XPU_MCR_DYNAMIC_CLK_EN_BMSK                                     0x100
#define HWIO_XPU_MCR_DYNAMIC_CLK_EN_SHFT                                       0x8
#define HWIO_XPU_MCR_DCDEE_BMSK                                               0x10
#define HWIO_XPU_MCR_DCDEE_SHFT                                                0x4
#define HWIO_XPU_MCR_EIE_BMSK                                                  0x8
#define HWIO_XPU_MCR_EIE_SHFT                                                  0x3
#define HWIO_XPU_MCR_CLERE_BMSK                                                0x4
#define HWIO_XPU_MCR_CLERE_SHFT                                                0x2
#define HWIO_XPU_MCR_CFGERE_BMSK                                               0x2
#define HWIO_XPU_MCR_CFGERE_SHFT                                               0x1
#define HWIO_XPU_MCR_XPUMSAE_BMSK                                              0x1
#define HWIO_XPU_MCR_XPUMSAE_SHFT                                                0

#define HWIO_XPU_MEAR0_ADDR                                             (BAM_NDP_REG_BASE      + 0x00002140)
#define HWIO_XPU_MEAR0_PHYS                                             (BAM_NDP_REG_BASE_PHYS + 0x00002140)
#define HWIO_XPU_MEAR0_RMSK                                             0xffffffff
#define HWIO_XPU_MEAR0_SHFT                                                      0
#define HWIO_XPU_MEAR0_IN                                               \
        in_dword_masked(HWIO_XPU_MEAR0_ADDR, HWIO_XPU_MEAR0_RMSK)
#define HWIO_XPU_MEAR0_INM(m)                                           \
        in_dword_masked(HWIO_XPU_MEAR0_ADDR, m)
#define HWIO_XPU_MEAR0_PA_BMSK                                          0xffffffff
#define HWIO_XPU_MEAR0_PA_SHFT                                                   0

#define HWIO_XPU_MESR_ADDR                                              (BAM_NDP_REG_BASE      + 0x00002148)
#define HWIO_XPU_MESR_PHYS                                              (BAM_NDP_REG_BASE_PHYS + 0x00002148)
#define HWIO_XPU_MESR_RMSK                                              0x8000000f
#define HWIO_XPU_MESR_SHFT                                                       0
#define HWIO_XPU_MESR_IN                                                \
        in_dword_masked(HWIO_XPU_MESR_ADDR, HWIO_XPU_MESR_RMSK)
#define HWIO_XPU_MESR_INM(m)                                            \
        in_dword_masked(HWIO_XPU_MESR_ADDR, m)
#define HWIO_XPU_MESR_OUT(v)                                            \
        out_dword(HWIO_XPU_MESR_ADDR,v)
#define HWIO_XPU_MESR_OUTM(m,v)                                         \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_MESR_ADDR,m,v,HWIO_XPU_MESR_IN); \
        HWIO_INTFREE()
#define HWIO_XPU_MESR_MULTI_BMSK                                        0x80000000
#define HWIO_XPU_MESR_MULTI_SHFT                                              0x1f
#define HWIO_XPU_MESR_CLMULTI_BMSK                                             0x8
#define HWIO_XPU_MESR_CLMULTI_SHFT                                             0x3
#define HWIO_XPU_MESR_CFGMULTI_BMSK                                            0x4
#define HWIO_XPU_MESR_CFGMULTI_SHFT                                            0x2
#define HWIO_XPU_MESR_CLIENT_BMSK                                              0x2
#define HWIO_XPU_MESR_CLIENT_SHFT                                              0x1
#define HWIO_XPU_MESR_CFG_BMSK                                                 0x1
#define HWIO_XPU_MESR_CFG_SHFT                                                   0

#define HWIO_XPU_MESRRESTORE_ADDR                                       (BAM_NDP_REG_BASE      + 0x0000214c)
#define HWIO_XPU_MESRRESTORE_PHYS                                       (BAM_NDP_REG_BASE_PHYS + 0x0000214c)
#define HWIO_XPU_MESRRESTORE_RMSK                                       0x8000000f
#define HWIO_XPU_MESRRESTORE_SHFT                                                0
#define HWIO_XPU_MESRRESTORE_IN                                         \
        in_dword_masked(HWIO_XPU_MESRRESTORE_ADDR, HWIO_XPU_MESRRESTORE_RMSK)
#define HWIO_XPU_MESRRESTORE_INM(m)                                     \
        in_dword_masked(HWIO_XPU_MESRRESTORE_ADDR, m)
#define HWIO_XPU_MESRRESTORE_OUT(v)                                     \
        out_dword(HWIO_XPU_MESRRESTORE_ADDR,v)
#define HWIO_XPU_MESRRESTORE_OUTM(m,v)                                  \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_MESRRESTORE_ADDR,m,v,HWIO_XPU_MESRRESTORE_IN); \
        HWIO_INTFREE()
#define HWIO_XPU_MESRRESTORE_MULTI_BMSK                                 0x80000000
#define HWIO_XPU_MESRRESTORE_MULTI_SHFT                                       0x1f
#define HWIO_XPU_MESRRESTORE_CLMULTI_BMSK                                      0x8
#define HWIO_XPU_MESRRESTORE_CLMULTI_SHFT                                      0x3
#define HWIO_XPU_MESRRESTORE_CFGMULTI_BMSK                                     0x4
#define HWIO_XPU_MESRRESTORE_CFGMULTI_SHFT                                     0x2
#define HWIO_XPU_MESRRESTORE_CLIENT_BMSK                                       0x2
#define HWIO_XPU_MESRRESTORE_CLIENT_SHFT                                       0x1
#define HWIO_XPU_MESRRESTORE_CFG_BMSK                                          0x1
#define HWIO_XPU_MESRRESTORE_CFG_SHFT                                            0

#define HWIO_XPU_MESYNR0_ADDR                                           (BAM_NDP_REG_BASE      + 0x00002150)
#define HWIO_XPU_MESYNR0_PHYS                                           (BAM_NDP_REG_BASE_PHYS + 0x00002150)
#define HWIO_XPU_MESYNR0_RMSK                                           0xffffffff
#define HWIO_XPU_MESYNR0_SHFT                                                    0
#define HWIO_XPU_MESYNR0_IN                                             \
        in_dword_masked(HWIO_XPU_MESYNR0_ADDR, HWIO_XPU_MESYNR0_RMSK)
#define HWIO_XPU_MESYNR0_INM(m)                                         \
        in_dword_masked(HWIO_XPU_MESYNR0_ADDR, m)
#define HWIO_XPU_MESYNR0_ATID_BMSK                                      0xff000000
#define HWIO_XPU_MESYNR0_ATID_SHFT                                            0x18
#define HWIO_XPU_MESYNR0_AVMID_BMSK                                       0xff0000
#define HWIO_XPU_MESYNR0_AVMID_SHFT                                           0x10
#define HWIO_XPU_MESYNR0_ABID_BMSK                                          0xe000
#define HWIO_XPU_MESYNR0_ABID_SHFT                                             0xd
#define HWIO_XPU_MESYNR0_APID_BMSK                                          0x1f00
#define HWIO_XPU_MESYNR0_APID_SHFT                                             0x8
#define HWIO_XPU_MESYNR0_AMID_BMSK                                            0xff
#define HWIO_XPU_MESYNR0_AMID_SHFT                                               0

#define HWIO_XPU_MESYNR1_ADDR                                           (BAM_NDP_REG_BASE      + 0x00002154)
#define HWIO_XPU_MESYNR1_PHYS                                           (BAM_NDP_REG_BASE_PHYS + 0x00002154)
#define HWIO_XPU_MESYNR1_RMSK                                           0xffffffff
#define HWIO_XPU_MESYNR1_SHFT                                                    0
#define HWIO_XPU_MESYNR1_IN                                             \
        in_dword_masked(HWIO_XPU_MESYNR1_ADDR, HWIO_XPU_MESYNR1_RMSK)
#define HWIO_XPU_MESYNR1_INM(m)                                         \
        in_dword_masked(HWIO_XPU_MESYNR1_ADDR, m)
#define HWIO_XPU_MESYNR1_DCD_BMSK                                       0x80000000
#define HWIO_XPU_MESYNR1_DCD_SHFT                                             0x1f
#define HWIO_XPU_MESYNR1_AC_BMSK                                        0x40000000
#define HWIO_XPU_MESYNR1_AC_SHFT                                              0x1e
#define HWIO_XPU_MESYNR1_BURSTLEN_BMSK                                  0x20000000
#define HWIO_XPU_MESYNR1_BURSTLEN_SHFT                                        0x1d
#define HWIO_XPU_MESYNR1_ARDALLOCATE_BMSK                               0x10000000
#define HWIO_XPU_MESYNR1_ARDALLOCATE_SHFT                                     0x1c
#define HWIO_XPU_MESYNR1_ABURST_BMSK                                     0x8000000
#define HWIO_XPU_MESYNR1_ABURST_SHFT                                          0x1b
#define HWIO_XPU_MESYNR1_AEXCLUSIVE_BMSK                                 0x4000000
#define HWIO_XPU_MESYNR1_AEXCLUSIVE_SHFT                                      0x1a
#define HWIO_XPU_MESYNR1_AWRITE_BMSK                                     0x2000000
#define HWIO_XPU_MESYNR1_AWRITE_SHFT                                          0x19
#define HWIO_XPU_MESYNR1_AFULL_BMSK                                      0x1000000
#define HWIO_XPU_MESYNR1_AFULL_SHFT                                           0x18
#define HWIO_XPU_MESYNR1_ARDBEADNDXEN_BMSK                                0x800000
#define HWIO_XPU_MESYNR1_ARDBEADNDXEN_SHFT                                    0x17
#define HWIO_XPU_MESYNR1_AOOO_BMSK                                        0x400000
#define HWIO_XPU_MESYNR1_AOOO_SHFT                                            0x16
#define HWIO_XPU_MESYNR1_APREQPRIORITY_BMSK                               0x380000
#define HWIO_XPU_MESYNR1_APREQPRIORITY_SHFT                                   0x13
#define HWIO_XPU_MESYNR1_ASIZE_BMSK                                        0x70000
#define HWIO_XPU_MESYNR1_ASIZE_SHFT                                           0x10
#define HWIO_XPU_MESYNR1_AMSSSELFAUTH_BMSK                                  0x8000
#define HWIO_XPU_MESYNR1_AMSSSELFAUTH_SHFT                                     0xf
#define HWIO_XPU_MESYNR1_ALEN_BMSK                                          0x7f00
#define HWIO_XPU_MESYNR1_ALEN_SHFT                                             0x8
#define HWIO_XPU_MESYNR1_AINST_BMSK                                           0x80
#define HWIO_XPU_MESYNR1_AINST_SHFT                                            0x7
#define HWIO_XPU_MESYNR1_APROTNS_BMSK                                         0x40
#define HWIO_XPU_MESYNR1_APROTNS_SHFT                                          0x6
#define HWIO_XPU_MESYNR1_APRIV_BMSK                                           0x20
#define HWIO_XPU_MESYNR1_APRIV_SHFT                                            0x5
#define HWIO_XPU_MESYNR1_AINNERSHARED_BMSK                                    0x10
#define HWIO_XPU_MESYNR1_AINNERSHARED_SHFT                                     0x4
#define HWIO_XPU_MESYNR1_ASHARED_BMSK                                          0x8
#define HWIO_XPU_MESYNR1_ASHARED_SHFT                                          0x3
#define HWIO_XPU_MESYNR1_AMEMTYPE_BMSK                                         0x7
#define HWIO_XPU_MESYNR1_AMEMTYPE_SHFT                                           0

#define HWIO_XPU_MESYNR2_ADDR                                           (BAM_NDP_REG_BASE      + 0x00002158)
#define HWIO_XPU_MESYNR2_PHYS                                           (BAM_NDP_REG_BASE_PHYS + 0x00002158)
#define HWIO_XPU_MESYNR2_RMSK                                                  0x7
#define HWIO_XPU_MESYNR2_SHFT                                                    0
#define HWIO_XPU_MESYNR2_IN                                             \
        in_dword_masked(HWIO_XPU_MESYNR2_ADDR, HWIO_XPU_MESYNR2_RMSK)
#define HWIO_XPU_MESYNR2_INM(m)                                         \
        in_dword_masked(HWIO_XPU_MESYNR2_ADDR, m)
#define HWIO_XPU_MESYNR2_MODEM_PRT_HIT_BMSK                                    0x4
#define HWIO_XPU_MESYNR2_MODEM_PRT_HIT_SHFT                                    0x2
#define HWIO_XPU_MESYNR2_SECURE_PRT_HIT_BMSK                                   0x2
#define HWIO_XPU_MESYNR2_SECURE_PRT_HIT_SHFT                                   0x1
#define HWIO_XPU_MESYNR2_NONSECURE_PRT_HIT_BMSK                                0x1
#define HWIO_XPU_MESYNR2_NONSECURE_PRT_HIT_SHFT                                  0

#define HWIO_XPU_CR_ADDR                                                (BAM_NDP_REG_BASE      + 0x00002080)
#define HWIO_XPU_CR_PHYS                                                (BAM_NDP_REG_BASE_PHYS + 0x00002080)
#define HWIO_XPU_CR_RMSK                                                     0x71f
#define HWIO_XPU_CR_SHFT                                                         0
#define HWIO_XPU_CR_IN                                                  \
        in_dword_masked(HWIO_XPU_CR_ADDR, HWIO_XPU_CR_RMSK)
#define HWIO_XPU_CR_INM(m)                                              \
        in_dword_masked(HWIO_XPU_CR_ADDR, m)
#define HWIO_XPU_CR_OUT(v)                                              \
        out_dword(HWIO_XPU_CR_ADDR,v)
#define HWIO_XPU_CR_OUTM(m,v)                                           \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_CR_ADDR,m,v,HWIO_XPU_CR_IN); \
        HWIO_INTFREE()
#define HWIO_XPU_CR_CLEIE_BMSK                                               0x400
#define HWIO_XPU_CR_CLEIE_SHFT                                                 0xa
#define HWIO_XPU_CR_CFGEIE_BMSK                                              0x200
#define HWIO_XPU_CR_CFGEIE_SHFT                                                0x9
#define HWIO_XPU_CR_DYNAMIC_CLK_EN_BMSK                                      0x100
#define HWIO_XPU_CR_DYNAMIC_CLK_EN_SHFT                                        0x8
#define HWIO_XPU_CR_DCDEE_BMSK                                                0x10
#define HWIO_XPU_CR_DCDEE_SHFT                                                 0x4
#define HWIO_XPU_CR_EIE_BMSK                                                   0x8
#define HWIO_XPU_CR_EIE_SHFT                                                   0x3
#define HWIO_XPU_CR_CLERE_BMSK                                                 0x4
#define HWIO_XPU_CR_CLERE_SHFT                                                 0x2
#define HWIO_XPU_CR_CFGERE_BMSK                                                0x2
#define HWIO_XPU_CR_CFGERE_SHFT                                                0x1
#define HWIO_XPU_CR_XPUVMIDE_BMSK                                              0x1
#define HWIO_XPU_CR_XPUVMIDE_SHFT                                                0

#define HWIO_XPU_RPU_ACRn_ADDR(n)                                       (BAM_NDP_REG_BASE      + 0x000020a0 + 0x4 * (n))
#define HWIO_XPU_RPU_ACRn_PHYS(n)                                       (BAM_NDP_REG_BASE_PHYS + 0x000020a0 + 0x4 * (n))
#define HWIO_XPU_RPU_ACRn_RMSK                                          0xffffffff
#define HWIO_XPU_RPU_ACRn_SHFT                                                   0
#define HWIO_XPU_RPU_ACRn_MAXn                                                   0
#define HWIO_XPU_RPU_ACRn_INI(n) \
        in_dword(HWIO_XPU_RPU_ACRn_ADDR(n))
#define HWIO_XPU_RPU_ACRn_INMI(n,mask) \
        in_dword_masked(HWIO_XPU_RPU_ACRn_ADDR(n), mask)
#define HWIO_XPU_RPU_ACRn_OUTI(n,val) \
        out_dword(HWIO_XPU_RPU_ACRn_ADDR(n),val)
#define HWIO_XPU_RPU_ACRn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_RPU_ACRn_ADDR(n),mask,val,HWIO_XPU_RPU_ACRn_INI(n));\
        HWIO_INTFREE()
#define HWIO_XPU_RPU_ACRn_RWE_BMSK                                      0xffffffff
#define HWIO_XPU_RPU_ACRn_RWE_SHFT                                               0

#define HWIO_XPU_EAR0_ADDR                                              (BAM_NDP_REG_BASE      + 0x000020c0)
#define HWIO_XPU_EAR0_PHYS                                              (BAM_NDP_REG_BASE_PHYS + 0x000020c0)
#define HWIO_XPU_EAR0_RMSK                                              0xffffffff
#define HWIO_XPU_EAR0_SHFT                                                       0
#define HWIO_XPU_EAR0_IN                                                \
        in_dword_masked(HWIO_XPU_EAR0_ADDR, HWIO_XPU_EAR0_RMSK)
#define HWIO_XPU_EAR0_INM(m)                                            \
        in_dword_masked(HWIO_XPU_EAR0_ADDR, m)
#define HWIO_XPU_EAR0_PA_BMSK                                           0xffffffff
#define HWIO_XPU_EAR0_PA_SHFT                                                    0

#define HWIO_XPU_ESR_ADDR                                               (BAM_NDP_REG_BASE      + 0x000020c8)
#define HWIO_XPU_ESR_PHYS                                               (BAM_NDP_REG_BASE_PHYS + 0x000020c8)
#define HWIO_XPU_ESR_RMSK                                               0x8000000f
#define HWIO_XPU_ESR_SHFT                                                        0
#define HWIO_XPU_ESR_IN                                                 \
        in_dword_masked(HWIO_XPU_ESR_ADDR, HWIO_XPU_ESR_RMSK)
#define HWIO_XPU_ESR_INM(m)                                             \
        in_dword_masked(HWIO_XPU_ESR_ADDR, m)
#define HWIO_XPU_ESR_OUT(v)                                             \
        out_dword(HWIO_XPU_ESR_ADDR,v)
#define HWIO_XPU_ESR_OUTM(m,v)                                          \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_ESR_ADDR,m,v,HWIO_XPU_ESR_IN); \
        HWIO_INTFREE()
#define HWIO_XPU_ESR_MULTI_BMSK                                         0x80000000
#define HWIO_XPU_ESR_MULTI_SHFT                                               0x1f
#define HWIO_XPU_ESR_CLMULTI_BMSK                                              0x8
#define HWIO_XPU_ESR_CLMULTI_SHFT                                              0x3
#define HWIO_XPU_ESR_CFGMULTI_BMSK                                             0x4
#define HWIO_XPU_ESR_CFGMULTI_SHFT                                             0x2
#define HWIO_XPU_ESR_CLIENT_BMSK                                               0x2
#define HWIO_XPU_ESR_CLIENT_SHFT                                               0x1
#define HWIO_XPU_ESR_CFG_BMSK                                                  0x1
#define HWIO_XPU_ESR_CFG_SHFT                                                    0

#define HWIO_XPU_ESRRESTORE_ADDR                                        (BAM_NDP_REG_BASE      + 0x000020cc)
#define HWIO_XPU_ESRRESTORE_PHYS                                        (BAM_NDP_REG_BASE_PHYS + 0x000020cc)
#define HWIO_XPU_ESRRESTORE_RMSK                                        0x8000000f
#define HWIO_XPU_ESRRESTORE_SHFT                                                 0
#define HWIO_XPU_ESRRESTORE_IN                                          \
        in_dword_masked(HWIO_XPU_ESRRESTORE_ADDR, HWIO_XPU_ESRRESTORE_RMSK)
#define HWIO_XPU_ESRRESTORE_INM(m)                                      \
        in_dword_masked(HWIO_XPU_ESRRESTORE_ADDR, m)
#define HWIO_XPU_ESRRESTORE_OUT(v)                                      \
        out_dword(HWIO_XPU_ESRRESTORE_ADDR,v)
#define HWIO_XPU_ESRRESTORE_OUTM(m,v)                                   \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_ESRRESTORE_ADDR,m,v,HWIO_XPU_ESRRESTORE_IN); \
        HWIO_INTFREE()
#define HWIO_XPU_ESRRESTORE_MULTI_BMSK                                  0x80000000
#define HWIO_XPU_ESRRESTORE_MULTI_SHFT                                        0x1f
#define HWIO_XPU_ESRRESTORE_CLMULTI_BMSK                                       0x8
#define HWIO_XPU_ESRRESTORE_CLMULTI_SHFT                                       0x3
#define HWIO_XPU_ESRRESTORE_CFGMULTI_BMSK                                      0x4
#define HWIO_XPU_ESRRESTORE_CFGMULTI_SHFT                                      0x2
#define HWIO_XPU_ESRRESTORE_CLIENT_BMSK                                        0x2
#define HWIO_XPU_ESRRESTORE_CLIENT_SHFT                                        0x1
#define HWIO_XPU_ESRRESTORE_CFG_BMSK                                           0x1
#define HWIO_XPU_ESRRESTORE_CFG_SHFT                                             0

#define HWIO_XPU_ESYNR0_ADDR                                            (BAM_NDP_REG_BASE      + 0x000020d0)
#define HWIO_XPU_ESYNR0_PHYS                                            (BAM_NDP_REG_BASE_PHYS + 0x000020d0)
#define HWIO_XPU_ESYNR0_RMSK                                            0xffffffff
#define HWIO_XPU_ESYNR0_SHFT                                                     0
#define HWIO_XPU_ESYNR0_IN                                              \
        in_dword_masked(HWIO_XPU_ESYNR0_ADDR, HWIO_XPU_ESYNR0_RMSK)
#define HWIO_XPU_ESYNR0_INM(m)                                          \
        in_dword_masked(HWIO_XPU_ESYNR0_ADDR, m)
#define HWIO_XPU_ESYNR0_ATID_BMSK                                       0xff000000
#define HWIO_XPU_ESYNR0_ATID_SHFT                                             0x18
#define HWIO_XPU_ESYNR0_AVMID_BMSK                                        0xff0000
#define HWIO_XPU_ESYNR0_AVMID_SHFT                                            0x10
#define HWIO_XPU_ESYNR0_ABID_BMSK                                           0xe000
#define HWIO_XPU_ESYNR0_ABID_SHFT                                              0xd
#define HWIO_XPU_ESYNR0_APID_BMSK                                           0x1f00
#define HWIO_XPU_ESYNR0_APID_SHFT                                              0x8
#define HWIO_XPU_ESYNR0_AMID_BMSK                                             0xff
#define HWIO_XPU_ESYNR0_AMID_SHFT                                                0

#define HWIO_XPU_ESYNR1_ADDR                                            (BAM_NDP_REG_BASE      + 0x000020d4)
#define HWIO_XPU_ESYNR1_PHYS                                            (BAM_NDP_REG_BASE_PHYS + 0x000020d4)
#define HWIO_XPU_ESYNR1_RMSK                                            0xffffffff
#define HWIO_XPU_ESYNR1_SHFT                                                     0
#define HWIO_XPU_ESYNR1_IN                                              \
        in_dword_masked(HWIO_XPU_ESYNR1_ADDR, HWIO_XPU_ESYNR1_RMSK)
#define HWIO_XPU_ESYNR1_INM(m)                                          \
        in_dword_masked(HWIO_XPU_ESYNR1_ADDR, m)
#define HWIO_XPU_ESYNR1_DCD_BMSK                                        0x80000000
#define HWIO_XPU_ESYNR1_DCD_SHFT                                              0x1f
#define HWIO_XPU_ESYNR1_AC_BMSK                                         0x40000000
#define HWIO_XPU_ESYNR1_AC_SHFT                                               0x1e
#define HWIO_XPU_ESYNR1_BURSTLEN_BMSK                                   0x20000000
#define HWIO_XPU_ESYNR1_BURSTLEN_SHFT                                         0x1d
#define HWIO_XPU_ESYNR1_ARDALLOCATE_BMSK                                0x10000000
#define HWIO_XPU_ESYNR1_ARDALLOCATE_SHFT                                      0x1c
#define HWIO_XPU_ESYNR1_ABURST_BMSK                                      0x8000000
#define HWIO_XPU_ESYNR1_ABURST_SHFT                                           0x1b
#define HWIO_XPU_ESYNR1_AEXCLUSIVE_BMSK                                  0x4000000
#define HWIO_XPU_ESYNR1_AEXCLUSIVE_SHFT                                       0x1a
#define HWIO_XPU_ESYNR1_AWRITE_BMSK                                      0x2000000
#define HWIO_XPU_ESYNR1_AWRITE_SHFT                                           0x19
#define HWIO_XPU_ESYNR1_AFULL_BMSK                                       0x1000000
#define HWIO_XPU_ESYNR1_AFULL_SHFT                                            0x18
#define HWIO_XPU_ESYNR1_ARDBEADNDXEN_BMSK                                 0x800000
#define HWIO_XPU_ESYNR1_ARDBEADNDXEN_SHFT                                     0x17
#define HWIO_XPU_ESYNR1_AOOO_BMSK                                         0x400000
#define HWIO_XPU_ESYNR1_AOOO_SHFT                                             0x16
#define HWIO_XPU_ESYNR1_APREQPRIORITY_BMSK                                0x380000
#define HWIO_XPU_ESYNR1_APREQPRIORITY_SHFT                                    0x13
#define HWIO_XPU_ESYNR1_ASIZE_BMSK                                         0x70000
#define HWIO_XPU_ESYNR1_ASIZE_SHFT                                            0x10
#define HWIO_XPU_ESYNR1_AMSSSELFAUTH_BMSK                                   0x8000
#define HWIO_XPU_ESYNR1_AMSSSELFAUTH_SHFT                                      0xf
#define HWIO_XPU_ESYNR1_ALEN_BMSK                                           0x7f00
#define HWIO_XPU_ESYNR1_ALEN_SHFT                                              0x8
#define HWIO_XPU_ESYNR1_AINST_BMSK                                            0x80
#define HWIO_XPU_ESYNR1_AINST_SHFT                                             0x7
#define HWIO_XPU_ESYNR1_APROTNS_BMSK                                          0x40
#define HWIO_XPU_ESYNR1_APROTNS_SHFT                                           0x6
#define HWIO_XPU_ESYNR1_APRIV_BMSK                                            0x20
#define HWIO_XPU_ESYNR1_APRIV_SHFT                                             0x5
#define HWIO_XPU_ESYNR1_AINNERSHARED_BMSK                                     0x10
#define HWIO_XPU_ESYNR1_AINNERSHARED_SHFT                                      0x4
#define HWIO_XPU_ESYNR1_ASHARED_BMSK                                           0x8
#define HWIO_XPU_ESYNR1_ASHARED_SHFT                                           0x3
#define HWIO_XPU_ESYNR1_AMEMTYPE_BMSK                                          0x7
#define HWIO_XPU_ESYNR1_AMEMTYPE_SHFT                                            0

#define HWIO_XPU_ESYNR2_ADDR                                            (BAM_NDP_REG_BASE      + 0x000020d8)
#define HWIO_XPU_ESYNR2_PHYS                                            (BAM_NDP_REG_BASE_PHYS + 0x000020d8)
#define HWIO_XPU_ESYNR2_RMSK                                                   0x7
#define HWIO_XPU_ESYNR2_SHFT                                                     0
#define HWIO_XPU_ESYNR2_IN                                              \
        in_dword_masked(HWIO_XPU_ESYNR2_ADDR, HWIO_XPU_ESYNR2_RMSK)
#define HWIO_XPU_ESYNR2_INM(m)                                          \
        in_dword_masked(HWIO_XPU_ESYNR2_ADDR, m)
#define HWIO_XPU_ESYNR2_MODEM_PRT_HIT_BMSK                                     0x4
#define HWIO_XPU_ESYNR2_MODEM_PRT_HIT_SHFT                                     0x2
#define HWIO_XPU_ESYNR2_SECURE_PRT_HIT_BMSK                                    0x2
#define HWIO_XPU_ESYNR2_SECURE_PRT_HIT_SHFT                                    0x1
#define HWIO_XPU_ESYNR2_NONSECURE_PRT_HIT_BMSK                                 0x1
#define HWIO_XPU_ESYNR2_NONSECURE_PRT_HIT_SHFT                                   0

#define HWIO_XPU_IDR0_ADDR                                              (BAM_NDP_REG_BASE      + 0x00002074)
#define HWIO_XPU_IDR0_PHYS                                              (BAM_NDP_REG_BASE_PHYS + 0x00002074)
#define HWIO_XPU_IDR0_RMSK                                              0xc000bfff
#define HWIO_XPU_IDR0_SHFT                                                       0
#define HWIO_XPU_IDR0_IN                                                \
        in_dword_masked(HWIO_XPU_IDR0_ADDR, HWIO_XPU_IDR0_RMSK)
#define HWIO_XPU_IDR0_INM(m)                                            \
        in_dword_masked(HWIO_XPU_IDR0_ADDR, m)
#define HWIO_XPU_IDR0_CLIENTREQ_HALT_ACK_HW_EN_BMSK                     0x80000000
#define HWIO_XPU_IDR0_CLIENTREQ_HALT_ACK_HW_EN_SHFT                           0x1f
#define HWIO_XPU_IDR0_SAVERESTORE_HW_EN_BMSK                            0x40000000
#define HWIO_XPU_IDR0_SAVERESTORE_HW_EN_SHFT                                  0x1e
#define HWIO_XPU_IDR0_BLED_BMSK                                             0x8000
#define HWIO_XPU_IDR0_BLED_SHFT                                                0xf
#define HWIO_XPU_IDR0_XPUT_BMSK                                             0x3000
#define HWIO_XPU_IDR0_XPUT_SHFT                                                0xc
#define HWIO_XPU_IDR0_PT_BMSK                                                0x800
#define HWIO_XPU_IDR0_PT_SHFT                                                  0xb
#define HWIO_XPU_IDR0_MV_BMSK                                                0x400
#define HWIO_XPU_IDR0_MV_SHFT                                                  0xa
#define HWIO_XPU_IDR0_NRG_BMSK                                               0x3ff
#define HWIO_XPU_IDR0_NRG_SHFT                                                   0

#define HWIO_XPU_IDR1_ADDR                                              (BAM_NDP_REG_BASE      + 0x00002078)
#define HWIO_XPU_IDR1_PHYS                                              (BAM_NDP_REG_BASE_PHYS + 0x00002078)
#define HWIO_XPU_IDR1_RMSK                                              0x7f3ffeff
#define HWIO_XPU_IDR1_SHFT                                                       0
#define HWIO_XPU_IDR1_IN                                                \
        in_dword_masked(HWIO_XPU_IDR1_ADDR, HWIO_XPU_IDR1_RMSK)
#define HWIO_XPU_IDR1_INM(m)                                            \
        in_dword_masked(HWIO_XPU_IDR1_ADDR, m)
#define HWIO_XPU_IDR1_AMT_HW_ENABLE_BMSK                                0x40000000
#define HWIO_XPU_IDR1_AMT_HW_ENABLE_SHFT                                      0x1e
#define HWIO_XPU_IDR1_CLIENT_ADDR_WIDTH_BMSK                            0x3f000000
#define HWIO_XPU_IDR1_CLIENT_ADDR_WIDTH_SHFT                                  0x18
#define HWIO_XPU_IDR1_CONFIG_ADDR_WIDTH_BMSK                              0x3f0000
#define HWIO_XPU_IDR1_CONFIG_ADDR_WIDTH_SHFT                                  0x10
#define HWIO_XPU_IDR1_QRIB_EN_BMSK                                          0x8000
#define HWIO_XPU_IDR1_QRIB_EN_SHFT                                             0xf
#define HWIO_XPU_IDR1_ASYNC_MODE_BMSK                                       0x4000
#define HWIO_XPU_IDR1_ASYNC_MODE_SHFT                                          0xe
#define HWIO_XPU_IDR1_CONFIG_TYPE_BMSK                                      0x2000
#define HWIO_XPU_IDR1_CONFIG_TYPE_SHFT                                         0xd
#define HWIO_XPU_IDR1_CLIENT_PIPELINE_ENABLED_BMSK                          0x1000
#define HWIO_XPU_IDR1_CLIENT_PIPELINE_ENABLED_SHFT                             0xc
#define HWIO_XPU_IDR1_MSA_CHECK_HW_ENABLE_BMSK                               0x800
#define HWIO_XPU_IDR1_MSA_CHECK_HW_ENABLE_SHFT                                 0xb
#define HWIO_XPU_IDR1_XPU_SYND_REG_ABSENT_BMSK                               0x400
#define HWIO_XPU_IDR1_XPU_SYND_REG_ABSENT_SHFT                                 0xa
#define HWIO_XPU_IDR1_TZXPU_BMSK                                             0x200
#define HWIO_XPU_IDR1_TZXPU_SHFT                                               0x9
#define HWIO_XPU_IDR1_NVMID_BMSK                                              0xff
#define HWIO_XPU_IDR1_NVMID_SHFT                                                 0

#define HWIO_XPU_REV_ADDR                                               (BAM_NDP_REG_BASE      + 0x0000207c)
#define HWIO_XPU_REV_PHYS                                               (BAM_NDP_REG_BASE_PHYS + 0x0000207c)
#define HWIO_XPU_REV_RMSK                                               0xffffffff
#define HWIO_XPU_REV_SHFT                                                        0
#define HWIO_XPU_REV_IN                                                 \
        in_dword_masked(HWIO_XPU_REV_ADDR, HWIO_XPU_REV_RMSK)
#define HWIO_XPU_REV_INM(m)                                             \
        in_dword_masked(HWIO_XPU_REV_ADDR, m)
#define HWIO_XPU_REV_MAJOR_BMSK                                         0xf0000000
#define HWIO_XPU_REV_MAJOR_SHFT                                               0x1c
#define HWIO_XPU_REV_MINOR_BMSK                                          0xfff0000
#define HWIO_XPU_REV_MINOR_SHFT                                               0x10
#define HWIO_XPU_REV_STEP_BMSK                                              0xffff
#define HWIO_XPU_REV_STEP_SHFT                                                   0

#define HWIO_XPU_RGn_RACRm_ADDR(m,n)                                    (BAM_NDP_REG_BASE      + 0x00002200 + 0x80 * (n)+0x4 * (m))
#define HWIO_XPU_RGn_RACRm_PHYS(m,n)                                    (BAM_NDP_REG_BASE_PHYS + 0x00002200 + 0x80 * (n)+0x4 * (m))
#define HWIO_XPU_RGn_RACRm_RMSK                                         0xffffffff
#define HWIO_XPU_RGn_RACRm_SHFT                                                  0
#define HWIO_XPU_RGn_RACRm_MAXm                                                  0
#define HWIO_XPU_RGn_RACRm_MAXn                                               0x25
#define HWIO_XPU_RGn_RACRm_INI2(m,n) \
        in_dword(HWIO_XPU_RGn_RACRm_ADDR(m,n))
#define HWIO_XPU_RGn_RACRm_INMI2(m,n,mask) \
        in_dword_masked(HWIO_XPU_RGn_RACRm_ADDR(m,n), mask)
#define HWIO_XPU_RGn_RACRm_OUTI2(m,n,val) \
        out_dword(HWIO_XPU_RGn_RACRm_ADDR(m,n),val)
#define HWIO_XPU_RGn_RACRm_OUTMI2(m,n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_RGn_RACRm_ADDR(m,n),mask,val,HWIO_XPU_RGn_RACRm_INI2(m,n));\
        HWIO_INTFREE()
#define HWIO_XPU_RGn_RACRm_RWE_BMSK                                     0xffffffff
#define HWIO_XPU_RGn_RACRm_RWE_SHFT                                              0

#define HWIO_XPU_RGn_SCR_ADDR(n)                                        (BAM_NDP_REG_BASE      + 0x00002250 + 0x80 * (n))
#define HWIO_XPU_RGn_SCR_PHYS(n)                                        (BAM_NDP_REG_BASE_PHYS + 0x00002250 + 0x80 * (n))
#define HWIO_XPU_RGn_SCR_RMSK                                                 0x3f
#define HWIO_XPU_RGn_SCR_SHFT                                                    0
#define HWIO_XPU_RGn_SCR_MAXn                                                 0x25
#define HWIO_XPU_RGn_SCR_INI(n) \
        in_dword(HWIO_XPU_RGn_SCR_ADDR(n))
#define HWIO_XPU_RGn_SCR_INMI(n,mask) \
        in_dword_masked(HWIO_XPU_RGn_SCR_ADDR(n), mask)
#define HWIO_XPU_RGn_SCR_OUTI(n,val) \
        out_dword(HWIO_XPU_RGn_SCR_ADDR(n),val)
#define HWIO_XPU_RGn_SCR_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_RGn_SCR_ADDR(n),mask,val,HWIO_XPU_RGn_SCR_INI(n));\
        HWIO_INTFREE()
#define HWIO_XPU_RGn_SCR_SCLROE_BMSK                                          0x20
#define HWIO_XPU_RGn_SCR_SCLROE_SHFT                                           0x5
#define HWIO_XPU_RGn_SCR_VMIDCLROE_BMSK                                       0x10
#define HWIO_XPU_RGn_SCR_VMIDCLROE_SHFT                                        0x4
#define HWIO_XPU_RGn_SCR_MSACLROE_BMSK                                         0x8
#define HWIO_XPU_RGn_SCR_MSACLROE_SHFT                                         0x3
#define HWIO_XPU_RGn_SCR_VMIDCLRWE_BMSK                                        0x4
#define HWIO_XPU_RGn_SCR_VMIDCLRWE_SHFT                                        0x2
#define HWIO_XPU_RGn_SCR_MSACLRWE_BMSK                                         0x2
#define HWIO_XPU_RGn_SCR_MSACLRWE_SHFT                                         0x1
#define HWIO_XPU_RGn_SCR_NS_BMSK                                               0x1
#define HWIO_XPU_RGn_SCR_NS_SHFT                                                 0

#define HWIO_XPU_RGn_MCR_ADDR(n)                                        (BAM_NDP_REG_BASE      + 0x00002254 + 0x80 * (n))
#define HWIO_XPU_RGn_MCR_PHYS(n)                                        (BAM_NDP_REG_BASE_PHYS + 0x00002254 + 0x80 * (n))
#define HWIO_XPU_RGn_MCR_RMSK                                                 0x3f
#define HWIO_XPU_RGn_MCR_SHFT                                                    0
#define HWIO_XPU_RGn_MCR_MAXn                                                 0x25
#define HWIO_XPU_RGn_MCR_INI(n) \
        in_dword(HWIO_XPU_RGn_MCR_ADDR(n))
#define HWIO_XPU_RGn_MCR_INMI(n,mask) \
        in_dword_masked(HWIO_XPU_RGn_MCR_ADDR(n), mask)
#define HWIO_XPU_RGn_MCR_OUTI(n,val) \
        out_dword(HWIO_XPU_RGn_MCR_ADDR(n),val)
#define HWIO_XPU_RGn_MCR_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_XPU_RGn_MCR_ADDR(n),mask,val,HWIO_XPU_RGn_MCR_INI(n));\
        HWIO_INTFREE()
#define HWIO_XPU_RGn_MCR_MSACLROE_BMSK                                        0x20
#define HWIO_XPU_RGn_MCR_MSACLROE_SHFT                                         0x5
#define HWIO_XPU_RGn_MCR_VMIDCLROE_BMSK                                       0x10
#define HWIO_XPU_RGn_MCR_VMIDCLROE_SHFT                                        0x4
#define HWIO_XPU_RGn_MCR_SCLROE_BMSK                                           0x8
#define HWIO_XPU_RGn_MCR_SCLROE_SHFT                                           0x3
#define HWIO_XPU_RGn_MCR_VMIDCLE_BMSK                                          0x4
#define HWIO_XPU_RGn_MCR_VMIDCLE_SHFT                                          0x2
#define HWIO_XPU_RGn_MCR_SCLE_BMSK                                             0x2
#define HWIO_XPU_RGn_MCR_SCLE_SHFT                                             0x1
#define HWIO_XPU_RGn_MCR_MSAE_BMSK                                             0x1
#define HWIO_XPU_RGn_MCR_MSAE_SHFT                                               0

/*------------------------------------------------------------------------------
 * MODULE: ipa_vmidmt
 *------------------------------------------------------------------------------*/

#define IPA_VMIDMT_REG_BASE                                             (IPA_WRAPPER_BASE + 0x00019000)
#define IPA_VMIDMT_REG_BASE_PHYS                                        0x00019000

#define HWIO_IPA_VMIDMT_SCR0_ADDR                                       (IPA_VMIDMT_REG_BASE      + 00000000)
#define HWIO_IPA_VMIDMT_SCR0_PHYS                                       (IPA_VMIDMT_REG_BASE_PHYS + 00000000)
#define HWIO_IPA_VMIDMT_SCR0_RMSK                                       0x3ff707f5
#define HWIO_IPA_VMIDMT_SCR0_SHFT                                                0
#define HWIO_IPA_VMIDMT_SCR0_IN                                         \
        in_dword_masked(HWIO_IPA_VMIDMT_SCR0_ADDR, HWIO_IPA_VMIDMT_SCR0_RMSK)
#define HWIO_IPA_VMIDMT_SCR0_INM(m)                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_SCR0_ADDR, m)
#define HWIO_IPA_VMIDMT_SCR0_OUT(v)                                     \
        out_dword(HWIO_IPA_VMIDMT_SCR0_ADDR,v)
#define HWIO_IPA_VMIDMT_SCR0_OUTM(m,v)                                  \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_SCR0_ADDR,m,v,HWIO_IPA_VMIDMT_SCR0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_SCR0_NSCFG_BMSK                                 0x30000000
#define HWIO_IPA_VMIDMT_SCR0_NSCFG_SHFT                                       0x1c
#define HWIO_IPA_VMIDMT_SCR0_WACFG_BMSK                                  0xc000000
#define HWIO_IPA_VMIDMT_SCR0_WACFG_SHFT                                       0x1a
#define HWIO_IPA_VMIDMT_SCR0_RACFG_BMSK                                  0x3000000
#define HWIO_IPA_VMIDMT_SCR0_RACFG_SHFT                                       0x18
#define HWIO_IPA_VMIDMT_SCR0_SHCFG_BMSK                                   0xc00000
#define HWIO_IPA_VMIDMT_SCR0_SHCFG_SHFT                                       0x16
#define HWIO_IPA_VMIDMT_SCR0_SMCFCFG_BMSK                                 0x200000
#define HWIO_IPA_VMIDMT_SCR0_SMCFCFG_SHFT                                     0x15
#define HWIO_IPA_VMIDMT_SCR0_MTCFG_BMSK                                   0x100000
#define HWIO_IPA_VMIDMT_SCR0_MTCFG_SHFT                                       0x14
#define HWIO_IPA_VMIDMT_SCR0_MEMATTR_BMSK                                  0x70000
#define HWIO_IPA_VMIDMT_SCR0_MEMATTR_SHFT                                     0x10
#define HWIO_IPA_VMIDMT_SCR0_USFCFG_BMSK                                     0x400
#define HWIO_IPA_VMIDMT_SCR0_USFCFG_SHFT                                       0xa
#define HWIO_IPA_VMIDMT_SCR0_GSE_BMSK                                        0x200
#define HWIO_IPA_VMIDMT_SCR0_GSE_SHFT                                          0x9
#define HWIO_IPA_VMIDMT_SCR0_STALLD_BMSK                                     0x100
#define HWIO_IPA_VMIDMT_SCR0_STALLD_SHFT                                       0x8
#define HWIO_IPA_VMIDMT_SCR0_TRANSIENTCFG_BMSK                                0xc0
#define HWIO_IPA_VMIDMT_SCR0_TRANSIENTCFG_SHFT                                 0x6
#define HWIO_IPA_VMIDMT_SCR0_GCFGFIE_BMSK                                     0x20
#define HWIO_IPA_VMIDMT_SCR0_GCFGFIE_SHFT                                      0x5
#define HWIO_IPA_VMIDMT_SCR0_GCFGERE_BMSK                                     0x10
#define HWIO_IPA_VMIDMT_SCR0_GCFGERE_SHFT                                      0x4
#define HWIO_IPA_VMIDMT_SCR0_GFIE_BMSK                                         0x4
#define HWIO_IPA_VMIDMT_SCR0_GFIE_SHFT                                         0x2
#define HWIO_IPA_VMIDMT_SCR0_CLIENTPD_BMSK                                     0x1
#define HWIO_IPA_VMIDMT_SCR0_CLIENTPD_SHFT                                       0

#define HWIO_IPA_VMIDMT_SCR1_ADDR                                       (IPA_VMIDMT_REG_BASE      + 0x00000004)
#define HWIO_IPA_VMIDMT_SCR1_PHYS                                       (IPA_VMIDMT_REG_BASE_PHYS + 0x00000004)
#define HWIO_IPA_VMIDMT_SCR1_RMSK                                        0x1003f00
#define HWIO_IPA_VMIDMT_SCR1_SHFT                                                0
#define HWIO_IPA_VMIDMT_SCR1_IN                                         \
        in_dword_masked(HWIO_IPA_VMIDMT_SCR1_ADDR, HWIO_IPA_VMIDMT_SCR1_RMSK)
#define HWIO_IPA_VMIDMT_SCR1_INM(m)                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_SCR1_ADDR, m)
#define HWIO_IPA_VMIDMT_SCR1_OUT(v)                                     \
        out_dword(HWIO_IPA_VMIDMT_SCR1_ADDR,v)
#define HWIO_IPA_VMIDMT_SCR1_OUTM(m,v)                                  \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_SCR1_ADDR,m,v,HWIO_IPA_VMIDMT_SCR1_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_SCR1_GASRAE_BMSK                                 0x1000000
#define HWIO_IPA_VMIDMT_SCR1_GASRAE_SHFT                                      0x18
#define HWIO_IPA_VMIDMT_SCR1_NSNUMSMRGO_BMSK                                0x3f00
#define HWIO_IPA_VMIDMT_SCR1_NSNUMSMRGO_SHFT                                   0x8

#define HWIO_IPA_VMIDMT_SCR2_ADDR                                       (IPA_VMIDMT_REG_BASE      + 0x00000008)
#define HWIO_IPA_VMIDMT_SCR2_PHYS                                       (IPA_VMIDMT_REG_BASE_PHYS + 0x00000008)
#define HWIO_IPA_VMIDMT_SCR2_RMSK                                             0x1f
#define HWIO_IPA_VMIDMT_SCR2_SHFT                                                0
#define HWIO_IPA_VMIDMT_SCR2_IN                                         \
        in_dword_masked(HWIO_IPA_VMIDMT_SCR2_ADDR, HWIO_IPA_VMIDMT_SCR2_RMSK)
#define HWIO_IPA_VMIDMT_SCR2_INM(m)                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_SCR2_ADDR, m)
#define HWIO_IPA_VMIDMT_SCR2_OUT(v)                                     \
        out_dword(HWIO_IPA_VMIDMT_SCR2_ADDR,v)
#define HWIO_IPA_VMIDMT_SCR2_OUTM(m,v)                                  \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_SCR2_ADDR,m,v,HWIO_IPA_VMIDMT_SCR2_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_SCR2_BPVMID_BMSK                                      0x1f
#define HWIO_IPA_VMIDMT_SCR2_BPVMID_SHFT                                         0

#define HWIO_IPA_VMIDMT_SACR_ADDR                                       (IPA_VMIDMT_REG_BASE      + 0x00000010)
#define HWIO_IPA_VMIDMT_SACR_PHYS                                       (IPA_VMIDMT_REG_BASE_PHYS + 0x00000010)
#define HWIO_IPA_VMIDMT_SACR_RMSK                                       0x70000013
#define HWIO_IPA_VMIDMT_SACR_SHFT                                                0
#define HWIO_IPA_VMIDMT_SACR_IN                                         \
        in_dword_masked(HWIO_IPA_VMIDMT_SACR_ADDR, HWIO_IPA_VMIDMT_SACR_RMSK)
#define HWIO_IPA_VMIDMT_SACR_INM(m)                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_SACR_ADDR, m)
#define HWIO_IPA_VMIDMT_SACR_OUT(v)                                     \
        out_dword(HWIO_IPA_VMIDMT_SACR_ADDR,v)
#define HWIO_IPA_VMIDMT_SACR_OUTM(m,v)                                  \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_SACR_ADDR,m,v,HWIO_IPA_VMIDMT_SACR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_SACR_BPRCNSH_BMSK                               0x40000000
#define HWIO_IPA_VMIDMT_SACR_BPRCNSH_SHFT                                     0x1e
#define HWIO_IPA_VMIDMT_SACR_BPRCISH_BMSK                               0x20000000
#define HWIO_IPA_VMIDMT_SACR_BPRCISH_SHFT                                     0x1d
#define HWIO_IPA_VMIDMT_SACR_BPRCOSH_BMSK                               0x10000000
#define HWIO_IPA_VMIDMT_SACR_BPRCOSH_SHFT                                     0x1c
#define HWIO_IPA_VMIDMT_SACR_BPREQPRIORITYCFG_BMSK                            0x10
#define HWIO_IPA_VMIDMT_SACR_BPREQPRIORITYCFG_SHFT                             0x4
#define HWIO_IPA_VMIDMT_SACR_BPREQPRIORITY_BMSK                                0x3
#define HWIO_IPA_VMIDMT_SACR_BPREQPRIORITY_SHFT                                  0

#define HWIO_IPA_VMIDMT_SIDR0_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000020)
#define HWIO_IPA_VMIDMT_SIDR0_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000020)
#define HWIO_IPA_VMIDMT_SIDR0_RMSK                                      0x88001eff
#define HWIO_IPA_VMIDMT_SIDR0_SHFT                                               0
#define HWIO_IPA_VMIDMT_SIDR0_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR0_ADDR, HWIO_IPA_VMIDMT_SIDR0_RMSK)
#define HWIO_IPA_VMIDMT_SIDR0_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR0_ADDR, m)
#define HWIO_IPA_VMIDMT_SIDR0_SES_BMSK                                  0x80000000
#define HWIO_IPA_VMIDMT_SIDR0_SES_SHFT                                        0x1f
#define HWIO_IPA_VMIDMT_SIDR0_SMS_BMSK                                   0x8000000
#define HWIO_IPA_VMIDMT_SIDR0_SMS_SHFT                                        0x1b
#define HWIO_IPA_VMIDMT_SIDR0_NUMSIDB_BMSK                                  0x1e00
#define HWIO_IPA_VMIDMT_SIDR0_NUMSIDB_SHFT                                     0x9
#define HWIO_IPA_VMIDMT_SIDR0_NUMSMRG_BMSK                                    0xff
#define HWIO_IPA_VMIDMT_SIDR0_NUMSMRG_SHFT                                       0

#define HWIO_IPA_VMIDMT_SIDR1_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000024)
#define HWIO_IPA_VMIDMT_SIDR1_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000024)
#define HWIO_IPA_VMIDMT_SIDR1_RMSK                                          0x9f00
#define HWIO_IPA_VMIDMT_SIDR1_SHFT                                               0
#define HWIO_IPA_VMIDMT_SIDR1_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR1_ADDR, HWIO_IPA_VMIDMT_SIDR1_RMSK)
#define HWIO_IPA_VMIDMT_SIDR1_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR1_ADDR, m)
#define HWIO_IPA_VMIDMT_SIDR1_SMCD_BMSK                                     0x8000
#define HWIO_IPA_VMIDMT_SIDR1_SMCD_SHFT                                        0xf
#define HWIO_IPA_VMIDMT_SIDR1_SSDTP_BMSK                                    0x1000
#define HWIO_IPA_VMIDMT_SIDR1_SSDTP_SHFT                                       0xc
#define HWIO_IPA_VMIDMT_SIDR1_NUMSSDNDX_BMSK                                 0xf00
#define HWIO_IPA_VMIDMT_SIDR1_NUMSSDNDX_SHFT                                   0x8

#define HWIO_IPA_VMIDMT_SIDR2_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000028)
#define HWIO_IPA_VMIDMT_SIDR2_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000028)
#define HWIO_IPA_VMIDMT_SIDR2_RMSK                                            0xff
#define HWIO_IPA_VMIDMT_SIDR2_SHFT                                               0
#define HWIO_IPA_VMIDMT_SIDR2_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR2_ADDR, HWIO_IPA_VMIDMT_SIDR2_RMSK)
#define HWIO_IPA_VMIDMT_SIDR2_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR2_ADDR, m)
#define HWIO_IPA_VMIDMT_SIDR2_OAS_BMSK                                        0xf0
#define HWIO_IPA_VMIDMT_SIDR2_OAS_SHFT                                         0x4
#define HWIO_IPA_VMIDMT_SIDR2_IAS_BMSK                                         0xf
#define HWIO_IPA_VMIDMT_SIDR2_IAS_SHFT                                           0

#define HWIO_IPA_VMIDMT_SIDR4_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000030)
#define HWIO_IPA_VMIDMT_SIDR4_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000030)
#define HWIO_IPA_VMIDMT_SIDR4_RMSK                                      0xffffffff
#define HWIO_IPA_VMIDMT_SIDR4_SHFT                                               0
#define HWIO_IPA_VMIDMT_SIDR4_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR4_ADDR, HWIO_IPA_VMIDMT_SIDR4_RMSK)
#define HWIO_IPA_VMIDMT_SIDR4_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR4_ADDR, m)
#define HWIO_IPA_VMIDMT_SIDR4_MAJOR_BMSK                                0xf0000000
#define HWIO_IPA_VMIDMT_SIDR4_MAJOR_SHFT                                      0x1c
#define HWIO_IPA_VMIDMT_SIDR4_MINOR_BMSK                                 0xfff0000
#define HWIO_IPA_VMIDMT_SIDR4_MINOR_SHFT                                      0x10
#define HWIO_IPA_VMIDMT_SIDR4_STEP_BMSK                                     0xffff
#define HWIO_IPA_VMIDMT_SIDR4_STEP_SHFT                                          0

#define HWIO_IPA_VMIDMT_SIDR5_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000034)
#define HWIO_IPA_VMIDMT_SIDR5_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000034)
#define HWIO_IPA_VMIDMT_SIDR5_RMSK                                        0xff03ff
#define HWIO_IPA_VMIDMT_SIDR5_SHFT                                               0
#define HWIO_IPA_VMIDMT_SIDR5_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR5_ADDR, HWIO_IPA_VMIDMT_SIDR5_RMSK)
#define HWIO_IPA_VMIDMT_SIDR5_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR5_ADDR, m)
#define HWIO_IPA_VMIDMT_SIDR5_NUMMSDRB_BMSK                               0xff0000
#define HWIO_IPA_VMIDMT_SIDR5_NUMMSDRB_SHFT                                   0x10
#define HWIO_IPA_VMIDMT_SIDR5_MSAE_BMSK                                      0x200
#define HWIO_IPA_VMIDMT_SIDR5_MSAE_SHFT                                        0x9
#define HWIO_IPA_VMIDMT_SIDR5_QRIBE_BMSK                                     0x100
#define HWIO_IPA_VMIDMT_SIDR5_QRIBE_SHFT                                       0x8
#define HWIO_IPA_VMIDMT_SIDR5_NVMID_BMSK                                      0xff
#define HWIO_IPA_VMIDMT_SIDR5_NVMID_SHFT                                         0

#define HWIO_IPA_VMIDMT_SIDR7_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x0000003c)
#define HWIO_IPA_VMIDMT_SIDR7_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x0000003c)
#define HWIO_IPA_VMIDMT_SIDR7_RMSK                                            0xff
#define HWIO_IPA_VMIDMT_SIDR7_SHFT                                               0
#define HWIO_IPA_VMIDMT_SIDR7_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR7_ADDR, HWIO_IPA_VMIDMT_SIDR7_RMSK)
#define HWIO_IPA_VMIDMT_SIDR7_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_SIDR7_ADDR, m)
#define HWIO_IPA_VMIDMT_SIDR7_MAJOR_BMSK                                      0xf0
#define HWIO_IPA_VMIDMT_SIDR7_MAJOR_SHFT                                       0x4
#define HWIO_IPA_VMIDMT_SIDR7_MINOR_BMSK                                       0xf
#define HWIO_IPA_VMIDMT_SIDR7_MINOR_SHFT                                         0

#define HWIO_IPA_VMIDMT_SGFAR0_ADDR                                     (IPA_VMIDMT_REG_BASE      + 0x00000040)
#define HWIO_IPA_VMIDMT_SGFAR0_PHYS                                     (IPA_VMIDMT_REG_BASE_PHYS + 0x00000040)
#define HWIO_IPA_VMIDMT_SGFAR0_RMSK                                     0xffffffff
#define HWIO_IPA_VMIDMT_SGFAR0_SHFT                                              0
#define HWIO_IPA_VMIDMT_SGFAR0_IN                                       \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFAR0_ADDR, HWIO_IPA_VMIDMT_SGFAR0_RMSK)
#define HWIO_IPA_VMIDMT_SGFAR0_INM(m)                                   \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFAR0_ADDR, m)
#define HWIO_IPA_VMIDMT_SGFAR0_SGFEA0_BMSK                              0xffffffff
#define HWIO_IPA_VMIDMT_SGFAR0_SGFEA0_SHFT                                       0

#define HWIO_IPA_VMIDMT_SGFSR_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000048)
#define HWIO_IPA_VMIDMT_SGFSR_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000048)
#define HWIO_IPA_VMIDMT_SGFSR_RMSK                                      0xc0000026
#define HWIO_IPA_VMIDMT_SGFSR_SHFT                                               0
#define HWIO_IPA_VMIDMT_SGFSR_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFSR_ADDR, HWIO_IPA_VMIDMT_SGFSR_RMSK)
#define HWIO_IPA_VMIDMT_SGFSR_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFSR_ADDR, m)
#define HWIO_IPA_VMIDMT_SGFSR_OUT(v)                                    \
        out_dword(HWIO_IPA_VMIDMT_SGFSR_ADDR,v)
#define HWIO_IPA_VMIDMT_SGFSR_OUTM(m,v)                                 \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_SGFSR_ADDR,m,v,HWIO_IPA_VMIDMT_SGFSR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_SGFSR_MULTI_CLIENT_BMSK                         0x80000000
#define HWIO_IPA_VMIDMT_SGFSR_MULTI_CLIENT_SHFT                               0x1f
#define HWIO_IPA_VMIDMT_SGFSR_MULTI_CFG_BMSK                            0x40000000
#define HWIO_IPA_VMIDMT_SGFSR_MULTI_CFG_SHFT                                  0x1e
#define HWIO_IPA_VMIDMT_SGFSR_CAF_BMSK                                        0x20
#define HWIO_IPA_VMIDMT_SGFSR_CAF_SHFT                                         0x5
#define HWIO_IPA_VMIDMT_SGFSR_SMCF_BMSK                                        0x4
#define HWIO_IPA_VMIDMT_SGFSR_SMCF_SHFT                                        0x2
#define HWIO_IPA_VMIDMT_SGFSR_USF_BMSK                                         0x2
#define HWIO_IPA_VMIDMT_SGFSR_USF_SHFT                                         0x1

#define HWIO_IPA_VMIDMT_SGFSRRESTORE_ADDR                               (IPA_VMIDMT_REG_BASE      + 0x0000004c)
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_PHYS                               (IPA_VMIDMT_REG_BASE_PHYS + 0x0000004c)
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_RMSK                               0xc0000026
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_SHFT                                        0
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_IN                                 \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFSRRESTORE_ADDR, HWIO_IPA_VMIDMT_SGFSRRESTORE_RMSK)
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_INM(m)                             \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFSRRESTORE_ADDR, m)
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_OUT(v)                             \
        out_dword(HWIO_IPA_VMIDMT_SGFSRRESTORE_ADDR,v)
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_OUTM(m,v)                          \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_SGFSRRESTORE_ADDR,m,v,HWIO_IPA_VMIDMT_SGFSRRESTORE_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_MULTI_CLIENT_BMSK                  0x80000000
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_MULTI_CLIENT_SHFT                        0x1f
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_MULTI_CFG_BMSK                     0x40000000
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_MULTI_CFG_SHFT                           0x1e
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_CAF_BMSK                                 0x20
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_CAF_SHFT                                  0x5
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_SMCF_BMSK                                 0x4
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_SMCF_SHFT                                 0x2
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_USF_BMSK                                  0x2
#define HWIO_IPA_VMIDMT_SGFSRRESTORE_USF_SHFT                                  0x1

#define HWIO_IPA_VMIDMT_SGFSYNDR0_ADDR                                  (IPA_VMIDMT_REG_BASE      + 0x00000050)
#define HWIO_IPA_VMIDMT_SGFSYNDR0_PHYS                                  (IPA_VMIDMT_REG_BASE_PHYS + 0x00000050)
#define HWIO_IPA_VMIDMT_SGFSYNDR0_RMSK                                       0x132
#define HWIO_IPA_VMIDMT_SGFSYNDR0_SHFT                                           0
#define HWIO_IPA_VMIDMT_SGFSYNDR0_IN                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFSYNDR0_ADDR, HWIO_IPA_VMIDMT_SGFSYNDR0_RMSK)
#define HWIO_IPA_VMIDMT_SGFSYNDR0_INM(m)                                \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFSYNDR0_ADDR, m)
#define HWIO_IPA_VMIDMT_SGFSYNDR0_MSSSELFAUTH_BMSK                           0x100
#define HWIO_IPA_VMIDMT_SGFSYNDR0_MSSSELFAUTH_SHFT                             0x8
#define HWIO_IPA_VMIDMT_SGFSYNDR0_NSATTR_BMSK                                 0x20
#define HWIO_IPA_VMIDMT_SGFSYNDR0_NSATTR_SHFT                                  0x5
#define HWIO_IPA_VMIDMT_SGFSYNDR0_NSSTATE_BMSK                                0x10
#define HWIO_IPA_VMIDMT_SGFSYNDR0_NSSTATE_SHFT                                 0x4
#define HWIO_IPA_VMIDMT_SGFSYNDR0_WNR_BMSK                                     0x2
#define HWIO_IPA_VMIDMT_SGFSYNDR0_WNR_SHFT                                     0x1

#define HWIO_IPA_VMIDMT_SGFSYNDR1_ADDR                                  (IPA_VMIDMT_REG_BASE      + 0x00000054)
#define HWIO_IPA_VMIDMT_SGFSYNDR1_PHYS                                  (IPA_VMIDMT_REG_BASE_PHYS + 0x00000054)
#define HWIO_IPA_VMIDMT_SGFSYNDR1_RMSK                                  0x1f1f001f
#define HWIO_IPA_VMIDMT_SGFSYNDR1_SHFT                                           0
#define HWIO_IPA_VMIDMT_SGFSYNDR1_IN                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFSYNDR1_ADDR, HWIO_IPA_VMIDMT_SGFSYNDR1_RMSK)
#define HWIO_IPA_VMIDMT_SGFSYNDR1_INM(m)                                \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFSYNDR1_ADDR, m)
#define HWIO_IPA_VMIDMT_SGFSYNDR1_MSDINDEX_BMSK                         0x1f000000
#define HWIO_IPA_VMIDMT_SGFSYNDR1_MSDINDEX_SHFT                               0x18
#define HWIO_IPA_VMIDMT_SGFSYNDR1_SSDINDEX_BMSK                           0x1f0000
#define HWIO_IPA_VMIDMT_SGFSYNDR1_SSDINDEX_SHFT                               0x10
#define HWIO_IPA_VMIDMT_SGFSYNDR1_STREAMINDEX_BMSK                            0x1f
#define HWIO_IPA_VMIDMT_SGFSYNDR1_STREAMINDEX_SHFT                               0

#define HWIO_IPA_VMIDMT_SGFSYNDR2_ADDR                                  (IPA_VMIDMT_REG_BASE      + 0x00000058)
#define HWIO_IPA_VMIDMT_SGFSYNDR2_PHYS                                  (IPA_VMIDMT_REG_BASE_PHYS + 0x00000058)
#define HWIO_IPA_VMIDMT_SGFSYNDR2_RMSK                                   0x71fffff
#define HWIO_IPA_VMIDMT_SGFSYNDR2_SHFT                                           0
#define HWIO_IPA_VMIDMT_SGFSYNDR2_IN                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFSYNDR2_ADDR, HWIO_IPA_VMIDMT_SGFSYNDR2_RMSK)
#define HWIO_IPA_VMIDMT_SGFSYNDR2_INM(m)                                \
        in_dword_masked(HWIO_IPA_VMIDMT_SGFSYNDR2_ADDR, m)
#define HWIO_IPA_VMIDMT_SGFSYNDR2_ATID_BMSK                              0x7000000
#define HWIO_IPA_VMIDMT_SGFSYNDR2_ATID_SHFT                                   0x18
#define HWIO_IPA_VMIDMT_SGFSYNDR2_AVMID_BMSK                              0x1f0000
#define HWIO_IPA_VMIDMT_SGFSYNDR2_AVMID_SHFT                                  0x10
#define HWIO_IPA_VMIDMT_SGFSYNDR2_ABID_BMSK                                 0xe000
#define HWIO_IPA_VMIDMT_SGFSYNDR2_ABID_SHFT                                    0xd
#define HWIO_IPA_VMIDMT_SGFSYNDR2_APID_BMSK                                 0x1f00
#define HWIO_IPA_VMIDMT_SGFSYNDR2_APID_SHFT                                    0x8
#define HWIO_IPA_VMIDMT_SGFSYNDR2_AMID_BMSK                                   0xff
#define HWIO_IPA_VMIDMT_SGFSYNDR2_AMID_SHFT                                      0

#define HWIO_IPA_VMIDMT_VMIDMTSCR0_ADDR                                 (IPA_VMIDMT_REG_BASE      + 0x00000090)
#define HWIO_IPA_VMIDMT_VMIDMTSCR0_PHYS                                 (IPA_VMIDMT_REG_BASE_PHYS + 0x00000090)
#define HWIO_IPA_VMIDMT_VMIDMTSCR0_RMSK                                        0x1
#define HWIO_IPA_VMIDMT_VMIDMTSCR0_SHFT                                          0
#define HWIO_IPA_VMIDMT_VMIDMTSCR0_IN                                   \
        in_dword_masked(HWIO_IPA_VMIDMT_VMIDMTSCR0_ADDR, HWIO_IPA_VMIDMT_VMIDMTSCR0_RMSK)
#define HWIO_IPA_VMIDMT_VMIDMTSCR0_INM(m)                               \
        in_dword_masked(HWIO_IPA_VMIDMT_VMIDMTSCR0_ADDR, m)
#define HWIO_IPA_VMIDMT_VMIDMTSCR0_OUT(v)                               \
        out_dword(HWIO_IPA_VMIDMT_VMIDMTSCR0_ADDR,v)
#define HWIO_IPA_VMIDMT_VMIDMTSCR0_OUTM(m,v)                            \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_VMIDMTSCR0_ADDR,m,v,HWIO_IPA_VMIDMT_VMIDMTSCR0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_VMIDMTSCR0_CLKONOFFE_BMSK                              0x1
#define HWIO_IPA_VMIDMT_VMIDMTSCR0_CLKONOFFE_SHFT                                0

#define HWIO_IPA_VMIDMT_CR0_ADDR                                        (IPA_VMIDMT_REG_BASE      + 00000000)
#define HWIO_IPA_VMIDMT_CR0_PHYS                                        (IPA_VMIDMT_REG_BASE_PHYS + 00000000)
#define HWIO_IPA_VMIDMT_CR0_RMSK                                         0xff70ff5
#define HWIO_IPA_VMIDMT_CR0_SHFT                                                 0
#define HWIO_IPA_VMIDMT_CR0_IN                                          \
        in_dword_masked(HWIO_IPA_VMIDMT_CR0_ADDR, HWIO_IPA_VMIDMT_CR0_RMSK)
#define HWIO_IPA_VMIDMT_CR0_INM(m)                                      \
        in_dword_masked(HWIO_IPA_VMIDMT_CR0_ADDR, m)
#define HWIO_IPA_VMIDMT_CR0_OUT(v)                                      \
        out_dword(HWIO_IPA_VMIDMT_CR0_ADDR,v)
#define HWIO_IPA_VMIDMT_CR0_OUTM(m,v)                                   \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_CR0_ADDR,m,v,HWIO_IPA_VMIDMT_CR0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_CR0_WACFG_BMSK                                   0xc000000
#define HWIO_IPA_VMIDMT_CR0_WACFG_SHFT                                        0x1a
#define HWIO_IPA_VMIDMT_CR0_RACFG_BMSK                                   0x3000000
#define HWIO_IPA_VMIDMT_CR0_RACFG_SHFT                                        0x18
#define HWIO_IPA_VMIDMT_CR0_SHCFG_BMSK                                    0xc00000
#define HWIO_IPA_VMIDMT_CR0_SHCFG_SHFT                                        0x16
#define HWIO_IPA_VMIDMT_CR0_SMCFCFG_BMSK                                  0x200000
#define HWIO_IPA_VMIDMT_CR0_SMCFCFG_SHFT                                      0x15
#define HWIO_IPA_VMIDMT_CR0_MTCFG_BMSK                                    0x100000
#define HWIO_IPA_VMIDMT_CR0_MTCFG_SHFT                                        0x14
#define HWIO_IPA_VMIDMT_CR0_MEMATTR_BMSK                                   0x70000
#define HWIO_IPA_VMIDMT_CR0_MEMATTR_SHFT                                      0x10
#define HWIO_IPA_VMIDMT_CR0_VMIDPNE_BMSK                                     0x800
#define HWIO_IPA_VMIDMT_CR0_VMIDPNE_SHFT                                       0xb
#define HWIO_IPA_VMIDMT_CR0_USFCFG_BMSK                                      0x400
#define HWIO_IPA_VMIDMT_CR0_USFCFG_SHFT                                        0xa
#define HWIO_IPA_VMIDMT_CR0_GSE_BMSK                                         0x200
#define HWIO_IPA_VMIDMT_CR0_GSE_SHFT                                           0x9
#define HWIO_IPA_VMIDMT_CR0_STALLD_BMSK                                      0x100
#define HWIO_IPA_VMIDMT_CR0_STALLD_SHFT                                        0x8
#define HWIO_IPA_VMIDMT_CR0_TRANSIENTCFG_BMSK                                 0xc0
#define HWIO_IPA_VMIDMT_CR0_TRANSIENTCFG_SHFT                                  0x6
#define HWIO_IPA_VMIDMT_CR0_GCFGFIE_BMSK                                      0x20
#define HWIO_IPA_VMIDMT_CR0_GCFGFIE_SHFT                                       0x5
#define HWIO_IPA_VMIDMT_CR0_GCFGERE_BMSK                                      0x10
#define HWIO_IPA_VMIDMT_CR0_GCFGERE_SHFT                                       0x4
#define HWIO_IPA_VMIDMT_CR0_GFIE_BMSK                                          0x4
#define HWIO_IPA_VMIDMT_CR0_GFIE_SHFT                                          0x2
#define HWIO_IPA_VMIDMT_CR0_CLIENTPD_BMSK                                      0x1
#define HWIO_IPA_VMIDMT_CR0_CLIENTPD_SHFT                                        0

#define HWIO_IPA_VMIDMT_CR2_ADDR                                        (IPA_VMIDMT_REG_BASE      + 0x00000008)
#define HWIO_IPA_VMIDMT_CR2_PHYS                                        (IPA_VMIDMT_REG_BASE_PHYS + 0x00000008)
#define HWIO_IPA_VMIDMT_CR2_RMSK                                              0x1f
#define HWIO_IPA_VMIDMT_CR2_SHFT                                                 0
#define HWIO_IPA_VMIDMT_CR2_IN                                          \
        in_dword_masked(HWIO_IPA_VMIDMT_CR2_ADDR, HWIO_IPA_VMIDMT_CR2_RMSK)
#define HWIO_IPA_VMIDMT_CR2_INM(m)                                      \
        in_dword_masked(HWIO_IPA_VMIDMT_CR2_ADDR, m)
#define HWIO_IPA_VMIDMT_CR2_OUT(v)                                      \
        out_dword(HWIO_IPA_VMIDMT_CR2_ADDR,v)
#define HWIO_IPA_VMIDMT_CR2_OUTM(m,v)                                   \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_CR2_ADDR,m,v,HWIO_IPA_VMIDMT_CR2_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_CR2_BPVMID_BMSK                                       0x1f
#define HWIO_IPA_VMIDMT_CR2_BPVMID_SHFT                                          0

#define HWIO_IPA_VMIDMT_ACR_ADDR                                        (IPA_VMIDMT_REG_BASE      + 0x00000010)
#define HWIO_IPA_VMIDMT_ACR_PHYS                                        (IPA_VMIDMT_REG_BASE_PHYS + 0x00000010)
#define HWIO_IPA_VMIDMT_ACR_RMSK                                        0x70000013
#define HWIO_IPA_VMIDMT_ACR_SHFT                                                 0
#define HWIO_IPA_VMIDMT_ACR_IN                                          \
        in_dword_masked(HWIO_IPA_VMIDMT_ACR_ADDR, HWIO_IPA_VMIDMT_ACR_RMSK)
#define HWIO_IPA_VMIDMT_ACR_INM(m)                                      \
        in_dword_masked(HWIO_IPA_VMIDMT_ACR_ADDR, m)
#define HWIO_IPA_VMIDMT_ACR_OUT(v)                                      \
        out_dword(HWIO_IPA_VMIDMT_ACR_ADDR,v)
#define HWIO_IPA_VMIDMT_ACR_OUTM(m,v)                                   \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_ACR_ADDR,m,v,HWIO_IPA_VMIDMT_ACR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_ACR_BPRCNSH_BMSK                                0x40000000
#define HWIO_IPA_VMIDMT_ACR_BPRCNSH_SHFT                                      0x1e
#define HWIO_IPA_VMIDMT_ACR_BPRCISH_BMSK                                0x20000000
#define HWIO_IPA_VMIDMT_ACR_BPRCISH_SHFT                                      0x1d
#define HWIO_IPA_VMIDMT_ACR_BPRCOSH_BMSK                                0x10000000
#define HWIO_IPA_VMIDMT_ACR_BPRCOSH_SHFT                                      0x1c
#define HWIO_IPA_VMIDMT_ACR_BPREQPRIORITYCFG_BMSK                             0x10
#define HWIO_IPA_VMIDMT_ACR_BPREQPRIORITYCFG_SHFT                              0x4
#define HWIO_IPA_VMIDMT_ACR_BPREQPRIORITY_BMSK                                 0x3
#define HWIO_IPA_VMIDMT_ACR_BPREQPRIORITY_SHFT                                   0

#define HWIO_IPA_VMIDMT_IDR0_ADDR                                       (IPA_VMIDMT_REG_BASE      + 0x00000020)
#define HWIO_IPA_VMIDMT_IDR0_PHYS                                       (IPA_VMIDMT_REG_BASE_PHYS + 0x00000020)
#define HWIO_IPA_VMIDMT_IDR0_RMSK                                        0x8001eff
#define HWIO_IPA_VMIDMT_IDR0_SHFT                                                0
#define HWIO_IPA_VMIDMT_IDR0_IN                                         \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR0_ADDR, HWIO_IPA_VMIDMT_IDR0_RMSK)
#define HWIO_IPA_VMIDMT_IDR0_INM(m)                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR0_ADDR, m)
#define HWIO_IPA_VMIDMT_IDR0_SMS_BMSK                                    0x8000000
#define HWIO_IPA_VMIDMT_IDR0_SMS_SHFT                                         0x1b
#define HWIO_IPA_VMIDMT_IDR0_NUMSIDB_BMSK                                   0x1e00
#define HWIO_IPA_VMIDMT_IDR0_NUMSIDB_SHFT                                      0x9
#define HWIO_IPA_VMIDMT_IDR0_NUMSMRG_BMSK                                     0xff
#define HWIO_IPA_VMIDMT_IDR0_NUMSMRG_SHFT                                        0

#define HWIO_IPA_VMIDMT_IDR1_ADDR                                       (IPA_VMIDMT_REG_BASE      + 0x00000024)
#define HWIO_IPA_VMIDMT_IDR1_PHYS                                       (IPA_VMIDMT_REG_BASE_PHYS + 0x00000024)
#define HWIO_IPA_VMIDMT_IDR1_RMSK                                           0x9f00
#define HWIO_IPA_VMIDMT_IDR1_SHFT                                                0
#define HWIO_IPA_VMIDMT_IDR1_IN                                         \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR1_ADDR, HWIO_IPA_VMIDMT_IDR1_RMSK)
#define HWIO_IPA_VMIDMT_IDR1_INM(m)                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR1_ADDR, m)
#define HWIO_IPA_VMIDMT_IDR1_SMCD_BMSK                                      0x8000
#define HWIO_IPA_VMIDMT_IDR1_SMCD_SHFT                                         0xf
#define HWIO_IPA_VMIDMT_IDR1_SSDTP_BMSK                                     0x1000
#define HWIO_IPA_VMIDMT_IDR1_SSDTP_SHFT                                        0xc
#define HWIO_IPA_VMIDMT_IDR1_NUMSSDNDX_BMSK                                  0xf00
#define HWIO_IPA_VMIDMT_IDR1_NUMSSDNDX_SHFT                                    0x8

#define HWIO_IPA_VMIDMT_IDR2_ADDR                                       (IPA_VMIDMT_REG_BASE      + 0x00000028)
#define HWIO_IPA_VMIDMT_IDR2_PHYS                                       (IPA_VMIDMT_REG_BASE_PHYS + 0x00000028)
#define HWIO_IPA_VMIDMT_IDR2_RMSK                                             0xff
#define HWIO_IPA_VMIDMT_IDR2_SHFT                                                0
#define HWIO_IPA_VMIDMT_IDR2_IN                                         \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR2_ADDR, HWIO_IPA_VMIDMT_IDR2_RMSK)
#define HWIO_IPA_VMIDMT_IDR2_INM(m)                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR2_ADDR, m)
#define HWIO_IPA_VMIDMT_IDR2_OAS_BMSK                                         0xf0
#define HWIO_IPA_VMIDMT_IDR2_OAS_SHFT                                          0x4
#define HWIO_IPA_VMIDMT_IDR2_IAS_BMSK                                          0xf
#define HWIO_IPA_VMIDMT_IDR2_IAS_SHFT                                            0

#define HWIO_IPA_VMIDMT_IDR4_ADDR                                       (IPA_VMIDMT_REG_BASE      + 0x00000030)
#define HWIO_IPA_VMIDMT_IDR4_PHYS                                       (IPA_VMIDMT_REG_BASE_PHYS + 0x00000030)
#define HWIO_IPA_VMIDMT_IDR4_RMSK                                       0xffffffff
#define HWIO_IPA_VMIDMT_IDR4_SHFT                                                0
#define HWIO_IPA_VMIDMT_IDR4_IN                                         \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR4_ADDR, HWIO_IPA_VMIDMT_IDR4_RMSK)
#define HWIO_IPA_VMIDMT_IDR4_INM(m)                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR4_ADDR, m)
#define HWIO_IPA_VMIDMT_IDR4_MAJOR_BMSK                                 0xf0000000
#define HWIO_IPA_VMIDMT_IDR4_MAJOR_SHFT                                       0x1c
#define HWIO_IPA_VMIDMT_IDR4_MINOR_BMSK                                  0xfff0000
#define HWIO_IPA_VMIDMT_IDR4_MINOR_SHFT                                       0x10
#define HWIO_IPA_VMIDMT_IDR4_STEP_BMSK                                      0xffff
#define HWIO_IPA_VMIDMT_IDR4_STEP_SHFT                                           0

#define HWIO_IPA_VMIDMT_IDR5_ADDR                                       (IPA_VMIDMT_REG_BASE      + 0x00000034)
#define HWIO_IPA_VMIDMT_IDR5_PHYS                                       (IPA_VMIDMT_REG_BASE_PHYS + 0x00000034)
#define HWIO_IPA_VMIDMT_IDR5_RMSK                                         0xff03ff
#define HWIO_IPA_VMIDMT_IDR5_SHFT                                                0
#define HWIO_IPA_VMIDMT_IDR5_IN                                         \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR5_ADDR, HWIO_IPA_VMIDMT_IDR5_RMSK)
#define HWIO_IPA_VMIDMT_IDR5_INM(m)                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR5_ADDR, m)
#define HWIO_IPA_VMIDMT_IDR5_NUMMSDRB_BMSK                                0xff0000
#define HWIO_IPA_VMIDMT_IDR5_NUMMSDRB_SHFT                                    0x10
#define HWIO_IPA_VMIDMT_IDR5_MSAE_BMSK                                       0x200
#define HWIO_IPA_VMIDMT_IDR5_MSAE_SHFT                                         0x9
#define HWIO_IPA_VMIDMT_IDR5_QRIBE_BMSK                                      0x100
#define HWIO_IPA_VMIDMT_IDR5_QRIBE_SHFT                                        0x8
#define HWIO_IPA_VMIDMT_IDR5_NVMID_BMSK                                       0xff
#define HWIO_IPA_VMIDMT_IDR5_NVMID_SHFT                                          0

#define HWIO_IPA_VMIDMT_IDR7_ADDR                                       (IPA_VMIDMT_REG_BASE      + 0x0000003c)
#define HWIO_IPA_VMIDMT_IDR7_PHYS                                       (IPA_VMIDMT_REG_BASE_PHYS + 0x0000003c)
#define HWIO_IPA_VMIDMT_IDR7_RMSK                                             0xff
#define HWIO_IPA_VMIDMT_IDR7_SHFT                                                0
#define HWIO_IPA_VMIDMT_IDR7_IN                                         \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR7_ADDR, HWIO_IPA_VMIDMT_IDR7_RMSK)
#define HWIO_IPA_VMIDMT_IDR7_INM(m)                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_IDR7_ADDR, m)
#define HWIO_IPA_VMIDMT_IDR7_MAJOR_BMSK                                       0xf0
#define HWIO_IPA_VMIDMT_IDR7_MAJOR_SHFT                                        0x4
#define HWIO_IPA_VMIDMT_IDR7_MINOR_BMSK                                        0xf
#define HWIO_IPA_VMIDMT_IDR7_MINOR_SHFT                                          0

#define HWIO_IPA_VMIDMT_GFAR0_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000040)
#define HWIO_IPA_VMIDMT_GFAR0_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000040)
#define HWIO_IPA_VMIDMT_GFAR0_RMSK                                      0xffffffff
#define HWIO_IPA_VMIDMT_GFAR0_SHFT                                               0
#define HWIO_IPA_VMIDMT_GFAR0_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_GFAR0_ADDR, HWIO_IPA_VMIDMT_GFAR0_RMSK)
#define HWIO_IPA_VMIDMT_GFAR0_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_GFAR0_ADDR, m)
#define HWIO_IPA_VMIDMT_GFAR0_GFEA0_BMSK                                0xffffffff
#define HWIO_IPA_VMIDMT_GFAR0_GFEA0_SHFT                                         0

#define HWIO_IPA_VMIDMT_GFSR_ADDR                                       (IPA_VMIDMT_REG_BASE      + 0x00000048)
#define HWIO_IPA_VMIDMT_GFSR_PHYS                                       (IPA_VMIDMT_REG_BASE_PHYS + 0x00000048)
#define HWIO_IPA_VMIDMT_GFSR_RMSK                                       0xc00000a6
#define HWIO_IPA_VMIDMT_GFSR_SHFT                                                0
#define HWIO_IPA_VMIDMT_GFSR_IN                                         \
        in_dword_masked(HWIO_IPA_VMIDMT_GFSR_ADDR, HWIO_IPA_VMIDMT_GFSR_RMSK)
#define HWIO_IPA_VMIDMT_GFSR_INM(m)                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_GFSR_ADDR, m)
#define HWIO_IPA_VMIDMT_GFSR_OUT(v)                                     \
        out_dword(HWIO_IPA_VMIDMT_GFSR_ADDR,v)
#define HWIO_IPA_VMIDMT_GFSR_OUTM(m,v)                                  \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_GFSR_ADDR,m,v,HWIO_IPA_VMIDMT_GFSR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_GFSR_MULTI_CLIENT_BMSK                          0x80000000
#define HWIO_IPA_VMIDMT_GFSR_MULTI_CLIENT_SHFT                                0x1f
#define HWIO_IPA_VMIDMT_GFSR_MULTI_CFG_BMSK                             0x40000000
#define HWIO_IPA_VMIDMT_GFSR_MULTI_CFG_SHFT                                   0x1e
#define HWIO_IPA_VMIDMT_GFSR_PF_BMSK                                          0x80
#define HWIO_IPA_VMIDMT_GFSR_PF_SHFT                                           0x7
#define HWIO_IPA_VMIDMT_GFSR_CAF_BMSK                                         0x20
#define HWIO_IPA_VMIDMT_GFSR_CAF_SHFT                                          0x5
#define HWIO_IPA_VMIDMT_GFSR_SMCF_BMSK                                         0x4
#define HWIO_IPA_VMIDMT_GFSR_SMCF_SHFT                                         0x2
#define HWIO_IPA_VMIDMT_GFSR_USF_BMSK                                          0x2
#define HWIO_IPA_VMIDMT_GFSR_USF_SHFT                                          0x1

#define HWIO_IPA_VMIDMT_GFSRRESTORE_ADDR                                (IPA_VMIDMT_REG_BASE      + 0x0000004c)
#define HWIO_IPA_VMIDMT_GFSRRESTORE_PHYS                                (IPA_VMIDMT_REG_BASE_PHYS + 0x0000004c)
#define HWIO_IPA_VMIDMT_GFSRRESTORE_RMSK                                0xc00000a6
#define HWIO_IPA_VMIDMT_GFSRRESTORE_SHFT                                         0
#define HWIO_IPA_VMIDMT_GFSRRESTORE_IN                                  \
        in_dword_masked(HWIO_IPA_VMIDMT_GFSRRESTORE_ADDR, HWIO_IPA_VMIDMT_GFSRRESTORE_RMSK)
#define HWIO_IPA_VMIDMT_GFSRRESTORE_INM(m)                              \
        in_dword_masked(HWIO_IPA_VMIDMT_GFSRRESTORE_ADDR, m)
#define HWIO_IPA_VMIDMT_GFSRRESTORE_OUT(v)                              \
        out_dword(HWIO_IPA_VMIDMT_GFSRRESTORE_ADDR,v)
#define HWIO_IPA_VMIDMT_GFSRRESTORE_OUTM(m,v)                           \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_GFSRRESTORE_ADDR,m,v,HWIO_IPA_VMIDMT_GFSRRESTORE_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_GFSRRESTORE_MULTI_CLIENT_BMSK                   0x80000000
#define HWIO_IPA_VMIDMT_GFSRRESTORE_MULTI_CLIENT_SHFT                         0x1f
#define HWIO_IPA_VMIDMT_GFSRRESTORE_MULTI_CFG_BMSK                      0x40000000
#define HWIO_IPA_VMIDMT_GFSRRESTORE_MULTI_CFG_SHFT                            0x1e
#define HWIO_IPA_VMIDMT_GFSRRESTORE_PF_BMSK                                   0x80
#define HWIO_IPA_VMIDMT_GFSRRESTORE_PF_SHFT                                    0x7
#define HWIO_IPA_VMIDMT_GFSRRESTORE_CAF_BMSK                                  0x20
#define HWIO_IPA_VMIDMT_GFSRRESTORE_CAF_SHFT                                   0x5
#define HWIO_IPA_VMIDMT_GFSRRESTORE_SMCF_BMSK                                  0x4
#define HWIO_IPA_VMIDMT_GFSRRESTORE_SMCF_SHFT                                  0x2
#define HWIO_IPA_VMIDMT_GFSRRESTORE_USF_BMSK                                   0x2
#define HWIO_IPA_VMIDMT_GFSRRESTORE_USF_SHFT                                   0x1

#define HWIO_IPA_VMIDMT_GFSYNDR0_ADDR                                   (IPA_VMIDMT_REG_BASE      + 0x00000050)
#define HWIO_IPA_VMIDMT_GFSYNDR0_PHYS                                   (IPA_VMIDMT_REG_BASE_PHYS + 0x00000050)
#define HWIO_IPA_VMIDMT_GFSYNDR0_RMSK                                        0x132
#define HWIO_IPA_VMIDMT_GFSYNDR0_SHFT                                            0
#define HWIO_IPA_VMIDMT_GFSYNDR0_IN                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_GFSYNDR0_ADDR, HWIO_IPA_VMIDMT_GFSYNDR0_RMSK)
#define HWIO_IPA_VMIDMT_GFSYNDR0_INM(m)                                 \
        in_dword_masked(HWIO_IPA_VMIDMT_GFSYNDR0_ADDR, m)
#define HWIO_IPA_VMIDMT_GFSYNDR0_MSSSELFAUTH_BMSK                            0x100
#define HWIO_IPA_VMIDMT_GFSYNDR0_MSSSELFAUTH_SHFT                              0x8
#define HWIO_IPA_VMIDMT_GFSYNDR0_NSATTR_BMSK                                  0x20
#define HWIO_IPA_VMIDMT_GFSYNDR0_NSATTR_SHFT                                   0x5
#define HWIO_IPA_VMIDMT_GFSYNDR0_NSSTATE_BMSK                                 0x10
#define HWIO_IPA_VMIDMT_GFSYNDR0_NSSTATE_SHFT                                  0x4
#define HWIO_IPA_VMIDMT_GFSYNDR0_WNR_BMSK                                      0x2
#define HWIO_IPA_VMIDMT_GFSYNDR0_WNR_SHFT                                      0x1

#define HWIO_IPA_VMIDMT_GFSYNDR1_ADDR                                   (IPA_VMIDMT_REG_BASE      + 0x00000054)
#define HWIO_IPA_VMIDMT_GFSYNDR1_PHYS                                   (IPA_VMIDMT_REG_BASE_PHYS + 0x00000054)
#define HWIO_IPA_VMIDMT_GFSYNDR1_RMSK                                   0x1f1f001f
#define HWIO_IPA_VMIDMT_GFSYNDR1_SHFT                                            0
#define HWIO_IPA_VMIDMT_GFSYNDR1_IN                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_GFSYNDR1_ADDR, HWIO_IPA_VMIDMT_GFSYNDR1_RMSK)
#define HWIO_IPA_VMIDMT_GFSYNDR1_INM(m)                                 \
        in_dword_masked(HWIO_IPA_VMIDMT_GFSYNDR1_ADDR, m)
#define HWIO_IPA_VMIDMT_GFSYNDR1_MSDINDEX_BMSK                          0x1f000000
#define HWIO_IPA_VMIDMT_GFSYNDR1_MSDINDEX_SHFT                                0x18
#define HWIO_IPA_VMIDMT_GFSYNDR1_SSDINDEX_BMSK                            0x1f0000
#define HWIO_IPA_VMIDMT_GFSYNDR1_SSDINDEX_SHFT                                0x10
#define HWIO_IPA_VMIDMT_GFSYNDR1_STREAMINDEX_BMSK                             0x1f
#define HWIO_IPA_VMIDMT_GFSYNDR1_STREAMINDEX_SHFT                                0

#define HWIO_IPA_VMIDMT_GFSYNDR2_ADDR                                   (IPA_VMIDMT_REG_BASE      + 0x00000058)
#define HWIO_IPA_VMIDMT_GFSYNDR2_PHYS                                   (IPA_VMIDMT_REG_BASE_PHYS + 0x00000058)
#define HWIO_IPA_VMIDMT_GFSYNDR2_RMSK                                    0x71fffff
#define HWIO_IPA_VMIDMT_GFSYNDR2_SHFT                                            0
#define HWIO_IPA_VMIDMT_GFSYNDR2_IN                                     \
        in_dword_masked(HWIO_IPA_VMIDMT_GFSYNDR2_ADDR, HWIO_IPA_VMIDMT_GFSYNDR2_RMSK)
#define HWIO_IPA_VMIDMT_GFSYNDR2_INM(m)                                 \
        in_dword_masked(HWIO_IPA_VMIDMT_GFSYNDR2_ADDR, m)
#define HWIO_IPA_VMIDMT_GFSYNDR2_ATID_BMSK                               0x7000000
#define HWIO_IPA_VMIDMT_GFSYNDR2_ATID_SHFT                                    0x18
#define HWIO_IPA_VMIDMT_GFSYNDR2_AVMID_BMSK                               0x1f0000
#define HWIO_IPA_VMIDMT_GFSYNDR2_AVMID_SHFT                                   0x10
#define HWIO_IPA_VMIDMT_GFSYNDR2_ABID_BMSK                                  0xe000
#define HWIO_IPA_VMIDMT_GFSYNDR2_ABID_SHFT                                     0xd
#define HWIO_IPA_VMIDMT_GFSYNDR2_APID_BMSK                                  0x1f00
#define HWIO_IPA_VMIDMT_GFSYNDR2_APID_SHFT                                     0x8
#define HWIO_IPA_VMIDMT_GFSYNDR2_AMID_BMSK                                    0xff
#define HWIO_IPA_VMIDMT_GFSYNDR2_AMID_SHFT                                       0

#define HWIO_IPA_VMIDMT_VMIDMTCR0_ADDR                                  (IPA_VMIDMT_REG_BASE      + 0x00000090)
#define HWIO_IPA_VMIDMT_VMIDMTCR0_PHYS                                  (IPA_VMIDMT_REG_BASE_PHYS + 0x00000090)
#define HWIO_IPA_VMIDMT_VMIDMTCR0_RMSK                                         0x1
#define HWIO_IPA_VMIDMT_VMIDMTCR0_SHFT                                           0
#define HWIO_IPA_VMIDMT_VMIDMTCR0_IN                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_VMIDMTCR0_ADDR, HWIO_IPA_VMIDMT_VMIDMTCR0_RMSK)
#define HWIO_IPA_VMIDMT_VMIDMTCR0_INM(m)                                \
        in_dword_masked(HWIO_IPA_VMIDMT_VMIDMTCR0_ADDR, m)
#define HWIO_IPA_VMIDMT_VMIDMTCR0_OUT(v)                                \
        out_dword(HWIO_IPA_VMIDMT_VMIDMTCR0_ADDR,v)
#define HWIO_IPA_VMIDMT_VMIDMTCR0_OUTM(m,v)                             \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_VMIDMTCR0_ADDR,m,v,HWIO_IPA_VMIDMT_VMIDMTCR0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_VMIDMTCR0_CLKONOFFE_BMSK                               0x1
#define HWIO_IPA_VMIDMT_VMIDMTCR0_CLKONOFFE_SHFT                                 0

#define HWIO_IPA_VMIDMT_VMIDMTACR_ADDR                                  (IPA_VMIDMT_REG_BASE      + 0x0000009c)
#define HWIO_IPA_VMIDMT_VMIDMTACR_PHYS                                  (IPA_VMIDMT_REG_BASE_PHYS + 0x0000009c)
#define HWIO_IPA_VMIDMT_VMIDMTACR_RMSK                                  0xffffffff
#define HWIO_IPA_VMIDMT_VMIDMTACR_SHFT                                           0
#define HWIO_IPA_VMIDMT_VMIDMTACR_IN                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_VMIDMTACR_ADDR, HWIO_IPA_VMIDMT_VMIDMTACR_RMSK)
#define HWIO_IPA_VMIDMT_VMIDMTACR_INM(m)                                \
        in_dword_masked(HWIO_IPA_VMIDMT_VMIDMTACR_ADDR, m)
#define HWIO_IPA_VMIDMT_VMIDMTACR_OUT(v)                                \
        out_dword(HWIO_IPA_VMIDMT_VMIDMTACR_ADDR,v)
#define HWIO_IPA_VMIDMT_VMIDMTACR_OUTM(m,v)                             \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_VMIDMTACR_ADDR,m,v,HWIO_IPA_VMIDMT_VMIDMTACR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_VMIDMTACR_RWE_BMSK                              0xffffffff
#define HWIO_IPA_VMIDMT_VMIDMTACR_RWE_SHFT                                       0

#define HWIO_IPA_VMIDMT_NSCR0_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000400)
#define HWIO_IPA_VMIDMT_NSCR0_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000400)
#define HWIO_IPA_VMIDMT_NSCR0_RMSK                                       0xff70ff5
#define HWIO_IPA_VMIDMT_NSCR0_SHFT                                               0
#define HWIO_IPA_VMIDMT_NSCR0_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_NSCR0_ADDR, HWIO_IPA_VMIDMT_NSCR0_RMSK)
#define HWIO_IPA_VMIDMT_NSCR0_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_NSCR0_ADDR, m)
#define HWIO_IPA_VMIDMT_NSCR0_OUT(v)                                    \
        out_dword(HWIO_IPA_VMIDMT_NSCR0_ADDR,v)
#define HWIO_IPA_VMIDMT_NSCR0_OUTM(m,v)                                 \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_NSCR0_ADDR,m,v,HWIO_IPA_VMIDMT_NSCR0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_NSCR0_WACFG_BMSK                                 0xc000000
#define HWIO_IPA_VMIDMT_NSCR0_WACFG_SHFT                                      0x1a
#define HWIO_IPA_VMIDMT_NSCR0_RACFG_BMSK                                 0x3000000
#define HWIO_IPA_VMIDMT_NSCR0_RACFG_SHFT                                      0x18
#define HWIO_IPA_VMIDMT_NSCR0_SHCFG_BMSK                                  0xc00000
#define HWIO_IPA_VMIDMT_NSCR0_SHCFG_SHFT                                      0x16
#define HWIO_IPA_VMIDMT_NSCR0_SMCFCFG_BMSK                                0x200000
#define HWIO_IPA_VMIDMT_NSCR0_SMCFCFG_SHFT                                    0x15
#define HWIO_IPA_VMIDMT_NSCR0_MTCFG_BMSK                                  0x100000
#define HWIO_IPA_VMIDMT_NSCR0_MTCFG_SHFT                                      0x14
#define HWIO_IPA_VMIDMT_NSCR0_MEMATTR_BMSK                                 0x70000
#define HWIO_IPA_VMIDMT_NSCR0_MEMATTR_SHFT                                    0x10
#define HWIO_IPA_VMIDMT_NSCR0_VMIDPNE_BMSK                                   0x800
#define HWIO_IPA_VMIDMT_NSCR0_VMIDPNE_SHFT                                     0xb
#define HWIO_IPA_VMIDMT_NSCR0_USFCFG_BMSK                                    0x400
#define HWIO_IPA_VMIDMT_NSCR0_USFCFG_SHFT                                      0xa
#define HWIO_IPA_VMIDMT_NSCR0_GSE_BMSK                                       0x200
#define HWIO_IPA_VMIDMT_NSCR0_GSE_SHFT                                         0x9
#define HWIO_IPA_VMIDMT_NSCR0_STALLD_BMSK                                    0x100
#define HWIO_IPA_VMIDMT_NSCR0_STALLD_SHFT                                      0x8
#define HWIO_IPA_VMIDMT_NSCR0_TRANSIENTCFG_BMSK                               0xc0
#define HWIO_IPA_VMIDMT_NSCR0_TRANSIENTCFG_SHFT                                0x6
#define HWIO_IPA_VMIDMT_NSCR0_GCFGFIE_BMSK                                    0x20
#define HWIO_IPA_VMIDMT_NSCR0_GCFGFIE_SHFT                                     0x5
#define HWIO_IPA_VMIDMT_NSCR0_GCFGERE_BMSK                                    0x10
#define HWIO_IPA_VMIDMT_NSCR0_GCFGERE_SHFT                                     0x4
#define HWIO_IPA_VMIDMT_NSCR0_GFIE_BMSK                                        0x4
#define HWIO_IPA_VMIDMT_NSCR0_GFIE_SHFT                                        0x2
#define HWIO_IPA_VMIDMT_NSCR0_CLIENTPD_BMSK                                    0x1
#define HWIO_IPA_VMIDMT_NSCR0_CLIENTPD_SHFT                                      0

#define HWIO_IPA_VMIDMT_NSCR2_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000408)
#define HWIO_IPA_VMIDMT_NSCR2_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000408)
#define HWIO_IPA_VMIDMT_NSCR2_RMSK                                            0x1f
#define HWIO_IPA_VMIDMT_NSCR2_SHFT                                               0
#define HWIO_IPA_VMIDMT_NSCR2_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_NSCR2_ADDR, HWIO_IPA_VMIDMT_NSCR2_RMSK)
#define HWIO_IPA_VMIDMT_NSCR2_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_NSCR2_ADDR, m)
#define HWIO_IPA_VMIDMT_NSCR2_OUT(v)                                    \
        out_dword(HWIO_IPA_VMIDMT_NSCR2_ADDR,v)
#define HWIO_IPA_VMIDMT_NSCR2_OUTM(m,v)                                 \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_NSCR2_ADDR,m,v,HWIO_IPA_VMIDMT_NSCR2_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_NSCR2_BPVMID_BMSK                                     0x1f
#define HWIO_IPA_VMIDMT_NSCR2_BPVMID_SHFT                                        0

#define HWIO_IPA_VMIDMT_NSACR_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000410)
#define HWIO_IPA_VMIDMT_NSACR_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000410)
#define HWIO_IPA_VMIDMT_NSACR_RMSK                                      0x70000013
#define HWIO_IPA_VMIDMT_NSACR_SHFT                                               0
#define HWIO_IPA_VMIDMT_NSACR_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_NSACR_ADDR, HWIO_IPA_VMIDMT_NSACR_RMSK)
#define HWIO_IPA_VMIDMT_NSACR_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_NSACR_ADDR, m)
#define HWIO_IPA_VMIDMT_NSACR_OUT(v)                                    \
        out_dword(HWIO_IPA_VMIDMT_NSACR_ADDR,v)
#define HWIO_IPA_VMIDMT_NSACR_OUTM(m,v)                                 \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_NSACR_ADDR,m,v,HWIO_IPA_VMIDMT_NSACR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_NSACR_BPRCNSH_BMSK                              0x40000000
#define HWIO_IPA_VMIDMT_NSACR_BPRCNSH_SHFT                                    0x1e
#define HWIO_IPA_VMIDMT_NSACR_BPRCISH_BMSK                              0x20000000
#define HWIO_IPA_VMIDMT_NSACR_BPRCISH_SHFT                                    0x1d
#define HWIO_IPA_VMIDMT_NSACR_BPRCOSH_BMSK                              0x10000000
#define HWIO_IPA_VMIDMT_NSACR_BPRCOSH_SHFT                                    0x1c
#define HWIO_IPA_VMIDMT_NSACR_BPREQPRIORITYCFG_BMSK                           0x10
#define HWIO_IPA_VMIDMT_NSACR_BPREQPRIORITYCFG_SHFT                            0x4
#define HWIO_IPA_VMIDMT_NSACR_BPREQPRIORITY_BMSK                               0x3
#define HWIO_IPA_VMIDMT_NSACR_BPREQPRIORITY_SHFT                                 0

#define HWIO_IPA_VMIDMT_NSGFAR0_ADDR                                    (IPA_VMIDMT_REG_BASE      + 0x00000440)
#define HWIO_IPA_VMIDMT_NSGFAR0_PHYS                                    (IPA_VMIDMT_REG_BASE_PHYS + 0x00000440)
#define HWIO_IPA_VMIDMT_NSGFAR0_RMSK                                    0xffffffff
#define HWIO_IPA_VMIDMT_NSGFAR0_SHFT                                             0
#define HWIO_IPA_VMIDMT_NSGFAR0_IN                                      \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFAR0_ADDR, HWIO_IPA_VMIDMT_NSGFAR0_RMSK)
#define HWIO_IPA_VMIDMT_NSGFAR0_INM(m)                                  \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFAR0_ADDR, m)
#define HWIO_IPA_VMIDMT_NSGFAR0_GFEA0_BMSK                              0xffffffff
#define HWIO_IPA_VMIDMT_NSGFAR0_GFEA0_SHFT                                       0

#define HWIO_IPA_VMIDMT_NSGFSR_ADDR                                     (IPA_VMIDMT_REG_BASE      + 0x00000448)
#define HWIO_IPA_VMIDMT_NSGFSR_PHYS                                     (IPA_VMIDMT_REG_BASE_PHYS + 0x00000448)
#define HWIO_IPA_VMIDMT_NSGFSR_RMSK                                     0xc00000a6
#define HWIO_IPA_VMIDMT_NSGFSR_SHFT                                              0
#define HWIO_IPA_VMIDMT_NSGFSR_IN                                       \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFSR_ADDR, HWIO_IPA_VMIDMT_NSGFSR_RMSK)
#define HWIO_IPA_VMIDMT_NSGFSR_INM(m)                                   \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFSR_ADDR, m)
#define HWIO_IPA_VMIDMT_NSGFSR_OUT(v)                                   \
        out_dword(HWIO_IPA_VMIDMT_NSGFSR_ADDR,v)
#define HWIO_IPA_VMIDMT_NSGFSR_OUTM(m,v)                                \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_NSGFSR_ADDR,m,v,HWIO_IPA_VMIDMT_NSGFSR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_NSGFSR_MULTI_CLIENT_BMSK                        0x80000000
#define HWIO_IPA_VMIDMT_NSGFSR_MULTI_CLIENT_SHFT                              0x1f
#define HWIO_IPA_VMIDMT_NSGFSR_MULTI_CFG_BMSK                           0x40000000
#define HWIO_IPA_VMIDMT_NSGFSR_MULTI_CFG_SHFT                                 0x1e
#define HWIO_IPA_VMIDMT_NSGFSR_PF_BMSK                                        0x80
#define HWIO_IPA_VMIDMT_NSGFSR_PF_SHFT                                         0x7
#define HWIO_IPA_VMIDMT_NSGFSR_CAF_BMSK                                       0x20
#define HWIO_IPA_VMIDMT_NSGFSR_CAF_SHFT                                        0x5
#define HWIO_IPA_VMIDMT_NSGFSR_SMCF_BMSK                                       0x4
#define HWIO_IPA_VMIDMT_NSGFSR_SMCF_SHFT                                       0x2
#define HWIO_IPA_VMIDMT_NSGFSR_USF_BMSK                                        0x2
#define HWIO_IPA_VMIDMT_NSGFSR_USF_SHFT                                        0x1

#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_ADDR                              (IPA_VMIDMT_REG_BASE      + 0x0000044c)
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_PHYS                              (IPA_VMIDMT_REG_BASE_PHYS + 0x0000044c)
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_RMSK                              0xc00000a6
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_SHFT                                       0
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_IN                                \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFSRRESTORE_ADDR, HWIO_IPA_VMIDMT_NSGFSRRESTORE_RMSK)
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_INM(m)                            \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFSRRESTORE_ADDR, m)
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_OUT(v)                            \
        out_dword(HWIO_IPA_VMIDMT_NSGFSRRESTORE_ADDR,v)
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_OUTM(m,v)                         \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_NSGFSRRESTORE_ADDR,m,v,HWIO_IPA_VMIDMT_NSGFSRRESTORE_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_MULTI_CLIENT_BMSK                 0x80000000
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_MULTI_CLIENT_SHFT                       0x1f
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_MULTI_CFG_BMSK                    0x40000000
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_MULTI_CFG_SHFT                          0x1e
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_PF_BMSK                                 0x80
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_PF_SHFT                                  0x7
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_CAF_BMSK                                0x20
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_CAF_SHFT                                 0x5
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_SMCF_BMSK                                0x4
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_SMCF_SHFT                                0x2
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_USF_BMSK                                 0x2
#define HWIO_IPA_VMIDMT_NSGFSRRESTORE_USF_SHFT                                 0x1

#define HWIO_IPA_VMIDMT_NSGFSYNDR0_ADDR                                 (IPA_VMIDMT_REG_BASE      + 0x00000450)
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_PHYS                                 (IPA_VMIDMT_REG_BASE_PHYS + 0x00000450)
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_RMSK                                      0x132
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_SHFT                                          0
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_IN                                   \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFSYNDR0_ADDR, HWIO_IPA_VMIDMT_NSGFSYNDR0_RMSK)
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_INM(m)                               \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFSYNDR0_ADDR, m)
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_MSSSELFAUTH_BMSK                          0x100
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_MSSSELFAUTH_SHFT                            0x8
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_NSATTR_BMSK                                0x20
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_NSATTR_SHFT                                 0x5
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_NSSTATE_BMSK                               0x10
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_NSSTATE_SHFT                                0x4
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_WNR_BMSK                                    0x2
#define HWIO_IPA_VMIDMT_NSGFSYNDR0_WNR_SHFT                                    0x1

#define HWIO_IPA_VMIDMT_NSGFSYNDR1_ADDR                                 (IPA_VMIDMT_REG_BASE      + 0x00000454)
#define HWIO_IPA_VMIDMT_NSGFSYNDR1_PHYS                                 (IPA_VMIDMT_REG_BASE_PHYS + 0x00000454)
#define HWIO_IPA_VMIDMT_NSGFSYNDR1_RMSK                                 0x1f1f001f
#define HWIO_IPA_VMIDMT_NSGFSYNDR1_SHFT                                          0
#define HWIO_IPA_VMIDMT_NSGFSYNDR1_IN                                   \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFSYNDR1_ADDR, HWIO_IPA_VMIDMT_NSGFSYNDR1_RMSK)
#define HWIO_IPA_VMIDMT_NSGFSYNDR1_INM(m)                               \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFSYNDR1_ADDR, m)
#define HWIO_IPA_VMIDMT_NSGFSYNDR1_MSDINDEX_BMSK                        0x1f000000
#define HWIO_IPA_VMIDMT_NSGFSYNDR1_MSDINDEX_SHFT                              0x18
#define HWIO_IPA_VMIDMT_NSGFSYNDR1_SSDINDEX_BMSK                          0x1f0000
#define HWIO_IPA_VMIDMT_NSGFSYNDR1_SSDINDEX_SHFT                              0x10
#define HWIO_IPA_VMIDMT_NSGFSYNDR1_STREAMINDEX_BMSK                           0x1f
#define HWIO_IPA_VMIDMT_NSGFSYNDR1_STREAMINDEX_SHFT                              0

#define HWIO_IPA_VMIDMT_NSGFSYNDR2_ADDR                                 (IPA_VMIDMT_REG_BASE      + 0x00000458)
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_PHYS                                 (IPA_VMIDMT_REG_BASE_PHYS + 0x00000458)
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_RMSK                                  0x71fffff
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_SHFT                                          0
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_IN                                   \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFSYNDR2_ADDR, HWIO_IPA_VMIDMT_NSGFSYNDR2_RMSK)
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_INM(m)                               \
        in_dword_masked(HWIO_IPA_VMIDMT_NSGFSYNDR2_ADDR, m)
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_ATID_BMSK                             0x7000000
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_ATID_SHFT                                  0x18
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_AVMID_BMSK                             0x1f0000
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_AVMID_SHFT                                 0x10
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_ABID_BMSK                                0xe000
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_ABID_SHFT                                   0xd
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_APID_BMSK                                0x1f00
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_APID_SHFT                                   0x8
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_AMID_BMSK                                  0xff
#define HWIO_IPA_VMIDMT_NSGFSYNDR2_AMID_SHFT                                     0

#define HWIO_IPA_VMIDMT_NSVMIDMTCR0_ADDR                                (IPA_VMIDMT_REG_BASE      + 0x00000490)
#define HWIO_IPA_VMIDMT_NSVMIDMTCR0_PHYS                                (IPA_VMIDMT_REG_BASE_PHYS + 0x00000490)
#define HWIO_IPA_VMIDMT_NSVMIDMTCR0_RMSK                                       0x1
#define HWIO_IPA_VMIDMT_NSVMIDMTCR0_SHFT                                         0
#define HWIO_IPA_VMIDMT_NSVMIDMTCR0_IN                                  \
        in_dword_masked(HWIO_IPA_VMIDMT_NSVMIDMTCR0_ADDR, HWIO_IPA_VMIDMT_NSVMIDMTCR0_RMSK)
#define HWIO_IPA_VMIDMT_NSVMIDMTCR0_INM(m)                              \
        in_dword_masked(HWIO_IPA_VMIDMT_NSVMIDMTCR0_ADDR, m)
#define HWIO_IPA_VMIDMT_NSVMIDMTCR0_OUT(v)                              \
        out_dword(HWIO_IPA_VMIDMT_NSVMIDMTCR0_ADDR,v)
#define HWIO_IPA_VMIDMT_NSVMIDMTCR0_OUTM(m,v)                           \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_NSVMIDMTCR0_ADDR,m,v,HWIO_IPA_VMIDMT_NSVMIDMTCR0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_NSVMIDMTCR0_CLKONOFFE_BMSK                             0x1
#define HWIO_IPA_VMIDMT_NSVMIDMTCR0_CLKONOFFE_SHFT                               0

#define HWIO_IPA_VMIDMT_SSDR0_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000080)
#define HWIO_IPA_VMIDMT_SSDR0_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000080)
#define HWIO_IPA_VMIDMT_SSDR0_RMSK                                      0xffffffff
#define HWIO_IPA_VMIDMT_SSDR0_SHFT                                               0
#define HWIO_IPA_VMIDMT_SSDR0_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_SSDR0_ADDR, HWIO_IPA_VMIDMT_SSDR0_RMSK)
#define HWIO_IPA_VMIDMT_SSDR0_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_SSDR0_ADDR, m)
#define HWIO_IPA_VMIDMT_SSDR0_OUT(v)                                    \
        out_dword(HWIO_IPA_VMIDMT_SSDR0_ADDR,v)
#define HWIO_IPA_VMIDMT_SSDR0_OUTM(m,v)                                 \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_SSDR0_ADDR,m,v,HWIO_IPA_VMIDMT_SSDR0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_SSDR0_RWE_BMSK                                  0xffffffff
#define HWIO_IPA_VMIDMT_SSDR0_RWE_SHFT                                           0

#define HWIO_IPA_VMIDMT_MSDR0_ADDR                                      (IPA_VMIDMT_REG_BASE      + 0x00000480)
#define HWIO_IPA_VMIDMT_MSDR0_PHYS                                      (IPA_VMIDMT_REG_BASE_PHYS + 0x00000480)
#define HWIO_IPA_VMIDMT_MSDR0_RMSK                                      0xffffffff
#define HWIO_IPA_VMIDMT_MSDR0_SHFT                                               0
#define HWIO_IPA_VMIDMT_MSDR0_IN                                        \
        in_dword_masked(HWIO_IPA_VMIDMT_MSDR0_ADDR, HWIO_IPA_VMIDMT_MSDR0_RMSK)
#define HWIO_IPA_VMIDMT_MSDR0_INM(m)                                    \
        in_dword_masked(HWIO_IPA_VMIDMT_MSDR0_ADDR, m)
#define HWIO_IPA_VMIDMT_MSDR0_OUT(v)                                    \
        out_dword(HWIO_IPA_VMIDMT_MSDR0_ADDR,v)
#define HWIO_IPA_VMIDMT_MSDR0_OUTM(m,v)                                 \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_MSDR0_ADDR,m,v,HWIO_IPA_VMIDMT_MSDR0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_MSDR0_RWE_BMSK                                  0xffffffff
#define HWIO_IPA_VMIDMT_MSDR0_RWE_SHFT                                           0

#define HWIO_IPA_VMIDMT_MCR_ADDR                                        (IPA_VMIDMT_REG_BASE      + 0x00000494)
#define HWIO_IPA_VMIDMT_MCR_PHYS                                        (IPA_VMIDMT_REG_BASE_PHYS + 0x00000494)
#define HWIO_IPA_VMIDMT_MCR_RMSK                                               0x7
#define HWIO_IPA_VMIDMT_MCR_SHFT                                                 0
#define HWIO_IPA_VMIDMT_MCR_IN                                          \
        in_dword_masked(HWIO_IPA_VMIDMT_MCR_ADDR, HWIO_IPA_VMIDMT_MCR_RMSK)
#define HWIO_IPA_VMIDMT_MCR_INM(m)                                      \
        in_dword_masked(HWIO_IPA_VMIDMT_MCR_ADDR, m)
#define HWIO_IPA_VMIDMT_MCR_OUT(v)                                      \
        out_dword(HWIO_IPA_VMIDMT_MCR_ADDR,v)
#define HWIO_IPA_VMIDMT_MCR_OUTM(m,v)                                   \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_MCR_ADDR,m,v,HWIO_IPA_VMIDMT_MCR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_MCR_CLKONOFFE_BMSK                                     0x4
#define HWIO_IPA_VMIDMT_MCR_CLKONOFFE_SHFT                                     0x2
#define HWIO_IPA_VMIDMT_MCR_BPMSACFG_BMSK                                      0x2
#define HWIO_IPA_VMIDMT_MCR_BPMSACFG_SHFT                                      0x1
#define HWIO_IPA_VMIDMT_MCR_BPSMSACFG_BMSK                                     0x1
#define HWIO_IPA_VMIDMT_MCR_BPSMSACFG_SHFT                                       0

#define HWIO_IPA_VMIDMT_S2VRn_ADDR(n)                                   (IPA_VMIDMT_REG_BASE      + 0x00000c00 + 0x4 * (n))
#define HWIO_IPA_VMIDMT_S2VRn_PHYS(n)                                   (IPA_VMIDMT_REG_BASE_PHYS + 0x00000c00 + 0x4 * (n))
#define HWIO_IPA_VMIDMT_S2VRn_RMSK                                      0x30ff7b1f
#define HWIO_IPA_VMIDMT_S2VRn_SHFT                                               0
#define HWIO_IPA_VMIDMT_S2VRn_MAXn                                            0x1f
#define HWIO_IPA_VMIDMT_S2VRn_INI(n) \
        in_dword(HWIO_IPA_VMIDMT_S2VRn_ADDR(n))
#define HWIO_IPA_VMIDMT_S2VRn_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_VMIDMT_S2VRn_ADDR(n), mask)
#define HWIO_IPA_VMIDMT_S2VRn_OUTI(n,val) \
        out_dword(HWIO_IPA_VMIDMT_S2VRn_ADDR(n),val)
#define HWIO_IPA_VMIDMT_S2VRn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_S2VRn_ADDR(n),mask,val,HWIO_IPA_VMIDMT_S2VRn_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_S2VRn_TRANSIENTCFG_BMSK                         0x30000000
#define HWIO_IPA_VMIDMT_S2VRn_TRANSIENTCFG_SHFT                               0x1c
#define HWIO_IPA_VMIDMT_S2VRn_WACFG_BMSK                                  0xc00000
#define HWIO_IPA_VMIDMT_S2VRn_WACFG_SHFT                                      0x16
#define HWIO_IPA_VMIDMT_S2VRn_RACFG_BMSK                                  0x300000
#define HWIO_IPA_VMIDMT_S2VRn_RACFG_SHFT                                      0x14
#define HWIO_IPA_VMIDMT_S2VRn_NSCFG_BMSK                                   0xc0000
#define HWIO_IPA_VMIDMT_S2VRn_NSCFG_SHFT                                      0x12
#define HWIO_IPA_VMIDMT_S2VRn_TYPE_BMSK                                    0x30000
#define HWIO_IPA_VMIDMT_S2VRn_TYPE_SHFT                                       0x10
#define HWIO_IPA_VMIDMT_S2VRn_MEMATTR_BMSK                                  0x7000
#define HWIO_IPA_VMIDMT_S2VRn_MEMATTR_SHFT                                     0xc
#define HWIO_IPA_VMIDMT_S2VRn_MTCFG_BMSK                                     0x800
#define HWIO_IPA_VMIDMT_S2VRn_MTCFG_SHFT                                       0xb
#define HWIO_IPA_VMIDMT_S2VRn_SHCFG_BMSK                                     0x300
#define HWIO_IPA_VMIDMT_S2VRn_SHCFG_SHFT                                       0x8
#define HWIO_IPA_VMIDMT_S2VRn_VMID_BMSK                                       0x1f
#define HWIO_IPA_VMIDMT_S2VRn_VMID_SHFT                                          0

#define HWIO_IPA_VMIDMT_AS2VRn_ADDR(n)                                  (IPA_VMIDMT_REG_BASE      + 0x00000e00 + 0x4 * (n))
#define HWIO_IPA_VMIDMT_AS2VRn_PHYS(n)                                  (IPA_VMIDMT_REG_BASE_PHYS + 0x00000e00 + 0x4 * (n))
#define HWIO_IPA_VMIDMT_AS2VRn_RMSK                                     0x70000013
#define HWIO_IPA_VMIDMT_AS2VRn_SHFT                                              0
#define HWIO_IPA_VMIDMT_AS2VRn_MAXn                                           0x1f
#define HWIO_IPA_VMIDMT_AS2VRn_INI(n) \
        in_dword(HWIO_IPA_VMIDMT_AS2VRn_ADDR(n))
#define HWIO_IPA_VMIDMT_AS2VRn_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_VMIDMT_AS2VRn_ADDR(n), mask)
#define HWIO_IPA_VMIDMT_AS2VRn_OUTI(n,val) \
        out_dword(HWIO_IPA_VMIDMT_AS2VRn_ADDR(n),val)
#define HWIO_IPA_VMIDMT_AS2VRn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_AS2VRn_ADDR(n),mask,val,HWIO_IPA_VMIDMT_AS2VRn_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_AS2VRn_RCNSH_BMSK                               0x40000000
#define HWIO_IPA_VMIDMT_AS2VRn_RCNSH_SHFT                                     0x1e
#define HWIO_IPA_VMIDMT_AS2VRn_RCISH_BMSK                               0x20000000
#define HWIO_IPA_VMIDMT_AS2VRn_RCISH_SHFT                                     0x1d
#define HWIO_IPA_VMIDMT_AS2VRn_RCOSH_BMSK                               0x10000000
#define HWIO_IPA_VMIDMT_AS2VRn_RCOSH_SHFT                                     0x1c
#define HWIO_IPA_VMIDMT_AS2VRn_REQPRIORITYCFG_BMSK                            0x10
#define HWIO_IPA_VMIDMT_AS2VRn_REQPRIORITYCFG_SHFT                             0x4
#define HWIO_IPA_VMIDMT_AS2VRn_REQPRIORITY_BMSK                                0x3
#define HWIO_IPA_VMIDMT_AS2VRn_REQPRIORITY_SHFT                                  0

#define HWIO_IPA_VMIDMT_SMRn_ADDR(n)                                    (IPA_VMIDMT_REG_BASE      + 0x00000800 + 0x4 * (n))
#define HWIO_IPA_VMIDMT_SMRn_PHYS(n)                                    (IPA_VMIDMT_REG_BASE_PHYS + 0x00000800 + 0x4 * (n))
#define HWIO_IPA_VMIDMT_SMRn_RMSK                                       0x801f001f
#define HWIO_IPA_VMIDMT_SMRn_SHFT                                                0
#define HWIO_IPA_VMIDMT_SMRn_MAXn                                             0x1f
#define HWIO_IPA_VMIDMT_SMRn_INI(n) \
        in_dword(HWIO_IPA_VMIDMT_SMRn_ADDR(n))
#define HWIO_IPA_VMIDMT_SMRn_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_VMIDMT_SMRn_ADDR(n), mask)
#define HWIO_IPA_VMIDMT_SMRn_OUTI(n,val) \
        out_dword(HWIO_IPA_VMIDMT_SMRn_ADDR(n),val)
#define HWIO_IPA_VMIDMT_SMRn_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_VMIDMT_SMRn_ADDR(n),mask,val,HWIO_IPA_VMIDMT_SMRn_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_VMIDMT_SMRn_VALID_BMSK                                 0x80000000
#define HWIO_IPA_VMIDMT_SMRn_VALID_SHFT                                       0x1f
#define HWIO_IPA_VMIDMT_SMRn_MASK_BMSK                                    0x1f0000
#define HWIO_IPA_VMIDMT_SMRn_MASK_SHFT                                        0x10
#define HWIO_IPA_VMIDMT_SMRn_ID_BMSK                                          0x1f
#define HWIO_IPA_VMIDMT_SMRn_ID_SHFT                                             0

/*------------------------------------------------------------------------------
 * MODULE: ipa
 *------------------------------------------------------------------------------*/

#define IPA_REG_BASE                                                    (IPA_WRAPPER_BASE + 0x00020000)
#define IPA_REG_BASE_PHYS                                               0x00020000

#define HWIO_IPA_IRQ_SRCS_EE_n_ADDR(n)                                  (IPA_REG_BASE      + 0x00001000 + 0x1000 * (n))
#define HWIO_IPA_IRQ_SRCS_EE_n_PHYS(n)                                  (IPA_REG_BASE_PHYS + 0x00001000 + 0x1000 * (n))
#define HWIO_IPA_IRQ_SRCS_EE_n_RMSK                                        0xfffff
#define HWIO_IPA_IRQ_SRCS_EE_n_SHFT                                              0
#define HWIO_IPA_IRQ_SRCS_EE_n_MAXn                                            0x3
#define HWIO_IPA_IRQ_SRCS_EE_n_INI(n) \
        in_dword(HWIO_IPA_IRQ_SRCS_EE_n_ADDR(n))
#define HWIO_IPA_IRQ_SRCS_EE_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_IRQ_SRCS_EE_n_ADDR(n), mask)
#define HWIO_IPA_IRQ_SRCS_EE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_IRQ_SRCS_EE_n_ADDR(n),val)
#define HWIO_IPA_IRQ_SRCS_EE_n_IPA_IRQ_SRCS_BMSK                           0xfffff
#define HWIO_IPA_IRQ_SRCS_EE_n_IPA_IRQ_SRCS_SHFT                                 0

#define HWIO_IPA_IRQ_SRCS_MSK_EE_n_ADDR(n)                              (IPA_REG_BASE      + 0x00001004 + 0x1000 * (n))
#define HWIO_IPA_IRQ_SRCS_MSK_EE_n_PHYS(n)                              (IPA_REG_BASE_PHYS + 0x00001004 + 0x1000 * (n))
#define HWIO_IPA_IRQ_SRCS_MSK_EE_n_RMSK                                    0xfffff
#define HWIO_IPA_IRQ_SRCS_MSK_EE_n_SHFT                                          0
#define HWIO_IPA_IRQ_SRCS_MSK_EE_n_MAXn                                        0x3
#define HWIO_IPA_IRQ_SRCS_MSK_EE_n_INI(n) \
        in_dword(HWIO_IPA_IRQ_SRCS_MSK_EE_n_ADDR(n))
#define HWIO_IPA_IRQ_SRCS_MSK_EE_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_IRQ_SRCS_MSK_EE_n_ADDR(n), mask)
#define HWIO_IPA_IRQ_SRCS_MSK_EE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_IRQ_SRCS_MSK_EE_n_ADDR(n),val)
#define HWIO_IPA_IRQ_SRCS_MSK_EE_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_IRQ_SRCS_MSK_EE_n_ADDR(n),mask,val,HWIO_IPA_IRQ_SRCS_MSK_EE_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_IRQ_SRCS_MSK_EE_n_IPA_IRQ_SRCS_MSK_BMSK                   0xfffff
#define HWIO_IPA_IRQ_SRCS_MSK_EE_n_IPA_IRQ_SRCS_MSK_SHFT                         0

#define HWIO_IPA_IRQ_STTS_EE_n_ADDR(n)                                  (IPA_REG_BASE      + 0x00001008 + 0x1000 * (n))
#define HWIO_IPA_IRQ_STTS_EE_n_PHYS(n)                                  (IPA_REG_BASE_PHYS + 0x00001008 + 0x1000 * (n))
#define HWIO_IPA_IRQ_STTS_EE_n_RMSK                                         0x3fff
#define HWIO_IPA_IRQ_STTS_EE_n_SHFT                                              0
#define HWIO_IPA_IRQ_STTS_EE_n_MAXn                                            0x3
#define HWIO_IPA_IRQ_STTS_EE_n_INI(n) \
        in_dword(HWIO_IPA_IRQ_STTS_EE_n_ADDR(n))
#define HWIO_IPA_IRQ_STTS_EE_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_IRQ_STTS_EE_n_ADDR(n), mask)
#define HWIO_IPA_IRQ_STTS_EE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_IRQ_STTS_EE_n_ADDR(n),val)
#define HWIO_IPA_IRQ_STTS_EE_n_TX_ERR_IRQ_BMSK                              0x2000
#define HWIO_IPA_IRQ_STTS_EE_n_TX_ERR_IRQ_SHFT                                 0xd
#define HWIO_IPA_IRQ_STTS_EE_n_DEAGGR_ERR_IRQ_BMSK                          0x1000
#define HWIO_IPA_IRQ_STTS_EE_n_DEAGGR_ERR_IRQ_SHFT                             0xc
#define HWIO_IPA_IRQ_STTS_EE_n_RX_ERR_IRQ_BMSK                               0x800
#define HWIO_IPA_IRQ_STTS_EE_n_RX_ERR_IRQ_SHFT                                 0xb
#define HWIO_IPA_IRQ_STTS_EE_n_PROC_TO_UC_ACK_Q_NOT_EMPTY_IRQ_BMSK           0x400
#define HWIO_IPA_IRQ_STTS_EE_n_PROC_TO_UC_ACK_Q_NOT_EMPTY_IRQ_SHFT             0xa
#define HWIO_IPA_IRQ_STTS_EE_n_UC_TO_PROC_ACK_Q_NOT_FULL_IRQ_BMSK            0x200
#define HWIO_IPA_IRQ_STTS_EE_n_UC_TO_PROC_ACK_Q_NOT_FULL_IRQ_SHFT              0x9
#define HWIO_IPA_IRQ_STTS_EE_n_UC_TX_CMD_Q_NOT_FULL_IRQ_BMSK                 0x100
#define HWIO_IPA_IRQ_STTS_EE_n_UC_TX_CMD_Q_NOT_FULL_IRQ_SHFT                   0x8
#define HWIO_IPA_IRQ_STTS_EE_n_UC_RX_CMD_Q_NOT_FULL_IRQ_BMSK                  0x80
#define HWIO_IPA_IRQ_STTS_EE_n_UC_RX_CMD_Q_NOT_FULL_IRQ_SHFT                   0x7
#define HWIO_IPA_IRQ_STTS_EE_n_UC_IN_Q_NOT_EMPTY_IRQ_BMSK                     0x40
#define HWIO_IPA_IRQ_STTS_EE_n_UC_IN_Q_NOT_EMPTY_IRQ_SHFT                      0x6
#define HWIO_IPA_IRQ_STTS_EE_n_UC_IRQ_3_BMSK                                  0x20
#define HWIO_IPA_IRQ_STTS_EE_n_UC_IRQ_3_SHFT                                   0x5
#define HWIO_IPA_IRQ_STTS_EE_n_UC_IRQ_2_BMSK                                  0x10
#define HWIO_IPA_IRQ_STTS_EE_n_UC_IRQ_2_SHFT                                   0x4
#define HWIO_IPA_IRQ_STTS_EE_n_UC_IRQ_1_BMSK                                   0x8
#define HWIO_IPA_IRQ_STTS_EE_n_UC_IRQ_1_SHFT                                   0x3
#define HWIO_IPA_IRQ_STTS_EE_n_UC_IRQ_0_BMSK                                   0x4
#define HWIO_IPA_IRQ_STTS_EE_n_UC_IRQ_0_SHFT                                   0x2
#define HWIO_IPA_IRQ_STTS_EE_n_EOT_COAL_IRQ_BMSK                               0x2
#define HWIO_IPA_IRQ_STTS_EE_n_EOT_COAL_IRQ_SHFT                               0x1
#define HWIO_IPA_IRQ_STTS_EE_n_BAD_SNOC_ACCESS_IRQ_BMSK                        0x1
#define HWIO_IPA_IRQ_STTS_EE_n_BAD_SNOC_ACCESS_IRQ_SHFT                          0

#define HWIO_IPA_IRQ_EN_EE_n_ADDR(n)                                    (IPA_REG_BASE      + 0x0000100c + 0x1000 * (n))
#define HWIO_IPA_IRQ_EN_EE_n_PHYS(n)                                    (IPA_REG_BASE_PHYS + 0x0000100c + 0x1000 * (n))
#define HWIO_IPA_IRQ_EN_EE_n_RMSK                                           0x3fff
#define HWIO_IPA_IRQ_EN_EE_n_SHFT                                                0
#define HWIO_IPA_IRQ_EN_EE_n_MAXn                                              0x3
#define HWIO_IPA_IRQ_EN_EE_n_INI(n) \
        in_dword(HWIO_IPA_IRQ_EN_EE_n_ADDR(n))
#define HWIO_IPA_IRQ_EN_EE_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_IRQ_EN_EE_n_ADDR(n), mask)
#define HWIO_IPA_IRQ_EN_EE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_IRQ_EN_EE_n_ADDR(n),val)
#define HWIO_IPA_IRQ_EN_EE_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_IRQ_EN_EE_n_ADDR(n),mask,val,HWIO_IPA_IRQ_EN_EE_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_IRQ_EN_EE_n_TX_ERR_IRQ_EN_BMSK                             0x2000
#define HWIO_IPA_IRQ_EN_EE_n_TX_ERR_IRQ_EN_SHFT                                0xd
#define HWIO_IPA_IRQ_EN_EE_n_DEAGGR_ERR_IRQ_EN_BMSK                         0x1000
#define HWIO_IPA_IRQ_EN_EE_n_DEAGGR_ERR_IRQ_EN_SHFT                            0xc
#define HWIO_IPA_IRQ_EN_EE_n_RX_ERR_IRQ_EN_BMSK                              0x800
#define HWIO_IPA_IRQ_EN_EE_n_RX_ERR_IRQ_EN_SHFT                                0xb
#define HWIO_IPA_IRQ_EN_EE_n_PROC_TO_UC_ACK_Q_NOT_EMPTY_IRQ_EN_BMSK          0x400
#define HWIO_IPA_IRQ_EN_EE_n_PROC_TO_UC_ACK_Q_NOT_EMPTY_IRQ_EN_SHFT            0xa
#define HWIO_IPA_IRQ_EN_EE_n_UC_TO_PROC_ACK_Q_NOT_FULL_IRQ_EN_BMSK           0x200
#define HWIO_IPA_IRQ_EN_EE_n_UC_TO_PROC_ACK_Q_NOT_FULL_IRQ_EN_SHFT             0x9
#define HWIO_IPA_IRQ_EN_EE_n_UC_TX_CMD_Q_NOT_FULL_IRQ_EN_BMSK                0x100
#define HWIO_IPA_IRQ_EN_EE_n_UC_TX_CMD_Q_NOT_FULL_IRQ_EN_SHFT                  0x8
#define HWIO_IPA_IRQ_EN_EE_n_UC_RX_CMD_Q_NOT_FULL_IRQ_EN_BMSK                 0x80
#define HWIO_IPA_IRQ_EN_EE_n_UC_RX_CMD_Q_NOT_FULL_IRQ_EN_SHFT                  0x7
#define HWIO_IPA_IRQ_EN_EE_n_UC_IN_Q_NOT_EMPTY_IRQ_EN_BMSK                    0x40
#define HWIO_IPA_IRQ_EN_EE_n_UC_IN_Q_NOT_EMPTY_IRQ_EN_SHFT                     0x6
#define HWIO_IPA_IRQ_EN_EE_n_UC_IRQ_3_IRQ_EN_BMSK                             0x20
#define HWIO_IPA_IRQ_EN_EE_n_UC_IRQ_3_IRQ_EN_SHFT                              0x5
#define HWIO_IPA_IRQ_EN_EE_n_UC_IRQ_2_IRQ_EN_BMSK                             0x10
#define HWIO_IPA_IRQ_EN_EE_n_UC_IRQ_2_IRQ_EN_SHFT                              0x4
#define HWIO_IPA_IRQ_EN_EE_n_UC_IRQ_1_IRQ_EN_BMSK                              0x8
#define HWIO_IPA_IRQ_EN_EE_n_UC_IRQ_1_IRQ_EN_SHFT                              0x3
#define HWIO_IPA_IRQ_EN_EE_n_UC_IRQ_0_IRQ_EN_BMSK                              0x4
#define HWIO_IPA_IRQ_EN_EE_n_UC_IRQ_0_IRQ_EN_SHFT                              0x2
#define HWIO_IPA_IRQ_EN_EE_n_EOT_COAL_IRQ_EN_BMSK                              0x2
#define HWIO_IPA_IRQ_EN_EE_n_EOT_COAL_IRQ_EN_SHFT                              0x1
#define HWIO_IPA_IRQ_EN_EE_n_BAD_SNOC_ACCESS_IRQ_EN_BMSK                       0x1
#define HWIO_IPA_IRQ_EN_EE_n_BAD_SNOC_ACCESS_IRQ_EN_SHFT                         0

#define HWIO_IPA_IRQ_CLR_EE_n_ADDR(n)                                   (IPA_REG_BASE      + 0x00001010 + 0x1000 * (n))
#define HWIO_IPA_IRQ_CLR_EE_n_PHYS(n)                                   (IPA_REG_BASE_PHYS + 0x00001010 + 0x1000 * (n))
#define HWIO_IPA_IRQ_CLR_EE_n_RMSK                                          0x3fff
#define HWIO_IPA_IRQ_CLR_EE_n_SHFT                                               0
#define HWIO_IPA_IRQ_CLR_EE_n_MAXn                                             0x3
#define HWIO_IPA_IRQ_CLR_EE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_IRQ_CLR_EE_n_ADDR(n),val)
#define HWIO_IPA_IRQ_CLR_EE_n_TX_ERR_IRQ_CLR_BMSK                           0x2000
#define HWIO_IPA_IRQ_CLR_EE_n_TX_ERR_IRQ_CLR_SHFT                              0xd
#define HWIO_IPA_IRQ_CLR_EE_n_DEAGGR_ERR_IRQ_CLR_BMSK                       0x1000
#define HWIO_IPA_IRQ_CLR_EE_n_DEAGGR_ERR_IRQ_CLR_SHFT                          0xc
#define HWIO_IPA_IRQ_CLR_EE_n_RX_ERR_IRQ_CLR_BMSK                            0x800
#define HWIO_IPA_IRQ_CLR_EE_n_RX_ERR_IRQ_CLR_SHFT                              0xb
#define HWIO_IPA_IRQ_CLR_EE_n_PROC_TO_UC_ACK_Q_NOT_EMPTY_IRQ_CLR_BMSK        0x400
#define HWIO_IPA_IRQ_CLR_EE_n_PROC_TO_UC_ACK_Q_NOT_EMPTY_IRQ_CLR_SHFT          0xa
#define HWIO_IPA_IRQ_CLR_EE_n_UC_TO_PROC_ACK_Q_NOT_FULL_IRQ_CLR_BMSK         0x200
#define HWIO_IPA_IRQ_CLR_EE_n_UC_TO_PROC_ACK_Q_NOT_FULL_IRQ_CLR_SHFT           0x9
#define HWIO_IPA_IRQ_CLR_EE_n_UC_TX_CMD_Q_NOT_FULL_IRQ_CLR_BMSK              0x100
#define HWIO_IPA_IRQ_CLR_EE_n_UC_TX_CMD_Q_NOT_FULL_IRQ_CLR_SHFT                0x8
#define HWIO_IPA_IRQ_CLR_EE_n_UC_RX_CMD_Q_NOT_FULL_IRQ_CLR_BMSK               0x80
#define HWIO_IPA_IRQ_CLR_EE_n_UC_RX_CMD_Q_NOT_FULL_IRQ_CLR_SHFT                0x7
#define HWIO_IPA_IRQ_CLR_EE_n_UC_IN_Q_NOT_EMPTY_IRQ_CLR_BMSK                  0x40
#define HWIO_IPA_IRQ_CLR_EE_n_UC_IN_Q_NOT_EMPTY_IRQ_CLR_SHFT                   0x6
#define HWIO_IPA_IRQ_CLR_EE_n_UC_IRQ_3_CLR_BMSK                               0x20
#define HWIO_IPA_IRQ_CLR_EE_n_UC_IRQ_3_CLR_SHFT                                0x5
#define HWIO_IPA_IRQ_CLR_EE_n_UC_IRQ_2_CLR_BMSK                               0x10
#define HWIO_IPA_IRQ_CLR_EE_n_UC_IRQ_2_CLR_SHFT                                0x4
#define HWIO_IPA_IRQ_CLR_EE_n_UC_IRQ_1_CLR_BMSK                                0x8
#define HWIO_IPA_IRQ_CLR_EE_n_UC_IRQ_1_CLR_SHFT                                0x3
#define HWIO_IPA_IRQ_CLR_EE_n_UC_IRQ_0_CLR_BMSK                                0x4
#define HWIO_IPA_IRQ_CLR_EE_n_UC_IRQ_0_CLR_SHFT                                0x2
#define HWIO_IPA_IRQ_CLR_EE_n_EOT_COAL_IRQ_CLR_BMSK                            0x2
#define HWIO_IPA_IRQ_CLR_EE_n_EOT_COAL_IRQ_CLR_SHFT                            0x1
#define HWIO_IPA_IRQ_CLR_EE_n_BAD_SNOC_ACCESS_IRQ_CLR_BMSK                     0x1
#define HWIO_IPA_IRQ_CLR_EE_n_BAD_SNOC_ACCESS_IRQ_CLR_SHFT                       0

#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_ADDR(n)                          (IPA_REG_BASE      + 0x00001014 + 0x1000 * (n))
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_PHYS(n)                          (IPA_REG_BASE_PHYS + 0x00001014 + 0x1000 * (n))
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_RMSK                                 0x1f3f
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_SHFT                                      0
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_MAXn                                    0x3
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_INI(n) \
        in_dword(HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_ADDR(n))
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_ADDR(n), mask)
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_ADDR(n),val)
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_ADDR(n),mask,val,HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_COUNTER_THRSH_BMSK                   0x1f00
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_COUNTER_THRSH_SHFT                      0x8
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_TIMER_THRSH_BMSK                       0x3f
#define HWIO_IPA_IRQ_EOT_COAL_CFG_EE_n_TIMER_THRSH_SHFT                          0

#define HWIO_IPA_SNOC_FEC_EE_n_ADDR(n)                                  (IPA_REG_BASE      + 0x00001018 + 0x1000 * (n))
#define HWIO_IPA_SNOC_FEC_EE_n_PHYS(n)                                  (IPA_REG_BASE_PHYS + 0x00001018 + 0x1000 * (n))
#define HWIO_IPA_SNOC_FEC_EE_n_RMSK                                     0xffffffff
#define HWIO_IPA_SNOC_FEC_EE_n_SHFT                                              0
#define HWIO_IPA_SNOC_FEC_EE_n_MAXn                                            0x3
#define HWIO_IPA_SNOC_FEC_EE_n_INI(n) \
        in_dword(HWIO_IPA_SNOC_FEC_EE_n_ADDR(n))
#define HWIO_IPA_SNOC_FEC_EE_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_SNOC_FEC_EE_n_ADDR(n), mask)
#define HWIO_IPA_SNOC_FEC_EE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_SNOC_FEC_EE_n_ADDR(n),val)
#define HWIO_IPA_SNOC_FEC_EE_n_READ_NOT_WRITE_BMSK                      0x80000000
#define HWIO_IPA_SNOC_FEC_EE_n_READ_NOT_WRITE_SHFT                            0x1f
#define HWIO_IPA_SNOC_FEC_EE_n_ADDR_BMSK                                0x7fffffff
#define HWIO_IPA_SNOC_FEC_EE_n_ADDR_SHFT                                         0

#define HWIO_IPA_SET_UC_IRQ_EE_n_ADDR(n)                                (IPA_REG_BASE      + 0x00004028 + 0x4 * (n))
#define HWIO_IPA_SET_UC_IRQ_EE_n_PHYS(n)                                (IPA_REG_BASE_PHYS + 0x00004028 + 0x4 * (n))
#define HWIO_IPA_SET_UC_IRQ_EE_n_RMSK                                          0xf
#define HWIO_IPA_SET_UC_IRQ_EE_n_SHFT                                            0
#define HWIO_IPA_SET_UC_IRQ_EE_n_MAXn                                          0x3
#define HWIO_IPA_SET_UC_IRQ_EE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_SET_UC_IRQ_EE_n_ADDR(n),val)
#define HWIO_IPA_SET_UC_IRQ_EE_n_SET_UC_IRQ_3_BMSK                             0x8
#define HWIO_IPA_SET_UC_IRQ_EE_n_SET_UC_IRQ_3_SHFT                             0x3
#define HWIO_IPA_SET_UC_IRQ_EE_n_SET_UC_IRQ_2_BMSK                             0x4
#define HWIO_IPA_SET_UC_IRQ_EE_n_SET_UC_IRQ_2_SHFT                             0x2
#define HWIO_IPA_SET_UC_IRQ_EE_n_SET_UC_IRQ_1_BMSK                             0x2
#define HWIO_IPA_SET_UC_IRQ_EE_n_SET_UC_IRQ_1_SHFT                             0x1
#define HWIO_IPA_SET_UC_IRQ_EE_n_SET_UC_IRQ_0_BMSK                             0x1
#define HWIO_IPA_SET_UC_IRQ_EE_n_SET_UC_IRQ_0_SHFT                               0

#define HWIO_IPA_SET_UC_IRQ_ALL_EES_ADDR                                (IPA_REG_BASE      + 0x00004038)
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_PHYS                                (IPA_REG_BASE_PHYS + 0x00004038)
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_RMSK                                       0xf
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_SHFT                                         0
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_OUT(v)                              \
        out_dword(HWIO_IPA_SET_UC_IRQ_ALL_EES_ADDR,v)
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_SET_UC_IRQ_3_BMSK                          0x8
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_SET_UC_IRQ_3_SHFT                          0x3
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_SET_UC_IRQ_2_BMSK                          0x4
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_SET_UC_IRQ_2_SHFT                          0x2
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_SET_UC_IRQ_1_BMSK                          0x2
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_SET_UC_IRQ_1_SHFT                          0x1
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_SET_UC_IRQ_0_BMSK                          0x1
#define HWIO_IPA_SET_UC_IRQ_ALL_EES_SET_UC_IRQ_0_SHFT                            0

#define HWIO_IPA_IRQ_EE_UC_n_ADDR(n)                                    (IPA_REG_BASE      + 0x0000101c + 0x1000 * (n))
#define HWIO_IPA_IRQ_EE_UC_n_PHYS(n)                                    (IPA_REG_BASE_PHYS + 0x0000101c + 0x1000 * (n))
#define HWIO_IPA_IRQ_EE_UC_n_RMSK                                              0x1
#define HWIO_IPA_IRQ_EE_UC_n_SHFT                                                0
#define HWIO_IPA_IRQ_EE_UC_n_MAXn                                              0x3
#define HWIO_IPA_IRQ_EE_UC_n_INI(n) \
        in_dword(HWIO_IPA_IRQ_EE_UC_n_ADDR(n))
#define HWIO_IPA_IRQ_EE_UC_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_IRQ_EE_UC_n_ADDR(n), mask)
#define HWIO_IPA_IRQ_EE_UC_n_OUTI(n,val) \
        out_dword(HWIO_IPA_IRQ_EE_UC_n_ADDR(n),val)
#define HWIO_IPA_IRQ_EE_UC_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_IRQ_EE_UC_n_ADDR(n),mask,val,HWIO_IPA_IRQ_EE_UC_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_IRQ_EE_UC_n_INT_BMSK                                          0x1
#define HWIO_IPA_IRQ_EE_UC_n_INT_SHFT                                            0

#define HWIO_IPA_FEC_ADDR_EE_n_ADDR(n)                                  (IPA_REG_BASE      + 0x00001020 + 0x1000 * (n))
#define HWIO_IPA_FEC_ADDR_EE_n_PHYS(n)                                  (IPA_REG_BASE_PHYS + 0x00001020 + 0x1000 * (n))
#define HWIO_IPA_FEC_ADDR_EE_n_RMSK                                     0xffffffff
#define HWIO_IPA_FEC_ADDR_EE_n_SHFT                                              0
#define HWIO_IPA_FEC_ADDR_EE_n_MAXn                                            0x3
#define HWIO_IPA_FEC_ADDR_EE_n_INI(n) \
        in_dword(HWIO_IPA_FEC_ADDR_EE_n_ADDR(n))
#define HWIO_IPA_FEC_ADDR_EE_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_FEC_ADDR_EE_n_ADDR(n), mask)
#define HWIO_IPA_FEC_ADDR_EE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_FEC_ADDR_EE_n_ADDR(n),val)
#define HWIO_IPA_FEC_ADDR_EE_n_ADDR_BMSK                                0xffffffff
#define HWIO_IPA_FEC_ADDR_EE_n_ADDR_SHFT                                         0

#define HWIO_IPA_FEC_ATTR_EE_n_ADDR(n)                                  (IPA_REG_BASE      + 0x00001024 + 0x1000 * (n))
#define HWIO_IPA_FEC_ATTR_EE_n_PHYS(n)                                  (IPA_REG_BASE_PHYS + 0x00001024 + 0x1000 * (n))
#define HWIO_IPA_FEC_ATTR_EE_n_RMSK                                     0xffffffff
#define HWIO_IPA_FEC_ATTR_EE_n_SHFT                                              0
#define HWIO_IPA_FEC_ATTR_EE_n_MAXn                                            0x3
#define HWIO_IPA_FEC_ATTR_EE_n_INI(n) \
        in_dword(HWIO_IPA_FEC_ATTR_EE_n_ADDR(n))
#define HWIO_IPA_FEC_ATTR_EE_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_FEC_ATTR_EE_n_ADDR(n), mask)
#define HWIO_IPA_FEC_ATTR_EE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_FEC_ATTR_EE_n_ADDR(n),val)
#define HWIO_IPA_FEC_ATTR_EE_n_MISC2_BMSK                               0xffffe000
#define HWIO_IPA_FEC_ATTR_EE_n_MISC2_SHFT                                      0xd
#define HWIO_IPA_FEC_ATTR_EE_n_PIPE_NUM_BMSK                                0x1f00
#define HWIO_IPA_FEC_ATTR_EE_n_PIPE_NUM_SHFT                                   0x8
#define HWIO_IPA_FEC_ATTR_EE_n_MISC1_BMSK                                     0xc0
#define HWIO_IPA_FEC_ATTR_EE_n_MISC1_SHFT                                      0x6
#define HWIO_IPA_FEC_ATTR_EE_n_OPCODE_BMSK                                    0x3f
#define HWIO_IPA_FEC_ATTR_EE_n_OPCODE_SHFT                                       0

#define HWIO_IPA_COMP_HW_VERSION_ADDR                                   (IPA_REG_BASE      + 0x00000030)
#define HWIO_IPA_COMP_HW_VERSION_PHYS                                   (IPA_REG_BASE_PHYS + 0x00000030)
#define HWIO_IPA_COMP_HW_VERSION_RMSK                                   0xffffffff
#define HWIO_IPA_COMP_HW_VERSION_SHFT                                            0
#define HWIO_IPA_COMP_HW_VERSION_IN                                     \
        in_dword_masked(HWIO_IPA_COMP_HW_VERSION_ADDR, HWIO_IPA_COMP_HW_VERSION_RMSK)
#define HWIO_IPA_COMP_HW_VERSION_INM(m)                                 \
        in_dword_masked(HWIO_IPA_COMP_HW_VERSION_ADDR, m)
#define HWIO_IPA_COMP_HW_VERSION_MAJOR_BMSK                             0xf0000000
#define HWIO_IPA_COMP_HW_VERSION_MAJOR_SHFT                                   0x1c
#define HWIO_IPA_COMP_HW_VERSION_MINOR_BMSK                              0xfff0000
#define HWIO_IPA_COMP_HW_VERSION_MINOR_SHFT                                   0x10
#define HWIO_IPA_COMP_HW_VERSION_STEP_BMSK                                  0xffff
#define HWIO_IPA_COMP_HW_VERSION_STEP_SHFT                                       0

#define HWIO_IPA_VERSION_ADDR                                           (IPA_REG_BASE      + 0x00000034)
#define HWIO_IPA_VERSION_PHYS                                           (IPA_REG_BASE_PHYS + 0x00000034)
#define HWIO_IPA_VERSION_RMSK                                           0xffffffff
#define HWIO_IPA_VERSION_SHFT                                                    0
#define HWIO_IPA_VERSION_IN                                             \
        in_dword_masked(HWIO_IPA_VERSION_ADDR, HWIO_IPA_VERSION_RMSK)
#define HWIO_IPA_VERSION_INM(m)                                         \
        in_dword_masked(HWIO_IPA_VERSION_ADDR, m)
#define HWIO_IPA_VERSION_IPA_R_REV_BMSK                                 0xff000000
#define HWIO_IPA_VERSION_IPA_R_REV_SHFT                                       0x18
#define HWIO_IPA_VERSION_IPA_Q_REV_BMSK                                   0xff0000
#define HWIO_IPA_VERSION_IPA_Q_REV_SHFT                                       0x10
#define HWIO_IPA_VERSION_IPA_P_REV_BMSK                                     0xff00
#define HWIO_IPA_VERSION_IPA_P_REV_SHFT                                        0x8
#define HWIO_IPA_VERSION_IPA_ECO_REV_BMSK                                     0xff
#define HWIO_IPA_VERSION_IPA_ECO_REV_SHFT                                        0

#define HWIO_IPA_COMP_CFG_ADDR                                          (IPA_REG_BASE      + 0x00000038)
#define HWIO_IPA_COMP_CFG_PHYS                                          (IPA_REG_BASE_PHYS + 0x00000038)
#define HWIO_IPA_COMP_CFG_RMSK                                                 0x3
#define HWIO_IPA_COMP_CFG_SHFT                                                   0
#define HWIO_IPA_COMP_CFG_IN                                            \
        in_dword_masked(HWIO_IPA_COMP_CFG_ADDR, HWIO_IPA_COMP_CFG_RMSK)
#define HWIO_IPA_COMP_CFG_INM(m)                                        \
        in_dword_masked(HWIO_IPA_COMP_CFG_ADDR, m)
#define HWIO_IPA_COMP_CFG_OUT(v)                                        \
        out_dword(HWIO_IPA_COMP_CFG_ADDR,v)
#define HWIO_IPA_COMP_CFG_OUTM(m,v)                                     \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_COMP_CFG_ADDR,m,v,HWIO_IPA_COMP_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_COMP_CFG_BAM_SNOC_BYPASS_DIS_BMSK                             0x2
#define HWIO_IPA_COMP_CFG_BAM_SNOC_BYPASS_DIS_SHFT                             0x1
#define HWIO_IPA_COMP_CFG_ENABLE_BMSK                                          0x1
#define HWIO_IPA_COMP_CFG_ENABLE_SHFT                                            0

#define HWIO_IPA_COMP_SW_RESET_ADDR                                     (IPA_REG_BASE      + 0x0000003c)
#define HWIO_IPA_COMP_SW_RESET_PHYS                                     (IPA_REG_BASE_PHYS + 0x0000003c)
#define HWIO_IPA_COMP_SW_RESET_RMSK                                            0x1
#define HWIO_IPA_COMP_SW_RESET_SHFT                                              0
#define HWIO_IPA_COMP_SW_RESET_OUT(v)                                   \
        out_dword(HWIO_IPA_COMP_SW_RESET_ADDR,v)
#define HWIO_IPA_COMP_SW_RESET_SW_RESET_BMSK                                   0x1
#define HWIO_IPA_COMP_SW_RESET_SW_RESET_SHFT                                     0

#define HWIO_IPA_CLKON_CFG_ADDR                                         (IPA_REG_BASE      + 0x00000040)
#define HWIO_IPA_CLKON_CFG_PHYS                                         (IPA_REG_BASE_PHYS + 0x00000040)
#define HWIO_IPA_CLKON_CFG_RMSK                                                0xf
#define HWIO_IPA_CLKON_CFG_SHFT                                                  0
#define HWIO_IPA_CLKON_CFG_IN                                           \
        in_dword_masked(HWIO_IPA_CLKON_CFG_ADDR, HWIO_IPA_CLKON_CFG_RMSK)
#define HWIO_IPA_CLKON_CFG_INM(m)                                       \
        in_dword_masked(HWIO_IPA_CLKON_CFG_ADDR, m)
#define HWIO_IPA_CLKON_CFG_OUT(v)                                       \
        out_dword(HWIO_IPA_CLKON_CFG_ADDR,v)
#define HWIO_IPA_CLKON_CFG_OUTM(m,v)                                    \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_CLKON_CFG_ADDR,m,v,HWIO_IPA_CLKON_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_CLKON_CFG_CGC_OPEN_MISC_BMSK                                  0x8
#define HWIO_IPA_CLKON_CFG_CGC_OPEN_MISC_SHFT                                  0x3
#define HWIO_IPA_CLKON_CFG_CGC_OPEN_TX_BMSK                                    0x4
#define HWIO_IPA_CLKON_CFG_CGC_OPEN_TX_SHFT                                    0x2
#define HWIO_IPA_CLKON_CFG_CGC_OPEN_PROC_BMSK                                  0x2
#define HWIO_IPA_CLKON_CFG_CGC_OPEN_PROC_SHFT                                  0x1
#define HWIO_IPA_CLKON_CFG_CGC_OPEN_RX_BMSK                                    0x1
#define HWIO_IPA_CLKON_CFG_CGC_OPEN_RX_SHFT                                      0

#define HWIO_IPA_ROUTE_ADDR                                             (IPA_REG_BASE      + 0x00000044)
#define HWIO_IPA_ROUTE_PHYS                                             (IPA_REG_BASE_PHYS + 0x00000044)
#define HWIO_IPA_ROUTE_RMSK                                               0x3fffff
#define HWIO_IPA_ROUTE_SHFT                                                      0
#define HWIO_IPA_ROUTE_IN                                               \
        in_dword_masked(HWIO_IPA_ROUTE_ADDR, HWIO_IPA_ROUTE_RMSK)
#define HWIO_IPA_ROUTE_INM(m)                                           \
        in_dword_masked(HWIO_IPA_ROUTE_ADDR, m)
#define HWIO_IPA_ROUTE_OUT(v)                                           \
        out_dword(HWIO_IPA_ROUTE_ADDR,v)
#define HWIO_IPA_ROUTE_OUTM(m,v)                                        \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ROUTE_ADDR,m,v,HWIO_IPA_ROUTE_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_ROUTE_ROUTE_FRAG_DEF_PIPE_BMSK                           0x3e0000
#define HWIO_IPA_ROUTE_ROUTE_FRAG_DEF_PIPE_SHFT                               0x11
#define HWIO_IPA_ROUTE_ROUTE_DEF_HDR_OFST_BMSK                             0x1ff80
#define HWIO_IPA_ROUTE_ROUTE_DEF_HDR_OFST_SHFT                                 0x7
#define HWIO_IPA_ROUTE_ROUTE_DEF_HDR_TABLE_BMSK                               0x40
#define HWIO_IPA_ROUTE_ROUTE_DEF_HDR_TABLE_SHFT                                0x6
#define HWIO_IPA_ROUTE_ROUTE_DEF_PIPE_BMSK                                    0x3e
#define HWIO_IPA_ROUTE_ROUTE_DEF_PIPE_SHFT                                     0x1
#define HWIO_IPA_ROUTE_ROUTE_DIS_BMSK                                          0x1
#define HWIO_IPA_ROUTE_ROUTE_DIS_SHFT                                            0

#define HWIO_IPA_FILTER_ADDR                                            (IPA_REG_BASE      + 0x00000048)
#define HWIO_IPA_FILTER_PHYS                                            (IPA_REG_BASE_PHYS + 0x00000048)
#define HWIO_IPA_FILTER_RMSK                                                   0x1
#define HWIO_IPA_FILTER_SHFT                                                     0
#define HWIO_IPA_FILTER_IN                                              \
        in_dword_masked(HWIO_IPA_FILTER_ADDR, HWIO_IPA_FILTER_RMSK)
#define HWIO_IPA_FILTER_INM(m)                                          \
        in_dword_masked(HWIO_IPA_FILTER_ADDR, m)
#define HWIO_IPA_FILTER_OUT(v)                                          \
        out_dword(HWIO_IPA_FILTER_ADDR,v)
#define HWIO_IPA_FILTER_OUTM(m,v)                                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_FILTER_ADDR,m,v,HWIO_IPA_FILTER_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_FILTER_FILTER_DIS_BMSK                                        0x1
#define HWIO_IPA_FILTER_FILTER_DIS_SHFT                                          0

#define HWIO_IPA_MASTER_PRIORITY_ADDR                                   (IPA_REG_BASE      + 0x0000004c)
#define HWIO_IPA_MASTER_PRIORITY_PHYS                                   (IPA_REG_BASE_PHYS + 0x0000004c)
#define HWIO_IPA_MASTER_PRIORITY_RMSK                                   0xffffffff
#define HWIO_IPA_MASTER_PRIORITY_SHFT                                            0
#define HWIO_IPA_MASTER_PRIORITY_IN                                     \
        in_dword_masked(HWIO_IPA_MASTER_PRIORITY_ADDR, HWIO_IPA_MASTER_PRIORITY_RMSK)
#define HWIO_IPA_MASTER_PRIORITY_INM(m)                                 \
        in_dword_masked(HWIO_IPA_MASTER_PRIORITY_ADDR, m)
#define HWIO_IPA_MASTER_PRIORITY_OUT(v)                                 \
        out_dword(HWIO_IPA_MASTER_PRIORITY_ADDR,v)
#define HWIO_IPA_MASTER_PRIORITY_OUTM(m,v)                              \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_MASTER_PRIORITY_ADDR,m,v,HWIO_IPA_MASTER_PRIORITY_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_MASTER_PRIORITY_MASTER_7_WR_BMSK                       0xc0000000
#define HWIO_IPA_MASTER_PRIORITY_MASTER_7_WR_SHFT                             0x1e
#define HWIO_IPA_MASTER_PRIORITY_MASTER_7_RD_BMSK                       0x30000000
#define HWIO_IPA_MASTER_PRIORITY_MASTER_7_RD_SHFT                             0x1c
#define HWIO_IPA_MASTER_PRIORITY_MASTER_6_WR_BMSK                        0xc000000
#define HWIO_IPA_MASTER_PRIORITY_MASTER_6_WR_SHFT                             0x1a
#define HWIO_IPA_MASTER_PRIORITY_MASTER_6_RD_BMSK                        0x3000000
#define HWIO_IPA_MASTER_PRIORITY_MASTER_6_RD_SHFT                             0x18
#define HWIO_IPA_MASTER_PRIORITY_MASTER_5_WR_BMSK                         0xc00000
#define HWIO_IPA_MASTER_PRIORITY_MASTER_5_WR_SHFT                             0x16
#define HWIO_IPA_MASTER_PRIORITY_MASTER_5_RD_BMSK                         0x300000
#define HWIO_IPA_MASTER_PRIORITY_MASTER_5_RD_SHFT                             0x14
#define HWIO_IPA_MASTER_PRIORITY_MASTER_4_WR_BMSK                          0xc0000
#define HWIO_IPA_MASTER_PRIORITY_MASTER_4_WR_SHFT                             0x12
#define HWIO_IPA_MASTER_PRIORITY_MASTER_4_RD_BMSK                          0x30000
#define HWIO_IPA_MASTER_PRIORITY_MASTER_4_RD_SHFT                             0x10
#define HWIO_IPA_MASTER_PRIORITY_MASTER_3_WR_BMSK                           0xc000
#define HWIO_IPA_MASTER_PRIORITY_MASTER_3_WR_SHFT                              0xe
#define HWIO_IPA_MASTER_PRIORITY_MASTER_3_RD_BMSK                           0x3000
#define HWIO_IPA_MASTER_PRIORITY_MASTER_3_RD_SHFT                              0xc
#define HWIO_IPA_MASTER_PRIORITY_MASTER_2_WR_BMSK                            0xc00
#define HWIO_IPA_MASTER_PRIORITY_MASTER_2_WR_SHFT                              0xa
#define HWIO_IPA_MASTER_PRIORITY_MASTER_2_RD_BMSK                            0x300
#define HWIO_IPA_MASTER_PRIORITY_MASTER_2_RD_SHFT                              0x8
#define HWIO_IPA_MASTER_PRIORITY_MASTER_1_WR_BMSK                             0xc0
#define HWIO_IPA_MASTER_PRIORITY_MASTER_1_WR_SHFT                              0x6
#define HWIO_IPA_MASTER_PRIORITY_MASTER_1_RD_BMSK                             0x30
#define HWIO_IPA_MASTER_PRIORITY_MASTER_1_RD_SHFT                              0x4
#define HWIO_IPA_MASTER_PRIORITY_MASTER_0_WR_BMSK                              0xc
#define HWIO_IPA_MASTER_PRIORITY_MASTER_0_WR_SHFT                              0x2
#define HWIO_IPA_MASTER_PRIORITY_MASTER_0_RD_BMSK                              0x3
#define HWIO_IPA_MASTER_PRIORITY_MASTER_0_RD_SHFT                                0

#define HWIO_IPA_SHARED_MEM_SIZE_ADDR                                   (IPA_REG_BASE      + 0x00000050)
#define HWIO_IPA_SHARED_MEM_SIZE_PHYS                                   (IPA_REG_BASE_PHYS + 0x00000050)
#define HWIO_IPA_SHARED_MEM_SIZE_RMSK                                   0xffffffff
#define HWIO_IPA_SHARED_MEM_SIZE_SHFT                                            0
#define HWIO_IPA_SHARED_MEM_SIZE_IN                                     \
        in_dword_masked(HWIO_IPA_SHARED_MEM_SIZE_ADDR, HWIO_IPA_SHARED_MEM_SIZE_RMSK)
#define HWIO_IPA_SHARED_MEM_SIZE_INM(m)                                 \
        in_dword_masked(HWIO_IPA_SHARED_MEM_SIZE_ADDR, m)
#define HWIO_IPA_SHARED_MEM_SIZE_SHARED_MEM_BADDR_BMSK                  0xffff0000
#define HWIO_IPA_SHARED_MEM_SIZE_SHARED_MEM_BADDR_SHFT                        0x10
#define HWIO_IPA_SHARED_MEM_SIZE_SHARED_MEM_SIZE_BMSK                       0xffff
#define HWIO_IPA_SHARED_MEM_SIZE_SHARED_MEM_SIZE_SHFT                            0

#define HWIO_IPA_NAT_TIMER_ADDR                                         (IPA_REG_BASE      + 0x00000054)
#define HWIO_IPA_NAT_TIMER_PHYS                                         (IPA_REG_BASE_PHYS + 0x00000054)
#define HWIO_IPA_NAT_TIMER_RMSK                                           0xffffff
#define HWIO_IPA_NAT_TIMER_SHFT                                                  0
#define HWIO_IPA_NAT_TIMER_IN                                           \
        in_dword_masked(HWIO_IPA_NAT_TIMER_ADDR, HWIO_IPA_NAT_TIMER_RMSK)
#define HWIO_IPA_NAT_TIMER_INM(m)                                       \
        in_dword_masked(HWIO_IPA_NAT_TIMER_ADDR, m)
#define HWIO_IPA_NAT_TIMER_NAT_TIMER_BMSK                                 0xffffff
#define HWIO_IPA_NAT_TIMER_NAT_TIMER_SHFT                                        0

#define HWIO_IPA_NAT_TIMER_RESET_ADDR                                   (IPA_REG_BASE      + 0x00000058)
#define HWIO_IPA_NAT_TIMER_RESET_PHYS                                   (IPA_REG_BASE_PHYS + 0x00000058)
#define HWIO_IPA_NAT_TIMER_RESET_RMSK                                          0x1
#define HWIO_IPA_NAT_TIMER_RESET_SHFT                                            0
#define HWIO_IPA_NAT_TIMER_RESET_OUT(v)                                 \
        out_dword(HWIO_IPA_NAT_TIMER_RESET_ADDR,v)
#define HWIO_IPA_NAT_TIMER_RESET_NAT_TIMER_RESET_BMSK                          0x1
#define HWIO_IPA_NAT_TIMER_RESET_NAT_TIMER_RESET_SHFT                            0

#define HWIO_IPA_TAG_TIMER_ADDR                                         (IPA_REG_BASE      + 0x0000005c)
#define HWIO_IPA_TAG_TIMER_PHYS                                         (IPA_REG_BASE_PHYS + 0x0000005c)
#define HWIO_IPA_TAG_TIMER_RMSK                                         0xffffffff
#define HWIO_IPA_TAG_TIMER_SHFT                                                  0
#define HWIO_IPA_TAG_TIMER_IN                                           \
        in_dword_masked(HWIO_IPA_TAG_TIMER_ADDR, HWIO_IPA_TAG_TIMER_RMSK)
#define HWIO_IPA_TAG_TIMER_INM(m)                                       \
        in_dword_masked(HWIO_IPA_TAG_TIMER_ADDR, m)
#define HWIO_IPA_TAG_TIMER_TAG_TIMER_BMSK                               0xffffffff
#define HWIO_IPA_TAG_TIMER_TAG_TIMER_SHFT                                        0

#define HWIO_IPA_QCNCM_ADDR                                             (IPA_REG_BASE      + 0x00000060)
#define HWIO_IPA_QCNCM_PHYS                                             (IPA_REG_BASE_PHYS + 0x00000060)
#define HWIO_IPA_QCNCM_RMSK                                              0xffffff1
#define HWIO_IPA_QCNCM_SHFT                                                      0
#define HWIO_IPA_QCNCM_IN                                               \
        in_dword_masked(HWIO_IPA_QCNCM_ADDR, HWIO_IPA_QCNCM_RMSK)
#define HWIO_IPA_QCNCM_INM(m)                                           \
        in_dword_masked(HWIO_IPA_QCNCM_ADDR, m)
#define HWIO_IPA_QCNCM_OUT(v)                                           \
        out_dword(HWIO_IPA_QCNCM_ADDR,v)
#define HWIO_IPA_QCNCM_OUTM(m,v)                                        \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_QCNCM_ADDR,m,v,HWIO_IPA_QCNCM_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_QCNCM_QCNCM_MODE_VAL_BMSK                               0xffffff0
#define HWIO_IPA_QCNCM_QCNCM_MODE_VAL_SHFT                                     0x4
#define HWIO_IPA_QCNCM_QCNCM_MODE_EN_BMSK                                      0x1
#define HWIO_IPA_QCNCM_QCNCM_MODE_EN_SHFT                                        0

#define HWIO_IPA_SINGLE_NDP_MODE_ADDR                                   (IPA_REG_BASE      + 0x00000064)
#define HWIO_IPA_SINGLE_NDP_MODE_PHYS                                   (IPA_REG_BASE_PHYS + 0x00000064)
#define HWIO_IPA_SINGLE_NDP_MODE_RMSK                                          0x1
#define HWIO_IPA_SINGLE_NDP_MODE_SHFT                                            0
#define HWIO_IPA_SINGLE_NDP_MODE_IN                                     \
        in_dword_masked(HWIO_IPA_SINGLE_NDP_MODE_ADDR, HWIO_IPA_SINGLE_NDP_MODE_RMSK)
#define HWIO_IPA_SINGLE_NDP_MODE_INM(m)                                 \
        in_dword_masked(HWIO_IPA_SINGLE_NDP_MODE_ADDR, m)
#define HWIO_IPA_SINGLE_NDP_MODE_OUT(v)                                 \
        out_dword(HWIO_IPA_SINGLE_NDP_MODE_ADDR,v)
#define HWIO_IPA_SINGLE_NDP_MODE_OUTM(m,v)                              \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_SINGLE_NDP_MODE_ADDR,m,v,HWIO_IPA_SINGLE_NDP_MODE_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_SINGLE_NDP_MODE_SINGLE_NDP_EN_BMSK                            0x1
#define HWIO_IPA_SINGLE_NDP_MODE_SINGLE_NDP_EN_SHFT                              0

#define HWIO_IPA_FRAG_RULES_CLR_ADDR                                    (IPA_REG_BASE      + 0x00000068)
#define HWIO_IPA_FRAG_RULES_CLR_PHYS                                    (IPA_REG_BASE_PHYS + 0x00000068)
#define HWIO_IPA_FRAG_RULES_CLR_RMSK                                           0x1
#define HWIO_IPA_FRAG_RULES_CLR_SHFT                                             0
#define HWIO_IPA_FRAG_RULES_CLR_OUT(v)                                  \
        out_dword(HWIO_IPA_FRAG_RULES_CLR_ADDR,v)
#define HWIO_IPA_FRAG_RULES_CLR_CLR_BMSK                                       0x1
#define HWIO_IPA_FRAG_RULES_CLR_CLR_SHFT                                         0

#define HWIO_IPA_PROC_IPH_CFG_ADDR                                      (IPA_REG_BASE      + 0x0000006c)
#define HWIO_IPA_PROC_IPH_CFG_PHYS                                      (IPA_REG_BASE_PHYS + 0x0000006c)
#define HWIO_IPA_PROC_IPH_CFG_RMSK                                             0x3
#define HWIO_IPA_PROC_IPH_CFG_SHFT                                               0
#define HWIO_IPA_PROC_IPH_CFG_IN                                        \
        in_dword_masked(HWIO_IPA_PROC_IPH_CFG_ADDR, HWIO_IPA_PROC_IPH_CFG_RMSK)
#define HWIO_IPA_PROC_IPH_CFG_INM(m)                                    \
        in_dword_masked(HWIO_IPA_PROC_IPH_CFG_ADDR, m)
#define HWIO_IPA_PROC_IPH_CFG_OUT(v)                                    \
        out_dword(HWIO_IPA_PROC_IPH_CFG_ADDR,v)
#define HWIO_IPA_PROC_IPH_CFG_OUTM(m,v)                                 \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_PROC_IPH_CFG_ADDR,m,v,HWIO_IPA_PROC_IPH_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_PROC_IPH_CFG_IPH_THRESHOLD_BMSK                               0x3
#define HWIO_IPA_PROC_IPH_CFG_IPH_THRESHOLD_SHFT                                 0

#define HWIO_IPA_ENDP_INIT_CTRL_n_ADDR(n)                               (IPA_REG_BASE      + 0x00000070 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_CTRL_n_PHYS(n)                               (IPA_REG_BASE_PHYS + 0x00000070 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_CTRL_n_RMSK                                         0x3
#define HWIO_IPA_ENDP_INIT_CTRL_n_SHFT                                           0
#define HWIO_IPA_ENDP_INIT_CTRL_n_MAXn                                        0x13
#define HWIO_IPA_ENDP_INIT_CTRL_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_CTRL_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_CTRL_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_CTRL_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_CTRL_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_CTRL_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_CTRL_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_CTRL_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_CTRL_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_CTRL_n_ENDP_DELAY_BMSK                              0x2
#define HWIO_IPA_ENDP_INIT_CTRL_n_ENDP_DELAY_SHFT                              0x1
#define HWIO_IPA_ENDP_INIT_CTRL_n_ENDP_SUSPEND_BMSK                            0x1
#define HWIO_IPA_ENDP_INIT_CTRL_n_ENDP_SUSPEND_SHFT                              0

#define HWIO_IPA_ENDP_INIT_CFG_n_ADDR(n)                                (IPA_REG_BASE      + 0x000000c0 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_CFG_n_PHYS(n)                                (IPA_REG_BASE_PHYS + 0x000000c0 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_CFG_n_RMSK                                         0x7f
#define HWIO_IPA_ENDP_INIT_CFG_n_SHFT                                            0
#define HWIO_IPA_ENDP_INIT_CFG_n_MAXn                                         0x13
#define HWIO_IPA_ENDP_INIT_CFG_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_CFG_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_CFG_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_CFG_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_CFG_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_CFG_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_CFG_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_CFG_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_CFG_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_CFG_n_CS_METADATA_HDR_OFFSET_BMSK                  0x78
#define HWIO_IPA_ENDP_INIT_CFG_n_CS_METADATA_HDR_OFFSET_SHFT                   0x3
#define HWIO_IPA_ENDP_INIT_CFG_n_CS_OFFLOAD_EN_BMSK                            0x6
#define HWIO_IPA_ENDP_INIT_CFG_n_CS_OFFLOAD_EN_SHFT                            0x1
#define HWIO_IPA_ENDP_INIT_CFG_n_FRAG_OFFLOAD_EN_BMSK                          0x1
#define HWIO_IPA_ENDP_INIT_CFG_n_FRAG_OFFLOAD_EN_SHFT                            0

#define HWIO_IPA_ENDP_INIT_NAT_n_ADDR(n)                                (IPA_REG_BASE      + 0x00000120 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_NAT_n_PHYS(n)                                (IPA_REG_BASE_PHYS + 0x00000120 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_NAT_n_RMSK                                          0x3
#define HWIO_IPA_ENDP_INIT_NAT_n_SHFT                                            0
#define HWIO_IPA_ENDP_INIT_NAT_n_MAXn                                         0x13
#define HWIO_IPA_ENDP_INIT_NAT_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_NAT_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_NAT_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_NAT_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_NAT_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_NAT_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_NAT_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_NAT_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_NAT_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_NAT_n_NAT_EN_BMSK                                   0x3
#define HWIO_IPA_ENDP_INIT_NAT_n_NAT_EN_SHFT                                     0

#define HWIO_IPA_ENDP_INIT_HDR_n_ADDR(n)                                (IPA_REG_BASE      + 0x00000170 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HDR_n_PHYS(n)                                (IPA_REG_BASE_PHYS + 0x00000170 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HDR_n_RMSK                                    0xfffffff
#define HWIO_IPA_ENDP_INIT_HDR_n_SHFT                                            0
#define HWIO_IPA_ENDP_INIT_HDR_n_MAXn                                         0x13
#define HWIO_IPA_ENDP_INIT_HDR_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_HDR_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_HDR_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_HDR_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_HDR_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_HDR_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_HDR_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_HDR_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_HDR_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_LEN_INC_DEAGG_HDR_BMSK              0x8000000
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_LEN_INC_DEAGG_HDR_SHFT                   0x1b
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_A5_MUX_BMSK                         0x4000000
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_A5_MUX_SHFT                              0x1a
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_OFST_PKT_SIZE_BMSK                  0x3f00000
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_OFST_PKT_SIZE_SHFT                       0x14
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_OFST_PKT_SIZE_VALID_BMSK              0x80000
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_OFST_PKT_SIZE_VALID_SHFT                 0x13
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_ADDITIONAL_CONST_LEN_BMSK             0x7e000
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_ADDITIONAL_CONST_LEN_SHFT                 0xd
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_OFST_METADATA_BMSK                     0x1f80
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_OFST_METADATA_SHFT                        0x7
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_OFST_METADATA_VALID_BMSK                 0x40
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_OFST_METADATA_VALID_SHFT                  0x6
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_LEN_BMSK                                 0x3f
#define HWIO_IPA_ENDP_INIT_HDR_n_HDR_LEN_SHFT                                    0

#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_ADDR(n)                            (IPA_REG_BASE      + 0x000001c0 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_PHYS(n)                            (IPA_REG_BASE_PHYS + 0x000001c0 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_RMSK                                   0x1fff
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_SHFT                                        0
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_MAXn                                     0x13
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_HDR_EXT_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_HDR_EXT_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_HDR_EXT_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_HDR_EXT_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_HDR_EXT_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_PAD_TO_ALIGNMENT_BMSK              0x1c00
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_PAD_TO_ALIGNMENT_SHFT                 0xa
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_TOTAL_LEN_OR_PAD_OFFSET_BMSK        0x3f0
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_TOTAL_LEN_OR_PAD_OFFSET_SHFT          0x4
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_PAYLOAD_LEN_INC_PADDING_BMSK          0x8
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_PAYLOAD_LEN_INC_PADDING_SHFT          0x3
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_TOTAL_LEN_OR_PAD_BMSK                 0x4
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_TOTAL_LEN_OR_PAD_SHFT                 0x2
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_TOTAL_LEN_OR_PAD_VALID_BMSK           0x2
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_TOTAL_LEN_OR_PAD_VALID_SHFT           0x1
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_ENDIANESS_BMSK                        0x1
#define HWIO_IPA_ENDP_INIT_HDR_EXT_n_HDR_ENDIANESS_SHFT                          0

#define HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_ADDR(n)                  (IPA_REG_BASE      + 0x00000220 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_PHYS(n)                  (IPA_REG_BASE_PHYS + 0x00000220 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_RMSK                     0xffffffff
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_SHFT                              0
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_MAXn                           0x13
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_METADATA_MASK_BMSK       0xffffffff
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_MASK_n_METADATA_MASK_SHFT                0

#define HWIO_IPA_ENDP_INIT_HDR_METADATA_n_ADDR(n)                       (IPA_REG_BASE      + 0x00000270 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_n_PHYS(n)                       (IPA_REG_BASE_PHYS + 0x00000270 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_n_RMSK                          0xffffffff
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_n_SHFT                                   0
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_n_MAXn                                0x13
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_HDR_METADATA_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_HDR_METADATA_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_HDR_METADATA_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_HDR_METADATA_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_HDR_METADATA_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_n_METADATA_BMSK                 0xffffffff
#define HWIO_IPA_ENDP_INIT_HDR_METADATA_n_METADATA_SHFT                          0

#define HWIO_IPA_ENDP_INIT_MODE_n_ADDR(n)                               (IPA_REG_BASE      + 0x000002c0 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_MODE_n_PHYS(n)                               (IPA_REG_BASE_PHYS + 0x000002c0 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_MODE_n_RMSK                                   0xffff1f3
#define HWIO_IPA_ENDP_INIT_MODE_n_SHFT                                           0
#define HWIO_IPA_ENDP_INIT_MODE_n_MAXn                                        0x13
#define HWIO_IPA_ENDP_INIT_MODE_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_MODE_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_MODE_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_MODE_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_MODE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_MODE_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_MODE_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_MODE_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_MODE_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_MODE_n_BYTE_THRESHOLD_BMSK                    0xffff000
#define HWIO_IPA_ENDP_INIT_MODE_n_BYTE_THRESHOLD_SHFT                          0xc
#define HWIO_IPA_ENDP_INIT_MODE_n_DEST_PIPE_INDEX_BMSK                       0x1f0
#define HWIO_IPA_ENDP_INIT_MODE_n_DEST_PIPE_INDEX_SHFT                         0x4
#define HWIO_IPA_ENDP_INIT_MODE_n_MODE_BMSK                                    0x3
#define HWIO_IPA_ENDP_INIT_MODE_n_MODE_SHFT                                      0

#define HWIO_IPA_ENDP_INIT_AGGR_n_ADDR(n)                               (IPA_REG_BASE      + 0x00000320 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_AGGR_n_PHYS(n)                               (IPA_REG_BASE_PHYS + 0x00000320 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_AGGR_n_RMSK                                    0x3fffff
#define HWIO_IPA_ENDP_INIT_AGGR_n_SHFT                                           0
#define HWIO_IPA_ENDP_INIT_AGGR_n_MAXn                                        0x13
#define HWIO_IPA_ENDP_INIT_AGGR_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_AGGR_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_AGGR_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_AGGR_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_AGGR_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_AGGR_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_AGGR_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_AGGR_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_AGGR_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_SW_EOF_ACTIVE_BMSK                 0x200000
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_SW_EOF_ACTIVE_SHFT                     0x15
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_PKT_LIMIT_BMSK                     0x1f8000
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_PKT_LIMIT_SHFT                          0xf
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_TIME_LIMIT_BMSK                      0x7c00
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_TIME_LIMIT_SHFT                         0xa
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_BYTE_LIMIT_BMSK                       0x3e0
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_BYTE_LIMIT_SHFT                         0x5
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_TYPE_BMSK                              0x1c
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_TYPE_SHFT                               0x2
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_EN_BMSK                                 0x3
#define HWIO_IPA_ENDP_INIT_AGGR_n_AGGR_EN_SHFT                                   0

#define HWIO_IPA_ENDP_INIT_ROUTE_n_ADDR(n)                              (IPA_REG_BASE      + 0x00000370 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_ROUTE_n_PHYS(n)                              (IPA_REG_BASE_PHYS + 0x00000370 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_ROUTE_n_RMSK                                       0x1f
#define HWIO_IPA_ENDP_INIT_ROUTE_n_SHFT                                          0
#define HWIO_IPA_ENDP_INIT_ROUTE_n_MAXn                                       0x13
#define HWIO_IPA_ENDP_INIT_ROUTE_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_ROUTE_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_ROUTE_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_ROUTE_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_ROUTE_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_ROUTE_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_ROUTE_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_ROUTE_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_ROUTE_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_ROUTE_n_ROUTE_TABLE_INDEX_BMSK                     0x1f
#define HWIO_IPA_ENDP_INIT_ROUTE_n_ROUTE_TABLE_INDEX_SHFT                        0

#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_ADDR(n)                       (IPA_REG_BASE      + 0x000003c0 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_PHYS(n)                       (IPA_REG_BASE_PHYS + 0x000003c0 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_RMSK                                 0x1
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_SHFT                                   0
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_MAXn                                0x13
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_EN_BMSK                              0x1
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_EN_n_EN_SHFT                                0

#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_ADDR(n)                    (IPA_REG_BASE      + 0x00000420 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_PHYS(n)                    (IPA_REG_BASE_PHYS + 0x00000420 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_RMSK                            0x1ff
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_SHFT                                0
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_MAXn                             0x13
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_TIMER_BMSK                      0x1ff
#define HWIO_IPA_ENDP_INIT_HOL_BLOCK_TIMER_n_TIMER_SHFT                          0

#define HWIO_IPA_ENDP_INIT_DEAGGR_n_ADDR(n)                             (IPA_REG_BASE      + 0x00000470 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_PHYS(n)                             (IPA_REG_BASE_PHYS + 0x00000470 + 0x4 * (n))
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_RMSK                                    0x3f7f
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_SHFT                                         0
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_MAXn                                      0x13
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_INIT_DEAGGR_n_ADDR(n))
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_INIT_DEAGGR_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_INIT_DEAGGR_n_ADDR(n),val)
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_INIT_DEAGGR_n_ADDR(n),mask,val,HWIO_IPA_ENDP_INIT_DEAGGR_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_PACKET_OFFSET_BMSK                      0x3f00
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_PACKET_OFFSET_SHFT                         0x8
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_PACKET_OFFSET_VALID_BMSK                  0x40
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_PACKET_OFFSET_VALID_SHFT                   0x6
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_DEAGGR_HDR_LEN_BMSK                       0x3f
#define HWIO_IPA_ENDP_INIT_DEAGGR_n_DEAGGR_HDR_LEN_SHFT                          0

#define HWIO_IPA_ENDP_STATUS_n_ADDR(n)                                  (IPA_REG_BASE      + 0x000004c0 + 0x4 * (n))
#define HWIO_IPA_ENDP_STATUS_n_PHYS(n)                                  (IPA_REG_BASE_PHYS + 0x000004c0 + 0x4 * (n))
#define HWIO_IPA_ENDP_STATUS_n_RMSK                                           0x3f
#define HWIO_IPA_ENDP_STATUS_n_SHFT                                              0
#define HWIO_IPA_ENDP_STATUS_n_MAXn                                           0x13
#define HWIO_IPA_ENDP_STATUS_n_INI(n) \
        in_dword(HWIO_IPA_ENDP_STATUS_n_ADDR(n))
#define HWIO_IPA_ENDP_STATUS_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_ENDP_STATUS_n_ADDR(n), mask)
#define HWIO_IPA_ENDP_STATUS_n_OUTI(n,val) \
        out_dword(HWIO_IPA_ENDP_STATUS_n_ADDR(n),val)
#define HWIO_IPA_ENDP_STATUS_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_ENDP_STATUS_n_ADDR(n),mask,val,HWIO_IPA_ENDP_STATUS_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_ENDP_STATUS_n_STATUS_ENDP_BMSK                               0x3e
#define HWIO_IPA_ENDP_STATUS_n_STATUS_ENDP_SHFT                                0x1
#define HWIO_IPA_ENDP_STATUS_n_STATUS_EN_BMSK                                  0x1
#define HWIO_IPA_ENDP_STATUS_n_STATUS_EN_SHFT                                    0

#define HWIO_IPA_DEBUG_CNT_REG_n_ADDR(n)                                (IPA_REG_BASE      + 0x00000600 + 0x4 * (n))
#define HWIO_IPA_DEBUG_CNT_REG_n_PHYS(n)                                (IPA_REG_BASE_PHYS + 0x00000600 + 0x4 * (n))
#define HWIO_IPA_DEBUG_CNT_REG_n_RMSK                                   0xffffffff
#define HWIO_IPA_DEBUG_CNT_REG_n_SHFT                                            0
#define HWIO_IPA_DEBUG_CNT_REG_n_MAXn                                          0xf
#define HWIO_IPA_DEBUG_CNT_REG_n_INI(n) \
        in_dword(HWIO_IPA_DEBUG_CNT_REG_n_ADDR(n))
#define HWIO_IPA_DEBUG_CNT_REG_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_DEBUG_CNT_REG_n_ADDR(n), mask)
#define HWIO_IPA_DEBUG_CNT_REG_n_OUTI(n,val) \
        out_dword(HWIO_IPA_DEBUG_CNT_REG_n_ADDR(n),val)
#define HWIO_IPA_DEBUG_CNT_REG_n_DBG_CNT_REG_BMSK                       0xffffffff
#define HWIO_IPA_DEBUG_CNT_REG_n_DBG_CNT_REG_SHFT                                0

#define HWIO_IPA_DEBUG_CNT_CTRL_n_ADDR(n)                               (IPA_REG_BASE      + 0x00000640 + 0x4 * (n))
#define HWIO_IPA_DEBUG_CNT_CTRL_n_PHYS(n)                               (IPA_REG_BASE_PHYS + 0x00000640 + 0x4 * (n))
#define HWIO_IPA_DEBUG_CNT_CTRL_n_RMSK                                  0x1ff1f171
#define HWIO_IPA_DEBUG_CNT_CTRL_n_SHFT                                           0
#define HWIO_IPA_DEBUG_CNT_CTRL_n_MAXn                                         0xf
#define HWIO_IPA_DEBUG_CNT_CTRL_n_INI(n) \
        in_dword(HWIO_IPA_DEBUG_CNT_CTRL_n_ADDR(n))
#define HWIO_IPA_DEBUG_CNT_CTRL_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_DEBUG_CNT_CTRL_n_ADDR(n), mask)
#define HWIO_IPA_DEBUG_CNT_CTRL_n_OUTI(n,val) \
        out_dword(HWIO_IPA_DEBUG_CNT_CTRL_n_ADDR(n),val)
#define HWIO_IPA_DEBUG_CNT_CTRL_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_DEBUG_CNT_CTRL_n_ADDR(n),mask,val,HWIO_IPA_DEBUG_CNT_CTRL_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_DEBUG_CNT_CTRL_n_DBG_CNT_RULE_INDEX_BMSK               0x1ff00000
#define HWIO_IPA_DEBUG_CNT_CTRL_n_DBG_CNT_RULE_INDEX_SHFT                     0x14
#define HWIO_IPA_DEBUG_CNT_CTRL_n_DBG_CNT_SOURCE_PIPE_BMSK                 0x1f000
#define HWIO_IPA_DEBUG_CNT_CTRL_n_DBG_CNT_SOURCE_PIPE_SHFT                     0xc
#define HWIO_IPA_DEBUG_CNT_CTRL_n_DBG_CNT_PRODUCT_BMSK                       0x100
#define HWIO_IPA_DEBUG_CNT_CTRL_n_DBG_CNT_PRODUCT_SHFT                         0x8
#define HWIO_IPA_DEBUG_CNT_CTRL_n_DBG_CNT_TYPE_BMSK                           0x70
#define HWIO_IPA_DEBUG_CNT_CTRL_n_DBG_CNT_TYPE_SHFT                            0x4
#define HWIO_IPA_DEBUG_CNT_CTRL_n_DBG_CNT_EN_BMSK                              0x1
#define HWIO_IPA_DEBUG_CNT_CTRL_n_DBG_CNT_EN_SHFT                                0

#define HWIO_IPA_DEBUG_DATA_SEL_ADDR                                    (IPA_REG_BASE      + 0x00003028)
#define HWIO_IPA_DEBUG_DATA_SEL_PHYS                                    (IPA_REG_BASE_PHYS + 0x00003028)
#define HWIO_IPA_DEBUG_DATA_SEL_RMSK                                       0x11fff
#define HWIO_IPA_DEBUG_DATA_SEL_SHFT                                             0
#define HWIO_IPA_DEBUG_DATA_SEL_IN                                      \
        in_dword_masked(HWIO_IPA_DEBUG_DATA_SEL_ADDR, HWIO_IPA_DEBUG_DATA_SEL_RMSK)
#define HWIO_IPA_DEBUG_DATA_SEL_INM(m)                                  \
        in_dword_masked(HWIO_IPA_DEBUG_DATA_SEL_ADDR, m)
#define HWIO_IPA_DEBUG_DATA_SEL_OUT(v)                                  \
        out_dword(HWIO_IPA_DEBUG_DATA_SEL_ADDR,v)
#define HWIO_IPA_DEBUG_DATA_SEL_OUTM(m,v)                               \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_DEBUG_DATA_SEL_ADDR,m,v,HWIO_IPA_DEBUG_DATA_SEL_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_DEBUG_DATA_SEL_UC_DAP_EN_BMSK                             0x10000
#define HWIO_IPA_DEBUG_DATA_SEL_UC_DAP_EN_SHFT                                0x10
#define HWIO_IPA_DEBUG_DATA_SEL_PIPE_SELECT_BMSK                            0x1f00
#define HWIO_IPA_DEBUG_DATA_SEL_PIPE_SELECT_SHFT                               0x8
#define HWIO_IPA_DEBUG_DATA_SEL_INTERNAL_BLOCK_SELECT_BMSK                    0xf0
#define HWIO_IPA_DEBUG_DATA_SEL_INTERNAL_BLOCK_SELECT_SHFT                     0x4
#define HWIO_IPA_DEBUG_DATA_SEL_EXTERNAL_BLOCK_SELECT_BMSK                     0xf
#define HWIO_IPA_DEBUG_DATA_SEL_EXTERNAL_BLOCK_SELECT_SHFT                       0

#define HWIO_IPA_DEBUG_DATA_ADDR                                        (IPA_REG_BASE      + 0x0000302c)
#define HWIO_IPA_DEBUG_DATA_PHYS                                        (IPA_REG_BASE_PHYS + 0x0000302c)
#define HWIO_IPA_DEBUG_DATA_RMSK                                        0xffffffff
#define HWIO_IPA_DEBUG_DATA_SHFT                                                 0
#define HWIO_IPA_DEBUG_DATA_IN                                          \
        in_dword_masked(HWIO_IPA_DEBUG_DATA_ADDR, HWIO_IPA_DEBUG_DATA_RMSK)
#define HWIO_IPA_DEBUG_DATA_INM(m)                                      \
        in_dword_masked(HWIO_IPA_DEBUG_DATA_ADDR, m)
#define HWIO_IPA_DEBUG_DATA_DEBUG_DATA_BMSK                             0xffffffff
#define HWIO_IPA_DEBUG_DATA_DEBUG_DATA_SHFT                                      0

#define HWIO_IPA_TESTBUS_SEL_ADDR                                       (IPA_REG_BASE      + 0x00003030)
#define HWIO_IPA_TESTBUS_SEL_PHYS                                       (IPA_REG_BASE_PHYS + 0x00003030)
#define HWIO_IPA_TESTBUS_SEL_RMSK                                          0x1fff1
#define HWIO_IPA_TESTBUS_SEL_SHFT                                                0
#define HWIO_IPA_TESTBUS_SEL_IN                                         \
        in_dword_masked(HWIO_IPA_TESTBUS_SEL_ADDR, HWIO_IPA_TESTBUS_SEL_RMSK)
#define HWIO_IPA_TESTBUS_SEL_INM(m)                                     \
        in_dword_masked(HWIO_IPA_TESTBUS_SEL_ADDR, m)
#define HWIO_IPA_TESTBUS_SEL_OUT(v)                                     \
        out_dword(HWIO_IPA_TESTBUS_SEL_ADDR,v)
#define HWIO_IPA_TESTBUS_SEL_OUTM(m,v)                                  \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_TESTBUS_SEL_ADDR,m,v,HWIO_IPA_TESTBUS_SEL_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_TESTBUS_SEL_PIPE_SELECT_BMSK                              0x1f000
#define HWIO_IPA_TESTBUS_SEL_PIPE_SELECT_SHFT                                  0xc
#define HWIO_IPA_TESTBUS_SEL_INTERNAL_BLOCK_SELECT_BMSK                      0xf00
#define HWIO_IPA_TESTBUS_SEL_INTERNAL_BLOCK_SELECT_SHFT                        0x8
#define HWIO_IPA_TESTBUS_SEL_EXTERNAL_BLOCK_SELECT_BMSK                       0xf0
#define HWIO_IPA_TESTBUS_SEL_EXTERNAL_BLOCK_SELECT_SHFT                        0x4
#define HWIO_IPA_TESTBUS_SEL_TESTBUS_EN_BMSK                                   0x1
#define HWIO_IPA_TESTBUS_SEL_TESTBUS_EN_SHFT                                     0

#define HWIO_IPA_RX_PROC_CMDQ_CMD_ADDR                                  (IPA_REG_BASE      + 0x00003034)
#define HWIO_IPA_RX_PROC_CMDQ_CMD_PHYS                                  (IPA_REG_BASE_PHYS + 0x00003034)
#define HWIO_IPA_RX_PROC_CMDQ_CMD_RMSK                                         0xf
#define HWIO_IPA_RX_PROC_CMDQ_CMD_SHFT                                           0
#define HWIO_IPA_RX_PROC_CMDQ_CMD_OUT(v)                                \
        out_dword(HWIO_IPA_RX_PROC_CMDQ_CMD_ADDR,v)
#define HWIO_IPA_RX_PROC_CMDQ_CMD_RELEASE_WR_CMD_BMSK                          0x8
#define HWIO_IPA_RX_PROC_CMDQ_CMD_RELEASE_WR_CMD_SHFT                          0x3
#define HWIO_IPA_RX_PROC_CMDQ_CMD_RELEASE_RD_CMD_BMSK                          0x4
#define HWIO_IPA_RX_PROC_CMDQ_CMD_RELEASE_RD_CMD_SHFT                          0x2
#define HWIO_IPA_RX_PROC_CMDQ_CMD_POP_CMD_BMSK                                 0x2
#define HWIO_IPA_RX_PROC_CMDQ_CMD_POP_CMD_SHFT                                 0x1
#define HWIO_IPA_RX_PROC_CMDQ_CMD_WRITE_CMD_BMSK                               0x1
#define HWIO_IPA_RX_PROC_CMDQ_CMD_WRITE_CMD_SHFT                                 0

#define HWIO_IPA_RX_PROC_CMDQ_CFG_ADDR                                  (IPA_REG_BASE      + 0x00003038)
#define HWIO_IPA_RX_PROC_CMDQ_CFG_PHYS                                  (IPA_REG_BASE_PHYS + 0x00003038)
#define HWIO_IPA_RX_PROC_CMDQ_CFG_RMSK                                         0x3
#define HWIO_IPA_RX_PROC_CMDQ_CFG_SHFT                                           0
#define HWIO_IPA_RX_PROC_CMDQ_CFG_IN                                    \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_CFG_ADDR, HWIO_IPA_RX_PROC_CMDQ_CFG_RMSK)
#define HWIO_IPA_RX_PROC_CMDQ_CFG_INM(m)                                \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_CFG_ADDR, m)
#define HWIO_IPA_RX_PROC_CMDQ_CFG_OUT(v)                                \
        out_dword(HWIO_IPA_RX_PROC_CMDQ_CFG_ADDR,v)
#define HWIO_IPA_RX_PROC_CMDQ_CFG_OUTM(m,v)                             \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_RX_PROC_CMDQ_CFG_ADDR,m,v,HWIO_IPA_RX_PROC_CMDQ_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_RX_PROC_CMDQ_CFG_BLOCK_WR_BMSK                                0x2
#define HWIO_IPA_RX_PROC_CMDQ_CFG_BLOCK_WR_SHFT                                0x1
#define HWIO_IPA_RX_PROC_CMDQ_CFG_BLOCK_RD_BMSK                                0x1
#define HWIO_IPA_RX_PROC_CMDQ_CFG_BLOCK_RD_SHFT                                  0

#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_ADDR                            (IPA_REG_BASE      + 0x0000303c)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_PHYS                            (IPA_REG_BASE_PHYS + 0x0000303c)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_RMSK                            0xffffffff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_SHFT                                     0
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_IN                              \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_ADDR, HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_RMSK)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_ADDR, m)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_OUT(v)                          \
        out_dword(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_ADDR,v)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_OUTM(m,v)                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_ADDR,m,v,HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_RX_SRC_LEN_F_BMSK               0xffff0000
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_RX_SRC_LEN_F_SHFT                     0x10
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_RX_PACKET_LEN_F_BMSK                0xffff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_0_RX_PACKET_LEN_F_SHFT                     0

#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_ADDR                            (IPA_REG_BASE      + 0x00003040)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_PHYS                            (IPA_REG_BASE_PHYS + 0x00003040)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_RMSK                            0xffffffff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_SHFT                                     0
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_IN                              \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_ADDR, HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_RMSK)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_INM(m)                          \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_ADDR, m)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_OUT(v)                          \
        out_dword(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_ADDR,v)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_OUTM(m,v)                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_ADDR,m,v,HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_RX_SRC_ADDR_F_LSB_BMSK          0xffff0000
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_RX_SRC_ADDR_F_LSB_SHFT                0x10
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_RX_FLAGS_F_BMSK                     0xfc00
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_RX_FLAGS_F_SHFT                        0xa
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_RX_ORDER_F_BMSK                      0x300
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_RX_ORDER_F_SHFT                        0x8
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_RX_SRC_PIPE_F_BMSK                    0xff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_1_RX_SRC_PIPE_F_SHFT                       0

#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_ADDR                            (IPA_REG_BASE      + 0x00003044)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_PHYS                            (IPA_REG_BASE_PHYS + 0x00003044)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_RMSK                            0xffffffff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_SHFT                                     0
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_IN                              \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_ADDR, HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_RMSK)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_INM(m)                          \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_ADDR, m)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_OUT(v)                          \
        out_dword(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_ADDR,v)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_OUTM(m,v)                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_ADDR,m,v,HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_RX_MBIM_METADATA_F_BMSK         0xff000000
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_RX_MBIM_METADATA_F_SHFT               0x18
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_RX_OPCODE_F_BMSK                  0xff0000
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_RX_OPCODE_F_SHFT                      0x10
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_RX_SRC_ADDR_F_MSB_BMSK              0xffff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_WR_2_RX_SRC_ADDR_F_MSB_SHFT                   0

#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_ADDR                            (IPA_REG_BASE      + 0x00003048)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_PHYS                            (IPA_REG_BASE_PHYS + 0x00003048)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_RMSK                            0xffffffff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_SHFT                                     0
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_IN                              \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_ADDR, HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_RMSK)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_ADDR, m)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_RX_SRC_LEN_F_BMSK               0xffff0000
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_RX_SRC_LEN_F_SHFT                     0x10
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_RX_PACKET_LEN_F_BMSK                0xffff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_0_RX_PACKET_LEN_F_SHFT                     0

#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_ADDR                            (IPA_REG_BASE      + 0x0000304c)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_PHYS                            (IPA_REG_BASE_PHYS + 0x0000304c)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_RMSK                            0xffffffff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_SHFT                                     0
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_IN                              \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_ADDR, HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_RMSK)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_INM(m)                          \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_ADDR, m)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_RX_SRC_ADDR_F_LSB_BMSK          0xffff0000
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_RX_SRC_ADDR_F_LSB_SHFT                0x10
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_RX_FLAGS_F_BMSK                     0xfc00
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_RX_FLAGS_F_SHFT                        0xa
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_RX_ORDER_F_BMSK                      0x300
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_RX_ORDER_F_SHFT                        0x8
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_RX_SRC_PIPE_F_BMSK                    0xff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_1_RX_SRC_PIPE_F_SHFT                       0

#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_ADDR                            (IPA_REG_BASE      + 0x00003050)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_PHYS                            (IPA_REG_BASE_PHYS + 0x00003050)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_RMSK                            0xffffffff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_SHFT                                     0
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_IN                              \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_ADDR, HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_RMSK)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_INM(m)                          \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_ADDR, m)
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_RX_MBIM_METADATA_F_BMSK         0xff000000
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_RX_MBIM_METADATA_F_SHFT               0x18
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_RX_OPCODE_F_BMSK                  0xff0000
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_RX_OPCODE_F_SHFT                      0x10
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_RX_SRC_ADDR_F_MSB_BMSK              0xffff
#define HWIO_IPA_RX_PROC_CMDQ_DATA_RD_2_RX_SRC_ADDR_F_MSB_SHFT                   0

#define HWIO_IPA_RX_PROC_CMDQ_STATUS_ADDR                               (IPA_REG_BASE      + 0x00003054)
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_PHYS                               (IPA_REG_BASE_PHYS + 0x00003054)
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_RMSK                                    0xff7
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_SHFT                                        0
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_IN                                 \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_STATUS_ADDR, HWIO_IPA_RX_PROC_CMDQ_STATUS_RMSK)
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_INM(m)                             \
        in_dword_masked(HWIO_IPA_RX_PROC_CMDQ_STATUS_ADDR, m)
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_CMDQ_DEPTH_BMSK                         0xf00
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_CMDQ_DEPTH_SHFT                           0x8
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_CMDQ_COUNT_BMSK                          0xf0
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_CMDQ_COUNT_SHFT                           0x4
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_CMDQ_FULL_BMSK                            0x4
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_CMDQ_FULL_SHFT                            0x2
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_CMDQ_EMPTY_BMSK                           0x2
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_CMDQ_EMPTY_SHFT                           0x1
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_STATUS_BMSK                               0x1
#define HWIO_IPA_RX_PROC_CMDQ_STATUS_STATUS_SHFT                                 0

#define HWIO_IPA_PROC_TX_CMDQ_CMD_ADDR                                  (IPA_REG_BASE      + 0x00003058)
#define HWIO_IPA_PROC_TX_CMDQ_CMD_PHYS                                  (IPA_REG_BASE_PHYS + 0x00003058)
#define HWIO_IPA_PROC_TX_CMDQ_CMD_RMSK                                         0xf
#define HWIO_IPA_PROC_TX_CMDQ_CMD_SHFT                                           0
#define HWIO_IPA_PROC_TX_CMDQ_CMD_OUT(v)                                \
        out_dword(HWIO_IPA_PROC_TX_CMDQ_CMD_ADDR,v)
#define HWIO_IPA_PROC_TX_CMDQ_CMD_RELEASE_WR_CMD_BMSK                          0x8
#define HWIO_IPA_PROC_TX_CMDQ_CMD_RELEASE_WR_CMD_SHFT                          0x3
#define HWIO_IPA_PROC_TX_CMDQ_CMD_RELEASE_RD_CMD_BMSK                          0x4
#define HWIO_IPA_PROC_TX_CMDQ_CMD_RELEASE_RD_CMD_SHFT                          0x2
#define HWIO_IPA_PROC_TX_CMDQ_CMD_POP_CMD_BMSK                                 0x2
#define HWIO_IPA_PROC_TX_CMDQ_CMD_POP_CMD_SHFT                                 0x1
#define HWIO_IPA_PROC_TX_CMDQ_CMD_WRITE_CMD_BMSK                               0x1
#define HWIO_IPA_PROC_TX_CMDQ_CMD_WRITE_CMD_SHFT                                 0

#define HWIO_IPA_PROC_TX_CMDQ_CFG_ADDR                                  (IPA_REG_BASE      + 0x0000305c)
#define HWIO_IPA_PROC_TX_CMDQ_CFG_PHYS                                  (IPA_REG_BASE_PHYS + 0x0000305c)
#define HWIO_IPA_PROC_TX_CMDQ_CFG_RMSK                                         0x3
#define HWIO_IPA_PROC_TX_CMDQ_CFG_SHFT                                           0
#define HWIO_IPA_PROC_TX_CMDQ_CFG_IN                                    \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_CFG_ADDR, HWIO_IPA_PROC_TX_CMDQ_CFG_RMSK)
#define HWIO_IPA_PROC_TX_CMDQ_CFG_INM(m)                                \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_CFG_ADDR, m)
#define HWIO_IPA_PROC_TX_CMDQ_CFG_OUT(v)                                \
        out_dword(HWIO_IPA_PROC_TX_CMDQ_CFG_ADDR,v)
#define HWIO_IPA_PROC_TX_CMDQ_CFG_OUTM(m,v)                             \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_PROC_TX_CMDQ_CFG_ADDR,m,v,HWIO_IPA_PROC_TX_CMDQ_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_PROC_TX_CMDQ_CFG_BLOCK_WR_BMSK                                0x2
#define HWIO_IPA_PROC_TX_CMDQ_CFG_BLOCK_WR_SHFT                                0x1
#define HWIO_IPA_PROC_TX_CMDQ_CFG_BLOCK_RD_BMSK                                0x1
#define HWIO_IPA_PROC_TX_CMDQ_CFG_BLOCK_RD_SHFT                                  0

#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_ADDR                            (IPA_REG_BASE      + 0x00003060)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_PHYS                            (IPA_REG_BASE_PHYS + 0x00003060)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_RMSK                            0xffffffff
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_SHFT                                     0
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_IN                              \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_ADDR, HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_RMSK)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_ADDR, m)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_OUT(v)                          \
        out_dword(HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_ADDR,v)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_OUTM(m,v)                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_ADDR,m,v,HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_TX_DEST_LEN_F_BMSK              0xffff0000
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_TX_DEST_LEN_F_SHFT                    0x10
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_TX_PACKET_LEN_F_BMSK                0xffff
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_0_TX_PACKET_LEN_F_SHFT                     0

#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_ADDR                            (IPA_REG_BASE      + 0x00003064)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_PHYS                            (IPA_REG_BASE_PHYS + 0x00003064)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_RMSK                            0xffffffff
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_SHFT                                     0
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_IN                              \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_ADDR, HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_RMSK)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_ADDR, m)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_OUT(v)                          \
        out_dword(HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_ADDR,v)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_OUTM(m,v)                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_ADDR,m,v,HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_TX_DEST_ADDR_F_BMSK             0xffff0000
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_TX_DEST_ADDR_F_SHFT                   0x10
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_TX_FLAGS_F_BMSK                     0xfc00
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_TX_FLAGS_F_SHFT                        0xa
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_TX_ORDER_F_BMSK                      0x300
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_TX_ORDER_F_SHFT                        0x8
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_TX_DEST_PIPE_F_BMSK                   0xff
#define HWIO_IPA_PROC_TX_CMDQ_DATA_WR_1_TX_DEST_PIPE_F_SHFT                      0

#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_ADDR                            (IPA_REG_BASE      + 0x00003068)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_PHYS                            (IPA_REG_BASE_PHYS + 0x00003068)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_RMSK                            0xffffffff
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_SHFT                                     0
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_IN                              \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_ADDR, HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_RMSK)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_ADDR, m)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_TX_DEST_LEN_F_BMSK              0xffff0000
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_TX_DEST_LEN_F_SHFT                    0x10
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_TX_PACKET_LEN_F_BMSK                0xffff
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_0_TX_PACKET_LEN_F_SHFT                     0

#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_ADDR                            (IPA_REG_BASE      + 0x0000306c)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_PHYS                            (IPA_REG_BASE_PHYS + 0x0000306c)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_RMSK                            0xffffffff
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_SHFT                                     0
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_IN                              \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_ADDR, HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_RMSK)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_ADDR, m)
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_TX_DEST_ADDR_F_BMSK             0xffff0000
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_TX_DEST_ADDR_F_SHFT                   0x10
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_TX_FLAGS_F_BMSK                     0xfc00
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_TX_FLAGS_F_SHFT                        0xa
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_TX_ORDER_F_BMSK                      0x300
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_TX_ORDER_F_SHFT                        0x8
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_TX_DEST_PIPE_F_BMSK                   0xff
#define HWIO_IPA_PROC_TX_CMDQ_DATA_RD_1_TX_DEST_PIPE_F_SHFT                      0

#define HWIO_IPA_PROC_TX_CMDQ_STATUS_ADDR                               (IPA_REG_BASE      + 0x00003070)
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_PHYS                               (IPA_REG_BASE_PHYS + 0x00003070)
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_RMSK                                    0xff7
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_SHFT                                        0
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_IN                                 \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_STATUS_ADDR, HWIO_IPA_PROC_TX_CMDQ_STATUS_RMSK)
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_INM(m)                             \
        in_dword_masked(HWIO_IPA_PROC_TX_CMDQ_STATUS_ADDR, m)
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_CMDQ_DEPTH_BMSK                         0xf00
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_CMDQ_DEPTH_SHFT                           0x8
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_CMDQ_COUNT_BMSK                          0xf0
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_CMDQ_COUNT_SHFT                           0x4
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_CMDQ_FULL_BMSK                            0x4
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_CMDQ_FULL_SHFT                            0x2
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_CMDQ_EMPTY_BMSK                           0x2
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_CMDQ_EMPTY_SHFT                           0x1
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_STATUS_BMSK                               0x1
#define HWIO_IPA_PROC_TX_CMDQ_STATUS_STATUS_SHFT                                 0

#define HWIO_IPA_PROC_RX_ACKQ_CMD_ADDR                                  (IPA_REG_BASE      + 0x00003100)
#define HWIO_IPA_PROC_RX_ACKQ_CMD_PHYS                                  (IPA_REG_BASE_PHYS + 0x00003100)
#define HWIO_IPA_PROC_RX_ACKQ_CMD_RMSK                                         0xf
#define HWIO_IPA_PROC_RX_ACKQ_CMD_SHFT                                           0
#define HWIO_IPA_PROC_RX_ACKQ_CMD_OUT(v)                                \
        out_dword(HWIO_IPA_PROC_RX_ACKQ_CMD_ADDR,v)
#define HWIO_IPA_PROC_RX_ACKQ_CMD_RELEASE_WR_CMD_BMSK                          0x8
#define HWIO_IPA_PROC_RX_ACKQ_CMD_RELEASE_WR_CMD_SHFT                          0x3
#define HWIO_IPA_PROC_RX_ACKQ_CMD_RELEASE_RD_CMD_BMSK                          0x4
#define HWIO_IPA_PROC_RX_ACKQ_CMD_RELEASE_RD_CMD_SHFT                          0x2
#define HWIO_IPA_PROC_RX_ACKQ_CMD_POP_CMD_BMSK                                 0x2
#define HWIO_IPA_PROC_RX_ACKQ_CMD_POP_CMD_SHFT                                 0x1
#define HWIO_IPA_PROC_RX_ACKQ_CMD_WRITE_CMD_BMSK                               0x1
#define HWIO_IPA_PROC_RX_ACKQ_CMD_WRITE_CMD_SHFT                                 0

#define HWIO_IPA_PROC_RX_ACKQ_CFG_ADDR                                  (IPA_REG_BASE      + 0x00003104)
#define HWIO_IPA_PROC_RX_ACKQ_CFG_PHYS                                  (IPA_REG_BASE_PHYS + 0x00003104)
#define HWIO_IPA_PROC_RX_ACKQ_CFG_RMSK                                         0x3
#define HWIO_IPA_PROC_RX_ACKQ_CFG_SHFT                                           0
#define HWIO_IPA_PROC_RX_ACKQ_CFG_IN                                    \
        in_dword_masked(HWIO_IPA_PROC_RX_ACKQ_CFG_ADDR, HWIO_IPA_PROC_RX_ACKQ_CFG_RMSK)
#define HWIO_IPA_PROC_RX_ACKQ_CFG_INM(m)                                \
        in_dword_masked(HWIO_IPA_PROC_RX_ACKQ_CFG_ADDR, m)
#define HWIO_IPA_PROC_RX_ACKQ_CFG_OUT(v)                                \
        out_dword(HWIO_IPA_PROC_RX_ACKQ_CFG_ADDR,v)
#define HWIO_IPA_PROC_RX_ACKQ_CFG_OUTM(m,v)                             \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_PROC_RX_ACKQ_CFG_ADDR,m,v,HWIO_IPA_PROC_RX_ACKQ_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_PROC_RX_ACKQ_CFG_BLOCK_WR_BMSK                                0x2
#define HWIO_IPA_PROC_RX_ACKQ_CFG_BLOCK_WR_SHFT                                0x1
#define HWIO_IPA_PROC_RX_ACKQ_CFG_BLOCK_RD_BMSK                                0x1
#define HWIO_IPA_PROC_RX_ACKQ_CFG_BLOCK_RD_SHFT                                  0

#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ADDR                            (IPA_REG_BASE      + 0x00003108)
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_PHYS                            (IPA_REG_BASE_PHYS + 0x00003108)
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_RMSK                            0xffffffff
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_SHFT                                     0
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_IN                              \
        in_dword_masked(HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ADDR, HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_RMSK)
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ADDR, m)
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_OUT(v)                          \
        out_dword(HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ADDR,v)
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_OUTM(m,v)                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ADDR,m,v,HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ACK_VALUE2_TYPE_BMSK            0xf0000000
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ACK_VALUE2_TYPE_SHFT                  0x1c
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ACK_VALUE1_TYPE_BMSK             0xf000000
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ACK_VALUE1_TYPE_SHFT                  0x18
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ACK_VALUE2_BMSK                   0xff0000
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ACK_VALUE2_SHFT                       0x10
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ACK_VALUE1_BMSK                     0xffff
#define HWIO_IPA_PROC_RX_ACKQ_DATA_WR_0_ACK_VALUE1_SHFT                          0

#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_ADDR                            (IPA_REG_BASE      + 0x0000310c)
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_PHYS                            (IPA_REG_BASE_PHYS + 0x0000310c)
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_RMSK                            0xffffffff
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_SHFT                                     0
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_IN                              \
        in_dword_masked(HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_ADDR, HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_RMSK)
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_ADDR, m)
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_ACK_VALUE2_TYPE_BMSK            0xf0000000
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_ACK_VALUE2_TYPE_SHFT                  0x1c
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_ACK_VALUE1_TYPE_BMSK             0xf000000
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_ACK_VALUE1_TYPE_SHFT                  0x18
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_ACK_VALUE2_BMSK                   0xff0000
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_ACK_VALUE2_SHFT                       0x10
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_ACK_VALUE1_BMSK                     0xffff
#define HWIO_IPA_PROC_RX_ACKQ_DATA_RD_0_ACK_VALUE1_SHFT                          0

#define HWIO_IPA_PROC_RX_ACKQ_STATUS_ADDR                               (IPA_REG_BASE      + 0x00003110)
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_PHYS                               (IPA_REG_BASE_PHYS + 0x00003110)
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_RMSK                                    0xff7
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_SHFT                                        0
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_IN                                 \
        in_dword_masked(HWIO_IPA_PROC_RX_ACKQ_STATUS_ADDR, HWIO_IPA_PROC_RX_ACKQ_STATUS_RMSK)
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_INM(m)                             \
        in_dword_masked(HWIO_IPA_PROC_RX_ACKQ_STATUS_ADDR, m)
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_ACKQ_DEPTH_BMSK                         0xf00
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_ACKQ_DEPTH_SHFT                           0x8
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_ACKQ_COUNT_BMSK                          0xf0
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_ACKQ_COUNT_SHFT                           0x4
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_ACKQ_FULL_BMSK                            0x4
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_ACKQ_FULL_SHFT                            0x2
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_ACKQ_EMPTY_BMSK                           0x2
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_ACKQ_EMPTY_SHFT                           0x1
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_STATUS_BMSK                               0x1
#define HWIO_IPA_PROC_RX_ACKQ_STATUS_STATUS_SHFT                                 0

#define HWIO_IPA_TX_PROC_ACKQ_CMD_ADDR                                  (IPA_REG_BASE      + 0x00003114)
#define HWIO_IPA_TX_PROC_ACKQ_CMD_PHYS                                  (IPA_REG_BASE_PHYS + 0x00003114)
#define HWIO_IPA_TX_PROC_ACKQ_CMD_RMSK                                         0xf
#define HWIO_IPA_TX_PROC_ACKQ_CMD_SHFT                                           0
#define HWIO_IPA_TX_PROC_ACKQ_CMD_OUT(v)                                \
        out_dword(HWIO_IPA_TX_PROC_ACKQ_CMD_ADDR,v)
#define HWIO_IPA_TX_PROC_ACKQ_CMD_RELEASE_WR_CMD_BMSK                          0x8
#define HWIO_IPA_TX_PROC_ACKQ_CMD_RELEASE_WR_CMD_SHFT                          0x3
#define HWIO_IPA_TX_PROC_ACKQ_CMD_RELEASE_RD_CMD_BMSK                          0x4
#define HWIO_IPA_TX_PROC_ACKQ_CMD_RELEASE_RD_CMD_SHFT                          0x2
#define HWIO_IPA_TX_PROC_ACKQ_CMD_POP_CMD_BMSK                                 0x2
#define HWIO_IPA_TX_PROC_ACKQ_CMD_POP_CMD_SHFT                                 0x1
#define HWIO_IPA_TX_PROC_ACKQ_CMD_WRITE_CMD_BMSK                               0x1
#define HWIO_IPA_TX_PROC_ACKQ_CMD_WRITE_CMD_SHFT                                 0

#define HWIO_IPA_TX_PROC_ACKQ_CFG_ADDR                                  (IPA_REG_BASE      + 0x00003118)
#define HWIO_IPA_TX_PROC_ACKQ_CFG_PHYS                                  (IPA_REG_BASE_PHYS + 0x00003118)
#define HWIO_IPA_TX_PROC_ACKQ_CFG_RMSK                                         0x3
#define HWIO_IPA_TX_PROC_ACKQ_CFG_SHFT                                           0
#define HWIO_IPA_TX_PROC_ACKQ_CFG_IN                                    \
        in_dword_masked(HWIO_IPA_TX_PROC_ACKQ_CFG_ADDR, HWIO_IPA_TX_PROC_ACKQ_CFG_RMSK)
#define HWIO_IPA_TX_PROC_ACKQ_CFG_INM(m)                                \
        in_dword_masked(HWIO_IPA_TX_PROC_ACKQ_CFG_ADDR, m)
#define HWIO_IPA_TX_PROC_ACKQ_CFG_OUT(v)                                \
        out_dword(HWIO_IPA_TX_PROC_ACKQ_CFG_ADDR,v)
#define HWIO_IPA_TX_PROC_ACKQ_CFG_OUTM(m,v)                             \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_TX_PROC_ACKQ_CFG_ADDR,m,v,HWIO_IPA_TX_PROC_ACKQ_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_TX_PROC_ACKQ_CFG_BLOCK_WR_BMSK                                0x2
#define HWIO_IPA_TX_PROC_ACKQ_CFG_BLOCK_WR_SHFT                                0x1
#define HWIO_IPA_TX_PROC_ACKQ_CFG_BLOCK_RD_BMSK                                0x1
#define HWIO_IPA_TX_PROC_ACKQ_CFG_BLOCK_RD_SHFT                                  0

#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ADDR                            (IPA_REG_BASE      + 0x0000311c)
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_PHYS                            (IPA_REG_BASE_PHYS + 0x0000311c)
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_RMSK                            0xffffffff
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_SHFT                                     0
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_IN                              \
        in_dword_masked(HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ADDR, HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_RMSK)
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ADDR, m)
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_OUT(v)                          \
        out_dword(HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ADDR,v)
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_OUTM(m,v)                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ADDR,m,v,HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ACK_VALUE2_TYPE_BMSK            0xf0000000
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ACK_VALUE2_TYPE_SHFT                  0x1c
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ACK_VALUE1_TYPE_BMSK             0xf000000
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ACK_VALUE1_TYPE_SHFT                  0x18
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ACK_VALUE2_BMSK                   0xff0000
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ACK_VALUE2_SHFT                       0x10
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ACK_VALUE1_BMSK                     0xffff
#define HWIO_IPA_TX_PROC_ACKQ_DATA_WR_0_ACK_VALUE1_SHFT                          0

#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_ADDR                            (IPA_REG_BASE      + 0x00003120)
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_PHYS                            (IPA_REG_BASE_PHYS + 0x00003120)
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_RMSK                            0xffffffff
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_SHFT                                     0
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_IN                              \
        in_dword_masked(HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_ADDR, HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_RMSK)
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_ADDR, m)
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_ACK_VALUE2_TYPE_BMSK            0xf0000000
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_ACK_VALUE2_TYPE_SHFT                  0x1c
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_ACK_VALUE1_TYPE_BMSK             0xf000000
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_ACK_VALUE1_TYPE_SHFT                  0x18
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_ACK_VALUE2_BMSK                   0xff0000
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_ACK_VALUE2_SHFT                       0x10
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_ACK_VALUE1_BMSK                     0xffff
#define HWIO_IPA_TX_PROC_ACKQ_DATA_RD_0_ACK_VALUE1_SHFT                          0

#define HWIO_IPA_TX_PROC_ACKQ_STATUS_ADDR                               (IPA_REG_BASE      + 0x00003124)
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_PHYS                               (IPA_REG_BASE_PHYS + 0x00003124)
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_RMSK                                    0xff7
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_SHFT                                        0
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_IN                                 \
        in_dword_masked(HWIO_IPA_TX_PROC_ACKQ_STATUS_ADDR, HWIO_IPA_TX_PROC_ACKQ_STATUS_RMSK)
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_INM(m)                             \
        in_dword_masked(HWIO_IPA_TX_PROC_ACKQ_STATUS_ADDR, m)
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_ACKQ_DEPTH_BMSK                         0xf00
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_ACKQ_DEPTH_SHFT                           0x8
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_ACKQ_COUNT_BMSK                          0xf0
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_ACKQ_COUNT_SHFT                           0x4
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_ACKQ_FULL_BMSK                            0x4
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_ACKQ_FULL_SHFT                            0x2
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_ACKQ_EMPTY_BMSK                           0x2
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_ACKQ_EMPTY_SHFT                           0x1
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_STATUS_BMSK                               0x1
#define HWIO_IPA_TX_PROC_ACKQ_STATUS_STATUS_SHFT                                 0

#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_ADDR                               (IPA_REG_BASE      + 0x00003128)
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_PHYS                               (IPA_REG_BASE_PHYS + 0x00003128)
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_RMSK                                      0xf
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_SHFT                                        0
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_OUT(v)                             \
        out_dword(HWIO_IPA_UC_PROC_RX_CMDQ_CMD_ADDR,v)
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_RELEASE_WR_CMD_BMSK                       0x8
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_RELEASE_WR_CMD_SHFT                       0x3
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_RELEASE_RD_CMD_BMSK                       0x4
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_RELEASE_RD_CMD_SHFT                       0x2
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_POP_CMD_BMSK                              0x2
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_POP_CMD_SHFT                              0x1
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_WRITE_CMD_BMSK                            0x1
#define HWIO_IPA_UC_PROC_RX_CMDQ_CMD_WRITE_CMD_SHFT                              0

#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_ADDR                               (IPA_REG_BASE      + 0x0000312c)
#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_PHYS                               (IPA_REG_BASE_PHYS + 0x0000312c)
#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_RMSK                                      0x3
#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_SHFT                                        0
#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_IN                                 \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_CFG_ADDR, HWIO_IPA_UC_PROC_RX_CMDQ_CFG_RMSK)
#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_INM(m)                             \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_CFG_ADDR, m)
#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_OUT(v)                             \
        out_dword(HWIO_IPA_UC_PROC_RX_CMDQ_CFG_ADDR,v)
#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_OUTM(m,v)                          \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_PROC_RX_CMDQ_CFG_ADDR,m,v,HWIO_IPA_UC_PROC_RX_CMDQ_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_BLOCK_WR_BMSK                             0x2
#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_BLOCK_WR_SHFT                             0x1
#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_BLOCK_RD_BMSK                             0x1
#define HWIO_IPA_UC_PROC_RX_CMDQ_CFG_BLOCK_RD_SHFT                               0

#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_ADDR                         (IPA_REG_BASE      + 0x00003130)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_PHYS                         (IPA_REG_BASE_PHYS + 0x00003130)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_RMSK                         0xffffffff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_SHFT                                  0
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_IN                           \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_ADDR, HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_RMSK)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_INM(m)                       \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_ADDR, m)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_OUT(v)                       \
        out_dword(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_ADDR,v)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_OUTM(m,v)                    \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_ADDR,m,v,HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_RX_SRC_LEN_F_BMSK            0xffff0000
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_RX_SRC_LEN_F_SHFT                  0x10
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_RX_PACKET_LEN_F_BMSK             0xffff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_0_RX_PACKET_LEN_F_SHFT                  0

#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_ADDR                         (IPA_REG_BASE      + 0x00003134)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_PHYS                         (IPA_REG_BASE_PHYS + 0x00003134)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_RMSK                         0xffffffff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_SHFT                                  0
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_IN                           \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_ADDR, HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_RMSK)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_INM(m)                       \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_ADDR, m)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_OUT(v)                       \
        out_dword(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_ADDR,v)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_OUTM(m,v)                    \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_ADDR,m,v,HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_RX_SRC_ADDR_F_LSB_BMSK       0xffff0000
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_RX_SRC_ADDR_F_LSB_SHFT             0x10
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_RX_FLAGS_F_BMSK                  0xfc00
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_RX_FLAGS_F_SHFT                     0xa
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_RX_ORDER_F_BMSK                   0x300
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_RX_ORDER_F_SHFT                     0x8
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_RX_SRC_PIPE_F_BMSK                 0xff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_1_RX_SRC_PIPE_F_SHFT                    0

#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_ADDR                         (IPA_REG_BASE      + 0x00003138)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_PHYS                         (IPA_REG_BASE_PHYS + 0x00003138)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_RMSK                         0xffffffff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_SHFT                                  0
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_IN                           \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_ADDR, HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_RMSK)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_INM(m)                       \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_ADDR, m)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_OUT(v)                       \
        out_dword(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_ADDR,v)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_OUTM(m,v)                    \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_ADDR,m,v,HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_RX_MBIM_METADATA_F_BMSK      0xff000000
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_RX_MBIM_METADATA_F_SHFT            0x18
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_RX_OPCODE_F_BMSK               0xff0000
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_RX_OPCODE_F_SHFT                   0x10
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_RX_SRC_ADDR_F_MSB_BMSK           0xffff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_WR_2_RX_SRC_ADDR_F_MSB_SHFT                0

#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_ADDR                         (IPA_REG_BASE      + 0x0000313c)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_PHYS                         (IPA_REG_BASE_PHYS + 0x0000313c)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_RMSK                         0xffffffff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_SHFT                                  0
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_IN                           \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_ADDR, HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_RMSK)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_INM(m)                       \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_ADDR, m)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_RX_SRC_LEN_F_BMSK            0xffff0000
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_RX_SRC_LEN_F_SHFT                  0x10
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_RX_PACKET_LEN_F_BMSK             0xffff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_0_RX_PACKET_LEN_F_SHFT                  0

#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_ADDR                         (IPA_REG_BASE      + 0x00003140)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_PHYS                         (IPA_REG_BASE_PHYS + 0x00003140)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_RMSK                         0xffffffff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_SHFT                                  0
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_IN                           \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_ADDR, HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_RMSK)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_INM(m)                       \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_ADDR, m)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_RX_SRC_ADDR_F_LSB_BMSK       0xffff0000
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_RX_SRC_ADDR_F_LSB_SHFT             0x10
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_RX_FLAGS_F_BMSK                  0xfc00
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_RX_FLAGS_F_SHFT                     0xa
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_RX_ORDER_F_BMSK                   0x300
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_RX_ORDER_F_SHFT                     0x8
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_RX_SRC_PIPE_F_BMSK                 0xff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_1_RX_SRC_PIPE_F_SHFT                    0

#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_ADDR                         (IPA_REG_BASE      + 0x00003144)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_PHYS                         (IPA_REG_BASE_PHYS + 0x00003144)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_RMSK                         0xffffffff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_SHFT                                  0
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_IN                           \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_ADDR, HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_RMSK)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_INM(m)                       \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_ADDR, m)
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_RX_MBIM_METADATA_F_BMSK      0xff000000
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_RX_MBIM_METADATA_F_SHFT            0x18
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_RX_OPCODE_F_BMSK               0xff0000
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_RX_OPCODE_F_SHFT                   0x10
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_RX_SRC_ADDR_F_MSB_BMSK           0xffff
#define HWIO_IPA_UC_PROC_RX_CMDQ_DATA_RD_2_RX_SRC_ADDR_F_MSB_SHFT                0

#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_ADDR                            (IPA_REG_BASE      + 0x00003148)
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_PHYS                            (IPA_REG_BASE_PHYS + 0x00003148)
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_RMSK                                 0xff7
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_SHFT                                     0
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_IN                              \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_ADDR, HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_RMSK)
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_INM(m)                          \
        in_dword_masked(HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_ADDR, m)
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_CMDQ_DEPTH_BMSK                      0xf00
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_CMDQ_DEPTH_SHFT                        0x8
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_CMDQ_COUNT_BMSK                       0xf0
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_CMDQ_COUNT_SHFT                        0x4
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_CMDQ_FULL_BMSK                         0x4
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_CMDQ_FULL_SHFT                         0x2
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_CMDQ_EMPTY_BMSK                        0x2
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_CMDQ_EMPTY_SHFT                        0x1
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_STATUS_BMSK                            0x1
#define HWIO_IPA_UC_PROC_RX_CMDQ_STATUS_STATUS_SHFT                              0

#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_ADDR                               (IPA_REG_BASE      + 0x0000314c)
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_PHYS                               (IPA_REG_BASE_PHYS + 0x0000314c)
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_RMSK                                      0xf
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_SHFT                                        0
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_OUT(v)                             \
        out_dword(HWIO_IPA_UC_PROC_TX_CMDQ_CMD_ADDR,v)
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_RELEASE_WR_CMD_BMSK                       0x8
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_RELEASE_WR_CMD_SHFT                       0x3
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_RELEASE_RD_CMD_BMSK                       0x4
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_RELEASE_RD_CMD_SHFT                       0x2
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_POP_CMD_BMSK                              0x2
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_POP_CMD_SHFT                              0x1
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_WRITE_CMD_BMSK                            0x1
#define HWIO_IPA_UC_PROC_TX_CMDQ_CMD_WRITE_CMD_SHFT                              0

#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_ADDR                               (IPA_REG_BASE      + 0x00003150)
#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_PHYS                               (IPA_REG_BASE_PHYS + 0x00003150)
#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_RMSK                                      0x3
#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_SHFT                                        0
#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_IN                                 \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_CFG_ADDR, HWIO_IPA_UC_PROC_TX_CMDQ_CFG_RMSK)
#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_INM(m)                             \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_CFG_ADDR, m)
#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_OUT(v)                             \
        out_dword(HWIO_IPA_UC_PROC_TX_CMDQ_CFG_ADDR,v)
#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_OUTM(m,v)                          \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_PROC_TX_CMDQ_CFG_ADDR,m,v,HWIO_IPA_UC_PROC_TX_CMDQ_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_BLOCK_WR_BMSK                             0x2
#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_BLOCK_WR_SHFT                             0x1
#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_BLOCK_RD_BMSK                             0x1
#define HWIO_IPA_UC_PROC_TX_CMDQ_CFG_BLOCK_RD_SHFT                               0

#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_ADDR                         (IPA_REG_BASE      + 0x00003154)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_PHYS                         (IPA_REG_BASE_PHYS + 0x00003154)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_RMSK                         0xffffffff
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_SHFT                                  0
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_IN                           \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_ADDR, HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_RMSK)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_INM(m)                       \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_ADDR, m)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_OUT(v)                       \
        out_dword(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_ADDR,v)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_OUTM(m,v)                    \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_ADDR,m,v,HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_TX_DEST_LEN_F_BMSK           0xffff0000
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_TX_DEST_LEN_F_SHFT                 0x10
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_TX_PACKET_LEN_F_BMSK             0xffff
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_0_TX_PACKET_LEN_F_SHFT                  0

#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_ADDR                         (IPA_REG_BASE      + 0x00003158)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_PHYS                         (IPA_REG_BASE_PHYS + 0x00003158)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_RMSK                         0xffffffff
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_SHFT                                  0
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_IN                           \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_ADDR, HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_RMSK)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_INM(m)                       \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_ADDR, m)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_OUT(v)                       \
        out_dword(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_ADDR,v)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_OUTM(m,v)                    \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_ADDR,m,v,HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_TX_DEST_ADDR_F_BMSK          0xffff0000
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_TX_DEST_ADDR_F_SHFT                0x10
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_TX_FLAGS_F_BMSK                  0xfc00
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_TX_FLAGS_F_SHFT                     0xa
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_TX_ORDER_F_BMSK                   0x300
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_TX_ORDER_F_SHFT                     0x8
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_TX_DEST_PIPE_F_BMSK                0xff
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_WR_1_TX_DEST_PIPE_F_SHFT                   0

#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_ADDR                         (IPA_REG_BASE      + 0x0000315c)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_PHYS                         (IPA_REG_BASE_PHYS + 0x0000315c)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_RMSK                         0xffffffff
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_SHFT                                  0
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_IN                           \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_ADDR, HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_RMSK)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_INM(m)                       \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_ADDR, m)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_TX_DEST_LEN_F_BMSK           0xffff0000
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_TX_DEST_LEN_F_SHFT                 0x10
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_TX_PACKET_LEN_F_BMSK             0xffff
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_0_TX_PACKET_LEN_F_SHFT                  0

#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_ADDR                         (IPA_REG_BASE      + 0x00003160)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_PHYS                         (IPA_REG_BASE_PHYS + 0x00003160)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_RMSK                         0xffffffff
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_SHFT                                  0
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_IN                           \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_ADDR, HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_RMSK)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_INM(m)                       \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_ADDR, m)
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_TX_DEST_ADDR_F_BMSK          0xffff0000
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_TX_DEST_ADDR_F_SHFT                0x10
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_TX_FLAGS_F_BMSK                  0xfc00
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_TX_FLAGS_F_SHFT                     0xa
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_TX_ORDER_F_BMSK                   0x300
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_TX_ORDER_F_SHFT                     0x8
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_TX_DEST_PIPE_F_BMSK                0xff
#define HWIO_IPA_UC_PROC_TX_CMDQ_DATA_RD_1_TX_DEST_PIPE_F_SHFT                   0

#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_ADDR                            (IPA_REG_BASE      + 0x00003164)
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_PHYS                            (IPA_REG_BASE_PHYS + 0x00003164)
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_RMSK                                 0xff7
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_SHFT                                     0
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_IN                              \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_ADDR, HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_RMSK)
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_INM(m)                          \
        in_dword_masked(HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_ADDR, m)
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_CMDQ_DEPTH_BMSK                      0xf00
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_CMDQ_DEPTH_SHFT                        0x8
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_CMDQ_COUNT_BMSK                       0xf0
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_CMDQ_COUNT_SHFT                        0x4
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_CMDQ_FULL_BMSK                         0x4
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_CMDQ_FULL_SHFT                         0x2
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_CMDQ_EMPTY_BMSK                        0x2
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_CMDQ_EMPTY_SHFT                        0x1
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_STATUS_BMSK                            0x1
#define HWIO_IPA_UC_PROC_TX_CMDQ_STATUS_STATUS_SHFT                              0

#define HWIO_IPA_PROC_UC_CMDQ_CMD_ADDR                                  (IPA_REG_BASE      + 0x00003168)
#define HWIO_IPA_PROC_UC_CMDQ_CMD_PHYS                                  (IPA_REG_BASE_PHYS + 0x00003168)
#define HWIO_IPA_PROC_UC_CMDQ_CMD_RMSK                                         0xf
#define HWIO_IPA_PROC_UC_CMDQ_CMD_SHFT                                           0
#define HWIO_IPA_PROC_UC_CMDQ_CMD_OUT(v)                                \
        out_dword(HWIO_IPA_PROC_UC_CMDQ_CMD_ADDR,v)
#define HWIO_IPA_PROC_UC_CMDQ_CMD_RELEASE_WR_CMD_BMSK                          0x8
#define HWIO_IPA_PROC_UC_CMDQ_CMD_RELEASE_WR_CMD_SHFT                          0x3
#define HWIO_IPA_PROC_UC_CMDQ_CMD_RELEASE_RD_CMD_BMSK                          0x4
#define HWIO_IPA_PROC_UC_CMDQ_CMD_RELEASE_RD_CMD_SHFT                          0x2
#define HWIO_IPA_PROC_UC_CMDQ_CMD_POP_CMD_BMSK                                 0x2
#define HWIO_IPA_PROC_UC_CMDQ_CMD_POP_CMD_SHFT                                 0x1
#define HWIO_IPA_PROC_UC_CMDQ_CMD_WRITE_CMD_BMSK                               0x1
#define HWIO_IPA_PROC_UC_CMDQ_CMD_WRITE_CMD_SHFT                                 0

#define HWIO_IPA_PROC_UC_CMDQ_CFG_ADDR                                  (IPA_REG_BASE      + 0x0000316c)
#define HWIO_IPA_PROC_UC_CMDQ_CFG_PHYS                                  (IPA_REG_BASE_PHYS + 0x0000316c)
#define HWIO_IPA_PROC_UC_CMDQ_CFG_RMSK                                         0x3
#define HWIO_IPA_PROC_UC_CMDQ_CFG_SHFT                                           0
#define HWIO_IPA_PROC_UC_CMDQ_CFG_IN                                    \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_CFG_ADDR, HWIO_IPA_PROC_UC_CMDQ_CFG_RMSK)
#define HWIO_IPA_PROC_UC_CMDQ_CFG_INM(m)                                \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_CFG_ADDR, m)
#define HWIO_IPA_PROC_UC_CMDQ_CFG_OUT(v)                                \
        out_dword(HWIO_IPA_PROC_UC_CMDQ_CFG_ADDR,v)
#define HWIO_IPA_PROC_UC_CMDQ_CFG_OUTM(m,v)                             \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_PROC_UC_CMDQ_CFG_ADDR,m,v,HWIO_IPA_PROC_UC_CMDQ_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_PROC_UC_CMDQ_CFG_BLOCK_WR_BMSK                                0x2
#define HWIO_IPA_PROC_UC_CMDQ_CFG_BLOCK_WR_SHFT                                0x1
#define HWIO_IPA_PROC_UC_CMDQ_CFG_BLOCK_RD_BMSK                                0x1
#define HWIO_IPA_PROC_UC_CMDQ_CFG_BLOCK_RD_SHFT                                  0

#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_ADDR                            (IPA_REG_BASE      + 0x00003170)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_PHYS                            (IPA_REG_BASE_PHYS + 0x00003170)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_RMSK                            0xffffffff
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_SHFT                                     0
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_IN                              \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_ADDR, HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_RMSK)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_ADDR, m)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_OUT(v)                          \
        out_dword(HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_ADDR,v)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_OUTM(m,v)                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_ADDR,m,v,HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_TX_DEST_LEN_F_BMSK              0xffff0000
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_TX_DEST_LEN_F_SHFT                    0x10
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_TX_PACKET_LEN_F_BMSK                0xffff
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_0_TX_PACKET_LEN_F_SHFT                     0

#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_ADDR                            (IPA_REG_BASE      + 0x00003174)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_PHYS                            (IPA_REG_BASE_PHYS + 0x00003174)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_RMSK                            0xffffffff
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_SHFT                                     0
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_IN                              \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_ADDR, HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_RMSK)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_ADDR, m)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_OUT(v)                          \
        out_dword(HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_ADDR,v)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_OUTM(m,v)                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_ADDR,m,v,HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_TX_DEST_ADDR_F_BMSK             0xffff0000
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_TX_DEST_ADDR_F_SHFT                   0x10
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_TX_FLAGS_F_BMSK                     0xfc00
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_TX_FLAGS_F_SHFT                        0xa
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_TX_ORDER_F_BMSK                      0x300
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_TX_ORDER_F_SHFT                        0x8
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_TX_DEST_PIPE_F_BMSK                   0xff
#define HWIO_IPA_PROC_UC_CMDQ_DATA_WR_1_TX_DEST_PIPE_F_SHFT                      0

#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_ADDR                            (IPA_REG_BASE      + 0x00003178)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_PHYS                            (IPA_REG_BASE_PHYS + 0x00003178)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_RMSK                            0xffffffff
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_SHFT                                     0
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_IN                              \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_ADDR, HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_RMSK)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_ADDR, m)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_TX_DEST_LEN_F_BMSK              0xffff0000
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_TX_DEST_LEN_F_SHFT                    0x10
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_TX_PACKET_LEN_F_BMSK                0xffff
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_0_TX_PACKET_LEN_F_SHFT                     0

#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_ADDR                            (IPA_REG_BASE      + 0x0000317c)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_PHYS                            (IPA_REG_BASE_PHYS + 0x0000317c)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_RMSK                            0xffffffff
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_SHFT                                     0
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_IN                              \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_ADDR, HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_RMSK)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_ADDR, m)
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_TX_DEST_ADDR_F_BMSK             0xffff0000
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_TX_DEST_ADDR_F_SHFT                   0x10
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_TX_FLAGS_F_BMSK                     0xfc00
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_TX_FLAGS_F_SHFT                        0xa
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_TX_ORDER_F_BMSK                      0x300
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_TX_ORDER_F_SHFT                        0x8
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_TX_DEST_PIPE_F_BMSK                   0xff
#define HWIO_IPA_PROC_UC_CMDQ_DATA_RD_1_TX_DEST_PIPE_F_SHFT                      0

#define HWIO_IPA_PROC_UC_CMDQ_STATUS_ADDR                               (IPA_REG_BASE      + 0x00003180)
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_PHYS                               (IPA_REG_BASE_PHYS + 0x00003180)
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_RMSK                                    0xff7
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_SHFT                                        0
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_IN                                 \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_STATUS_ADDR, HWIO_IPA_PROC_UC_CMDQ_STATUS_RMSK)
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_INM(m)                             \
        in_dword_masked(HWIO_IPA_PROC_UC_CMDQ_STATUS_ADDR, m)
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_CMDQ_DEPTH_BMSK                         0xf00
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_CMDQ_DEPTH_SHFT                           0x8
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_CMDQ_COUNT_BMSK                          0xf0
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_CMDQ_COUNT_SHFT                           0x4
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_CMDQ_FULL_BMSK                            0x4
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_CMDQ_FULL_SHFT                            0x2
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_CMDQ_EMPTY_BMSK                           0x2
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_CMDQ_EMPTY_SHFT                           0x1
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_STATUS_BMSK                               0x1
#define HWIO_IPA_PROC_UC_CMDQ_STATUS_STATUS_SHFT                                 0

#define HWIO_IPA_UC_PROC_ACKQ_CMD_ADDR                                  (IPA_REG_BASE      + 0x00003184)
#define HWIO_IPA_UC_PROC_ACKQ_CMD_PHYS                                  (IPA_REG_BASE_PHYS + 0x00003184)
#define HWIO_IPA_UC_PROC_ACKQ_CMD_RMSK                                         0xf
#define HWIO_IPA_UC_PROC_ACKQ_CMD_SHFT                                           0
#define HWIO_IPA_UC_PROC_ACKQ_CMD_OUT(v)                                \
        out_dword(HWIO_IPA_UC_PROC_ACKQ_CMD_ADDR,v)
#define HWIO_IPA_UC_PROC_ACKQ_CMD_RELEASE_WR_CMD_BMSK                          0x8
#define HWIO_IPA_UC_PROC_ACKQ_CMD_RELEASE_WR_CMD_SHFT                          0x3
#define HWIO_IPA_UC_PROC_ACKQ_CMD_RELEASE_RD_CMD_BMSK                          0x4
#define HWIO_IPA_UC_PROC_ACKQ_CMD_RELEASE_RD_CMD_SHFT                          0x2
#define HWIO_IPA_UC_PROC_ACKQ_CMD_POP_CMD_BMSK                                 0x2
#define HWIO_IPA_UC_PROC_ACKQ_CMD_POP_CMD_SHFT                                 0x1
#define HWIO_IPA_UC_PROC_ACKQ_CMD_WRITE_CMD_BMSK                               0x1
#define HWIO_IPA_UC_PROC_ACKQ_CMD_WRITE_CMD_SHFT                                 0

#define HWIO_IPA_UC_PROC_ACKQ_CFG_ADDR                                  (IPA_REG_BASE      + 0x00003188)
#define HWIO_IPA_UC_PROC_ACKQ_CFG_PHYS                                  (IPA_REG_BASE_PHYS + 0x00003188)
#define HWIO_IPA_UC_PROC_ACKQ_CFG_RMSK                                         0x3
#define HWIO_IPA_UC_PROC_ACKQ_CFG_SHFT                                           0
#define HWIO_IPA_UC_PROC_ACKQ_CFG_IN                                    \
        in_dword_masked(HWIO_IPA_UC_PROC_ACKQ_CFG_ADDR, HWIO_IPA_UC_PROC_ACKQ_CFG_RMSK)
#define HWIO_IPA_UC_PROC_ACKQ_CFG_INM(m)                                \
        in_dword_masked(HWIO_IPA_UC_PROC_ACKQ_CFG_ADDR, m)
#define HWIO_IPA_UC_PROC_ACKQ_CFG_OUT(v)                                \
        out_dword(HWIO_IPA_UC_PROC_ACKQ_CFG_ADDR,v)
#define HWIO_IPA_UC_PROC_ACKQ_CFG_OUTM(m,v)                             \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_PROC_ACKQ_CFG_ADDR,m,v,HWIO_IPA_UC_PROC_ACKQ_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_PROC_ACKQ_CFG_BLOCK_WR_BMSK                                0x2
#define HWIO_IPA_UC_PROC_ACKQ_CFG_BLOCK_WR_SHFT                                0x1
#define HWIO_IPA_UC_PROC_ACKQ_CFG_BLOCK_RD_BMSK                                0x1
#define HWIO_IPA_UC_PROC_ACKQ_CFG_BLOCK_RD_SHFT                                  0

#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ADDR                            (IPA_REG_BASE      + 0x0000318c)
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_PHYS                            (IPA_REG_BASE_PHYS + 0x0000318c)
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_RMSK                            0xffffffff
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_SHFT                                     0
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_IN                              \
        in_dword_masked(HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ADDR, HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_RMSK)
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ADDR, m)
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_OUT(v)                          \
        out_dword(HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ADDR,v)
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_OUTM(m,v)                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ADDR,m,v,HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ACK_VALUE2_TYPE_BMSK            0xf0000000
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ACK_VALUE2_TYPE_SHFT                  0x1c
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ACK_VALUE1_TYPE_BMSK             0xf000000
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ACK_VALUE1_TYPE_SHFT                  0x18
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ACK_VALUE2_BMSK                   0xff0000
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ACK_VALUE2_SHFT                       0x10
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ACK_VALUE1_BMSK                     0xffff
#define HWIO_IPA_UC_PROC_ACKQ_DATA_WR_0_ACK_VALUE1_SHFT                          0

#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_ADDR                            (IPA_REG_BASE      + 0x00003190)
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_PHYS                            (IPA_REG_BASE_PHYS + 0x00003190)
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_RMSK                            0xffffffff
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_SHFT                                     0
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_IN                              \
        in_dword_masked(HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_ADDR, HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_RMSK)
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_ADDR, m)
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_ACK_VALUE2_TYPE_BMSK            0xf0000000
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_ACK_VALUE2_TYPE_SHFT                  0x1c
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_ACK_VALUE1_TYPE_BMSK             0xf000000
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_ACK_VALUE1_TYPE_SHFT                  0x18
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_ACK_VALUE2_BMSK                   0xff0000
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_ACK_VALUE2_SHFT                       0x10
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_ACK_VALUE1_BMSK                     0xffff
#define HWIO_IPA_UC_PROC_ACKQ_DATA_RD_0_ACK_VALUE1_SHFT                          0

#define HWIO_IPA_UC_PROC_ACKQ_STATUS_ADDR                               (IPA_REG_BASE      + 0x00003194)
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_PHYS                               (IPA_REG_BASE_PHYS + 0x00003194)
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_RMSK                                    0xff7
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_SHFT                                        0
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_IN                                 \
        in_dword_masked(HWIO_IPA_UC_PROC_ACKQ_STATUS_ADDR, HWIO_IPA_UC_PROC_ACKQ_STATUS_RMSK)
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_INM(m)                             \
        in_dword_masked(HWIO_IPA_UC_PROC_ACKQ_STATUS_ADDR, m)
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_ACKQ_DEPTH_BMSK                         0xf00
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_ACKQ_DEPTH_SHFT                           0x8
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_ACKQ_COUNT_BMSK                          0xf0
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_ACKQ_COUNT_SHFT                           0x4
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_ACKQ_FULL_BMSK                            0x4
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_ACKQ_FULL_SHFT                            0x2
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_ACKQ_EMPTY_BMSK                           0x2
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_ACKQ_EMPTY_SHFT                           0x1
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_STATUS_BMSK                               0x1
#define HWIO_IPA_UC_PROC_ACKQ_STATUS_STATUS_SHFT                                 0

#define HWIO_IPA_PROC_UC_ACKQ_CMD_ADDR                                  (IPA_REG_BASE      + 0x00003198)
#define HWIO_IPA_PROC_UC_ACKQ_CMD_PHYS                                  (IPA_REG_BASE_PHYS + 0x00003198)
#define HWIO_IPA_PROC_UC_ACKQ_CMD_RMSK                                         0xf
#define HWIO_IPA_PROC_UC_ACKQ_CMD_SHFT                                           0
#define HWIO_IPA_PROC_UC_ACKQ_CMD_OUT(v)                                \
        out_dword(HWIO_IPA_PROC_UC_ACKQ_CMD_ADDR,v)
#define HWIO_IPA_PROC_UC_ACKQ_CMD_RELEASE_WR_CMD_BMSK                          0x8
#define HWIO_IPA_PROC_UC_ACKQ_CMD_RELEASE_WR_CMD_SHFT                          0x3
#define HWIO_IPA_PROC_UC_ACKQ_CMD_RELEASE_RD_CMD_BMSK                          0x4
#define HWIO_IPA_PROC_UC_ACKQ_CMD_RELEASE_RD_CMD_SHFT                          0x2
#define HWIO_IPA_PROC_UC_ACKQ_CMD_POP_CMD_BMSK                                 0x2
#define HWIO_IPA_PROC_UC_ACKQ_CMD_POP_CMD_SHFT                                 0x1
#define HWIO_IPA_PROC_UC_ACKQ_CMD_WRITE_CMD_BMSK                               0x1
#define HWIO_IPA_PROC_UC_ACKQ_CMD_WRITE_CMD_SHFT                                 0

#define HWIO_IPA_PROC_UC_ACKQ_CFG_ADDR                                  (IPA_REG_BASE      + 0x0000319c)
#define HWIO_IPA_PROC_UC_ACKQ_CFG_PHYS                                  (IPA_REG_BASE_PHYS + 0x0000319c)
#define HWIO_IPA_PROC_UC_ACKQ_CFG_RMSK                                         0x3
#define HWIO_IPA_PROC_UC_ACKQ_CFG_SHFT                                           0
#define HWIO_IPA_PROC_UC_ACKQ_CFG_IN                                    \
        in_dword_masked(HWIO_IPA_PROC_UC_ACKQ_CFG_ADDR, HWIO_IPA_PROC_UC_ACKQ_CFG_RMSK)
#define HWIO_IPA_PROC_UC_ACKQ_CFG_INM(m)                                \
        in_dword_masked(HWIO_IPA_PROC_UC_ACKQ_CFG_ADDR, m)
#define HWIO_IPA_PROC_UC_ACKQ_CFG_OUT(v)                                \
        out_dword(HWIO_IPA_PROC_UC_ACKQ_CFG_ADDR,v)
#define HWIO_IPA_PROC_UC_ACKQ_CFG_OUTM(m,v)                             \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_PROC_UC_ACKQ_CFG_ADDR,m,v,HWIO_IPA_PROC_UC_ACKQ_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_PROC_UC_ACKQ_CFG_BLOCK_WR_BMSK                                0x2
#define HWIO_IPA_PROC_UC_ACKQ_CFG_BLOCK_WR_SHFT                                0x1
#define HWIO_IPA_PROC_UC_ACKQ_CFG_BLOCK_RD_BMSK                                0x1
#define HWIO_IPA_PROC_UC_ACKQ_CFG_BLOCK_RD_SHFT                                  0

#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ADDR                            (IPA_REG_BASE      + 0x000031a0)
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_PHYS                            (IPA_REG_BASE_PHYS + 0x000031a0)
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_RMSK                            0xffffffff
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_SHFT                                     0
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_IN                              \
        in_dword_masked(HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ADDR, HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_RMSK)
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ADDR, m)
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_OUT(v)                          \
        out_dword(HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ADDR,v)
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_OUTM(m,v)                       \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ADDR,m,v,HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ACK_VALUE2_TYPE_BMSK            0xf0000000
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ACK_VALUE2_TYPE_SHFT                  0x1c
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ACK_VALUE1_TYPE_BMSK             0xf000000
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ACK_VALUE1_TYPE_SHFT                  0x18
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ACK_VALUE2_BMSK                   0xff0000
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ACK_VALUE2_SHFT                       0x10
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ACK_VALUE1_BMSK                     0xffff
#define HWIO_IPA_PROC_UC_ACKQ_DATA_WR_0_ACK_VALUE1_SHFT                          0

#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_ADDR                            (IPA_REG_BASE      + 0x000031a4)
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_PHYS                            (IPA_REG_BASE_PHYS + 0x000031a4)
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_RMSK                            0xffffffff
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_SHFT                                     0
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_IN                              \
        in_dword_masked(HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_ADDR, HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_RMSK)
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_INM(m)                          \
        in_dword_masked(HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_ADDR, m)
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_ACK_VALUE2_TYPE_BMSK            0xf0000000
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_ACK_VALUE2_TYPE_SHFT                  0x1c
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_ACK_VALUE1_TYPE_BMSK             0xf000000
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_ACK_VALUE1_TYPE_SHFT                  0x18
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_ACK_VALUE2_BMSK                   0xff0000
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_ACK_VALUE2_SHFT                       0x10
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_ACK_VALUE1_BMSK                     0xffff
#define HWIO_IPA_PROC_UC_ACKQ_DATA_RD_0_ACK_VALUE1_SHFT                          0

#define HWIO_IPA_PROC_UC_ACKQ_STATUS_ADDR                               (IPA_REG_BASE      + 0x000031a8)
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_PHYS                               (IPA_REG_BASE_PHYS + 0x000031a8)
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_RMSK                                    0xff7
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_SHFT                                        0
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_IN                                 \
        in_dword_masked(HWIO_IPA_PROC_UC_ACKQ_STATUS_ADDR, HWIO_IPA_PROC_UC_ACKQ_STATUS_RMSK)
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_INM(m)                             \
        in_dword_masked(HWIO_IPA_PROC_UC_ACKQ_STATUS_ADDR, m)
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_ACKQ_DEPTH_BMSK                         0xf00
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_ACKQ_DEPTH_SHFT                           0x8
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_ACKQ_COUNT_BMSK                          0xf0
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_ACKQ_COUNT_SHFT                           0x4
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_ACKQ_FULL_BMSK                            0x4
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_ACKQ_FULL_SHFT                            0x2
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_ACKQ_EMPTY_BMSK                           0x2
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_ACKQ_EMPTY_SHFT                           0x1
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_STATUS_BMSK                               0x1
#define HWIO_IPA_PROC_UC_ACKQ_STATUS_STATUS_SHFT                                 0

#define HWIO_IPA_STEP_MODE_REG_ADDR                                     (IPA_REG_BASE      + 0x00003080)
#define HWIO_IPA_STEP_MODE_REG_PHYS                                     (IPA_REG_BASE_PHYS + 0x00003080)
#define HWIO_IPA_STEP_MODE_REG_RMSK                                          0x3ff
#define HWIO_IPA_STEP_MODE_REG_SHFT                                              0
#define HWIO_IPA_STEP_MODE_REG_IN                                       \
        in_dword_masked(HWIO_IPA_STEP_MODE_REG_ADDR, HWIO_IPA_STEP_MODE_REG_RMSK)
#define HWIO_IPA_STEP_MODE_REG_INM(m)                                   \
        in_dword_masked(HWIO_IPA_STEP_MODE_REG_ADDR, m)
#define HWIO_IPA_STEP_MODE_REG_OUT(v)                                   \
        out_dword(HWIO_IPA_STEP_MODE_REG_ADDR,v)
#define HWIO_IPA_STEP_MODE_REG_OUTM(m,v)                                \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_STEP_MODE_REG_ADDR,m,v,HWIO_IPA_STEP_MODE_REG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_STEP_MODE_REG_IPA_STEP_MODE_BMSK                            0x3ff
#define HWIO_IPA_STEP_MODE_REG_IPA_STEP_MODE_SHFT                                0

#define HWIO_IPA_HW_EVENTS_CFG_ADDR                                     (IPA_REG_BASE      + 0x00003084)
#define HWIO_IPA_HW_EVENTS_CFG_PHYS                                     (IPA_REG_BASE_PHYS + 0x00003084)
#define HWIO_IPA_HW_EVENTS_CFG_RMSK                                          0x1ff
#define HWIO_IPA_HW_EVENTS_CFG_SHFT                                              0
#define HWIO_IPA_HW_EVENTS_CFG_IN                                       \
        in_dword_masked(HWIO_IPA_HW_EVENTS_CFG_ADDR, HWIO_IPA_HW_EVENTS_CFG_RMSK)
#define HWIO_IPA_HW_EVENTS_CFG_INM(m)                                   \
        in_dword_masked(HWIO_IPA_HW_EVENTS_CFG_ADDR, m)
#define HWIO_IPA_HW_EVENTS_CFG_OUT(v)                                   \
        out_dword(HWIO_IPA_HW_EVENTS_CFG_ADDR,v)
#define HWIO_IPA_HW_EVENTS_CFG_OUTM(m,v)                                \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_HW_EVENTS_CFG_ADDR,m,v,HWIO_IPA_HW_EVENTS_CFG_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_HW_EVENTS_CFG_RX_EVENTS_PIPE_SELECT_BMSK                    0x1f0
#define HWIO_IPA_HW_EVENTS_CFG_RX_EVENTS_PIPE_SELECT_SHFT                      0x4
#define HWIO_IPA_HW_EVENTS_CFG_HW_EVENTS_SELECT_BMSK                           0xf
#define HWIO_IPA_HW_EVENTS_CFG_HW_EVENTS_SELECT_SHFT                             0

#define HWIO_IPA_REPLICATE_ADDR                                         (IPA_REG_BASE      + 0x00003088)
#define HWIO_IPA_REPLICATE_PHYS                                         (IPA_REG_BASE_PHYS + 0x00003088)
#define HWIO_IPA_REPLICATE_RMSK                                                0x3
#define HWIO_IPA_REPLICATE_SHFT                                                  0
#define HWIO_IPA_REPLICATE_IN                                           \
        in_dword_masked(HWIO_IPA_REPLICATE_ADDR, HWIO_IPA_REPLICATE_RMSK)
#define HWIO_IPA_REPLICATE_INM(m)                                       \
        in_dword_masked(HWIO_IPA_REPLICATE_ADDR, m)
#define HWIO_IPA_REPLICATE_OUT(v)                                       \
        out_dword(HWIO_IPA_REPLICATE_ADDR,v)
#define HWIO_IPA_REPLICATE_OUTM(m,v)                                    \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_REPLICATE_ADDR,m,v,HWIO_IPA_REPLICATE_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_REPLICATE_REPLICATE_LENGTH_BMSK                               0x2
#define HWIO_IPA_REPLICATE_REPLICATE_LENGTH_SHFT                               0x1
#define HWIO_IPA_REPLICATE_REPLICATE_EN_BMSK                                   0x1
#define HWIO_IPA_REPLICATE_REPLICATE_EN_SHFT                                     0

#define HWIO_IPA_LOG_BUF_STATUS_ADDR_ADDR                               (IPA_REG_BASE      + 0x000020a0)
#define HWIO_IPA_LOG_BUF_STATUS_ADDR_PHYS                               (IPA_REG_BASE_PHYS + 0x000020a0)
#define HWIO_IPA_LOG_BUF_STATUS_ADDR_RMSK                               0xffffffff
#define HWIO_IPA_LOG_BUF_STATUS_ADDR_SHFT                                        0
#define HWIO_IPA_LOG_BUF_STATUS_ADDR_IN                                 \
        in_dword_masked(HWIO_IPA_LOG_BUF_STATUS_ADDR_ADDR, HWIO_IPA_LOG_BUF_STATUS_ADDR_RMSK)
#define HWIO_IPA_LOG_BUF_STATUS_ADDR_INM(m)                             \
        in_dword_masked(HWIO_IPA_LOG_BUF_STATUS_ADDR_ADDR, m)
#define HWIO_IPA_LOG_BUF_STATUS_ADDR_OUT(v)                             \
        out_dword(HWIO_IPA_LOG_BUF_STATUS_ADDR_ADDR,v)
#define HWIO_IPA_LOG_BUF_STATUS_ADDR_OUTM(m,v)                          \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_LOG_BUF_STATUS_ADDR_ADDR,m,v,HWIO_IPA_LOG_BUF_STATUS_ADDR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_LOG_BUF_STATUS_ADDR_START_ADDR_BMSK                    0xffffffff
#define HWIO_IPA_LOG_BUF_STATUS_ADDR_START_ADDR_SHFT                             0

#define HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_ADDR                          (IPA_REG_BASE      + 0x000020a4)
#define HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_PHYS                          (IPA_REG_BASE_PHYS + 0x000020a4)
#define HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_RMSK                          0xffffffff
#define HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_SHFT                                   0
#define HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_IN                            \
        in_dword_masked(HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_ADDR, HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_RMSK)
#define HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_INM(m)                        \
        in_dword_masked(HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_ADDR, m)
#define HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_OUT(v)                        \
        out_dword(HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_ADDR,v)
#define HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_OUTM(m,v)                     \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_ADDR,m,v,HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_WRITE_ADDR_BMSK               0xffffffff
#define HWIO_IPA_LOG_BUF_STATUS_WRITE_PTR_WRITE_ADDR_SHFT                        0

#define HWIO_IPA_LOG_BUF_STATUS_SIZE_ADDR                               (IPA_REG_BASE      + 0x000020a8)
#define HWIO_IPA_LOG_BUF_STATUS_SIZE_PHYS                               (IPA_REG_BASE_PHYS + 0x000020a8)
#define HWIO_IPA_LOG_BUF_STATUS_SIZE_RMSK                                  0x1ffff
#define HWIO_IPA_LOG_BUF_STATUS_SIZE_SHFT                                        0
#define HWIO_IPA_LOG_BUF_STATUS_SIZE_IN                                 \
        in_dword_masked(HWIO_IPA_LOG_BUF_STATUS_SIZE_ADDR, HWIO_IPA_LOG_BUF_STATUS_SIZE_RMSK)
#define HWIO_IPA_LOG_BUF_STATUS_SIZE_INM(m)                             \
        in_dword_masked(HWIO_IPA_LOG_BUF_STATUS_SIZE_ADDR, m)
#define HWIO_IPA_LOG_BUF_STATUS_SIZE_OUT(v)                             \
        out_dword(HWIO_IPA_LOG_BUF_STATUS_SIZE_ADDR,v)
#define HWIO_IPA_LOG_BUF_STATUS_SIZE_OUTM(m,v)                          \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_LOG_BUF_STATUS_SIZE_ADDR,m,v,HWIO_IPA_LOG_BUF_STATUS_SIZE_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_LOG_BUF_STATUS_SIZE_DISABLE_BMSK                          0x10000
#define HWIO_IPA_LOG_BUF_STATUS_SIZE_DISABLE_SHFT                             0x10
#define HWIO_IPA_LOG_BUF_STATUS_SIZE_SIZE_BMSK                              0xffff
#define HWIO_IPA_LOG_BUF_STATUS_SIZE_SIZE_SHFT                                   0

#define HWIO_IPA_LOG_BUF_HW_CMD_ADDR_ADDR                               (IPA_REG_BASE      + 0x000030ac)
#define HWIO_IPA_LOG_BUF_HW_CMD_ADDR_PHYS                               (IPA_REG_BASE_PHYS + 0x000030ac)
#define HWIO_IPA_LOG_BUF_HW_CMD_ADDR_RMSK                               0xffffffff
#define HWIO_IPA_LOG_BUF_HW_CMD_ADDR_SHFT                                        0
#define HWIO_IPA_LOG_BUF_HW_CMD_ADDR_IN                                 \
        in_dword_masked(HWIO_IPA_LOG_BUF_HW_CMD_ADDR_ADDR, HWIO_IPA_LOG_BUF_HW_CMD_ADDR_RMSK)
#define HWIO_IPA_LOG_BUF_HW_CMD_ADDR_INM(m)                             \
        in_dword_masked(HWIO_IPA_LOG_BUF_HW_CMD_ADDR_ADDR, m)
#define HWIO_IPA_LOG_BUF_HW_CMD_ADDR_OUT(v)                             \
        out_dword(HWIO_IPA_LOG_BUF_HW_CMD_ADDR_ADDR,v)
#define HWIO_IPA_LOG_BUF_HW_CMD_ADDR_OUTM(m,v)                          \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_LOG_BUF_HW_CMD_ADDR_ADDR,m,v,HWIO_IPA_LOG_BUF_HW_CMD_ADDR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_LOG_BUF_HW_CMD_ADDR_START_ADDR_BMSK                    0xffffffff
#define HWIO_IPA_LOG_BUF_HW_CMD_ADDR_START_ADDR_SHFT                             0

#define HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_ADDR                          (IPA_REG_BASE      + 0x000030b0)
#define HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_PHYS                          (IPA_REG_BASE_PHYS + 0x000030b0)
#define HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_RMSK                          0xffffffff
#define HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_SHFT                                   0
#define HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_IN                            \
        in_dword_masked(HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_ADDR, HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_RMSK)
#define HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_INM(m)                        \
        in_dword_masked(HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_ADDR, m)
#define HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_OUT(v)                        \
        out_dword(HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_ADDR,v)
#define HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_OUTM(m,v)                     \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_ADDR,m,v,HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_WRITR_ADDR_BMSK               0xffffffff
#define HWIO_IPA_LOG_BUF_HW_CMD_WRITE_PTR_WRITR_ADDR_SHFT                        0

#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_ADDR                               (IPA_REG_BASE      + 0x000030b4)
#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_PHYS                               (IPA_REG_BASE_PHYS + 0x000030b4)
#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_RMSK                                  0x1ffff
#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_SHFT                                        0
#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_IN                                 \
        in_dword_masked(HWIO_IPA_LOG_BUF_HW_CMD_SIZE_ADDR, HWIO_IPA_LOG_BUF_HW_CMD_SIZE_RMSK)
#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_INM(m)                             \
        in_dword_masked(HWIO_IPA_LOG_BUF_HW_CMD_SIZE_ADDR, m)
#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_OUT(v)                             \
        out_dword(HWIO_IPA_LOG_BUF_HW_CMD_SIZE_ADDR,v)
#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_OUTM(m,v)                          \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_LOG_BUF_HW_CMD_SIZE_ADDR,m,v,HWIO_IPA_LOG_BUF_HW_CMD_SIZE_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_DISABLE_BMSK                          0x10000
#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_DISABLE_SHFT                             0x10
#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_SIZE_BMSK                              0xffff
#define HWIO_IPA_LOG_BUF_HW_CMD_SIZE_SIZE_SHFT                                   0

#define HWIO_IPA_SPARE_REG_1_ADDR                                       (IPA_REG_BASE      + 0x00003090)
#define HWIO_IPA_SPARE_REG_1_PHYS                                       (IPA_REG_BASE_PHYS + 0x00003090)
#define HWIO_IPA_SPARE_REG_1_RMSK                                       0xffffffff
#define HWIO_IPA_SPARE_REG_1_SHFT                                                0
#define HWIO_IPA_SPARE_REG_1_IN                                         \
        in_dword_masked(HWIO_IPA_SPARE_REG_1_ADDR, HWIO_IPA_SPARE_REG_1_RMSK)
#define HWIO_IPA_SPARE_REG_1_INM(m)                                     \
        in_dword_masked(HWIO_IPA_SPARE_REG_1_ADDR, m)
#define HWIO_IPA_SPARE_REG_1_OUT(v)                                     \
        out_dword(HWIO_IPA_SPARE_REG_1_ADDR,v)
#define HWIO_IPA_SPARE_REG_1_OUTM(m,v)                                  \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_SPARE_REG_1_ADDR,m,v,HWIO_IPA_SPARE_REG_1_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_SPARE_REG_1_GENERAL_CONFIG_BMSK                        0xffffffff
#define HWIO_IPA_SPARE_REG_1_GENERAL_CONFIG_SHFT                                 0

#define HWIO_IPA_SPARE_REG_2_ADDR                                       (IPA_REG_BASE      + 0x00003094)
#define HWIO_IPA_SPARE_REG_2_PHYS                                       (IPA_REG_BASE_PHYS + 0x00003094)
#define HWIO_IPA_SPARE_REG_2_RMSK                                       0xffffffff
#define HWIO_IPA_SPARE_REG_2_SHFT                                                0
#define HWIO_IPA_SPARE_REG_2_IN                                         \
        in_dword_masked(HWIO_IPA_SPARE_REG_2_ADDR, HWIO_IPA_SPARE_REG_2_RMSK)
#define HWIO_IPA_SPARE_REG_2_INM(m)                                     \
        in_dword_masked(HWIO_IPA_SPARE_REG_2_ADDR, m)
#define HWIO_IPA_SPARE_REG_2_OUT(v)                                     \
        out_dword(HWIO_IPA_SPARE_REG_2_ADDR,v)
#define HWIO_IPA_SPARE_REG_2_OUTM(m,v)                                  \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_SPARE_REG_2_ADDR,m,v,HWIO_IPA_SPARE_REG_2_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_SPARE_REG_2_GENERAL_CONFIG_BMSK                        0xffffffff
#define HWIO_IPA_SPARE_REG_2_GENERAL_CONFIG_SHFT                                 0

#define HWIO_IPA_SRAM_DIRECT_ACCESS_n_ADDR(n)                           (IPA_REG_BASE      + 0x00005000 + 0x4 * (n))
#define HWIO_IPA_SRAM_DIRECT_ACCESS_n_PHYS(n)                           (IPA_REG_BASE_PHYS + 0x00005000 + 0x4 * (n))
#define HWIO_IPA_SRAM_DIRECT_ACCESS_n_RMSK                              0xffffffff
#define HWIO_IPA_SRAM_DIRECT_ACCESS_n_SHFT                                       0
#define HWIO_IPA_SRAM_DIRECT_ACCESS_n_MAXn                                   0xfff
#define HWIO_IPA_SRAM_DIRECT_ACCESS_n_INI(n) \
        in_dword(HWIO_IPA_SRAM_DIRECT_ACCESS_n_ADDR(n))
#define HWIO_IPA_SRAM_DIRECT_ACCESS_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_SRAM_DIRECT_ACCESS_n_ADDR(n), mask)
#define HWIO_IPA_SRAM_DIRECT_ACCESS_n_OUTI(n,val) \
        out_dword(HWIO_IPA_SRAM_DIRECT_ACCESS_n_ADDR(n),val)
#define HWIO_IPA_SRAM_DIRECT_ACCESS_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_SRAM_DIRECT_ACCESS_n_ADDR(n),mask,val,HWIO_IPA_SRAM_DIRECT_ACCESS_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_SRAM_DIRECT_ACCESS_n_DATA_WORD_BMSK                    0xffffffff
#define HWIO_IPA_SRAM_DIRECT_ACCESS_n_DATA_WORD_SHFT                             0

/*------------------------------------------------------------------------------
 * MODULE: ipa_uc
 *------------------------------------------------------------------------------*/

#define IPA_UC_REG_BASE                                                 (IPA_WRAPPER_BASE + 0x00030000)
#define IPA_UC_REG_BASE_PHYS                                            0x00030000

#define HWIO_IPA_UC_STATUS_ADDR                                         (IPA_UC_REG_BASE      + 0x00008000)
#define HWIO_IPA_UC_STATUS_PHYS                                         (IPA_UC_REG_BASE_PHYS + 0x00008000)
#define HWIO_IPA_UC_STATUS_RMSK                                                0x7
#define HWIO_IPA_UC_STATUS_SHFT                                                  0
#define HWIO_IPA_UC_STATUS_IN                                           \
        in_dword_masked(HWIO_IPA_UC_STATUS_ADDR, HWIO_IPA_UC_STATUS_RMSK)
#define HWIO_IPA_UC_STATUS_INM(m)                                       \
        in_dword_masked(HWIO_IPA_UC_STATUS_ADDR, m)
#define HWIO_IPA_UC_STATUS_LOCKUP_BMSK                                         0x4
#define HWIO_IPA_UC_STATUS_LOCKUP_SHFT                                         0x2
#define HWIO_IPA_UC_STATUS_SLEEP_BMSK                                          0x2
#define HWIO_IPA_UC_STATUS_SLEEP_SHFT                                          0x1
#define HWIO_IPA_UC_STATUS_SLEEPDEEP_BMSK                                      0x1
#define HWIO_IPA_UC_STATUS_SLEEPDEEP_SHFT                                        0

#define HWIO_IPA_UC_CONTROL_ADDR                                        (IPA_UC_REG_BASE      + 0x00008004)
#define HWIO_IPA_UC_CONTROL_PHYS                                        (IPA_UC_REG_BASE_PHYS + 0x00008004)
#define HWIO_IPA_UC_CONTROL_RMSK                                              0xf7
#define HWIO_IPA_UC_CONTROL_SHFT                                                 0
#define HWIO_IPA_UC_CONTROL_IN                                          \
        in_dword_masked(HWIO_IPA_UC_CONTROL_ADDR, HWIO_IPA_UC_CONTROL_RMSK)
#define HWIO_IPA_UC_CONTROL_INM(m)                                      \
        in_dword_masked(HWIO_IPA_UC_CONTROL_ADDR, m)
#define HWIO_IPA_UC_CONTROL_OUT(v)                                      \
        out_dword(HWIO_IPA_UC_CONTROL_ADDR,v)
#define HWIO_IPA_UC_CONTROL_OUTM(m,v)                                   \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_CONTROL_ADDR,m,v,HWIO_IPA_UC_CONTROL_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_CONTROL_MBOX_DIS_BMSK                                     0xf0
#define HWIO_IPA_UC_CONTROL_MBOX_DIS_SHFT                                      0x4
#define HWIO_IPA_UC_CONTROL_QMB_SNOC_BYPASS_DIS_BMSK                           0x4
#define HWIO_IPA_UC_CONTROL_QMB_SNOC_BYPASS_DIS_SHFT                           0x2
#define HWIO_IPA_UC_CONTROL_UC_DSMODE_BMSK                                     0x2
#define HWIO_IPA_UC_CONTROL_UC_DSMODE_SHFT                                     0x1
#define HWIO_IPA_UC_CONTROL_UC_ENABLE_BMSK                                     0x1
#define HWIO_IPA_UC_CONTROL_UC_ENABLE_SHFT                                       0

#define HWIO_IPA_UC_BASE_ADDR_ADDR                                      (IPA_UC_REG_BASE      + 0x00008008)
#define HWIO_IPA_UC_BASE_ADDR_PHYS                                      (IPA_UC_REG_BASE_PHYS + 0x00008008)
#define HWIO_IPA_UC_BASE_ADDR_RMSK                                      0xffffffff
#define HWIO_IPA_UC_BASE_ADDR_SHFT                                               0
#define HWIO_IPA_UC_BASE_ADDR_IN                                        \
        in_dword_masked(HWIO_IPA_UC_BASE_ADDR_ADDR, HWIO_IPA_UC_BASE_ADDR_RMSK)
#define HWIO_IPA_UC_BASE_ADDR_INM(m)                                    \
        in_dword_masked(HWIO_IPA_UC_BASE_ADDR_ADDR, m)
#define HWIO_IPA_UC_BASE_ADDR_OUT(v)                                    \
        out_dword(HWIO_IPA_UC_BASE_ADDR_ADDR,v)
#define HWIO_IPA_UC_BASE_ADDR_OUTM(m,v)                                 \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_BASE_ADDR_ADDR,m,v,HWIO_IPA_UC_BASE_ADDR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_BASE_ADDR_BASE_BMSK                                 0xfffc0000
#define HWIO_IPA_UC_BASE_ADDR_BASE_SHFT                                       0x12
#define HWIO_IPA_UC_BASE_ADDR_ZERO_BMSK                                    0x3ffff
#define HWIO_IPA_UC_BASE_ADDR_ZERO_SHFT                                          0

#define HWIO_IPA_UC_SYS_BUS_ATTRIB_ADDR                                 (IPA_UC_REG_BASE      + 0x0000800c)
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_PHYS                                 (IPA_UC_REG_BASE_PHYS + 0x0000800c)
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_RMSK                                     0x1117
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_SHFT                                          0
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_IN                                   \
        in_dword_masked(HWIO_IPA_UC_SYS_BUS_ATTRIB_ADDR, HWIO_IPA_UC_SYS_BUS_ATTRIB_RMSK)
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_INM(m)                               \
        in_dword_masked(HWIO_IPA_UC_SYS_BUS_ATTRIB_ADDR, m)
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_OUT(v)                               \
        out_dword(HWIO_IPA_UC_SYS_BUS_ATTRIB_ADDR,v)
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_OUTM(m,v)                            \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_SYS_BUS_ATTRIB_ADDR,m,v,HWIO_IPA_UC_SYS_BUS_ATTRIB_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_SHARED_BMSK                              0x1000
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_SHARED_SHFT                                 0xc
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_INNERSHARED_BMSK                          0x100
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_INNERSHARED_SHFT                            0x8
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_NOALLOCATE_BMSK                            0x10
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_NOALLOCATE_SHFT                             0x4
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_MEMTYPE_BMSK                                0x7
#define HWIO_IPA_UC_SYS_BUS_ATTRIB_MEMTYPE_SHFT                                  0

#define HWIO_IPA_UC_QMB_SYS_ADDR_ADDR                                   (IPA_UC_REG_BASE      + 0x00008100)
#define HWIO_IPA_UC_QMB_SYS_ADDR_PHYS                                   (IPA_UC_REG_BASE_PHYS + 0x00008100)
#define HWIO_IPA_UC_QMB_SYS_ADDR_RMSK                                   0xffffffff
#define HWIO_IPA_UC_QMB_SYS_ADDR_SHFT                                            0
#define HWIO_IPA_UC_QMB_SYS_ADDR_IN                                     \
        in_dword_masked(HWIO_IPA_UC_QMB_SYS_ADDR_ADDR, HWIO_IPA_UC_QMB_SYS_ADDR_RMSK)
#define HWIO_IPA_UC_QMB_SYS_ADDR_INM(m)                                 \
        in_dword_masked(HWIO_IPA_UC_QMB_SYS_ADDR_ADDR, m)
#define HWIO_IPA_UC_QMB_SYS_ADDR_OUT(v)                                 \
        out_dword(HWIO_IPA_UC_QMB_SYS_ADDR_ADDR,v)
#define HWIO_IPA_UC_QMB_SYS_ADDR_OUTM(m,v)                              \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_QMB_SYS_ADDR_ADDR,m,v,HWIO_IPA_UC_QMB_SYS_ADDR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_QMB_SYS_ADDR_ADDR_BMSK                              0xffffffff
#define HWIO_IPA_UC_QMB_SYS_ADDR_ADDR_SHFT                                       0

#define HWIO_IPA_UC_QMB_LOCAL_ADDR_ADDR                                 (IPA_UC_REG_BASE      + 0x00008104)
#define HWIO_IPA_UC_QMB_LOCAL_ADDR_PHYS                                 (IPA_UC_REG_BASE_PHYS + 0x00008104)
#define HWIO_IPA_UC_QMB_LOCAL_ADDR_RMSK                                    0x17fff
#define HWIO_IPA_UC_QMB_LOCAL_ADDR_SHFT                                          0
#define HWIO_IPA_UC_QMB_LOCAL_ADDR_IN                                   \
        in_dword_masked(HWIO_IPA_UC_QMB_LOCAL_ADDR_ADDR, HWIO_IPA_UC_QMB_LOCAL_ADDR_RMSK)
#define HWIO_IPA_UC_QMB_LOCAL_ADDR_INM(m)                               \
        in_dword_masked(HWIO_IPA_UC_QMB_LOCAL_ADDR_ADDR, m)
#define HWIO_IPA_UC_QMB_LOCAL_ADDR_OUT(v)                               \
        out_dword(HWIO_IPA_UC_QMB_LOCAL_ADDR_ADDR,v)
#define HWIO_IPA_UC_QMB_LOCAL_ADDR_OUTM(m,v)                            \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_QMB_LOCAL_ADDR_ADDR,m,v,HWIO_IPA_UC_QMB_LOCAL_ADDR_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_QMB_LOCAL_ADDR_IPA_ADDR_BMSK                           0x10000
#define HWIO_IPA_UC_QMB_LOCAL_ADDR_IPA_ADDR_SHFT                              0x10
#define HWIO_IPA_UC_QMB_LOCAL_ADDR_ADDR_BMSK                                0x7fff
#define HWIO_IPA_UC_QMB_LOCAL_ADDR_ADDR_SHFT                                     0

#define HWIO_IPA_UC_QMB_LENGTH_ADDR                                     (IPA_UC_REG_BASE      + 0x00008108)
#define HWIO_IPA_UC_QMB_LENGTH_PHYS                                     (IPA_UC_REG_BASE_PHYS + 0x00008108)
#define HWIO_IPA_UC_QMB_LENGTH_RMSK                                           0x7f
#define HWIO_IPA_UC_QMB_LENGTH_SHFT                                              0
#define HWIO_IPA_UC_QMB_LENGTH_IN                                       \
        in_dword_masked(HWIO_IPA_UC_QMB_LENGTH_ADDR, HWIO_IPA_UC_QMB_LENGTH_RMSK)
#define HWIO_IPA_UC_QMB_LENGTH_INM(m)                                   \
        in_dword_masked(HWIO_IPA_UC_QMB_LENGTH_ADDR, m)
#define HWIO_IPA_UC_QMB_LENGTH_OUT(v)                                   \
        out_dword(HWIO_IPA_UC_QMB_LENGTH_ADDR,v)
#define HWIO_IPA_UC_QMB_LENGTH_OUTM(m,v)                                \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_QMB_LENGTH_ADDR,m,v,HWIO_IPA_UC_QMB_LENGTH_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_QMB_LENGTH_LENGTH_BMSK                                    0x7f
#define HWIO_IPA_UC_QMB_LENGTH_LENGTH_SHFT                                       0

#define HWIO_IPA_UC_QMB_TRIGGER_ADDR                                    (IPA_UC_REG_BASE      + 0x0000810c)
#define HWIO_IPA_UC_QMB_TRIGGER_PHYS                                    (IPA_UC_REG_BASE_PHYS + 0x0000810c)
#define HWIO_IPA_UC_QMB_TRIGGER_RMSK                                          0x31
#define HWIO_IPA_UC_QMB_TRIGGER_SHFT                                             0
#define HWIO_IPA_UC_QMB_TRIGGER_IN                                      \
        in_dword_masked(HWIO_IPA_UC_QMB_TRIGGER_ADDR, HWIO_IPA_UC_QMB_TRIGGER_RMSK)
#define HWIO_IPA_UC_QMB_TRIGGER_INM(m)                                  \
        in_dword_masked(HWIO_IPA_UC_QMB_TRIGGER_ADDR, m)
#define HWIO_IPA_UC_QMB_TRIGGER_OUT(v)                                  \
        out_dword(HWIO_IPA_UC_QMB_TRIGGER_ADDR,v)
#define HWIO_IPA_UC_QMB_TRIGGER_OUTM(m,v)                               \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_QMB_TRIGGER_ADDR,m,v,HWIO_IPA_UC_QMB_TRIGGER_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_QMB_TRIGGER_POSTING_BMSK                                  0x30
#define HWIO_IPA_UC_QMB_TRIGGER_POSTING_SHFT                                   0x4
#define HWIO_IPA_UC_QMB_TRIGGER_DIRECTION_BMSK                                 0x1
#define HWIO_IPA_UC_QMB_TRIGGER_DIRECTION_SHFT                                   0

#define HWIO_IPA_UC_QMB_PENDING_TID_ADDR                                (IPA_UC_REG_BASE      + 0x00008110)
#define HWIO_IPA_UC_QMB_PENDING_TID_PHYS                                (IPA_UC_REG_BASE_PHYS + 0x00008110)
#define HWIO_IPA_UC_QMB_PENDING_TID_RMSK                                   0x11117
#define HWIO_IPA_UC_QMB_PENDING_TID_SHFT                                         0
#define HWIO_IPA_UC_QMB_PENDING_TID_IN                                  \
        in_dword_masked(HWIO_IPA_UC_QMB_PENDING_TID_ADDR, HWIO_IPA_UC_QMB_PENDING_TID_RMSK)
#define HWIO_IPA_UC_QMB_PENDING_TID_INM(m)                              \
        in_dword_masked(HWIO_IPA_UC_QMB_PENDING_TID_ADDR, m)
#define HWIO_IPA_UC_QMB_PENDING_TID_ERROR_SECURITY_BMSK                    0x10000
#define HWIO_IPA_UC_QMB_PENDING_TID_ERROR_SECURITY_SHFT                       0x10
#define HWIO_IPA_UC_QMB_PENDING_TID_ERROR_MAX_COMP_BMSK                     0x1000
#define HWIO_IPA_UC_QMB_PENDING_TID_ERROR_MAX_COMP_SHFT                        0xc
#define HWIO_IPA_UC_QMB_PENDING_TID_ERROR_MAX_OS_BMSK                        0x100
#define HWIO_IPA_UC_QMB_PENDING_TID_ERROR_MAX_OS_SHFT                          0x8
#define HWIO_IPA_UC_QMB_PENDING_TID_ERROR_BUS_BMSK                            0x10
#define HWIO_IPA_UC_QMB_PENDING_TID_ERROR_BUS_SHFT                             0x4
#define HWIO_IPA_UC_QMB_PENDING_TID_TID_BMSK                                   0x7
#define HWIO_IPA_UC_QMB_PENDING_TID_TID_SHFT                                     0

#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_ADDR                          (IPA_UC_REG_BASE      + 0x00008114)
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PHYS                          (IPA_UC_REG_BASE_PHYS + 0x00008114)
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_RMSK                               0x117
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_SHFT                                   0
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_IN                            \
        in_dword_masked(HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_ADDR, HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_RMSK)
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_INM(m)                        \
        in_dword_masked(HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_ADDR, m)
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_VALID_BMSK                         0x100
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_VALID_SHFT                           0x8
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_ERROR_BMSK                          0x10
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_ERROR_SHFT                           0x4
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_TID_BMSK                             0x7
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_TID_SHFT                               0

#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_ADDR                     (IPA_UC_REG_BASE      + 0x00008118)
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_PHYS                     (IPA_UC_REG_BASE_PHYS + 0x00008118)
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_RMSK                          0x117
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_SHFT                              0
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_IN                       \
        in_dword_masked(HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_ADDR, HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_RMSK)
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_INM(m)                   \
        in_dword_masked(HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_ADDR, m)
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_VALID_BMSK                    0x100
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_VALID_SHFT                      0x8
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_ERROR_BMSK                     0x10
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_ERROR_SHFT                      0x4
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_TID_BMSK                        0x7
#define HWIO_IPA_UC_QMB_COMPLETED_RD_FIFO_PEEK_TID_SHFT                          0

#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_ADDR                          (IPA_UC_REG_BASE      + 0x0000811c)
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PHYS                          (IPA_UC_REG_BASE_PHYS + 0x0000811c)
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_RMSK                               0x117
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_SHFT                                   0
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_IN                            \
        in_dword_masked(HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_ADDR, HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_RMSK)
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_INM(m)                        \
        in_dword_masked(HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_ADDR, m)
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_VALID_BMSK                         0x100
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_VALID_SHFT                           0x8
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_ERROR_BMSK                          0x10
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_ERROR_SHFT                           0x4
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_TID_BMSK                             0x7
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_TID_SHFT                               0

#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_ADDR                     (IPA_UC_REG_BASE      + 0x00008120)
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_PHYS                     (IPA_UC_REG_BASE_PHYS + 0x00008120)
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_RMSK                          0x117
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_SHFT                              0
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_IN                       \
        in_dword_masked(HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_ADDR, HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_RMSK)
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_INM(m)                   \
        in_dword_masked(HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_ADDR, m)
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_VALID_BMSK                    0x100
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_VALID_SHFT                      0x8
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_ERROR_BMSK                     0x10
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_ERROR_SHFT                      0x4
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_TID_BMSK                        0x7
#define HWIO_IPA_UC_QMB_COMPLETED_WR_FIFO_PEEK_TID_SHFT                          0

#define HWIO_IPA_UC_QMB_MISC_ADDR                                       (IPA_UC_REG_BASE      + 0x00008124)
#define HWIO_IPA_UC_QMB_MISC_PHYS                                       (IPA_UC_REG_BASE_PHYS + 0x00008124)
#define HWIO_IPA_UC_QMB_MISC_RMSK                                        0x11333ff
#define HWIO_IPA_UC_QMB_MISC_SHFT                                                0
#define HWIO_IPA_UC_QMB_MISC_IN                                         \
        in_dword_masked(HWIO_IPA_UC_QMB_MISC_ADDR, HWIO_IPA_UC_QMB_MISC_RMSK)
#define HWIO_IPA_UC_QMB_MISC_INM(m)                                     \
        in_dword_masked(HWIO_IPA_UC_QMB_MISC_ADDR, m)
#define HWIO_IPA_UC_QMB_MISC_OUT(v)                                     \
        out_dword(HWIO_IPA_UC_QMB_MISC_ADDR,v)
#define HWIO_IPA_UC_QMB_MISC_OUTM(m,v)                                  \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_QMB_MISC_ADDR,m,v,HWIO_IPA_UC_QMB_MISC_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_QMB_MISC_OOOWR_BMSK                                  0x1000000
#define HWIO_IPA_UC_QMB_MISC_OOOWR_SHFT                                       0x18
#define HWIO_IPA_UC_QMB_MISC_OOORD_BMSK                                   0x100000
#define HWIO_IPA_UC_QMB_MISC_OOORD_SHFT                                       0x14
#define HWIO_IPA_UC_QMB_MISC_WR_PRIORITY_BMSK                              0x30000
#define HWIO_IPA_UC_QMB_MISC_WR_PRIORITY_SHFT                                 0x10
#define HWIO_IPA_UC_QMB_MISC_RD_PRIORITY_BMSK                               0x3000
#define HWIO_IPA_UC_QMB_MISC_RD_PRIORITY_SHFT                                  0xc
#define HWIO_IPA_UC_QMB_MISC_USER_BMSK                                       0x3ff
#define HWIO_IPA_UC_QMB_MISC_USER_SHFT                                           0

#define HWIO_IPA_UC_QMB_STATUS_ADDR                                     (IPA_UC_REG_BASE      + 0x00008128)
#define HWIO_IPA_UC_QMB_STATUS_PHYS                                     (IPA_UC_REG_BASE_PHYS + 0x00008128)
#define HWIO_IPA_UC_QMB_STATUS_RMSK                                     0x1fff1fff
#define HWIO_IPA_UC_QMB_STATUS_SHFT                                              0
#define HWIO_IPA_UC_QMB_STATUS_IN                                       \
        in_dword_masked(HWIO_IPA_UC_QMB_STATUS_ADDR, HWIO_IPA_UC_QMB_STATUS_RMSK)
#define HWIO_IPA_UC_QMB_STATUS_INM(m)                                   \
        in_dword_masked(HWIO_IPA_UC_QMB_STATUS_ADDR, m)
#define HWIO_IPA_UC_QMB_STATUS_COMPLETED_WR_FIFO_FULL_BMSK              0x10000000
#define HWIO_IPA_UC_QMB_STATUS_COMPLETED_WR_FIFO_FULL_SHFT                    0x1c
#define HWIO_IPA_UC_QMB_STATUS_COMPLETED_WR_CNT_BMSK                     0xf000000
#define HWIO_IPA_UC_QMB_STATUS_COMPLETED_WR_CNT_SHFT                          0x18
#define HWIO_IPA_UC_QMB_STATUS_OUTSTANDING_WR_CNT_BMSK                    0xf00000
#define HWIO_IPA_UC_QMB_STATUS_OUTSTANDING_WR_CNT_SHFT                        0x14
#define HWIO_IPA_UC_QMB_STATUS_MAX_OUTSTANDING_WR_BMSK                     0xf0000
#define HWIO_IPA_UC_QMB_STATUS_MAX_OUTSTANDING_WR_SHFT                        0x10
#define HWIO_IPA_UC_QMB_STATUS_COMPLETED_RD_FIFO_FULL_BMSK                  0x1000
#define HWIO_IPA_UC_QMB_STATUS_COMPLETED_RD_FIFO_FULL_SHFT                     0xc
#define HWIO_IPA_UC_QMB_STATUS_COMPLETED_RD_CNT_BMSK                         0xf00
#define HWIO_IPA_UC_QMB_STATUS_COMPLETED_RD_CNT_SHFT                           0x8
#define HWIO_IPA_UC_QMB_STATUS_OUTSTANDING_RD_CNT_BMSK                        0xf0
#define HWIO_IPA_UC_QMB_STATUS_OUTSTANDING_RD_CNT_SHFT                         0x4
#define HWIO_IPA_UC_QMB_STATUS_MAX_OUTSTANDING_RD_BMSK                         0xf
#define HWIO_IPA_UC_QMB_STATUS_MAX_OUTSTANDING_RD_SHFT                           0

#define HWIO_IPA_UC_QMB_BUS_ATTRIB_ADDR                                 (IPA_UC_REG_BASE      + 0x0000812c)
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_PHYS                                 (IPA_UC_REG_BASE_PHYS + 0x0000812c)
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_RMSK                                     0x1117
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_SHFT                                          0
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_IN                                   \
        in_dword_masked(HWIO_IPA_UC_QMB_BUS_ATTRIB_ADDR, HWIO_IPA_UC_QMB_BUS_ATTRIB_RMSK)
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_INM(m)                               \
        in_dword_masked(HWIO_IPA_UC_QMB_BUS_ATTRIB_ADDR, m)
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_OUT(v)                               \
        out_dword(HWIO_IPA_UC_QMB_BUS_ATTRIB_ADDR,v)
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_OUTM(m,v)                            \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_QMB_BUS_ATTRIB_ADDR,m,v,HWIO_IPA_UC_QMB_BUS_ATTRIB_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_SHARED_BMSK                              0x1000
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_SHARED_SHFT                                 0xc
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_INNERSHARED_BMSK                          0x100
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_INNERSHARED_SHFT                            0x8
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_NOALLOCATE_BMSK                            0x10
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_NOALLOCATE_SHFT                             0x4
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_MEMTYPE_BMSK                                0x7
#define HWIO_IPA_UC_QMB_BUS_ATTRIB_MEMTYPE_SHFT                                  0

#define HWIO_IPA_UC_MBOX_INT_STTS_n_ADDR(n)                             (IPA_UC_REG_BASE      + 0x00008200 + 0x10 * (n))
#define HWIO_IPA_UC_MBOX_INT_STTS_n_PHYS(n)                             (IPA_UC_REG_BASE_PHYS + 0x00008200 + 0x10 * (n))
#define HWIO_IPA_UC_MBOX_INT_STTS_n_RMSK                                    0xffff
#define HWIO_IPA_UC_MBOX_INT_STTS_n_SHFT                                         0
#define HWIO_IPA_UC_MBOX_INT_STTS_n_MAXn                                       0x3
#define HWIO_IPA_UC_MBOX_INT_STTS_n_INI(n) \
        in_dword(HWIO_IPA_UC_MBOX_INT_STTS_n_ADDR(n))
#define HWIO_IPA_UC_MBOX_INT_STTS_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_UC_MBOX_INT_STTS_n_ADDR(n), mask)
#define HWIO_IPA_UC_MBOX_INT_STTS_n_OUTI(n,val) \
        out_dword(HWIO_IPA_UC_MBOX_INT_STTS_n_ADDR(n),val)
#define HWIO_IPA_UC_MBOX_INT_STTS_n_IRQ_STATUS_BMSK                         0xffff
#define HWIO_IPA_UC_MBOX_INT_STTS_n_IRQ_STATUS_SHFT                              0

#define HWIO_IPA_UC_MBOX_INT_EN_n_ADDR(n)                               (IPA_UC_REG_BASE      + 0x00008204 + 0x10 * (n))
#define HWIO_IPA_UC_MBOX_INT_EN_n_PHYS(n)                               (IPA_UC_REG_BASE_PHYS + 0x00008204 + 0x10 * (n))
#define HWIO_IPA_UC_MBOX_INT_EN_n_RMSK                                      0xffff
#define HWIO_IPA_UC_MBOX_INT_EN_n_SHFT                                           0
#define HWIO_IPA_UC_MBOX_INT_EN_n_MAXn                                         0x3
#define HWIO_IPA_UC_MBOX_INT_EN_n_INI(n) \
        in_dword(HWIO_IPA_UC_MBOX_INT_EN_n_ADDR(n))
#define HWIO_IPA_UC_MBOX_INT_EN_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_UC_MBOX_INT_EN_n_ADDR(n), mask)
#define HWIO_IPA_UC_MBOX_INT_EN_n_OUTI(n,val) \
        out_dword(HWIO_IPA_UC_MBOX_INT_EN_n_ADDR(n),val)
#define HWIO_IPA_UC_MBOX_INT_EN_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_MBOX_INT_EN_n_ADDR(n),mask,val,HWIO_IPA_UC_MBOX_INT_EN_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_UC_MBOX_INT_EN_n_IRQ_EN_BMSK                               0xffff
#define HWIO_IPA_UC_MBOX_INT_EN_n_IRQ_EN_SHFT                                    0

#define HWIO_IPA_UC_MBOX_INT_CLR_n_ADDR(n)                              (IPA_UC_REG_BASE      + 0x00008208 + 0x10 * (n))
#define HWIO_IPA_UC_MBOX_INT_CLR_n_PHYS(n)                              (IPA_UC_REG_BASE_PHYS + 0x00008208 + 0x10 * (n))
#define HWIO_IPA_UC_MBOX_INT_CLR_n_RMSK                                     0xffff
#define HWIO_IPA_UC_MBOX_INT_CLR_n_SHFT                                          0
#define HWIO_IPA_UC_MBOX_INT_CLR_n_MAXn                                        0x3
#define HWIO_IPA_UC_MBOX_INT_CLR_n_OUTI(n,val) \
        out_dword(HWIO_IPA_UC_MBOX_INT_CLR_n_ADDR(n),val)
#define HWIO_IPA_UC_MBOX_INT_CLR_n_IRQ_CLR_BMSK                             0xffff
#define HWIO_IPA_UC_MBOX_INT_CLR_n_IRQ_CLR_SHFT                                  0

#define HWIO_IPA_UC_IPA_INT_STTS_n_ADDR(n)                              (IPA_UC_REG_BASE      + 0x00008300 + 0x10 * (n))
#define HWIO_IPA_UC_IPA_INT_STTS_n_PHYS(n)                              (IPA_UC_REG_BASE_PHYS + 0x00008300 + 0x10 * (n))
#define HWIO_IPA_UC_IPA_INT_STTS_n_RMSK                                        0xf
#define HWIO_IPA_UC_IPA_INT_STTS_n_SHFT                                          0
#define HWIO_IPA_UC_IPA_INT_STTS_n_MAXn                                        0x3
#define HWIO_IPA_UC_IPA_INT_STTS_n_INI(n) \
        in_dword(HWIO_IPA_UC_IPA_INT_STTS_n_ADDR(n))
#define HWIO_IPA_UC_IPA_INT_STTS_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_UC_IPA_INT_STTS_n_ADDR(n), mask)
#define HWIO_IPA_UC_IPA_INT_STTS_n_OUTI(n,val) \
        out_dword(HWIO_IPA_UC_IPA_INT_STTS_n_ADDR(n),val)
#define HWIO_IPA_UC_IPA_INT_STTS_n_IRQ_STATUS_BMSK                             0xf
#define HWIO_IPA_UC_IPA_INT_STTS_n_IRQ_STATUS_SHFT                               0

#define HWIO_IPA_UC_IPA_INT_EN_n_ADDR(n)                                (IPA_UC_REG_BASE      + 0x00008304 + 0x10 * (n))
#define HWIO_IPA_UC_IPA_INT_EN_n_PHYS(n)                                (IPA_UC_REG_BASE_PHYS + 0x00008304 + 0x10 * (n))
#define HWIO_IPA_UC_IPA_INT_EN_n_RMSK                                          0xf
#define HWIO_IPA_UC_IPA_INT_EN_n_SHFT                                            0
#define HWIO_IPA_UC_IPA_INT_EN_n_MAXn                                          0x3
#define HWIO_IPA_UC_IPA_INT_EN_n_INI(n) \
        in_dword(HWIO_IPA_UC_IPA_INT_EN_n_ADDR(n))
#define HWIO_IPA_UC_IPA_INT_EN_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_UC_IPA_INT_EN_n_ADDR(n), mask)
#define HWIO_IPA_UC_IPA_INT_EN_n_OUTI(n,val) \
        out_dword(HWIO_IPA_UC_IPA_INT_EN_n_ADDR(n),val)
#define HWIO_IPA_UC_IPA_INT_EN_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_IPA_INT_EN_n_ADDR(n),mask,val,HWIO_IPA_UC_IPA_INT_EN_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_UC_IPA_INT_EN_n_IRQ_EN_BMSK                                   0xf
#define HWIO_IPA_UC_IPA_INT_EN_n_IRQ_EN_SHFT                                     0

#define HWIO_IPA_UC_IPA_INT_CLR_n_ADDR(n)                               (IPA_UC_REG_BASE      + 0x00008308 + 0x10 * (n))
#define HWIO_IPA_UC_IPA_INT_CLR_n_PHYS(n)                               (IPA_UC_REG_BASE_PHYS + 0x00008308 + 0x10 * (n))
#define HWIO_IPA_UC_IPA_INT_CLR_n_RMSK                                         0xf
#define HWIO_IPA_UC_IPA_INT_CLR_n_SHFT                                           0
#define HWIO_IPA_UC_IPA_INT_CLR_n_MAXn                                         0x3
#define HWIO_IPA_UC_IPA_INT_CLR_n_OUTI(n,val) \
        out_dword(HWIO_IPA_UC_IPA_INT_CLR_n_ADDR(n),val)
#define HWIO_IPA_UC_IPA_INT_CLR_n_IRQ_CLR_BMSK                                 0xf
#define HWIO_IPA_UC_IPA_INT_CLR_n_IRQ_CLR_SHFT                                   0

#define HWIO_IPA_UC_HWEV_INT_STTS_ADDR                                  (IPA_UC_REG_BASE      + 0x00008400)
#define HWIO_IPA_UC_HWEV_INT_STTS_PHYS                                  (IPA_UC_REG_BASE_PHYS + 0x00008400)
#define HWIO_IPA_UC_HWEV_INT_STTS_RMSK                                  0xffffffff
#define HWIO_IPA_UC_HWEV_INT_STTS_SHFT                                           0
#define HWIO_IPA_UC_HWEV_INT_STTS_IN                                    \
        in_dword_masked(HWIO_IPA_UC_HWEV_INT_STTS_ADDR, HWIO_IPA_UC_HWEV_INT_STTS_RMSK)
#define HWIO_IPA_UC_HWEV_INT_STTS_INM(m)                                \
        in_dword_masked(HWIO_IPA_UC_HWEV_INT_STTS_ADDR, m)
#define HWIO_IPA_UC_HWEV_INT_STTS_IRQ_STATUS_BMSK                       0xffffffff
#define HWIO_IPA_UC_HWEV_INT_STTS_IRQ_STATUS_SHFT                                0

#define HWIO_IPA_UC_HWEV_INT_EN_ADDR                                    (IPA_UC_REG_BASE      + 0x00008404)
#define HWIO_IPA_UC_HWEV_INT_EN_PHYS                                    (IPA_UC_REG_BASE_PHYS + 0x00008404)
#define HWIO_IPA_UC_HWEV_INT_EN_RMSK                                    0xffffffff
#define HWIO_IPA_UC_HWEV_INT_EN_SHFT                                             0
#define HWIO_IPA_UC_HWEV_INT_EN_IN                                      \
        in_dword_masked(HWIO_IPA_UC_HWEV_INT_EN_ADDR, HWIO_IPA_UC_HWEV_INT_EN_RMSK)
#define HWIO_IPA_UC_HWEV_INT_EN_INM(m)                                  \
        in_dword_masked(HWIO_IPA_UC_HWEV_INT_EN_ADDR, m)
#define HWIO_IPA_UC_HWEV_INT_EN_OUT(v)                                  \
        out_dword(HWIO_IPA_UC_HWEV_INT_EN_ADDR,v)
#define HWIO_IPA_UC_HWEV_INT_EN_OUTM(m,v)                               \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_HWEV_INT_EN_ADDR,m,v,HWIO_IPA_UC_HWEV_INT_EN_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_HWEV_INT_EN_IRQ_EN_BMSK                             0xffffffff
#define HWIO_IPA_UC_HWEV_INT_EN_IRQ_EN_SHFT                                      0

#define HWIO_IPA_UC_HWEV_INT_CLR_ADDR                                   (IPA_UC_REG_BASE      + 0x00008408)
#define HWIO_IPA_UC_HWEV_INT_CLR_PHYS                                   (IPA_UC_REG_BASE_PHYS + 0x00008408)
#define HWIO_IPA_UC_HWEV_INT_CLR_RMSK                                   0xffffffff
#define HWIO_IPA_UC_HWEV_INT_CLR_SHFT                                            0
#define HWIO_IPA_UC_HWEV_INT_CLR_OUT(v)                                 \
        out_dword(HWIO_IPA_UC_HWEV_INT_CLR_ADDR,v)
#define HWIO_IPA_UC_HWEV_INT_CLR_IRQ_CLR_BMSK                           0xffffffff
#define HWIO_IPA_UC_HWEV_INT_CLR_IRQ_CLR_SHFT                                    0

#define HWIO_IPA_UC_SWEV_INT_STTS_ADDR                                  (IPA_UC_REG_BASE      + 0x00008410)
#define HWIO_IPA_UC_SWEV_INT_STTS_PHYS                                  (IPA_UC_REG_BASE_PHYS + 0x00008410)
#define HWIO_IPA_UC_SWEV_INT_STTS_RMSK                                  0xffffffff
#define HWIO_IPA_UC_SWEV_INT_STTS_SHFT                                           0
#define HWIO_IPA_UC_SWEV_INT_STTS_IN                                    \
        in_dword_masked(HWIO_IPA_UC_SWEV_INT_STTS_ADDR, HWIO_IPA_UC_SWEV_INT_STTS_RMSK)
#define HWIO_IPA_UC_SWEV_INT_STTS_INM(m)                                \
        in_dword_masked(HWIO_IPA_UC_SWEV_INT_STTS_ADDR, m)
#define HWIO_IPA_UC_SWEV_INT_STTS_IRQ_STATUS_BMSK                       0xffffffff
#define HWIO_IPA_UC_SWEV_INT_STTS_IRQ_STATUS_SHFT                                0

#define HWIO_IPA_UC_SWEV_INT_EN_ADDR                                    (IPA_UC_REG_BASE      + 0x00008414)
#define HWIO_IPA_UC_SWEV_INT_EN_PHYS                                    (IPA_UC_REG_BASE_PHYS + 0x00008414)
#define HWIO_IPA_UC_SWEV_INT_EN_RMSK                                    0xffffffff
#define HWIO_IPA_UC_SWEV_INT_EN_SHFT                                             0
#define HWIO_IPA_UC_SWEV_INT_EN_IN                                      \
        in_dword_masked(HWIO_IPA_UC_SWEV_INT_EN_ADDR, HWIO_IPA_UC_SWEV_INT_EN_RMSK)
#define HWIO_IPA_UC_SWEV_INT_EN_INM(m)                                  \
        in_dword_masked(HWIO_IPA_UC_SWEV_INT_EN_ADDR, m)
#define HWIO_IPA_UC_SWEV_INT_EN_OUT(v)                                  \
        out_dword(HWIO_IPA_UC_SWEV_INT_EN_ADDR,v)
#define HWIO_IPA_UC_SWEV_INT_EN_OUTM(m,v)                               \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_SWEV_INT_EN_ADDR,m,v,HWIO_IPA_UC_SWEV_INT_EN_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_SWEV_INT_EN_IRQ_EN_BMSK                             0xffffffff
#define HWIO_IPA_UC_SWEV_INT_EN_IRQ_EN_SHFT                                      0

#define HWIO_IPA_UC_SWEV_INT_CLR_ADDR                                   (IPA_UC_REG_BASE      + 0x00008418)
#define HWIO_IPA_UC_SWEV_INT_CLR_PHYS                                   (IPA_UC_REG_BASE_PHYS + 0x00008418)
#define HWIO_IPA_UC_SWEV_INT_CLR_RMSK                                   0xffffffff
#define HWIO_IPA_UC_SWEV_INT_CLR_SHFT                                            0
#define HWIO_IPA_UC_SWEV_INT_CLR_OUT(v)                                 \
        out_dword(HWIO_IPA_UC_SWEV_INT_CLR_ADDR,v)
#define HWIO_IPA_UC_SWEV_INT_CLR_IRQ_CLR_BMSK                           0xffffffff
#define HWIO_IPA_UC_SWEV_INT_CLR_IRQ_CLR_SHFT                                    0

#define HWIO_IPA_UC_TIMER_CTRL_n_ADDR(n)                                (IPA_UC_REG_BASE      + 0x00008500 + 0x10 * (n))
#define HWIO_IPA_UC_TIMER_CTRL_n_PHYS(n)                                (IPA_UC_REG_BASE_PHYS + 0x00008500 + 0x10 * (n))
#define HWIO_IPA_UC_TIMER_CTRL_n_RMSK                                    0x17fffff
#define HWIO_IPA_UC_TIMER_CTRL_n_SHFT                                            0
#define HWIO_IPA_UC_TIMER_CTRL_n_MAXn                                          0x3
#define HWIO_IPA_UC_TIMER_CTRL_n_INI(n) \
        in_dword(HWIO_IPA_UC_TIMER_CTRL_n_ADDR(n))
#define HWIO_IPA_UC_TIMER_CTRL_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_UC_TIMER_CTRL_n_ADDR(n), mask)
#define HWIO_IPA_UC_TIMER_CTRL_n_OUTI(n,val) \
        out_dword(HWIO_IPA_UC_TIMER_CTRL_n_ADDR(n),val)
#define HWIO_IPA_UC_TIMER_CTRL_n_OUTMI(n,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_TIMER_CTRL_n_ADDR(n),mask,val,HWIO_IPA_UC_TIMER_CTRL_n_INI(n));\
        HWIO_INTFREE()
#define HWIO_IPA_UC_TIMER_CTRL_n_RETRIG_BMSK                             0x1000000
#define HWIO_IPA_UC_TIMER_CTRL_n_RETRIG_SHFT                                  0x18
#define HWIO_IPA_UC_TIMER_CTRL_n_EVENT_SEL_BMSK                           0x7f0000
#define HWIO_IPA_UC_TIMER_CTRL_n_EVENT_SEL_SHFT                               0x10
#define HWIO_IPA_UC_TIMER_CTRL_n_COUNT_BMSK                                 0xffff
#define HWIO_IPA_UC_TIMER_CTRL_n_COUNT_SHFT                                      0

#define HWIO_IPA_UC_TIMER_CTRL_R_n_ADDR(n)                              (IPA_UC_REG_BASE      + 0x00008504 + 0x10 * (n))
#define HWIO_IPA_UC_TIMER_CTRL_R_n_PHYS(n)                              (IPA_UC_REG_BASE_PHYS + 0x00008504 + 0x10 * (n))
#define HWIO_IPA_UC_TIMER_CTRL_R_n_RMSK                                 0x317fffff
#define HWIO_IPA_UC_TIMER_CTRL_R_n_SHFT                                          0
#define HWIO_IPA_UC_TIMER_CTRL_R_n_MAXn                                        0x3
#define HWIO_IPA_UC_TIMER_CTRL_R_n_INI(n) \
        in_dword(HWIO_IPA_UC_TIMER_CTRL_R_n_ADDR(n))
#define HWIO_IPA_UC_TIMER_CTRL_R_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_UC_TIMER_CTRL_R_n_ADDR(n), mask)
#define HWIO_IPA_UC_TIMER_CTRL_R_n_OUTI(n,val) \
        out_dword(HWIO_IPA_UC_TIMER_CTRL_R_n_ADDR(n),val)
#define HWIO_IPA_UC_TIMER_CTRL_R_n_EVENT_UNSTABLE_BMSK                  0x20000000
#define HWIO_IPA_UC_TIMER_CTRL_R_n_EVENT_UNSTABLE_SHFT                        0x1d
#define HWIO_IPA_UC_TIMER_CTRL_R_n_COUNT_UNSTABLE_BMSK                  0x10000000
#define HWIO_IPA_UC_TIMER_CTRL_R_n_COUNT_UNSTABLE_SHFT                        0x1c
#define HWIO_IPA_UC_TIMER_CTRL_R_n_RETRIG_BMSK                           0x1000000
#define HWIO_IPA_UC_TIMER_CTRL_R_n_RETRIG_SHFT                                0x18
#define HWIO_IPA_UC_TIMER_CTRL_R_n_EVENT_SEL_BMSK                         0x7f0000
#define HWIO_IPA_UC_TIMER_CTRL_R_n_EVENT_SEL_SHFT                             0x10
#define HWIO_IPA_UC_TIMER_CTRL_R_n_COUNT_BMSK                               0xffff
#define HWIO_IPA_UC_TIMER_CTRL_R_n_COUNT_SHFT                                    0

#define HWIO_IPA_UC_TIMER_STATUS_n_ADDR(n)                              (IPA_UC_REG_BASE      + 0x00008508 + 0x10 * (n))
#define HWIO_IPA_UC_TIMER_STATUS_n_PHYS(n)                              (IPA_UC_REG_BASE_PHYS + 0x00008508 + 0x10 * (n))
#define HWIO_IPA_UC_TIMER_STATUS_n_RMSK                                  0x100ffff
#define HWIO_IPA_UC_TIMER_STATUS_n_SHFT                                          0
#define HWIO_IPA_UC_TIMER_STATUS_n_MAXn                                        0x3
#define HWIO_IPA_UC_TIMER_STATUS_n_INI(n) \
        in_dword(HWIO_IPA_UC_TIMER_STATUS_n_ADDR(n))
#define HWIO_IPA_UC_TIMER_STATUS_n_INMI(n,mask) \
        in_dword_masked(HWIO_IPA_UC_TIMER_STATUS_n_ADDR(n), mask)
#define HWIO_IPA_UC_TIMER_STATUS_n_OUTI(n,val) \
        out_dword(HWIO_IPA_UC_TIMER_STATUS_n_ADDR(n),val)
#define HWIO_IPA_UC_TIMER_STATUS_n_ACTIVE_BMSK                           0x1000000
#define HWIO_IPA_UC_TIMER_STATUS_n_ACTIVE_SHFT                                0x18
#define HWIO_IPA_UC_TIMER_STATUS_n_COUNT_BMSK                               0xffff
#define HWIO_IPA_UC_TIMER_STATUS_n_COUNT_SHFT                                    0

#define HWIO_IPA_UC_SPARE_ADDR                                          (IPA_UC_REG_BASE      + 0x00009ffc)
#define HWIO_IPA_UC_SPARE_PHYS                                          (IPA_UC_REG_BASE_PHYS + 0x00009ffc)
#define HWIO_IPA_UC_SPARE_RMSK                                          0xffffffff
#define HWIO_IPA_UC_SPARE_SHFT                                                   0
#define HWIO_IPA_UC_SPARE_IN                                            \
        in_dword_masked(HWIO_IPA_UC_SPARE_ADDR, HWIO_IPA_UC_SPARE_RMSK)
#define HWIO_IPA_UC_SPARE_INM(m)                                        \
        in_dword_masked(HWIO_IPA_UC_SPARE_ADDR, m)
#define HWIO_IPA_UC_SPARE_OUT(v)                                        \
        out_dword(HWIO_IPA_UC_SPARE_ADDR,v)
#define HWIO_IPA_UC_SPARE_OUTM(m,v)                                     \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_SPARE_ADDR,m,v,HWIO_IPA_UC_SPARE_IN); \
        HWIO_INTFREE()
#define HWIO_IPA_UC_SPARE_SPARE_BMSK                                    0xffffffff
#define HWIO_IPA_UC_SPARE_SPARE_SHFT                                             0

#define HWIO_IPA_UC_MAILBOX_m_n_ADDR(n,m)                               (IPA_UC_REG_BASE      + 0x0000a000 + 0x80 * (m)+0x4 * (n))
#define HWIO_IPA_UC_MAILBOX_m_n_PHYS(n,m)                               (IPA_UC_REG_BASE_PHYS + 0x0000a000 + 0x80 * (m)+0x4 * (n))
#define HWIO_IPA_UC_MAILBOX_m_n_RMSK                                    0xffffffff
#define HWIO_IPA_UC_MAILBOX_m_n_SHFT                                             0
#define HWIO_IPA_UC_MAILBOX_m_n_MAXn                                          0x1f
#define HWIO_IPA_UC_MAILBOX_m_n_MAXm                                           0x1
#define HWIO_IPA_UC_MAILBOX_m_n_INI2(n,m) \
        in_dword(HWIO_IPA_UC_MAILBOX_m_n_ADDR(n,m))
#define HWIO_IPA_UC_MAILBOX_m_n_INMI2(n,m,mask) \
        in_dword_masked(HWIO_IPA_UC_MAILBOX_m_n_ADDR(n,m), mask)
#define HWIO_IPA_UC_MAILBOX_m_n_OUTI2(n,m,val) \
        out_dword(HWIO_IPA_UC_MAILBOX_m_n_ADDR(n,m),val)
#define HWIO_IPA_UC_MAILBOX_m_n_OUTMI2(n,m,mask,val) \
        HWIO_INTLOCK(); \
        out_dword_masked_ns(HWIO_IPA_UC_MAILBOX_m_n_ADDR(n,m),mask,val,HWIO_IPA_UC_MAILBOX_m_n_INI2(n,m));\
        HWIO_INTFREE()
#define HWIO_IPA_UC_MAILBOX_m_n_DATA_BMSK                               0xffffffff
#define HWIO_IPA_UC_MAILBOX_m_n_DATA_SHFT                                        0



#endif /* __MSMHWIOREG_H__ */

