#ifndef __MSMHWIOBASE_H__
#define __MSMHWIOBASE_H__
/*
===========================================================================
*/
/**
  @file msmhwiobase.h
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/driver/ipa_uc/9x45/inc/IPA_2_5/msmhwiobase.h#1 $
  $Author: mplcsds1 $

  ===========================================================================
*/

/*------------------------------------------------------------------------------
 * MODULE: ipa_wrapper
 *------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 * In Tesla, IPA was allocated 0x07900000 address range. However for CM3, 
 * the address accesses from 0x00000000-0x20000000 will be placed on the I/D bus 
 * and will not goto the S-bus. So if we try to access a IPA register from uC and 
 * place the transaction as 0x079XXXXX, it'll end up going to the I/D bus instead 
 * of S-bus. To workaround this issue, the IPA address space is mapped to 
 * 0x279XXXXX so the transactions get placed on the S-bus. There is hw transalation 
 * logic that will retranslate them back to 0x079XXXXX addresses before it goes to 
 * the ipa_uc_interconnect.
 *------------------------------------------------------------------------------*/
#define IPA_WRAPPER_MSK                                                                          0x20000000

#define IPA_ACTUAL_WRAPPER_BASE                                                                  0x07900000
#define IPA_ACTUAL_REG_BASE                                                                      (IPA_ACTUAL_WRAPPER_BASE + 0x00040000)

#define IPA_WRAPPER_BASE                                                                         (IPA_WRAPPER_MSK + IPA_ACTUAL_WRAPPER_BASE)
#define IPA_WRAPPER_BASE_SIZE                                                                    0x00000000
#define IPA_WRAPPER_BASE_PHYS                                                                    0x27900000

#endif /* __MSMHWIOBASE_H__ */
