/*==============================================================================

                            ds_http_uri.cpp

GENERAL DESCRIPTION
  Internal iface info structure and functions for DS HTTP

  Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
==============================================================================*/

/*==============================================================================
                           EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when        who    what, where, why
--------    ---    ----------------------------------------------------------
07/28/15    ml     Created file/Initial version.
==============================================================================*/
#include "ds_http_uri.h"

#include <stringl/stringl.h>



ds_http_ip_addr_info::ds_http_ip_addr_info()
: ip_addr_type(DS_HTTP_IPV4), ip_addr(0)
{ }

ds_http_ip_addr_info::ds_http_ip_addr_info(const ds_http_ip_addr_info& other)
: ip_addr_type(other.ip_addr_type), ip_addr(other.ip_addr)
{
  memscpy(ip_addr6, DS_HTTP_IPV6_SIZE, other.ip_addr6, DS_HTTP_IPV6_SIZE);
}


ds_http_ip_addr_info& ds_http_ip_addr_info::operator=(const ds_http_ip_addr_info& other)
{
  ip_addr_type = other.ip_addr_type;
  ip_addr      = other.ip_addr;
  memscpy(ip_addr6, DS_HTTP_IPV6_SIZE, other.ip_addr6, DS_HTTP_IPV6_SIZE);

  return *this;
}

bool ds_http_ip_addr_info::operator==(const ds_http_ip_addr_info& rhs) const
{
  if(ip_addr_type != rhs.ip_addr_type)
    return false;

  switch(ip_addr_type)
  {
    case DS_HTTP_IPV4:
      return (ip_addr == rhs.ip_addr);

    case DS_HTTP_IPV6:
      return 0 == memcmp(ip_addr6, rhs.ip_addr6, DS_HTTP_IPV6_SIZE);

    default:
      return false;
  }
}

void ds_http_ip_addr_info::clear()
{
  ip_addr = 0;
  memset(ip_addr6, 0, DS_HTTP_IPV6_SIZE);
}

