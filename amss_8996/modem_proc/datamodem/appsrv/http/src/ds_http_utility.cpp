/*==============================================================================

                              ds_http_utility.h

GENERAL DESCRIPTION
  Utility functions for ds_http

  Copyright (c) 2014 by Qualcomm Technologies Incorporated. All Rights Reserved.
==============================================================================*/

/*==============================================================================
                           EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when        who    what, where, why
--------    ---    ----------------------------------------------------------
07/21/14    ml     Created file/Initial version.
==============================================================================*/
#include "ds_http_utility.h"
#include <stringl/stringl.h>
#include <ctype.h> // isdigit()

static const char hex_str[]      = "0123456789abcdef";
static const char digit[]        = "0123456789";




bool is_numeric_host(const char* hostname)
{
  while('\0' != *hostname)
  {
    if(0 == isdigit(*hostname) && '.' != *hostname)
      return false;
    hostname++;
  }
  return true;
}



/* Returns true if the string contains \r\n */
bool has_carriage_return(const char* str, uint32 len)
{
  return (NULL != strstr(str, "\r\n"));
}



void hex_to_hex_str(unsigned char hex[16], char str[32])
{
  unsigned int a = 0, b = 0;

  for(int i=0; i < 16; ++i)
  {
    a = hex[i]>>4;
    a = a & 0x0f;
    b = hex[i] & 0x0f;

    str[i*2] = hex_str[a];
    str[i*2+1] = hex_str[b];
  }
}



void char_to_hex_str(char chr, char str[2])
{
  unsigned int a = 0, b = 0;

  a = chr>>4;
  a = a & 0x0f;
  b = chr & 0x0f;

  str[0] = hex_str[a];
  str[1] = hex_str[b];
}



char* uitoa(uint32 value, char str[11]){
  uint32 offset = 0;
  uint32 mod    = 0;

  if(0 == value)
  {
    str[0] = digit[0];
    return str;
  }

  // find offset
  mod = value/10;
  while(0 != mod)
  {
    offset++;
    mod /= 10;
  }

  // assign char values to string
  while(0 != value)
  {
    mod = value%10;
    str[offset] = digit[mod];
    value /= 10;
    --offset;
  }
  return str;
}


