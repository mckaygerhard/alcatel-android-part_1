/*==============================================================================

                            ds_http_uri.h

GENERAL DESCRIPTION
  URI info manager
    - DNS resolution result cache
    - Permanent redirect cache
    - Permanent redirect file cache management
    - URI utilities (e.g. parsing)

  Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
==============================================================================*/

/*==============================================================================
                           EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when        who    what, where, why
--------    ---    ----------------------------------------------------------
07/28/15    ml     Created file/Initial version.
==============================================================================*/
#ifndef DS_HTTP_URI_H
#define DS_HTTP_URI_H

#include "ds_http_types.h"

const uint32 DS_HTTP_IPV6_SIZE = sizeof(uint32) * 4;

enum ds_http_ip_version
{
  DS_HTTP_IPV4 = 0,
  DS_HTTP_IPV6
};


struct ds_http_ip_addr_info
{
  ds_http_ip_version ip_addr_type;
  uint32             ip_addr;
  uint32             ip_addr6[4];

  ds_http_ip_addr_info();
  ds_http_ip_addr_info(const ds_http_ip_addr_info& other);
  ds_http_ip_addr_info& operator=(const ds_http_ip_addr_info& other);
  bool operator==(const ds_http_ip_addr_info& rhs) const;

  void clear();
};


#endif /* DS_HTTP_URI_H */
