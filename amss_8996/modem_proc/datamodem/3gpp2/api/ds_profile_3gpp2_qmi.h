/******************************************************************************
  @file    ds_profile_3gpp2_qmi.h
  @brief   

  DESCRIPTION
  Tech specific 3GPP2 Profile Management through QMI, header file

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (C) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved. 
  QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/3gpp2/api/ds_profile_3gpp2_qmi.h#1 $ $DateTime: 2016/03/28 23:02:50 $ $Author: mplcsds1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/04/11   gs      First version of the header file.
===========================================================================*/
#ifndef DS_PROFILE_3GPP2_QMI_H
#define DS_PROFILE_3GPP2_QMI_H

#include "comdef.h"


#endif /* DS_PROFILE_3GPP2_QMI_H */
