/******************************************************************************
  @file    ds_profile_qmi.h
  @brief   DS PROFILE - QMI related code, which is not technology specific

  DESCRIPTION
      DS PROFILE - QMI related code, which is not technology specific

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
  ---------------------------------------------------------------------------
******************************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/interface/dsprofile/inc/ds_profile_qmi.h#1 $ $DateTime: 2016/03/28 23:02:50 $ $Author: mplcsds1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
02/14/12   gs      First version 
===========================================================================*/
#ifndef DS_PROFILE_QMI_QNX_H
#define DS_PROFILE_QMI_QNX_H


#endif /* DS_PROFILE_QMI_QNX_H_ */

