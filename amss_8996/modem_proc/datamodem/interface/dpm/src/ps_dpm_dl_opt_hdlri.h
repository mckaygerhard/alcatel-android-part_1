#ifndef _PS_DL_OPT_HDLRI_H
#define _PS_DL_OPT_HDLRI_H
/*===========================================================================

                     PS_OPTIMIZED_HDLR_H . H
DESCRIPTION


EXTERNALIZED FUNCTIONS


Copyright (c) 2004-2011 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE
  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/interface/dpm/src/ps_dpm_dl_opt_hdlri.h#1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------

===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#endif /* _PS_DL_OPT_HDLRI_H */

