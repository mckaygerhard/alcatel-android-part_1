#ifndef __DSS_PRIMARYQOSMODIFYHANDLER_H__
#define __DSS_PRIMARYQOSMODIFYHANDLER_H__

/*====================================================

FILE:  DSS_PrimaryQoSModifyHandler.h

SERVICES:
   Handle network outage events.

=====================================================

Copyright (c) 2008 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary

=====================================================*/
/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/interface/dss/src/DSS_PrimaryQoSModifyHandler.h#1 $
  $DateTime: 2016/03/28 23:02:50 $$Author: mplcsds1 $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2010-04-18 en  History added.

===========================================================================*/

#include "DSS_EventHandlerNetApp.h"

class DSSPrimaryQoSModifyHandler : public DSSEventHandlerNetApp
{
protected:
   virtual void EventOccurred();
   virtual AEEResult RegisterIDL();

public:
   static DSSPrimaryQoSModifyHandler* CreateInstance();
   DSSPrimaryQoSModifyHandler();
};

#endif // __DSS_PRIMARYQOSMODIFYHANDLER_H__
