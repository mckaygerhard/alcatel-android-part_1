#ifndef __DSS_NETWORK_EXT_IPV6_DEL_ADDR_IPHANDLER_H__
#define __DSS_NETWORK_EXT_IPV6_DEL_ADDR_IPHANDLER_H__

/*====================================================

FILE:  DSS_NetworkExtIPv6DelAddrHandler.h.h

SERVICES:
   Handle network external IPv6 address deleted event.

=====================================================

Copyright (c) 2012 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary

=====================================================*/
/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/interface/dss/src/DSS_NetworkExtIPv6DelAddrHandler.h#1 $
  $DateTime: 2016/03/28 23:02:50 $$Author: mplcsds1 $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2012-08-07 vm  Created.

===========================================================================*/

#include "DSS_EventHandlerNetApp.h"

class DSSNetworkExtIPv6DelAddrHandler : public DSSEventHandlerNetApp
{
protected:
   virtual void EventOccurred();
   virtual AEEResult RegisterIDL();

public:
   static DSSNetworkExtIPv6DelAddrHandler* CreateInstance();
   DSSNetworkExtIPv6DelAddrHandler();
   virtual void Destructor() throw();
};

#endif // __DSS_NETWORK_EXT_IPV6_DEL_ADDR_IPHANDLER_H__
