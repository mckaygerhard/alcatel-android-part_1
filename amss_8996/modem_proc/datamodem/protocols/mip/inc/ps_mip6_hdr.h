#ifndef PS_MIP6_HDR_H
#define PS_MIP6_HDR_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                          P S _ M I P 6 _ H D R . H 


GENERAL DESCRIPTION
  This module implements Mobile IPv6 Mobility Headers (MH)

EXTERNALIZED FUNCTIONS

  ps_mip6_hdr_create()
    This function adds the MIPv6 mobility header to the outgoing packet
  
  ps_mip6_hdr_parse()
    This function parses the MIPv6 mobility header in the incoming packet

  ps_mip6_option_create()
    This function adds a MIPv6 mobility option to the outgoing packet.
  
  ps_mip6_option_parse()
    This function parses a MIPv6 mobility option in the incoming packet

INITIALIZATION AND SEQUENCING REQUIREMENTS
  
    Copyright (c)2006-2009 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/protocols/mip/inc/ps_mip6_hdr.h#1 $
  $Author: mplcsds1 $ 
  $DateTime: 2016/03/28 23:02:50 $
===========================================================================*/
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

                                INCLUDE FILES

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
#include "datamodem_variation.h"
#include "customer.h"
#include "comdef.h"
#endif /* PS_MIP6_HDR_H */
