#ifndef PS_MIP6_MSM_H
#define PS_MIP6_MSM_H
/*===========================================================================

                           P S _ M I P 6 _ M S M . H
                   
DESCRIPTION
  The header file for MIP6 Meta State Machine.

EXTERNALIZED FUNCTIONS

Copyright (c) 2007-2009 Qualcomm Technologies Incorporated. 
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/protocols/mip/inc/ps_mip6_msm.h#1 $
  $Author: mplcsds1 $ $DateTime: 2016/03/28 23:02:50 $
===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#if defined (FEATURE_DATA_PS) && defined (FEATURE_DATA_PS_IPV6)
#endif /* FEATURE_DATA_PS && FEATURE_DATA_PS_IPV6*/
#endif /* PS_MIP6_MSM_H */
