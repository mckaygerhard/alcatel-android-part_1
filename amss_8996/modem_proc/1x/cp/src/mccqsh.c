/*===========================================================================

         C D M A   Q S H    F R A M E W O R K    S U P P O R T

DESCRIPTION
  This contains all the 1X_CP internal data and API definitions required for 
  supporting QSH (Qualcomm Sherlock Holmes) framework.

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc. 
  All Rights Reserved Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies, Inc. and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.


===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

$PVCSPath: L:/src/asw/MSM5100/CP_REL_A/vcs/onex_voice_adapt_if.h   1.8   10 Mar 2015 13:34:04   azafer  $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/1x/cp/src/mccqsh.c#1 $ $DateTime: 2016/03/28 23:03:55 $ $Author: mplcsds1 $


when       who     what, where, why
--------   ---     ------------------------------------------------------------
03/07/16   agh     Log only valid values of TX power in traffic state
11/03/15   agh     Fixed units and default values of some metrics
10/28/15   agh     Fixed compilation errors in No-TDSCDMA-or-L1 strip builds
10/26/15   agh     Initial Revision

  ============================================================================*/

/*----------------------------------------------------------------------------
  Include files for Module
----------------------------------------------------------------------------*/
#include "mccqsh_i.h"
#include "txccommon_i.h"
#include "srch_v.h"


/* Macros and Data definitions */

#define MC_QSH_METRIC_CELL_INFO_COUNT 10
#define MC_QSH_METRIC_RF_PARAMS_COUNT 10

/* Periodicity of metrics in ms */
#define MC_QSH_METRIC_PERIODICITY 200

#ifdef FEATURE_1XCP_QSH_SUPPORT
qsh_ext_metric_cfg_s  onexl3_qsh_metric_default_cfg_arr[ONEXL3_QSH_METRIC_MAX];
#endif /* FEATURE_1XCP_QSH_SUPPORT */

/* Variable to store last known valid value of TX power in traffic state */
static int16 valid_tx_power = -32768;

/*===========================================================================

                      FUNCTION DEFINITIONS

===========================================================================*/

#ifdef FEATURE_1XCP_QSH_SUPPORT
/*===========================================================================
  
  FUNCTION MC_QSH_INIT
  
  DESCRIPTION
    This function registers 1X_CP client with the QSH module
  
  DEPENDENCIES
    None.
  
  RETURN VALUE
    None
  
  SIDE EFFECTS
    None.
  
===========================================================================*/

void mc_qsh_init
( 
  void 
)
{
  qsh_client_reg_s mc_client_reg;  
  int i;

  /* Initialize the local global array to keep track of registered metrics 
   * and events */
  memset(&mc_qsh_data, 0, sizeof(mc_qsh_data_type));
  for (i = 0; i<ONEXL3_QSH_METRIC_MAX; i++)
  {
    mc_qsh_data.metric_cfg[i].id = QSH_METRIC_ID_NONE;
    mc_qsh_data.metric_cfg[i].action = QSH_METRIC_ACTION_MAX;
  }
  
  /* Initialize 1XCP client */
  qsh_client_reg_init(&mc_client_reg);
  
  /* Provide 1X_CP client information to QSH */
  mc_client_reg.client = QSH_CLT_ONEXL3;
  mc_client_reg.client_cb_ptr = mc_qsh_cb;
  mc_client_reg.major_ver = ONEXL3_QSH_MAJOR_VER;
  mc_client_reg.minor_ver = ONEXL3_QSH_MINOR_VER;
  mc_client_reg.cb_action_support_mask = QSH_ACTION_METRIC_CFG | 
                                         QSH_ACTION_METRIC_TIMER_EXPIRY;

#ifdef FEATURE_QSH_EVENT_METRIC
  qsh_client_metric_cfg_init(onexl3_qsh_metric_default_cfg_arr,
                          ONEXL3_QSH_METRIC_MAX);

 /* Initialize the metrics array with parameters corresponding to each metric 
  * that needs to be collected */
  mc_init_qsh_metric_cfg_array();  
  
  mc_client_reg.metric_info.metric_cfg_count = 
                    ONEXL3_QSH_METRIC_MAX;
  mc_client_reg.metric_info.metric_cfg_arr_ptr = 
                             &onexl3_qsh_metric_default_cfg_arr[0];
#endif /* FEATURE_QSH_EVENT_METRIC */

  qsh_client_reg(&mc_client_reg);
  
  M1X_MSG( DCP, LEGACY_HIGH, 
      "QSH: registered 1X-CP with QSH module" );

} /* mc_qsh_init */

/*===========================================================================

FUNCTION MC_QSH_CB

DESCRIPTION
  Callback function invoked by QSH module for 1X to process QSH requests.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void mc_qsh_cb
(
  qsh_client_cb_params_s *cb_params_ptr
)
{

  if( cb_params_ptr->action == QSH_ACTION_METRIC_CFG ||
      cb_params_ptr->action == QSH_ACTION_METRIC_TIMER_EXPIRY )
  {
    if(cb_params_ptr->action_mode == QSH_ACTION_MODE_SYNC_REQUIRED)
    {
      /* Called in QSH task context. Should have mutext protection here but
       * it's not feasible to add it for the data structures being 
       * accessed from within this API. SYNC mode should be rare as should
       * write accesses to CDMA structure within this API. So data corruption
       * chances are extremely rare. */
      mc_process_qsh_cmd(cb_params_ptr);
    }
    else
    { 
      /* Queue the cmd on MC task */
      mc_cdma_qsh_command(cb_params_ptr);
    }
  }
}/* mc_qsh_cb */

/*===========================================================================

FUNCTION MC_PROCESS_QSH_CMD

DESCRIPTION
  This API processes the cmds received from QSH module. This could either be 
  invoked in QSH context or in MC task context.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void mc_process_qsh_cmd
(
  qsh_client_cb_params_s* qsh_cmd_ptr
)
{
  uint16 metric_id = ONEXL3_QSH_METRIC_MAX;
  qsh_client_metric_cfg_s *metric_cfg;
  qsh_client_action_done_s cb_done_ptr;
  int idx;

  switch(qsh_cmd_ptr->action)
  { 

#ifdef FEATURE_QSH_EVENT_METRIC  
    case QSH_ACTION_METRIC_CFG:
    {
      metric_id = qsh_cmd_ptr->action_params.metric_cfg.id;

      M1X_MSG( DCP, LEGACY_HIGH, 
          "QSH: ACTION_METRIC_CFG metric_id: %d, action: %d", metric_id,
          qsh_cmd_ptr->action_params.metric_cfg.action);
      
      if(metric_id < ONEXL3_QSH_METRIC_MAX)
      {
        metric_cfg = &qsh_cmd_ptr->action_params.metric_cfg;
        
        if(metric_cfg->action == QSH_METRIC_ACTION_START)
        {
          if(metric_cfg->start_addr == NULL)
          {
            M1X_MSG( DCP, LEGACY_ERROR,
                "QSH: Start address passed as NULL!" );
          }
          mc_qsh_data.metric_cfg[metric_id] = *metric_cfg;
          
          /* Handle the specific metric here */
          switch(metric_id)
          {   
            case ONEXL3_QSH_METRIC_CELL_INFO:
            {
              mc_send_cell_info_metrics();
              break;
            }
            case ONEXL3_QSH_METRIC_RF_PARAMS:
            {
              mc_send_rf_params_metrics(QSH_CLIENT_METRIC_LOG_REASON_EVENT_INTERNAL);
              break;
            }
            default:
              break;
          }          
        }
        else if(metric_cfg->action == QSH_METRIC_ACTION_STOP)
        {
          mc_qsh_data.metric_cfg[metric_id].start_addr = NULL ;
          mc_qsh_data.metric_cfg[metric_id].action = QSH_METRIC_ACTION_STOP;
        }    
      }
      break;
    }

    case QSH_ACTION_METRIC_TIMER_EXPIRY:
    {                           
      for( idx = 0; idx < qsh_cmd_ptr->action_params.metric_timer_expiry.metric_id_count &&
                  idx < ONEXL3_QSH_METRIC_MAX ; idx++ )
      {
        metric_id = qsh_cmd_ptr->action_params.metric_timer_expiry.params[idx].metric_id;        
        switch(metric_id)
        {
          case ONEXL3_QSH_METRIC_RF_PARAMS:
          {
            mc_send_rf_params_metrics(QSH_CLIENT_METRIC_LOG_REASON_TIMER_EXPIRY);         
            break;
          }
            
          default:
            /*Everything else is event based */
            break;
        }
      }
      break;
    }      
#endif /* FEATURE_QSH_EVENT_METRIC */

    default:
      /* Other actions are not supported currently */ 
      M1X_MSG( DCP, LEGACY_ERROR, 
                "QSH: Unsupported action received: %d",
                qsh_cmd_ptr->action );
      break;
  }

  /* For Metric Timer expiry and Analysis actions, qsh_client_action_done 
   * doesn't need to be called. For other actions it must be called */
  if( qsh_cmd_ptr->action != QSH_ACTION_METRIC_TIMER_EXPIRY &&
      qsh_cmd_ptr->action != QSH_ACTION_ANALYSIS )
  {
    /*  QSH should be intimated that our metric configuration is updated */
    qsh_client_action_done_init(&cb_done_ptr);
    cb_done_ptr.cb_params_ptr = qsh_cmd_ptr;
    if(qsh_cmd_ptr->action_mode == QSH_ACTION_MODE_SYNC_REQUIRED)
    {
      cb_done_ptr.action_mode_done = QSH_ACTION_MODE_DONE_SYNC;
    }
    else
    {
      cb_done_ptr.action_mode_done = QSH_ACTION_MODE_DONE_ASYNC;
    }
    qsh_client_action_done(&cb_done_ptr);
  }
}

/*===========================================================================

FUNCTION MC_SEND_CELL_INFO_METRICS

DESCRIPTION
  This API would be used by 1X-CP to send cell info metrics to QSH module. 
  This metric is reported whenever there is a 1X cell change after SPM 
  is available on the new cell.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void mc_send_cell_info_metrics
(
  void
)
{
  onexl3_qsh_metric_cell_info_s *metrics_ptr;
  
  qsh_client_metric_cfg_s *metric_cfg = &(mc_qsh_data.metric_cfg[ONEXL3_QSH_METRIC_CELL_INFO]);
    
  if(mc_qsh_is_metric_enabled(ONEXL3_QSH_METRIC_CELL_INFO) == FALSE)
  {
    M1X_MSG( DCP, LEGACY_MED, 
             "Cell info metrics disabled, skip sending to QSH");
    return;
  }
  
  metrics_ptr = (onexl3_qsh_metric_cell_info_s *)metric_cfg->start_addr;

  M1X_MSG( DCP, LEGACY_MED, 
           "QSH: Sending Cell info metrics, start addr: 0x%x, size %d, req size %d", 
           metrics_ptr, metric_cfg->size_bytes, 
           sizeof(onexl3_qsh_metric_cell_info_s));
    
  if ( metrics_ptr != NULL &&
       metric_cfg->size_bytes >= sizeof(onexl3_qsh_metric_cell_info_s))
  {

    if (mcc_update_cell_info_metrics(metrics_ptr))
    {
      M1X_MSG( DCP, LEGACY_HIGH, 
          "QSH: Sent Cell info to QSH, pilot %d, reg_zone %d", 
          metrics_ptr->pilot_pn, metrics_ptr->reg_zone);    
      
      mc_qsh_commit_data( ONEXL3_QSH_METRIC_CELL_INFO, 
                  QSH_CLIENT_METRIC_LOG_REASON_EVENT_INTERNAL, metric_cfg );  
    }
  }
} /* mc_send_cell_info_metrics */

/*===========================================================================

FUNCTION MCC_UPDATE_CELL_INFO_METRICS

DESCRIPTION
  This API updates the cell_info metrics structure with the current BS info 
  to be reported to QSH module on BS change.

DEPENDENCIES
  cur_bs_ptr should not be Null and SPM should have been received, otherwise
  garbage values may be populated in the structure.

RETURN VALUE
  True if metrics are available, False otherwise.

SIDE EFFECTS
  None.

===========================================================================*/

boolean mcc_update_cell_info_metrics
(
  onexl3_qsh_metric_cell_info_s *metrics_ptr
)
{
  switch(MAIN_STATE( cdma.curr_state ))
  {    

    #ifdef FEATURE_MODEM_1X_IRAT_LTO1X
    /* Id we are in ENTER/EXIT state while this API is called then this API is 
     * called while processing sib8 message and use cell info from sib8 */
    case CDMA_CSFB:
    case CDMA_ENTER:
    case CDMA_EXIT:
    {
      return mcc_csfb_update_cell_info_metrics(metrics_ptr);
    } 
    #endif /* FEATURE_MODEM_1X_IRAT_LTO1X */
    
    default:
    {
      if (cur_bs_ptr != NULL)
      {
        metrics_ptr->band_class = cdma.band_class;
        metrics_ptr->cfg_msg_seq = cur_bs_ptr->rx.config_msg_seq;
        metrics_ptr->freq = cdma.cdmach;
        metrics_ptr->pilot_pn = cur_bs_ptr->pilot_pn;
        metrics_ptr->base_id = cur_bs_ptr->csp.sp.base_id;
        metrics_ptr->reg_zone = cur_bs_ptr->csp.sp.reg_zone;
        metrics_ptr->sid = cur_bs_ptr->csp.sp.sid;
        metrics_ptr->nid = cur_bs_ptr->csp.sp.nid;
        return TRUE;
      }      
      break;
    }
  }

  return FALSE;  
  
} /* mcc_update_cell_info_metrics */

/*===========================================================================

FUNCTION MC_SEND_RF_PARAMS_METRICS

DESCRIPTION
  This API would be used by 1X-CP to send RF params metrics to QSH module. 
  This metric is reported periodically and the required parameters are fetched
  by 1X-CP from 1X-L1 (Searcher) module.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void mc_send_rf_params_metrics
(
  qsh_client_metric_log_reason_e reason
)
{
  onexl3_qsh_metric_rf_params_s *metrics_ptr;
  
  qsh_client_metric_cfg_s *metric_cfg = &(mc_qsh_data.metric_cfg[ONEXL3_QSH_METRIC_RF_PARAMS]);
  
  if(mc_qsh_is_metric_enabled(ONEXL3_QSH_METRIC_RF_PARAMS) == FALSE)
  {
    return;
  }
  
  metrics_ptr = (onexl3_qsh_metric_rf_params_s *)metric_cfg->start_addr;
  
  if ( metrics_ptr != NULL &&
       metric_cfg->size_bytes >= sizeof(onexl3_qsh_metric_rf_params_s))
  {
    if(mcc_update_rf_params_metrics(metrics_ptr))
    {          
      mc_qsh_commit_data( ONEXL3_QSH_METRIC_RF_PARAMS, 
                          reason, metric_cfg );
    }
  }
} /* mc_send_rf_params_metrics */

/*===========================================================================

FUNCTION MCC_UPDATE_RF_PARAMS_METRICS

DESCRIPTION
  This API updates the RF params metrics structure with the RF params 
  of the current BS to be reported to QSH module periodically.

DEPENDENCIES
  None.

RETURN VALUE
  True if metrics are available, False otherwise.

SIDE EFFECTS
  None

===========================================================================*/

boolean mcc_update_rf_params_metrics
(
  onexl3_qsh_metric_rf_params_s *metrics_ptr
)
{   
  byte rssi0, rssi1, rssi_comb, ecio0, ecio1, ecio_comb;
  int16 tmp_tx_pwr;

  /* This API returns RSSI in db units, returns 125 if RSSI value is default */
  srch_get_cdma_rssi_all_chains( &rssi_comb, &rssi0, &rssi1 );
    
  /* The above SRCH APIs returned the values in 1/2 dB units and we need
   * to convert it to dB units before passing to QSH. Returns 63 if ECIO 
   * value is default */  
  srch_get_cdma_raw_ecio_all_chains( &ecio_comb, &ecio0, &ecio1 );  

  /* Tx API returns tx power in 1/10 dB units and returns -32768 for TX OFF */
  tmp_tx_pwr = txc_get_tx_agc_dbm10();  
  if (tmp_tx_pwr != -32768)
  {
    tmp_tx_pwr = tmp_tx_pwr/10;
    
    /* If we are in traffic state store the last known valid TX power value */
    if(MAIN_STATE(cdma.curr_state) == CDMA_TC)
    {
      valid_tx_power = tmp_tx_pwr;
    }
  }
  else
  {
    if(MAIN_STATE(cdma.curr_state) == CDMA_TC)
    {
      /* Report the last known valid TX power in traffic state */
      tmp_tx_pwr = valid_tx_power;
    }
    else
    {
      /* Since we are not in Traffic state any more reset 
       * the value of last known valid TX power in traffic state */
      valid_tx_power = tmp_tx_pwr;
    }
  }
  
  metrics_ptr->rssi0 = rssi0;
  metrics_ptr->rssi1 = rssi1;
  metrics_ptr->rssi_comb = rssi_comb;
  metrics_ptr->ecio0 = (ecio0 == 63) ? ecio0 : ecio0/2;
  metrics_ptr->ecio1 = (ecio1 == 63) ? ecio1 : ecio1/2;
  metrics_ptr->ecio_comb = (ecio_comb == 63) ? ecio_comb : ecio_comb/2;
  metrics_ptr->tx_pwr = tmp_tx_pwr;  
  
  return TRUE;

} /* mcc_update_rf_params_metrics */

/*===========================================================================

FUNCTION MC_QSH_COMMIT_DATA

DESCRIPTION
  This API is used by 1X-CP to send metrics info to QSH module. The metrics 
  could be sent due to metric timer expiry or due to 1X internal event such 
  as BS change.
  Metrics sent through this API will also ensure that write address for this
  metric would move to the next index in that metric's FIFO thus ensuring that
  next instance of this metric wouldn't overwrite the previous one.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void mc_qsh_commit_data
(
  uint8 metric_id,
  qsh_client_metric_log_reason_e reason,
  qsh_client_metric_cfg_s *metric_cfg
)
{  
  qsh_client_metric_log_done_s cb_done;

  qsh_client_metric_log_done_init(&cb_done);
  
  cb_done.client = QSH_CLT_ONEXL3;
  cb_done.metric_id = metric_id;
  cb_done.metric_context_id = metric_cfg->metric_context_id;
  cb_done.log_reason = reason; 
  metric_cfg->start_addr = qsh_client_metric_log_done(&cb_done);
  
} /* mc_qsh_commit_data */

/*===========================================================================

FUNCTION MC_INIT_QSH_METRIC_CFG_ARRAY

DESCRIPTION
  To configure the 1X_CP array for metrics conllection before passing it to QSH.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void mc_init_qsh_metric_cfg_array
(
  void
)
{
  qsh_ext_metric_cfg_s *metric_cfg_ptr;
  sys_modem_as_id_e_type cur_asid = mc_qsh_get_asid();

  metric_cfg_ptr = &(onexl3_qsh_metric_default_cfg_arr[ONEXL3_QSH_METRIC_CELL_INFO]);

  metric_cfg_ptr->id = ONEXL3_QSH_METRIC_CELL_INFO;
  metric_cfg_ptr->subs_id = cur_asid;
  metric_cfg_ptr->fifo.element_size_bytes = sizeof(onexl3_qsh_metric_cell_info_s); /* Temp */
  metric_cfg_ptr->fifo.element_count_total = MC_QSH_METRIC_CELL_INFO_COUNT;
  metric_cfg_ptr->sampling_period_ms = 0;  /* Indicates that this metric is not periodic */


  metric_cfg_ptr = &(onexl3_qsh_metric_default_cfg_arr[ONEXL3_QSH_METRIC_RF_PARAMS]);

  metric_cfg_ptr->id = ONEXL3_QSH_METRIC_RF_PARAMS;
  metric_cfg_ptr->subs_id = cur_asid;
  metric_cfg_ptr->fifo.element_size_bytes = sizeof(onexl3_qsh_metric_rf_params_s); /* Temp */
  metric_cfg_ptr->fifo.element_count_total = MC_QSH_METRIC_RF_PARAMS_COUNT;
  metric_cfg_ptr->sampling_period_ms = MC_QSH_METRIC_PERIODICITY;  /* This metric is periodic */  
  
} /* mc_init_qsh_metric_cfg_array */

/*===========================================================================

FUNCTION MC_QSH_IS_METRIC_ENABLED

DESCRIPTION
  Checks if a particular 1X_CP metric is enabled by QSH module or not.

DEPENDENCIES
  None.

RETURN VALUE
  True, if metric is enabled.
  False, otherwise.

SIDE EFFECTS
  None.

===========================================================================*/

boolean mc_qsh_is_metric_enabled
(
  onexl3_qsh_metric_e metric_id
)
{
  boolean ret = FALSE;

  if(mc_qsh_data.metric_cfg[metric_id].action == QSH_METRIC_ACTION_START)
  {
    ret = TRUE;
  }
  return ret;  
} /* mc_qsh_is_metric_enabled */

/*===========================================================================

FUNCTION MC_QSH_GET_ASID

DESCRIPTION

  This API returns the ASID value for the current CDMA subscription
  If 1X_CP has not yet been informed by MMode of the ASID value
  this API will default the ASID to SYS_MODEM_AS_ID_1

DEPENDENCIES
  None.

RETURN VALUE
  ASID value

SIDE EFFECTS
  None

===========================================================================*/
sys_modem_as_id_e_type mc_qsh_get_asid
(  
  void
)
{
  sys_modem_as_id_e_type cur_asid = mcc_get_asid();
  
  if (cur_asid == SYS_MODEM_AS_ID_NONE)
  {
    cur_asid = SYS_MODEM_AS_ID_1;
    M1X_MSG( DCP,  LEGACY_HIGH, 
             "1X ASID is None, defaulting to ASID 1");    
  }
  return cur_asid;
  
} /* mc_qsh_get_asid */
#endif /* FEATURE_1XCP_QSH_SUPPORT */

