#ifndef _KR_VSIM_H_
#define _KR_VSIM_H_


#define VSIM_MAXIMUM_PROFILES_AVAILABLE 16


/*===========================================================================

STURCTURE SimProperties

DESCRIPTION
    This structure is to be populated with the VSIM data and passed into
    the 'krvsim_setup_sim' function.  For a description of the SIM files,
    please refer to ETSI TS 131102

CONTENTS
    IMSI
        - EF_6F09 (International Modile Subscriber Identity)
        - 9 bytes
        - Mandatory
    ICCID
        - EF_2FE2 (Integrated Circuit Card Identifier)
        - 10 bytes
        - Mandatory
    EHPLMN
        - EF_6FD9 (Equivalent Home PLMN list)
        - 6 bytes
        - Mandatory, initialize unused PLMN entries as (FF, FF, FF)
    HPLMN
        - EF_6F62 (Home PLMN list)
        - 100 bytes
        - Mandatory, initialize unused PLMN entries as (FF, FF, FF, 00, 00)
        - This is the primary list the VSIM uses to connect to networks.
          To achieve a faster connect time, initialize this list with PLMNs
          from the country the user is current in and you have agreements
          with. Having a gigantic list of every country's PLMNs is inefficiet
          and the user will have to wait a long time for a connection. Every
          PLMN entry is 5 bytes long (3 bytes for PLMN, 2 bytes for Access
          Technology).
    PLMN
        - EF_6F30 (Public Land Mobile Network)
        - 30 bytes
        - Mandatory, initialize unused PLMN entries as (FF, FF, FF)
    SMSP
        - EF_6F42 (SMS Service Parameters)
        - 28 bytes
        - Mandatory
        - Contains some SMS service details as well as the SMSC number
    FPLMN
        - EF_6F7B (Forbidden PLMN)
        - 12 bytes
        - Mandatory, initialize unused PLMN entries as (FF, FF, FF)
        - Contains a list of PLMNs the MS should not attempt to connect to
    KI
        - 16 bytes
        - Mandatory
        - This is the private key the SIM uses in the authentication algorithm.
          The VSIM uses a custom Milenage for network authentication
    OPC
        - 16 bytes
        - Mandatory
        - The OPC is used during authentication
    is3GSimCard
        - If 0, the VSIM will initialize as a SIM (A0 command structure)
        - If 1 (or anyting != 0), the VSIM will initialize as a USIM (00 command structure)

    PROFILE
        - 1...16 representing the different profiles we have

===========================================================================*/
typedef struct {
    unsigned char IMSI[9];
    unsigned char ICCID[10];
    unsigned char EHPLMN[6];
    unsigned char HPLMN[100]; // 5 bytes per PLMN entry [includes 2 bytes for access tech]
    unsigned char PLMN[30];
    unsigned char SMSP[28];
    unsigned char FPLMN[12];
    unsigned char KI[16];
    unsigned char OPC[16];


    unsigned char PROFILE1[9];
    unsigned char PROFILE2[9];
    unsigned char PROFILE3[9];
    unsigned char PROFILE4[9];
    unsigned char PROFILE5[9];
    unsigned char PROFILE6[9];
    unsigned char PROFILE7[9];
    unsigned char PROFILE8[9];
    unsigned char PROFILE9[9];
    unsigned char PROFILE10[9];
    unsigned char PROFILE11[9];
    unsigned char PROFILE12[9];
    unsigned char PROFILE13[9];
    unsigned char PROFILE14[9];
    unsigned char PROFILE15[9];
    unsigned char PROFILE16[9];

    //unsigned char LOCI[11];      // 6F7E
    //unsigned char PSLOCI[14];    // 6F73
    //unsigned char EPSLOCI[18];   // 6FE3
    //unsigned char LOCIGPRS[14];  // 6F53

    int is3GSimCard;
    int isDirectCalling;
} SimProperties;



/*===========================================================================

FUNCTION krvsim_reset

DESCRIPTION
    This function will reset the VSIM, save its ATR in the provided buffer
    and return the size of the ATR

PARAMETERS
    buffer
        The ATR will be saved into this buffer, allocate at least 30 bytes
    buffer_size
        The size of the buffer

RETURN VALUE
    SUCCESS
        The number of bytes saved in the buffer
    ERROR
        -1

===========================================================================*/
int krvsim_reset (unsigned char* buffer, int buffer_size);




/*===========================================================================

FUNCTION krvsim_pps_procedure

DESCRIPTION
    This function will preform the PPS procedure and return the PPS response

PARAMETERS
    bb_data
        The baseband data containing the PPS
    bb_data_length
        The length of the PPS data
    buffer
        The PPS response will be saved into this buffer, allocate at least 5 bytes
    buffer_size
        The size of the buffer

RETURN VALUE
    SUCCESS
        The number of bytes saved in the buffer
    ERROR
        -1

===========================================================================*/
int krvsim_pps_procedure (unsigned char* bb_data, int bb_data_size, unsigned char* buffer, int buffer_size);




/*===========================================================================

FUNCTION krvsim_process_communication

DESCRIPTION
    This function will preform the PPS procedure and return the PPS response

PARAMETERS
    bb_data
        The baseband data to send to the SIM
    bb_data_length
        The length of the baseband data
    buffer
        The SIM response will be saved into this buffer, allocate at least 260 bytes
    buffer_size
        The size of the buffer

RETURN VALUE
    SUCCESS
        The number of bytes the SIM responded with
    ERROR
        0 or -1

===========================================================================*/
int krvsim_process_communication (unsigned char* bb_data, int bb_data_size, unsigned char* buffer, int length);




/*===========================================================================

FUNCTION krvsim_setup_sim

DESCRIPTION
    This function will initialize the VSIM with the given credentials

PARAMETERS
    sim_details
        The details of the SIM card

RETURN VALUE
    VOID

===========================================================================*/
void krvsim_setup_sim (SimProperties* sim_details);



/*===========================================================================

FUNCTION krvsim_is_direct_dialing

DESCRIPTION
    This function will set the SIM to direct/callback dialing mode

PARAMETERS
    is_on
        1 means direct dialing, anything else means callback

RETURN VALUE
    VOID

===========================================================================*/
void krvsim_set_direct_dialing (int is_on);

#endif
