#ifndef _KR_VSIM_CONTROL_PUBLIC_H_
#define _KR_VSIM_CONTROL_PUBLIC_H_


// These functions are used by KnowRoaming's private source code, do not put anything else here!

void tclHotswapSetSignalRemoved (int simSlot);
void tclHotswapSetSignalInserted (int simSlot);

int  tclSleep (int ms);

int  tclFileDebugLogWrite (char* data, int length);
int  tclFileCheck (const char* filename);
int  tclFileRead (const char* filename, unsigned char* buffer, int read_length, int fileOffset);
int  tclFileWriteTrunc (const char* filename, unsigned char* data, int length);
int  tclFileWriteAppnd (const char* filename, unsigned char* data, int length, int fileOffset);

void tclThreadSimReset (int debugInfo);

/*MODIFIED-BEGIN by “liyi.ding”, 2016-04-07,BUG-1752868*/
void tclThreadSimRemoval1();
void tclThreadSimRemoval2();
void tclThreadSimInsert1();
void tclThreadSimInsert2();

void tclThreadSimInsertBoth();
void tclThreadSimRemovalBoth();
void tclErrorOccuredOnSlot(int slot);
/*MODIFIED-END by “liyi.ding”,BUG-1752868*/
#endif
