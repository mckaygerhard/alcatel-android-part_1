#include <pthread.h>

#include "uim_msg.h"
#include "mmgsdilib_common.h"
#include "fs_lib.h"
#include "fs_public.h"
#include "fs_estdio.h"
#include "uim_v.h"
#include "uim_p.h"
#include "uimglobals.h"
#include "uimgen.h"                /* To access uim_gen APIs */
#include "uimdrv.h"                /* UIM driver header file               */
#include "krvsim_control_public.h"
#include "krvsim_control_private.h"
#include "uimdrv_clk.h"
#include "gstk_exp.h"



// Use these for quick replacements of UIM MSG level types
#define LOG_ERR_0(msg)                UIM_MSG_MED_0(msg)
#define LOG_ERR_1(msg, param1)        UIM_MSG_MED_1(msg, param1)
#define LOG_INFO_0(msg)               UIM_MSG_MED_0(msg)
#define LOG_INFO_1(msg, param1)       UIM_MSG_MED_1(msg, param1)


volatile int m_error_on_slot_1 = 0;
volatile int m_error_on_slot_2 = 0;

void tclErrorOccuredOnSlot(int slot)
{
    UIM_MSG_MED_1 ("VSIMLOG - Error Found on ", slot);

    if(slot ==1 )
        m_error_on_slot_1 =1;
    else if(slot == 2)
        m_error_on_slot_2 = 1;
}

void tclResetErrorOnSlot1()
{
    UIM_MSG_MED_0("VSIMLOG - RESETING ERROR CHECK");
    m_error_on_slot_1 = 0;
}

void tclResetErrorOnSlot2()
{
    UIM_MSG_MED_0("VSIMLOG - RESETING ERROR CHECK");
    m_error_on_slot_2 = 0;
}

/*
    * Running as a thread.
    * Be careful... there is a timing issue here.
    * Flooding resets will break STK, so app side needs to guard. I was worried about using mutex's, so instead
     put a simple check that could fail in a race condition.
*/
static void tclThreadVsimReset (void* arg) {
    UIM_MSG_MED_0 ("VSIMLOG - RESET THREAD STARTED");

    uim_rt_status_enum status;
    uim_instance_global_type* uim_ptr;
    int sleepCheck = 0;                                 // variable for checking how long clock has run
    int simInstanceBeingUsed;


    // Make sure this is happening only once, return otherwise
    if (vsim_private_is_ongoing_reset() == 0) {
        vsim_private_set_ongoing_reset();
    }
    else {
        LOG_INFO_0 ("VSIMLOG - RESET ONGOING. Skipping.");
        return;
    }


    // Get the current SIM instance -- if none active, just return
    if (vsim_private_is_enabled (0) == 1) {
        simInstanceBeingUsed = 0;
        LOG_INFO_1 ("VSIMLOG - VSIM PRESENT %d ", simInstanceBeingUsed);
    }
    else if (vsim_private_is_enabled (1) == 1) {
        simInstanceBeingUsed = 1;
        LOG_INFO_1 ("VSIMLOG - VSIM PRESENT %d ", simInstanceBeingUsed);
    }
    else {
        LOG_INFO_0 ("VSIMLOG - NO VSIM. Skipping.");
        vsim_private_clear_ongoing_reset();
        return;
    }

    // sleep to allow any RIL//MGSDI related stuff to finish.
    tclSleep (100);


    /*
        * Make sure clock is off for best chance of success
        * After 1s, something is wrong. Break out.
        * I let it continue though in the case we timed out.
    */

    sleepCheck = 0;
    while (vsim_private_is_clock_running() == 1) {
        sleepCheck += tclSleep (10);

        if (sleepCheck > 1000) {
            LOG_ERR_1 ("VSIMLOG - TIMEOUT - Clock is: %d after a while...", vsim_private_is_clock_running());
            break;
        }
    }
    LOG_INFO_0 ("VSIMLOG - 4 - Clock Down Preparing to Remove");

    // reset some variables
    vsim_private_clear_power_on_state();
    vsim_private_clear_gstk_shutdown();

    // remove
    uim_ptr = uim_get_instance_ptr (simInstanceBeingUsed);
    tclHotswapSetSignalRemoved (simInstanceBeingUsed);


    // either has not been triggered - release provided by uimdrv uim_power_down or the hotswap finishing
    // both must be satisifed to minize possible issues

    LOG_INFO_0 ("VSIMLOG - 5 - Waiting For Hotswap");
    sleepCheck = 0;
    while ( q_cnt (& uim_ptr->hotswap.hotswap_rem_cmd_q) > 0 || vsim_private_get_power_on_state() == 0) {
        uim_ptr = uim_get_instance_ptr (simInstanceBeingUsed);
        LOG_INFO_1 ("VSIMLOG - Hot Swap Queue Count: %d",   q_cnt (& uim_ptr->hotswap.hotswap_rem_cmd_q) );
        LOG_INFO_1 ("VSIMLOG - VSIM Power: %d",  vsim_private_get_power_on_state());
        sleepCheck += tclSleep (300); //LONG WAITS

        if ( sleepCheck > 5000) {
            LOG_ERR_0 ("VSIMLOG - TIMEOUT Waiting for hotswap" );
            break;
        }
    }

    // Make sure STK is down
    LOG_INFO_1 ("VSIMLOG - Waiting to kill GSTK %d", vsim_private_is_gstk_shutdown() ); //it could have executed by now, but check
    sleepCheck = 0; //reset sleep check
    while (vsim_private_is_gstk_shutdown() == 0) {
        sleepCheck += tclSleep (10);
        if ( sleepCheck > 5000) {
            LOG_ERR_0 ("VSIMLOG - TIMEOUT Waiting for GSTK" );
            break;
        }
    }

    // sleep some more to allow any RIL related stuff to finish
    tclSleep (100);


    // begin insertion and reset variables
    vsim_private_set_power_off_state(); // we are in the power off state
    vsim_private_clear_clock_run_once(); // want to know when the clock has ran at least one time
    tclHotswapSetSignalInserted (simInstanceBeingUsed);


    // power was and the clock came back up
    sleepCheck = 0;
    while (vsim_private_get_power_off_state() == 1 || vsim_private_get_clock_run_once() == 0) {
        sleepCheck += tclSleep (5);

        if (sleepCheck > 3000) {
            LOG_ERR_0 ("VSIMLOG - TIMEOUT Waiting for Power up");
            break;
        }
    }

    LOG_INFO_0 ("VSIMLOG - RESET FINISHED");
    vsim_private_clear_ongoing_reset();
}

// Set hotswap signal to removed
void tclHotswapSetSignalRemoved (int simSlot) {
    uim_instance_global_type* uim_ptr = NULL;

    boolean retStatus = FALSE;

    //clear flags to prevent hotswap break
    vsim_private_clear_power_on_state();

    if (simSlot == 0) {
        uim_ptr = uim_get_instance_ptr (UIM_INSTANCE_1);
        retStatus = uim_hotswap_set_signal (UIM_CARD_REMOVED, uim_ptr);
    }
    else if (simSlot == 1) {
        uim_ptr = uim_get_instance_ptr (UIM_INSTANCE_2);
        retStatus = uim_hotswap_set_signal (UIM_CARD_REMOVED, uim_ptr);
    }
    else if (simSlot == 2) {
        uim_ptr = uim_get_instance_ptr (UIM_INSTANCE_3);
        retStatus = uim_hotswap_set_signal (UIM_CARD_REMOVED, uim_ptr);
    }


    //ensure hotswap is done before moving on
    int sleepCheck = 0;
    while ( q_cnt (& uim_ptr->hotswap.hotswap_rem_cmd_q) > 0 ) {
        LOG_INFO_1 ("VSIMLOG - Hot Swap Queue Count(Removed): %d",   q_cnt (& uim_ptr->hotswap.hotswap_rem_cmd_q) );
        sleepCheck += tclSleep (200); //LONG WAITS

        if ( sleepCheck > 1000) {
            LOG_ERR_0 ("VSIMLOG - TIMEOUT Waiting for hotswap (removed)" );
            break;
        }
    }

    vsim_private_set_powerdown_complete();

    if (retStatus == FALSE){
        LOG_INFO_1 ("VSIMLOG - DOM REMOVE BROKEN, PRINT ", simSlot);
    }

}

// Set hotswap signal to inserted
void tclHotswapSetSignalInserted (int simSlot) {
    uim_instance_global_type* uim_ptr = NULL;

    boolean retStatus = FALSE;

    if (simSlot == 0) {
        uim_ptr = uim_get_instance_ptr (UIM_INSTANCE_1);
        retStatus = uim_hotswap_set_signal (UIM_CARD_INSERTED, uim_ptr);
    }
    else if (simSlot == 1) {
        uim_ptr = uim_get_instance_ptr (UIM_INSTANCE_2);
        retStatus = uim_hotswap_set_signal (UIM_CARD_INSERTED, uim_ptr);
    }
    else if (simSlot == 2) {
        uim_ptr = uim_get_instance_ptr (UIM_INSTANCE_3);
        retStatus = uim_hotswap_set_signal (UIM_CARD_INSERTED, uim_ptr);
    }
/*
    //ensure hotswap is done before moving on
    int sleepCheck = 0;g
    while ( q_cnt (& uim_ptr->hotswap.hotswap_rem_cmd_q) > 0 ) {

        LOG_INFO_1 ("VSIMLOG - Hot Swap Queue Count(Inserted): %d",   q_cnt (& uim_ptr->hotswap.hotswap_rem_cmd_q) );
        sleepCheck += tclSleep (200); //LONG WAITS

        if ( sleepCheck > 1000) {
            LOG_ERR_0 ("VSIMLOG - TIMEOUT Waiting for hotswap (Inserted)" );
            break;
        }
    }


    tclSleep(400);
*/
    if (retStatus == FALSE){
        LOG_INFO_1 ("VSIMLOG - DOM REMOVE BROKEN, PRINT ", simSlot);
    }
}

// Microsecond sleep
int tclSleep(int ms) {
    rex_sleep (ms);
    return ms;
}


// EFS file reading
int tclFileRead (const char* filename, unsigned char* buffer, int read_length, int fileOffset) {
    // If there is an offset, we need to use stream-based IO
    if (fileOffset > 0) {
        EFS_EFILE* fd = efs_efopen (filename, "rb");
        if (!fd) {
            LOG_ERR_0 ("VSIMLOG - tclFileRead: Could not open file for reading");
            return 0;
        }

        // Seek to the position (from the beginning of the file)
        int result = efs_efseek (fd, fileOffset, SEEK_SET);
        if (result != 0) {
            LOG_ERR_0 ("VSIMLOG - tclFileRead: Could not seek");
            efs_efclose (fd);
            return 0;
        }

        int bytes_read = efs_efread (buffer, 1, read_length, fd);
        if (bytes_read != read_length) {
            LOG_ERR_0 ("VSIMLOG - tclFileRead: Could not read all the requested bytes");
            efs_efclose (fd);
            return 0;
        }


        LOG_INFO_0 ("VSIMLOG - VSIM_READ_CONFIG Success");
        efs_efclose (fd);
        return 1;
    }
    else {
        int fd = efs_open (filename, O_RDONLY);
        if (fd < 0) {
            LOG_ERR_0 ("VSIMLOG - tclFileRead: Could not open file for reading");
            return 0;
        }

        int bytes_read = efs_read (fd, buffer, read_length);
        if (bytes_read != read_length) {
            LOG_ERR_0 ("VSIMLOG - tclFileRead: Could not read all the requested bytes");
            efs_close (fd);
            return 0;
        }

        LOG_INFO_0 ("VSIMLOG - VSIM_READ_CONFIG Success");
        efs_close (fd);
        return 1;
    }
}



// EFS file writing -- able to write to arbitrary positions in the file
int tclFileWriteAppnd (const char* filename, unsigned char* data, int length, int fileOffset) {
    // Open file for read/update (will error if doesn't exist)
    EFS_EFILE* fd = efs_efopen (filename, "rb+");
    if (!fd) {
        LOG_ERR_0 ("VSIMLOG - VSIM_FILE_SAVE: File does not exist, will create one");

        // If the file doesn't exist, open as write mode (creates the file or truncates)
        fd = efs_efopen (filename, "wb+");
        if (!fd) {
            LOG_ERR_0 ("VSIMLOG - VSIM_FILE_SAVE: Could not create file");
            return 0;
        }
    }

    // If there is a file offset, do a seek
    if (fileOffset > 0) {
        int result = efs_efseek (fd, fileOffset, SEEK_SET);
        if (result != 0) {
            LOG_ERR_0 ("VSIMLOG - VSIM_FILE_SAVE: Could not seek");
            efs_efclose (fd);
            return 0;
        }
    }

    // Write the data
    int bytes_written = efs_efwrite (data, 1, length, fd);
    if (bytes_written != length) {
        LOG_ERR_0 ("VSIMLOG - VSIM_FILE_SAVE: Error writing to file, not all bytes could be written");
        efs_efclose (fd);
        return 0;
    }

/*
    //sync the file system
    if(efs_sync("/") !=0)
    {
        LOG_ERR_0 ("VSIMLOG - Sync Failure - Write Append");
    }*/

    // Close the file
    efs_efclose (fd);

    // Quick sleep. Noticed odd behaviour with writing
    tclSleep (3);

    // Return success
    return 1;
}


// EFS file writing -- this will overwrite the file every time!
int tclFileWriteTrunc (const char* filename, unsigned char* data, int length) {
    int fd = efs_open (filename, O_CREAT | O_TRUNC | O_WRONLY);
    if (fd < 0) {
        LOG_ERR_0 ("VSIMLOG - VSIM_FILE_SAVE: Could not open file for writing");
        return 0;
    }

    int bytes_written = efs_write (fd, data, length);
    if (bytes_written != length) {
        LOG_ERR_0 ("VSIMLOG - VSIM_FILE_SAVE: Error writing to file, not all bytes could be written");
        efs_close (fd);
        return 0;
    }

  /*  //sync the file system
    if(efs_sync("/") !=0)
    {
        LOG_ERR_0 ("VSIMLOG - Sync Failure - Write Append");
    }*/


    efs_close (fd);
    tclSleep (3); //quick sleep. Noticed odd behaviour with writing
    return 1;
}


// Return 1 if the file exists
int tclFileCheck (const char* filename) {
    int fd = efs_open (filename, O_RDONLY);
    if (fd < 0) {
        return 0;
    }
    else {
        efs_close (fd);
        return 1;
    }
}


// Spin off a thread ot let other execution finish
// Used by SIM core when UIM tries to do a sim refersh
void tclThreadSimReset (int debugInfo) {
    LOG_INFO_1 ("VSIMLOG - THREADED RESET CALLED FROM LOCATION: %d", debugInfo);

    pthread_t pth;
    pthread_create (&pth, NULL, (void*) &tclThreadVsimReset, NULL);

    // TODO do we need to join or memory leak??
}



static void tclThreadVsimRemoval1 (void* arg) {
    LOG_INFO_0 ("VSIMLOG - REMOVE 1 THREAD STARTED");

    rex_sleep(1000); //one second for mmgsdi to return
    tclHotswapSetSignalRemoved(0);


}


static void tclThreadVsimRemoval2 (void* arg) {
    LOG_INFO_0 ("VSIMLOG - REMOVE 2 THREAD STARTED");

    rex_sleep(1000); //one second for mmgsdi to return
    tclHotswapSetSignalRemoved(1);


}

void tclThreadSimRemoval1 () {
    LOG_INFO_0("VSIMLOG - THREADED REMOVE 1 CALLED FROM LOCATION");
    tclResetErrorOnSlot1(); //reset slot 1 error watch

    pthread_t pth;
    pthread_create (&pth, NULL, (void*) &tclThreadVsimRemoval1, NULL);

    // TODO do we need to join or memory leak??
}

void tclThreadSimRemoval2 () {
    LOG_INFO_0("VSIMLOG - THREADED REMOVE 2 CALLED FROM LOCATION");
    tclResetErrorOnSlot2(); //reset slot 2 error watch

    pthread_t pth;
    pthread_create (&pth, NULL, (void*) &tclThreadVsimRemoval2, NULL);

    // TODO do we need to join or memory leak??
}


static void tclThreadVsimInsert1(void* arg) {
    LOG_INFO_0 ("VSIMLOG - INSERT THREAD SLOT 1 STARTED");

    tclSleep(1000); //one second for mmgsdi to return

    int sleepCheck =0;
    int max_sleep = 1000;

    LOG_INFO_1 ("VSIMLOG - Slot 1 is %d", m_error_on_slot_1 );

    while(m_error_on_slot_1 == 0)
    {
        LOG_INFO_0 ("VSIMLOG - Waiting for slot 1");
        sleepCheck += tclSleep(200); //wait 200 ms

        if(sleepCheck > max_sleep)
        {
            LOG_INFO_0 ("VSIMLOG - Timeout waiting for slot 1");
            break;
        }
    }

    LOG_INFO_1 ("VSIMLOG - Check finished, Slot 1 is %d", m_error_on_slot_1 );
    tclHotswapSetSignalInserted(0);
}


static void tclThreadVsimInsert2(void* arg) {
    LOG_INFO_0 ("VSIMLOG - INSERT THREAD SLOT 2 STARTED");

    tclSleep(1000); //one second for mmgsdi to return

    int sleepCheck = 0;
    int max_sleep = 1000;
    while(m_error_on_slot_2 == 0)
    {
        LOG_INFO_0 ("VSIMLOG - Waiting for slot 2");
        sleepCheck += tclSleep(200); //wait 200 ms

        if(sleepCheck > max_sleep)
        {
            LOG_INFO_0 ("VSIMLOG - Timeout waiting for slot 2");
            break;
        }
    }  

    LOG_INFO_1 ("VSIMLOG - Check finished, Slot 2 is %d", m_error_on_slot_2 );
    tclHotswapSetSignalInserted(1);
}

static void tclThreadVsimInsertBoth(void* arg) {
    LOG_INFO_0 ("VSIMLOG - INSERT Both THREAD STARTED");

    tclSleep(0); //one second for mmgsdi to return

    int sleepCheck = 0 ;
    int max_sleep = 1000;

    LOG_INFO_1 ("VSIMLOG - Slot 1 is %d", m_error_on_slot_1 );
    LOG_INFO_1 ("VSIMLOG - Slot 2 is %d", m_error_on_slot_2 );

    while(m_error_on_slot_1 == 0)
    {
        LOG_INFO_0 ("VSIMLOG - Waiting for slot 1");
        sleepCheck += tclSleep(200); //wait 200 ms

        if(sleepCheck > max_sleep)
        {
            LOG_INFO_0 ("VSIMLOG - Timeout waiting for slot 1");
            break;
        }
    }
    LOG_INFO_1 ("VSIMLOG - Check finished, Slot 1 is %d", m_error_on_slot_1 );
    tclSleep(0); //space the inserts

    sleepCheck = 0;
    while(m_error_on_slot_2 == 0)
    {
        LOG_INFO_0 ("VSIMLOG - Waiting for slot 2");
        sleepCheck += tclSleep(200); //wait 200 ms

        if(sleepCheck > max_sleep)
        {
            LOG_INFO_0 ("VSIMLOG - Timeout waiting for slot 2");
            break;
        }
    }  

    LOG_INFO_1 ("VSIMLOG - Check finished, Slot 2 is %d", m_error_on_slot_2 );


    /*
    	Test Change for Ding
		
		Qualcomm does not handle quick swap well. Let us boot physical sim first
	*/

	if(vsim_private_is_enabled (0) == 1) //is VSIM on slot 1? boot slot2 first then 
	{

	    LOG_INFO_0 ("VSIMLOG - Error triggered on slot 2, inserting");
	    tclHotswapSetSignalInserted(1);	

	    tclSleep(1000); //space the inserts

	    LOG_INFO_0 ("VSIMLOG - Error triggered on slot 1, inserting");
	    tclHotswapSetSignalInserted(0); 


	}
	else
	{

		//if vsim is slot2, proceed as expected, boot slot 1 first
	    LOG_INFO_0 ("VSIMLOG - Error triggered on slot 1, inserting");
	    tclHotswapSetSignalInserted(0); 

	    tclSleep(1000); //space the inserts

	    LOG_INFO_0 ("VSIMLOG - Error triggered on slot 2, inserting");
	    tclHotswapSetSignalInserted(1);	
	}



}


static void tclThreadVsimRemoveBoth(void* arg) {
    LOG_INFO_0 ("VSIMLOG - INSERT Both THREAD STARTED");

    tclResetErrorOnSlot1(); //reset
    tclResetErrorOnSlot2(); //reset

    //seems like secret sauce to disable 2 first then 1
    //tclSleep(0); //space the inserts
    tclHotswapSetSignalRemoved(1);
    tclSleep(0); //small space
    tclHotswapSetSignalRemoved(0); 
    
}


void tclThreadSimInsertBoth () {
    LOG_INFO_0 ("VSIMLOG - THREADED INSERT CALLED FROM LOCATION");

    pthread_t pth;
    pthread_create (&pth, NULL, (void*) &tclThreadVsimInsertBoth, NULL);

    // TODO do we need to join or memory leak??
}

void tclThreadSimRemovalBoth () {
    LOG_INFO_0 ("VSIMLOG - THREADED INSERT CALLED FROM LOCATION");

    pthread_t pth;
    pthread_create (&pth, NULL, (void*) &tclThreadVsimRemoveBoth, NULL);

    // TODO do we need to join or memory leak??
}



void tclThreadSimInsert1 () {
    LOG_INFO_0 ("VSIMLOG - THREADED INSERT CALLED FROM LOCATION");

    pthread_t pth;
    pthread_create (&pth, NULL, (void*) &tclThreadVsimInsert1, NULL);

    // TODO do we need to join or memory leak??
}

void tclThreadSimInsert2 () {
    LOG_INFO_0 ("VSIMLOG - THREADED INSERT CALLED FROM LOCATION");

    pthread_t pth;
    pthread_create (&pth, NULL, (void*) &tclThreadVsimInsert2, NULL);

    // TODO do we need to join or memory leak??
}

// This is called to save logging information from private code to the EFS
// DISABLED BY DEFAULT OR EFS WILL FILL UP
int tclFileDebugLogWrite (char* data, int length) {
    // FUNCTION IS DISABLED BY DEFAULT -- use this for debugging only!
    #ifndef VSIM_DEBUGGING
        return 1;
    #endif

    //special case
    // Open file for read/update (will error if doesn't exist)
    EFS_EFILE* fd = efs_efopen ("/data/vsim_debug.log", "wb");
    if (!fd) {
        return 0;
    }

    // Write the data
    int bytes_written = efs_efwrite (data, 1, length, fd);
    if (bytes_written != length) {
        LOG_ERR_0 ("VSIMLOG - VSIM_FILE_SAVE: Error writing to file, not all bytes could be written");
        efs_efclose (fd);
        return 0;
    }

        //sync the file system
   /* if(efs_sync("/") !=0)
    {
        LOG_ERR_0 ("VSIMLOG - Sync Failure - Write Append");
    }*/

    // Close the file
    efs_efclose (fd);

    // Quick sleep. Noticed odd behaviour with writing
    tclSleep (3);

    // Return success
    return 1;
}