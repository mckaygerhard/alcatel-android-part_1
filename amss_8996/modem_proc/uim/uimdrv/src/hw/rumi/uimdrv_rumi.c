/*============================================================================
  FILE:         uimdrv_rumi.c

  OVERVIEW:     File contains RUMI specific driver code.

  DEPENDENCIES: N/A

                Copyright (c) 2015 QUALCOMM Technologies, Inc(QTI).
                All Rights Reserved.
                QUALCOMM Technologies Confidential and Proprietary
============================================================================*/

/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/uim/uimdrv/src/hw/rumi/uimdrv_rumi.c#1 $
$DateTime: 2016/03/28 23:02:52 $
$Author: mplcsds1 $

  when      who     what, where, why
------      ----    -----------------------------------------------------------
05/07/15    ll      Change RUMI level translator GPIO numbers
04/28/15    ll      9x55 Bring-up changes
03/30/15    ak      Compile fix for Atlas RUMI
03/04/15    na      Removal of compiler warnings
01/22/15    na      Rumi crash issue
01/16/15    na      Support of RUMI
08/15/13    ak      Fix VSEL control functions for UIM2
07/09/13    ak      Created
============================================================================*/
#include "uimdrv_main.h"
#include "uim_v.h"
#include "DDITlmm.h"
#include "uimdrv_rumi.h"

#define UIM1_RUMI_V_SEL_GPIO_NUM                           69
#define UIM2_RUMI_V_SEL_GPIO_NUM                           70
#define UIM1_RUMI_V_ENABLE_GPIO_NUM                        71
#define UIM2_RUMI_V_ENABLE_GPIO_NUM                        72


#define UIM1_RUMI_V_ENABLE_GPIO \
	    DAL_GPIO_CFG(UIM1_RUMI_V_ENABLE_GPIO_NUM, 0, (uint32)DAL_GPIO_OUTPUT, (uint32)DAL_GPIO_NO_PULL, (uint32)DAL_GPIO_2MA)   /* UIM1 VOLTAGE ENABLE   */

#define UIM1_RUMI_V_SEL_GPIO \
	    DAL_GPIO_CFG(UIM1_RUMI_V_SEL_GPIO_NUM, 0, (uint32)DAL_GPIO_OUTPUT, (uint32)DAL_GPIO_NO_PULL, (uint32)DAL_GPIO_2MA)      /* UIM1 VOLTAGE SELECT  */

#define UIM2_RUMI_V_ENABLE_GPIO \
	    DAL_GPIO_CFG(UIM2_RUMI_V_ENABLE_GPIO_NUM, 0, (uint32)DAL_GPIO_OUTPUT, (uint32)DAL_GPIO_NO_PULL, (uint32)DAL_GPIO_2MA)   /* UIM2 VOLTAGE ENABLE   */

#define UIM2_RUMI_V_SEL_GPIO \
	    DAL_GPIO_CFG(UIM2_RUMI_V_SEL_GPIO_NUM, 0, (uint32)DAL_GPIO_OUTPUT, (uint32)DAL_GPIO_NO_PULL, (uint32)DAL_GPIO_2MA)      /* UIM2 VOLTAGE SELECT  */

#define UIM_HWIO_RUMI_IN(addr)        (*((volatile uint32 *) (addr)))
#define UIM_HWIO_RUMI_OUT(addr, val)  (*((volatile uint32 *) (addr)) = ((uint32) (val)))

/* Starting from Thor, MSS_PERPH_REG_BASE was changed to MPSS_PERPH_REG_BASE in HWIO */
#if defined(FEATURE_THOR_MODEM) || defined(FEATURE_ATLAS_MODEM)
#define UIM_PERPH_REG_BASE MPSS_PERPH_REG_BASE
#else
#define UIM_PERPH_REG_BASE MSS_PERPH_REG_BASE
#endif

/* Clock driver */
/* SIM CLK */
#define MSS_UIM0_CBCR_RUMI               (UIM_PERPH_REG_BASE + 0x2044)
#define MSS_UIM0_SRC_CMD_RCGR_RUMI       (UIM_PERPH_REG_BASE + 0x2010)
#define MSS_UIM0_SRC_CFG_RCGR_RUMI       (UIM_PERPH_REG_BASE + 0x2014)
#define MSS_UIM1_CBCR_RUMI               (UIM_PERPH_REG_BASE + 0x2064)
#define MSS_UIM1_SRC_CMD_RCGR_RUMI       (UIM_PERPH_REG_BASE + 0x2018)
#define MSS_UIM1_SRC_CFG_RCGR_RUMI       (UIM_PERPH_REG_BASE + 0x201C)

/* UART CLK */
#define MSS_UART_BIT_UIM0_CBCR_RUMI      (UIM_PERPH_REG_BASE + 0x2048)
#define MSS_UART_BIT_UIM1_CBCR_RUMI      (UIM_PERPH_REG_BASE + 0x2068)
/* HCLK */
#define MSS_BUS_UIM0_CBCR_RUMI           (UIM_PERPH_REG_BASE + 0x204C)
#define MSS_BUS_UIM1_CBCR_RUMI           (UIM_PERPH_REG_BASE + 0x206C)

/* UART BAUD RATE */
#define MSS_UIM0_MND_CMD_RCGR_RUMI       (UIM_PERPH_REG_BASE + 0x2030)
#define MSS_UIM0_MND_CFG_RCGR_RUMI       (UIM_PERPH_REG_BASE + 0x2034)
#define MSS_UIM0_MND_M_RUMI              (UIM_PERPH_REG_BASE + 0x2038)
#define MSS_UIM0_MND_N_RUMI              (UIM_PERPH_REG_BASE + 0x203C)
#define MSS_UIM0_MND_D_RUMI              (UIM_PERPH_REG_BASE + 0x2040)

#define MSS_UIM1_MND_CMD_RCGR_RUMI       (UIM_PERPH_REG_BASE + 0x2050)
#define MSS_UIM1_MND_CFG_RCGR_RUMI       (UIM_PERPH_REG_BASE + 0x2054)
#define MSS_UIM1_MND_M_RUMI              (UIM_PERPH_REG_BASE + 0x2058)
#define MSS_UIM1_MND_N_RUMI              (UIM_PERPH_REG_BASE + 0x205C)
#define MSS_UIM1_MND_D_RUMI              (UIM_PERPH_REG_BASE + 0x2060)

/*MND REG Table*/
uimdrv_rumi_fi_di_mnd_reg_map_type uimdrv_rumi_mnd_reg[] =
{{UIM_CRCF_372_1,UIM_BRAF_1,  {0x01F8,0x1D19,0x1B21}},
 {UIM_CRCF_372_1,UIM_BRAF_12, {0x00C0,0xF97B,0xF8BB}},
 {UIM_CRCF_512,UIM_BRAF_1,    {0x0010,0xF60F,0xF5FF}},
 {UIM_CRCF_512,UIM_BRAF_2,    {0x0020,0xF61F,0xF5FF}},
 {UIM_CRCF_512,UIM_BRAF_4,    {0x0040,0xF63F,0xF5FF}},
 {UIM_CRCF_512,UIM_BRAF_8,    {0x0080,0xF67F,0xF5FF}},
 {UIM_CRCF_512,UIM_BRAF_16,   {0x0100,0xF6FF,0xF5FF}},
 {UIM_CRCF_512,UIM_BRAF_32,   {0x0200,0xF7FF,0xF5FF}},
 {UIM_CRCF_512,UIM_BRAF_64,   {0x0400,0xF9FF,0xF5FF}},
 /* End of the ROW */
 {UIM_CRCF_SIZE,UIM_BRAF_SIZE,{0x00,0x00,0x00}}
};


/*===========================================================================
FUNCTION uimdrv_rumi_config_gpios

DESCRIPTION
  CONFIG_VLTG_ENABLE_VSEL_SLOT configures the GPIOs connected to the V ENABLE and
  V SELECT lines.  For RUMI only.

===========================================================================*/
void uimdrv_rumi_config_gpios(uim_instance_enum_type uim_instance_id)
{
#ifdef T_RUMI_EMULATION
  if (UIM_INSTANCE_1 == uim_instance_id)
  {
    if(NULL != uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr)
    {
      /* Configure v_enable line */
      (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr,
                                (uint32)UIM1_RUMI_V_ENABLE_GPIO,
                                (uint32)DAL_TLMM_GPIO_ENABLE );
      /* Configure v_sel line */
      (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr,
                                (uint32)UIM1_RUMI_V_SEL_GPIO,
                                (uint32)DAL_TLMM_GPIO_ENABLE );
    }
  }
  else
  {
    if(NULL != uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr)
    {
      /* Configure v_enable line */
      (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr,
                                (uint32)UIM2_RUMI_V_ENABLE_GPIO,
                                (uint32)DAL_TLMM_GPIO_ENABLE );
      /* Configure v_sel line */
      (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr,
                                (uint32)UIM2_RUMI_V_SEL_GPIO,
                                (uint32)DAL_TLMM_GPIO_ENABLE );
    }
  }
#endif /* T_RUMI_EMULATION */

  return;
}


/*===========================================================================
FUNCTION uimdrv_rumi_vsel_off

DESCRIPTION
  CONFIG_VSEL_OFF_SLOT disables the V ENABLE line and sets the V SELECT line
  to 1.8v.  For RUMI only.
===========================================================================*/
void uimdrv_rumi_vsel_off(uim_instance_enum_type uim_instance_id)
{
#ifdef T_RUMI_EMULATION
  /* Set v_enable line to low */
  if (UIM_INSTANCE_1 == uim_instance_id)
  {
    if(NULL != uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr)
    {
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM1_RUMI_V_ENABLE_GPIO, DAL_GPIO_LOW_VALUE);
      /* set v_sel line to 1.8 v */
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM1_RUMI_V_SEL_GPIO, DAL_GPIO_LOW_VALUE);
    }
  }
  else
  {
    if(NULL != uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr)
    {
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM2_RUMI_V_ENABLE_GPIO, DAL_GPIO_LOW_VALUE);
      /* set v_sel line to 1.8 v */
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM2_RUMI_V_SEL_GPIO, DAL_GPIO_LOW_VALUE);
    }
  }
#endif /* T_RUMI_EMULATION */

  return;
}


/*===========================================================================
FUNCTION uimdrv_rumi_vsel_on_class_c

DESCRIPTION
  Turns on the V_ENABLE line and sets the V_SELECT line for 1.8v operation.
  For RUMI only.
===========================================================================*/
void uimdrv_rumi_vsel_on_class_c(uim_instance_enum_type uim_instance_id)
{
#ifdef T_RUMI_EMULATION
  if (UIM_INSTANCE_1 == uim_instance_id)
  {
    if(NULL != uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr)
    {
      /* Set v_enable line to high */
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM1_RUMI_V_ENABLE_GPIO, DAL_GPIO_HIGH_VALUE);
      /* set v_sel line to 1.8 v */
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM1_RUMI_V_SEL_GPIO, DAL_GPIO_LOW_VALUE);
    }
  }
  else
  {
    if(NULL != uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr)
    {
      /* Set v_enable line to high */
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM2_RUMI_V_ENABLE_GPIO, DAL_GPIO_HIGH_VALUE);
      /* set v_sel line to 3.0 v */
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM2_RUMI_V_SEL_GPIO, DAL_GPIO_LOW_VALUE);
    }
  }
#endif /* T_RUMI_EMULATION */

  return;
}


/*===========================================================================
FUNCTION uimdrv_rumi_vsel_on_class_b

DESCRIPTION
  Turns on the V_ENABLE line and sets the V_SELECT line for 3v operation.
  For RUMI only.
===========================================================================*/
void uimdrv_rumi_vsel_on_class_b(uim_instance_enum_type uim_instance_id)
{
#ifdef T_RUMI_EMULATION
  if (UIM_INSTANCE_1 == uim_instance_id)
  {
    if(NULL != uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr)
    {
      /* Set v_enable line to high */
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM1_RUMI_V_ENABLE_GPIO, DAL_GPIO_HIGH_VALUE);
      /* set v_sel line to 1.8 v */
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM1_RUMI_V_SEL_GPIO, DAL_GPIO_HIGH_VALUE);
    }
  }
  else
  {
    if(NULL != uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr)
    {
      /* Set v_enable line to high */
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM2_RUMI_V_ENABLE_GPIO, DAL_GPIO_HIGH_VALUE);
      /* set v_sel line to 3.0 v */
      (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance_id].m_TlmmHandle_ptr, (uint32)UIM2_RUMI_V_SEL_GPIO, DAL_GPIO_HIGH_VALUE);
    }
  }
#endif /* T_RUMI_EMULATION */

  return;
}


/*===========================================================================
FUNCTION uimdrv_rumi_enable_uart_clock

DESCRIPTION
  Enabling the UART CLOCK
===========================================================================*/
void uimdrv_rumi_enable_uart_clock(uim_instance_enum_type uim_instance_id)
{
#ifdef T_RUMI_EMULATION
  uint32 uimdrv_rumi_current_cbcr_value = 0x00;
  if(UIM_INSTANCE_1 == uim_instance_id)
  {
    uimdrv_rumi_current_cbcr_value = UIM_HWIO_RUMI_IN(MSS_UART_BIT_UIM0_CBCR_RUMI);
    UIM_HWIO_RUMI_OUT(MSS_UART_BIT_UIM0_CBCR_RUMI,(uimdrv_rumi_current_cbcr_value |0x1));
    uim_clk_busy_wait (100);

  }
  else if(UIM_INSTANCE_2 == uim_instance_id)
  {
    uimdrv_rumi_current_cbcr_value = UIM_HWIO_RUMI_IN(MSS_UART_BIT_UIM1_CBCR_RUMI);
    UIM_HWIO_RUMI_OUT(MSS_UART_BIT_UIM1_CBCR_RUMI,(uimdrv_rumi_current_cbcr_value |0x1));
    uim_clk_busy_wait (100);
  }
#endif
  return;
}/* uimdrv_rumi_enable_uart_clock */


/*===========================================================================
FUNCTION uimdrv_rumi_enable_sim_clock

DESCRIPTION
  Enabling the SIM CLOCK
===========================================================================*/
void uimdrv_rumi_enable_sim_clock(uim_instance_enum_type uim_instance_id)
{
#ifdef T_RUMI_EMULATION
  if(UIM_INSTANCE_1 == uim_instance_id)
  {
    UIM_HWIO_RUMI_OUT(MSS_UIM0_CBCR_RUMI,0x01);
    uim_clk_busy_wait (100);
  }
  else if(UIM_INSTANCE_2 == uim_instance_id)
  {
    UIM_HWIO_RUMI_OUT(MSS_UIM1_CBCR_RUMI,0x01);
    uim_clk_busy_wait (100);
  }
#endif
  return;
}/* uimdrv_rumi_enable_simt_clock */


/*===========================================================================
FUNCTION uimdrv_rumi_enable_h_clock

DESCRIPTION
  Enabling the HCLOCK CLOCK
===========================================================================*/
void uimdrv_rumi_enable_h_clock(uim_instance_enum_type uim_instance_id)
{
#ifdef T_RUMI_EMULATION
  uint32 uimdrv_rumi_current_bus_uim_cbcr_value = 0x00;
  if(UIM_INSTANCE_1 == uim_instance_id)
  {
    uimdrv_rumi_current_bus_uim_cbcr_value = UIM_HWIO_RUMI_IN(MSS_BUS_UIM0_CBCR_RUMI);
    UIM_HWIO_RUMI_OUT(MSS_BUS_UIM0_CBCR_RUMI,(uimdrv_rumi_current_bus_uim_cbcr_value |0x1));
  }
  else if(UIM_INSTANCE_2 == uim_instance_id)
  {
    uimdrv_rumi_current_bus_uim_cbcr_value = UIM_HWIO_RUMI_IN(MSS_BUS_UIM1_CBCR_RUMI);
    UIM_HWIO_RUMI_OUT(MSS_BUS_UIM1_CBCR_RUMI,(uimdrv_rumi_current_bus_uim_cbcr_value |0x1));
  }

#endif
  return;
}/* uimdrv_rumi_enable_h_clock */


/*===========================================================================
FUNCTION uimdrv_rumi_set_baud_rate

DESCRIPTION
  Setting the baud rate
===========================================================================*/
void uimdrv_rumi_set_baud_rate(uim_instance_enum_type uim_instance_id, word Fi, word Di)
{
#ifdef T_RUMI_EMULATION
  uint32 loop_idx=0x0;

  if(UIM_INSTANCE_1 == uim_instance_id)
  {

    UIM_HWIO_RUMI_OUT(MSS_UIM0_MND_CFG_RCGR_RUMI,0x2000);
    uim_clk_busy_wait (100);
    for(loop_idx=0x00; uimdrv_rumi_mnd_reg[loop_idx].mnd.mreg; loop_idx++)
    {
      if( (Fi == uimdrv_rumi_mnd_reg[loop_idx].FI) &&
          (Di == uimdrv_rumi_mnd_reg[loop_idx].DI)
        )
      {
        UIM_HWIO_RUMI_OUT(MSS_UIM0_MND_M_RUMI,uimdrv_rumi_mnd_reg[loop_idx].mnd.mreg);
        uim_clk_busy_wait(100);
        UIM_HWIO_RUMI_OUT(MSS_UIM0_MND_N_RUMI,uimdrv_rumi_mnd_reg[loop_idx].mnd.nreg);
        uim_clk_busy_wait(100);
        UIM_HWIO_RUMI_OUT(MSS_UIM0_MND_D_RUMI,uimdrv_rumi_mnd_reg[loop_idx].mnd.dreg);
        uim_clk_busy_wait(100);
        UIM_HWIO_RUMI_OUT(MSS_UIM0_MND_CMD_RCGR_RUMI,0x3);
        uim_clk_busy_wait(100);
      }
    }
  }
  else if(UIM_INSTANCE_2 == uim_instance_id)
  {
    UIM_HWIO_RUMI_OUT(MSS_UIM1_MND_CFG_RCGR_RUMI,0x2000);
    uim_clk_busy_wait (100);
    for(loop_idx=0x00; uimdrv_rumi_mnd_reg[loop_idx].mnd.mreg; loop_idx++)
    {
      if( (Fi == uimdrv_rumi_mnd_reg[loop_idx].FI) &&
          (Di == uimdrv_rumi_mnd_reg[loop_idx].DI)
        )
      {
        UIM_HWIO_RUMI_OUT(MSS_UIM1_MND_M_RUMI,uimdrv_rumi_mnd_reg[loop_idx].mnd.mreg);
        uim_clk_busy_wait(100);
        UIM_HWIO_RUMI_OUT(MSS_UIM1_MND_N_RUMI,uimdrv_rumi_mnd_reg[loop_idx].mnd.nreg);
        uim_clk_busy_wait(100);
        UIM_HWIO_RUMI_OUT(MSS_UIM1_MND_D_RUMI,uimdrv_rumi_mnd_reg[loop_idx].mnd.dreg);
        uim_clk_busy_wait(100);
        UIM_HWIO_RUMI_OUT(MSS_UIM1_MND_CMD_RCGR_RUMI,0x3);
        uim_clk_busy_wait(100);
      }
    }
  }
#endif
  return;
}/* uimdrv_rumi_set_baud_rate */


/*===========================================================================
FUNCTION uimdrv_rumi_set_clk_freq_3_84Mhz

DESCRIPTION
  Configuring the clock @3.84MHz
===========================================================================*/
void uimdrv_rumi_set_clk_freq_3_84Mhz(uim_instance_enum_type uim_instance_id)
{
#ifdef T_RUMI_EMULATION
  /* For xo/5 = 3.84MHz, write below register with 0x9 */
  if(UIM_INSTANCE_1 == uim_instance_id)
  {
    UIM_HWIO_RUMI_OUT(MSS_UIM0_SRC_CFG_RCGR_RUMI,0x9);
    uim_clk_busy_wait(100);
    UIM_HWIO_RUMI_OUT(MSS_UIM0_SRC_CMD_RCGR_RUMI,0x3);
  }
  else
  {
    UIM_HWIO_RUMI_OUT(MSS_UIM1_SRC_CFG_RCGR_RUMI,0x9);
    uim_clk_busy_wait(100);
    UIM_HWIO_RUMI_OUT(MSS_UIM1_SRC_CMD_RCGR_RUMI,0x3);
  }
#endif
  return;
}/* uimdrv_rumi_set_clk_freq_3_84Mhz */


/*===========================================================================
FUNCTION uimdrv_rumi_set_clk_freq_4_8Mhz

DESCRIPTION
  Configuring the clock @4.8MHz
===========================================================================*/
void uimdrv_rumi_set_clk_freq_4_8Mhz(uim_instance_enum_type uim_instance_id)
{

#ifdef T_RUMI_EMULATION
  /* For xo/4 = 4.8MHz, write below register with 0x7 */
  if(UIM_INSTANCE_1 == uim_instance_id)
  {
    UIM_HWIO_RUMI_OUT(MSS_UIM0_SRC_CFG_RCGR_RUMI,0x7);
    uim_clk_busy_wait(100);
    UIM_HWIO_RUMI_OUT(MSS_UIM0_SRC_CMD_RCGR_RUMI,0x3);
  }
  else
  {
    UIM_HWIO_RUMI_OUT(MSS_UIM1_SRC_CFG_RCGR_RUMI,0x7);
    uim_clk_busy_wait(100);
    UIM_HWIO_RUMI_OUT(MSS_UIM1_SRC_CMD_RCGR_RUMI,0x3);
  }
#endif
  return;
} /* uimdrv_rumi_set_clk_freq_4_8Mhz */
