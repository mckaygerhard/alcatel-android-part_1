/*============================================================================
  FILE:         uim_powerup_logging.c

  OVERVIEW:     The file defines the powerup logging module

  DEPENDENCIES: N/A

                Copyright (c) 2014 - 2015 QUALCOMM Technologies, Inc(QTI).
                All Rights Reserved.
                QUALCOMM Technologies Confidential and Proprietary
============================================================================*/

/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/uim/common/src/uim_powerup_logging.c#1 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
06/29/15   lm       Enable memory leak detection
08/04/15   sam      Initial version

=============================================================================*/

/*=============================================================================

                     INCLUDE FILES FOR MODULE

=============================================================================*/
#include "uim_variation.h"
#ifdef FEATURE_UIM_POWERUP_LOGGING
#include "uim_powerup_logging.h"
#include "modem_mem.h"
#include "uimutil.h"
#ifdef FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#endif /* FEATURE_UIM_TEST_FRAMEWORK */


/*Boolean to check if the intialization is done or not*/
boolean pup_globals_init = FALSE;

/*Ptr to the UIM logging module globals*/
uim_pup_log_global_type *uim_pup_ptr = NULL;


/**
*This function adds an entry to the  circular buffer based on the tcb_ptr.
*
*  @param uim_pup_enum  tag    The tag of the msg string
*  @param uint32  p1                    The first parameter for the printf style 
*                                                msg string.  
*  @param uint32  p2                    The first parameter for the printf style 
*                                                msg string.  
*  @param uint32  p3                    The first parameter for the printf style 
*                                                msg string.  
*  @param uint32  p4                    The first parameter for the printf style 
*                                                 msg string.
*  @param uint8   no_of_args         The number of printf style parametrs/arguments 
*                                                 present in the msg string.
*/
void uim_pup_log_add
(
  uim_pup_enum tag, 
  uint32 p1, 
  uint32 p2, 
  uint32 p3,
  uint32 p4,
  uint8 no_of_args
  )
{
  int  i = 0;
  
  /*this will point to the memroy slot to which the current log will be pushed to */
  uim_pup_msg_type *write_index_ptr = NULL;
  
  /*temp variable to hold current task's tcb_ptr*/
  rex_tcb_type  *temp_tcb_ptr;
  
  temp_tcb_ptr = rex_self();

  /*Check if the current task has registered for powerup logging.
      if the task has registered it will be present in the memory block table*/
  for (i=0;i<uim_pup_ptr->total_blocks;i++)
  {
    /*cross verfiy with the tcb_ptr stored in the memory block table*/
    if(temp_tcb_ptr == uim_pup_ptr->block[i]->tcb_ptr)
    {
      if((TRUE == uim_pup_ptr->block[i]->block_full) &&
         (STOP_LOGGING_WHEN_BUFFER_FULL == uim_pup_ptr->config.pup_logging_type))
      {
        return;
      }

      /*tcb_ptr  match found. retrieve the current message slot  from matched block 'i'*/
      write_index_ptr =uim_pup_ptr->block[i]->msg_array_ptr + uim_pup_ptr->block[i]->current_index;

      /*incrment the current index to next msg slot in the block*/
      ++uim_pup_ptr->block[i]->current_index;

      /*check if we have reached the last msg_slot of the block
        if yes , move to the first_slot/head/start of the memory block.  */
      if(uim_pup_ptr->block[i]->current_index >= uim_pup_ptr->block[i]->max_no_of_logs)
      {
        uim_pup_ptr->block[i]->current_index = 0;

        /*True the block_full flag. this is used to update the flush index*/
        uim_pup_ptr->block[i]->block_full = TRUE;
      }

      /*if the memory block gets overwritten, then the flush index also needs to be updated 
        to always start the flushing from oldest log*/
      if(uim_pup_ptr->block[i]->block_full == TRUE)
      {
         uim_pup_ptr->block[i]->flush_index = uim_pup_ptr->block[i]->current_index;
      }
      break;
    }
  }

  /*write index will be null, the task is not registered for pup logging*/
   if(NULL == write_index_ptr)
   {
     return;
   }

  /*push the current log to the msg_slot in the memory block*/
  memset(write_index_ptr, 0x00, sizeof(uim_pup_msg_type));
 
  write_index_ptr->p1 = p1;
  write_index_ptr->p2 = p2;
  write_index_ptr->p3 = p3;
  write_index_ptr->p4 = p4;
  write_index_ptr->tag =  tag;
  uim_get_tstamp (&write_index_ptr->tstamp);
  write_index_ptr->no_of_args = no_of_args;
}/*uim_pup_log_add*/


/**
*This function will start the process of flushing the power up log messages to QXDM.  
*
* the messages will be flushed in groups of 5 at a time to avoid bus clogging. 
* and between each group of messages there is a delay of 50ms.
*
* Each memory block of the block table is flusehed one by one.
*
* In each memory block the messsages are flushed from oldest to the newest logs.
*/
void uim_pup_log_flush_to_diag (void)
{
  int i =0;

  /*Boolean to indicate if to resart timer of 250ms*/
  boolean reset_timer = TRUE;
  
  /*this will point to the memroy slot that will be flushed to daig */
  uim_pup_msg_type *flush_index_ptr = NULL;
  
  /*ptr to hold the current memory block that is being  flushed*/
  uim_pup_block_type* block_ptr =  NULL;

  /*Change the state to flusshing to stop pup logging*/
  if(UIM_FLUSHING != uim_pup_ptr->current_state)
  {
    uim_pup_ptr->current_state = UIM_FLUSHING;
#ifndef FEATURE_UIM_TEST_FRAMEWORK
    (void)timer_set( &uim_pup_ptr->logging_timer,
                      1000, 0, T_MSEC);
#endif
    MSG(MSG_SSID_DFLT, MSG_LEGACY_MED,"waiting for 1 sec before flushing for any pendig requests to be completed");
    return;
  }


  if((NO_OF_MAX_INSTANCES <= uim_pup_ptr->current_block) ||
  	  NULL == uim_pup_ptr->block[uim_pup_ptr->current_block])
  {
    reset_timer = FALSE;
  }
  else
  {
    block_ptr =  uim_pup_ptr->block[uim_pup_ptr->current_block];
    /*Flush only 5 messages at a time to avoid bus clogging*/
    for(i=0; i<5; i++)
    {
      /*--> if the memory block is not yet full, then check if the 
             memory block is completely flushed before printing the log. 
        --> if all the memroy blocks are flushed, then set restart timer to FLASE.*/
      if(FALSE == block_ptr->block_full && 
        (block_ptr->flush_index == block_ptr->current_index))
      {
        ++uim_pup_ptr->current_block;
        if(uim_pup_ptr->current_block >= uim_pup_ptr->total_blocks)
        {
          /*If all the blocks are cmpleted, then donot restart the timer*/
          reset_timer = FALSE;
        }
        break;
      }

      flush_index_ptr =block_ptr->msg_array_ptr + block_ptr->flush_index;
	
      memset(uim_pup_ptr->temp_string, 0x00, sizeof(uim_pup_ptr->temp_string));
	  
      /*Flush the msg to QXDM through diag*/
      (void)snprintf( uim_pup_ptr->temp_string, sizeof(uim_pup_ptr->temp_string), 
                    "pup_log[0%ld]: %02d:%02d:%02d:%03d", 
                     block_ptr->flush_index,
                     flush_index_ptr->tstamp.hour,
                     flush_index_ptr->tstamp.minute,
                     flush_index_ptr->tstamp.second,
                     flush_index_ptr->tstamp.ms);
	  
      MSG_SPRINTF_7(MSG_SSID_DFLT, MSG_LEGACY_HIGH,"%s  tag:%x  no_of_Args:0%d  p1:%x  p2:%x  p3:%x  p4:%x " , uim_pup_ptr->temp_string,
                                flush_index_ptr->tag,
                                flush_index_ptr->no_of_args,
                                flush_index_ptr->p1,
                                flush_index_ptr->p2,
                                flush_index_ptr->p3,
                                flush_index_ptr->p4);

      /*move to the next msg_slot in the current memory block*/
      ++block_ptr->flush_index;

      /*check if we need to move to the head of the memory block*/
      if(block_ptr->flush_index >= block_ptr->max_no_of_logs)
      {
        block_ptr->flush_index = 0;
      }

      /*--> if the memory block is full and overwrittenl, then check if the 
            memory block is completely flushed after printing the log. 
        --> if all the memroy blocks are flushed, then set restart timer to FLASE.*/
      if(TRUE == block_ptr->block_full && 
	  	(block_ptr->flush_index == block_ptr->current_index))
      {
        ++uim_pup_ptr->current_block;
        if(uim_pup_ptr->current_block >= uim_pup_ptr->total_blocks)
        {
          /*If all the blocks are cmpleted, then donot restart the timer*/
          reset_timer = FALSE;
        }
        break;
      }
    }
  }
  
  /*Restart the timer if there are still logs remaining to be flushed*/
  if (TRUE == reset_timer)
  {
#ifndef FEATURE_UIM_TEST_FRAMEWORK
    (void)timer_set( &uim_pup_ptr->logging_timer,
                       50, 0, T_MSEC);
#endif
  }
  else
  {
    /*if all the memory blocks are flushed. then undef the timer*/
    uim_pup_ptr->current_block= 0;
    timer_undef(&uim_pup_ptr->logging_timer);

    /*Based on NV either stop logging or continue to log*/
    if(CONTINUE_LOGGING_INDEFINITELY == uim_pup_ptr->config.pup_logging_type)
    {
      uim_pup_ptr->current_state = UIM_LOGGING;
    }
    else
    {
      uim_pup_ptr->current_state = UIM_STOPPED;

     /*if the logging is stopped then clean up the globals.
       free all the memory*/
     uim_pup_log_clean();
    }
  }
}/*uim_pup_log_flush_to_diag*/


/**
*Function to generate the timestamp
*
*@param uim_pup_tstamp_type *tstamp  Pointer to timestamp
*/
void uim_get_tstamp(uim_pup_tstamp_type *tstamp)
{
  time_julian_type temp_tstamp;
  qword temp_tstamp_1;
  uint64 temp_time_in_ms;

  time_get_ms(temp_tstamp_1);
  temp_time_in_ms = (((uint64)temp_tstamp_1[1]) << 32) | ((uint64)temp_tstamp_1[0]);

#ifndef FEATURE_UIM_TEST_FRAMEWORK
  time_get_julian(&temp_tstamp);
#else
  #error code not present
#endif

  tstamp->ms = (uint16) (temp_time_in_ms % 1000);
  tstamp->hour = (uint8)temp_tstamp.hour;
  tstamp->minute = (uint8)temp_tstamp.minute;
  tstamp->second = (uint8)temp_tstamp.second;
}/*uim_get_tstamp*/


/**
*This function is to:
* - to intialize the common globals of the pup module
* - to also register the caller task to pup logging.
*
* the common globals is intilaized once in the first call.
*
* whichever calls this function, it gets registered to the pup logging module. 
* by assiging/mallocing a memory block and getting added to the mem block table.
*/
void uim_pup_log_init(uim_instance_enum_type instance_id)
{
  uim_pup_block_type *block_ptr =NULL;

#ifdef FEATURE_UIM_TEST_FRAMEWORK
  #error code not present
#endif
  /*use critical section since the same globals can be accesd by the other tasks as well*/
  rex_enter_crit_sect(&pup_log_crit_sec);
  if (FALSE == pup_globals_init)
  {
    /*set the flag pup_globals_init to TRUE so that the common globals
    are not remalloced by the other task calls */
    pup_globals_init = TRUE;

    /*allocate memory for the common globals*/
    uim_pup_ptr = uim_malloc(sizeof(uim_pup_log_global_type));

    /*if the allocation fails then leave the critical section and return*/
    if(NULL == uim_pup_ptr)
    {
      rex_leave_crit_sect(&pup_log_crit_sec);
      return;
    }

    memset(uim_pup_ptr, 0x00, sizeof(uim_pup_log_global_type));

    /*read the NV from EFS to the global*/
    if (UIM_COMMON_EFS_SUCCESS == uim_common_efs_read(UIM_COMMON_EFS_UIM_PUP_LOGGING_CONFIG,
                                                      UIM_COMMON_EFS_ITEM_FILE_TYPE,
                                                     (uim_common_efs_context_type)UIM_COMMON_EFS_CONTEXT_0,
                                                     (uint8 *)&(uim_pup_ptr->config),
                                                     sizeof(uim_pup_logging_config_type)))
    {
      if(0 != uim_pup_ptr->config.no_of_msgs_per_slot)
      {
        if((UIM_PUP_LOGGING_TIMER_VALUE_MIN > uim_pup_ptr->config.timer_value_in_ms)
           || (UIM_PUP_LOGGING_TIMER_VALUE_MAX < uim_pup_ptr->config.timer_value_in_ms))
      	{
          uim_pup_ptr->config.timer_value_in_ms = UIM_PUP_LOGGING_TIMER_VALUE_DEFAULT;
      	}
        if(UIM_PUP_LOGGING_NO_OF_MSGS_PER_SLOT_MAX < uim_pup_ptr->config.no_of_msgs_per_slot)
        {
          uim_pup_ptr->config.no_of_msgs_per_slot = UIM_PUP_LOGGING_NO_OF_MSGS_PER_SLOT_DEFAULT;
        }
      }
      else
      {
        /*if the no_of_msgs_per_slot is 0, then the pup logging is disalbed
          free the common globals and return*/
        uim_pup_mem_free((void**)&uim_pup_ptr);
        rex_leave_crit_sect(&pup_log_crit_sec);
        return;
      }
    }
    else
    {
      /*If efs file read fails, wirte the default values and write to efs*/
      memset(&uim_pup_ptr->config, 0x0, sizeof(uim_pup_logging_config_type));
      uim_pup_ptr->config.timer_value_in_ms = UIM_PUP_LOGGING_TIMER_VALUE_DEFAULT;
      uim_pup_ptr->config.disable_pup_logging[2] = TRUE;
      uim_pup_ptr->config.no_of_msgs_per_slot = UIM_PUP_LOGGING_NO_OF_MSGS_PER_SLOT_DEFAULT;
      uim_pup_ptr->config.pup_logging_type = STOP_LOGGING_AT_TIMER_EXPIRY;
      (void)uim_common_efs_write(UIM_COMMON_EFS_UIM_PUP_LOGGING_CONFIG,
                                  UIM_COMMON_EFS_ITEM_FILE_TYPE,
                                 (uim_common_efs_context_type)UIM_COMMON_EFS_CONTEXT_0,
                                 (const char*)&(uim_pup_ptr->config),
                                 sizeof(uim_pup_logging_config_type));
    }

    /*Define hte powerup logging timer and set to the nv read value*/		
    timer_def(&(uim_pup_ptr->logging_timer), &(uim_pup_ptr->logging_timer_group), NULL, 0,
				(timer_t1_cb_type)uim_pup_log_flush_to_diag, (timer_cb_data_type)0);
#ifndef FEATURE_UIM_TEST_FRAMEWORK
    (void)timer_set( &uim_pup_ptr->logging_timer,
                      uim_pup_ptr->config.timer_value_in_ms, 0, T_MSEC);
#endif

    /*move the current state to logging to start logging*/
    uim_pup_ptr->current_state = UIM_LOGGING;
  }

  /*Register the current task by allocating a memory block and add it to the memory block table */
  if(NULL == uim_pup_ptr || TRUE == uim_pup_ptr->config.disable_pup_logging[instance_id])
  {
    /*Either the pup globals is null due to malloc faiure or the NV is set to diable pup logging for this nstance*/
    rex_leave_crit_sect(&pup_log_crit_sec);
    return;
  }
  
  /*intialize the memory block parameters*/
  block_ptr = uim_malloc(sizeof(uim_pup_block_type));
  if(NULL== block_ptr)
  {
    rex_leave_crit_sect(&pup_log_crit_sec);
    return;
  }
  memset(block_ptr, 0x00, sizeof(uim_pup_block_type));
  
  /*store the tcb_ptr to the block. for comparison dring logging*/
  block_ptr->tcb_ptr = rex_self();
    
  /*Allocate memory for this memory block. the no of msg_slots in this memory block is determied throgh the NV*/
  block_ptr->max_no_of_logs =uim_pup_ptr->config.no_of_msgs_per_slot;
  block_ptr->msg_array_ptr = uim_malloc((block_ptr->max_no_of_logs) * sizeof(uim_pup_msg_type));
  
  if(NULL== block_ptr->msg_array_ptr)
  {
    /*if the allocation failed then free this block parameters*/
    uim_pup_mem_free((void**)&block_ptr);
    rex_leave_crit_sect(&pup_log_crit_sec);
    return;
  }

  /*set the current index/ptr to the first msg_slot/head/start of the memory block*/
  block_ptr->current_index = 0;

  /*set the flush index/ptr to the first msg_lot/head/start of the memory block*/
  block_ptr->flush_index = 0;
  
  block_ptr->block_full = FALSE;

  /*Add the block to the memory table*/
  uim_pup_ptr->block[uim_pup_ptr->total_blocks] = block_ptr;
  uim_pup_ptr->total_blocks += 1;
  rex_leave_crit_sect(&pup_log_crit_sec);
}


/* Function to clean the memory allocated  in the powerup module.
*   pnce the pup logging is stopped. this function is called to free all the memory.
*/
void uim_pup_log_clean()
{
  int i =0;

  /*Free all the memory that was allocated*/
  if(NULL == uim_pup_ptr)
  {
    return;
  }
  for(i=0; i<uim_pup_ptr->total_blocks; i++)
  {
    if(NULL == uim_pup_ptr->block[i])
    {
      continue;
    }
    uim_pup_mem_free((void**)&(uim_pup_ptr->block[i]->msg_array_ptr));
    uim_pup_mem_free((void**)&(uim_pup_ptr->block[i]));
  }
  uim_pup_mem_free((void**)&uim_pup_ptr);
//MSG(MSG_SSID_DFLT, MSG_LEGACY_HIGH,"pup log clean");
}


/**
*Pup logging API for msgs with no arguments/parametrs
*
*  @param uim_pup_enum  tag    The tag of the msg string
*/
void uim_pup_log_add_0
(
  uim_pup_enum tag
)
{
  if((NULL == uim_pup_ptr) ||
  (UIM_LOGGING != uim_pup_ptr->current_state) ||
  (0 == uim_pup_ptr->total_blocks))
  {
    return;
  }
  uim_pup_log_add(tag,0, 0, 0, 0, 0);
}


/**
*Pup logging API for msgs with 1 arguments/parametrs
*
*  @param uim_pup_enum  tag    The tag of the msg string
*  @param uint32  p1                    The first parameter for the printf style 
*                                                msg string.  
*/
void uim_pup_log_add_1
(
  uim_pup_enum tag, 
  uint32 p1
)
{
  if((NULL == uim_pup_ptr) ||
     (UIM_LOGGING != uim_pup_ptr->current_state) ||
     (0 == uim_pup_ptr->total_blocks))
  {
    return;
  }
  uim_pup_log_add(tag,p1, 0, 0, 0, 1);
}

/**
*Pup logging API for msgs with 2 arguments/parametrs
*
*  @param uim_pup_enum  tag    The tag of the msg string
*  @param uint32  p1                    The first parameter for the printf style 
*                                                msg string.  
*  @param uint32  p2                    The first parameter for the printf style 
*                                                msg string.  
*/
void uim_pup_log_add_2
(
  uim_pup_enum tag, 
  uint32 p1, 
  uint32 p2
)
{
  if((NULL == uim_pup_ptr) ||
     (UIM_LOGGING != uim_pup_ptr->current_state) ||
     (0 == uim_pup_ptr->total_blocks))
  {
    return;
  }
  uim_pup_log_add(tag,p1, p2, 0, 0, 2);
}


/**
*Pup logging API for msgs with 3 arguments/parametrs
*
*  @param uim_pup_enum  tag    The tag of the msg string
*  @param uint32  p1                    The first parameter for the printf style 
*                                                msg string.  
*  @param uint32  p2                    The first parameter for the printf style 
*                                                msg string.  
*  @param uint32  p3                    The first parameter for the printf style 
*                                                msg string.  
*/
void uim_pup_log_add_3
(
  uim_pup_enum tag, 
  uint32 p1, 
  uint32 p2, 
  uint32 p3
)
{
  if((NULL == uim_pup_ptr) ||
     (UIM_LOGGING != uim_pup_ptr->current_state) ||
     (0 == uim_pup_ptr->total_blocks))
  {
    return;
  }
  uim_pup_log_add(tag,p1, p2, p3, 0, 3);
}


/**
*Pup logging API for msgs with 4 arguments/parametrs
*
*  @param uim_pup_enum  tag    The tag of the msg string
*  @param uint32  p1                    The first parameter for the printf style 
*                                                msg string.  
*  @param uint32  p2                    The first parameter for the printf style 
*                                                msg string.  
*  @param uint32  p3                    The first parameter for the printf style 
*                                                msg string.  
*  @param uint32  p4                    The first parameter for the printf style 
*                                                 msg string.
*/
void uim_pup_log_add_4
(
  uim_pup_enum tag, 
  uint32 p1, 
  uint32 p2, 
  uint32 p3,
  uint32 p4
)
{
  if((NULL == uim_pup_ptr) ||
     (UIM_LOGGING != uim_pup_ptr->current_state) ||
	 (0 == uim_pup_ptr->total_blocks))
  {
    return;
  }
  uim_pup_log_add(tag,p1, p2, p3, p4, 4);
}

/*
*Function to free the malloced memory
*  @param void* ptr   poitner to the memory block that needs to be freed
*/

void uim_pup_mem_free(void** ptr)
{
  if(NULL != *ptr)
  {
    modem_mem_free((void*)(*ptr),MODEM_MEM_CLIENT_UIM);
    *ptr = NULL;
  }
}

#endif/*FEATURE_UIM_POWERUP_LOGGING*/
