/* Copyright (C) 2016 Tcl Corporation Limited */
#include "simlock_load.h"
#include "simlock_common.h"
#include "fs_public.h"
#define __JRD_TRACE_LOG__
#include "jrd_trace.h"
#include "smem.h"

#ifdef FEATURE_TCTNB_EMMC_IMEI

/***********************************************************************
FUNCTION     jrd_secro_get_length

PARAMETERS
len    : length of the data part of secro partition

DESCRIPTION
This function returns the length of the partition

RETURN VALUE
> 0:error
=0 :success
Notice:Actually,this function is almost useless,because data->sec_len obtained is  always 0,
and after this function is called,we will assign the len to SECURE_RO_BUF_SIZE..
So,should this function be removed?
**********************************************************************/
uint8 jrd_secro_get_length_and_errorCode(uint32 *len, uint8 *errorCode)
{
    int res = 0;
#ifdef FEATURE_TCTNB_RPMB_FOR_SIMLOCK
    uint8 *buf  = NULL;
#endif
    uint32 imei_size = 0;
    uint8 *data = NULL;
    uint8 *secure_imei_data = NULL;
    if(NULL==len||NULL==errorCode)
    {
        JRD_TRACE(("NULL==len||NULL==errorCode"));
        return SIMLOCK_INCORRECT_PARAMS;
    }
    data = (uint8 *)smem_get_addr(SMEM_ID_VENDOR0,&imei_size);
    JRD_TRACE(("imei_size=0x%x",imei_size));
    if(NULL == data)
    {
        res = 1;
        goto round;
    }

    secure_imei_data = (uint8*)malloc(imei_size);
    if (secure_imei_data == NULL)
    {
        JRD_TRACE(("Failed to allocate request memory"));
        return SIMLOCK_MEMORY_ERROR_HEAP_EXHAUSTED;
    }
    memset(secure_imei_data, 0x00, imei_size);
    memcpy((void*)secure_imei_data, (void*)data, imei_size);

    if(IMEI_RAM_MAGIC1 != ((secure_imei*)secure_imei_data)->magic1)
    {
        res = 2;
        goto round;
    }

#ifdef FEATURE_TCTNB_RPMB_FOR_SIMLOCK
    buf = (uint8 *)(&( ((secure_imei*)secure_imei_data)->sec_len));
    *len = buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24);
    *errorCode = ((secure_imei*)secure_imei_data)->sec_buf[0];//data->sec_buf[0] indicates error code
#else
    *len = 0;
#endif

round:
    if(secure_imei_data)free(secure_imei_data);
    return res;
}

/***********************************************************************
FUNCTION     jrd_secro_load

PARAMETERS
buffer : pointer on the outpu buffer
len    : length of this buffer in bytes( <= 10K)

DESCRIPTION
This function copy len bytes of datas
into the buffer given in parameter

RETURN VALUE
> 0 :error
**********************************************************************/
uint8 jrd_secro_load(uint8 *buffer_p, uint32 len)
{
    int res = 0;
#ifdef FEATURE_TCTNB_RPMB_FOR_SIMLOCK
    uint8 *buf  = NULL;
#endif
    uint32 imei_size = 0;
    uint8 *data = NULL;
    uint8 *secure_imei_data = NULL;
    if(NULL==buffer_p)return SIMLOCK_INCORRECT_PARAMS;

    data = (uint8 *)smem_get_addr(SMEM_ID_VENDOR0,&imei_size);

    if(NULL == data)
    {
        res = 1;
        goto round;
    }

    secure_imei_data = (uint8*)malloc(imei_size);
    if (secure_imei_data == NULL)
    {
        JRD_TRACE(("Failed to allocate request memory"));
        return SIMLOCK_MEMORY_ERROR_HEAP_EXHAUSTED;
    }

    memset(secure_imei_data, 0x00, imei_size);
    memcpy((void*)secure_imei_data, (void*)data, imei_size);

    if(IMEI_RAM_MAGIC1 != ((secure_imei*)secure_imei_data)->magic1)
    {
        res = 2;
        goto round;
    }

#ifdef FEATURE_TCTNB_RPMB_FOR_SIMLOCK
    if (NULL == ((secure_imei*)secure_imei_data)->sec_buf || len > SECURE_RO_BUF_SIZE)
    {
        res = 3;
        goto round;
    }

/*data->sec_buf[0] indicates error code,ignore errorCode 2*/
    if(0x00 != ((secure_imei*)secure_imei_data)->sec_buf[0] &&
        0x02 != ((secure_imei*)secure_imei_data)->sec_buf[0])
    {
        res = 4;
        goto round;
    }

    memcpy(buffer_p, (const void *)(&(((secure_imei*)secure_imei_data)->sec_buf[1])), len);
#endif
    round:
        if(secure_imei_data)free(secure_imei_data);
        return res;
}

/***********************************************************************
FUNCTION     jrd_NCK_load

PARAMETERS
buffer : pointer on the outpu buffer
len    : length of this buffer in bytes( <= 10K),>=sizeof(secure_Sha)

DESCRIPTION
This function copy len bytes of datas
into the buffer given in parameter

RETURN VALUE
< 0 on error
**********************************************************************/
uint8 jrd_NCK_load(uint8 *buffer_p, uint32 len)
{
    int res = 0;
    uint32 imei_size = 0;
    uint8 *data = NULL;
    uint8 *secure_imei_data = NULL;
#if 0
    uint8 testSalt[NCK_SALT_SIZE] = {};
    uint8 testN[20] = {0xb5,0x7f,0x2b,0xa2,0x47,0x62,0xf2,0xcd,0x3e,0x75,0x96,0x64,0x3c,0x2d,0xe2,0x75,0x6b,0x85,0xfc,0xf3};
    uint8 testNS[20] = {0x6e,0x1a,0x19,0x7e,0x89,0xd6,0xd1,0x8a,0x3d,0x52,0xfa,0x3f,0xf8,0x24,0xa4,0xa8, 0xff,0xbf,0x41,0x73};
    uint8 testSP[20] = {0x3b,0x47,0xbb,0xa0,0xcc,0x26,0x8a,0x05,0xb1,0xf4,0xc6,0xcc,0x1d,0xa9,0x29,0x96, 0xdb,0x4c,0x4d,0xbc};
    uint8 testCP[20] = {0x26,0x3d,0x1e,0x37,0x20,0x71,0xde,0xc7,0x5e,0x89,0xd5,0x37,0x62,0x76,0xda,0xbb, 0x6c,0x6d,0x4d,0x2b};
    uint8 testSIM[20] = {0x6a,0x82,0x7e,0xa4,0x68,0xc6,0xfb,0xef,0x19,0xc1,0xc2,0x49,0xd9,0x9c,0xa8,0x08, 0xdf,0x77,0x23,0x80};
#endif
    if(NULL==buffer_p)return SIMLOCK_INCORRECT_PARAMS;

    data = (uint8 *)smem_get_addr(SMEM_ID_VENDOR0,&imei_size);

    if(NULL == data)
    {
        res = 1;
        goto round;
    }

    secure_imei_data = (uint8*)malloc(imei_size);
    if (secure_imei_data == NULL)
    {
        JRD_TRACE(("Failed to allocate request memory"));
        return SIMLOCK_MEMORY_ERROR_HEAP_EXHAUSTED;
    }
    memset(secure_imei_data, 0x00, imei_size);
    memcpy((void*)secure_imei_data, (void*)data, imei_size);

    if(IMEI_RAM_MAGIC1 != ((secure_imei*)secure_imei_data)->magic1)
    {
        res = 2;
        goto round;
    }
  memcpy(buffer_p, (const void*)(&(((secure_imei*)secure_imei_data)->sha1.Salt[0])), len);
#if 0 /*hardcode for local test*/
  memcpy(buffer_p, testSalt, NCK_SALT_SIZE);
  memcpy(buffer_p+NCK_SALT_SIZE, testN, 20);
  memcpy(buffer_p+NCK_SALT_SIZE+1*NCK_HASH_SIZE, testNS, 20);
  memcpy(buffer_p+NCK_SALT_SIZE+2*NCK_HASH_SIZE, testSP, 20);
  memcpy(buffer_p+NCK_SALT_SIZE+3*NCK_HASH_SIZE, testCP, 20);
  memcpy(buffer_p+NCK_SALT_SIZE+4*NCK_HASH_SIZE, testSIM, 20);
#endif
  round:
    if(secure_imei_data)free(secure_imei_data);
    return res;
}

/*Add parameter 3 to distinguish different slots*/
uint8 jrd_read_otp_imei(uint8* imei_buffer, uint16 size, uint8 source)
{
   int res = 0;
    uint32 imei_size = 0;
    uint8 *data = NULL;
    uint8 *secure_imei_data = NULL;
    if(NULL==imei_buffer||source<1 ||source>4)return SIMLOCK_INCORRECT_PARAMS;

    data = (uint8 *)smem_get_addr(SMEM_ID_VENDOR0,&imei_size);

    if(NULL == data)
    {
        res = 1;
        goto round;
    }

    secure_imei_data = (uint8*)malloc(imei_size);
    if (secure_imei_data == NULL)
    {
        JRD_TRACE(("Failed to allocate request memory"));
        return SIMLOCK_MEMORY_ERROR_HEAP_EXHAUSTED;
    }
    memset(secure_imei_data, 0x00, imei_size);
    memcpy((void*)secure_imei_data, (void*)data, imei_size);

    if(IMEI_RAM_MAGIC1 != ((secure_imei*)secure_imei_data)->magic1)
    {
        res = 2;
        goto round;
    }

  if(1 == source)
  {
    memcpy(imei_buffer, (const void *)((secure_imei*)secure_imei_data)->imei_1, size);
  }
  else if(2 == source)
  {
    memcpy(imei_buffer, (const void *)((secure_imei*)secure_imei_data)->imei_2, size);
  }
  else if(3 == source)
  {
    memcpy(imei_buffer, (const void *)((secure_imei*)secure_imei_data)->imei_3, size);
  }
  else if(4 == source)
  {
    memcpy(imei_buffer, (const void *)((secure_imei*)secure_imei_data)->imei_4, size);
  }

round:
    if(secure_imei_data)free(secure_imei_data);
    return res;
}

uint8 jrd_read_CU(uint8 *CU_buffer, int size)
{
    int res = 0;
    uint32 imei_size = 0;
    uint8 *data = NULL;
    uint8 *secure_imei_data = NULL;
    if(NULL==CU_buffer)return SIMLOCK_INCORRECT_PARAMS;

    data = (uint8 *)smem_get_addr(SMEM_ID_VENDOR0,&imei_size);

    if(NULL == data)
    {
        res = 1;
        goto round;
    }

    secure_imei_data = (uint8*)malloc(imei_size);
    if (secure_imei_data == NULL)
    {
        JRD_TRACE(("Failed to allocate request memory"));
        return SIMLOCK_MEMORY_ERROR_HEAP_EXHAUSTED;
    }
    memset(secure_imei_data, 0x00, imei_size);
    memcpy((void*)secure_imei_data, (void*)data, imei_size);

    if(IMEI_RAM_MAGIC1 != ((secure_imei*)secure_imei_data)->magic1)
    {
        res = 2;
        goto round;
    }
#ifdef  FEATURE_TCTNB_RPMB_FOR_SIMLOCK
    memcpy(CU_buffer,
        (const void *)(& (((secure_imei*)secure_imei_data)->CU_buf[0])),
        (size<=((secure_imei*)secure_imei_data)->CU_len?size:((secure_imei*)secure_imei_data)->CU_len));
#endif
    round:
        if(secure_imei_data)free(secure_imei_data);
        return res;
}

#ifdef __DUMP__
void simlock_load_test()
{
    uint8 *imei_data=NULL;
    uint32 size= 0;
    uint8 ret=0;
    uint8 errorCode=0;
    uint32 i=0;
    JRD_TRACE(("Begin to dump secure::::::::::::::"));

/*Dump X data*/
    ret = jrd_secro_get_length_and_errorCode(&size, &errorCode);
    JRD_TRACE(("Get load length errorcode,ret=%d,len=%d,errorCode=%d", ret, size, errorCode));
    imei_data = (uint8*)malloc(size);
    if (!imei_data)JRD_TRACE(("Failed to allocate request memory"));
    memset(imei_data, 0x00, size);
    ret = jrd_secro_load(imei_data,size);
    JRD_TRACE(("Get load data,ret=%d",ret));
    if(imei_data != NULL)
    {
        i=0;
        while(i<size)
        {
            JRD_TRACE(("X data,[%d]->[%d]:0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x, 0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x",
            i,i+15,imei_data[i],imei_data[i+1],imei_data[i+2],imei_data[i+3],imei_data[i+4],imei_data[i+5],imei_data[i+6],imei_data[i+7],imei_data[i+8],imei_data[i+9],imei_data[i+10],imei_data[i+11],imei_data[i+12],imei_data[i+13],imei_data[i+14],imei_data[i+15]));
            i+=16;
        }
        free(imei_data);
    }

/*Dump imei*/
    imei_data = (uint8*)malloc(FLASH_IMEI_SIZE);
    memset(imei_data, 0x00, FLASH_IMEI_SIZE);
    ret= jrd_read_otp_imei(imei_data,FLASH_IMEI_SIZE,1);
    JRD_TRACE(("Get imei1,ret=%d",ret));
    if(imei_data != NULL)
    {
        i=0;
        JRD_TRACE(("imei1,[0]->[7]:0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x",
                imei_data[i],imei_data[i+1],imei_data[i+2],imei_data[i+3],imei_data[i+4],imei_data[i+5],imei_data[i+6],imei_data[i+7]));
    }
    ret = jrd_read_otp_imei(imei_data, FLASH_IMEI_SIZE, 2);
    JRD_TRACE(("Get imei2,ret=%d",ret));
    if(imei_data != NULL)
    {
        i=0;
        JRD_TRACE(("imei2,[0]->[7]:0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x",
                imei_data[i],imei_data[i+1],imei_data[i+2],imei_data[i+3],imei_data[i+4],imei_data[i+5],imei_data[i+6],imei_data[i+7]));
        free(imei_data);
    }

/*Dump SHA*/
    imei_data = (uint8*)malloc(2*sizeof(secure_Sha));
    if(!imei_data)JRD_TRACE(("Failed to allocate request memory"));
    memset(imei_data, 0x00, 2*sizeof(secure_Sha));
    ret = jrd_NCK_load(imei_data, 2*sizeof(secure_Sha));
    JRD_TRACE(("Get SHA,ret=%d",ret));
    if(imei_data != NULL)
    {
        i = 0;
        while(i<2*sizeof(secure_Sha))
        {
           JRD_TRACE(("sha,[%d]->[%d]:0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x",
                i,i+15,imei_data[i],imei_data[i+1],imei_data[i+2],imei_data[i+3],imei_data[i+4],imei_data[i+5],imei_data[i+6],imei_data[i+7],imei_data[i+8],imei_data[i+9],imei_data[i+10],imei_data[i+11],imei_data[i+12],imei_data[i+13],imei_data[i+14],imei_data[i+15]));
                i+=16;
        }
        free(imei_data);
    }

/*Dump CU*/
    imei_data = (uint8*)malloc(20);
    if(!imei_data)JRD_TRACE(("Failed to allocate request memory"));
    memset(imei_data, 0, 20);
    ret = jrd_read_CU(imei_data, 20);
    JRD_TRACE(("Get CU,ret=%d",ret));
    if(imei_data != NULL)
    {
        i = 0;
        JRD_TRACE(("CU,[%d]->[%d]:0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x,0X%02x",
            i,i+15,imei_data[i],imei_data[i+1],imei_data[i+2],imei_data[i+3],imei_data[i+4],imei_data[i+5],imei_data[i+6],imei_data[i+7],imei_data[i+8],imei_data[i+9],imei_data[i+10],imei_data[i+11],imei_data[i+12],imei_data[i+13],imei_data[i+14],imei_data[i+15]));
        free(imei_data);
    }

    JRD_TRACE(("End to dump secure::::::::::::::"));
}
#endif

/*!!!!!!ONLY for local test with x file in efs!!!!!*/
#define PERSO_FILE "/mmode/x12345678901.mbn"

uint8 simlock_perso_load_Xfile_from_efs
(
    uint8 *outPersoBuffer,
    uint32 len
)
{
    int32 fd;
    int32 res;
    if(NULL==outPersoBuffer)
    {
        JRD_TRACE(("NULL==outPersoBuffer"));
        return 3;
    }
    fd = efs_open(PERSO_FILE, O_RDONLY);
    JRD_TRACE(("fd of efs_open=%d",fd));
    if (fd < 0)
    {
        JRD_TRACE(( "Error opening EFS x12345678901.mbn %d", fd));
        return 1;
    }
    memset(outPersoBuffer, 0x00, len);
    res = efs_read ( fd, (void *)outPersoBuffer, len);

    JRD_TRACE(("res of efs_read=%d",res));if(res<=0)return 2;
    return 0;
}
#endif /*FEATURE_TCTNB_EMMC_IMEI*/
