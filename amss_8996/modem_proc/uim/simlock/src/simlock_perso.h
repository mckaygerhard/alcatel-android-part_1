/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef SIMLOCK_PERSO_H
#define SIMLOCK_PERSO_H

/*===========================================================================


            S I M L O C K   C O M M O N   D E F I N I T I O N S

                      A N D

                F U N C T I O N S


===========================================================================*/

/*===========================================================================

$Header: //components/rel/uim.mpss/3.1/api/simlock_perso.h#1 $$ $DateTime: 2015/03/30 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
03/20/15   LI Kai      Initial revision
===========================================================================*/


/* <EJECT> */
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "simlock.h"

/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/
/* Codes related defines */

#define  SML_TYPE_LEN (1) /*All types occupy only 1 bytes*/
#define  SML_NUM_LEN  (1) /*Nums of all data occupy only 1 byte*/
#define  SML_DATA_SIZE_LEN  (1)/*Small data length,including Lock_state,Retry_count,MNC,GID1,GID2,MSIN*/
#define  SML_DATA_SIZE_LEN_EXT (4)/*Large data length,including whole structure,Group,SIM,CK,Codes*/
#define  SML_SEC_PAR_VER_LEN    (4)/*Such as "01 00 00 00"*/
#define  SML_VER_STR_LEN          (16)/*Such as "x12345678901"*/

#define  SML_GROUP_NUM_LEN      (1)
#define  SML_TAIL_LEN                  (2) /*7F 6E*/
#define  SML_SHA_LEN   (64)//sha512 length
#define  SML_RSA_LEN   (128)
#define  SML_ATTESTATION_LEN     (SML_SHA_LEN+SML_RSA_LEN)
#define  SML_CU_NAME_SIZE         (16)/*Such as "OT998"...*/
#define  SML_CU_NAME_TRACE       (12)/*Use 12 temporily*/

#define  SML_CU_NAME_SIZE         (16)/*Such as "OT998"...*/
#define  SML_POLICY_SIZE (4)
#define  SML_CK_LEN_MAX (32)
#define  SML_CK_LEN (8)/*Current CK length*/
#define  SML_GID_LEN_MAX (2)
#define  SML_HRPD_LEN_MAX (4)
/*
SML Data:
sec_par_ver:        4 byte
sml_ver_str[]:       16 byte
struct_len:             4 byte, whole structure size
Group_num:           1 byte
tail 0x7F6E:            2 byte
Personalization data:Encrypted data
Attestation Variable: (MAC,signature)
*/
/*=============================================================================

                   ENUMERATED DATA DECLARATIONS

=============================================================================*/

/* ----------------------------------------------------------------------------
   ENUM:      SIMLOCK_PERSO_RESULT_ENUM_TYPE

   DESCRIPTION:
     This enum specifies the result of getting SIMLock buffer
-------------------------------------------------------------------------------*/

typedef enum {
  SIMLOCK_PERSO_SUCCESS,
  SIMLOCK_PERSO_GENERIC_ERROR,

  SIMLOCK_PERSO_RESERVED2,/*usless*/

  SIMLOCK_PERSO_INCORRECT_PARAMS,
  SIMLOCK_PERSO_MEMORY_EXHAUSTED_ERROR,

  /*usless*/
  SIMLOCK_PERSO_RESERVED5,
  SIMLOCK_PERSO_RESERVED6,
  SIMLOCK_PERSO_RESERVED7,
  SIMLOCK_PERSO_RESERVED8,
  SIMLOCK_PERSO_RESERVED9,

  SIMLOCK_PERSO_CHECK_TAIL_ERROR,
  SIMLOCK_PERSO_CHECK_TAG_ERROR,
  SIMLOCK_PERSO_CHECK_LENGTH_ERROR,
  SIMLOCK_PERSO_CHECK_VALUE_ERROR,
  SIMLOCK_PERSO_SHA_ERROR,
  SIMLOCK_PERSO_RSA_CONVERT_ERROR,
  SIMLOCK_PERSO_RSA_DEC_ERROR,
  SIMLOCK_PERSO_CHECK_SIGNATURE_ERROR,
  SIMLOCK_PERSO_CHECK_RSA_ERROR,
} simlock_perso_result_enum_type;

typedef enum {
    CATEGORY_3GPP_NW = 0xB0,
    CATEGORY_3GPP_NS,
    CATEGORY_3GPP_SP,
    CATEGORY_3GPP_CP,
    CATEGORY_3GPP_SIM,

    CATEGORY_3GPP2_NW1,
    CATEGORY_3GPP2_NW2,
    CATEGORY_3GPP2_HRPD,
    CATEGORY_3GPP2_SP,
    CATEGORY_3GPP2_CP,
    CATEGORY_3GPP2_RUIM,
    CATEGORY_END,
    CATEGORY_MAX = 0xBF
} simlock_perso_category_enum_type;

typedef enum {
    SIM_GROUP_1 = 0XF0,/*0xF0 ~0x0FF, Max num=16*/
    SIM_GROUP_2,
    SIM_GROUP_3,
    SIM_GROUP_4,
    /*
    Add other groups
    */
    SIM_GROUP_END,
    SIM_GROUP_MAX = 0xFF
} simlock_perso_group_enum_type;

typedef enum {
    SIM_SLOT_1 = 0XE0,
    SIM_SLOT_2,
    SIM_SLOT_3,
    SIM_SLOT_4,
    SIM_SLOT_END,
    SIM_SLOT_MAX = 0xEF
} simlock_perso_slot_enum_type;

typedef enum {
    SIM_LOCK_STATUS = 0XC0,/*The range is from 0xC0 to 0xDF*/
    SIM_UNLOCK_RETRIES,
    SIM_CKS,
    SIM_CAT_CODES,
    SIM_DATA_END,
    SIM_DATA_MAX = 0xDF
} simlock_perso_data_enum_type;

typedef enum {
  SML_STATUS_FREE,
  SML_STATUS_LOCK,
  SML_STATUS_AUTO,
  SML_STATUS_NUM ,
  SML_STATUS_ERROR
}simlock_perso_lockStatus_enum_type;

typedef struct
{
  uint8 tag;
  uint8 length[SML_DATA_SIZE_LEN];
  uint8 *data;
}
TLV_ST;
typedef struct
{
  uint8 tag;
  uint8 length[SML_DATA_SIZE_LEN_EXT];
  uint8 *data;
}
TLV_ST_EXT;

typedef TLV_ST_EXT simlock_perso_one_CK_data_type;
typedef TLV_ST_EXT  simlock_perso_one_code_data_type;//Tag:0xB0-0xBA//
typedef TLV_ST simlock_perso_lockStatus_data_type;/*Tag:0xC0*/
typedef TLV_ST simlock_perso_unlockRetry_data_type;/*Tag:0xC1*/
typedef struct
{
  simlock_perso_data_enum_type   eDataTag;/*0xC2*/
  uint8       length[SML_DATA_SIZE_LEN_EXT];
  simlock_perso_one_CK_data_type *CKData;
} simlock_perso_CKs_data_type;

typedef struct
{
  simlock_perso_data_enum_type   eDataTag;/*0xC3*/
  uint8       length[SML_DATA_SIZE_LEN_EXT];
  simlock_perso_one_code_data_type *oneCodesData;
} simlock_perso_codes_data_type;

typedef struct
{
  simlock_perso_slot_enum_type   eSlotTag;
  uint8 length[SML_DATA_SIZE_LEN_EXT];
  simlock_perso_lockStatus_data_type lockStatusData;
  simlock_perso_unlockRetry_data_type unlockRetryData;
  simlock_perso_CKs_data_type CKsData;
  simlock_perso_codes_data_type codesData[11];
} simlock_perso_sim_data_type;
typedef struct
{
  simlock_perso_group_enum_type eGroupTag;
  uint8              length[SML_DATA_SIZE_LEN_EXT];
  uint8              numOfCU[SML_NUM_LEN];
  uint8             *CUNames;/*  numOfCU*16  s*/
  simlock_slot_policy_enum_type policy[SML_POLICY_SIZE];/*use only 1 byte actually*/
  simlock_perso_sim_data_type   *simData;
} simlock_perso_group_data_type;

typedef  simlock_nw_code_data_type simlock_perso_3GPP_N_data_type;
typedef  simlock_ns_code_data_type simlock_perso_3GPP_NS_data_type;
typedef struct
{
  simlock_nw_code_data_type   _3GPPNcode;
  uint8        GID1_length[SML_DATA_SIZE_LEN];
  uint8        *GID1;
} simlock_perso_3GPP_SP_data_type;
typedef struct
{
  simlock_nw_code_data_type   _3GPPNcode;
  uint8          GID1_length[SML_DATA_SIZE_LEN];
  uint8          *GID1;
  uint8          GID2_length[SML_DATA_SIZE_LEN];
  uint8          *GID2;
} simlock_perso_3GPP_CP_data_type;
typedef struct
{
  simlock_nw_code_data_type   _3GPPNcode;
  uint8  Msin_length[SML_DATA_SIZE_LEN];
  uint8  Msin[SIMLOCK_MSIN_MAX];
} simlock_perso_3GPP_SIM_data_type;
typedef simlock_nw_code_data_type   simlock_perso_3GPP2_NW1_data_type;
typedef simlock_nw_type2_code_data_type  simlock_perso_3GPP2_NW2_data_type;
typedef struct
{
  uint8 hrpd[SML_HRPD_LEN_MAX];
} simlock_perso_3GPP2_HRPD_data_type;
typedef simlock_perso_3GPP_SP_data_type   simlock_perso_3GPP2_SP_data_type;
typedef simlock_perso_3GPP_CP_data_type   simlock_perso_3GPP2_CP_data_type;
typedef simlock_perso_3GPP_SIM_data_type simlock_perso_3GPP2_RUIM_data_type;
#if 0
typedef struct
{
    simlock_slot_enum_type       slot;
    simlock_category_enum_type  category_type;
    union {
    /* 3GPP */
    simlock_nw_code_list_type        nw_3gpp_code_list;
    simlock_ns_code_list_type        ns_3gpp_code_list;
    simlock_sp_code_list_type        sp_3gpp_code_list;
    simlock_cp_code_list_type        cp_3gpp_code_list;
    simlock_sim_code_list_type       sim_3gpp_code_list;
    /* 3GPP2 */
    simlock_nw_code_list_type        nw_type1_3gpp2_code_list;
    simlock_nw_type2_code_list_type  nw_type2_3gpp2_code_list;
    simlock_sim_code_list_type       ruim_3gpp2_code_list;
  } code_data;
}perso_category_data;
#endif
simlock_perso_result_enum_type simlock_perso_parse_and_get_persoData
(
    uint8 *pPersoBuffer,
    uint8 *ConfigDataFromPerso,
    uint8  * uintCategoryDataFromPerso,
    uint8* CUdataTrace
);
#endif /*SIMLOCK_PERSO_H*/
