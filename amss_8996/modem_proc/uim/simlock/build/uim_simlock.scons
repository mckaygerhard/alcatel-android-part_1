##===============================================================================
# UIM SIMLOCK Subsystem build script
# GENERAL DESCRIPTION
#    build script for modem/uim/simlock
#
# Copyright (c) 2014 - 2015 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //source/qcom/qct/modem/uim/simlock/build/main/latest/uim_simlock.scons#1
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 05/15/15   vv      Support for RSU set configuration in simlock
# 05/15/15   vv      Support for RSU in simlock
# 05/15/15   stv     SIMLOCK time related files 
# 11/26/14   av      Changes to make sure task opts for inclusion in FTM mode
# 11/10/14   vv      Add support for setting emergency mode for all the RATs
# 09/29/14   kr      removing compiler warnings
# 09/26/14   vv      Added support for the remote SFS
# 08/29/14   vv      Added support for temporary unlock
# 06/10/14   vv      Enable Simlock UT
# 06/03/14   vv      Added support for OTA unlock
# 05/21/14   vv      Increase the SIMLOCK stack size
# 05/12/14   vv      Enable SIMLOCK test framework
# 04/14/14   vv      Added support for identifying conflicting codes
# 02/25/14   tl      Initial version
#
#===============================================================================


#-----------------------------------------
# Import and clone the SCons environment
#-----------------------------------------

#[Feature]-Add-BEGIN by TCTSH.shenglong-fang,2015-11-16,Task-868360
import sys
import os
#[Feature]-Add-END by TCTSH.shenglong-fang.

Import('env')
env = env.Clone()

# Verify that USES_RUIM is set, otherwise bail out now
if 'USES_RUIM' not in env:
    Return()

#-----------------------------------------
# Setup source PATH
#-----------------------------------------
SRCPATH = "../src"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-----------------------------------------
# Set MSG_BT_SSID_DFLT for legacy MSG macros
#-----------------------------------------
env.Append(CPPDEFINES = [
   "MSG_BT_SSID_DFLT=MSG_SSID_RUIM",
])

#-----------------------------------------
# Necessary Public API's
#-----------------------------------------
CORE_APIS = [
    'DEBUGTOOLS',
    'SERVICES',
    'STORAGE',
    'SYSTEMDRIVERS',
    'DAL',
    'SECUREMSM',
    'MPROC',
    'KERNEL',
    ]

SIMLOCK_PUBLIC_APIS = [
    'UIM',
    'COMMON',
    'UIMRMT',
    ]

# We need the UIM "inc" dirs
env.RequireRestrictedApi(['UIM'])

#-----------------------------------------
# Generate the library and add to an image
#-----------------------------------------
SIMLOCK_C_SOURCES = [
    '${BUILDPATH}/simlock_modem_lib.c',
    '${BUILDPATH}/simlock_modem.c',
    '${BUILDPATH}/simlock.c',
    '${BUILDPATH}/simlocklib.c',
    '${BUILDPATH}/simlock_3gpp.c',
    '${BUILDPATH}/simlock_3gpp2.c',
    '${BUILDPATH}/simlock_category.c',
    '${BUILDPATH}/simlock_config.c',
    '${BUILDPATH}/simlock_crypto.c',
    '${BUILDPATH}/simlock_efs.c',
    '${BUILDPATH}/simlock_file.c',
    '${BUILDPATH}/simlock_sfs.c',
    '${BUILDPATH}/simlock_sfs_remote.c',
    '${BUILDPATH}/simlock_util.c',
    '${BUILDPATH}/simlock_category_conflicts.c',
    '${BUILDPATH}/simlock_modem_ota.c',
    '${BUILDPATH}/simlock_timer.c',
    '${BUILDPATH}/simlock_switch_sfs.c',
    '${BUILDPATH}/simlock_cm.c',
    '${BUILDPATH}/simlock_time.c',
    '${BUILDPATH}/simlock_time_nitz.c',
    '${BUILDPATH}/simlock_time_gps.c',
    '${BUILDPATH}/simlock_rsu.c',
    '${BUILDPATH}/simlock_rsu_key.c',
    '${BUILDPATH}/simlock_rsu_config.c',
    '${BUILDPATH}/simlock_temp_unlock.c',

    #[Feature]-Add-BEGIN by TCTSH.shenglong-fang,2015-11-16,Task-868360
    '${BUILDPATH}/jrd_trace.c',
    '${BUILDPATH}/simlock_load.c',
    #[Feature]-Add-END by TCTSH.shenglong-fang.
]

# Add our library to the MODEM_AMSS image
env.AddLibrary(['MODEM_MODEM','MOB_UIM'], '${BUILDPATH}/uim_simlock', SIMLOCK_C_SOURCES)


#[Feature]-Add-BEGIN by TCTSH.shenglong-fang,2015-11-16,Task-868360
uim_simlock_perso = File(env.subst("${BUILDPATH}/uim_simlock_perso${LIBSUFFIX}"))
env.AddLibsToImage( ['MODEM_MODEM'],[uim_simlock_perso])
#[Feature]-Add-END by TCTSH.shenglong-fang.

# Load test units
env.LoadSoftwareUnits()

# Build image for which this task belongs
RCINIT_SIMLOCK = ['MODEM_MODEM','MOB_UIM']

# RC Init Function Dictionary
RCINIT_INIT_SIMLOCK = {
            'sequence_group'      : 'RCINIT_GROUP_4',
            'init_name'           : 'simlock_task_init',
            'init_function'       : 'simlock_task_init',
            'dependencies'        : [],
            'policy_optin'        : ['default', 'ftm', ]
    }

# RC Init Task Dictionary
RCINIT_TASK_SIMLOCK= {
            'thread_name'         : 'simlock',
            'sequence_group'      : 'RCINIT_GROUP_4',
            'stack_size_bytes'    : '4096',
            'priority_amss_order' : 'GSDI_PRI_ORDER',
            'stack_name'          : 'simlock_stack',
            'thread_entry'        : 'simlock_main',
            'tcb_name'            : 'simlock_tcb',
            'cpu_affinity'        : env.subst('$MODEM_CPU_AFFINITY'),
            'policy_optin'        : ['default', 'ftm', ]
    }

# Add init function to RCInit
if 'USES_MODEM_RCINIT' in env:
  env.AddRCInitTask(RCINIT_SIMLOCK, RCINIT_TASK_SIMLOCK)
  env.AddRCInitFunc(RCINIT_SIMLOCK, RCINIT_INIT_SIMLOCK)
