#ifndef RF_CDMA_AUTO_PIN_H
#define RF_CDMA_AUTO_PIN_H
/*!
  @file
  rf_cdma_auto_pin.h

  @brief
  Provides CDMA auto pin Compensation functionality

  @details

*/

/*===========================================================================

Copyright (c) 2008 - 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$DateTime: 2016/03/28 23:06:37 $ $Author: mplcsds1 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rftech_cdma/api/rf_cdma_auto_pin.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/31/15   wwl     Add autopin c2k state machine support functions
06/26/15   wwl     Initial version.


============================================================================*/
#include "comdef.h"
#include "rfcom.h"
#include "rfcommon_time_profile.h"
#include "rfcommon_locks.h"
#include "rf_dispatch.h"

/*----------------------------------------------------------------------------*/
void
rf_cdma_auto_pin_tx_wakeup
(
  rfm_device_enum_type tx_dev,
  rfm_mode_enum_type rfm_tech
);

/*----------------------------------------------------------------------------*/
void
rf_cdma_auto_pin_tx_sleep
(
  rfm_device_enum_type tx_dev,
  rfm_mode_enum_type rfm_tech
);

/*----------------------------------------------------------------------------*/
void
rf_cdma_auto_pin_update_handler
(
  void* cmd_ptr,
  rf_dispatch_cid_info_type *cid_info,
  void *cb_data
);

/*----------------------------------------------------------------------------*/
boolean
rf_cdma_auto_pin_data_valid
(
  rfm_device_enum_type tx_dev
);
/*----------------------------------------------------------------------------*/
boolean
rf_cdma_auto_pin_tx_state
(
  rfm_device_enum_type tx_dev
);

/*----------------------------------------------------------------------------*/
void
rf_cdma_auto_pin_cmd_proc_init
(
  void
);
#endif /* RF_CDMA_AUTO_PIN_H */
