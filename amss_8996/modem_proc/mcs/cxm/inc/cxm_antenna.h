#ifndef CXM_ANTENNA_H
#define CXM_ANTENNA_H
/*!
  @file
  cxm_antenna.h

  @brief
  This file contains the message handlers and algorithms for sharing
  the diversity antenna with WLAN using WCI-2 Control Plane msging over QMB

*/

/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/cxm/inc/cxm_antenna.h#2 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
11/17/13   btl     Initial version

==========================================================================*/

/*=============================================================================

                      INCLUDES

=============================================================================*/
#include "coexistence_service_v02.h"
#include "trm.h"
#include "qurt_mutex.h"

/*===========================================================================

                       DATA TYPES AND MACROS

===========================================================================*/

/*! @brief Listing of logical states software uses for antenna decisions */
typedef enum
{
  /* antenna sharing not enabled */
  CXM_ANTENNA_UNUSED,

  /* TRM request & notify in force, awaiting grant */
  CXM_ANTENNA_PENDING,

  /* antenna released for short-duration IRAT measurement */
  CXM_ANTENNA_IRAT,

  /* antenna granted to WLAN */
  CXM_ANTENNA_GRANTED,

  /* Waiting for WLAN to clean up */
  CXM_ANTENNA_CLEANING,

  /* Keeping UART on just in case, but WLAN does not have antenna */
  CXM_ANTENNA_GUARDING
} cxm_antenna_state_e;

/*! @brief Antenna Sharing data storage type */
typedef struct
{
  /* indicates current state in antenna sharing cycle */
  cxm_antenna_state_e       state;

  /* QMB message contents to be sent to WLAN */
  coex_antenna_state_v02   *info;

  /* QMB state contents to be sent to WLAN in sync req/resp */
  coex_tech_sync_state_v02 *sync;

  /* QMB token wrap -- not currently used */
  boolean                  *token_wrap;

  /* Duration WLAN can use the antenna for */
  trm_duration_t            grant_dur;

  /* Duration of IRAT measurement, for which WLAN will give up the antenna */
  trm_duration_t            irat_dur;

  /* Timestamp of grant start */
  uint64                    grant_start_ts;

  /* Timestamp of measurement gap start */
  uint64                    irat_start_ts;

  /* Period to set guard timer for, in ticks */
  uint32                    guard_period;

  /* timer for releasing on time */
  timer_type                grant_timer;

  /* timer for guarding against WLAN using antenna too early */
  timer_type                guard_timer;
} cxm_antenna_state_s;

/*=============================================================================

                      EXTERNAL FUNCTION DECLARATIONS

=============================================================================*/

/*=============================================================================

  FUNCTION:  cxm_get_antenna_state_ptr

=============================================================================*/
/*!
    @brief
    Returns a pointer to the global cxm_antenna_state struct for diag logging.

    @return
    cxm_antenna_state_s *
*/
/*===========================================================================*/
cxm_antenna_state_s *cxm_get_antenna_state_ptr( void );

/*=============================================================================

  FUNCTION:  cxm_antenna_init

=============================================================================*/
/*!
    @brief
    Initialize resources and start diversity antenna resource request

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_init
(
  coex_tech_sync_state_v02 *sync_state_p,
  coex_antenna_state_v02   *state_info_p,
  boolean                  *token_wrap_flag_p
);

/*=============================================================================

  FUNCTION:  cxm_antenna_deinit

=============================================================================*/
/*!
    @brief
    Release resources associated with WWAN/WLAN antenna sharing

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_deinit( void );

/*=============================================================================

  FUNCTION:  cxm_antenna_handle_grant

=============================================================================*/
/*!
    @brief
    We have been granted the WLAN antenna; execute appropriate actions and
    send message to WLAN

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_handle_grant( void );

/*=============================================================================

  FUNCTION:  cxm_antenna_handle_release

=============================================================================*/
/*!
    @brief
    We have been asked us to release Antenna. Can be caused two ways:
      1. TRM asked us to release through callback
      2. Our grant duration is up and our timer fired

    @detail
    First try to re-acquire antenna. If that fails, submit a new request
    and notify WLAN that we lost it.

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_handle_release( boolean cleanup_needed );

/*=============================================================================

  FUNCTION:  cxm_antenna_update_duration

=============================================================================*/
/*!
    @brief
    Update the grant duration saved in the QMB antenna state struct, based
    on the original grant duration and how much time has passed since grant

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_update_duration( void );

/*=============================================================================

  FUNCTION:  cxm_antenna_diag_send_state_ind

=============================================================================*/
/*!
    @brief
    Update state and send the message over QMB, meant to be used
    for simulating antenna grants/revokes through CXM Diag.

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_diag_send_state_ind( 
  boolean granted, 
  uint16  grant_dur 
);

/*=============================================================================

  FUNCTION:  cxm_antenna_handle_guard_timer

=============================================================================*/
/*!
    @brief
    Guard period has expired, move into next state

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_handle_guard_timer( void );

#endif /* CXM_ANTENNA_H */
