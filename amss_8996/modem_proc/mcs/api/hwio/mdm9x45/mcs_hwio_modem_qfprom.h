#ifndef __MCS_HWIO_MODEM_QFPROM_H__
#define __MCS_HWIO_MODEM_QFPROM_H__
/*
===========================================================================
*/
/**
  @file mcs_hwio_modem_qfprom.h
  @brief Auto-generated HWIO interface include file based on tesla_v1.0_p3q3r19.6_MTO.

  This file contains HWIO register definitions for the following modules:
    SECURITY_CONTROL_CORE

  'Include' filters applied: QFPROM[SECURITY_CONTROL_CORE] 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/api/hwio/mdm9x45/mcs_hwio_modem_qfprom.h#1 $
  $DateTime: 2016/03/28 23:03:58 $
  $Author: mplcsds1 $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: SECURITY_CONTROL_CORE
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_CORE_REG_BASE                                                         (SECURITY_CONTROL_BASE      + 0x00000000)
#define SECURITY_CONTROL_CORE_REG_BASE_PHYS                                                    (SECURITY_CONTROL_BASE_PHYS + 0x00000000)

#define HWIO_QFPROM_RAW_JTAG_ID_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000000)
#define HWIO_QFPROM_RAW_JTAG_ID_PHYS                                                           (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000000)
#define HWIO_QFPROM_RAW_JTAG_ID_RMSK                                                           0xffffffff
#define HWIO_QFPROM_RAW_JTAG_ID_POR                                                            0x00000000
#define HWIO_QFPROM_RAW_JTAG_ID_POR_RMSK                                                       0x00000000
#define HWIO_QFPROM_RAW_JTAG_ID_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_JTAG_ID_ADDR, HWIO_QFPROM_RAW_JTAG_ID_RMSK)
#define HWIO_QFPROM_RAW_JTAG_ID_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_JTAG_ID_ADDR, m)
#define HWIO_QFPROM_RAW_JTAG_ID_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_JTAG_ID_ADDR,v)
#define HWIO_QFPROM_RAW_JTAG_ID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_JTAG_ID_ADDR,m,v,HWIO_QFPROM_RAW_JTAG_ID_IN)
#define HWIO_QFPROM_RAW_JTAG_ID_IDDQ_CX_ON_BMSK                                                0xc0000000
#define HWIO_QFPROM_RAW_JTAG_ID_IDDQ_CX_ON_SHFT                                                      0x1e
#define HWIO_QFPROM_RAW_JTAG_ID_IDDQ_RC_BMSK                                                   0x20000000
#define HWIO_QFPROM_RAW_JTAG_ID_IDDQ_RC_SHFT                                                         0x1d
#define HWIO_QFPROM_RAW_JTAG_ID_MACCHIATO_EN_BMSK                                              0x10000000
#define HWIO_QFPROM_RAW_JTAG_ID_MACCHIATO_EN_SHFT                                                    0x1c
#define HWIO_QFPROM_RAW_JTAG_ID_FEATURE_ID_BMSK                                                 0xff00000
#define HWIO_QFPROM_RAW_JTAG_ID_FEATURE_ID_SHFT                                                      0x14
#define HWIO_QFPROM_RAW_JTAG_ID_JTAG_ID_BMSK                                                      0xfffff
#define HWIO_QFPROM_RAW_JTAG_ID_JTAG_ID_SHFT                                                          0x0

#define HWIO_QFPROM_RAW_PTE1_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000004)
#define HWIO_QFPROM_RAW_PTE1_PHYS                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000004)
#define HWIO_QFPROM_RAW_PTE1_RMSK                                                              0xffffffff
#define HWIO_QFPROM_RAW_PTE1_POR                                                               0x00000000
#define HWIO_QFPROM_RAW_PTE1_POR_RMSK                                                          0x00000000
#define HWIO_QFPROM_RAW_PTE1_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE1_ADDR, HWIO_QFPROM_RAW_PTE1_RMSK)
#define HWIO_QFPROM_RAW_PTE1_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE1_ADDR, m)
#define HWIO_QFPROM_RAW_PTE1_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE1_ADDR,v)
#define HWIO_QFPROM_RAW_PTE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE1_ADDR,m,v,HWIO_QFPROM_RAW_PTE1_IN)
#define HWIO_QFPROM_RAW_PTE1_IDDQ_BIN_BMSK                                                     0xe0000000
#define HWIO_QFPROM_RAW_PTE1_IDDQ_BIN_SHFT                                                           0x1d
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MULTIPLIER_BMSK                                              0x1c000000
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MULTIPLIER_SHFT                                                    0x1a
#define HWIO_QFPROM_RAW_PTE1_PROCESS_NODE_ID_BMSK                                               0x2000000
#define HWIO_QFPROM_RAW_PTE1_PROCESS_NODE_ID_SHFT                                                    0x19
#define HWIO_QFPROM_RAW_PTE1_PROCESS_NODE_ID_TN1_FVAL                                                 0x0
#define HWIO_QFPROM_RAW_PTE1_PROCESS_NODE_ID_TN3_FVAL                                                 0x1
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MX_OFF_BMSK                                                   0x1f80000
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MX_OFF_SHFT                                                        0x13
#define HWIO_QFPROM_RAW_PTE1_IDDQ_CX_OFF_BMSK                                                     0x7f000
#define HWIO_QFPROM_RAW_PTE1_IDDQ_CX_OFF_SHFT                                                         0xc
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MX_ON_BMSK                                                        0xfe0
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MX_ON_SHFT                                                          0x5
#define HWIO_QFPROM_RAW_PTE1_IDDQ_CX_ON_BMSK                                                         0x1f
#define HWIO_QFPROM_RAW_PTE1_IDDQ_CX_ON_SHFT                                                          0x0

#define HWIO_QFPROM_RAW_SERIAL_NUM_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000008)
#define HWIO_QFPROM_RAW_SERIAL_NUM_PHYS                                                        (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000008)
#define HWIO_QFPROM_RAW_SERIAL_NUM_RMSK                                                        0xffffffff
#define HWIO_QFPROM_RAW_SERIAL_NUM_POR                                                         0x00000000
#define HWIO_QFPROM_RAW_SERIAL_NUM_POR_RMSK                                                    0x00000000
#define HWIO_QFPROM_RAW_SERIAL_NUM_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR, HWIO_QFPROM_RAW_SERIAL_NUM_RMSK)
#define HWIO_QFPROM_RAW_SERIAL_NUM_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR, m)
#define HWIO_QFPROM_RAW_SERIAL_NUM_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR,v)
#define HWIO_QFPROM_RAW_SERIAL_NUM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR,m,v,HWIO_QFPROM_RAW_SERIAL_NUM_IN)
#define HWIO_QFPROM_RAW_SERIAL_NUM_SERIAL_NUM_BMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_SERIAL_NUM_SERIAL_NUM_SHFT                                                    0x0

#define HWIO_QFPROM_RAW_PTE2_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000000c)
#define HWIO_QFPROM_RAW_PTE2_PHYS                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000000c)
#define HWIO_QFPROM_RAW_PTE2_RMSK                                                              0xffffffff
#define HWIO_QFPROM_RAW_PTE2_POR                                                               0x00000000
#define HWIO_QFPROM_RAW_PTE2_POR_RMSK                                                          0x00000000
#define HWIO_QFPROM_RAW_PTE2_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE2_ADDR, HWIO_QFPROM_RAW_PTE2_RMSK)
#define HWIO_QFPROM_RAW_PTE2_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE2_ADDR, m)
#define HWIO_QFPROM_RAW_PTE2_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE2_ADDR,v)
#define HWIO_QFPROM_RAW_PTE2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE2_ADDR,m,v,HWIO_QFPROM_RAW_PTE2_IN)
#define HWIO_QFPROM_RAW_PTE2_PTE_IDDQ_SPARE_BMSK                                               0xfc000000
#define HWIO_QFPROM_RAW_PTE2_PTE_IDDQ_SPARE_SHFT                                                     0x1a
#define HWIO_QFPROM_RAW_PTE2_IDDQ_MSS_OFF_BMSK                                                  0x3f80000
#define HWIO_QFPROM_RAW_PTE2_IDDQ_MSS_OFF_SHFT                                                       0x13
#define HWIO_QFPROM_RAW_PTE2_IDDQ_MSS_ON_BMSK                                                     0x7f800
#define HWIO_QFPROM_RAW_PTE2_IDDQ_MSS_ON_SHFT                                                         0xb
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_BMSK                                                        0x700
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_SHFT                                                          0x8
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_TSMC_FVAL                                                     0x0
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_GLOBAL_FOUNDRY_FVAL                                           0x1
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_SAMSUNG_FVAL                                                  0x2
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_IBM_FVAL                                                      0x3
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_UMC_FVAL                                                      0x4
#define HWIO_QFPROM_RAW_PTE2_LOGIC_RETENTION_BMSK                                                    0xe0
#define HWIO_QFPROM_RAW_PTE2_LOGIC_RETENTION_SHFT                                                     0x5
#define HWIO_QFPROM_RAW_PTE2_SPEED_BIN_BMSK                                                          0x1c
#define HWIO_QFPROM_RAW_PTE2_SPEED_BIN_SHFT                                                           0x2
#define HWIO_QFPROM_RAW_PTE2_MX_RET_BIN_BMSK                                                          0x3
#define HWIO_QFPROM_RAW_PTE2_MX_RET_BIN_SHFT                                                          0x0

#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000010)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000010)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR, HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RMSK)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR,m,v,HWIO_QFPROM_RAW_RD_WR_PERM_LSB_IN)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE_BMSK                                              0xffc00000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE_SHFT                                                    0x16
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE21_BMSK                                              0x200000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE21_SHFT                                                  0x15
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE21_ALLOW_READ_FVAL                                        0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE21_DISABLE_READ_FVAL                                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE20_BMSK                                              0x100000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE20_SHFT                                                  0x14
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE20_ALLOW_READ_FVAL                                        0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE20_DISABLE_READ_FVAL                                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE19_BMSK                                               0x80000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE19_SHFT                                                  0x13
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE19_ALLOW_READ_FVAL                                        0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE19_DISABLE_READ_FVAL                                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE18_BMSK                                               0x40000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE18_SHFT                                                  0x12
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE18_ALLOW_READ_FVAL                                        0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE18_DISABLE_READ_FVAL                                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE17_BMSK                                               0x20000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE17_SHFT                                                  0x11
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE17_ALLOW_READ_FVAL                                        0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE17_DISABLE_READ_FVAL                                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE16_BMSK                                               0x10000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE16_SHFT                                                  0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE16_ALLOW_READ_FVAL                                        0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE16_DISABLE_READ_FVAL                                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEC_EN_BMSK                                                 0x8000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEC_EN_SHFT                                                    0xf
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEC_EN_ALLOW_READ_FVAL                                         0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEC_EN_DISABLE_READ_FVAL                                       0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_BOOT_ROM_PATCH_BMSK                                         0x4000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_BOOT_ROM_PATCH_SHFT                                            0xe
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_BOOT_ROM_PATCH_ALLOW_READ_FVAL                                 0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_BOOT_ROM_PATCH_DISABLE_READ_FVAL                               0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_MEM_CONFIG_BMSK                                             0x2000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_MEM_CONFIG_SHFT                                                0xd
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_MEM_CONFIG_ALLOW_READ_FVAL                                     0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_MEM_CONFIG_DISABLE_READ_FVAL                                   0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CALIB_BMSK                                                  0x1000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CALIB_SHFT                                                     0xc
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CALIB_ALLOW_READ_FVAL                                          0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CALIB_DISABLE_READ_FVAL                                        0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PK_HASH_BMSK                                                 0x800
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PK_HASH_SHFT                                                   0xb
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PK_HASH_ALLOW_READ_FVAL                                        0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PK_HASH_DISABLE_READ_FVAL                                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_QC_SEC_BOOT_BMSK                                             0x400
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_QC_SEC_BOOT_SHFT                                               0xa
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_QC_SEC_BOOT_ALLOW_READ_FVAL                                    0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_QC_SEC_BOOT_DISABLE_READ_FVAL                                  0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_SEC_BOOT_BMSK                                            0x200
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_SEC_BOOT_SHFT                                              0x9
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_SEC_BOOT_ALLOW_READ_FVAL                                   0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_SEC_BOOT_DISABLE_READ_FVAL                                 0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_BMSK                                  0x100
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_SHFT                                    0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_ALLOW_READ_FVAL                         0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_DISABLE_READ_FVAL                       0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_BMSK                                   0x80
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_SHFT                                    0x7
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_ALLOW_READ_FVAL                         0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_DISABLE_READ_FVAL                       0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEAT_CONFIG_BMSK                                              0x40
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEAT_CONFIG_SHFT                                               0x6
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEAT_CONFIG_ALLOW_READ_FVAL                                    0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEAT_CONFIG_DISABLE_READ_FVAL                                  0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_CONFIG_BMSK                                               0x20
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_CONFIG_SHFT                                                0x5
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_CONFIG_ALLOW_READ_FVAL                                     0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_CONFIG_DISABLE_READ_FVAL                                   0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_BMSK                                          0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_SHFT                                           0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_ALLOW_READ_FVAL                                0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_DISABLE_READ_FVAL                              0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_BMSK                                           0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_SHFT                                           0x3
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_ALLOW_READ_FVAL                                0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_DISABLE_READ_FVAL                              0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_BMSK                                           0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_SHFT                                           0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_ALLOW_READ_FVAL                                0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_DISABLE_READ_FVAL                              0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RD_WR_PERM_BMSK                                                0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RD_WR_PERM_SHFT                                                0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RD_WR_PERM_ALLOW_READ_FVAL                                     0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RD_WR_PERM_DISABLE_READ_FVAL                                   0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PTE_BMSK                                                       0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PTE_SHFT                                                       0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PTE_ALLOW_READ_FVAL                                            0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PTE_DISABLE_READ_FVAL                                          0x1

#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000014)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000014)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR, HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RMSK)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR,m,v,HWIO_QFPROM_RAW_RD_WR_PERM_MSB_IN)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE_BMSK                                              0xffc00000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE_SHFT                                                    0x16
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE21_BMSK                                              0x200000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE21_SHFT                                                  0x15
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE21_ALLOW_WRITE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE21_DISABLE_WRITE_FVAL                                     0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE20_BMSK                                              0x100000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE20_SHFT                                                  0x14
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE20_ALLOW_WRITE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE20_DISABLE_WRITE_FVAL                                     0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE19_BMSK                                               0x80000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE19_SHFT                                                  0x13
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE19_ALLOW_WRITE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE19_DISABLE_WRITE_FVAL                                     0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE18_BMSK                                               0x40000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE18_SHFT                                                  0x12
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE18_ALLOW_WRITE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE18_DISABLE_WRITE_FVAL                                     0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE17_BMSK                                               0x20000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE17_SHFT                                                  0x11
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE17_ALLOW_WRITE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE17_DISABLE_WRITE_FVAL                                     0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE16_BMSK                                               0x10000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE16_SHFT                                                  0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE16_ALLOW_WRITE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE16_DISABLE_WRITE_FVAL                                     0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEC_EN_BMSK                                                 0x8000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEC_EN_SHFT                                                    0xf
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEC_EN_ALLOW_WRITE_FVAL                                        0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEC_EN_DISABLE_WRITE_FVAL                                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_BOOT_ROM_PATCH_BMSK                                         0x4000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_BOOT_ROM_PATCH_SHFT                                            0xe
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_BOOT_ROM_PATCH_ALLOW_WRITE_FVAL                                0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_BOOT_ROM_PATCH_DISABLE_WRITE_FVAL                              0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_MEM_CONFIG_BMSK                                             0x2000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_MEM_CONFIG_SHFT                                                0xd
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_MEM_CONFIG_ALLOW_WRITE_FVAL                                    0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_MEM_CONFIG_DISABLE_WRITE_FVAL                                  0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CALIB_BMSK                                                  0x1000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CALIB_SHFT                                                     0xc
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CALIB_ALLOW_WRITE_FVAL                                         0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CALIB_DISABLE_WRITE_FVAL                                       0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PK_HASH_BMSK                                                 0x800
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PK_HASH_SHFT                                                   0xb
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PK_HASH_ALLOW_WRITE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PK_HASH_DISABLE_WRITE_FVAL                                     0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_QC_SEC_BOOT_BMSK                                             0x400
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_QC_SEC_BOOT_SHFT                                               0xa
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_QC_SEC_BOOT_ALLOW_WRITE_FVAL                                   0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_QC_SEC_BOOT_DISABLE_WRITE_FVAL                                 0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_SEC_BOOT_BMSK                                            0x200
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_SEC_BOOT_SHFT                                              0x9
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_SEC_BOOT_ALLOW_WRITE_FVAL                                  0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_SEC_BOOT_DISABLE_WRITE_FVAL                                0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_BMSK                                  0x100
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_SHFT                                    0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_ALLOW_WRITE_FVAL                        0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_DISABLE_WRITE_FVAL                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_BMSK                                   0x80
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_SHFT                                    0x7
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_ALLOW_WRITE_FVAL                        0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_DISABLE_WRITE_FVAL                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEAT_CONFIG_BMSK                                              0x40
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEAT_CONFIG_SHFT                                               0x6
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEAT_CONFIG_ALLOW_WRITE_FVAL                                   0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEAT_CONFIG_DISABLE_WRITE_FVAL                                 0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_CONFIG_BMSK                                               0x20
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_CONFIG_SHFT                                                0x5
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_CONFIG_ALLOW_WRITE_FVAL                                    0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_CONFIG_DISABLE_WRITE_FVAL                                  0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_BMSK                                          0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_SHFT                                           0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_ALLOW_WRITE_FVAL                               0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_DISABLE_WRITE_FVAL                             0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_BMSK                                           0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_SHFT                                           0x3
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_ALLOW_WRITE_FVAL                               0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_DISABLE_WRITE_FVAL                             0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_BMSK                                           0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_SHFT                                           0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_ALLOW_WRITE_FVAL                               0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_DISABLE_WRITE_FVAL                             0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RD_WR_PERM_BMSK                                                0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RD_WR_PERM_SHFT                                                0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RD_WR_PERM_ALLOW_WRITE_FVAL                                    0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RD_WR_PERM_DISABLE_WRITE_FVAL                                  0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PTE_BMSK                                                       0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PTE_SHFT                                                       0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PTE_ALLOW_WRITE_FVAL                                           0x0
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PTE_DISABLE_WRITE_FVAL                                         0x1

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000018)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000018)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_BMSK                                0xfc000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_SHFT                                      0x1a
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_TZ_BMSK                                             0x3fff000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_TZ_SHFT                                                   0xc
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_SBL1_BMSK                                               0xffe
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_SBL1_SHFT                                                 0x1
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_BMSK                                 0x1
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_SHFT                                 0x0
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_RPMB_KEY_NOT_PROVISIONED_FVAL        0x0
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_RPMB_KEY_PROVISIONED_FVAL            0x1

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000001c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000001c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_APPSBL0_BMSK                                       0xfffc0000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_APPSBL0_SHFT                                             0x12
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_BMSK                                   0x3ffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_SHFT                                       0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000020)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000020)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_APPSBL1_BMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_APPSBL1_SHFT                                              0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000024)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000024)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_BMSK                       0xff000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_SHFT                             0x18
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_PRODUCTION_DEVICE_NO_CERTIFICATE_SELECTED_FVAL        0x0
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_15_FVAL        0xf
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_14_FVAL       0x1e
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_13_FVAL       0x2d
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_12_FVAL       0x3c
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_11_FVAL       0x4b
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_10_FVAL       0x5a
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_9_FVAL       0x69
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_8_FVAL       0x78
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_7_FVAL       0x87
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_6_FVAL       0x96
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_5_FVAL       0xa5
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_4_FVAL       0xb4
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_3_FVAL       0xc3
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_2_FVAL       0xd2
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_1_FVAL       0xe1
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_PRODUCTION_DEVICE_FIXED_TO_CERTIFICATE_0_FVAL       0xf0
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_HYPERVISOR_BMSK                                      0xfff000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_HYPERVISOR_SHFT                                           0xc
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RPM_BMSK                                                0xff0
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RPM_SHFT                                                  0x4
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_APPSBL2_BMSK                                              0xf
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_APPSBL2_SHFT                                              0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000028)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000028)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MSS_BMSK                                           0xffff0000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MSS_SHFT                                                 0x10
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MBA_BMSK                                               0xffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MBA_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000002c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000002c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_SPARE0_BMSK                                        0xffffff00
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_SPARE0_SHFT                                               0x8
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_BMSK                       0xff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_SHFT                        0x0
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_PRODUCTION_DEVICE_NO_CERTIFICATE_SELECTED_FVAL        0x0
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_15_FVAL        0xf
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_14_FVAL       0x1e
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_13_FVAL       0x2d
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_12_FVAL       0x3c
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_11_FVAL       0x4b
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_10_FVAL       0x5a
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_9_FVAL       0x69
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_8_FVAL       0x78
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_7_FVAL       0x87
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_6_FVAL       0x96
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_5_FVAL       0xa5
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_4_FVAL       0xb4
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_3_FVAL       0xc3
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_2_FVAL       0xd2
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_1_FVAL       0xe1
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_PRODUCTION_DEVICE_FIXED_TO_CERTIFICATE_0_FVAL       0xf0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000030)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000030)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE0_BMSK                                        0xff000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE0_SHFT                                              0x18
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_BMSK                              0x800000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_SHFT                                  0x17
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_NOT_SECURE_FVAL                        0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_SECURE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_BMSK                                 0x400000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_SHFT                                     0x16
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_BMSK                             0x3c0000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_SHFT                                 0x12
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_BMSK                               0x20000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_SHFT                                  0x11
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_NOT_SECURE_FVAL                        0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_SECURE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_BMSK                               0x10000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_SHFT                                  0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_NOT_SECURE_FVAL                        0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_SECURE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_BMSK                                0x8000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_SHFT                                   0xf
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_NOT_SECURE_FVAL                        0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_SECURE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_BMSK                                0x4000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_SHFT                                   0xe
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_NOT_SECURE_FVAL                        0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_SECURE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_BMSK                                0x2000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_SHFT                                   0xd
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_NOT_SECURE_FVAL                        0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_SECURE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_WDOG_EN_BMSK                                           0x1000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_WDOG_EN_SHFT                                              0xc
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_WDOG_EN_USE_GPIO_FVAL                                     0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_WDOG_EN_IGNORE_GPIO_ENABLE_WDOG_FVAL                      0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_BMSK                                   0x800
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_SHFT                                     0xb
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_NORMAL_MODE_FVAL                         0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_SECURE_MODE_FVAL                         0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_BMSK                               0x400
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_SHFT                                 0xa
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_BOOT_FROM_SD_CARD_CONNECTED_ON_SDC2_PORT_FVAL        0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_BOOT_FROM_SD_CARD_CONNECTED_ON_SDC3_PORT_FVAL        0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_BMSK                      0x200
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_SHFT                        0x9
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_BMSK                                0x100
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_SHFT                                  0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_BMSK                                           0xe0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_SHFT                                            0x5
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_DEFAULT_FVAL                                    0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_PCIE_FVAL                                       0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_USB_FVAL                                        0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_EMMC_USB_FVAL                                   0x3
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_BMSK                                 0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_SHFT                                  0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_ENUM_19_2_MHZ_FVAL                    0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_ENUM_25_MHZ_FVAL                      0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_BMSK                                  0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_SHFT                                  0x3
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_USE_FORCE_USB_BOOT_GPIO_TO_FORCE_BOOT_FROM_USB_FVAL        0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_NOT_USE_FORCE_USB_BOOT_PIN_FVAL        0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_BMSK                                                0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_SHFT                                                0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_BMSK                                         0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_SHFT                                         0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_TIMEOUT_DISABLED_FVAL                        0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_TIMEOUT_ENABLED_90S_FVAL                     0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_BMSK                                      0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_SHFT                                      0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_DOWNLOADER_ENABLED_FVAL                   0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_DOWNLOADER_DISABLED_FVAL                  0x1

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000034)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000034)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_BMSK                                        0xff800000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_SHFT                                              0x17
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_BMSK                        0x780000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_SHFT                            0x13
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_BMSK                             0x40000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_SHFT                                0x12
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_ENABLE_FVAL                          0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_DISABLE_FVAL                         0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_BMSK                              0x20000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_SHFT                                 0x11
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_ENABLE_FVAL                           0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_DISABLE_FVAL                          0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_BMSK                               0x10000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_SHFT                                  0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_DISABLE_FVAL                           0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_BMSK                                 0x8000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_SHFT                                    0xf
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_ENABLE_FVAL                             0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_DISABLE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_BMSK                                 0x4000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_SHFT                                    0xe
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_ENABLE_FVAL                             0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_DISABLE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_BMSK                              0x2000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_SHFT                                 0xd
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_ENABLE_FVAL                          0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_DISABLE_FVAL                         0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_BMSK                               0x1000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_SHFT                                  0xc
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_ENABLE_FVAL                           0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_DISABLE_FVAL                          0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_BMSK                                 0x800
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_SHFT                                   0xb
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_DISABLE_FVAL                           0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_BMSK                                 0x400
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_SHFT                                   0xa
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_DISABLE_FVAL                           0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_BMSK                                     0x200
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_SHFT                                       0x9
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_BMSK                                     0x100
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_SHFT                                       0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_BMSK                               0x80
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_SHFT                                0x7
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_ENABLE_FVAL                         0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_DISABLE_FVAL                        0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_BMSK                                   0x40
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_SHFT                                    0x6
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_ENABLE_FVAL                             0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_DISABLE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_BMSK                              0x20
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_SHFT                               0x5
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_BMSK                                   0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_SHFT                                    0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_ENABLE_FVAL                             0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_DISABLE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_BMSK                                   0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_SHFT                                   0x3
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_DISABLE_FVAL                           0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_BMSK                                    0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_SHFT                                    0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_ENABLE_FVAL                             0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_DISABLE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_BMSK                                    0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_SHFT                                    0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_ENABLE_FVAL                             0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_DISABLE_FVAL                            0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_BMSK                                    0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_SHFT                                    0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_ENABLE_FVAL                             0x0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_DISABLE_FVAL                            0x1

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000038)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000038)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_BMSK                                0xffff0000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_SHFT                                      0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_BMSK                                         0xffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_SHFT                                            0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000003c)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000003c)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_VID_BMSK                                    0xffff0000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_VID_SHFT                                          0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_PID_BMSK                                        0xffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_PID_SHFT                                           0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000040)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000040)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_VU_EN_BMSK                             0xfc000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_VU_EN_SHFT                                   0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_NAV_EN_BMSK                                   0x2000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_NAV_EN_SHFT                                        0x19
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_LDO_EN_BMSK                                   0x1000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_LDO_EN_SHFT                                        0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_ECS_EN_BMSK                                    0x800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_ECS_EN_SHFT                                        0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_WCDMA_EN_BMSK                            0x600000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_WCDMA_EN_SHFT                                0x15
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_TDSCDMA_EN_BMSK                          0x180000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_TDSCDMA_EN_SHFT                              0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_EN_BMSK                               0x60000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_EN_SHFT                                  0x11
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT2_EN_BMSK                    0x18000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT2_EN_SHFT                        0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT1_EN_BMSK                     0x6000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT1_EN_SHFT                        0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_MIMO_EN_BMSK                          0x1800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_MIMO_EN_SHFT                             0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_EN_BMSK                                0x600
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_EN_SHFT                                  0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_DC_EN_BMSK                             0x180
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_DC_EN_SHFT                               0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_GENRAN_EN_BMSK                               0x60
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_GENRAN_EN_SHFT                                0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_EN_BMSK                                      0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_EN_SHFT                                       0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_DO_EN_BMSK                                    0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_DO_EN_SHFT                                    0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_1X_EN_BMSK                                    0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_1X_EN_SHFT                                    0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000044)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000044)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_BMSK                  0x80000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_SHFT                        0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE_BMSK                                        0x7ff00000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE_SHFT                                              0x14
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_MDSP_FW_EN_BMSK                                 0xfff00
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_MDSP_FW_EN_SHFT                                     0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_SPARE_BMSK                                         0xc0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_SPARE_SHFT                                          0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_VU_EN_BMSK                                   0x3f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_VU_EN_SHFT                                    0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000048)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000048)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_BMSK                                  0x80000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_SHFT                                        0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_ENABLE_FVAL                                  0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_DISABLE_FVAL                                 0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SHADOW_READ_DISABLE_BMSK                          0x40000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SHADOW_READ_DISABLE_SHFT                                0x1e
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SPARE1_BMSK                                       0x30000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SPARE1_SHFT                                             0x1c
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_SMT_PERM_ENABLE_BMSK                          0x8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_SMT_PERM_ENABLE_SHFT                               0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_RCP_BYPASS_ENABLE_BMSK                        0x4000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_RCP_BYPASS_ENABLE_SHFT                             0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_SMMU_BYPASS_DISABLE_BMSK                      0x2000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_SMMU_BYPASS_DISABLE_SHFT                           0x19
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_GPIO_SLIMBUS_PD_DISABLE_BMSK                  0x1000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_GPIO_SLIMBUS_PD_DISABLE_SHFT                       0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_PCIE20_RC_DISABLE_BMSK                         0x800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_PCIE20_RC_DISABLE_SHFT                             0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_PCIE_DISABLE_BMSK                              0x400000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_PCIE_DISABLE_SHFT                                  0x16
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_NDINT_DISABLE_BMSK                                  0x200000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_NDINT_DISABLE_SHFT                                      0x15
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_BMSK                        0x100000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_SHFT                            0x14
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_ENABLE_FVAL                      0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_DISABLE_FVAL                     0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_BMSK                          0x80000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_SHFT                             0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_ENABLE_FVAL                       0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_DISABLE_FVAL                      0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_BMSK                           0x40000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_SHFT                              0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_BMSK                            0x20000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_SHFT                               0x11
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_ENABLE_FVAL                         0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_DISABLE_FVAL                        0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_BMSK                            0x10000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_SHFT                               0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_ENABLE_FVAL                         0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_DISABLE_FVAL                        0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_BMSK                          0x8000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_SHFT                             0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_ENABLE_FVAL                      0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_DISABLE_FVAL                     0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_BMSK                           0x4000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_SHFT                              0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_ENABLE_FVAL                       0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_DISABLE_FVAL                      0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_BMSK                            0x2000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_SHFT                               0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_BMSK                            0x1000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_SHFT                               0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_BMSK                                 0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_SHFT                                   0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_BMSK                                 0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_SHFT                                   0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_BMSK                          0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_SHFT                            0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_ENABLE_FVAL                     0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_DISABLE_FVAL                    0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_BMSK                              0x100
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_SHFT                                0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_ENABLE_FVAL                         0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_DISABLE_FVAL                        0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_BMSK                          0x80
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_SHFT                           0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_ENABLE_FVAL                    0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_DISABLE_FVAL                   0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_BMSK                               0x40
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_SHFT                                0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_ENABLE_FVAL                         0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_DISABLE_FVAL                        0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_BMSK                              0x20
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_SHFT                               0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_BMSK                               0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_SHFT                                0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_ENABLE_FVAL                         0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_DISABLE_FVAL                        0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_BMSK                                0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_SHFT                                0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_ENABLE_FVAL                         0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_DISABLE_FVAL                        0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_BMSK                                    0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_SHFT                                    0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_ENABLE_FVAL                             0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_DISABLE_FVAL                            0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_BMSK                                     0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_SHFT                                     0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_ENABLE_FVAL                              0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_DISABLE_FVAL                             0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_BMSK                                         0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_SHFT                                         0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_ENABLE_FVAL                                  0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_DISABLE_FVAL                                 0x1

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000004c)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000004c)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_BMSK                       0xf8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_SHFT                             0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_BMSK                      0x4000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_SHFT                           0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_BMSK                             0x3ffc000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_SHFT                                   0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SPARE1_BMSK                                           0x3c00
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SPARE1_SHFT                                              0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_BMSK                           0x3e0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_SHFT                             0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_BMSK                             0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_SHFT                              0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000050)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000050)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_BMSK             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_SHFT                    0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000054)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000054)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_STACKED_MEMORY_ID_BMSK                            0xf0000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_STACKED_MEMORY_ID_SHFT                                  0x1c
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APSS_BOOT_TRIGGER_DISABLE_BMSK                     0x8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APSS_BOOT_TRIGGER_DISABLE_SHFT                          0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_BMSK                            0x7800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_SHFT                                 0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_BMSK                              0x780000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_SHFT                                  0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_CFG_BMSK                               0x40000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_CFG_SHFT                                  0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_DELAY_BMSK                             0x30000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_DELAY_SHFT                                0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_SEL_EN_USB_PHY_TUNE_BMSK               0x8000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_SEL_EN_USB_PHY_TUNE_SHFT                  0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_USB_SS_ENABLE_BMSK                                    0x4000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_USB_SS_ENABLE_SHFT                                       0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_BMSK                              0x3000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_SHFT                                 0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_XO_FVAL                              0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_ENUM_384_MHZ_FVAL                    0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_ENUM_614_4_MHZ_FVAL                  0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_ENUM_998_4_MHZ_FVAL                  0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_BMSK                                0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_SHFT                                  0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MSA_ENA_BMSK                                           0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MSA_ENA_SHFT                                             0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_BMSK                                 0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_SHFT                                   0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_MODEM_IMAGE_NOT_AUTHENTICATED_FVAL        0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_FORCE_MODEM_IMAGE_AUTHENTICATION_FVAL        0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_BMSK                                         0x180
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_SHFT                                           0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_BMSK                                0x40
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_SHFT                                 0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_BMSK                             0x20
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_SHFT                              0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_ENABLE_PATCHING_FVAL              0x0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_DISABLE_PATCHING_FVAL             0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_BMSK                  0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_SHFT                   0x0

#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000058 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_PHYS(n)                                (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000058 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                            3
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_POR                                    0x00000000
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_POR_RMSK                               0x00000000
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                         0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                                0x0

#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000005c + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_PHYS(n)                                (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000005c + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                            3
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_POR                                    0x00000000
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_POR_RMSK                               0x00000000
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                         0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                                0x0

#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000078 + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_PHYS(n)                                (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000078 + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                            3
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_POR                                    0x00000000
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_POR_RMSK                               0x00000000
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                         0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                                0x0

#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000007c + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_PHYS(n)                                (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000007c + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                            3
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_POR                                    0x00000000
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_POR_RMSK                               0x00000000
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                         0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                                0x0

#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000098)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000098)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_POR                                              0x00000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_BMSK                                   0xff000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_SHFT                                         0x18
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_BMSK                                     0xff0000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_SHFT                                         0x10
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_BMSK                                       0xff00
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_SHFT                                          0x8
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_BMSK                                         0xff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_SHFT                                          0x0

#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000009c)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000009c)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_POR                                              0x00000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SPARE0_BMSK                                      0x80000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SPARE0_SHFT                                            0x1f
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_FEC_VALUE_BMSK                                   0x7f000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_FEC_VALUE_SHFT                                         0x18
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_BMSK                                     0xff0000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_SHFT                                         0x10
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_BMSK                                       0xff00
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_SHFT                                          0x8
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_BMSK                                         0xff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_SHFT                                          0x0

#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000a0)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000a0)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT4_BMSK                                    0xff000000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT4_SHFT                                          0x18
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT3_BMSK                                      0xff0000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT3_SHFT                                          0x10
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT2_BMSK                                        0xff00
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT2_SHFT                                           0x8
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT1_BMSK                                          0xff
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT1_SHFT                                           0x0

#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000a4)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000a4)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SPARE0_BMSK                                       0x80000000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SPARE0_SHFT                                             0x1f
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT7_BMSK                                      0xff0000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT7_SHFT                                          0x10
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT6_BMSK                                        0xff00
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT6_SHFT                                           0x8
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT5_BMSK                                          0xff
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT5_SHFT                                           0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n)                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000a8 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_PHYS(n)                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000a8 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_MAXn                                                           3
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_POR                                                   0x00000000
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_POR_RMSK                                              0x00000000
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_HASH_DATA0_BMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_HASH_DATA0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n)                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000ac + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_PHYS(n)                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000ac + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_MAXn                                                           3
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_POR                                                   0x00000000
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_POR_RMSK                                              0x00000000
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_HASH_DATA1_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_HASH_DATA1_SHFT                                              0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000c8)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000c8)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_POR                                                   0x00000000
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_POR_RMSK                                              0x00000000
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR, HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR,m,v,HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_IN)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_HASH_DATA0_BMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_HASH_DATA0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000cc)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000cc)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_RMSK                                                  0xff000000
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_POR                                                   0x00000000
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_POR_RMSK                                              0x00ffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR, HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR,m,v,HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_IN)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_FEC_VALUE_SHFT                                              0x18

#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000d0)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000d0)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS2_POINT1_BMSK                                      0xf0000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS2_POINT1_SHFT                                            0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS1_POINT1_BMSK                                       0xf000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS1_POINT1_SHFT                                            0x18
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS0_POINT1_BMSK                                        0xf00000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS0_POINT1_SHFT                                            0x14
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS_BASE1_BMSK                                           0xffc00
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS_BASE1_SHFT                                               0xa
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS_BASE0_BMSK                                             0x3ff
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS_BASE0_SHFT                                               0x0

#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000d4)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000d4)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_USB_PHY_TUNING_BMSK                                       0xf00000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_USB_PHY_TUNING_SHFT                                           0x14
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_SPARE_BMSK                                                 0xff800
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_SPARE_SHFT                                                     0xb
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS_CAL_SEL_BMSK                                           0x700
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS_CAL_SEL_SHFT                                             0x8
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS4_POINT1_BMSK                                            0xf0
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS4_POINT1_SHFT                                             0x4
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS3_POINT1_BMSK                                             0xf
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS3_POINT1_SHFT                                             0x0

#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000d8)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000d8)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_0_BMSK                    0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_0_SHFT                          0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS2_BMSK                                0x7c000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS2_SHFT                                      0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_1_BMSK                     0x2000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_1_SHFT                          0x19
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS_BMSK                                  0x1f00000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS_SHFT                                       0x14
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_2_BMSK                       0x80000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_2_SHFT                          0x13
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_NOM_BMSK                                    0x7c000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_NOM_SHFT                                        0xe
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_3_BMSK                        0x2000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_3_SHFT                           0xd
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_TUR_BMSK                                     0x1f00
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_TUR_SHFT                                        0x8
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_TARG_VOLT_OFFSET_SVS2_BMSK                               0x80
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_TARG_VOLT_OFFSET_SVS2_SHFT                                0x7
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_NOM_BMSK                                0x78
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_NOM_SHFT                                 0x3
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_TUR_BMSK                                 0x7
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_TUR_SHFT                                 0x0

#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000dc)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000dc)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_0_BMSK                      0x800000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_0_SHFT                          0x17
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS2_BMSK                                  0x7c0000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS2_SHFT                                      0x12
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_1_BMSK                       0x20000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_1_SHFT                          0x11
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS_BMSK                                    0x1f000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS_SHFT                                        0xc
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_2_BMSK                         0x800
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_2_SHFT                           0xb
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_NOM_BMSK                                      0x7c0
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_NOM_SHFT                                        0x6
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_3_BMSK                          0x20
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_3_SHFT                           0x5
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_TUR_BMSK                                       0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_TUR_SHFT                                        0x0

#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000e0)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000e0)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_TARGET1_BMSK                                        0xf0000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_TARGET1_SHFT                                              0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_TARGET0_BMSK                                         0xfc00000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_TARGET0_SHFT                                              0x16
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_ENABLE_BMSK                                           0x200000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_ENABLE_SHFT                                               0x15
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS2_BMSK                           0x1e0000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS2_SHFT                               0x11
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BMSK                             0x1c000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_SHFT                                 0xe
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR2_TARG_VOLT_NOM_BMSK                                     0x3e00
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR2_TARG_VOLT_NOM_SHFT                                        0x9
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BIT_0_BMSK                         0x100
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BIT_0_SHFT                           0x8
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR2_TARG_VOLT_TUR_BMSK                                       0xf8
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR2_TARG_VOLT_TUR_SHFT                                        0x3
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR_REV_BMSK                                                   0x7
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR_REV_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000e4)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000e4)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BMSK                           0xc00000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_SHFT                               0x16
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS_BMSK                            0x3c0000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS_SHFT                                0x12
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_NOM_BMSK                             0x3c000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_NOM_SHFT                                 0xe
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_TUR_BMSK                              0x3c00
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_TUR_SHFT                                 0xa
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_SPARE_BMSK                                                   0x300
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_SPARE_SHFT                                                     0x8
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_VCM_BMSK                                         0xc0
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_VCM_SHFT                                          0x6
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_LDO_BMSK                                         0x30
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_LDO_SHFT                                          0x4
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_VREF_BMSK                                         0xc
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_VREF_SHFT                                         0x2
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_LDO_ENABLE_SVS2_BMSK                                           0x2
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_LDO_ENABLE_SVS2_SHFT                                           0x1
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_LDO_TARGET1_BMSK                                               0x1
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_LDO_TARGET1_SHFT                                               0x0

#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000e8)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000e8)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW3_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW3_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_BHS_SLOPE_BMSK                                      0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_BHS_SLOPE_SHFT                                            0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_BHS_INTERCEPT_BMSK                                  0x7fe00000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_BHS_INTERCEPT_SHFT                                        0x15
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_LDO_SLOPE_BMSK                                        0x1ff800
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_LDO_SLOPE_SHFT                                             0xb
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_LDO_INTERCEPT_BMSK                                       0x7fe
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_LDO_INTERCEPT_SHFT                                         0x1
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_ENABLE_BMSK                                                0x1
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_ENABLE_SHFT                                                0x0

#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000ec)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000ec)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW3_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW3_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_G_B1_BMSK                                                 0xe00000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_G_B1_SHFT                                                     0x15
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_SAR_LDO_ERR0_BMSK                                         0x180000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_SAR_LDO_ERR0_SHFT                                             0x13
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_TARG_VOLT_OFFSET_TUR_BIT_3_BMSK                       0x40000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_TARG_VOLT_OFFSET_TUR_BIT_3_SHFT                          0x12
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BIT_3_BMSK                      0x20000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BIT_3_SHFT                         0x11
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ECS_MISC_BMSK                                              0x1fe00
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ECS_MISC_SHFT                                                  0x9
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ECS_BHS_SLOPE_BMSK                                           0x1ff
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ECS_BHS_SLOPE_SHFT                                             0x0

#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000f0)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000f0)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW4_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW4_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M2_BMSK                                            0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M2_SHFT                                                  0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M1_BMSK                                            0x38000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M1_SHFT                                                  0x1b
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M0_BMSK                                             0x7000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M0_SHFT                                                  0x18
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_Q6SS0_LDO_VREF_TRIM_BMSK                                  0xf80000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_Q6SS0_LDO_VREF_TRIM_SHFT                                      0x13
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CLK_LDO_ERR0_BMSK                                          0x60000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CLK_LDO_ERR0_SHFT                                             0x11
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_VREF_ERR_B0_BMSK                                           0x18000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_VREF_ERR_B0_SHFT                                               0xf
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M3_BMSK                                                0x7000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M3_SHFT                                                   0xc
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M2_BMSK                                                 0xe00
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M2_SHFT                                                   0x9
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M1_BMSK                                                 0x1c0
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M1_SHFT                                                   0x6
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M0_BMSK                                                  0x38
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M0_SHFT                                                   0x3
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_G_B0_BMSK                                                      0x7
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_G_B0_SHFT                                                      0x0

#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000f4)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000f4)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW4_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW4_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M3_BMSK                                              0xc00000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M3_SHFT                                                  0x16
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M2_BMSK                                              0x380000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M2_SHFT                                                  0x13
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M1_BMSK                                               0x70000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M1_SHFT                                                  0x10
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M0_BMSK                                                0xe000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M0_SHFT                                                   0xd
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_G_B2_BMSK                                                   0x1c00
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_G_B2_SHFT                                                      0xa
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_SAR_LDO_ERR1_BMSK                                            0x300
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_SAR_LDO_ERR1_SHFT                                              0x8
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CLK_LDO_ERR1_BMSK                                             0xc0
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CLK_LDO_ERR1_SHFT                                              0x6
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_VREF_ERR_B1_BMSK                                              0x30
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_VREF_ERR_B1_SHFT                                               0x4
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B1M3_BMSK                                                   0xe
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B1M3_SHFT                                                   0x1
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B1M2_BMSK                                                   0x1
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B1M2_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000f8)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000f8)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW5_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW5_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B4M0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B4M0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_G_B4_BMSK                                               0x70000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_G_B4_SHFT                                                     0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_SAR_LDO_ERR3_BMSK                                        0xc000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_SAR_LDO_ERR3_SHFT                                             0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CLK_LDO_ERR3_BMSK                                        0x3000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CLK_LDO_ERR3_SHFT                                             0x18
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_VREF_ERR_B3_BMSK                                          0xc00000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_VREF_ERR_B3_SHFT                                              0x16
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M3_BMSK                                              0x380000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M3_SHFT                                                  0x13
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M2_BMSK                                               0x70000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M2_SHFT                                                  0x10
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M1_BMSK                                                0xe000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M1_SHFT                                                   0xd
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M0_BMSK                                                0x1c00
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M0_SHFT                                                   0xa
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_G_B3_BMSK                                                    0x380
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_G_B3_SHFT                                                      0x7
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_SAR_LDO_ERR2_BMSK                                             0x60
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_SAR_LDO_ERR2_SHFT                                              0x5
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CLK_LDO_ERR2_BMSK                                             0x18
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CLK_LDO_ERR2_SHFT                                              0x3
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_VREF_ERR_B2_BMSK                                               0x6
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_VREF_ERR_B2_SHFT                                               0x1
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B2M3_BMSK                                                   0x1
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B2M3_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000fc)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000fc)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW5_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW5_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B5M1_BMSK                                              0x800000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B5M1_SHFT                                                  0x17
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B5M0_BMSK                                              0x700000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B5M0_SHFT                                                  0x14
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_G_B5_BMSK                                                  0xe0000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_G_B5_SHFT                                                     0x11
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_SAR_LDO_ERR4_BMSK                                          0x18000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_SAR_LDO_ERR4_SHFT                                              0xf
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CLK_LDO_ERR4_BMSK                                           0x6000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CLK_LDO_ERR4_SHFT                                              0xd
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_VREF_ERR_B4_BMSK                                            0x1800
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_VREF_ERR_B4_SHFT                                               0xb
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M3_BMSK                                                 0x700
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M3_SHFT                                                   0x8
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M2_BMSK                                                  0xe0
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M2_SHFT                                                   0x5
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M1_BMSK                                                  0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M1_SHFT                                                   0x2
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M0_BMSK                                                   0x3
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000100)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000100)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW6_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW6_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CLK_LDO_ERR6_BMSK                                       0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CLK_LDO_ERR6_SHFT                                             0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_ERR_B6_BMSK                                        0x60000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_ERR_B6_SHFT                                              0x1d
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M3_BMSK                                            0x1c000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M3_SHFT                                                  0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M2_BMSK                                             0x3800000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M2_SHFT                                                  0x17
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M1_BMSK                                              0x700000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M1_SHFT                                                  0x14
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M0_BMSK                                               0xe0000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M0_SHFT                                                  0x11
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_G_B6_BMSK                                                  0x1c000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_G_B6_SHFT                                                      0xe
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_SAR_LDO_ERR5_BMSK                                           0x3000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_SAR_LDO_ERR5_SHFT                                              0xc
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CLK_LDO_ERR5_BMSK                                            0xc00
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CLK_LDO_ERR5_SHFT                                              0xa
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_ERR_B5_BMSK                                             0x300
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_ERR_B5_SHFT                                               0x8
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M3_BMSK                                                  0xe0
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M3_SHFT                                                   0x5
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M2_BMSK                                                  0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M2_SHFT                                                   0x2
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M1_BMSK                                                   0x3
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000104)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000104)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_POR                                                     0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_POR_RMSK                                                0x00000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW6_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW6_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_BMSK                                   0xff0000
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_SHFT                                       0x10
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_BMSK                                     0xff00
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_SHFT                                        0x8
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_AVG_ERROR_BMSK                             0x80
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_AVG_ERROR_SHFT                              0x7
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_AVG_ERROR_BMSK                             0x40
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_AVG_ERROR_SHFT                              0x6
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC_0_1_FUSEFLAG_BMSK                                 0x20
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC_0_1_FUSEFLAG_SHFT                                  0x5
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_OVERFLOW_BMSK                              0x10
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_OVERFLOW_SHFT                               0x4
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_OVERFLOW_BMSK                               0x8
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_OVERFLOW_SHFT                               0x3
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_SAR_LDO_ERR6_BMSK                                              0x6
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_SAR_LDO_ERR6_SHFT                                              0x1
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CLK_LDO_ERR6_BMSK                                              0x1
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CLK_LDO_ERR6_SHFT                                              0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000108 + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_PHYS(n)                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000108 + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_MAXn                                                       15
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_REDUN_DATA_BMSK                                    0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_REDUN_DATA_SHFT                                           0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000010c + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_PHYS(n)                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000010c + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_MAXn                                                       15
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_POR                                                0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_SPARE0_BMSK                                        0x80000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_SPARE0_SHFT                                              0x1f
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_FEC_VALUE_BMSK                                     0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_FEC_VALUE_SHFT                                           0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_REDUN_DATA_BMSK                                      0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_REDUN_DATA_SHFT                                           0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000188)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000188)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_MEM_ACCEL_COMPILER_BMSK                           0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_MEM_ACCEL_COMPILER_SHFT                                  0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000018c)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000018c)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_RMSK                                              0x7fffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_SPARE0_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_SPARE0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000190)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000190)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_CUSTOM_BMSK                             0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_CUSTOM_SHFT                                    0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000194)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000194)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_RMSK                                              0x7fffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_SPARE0_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_SPARE0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000198)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000198)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_CUSTOM_BMSK                             0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_CUSTOM_SHFT                                    0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000019c)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000019c)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_RMSK                                              0x7fffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_SPARE0_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_SPARE0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001a0)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001a0)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_MEM_ACCEL_COMPILER_BMSK                           0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_MEM_ACCEL_COMPILER_SHFT                                  0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001a4)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001a4)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_RMSK                                              0x7fffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_SPARE0_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_SPARE0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001a8)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001a8)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_MEM_ACCEL_CUSTOM_BMSK                             0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_MEM_ACCEL_CUSTOM_SHFT                                    0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001ac)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001ac)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_RMSK                                              0x7fffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_POR                                               0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_SPARE0_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_SPARE0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001b0 + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_PHYS(n)                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001b0 + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_MAXn                                                        31
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_POR                                                 0x00000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_POR_RMSK                                            0x00000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_PATCH_DATA_BMSK                                     0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_PATCH_DATA_SHFT                                            0x0

#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001b4 + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PHYS(n)                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001b4 + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_MAXn                                                        31
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_POR                                                 0x00000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_POR_RMSK                                            0x00000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE0_BMSK                                         0x80000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE0_SHFT                                               0x1f
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_FEC_VALUE_BMSK                                      0x7f000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_FEC_VALUE_SHFT                                            0x18
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE3_BMSK                                           0xfe0000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE3_SHFT                                               0x11
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_ADDR_BMSK                                        0x1fffe
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_ADDR_SHFT                                            0x1
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_EN_BMSK                                              0x1
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_EN_SHFT                                              0x0

#define HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002b0)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_PHYS                                                        (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002b0)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_RMSK                                                        0xffffffff
#define HWIO_QFPROM_RAW_FEC_EN_LSB_POR                                                         0x00000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_POR_RMSK                                                    0x00000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR, HWIO_QFPROM_RAW_FEC_EN_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEC_EN_LSB_IN)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION31_FEC_EN_BMSK                                        0x80000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION31_FEC_EN_SHFT                                              0x1f
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION31_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION31_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION30_FEC_EN_BMSK                                        0x40000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION30_FEC_EN_SHFT                                              0x1e
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION30_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION30_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION29_FEC_EN_BMSK                                        0x20000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION29_FEC_EN_SHFT                                              0x1d
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION29_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION29_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION28_FEC_EN_BMSK                                        0x10000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION28_FEC_EN_SHFT                                              0x1c
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION28_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION28_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION27_FEC_EN_BMSK                                         0x8000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION27_FEC_EN_SHFT                                              0x1b
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION27_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION27_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION26_FEC_EN_BMSK                                         0x4000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION26_FEC_EN_SHFT                                              0x1a
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION26_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION26_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION25_FEC_EN_BMSK                                         0x2000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION25_FEC_EN_SHFT                                              0x19
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION25_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION25_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION24_FEC_EN_BMSK                                         0x1000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION24_FEC_EN_SHFT                                              0x18
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION24_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION24_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION23_FEC_EN_BMSK                                          0x800000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION23_FEC_EN_SHFT                                              0x17
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION23_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION23_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION22_FEC_EN_BMSK                                          0x400000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION22_FEC_EN_SHFT                                              0x16
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION22_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION22_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION21_FEC_EN_BMSK                                          0x200000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION21_FEC_EN_SHFT                                              0x15
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION21_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION21_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION20_FEC_EN_BMSK                                          0x100000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION20_FEC_EN_SHFT                                              0x14
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION20_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION20_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION19_FEC_EN_BMSK                                           0x80000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION19_FEC_EN_SHFT                                              0x13
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION19_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION19_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION18_FEC_EN_BMSK                                           0x40000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION18_FEC_EN_SHFT                                              0x12
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION18_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION18_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION17_FEC_EN_BMSK                                           0x20000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION17_FEC_EN_SHFT                                              0x11
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION17_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION17_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION16_FEC_EN_BMSK                                           0x10000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION16_FEC_EN_SHFT                                              0x10
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION16_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION16_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION15_FEC_EN_BMSK                                            0x8000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION15_FEC_EN_SHFT                                               0xf
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION15_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION15_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION14_FEC_EN_BMSK                                            0x4000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION14_FEC_EN_SHFT                                               0xe
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION14_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION14_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION13_FEC_EN_BMSK                                            0x2000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION13_FEC_EN_SHFT                                               0xd
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION13_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION13_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION12_FEC_EN_BMSK                                            0x1000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION12_FEC_EN_SHFT                                               0xc
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION12_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION12_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION11_FEC_EN_BMSK                                             0x800
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION11_FEC_EN_SHFT                                               0xb
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION11_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION11_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION10_FEC_EN_BMSK                                             0x400
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION10_FEC_EN_SHFT                                               0xa
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION10_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION10_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION9_FEC_EN_BMSK                                              0x200
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION9_FEC_EN_SHFT                                                0x9
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION9_FEC_EN_DISABLE_FVAL                                        0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION9_FEC_EN_ENABLE_FVAL                                         0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION8_FEC_EN_BMSK                                              0x100
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION8_FEC_EN_SHFT                                                0x8
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION8_FEC_EN_DISABLE_FVAL                                        0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION8_FEC_EN_ENABLE_FVAL                                         0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION7_FEC_EN_BMSK                                               0x80
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION7_FEC_EN_SHFT                                                0x7
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION7_FEC_EN_DISABLE_FVAL                                        0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION7_FEC_EN_ENABLE_FVAL                                         0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION6_FEC_EN_BMSK                                               0x40
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION6_FEC_EN_SHFT                                                0x6
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION6_FEC_EN_DISABLE_FVAL                                        0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION6_FEC_EN_ENABLE_FVAL                                         0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION5_FEC_EN_BMSK                                               0x20
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION5_FEC_EN_SHFT                                                0x5
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION5_FEC_EN_DISABLE_FVAL                                        0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION5_FEC_EN_ENABLE_FVAL                                         0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION4_FEC_EN_BMSK                                               0x10
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION4_FEC_EN_SHFT                                                0x4
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION4_FEC_EN_DISABLE_FVAL                                        0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION4_FEC_EN_ENABLE_FVAL                                         0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION3_FEC_EN_BMSK                                                0x8
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION3_FEC_EN_SHFT                                                0x3
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION3_FEC_EN_DISABLE_FVAL                                        0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION3_FEC_EN_ENABLE_FVAL                                         0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION2_FEC_EN_BMSK                                                0x4
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION2_FEC_EN_SHFT                                                0x2
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION2_FEC_EN_DISABLE_FVAL                                        0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION2_FEC_EN_ENABLE_FVAL                                         0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION1_FEC_EN_BMSK                                                0x2
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION1_FEC_EN_SHFT                                                0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION1_FEC_EN_DISABLE_FVAL                                        0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION1_FEC_EN_ENABLE_FVAL                                         0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION0_FEC_EN_BMSK                                                0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION0_FEC_EN_SHFT                                                0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION0_FEC_EN_DISABLE_FVAL                                        0x0
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION0_FEC_EN_ENABLE_FVAL                                         0x1

#define HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002b4)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_PHYS                                                        (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002b4)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_RMSK                                                        0xffffffff
#define HWIO_QFPROM_RAW_FEC_EN_MSB_POR                                                         0x00000000
#define HWIO_QFPROM_RAW_FEC_EN_MSB_POR_RMSK                                                    0x00000000
#define HWIO_QFPROM_RAW_FEC_EN_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR, HWIO_QFPROM_RAW_FEC_EN_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEC_EN_MSB_IN)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_FEC_EN_REDUNDANCY_BMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_FEC_EN_MSB_FEC_EN_REDUNDANCY_SHFT                                             0x0

#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002b8)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002b8)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG16_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG16_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG16_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG16_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG16_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG16_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002bc)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002bc)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG16_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG16_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG16_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG16_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG16_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG16_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002c0)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002c0)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG17_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG17_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG17_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG17_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG17_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG17_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002c4)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002c4)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG17_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG17_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG17_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG17_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG17_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG17_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002c8)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002c8)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG18_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG18_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002cc)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002cc)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG18_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG18_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002d0)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002d0)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG19_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG19_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002d4)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002d4)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG19_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG19_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002d8)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002d8)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG20_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG20_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002dc)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002dc)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG20_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG20_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002e0)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002e0)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG21_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG21_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002e4)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002e4)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG21_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG21_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n)                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000400 + 0x4 * (n))
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_PHYS(n)                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000400 + 0x4 * (n))
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_RMSK                                                      0xffffffff
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_MAXn                                                              39
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_POR                                                       0x00000000
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_POR_RMSK                                                  0x00000000
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n), HWIO_QFPROM_RAW_ACC_PRIVATEn_RMSK)
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ACC_PRIVATE_BMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ACC_PRIVATE_SHFT                                                 0x0

#define HWIO_QFPROM_BLOW_TIMER_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000203c)
#define HWIO_QFPROM_BLOW_TIMER_PHYS                                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000203c)
#define HWIO_QFPROM_BLOW_TIMER_RMSK                                                                 0xfff
#define HWIO_QFPROM_BLOW_TIMER_POR                                                             0x00000000
#define HWIO_QFPROM_BLOW_TIMER_POR_RMSK                                                        0xffffffff
#define HWIO_QFPROM_BLOW_TIMER_IN          \
        in_dword_masked(HWIO_QFPROM_BLOW_TIMER_ADDR, HWIO_QFPROM_BLOW_TIMER_RMSK)
#define HWIO_QFPROM_BLOW_TIMER_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BLOW_TIMER_ADDR, m)
#define HWIO_QFPROM_BLOW_TIMER_OUT(v)      \
        out_dword(HWIO_QFPROM_BLOW_TIMER_ADDR,v)
#define HWIO_QFPROM_BLOW_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_BLOW_TIMER_ADDR,m,v,HWIO_QFPROM_BLOW_TIMER_IN)
#define HWIO_QFPROM_BLOW_TIMER_BLOW_TIMER_BMSK                                                      0xfff
#define HWIO_QFPROM_BLOW_TIMER_BLOW_TIMER_SHFT                                                        0x0

#define HWIO_QFPROM_TEST_CTRL_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002040)
#define HWIO_QFPROM_TEST_CTRL_PHYS                                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002040)
#define HWIO_QFPROM_TEST_CTRL_RMSK                                                                    0xf
#define HWIO_QFPROM_TEST_CTRL_POR                                                              0x00000000
#define HWIO_QFPROM_TEST_CTRL_POR_RMSK                                                         0xffffffff
#define HWIO_QFPROM_TEST_CTRL_IN          \
        in_dword_masked(HWIO_QFPROM_TEST_CTRL_ADDR, HWIO_QFPROM_TEST_CTRL_RMSK)
#define HWIO_QFPROM_TEST_CTRL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_TEST_CTRL_ADDR, m)
#define HWIO_QFPROM_TEST_CTRL_OUT(v)      \
        out_dword(HWIO_QFPROM_TEST_CTRL_ADDR,v)
#define HWIO_QFPROM_TEST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_TEST_CTRL_ADDR,m,v,HWIO_QFPROM_TEST_CTRL_IN)
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_ROM_BMSK                                                        0x8
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_ROM_SHFT                                                        0x3
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_WL_BMSK                                                         0x4
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_WL_SHFT                                                         0x2
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_BL_BMSK                                                         0x2
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_BL_SHFT                                                         0x1
#define HWIO_QFPROM_TEST_CTRL_EN_FUSE_RES_MEAS_BMSK                                                   0x1
#define HWIO_QFPROM_TEST_CTRL_EN_FUSE_RES_MEAS_SHFT                                                   0x0

#define HWIO_QFPROM_ACCEL_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002044)
#define HWIO_QFPROM_ACCEL_PHYS                                                                 (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002044)
#define HWIO_QFPROM_ACCEL_RMSK                                                                      0xfff
#define HWIO_QFPROM_ACCEL_POR                                                                  0x00000b00
#define HWIO_QFPROM_ACCEL_POR_RMSK                                                             0xffffffff
#define HWIO_QFPROM_ACCEL_IN          \
        in_dword_masked(HWIO_QFPROM_ACCEL_ADDR, HWIO_QFPROM_ACCEL_RMSK)
#define HWIO_QFPROM_ACCEL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_ACCEL_ADDR, m)
#define HWIO_QFPROM_ACCEL_OUT(v)      \
        out_dword(HWIO_QFPROM_ACCEL_ADDR,v)
#define HWIO_QFPROM_ACCEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_ACCEL_ADDR,m,v,HWIO_QFPROM_ACCEL_IN)
#define HWIO_QFPROM_ACCEL_QFPROM_GATELAST_BMSK                                                      0x800
#define HWIO_QFPROM_ACCEL_QFPROM_GATELAST_SHFT                                                        0xb
#define HWIO_QFPROM_ACCEL_QFPROM_TRIPPT_SEL_BMSK                                                    0x700
#define HWIO_QFPROM_ACCEL_QFPROM_TRIPPT_SEL_SHFT                                                      0x8
#define HWIO_QFPROM_ACCEL_QFPROM_ACCEL_BMSK                                                          0xff
#define HWIO_QFPROM_ACCEL_QFPROM_ACCEL_SHFT                                                           0x0

#define HWIO_QFPROM_BLOW_STATUS_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002048)
#define HWIO_QFPROM_BLOW_STATUS_PHYS                                                           (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002048)
#define HWIO_QFPROM_BLOW_STATUS_RMSK                                                                  0x3
#define HWIO_QFPROM_BLOW_STATUS_POR                                                            0x00000000
#define HWIO_QFPROM_BLOW_STATUS_POR_RMSK                                                       0xffffffff
#define HWIO_QFPROM_BLOW_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM_BLOW_STATUS_ADDR, HWIO_QFPROM_BLOW_STATUS_RMSK)
#define HWIO_QFPROM_BLOW_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BLOW_STATUS_ADDR, m)
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_WR_ERR_BMSK                                                    0x2
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_WR_ERR_SHFT                                                    0x1
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_WR_ERR_NO_ERROR_FVAL                                           0x0
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_WR_ERR_ERROR_FVAL                                              0x1
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_BUSY_BMSK                                                      0x1
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_BUSY_SHFT                                                      0x0

#define HWIO_QFPROM_ROM_ERROR_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000204c)
#define HWIO_QFPROM_ROM_ERROR_PHYS                                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000204c)
#define HWIO_QFPROM_ROM_ERROR_RMSK                                                                    0x1
#define HWIO_QFPROM_ROM_ERROR_POR                                                              0x00000000
#define HWIO_QFPROM_ROM_ERROR_POR_RMSK                                                         0xffffffff
#define HWIO_QFPROM_ROM_ERROR_IN          \
        in_dword_masked(HWIO_QFPROM_ROM_ERROR_ADDR, HWIO_QFPROM_ROM_ERROR_RMSK)
#define HWIO_QFPROM_ROM_ERROR_INM(m)      \
        in_dword_masked(HWIO_QFPROM_ROM_ERROR_ADDR, m)
#define HWIO_QFPROM_ROM_ERROR_ERROR_BMSK                                                              0x1
#define HWIO_QFPROM_ROM_ERROR_ERROR_SHFT                                                              0x0
#define HWIO_QFPROM_ROM_ERROR_ERROR_NO_ERROR_FVAL                                                     0x0
#define HWIO_QFPROM_ROM_ERROR_ERROR_ERROR_FVAL                                                        0x1

#define HWIO_QFPROM_BIST_CTRL_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002050)
#define HWIO_QFPROM_BIST_CTRL_PHYS                                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002050)
#define HWIO_QFPROM_BIST_CTRL_RMSK                                                                   0x7f
#define HWIO_QFPROM_BIST_CTRL_POR                                                              0x00000000
#define HWIO_QFPROM_BIST_CTRL_POR_RMSK                                                         0xffffffff
#define HWIO_QFPROM_BIST_CTRL_IN          \
        in_dword_masked(HWIO_QFPROM_BIST_CTRL_ADDR, HWIO_QFPROM_BIST_CTRL_RMSK)
#define HWIO_QFPROM_BIST_CTRL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BIST_CTRL_ADDR, m)
#define HWIO_QFPROM_BIST_CTRL_OUT(v)      \
        out_dword(HWIO_QFPROM_BIST_CTRL_ADDR,v)
#define HWIO_QFPROM_BIST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_BIST_CTRL_ADDR,m,v,HWIO_QFPROM_BIST_CTRL_IN)
#define HWIO_QFPROM_BIST_CTRL_AUTH_REGION_BMSK                                                       0x7c
#define HWIO_QFPROM_BIST_CTRL_AUTH_REGION_SHFT                                                        0x2
#define HWIO_QFPROM_BIST_CTRL_SHA_ENABLE_BMSK                                                         0x2
#define HWIO_QFPROM_BIST_CTRL_SHA_ENABLE_SHFT                                                         0x1
#define HWIO_QFPROM_BIST_CTRL_START_BMSK                                                              0x1
#define HWIO_QFPROM_BIST_CTRL_START_SHFT                                                              0x0

#define HWIO_QFPROM_BIST_ERROR_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002054)
#define HWIO_QFPROM_BIST_ERROR_PHYS                                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002054)
#define HWIO_QFPROM_BIST_ERROR_RMSK                                                            0xffffffff
#define HWIO_QFPROM_BIST_ERROR_POR                                                             0x00000000
#define HWIO_QFPROM_BIST_ERROR_POR_RMSK                                                        0xffffffff
#define HWIO_QFPROM_BIST_ERROR_IN          \
        in_dword_masked(HWIO_QFPROM_BIST_ERROR_ADDR, HWIO_QFPROM_BIST_ERROR_RMSK)
#define HWIO_QFPROM_BIST_ERROR_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BIST_ERROR_ADDR, m)
#define HWIO_QFPROM_BIST_ERROR_ERROR_BMSK                                                      0xffffffff
#define HWIO_QFPROM_BIST_ERROR_ERROR_SHFT                                                             0x0
#define HWIO_QFPROM_BIST_ERROR_ERROR_NO_ERROR_FVAL                                                    0x0
#define HWIO_QFPROM_BIST_ERROR_ERROR_ERROR_FVAL                                                       0x1

#define HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n)                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002060 + 0x4 * (n))
#define HWIO_QFPROM_HASH_SIGNATUREn_PHYS(n)                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002060 + 0x4 * (n))
#define HWIO_QFPROM_HASH_SIGNATUREn_RMSK                                                       0xffffffff
#define HWIO_QFPROM_HASH_SIGNATUREn_MAXn                                                                7
#define HWIO_QFPROM_HASH_SIGNATUREn_POR                                                        0x00000000
#define HWIO_QFPROM_HASH_SIGNATUREn_POR_RMSK                                                   0xffffffff
#define HWIO_QFPROM_HASH_SIGNATUREn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n), HWIO_QFPROM_HASH_SIGNATUREn_RMSK)
#define HWIO_QFPROM_HASH_SIGNATUREn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n), mask)
#define HWIO_QFPROM_HASH_SIGNATUREn_HASH_VALUE_BMSK                                            0xffffffff
#define HWIO_QFPROM_HASH_SIGNATUREn_HASH_VALUE_SHFT                                                   0x0

#define HWIO_QFPROM0_MATCH_STATUS_ADDR                                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002098)
#define HWIO_QFPROM0_MATCH_STATUS_PHYS                                                         (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002098)
#define HWIO_QFPROM0_MATCH_STATUS_RMSK                                                         0xffffffff
#define HWIO_QFPROM0_MATCH_STATUS_POR                                                          0x00000000
#define HWIO_QFPROM0_MATCH_STATUS_POR_RMSK                                                     0x00000000
#define HWIO_QFPROM0_MATCH_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM0_MATCH_STATUS_ADDR, HWIO_QFPROM0_MATCH_STATUS_RMSK)
#define HWIO_QFPROM0_MATCH_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM0_MATCH_STATUS_ADDR, m)
#define HWIO_QFPROM0_MATCH_STATUS_FLAG_BMSK                                                    0xffffffff
#define HWIO_QFPROM0_MATCH_STATUS_FLAG_SHFT                                                           0x0

#define HWIO_QFPROM1_MATCH_STATUS_ADDR                                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000209c)
#define HWIO_QFPROM1_MATCH_STATUS_PHYS                                                         (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000209c)
#define HWIO_QFPROM1_MATCH_STATUS_RMSK                                                         0xffffffff
#define HWIO_QFPROM1_MATCH_STATUS_POR                                                          0x00000000
#define HWIO_QFPROM1_MATCH_STATUS_POR_RMSK                                                     0x00000000
#define HWIO_QFPROM1_MATCH_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM1_MATCH_STATUS_ADDR, HWIO_QFPROM1_MATCH_STATUS_RMSK)
#define HWIO_QFPROM1_MATCH_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM1_MATCH_STATUS_ADDR, m)
#define HWIO_QFPROM1_MATCH_STATUS_FLAG_BMSK                                                    0xffffffff
#define HWIO_QFPROM1_MATCH_STATUS_FLAG_SHFT                                                           0x0

#define HWIO_QFPROM_CORR_JTAG_ID_ADDR                                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004000)
#define HWIO_QFPROM_CORR_JTAG_ID_PHYS                                                          (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004000)
#define HWIO_QFPROM_CORR_JTAG_ID_RMSK                                                          0xffffffff
#define HWIO_QFPROM_CORR_JTAG_ID_POR                                                           0x00000000
#define HWIO_QFPROM_CORR_JTAG_ID_POR_RMSK                                                      0x00000000
#define HWIO_QFPROM_CORR_JTAG_ID_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_JTAG_ID_ADDR, HWIO_QFPROM_CORR_JTAG_ID_RMSK)
#define HWIO_QFPROM_CORR_JTAG_ID_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_JTAG_ID_ADDR, m)
#define HWIO_QFPROM_CORR_JTAG_ID_IDDQ_CX_ON_BMSK                                               0xc0000000
#define HWIO_QFPROM_CORR_JTAG_ID_IDDQ_CX_ON_SHFT                                                     0x1e
#define HWIO_QFPROM_CORR_JTAG_ID_IDDQ_RC_BMSK                                                  0x20000000
#define HWIO_QFPROM_CORR_JTAG_ID_IDDQ_RC_SHFT                                                        0x1d
#define HWIO_QFPROM_CORR_JTAG_ID_MACCHIATO_EN_BMSK                                             0x10000000
#define HWIO_QFPROM_CORR_JTAG_ID_MACCHIATO_EN_SHFT                                                   0x1c
#define HWIO_QFPROM_CORR_JTAG_ID_FEATURE_ID_BMSK                                                0xff00000
#define HWIO_QFPROM_CORR_JTAG_ID_FEATURE_ID_SHFT                                                     0x14
#define HWIO_QFPROM_CORR_JTAG_ID_JTAG_ID_BMSK                                                     0xfffff
#define HWIO_QFPROM_CORR_JTAG_ID_JTAG_ID_SHFT                                                         0x0

#define HWIO_QFPROM_CORR_PTE1_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004004)
#define HWIO_QFPROM_CORR_PTE1_PHYS                                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004004)
#define HWIO_QFPROM_CORR_PTE1_RMSK                                                             0xffffffff
#define HWIO_QFPROM_CORR_PTE1_POR                                                              0x00000000
#define HWIO_QFPROM_CORR_PTE1_POR_RMSK                                                         0x00000000
#define HWIO_QFPROM_CORR_PTE1_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE1_ADDR, HWIO_QFPROM_CORR_PTE1_RMSK)
#define HWIO_QFPROM_CORR_PTE1_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE1_ADDR, m)
#define HWIO_QFPROM_CORR_PTE1_IDDQ_BIN_BMSK                                                    0xe0000000
#define HWIO_QFPROM_CORR_PTE1_IDDQ_BIN_SHFT                                                          0x1d
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MULTIPLIER_BMSK                                             0x1c000000
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MULTIPLIER_SHFT                                                   0x1a
#define HWIO_QFPROM_CORR_PTE1_PROCESS_NODE_ID_BMSK                                              0x2000000
#define HWIO_QFPROM_CORR_PTE1_PROCESS_NODE_ID_SHFT                                                   0x19
#define HWIO_QFPROM_CORR_PTE1_PROCESS_NODE_ID_TN1_FVAL                                                0x0
#define HWIO_QFPROM_CORR_PTE1_PROCESS_NODE_ID_TN3_FVAL                                                0x1
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MX_OFF_BMSK                                                  0x1f80000
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MX_OFF_SHFT                                                       0x13
#define HWIO_QFPROM_CORR_PTE1_IDDQ_CX_OFF_BMSK                                                    0x7f000
#define HWIO_QFPROM_CORR_PTE1_IDDQ_CX_OFF_SHFT                                                        0xc
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MX_ON_BMSK                                                       0xfe0
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MX_ON_SHFT                                                         0x5
#define HWIO_QFPROM_CORR_PTE1_IDDQ_CX_ON_BMSK                                                        0x1f
#define HWIO_QFPROM_CORR_PTE1_IDDQ_CX_ON_SHFT                                                         0x0

#define HWIO_QFPROM_CORR_SERIAL_NUM_ADDR                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004008)
#define HWIO_QFPROM_CORR_SERIAL_NUM_PHYS                                                       (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004008)
#define HWIO_QFPROM_CORR_SERIAL_NUM_RMSK                                                       0xffffffff
#define HWIO_QFPROM_CORR_SERIAL_NUM_POR                                                        0x00000000
#define HWIO_QFPROM_CORR_SERIAL_NUM_POR_RMSK                                                   0x00000000
#define HWIO_QFPROM_CORR_SERIAL_NUM_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SERIAL_NUM_ADDR, HWIO_QFPROM_CORR_SERIAL_NUM_RMSK)
#define HWIO_QFPROM_CORR_SERIAL_NUM_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SERIAL_NUM_ADDR, m)
#define HWIO_QFPROM_CORR_SERIAL_NUM_SERIAL_NUM_BMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_SERIAL_NUM_SERIAL_NUM_SHFT                                                   0x0

#define HWIO_QFPROM_CORR_PTE2_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000400c)
#define HWIO_QFPROM_CORR_PTE2_PHYS                                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000400c)
#define HWIO_QFPROM_CORR_PTE2_RMSK                                                             0xffffffff
#define HWIO_QFPROM_CORR_PTE2_POR                                                              0x00000000
#define HWIO_QFPROM_CORR_PTE2_POR_RMSK                                                         0x00000000
#define HWIO_QFPROM_CORR_PTE2_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE2_ADDR, HWIO_QFPROM_CORR_PTE2_RMSK)
#define HWIO_QFPROM_CORR_PTE2_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE2_ADDR, m)
#define HWIO_QFPROM_CORR_PTE2_PTE_IDDQ_SPARE_BMSK                                              0xfc000000
#define HWIO_QFPROM_CORR_PTE2_PTE_IDDQ_SPARE_SHFT                                                    0x1a
#define HWIO_QFPROM_CORR_PTE2_IDDQ_MSS_OFF_BMSK                                                 0x3f80000
#define HWIO_QFPROM_CORR_PTE2_IDDQ_MSS_OFF_SHFT                                                      0x13
#define HWIO_QFPROM_CORR_PTE2_IDDQ_MSS_ON_BMSK                                                    0x7f800
#define HWIO_QFPROM_CORR_PTE2_IDDQ_MSS_ON_SHFT                                                        0xb
#define HWIO_QFPROM_CORR_PTE2_FOUNDRY_ID_BMSK                                                       0x700
#define HWIO_QFPROM_CORR_PTE2_FOUNDRY_ID_SHFT                                                         0x8
#define HWIO_QFPROM_CORR_PTE2_FOUNDRY_ID_TSMC_FVAL                                                    0x0
#define HWIO_QFPROM_CORR_PTE2_FOUNDRY_ID_GLOBAL_FOUNDRY_FVAL                                          0x1
#define HWIO_QFPROM_CORR_PTE2_FOUNDRY_ID_SAMSUNG_FVAL                                                 0x2
#define HWIO_QFPROM_CORR_PTE2_FOUNDRY_ID_IBM_FVAL                                                     0x3
#define HWIO_QFPROM_CORR_PTE2_FOUNDRY_ID_UMC_FVAL                                                     0x4
#define HWIO_QFPROM_CORR_PTE2_LOGIC_RETENTION_BMSK                                                   0xe0
#define HWIO_QFPROM_CORR_PTE2_LOGIC_RETENTION_SHFT                                                    0x5
#define HWIO_QFPROM_CORR_PTE2_SPEED_BIN_BMSK                                                         0x1c
#define HWIO_QFPROM_CORR_PTE2_SPEED_BIN_SHFT                                                          0x2
#define HWIO_QFPROM_CORR_PTE2_MX_RET_BIN_BMSK                                                         0x3
#define HWIO_QFPROM_CORR_PTE2_MX_RET_BIN_SHFT                                                         0x0

#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004010)
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004010)
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ADDR, HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RMSK)
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE_BMSK                                             0xffc00000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE_SHFT                                                   0x16
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE21_BMSK                                             0x200000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE21_SHFT                                                 0x15
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE21_ALLOW_READ_FVAL                                       0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE21_DISABLE_READ_FVAL                                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE20_BMSK                                             0x100000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE20_SHFT                                                 0x14
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE20_ALLOW_READ_FVAL                                       0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE20_DISABLE_READ_FVAL                                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE19_BMSK                                              0x80000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE19_SHFT                                                 0x13
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE19_ALLOW_READ_FVAL                                       0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE19_DISABLE_READ_FVAL                                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE18_BMSK                                              0x40000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE18_SHFT                                                 0x12
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE18_ALLOW_READ_FVAL                                       0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE18_DISABLE_READ_FVAL                                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE17_BMSK                                              0x20000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE17_SHFT                                                 0x11
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE17_ALLOW_READ_FVAL                                       0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE17_DISABLE_READ_FVAL                                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE16_BMSK                                              0x10000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE16_SHFT                                                 0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE16_ALLOW_READ_FVAL                                       0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE16_DISABLE_READ_FVAL                                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEC_EN_BMSK                                                0x8000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEC_EN_SHFT                                                   0xf
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEC_EN_ALLOW_READ_FVAL                                        0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEC_EN_DISABLE_READ_FVAL                                      0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_BOOT_ROM_PATCH_BMSK                                        0x4000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_BOOT_ROM_PATCH_SHFT                                           0xe
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_BOOT_ROM_PATCH_ALLOW_READ_FVAL                                0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_BOOT_ROM_PATCH_DISABLE_READ_FVAL                              0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_MEM_CONFIG_BMSK                                            0x2000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_MEM_CONFIG_SHFT                                               0xd
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_MEM_CONFIG_ALLOW_READ_FVAL                                    0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_MEM_CONFIG_DISABLE_READ_FVAL                                  0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CALIB_BMSK                                                 0x1000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CALIB_SHFT                                                    0xc
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CALIB_ALLOW_READ_FVAL                                         0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CALIB_DISABLE_READ_FVAL                                       0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PK_HASH_BMSK                                                0x800
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PK_HASH_SHFT                                                  0xb
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PK_HASH_ALLOW_READ_FVAL                                       0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PK_HASH_DISABLE_READ_FVAL                                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_QC_SEC_BOOT_BMSK                                            0x400
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_QC_SEC_BOOT_SHFT                                              0xa
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_QC_SEC_BOOT_ALLOW_READ_FVAL                                   0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_QC_SEC_BOOT_DISABLE_READ_FVAL                                 0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_SEC_BOOT_BMSK                                           0x200
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_SEC_BOOT_SHFT                                             0x9
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_SEC_BOOT_ALLOW_READ_FVAL                                  0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_SEC_BOOT_DISABLE_READ_FVAL                                0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_BMSK                                 0x100
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_SHFT                                   0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_ALLOW_READ_FVAL                        0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_DISABLE_READ_FVAL                      0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_BMSK                                  0x80
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_SHFT                                   0x7
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_ALLOW_READ_FVAL                        0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_DISABLE_READ_FVAL                      0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEAT_CONFIG_BMSK                                             0x40
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEAT_CONFIG_SHFT                                              0x6
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEAT_CONFIG_ALLOW_READ_FVAL                                   0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEAT_CONFIG_DISABLE_READ_FVAL                                 0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_CONFIG_BMSK                                              0x20
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_CONFIG_SHFT                                               0x5
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_CONFIG_ALLOW_READ_FVAL                                    0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_CONFIG_DISABLE_READ_FVAL                                  0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_BMSK                                         0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_SHFT                                          0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_ALLOW_READ_FVAL                               0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_DISABLE_READ_FVAL                             0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_BMSK                                          0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_SHFT                                          0x3
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_ALLOW_READ_FVAL                               0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_DISABLE_READ_FVAL                             0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_BMSK                                          0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_SHFT                                          0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_ALLOW_READ_FVAL                               0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_DISABLE_READ_FVAL                             0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RD_WR_PERM_BMSK                                               0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RD_WR_PERM_SHFT                                               0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RD_WR_PERM_ALLOW_READ_FVAL                                    0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RD_WR_PERM_DISABLE_READ_FVAL                                  0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PTE_BMSK                                                      0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PTE_SHFT                                                      0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PTE_ALLOW_READ_FVAL                                           0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PTE_DISABLE_READ_FVAL                                         0x1

#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004014)
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004014)
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ADDR, HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RMSK)
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE_BMSK                                             0xffc00000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE_SHFT                                                   0x16
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE21_BMSK                                             0x200000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE21_SHFT                                                 0x15
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE21_ALLOW_WRITE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE21_DISABLE_WRITE_FVAL                                    0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE20_BMSK                                             0x100000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE20_SHFT                                                 0x14
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE20_ALLOW_WRITE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE20_DISABLE_WRITE_FVAL                                    0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE19_BMSK                                              0x80000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE19_SHFT                                                 0x13
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE19_ALLOW_WRITE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE19_DISABLE_WRITE_FVAL                                    0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE18_BMSK                                              0x40000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE18_SHFT                                                 0x12
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE18_ALLOW_WRITE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE18_DISABLE_WRITE_FVAL                                    0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE17_BMSK                                              0x20000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE17_SHFT                                                 0x11
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE17_ALLOW_WRITE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE17_DISABLE_WRITE_FVAL                                    0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE16_BMSK                                              0x10000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE16_SHFT                                                 0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE16_ALLOW_WRITE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE16_DISABLE_WRITE_FVAL                                    0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEC_EN_BMSK                                                0x8000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEC_EN_SHFT                                                   0xf
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEC_EN_ALLOW_WRITE_FVAL                                       0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEC_EN_DISABLE_WRITE_FVAL                                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_BOOT_ROM_PATCH_BMSK                                        0x4000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_BOOT_ROM_PATCH_SHFT                                           0xe
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_BOOT_ROM_PATCH_ALLOW_WRITE_FVAL                               0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_BOOT_ROM_PATCH_DISABLE_WRITE_FVAL                             0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_MEM_CONFIG_BMSK                                            0x2000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_MEM_CONFIG_SHFT                                               0xd
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_MEM_CONFIG_ALLOW_WRITE_FVAL                                   0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_MEM_CONFIG_DISABLE_WRITE_FVAL                                 0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CALIB_BMSK                                                 0x1000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CALIB_SHFT                                                    0xc
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CALIB_ALLOW_WRITE_FVAL                                        0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CALIB_DISABLE_WRITE_FVAL                                      0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PK_HASH_BMSK                                                0x800
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PK_HASH_SHFT                                                  0xb
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PK_HASH_ALLOW_WRITE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PK_HASH_DISABLE_WRITE_FVAL                                    0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_QC_SEC_BOOT_BMSK                                            0x400
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_QC_SEC_BOOT_SHFT                                              0xa
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_QC_SEC_BOOT_ALLOW_WRITE_FVAL                                  0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_QC_SEC_BOOT_DISABLE_WRITE_FVAL                                0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_SEC_BOOT_BMSK                                           0x200
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_SEC_BOOT_SHFT                                             0x9
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_SEC_BOOT_ALLOW_WRITE_FVAL                                 0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_SEC_BOOT_DISABLE_WRITE_FVAL                               0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_BMSK                                 0x100
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_SHFT                                   0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_ALLOW_WRITE_FVAL                       0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_DISABLE_WRITE_FVAL                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_BMSK                                  0x80
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_SHFT                                   0x7
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_ALLOW_WRITE_FVAL                       0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_DISABLE_WRITE_FVAL                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEAT_CONFIG_BMSK                                             0x40
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEAT_CONFIG_SHFT                                              0x6
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEAT_CONFIG_ALLOW_WRITE_FVAL                                  0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEAT_CONFIG_DISABLE_WRITE_FVAL                                0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_CONFIG_BMSK                                              0x20
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_CONFIG_SHFT                                               0x5
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_CONFIG_ALLOW_WRITE_FVAL                                   0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_CONFIG_DISABLE_WRITE_FVAL                                 0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_BMSK                                         0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_SHFT                                          0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_ALLOW_WRITE_FVAL                              0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_DISABLE_WRITE_FVAL                            0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_BMSK                                          0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_SHFT                                          0x3
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_ALLOW_WRITE_FVAL                              0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_DISABLE_WRITE_FVAL                            0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_BMSK                                          0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_SHFT                                          0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_ALLOW_WRITE_FVAL                              0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_DISABLE_WRITE_FVAL                            0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RD_WR_PERM_BMSK                                               0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RD_WR_PERM_SHFT                                               0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RD_WR_PERM_ALLOW_WRITE_FVAL                                   0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RD_WR_PERM_DISABLE_WRITE_FVAL                                 0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PTE_BMSK                                                      0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PTE_SHFT                                                      0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PTE_ALLOW_WRITE_FVAL                                          0x0
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PTE_DISABLE_WRITE_FVAL                                        0x1

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004018)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004018)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_BMSK                               0xfc000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_SHFT                                     0x1a
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_TZ_BMSK                                            0x3fff000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_TZ_SHFT                                                  0xc
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_SBL1_BMSK                                              0xffe
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_SBL1_SHFT                                                0x1
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_BMSK                                0x1
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_SHFT                                0x0
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_RPMB_KEY_NOT_PROVISIONED_FVAL        0x0
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_RPMB_KEY_PROVISIONED_FVAL           0x1

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000401c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000401c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_APPSBL0_BMSK                                      0xfffc0000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_APPSBL0_SHFT                                            0x12
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_BMSK                                  0x3ffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_SHFT                                      0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004020)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004020)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_APPSBL1_BMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_APPSBL1_SHFT                                             0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004024)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004024)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_BMSK                      0xff000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_SHFT                            0x18
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_PRODUCTION_DEVICE_NO_CERTIFICATE_SELECTED_FVAL        0x0
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_15_FVAL        0xf
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_14_FVAL       0x1e
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_13_FVAL       0x2d
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_12_FVAL       0x3c
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_11_FVAL       0x4b
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_10_FVAL       0x5a
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_9_FVAL       0x69
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_8_FVAL       0x78
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_7_FVAL       0x87
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_6_FVAL       0x96
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_5_FVAL       0xa5
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_4_FVAL       0xb4
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_3_FVAL       0xc3
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_2_FVAL       0xd2
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_1_FVAL       0xe1
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_PRODUCTION_DEVICE_FIXED_TO_CERTIFICATE_0_FVAL       0xf0
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_HYPERVISOR_BMSK                                     0xfff000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_HYPERVISOR_SHFT                                          0xc
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RPM_BMSK                                               0xff0
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RPM_SHFT                                                 0x4
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_APPSBL2_BMSK                                             0xf
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_APPSBL2_SHFT                                             0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004028)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004028)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MSS_BMSK                                          0xffff0000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MSS_SHFT                                                0x10
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MBA_BMSK                                              0xffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MBA_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000402c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000402c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_SPARE0_BMSK                                       0xffffff00
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_SPARE0_SHFT                                              0x8
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_BMSK                      0xff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_SHFT                       0x0
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_PRODUCTION_DEVICE_NO_CERTIFICATE_SELECTED_FVAL        0x0
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_15_FVAL        0xf
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_14_FVAL       0x1e
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_13_FVAL       0x2d
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_12_FVAL       0x3c
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_11_FVAL       0x4b
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_10_FVAL       0x5a
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_9_FVAL       0x69
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_8_FVAL       0x78
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_7_FVAL       0x87
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_6_FVAL       0x96
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_5_FVAL       0xa5
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_4_FVAL       0xb4
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_3_FVAL       0xc3
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_2_FVAL       0xd2
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_DEVICE_FIXED_TO_CERTIFICATE_1_FVAL       0xe1
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_PRODUCTION_DEVICE_FIXED_TO_CERTIFICATE_0_FVAL       0xf0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004030)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004030)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE0_BMSK                                       0xff000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE0_SHFT                                             0x18
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_BMSK                             0x800000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_SHFT                                 0x17
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_NOT_SECURE_FVAL                       0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_SECURE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_BMSK                                0x400000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_SHFT                                    0x16
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_BMSK                            0x3c0000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_SHFT                                0x12
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_BMSK                              0x20000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_SHFT                                 0x11
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_NOT_SECURE_FVAL                       0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_SECURE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_BMSK                              0x10000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_SHFT                                 0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_NOT_SECURE_FVAL                       0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_SECURE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_BMSK                               0x8000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_SHFT                                  0xf
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_NOT_SECURE_FVAL                       0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_SECURE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_BMSK                               0x4000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_SHFT                                  0xe
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_NOT_SECURE_FVAL                       0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_SECURE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_BMSK                               0x2000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_SHFT                                  0xd
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_NOT_SECURE_FVAL                       0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_SECURE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_WDOG_EN_BMSK                                          0x1000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_WDOG_EN_SHFT                                             0xc
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_WDOG_EN_USE_GPIO_FVAL                                    0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_WDOG_EN_IGNORE_GPIO_ENABLE_WDOG_FVAL                     0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_BMSK                                  0x800
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_SHFT                                    0xb
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_NORMAL_MODE_FVAL                        0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_SECURE_MODE_FVAL                        0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_BMSK                              0x400
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_SHFT                                0xa
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_BOOT_FROM_SD_CARD_CONNECTED_ON_SDC2_PORT_FVAL        0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_BOOT_FROM_SD_CARD_CONNECTED_ON_SDC3_PORT_FVAL        0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_BMSK                     0x200
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_SHFT                       0x9
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_BMSK                               0x100
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_SHFT                                 0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_BMSK                                          0xe0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_SHFT                                           0x5
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_DEFAULT_FVAL                                   0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_PCIE_FVAL                                      0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_USB_FVAL                                       0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_EMMC_USB_FVAL                                  0x3
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_BMSK                                0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_SHFT                                 0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_ENUM_19_2_MHZ_FVAL                   0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_ENUM_25_MHZ_FVAL                     0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_BMSK                                 0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_SHFT                                 0x3
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_USE_FORCE_USB_BOOT_GPIO_TO_FORCE_BOOT_FROM_USB_FVAL        0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_NOT_USE_FORCE_USB_BOOT_PIN_FVAL        0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_BMSK                                               0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_SHFT                                               0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_BMSK                                        0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_SHFT                                        0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_TIMEOUT_DISABLED_FVAL                       0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_TIMEOUT_ENABLED_90S_FVAL                    0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_BMSK                                     0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_SHFT                                     0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_DOWNLOADER_ENABLED_FVAL                  0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_DOWNLOADER_DISABLED_FVAL                 0x1

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004034)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004034)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_BMSK                                       0xff800000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_SHFT                                             0x17
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_BMSK                       0x780000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_SHFT                           0x13
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_BMSK                            0x40000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_SHFT                               0x12
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_ENABLE_FVAL                         0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_DISABLE_FVAL                        0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_BMSK                             0x20000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_SHFT                                0x11
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_ENABLE_FVAL                          0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_DISABLE_FVAL                         0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_BMSK                              0x10000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_SHFT                                 0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_ENABLE_FVAL                           0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_DISABLE_FVAL                          0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_BMSK                                0x8000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_SHFT                                   0xf
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_DISABLE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_BMSK                                0x4000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_SHFT                                   0xe
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_DISABLE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_BMSK                             0x2000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_SHFT                                0xd
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_ENABLE_FVAL                         0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_DISABLE_FVAL                        0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_BMSK                              0x1000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_SHFT                                 0xc
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_ENABLE_FVAL                          0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_DISABLE_FVAL                         0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_BMSK                                0x800
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_SHFT                                  0xb
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_ENABLE_FVAL                           0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_DISABLE_FVAL                          0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_BMSK                                0x400
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_SHFT                                  0xa
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_ENABLE_FVAL                           0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_DISABLE_FVAL                          0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_BMSK                                    0x200
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_SHFT                                      0x9
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_BMSK                                    0x100
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_SHFT                                      0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_BMSK                              0x80
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_SHFT                               0x7
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_BMSK                                  0x40
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_SHFT                                   0x6
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_DISABLE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_BMSK                             0x20
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_SHFT                              0x5
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_ENABLE_FVAL                       0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_DISABLE_FVAL                      0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_BMSK                                  0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_SHFT                                   0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_DISABLE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_BMSK                                  0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_SHFT                                  0x3
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_ENABLE_FVAL                           0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_DISABLE_FVAL                          0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_BMSK                                   0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_SHFT                                   0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_DISABLE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_BMSK                                   0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_SHFT                                   0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_DISABLE_FVAL                           0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_BMSK                                   0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_SHFT                                   0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_DISABLE_FVAL                           0x1

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004038)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004038)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_BMSK                               0xffff0000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_SHFT                                     0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_BMSK                                        0xffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_SHFT                                           0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000403c)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000403c)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_VID_BMSK                                   0xffff0000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_VID_SHFT                                         0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_PID_BMSK                                       0xffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_PID_SHFT                                          0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004040)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004040)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_VU_EN_BMSK                            0xfc000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_VU_EN_SHFT                                  0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_NAV_EN_BMSK                                  0x2000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_NAV_EN_SHFT                                       0x19
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_LDO_EN_BMSK                                  0x1000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_LDO_EN_SHFT                                       0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_ECS_EN_BMSK                                   0x800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_ECS_EN_SHFT                                       0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_WCDMA_EN_BMSK                           0x600000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_WCDMA_EN_SHFT                               0x15
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_TDSCDMA_EN_BMSK                         0x180000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_TDSCDMA_EN_SHFT                             0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_EN_BMSK                              0x60000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_EN_SHFT                                 0x11
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT2_EN_BMSK                   0x18000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT2_EN_SHFT                       0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT1_EN_BMSK                    0x6000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT1_EN_SHFT                       0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_MIMO_EN_BMSK                         0x1800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_MIMO_EN_SHFT                            0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_EN_BMSK                               0x600
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_EN_SHFT                                 0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_DC_EN_BMSK                            0x180
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_DC_EN_SHFT                              0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_GENRAN_EN_BMSK                              0x60
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_GENRAN_EN_SHFT                               0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_EN_BMSK                                     0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_EN_SHFT                                      0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_DO_EN_BMSK                                   0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_DO_EN_SHFT                                   0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_1X_EN_BMSK                                   0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_1X_EN_SHFT                                   0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004044)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004044)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_BMSK                 0x80000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_SHFT                       0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE_BMSK                                       0x7ff00000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE_SHFT                                             0x14
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_MDSP_FW_EN_BMSK                                0xfff00
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_MDSP_FW_EN_SHFT                                    0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_SPARE_BMSK                                        0xc0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_SPARE_SHFT                                         0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_VU_EN_BMSK                                  0x3f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_VU_EN_SHFT                                   0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004048)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004048)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_BMSK                                 0x80000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_SHFT                                       0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_ENABLE_FVAL                                 0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_DISABLE_FVAL                                0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SHADOW_READ_DISABLE_BMSK                         0x40000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SHADOW_READ_DISABLE_SHFT                               0x1e
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SPARE1_BMSK                                      0x30000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SPARE1_SHFT                                            0x1c
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_SMT_PERM_ENABLE_BMSK                         0x8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_SMT_PERM_ENABLE_SHFT                              0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_RCP_BYPASS_ENABLE_BMSK                       0x4000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_RCP_BYPASS_ENABLE_SHFT                            0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_SMMU_BYPASS_DISABLE_BMSK                     0x2000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_SMMU_BYPASS_DISABLE_SHFT                          0x19
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_GPIO_SLIMBUS_PD_DISABLE_BMSK                 0x1000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_GPIO_SLIMBUS_PD_DISABLE_SHFT                      0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_PCIE20_RC_DISABLE_BMSK                        0x800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_PCIE20_RC_DISABLE_SHFT                            0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_PCIE_DISABLE_BMSK                             0x400000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_PCIE_DISABLE_SHFT                                 0x16
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_NDINT_DISABLE_BMSK                                 0x200000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_NDINT_DISABLE_SHFT                                     0x15
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_BMSK                       0x100000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_SHFT                           0x14
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_ENABLE_FVAL                     0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_DISABLE_FVAL                    0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_BMSK                         0x80000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_SHFT                            0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_ENABLE_FVAL                      0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_DISABLE_FVAL                     0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_BMSK                          0x40000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_SHFT                             0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_ENABLE_FVAL                       0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_DISABLE_FVAL                      0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_BMSK                           0x20000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_SHFT                              0x11
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_BMSK                           0x10000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_SHFT                              0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_BMSK                         0x8000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_SHFT                            0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_ENABLE_FVAL                     0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_DISABLE_FVAL                    0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_BMSK                          0x4000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_SHFT                             0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_ENABLE_FVAL                      0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_DISABLE_FVAL                     0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_BMSK                           0x2000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_SHFT                              0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_ENABLE_FVAL                       0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_DISABLE_FVAL                      0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_BMSK                           0x1000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_SHFT                              0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_ENABLE_FVAL                       0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_DISABLE_FVAL                      0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_BMSK                                0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_SHFT                                  0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_BMSK                                0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_SHFT                                  0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_BMSK                         0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_SHFT                           0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_ENABLE_FVAL                    0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_DISABLE_FVAL                   0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_BMSK                             0x100
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_SHFT                               0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_BMSK                         0x80
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_SHFT                          0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_ENABLE_FVAL                   0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_DISABLE_FVAL                  0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_BMSK                              0x40
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_SHFT                               0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_BMSK                             0x20
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_SHFT                              0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_ENABLE_FVAL                       0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_DISABLE_FVAL                      0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_BMSK                              0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_SHFT                               0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_BMSK                               0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_SHFT                               0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_ENABLE_FVAL                        0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_DISABLE_FVAL                       0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_BMSK                                   0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_SHFT                                   0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_ENABLE_FVAL                            0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_DISABLE_FVAL                           0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_BMSK                                    0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_SHFT                                    0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_ENABLE_FVAL                             0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_DISABLE_FVAL                            0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_BMSK                                        0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_SHFT                                        0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_ENABLE_FVAL                                 0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_DISABLE_FVAL                                0x1

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000404c)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000404c)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_BMSK                      0xf8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_SHFT                            0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_BMSK                     0x4000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_SHFT                          0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_BMSK                            0x3ffc000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_SHFT                                  0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SPARE1_BMSK                                          0x3c00
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SPARE1_SHFT                                             0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_BMSK                          0x3e0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_SHFT                            0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_BMSK                            0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_SHFT                             0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004050)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004050)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_BMSK            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_SHFT                   0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004054)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004054)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_STACKED_MEMORY_ID_BMSK                           0xf0000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_STACKED_MEMORY_ID_SHFT                                 0x1c
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APSS_BOOT_TRIGGER_DISABLE_BMSK                    0x8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APSS_BOOT_TRIGGER_DISABLE_SHFT                         0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_BMSK                           0x7800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_SHFT                                0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_BMSK                             0x780000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_SHFT                                 0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_CFG_BMSK                              0x40000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_CFG_SHFT                                 0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_DELAY_BMSK                            0x30000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_DELAY_SHFT                               0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_SEL_EN_USB_PHY_TUNE_BMSK              0x8000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_SEL_EN_USB_PHY_TUNE_SHFT                 0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_USB_SS_ENABLE_BMSK                                   0x4000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_USB_SS_ENABLE_SHFT                                      0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_BMSK                             0x3000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_SHFT                                0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_XO_FVAL                             0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_ENUM_384_MHZ_FVAL                   0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_ENUM_614_4_MHZ_FVAL                 0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_ENUM_998_4_MHZ_FVAL                 0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_BMSK                               0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_SHFT                                 0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MSA_ENA_BMSK                                          0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MSA_ENA_SHFT                                            0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_BMSK                                0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_SHFT                                  0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_MODEM_IMAGE_NOT_AUTHENTICATED_FVAL        0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_FORCE_MODEM_IMAGE_AUTHENTICATION_FVAL        0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_BMSK                                        0x180
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_SHFT                                          0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_BMSK                               0x40
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_SHFT                                0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_BMSK                            0x20
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_SHFT                             0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_ENABLE_PATCHING_FVAL             0x0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_DISABLE_PATCHING_FVAL            0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_BMSK                 0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_SHFT                  0x0

#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004058 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_PHYS(n)                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004058 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                           3
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_POR                                   0x00000000
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_POR_RMSK                              0x00000000
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                        0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                               0x0

#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000405c + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_PHYS(n)                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000405c + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                           3
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_POR                                   0x00000000
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_POR_RMSK                              0x00000000
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                        0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                               0x0

#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004078 + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_PHYS(n)                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004078 + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                           3
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_POR                                   0x00000000
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_POR_RMSK                              0x00000000
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                        0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                               0x0

#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000407c + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_PHYS(n)                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000407c + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                           3
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_POR                                   0x00000000
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_POR_RMSK                              0x00000000
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                        0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                               0x0

#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004098)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_PHYS                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004098)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_POR                                             0x00000000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_POR_RMSK                                        0x00000000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_BMSK                                  0xff000000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_SHFT                                        0x18
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_BMSK                                    0xff0000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_SHFT                                        0x10
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_BMSK                                      0xff00
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_SHFT                                         0x8
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_BMSK                                        0xff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_SHFT                                         0x0

#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000409c)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_PHYS                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000409c)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_RMSK                                              0xffffff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_POR                                             0x00000000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_POR_RMSK                                        0xff000000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_BMSK                                    0xff0000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_SHFT                                        0x10
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_BMSK                                      0xff00
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_SHFT                                         0x8
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_BMSK                                        0xff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_SHFT                                         0x0

#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040a0)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040a0)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT4_BMSK                                   0xff000000
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT4_SHFT                                         0x18
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT3_BMSK                                     0xff0000
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT3_SHFT                                         0x10
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT2_BMSK                                       0xff00
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT2_SHFT                                          0x8
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT1_BMSK                                         0xff
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT1_SHFT                                          0x0

#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040a4)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040a4)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_POR_RMSK                                         0xff000000
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT7_BMSK                                     0xff0000
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT7_SHFT                                         0x10
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT6_BMSK                                       0xff00
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT6_SHFT                                          0x8
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT5_BMSK                                         0xff
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT5_SHFT                                          0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_ADDR(n)                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040a8 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_PHYS(n)                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040a8 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_MAXn                                                          3
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_POR                                                  0x00000000
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_POR_RMSK                                             0x00000000
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_HASH_DATA0_BMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_HASH_DATA0_SHFT                                             0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_ADDR(n)                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040ac + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_PHYS(n)                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040ac + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_MAXn                                                          3
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_POR                                                  0x00000000
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_POR_RMSK                                             0xff000000
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_HASH_DATA1_BMSK                                        0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_HASH_DATA1_SHFT                                             0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040c8)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_PHYS                                                 (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040c8)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_POR                                                  0x00000000
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_POR_RMSK                                             0x00000000
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_ADDR, HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_HASH_DATA0_BMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_HASH_DATA0_SHFT                                             0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040cc)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_PHYS                                                 (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040cc)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_POR                                                  0x00000000
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_POR_RMSK                                             0xff000000
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_ADDR, HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_QFPROM_CORR_PK_HASH_ROW4_MSB_BMSK                      0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_QFPROM_CORR_PK_HASH_ROW4_MSB_SHFT                           0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040d0)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040d0)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS2_POINT1_BMSK                                     0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS2_POINT1_SHFT                                           0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS1_POINT1_BMSK                                      0xf000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS1_POINT1_SHFT                                           0x18
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS0_POINT1_BMSK                                       0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS0_POINT1_SHFT                                           0x14
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE1_BMSK                                          0xffc00
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE1_SHFT                                              0xa
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE0_BMSK                                            0x3ff
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE0_SHFT                                              0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040d4)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040d4)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_USB_PHY_TUNING_BMSK                                      0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_USB_PHY_TUNING_SHFT                                          0x14
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE_BMSK                                                0xff800
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE_SHFT                                                    0xb
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_CAL_SEL_BMSK                                          0x700
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_CAL_SEL_SHFT                                            0x8
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS4_POINT1_BMSK                                           0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS4_POINT1_SHFT                                            0x4
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS3_POINT1_BMSK                                            0xf
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS3_POINT1_SHFT                                            0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040d8)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040d8)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_0_BMSK                   0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_0_SHFT                         0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS2_BMSK                               0x7c000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS2_SHFT                                     0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_1_BMSK                    0x2000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_1_SHFT                         0x19
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS_BMSK                                 0x1f00000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS_SHFT                                      0x14
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_2_BMSK                      0x80000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_2_SHFT                         0x13
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_NOM_BMSK                                   0x7c000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_NOM_SHFT                                       0xe
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_3_BMSK                       0x2000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_3_SHFT                          0xd
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_TUR_BMSK                                    0x1f00
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_TUR_SHFT                                       0x8
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR1_TARG_VOLT_OFFSET_SVS2_BMSK                              0x80
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR1_TARG_VOLT_OFFSET_SVS2_SHFT                               0x7
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_NOM_BMSK                               0x78
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_NOM_SHFT                                0x3
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_TUR_BMSK                                0x7
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_TUR_SHFT                                0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040dc)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040dc)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_0_BMSK                     0x800000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_0_SHFT                         0x17
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS2_BMSK                                 0x7c0000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS2_SHFT                                     0x12
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_1_BMSK                      0x20000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_1_SHFT                         0x11
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS_BMSK                                   0x1f000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS_SHFT                                       0xc
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_2_BMSK                        0x800
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_2_SHFT                          0xb
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_NOM_BMSK                                     0x7c0
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_NOM_SHFT                                       0x6
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_3_BMSK                         0x20
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_3_SHFT                          0x5
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_TUR_BMSK                                      0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_TUR_SHFT                                       0x0

#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040e0)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040e0)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_TARGET1_BMSK                                       0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_TARGET1_SHFT                                             0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_TARGET0_BMSK                                        0xfc00000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_TARGET0_SHFT                                             0x16
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_ENABLE_BMSK                                          0x200000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_ENABLE_SHFT                                              0x15
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS2_BMSK                          0x1e0000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS2_SHFT                              0x11
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BMSK                            0x1c000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_SHFT                                0xe
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR2_TARG_VOLT_NOM_BMSK                                    0x3e00
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR2_TARG_VOLT_NOM_SHFT                                       0x9
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BIT_3_BMSK                        0x100
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BIT_3_SHFT                          0x8
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR2_TARG_VOLT_TUR_BMSK                                      0xf8
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR2_TARG_VOLT_TUR_SHFT                                       0x3
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR_REV_BMSK                                                  0x7
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR_REV_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040e4)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040e4)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BMSK                          0xc00000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_SHFT                              0x16
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS_BMSK                           0x3c0000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS_SHFT                               0x12
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_NOM_BMSK                            0x3c000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_NOM_SHFT                                0xe
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_TUR_BMSK                             0x3c00
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_TUR_SHFT                                0xa
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_SPARE_BMSK                                                  0x300
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_SPARE_SHFT                                                    0x8
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_VCM_BMSK                                        0xc0
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_VCM_SHFT                                         0x6
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_LDO_BMSK                                        0x30
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_LDO_SHFT                                         0x4
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_VREF_BMSK                                        0xc
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_VREF_SHFT                                        0x2
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_LDO_ENABLE_SVS2_BMSK                                          0x2
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_LDO_ENABLE_SVS2_SHFT                                          0x1
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_LDO_TARGET1_BMSK                                              0x1
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_LDO_TARGET1_SHFT                                              0x0

#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040e8)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040e8)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW3_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_BHS_SLOPE_BMSK                                     0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_BHS_SLOPE_SHFT                                           0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_BHS_INTERCEPT_BMSK                                 0x7fe00000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_BHS_INTERCEPT_SHFT                                       0x15
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_LDO_SLOPE_BMSK                                       0x1ff800
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_LDO_SLOPE_SHFT                                            0xb
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_LDO_INTERCEPT_BMSK                                      0x7fe
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_LDO_INTERCEPT_SHFT                                        0x1
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_ENABLE_BMSK                                               0x1
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_ENABLE_SHFT                                               0x0

#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040ec)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040ec)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW3_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_G_B1_BMSK                                                0xe00000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_G_B1_SHFT                                                    0x15
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_SAR_LDO_ERR0_BMSK                                        0x180000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_SAR_LDO_ERR0_SHFT                                            0x13
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR2_TARG_VOLT_OFFSET_TUR_BIT_3_BMSK                      0x40000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR2_TARG_VOLT_OFFSET_TUR_BIT_3_SHFT                         0x12
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BIT_3_BMSK                     0x20000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BIT_3_SHFT                        0x11
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ECS_MISC_BMSK                                             0x1fe00
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ECS_MISC_SHFT                                                 0x9
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ECS_BHS_SLOPE_BMSK                                          0x1ff
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ECS_BHS_SLOPE_SHFT                                            0x0

#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040f0)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040f0)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW4_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M2_BMSK                                           0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M2_SHFT                                                 0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M1_BMSK                                           0x38000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M1_SHFT                                                 0x1b
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M0_BMSK                                            0x7000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M0_SHFT                                                 0x18
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_Q6SS0_LDO_VREF_TRIM_BMSK                                 0xf80000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_Q6SS0_LDO_VREF_TRIM_SHFT                                     0x13
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CLK_LDO_ERR0_BMSK                                         0x60000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CLK_LDO_ERR0_SHFT                                            0x11
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_VREF_ERR_B0_BMSK                                          0x18000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_VREF_ERR_B0_SHFT                                              0xf
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M3_BMSK                                               0x7000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M3_SHFT                                                  0xc
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M2_BMSK                                                0xe00
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M2_SHFT                                                  0x9
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M1_BMSK                                                0x1c0
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M1_SHFT                                                  0x6
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M0_BMSK                                                 0x38
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M0_SHFT                                                  0x3
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_G_B0_BMSK                                                     0x7
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_G_B0_SHFT                                                     0x0

#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040f4)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040f4)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW4_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M3_BMSK                                             0xc00000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M3_SHFT                                                 0x16
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M2_BMSK                                             0x380000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M2_SHFT                                                 0x13
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M1_BMSK                                              0x70000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M1_SHFT                                                 0x10
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M0_BMSK                                               0xe000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M0_SHFT                                                  0xd
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_G_B2_BMSK                                                  0x1c00
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_G_B2_SHFT                                                     0xa
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_SAR_LDO_ERR1_BMSK                                           0x300
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_SAR_LDO_ERR1_SHFT                                             0x8
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CLK_LDO_ERR1_BMSK                                            0xc0
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CLK_LDO_ERR1_SHFT                                             0x6
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_VREF_ERR_B1_BMSK                                             0x30
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_VREF_ERR_B1_SHFT                                              0x4
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B1M3_BMSK                                                  0xe
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B1M3_SHFT                                                  0x1
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B1M2_BMSK                                                  0x1
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B1M2_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040f8)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040f8)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW5_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B4M0_BMSK                                           0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B4M0_SHFT                                                 0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_G_B4_BMSK                                              0x70000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_G_B4_SHFT                                                    0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_SAR_LDO_ERR3_BMSK                                       0xc000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_SAR_LDO_ERR3_SHFT                                            0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CLK_LDO_ERR3_BMSK                                       0x3000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CLK_LDO_ERR3_SHFT                                            0x18
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_VREF_ERR_B3_BMSK                                         0xc00000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_VREF_ERR_B3_SHFT                                             0x16
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M3_BMSK                                             0x380000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M3_SHFT                                                 0x13
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M2_BMSK                                              0x70000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M2_SHFT                                                 0x10
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M1_BMSK                                               0xe000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M1_SHFT                                                  0xd
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M0_BMSK                                               0x1c00
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M0_SHFT                                                  0xa
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_G_B3_BMSK                                                   0x380
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_G_B3_SHFT                                                     0x7
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_SAR_LDO_ERR2_BMSK                                            0x60
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_SAR_LDO_ERR2_SHFT                                             0x5
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CLK_LDO_ERR2_BMSK                                            0x18
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CLK_LDO_ERR2_SHFT                                             0x3
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_VREF_ERR_B2_BMSK                                              0x6
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_VREF_ERR_B2_SHFT                                              0x1
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B2M3_BMSK                                                  0x1
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B2M3_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040fc)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040fc)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW5_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B5M1_BMSK                                             0x800000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B5M1_SHFT                                                 0x17
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B5M0_BMSK                                             0x700000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B5M0_SHFT                                                 0x14
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_G_B5_BMSK                                                 0xe0000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_G_B5_SHFT                                                    0x11
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_SAR_LDO_ERR4_BMSK                                         0x18000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_SAR_LDO_ERR4_SHFT                                             0xf
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CLK_LDO_ERR4_BMSK                                          0x6000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CLK_LDO_ERR4_SHFT                                             0xd
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_VREF_ERR_B4_BMSK                                           0x1800
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_VREF_ERR_B4_SHFT                                              0xb
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M3_BMSK                                                0x700
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M3_SHFT                                                  0x8
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M2_BMSK                                                 0xe0
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M2_SHFT                                                  0x5
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M1_BMSK                                                 0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M1_SHFT                                                  0x2
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M0_BMSK                                                  0x3
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004100)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004100)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW6_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CLK_LDO_ERR6_BMSK                                      0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CLK_LDO_ERR6_SHFT                                            0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_ERR_B6_BMSK                                       0x60000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_ERR_B6_SHFT                                             0x1d
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M3_BMSK                                           0x1c000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M3_SHFT                                                 0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M2_BMSK                                            0x3800000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M2_SHFT                                                 0x17
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M1_BMSK                                             0x700000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M1_SHFT                                                 0x14
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M0_BMSK                                              0xe0000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M0_SHFT                                                 0x11
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_G_B6_BMSK                                                 0x1c000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_G_B6_SHFT                                                     0xe
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_SAR_LDO_ERR5_BMSK                                          0x3000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_SAR_LDO_ERR5_SHFT                                             0xc
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CLK_LDO_ERR5_BMSK                                           0xc00
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CLK_LDO_ERR5_SHFT                                             0xa
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_ERR_B5_BMSK                                            0x300
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_ERR_B5_SHFT                                              0x8
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M3_BMSK                                                 0xe0
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M3_SHFT                                                  0x5
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M2_BMSK                                                 0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M2_SHFT                                                  0x2
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M1_BMSK                                                  0x3
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004104)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004104)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_POR                                                    0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_POR_RMSK                                               0x00000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW6_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_BMSK                                  0xff0000
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_SHFT                                      0x10
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_BMSK                                    0xff00
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_SHFT                                       0x8
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_AVG_ERROR_BMSK                            0x80
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_AVG_ERROR_SHFT                             0x7
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_AVG_ERROR_BMSK                            0x40
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_AVG_ERROR_SHFT                             0x6
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC_0_1_FUSEFLAG_BMSK                                0x20
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC_0_1_FUSEFLAG_SHFT                                 0x5
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_OVERFLOW_BMSK                             0x10
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_OVERFLOW_SHFT                              0x4
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_OVERFLOW_BMSK                              0x8
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_OVERFLOW_SHFT                              0x3
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_SAR_LDO_ERR6_BMSK                                             0x6
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_SAR_LDO_ERR6_SHFT                                             0x1
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CLK_LDO_ERR6_BMSK                                             0x1
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CLK_LDO_ERR6_SHFT                                             0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004108 + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_PHYS(n)                                           (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004108 + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_MAXn                                                      15
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_POR_RMSK                                          0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_REDUN_DATA_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_REDUN_DATA_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000410c + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_PHYS(n)                                           (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000410c + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_RMSK                                                0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_MAXn                                                      15
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_POR                                               0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_POR_RMSK                                          0xff000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_REDUN_DATA_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_REDUN_DATA_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004188)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004188)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000418c)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000418c)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_POR_RMSK                                         0xff000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004190)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004190)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004194)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004194)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_POR_RMSK                                         0xff000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004198)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004198)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000419c)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000419c)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_POR_RMSK                                         0xff000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041a0)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041a0)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041a4)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041a4)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_POR_RMSK                                         0xff000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041a8)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041a8)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_POR_RMSK                                         0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041ac)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041ac)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_POR                                              0x00000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_POR_RMSK                                         0xff000000
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041b0 + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_PHYS(n)                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041b0 + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_MAXn                                                       31
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_POR                                                0x00000000
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_POR_RMSK                                           0x00000000
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_PATCH_DATA_BMSK                                    0xffffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_PATCH_DATA_SHFT                                           0x0

#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041b4 + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PHYS(n)                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041b4 + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_RMSK                                                 0xffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_MAXn                                                       31
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_POR                                                0x00000000
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_POR_RMSK                                           0xff000000
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_SPARE3_BMSK                                          0xfe0000
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_SPARE3_SHFT                                              0x11
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_ADDR_BMSK                                       0x1fffe
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_ADDR_SHFT                                           0x1
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_EN_BMSK                                             0x1
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_EN_SHFT                                             0x0

#define HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042b0)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_PHYS                                                       (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042b0)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_RMSK                                                       0xffffffff
#define HWIO_QFPROM_CORR_FEC_EN_LSB_POR                                                        0x00000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_POR_RMSK                                                   0x00000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR, HWIO_QFPROM_CORR_FEC_EN_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR,v)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR,m,v,HWIO_QFPROM_CORR_FEC_EN_LSB_IN)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION31_FEC_EN_BMSK                                       0x80000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION31_FEC_EN_SHFT                                             0x1f
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION31_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION31_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION30_FEC_EN_BMSK                                       0x40000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION30_FEC_EN_SHFT                                             0x1e
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION30_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION30_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION29_FEC_EN_BMSK                                       0x20000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION29_FEC_EN_SHFT                                             0x1d
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION29_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION29_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION28_FEC_EN_BMSK                                       0x10000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION28_FEC_EN_SHFT                                             0x1c
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION28_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION28_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION27_FEC_EN_BMSK                                        0x8000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION27_FEC_EN_SHFT                                             0x1b
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION27_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION27_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION26_FEC_EN_BMSK                                        0x4000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION26_FEC_EN_SHFT                                             0x1a
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION26_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION26_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION25_FEC_EN_BMSK                                        0x2000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION25_FEC_EN_SHFT                                             0x19
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION25_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION25_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION24_FEC_EN_BMSK                                        0x1000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION24_FEC_EN_SHFT                                             0x18
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION24_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION24_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION23_FEC_EN_BMSK                                         0x800000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION23_FEC_EN_SHFT                                             0x17
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION23_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION23_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION22_FEC_EN_BMSK                                         0x400000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION22_FEC_EN_SHFT                                             0x16
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION22_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION22_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION21_FEC_EN_BMSK                                         0x200000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION21_FEC_EN_SHFT                                             0x15
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION21_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION21_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION20_FEC_EN_BMSK                                         0x100000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION20_FEC_EN_SHFT                                             0x14
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION20_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION20_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION19_FEC_EN_BMSK                                          0x80000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION19_FEC_EN_SHFT                                             0x13
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION19_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION19_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION18_FEC_EN_BMSK                                          0x40000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION18_FEC_EN_SHFT                                             0x12
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION18_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION18_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION17_FEC_EN_BMSK                                          0x20000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION17_FEC_EN_SHFT                                             0x11
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION17_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION17_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION16_FEC_EN_BMSK                                          0x10000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION16_FEC_EN_SHFT                                             0x10
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION16_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION16_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION15_FEC_EN_BMSK                                           0x8000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION15_FEC_EN_SHFT                                              0xf
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION15_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION15_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION14_FEC_EN_BMSK                                           0x4000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION14_FEC_EN_SHFT                                              0xe
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION14_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION14_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION13_FEC_EN_BMSK                                           0x2000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION13_FEC_EN_SHFT                                              0xd
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION13_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION13_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION12_FEC_EN_BMSK                                           0x1000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION12_FEC_EN_SHFT                                              0xc
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION12_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION12_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION11_FEC_EN_BMSK                                            0x800
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION11_FEC_EN_SHFT                                              0xb
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION11_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION11_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION10_FEC_EN_BMSK                                            0x400
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION10_FEC_EN_SHFT                                              0xa
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION10_FEC_EN_DISABLE_FVAL                                      0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION10_FEC_EN_ENABLE_FVAL                                       0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION9_FEC_EN_BMSK                                             0x200
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION9_FEC_EN_SHFT                                               0x9
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION9_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION9_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION8_FEC_EN_BMSK                                             0x100
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION8_FEC_EN_SHFT                                               0x8
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION8_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION8_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION7_FEC_EN_BMSK                                              0x80
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION7_FEC_EN_SHFT                                               0x7
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION7_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION7_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION6_FEC_EN_BMSK                                              0x40
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION6_FEC_EN_SHFT                                               0x6
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION6_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION6_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION5_FEC_EN_BMSK                                              0x20
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION5_FEC_EN_SHFT                                               0x5
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION5_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION5_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION4_FEC_EN_BMSK                                              0x10
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION4_FEC_EN_SHFT                                               0x4
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION4_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION4_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION3_FEC_EN_BMSK                                               0x8
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION3_FEC_EN_SHFT                                               0x3
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION3_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION3_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION2_FEC_EN_BMSK                                               0x4
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION2_FEC_EN_SHFT                                               0x2
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION2_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION2_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION1_FEC_EN_BMSK                                               0x2
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION1_FEC_EN_SHFT                                               0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION1_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION1_FEC_EN_ENABLE_FVAL                                        0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION0_FEC_EN_BMSK                                               0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION0_FEC_EN_SHFT                                               0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION0_FEC_EN_DISABLE_FVAL                                       0x0
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION0_FEC_EN_ENABLE_FVAL                                        0x1

#define HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042b4)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_PHYS                                                       (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042b4)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_RMSK                                                       0xffffffff
#define HWIO_QFPROM_CORR_FEC_EN_MSB_POR                                                        0x00000000
#define HWIO_QFPROM_CORR_FEC_EN_MSB_POR_RMSK                                                   0x00000000
#define HWIO_QFPROM_CORR_FEC_EN_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR, HWIO_QFPROM_CORR_FEC_EN_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_FEC_EN_REDUNDANCY_BMSK                                     0xffffffff
#define HWIO_QFPROM_CORR_FEC_EN_MSB_FEC_EN_REDUNDANCY_SHFT                                            0x0

#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042b8)
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042b8)
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_POR_RMSK                                              0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG16_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG16_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG16_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042bc)
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042bc)
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_POR_RMSK                                              0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG16_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG16_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG16_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042c0)
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042c0)
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_POR_RMSK                                              0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG17_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG17_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG17_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042c4)
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042c4)
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_POR_RMSK                                              0xff000000
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG17_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG17_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG17_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042c8)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042c8)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_POR_RMSK                                              0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG18_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042cc)
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042cc)
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_POR_RMSK                                              0xff000000
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG18_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042d0)
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042d0)
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_POR_RMSK                                              0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG19_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042d4)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042d4)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_POR_RMSK                                              0xff000000
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG19_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR,v)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR,m,v,HWIO_QFPROM_CORR_SPARE_REG19_MSB_IN)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042d8)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042d8)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_POR_RMSK                                              0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG20_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR,v)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR,m,v,HWIO_QFPROM_CORR_SPARE_REG20_LSB_IN)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042dc)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042dc)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_POR_RMSK                                              0xff000000
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG20_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR,v)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR,m,v,HWIO_QFPROM_CORR_SPARE_REG20_MSB_IN)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042e0)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042e0)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_POR_RMSK                                              0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG21_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR,v)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR,m,v,HWIO_QFPROM_CORR_SPARE_REG21_LSB_IN)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042e4)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042e4)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_POR                                                   0x00000000
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_POR_RMSK                                              0xff000000
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG21_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR,v)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR,m,v,HWIO_QFPROM_CORR_SPARE_REG21_MSB_IN)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n)                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004400 + 0x4 * (n))
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_PHYS(n)                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004400 + 0x4 * (n))
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_RMSK                                                     0xffffffff
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_MAXn                                                             39
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_POR                                                      0x00000000
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_POR_RMSK                                                 0x00000000
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n), HWIO_QFPROM_CORR_ACC_PRIVATEn_RMSK)
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ACC_PRIVATE_BMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ACC_PRIVATE_SHFT                                                0x0

#define HWIO_QFPROM_CLK_CTL_ADDR                                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006078)
#define HWIO_QFPROM_CLK_CTL_PHYS                                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006078)
#define HWIO_QFPROM_CLK_CTL_RMSK                                                                      0x1
#define HWIO_QFPROM_CLK_CTL_POR                                                                0x00000000
#define HWIO_QFPROM_CLK_CTL_POR_RMSK                                                           0xffffffff
#define HWIO_QFPROM_CLK_CTL_IN          \
        in_dword_masked(HWIO_QFPROM_CLK_CTL_ADDR, HWIO_QFPROM_CLK_CTL_RMSK)
#define HWIO_QFPROM_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CLK_CTL_ADDR, m)
#define HWIO_QFPROM_CLK_CTL_OUT(v)      \
        out_dword(HWIO_QFPROM_CLK_CTL_ADDR,v)
#define HWIO_QFPROM_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CLK_CTL_ADDR,m,v,HWIO_QFPROM_CLK_CTL_IN)
#define HWIO_QFPROM_CLK_CTL_CLK_HALT_BMSK                                                             0x1
#define HWIO_QFPROM_CLK_CTL_CLK_HALT_SHFT                                                             0x0
#define HWIO_QFPROM_CLK_CTL_CLK_HALT_CLK_ENABLED_FVAL                                                 0x0
#define HWIO_QFPROM_CLK_CTL_CLK_HALT_CLK_DISABLED_FVAL                                                0x1


#endif /* __MCS_HWIO_MODEM_QFPROM_H__ */
