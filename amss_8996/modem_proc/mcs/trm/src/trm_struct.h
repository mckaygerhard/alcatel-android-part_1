#ifndef TRM_STRUCT_H
#define TRM_STRUCT_H
/*===========================================================================

                   T R M    S T R U C T   H E A D E R    F I L E

DESCRIPTION
   This file contains the declaration of TRM data structure.

  Copyright (c) 2014-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/trm/src/trm_struct.h#2 $

when         who     what, where, why
--------     ---     ----------------------------------------------------------
12/03/2015   mn      In ANY chain assignment logic, check the chain assignment with 
                      the chain owners if the client needs the chain ASAP. (CR: 945645)
10/25/2015   sk      LTE Tx denial issue fix(929556)
10/12/2015   sk      Provide devices sent unlock due to cobanded scenario as
                      conflicting clients/devices(920433)
09/29/2015   sg      TRM state info is made pointer based (CR: 916464)
08/21/2015   mn      Changes for LTE CA device hopping (CR: 894455).
07/29/2015   mn      Keep DR<->SR vote pending in a QTA gap. (CR: 875830).
07/27/2015   sp      Porting of operating mode log packet info (CR: 816022)
07/01/2015   mn      Handle data tech passing all devices in START QTA (CR: 864477)
06/25/2015   mn      Changes for LTA override in a 3 tech scenario for 
                      SRLTE + G DR-DSDS (CR: 859830).
05/13/2015   sk      Support for device sharing
06/12/2015   rj      Not updating END_QTA to CXM, if START_QTA is not updated 
06/08/2015   sp      Changes for 7mode dynamic PBR bias (825586) 
06/03/2015   mn      Make sure data tech has released the required devices 
                      before granting the idle tech devices in a QTA (CR: 846348).
04/18/2015   sk      Support for grouped APIs/async callbacks
03/12/2015   sp      Added tech-based restriction for DR mode 
02/18/2015   sk      Update log v8 for eMBMS logging(796676)
01/23/2015   jm      Adding page request attempts to TRM metrics (CR: 778020)
01/21/2015   jm      Adding TRM metric query support (CR: 778020)
12/17/2014   mn      Removing unwanted files from TRM. (CR: 687777).
12/12/2014   sk      Logging Enhancement(737879)
10/13/2014   sr      Storing alt path information(728163)
10/08/2014   sk      Added support for cross WTR PBR(708194)
09/17/2014   sk      Support for Tx Sharing DSDA changes
07/24/2014   mn      TRM needs to keep change reason pending if it is not 
                      allowed immediately. (CR: 694927).
07/18/2014   mn      1xSRLTE:- Requirement to default PBR bias values fro 1x 
                      and SLTE in SW code (CR: 681568).
07/01/2014   sk      Added support for SIM swap without reset(665739) 
06/26/2014   mn      NV control to enable/disable DR-DSDS (CR: 685806)
06/24/2014   mn      Adding support for num pending callbacks in req and not,
                      req and not enhanced and unlock callbacks. (CR: 684638).
04/11/2014   mn      Refactoring TRM

===========================================================================*/

#include "customer.h"
#include "trm_client.h"
#include "trm_client_array.h"
#include "trmlog.h"
#include "trm_band_handler.h"
#include "trm_device_ordering.h"
#include "trmi.h"

#define TRM_CLIENT_SEARCH_ACTIVITY_Q_SIZE               35
#define TRM_METRICS_MAX_TBL_SIZE                        50
#define TRM_MAX_METRIC_MASK_SIZE                         8

/*===========================================================================

                              HEADER CONSTANTS

  Numeric values for the identifiers in this section should not be changed.

===========================================================================*/

/*----------------------------------------------------------------------------
  TRM Initialization State
----------------------------------------------------------------------------*/

enum trm_init_status
{
  /* TRM init has not been called */
  TRM_NOT_INITIALIZED,

  /* TRM init is in progress (waiting for NV read?) */
  TRM_INITIALIZING,

  /* TRM init has completed */
  TRM_INITIALIZED
};

/*----------------------------------------------------------------------------
  RF h/w capabilities
----------------------------------------------------------------------------*/
struct trm_rf_hw_struct
{
  /* Configuration of all RF devices  */
  rfm_devices_configuration_type  config;

  /* RF configuration (single, dual dependent/independent, ... ) */
  trm_rf_enum_t             rf_type;

  /* Number of antenna */
  uint8                     num_ant;

  /* bit mask to determine capability of RF. We are reusing the 
     configuration from get simultaneous capability which uses SHDR for
     bit 0. It is no-op here 
     Bit0       : PAM
     Bit1-SVDO  : TRM_SVDO_IS_ENABLED
     Bit2-SVLTE : TRM_SVLTE_IS_ENABLED
     Bit3       : Reserved.
     Bit4-DSDA  : TRM_DSDA_IS_ENABLED
     Bit 5      : TRM_SGLTE_SGTDS_IS_ENABLED
  */
  uint32                     capability;
};

/*----------------------------------------------------------------------------
  RF Chain structure type
----------------------------------------------------------------------------*/

struct trm_chain_struct
{
  /* Current owner of the RF chain */
  TRMClientArray            owners;

  /* Time when the chain would be available */
  int64                     available;

  /* Alt Path Information Stored from RF */
  uint8                     rf_alt_path;

  /* Preferred associated Rx device */
  trm_group                 associated_rx_group;

  /* Preferred associated Tx device */
  trm_group                 associated_tx_group;

  /* Device capability */
  uint32                    capability;
};

/*----------------------------------------------------------------------------
  This store data for QTA per RAT. 
----------------------------------------------------------------------------*/
typedef struct
{
  /* RF Source tech data (would be RAT specific) for Script Building */
  rfm_meas_common_param_type* rf_params;

  /* Flag to indicate whether previous QTA Gap was Empty */
  boolean                     qta_gap_empty;

  /* Source client info in Source to Target QTA */
  trm_client_enum_t           src_client;

  /* Number data tech clients */
  uint8                       num_data_clients;
  
  /* This is an array of data tech clients that were holding chains at the time of 
     START QTA and were requested for an unlock */
  trm_client_enum_t           data_clients[TRM_ACTIVE_CLIENTS_MAX];

  /* TRM sends unlock to all cobanded techs even though there is not any actual
     conflict, while granting idle tech devices, TRM shouldnt check if such
     techs have released devices for assigning devices
     flag to indicate if conflict was due to co-banded scenario */
  boolean                     is_cobanded_conflict[TRM_ACTIVE_CLIENTS_MAX];

  /* Flag to indicate whether CXM is updated regarding START_QTA */
  boolean                     cxm_updated;
} trm_per_rat_qta_info_type;

/*----------------------------------------------------------------------------
  Per RAT information.
----------------------------------------------------------------------------*/
typedef struct
{
  /* Number of grant callbacks pending for a given RAT */
  uint8            num_pending_req_and_not_cbs;

  /* Number of grant enhanced callbacks pending for a given RAT */
  uint8            num_pending_req_and_not_enh_cbs;

  /* Number of unlock callbacks pending for a given RAT */
  uint8            num_pending_unlock_cbs;
} trm_per_rat_count_info_type;

/*----------------------------------------------------------------------------
  Per RAT information.
----------------------------------------------------------------------------*/
typedef struct
{
  /* Number of grant callbacks pending for a given RAT */
  trm_per_rat_count_info_type   num_pending_cbs;

  /* QTA related Params for each RAT */
  trm_per_rat_qta_info_type     qta_params;

  /* Flag to enforce a LTA override */
  uint64                        lta_override_bitmask;

  /* sleep indication for RAT, for ASDiv + WLAN antenna sharing */
  boolean                       is_sleeping;

  /* wakeup time for RAT, for ASDiv + WLAN antenna sharing */
  trm_timestamp_t               wakeup_time;

  /* "now" based time for RAT wakeup */
  int32                         wakeup_start;
} trm_per_rat_info_type;

/*----------------------------------------------------------------------------
  TRM NV Refresh struct type
----------------------------------------------------------------------------*/
typedef struct
{
   /* indicates whether refresh logic is enabled */
   boolean  is_enabled;

   /* indicates if NV update is required */
   boolean  is_refresh_needed;

}trm_nv_refresh_struct_type;

typedef struct
{
  /* 
     Identifier for associated counter.
   
     Combination is packed as follows for denials:
     - 8 MSB for Losing Client (trm_client_enum_t)
     - 8 bits for Losing Client Reason (trm_reason_enum_t)
     - 8 bits for Winning Client (trm_client_enum_t) 
     - 8 LSB for Winning Client Reason (trm_reason_enum_t)
   
     Combination is packed as follows for requests:
     - 8 bits for requesting client (trm_client_enum_t)
     - 8 LSB for requesting client reason (trm_reason_enum_t)
   */ 
  uint32 combination;

  /* Associated counter for this given winning/losing client combo */
  uint16 counter;

}trm_metrics_entry_type;

/*----------------------------------------------------------------------------
  TRM Metrics Info struct type
----------------------------------------------------------------------------*/
typedef struct
{
  /* Current size of denial table */
  uint8                   denial_tbl_size;

  /* Current size of attempt table */
  uint8                   attempt_tbl_size;

  /* Table which holds denial count based
     off of combination of winning/losing client */
  trm_metrics_entry_type  denials[TRM_METRICS_MAX_TBL_SIZE];

  /* Table which holds attempts based on
     requesting client/reason */
  trm_metrics_entry_type  attempts[TRM_METRICS_MAX_TBL_SIZE];

}trm_metrics_info_type;

/*----------------------------------------------------------------------------
  TRM Metrics Mapping Mask Info Type
----------------------------------------------------------------------------*/
typedef struct
{
  uint32            mask[TRM_MAX_METRIC_MASK_SIZE];
  uint8             size;
  trm_metric_type_t metric_type;
}trm_metric_mapping_mask_info_type;

/*rxtx split: Grouped async callback */
/*----------------------------------------------------------------------------
  TRM client List type
----------------------------------------------------------------------------*/
typedef struct
{
  /* list of client IDs */
  trm_client_enum_t     client_id[TRM_GROUPED_CLIENTS_MAX];

  /* Number of valid client Ids in the above list */
  uint8                 num;

}trm_client_list_type;

/*----------------------------------------------------------------------------
  TRM grouped list type
----------------------------------------------------------------------------*/
typedef struct
{
  /* client list associated to a group */
  trm_client_list_type              client_list;

  /* grouped async callback pointer */
  trm_grouped_async_event_cb_type   grouped_async_cb;

  /* mixed async callback pointer */
  trm_grouped_async_event_cb_type   mixed_async_cb;

  /* client registering the group,
     the registering client can only modify the group */
  trm_client_enum_t                 reg_client;

}trm_group_list_type;

/*-----------------------------------------------------------------------------
  TRMGroupedAsyncInfo Class

    Class which handles async callbacks for grouped clients.

-----------------------------------------------------------------------------*/
class TRMGroupedAsyncInfo
{
  /* List of all the groups registered by techs */
  trm_group_list_type      group_info[TRM_GROUPED_CLIENTS_MAX];

  /* Number of valide groups in the above group list */
  uint8                    num_of_group;

  /* array of clients that need the async callback
     This array is based on group,async_event_type */
  trm_client_list_type     async_cb_info[TRM_GROUPED_CLIENTS_MAX][TRM_ASYNC_EVENT_MAX];

  /* flag to indicate if grouped async callback needs to be made */
  boolean                  grouped_cb_needed;

public:

  TRMGroupedAsyncInfo();

  void initialize_data(void);

  /* Member function to register/add clients to the grouped list */
  void trm_grouped_async_add_list( trm_client_enum_t        reg_client,
                                   trm_grouped_register_async_event_cb_data_type *reg_info, 
                                   boolean is_mixed_async );

  /* member Function to modify the already registered grouped list */
  void trm_grouped_async_modify_list( trm_client_enum_t     mod_client,
                                      trm_grouped_modify_async_event_cb_data_type *mod_info);

  /* member funtion to update the list of grouped async callback that need to be made */
  void trm_grouped_async_update_cb_list( uint8  grouped_id,
                                         trm_async_callback_type_enum_t cb_type,
                                         trm_client_enum_t  client_id );

  /* memeber funtion to send the grouped async callback */
  void trm_send_grouped_async_cb( void );

  /* member function to send grouped RnN async info */
  void send_grouped_request_and_notify_async_info( uint8 grouped_id );

  /* member funtion to send grouped RnNEnh async info */
  void send_grouped_request_and_notify_enh_async_info( uint8 grouped_id);

  /* member funtion to send grouped modify chain state info */
  void send_grouped_modify_chain_state_async_info(uint8 grouped_id);

  /* member function to send grouped unlock indication */
  void send_grouped_unlock_chain_async_info( uint8 grouped_id );

  /* member function to send grouped device hop info */
  void send_grouped_connected_mode_async_info( uint8 grouped_id );

  /* member function to send grouped QTA info */
  void send_grouped_tuneaway_async_info( uint8 grouped_id );
  
  /* member function to send grouped Dual Receive info */
  void send_grouped_dual_receive_async_info( uint8 grouped_id );

  /* member function to check if mixed async callback is needed */
  boolean is_mixed_async_cb_needed( uint8 grouped_id );

  /* function pointer pointing to the APIs that send async callbacks */
  void (TRMGroupedAsyncInfo::* grouped_async_info_cb[TRM_ASYNC_EVENT_MAX]) (uint8 );

  /* member function to send mixed async information */
  void send_mixed_async_info( uint8 grouped_id );
};

typedef struct
{
  trm_client_list_type      client_list;

  trm_unlock_event_enum_t   unlock_state_type;

}trm_co_banded_unlock_info_type;

/*----------------------------------------------------------------------------
  TRM data storage structure
----------------------------------------------------------------------------*/

typedef struct
{
  /* Initialization state of TRM */
  trm_init_status           init_status;

  /* RF configuration (single, dual dependent/independent, ... ) */
  trm_rf_enum_t             rf_type;

  /* Actual rf h/w capability information */
  trm_rf_hw_struct          rf_hw;

  /* All clients of the Transceiver Resource Manager */
  TRMClient*                client;

  /* All RF chains */
  trm_chain_struct          chain[ TRM_MAX_CHAINS ];

  /* Number of available RF chains */
  uint32                    max_num_chains;

  /* Priority order sorted list of request clients */
  TRMClientOrderArray       order;

  /* Priority order sorted list of request clients */
  TRMClientArray            multi_request_list;

  /* The earliest lock request times for each RF group */
  trm_earliest_usage_info_type earliest[TRM_MAX_CHAINS];

  /* Lock extension data */
  trm_extension_struct      extend;

  /* Critical section - for initialization serialization, etc. */
  rex_crit_sect_type        crit_sect;

  /* Critical section - for extension APIs and trm_get_simult_cap */
  rex_crit_sect_type        extns_flag_crit_sect;


  boolean                   is_tech_based_dr_enabled;

  /* bit mask to determine modes enabled with NV. We are reusing the 
     configuration from get simultaneous capability which uses SHDR for
     bit 0. It is no-op here
     bit 0 : None
     bit 1 : SVLTE
     bit 2 : SVDO
   */
  uint32                    modes_enabled;

  uint32                    shdr_mode_mask;

  boolean                   is_scheduler_enabled;
  boolean                   is_called_from_multi_req;

  trm_rf_mode_map_type      rf_map[TRM_MAX_CLIENTS];

  /* Array storage for extension flags, one bitmap per reason */
  uint8                     extension_flags[TRM_REASON_MAX];

  /* Subscription capability tech mapping as per multimode subs */
  trm_tech_map_table_t      tech_asid_map;

  /* PBR efs settings data from the EFS */
  trm_pbr_efs_data_type     pbr_init_settings;

  /* TRM RF config */
  uint32                     rf_config;

  trm_tech_state_update_callback_type   lmtsmgr_cb;

  trm_hopping_config_info_type hop_config;

  /* Previous chain owner state */
  trm_chain_struct           last_chain[ TRM_MAX_CHAINS ];

  trm_log_struct_type        log_struct;

  TRMBandHandler             band_handler;

  /* feature list: list of techs with active feature */
  TRMFeatureArray            feat_info;

  /* temporary variables, should be cleaned after testing phase */
  boolean                    is_aps_logic_enabled;

  /* Queue to hold free command buffers. */
  q_type                               search_activity_free_q;

  /* QUEUE Buffers. */
  trm_client_search_activity_data_type search_activity_q_buffer[TRM_CLIENT_SEARCH_ACTIVITY_Q_SIZE];

  /* Search activity client queue buffer */
  q_type                               search_activity_client_q_buffer[TRM_MAX_CLIENTS];

  /* Information maintained per RAT */
  trm_per_rat_info_type                per_rat_info[TRM_RAT_GROUP_MAX];

  /* nv refresh info */
  trm_nv_refresh_struct_type           nv_refresh_info;

  /* default PBR bias needed flag */
  boolean                              default_pbr_bias_needed;

  /* dynamic PBR bias needed flag */
  boolean                              dynamic_pbr_bias_needed;

  /* ensure that PBR update happens only once after scheduler runs */
  boolean                              pbr_need_update;

  /* Operating mode log packet info */
  trm_operating_mode_info_type          op_mode_info;

  /* en/disable single TX dSDA
     temporary till RF has the support */
  boolean                              is_tx_sharing_enabled;

  /* Collection of performance metrics related to TRM */
  trm_metrics_info_type                metrics;

  /* Array of combination masks that map to the metric enum type */
  trm_metric_mapping_mask_info_type    metric_enum_masks[TRM_METRIC_MAX];

  /* Device ordering entity */
  TrmDeviceOrdering                    device_ordering;

  boolean                              is_irat_under_progress;

  /* Grouped ASync callback info */
  TRMGroupedAsyncInfo                  grouped_async_info;

  /* Device ordering autogen enabled flag */
  boolean                              dot_ag_enabled;

  /* QTA active bitmask */
  uint64                               qta_active_bitmask;

  #if defined (FEATURE_MCS_TABASCO) && defined (FEATURE_NON_G_POND_DR_DISABLED)
  boolean                              prx_on_d_wa_enabled;
  #endif

  
  /* Last state info sent to limits manager */
  trm_state_info_type                  last_state_info;

  trm_co_banded_unlock_info_type       co_banded_unlock_info;

  TRMConflictArray                     chain_owners;

  /* debug flag set after QTA proceed is granted and cleared after
     QTA gap is started */
  boolean                              waiting_qta_start;

} trm_struct;

#endif /* TRM_STRUCT_H */
