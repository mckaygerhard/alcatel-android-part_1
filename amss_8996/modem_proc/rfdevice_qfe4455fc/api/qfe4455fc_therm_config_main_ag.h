
#ifndef QFE4455FC_THERM_CONFIG_MAIN_AG_H
#define QFE4455FC_THERM_CONFIG_MAIN_AG_H
/*
WARNING: This QFE4455FC driver is auto-generated.

Generated using: qtherm_autogen.pl 
Generated from-  

	File: QFE4455FC_RFFE_Settings.xlsx 
	Released: 11/5/2015
	Author: bnejati
	Revision: 2.2
	Change Note: Added a fake write - analogIO - to common row of the common section of the LTE and GSM per RF SW request to get the update going in order to bring up laminate F open top parts.
	Tab: qfe4455fc_therm_settings

*/

/*=============================================================================

          RF DEVICE  A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the QFE4455FC THERM.

  Copyright (c) 2013-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in thermrt,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.

$Header: //source/qcom/qct/modem/rfdevice/qtherm/main/1.10.1/etc/qtherm_autogen.pl#1 60============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

    
#include "rf_rffe_common.h"
#include "rfdevice_qtherm_typedef.h"
#include "qfe4455fc_therm_config_ag.h" 

#ifdef __cplusplus
extern "C" {
#endif  

boolean rfdevice_qtherm_qfe4455fc_validate_n_create_cfg_ag
( 
  rfc_phy_device_info_type* cfg,   
  rfdevice_id_enum_type logical_rf_device_id , 
  uint8 chip_rev, 
  rfdevice_qtherm_settings_type* qtherm_settings,
  rfdevice_qtherm_func_tbl_type* therm_fn_ptrs
);

#ifdef __cplusplus
}
#endif
#endif