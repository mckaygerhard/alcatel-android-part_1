#ifndef __MCPM_L1EMULATOR_H__
#define __MCPM_L1EMULATOR_H__

/*=========================================================================


           M O D E M   C L O C K   A N D   P O W E R   M A N A G E R

                 U N I T  T E S T  H E A D E R  F I L E



GENERAL DESCRIPTION

  This file contains the MCPM state definitions.

PUBLIC EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS
  Invoke the MCPM_Init function to initialize the Modem Clock and Power Manager.


    Copyright (c) 2010 - 2012 by Qualcomm Technologies, Inc.  All Rights Reserved.

==========================================================================*/


/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mpower/mcpm/test/stubs/inc/mcpm_l1emulator.h#1 $

when       who      what, where, why
--------   ---      --------------------------------------------------------
03/05/15   yz      UT removal support
05/05/14   cl      First Jolokia official release branch off Bolt tip
10/21/13   sr       MSG_ macro changes.
09/22/11   ps       Phase-1 scheduled NPA support
07/08/11   ps       Added changes for awake timeline optimization
                    after NPA scheduled API integration
6/17/11    ps       MCPM changes to support FW Power collapse
11/15/10   mg       Initial Revision

==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/
#include "mcpm_sys_configs.h"
#include "mcpm_resrc_configs.h"
#include <rfm_resources.h>
#include <ULog.h>
#include <ULogFront.h>
#include <mcpm_npa.h>

/* ---------------  Defines -------------- */


#define IF_UT_ENABLED     if (0)
#define IS_UT_ENABLED     0
#define IS_SCENARIO_ENB   0
#define COMMIT_UT_EQ_CASE 0
#define MCPM_UT_ERR_FATAL MCPM_ERR_FATAL


#define CFL_FLAG_NONE                0x0
#define CFL_ISMCVSREQ                0x2


typedef struct
{
  uint32 utTechsToRun;

  //Values populated by resrc drivers to tell UT what values they are about to commit, to help
  //track freq error checks during randomized measurements and test when there a possibility
  //some resrc might be in a scheduled npa driver callback for some tech in sleep
  uint32 valToBeCommitted[MCPM_NUM_RESRC];

  //Maximum value of the resource
  uint32 maxResrcValue[MCPM_NUM_RESRC];
} ut_global_config;


typedef struct
{
  uint32 exectd;
  uint32 apitech;
} ut_apicolres;


/* ---------------  Exported variables -------------- */


/* ---------------  Exported Functions -------------- */

/* Saves which thread ID locked the 'Tech Lock' */
void L1EM_Set_TechLock_LastThreadId(mcpm_tech_type tech);
/* Update info related to when scheduled resources get applied etc. */
void L1EM_Update_Sched_Info(mcpm_npa_tech_data * schedData);
void L1EM_Update_Tech_ActiveAsNeighbor(mcpm_tech_type iratTech, mcpm_tech_type homeTech,
                                       boolean bIsTechActiveAsNeighbor);
void L1EM_Update_Req_Start(mcpm_tech_type eTech, mcpm_request_type eRequest, void * pmReqParms,
                           uint32 cntrlflag);
void L1EM_Update_Req_End(mcpm_tech_type eTech, mcpm_request_type eRequest, void * pmReqParms,
                           uint32 cntrlflag);
void MCPM_Update_ValToBeCommitted(MCPM_Resrc_IDType resrcId, uint32 resrcState);
void MCPM_UT_SysCfg_Lookup(mcpm_tech_type eTech, boolean bSuccess);
void MCPM_UT_Update_RF_Info(rfm_resource_info * rfInfo, mcpm_tech_type tech);
void mcpm_unit_test_init(const MCPM_Drv_CtxtType *pDrvCtxt);
/* --------------------------------------- */

#endif /* __MCPM_L1EMULATOR_H__ */
