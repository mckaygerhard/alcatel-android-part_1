#ifndef __MCPM_API_H__
#define __MCPM_API_H__

/*=========================================================================

           M O D E M   C L O C K   A N D   P O W E R   M A N A G E R 
               
                E X T E R N A L   H E A D E R   F I L E


  GENERAL DESCRIPTION
    This file contains the external interface functions and definitions for
    Modem Clock and Power Manager (MCPM).
  
  EXTERNALIZED FUNCTIONS
    MCPM_Config_Modem
  
  INITIALIZATION AND SEQUENCING REQUIREMENTS
    Invoke the MCPM_Init function to initialize the MCPM.
  
        Copyright (c) 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.


==========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.
 
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mpower/api/mcpm_api.h#1 $

when       who     what, where, why 
--------   ---     ---------------------------------------------------------
11/19/15   ls      Added MCPM INIT support.
09/01/15   bd      Added API to check modem QDSS status.
08/28/15   ls      Added new MCVS/MVC interface to support LTE advanced receiver. 
06/18/15   hg      Re-ordered W requests for common code cleanup.
06/04/15   hg      Added boost API support (FR 25663) and MCPM_NUM_RAT_TECH to mcpm_tech_type.
05/15/15   sz      Added W21X and T21X support.
05/05/15   mk      Added support for modem sensor interface.
01/27/15   hg      Reordered tech enums to allow for easier looping in multi-tech clk_proc mux logic.
12/09/14   hg      Added LTE INIT request.
11/18/14   hg      Added support for W light sleep.
09/15/14   hg      Reverted new enum addition as they are not used.
09/09/14   hg      Ported new enums from Bolt.
09/05/14   sc      Added support for CMCC power optimization feature.
08/22/14   sz      Added API to get data for MPLM.
08/13/14   yz      Added support for LTE 3CA. 
04/28/14   bd      Multi-tech GPIO Profiling Support
04/25/14   ls      FW_VPE MCVS client to support LTE NLIC.
03/21/14   sr      G + G DSDS multi-sim support.
02/13/14   ls      LTE VoLTE and CDRX support.
01/17/14   ls      Support for LTE's MCPM request for TDD and FDD DATA on Bolt.
09/27/13   sr      Corrected formatting of some comments. 
09/03/13   ls      Added a new clk_mdm freq enum.
07/31/13   sr      Removed GSM apis 
07/26/13   sr      Added enum MCPM_GSM_DL_NONEV. 
07/08/13   ls      Added more APIs (request type, DL/UL/BW data rate).
02/11/13   vs      Ported from Dime release branch.


==========================================================================*/ 


/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/


#include "customer.h"
#include "comdef.h"
#include "msg.h"
#include "npa.h"
#include "icbarb.h"
#include "rfm_mode_types.h"
#include "mplm_api.h"
/*=========================================================================
      Macros for the MCPM clk/pll dump
==========================================================================*/

#define MCPM_DUMP_CLK	0x00000001
#define MCPM_DUMP_BLK	0x00000002
#define MCPM_DUMP_PWRUP	0x00000004


#define MCPM_DO_NOP_REQ            MCPM_DO_PARMS_UPDATE_REQ
#define MCPM_DO_START_DATA_TX_REQ  MCPM_DO_START_DATA_REQ
#define MCPM_DO_STOP_DATA_TX_REQ   MCPM_DO_STOP_DATA_REQ

/*
 * Bit 0: "RECEIVER_CONFIG" field is updated
 * Bit 1: "NEIGHBOR_MEAS" field is updated
 * Bit 2: "DL_DATA_RATE" field is updated
 * Bit 3: "UL_DATA_RATE" field is updated
 * Bit 4: "RF_BANDWIDTH" field is updated
 */
#define MCPM_NO_UPDATE                 0x000
#define MCPM_RECEIVER_CONFIG_UPDATE    0x001
#define MCPM_NEIGHBOR_MEAS_UPDATE      0x002
#define MCPM_DL_DATA_RATE_UPDATE       0x004
#define MCPM_UL_DATA_RATE_UPDATE       0x008
#define MCPM_RF_BANDWIDTH_UPDATE       0x010
#define MCPM_WKTIME_RF_UPDATE          0x020
#define MCPM_WKTIME_RSRC_UPDATE        0x040
#define MCPM_FCW_DCO_RESET_UPDATE      0x080
#define MCPM_MODEM_PERF_MODE_UPDATE    0x100
#define MCPM_MCVS_SCALE_UPDATE         0x200

/*
 * MCVS Param update info
 */
#define MCPM_MCVS_VPE_UPDATE            0x001
#define MCPM_MCVS_CLKBUS_UPDATE         0x002
#define MCPM_MCVS_Q6_CLK_UPDATE         0x004
#define MCPM_MCVS_FW_VPE_UPDATE         0x008
#define MCPM_MCVS_FW_VPE_UL_UPDATE      MCPM_MCVS_FW_VPE_UPDATE
#define MCPM_MCVS_FW_VPE_DL_UPDATE      0x010

/*
 * RECEIVER_CONFIG
 * Bit 0: RXD (ON/OFF)
 * Bit 1: EQ (ON/OFF)
 * Bit 2: QICE (ON/OFF)
 * Bit 3: W-MIMO (ON/OFF)
 */

#define MCPM_RECEIVER_CONFIG_RXD       0x01  /* 1 rxd ON, 0 rxd OFF */
#define MCPM_RECEIVER_CONFIG_EQ        0x02  /* 1 EQ ON, 0 EQ OFF */
#define MCPM_RECEIVER_CONFIG_QICE      0x04  /* 1 QICE ON, 0 QICE OFF */
#define MCPM_RECEIVER_CONFIG_W_MIMO    0x08  /* 1 W MIMO CALL enabled */


#define MCPM_RECEIVER_CONFIG_RXD_SHIFT    0 
#define MCPM_RECEIVER_CONFIG_EQ_SHIFT     1   
#define MCPM_RECEIVER_CONFIG_QICE_SHIFT   2   
#define MCPM_RECEIVER_CONFIG_W_MIMO_SHIFT 3   

#define MCPM_MODEM_WCDMA_PERF_NONE      0x0
#define MCPM_MODEM_WCDMA_HIGH_PERF_MODE 0x1 /* OFFLINE clock to 144 for Nikel */


/*=========================================================================
      Typedefs
==========================================================================*/

/*
 * mcpm_request_type
 *
 * Requests to MCPM
 *
 * START_REQ should be sent when a technology is activated.
 *
 * STOP_REQ should be sent when a technology is deactivated.
 *
 * WAKE_UP_REQ should be sent when coming out sleep to decode a page indicator
 *
 * GO_TO_SLEEP_REQ should be sent when going into sleep.
 *
 * GO_TO_PSEUDO_SLEEP should be sent by 1X tech if RTC needs to be kept ON during sleep
 *
 * IDLE_REQ should be sent when the UE moves from page indicator decode 
 * to setup paging channel. 1X technology also calls this to move from 
 * 1X VOICE and 1X DATA calls. 
 *
 * VOICE_START_REQ should be sent on start of a voice call.
 *
 * VOICE_STOP_REQ should be sent on end of a voice call.
 *
 * DATA_START_REQ should be sent on start of a data call
 *
 * DATA_STOP_REQ should be sent on end of a data call
 *
 */

typedef enum
{
  /* 1X Requests */
  MCPM_1X_START_REQ                                   = 0,
  MCPM_1X_STOP_REQ                                    = 1,
  MCPM_1X_WAKE_UP_REQ                                 = 2,
  MCPM_1X_GO_TO_SLEEP_REQ                             = 3,
  MCPM_1X_GO_TO_PSEUDO_SLEEP_REQ                      = 4,
  MCPM_1X_IDLE_REQ                                    = 5,
  MCPM_1X_VOICE_REQ                                   = 6,
  MCPM_1X_DATA_REQ                                    = 7,
  MCPM_1X_PARMS_UPDATE_REQ                            = 8,
                                                      
  /* DO Requests */                                   
  MCPM_DO_START_REQ                                   = 9,
  MCPM_DO_STOP_REQ                                    = 10,
  MCPM_DO_WAKE_UP_REQ                                 = 11,
  MCPM_DO_GO_TO_SLEEP_REQ                             = 12,
  MCPM_DO_IDLE_REQ                                    = 13,
  MCPM_DO_START_DATA_REQ                              = 14,
  MCPM_DO_STOP_DATA_REQ                               = 15,
  MCPM_DO_PARMS_UPDATE_REQ                            = 16,
                                                      
  /* GERAN Requests */                                
  MCPM_GERAN_START_REQ                                = 17,
  MCPM_GERAN_STOP_REQ                                 = 18,
  MCPM_GERAN_INIT_REQ                                 = 19,
  MCPM_GERAN_WAKE_UP_REQ                              = 20,
  MCPM_GERAN_GO_TO_SLEEP_REQ                          = 21,
  MCPM_GERAN_IDLE_REQ                                 = 22,
  MCPM_GERAN_VOICE_START_REQ                          = 23,
  MCPM_GERAN_VOICE_STOP_REQ                           = 24,
  MCPM_GERAN_DATA_START_REQ                           = 25,
  MCPM_GERAN_DATA_STOP_REQ                            = 26,
  MCPM_GERAN_PARMS_UPDATE_REQ                         = 27,
                                                      
  /* GERAN1 Requests */                                
  MCPM_GERAN1_START_REQ                               = 28,
  MCPM_GERAN1_STOP_REQ                                = 29,
  MCPM_GERAN1_INIT_REQ                                = 30,
  MCPM_GERAN1_WAKE_UP_REQ                             = 31,
  MCPM_GERAN1_GO_TO_SLEEP_REQ                         = 32,
  MCPM_GERAN1_IDLE_REQ                                = 33,
  MCPM_GERAN1_VOICE_START_REQ                         = 34,
  MCPM_GERAN1_VOICE_STOP_REQ                          = 35,
  MCPM_GERAN1_DATA_START_REQ                          = 36,
  MCPM_GERAN1_DATA_STOP_REQ                           = 37,
  MCPM_GERAN1_PARMS_UPDATE_REQ                        = 38,

  /* LTE Requests */                                  
  MCPM_LTE_START_REQ                                  = 39,
  MCPM_LTE_STOP_REQ                                   = 40,
  MCPM_LTE_INIT_REQ                                   = 41,
  MCPM_LTE_ACQ_REQ                                    = 42,
  MCPM_LTE_WAKE_UP_REQ                                = 43,
  MCPM_LTE_GO_TO_SLEEP_REQ                            = 44,
  MCPM_LTE_IDLE_REQ                                   = 45,
  MCPM_LTE_DATA_START_REQ                             = 46,
  MCPM_LTE_FDD_DATA_START_REQ                         = 47, 
  MCPM_LTE_TDD_DATA_START_REQ                         = 48,
  MCPM_LTE_FDD_VOLTE_DATA_START_REQ                   = 49,
  MCPM_LTE_TDD_VOLTE_DATA_START_REQ                   = 50, 
  MCPM_LTE_GO_TO_LIGHT_SLEEP_REQ                      = 51,
  MCPM_LTE_GO_TO_LONG_LIGHT_SLEEP_REQ                 = 52,
  MCPM_LTE_GO_TO_LIGHT_SLEEP_NO_MODEM_FREEZE_REQ      = 53, 
  MCPM_LTE_DATA_STOP_REQ                              = 54,
  MCPM_LTE_PARMS_UPDATE_REQ                           = 55,
                                                      
  /* TDSCDMA Requests */                              
  MCPM_TDSCDMA_START_REQ                              = 56,
  MCPM_TDSCDMA_STOP_REQ                               = 57,
  MCPM_TDSCDMA_INIT_REQ                               = 58,
  MCPM_TDSCDMA_ACQ_REQ                                = 59,
  MCPM_TDSCDMA_WAKE_UP_REQ                            = 60,
  MCPM_TDSCDMA_GO_TO_SLEEP_REQ                        = 61,  
  MCPM_TDSCDMA_IDLE_REQ                               = 62,
  MCPM_TDSCDMA_VOICE_START_REQ                        = 63,
  MCPM_TDSCDMA_VOICE_STOP_REQ                         = 64,
  MCPM_TDSCDMA_DATA_START_REQ                         = 65,
  MCPM_TDSCDMA_DATA_STOP_REQ                          = 66,
  MCPM_TDSCDMA_PARMS_UPDATE_REQ                       = 67,
                                                      
  /* WCDMA Requests */                                
  MCPM_WCDMA_START_REQ                                = 68,
  MCPM_WCDMA_STOP_REQ                                 = 69,
  MCPM_WCDMA_INIT_REQ                                 = 70, /* Remember to update Crashscope team about this change! */
  MCPM_WCDMA_WAKE_UP_REQ                              = 71,
  MCPM_WCDMA_GO_TO_SLEEP_REQ                          = 72,
  MCPM_WCDMA_IDLE_REQ                                 = 73,
  MCPM_WCDMA_CDRX_GO_TO_LIGHT_SLEEP_REQ               = 74,
  MCPM_WCDMA_VOICE_START_REQ                          = 75,
  MCPM_WCDMA_VOICE_STOP_REQ                           = 76,
  MCPM_WCDMA_DATA_START_REQ                           = 77,
  MCPM_WCDMA_DATA_STOP_REQ                            = 78,
  MCPM_WCDMA_PARMS_UPDATE_REQ                         = 79,
                                                      
  /* WCDMA Requests */                                
  MCPM_WCDMA1_START_REQ                               = 80,
  MCPM_WCDMA1_STOP_REQ                                = 81,
  MCPM_WCDMA1_INIT_REQ                                = 82,
  MCPM_WCDMA1_WAKE_UP_REQ                             = 83,
  MCPM_WCDMA1_GO_TO_SLEEP_REQ                         = 84,
  MCPM_WCDMA1_IDLE_REQ                                = 85,
  MCPM_WCDMA1_VOICE_START_REQ                         = 86,
  MCPM_WCDMA1_VOICE_STOP_REQ                          = 87,
  MCPM_WCDMA1_DATA_START_REQ                          = 88,
  MCPM_WCDMA1_CDRX_GO_TO_LIGHT_SLEEP_REQ              = 89,
  MCPM_WCDMA1_DATA_STOP_REQ                           = 90,
  MCPM_WCDMA1_PARMS_UPDATE_REQ                        = 91,
                                                     
  /* GPS Requests */                                  
  MCPM_GPS_STOP_REQ                                   = 92,
  MCPM_GPS_ACQ_REQ                                    = 93,
  MCPM_GPS_NON_DPO_REQ                                = 94,
  MCPM_GPS_DPO_ON_REQ                                 = 95,
  MCPM_GPS_PARMS_UPDATE_REQ                           = 96,
                                                   
/* RF Requests */                                     
  MCPM_RF_START_REQ                                   = 97,
  MCPM_RF_STOP_REQ                                    = 98,
                                                      
  /* A2 Requests */                                   
  MCPM_A2_START_REQ                                   = 99,
  MCPM_A2_STOP_REQ                                    = 100,
                                                      
  /* GSM Ciphering Requests */                        
  MCPM_GSM_CIPHERING_START_REQ                        = 101,
  MCPM_GSM_CIPHERING_STOP_REQ                         = 102,

  /* GSM Ciphering1 Requests */                          
  MCPM_GSM_CIPHERING1_START_REQ                       = 103,
  MCPM_GSM_CIPHERING1_STOP_REQ                        = 104,

  MCPM_TECH_MAX_REQ
} mcpm_request_type;


/*
 * mcpm_amd_response_type
 * 
 * Response sent to layer 1 modules about device and AMD state.
 *
 * MCPM_AMD_STATE_STATIONARY is sent to indicate that UE is stationary.
 *
 * MCPM_AMD_STATE_MOBILITY is sent to indicate UE is in mobility.
 *
 * MCPM_AMD_ENABLE_REQUEST_SUCCESS_STATIONARY is sent to indicate AMD 
 * is enabled and UE is stationary.
 *
 * MCPM_AMD_ENABLE_REQUEST_SUCCESS_MOBILITY is sent to indicate AMD is 
 * enabled and UE is in mobility.
 *
 * MCPM_AMD_ENABLE_REQUEST_FAIL is sent to indicate AMD enable failed.
 *
 * MCPM_AMD_DISABLE_REQUEST_FAIL is sent to indicate AMD disable failed.
 * 
 * MCPM_AMD_STATE_ERROR is sent to indicate an error.
 * 
 * MCPM_AMD_ABRUPT_DISABLE is sent to indicate an abrupt disconnection with AMD.
 * 
 */
typedef enum {
    
 MCPM_AMD_STATE_STATIONARY                    = 0,
 MCPM_AMD_STATE_MOBILITY                      = 1,
 MCPM_AMD_ENABLE_REQUEST_SUCCESS_STATIONARY   = 2,
 MCPM_AMD_ENABLE_REQUEST_SUCCESS_MOBILITY     = 3,
 MCPM_AMD_ENABLE_REQUEST_FAIL                 = 4,
 MCPM_AMD_DISABLE_REQUEST_SUCCESS             = 5,
 MCPM_AMD_DISABLE_REQUEST_FAIL                = 6,
 MCPM_AMD_STATE_ERROR                         = 7,
 MCPM_AMD_ABRUPT_DISABLE                      = 8,   
}mcpm_amd_response_type;


/*----------------------------------------------------------------------------
  Layer 1 modules register AMD indication callback function prototype
----------------------------------------------------------------------------*/

typedef void (*MCPM_Sensor_Indication_Callback_Type)(mcpm_amd_response_type state);


/*----------------------------------------------------------------------------
  MODEM block register restore callback function prototype
----------------------------------------------------------------------------*/

typedef void (*mcpm_block_restore_callback_type) (void);


/*----------------------------------------------------------------------------
  L1 techs register sleep timeline callback function prototype
  Default 1X and DO will extend timeline
  TRUE in param implies Optimized timeline
  FALSE in param implies Extended timeline
----------------------------------------------------------------------------*/

typedef void (*mcpm_npa_slp_tmln_callback_type) (boolean);


/*----------------------------------------------------------------------------
  MODEM clock speed change callback function prototype
----------------------------------------------------------------------------*/

typedef void (*mcpm_clk_speed_change_callback_type) (void);


/*
 * mcpm_nbr_meas_type
 *
 * The neighbor measurement type
 */

typedef enum
{
  /* 1X Neighbors */
  MCPM_1X2L_MEAS_START,
  MCPM_1X2L_MEAS_STOP,
  
  /* DO Neighbors */
  MCPM_DO2L_MEAS_START,
  MCPM_DO2L_MEAS_STOP,
  MCPM_DO2G_MEAS_START,
  MCPM_DO2G_MEAS_STOP,
  
  /* GERAN and GERAN1 Neighbors */
  MCPM_G2L_MEAS_START,
  MCPM_G2L_MEAS_STOP,
  MCPM_G2W_MEAS_START,
  MCPM_G2W_MEAS_STOP,
  MCPM_G2TDS_MEAS_START,
  MCPM_G2TDS_MEAS_STOP,
  
  /* LTE Neighbors */
  MCPM_L2G_MEAS_START,
  MCPM_L2G_MEAS_STOP,
  MCPM_L2W_MEAS_START, 
  MCPM_L2W_MEAS_STOP, 
  MCPM_L21XDO_MEAS_START,
  MCPM_L21XDO_MEAS_STOP,
  MCPM_L2TDSCDMA_MEAS_START,
  MCPM_L2TDSCDMA_MEAS_STOP,
  
  /* TDSCDMA Neighbors */
  MCPM_TDSCDMA2G_MEAS_START,
  MCPM_TDSCDMA2G_MEAS_STOP,
  MCPM_TDSCDMA2L_MEAS_START,
  MCPM_TDSCDMA2L_MEAS_STOP,
  MCPM_TDSCDMA21XDO_MEAS_START,
  MCPM_TDSCDMA21XDO_MEAS_STOP,
  
  /* WCDMA Neighbors */
  MCPM_W2G_MEAS_START,
  MCPM_W2G_MEAS_STOP,
  MCPM_W2L_MEAS_START,
  MCPM_W2L_MEAS_STOP,
  MCPM_W21XDO_MEAS_START,
  MCPM_W21XDO_MEAS_STOP
  
} mcpm_nbr_meas_type;


/*
 * mcpm_rf_bandwidth_type
 *
 * The configured RF bandwidth
 */

typedef enum
{
  /* DO */
  MCPM_DO,
  MCPM_MCDO,
 
  /* LTE */
  MCPM_LTE_1p4MHZ,
  MCPM_LTE_3MHZ,
  MCPM_LTE_5MHZ,
  MCPM_LTE_10MHZ,
  MCPM_LTE_15MHZ,
  MCPM_LTE_20MHZ,
  MCPM_LTE_5MHZ_5MHZ,
  MCPM_LTE_10MHZ_10MHZ,
  MCPM_LTE_20MHZ_20MHZ,
  MCPM_LTE_10MHZ_5MHZ_5MHZ,
  MCPM_LTE_20MHZ_20MHZ_20MHZ,
  
  /* WCDMA */
  MCPM_WCDMA,
  MCPM_DC_WCDMA,
  MCPM_3C_WCDMA,
  MCPM_4C_WCDMA,
  MCPM_BW_NA,
  MCPM_NUM_BW
  
} mcpm_rf_bandwidth_type;


/*
 * MCPM_dl_datarate_type
 *
 * Downlink data rate.
 */

typedef enum
{

  /* 1X */
    
  /* DO */
  MCPM_DO_DL_2P4_MBPS,
  MCPM_DO_DL_3P1_MBPS,
  MCPM_DO_DL_9P3_MBPS,
  MCPM_DO_DL_14P7_MBPS,
    
  /* GERAN and GERAN1*/
  MCPM_GSM_DL_EV,
  MCPM_GSM_DL_NONEV,
   
  /* GPS */
   
  /* LTE */
  MCPM_LTE_DL_RATE_L10,
  MCPM_LTE_DL_RATE_L36,
  MCPM_LTE_DL_RATE_L50,
  MCPM_LTE_DL_RATE_L73,
  MCPM_LTE_DL_RATE_L100,
  MCPM_LTE_DL_RATE_L110,
  MCPM_LTE_DL_RATE_L146,
  MCPM_LTE_DL_RATE_L150,
  MCPM_LTE_DL_RATE_L200,
  MCPM_LTE_DL_RATE_L300,
  MCPM_LTE_DL_RATE_L400,
  MCPM_LTE_DL_RATE_L450,
  MCPM_LTE_DL_RATE_L600,
   
  /* TDSCDMA */
  
  /* WCDMA */
  MCPM_W_DL_R99DATA,
  MCPM_W_DL_3p6_MBPS,
  MCPM_W_DL_7p2_MBPS,
  MCPM_W_DL_14p4_MBPS,
  MCPM_W_DL_21_MBPS,
  MCPM_W_DL_28_MBPS,
  MCPM_W_DL_42_MBPS,
  MCPM_W_DL_63_MBPS,
  MCPM_W_DL_84_MBPS,
  MCPM_DL_RATE_NA,
  MCPM_NUM_DL_RATES
  
} MCPM_dl_datarate_type;


/*
 * mcpm_ul_datarate_type
 *
 * uplink datarate
 */

typedef enum
{
  /* 1X */
 
  /* DO */
  MCPM_DO_UL_153KBPS,
  MCPM_DO_UL_1P8MBPS,
  MCPM_DO_UL_5P4MBPS,
 
  /* GERAN and GERAN1*/
  MCPM_GSM_UL_EV,

  /* GPS */

  /* LTE */
  MCPM_LTE_UL_RATE_L5,
  MCPM_LTE_UL_RATE_L11,
  MCPM_LTE_UL_RATE_L13,
  MCPM_LTE_UL_RATE_L23,
  MCPM_LTE_UL_RATE_L25,
  MCPM_LTE_UL_RATE_L37,
  MCPM_LTE_UL_RATE_L40,
  MCPM_LTE_UL_RATE_L50,
  MCPM_LTE_UL_RATE_L75,
  MCPM_LTE_UL_RATE_L100,
  MCPM_LTE_UL_RATE_L150,
  
  /* TDSCDMA */

  /* WCDMA */
  MCPM_W_UL_R99DATA,
  MCPM_W_UL_1P46_MBPS,
  MCPM_W_UL_2MBPS,
  MCPM_W_UL_2P93_MBPS,
  MCPM_W_UL_5P76_MBPS,
  MCPM_W_UL_11P5_MBPS,
  MCPM_W_UL_22_MBPS,
  MCPM_UL_RATE_NA,
  MCPM_NUM_UL_RATES

} mcpm_ul_datarate_type;


/*
 * mcvs_request_type
 *
 * mcvs request type
 */

typedef enum 
{
  MCVS_REQUEST_NONE = 0, 
  MCVS_PRE_SCALE_REQUEST,  /* to prescale Cx/Mx based on clock requests to come in future */
  MCVS_FULL_REQUEST,       /* to update clocks - clk bus/VPE/Q6 - as requested possibly even causing RPM interaction */
  MCVS_FAST_CAP_REQUEST,   /* to update clocks as far as possible but return quickly - No RPM interaction*/
  MCVS_RELEASE_REQUEST     /* To release any request made in past */
} mcvs_request_type;


/*
 * mcpm_mcvsrequest_parms_type
 *
 *  Structure containing the MCPM MCVS Request and Request Attributes.
 *  mcvs_req_type                 - Indicates if request is pre-scale (0) - slow (1) or fast(2), other fileds reserved for future
 *  mcvs_update_info              - Mask that indicates which of the attrbutes are updated.
 * 
 *  modem_vpe_KHz                 - Provides the MP update Info
 *  modem_clk_bus_KHz             - Indicates the Clk bus update
 *  q6_clock_KHz                  - Indicates Q6 clock update
 */

typedef struct
{
  mcvs_request_type      mcvs_req_type;
  uint32                 mcvs_update_info;
  uint32                 modem_vpe_KHz;
  uint32                 modem_clk_bus_KHz;
  uint32                 q6_clock_KHz;
} mcpm_mcvsrequest_parms_type;
 
 
/*
 * mcpm_request_parms_type
 *
 * Structure containing the MCPM Request and Request Attributes.
 *
 *  update_info        - Indicates which of the attrbutes are updated.
 *  receiver_config    - Provides the receiver config information.
 *  neighbor_meas      - Indiactes the type of neighbor measurement
 *  dl_datarate        - Provides the downlink data rate.
 *  ul_datarate        - Provides the uplink data rate.
 *  rf_bandwidth       - Provides the RF bandwidth.
 *  wkup_time_for_rf   - Time when RF resource needed (in usec)
 *  wkup_time          - Time when bus or any other resource request needed.(in usec)
 *  modem_perf_mode    - Modem performance mode
 *  mcvs_request - L1s can send an MCVS request along with other parameter updates
 *                 MCVS request could be to pre-scale voltage rails based on clock frequency, or make immediate clk bus/Q6/VPE updates
 */
typedef struct
{

  uint32                        update_info;
  uint32                        receiver_config;
  mcpm_nbr_meas_type            neighbor_meas;
  MCPM_dl_datarate_type         dl_datarate;
  mcpm_ul_datarate_type         ul_datarate;
  mcpm_rf_bandwidth_type        rf_bandwidth;
  uint64                        wkup_time_for_rf;
  uint64                        wkup_time;
  uint32                        modem_perf_mode;
  mcpm_mcvsrequest_parms_type   mcvs_request;
} mcpm_request_parms_type;


/*
 * mcpm_modem_clk_type
 *
 * modem clock type.
 */

typedef enum
{

  /* MCPM Offline clk */
  MCPM_CLK_MODEM,

  /* MCPM TDEC clk */
  MCPM_CLK_TDEC,

  /* MCPM total number of clocks */
  MCPM_NUM_CLK

} mcpm_modem_clk_type;


/*
 * mcpm_clk_callback_trigger_type
 *
 * clock callback trigger type.
 */

typedef enum
{

  /* Trigger call back after clock change in MCPM */
  MCPM_CB_Trigger_AFTER_CLK_Change,

  /* Trigger call back before clock change in MCPM */
  MCPM_CB_Trigger_BEFORE_CLK_Change,

} mcpm_clk_callback_trigger_type;


/*
 * mcpm_tech_type
 *
 * MCPM supported technologies.
 */

typedef enum
{
  /* MCPM 1X technology definition */
  MCPM_1X_TECH,

  /* MCPM DO technology definition */
  MCPM_DO_TECH,

  /* MCPM WCDMA technology definition */
  MCPM_WCDMA_TECH,

  /* MCPM WCDMA technology definition */
  MCPM_WCDMA1_TECH,

  /* MCPM LTE technology definition */
  MCPM_LTE_TECH,

  /* MCPM TDSCDMA technology definition */
  MCPM_TDSCDMA_TECH,

  /* MCPM GERAN technology definition */
  MCPM_GERAN_TECH,

  /* MCPM GERAN1 technology definition */
  MCPM_GERAN1_TECH,

  /* Number of RAT technologies */
  MCPM_NUM_RAT_TECH,

  /* MCPM GPS technology definition */
  MCPM_GPS_TECH = MCPM_NUM_RAT_TECH,

  /* MCPM RF technology definition */
  MCPM_RF_TECH,

  /* MCPM A2 technology definition */
  MCPM_A2_TECH,

  /* MCPM GSM CIPHERING definition */
  MCPM_GSM_CIPHERING_TECH,

  /* MCPM GSM CIPHERING1 definition */
  MCPM_GSM_CIPHERING1_TECH,

  /* Number of total techs used for boundary checks */
  MCPM_NUM_TECH
} mcpm_tech_type;


/*
 * MCPM_RCM_TRIGGER
 * 
 * Supported RCM Triggers
 */

typedef enum
{
  RCM_TRIGGER_1,
  RCM_TRIGGER_2,
  RCM_TRIGGER_3,
  RCM_TRIGGER_4,
  RCM_TRIGGER_MAX
} MCPM_RCM_TRIGGER;


/*----------------------------------------------------------------------------
  A2 resources need be setup as fast as possible. Pulling out of A2 from the
  normal path of muxing.
----------------------------------------------------------------------------*/
#define MCPMDRV_NUM_MUX_TECH  (MCPM_A2_TECH)


/*
 * mcpm_modem_block_type
 *
 * MCPM modem block type.
 */

typedef enum
{

  /* MCPM EDGE block */
  MCPM_EDGE_BLOCK,

  /* MCPM TX block */
  MCPM_TX_BLOCK,

  /* MCPM Turbo decoder block */
  MCPM_TDEC_BLOCK,

  /* MCPM DemBack block */
  MCPM_DEMBACK_BLOCK,

  /* MCPM VPE block */
  MCPM_VPE_BLOCK,

  /* MCPM MEMSS block */
  MCPM_MEMSS_BLOCK,

  /* MCPM DEMSS block */
  MCPM_DEMSS_BLOCK,

  /* MCPM RXFE block */
  MCPM_RXFE_BLOCK,

  /* MCPM RXFE block */
  MCPM_STMR_BLOCK,

  /* MCPM total number of blocks */
  MCPM_NUM_BLOCK

}mcpm_modem_block_type;


/*
 * MCPMNPA_StatusType
 *
 * MCPM NPA status type.
 */

typedef enum
{
  MCPMNPA_STAT_NULL,
  MCPMNPA_STAT_OPT_SUCCESS = 1,
  MCPMNPA_STAT_OPT_FAILURE = 2,
  MCPMNPA_STAT_OPT_IMM_REQ_SENT = 3,
  MCPMNPA_STAT_OPT_ENABLE = 4
} MCPMNPA_StatusType;


/*
 * MCPM_ClkSpeedType
 *
 * MCPM clock speed type.
 */

typedef enum
{
  MCPM_CLK_NULL, 
  MCPM_CLK_19_2M,
  MCPM_CLK_72M,
  MCPM_CLK_143M,
  MCPM_CLK_144M,
  MCPM_CLK_250M,
  MCPM_CLK_333M,
  MCPM_CLK_384_5M,
  MCPM_CLK_399M,
  MCPM_NUM_CLKSPEED
} MCPM_ClkSpeedType;

#define MCPM_BIMC_BOOST_MASK              0x001
#define MCPM_CPU_BOOST_MASK               0x002
/*#define MCPM_VECTOR_CPU_BOOST_MASK        0x004*/

typedef void (*mcpm_deboost_callback)(void);
typedef void (*mcpm_cpuboost_callback)(void);

/*==========================================================================
               FUNCTION DECLARATIONS FOR MODULE
==========================================================================*/

/* =========================================================================
 **  Function : MCPM_Service_Boost_Req
 ** =========================================================================*/
/**
  This function is the main API called by L1 techs to request for boost/deboost..


  @param tech                 [in] Current technology mcpm_tech.
  @param boostRequest         [in] uses 2 LSB bits as flags.
                                  MCPM_BIMC_BOOST_MASK(0x1) and MCPM_CPU_BOOST_MASK (0x2)
  @param cbFcnPtr             [in] function pointer to call, when deboost is complete.
  @param cpuBoostCbFcnPtr     [in] function pointer to call, when CPU boost is complete.
  @param l1BoostTime          [in] client request for boost time... max 20 msec allowed.

  @return
  REturn 0 if request honoured else 1

  @dependencies
  TBD.

 */
uint32 MCPM_Service_Boost_Request
(
  mcpm_tech_type tech,
  uint32 boostRequest,
  mcpm_deboost_callback cbFcnPtr,
  mcpm_cpuboost_callback cpuBoostCbFcnPtr,
  uint32 l1BoostTime
);

/*==========================================================================

  FUNCTION      MCPM_CONFIG_MODEM

  DESCRIPTION   This function is the main interface function.
                Modem SW should call this function when the modem mode is 
                changed. MCPM expects the call made for the following mode
                changes.
                - Activate and deactivate the Portocal stack
                - Start and stop of voice call
                - Start and stop of data call
                - Change in RF bandwidth
                - Start and Stop of nbr measurement
                - Rxd/qice/EQ start and stop.

  PARAMETERS    req      - Modem SW mode change request.
                parms    - Attributes associated with req.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  None.

  SIDE EFFECTS  Clocks will be enabled and disabled during the test.

==========================================================================*/

void MCPM_Config_Modem(mcpm_request_type req, mcpm_request_parms_type *parms);


/*==========================================================================

  FUNCTION      MCPM_MCVSConfig_Modem

  DESCRIPTION   This function is the interface function for MCVS requests.
                Modem SW should call this function when the modem mode mcvs update is required. 
                MCPM expects the call made for the following mode
                changes.
                - VPE Clock update
                - CLK BUS Clock update
                - Q6 Clock update                

  PARAMETERS    tech      - Modem SW tech type.
                parms    - Attributes associated with req.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  None.

  SIDE EFFECTS  Clocks will be Updated during this without change in Config. If the request attribute type in parms is
                set to type immediate - then no RPM requests will be sent out to scale the voltages. If voltages have 
                not been pre-scaled, then frequencies will be capped.If request type is type slow - this call will 
                block if (mx,cx) voltage scaling is required and may take longer to finish. This call needs to be
                accompanied by a call to Release the MCVS Request

==========================================================================*/

void MCPM_MCVSConfig_Modem(mcpm_tech_type tech, mcpm_mcvsrequest_parms_type *parms);


/*==========================================================================

  FUNCTION      MCPM_MCVSConfig_Release
  DESCRIPTION   This function is the interface function for releasing the MCVS requests .
                Modem SW should call this function when the modem mode mcvs update request has been made. 
                MCPM expects the call made for the following mode
                changes.
                - VPE Clock update
                - CLK BUS Clock update
                - Q6 Clock update                

  PARAMETERS    tech      - Modem SW tech type.
                

  DEPENDENCIES  MCPM must be initialized. A prior call to MCPM_MCVSConfig_Modem from same tech type should have been made.
 

  RETURN VALUE  NONE.

  SIDE EFFECTS : This will bring down the clock configs to pre-mcvs level.
  
==========================================================================*/

void MCPM_MCVSConfig_Release(mcpm_tech_type tech);


/*============================================================================

FUNCTION MCPM_SET_BLOCK_RESTORE_CALLBACK

DESCRIPTION
  This function sets the callback function to call when modem block is bought
  out of power gating (power collapse)

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

============================================================================*/

void MCPM_Set_Block_Restore_Callback
(
  /* The tech that is registering the block */
  mcpm_tech_type tech,

  /* Modem block  */
  mcpm_modem_block_type block,

  /* register restore callback */
  mcpm_block_restore_callback_type restore_cb
);


/*============================================================================

FUNCTION MCPM_Set_Clock_Change_Callback

DESCRIPTION
  This function sets the callback function to call when modem clock state
  changes based on the trigger type.

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

============================================================================*/

void MCPM_Set_Clock_Change_Callback
(
  /* The tech that is registering the call-back */
  mcpm_tech_type tech,

  /* Modem clk  */
  mcpm_modem_clk_type clk_type,

  /* register clock change callback */
  mcpm_clk_speed_change_callback_type clk_speed_change_cb
);



/*============================================================================

FUNCTION MCPMDRV_CFG_Dump

DESCRIPTION
  This function dumps all the real time MCPM clk/registers values to F3 for
  now.

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

============================================================================*/

void MCPMDRV_CFG_Dump( uint32 mcpm_dump_val);


/*==========================================================================

  FUNCTION      MCPM_Send_RF_Req

  DESCRIPTION   This function can be called by RF drivers to send the RF requests
                to MCPM.

  PARAMETERS    mode                - Tech/RF mode that is making this request.
                npa_id              - PAM Id that MCPM will use in NPA request
                npa_handle          - NPA handle that MCPM should use to
                pass this request to PMIC
                time_rsrc_needed    - Time when this resource is needed (not used for now)
                sleep_req           - Bool to indicate if this is a sleep or awake request

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/
MCPMNPA_StatusType MCPM_Send_RF_Req
(
  /* Tech that is making this request. */
  rfm_mode_enum_type mode, 
  /* NPA Id sent by RF driver */
  uint8 npa_id, 
  /* NPA handle that is sent by RF driver */
  npa_client_handle npa_handle, 
  /* Time when this resource is needed. */
  uint64 time_rsrc_needed,
  /* Sleep or Awake request. */
  boolean sleep_req
);


/*============================================================================

FUNCTION MCPM_NPA_SET_SLEEP_TIMELINE_CALLBACK

DESCRIPTION
  This function sets the callback function to call when sleep timeline has to
  changed from optimized to extended and vice versa

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

============================================================================*/

void MCPM_NPA_Set_Sleep_Timeline_Callback
(
  /* The tech that is registering the block */
  mcpm_tech_type tech,

  /* register sleep timeline callback */
  mcpm_npa_slp_tmln_callback_type slp_tmln_cb
);

/*============================================================================

FUNCTION MCPMDRV_Get_Modem_Offline_Clock

DESCRIPTION
  This function returns current modem offline clock enum value.

DEPENDENCIES
  None.

RETURN VALUE
  clock speed type (enum).  

SIDE EFFECTS
  None.

============================================================================*/

MCPM_ClkSpeedType MCPMDRV_Get_Modem_Offline_Clock(mcpm_tech_type current_tech);


/*==========================================================================

FUNCTION        MCPM_Sensor_Start_Amd

DESCRIPTION     MCPM_Sensor_Start_Amd handles AMD Start request from layer 1 
                modules. 

DEPENDENCIES    Layer 1 modules should first register a call back using 
                MCPM_Set_AMD_Callback before calling MCPM_Sensor_Start_Amd.

PARAMETERS      tech          - Modem SW tech type
                report_period - report_period is how often layer 1 want an 
                                indication of UE state. report period of 
                                '0' will notify layer 1 only when there is 
                                a change in UE state.      
                                
RETURN VALUE    TRUE  - Success
                FALSE - Error

============================================================================*/

boolean MCPM_Sensor_Start_Amd(mcpm_tech_type tech, uint32 report_period);


/*==========================================================================

FUNCTION        MCPM_Sensor_Stop_Amd

DESCRIPTION     MCPM_Sensor_Stop_Amd handles AMD Stop request from 
                layer 1 modules. 

DEPENDENCIES    None
 
PARAMETERS      tech  -  Modem SW tech type

RETURN VALUE    TRUE  -  Success
                FALSE -  Error

==========================================================================*/

boolean MCPM_Sensor_Stop_Amd(mcpm_tech_type tech);


/*==========================================================================

FUNCTION        MCPM_Set_AMD_Callback

DESCRIPTION     Allows L1 modules to register a callback. This callback is  
                triggered when state change is detected by MCPM AMD SW.

DEPENDENCIES    None
 
PARAMETERS      tech   -  Modem SW tech type
                amd_cb -  Call back to be triggered to notify L1 about 
                          UE mobility state change and notify 
                          success/failure of AMD enable and disable
                          
RETURNS         None

==========================================================================*/

void MCPM_Set_AMD_Callback
(
  /* The tech that is registering the callback  */
  mcpm_tech_type tech,

  /* register AMD state indication callback */
  MCPM_Sensor_Indication_Callback_Type amd_cb
);


/*===========================================================================
FUNCTION     MCPM_Get_PwrOpt_Level

DESCRIPTION  This function is the interface function for L1 SW to get the 
             level of power optimization from Modem SW.

DEPENDENCIES MCPM must be initialized.

PARAMETERS   eTech  - mcpm_tech_type.

RETURN VALUE The level of power optimization for that tech.

SIDE EFFECTS None.
===========================================================================*/

uint8 MCPM_Get_PwrOpt_Level(mcpm_tech_type eTech);


/*==========================================================================

  FUNCTION      mcpm_gpio_profile_pwr_strobe_toggle

  DESCRIPTION   Used to toggle power strobe at profile points. Should not be used  
                directly. Instead should be used through macro MCPM_GPIO_TOGGLE.

  PARAMETERS    strobe      - The RCM power strobe to toggle

  DEPENDENCIES  MCPM must be initialized, MCPM GPIO feature must be enabled

  RETURN VALUE  None.

  SIDE EFFECTS  Toggles a specific dedicated debug strobe on RCM HW

==========================================================================*/

extern void MCPM_Gpio_Profile_Pwr_Strobe_Toggle(MCPM_RCM_TRIGGER strobe);

/*
 * Macro for toggling a specific RCM debug strobe
 */
 
#define MCPM_GPIO_TOGGLE(RCM_STROBE) MCPM_Gpio_Profile_Pwr_Strobe_Toggle(RCM_STROBE);

/*============================================================================

FUNCTION MCPM_Get_Data_MPLM

DESCRIPTION
  This function copies MCPM data into MPLM structure for modem power logging.

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

============================================================================*/

void MCPM_Get_Data_MPLM
(
  /* The tech that is registering the block */
  MPLM_MCPM_Buffer_Type* MPLMBuffer
);

/*==========================================================================

  FUNCTION      MCPM_is_qdss_en

  DESCRIPTION   Used to check if QDSS modem events are enabled for MCPM

  PARAMETERS    None.

  DEPENDENCIES  MCPM must be initialized,

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/
boolean MCPM_is_qdss_en(void);

#endif /* __MCPM_API_H__ */
