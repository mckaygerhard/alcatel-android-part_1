/*===*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                 HIT DIAG Handler
GENERAL DESCRIPTION
  This file contains functions associated with handling hit packets from diag.

EXTERNALIZED FUNCTIONS
  hitdiag_handler
  
REGIONAL FUNCTIONS
  hitdiagcmd_signal

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

  Copyright (c) 2006, 2007 by Qualcomm Technologies Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

============================================================================

                        EDIT HISTORY FOR MODULE

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/hdr/hit/src/hitdiag.c#1 $ $DateTime: 2016/03/28 23:02:56 $ $Author: mplcsds1 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     -------------------------------------------------------
01/15/2013   smd     Featurized hit cmd and hit diag.
05/07/2007   vh      Changed dynamic memory allocation to static one
03/28/2007   vh      Created

==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/
#include "hdr_variation.h"
#include "customer.h"

#ifdef FEATURE_HIT
#endif /* FEATURE_HIT */
