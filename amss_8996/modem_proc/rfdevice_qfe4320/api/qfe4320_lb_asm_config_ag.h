
#ifndef QFE4320_LB_ASM_CONFIG_AG_H
#define QFE4320_LB_ASM_CONFIG_AG_H/*
WARNING: This QFE4320_LB driver is auto-generated.

Generated using: qasm_autogen.pl 
Generated from-  

	File: QFE4320_RFFE_Settings.xlsx 
	Released: 8/25/2015
	Author: marco
	Revision: 1.17
	Change Note: Changed bypass cap of MB PA for all LB scripts to help isolation from LB PA harmonics to MB ASM pin. No impact on performance, no Cal/no Char required
	Tab: qfe4320_lb_asm_settings

*/

/*=============================================================================

          RF DEVICE  A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

  Copyright (c) 2013-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfdevice_qfe4320/api/qfe4320_lb_asm_config_ag.h#1 $
$Author: mplcsds1 $
$DateTime: 2016/03/28 23:07:37 $ 

=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

 
#include "comdef.h"
#include "rfc_common.h"
#include "rfdevice_qasm_typedef.h"

#ifdef __cplusplus
extern "C" {
#endif  


boolean rfdevice_qasm_qfe4320_lb_construct_driver_ag
(
  rfdevice_qasm_settings_type* qasm_settings,
  rfdevice_qasm_func_tbl_type* asm_fn_ptrs
);

#ifdef __cplusplus
}
#endif
#endif