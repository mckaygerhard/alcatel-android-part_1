﻿/******************************************************************************************

 File: RegistrationHandlerVoLTE.h
 Description: 
 
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/regmanager/src/RegistrationHandlerVoLTE.h#2 $
$DateTime: 2016/05/15 20:11:14 $
$Author: huawenc $

 Revision History
==========================================================================================
   Date    |   Author's Name    |  CR#   |  Review ID  |      Change Description
==========================================================================================
13-09-13  Priyank        543915     FR 3104: IMS on Wifi
--------------------------------------------------------------------------------------------------
19-09-13  Ahmet        537801    For certain op mode, derive IMPU from IMSI if ISIM doesn't have IMPU.
--------------------------------------------------------------------------------------------------
17-10-13  asharm       556464    IMS PDN not being disconnected after DeRegistration success (TTY/testmode)
                                 and IMS PDP brought up even though powered up in test mode
--------------------------------------------------------------------------------------------------
21-10-13  Ahmet        565090    For certain op modes, wait for Tdelay before app requested re-registration.
-------------------------------------------------------------------------------
17-11-13  Sreenidhi    569045    When no P-CSCF available, UE stop the timer due to some bug and request PDN when RAT is changed.
-------------------------------------------------------------------------------
14-11-13 Sankalp       562391    Oprt1 :Retry counter maintanence: - If the retry counter reaches NSIPretrycount and 
                                        there is no other P-CSCF available, then block PLMN for T3402 time
--------------------------------------------------------------------------------------------------
25-11-13  Manasi       573308    SIP 503 with "Outage Text" error response handling while in another call
-------------------------------------------------------------------------------
29-11-13   Sreenidhi     563954      VZW IMS Reg Retry failure - QIPCALL is not adding FTs 
-------------------------------------------------------------------------------------------------------------
11-02-14   Manasi     608295       VZW reregistration requirements
--------------------------------------------------------------------------------------------------
04-04-14  Sreenidhi    627310    [FR 20089] KT : Request for initial registration when UE try to make VoLTE call in non registered state
-------------------------------------------------------------------------------
08-04-14   Ahmet       645389      Handle ReRegistration request while there is transaction going on
------------------------------------------------------------------------------
15-04-14   Sreenidhi   642798      [FR 20091] KT : UE shall not perform IMS re-registration when Terminated-unregistered event in NOTIFY xml body
-----------------------------------------------------------------------------------------------
05-12-14   Manasi      655616     FR 20157: Handle dynamic update to EAB status
-------------------------------------------------------------------------------
05-08-14   Manasi      650051       OMADM changes    
-------------------------------------------------------------------------------
26-June-14  asharm     676272    No IMS de-reg with IMSI-based registration when Presence is enabled
-------------------------------------------------------------------------------
04-09-14   c_snaras    704269   FR 22822: UE receives multiple P-CSCF IP addresses for emergency registration.
-------------------------------------------------------------------------------
18-Sep-14  c_snaras    720328   VZW - IMS Reg on Session Failures in hVoLTE - Add Delay
-------------------------------------------------------------------------------
15-07-14  c_snaras    694697    IMS Re-registration is NOT triggered when the device camps back to LTE n/w after GSM call on SUB2
-------------------------------------------------------------------------------
01-05-15   c_snaras  770881      IMS REGISTER message is going out with old IP address in one of the corner LTE scenarios(timing dependent)
----------------------------------------------------------------------------------------------------
01-22-14       Manasi     777901   FR 25430: TMO - Handling No 911 Address on File
----------------------------------------------------------------------------------------------------
15-Jan-15  Sreenidhi  765853      FR 20934: Sending BYE to release dialogs before IMS dereg with LPM
-------------------------------------------------------------------------------
02-11-13   Purnacha  782286   FR 25431: Internal - Configurable voting when IMS is interrupted about USIM/ISIM refresh during a call 
-------------------------------------------------------------------------------
02-14-15   c_snaras        767931     FR 24213: VzW - Handling APN related parameter change when connected to IMS APN
----------------------------------------------------------------------------------------------------
16-02-15   Sreenidhi       782112     FR 24482: SBM SUBSCRIBE failure process
----------------------------------------------------------------------------------------------------
02-19-15       Manasi 797607   Send IMSA subscription ind on 4th attempt   
-------------------------------------------------------------------------------------------------
24-02-15     c_snaras    759613     SOMC: CR 759613: Airplane mode on/off doesn't reset Retry Counter
-------------------------------------------------------------------------------
02-19-15       Purna       790354  FR xxxxx: Wi-Fi calling registers while in a 2g/3g call
-------------------------------------------------------------------------------
10-03-15       Priyank       738646   FR 23978: Registration related features for Oprt 11
-------------------------------------------------------------------------------
20-03-15       Sreenidhi     810389   NO QMI log for call origination failure when initial Registration fails
-------------------------------------------------------------------------------
03-17-15       Purna       807623   TTY support for TMO
06-Apr-15  HuaWen  800357 FR 25037: Sprint - Inclusion of Country Code in IMS REGISTER over WiFi
-------------------------------------------------------------------------------
17-04-15       c_snaras   814723  FR 25036 : No registration triggered on IWLAN after moving to a different MCC
-------------------------------------------------------------------------------
06-30-15     Manasi       852185   Support for non-validated numbers
--------------------------------------------------------------------------------------------------
05-25-15       Priyank      834628   Initial REGI procedure should be started after reREGI failure with no response
-------------------------------------------------------------------------------
08-Sep-15      Priyank    901577      In TTA-LGU mode, IMS shall not trigger PDN release on receiving 403 twice, 
                                      TestMode enabled, N/w initiated Dereg, Any registration Blocking cause code
--------------------------------------------------------------------------------------------------
18-Dec-15      asharm      944147    Update country code when MCC changes for all SIP messages
********************************************************************************************/
#ifndef __REGISTRATION_HANDLER_VoLTE_H__
#define __REGISTRATION_HANDLER_VoLTE_H__

#include "RegistrationHandler.h"

class CRegistrationHandlerVoLTE: public CRegistrationHandler,public RequestListener,public ResponseListener
{
public:
  CRegistrationHandlerVoLTE(CRegMonitorHandler *, ImsRegType);
  virtual ~CRegistrationHandlerVoLTE();
  QPVOID Init();
  QPVOID DeInit();
  QPVOID OmadmSettingsUpdate( QPUINT32  iDM_Services_Mask );
  QPVOID Reset(RESETCONTROLPARAMS resetControlParams = eIgnoreNone);
  QPBOOL CheckRegistrationNeeded();
  QPBOOL IsRegParametersReady();
  QPBOOL IsRegistered() {return m_eRegHandlerState==eRegStateRegistered;}
  QPVOID ResetCounts(QPBOOL n_bResetLimited=QP_FALSE);
  QPVOID Register(CPCSCFListHandler * m_pPCSCFListHandler);

  //routines to update ongoing GSM call status
  QPBOOL GetPendingRegStatus(){ return m_bPendingRegStatus; }
  QPVOID MarkRegStatusPending(QPBOOL m_bRegStatusPending){ m_bPendingRegStatus = m_bRegStatusPending; }  
  QPBOOL GetGSMCallStatus(){ return m_bGSMCallActive; }
  QPBOOL GetWaitForActiveCallStatus(){ return m_bWaitforActiveCall; }
  QPBOOL GetActiveCallStatus(){ return m_bActiveCallStatus; }
  QPVOID UpdateGSMCallStatus(QPBOOL);
  QPVOID UpdateWCDMACallStatus(QPBOOL bCallStatus);
   QPBOOL GetWCDMACallStatus(){ return m_bWCDMACallActive; }
  QPVOID HandleRegTypeChange();
  QPUINT16 GetPermBlockRegReason(){ return m_iPermBlockRegErrCode; }
  QPVOID ResetPermBlockRegReason(){ m_iPermBlockRegErrCode = 0;}
  QPBOOL IsRegPendingDueToServiceChange(){ return m_bPendingRegistration; }

  QPBOOL IsTdelayRunning();

  QPVOID StartUnpublishWaitTimer(QPBOOL n_bPwrDwn);
  
  // From ResponseListener
  virtual QPVOID notifyResponse(CommonComponentsBase* pCommonComponentsBase, QPE_CC_ERROR  eStatusCode);
  // From RequestListener
  virtual QPVOID notifyRequest(RequestHandler* pRequestHandler, QPE_CC_ERROR eStatusCode);
  QPVOID  TimerFired(QPINT n_iID);
  QPVOID GetImsConfig();
  QPVOID SetImsPriorityConfig();
  QPVOID CheckISIMPublicURI();
  QPVOID ResetRetry();
  QPVOID DeRegister();
  QPVOID EventDeRegistrationOk();
  QPVOID EventDeRegistrationFailure();
  QPVOID ClearRegService(RESETCONTROLPARAMS resetControlParams = eIgnoreNone, QPBOOL n_bSrvUnavailable=QP_TRUE);
  QPVOID ResetPublish();

  QPVOID HandleAddService(QPCHAR* n_pFeatureTags,QPBOOL n_bNonValidated);
  QPVOID HandleRemoveService(QPCHAR* n_pFeatureTags, QPBOOL n_bNonValidated);
  QPVOID CancelReRegistration();
  QPVOID HandleSetSipMsgHeader(QPCHAR* n_pSipMsgHeader);
  QPVOID ProcessAvailableServices();
  QPVOID ProcessServiceUnavailable(QPE_REG_FAILURE_TYPE n_eFailureType = eRegFailureTemp , QPUINT8 n_PSpecialErrorCode = 0);
  QPBOOL IPAddressChanged();
  QPVOID EventRegistrationRestoration(QPCHAR* n_pErrCode);
  QPVOID EventMethodResponse(QPCHAR* n_pMethodResponse);
  QPVOID EventActiveCallStatus(CPCSCFListHandler * n_pPCSCFListHandler, QPCHAR* n_pStatus);
  /* This method will handle the re-reg timer expiry event */
  QPBOOL HandleReRegistrationTimerExpired(const QPE_DCM_RAT & n_eCurrRAT,const QPBOOL& n_bIsValidRAT);
  QPVOID HandlePCSCFUnavailableCase();
  /* Handle registration failure*/
  QPVOID HandleRegistrationFailure(CPCSCFListHandler * n_pPCSCFListHandler);
  /* Handlle common reg/subscription/message failure */
  QPVOID ProcessRegFailure(CPCSCFListHandler * n_pPCSCFListHandler,QPBOOL n_iNextRegDelayNeeded);
  /*return the current reg status in form of string*/
  QPCHAR* GetCurrRegStateInfoSendToClient();
  /*return the current reg status of emergency reg in form of string*/
  QPCHAR* GetCurrEmerRegStateInfoSendToClient();
  /*Store the rat info*/
  QPVOID SetRAT(QPE_DCM_RAT iNewRat);
  QPBOOL CheckEmergencyRegistrationNeeded();
  /*Handle the APCS success notification*/
  QPVOID HandleAPCSNotification( CPCSCFListHandler *n_pPCSFHandler);
  /*Handle the RCSe ACS success notification*/
  QPVOID HandleRCSeACSNotification( CPCSCFListHandler *n_pPCSFHandler);

  /*set the auth type*/
  QPVOID SetAuthType(const QPE_AUTH_SCHEME_CONFIG &n_eAuthType);
  /*set the sec type*/
  /*QPVOID SetSecType(const QPE_SECURITY_MODE &n_eSecType);*/
  /*set the Auth Params */
  QPVOID SetAuthParam();
  /*Reset m_bIsPLMNBlockedPermanently */
  QPVOID UnSetPermBlock();
  /* Set test mode */
  QPVOID SetTestMode(QPBOOL bTestMode);
  /* Set bypass publish/presence enabled/disabled */
  QPVOID SetBypassPublish(QPBOOL bBypassPublish);
  QPVOID SetPendingRegReq();
  QPBOOL HandleRATTechnologyChange();
  /*Checks if Max Emergency Attempts are reached*/
  QPBOOL IsEmergencyAttemptReached();
  QPVOID StartNonRegisteredWaitTimer();
  QPVOID StartRegistraionForSessionFailure();
  QPCHAR* ParseString(QPCHAR *n_pInStr,QPCHAR *n_pToken,QPUINT32 &n_iOut);
  QPVOID CloseRequestProcessor();
  QPVOID SetWaitForByeSent(QPBOOL bWaitForByeSent);
  QPVOID StartDeRegResponseWaitTimer();
  QPVOID ResetRegRetryCntOn305();
  QPBOOL IsUEIPAddressChanged();
  QPBOOL IsRegistrationBlockedPermanently(){return m_bIsPLMNBlockedPermanently;};
  QPVOID AddWifiLocCountryHeader();

private:
  CRegistrationHandlerVoLTE(const CRegistrationHandlerVoLTE&);
  CRegistrationHandlerVoLTE& operator= (const CRegistrationHandlerVoLTE&);

  QPVOID CleanTransaction();
  QPVOID CleanRegTransaction();
  QPVOID AddRegistrationHeaders();
  QPVOID EventRegistrationOk();
  QPBOOL MoveToLimetedSrvOrBlockSrvPerm(CPCSCFListHandler * n_pPCSCFListHandler);
  /* This method will check for the current error code and start a
     back off timer based on that
  */
  QPBOOL WaitBeforeNextAttempt();
  QPUINT32 WaitBeforeNextAttemptMethod3(); //Determine wait time for Method3 retry
  QPVOID ReadNvItems();
  QPVOID ResetAllTimers(QPBOOL n_bStopRegRetryTimer = QP_TRUE);
  // Used to retrieve the correct public uri to use
  QPCHAR* GetPublicUserId();
  QPVOID EventPartialDeregistration(QPINT n_iReason);
  /* This method will move reg handler to not registered state , clean the transaction
     and raise an event to try the next registration
     Input parameter : QP_REGNODE_TOBE_ATTEMPTED - Based on this value, either the next
                       registration attempt will be current pscf or the first pcscf
  */
  QPVOID EventRegistrationDeactivation(QP_REGNODE_TOBE_ATTEMPTED n_eNode, QPE_CC_ERROR n_eStatusCode = CC_OK);
  QPVOID EventRegistrationRejectedAll();
  QPVOID EventShortenedAll();
  QPVOID EventNetworkInitDeRegistration(QPUINT n_iEvtType);
  /* Method will check with require condition to see whether next pcscf needs to be selected
     while triggering a new register or not
  */
  QPBOOL NeedToSelectNextPCSCF();
  QPBOOL BlockRegistrationPermanently(CPCSCFListHandler * n_pPCSCFListHandler, RM_EVENT block_event=QPE_RM_REGHANDLER_PERM_BLOCK_EV);
  /* Handle the reg/re-reg failure with error 400/402 */ 
  QPBOOL HandleBadReqOrPaymentReqErrorCase(CPCSCFListHandler * n_pPCSCFListHandler);
  /* Handle the reg/re-reg failure with error 403 */
  QPBOOL HandleForbiddenErrorCase(CPCSCFListHandler * n_pPCSCFListHandler);

  /* This method will be used to start the emergency re-registration timers */
  QPBOOL StartEmerReRegistrationTimers();
  /* This method fetches RCS IMS Core Config Values */
  QPVOID GetImsConfigRCSCore();
  /*Method to check for particular VoLTE header and return the status of it*/
  QPBOOL checkVolteSrvStatus(CommonComponentsBase* n_pCommonComponentsBase);
  QPCHAR* ResolveErrorStringInTmoMode();

  CTimerCust*            m_pRatChangeTdelayTimer;  // Wait timer for RAT change eHRPD->LTE. 
  CTimerCust*            m_pWaitUnpublishTimer;  // Pointer to Wait for Unpublish timer.
  CTimerCust*            m_pEmerRegResWaitTimer;  // Pointer to Wait for Emergency Response timer.
  QPCHAR*                m_iCustomTimerValues; 
  CTimerCust*            m_pWaitNonRegisteredTimer;  // Pointer to Wait for Unpublish timer.
  CTimerCust*            m_pWaitDeRegResponseTimer;  // Pointer to Wait for Dereg response.

  QPUINT32               m_iTimeoutBeforeNextAttempt;  // Timeout befor next attempt
  QPUINT32               m_iDelayedAttemptTimerVal;    // Delayed Attempt Timer. By default 15 secs
  QPUINT32               m_iDelayedAttemptCurrentTimerVal;    // Delayed Attempt Timer for calculated timer value cases
  QPUINT32               m_iDelayedAttemptMaxTimerVal;    // Maximum Delayed Attempt Timer for calculated timer value cases
  QPUINT16               m_iNtwkInitDeRegTimerVal;     // Network init de-reg timer value by default 60 secs
  QPUINT32               m_iRATchangeTdelaytimerVal;    // Wait time for eHRPD->LTE
  QPBOOL                 m_bLimitedAccessRegistration;    // True if in limited service
  QPUINT8                m_iMaxBadReqOrPaymentReqAttempts;      /* max 400/402 attempts */
  QPUINT8                m_iNumOfBadReqOrPaymentReqAttempts;    /* Current number of 400/402 attempts */
  // Proxy currently used
  QPBOOL                 m_bIsPLMNBlockedPermanently;
  /* test mode */
  QPBOOL                 m_bTestMode;
  /*OMADM*/
  QPBOOL                 m_bOmadmConfigEnabled;    // True if configured
  QPBOOL                 m_bOmadmPresenceEnabled;    // True if configured
  /* Hold the customer mode */
  QPUINT8                m_iOprtMode;
  /* The re-reg has to be attempted configurable number of times on same pcscf before
     attemping to next pcscf with new registration. This variable will store the
     current re-reg attempt
  */
  QPUINT8                m_iReRegFailureCnt;
  /* This method will store the rat in which registration was attempted 
     This is require to know when registration final expiry happens at that
     time whether there was any system change or not
  */
  QPE_DCM_RAT            m_eRatOnRegistrationAttempted;
  /* This will store the re-try after header value and if the value is present
     will overwrite the algorithim value
  */
  QPUINT32               m_iRetryAfterHeaderTimerVal;
  /*This counter will store the max number of attempt on same P-CSCF can be
    tried out
  */
  QPUINT8               m_iMaxRegRetryCnt;

  QPUINT8               m_iMaxRegRetryCnton305;
  QPUINT8               m_iRegRetryCnton305;

  /*This counter will store the max number of subscribe failure with 403/NOTIFY TERM 
  on wifi in TMO mode
  */
  QPUINT8               m_iMaxWifiSubscribeFailCnt;
  //vars for ongoing GSM call status
  QPBOOL                 m_bGSMCallActive;
  QPBOOL                 m_bWCDMACallActive;
  QPBOOL                 m_bPendingRegStatus;

  QPE_DCM_RAT            m_eLatestValidRat;

  SIPPUBLISHSTATE        m_eSipPublishState;
  SIPPUBLISHSTATE        m_eSipLastPublishState;
  SIPPUBLISHWAITSTATE    m_eSipPublishWaitState;
  QPBOOL                 m_bTestModeBypassPublish;
  QPBOOL                 m_bPendingRegistration;

  //Timers related to Method4
  QPUINT16               m_iTimerVal[QP_TIMER_CNT_METHOD4_MAX];

  //Store SIP Timer values in the context
  QPUINT16               m_iSIPTimerVal[QP_MAX_SIP_TIMER]; 
 
  //Tracking the 403 error counts
  QPUINT8                m_iForbiddenErrCnt; 

  QP_AUTO_CONFIG_STATUS  m_eAPCSSuccess;
  /*Store the temp port status in 305 error*/
  QPBOOL                 m_bTmpPCSCFPortToBeUsed;
  QPBOOL                 m_bActiveCallStatus;
  QPBOOL                 m_bWaitforActiveCall;
  QPBOOL                 m_bWaitforRCSEActiveSession;

  QPUINT8                m_iEmerRegAttemptCnt;
  QPUINT16               m_iPermBlockRegErrCode;

  /* Indicates whether to wait for Bye to be sent before sending deregister */
  QPBOOL                 m_bWaitForByeToBeSent;

  /* Indicates that deregistration has been triggered for SBM subscribe */
  QPBOOL                 m_bDeregSentForSubscribe;
  
  /* Handle the custom registration failure behavior according to RCSe*/
  QPVOID handleCustomRegFailureMethodRCSe(CPCSCFListHandler * n_pPCSCFListHandler);

  //APIs to send IPC to registred clients
  QPVOID UpdateAvailableServiceToRegisteredClients(QPINT n_iServiceStatus, QPBOOL n_bQueueInvite = QP_FALSE, QPCHAR* n_pfeatureTags = QP_NULL);
  QPCHAR m_arrVoiceAcceptContact[QP_SIP_MAX_HDR_VAL_LEN];

  //Keep track of number of attempt on same pcscf
  QPUINT8                m_iRegRetryCount;
  //Keep track of subscribe fail with 403/NOTIFY TERM on wifi in TMO mode
  QPUINT8                m_iWifiSubscribeFailCnt;
  QPBOOL                 m_bReleasePDN;
  //Detaches from LTE and blocks PLMN
  QPVOID LTEDetachAndBlockPLMN(QPUINT32 timervalue);
  /* Adding the contact header */
  QPVOID AddContactHeader();
  /* Adding the custom header based on oprt requirement */
  QPVOID AddCustomContactHeader();
  /* Handle the custom registration failure behavior according to method0*/
  QPVOID handleCustomRegFailureMethod0(CPCSCFListHandler * n_pPCSCFListHandler);
  /* Handle the custom registration failure behavior according to method1*/
  QPVOID handleCustomRegFailureMethod1(CPCSCFListHandler * n_pPCSCFListHandler);
  /* Handle the custom registration failure behavior according to method2*/
  QPVOID handleCustomRegFailureMethod2(CPCSCFListHandler * n_pPCSCFListHandler);
  /* Handle the custom registration failure behavior according to method3*/
  QPVOID handleCustomRegFailureMethod3(CPCSCFListHandler * n_pPCSCFListHandler);
  /* Handle the custom registration failure behavior according to method4*/
  QPVOID handleCustomRegFailureMethod4(CPCSCFListHandler * n_pPCSCFListHandler);
  /* Handle the default registration failure behavior*/
  QPVOID handleCustomRegFailureMethodDefault(CPCSCFListHandler * n_pPCSCFListHandler);
  /* While handling the custom registration failure all the pre handling
     of a failure done here
  */
  QPVOID handleCustomPreRegConditionHandling();
  /*Handling the custom behaviour of handling 403/404 case*/
  QPVOID handleCustomForbiddenErr(CPCSCFListHandler * n_pPCSCFListHandler);
  /*Handling the custom behaviour of handling 403 for SUBSCRIBE or NOTIFY TERMINATE for TMO*/
  QPVOID handleWifiForbiddenErr(CPCSCFListHandler * n_pPCSCFListHandler);
  /* Handle subscription failure event*/
  QPVOID eventSubscriptionFailureStatus(QPINT n_iReasonCode);
  /* Handle the emergency registration failure behavior*/
  QPVOID handleEmergencyRegFailure(CPCSCFListHandler * n_pPCSCFListHandler);
  /*Handle LTE failure scenario after reaching to max retry count on last P-CSCF*/
  QPVOID handleLTEFailureScenarioMethod1(CPCSCFListHandler * n_pPCSCFListHandler);
  QPVOID StartEmerResponseWaitTimer();
  /*Check ErorCode Needs to be added for TMO or not */
  QPBOOL IsCurrentRatIWLAN();
  
  QPBOOL IsACSParamChanged(QP_RCSE_PARMAS_VALUE* rcseParamValue, QPCHAR* source , QPCHAR* dest);
  QPBOOL IsHomeDomainChanged();
  QPBOOL IsPublicUserIdChanged();
  QPBOOL IsPrivateUserIdChanged();
  QPBOOL IsSipUserNameChanged();
  QPBOOL IsSipPasswordChanged();
  QPBOOL IsSipRealmChanged();
  QPBOOL IsAuthTypeChanged();
  QPBOOL AreRCSParamsEmpty();
  QPE_AUTH_SCHEME_CONFIG GetRCSEAuthScheme();
  QPBOOL IsReRegBlockOn3G_2GReq();
};

#endif
