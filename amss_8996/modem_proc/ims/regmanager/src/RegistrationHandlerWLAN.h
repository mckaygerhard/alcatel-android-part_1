/******************************************************************************************

 File: RegistrationHandlerWLAN.h
 Description: 
 
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/regmanager/src/RegistrationHandlerWLAN.h#1 $
$DateTime: 2016/03/28 23:03:22 $
$Author: mplcsds1 $

 Revision History
==========================================================================================
   Date    |   Author's Name    |  CR#   |  Review ID  |      Change Description
==========================================================================================
13-09-13  Priyank        543915     FR 3104: IMS on Wifi
-------------------------------------------------------------------------------
17-11-13  Sreenidhi      569045     When no P-CSCF available, UE stop the timer due to some bug and request PDN when RAT is changed.
-------------------------------------------------------------------------------
08-04-14   Ahmet       645389      Handle ReRegistration request while there is transaction going on
-------------------------------------------------------------------------------
01-05-15   c_snaras   770881      IMS REGISTER message is going out with old IP address in one of the corner LTE scenarios(timing dependent)
-------------------------------------------------------------------------------
20-03-15   Sreenidhi  810389      NO QMI log for call origination failure when initial Registration fails
-------------------------------------------------------------------------------
06-26-15     Manasi       852185   Support for non-validated numbers
********************************************************************************************/
#ifndef __REGISTRATION_HANDLER_WLAN_H__
#define __REGISTRATION_HANDLER_WLAN_H__

#include "RegistrationHandler.h"

class CRegistrationHandlerWLAN: public CRegistrationHandler,public RequestListener,public ResponseListener
{
public:
  CRegistrationHandlerWLAN(CRegMonitorHandler*, ImsRegType);
  virtual ~CRegistrationHandlerWLAN();
  QPVOID Init();
  QPVOID DeInit();
  QPVOID Reset(RESETCONTROLPARAMS resetControlParams = eIgnoreNone);
  QPVOID ClearRegService(RESETCONTROLPARAMS resetControlParams = eIgnoreNone, QPBOOL n_bSrvUnavailable = QP_TRUE);
  QPBOOL CheckRegistrationNeeded();
  QPBOOL IsRegParametersReady();
  QPBOOL IsRegistered() {return m_eRegHandlerState==eRegStateRegistered;}
  QPVOID ResetCounts(QPBOOL n_bResetLimited=QP_FALSE);
  QPVOID Register(CPCSCFListHandler*);
  QPBOOL HandleRATTechnologyChange() {return QP_FALSE;};
  
  QPVOID GetImsConfig();
  
  /* Handle registration failure*/
  QPVOID HandleRegistrationFailure(CPCSCFListHandler *n_pPCSCFListHandler);
  QPVOID  ResetRetry();
  QPVOID SetRAT(QPE_DCM_RAT iNewRat);
  QPVOID DeRegister();
  QPVOID HandleAddService(QPCHAR* n_pFeatureTags, QPBOOL n_bNonValidated);
  QPVOID HandleRemoveService(QPCHAR* n_pFeatureTags, QPBOOL n_bNonValidated);
  QPVOID CancelReRegistration();
  QPVOID HandleSetSipMsgHeader(QPCHAR* n_pSipMsgHeader);
  QPVOID ProcessServiceUnavailable(QPE_REG_FAILURE_TYPE n_eFailureType = eRegFailureTemp, QPUINT8 n_PSpecialErrorCode = 0);
  QPBOOL IPAddressChanged();
  
  QPVOID EventRegistrationRestoration(QPCHAR* n_pErrCode);
  QPVOID EventMethodResponse(QPCHAR* n_pMethodResponse);
  /*return the current reg status in form of string*/
  QPCHAR* GetCurrRegStateInfoSendToClient();
  // From ResponseListener
  QPVOID notifyResponse(CommonComponentsBase* pCommonComponentsBase, QPE_CC_ERROR  eStatusCode);
  // From RequestListener
  QPVOID notifyRequest(RequestHandler* pRequestHandler, QPE_CC_ERROR eStatusCode);
  QPVOID SetImsPriorityConfig();
  QPVOID TimerFired(QPINT n_iID);
  QPVOID HandlePCSCFUnavailableCase();
  QPBOOL HandleReRegistrationTimerExpired(const QPE_DCM_RAT & n_eCurrRAT,const QPBOOL& n_bIsValidRAT);
  QPVOID SetPendingRegReq();
  QPVOID CloseRequestProcessor();

private:
  CRegistrationHandlerWLAN(const CRegistrationHandlerWLAN&);
  CRegistrationHandlerWLAN& operator= (const CRegistrationHandlerWLAN&);

  QPVOID   CleanTransaction();
  QPVOID   ResetAllTimers(QPBOOL n_bStopRegRetryTimer = QP_TRUE);
  QPVOID   AddRegistrationHeaders();
  QPVOID   AddContactHeader();
  QPVOID   SetAuthParam();
  QPCHAR*  GetPublicUserId();
  /* Handle the default registration failure behavior*/
  QPVOID   handleCustomRegFailureMethodDefault(CPCSCFListHandler*);
  QPUINT32 WaitBeforeNextAttempt(); //Determine wait time for Method3 retry
  //APIs to send IPC to registred clients
  QPVOID UpdateAvailableServiceToRegisteredClients(QPINT n_iServiceStatus, QPCHAR* n_pfeatureTags = QP_NULL);
  QPVOID EventRegistrationDeactivation(QP_REGNODE_TOBE_ATTEMPTED);
  QPVOID EventRegistrationOk();
  QPVOID ProcessAvailableServices();
  QPVOID EventDeRegistrationOk();
  QPVOID EventDeRegistrationFailure();
  QPVOID eventSubscriptionFailureStatus(QPINT);
  QPVOID EventNetworkInitDeRegistration(QPUINT);
  QPVOID EventPartialDeregistration(QPINT);
  QPVOID EventShortenedAll();

private:
  QPUINT8                   m_iMaxRegRetryCnt;
  QPUINT32                  m_iDelayedAttemptTimerVal;    // Delayed Attempt Timer. By default 15 secs
  QPUINT16                  m_iNtwkInitDeRegTimerVal;     // Network init de-reg timer value by default 60 secs
  QPBOOL                    m_bLimitedAccessRegistration; // True if in limited service
  QPBOOL                    m_bPendingRegistration;
  //Keep track of number of attempt on same pcscf
  QPUINT8                   m_iRegRetryCount;
  /* This will store the re-try after header value and if the value is present
     will overwrite the algorithim value */
  QPUINT32                  m_iRetryAfterHeaderTimerVal;
  QPUINT32                  m_iDelayedAttemptCurrentTimerVal;    // Delayed Attempt Timer for calculated timer value cases
  QPUINT32                  m_iDelayedAttemptMaxTimerVal;    // Maximum Delayed Attempt Timer for calculated timer value cases
  QPUINT32                  m_iTimeoutBeforeNextAttempt;  // Timeout befor next attempt
};

#endif //__REGISTRATION_HANDLER_WLAN_H__