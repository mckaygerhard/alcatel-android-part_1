﻿/******************************************************************************************

 File: PDPRATHandlerWLAN.h
 Description: 
 
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/regmanager/src/PDPRATHandlerWLAN.h#1 $
$DateTime: 2016/03/28 23:03:22 $
$Author: mplcsds1 $

 Revision History
==========================================================================================
   Date    |   Author's Name    |  CR#   |  Review ID  |      Change Description
==========================================================================================
13-09-13     Priyank          543915       FR 3104: IMS on Wifi
-------------------------------------------------------------------------------
27-11-13     Priyank          522807       In the case of WLAN PM priority which defines the order from where the values 
                                           have to be read for IMS Registration is not taking into effect.
-------------------------------------------------------------------------------
14-03-14     Ahmet            631224      Handle emergency call if emergency PDP is deactivating
-------------------------------------------------------------------------------
08-09-14    c_snaras          708477      FR 22825: VZW IMS PDN retry failure handling for SRLTE device.
********************************************************************************************/

#ifndef __PDPRAT_HANDLER_WLAN_H__
#define __PDPRAT_HANDLER_WLAN_H__

#include "PDPRATHandler.h"

class CPDPRATHandlerWLAN: public CPDPRATHandler
{
public:
   CPDPRATHandlerWLAN(QP_PDP_TYPE n_ePDPType);
  ~CPDPRATHandlerWLAN();
  QPVOID Init();
  QPVOID DeInit();
  QPVOID Reset(QPBOOL n_bRatToReset=QP_TRUE);
  QPBOOL PdpActivate();
  QPVOID UpdateHOSysPrefInfo(QPUINT32 numAvailAPNs , QPDCM_PREF_SYS_INFO* sysPrefInfos);
  QPVOID PdpDeactivate();
  QP_RM_RECOVERY_TYPE DoPDNRecovery();
  QPUINT8 GetErrorBasedOnDCMFailure() {return 0;}
  QPVOID StopPDNRetrytimers();
  QPVOID UpdatePDNRetrytimers();
  QPBOOL CheckRegistrationNeeded();
  QPBOOL IsPDPActive() { return ((m_ePDPState == ePDPStatepdpActivated)? QP_TRUE:QP_FALSE );}
  PDPHANDLERSTATE GetPDPState() { return m_ePDPState;}
  QPVOID ResetCounts();
  QPBOOL IsTimerRunning();
  QPBOOL InitPDP(const QP_APN_RAT &n_sAPNRat,const QP_APN_ATTR_INFO & n_sApnAttrInfo);
  QPVOID HandleRatNotification(QPE_DCM_RAT n_eRatParams);
  static QPVOID DcmCallback(QPE_DCM_MSG tDcmMsg, QPDCM_CONN_PROFILE* pDcmProfile, QPVOID* pMsgData);
  QPVOID PDNRecoveryTimerFired(QPINT n_iID);
  QPBOOL SRLTEPDNRecoveryNeeded();
  QPVOID EventDSProfileChange(QPBOOL n_bIsProfileChangeEv, QP_DCM_PROFILE_CHANGE_PARAMS *sIMSProfileBuf = QP_NULL);
  QPVOID StopWifiMeasurementIfStarted(){/*This method is not needed in WLAN as of now*/}

private:
  CPDPRATHandlerWLAN(const CPDPRATHandlerWLAN&);
  CPDPRATHandlerWLAN& operator= (const CPDPRATHandlerWLAN&);

  QPVOID EventPdpFailure(QPE_DCM_FAILURE_MSG iFailureReason = DCM_FAILURE_UNKNOWN);
  QPVOID EventPdpDeActivated();
  QPVOID EventPdpActivated();
  QPVOID EventExtendedIPConfig();

private:
  QPUINT8                 m_iNumIntermediatePDPFailureCnt;        //Count of intermediate retries
  QPUINT8                 m_iMaxIntermediatePDPRetries;           //Maximum number PDP activation retries done before informing clients
  CPDNRecoveryTimer*      m_pPDNRetryTimer;
};

#endif // __PDPRAT_HANDLER_WLAN_H__