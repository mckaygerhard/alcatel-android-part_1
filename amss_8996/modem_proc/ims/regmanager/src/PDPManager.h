﻿/************************************************************************
 Copyright (c) 2013-2014 Qualcomm Technologies, Inc. All rights reserved.

 File Name      : PDPManager.h
 Description    : This class is like a proxy to the PDPRatHandler
                  RegisterManager will interface with PDPManager
                  and then PDPManager will take necessary action
                  based on the current state
				  
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/regmanager/src/PDPManager.h#2 $
$DateTime: 2016/05/15 20:11:14 $
$Author: huawenc $

 Revision History 

 ========================================================================
    Date    |   Author's Name    |  BugID           |        Change Description
 ========================================================================
 24-06-13       Sankalp          Initial version      Manage multiple PDP Rat handler
 -------------------------------------------------------------------------------
 13-09-13       Priyank        543915     FR 3104: IMS on Wifi
-------------------------------------------------------------------------------
 09-10-13       Priyank        522807     UE doesn't register on IMS after CSIM REFRESH (USIM Init and Full FCN)
-------------------------------------------------------------------------------------------------------------
 05-11-2013     Manasi         570254     RM to report the Registration failure cause when failed due to Service reject with EMM cause 9,10
-------------------------------------------------------------------------------
 14-11-13       Sankalp        562391       Oprt1 :Retry counter maintanence: - If the retry counter reaches NSIPretrycount and 
                                            there is no other P-CSCF available, then block PLMN for T3402 time
 -------------------------------------------------------------------------------
 02-01-14       Sankalp        584251     FR 18905: Camping on the network not 
                                          supporting IMS PS voice			
--------------------------------------------------------------------------------------------------
13-Feb-2014      asharm       609752        Back to back VoLTE emergency call related issue in Field (ZTE MF271A)
-------------------------------------------------------------------------------
 03-04-13       Manasi        613808        FR 17135: Disabling Connection to IMS PDN during Roaming [IMS-RegManager]
-------------------------------------------------------------------------------
14-03-14   Ahmet       631224      Handle emergency call if emergency PDP is deactivating
-------------------------------------------------------------------------------
08-04-14   Ahmet       656562      Idle Mode Registration on iWLAN
-------------------------------------------------------------------------------
28-08-14        Purna         FR22881     IMS PDN and IMS Registration Management for CMCC
---------------------------------------------------------------------------------------------
30-10-14    Priyank       719523     Oprt Req : Between VoLTE-On cell to VoLTE-Off cell UE's PDP should be retained
----------------------------------------------------------------------------------------------------
01-22-14       Manasi     777901   FR 25430: TMO - Handling No 911 Address on File
-------------------------------------------------------------------------------
01-23-15       Purna      782278     CMCC NS-IOT test case 5.2.9.1 was failing due to UE is sending De-regsiter request after moving back to LTE from GSM
-------------------------------------------------------------------------------------------------------------
04-Feb-14   Priyank    722987       FR 20991: Retry E911 call in IMS when in Operator's Whitelist MCC
-------------------------------------------------------------------------------
02-13-15     Purna      786932     [hVoLTE]2.2.5.9 VoLTE Fails : IMS triggering REG Req while on 1X call
-------------------------------------------------------------------------------
10-03-15       Priyank       738646   FR 23978: Registration related features for Oprt 11
-------------------------------------------------------------------------------
27-03-15       Manasi        791549  Handling of Bad wifi in TMO mode
-------------------------------------------------------------------------------
06-04-15       Sreenidhi     811524  FR 26778: IMS service disable in 3G on PDP failure with any error code
--------------------------------------------------------------------------------------------------
04-30-15      c_snaras    827644      FR 27427: [T-Mobile Wifi Calling Requirement] Handling of non-T-Mobile SIM or a non-GBA capable SIM card 
---------------------------------------------------------------------------------------------------------------
10-Jun-15     Manasi        846141   FR 27968: ATT - IMS registration failure handling and registration recovery mechanisms
--------------------------------------------------------------------------
05-27-14      Purna      844254   FR 26242: VzW - Separate WiFi vs. Cellular pref configuration for home and roaming: IMS RM Impact: Phase 1
--------------------------------------------------------------------------------------------------
17-Jul-15      Priyank  849956  UE does not SIP registration in IPv4 after SIP registration in IPv6.
-------------------------------------------------------------------------------
08-Aug-15    narasimha   882621   hvolte 3.2.7 on 8952 TA1.0: RE-REG is Still happening on eHRPD with Same P-CSCF Address List due to FR 25135
-------------------------------------------------------------------------------
27-Aug-15      Priyank  895112  In LGU mode, RM shall not trigger Force PDN release in case of LPM
--------------------------------------------------------------------------------------------------
18-Dec-15      asharm      944147    Update country code when MCC changes for all SIP messages
--------------------------------------------------------------------------------------------------
02-Feb-16      narasimha      968623    no IMS reg after VoLTE enable
--------------------------------------------------------------------------------------------------------------
8-Apri-2016    HuaWenC            1000436   FR 34771: CMCC - Unblock IMS upon TAU Accept with VoPS=1
*****************************************************************************************************/

#ifndef __IMS_PDP_MANAGER_H
#define __IMS_PDP_MANAGER_H
/* DEPENDENT HEADER FILES*/
#include "qpdcm.h"
#include "qpIMSPolicyManager.h"
#include "PDPRATHandler.h"

/* DEFINES */
#define QP_MAX_PDP_INSTANCE   4

typedef enum eQpRMSIMCARDGBAType
{
  GBA_STATUS_INVALID = -1,  
  GBA_STATUS_DISABLED = 0,
  GBA_STATUS_ENABLED = 1,
  GBA_STATUS_INVALIDCARD = 2 
}QP_RM_SIMCARD_GBA_TYPE;

typedef struct RatSelectionParameters
{
  QP_WIFI_PREFS   wifiPreferences; //this includes call mode preference and Wifi calling(VOWIFI)
  QPBOOL          lteVopsEnabled;
  QPBOOL          volteDisabled;  
  QPBOOL          videoDisabled;
  QPBOOL          Is1XAvaialable;
  QP_DPL_RAT_QUALITY_1x ratQuality1X;
  QPBOOL                IsRoamingEnabled;
}RAT_SEL_PARAMS;

/* FORWARD DECLARATION */
class QPRegInfo;
class CPDPRATHandler;
/* CLASS */
class CPDPManager
{
public:
  CPDPManager();
  ~CPDPManager(); 
  /*initialize few nv data and call backs*/
  QPVOID Init();
  /*initialize WLAN Interface */
  QPVOID InitWLAN();
  /*Just a proxy ..pass it appropriate pdp handler*/
  QPVOID DeInit(ImsRegType RegType);
  /*Block pdp activation permanently*/
  QPVOID SetPermBlock(QPBOOL n_bBlockBeforeTAU = QP_FALSE);
  /*Unblock pdp activation*/
  QPVOID UnSetPermBlock();
 /*Block pdp activation permanently on wifi in TMO Mode*/
  QPVOID SetPermBlockWifi();
  /*Unblock pdp activation on wifi in TMO mode*/
  QPVOID UnSetPermBlockWifi();
  /*Set a flag to block pdp activation permanently on rejected notification*/
  QPVOID SetPermBlockDueToRejEvt();
  /*Is the current received rat is valid or not*/
  QPBOOL IsRATValid();
  /*Stop pdp retry timers of all the pdp handlers*/
  QPVOID StopPDNRetrytimers();
  /*Reset the counts of all the pdp handlers*/
  QPVOID ResetCounts();
  QPVOID ResetCounts(ImsRegType RegType);
  /*Reset flags of all the PDP handlers*/
  QPVOID Reset();
  /*When call back received from lower layer call this public api to handle it*/
  QPVOID HandleRatNotification(QPE_DCM_RAT n_eDcmRat,QPE_DCM_RAT_MASK n_eRATMask,QPBOOL n_bForceRatChange = QP_FALSE);
  QPVOID HandleHoSysPrefInfo(QPUINT32 numAvailAPNs , QPDCM_PREF_SYS_INFO* sysPrefInfo);
  /*Process few messages based on the type of RM event*/
  QPVOID ProcessMessage(RM_EVENT);
  /*Whenever there is a PDP succes/failure this will get called to update the state of given pdp*/
  QPVOID PDPStateUpdate(QPBOOL n_bPdpActivationStatus,QP_PDP_TYPE n_ePDPType);
  /*Return conn profile for given pdp type*/
  QPDCM_CONN_PROFILE* GetConnProfile(QP_PDP_TYPE n_ePDPType);
  /*Return PDP ID for given pdp type*/
  QPUINT32 GetPDPID(QP_PDP_TYPE n_ePDPType);
  /*Get the pdp success message on which pdp got activated or present*/
  QPCHAR *GetClientIndication(QP_PDP_TYPE n_ePDPType);
  /*Just check whether PDP is active based on the regtype or pdp type*/
  QPBOOL IsPDPActive(ImsRegType RegType, QP_PDP_TYPE n_ePDPType = QP_PDP_NONE);
  /*Gives the current PDP state */
  PDPHANDLERSTATE GetPDPState(ImsRegType RegType);
  /*Reset the flags based on the PDP type*/
  QPVOID Reset(QP_PDP_TYPE n_ePDPType,QPBOOL n_bRatToReset=QP_TRUE);
  /*Handle the pdn recovery on current pdp type*/
  QPBOOL DoPDNRecovery(QP_PDP_TYPE n_ePDPType);
  /*Get the current rat info*/
  QPE_DCM_RAT GetRAT(QPBOOL bQueryServingSystem =QP_FALSE);
  /*Handle few events in update method based on the type of event*/
  QPVOID Update(RM_EVENT n_eRMEvt);
  /*Request to activate the PDP on given PDP type*/
  QPBOOL PdpActivate(QP_PDP_TYPE n_ePDPType);
  /*Check whether any timer running on given pdp type*/
  QPBOOL IsTimerRunning(QP_PDP_TYPE n_ePDPType);
  /*Stop the PDP retyr timer on given reg type*/
  QPVOID StopPDNRetrytimers(ImsRegType RegType);
  /*Activate PDP on given reg type*/
  QPBOOL PdpActivate(ImsRegType RegType);
  /*Update PDP retry timer based on the reg type*/
  QPVOID UpdatePDNRetrytimers(ImsRegType RegType);
  /*Check registration is require on a given reg type*/
  QPBOOL CheckRegistrationNeeded(ImsRegType RegType);
  /*PDP deactivate request based on reg type or PDP type*/
  QPVOID PdpDeactivate(ImsRegType RegType,QP_PDP_TYPE  n_ePDPType = QP_PDP_NONE);
  /*Get the handle of PDP rat handler based on the PDP type, this require for event notification reg.s*/
  QPVOID *GetHandle(QP_PDP_TYPE n_ePDPType);
  /*Delete the PDP based on the PDP type*/
  QPVOID CleanPDP(QP_PDP_TYPE n_ePDPType);
  /*return the reg mode based on the pdp type*/
  QPE_REGISTRATION_MODES GetRegMode(QP_PDP_TYPE n_ePDPType);
  /*return the reg mode based on the pdp type*/
  QPE_AUTH_SCHEME_CONFIG GetAuthorizationScheme(QP_PDP_TYPE n_ePDPType);
  QPE_DCM_FAILURE_MSG GetFailureReason(QP_PDP_TYPE n_ePDPType);
  /*return the reg mode based on the pdp type*/
  /*QPE_SECURITY_TYPE GetSecurityType(QP_PDP_TYPE n_ePDPType);*/
  /*extract rat from the rat mask and act accordingly*/
  QPBOOL  UpdateRatFromRatMask();
  /*handle the tty notification*/
  QPVOID EventTTYModeChanged(QPE_DCM_TYY_MODE_TYPE eTTYModeType);
  /*handle the roaming notification*/
  QPVOID EventRoamingIndication(QP_DCM_ROAMING_APNLIST_TYPE* pRoamingAPNList);
  /*Get the pdp status if active and provide the detail to reg. application*/
  QPCHAR* GetCurrPdpStateInfoSendToClient();
  /*This method is invoked by Register Manager when it receives a Settings Change event from DPL*/
  QPVOID HandleWifiSettingsChange();
  QPVOID HandleVolteAndVtStatusChange();

  /* Get Emergency PDP retry status*/
  QP_RM_EMER_PDN_RECOVERY_TYPE GetEmerRetryStatus(QP_PDP_TYPE n_ePDPType);
  QPVOID EventHOSuccess(QP_PDP_TYPE n_ePDPType);
  QPVOID SetQuickPDPActivateFlag(ImsRegType RegType);
  QP_PDP_TYPE HandlePMConfigUpdate();
/*This method is invoked by Register Manager in case when it plans to bring up PDP*/
  QPVOID BringUpPDPOnPreferredRAT();
  QPVOID BringUpPDPIfIWLAN();
  QPVOID BringUpPDPAfterRefreshType0();
  QPBOOL Is3GRatPresentInRatMask();
  QPBOOL CheckIfServiceConfigured(ImsRegType RegType);
  QPBOOL GetOperatorReservedPCO(QP_PDP_TYPE n_ePDPType,QPUINT8& pcoValue);
  QPVOID StartWait2GHandleRatTimer();
  QPVOID EventLteActiveStatusChanged(QP_DCM_LTE_ACTIVE_STATUS_IND *eLteActiveStatusInd);
  QPVOID EventSSInfoChange(QP_DCM_CHANGED_SSInfo *pDcmChangedSSInfo);
   QPBOOL GetVolteDisabled(){ return m_sRatSelParams.volteDisabled;}
  /*Reset Bad wifi flag*/
  QPVOID UnSetBadwifi();
  QPBOOL CheckBadwifi();
  QPVOID EventFallBackLTEBadWifi(QP_PDP_TYPE n_ePDPType);
  QPUINT8 GetErrorBasedOnDCMFailure(QP_PDP_TYPE n_ePDPType);
  QPVOID SetBlockPDNOnUMTS(QPBOOL SetBlockPDNOnUMTS);
  QPVOID UpdateSIMGBABitStatus();
  QPVOID UpdateSIMGBAType(QP_RM_SIMCARD_GBA_TYPE eSimCardGBAType);
  QPE_DCM_TYY_MODE_TYPE GetCurrentTTYModeType(){ return m_eTTYModeType;}
  QPBOOL IsHandOverInProgress(QP_PDP_TYPE n_ePDPType);
  QPVOID SetPowerDownStatus(QPBOOL n_bPowerDownStatus);
  QPBOOL IsVoWIFIServiceEnabled();
  QPBOOL CheckEmergencyPDPNeeded(QPE_DCM_RAT currentRAT);
  QPVOID HandleRatSelectionParamsChange();
  QPVOID HandleTAUCompleteInd();  

private:
  /* PRIVATE METHODS */
  /*Register call back with lower layer to get the generic info e.g rat, t3402 , tty mode*/
  static QPVOID GenericDcmCallback(QPE_DCM_MSG  tDcmMsg, QPVOID*  pUserData, QPVOID*    pMsgData);
  static QPVOID Handle2GRatAfterGSMCallEnd(QPINT iTimerType,QPUINT32 iTimerId,QPVOID* pUserData);
  /*Handle the rat change notification and take an action accordingly*/
  QPVOID  HandleRatChange(QPE_DCM_RAT  eNewDcmRat);
  /*This will take input as apn type and return the pdp type index*/
  QPINT8  GetAPNIndex(QPE_APN_TYPE n_eAPNType);
  /*Raise event to register manager*/
  QPVOID  RaiseEvent( RM_EVENT n_eRmEvent, QP_PDP_TYPE n_ePDPType = QP_PDP_NONE);
  /*handle the pdn failure scenario*/
  QPVOID  HandlePDNRecoveryCase(QP_PDP_TYPE n_ePDPType);
  /*Create profile based on the input parameter*/
  QPBOOL  CreateProfile(QPE_DCM_RAT n_eRat,QPE_APN_TYPE n_eAPNType);
  /*From the input PDP type check whether any other pdp which is active based on the current apn fb*/
  QP_PDP_TYPE CheckLowerPriPDPActive(QP_PDP_TYPE n_ePDPType);
  /*This method will take input as pdp type and return apn type*/
  QPE_APN_TYPE GetAPNType(QP_PDP_TYPE n_ePDPType);
  /*api call to create an emergency profile*/
  QPVOID CreateEmergencyProfile();

  QPVOID EventChangeRat(QPE_DCM_RAT n_eDcmRat,QPE_DCM_RAT_MASK n_eRATMask,QPBOOL n_bForceRatChange = QP_FALSE);
  QPBOOL IsServiceAllowed(const QP_PDP_TYPE& n_ePDPType,const QPE_DCM_RAT& n_eRATInfo);
  QPBOOL IsServiceAllowed(QPE_DCM_RAT preferredRAT);
  QPVOID HandleLteVOPSChange(QPE_DCM_DOM_SEL_CALL_TYPE n_eVopsIndicator);
   /* Queries DPL for wifi preference settings*/
  QPBOOL QueryrWifiPreferences(QP_WIFI_PREFS& wifiPrefs);
  /* Checks whether the passed wifi settings are different with the one we already have */
  QPBOOL IsWifiSettingsChanged(QP_WIFI_PREFS& newSettings);
  /*Copies the new wifi Settings to the one we hold as instance variable */
  QPVOID UpdateWifiPreferences(QP_WIFI_PREFS& newSettings);
  QP_WIFI_PREFS GetUpdatedWifiPreferences();
  QPVOID HandleCardGBADisabled();
  QPBOOL IsIwlanOrWlanOnlyInRatMask();
  /*Finds the preferred RAT based on the RAT mask. It ultimately queries PM to get the preferred RAT 
  based on the user preferences*/
  QPE_DCM_RAT GetPreferredRAT(QPE_DCM_RAT_MASK& n_eRATMask);
  QPE_DCM_RAT GetPreferredRATForOprt0(QPE_DCM_RAT_MASK& n_eRATMask , QPE_DCM_RAT preferredRATBasedonWifiPrefs);
  QPBOOL IsVoicePossibleOnRat(QPE_DCM_RAT n_eDcmRat);
  QPVOID HandleVolteStatusChange();
  QPVOID Handle1XStatusChange(QPBOOL is1XAvaialable);
  QP_ALLOWED_SERVICE_ON GetAllowedServices(const QP_PDP_TYPE& n_ePDPType,const QPE_DCM_RAT& n_eRATInfo );
/*Checks whether RAT has changed from WWAN to IWLAN  and vice versa*/
  QPBOOL CheckIfRATTechnologyChanged(QPE_DCM_RAT preferredRAT);
  /* Checks whether UE is currently using CMCC mode */
  QPBOOL IsCMCCMode();
  /*Sets the default Settings if none of the wifi settings are set*/
  QPVOID SetToDefaultWifiPreferencesIfNotSet(QP_WIFI_PREFS& wifiPrefs);
  QPBOOL isWlanBringupSupported();
  QPBOOL GetVolteAndVideoStatus(RAT_SEL_PARAMS& volteAndVtStatus);
  QPVOID StopWifiMeasurementsIfIwlanIsLost();
  QPBOOL Is1XVoiceCapable();
  QPVOID Handle1XQualityChange();
  QPVOID UpdateSingoConfig(QPE_DCM_RAT n_eNewRAT);
  QPVOID EventDCMProfileRefCnt0(QPUINT32*);
  QPVOID HandleWifiRadioChange(QPBOOL wifiRadio);
  QPBOOL IsNewAttach(QPE_DCM_SERVING_DOMAIN eNewSrvDomain);
  
  /* MEMBER VARIABLES */
  /*holding the instances of pdp handlers*/
  CPDPRATHandler        *m_pPDPRatHandler[QP_PDP_TYPE_MAX];
  /*store the current rat info*/
  QPE_DCM_RAT            m_eCurrentRAT;
  /*store the current rat mask info*/
  QPE_DCM_RAT_MASK       m_eCurrentRATmask;
  /*hold the status of current rat whether it is valid or not*/
  QPBOOL                 m_bIsRatValid;
  /*store the cache instance*/
  QPRegInfo             *m_pRegInfo;
  /*store the information whether registration is blocked permanently or not*/
  QPBOOL                 m_iBlockRegPermDueToRejEvt;
  /*store the info regarding the pdp permanet block status*/
  QPBOOL                 m_bBlockIMSPDNConnPermanently;
  /*store the info regarding the pdp permanet block status in wifi in TMO mode*/
  QPBOOL                 m_bBlockIMSPDNConnPermanentlyWifi;
  /*IMS is blocked but need unblock when TAU complete or Rat Change*/
  QPBOOL                 m_bBlockIMSPDNBeforeTAU;

  /*store the apn fall back for the current valid rat*/
  QP_APN_PRIORITY_INFO   m_sPriorityInfo;
  /*Current tty mode info*/
  QPE_DCM_TYY_MODE_TYPE   m_eTTYModeType;
  RAT_SEL_PARAMS m_sRatSelParams;

  MafTimer*              m_pWait2GHandleRatTimer;
  QPDCM_RAT_PARAMS       m_iTempRatParamsFor2G;
    /*Current LTE status indication */
  QP_DCM_LTE_ACTIVE_STATUS_IND   m_eLteActiveStatusInd;
  QP_RM_SIMCARD_GBA_TYPE   m_eSimCardGBAType;

  QPE_DCM_SERVING_DOMAIN m_eCurrSrvDomain;
  QPUINT8 m_iOprtMode;
  QPBOOL                 m_bWifiRadioStatusOn;  /* Wifi Radio on/off.*/

};
#endif
