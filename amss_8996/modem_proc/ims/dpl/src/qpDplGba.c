﻿/* $Author: mplcsds1 $*/
/* $DateTime: 2016/03/28 23:03:22 $ */
/* $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/dpl/src/qpDplGba.c#1 $ */

/******************************************************************************
 Copyright (C) 2008 Qualcomm. All Rights Reserved.

 File Name      : qpDplGba.c
 Description    : Implements BootStrap Gba procedure.

 Revision History
 ==============================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ==============================================================================
 5-May-2014    nishantk     -     789668      FR25901: DPL changes for migrating IMS over to UIM GBA solution
-------------------------------------------------------------------------------
 19-May-2014   nishantk           838756      DPL support for UIM changes moving request id to CB level for GBA bootstrap
************************************************************************/

#include "ims_variation.h"
#include "fs_public.h"       /* File system typedefs and prototypes   */
#include <rex.h>

#include <qpdpl.h>
#include <qplog.h>
#include <dssdns.h>
#include <qpConfigNVItem.h>
#include <qpDplIPSecAKA.h>
#include <qpdplCommonMisc.h>
#include <qpdplAmssMisc.h>
#include <qpAppConfigIds.h>

#include <nv.h>
#include <nv_items.h>
#include <qpimsmmgsdi.h>
#include "secapi.h"
#include <gba_lib.h>
#include <qpDplGba.h>
#include <qpPlatformConfig.h>

/* ======== MAPPING FUNCTION === **/

gba_session_type GetMappingGBASessionType(QPE_GBA_SESSION_TYPE session_type)
{
  gba_session_type nReturnType = GBA_SESSION_MAX;

  if(session_type == QC_GBA_3GPP_PROV_SESSION_PRI)
  {
    nReturnType = GBA_3GPP_PROV_SESSION_PRI;
  }
  else if(session_type == QC_GBA_3GPP_PROV_SESSION_SEC)
  {
    nReturnType = GBA_3GPP_PROV_SESSION_SEC;
  }
  else if(session_type == QC_GBA_NON_PROV_SESSION_SLOT_1)
  {
    nReturnType = GBA_NON_PROV_SESSION_SLOT_1;
  }
  else if(session_type == QC_GBA_NON_PROV_SESSION_SLOT_2)
  {
    nReturnType = GBA_NON_PROV_SESSION_SLOT_2;
  }
  else if(session_type == QC_GBA_SESSION_MAX)
  {
    nReturnType = GBA_SESSION_MAX;
  }

  return nReturnType;
}


QP_GBA_RESULT_ENUM_TYPE GetMappingGBAResultType(gba_result_enum_type result_type)
{
  QP_GBA_RESULT_ENUM_TYPE nReturnType = QP_GBA_CANCELLED;

  if(result_type == GBA_SUCCESS)
  {
    nReturnType = QP_GBA_SUCCESS;
  }
  else if(result_type == GBA_GENERIC_ERROR)
  {
    nReturnType = QP_GBA_GENERIC_ERROR;
  }
  else if(result_type == GBA_INCORRECT_PARAMS)
  {
    nReturnType = QP_GBA_INCORRECT_PARAMS;
  }
  else if(result_type == GBA_MEMORY_ERROR_HEAP_EXHAUSTED)
  {
    nReturnType = QP_GBA_MEMORY_ERROR_HEAP_EXHAUSTED;
  }
  else if(result_type == GBA_UNSUPPORTED)
  {
    nReturnType = QP_GBA_UNSUPPORTED;
  }
  else if(result_type == GBA_AUTH_FAILURE)
  {
    nReturnType = QP_GBA_AUTH_FAILURE;
  }
  else if(result_type == GBA_SIM_ERROR)
  {
    nReturnType = QP_GBA_SIM_ERROR;
  }
  else if(result_type == GBA_SIM_NOT_READY)
  {
    nReturnType = QP_GBA_SIM_NOT_READY;
  }
  else if(result_type == GBA_NETWORK_ERROR)
  {
    nReturnType = QP_GBA_NETWORK_ERROR;
  }
  else if(result_type == GBA_NETWORK_NOT_READY)
  {
    nReturnType = QP_GBA_NETWORK_NOT_READY;
  }
  else if(result_type == GBA_TIMEOUT)
  {
    nReturnType = QP_GBA_TIMEOUT;
  }
  else if(result_type == GBA_CANCELLED)
  {
    nReturnType = QP_GBA_CANCELLED;
  }
    else if(result_type == GBA_SERVER_ERROR)
  {
    nReturnType = QP_GBA_SERVER_ERROR;
  }
  return nReturnType;
}

/** == API DEFINATIONS === **/

QPVOID qpDplProcessBootStrapResponseEvent()
{
  QPC_GLOBAL_DATA* pGlobalData;
  QP_BOOTSTRAP_CALLBACK AuthCallback;
  QPUINT16              iAppData=0;

  IMS_MSG_MED_0(LOG_IMS_DPL, "qpDplProcessBootStrapResponseEvent - Entry");

  pGlobalData = qpDplGetGlobalData();
  if(pGlobalData == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplProcessBootStrapResponseEvent - globaldata is NULL");
    return;
  }
  if (pGlobalData && pGlobalData->pGBAInfoCircularBuffer)
  {
    QPUINT16 iSize = 0;
    QP_GBA_INFO_EVENT_CB_STRUCT* pGbaResponseInfoCbStruct = (QP_GBA_INFO_EVENT_CB_STRUCT*)qpDplCircularBufferPeek(pGlobalData->pGBAInfoCircularBuffer, &iSize);
    if (QP_NULL != pGbaResponseInfoCbStruct)
    {
      IMS_MSG_MED_2(LOG_IMS_DPL, "qpDplProcessBootStrapResponseEvent - sGbaResultType[%d]   sRequestHandle[%d]",pGbaResponseInfoCbStruct->sGbaResultType,pGbaResponseInfoCbStruct->sRequestHandle);
      while(iAppData<MAX_NUMBER_OF_IMS_BOOTSTRAP_GBA_INSTANCE && pGlobalData->pGBABootStrapAppData[iAppData] && pGlobalData->pGBABootStrapAppData[iAppData]->pRequestHandle != pGbaResponseInfoCbStruct->sRequestHandle)
      {
        iAppData++;
      }
      if(iAppData < MAX_NUMBER_OF_IMS_BOOTSTRAP_GBA_INSTANCE)
      {
        IMS_MSG_MED_1(LOG_IMS_DPL, "qpDplProcessBootStrapResponseEvent - iAppData[%d]",iAppData);
        AuthCallback = (QP_BOOTSTRAP_CALLBACK)pGlobalData->pGBABootStrapAppData[iAppData]->pFunctionPointer;
        if (QP_NULL != AuthCallback)
        {
          AuthCallback(pGbaResponseInfoCbStruct->sGbaResultType,&pGbaResponseInfoCbStruct->sGbaResponse,pGlobalData->pGBABootStrapAppData[iAppData]->pUserData);
          // After notifying clearing the the memory allocated
          if(pGlobalData->pGBABootStrapAppData[iAppData])
          {
            IMS_MSG_MED_1(LOG_IMS_DPL, "qpDplProcessBootStrapResponseEvent - pGBABootStrapAppData is freed index[%d]",iAppData);
            qpDplFree(MEM_IMS_DPL,pGlobalData->pGBABootStrapAppData[iAppData]);
            pGlobalData->pGBABootStrapAppData[iAppData] = QP_NULL;
          }
        }
      }
      else
      {
        IMS_MSG_ERR_1(LOG_IMS_DPL, "qpDplProcessBootStrapResponseEvent - no matching handle found iAppData[%d]",iAppData);
      }
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplProcessBootStrapResponseEvent - pGbaResponseInfoCbStruct is NULL");
    }
    (QPVOID)qpDplCircularBufferPop(pGlobalData->pGBAInfoCircularBuffer);
  }
  return;
}

QPVOID qpDplBootstrapResponse (gba_result_enum_type status,QPUINT32 request_id,const gba_response_data_type * resp_ptr,const void * user_data_ptr)
{
  QPC_GLOBAL_DATA*  pGlobalData = QP_NULL;
  QPVOID* gbaIDCbStruct = QP_NULL;

  IMS_MSG_MED_0(LOG_IMS_DPL, "qpDplBootstrapResponse - Entry");

  if(user_data_ptr == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplBootstrapResponse - user data is NULL");
    return;
  }
  pGlobalData = (QPC_GLOBAL_DATA*)user_data_ptr;

  if(resp_ptr == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplBootstrapResponse - resp_ptr data is NULL");
  }

  gbaIDCbStruct = qpDplCircularBufferReserve(pGlobalData->pGBAInfoCircularBuffer, sizeof(QP_GBA_INFO_EVENT_CB_STRUCT));
  if (QP_NULL != gbaIDCbStruct)
  {
    qpDplMemset(gbaIDCbStruct, 0, sizeof(QP_GBA_INFO_EVENT_CB_STRUCT));
    ((QP_GBA_INFO_EVENT_CB_STRUCT*)gbaIDCbStruct)->sGbaResultType = GetMappingGBAResultType(status);
    ((QP_GBA_INFO_EVENT_CB_STRUCT*)gbaIDCbStruct)->sRequestHandle = request_id;
    IMS_MSG_MED_2(LOG_IMS_DPL, "qpDplBootstrapResponse - request_id[%d] sGbaResultType[%d]",request_id,status);
    if(resp_ptr)
    {
      ((QP_GBA_INFO_EVENT_CB_STRUCT*)gbaIDCbStruct)->sGbaResponse.error_code     = resp_ptr->resp_data.server_error.error_code;
      ((QP_GBA_INFO_EVENT_CB_STRUCT*)gbaIDCbStruct)->sGbaResponse.sBtid.iLen     = resp_ptr->resp_data.success.btid.data_len;
      ((QP_GBA_INFO_EVENT_CB_STRUCT*)gbaIDCbStruct)->sGbaResponse.sImpi.iLen     = resp_ptr->resp_data.success.impi.data_len;
      ((QP_GBA_INFO_EVENT_CB_STRUCT*)gbaIDCbStruct)->sGbaResponse.sLifetime.iLen = resp_ptr->resp_data.success.lifetime.data_len;
      IMS_MSG_MED_1(LOG_IMS_DPL, "qpDplBootstrapResponse - error_code[%d]",resp_ptr->resp_data.server_error.error_code);
      qpDplMemscpy((((QP_GBA_INFO_EVENT_CB_STRUCT*)gbaIDCbStruct)->sGbaResponse.sKsNaf),QP_GBA_KS_NAF_LEN, &resp_ptr->resp_data.success.ks_naf, GBA_KS_NAF_LEN);
      qpDplMemscpy(&(((QP_GBA_INFO_EVENT_CB_STRUCT*)gbaIDCbStruct)->sGbaResponse.sBtid.sData),QP_GBA_MAX_BTID_LEN, &resp_ptr->resp_data.success.btid.data,resp_ptr->resp_data.success.btid.data_len);
      qpDplMemscpy(&(((QP_GBA_INFO_EVENT_CB_STRUCT*)gbaIDCbStruct)->sGbaResponse.sImpi.sData),QP_GBA_MAX_IMPI_LEN, &resp_ptr->resp_data.success.impi.data, resp_ptr->resp_data.success.impi.data_len);
      qpDplMemscpy(&(((QP_GBA_INFO_EVENT_CB_STRUCT*)gbaIDCbStruct)->sGbaResponse.sLifetime.sData),QP_GBA_MAX_LIFETIME_LEN, &resp_ptr->resp_data.success.lifetime.data,resp_ptr->resp_data.success.lifetime.data_len);
    }
    qpDplCircularBufferConfirm(pGlobalData->pGBAInfoCircularBuffer, QP_TRUE);
    if (QP_FALSE == qpDplPostEventToEventQueue(QPCMSG_DPL_GBA_INFO_MSG,
      0,
      0,
      pGlobalData))
    {
      MSG_MED("qpDplBootstrapResponse - FAILURE in posting event", 0, 0, 0);
      (QPVOID)qpDplCircularBufferPop(pGlobalData->pGBAInfoCircularBuffer);
    }
    return;
  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpDplBootstrapResponse - qpDplCircularBufferReserve gives NULL");
    return;
  }
}

QP_EXPORT_C QPBOOL qpDplStartBootstrapProcedure(QP_BOOTSTRAP_PARAM *n_pGBAParam,QPE_CARD_DATA eCardType,QPBOOL eForceBootstrap)
{
  QPBOOL bSuccess = QP_TRUE;
  QPC_GLOBAL_DATA*  pGlobalData = QP_NULL;
  QPUINT8 index = 0;
  gba_naf_id_type  naf_id_ptr;
  gba_aid_type     aid_ptr;
  gba_session_type     pSessionType;
  char              *apn_data_ptr = QP_NULL;
  QPUINT16            iAppCounter=0;
  QPUINT8            apn_len = 0;

  IMS_MSG_MED_0(LOG_IMS_DPL, "qpDplStartBootstrapProcedure - Entry");

  pGlobalData = qpDplGetGlobalData();
  if((pGlobalData == QP_NULL) || (QP_NULL == n_pGBAParam))
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplStartBootstrapProcedure - globaldata is NULL");
    return QP_FALSE;
  }

  if(n_pGBAParam->BootStrapUserData.BootStrapCallBack == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplStartBootstrapProcedure - n_pGBAParam->AuthUserData.AUTHCallBack is NULL");
    return QP_FALSE;
  }
  
  while(pGlobalData->pGBABootStrapAppData[iAppCounter] != QP_NULL)
  {
    iAppCounter++;
  }
  if(pGlobalData->pGBABootStrapAppData[iAppCounter] == QP_NULL && iAppCounter<MAX_NUMBER_OF_IMS_BOOTSTRAP_GBA_INSTANCE)
  {
    IMS_MSG_MED_1(LOG_IMS_DPL, "qpDplStartBootstrapProcedure | mallocing the pGBABootStrapAppData->iAppCounter[%d]",iAppCounter);
    pGlobalData->pGBABootStrapAppData[iAppCounter] = (QP_BOOTSTRAP_APP_DATA *) qpDplMalloc(MEM_IMS_DPL, sizeof(QP_BOOTSTRAP_APP_DATA));
    if(QP_NULL == pGlobalData->pGBABootStrapAppData[iAppCounter])
    {
      IMS_MSG_FATAL_1(LOG_IMS_DPL,"qpDplStartBootstrapProcedure- pGBABootStrapAppData Malloc Failure for index[%d]",iAppCounter);
      return QP_FALSE;
    }
    qpDplMemset(pGlobalData->pGBABootStrapAppData[iAppCounter],0x00,sizeof(QP_BOOTSTRAP_APP_DATA));
  }
  else
  {
    IMS_MSG_FATAL_1(LOG_IMS_DPL,"qpDplStartBootstrapProcedure- unable to find free instance for pGBABootStrapAppData index[%d]",iAppCounter);
    return QP_FALSE;
  }

  pGlobalData->pGBABootStrapAppData[iAppCounter]->pFunctionPointer = (QPVOID*)n_pGBAParam->BootStrapUserData.BootStrapCallBack;
  pGlobalData->pGBABootStrapAppData[iAppCounter]->pUserData  = (QPVOID*)n_pGBAParam->BootStrapUserData.pUserData;
  pGlobalData->pGBABootStrapAppData[iAppCounter]->pCardData  = eCardType;

  IMS_MSG_HIGH_2(LOG_IMS_DPL, "qpDplStartBootstrapProcedure | iAppCounter[%d] eCardType - %d",iAppCounter,eCardType);

  qpDplMemset(&aid_ptr,0,sizeof(gba_aid_type));
  qpDplMemset(&naf_id_ptr,0,sizeof(gba_naf_id_type));

  if(eCardType == QP_CARD_ISIM)
  {
    pSessionType = GBA_NON_PROV_SESSION_SLOT_1;
    for(index = 0;index < IMS_DPL_MAX_NUM_APP ;index++)
    {
      if(is_isim_aid(&pGlobalData->pSessionIDTable[index]->app_info.aid))
      {
        aid_ptr.data_len = pGlobalData->pSessionIDTable[index]->app_info.aid.data_len;
        qpDplMemscpy(&aid_ptr.data,GBA_MAX_AID_LEN,&pGlobalData->pSessionIDTable[index]->app_info.aid.data_ptr,pGlobalData->pSessionIDTable[index]->app_info.aid.data_len);
        break;
      }
    }
  }
  else if(eCardType == QP_CARD_USIM)
  {
    pSessionType = GBA_3GPP_PROV_SESSION_PRI;
  }

  qpDplMemscpy(&naf_id_ptr.security_protocol,GBA_NAF_SECURITY_PROTOCOL_LEN,n_pGBAParam->sNafIdPtr.sNAFSecurityProtocolType,QP_GBA_NAF_SECURITY_PROTOCOL_LEN);
  qpDplMemscpy(&naf_id_ptr.fqdn.data,GBA_MAX_NAF_FQDN_LEN,n_pGBAParam->sNafIdPtr.sFQDNType.sData,n_pGBAParam->sNafIdPtr.sFQDNType.iLen);
  naf_id_ptr.fqdn.data_len = n_pGBAParam->sNafIdPtr.sFQDNType.iLen;


  if(n_pGBAParam->SApnData)
  {
    apn_len = qpDplStrlen(n_pGBAParam->SApnData);
    apn_data_ptr = (QPCHAR *) qpDplMalloc(MEM_IMS_DPL, sizeof(QPCHAR)*(apn_len+1));
    if(QP_NULL == apn_data_ptr)
    {
      IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpDplStartBootstrapProcedure- apn_data_ptr Malloc Failure");
      return QP_FALSE;
    }
    qpDplMemset(apn_data_ptr,0,sizeof(QPCHAR)*(apn_len+1));
    qpDplStrlcpy(apn_data_ptr,n_pGBAParam->SApnData,(apn_len+1));
  IMS_MSG_HIGH_3_STR(LOG_IMS_DPL, "qpDplStartBootstrapProcedure | apn_data_ptr[%s]  pSessionType[%d]  eForceBootstrap[%d]",apn_data_ptr,pSessionType,eForceBootstrap);
  }
  else
  {
    IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpDplStartBootstrapProcedure- apn_data_ptr is NULL passed by application");
  }

  if(gba_bootstrap(&naf_id_ptr,pSessionType,&aid_ptr,eForceBootstrap,&pGlobalData->pGBABootStrapAppData[iAppCounter]->pRequestHandle,apn_data_ptr,qpDplBootstrapResponse,(QPVOID*)pGlobalData) != GBA_SUCCESS)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplStartBootstrapProcedure | gba_bootstrap failing");
    bSuccess = QP_FALSE;
  }
  
  if(apn_data_ptr)
  {
    qpDplFree(MEM_IMS_DPL,apn_data_ptr);
    apn_data_ptr = QP_NULL;
  }

  IMS_MSG_HIGH_2(LOG_IMS_DPL, "qpDplStartBootstrapProcedure | pRequestHandle[%d] bSuccess - %d",pGlobalData->pGBABootStrapAppData[iAppCounter]->pRequestHandle,bSuccess);
  return bSuccess;   
}


QP_EXPORT_C QPBOOL qpDplGBAKeyValid(QP_GBA_KEY_PARAM *n_pCheckGBAParam,QPE_CARD_DATA eCardType)
{
  QPBOOL bSuccess = QP_TRUE;
  gba_aid_type    aid_ptr;
  gba_btid_type   btid_ptr;
  gba_lifetime_type     life_time_ptr;
  QPUINT8 index = 0;
  QPC_GLOBAL_DATA*  pGlobalData = QP_NULL;
  gba_session_type     pSessionType;

  IMS_MSG_MED_1(LOG_IMS_DPL, "qpDplGBAKeyValid - Entry eCardType[%d]",eCardType);

  pGlobalData = qpDplGetGlobalData();
  if((pGlobalData == QP_NULL) || (QP_NULL == n_pCheckGBAParam))
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplGBAKeyValid - globaldata is NULL");
    return QP_FALSE;
  }

  qpDplMemset(&aid_ptr,0,sizeof(gba_aid_type));
  qpDplMemset(&btid_ptr,0,sizeof(gba_btid_type));
  qpDplMemset(&life_time_ptr,0,sizeof(gba_lifetime_type));

  if(eCardType == QP_CARD_ISIM)
  {
    pSessionType = GBA_NON_PROV_SESSION_SLOT_1;
    for(index = 0;index < IMS_DPL_MAX_NUM_APP ;index++)
    {
      if(is_isim_aid(&pGlobalData->pSessionIDTable[index]->app_info.aid))
      {
        aid_ptr.data_len = pGlobalData->pSessionIDTable[index]->app_info.aid.data_len;
        qpDplMemscpy(&aid_ptr.data,GBA_MAX_AID_LEN,&pGlobalData->pSessionIDTable[index]->app_info.aid.data_ptr,pGlobalData->pSessionIDTable[index]->app_info.aid.data_len);
        break;
      }
    }
  }
  else if(eCardType == QP_CARD_USIM)
  {
    pSessionType = GBA_3GPP_PROV_SESSION_PRI;
  }

  qpDplMemscpy(&btid_ptr.data,GBA_MAX_BTID_LEN,n_pCheckGBAParam->sBtidPtr.sData,n_pCheckGBAParam->sBtidPtr.iLen);
  btid_ptr.data_len = n_pCheckGBAParam->sBtidPtr.iLen;

  qpDplMemscpy(&life_time_ptr.data,GBA_MAX_LIFETIME_LEN,n_pCheckGBAParam->sLifeTimePtr.sData,n_pCheckGBAParam->sLifeTimePtr.iLen);
  life_time_ptr.data_len = n_pCheckGBAParam->sLifeTimePtr.iLen;

  IMS_MSG_MED_1(LOG_IMS_DPL, "qpDplGBAKeyValid -  pSessionType[%d]",pSessionType);

  if(!gba_is_key_valid(pSessionType,&aid_ptr,&btid_ptr,&life_time_ptr))
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplGBAKeyValid | gba_is_key_valid is Failing");
    bSuccess = QP_FALSE;
  }
  return bSuccess;
}


QP_EXPORT_C QPBOOL qpDplGBACancel(QPE_CARD_DATA eCardType)
{
  QPBOOL bSuccess = QP_TRUE;
  QPC_GLOBAL_DATA*  pGlobalData = QP_NULL;
  QPUINT32           iAPPData = 0;

  IMS_MSG_MED_1(LOG_IMS_DPL, "qpDplGBACancel - Entry eCardType[%d]",eCardType);

  pGlobalData = qpDplGetGlobalData();
  if(pGlobalData == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplStartBootstrapProcedure - globaldata is NULL");
    return QP_FALSE;
  }

  while(pGlobalData->pGBABootStrapAppData[iAPPData] && pGlobalData->pGBABootStrapAppData[iAPPData]->pCardData != eCardType)
  {
    iAPPData++;
  }
  if(iAPPData >= MAX_NUMBER_OF_IMS_BOOTSTRAP_GBA_INSTANCE)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplStartBootstrapProcedure - No such APP data Found");
    return QP_FALSE;
  }

  IMS_MSG_MED_2(LOG_IMS_DPL, "qpDplGBACancel - iAPPData[%d]  pRequestHandle[%d]",iAPPData,pGlobalData->pGBABootStrapAppData[iAPPData]->pRequestHandle);

  if(gba_cancel(pGlobalData->pGBABootStrapAppData[iAPPData]->pRequestHandle) != GBA_SUCCESS)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpDplStartBootstrapProcedure - gba_cancel Failing");
    bSuccess = QP_FALSE;
  }

  if(pGlobalData->pGBABootStrapAppData[iAPPData])
  {
    qpDplFree(MEM_IMS_DPL,pGlobalData->pGBABootStrapAppData[iAPPData]);
    pGlobalData->pGBABootStrapAppData[iAPPData] = QP_NULL;
  }

  return bSuccess;
}