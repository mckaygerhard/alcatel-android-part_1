/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

qvp_rtp_codec. c

GENERAL DESCRIPTION

This file contains functions which talks to DPL CVD for various codec operations.

Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is
regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.


                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
30-10-15  pradeepg CR928766 - Device A doesn't play "busy" tone after reject of B party and VoLTE call is possible only after reboot
11-06-15  pradeepg CR789244    - FR 25455: Internal - Improvements to WiFi Interface selection criteria, Media metrics
24/07/13   vailani  CR#: 513217 - support for ATT log packet 0x17F2
25/08/15  Manju 	CR# :894000 - Incoming Alerting call get Stuck showing "Incoming" when SRVCC is triggered
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==*=*/


#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP

#ifdef  RTP_FILE_NUMBER 
  #undef RTP_FILE_NUMBER 
#endif 
#define RTP_FILE_NUMBER 4

/*===========================================================================
INCLUDE FILES FOR MODULE
===========================================================================*/
#include <comdef.h>
#include <stdlib.h>
#include "string.h"
#include "qvp_rtp_log.h"
#include "qvp_rtp_codec.h"
#include "qvp_app_common.h"
#include "qvp_rtp_codec_txt.h"
#include "qvp_rtp.h"

#define QVP_RTP_AMR_MODE_RATE_SID_5  5
#define QVP_RTP_EVS_PRIM_MODE_RATE_SID_5  6
#define QVP_RTP_EVRC_RATE_8  2
#define QVP_RTP_AUD_MAX_EVRC_BUF_SIZE 22
#define QVP_RTP_G711AU_SID  11
#define QVP_RTP_AUD_MAX_G711_BUF_SIZE 160
#define QVP_MAX_CMR_PENDING_REQ 2
boolean g_is_release_pending = FALSE;
extern boolean srvcc_tear_down_req;
extern boolean clear_all_session_req;
extern boolean g_enable_srvcc_optm;
qvp_rtp_rat_type		g_rat_type = QVP_RTP_RAT_LTE;
extern void qvp_rtp_process_tx_pkt
(
 qvp_rtp_app_id_type    app_id,      /* application which opened
                                     * the stream
                                     */
                                     qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
                                     uint8* frame_data,
                                     uint16 frame_length,
                                     uint32 tstamp,
                                     uint64 avtimestamp
                                     );
extern void qvp_rtp_service_sendaudio_report(uint32 n_imsntp_time, /* Most significant NTP Timestamp */
                              uint32 n_ilsntp_time, /* Least significant NTP Timestamp */
                              uint32 n_irtp_time_stamp /* RTP Timestamp */
                              );
extern void qvp_rtp_service_audio_rx_delay(uint16 rx_delay/*rx_delay of audio*/);
extern void qvp_rtp_service_media_codec_release_req_msg(qvp_rtp_app_id_type\
                    app_id, qvp_rtp_media_type media_type, boolean start_timer);
extern void qvp_rtp_mark_pending_task_done(qvp_rtp_app_id_type app_id);
extern qvp_rtp_stream_id_type qvp_rtp_find_internal_streamid ( qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type stream_id, int is_app);
extern void qvp_rtp_handoff_queue_commands(qvp_rtp_app_id_type app_id,boolean is_modem_valid);
extern void qvp_rtp_execute_pending_command(qvp_rtp_app_id_type app_id);
extern void qvp_rtp_post_execute_pending_command(qvp_rtp_app_id_type app_id);
extern boolean qvp_rtp_send_audio_release_srvcc_ind();

extern uint64 g_Dec_RequestTime;
extern boolean g_isNetworkCommandPending;
extern qvp_rtp_handoff_state	g_handoffstate;

//global codec context
qvp_rtp_codec_info_ctx codec_info_ctx;

void qvp_rtp_GetDecoderTimeline()
{
	qpAudioGetDecoderTimeline();
	return;
}
extern void qvp_rtp_response_handler
(
	qvp_rtp_app_id_type    app_id,		/* application which opened
										 * the stream
										 */
	qvp_rtp_stream_id_type	stream_id,	 /* stream which was opened */										 
	qvp_rtp_app_data_type  app_data,	
	qvp_rtp_response_type  msg_type, /*  msg type for ehich response has to be sent out */	
	qvp_rtp_status_type  status,
	boolean				  is_modem,  /* Is the response form modem*/
	qvp_rtp_stream_direction direction
);


void qvp_rtp_execute_pending_handoff(void);

extern void qvp_rtp_post_execute_pending_command(qvp_rtp_app_id_type app_id);

/*===========================================================================
FUNCTION qvp_rtp_codec_jb_retrieve_voip_xr_param
==============================================================================*/
qvp_rtp_status_type qvp_rtp_codec_jb_retrieve_voip_xr_param  ( QPVOID* data	  )
 {
  QP_AUDIO_DESC       audioDesc;

 // QVP_RTP_MSG_MED("API qvp_rtp_codec_jb_retrieve_voip_xr_param entered", 0, 0, 0);

  audioDesc = codec_info_ctx.audio_codec_info.audioDesc;
  //check for the initialized code descriptor.
  if(audioDesc == NULL)
  {
    QVP_RTP_ERR("qvp_rtp_codec_jb_retrieve_voip_xr_param : Trying to config codec with null codecDesc !!!", 0, 0, 0);

    return QVP_RTP_ERR_FATAL;
  }

  if( qp_get_xr_qdjparam(audioDesc, data) == FALSE )
  		return TRUE;
  else
		return FALSE;
  
 }
/*===========================================================================

FUNCTION QVP_RTP_LOG_QDJ_LOSS 

DESCRIPTION
  log the QDJ loss frame in RTP loss log

DEPENDENCIES
  None

PARAMETERS
    lost_pkt_cnt	 -		  Number of packets lost
	last_rtp_seq	 -		  Sequence number of the last RTP packet before the lost frames(s)
	loss_type		 -		  Indicates loss type
	lost_frame_cnt	 -		  0  Entire packet  is dropped 
							  1 -255  indicates the number of frames lost in the packet

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void qvp_rtp_log_qdj_loss ( uint8 lost_pkt_cnt,uint16 last_rtp_seq, qvp_rtp_loss_type loss_type, uint8 lost_frame_cnt)
{
	qvp_rtp_log_pkt_loss rtp_loss_pkt;
    memset( &rtp_loss_pkt , 0, sizeof( qvp_rtp_log_pkt_loss ) );

	rtp_loss_pkt.version = 6;
	rtp_loss_pkt.nlost = lost_pkt_cnt;
	rtp_loss_pkt.seq = last_rtp_seq;
	rtp_loss_pkt.ssrc = 0;

	if(codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_AMR) {
		rtp_loss_pkt.codectype = AMR;
	}
	else if(codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_G722_2) {
		rtp_loss_pkt.codectype = AMR_WB;
	}
	else if(codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_G711_U) {
		rtp_loss_pkt.codectype = G711_U;
	}
	else if(codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_G711_A) {
		rtp_loss_pkt.codectype = G711_A;
	}
	else if(codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_EVS) {
		rtp_loss_pkt.codectype = EVS;
	}
	
	//rtp_loss_pkt.codectype = codec_info_ctx.audio_codec_info.current_codec;
	rtp_loss_pkt.loss_type = loss_type;
	rtp_loss_pkt.nframeslost = lost_frame_cnt;

	 /* log the number of loss packets*/
	QVP_RTP_ERR_1(" qvp_rtp_log_qdj_loss Log RTP/QDJ packet loss: nframeslost = %d\r\n", rtp_loss_pkt.nframeslost);
	 QVP_RTP_MSG_HIGH_MEM("qvp_rtp_log_qdj_loss\
                    ver = %d \n lost_pkt_cnt %d \n last_rtp_seq = %d", rtp_loss_pkt.version, rtp_loss_pkt.nlost, rtp_loss_pkt.seq);
	 	 QVP_RTP_MSG_HIGH_MEM("qvp_rtp_log_qdj_loss\
                    ssrc = %d \n codectype %d \n loss_type = %d", rtp_loss_pkt.ssrc, rtp_loss_pkt.codectype, rtp_loss_pkt.loss_type);

     rtp_log(LOG_IMS_RTP_PACKET_LOSS_C, &rtp_loss_pkt, sizeof(qvp_rtp_log_pkt_loss));

	return;
	
}
/*===========================================================================
FUNCTION qvp_rtp_get_voice_call_params_from_QDJ

DESCRIPTION
  This function gets QDJ related voice call statistics parameters

PARAMETERS
  qvp_rtp_QDJ_voice_call_stat_params - pointer to QDJ_voice_call_stat_params
  codec_type - codec type
  
DEPENDENCIES
  None.

RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful
  appropriate error code otherwise


SIDE EFFECTS
  None.
===========================================================================*/
void qvp_rtp_get_voice_call_params_from_QDJ( qvp_rtp_QDJ_voice_call_stat_params* QDJ_voice_call_stat_params,
													   int8 *codec_type )
{

	if(codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_AMR) {
		*codec_type = AMR;
	}

	else if(codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_G722_2) {
		*codec_type = AMR_WB;
	}

	else if(codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_G711_A) {
		*codec_type = G711_A;
	}

	else if(codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_G711_U) {
		*codec_type = G711_U;
	}	
	else if(codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_EVS) {
		*codec_type = EVS;
	}		

QVP_RTP_MSG_HIGH_1_MEM("qvp_rtp_get_voice_call_params_from_QDJ :codec type = %d", *codec_type);
	qpAudioGetQDJVoiceCallStat( QDJ_voice_call_stat_params );

	return;
}
/*===========================================================================
FUNCTION qvp_rtp_codec_audio_cb

DESCRIPTION
This is the callback rtp uses to indicate the various DPL codec events

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
void qvp_rtp_codec_audio_cb(QPE_AUDIO_MSG tAudioMsg,
                            void* pParam1,
                            uint32 iParam2,
                            void* pUserData)
{

  qvp_rtp_codec_info_ctx * codec_info_cb_ptr = (qvp_rtp_codec_info_ctx *)pUserData;
  uint8				*play_pkt_ptr = NULL;
  uint16			play_pkt_len = 0;
  uint32			tstamp = 0;
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  uint32	FrameCount = iParam2;
  QP_MULTIMEDIA_FRAME *FrameData = NULL;
  QP_MULTIMEDIA_FRAME** BundleFrameData = NULL;
  boolean process_pending = FALSE;
  
  if(codec_info_cb_ptr == NULL)
  {
    QVP_RTP_ERR_0("qvp_rtp_codec_audio_cb: codec_info_cb_ptr is NULL");
    return;
  }

  QVP_RTP_MSG_MED("qvp_rtp_codec_audio_cb: Received Audio Message %d in state = %d Handoff in state - %d", tAudioMsg, codec_info_cb_ptr->audio_codec_info.audio_state, g_handoffstate);
  switch (tAudioMsg)
  {
  case AUDIO_MSG_DEV_INITIALISED:
    {
	  codec_info_cb_ptr->audio_codec_info.biscmrreq = 0;
      if(codec_info_cb_ptr->audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_INITIALIZING)
      {
        qvp_rtp_media_config     media_config;

        codec_info_cb_ptr->audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_INITDONE;
        //QVP_RTP_MSG_LOW_0("qvp_rtp_codec_audio_cb() : Audio Dev Initialize success.");
 
		if(g_handoffstate == HO_IN_PROGRESS)
		{
			process_pending = TRUE;
		}
		else
		{
        //notify qip call about init sucess
        if(codec_info_cb_ptr->audio_codec_info.handle_media_res)
	        {
		     qvp_rtp_response_handler(codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id, NULL,
		     QVP_RTP_MEDIA_CODEC_INIT_RSP, QVP_RTP_SUCCESS, TRUE, QVP_RTP_DIR_TXRX);   
	        }
		}
		
        //call rtp codec config api;
        qvp_rtp_memscpy(&media_config.audio_codec_config,sizeof(qvp_rtp_audio_codec_config),
                        &codec_info_cb_ptr->audio_codec_info.audio_codec_config,
                        sizeof(qvp_rtp_audio_codec_config));
        QVP_RTP_MSG_HIGH_MEM("API qvp_rtp_codec_audio_cb qvp_rtp_memscpy size \
                             = %d and clock rate %d for media_type",
                             sizeof(qvp_rtp_audio_codec_config),
                             media_config.audio_codec_config.iClockRate, 0);

		if(QVP_RTP_SUCCESS != qvp_rtp_media_codec_configure_pvt(codec_info_cb_ptr->audio_codec_info.app_id,
          codec_info_cb_ptr->audio_codec_info.stream_id, &media_config, QVP_RTP_DIR_TXRX))
        {
          QVP_RTP_ERR_0("codec_audio_cb() : qvp_rtp_media_config_codec failure, releasing codec.");
			  // if handoff case dont notify to qipcall 
			  	if(g_handoffstate == HO_IN_PROGRESS)
			{
				QVP_RTP_MSG_LOW_0("qvp_rtp_codec_audio_cb() :Handoff in progress");
					process_pending = TRUE;
				qvp_rtp_response_handler(codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id, NULL,
			     QVP_RTP_MEDIA_AUDIO_ERROR_RSP, status, TRUE, QVP_RTP_DIR_TXRX);				
			}		
				if(codec_info_cb_ptr->audio_codec_info.handle_media_res)
			{			  
		       codec_info_cb_ptr->audio_codec_info.stream_id = (qvp_rtp_stream_id_type)qvp_rtp_find_internal_streamid(codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id, FALSE);
			       qvp_rtp_response_handler(codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id, NULL,
				     QVP_RTP_MEDIA_CODEC_CONFIG_RSP, status, TRUE, QVP_RTP_DIR_TXRX); 
        }
        } 	     
      }
      else
      {
        QVP_RTP_ERR_1("codec_audio_cb: Rcvd AUDIO_MSG_DEV_INITIALISED in wrong state %d",codec_info_cb_ptr->audio_codec_info.audio_state);
	  // if handoff case dont notify to qipcall 
	 	if(g_handoffstate == HO_IN_PROGRESS)
	 	{
			process_pending = TRUE;
	  	}	
		else
		{
        if(codec_info_cb_ptr->audio_codec_info.handle_media_res)
	        {
		       qvp_rtp_response_handler(codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id, NULL,
			     QVP_RTP_MEDIA_CODEC_INIT_RSP, status, TRUE, QVP_RTP_DIR_TXRX); 
	        }
		}
      }
      if((clear_all_session_req || srvcc_tear_down_req) && g_is_release_pending)
        qvp_rtp_media_codec_release_pvt(codec_info_cb_ptr->audio_codec_info.app_id);
    }
    break;

  case AUDIO_MSG_DEV_UNINITIALISED:
    {
      // audio un-initialized now acquire can be called.
      codec_info_cb_ptr->audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_NONE;
      codec_info_cb_ptr->audio_codec_info.audioDesc = QP_NULL;
      QVP_RTP_MSG_LOW_0("codec_audio_cb() : Audio Dev Un-initialize success.");
	  // if handoff case dont notify to qipcall 
	  if(g_handoffstate == HO_IN_PROGRESS)
	  {
	//	process_pending = TRUE;
	  }
	  else
	  {
      //notify APP about un-init sucess?
      //notify to APP about config codec sucess
      g_is_release_pending = FALSE;
	  if(srvcc_tear_down_req && g_enable_srvcc_optm)
	  {
	    if(qvp_rtp_send_audio_release_srvcc_ind())
		{
		  //rex_sleep(300);
	  }
	  }
      if((codec_info_cb_ptr->audio_codec_info.handle_media_res && !clear_all_session_req) || (codec_info_cb_ptr->audio_codec_info.handle_media_res && srvcc_tear_down_req))
      {
  	       qvp_rtp_response_handler(codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id, NULL,
		     QVP_RTP_MEDIA_AUDIO_CODEC_RELEASE_RSP, QVP_RTP_SUCCESS, TRUE, QVP_RTP_DIR_TXRX); 
      }
	  if(srvcc_tear_down_req)
	  {
	    //if(qvp_rtp_clear_all_media_resource(codec_info_cb_ptr->audio_codec_info.app_id, QVP_RTP_AUDIO, QVP_RTP_RAT_LTE) != QVP_RTP_SUCCESS)
            //QVP_RTP_ERR_0("codec_audio_cb() : qvp_rtp_clear_all_media_resource  QVP_RTP_AUDIO failed.");
	    //if(qvp_rtp_clear_all_media_resource(codec_info_cb_ptr->audio_codec_info.app_id, QVP_RTP_VIDEO, QVP_RTP_RAT_LTE) != QVP_RTP_SUCCESS)
            //QVP_RTP_ERR_0("codec_audio_cb() : qvp_rtp_clear_all_media_resource QVP_RTP_VIDEO failed.");
			srvcc_tear_down_req = FALSE;
	  }

	  	 	 if(g_handoffstate == HO_TRIGGERED)
	  	{
			qvp_rtp_handoff_queue_commands(codec_info_cb_ptr->audio_codec_info.app_id,TRUE);	
	  	}	

		codec_info_cb_ptr->audio_codec_info.app_id = (qvp_rtp_app_id_type)0xdeadbeef;
		codec_info_cb_ptr->audio_codec_info.stream_id = (qvp_rtp_stream_id_type)0xdeadbeef;
		
     } 
    }
    break;

  case AUDIO_MSG_CODEC_CHANGED:
    {
	  if(codec_info_cb_ptr->audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_CONFIGURING)
	  {
	    codec_info_cb_ptr->audio_codec_info.biscmrreq = 0;
	  }	
      if(codec_info_cb_ptr->audio_codec_info.biscmrreq && 
         codec_info_cb_ptr->audio_codec_info.audio_state != QVP_RTP_CODEC_STATE_RECONFIGURE)
      {
         codec_info_cb_ptr->audio_codec_info.biscmrreq--;
         QVP_RTP_MSG_LOW_1("codec_audio_cb() : CMR change success. Remaining CMR requests = %d",
                            codec_info_cb_ptr->audio_codec_info.biscmrreq);
         return;
      }
      if(codec_info_cb_ptr->audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_CONFIGURING || 
         codec_info_cb_ptr->audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_RECONFIGURE)
      {
        QVP_RTP_MSG_LOW_0("codec_audio_cb() : Audio codec change success.");
        if(codec_info_cb_ptr->audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_RECONFIGURE)
        {
          codec_info_cb_ptr->audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_STARTED;
        }
        else
        {
          codec_info_cb_ptr->audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_CONFIGUREDONE;
        }
        status = QVP_RTP_SUCCESS;
        //codec set is completed, APP can start audio when it is not reconfigure?
      }
      else
      {
        QVP_RTP_ERR_1("codec_audio_cb: AUDIO_MSG_CODEC_CHANGED in wrong state %d",
                       codec_info_cb_ptr->audio_codec_info.audio_state);
      }

	  if(g_handoffstate == HO_IN_PROGRESS)
	  {
		process_pending = TRUE;	
	  }	  
	  else
	  {
	 	 if(g_handoffstate == HO_TRIGGERED)
	  	{
			qvp_rtp_handoff_queue_commands(codec_info_cb_ptr->audio_codec_info.app_id,TRUE);	
	  	}	  
      //notify to APP about config codec sucess
      if(codec_info_cb_ptr->audio_codec_info.handle_media_res)
      	{

   	      qvp_rtp_response_handler(codec_info_cb_ptr->audio_codec_info.app_id, 
   	                               codec_info_cb_ptr->audio_codec_info.stream_id,
   	                               NULL, QVP_RTP_MEDIA_CODEC_CONFIG_RSP, status,
   	                               TRUE, QVP_RTP_DIR_TXRX); 
      	}
      if((clear_all_session_req ||  srvcc_tear_down_req)\
          && g_is_release_pending) 
	  {
	    if(codec_info_cb_ptr->audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STARTED)
			      qvp_rtp_media_stream_stop_pvt(codec_info_cb_ptr->audio_codec_info.app_id,
			              codec_info_cb_ptr->audio_codec_info.stream_id,QVP_RTP_DIR_TXRX );
		else
        qvp_rtp_media_codec_release_pvt(codec_info_cb_ptr->audio_codec_info.app_id);
      }
    }
    }
    break;

  case AUDIO_MSG_STREAMING_STARTED:
    {
      if(codec_info_cb_ptr->audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STARTING)
      {
        codec_info_cb_ptr->audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_STARTED;
        status = QVP_RTP_SUCCESS;
        //notify to APP about stream start  sucess
      }
      else
      {
        QVP_RTP_ERR_1("codec_audio_cb: AUDIO_MSG_STREAMING_STARTED in wrong state %d",codec_info_cb_ptr->audio_codec_info.audio_state);
      }
	  // if handoff case dont notify to qipcall 
	  if(g_handoffstate == HO_IN_PROGRESS)
	  {
		process_pending = TRUE;	
	  }	  
	  else
	  {
      //notify to APP about config codec sucess
      if(codec_info_cb_ptr->audio_codec_info.handle_media_res)
      {
           qvp_rtp_response_handler(codec_info_cb_ptr->audio_codec_info.app_id, 
                                    codec_info_cb_ptr->audio_codec_info.stream_id,
                                    NULL, QVP_RTP_MEDIA_START_STREAM_RSP, status,
                                    TRUE, QVP_RTP_DIR_TXRX); 	     
      }
      if((clear_all_session_req || srvcc_tear_down_req) \
          && g_is_release_pending)
      		qvp_rtp_media_stream_stop_pvt(codec_info_cb_ptr->audio_codec_info.app_id,
                            codec_info_cb_ptr->audio_codec_info.stream_id,QVP_RTP_DIR_TXRX );

	 	 if(g_handoffstate == HO_TRIGGERED)
	  	{
          QVP_RTP_MSG_LOW_0("codec_audio_cb() :Handoff in triggered state");
			qvp_rtp_handoff_queue_commands(codec_info_cb_ptr->audio_codec_info.app_id,TRUE);	
	  	}	  
	  }
    }
    break;

  case AUDIO_MSG_STREAMING_STOPPED:
    {
      if(codec_info_cb_ptr->audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STOPPING )
      {
        codec_info_cb_ptr->audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_STOPPED;
        QVP_RTP_MSG_LOW_0("codec_audio_cb() : Audio streaming stop success...");
        status = QVP_RTP_SUCCESS;
      }
      else
      {
        QVP_RTP_ERR_1("codec_audio_cb: AUDIO_MSG_STREAMING_STOPPED in wrong state %d",codec_info_cb_ptr->audio_codec_info.audio_state);
      }
	  // if handoff case dont notify to qipcall 
	  if(g_handoffstate == HO_IN_PROGRESS)
	  {
		process_pending = TRUE;	
	  }
	  else
	  {
      //notify to APP about config codec sucess
      if(codec_info_cb_ptr->audio_codec_info.handle_media_res && !clear_all_session_req)
      {
          qvp_rtp_response_handler(codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id, NULL,
		     QVP_RTP_MEDIA_STOP_STREAM_RSP, status, TRUE, QVP_RTP_DIR_TXRX); 
	  }
	   if(g_handoffstate == HO_TRIGGERED)
	  {
		  qvp_rtp_handoff_queue_commands(codec_info_cb_ptr->audio_codec_info.app_id,TRUE);	  
	  }   
	  if(clear_all_session_req || srvcc_tear_down_req)
	  	qvp_rtp_media_codec_release_pvt(codec_info_cb_ptr->audio_codec_info.app_id);
      }
    }
    break;

  case AUDIO_MSG_RECORDED_DATA:
    {
	  uint64 avtstamp = 0;
      if(codec_info_cb_ptr->audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STARTED)
      {
        QVP_RTP_MSG_LOW_0("qvp_rtp_codec_audio_cb() : Audio data received.");
        if(FrameCount == 1)
        {
          BundleFrameData = (QP_MULTIMEDIA_FRAME**)pParam1;
          if(BundleFrameData != NULL)
          {
            FrameData = (QP_MULTIMEDIA_FRAME*)(BundleFrameData[FrameCount - 1]);
            if(FrameData != NULL)
            {
				/* Copy vocoder pkt ptr to the play pkt ptr */
				play_pkt_ptr = (uint8*)FrameData->pData;
				/* Copy pkt len*/
				play_pkt_len = (uint16)FrameData->iDataLen;
				/* copy pkt time stamp */
				tstamp = FrameData->sMediaPacketInfo.sMediaPktInfoTx.iTimeStamp;
				avtstamp = FrameData->sMediaPacketInfo.sMediaPktInfoTx.iAVTimeStamp64;
				QVP_RTP_MSG_MED("iAVTimeStamp64 %u %u uplink pkt len  %d",*((uint32*)&avtstamp),*((uint32*)&avtstamp + 1), play_pkt_len);

				if(play_pkt_ptr == QP_NULL)
				{
					QVP_RTP_ERR_2("qvp_rtp_codec_audio_cb() : Empty media pkt, app_id: %d stream_id: %d",
					codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id);
					return;
				}
							if(((codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_EVS) || 
							   (codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_AMR) || 
							   (codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_G722_2)) &&
				((play_pkt_len == 0) ))
				{
					QVP_RTP_MSG_LOW_2("qvp_rtp_codec_audio_cb() : Empty media pkt, app_id: %d stream_id: %d",
					codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id);
					return;
				}
				if(((codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_G711_U) || (codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_G711_A)) &&
				((FrameData->sFrameInfo.sG711Info.iFrameType == AUDIO_G711_NTX) ))
				{
					QVP_RTP_MSG_LOW_2("qvp_rtp_codec_audio_cb() : G711 No Transmission pkt, app_id: %d stream_id: %d",
					codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id);
					return;
				}
				
				//do we need to handle dtmf events?
      				qvp_rtp_process_tx_pkt(codec_info_cb_ptr->audio_codec_info.app_id, 
      				                    codec_info_cb_ptr->audio_codec_info.stream_id, 
      				                    play_pkt_ptr, play_pkt_len, tstamp, avtstamp );
		  }
		  else
		  {
			QVP_RTP_MSG_LOW_2("qvp_rtp_codec_audio_cb() : Framedata is NULL, app_id: %d stream_id: %d",
				codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id);
			return;
		  }		
		 }				  
        }
        else
        {
          QVP_RTP_ERR_1("qvp_rtp_codec_audio_cb() : number of frames %d per callback, we have assumed only one frame per callback",
            FrameCount);
        }
      }
      else
      {
        QVP_RTP_ERR_1("qvp_rtp_codec_audio_cb: AUDIO_MSG_RECORDED_DATA in wrong state %d",codec_info_cb_ptr->audio_codec_info.audio_state);
      }
    }
    break;

  case AUDIO_MSG_DATA_PLAYED:
    {
      //QVP_RTP_MSG_LOW_0("qvp_rtp_codec_audio_cb() : audio data played in rx path.");
    }
    break;

  case AUDIO_MSG_ERROR:
    {
     // QVP_RTP_ERR_0("qvp_rtp_codec_audio_cb() : received audio error.");
      //notify to APP about config codec sucess
      if(codec_info_cb_ptr->audio_codec_info.handle_media_res)
      {
        /*codec_info_cb_ptr->audio_codec_info.handle_media_res(codec_info_cb_ptr->audio_codec_info.app_id,
	        codec_info_cb_ptr->audio_codec_info.stream_id, QVP_RTP_MEDIA_ERROR,status, QVP_RTP_DIR_TXRX);*/
        qvp_rtp_response_handler(codec_info_cb_ptr->audio_codec_info.app_id, codec_info_cb_ptr->audio_codec_info.stream_id, NULL,
		     QVP_RTP_MEDIA_ERROR_RSP, status, TRUE, QVP_RTP_DIR_TXRX); 
      }
    }
    break;
  case AUDIO_MSG_TTY_TX_DATA:
   {
     //QVP_RTP_MSG_HIGH_0("audiocallback received for TTYTXData with");
	 qvp_rtp_tty_process_tx_data(AUDIO_MSG_TTY_TX_DATA,pParam1,0,&iParam2);
   }
   break;
  case AUDIO_MSG_TTY_ENABLED:
   {
     //QVP_RTP_MSG_HIGH_0("audiocallback received for TTYENABLED with");
	if (*((boolean*)(pParam1)) == TRUE)
	{
	  qvp_rtp_cvd_state = READY;
    QVP_RTP_MSG_HIGH_0("state set to ready calling process cvdq");
	  qvp_rtp_text_process_cvdq();
	}
	else
	{
	  qvp_rtp_cvd_state = NOT_READY;
	}
   }
   break;
  default:
    {
      QVP_RTP_MSG_LOW_0("qvp_rtp_codec_audio_cb() : default block is hit.");
    }
    break;
  }
  if(g_handoffstate == HO_IN_PROGRESS && process_pending)
  {
  	/**Assuming APP ID to be 0 */
	qvp_rtp_mark_pending_task_done(codec_info_cb_ptr->audio_codec_info.app_id);

	qvp_rtp_post_execute_pending_command(codec_info_cb_ptr->audio_codec_info.app_id);
  }  
}

/*===========================================================================
FUNCTION qvp_rtp_media_init_pvt

DESCRIPTION
This function initializes media layer

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_init_pvt
( 
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                       * the stream
                                       */
  qvp_rtp_media_type     media_type     
)
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;

  if (media_type == QVP_RTP_VIDEO)
  {
    if(codec_info_ctx.video_codec_info.media_inited == TRUE)
    {
     // QVP_RTP_ERR_0("qvp_rtp_media_init_pvt: Media is already inited for Video");
      codec_info_ctx.video_codec_info.media_ref_cnt++;
      status = QVP_RTP_SUCCESS;
    }
    else
    {
      //status = qvp_rtp_media_init_video_pvt(app_id);
      if (status == QVP_RTP_SUCCESS)
      {
        codec_info_ctx.video_codec_info.media_ref_cnt++;
        codec_info_ctx.video_codec_info.media_inited = TRUE;
      }
    }
  }
  else if (media_type == QVP_RTP_AUDIO)
  {
    if(codec_info_ctx.audio_codec_info.media_inited == TRUE)
    {
      //QVP_RTP_ERR_0("qvp_rtp_media_init_pvt: Media is already inited for Video");
      codec_info_ctx.video_codec_info.media_ref_cnt++;
      status = QVP_RTP_SUCCESS;
    }
    else
    {
      // Right now for Audio there is no need to invoke DPL Audio API
      codec_info_ctx.audio_codec_info.media_ref_cnt++;
      codec_info_ctx.audio_codec_info.media_inited = TRUE;
      status = QVP_RTP_SUCCESS;
    }
  }
  else if(media_type == QVP_RTP_TEXT)
  {
    //return success since there is nothing to be done for text stream at present
    status = QVP_RTP_SUCCESS;
  }
  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_deinit_pvt

DESCRIPTION
This function un-initializes media layer

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/

qvp_rtp_status_type qvp_rtp_media_deinit_pvt
( 
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                       * the stream
                                       */
  qvp_rtp_media_type     media_type
)
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;

  if (media_type == QVP_RTP_VIDEO)
  {
    if(codec_info_ctx.video_codec_info.media_inited == TRUE)
    {
      if (codec_info_ctx.video_codec_info.media_ref_cnt >= 1)
      {
        codec_info_ctx.video_codec_info.media_ref_cnt--;
        status = QVP_RTP_SUCCESS;
        if (codec_info_ctx.video_codec_info.media_ref_cnt == 0)
        {
          codec_info_ctx.video_codec_info.media_inited = FALSE;
          //status = qvp_rtp_media_deinit_video_pvt(app_id);
        }
      }
      else
      {
        QVP_RTP_ERR_1("qvp_rtp_media_init_pvt: Ref Cnt is not right: %x",codec_info_ctx.video_codec_info.media_ref_cnt);
      }
    }
    else
    {
      QVP_RTP_ERR_0("qvp_rtp_media_init_pvt: Media is not inited for Video");
    }
  }
  else if (media_type == QVP_RTP_AUDIO)
  {
    if(codec_info_ctx.audio_codec_info.media_inited == TRUE)
    {
      if (codec_info_ctx.audio_codec_info.media_ref_cnt >= 1)
      {
        codec_info_ctx.audio_codec_info.media_ref_cnt--;
        status = QVP_RTP_SUCCESS;
        if (codec_info_ctx.audio_codec_info.media_ref_cnt == 0)
        {
          codec_info_ctx.audio_codec_info.media_inited = FALSE;
        }
      }
      else
      {
        QVP_RTP_ERR_1("qvp_rtp_media_init_pvt: Ref Cnt is not right: %x",codec_info_ctx.audio_codec_info.media_ref_cnt);
      }
    }
    else
    {
      QVP_RTP_ERR_0("qvp_rtp_media_init_pvt: Media is not inited for Audio");
    }
  }
  else if(media_type == QVP_RTP_TEXT)
  {
     QVP_RTP_ERR_0("qvp_rtp_media_init_pvt: Media is deinit for Text");
     status = QVP_RTP_SUCCESS;
  }
  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_codec_initialize

DESCRIPTION
This function initializes media codec engine

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_codec_initialize_pvt(
                                      qvp_rtp_app_id_type    app_id,      /* application which opened
                                                                           * the stream
                                                                           */
                                      qvp_rtp_stream_id_type  stream_id,   /* stream which was opened */
                                      qvp_rtp_media_config*   media_config,
                                      qvp_rtp_rat_type		rat_type,
                                      qvp_rtp_media_handle_response  handle_media_res
                                      )
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  g_rat_type = rat_type;

  QVP_RTP_MSG_HIGH_MEM("API qvp_rtp_media_codec_initialize_pvt entered\
                  with appid %d and stream id %d clock_rate %d for media_type", app_id, stream_id,codec_info_ctx.audio_codec_info.audio_codec_config.iClockRate);
  g_is_release_pending = FALSE;
  //this whole code should be put under audio going forward
  if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_NONE)
  {
	QPE_AUDIO_RAT_PVT	audio_rat_type = AUDIO_RAT_LTE;				
	if((g_rat_type == QVP_RTP_RAT_WLAN) || (g_rat_type == QVP_RTP_RAT_IWLAN))
	{
		audio_rat_type = AUDIO_RAT_IWLAN;
	}
	
    codec_info_ctx.audio_codec_info.app_id = app_id;
    codec_info_ctx.audio_codec_info.stream_id = stream_id;
    //qvp_rtp_memscpy(&codec_info_ctx.media_config,media_config,sizeof(codec_info_ctx.media_config));
    qvp_rtp_memscpy(&codec_info_ctx.audio_codec_info.audio_codec_config,sizeof(qvp_rtp_audio_codec_config), &media_config->audio_codec_config, sizeof(qvp_rtp_audio_codec_config));
    //QVP_RTP_MSG_HIGH_2_MEM("API qvp_rtp_media_codec_initialize_pvt\
      //              memcspy size = %d  and clock rate %d for media_type", sizeof(qvp_rtp_audio_codec_config), codec_info_ctx.audio_codec_info.audio_codec_config.iClockRate);
    codec_info_ctx.audio_codec_info.handle_media_res = handle_media_res;
    codec_info_ctx.audio_codec_info.audioDesc = qpAudioInitialize(qvp_rtp_codec_audio_cb, &codec_info_ctx, AUDIO_PLAYER_RECORDER, audio_rat_type);

    if(codec_info_ctx.audio_codec_info.audioDesc != NULL)
    {
      //change state to initializing.
      codec_info_ctx.audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_INITIALIZING;
      /*QVP_RTP_MSG_LOW_2("qvp_rtp_media_codec_initialize_pvt : Method call to initialize AUDIO_PLAYER_RECORDER on %d app_id for %d stream_id success !!!",
        app_id,
        stream_id);*/
      status = QVP_RTP_SUCCESS;
    }
    else
    {
      //to inform APP not to wait for codec acquire as initialize is failed and dpl audio state is idle.
      codec_info_ctx.audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_NONE;
      QVP_RTP_ERR_2("codec_initialize_pvt : Method call to initialize AUDIO_PLAYER_RECORDER on %d app_id for %d stream_id failed !!!",
        app_id,
        stream_id);
      status = QVP_RTP_ERR_FATAL;
    }
  }
  else
  {
    QVP_RTP_ERR_1("qvp_rtp_media_codec_initialize_pvt: Method call to initialize AUDIO_PLAYER_RECORDER called on wrong %d state",codec_info_ctx.audio_codec_info.audio_state);
  }
  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_codec_configure_pvt

DESCRIPTION
This function configures media codec engine

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_codec_configure_pvt(
                                      qvp_rtp_app_id_type    app_id,      /* application which opened
                                                                           * the stream
                                                                           */
                                      qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
                                      qvp_rtp_media_config*   media_config,
                                      qvp_rtp_stream_direction  direction   /* Direction in which configuration
                                                                             * needs to be applied
                                                                             */
                                      )
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  QP_AUDIO_DESC       audioDesc;
  QP_CODEC_CONFIG     tCodecConfig;
  QPE_AUDIO_ERROR     eError;

  QVP_RTP_MSG_HIGH_MEM("API qvp_rtp_media_codec_configure_pvt entered\
                  with appid %d and stream id %d qvp_rtp_media_codec_configure audio state = %d for media_type", app_id, stream_id,codec_info_ctx.audio_codec_info.audio_state);

  //this whole code should be put under audio going forward
  //QVP_RTP_MSG_HIGH_1_MEM("qvp_rtp_media_codec_configure_pvt : qvp_rtp_media_codec_configure audio state = %d", codec_info_ctx.audio_codec_info.audio_state);

  audioDesc = codec_info_ctx.audio_codec_info.audioDesc;
  //check for the initialized code descriptor.
  if(audioDesc == NULL)
  {
    QVP_RTP_ERR_0("qvp_rtp_media_codec_configure_pvt : Trying to config codec with null codecDesc !!!");

    return QVP_RTP_ERR_FATAL;
  }

  if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_INITDONE || codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STARTED
    || codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_CONFIGUREDONE || codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STOPPED)
  {
    qvp_rtp_memscpy(&codec_info_ctx.audio_codec_info.audio_codec_config, sizeof(qvp_rtp_audio_codec_config), &media_config->audio_codec_config, sizeof(qvp_rtp_audio_codec_config));
    memset(&tCodecConfig, 0, sizeof(QP_CODEC_CONFIG));
    tCodecConfig.iClockRate = media_config->audio_codec_config.iClockRate;
    tCodecConfig.iFrameDuration = media_config->audio_codec_config.iFrameDuration;
    tCodecConfig.iPTime = media_config->audio_codec_config.iPTime;
    tCodecConfig.bLipSyncEnable = media_config->audio_codec_config.bLipSyncEnable;
    tCodecConfig.bSilenceSupp = media_config->audio_codec_config.bSilenceSupp;
    tCodecConfig.bDTXEnable = media_config->audio_codec_config.bDTXEnable;
    tCodecConfig.iDtxMax = media_config->audio_codec_config.iDtxMax;
    tCodecConfig.iDtxMin = media_config->audio_codec_config.iDtxMin;
    tCodecConfig.iHangover = media_config->audio_codec_config.iHangover;
    tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode = (QPE_AUDIO_AMR_CODEC_MODE)media_config->audio_codec_config.sCodecInfo.sAMRInfo.eCodecMode;
    {
      switch(media_config->audio_codec_config.eCodec)
      {
      case QVP_RTP_PYLD_EVRC0:
        {
          tCodecConfig.eCodec = AUDIO_CODEC_EVRC0;
        }
        break;

      case QVP_RTP_PYLD_EVRCB0:
        {
          tCodecConfig.eCodec = AUDIO_CODEC_EVRCB0;
        }
        break;
      case QVP_RTP_PYLD_EVRCWB0:
        {
          tCodecConfig.eCodec = AUDIO_CODEC_EVRCWB0;
        }
        break;

      case QVP_RTP_PYLD_AMR:
        {
          tCodecConfig.eCodec = AUDIO_CODEC_AMR;
          QVP_RTP_MSG_HIGH_2_MEM("Audio codec config params: Codec selected = %d, CodecMode selected =%d",media_config->audio_codec_config.eCodec,media_config->audio_codec_config.sCodecInfo.sAMRInfo.eCodecMode);
        }
        break;
			case QVP_RTP_PYLD_EVS:
				{
					tCodecConfig.eCodec = AUDIO_CODEC_EVS;
					tCodecConfig.sCodecInfo.sEVSInfo.ch_aw_enabled = media_config->audio_codec_config.sCodecInfo.sEVSInfo.ch_aw_enabled;
					tCodecConfig.sCodecInfo.sEVSInfo.tx_evs_red_offset = media_config->audio_codec_config.sCodecInfo.sEVSInfo.tx_evs_red_offset;
					tCodecConfig.sCodecInfo.sEVSInfo.eTxBW = (QPE_EVS_BW)media_config->audio_codec_config.sCodecInfo.sEVSInfo.tx_evs_bw;
               tCodecConfig.sCodecInfo.sEVSInfo.eRxBW = (QPE_EVS_BW)media_config->audio_codec_config.sCodecInfo.sEVSInfo.rx_evs_bw;
					tCodecConfig.sCodecInfo.sEVSInfo.is_evs_primary = media_config->audio_codec_config.sCodecInfo.sEVSInfo.is_evs_primary;
               tCodecConfig.sCodecInfo.sEVSInfo.tx_fer = 1;
					if(tCodecConfig.sCodecInfo.sEVSInfo.is_evs_primary)
						tCodecConfig.sCodecInfo.sEVSInfo.eCodecMode.ePrmyMode = (QPE_EVS_PRIMARY_MODE)media_config->audio_codec_config.sCodecInfo.sEVSInfo.eCodecMode.evs_primary_mode;
               QVP_RTP_MSG_HIGH_MEM("EVS codec params: evsprim = %d, evsprim mode =%d ch aw %d",
                                                            tCodecConfig.sCodecInfo.sEVSInfo.is_evs_primary,
                                                            tCodecConfig.sCodecInfo.sEVSInfo.eCodecMode.ePrmyMode,
                                                            tCodecConfig.sCodecInfo.sEVSInfo.ch_aw_enabled);
               QVP_RTP_MSG_HIGH_MEM("EVS codec params: eTxBW = %d, eRxBW mode =%d red offset %d",
                                                            tCodecConfig.sCodecInfo.sEVSInfo.eTxBW,
                                                            tCodecConfig.sCodecInfo.sEVSInfo.eRxBW,
                                                            tCodecConfig.sCodecInfo.sEVSInfo.tx_evs_red_offset);
				if(!tCodecConfig.sCodecInfo.sEVSInfo.tx_evs_red_offset)
					tCodecConfig.sCodecInfo.sEVSInfo.ch_aw_enabled = FALSE;															
					//else return error in PHASE 1 EVSTODO

        }
        break;
      case QVP_RTP_PYLD_G722_2:
        {
          tCodecConfig.eCodec = AUDIO_CODEC_AMR_WB;
          tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode = (QPE_AUDIO_AMR_CODEC_MODE) (media_config->audio_codec_config.sCodecInfo.sAMRInfo.eCodecMode - 8);
              QVP_RTP_MSG_HIGH_2_MEM("Audio codec config params: Codec selected = %d, CodecMode selected =%d",media_config->audio_codec_config.eCodec,media_config->audio_codec_config.sCodecInfo.sAMRInfo.eCodecMode);
        }
        break;
      case QVP_RTP_PYLD_G711_U:
        {
          tCodecConfig.eCodec = AUDIO_CODEC_G711U;
        }
        break;		
      case QVP_RTP_PYLD_G711_A:
        {
          tCodecConfig.eCodec = AUDIO_CODEC_G711A;
        }
        break;				
      default:
        {
          // Should be set to Default
          tCodecConfig.eCodec = AUDIO_CODEC_AMR;
          tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode = AUDIO_CODEC_MODE_7;
        }
        break;
      }
    }
/* The below condition is never TRUE since just above we have set the default codec as AMR so -1 is never possible*/
/*
    if(tCodecConfig.eCodec == -1)
    {
      QVP_RTP_ERR_2("qvp_rtp_media_codec_configure_pvt: Trying to codec config Unsupported %d codec with codecDesc %d!!!",
        media_config->audio_codec_config.eCodec,
        audioDesc);

      return QVP_RTP_ERR_FATAL;
    }
*/
    QVP_RTP_MSG_HIGH_MEM("config params: iClockRate = %d, iFrameDuration =%d, iPTime = %d",tCodecConfig.iClockRate,tCodecConfig.iFrameDuration,tCodecConfig.iPTime);
    QVP_RTP_MSG_HIGH_MEM("config params: bLipSyncEnable = %d, bSilenceSupp =%d, bDTXEnable = %d",tCodecConfig.bLipSyncEnable,tCodecConfig.bSilenceSupp,tCodecConfig.bDTXEnable);
    QVP_RTP_MSG_HIGH_MEM("config params: iDtxMax = %d, iDtxMin =%d, iHangover = %d",tCodecConfig.iDtxMax,tCodecConfig.iDtxMin,tCodecConfig.iHangover);

    //do codec set.
    eError = qpAudioCodecSet(audioDesc, tCodecConfig);
    if(eError != AUDIO_ERROR_OK)
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_configure_pvt : Method call to audio codec set failed ");
      //should we reelase the codec here?
      return QVP_RTP_ERR_FATAL;
    }
    status = QVP_RTP_SUCCESS;
    codec_info_ctx.audio_codec_info.app_id = app_id;
    codec_info_ctx.audio_codec_info.stream_id = stream_id;
    codec_info_ctx.audio_codec_info.current_codec = media_config->audio_codec_config.eCodec;
    if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STARTED)
    {
      QVP_RTP_MSG_HIGH_1_MEM("codec_configure_pvt : Call to acquire codec on in state %d sucess (reconfigure)!!!",
        codec_info_ctx.audio_codec_info.audio_state);
      codec_info_ctx.audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_RECONFIGURE;
    }
    else
    {
      QVP_RTP_MSG_HIGH_0_MEM("qvp_rtp_media_codec_configure_pvt : Method call to audio codec set success ");
      codec_info_ctx.audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_CONFIGURING;
    }

  }// Audio is initialized.
  else
  {
    if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_INITIALIZING)
    {
      QVP_RTP_ERR_1("codec_configure_pvt : Cant not do audio codec set, in state %d, as initializing codec is not complete.",
        codec_info_ctx.audio_codec_info.audio_state);
      //TODO: Is it necessary to wait for initialize complete and do audio codec set?
    }
    else
    {
      QVP_RTP_ERR_1("codec_configure_pvt : Cant not do audio codec set, in state %d", codec_info_ctx.audio_codec_info.audio_state);
    }
  }//Audio is not initialized.
  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_codec_release_pvt

DESCRIPTION
This function releases media codec engine

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_codec_release_pvt(
  qvp_rtp_app_id_type    app_id      /* application which opened
                                     * the stream
                                     */
                                     )
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  /*reset the g_Dec_requestTime */
  g_Dec_RequestTime = 0;

  /*Also set g_isNetworkCommandPending to false*/
  g_isNetworkCommandPending = FALSE;

  if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_INITDONE || codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_CONFIGUREDONE || codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STARTED || codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STOPPED)
  {
    //call dpl audio uninitialize.
    qpAudioUninitialize(codec_info_ctx.audio_codec_info.audioDesc, AUDIO_PLAYER_RECORDER);
    codec_info_ctx.audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_RELEASING;
    codec_info_ctx.audio_codec_info.audioDesc = NULL;
    //QVP_RTP_MSG_LOW_0("qvp_rtp_media_codec_release_pvt : Call to audio un-initialize success.");
    status = QVP_RTP_SUCCESS;
  }
  else
  {
    QVP_RTP_ERR_1("qvp_rtp_media_codec_release_pvt : Cant not do codec release, in state %d", codec_info_ctx.audio_codec_info.audio_state);
	qvp_rtp_service_media_codec_release_req_msg( app_id,
          QVP_RTP_AUDIO, FALSE);
	#if 0
    if(codec_info_ctx.audio_codec_info.handle_media_res)
    {
       /*codec_info_ctx.audio_codec_info.handle_media_res(codec_info_ctx.audio_codec_info.app_id,
                                            codec_info_ctx.audio_codec_info.stream_id, QVP_RTP_MEDIA_AUDIO_CODEC_RELEASE,status, QVP_RTP_DIR_TXRX);*/
       qvp_rtp_response_handler(codec_info_ctx.audio_codec_info.app_id, codec_info_ctx.audio_codec_info.stream_id, NULL,
		     QVP_RTP_MEDIA_AUDIO_CODEC_RELEASE_RSP, status, TRUE, QVP_RTP_DIR_TXRX); 
    }
	#endif
    if((clear_all_session_req || srvcc_tear_down_req)\
        && codec_info_ctx.audio_codec_info.audio_state != \
       QVP_RTP_CODEC_STATE_NONE) 
    {
      g_is_release_pending = TRUE;
  }
  }
  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_stream_start_pvt

DESCRIPTION
This function starts stream associated with stream id

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_stream_start_pvt
(
 qvp_rtp_app_id_type    app_id,      /* application which opened
                                     * the stream
                                     */
                                     qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
                                     qvp_rtp_stream_direction   direction
                                     )
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  QP_AUDIO_DESC       audioDesc;

  QVP_RTP_MSG_HIGH_2_MEM("API qvp_rtp_media_stream_start_pvt entered\
                  with appid %d and stream id %d for media_type", app_id, stream_id);

  //this whole code should be put under audio going forward
  QVP_RTP_MSG_HIGH_1_MEM("qvp_rtp_media_stream_start_pvt : qvp_rtp_media_stream_start audio state = %d", codec_info_ctx.audio_codec_info.audio_state);

  audioDesc = codec_info_ctx.audio_codec_info.audioDesc;
  //check for the initialized code descriptor.
  if(audioDesc == NULL)
  {
    QVP_RTP_ERR_0("stream_start_pvt : Trying to play stream with null codecaudioDesc !!!");

    return status;
  }
  codec_info_ctx.audio_codec_info.biscmrreq = 0;
  if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_CONFIGUREDONE || codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STOPPED)
  {
    QPE_AUDIO_ERROR eError = qpAudioStart(audioDesc);
    if(eError != AUDIO_ERROR_OK)
    {
      QVP_RTP_ERR_0("stream_start_pvt() : Audio start call failed.");
      status = QVP_RTP_ERR_FATAL;
    }
    else
    {
      codec_info_ctx.audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_STARTING;
			codec_info_ctx.audio_codec_info.app_id = app_id;
			codec_info_ctx.audio_codec_info.stream_id = stream_id;
      //QVP_RTP_MSG_LOW_0("stream_start_pvt() : Audio start call success.");
      status = QVP_RTP_SUCCESS;
    }
  }
  else
  {
    QVP_RTP_ERR_1("media_stream_start_pvt: start stream called on wrong %d state",codec_info_ctx.audio_codec_info.audio_state);
  }
  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_stream_stop_pvt

DESCRIPTION
This function stops stream associated with stream id

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_stream_stop_pvt
(
 qvp_rtp_app_id_type    app_id,      /* application which opened
                                     * the stream
                                     */
                                     qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
                                     qvp_rtp_stream_direction   direction
                                     )
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  QP_AUDIO_DESC       audioDesc;

  QVP_RTP_MSG_HIGH_2_MEM("API qvp_rtp_media_stream_stop_pvt entered\
                  with appid %d and stream id %d for media_type", app_id, stream_id);

  //this whole code should be put under audio going forward
  QVP_RTP_MSG_HIGH_1_MEM("qvp_rtp_media_stream_stop_pvt : qvp_rtp_media_stream_start audio state = %d", codec_info_ctx.audio_codec_info.audio_state);
  /*reset the g_Dec_requestTime */
  g_Dec_RequestTime = 0;

  /*Also set g_isNetworkCommandPending to false*/
  g_isNetworkCommandPending = FALSE;

  if(srvcc_tear_down_req && codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_NONE)
  {
	   qvp_rtp_response_handler(app_id, NULL, NULL,
		 QVP_RTP_MEDIA_AUDIO_CODEC_RELEASE_RSP, QVP_RTP_SUCCESS, TRUE, QVP_RTP_DIR_TXRX); 
	  if(g_enable_srvcc_optm)
	  {
		if(qvp_rtp_send_audio_release_srvcc_ind())
		{
		  //rex_sleep(300);
		}
	  }		 
  }  
  
  audioDesc = codec_info_ctx.audio_codec_info.audioDesc;
  //check for the initialized code descriptor.
  if(audioDesc == NULL)
  {
     srvcc_tear_down_req = FALSE;
    QVP_RTP_ERR_0("media_stream_stop_pvt : Trying to stop stream with null codecaudioDesc !!!");

    return status;
  }

  if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STARTED)
  {
    QPE_AUDIO_ERROR eError = qpAudioStop(audioDesc);
    if(eError != AUDIO_ERROR_OK)
    {
      QVP_RTP_ERR_0("stream_stop_pvt() : Audio stop call failed.");
      status = QVP_RTP_ERR_FATAL;
    }
    else
    {
      codec_info_ctx.audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_STOPPING;
			codec_info_ctx.audio_codec_info.app_id = app_id;
			codec_info_ctx.audio_codec_info.stream_id = stream_id;
      //QVP_RTP_MSG_LOW_0("qvp_rtp_media_stream_stop_pvt() : Audio stop call success.");
      status = QVP_RTP_SUCCESS;
      codec_info_ctx.audio_codec_info.app_id = app_id;
      codec_info_ctx.audio_codec_info.stream_id = stream_id; 
    }
  }
  else
  {
    if(clear_all_session_req || srvcc_tear_down_req)
    {
      status = QVP_RTP_SUCCESS;
	  if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_INITDONE || codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_CONFIGUREDONE || codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STOPPED)	
	  	qvp_rtp_media_codec_release_pvt(codec_info_ctx.audio_codec_info.app_id);
      if(codec_info_ctx.audio_codec_info.audio_state != QVP_RTP_CODEC_STATE_INITDONE && codec_info_ctx.audio_codec_info.audio_state != QVP_RTP_CODEC_STATE_CONFIGUREDONE && codec_info_ctx.audio_codec_info.audio_state != QVP_RTP_CODEC_STATE_STARTED && codec_info_ctx.audio_codec_info.audio_state != QVP_RTP_CODEC_STATE_STOPPED)
        g_is_release_pending = TRUE;	  
	}
	
    QVP_RTP_ERR_1("stream_stop_pvt:stop stream called on wrong %d state",codec_info_ctx.audio_codec_info.audio_state);
  }
  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_handle_incoming_pkt_cb

DESCRIPTION
This function handlesincomming rtp packets and calls play start

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
void qvp_rtp_handle_incoming_pkt_cb
(
 qvp_rtp_app_id_type     app_id,      /* RTP Application ID*/
 qvp_rtp_stream_id_type   stream_id,    /* stream id through which the
                                        * packet came
                                        */
                                        qvp_rtp_recv_pkt_type    *pkt       /* packet which is being delivered */
                                        )
{
  QP_MULTIMEDIA_FRAME	 frame;
  QP_AUDIO_DESC       audioDesc;
  if(pkt == QP_NULL )
  {
    QVP_RTP_ERR_0("incoming_pkt_cb: Received NULL Packet from DS!!");
    return;
  }
  QVP_RTP_MSG_HIGH("incoming_pkt_cb: Rcvd RTP packet len = %d, seq = %u, t = %u",
    pkt->len, pkt->seq, pkt->tstamp );
  if(!pkt->len)
  {
	QVP_RTP_ERR_0("qvp_rtp_handle_incoming_pkt_cb: Dropping No data packet");
	return;
  }
  if(codec_info_ctx.audio_codec_info.audio_state != QVP_RTP_CODEC_STATE_STARTED)
  {
    QVP_RTP_MSG_HIGH_1_MEM("qvp_rtp_handle_incoming_pkt_cb: Dropping packets as we are in wrong state %d....!!",codec_info_ctx.audio_codec_info.audio_state);
    return;    
  }
  if (codec_info_ctx.audio_codec_info.stream_id != stream_id )
  {
    QVP_RTP_ERR_2("qvp_rtp_handle_incoming_pkt_cb: Incoming stream does not match active id (%d,%d)!", codec_info_ctx.audio_codec_info.stream_id,
      stream_id);
    return;
  }
  if (codec_info_ctx.audio_codec_info.current_codec != QVP_RTP_PYLD_EVRC0 &&
    codec_info_ctx.audio_codec_info.current_codec != QVP_RTP_PYLD_EVRCB0 &&
    codec_info_ctx.audio_codec_info.current_codec != QVP_RTP_PYLD_EVRCWB0 &&
    codec_info_ctx.audio_codec_info.current_codec != QVP_RTP_PYLD_AMR &&
		codec_info_ctx.audio_codec_info.current_codec != QVP_RTP_PYLD_EVS &&
    codec_info_ctx.audio_codec_info.current_codec != QVP_RTP_PYLD_G722_2 &&
    codec_info_ctx.audio_codec_info.current_codec != QVP_RTP_PYLD_G711_U&&
    codec_info_ctx.audio_codec_info.current_codec != QVP_RTP_PYLD_G711_A )
  {
    QVP_RTP_ERR_1("incoming_pkt_cb: codec_info_ctx.audio_codec_info.current_codec is invalid = %d", codec_info_ctx.audio_codec_info.current_codec);
    return;
  }
  frame.sMediaPacketInfo.sMediaPktInfoRx.iTimeStamp = pkt->tstamp;
  frame.sMediaPacketInfo.sMediaPktInfoRx.iSeqNum = pkt->seq;
  frame.sMediaPacketInfo.sMediaPktInfoRx.bMarkerBit = pkt->marker_bit;
  switch(codec_info_ctx.audio_codec_info.current_codec)
  {
  case QVP_RTP_PYLD_EVRC0:
    {
      frame.sMediaPacketInfo.sMediaPktInfoRx.bSilence = ( pkt->len == QVP_RTP_EVRC_RATE_8 ) ? TRUE : FALSE;
      frame.sMediaPacketInfo.sMediaPktInfoRx.iPayloadType = AUDIO_CODEC_EVRC0;
    }
    break;

  case QVP_RTP_PYLD_EVRCB0:
    {
      frame.sMediaPacketInfo.sMediaPktInfoRx.bSilence = ( pkt->len == QVP_RTP_EVRC_RATE_8 ) ? TRUE : FALSE;
      frame.sMediaPacketInfo.sMediaPktInfoRx.iPayloadType = AUDIO_CODEC_EVRCB0;
    }
    break;

  case QVP_RTP_PYLD_EVRCWB0:
    {
      frame.sMediaPacketInfo.sMediaPktInfoRx.bSilence = ( pkt->len == QVP_RTP_EVRC_RATE_8 ) ? TRUE : FALSE;
      frame.sMediaPacketInfo.sMediaPktInfoRx.iPayloadType = AUDIO_CODEC_EVRCWB0;
    }
    break;
	case QVP_RTP_PYLD_EVS:
		{
			if(pkt->frm_info.info.aud_info.profile_info.evs_info.is_primary)
				frame.sMediaPacketInfo.sMediaPktInfoRx.bSilence = ( pkt->len == QVP_RTP_EVS_PRIM_MODE_RATE_SID_5) ? TRUE : FALSE;
			frame.sMediaPacketInfo.sMediaPktInfoRx.iPayloadType = AUDIO_CODEC_EVS;
			frame.sFrameInfo.sEVSInfo.iEVSModeRequest = pkt->frm_info.info.aud_info.profile_info.evs_info.evs_mode;

		}
		break;
  case QVP_RTP_PYLD_AMR:
    {
      frame.sMediaPacketInfo.sMediaPktInfoRx.bSilence = ( pkt->len == QVP_RTP_AMR_MODE_RATE_SID_5) ? TRUE : FALSE;
      frame.sMediaPacketInfo.sMediaPktInfoRx.iPayloadType = AUDIO_CODEC_AMR;
      if(pkt->frm_info.info.aud_info.profile_info.amr_info.amr_mode <=7)
        frame.sFrameInfo.sAMRInfo.iAMRModeRequest = pkt->frm_info.info.aud_info.profile_info.amr_info.amr_mode;
	  frame.amr_fqi=pkt->amr_fqi;/*fqi bit for amr*/
    }
    break;
  case QVP_RTP_PYLD_G722_2:
    {
      frame.sMediaPacketInfo.sMediaPktInfoRx.bSilence = ( pkt->len == QVP_RTP_AMR_MODE_RATE_SID_5) ? TRUE : FALSE;
      frame.sMediaPacketInfo.sMediaPktInfoRx.iPayloadType = AUDIO_CODEC_AMR_WB;
      if(pkt->frm_info.info.aud_info.profile_info.amr_info.amr_mode <=8)
        frame.sFrameInfo.sAMRInfo.iAMRModeRequest = pkt->frm_info.info.aud_info.profile_info.amr_info.amr_mode;
	  frame.amr_fqi=pkt->amr_fqi;/*fqi bit for amr*/
    }
    break;
  case QVP_RTP_PYLD_G711_U:
    {
	  QVP_RTP_ERR_0("qvp_rtp_handle_incoming_pkt_cb: Received SID QVP_RTP_PYLD_G711_U");
      frame.sMediaPacketInfo.sMediaPktInfoRx.bSilence = ( pkt->len == QVP_RTP_G711AU_SID) ? TRUE : FALSE;
      frame.sMediaPacketInfo.sMediaPktInfoRx.iPayloadType = AUDIO_CODEC_G711U;
	  frame.sFrameInfo.sG711Info.iCompandingType = AUDIO_G711_U_LAW;
    }
    break;
  case QVP_RTP_PYLD_G711_A:
    {
	  QVP_RTP_ERR_0("qvp_rtp_handle_incoming_pkt_cb: Received SID QVP_RTP_PYLD_G711_A");
      frame.sMediaPacketInfo.sMediaPktInfoRx.bSilence = ( pkt->len == QVP_RTP_G711AU_SID) ? TRUE : FALSE;
      frame.sMediaPacketInfo.sMediaPktInfoRx.iPayloadType = AUDIO_CODEC_G711A;
	  frame.sFrameInfo.sG711Info.iCompandingType = AUDIO_G711_A_LAW;
    }
    break;	
  default:
    {
      frame.sMediaPacketInfo.sMediaPktInfoRx.bSilence = FALSE;
    }
    break;
  }
  frame.pData = pkt->data;

  /* Check if the codec is EVRC
  */
  if( ( codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_EVRC0 )  ||
    ( codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_EVRCB0 ) ||
    ( codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_EVRCWB0 ) )
  {
    /* If the length of the packet is greater than
    ** APP MEDIA_AUD_MAX_EVRC_BUF_SIZE bytes, then
    ** return without enqueuing the packet into the buffer
    */
    if ( pkt->len > QVP_RTP_AUD_MAX_EVRC_BUF_SIZE )
    {
      QVP_RTP_ERR_2("incoming_pkt_cb: EVRC (%d) pkt len (%d) exceeds max",
        codec_info_ctx.audio_codec_info.current_codec, pkt->len);
      return;
    }
  }

  /* Check if the codec is G711
  */

  if( ( codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_G711_U ) || (codec_info_ctx.audio_codec_info.current_codec == QVP_RTP_PYLD_G711_A ) )
  {
    /* If the length of the packet is greater than
    ** APP MEDIA_AUD_MAX_G711_BUF_SIZE bytes, then
    ** return without enqueuing the packet into the buffer
    */
    if ( pkt->len > QVP_RTP_AUD_MAX_G711_BUF_SIZE )
    {
      QVP_RTP_ERR_2("incoming_pkt_cb: G711 (%d) pkt len (%d) exceeds max",
        codec_info_ctx.audio_codec_info.current_codec, pkt->len);
      return;
    }
  }


  frame.iDataLen = pkt->len;
  frame.iMaxBuffLen = pkt->len;

  audioDesc = codec_info_ctx.audio_codec_info.audioDesc;
  if(audioDesc == NULL)
  {
    QVP_RTP_ERR_0("incoming_pkt_cb: NULL descriptor.");
    return ;
  }

  if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STARTED)
  {
    QPE_AUDIO_ERROR eError = qpAudioPlayFrame(audioDesc, &frame);
    if(eError == AUDIO_ERROR_OK)
    {
     // QVP_RTP_MSG_LOW_0("qvp_rtp_handle_incoming_pkt_cb : Call to audio play frame success.");
    }
    else
    {
      QVP_RTP_ERR_1("incoming_pkt_cb : audio play frame failed with error = %d",eError);
    }
  }
}

/*===========================================================================
FUNCTION qvp_rtp_empty_jitter_buffer_pvt

DESCRIPTION
This function used to empty jitterbuffer

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_empty_jitter_buffer_pvt(
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                     * the stream
                                     */
qvp_rtp_stream_id_type  stream_id   /* stream which was opened */
                                     )
{
if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STARTED)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  QP_AUDIO_DESC       audioDesc;
  QPE_AUDIO_ERROR     eError;
  QVP_RTP_MSG_HIGH_2_MEM("API qvp_rtp_empty_jitter_buffer_pvt entered\
                  with appid %d and stream id %d for media_type", app_id, stream_id);

  audioDesc = codec_info_ctx.audio_codec_info.audioDesc;
  //check for the initialized code descriptor.
  if(audioDesc == NULL)
  {
    QVP_RTP_ERR_0("empty_jitter_buffer_pvt : null codecDesc !!!");

    status = QVP_RTP_ERR_FATAL;
  }
  if(status == QVP_RTP_SUCCESS && codec_info_ctx.audio_codec_info.audio_state != QVP_RTP_CODEC_STATE_NONE)
  {
    eError = qpDplAudioEmptyJitterBuffer(audioDesc);
    if(eError != AUDIO_ERROR_OK)
    {
      QVP_RTP_ERR_0("empty_jitter_buffer_pvt : qpDplAudioEmptyJitterBuffer failed ");
      //should we reelase the codec here?
      status = QVP_RTP_ERR_FATAL;
    }
    status = QVP_RTP_SUCCESS;
  }
  return status;
}
else
{
  QVP_RTP_MSG_LOW_0("empty_jitter_buffer_pvt: codec not stared not resetting qdj");
  return QVP_RTP_SUCCESS;
}
}

void qvp_rtp_handle_audio_rtcp_cb(
  qvp_rtp_app_id_type      app_id,    /* RTP Application ID*/
  qvp_rtp_stream_id_type   stream,    /* stream id to which the rtcp info belongs */
  qvp_rtcp_session_info_type *rtcp_info, /* RTCP info propogated to app */
  qvp_rtcp_local_param_type  *rtcp_stats  /* RTP Local statistics */
  )
{
  QP_AUDIO_DESC       audioDesc;
  //QPE_AUDIO_ERROR     eError;
  dword                                     hi_ntp; // the value for the high 32 bits 
  dword                                     lo_ntp; // the value for the high 32 bits 
  audioDesc = codec_info_ctx.audio_codec_info.audioDesc;

  //check for the initialized code descriptor.
  if(audioDesc == NULL)
  {
    QVP_RTP_ERR_0("handle_rtcp_cb : null codecDesc !!!");

    return;
  }
  QVP_RTP_MSG_LOW_2("handle_audio_rtcp_cb: app_id = %d, stream = %d",
    app_id, stream);
  hi_ntp = ( rtcp_info->info.sr.ntp >> 32);
  lo_ntp = (uint32)( rtcp_info->info.sr.ntp );
  if(rtcp_info->inf_type == QVP_RTCP_SR){
    //eError = qpDplAudioReport(audioDesc, hi_ntp, lo_ntp, rtcp_info->info.sr.tstamp);
    qvp_rtp_service_sendaudio_report(hi_ntp, lo_ntp, rtcp_info->info.sr.tstamp);
    /*if(eError != AUDIO_ERROR_OK)
    {
      QVP_RTP_ERR_0("qvp_rtp_handle_rtcp_cb : Method call to qpDplAudioReport failed ");
      return;
    }*/
    QVP_RTP_MSG_LOW("hi_ntp = %u, lo_ntp = %u, rtcp_info->info.sr.tstamp = %u", hi_ntp, lo_ntp, rtcp_info->info.sr.tstamp);
  }
}
void qvp_rtp_handle_audio_rx_delay_cb(uint16 rx_delay)
{
	qvp_rtp_service_audio_rx_delay(rx_delay);
}

qvp_rtp_status_type qvp_rtp_handle_new_red_update(uint32 fec_offset, uint32 fer_rate, void* event_data)
{
	QP_AUDIO_DESC       audioDesc;
	QPE_AUDIO_ERROR     eError;
	audioDesc = codec_info_ctx.audio_codec_info.audioDesc;

	//check for the initialized code descriptor.
	if(audioDesc == NULL)
	{
		QVP_RTP_ERR_0("qvp_rtp_handle_new_red_update:null codecDesc !!!");

		return QVP_RTP_ERR_FATAL;
	}

	if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STARTED || codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_RECONFIGURE)
	{
		eError = qpAudioUpdateRedOffset(audioDesc, fec_offset, fer_rate, (QPE_EVS_BW)event_data);
		if(eError != AUDIO_ERROR_OK)
		{
			QVP_RTP_ERR_1("qpAudioUpdateRedOffset : update redundancy failed with error = %d",eError);
			return QVP_RTP_ERR_FATAL;
		}

	}
	else
	{
		QVP_RTP_ERR_0("qpAudioUpdateRedOffset :call to update redundancy failed ");
		return QVP_RTP_ERR_FATAL;
	}
	return QVP_RTP_SUCCESS;
}

/*===========================================================================
FUNCTION qvp_rtp_handle_incoming_cmr_request

DESCRIPTION
This function handlesincomming cmr change request

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_handle_codec_mode_change_request(uint8 mode, void* event_data)
{
  QP_AUDIO_DESC       audioDesc;
  QP_CODEC_CONFIG     tCodecConfig;
  QPE_AUDIO_ERROR     eError;
  audioDesc = codec_info_ctx.audio_codec_info.audioDesc;

  //check for the initialized code descriptor.
  if(audioDesc == NULL)
  {
    QVP_RTP_ERR_0("handle_codec_mode_change_request:null codecDesc !!!");

    return QVP_RTP_ERR_FATAL;
  }

 if(codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_STARTED || codec_info_ctx.audio_codec_info.audio_state == QVP_RTP_CODEC_STATE_RECONFIGURE)
 {
  memset(&tCodecConfig, 0, sizeof(QP_CODEC_CONFIG));
  tCodecConfig.iClockRate = codec_info_ctx.audio_codec_info.audio_codec_config.iClockRate;
  tCodecConfig.iFrameDuration = codec_info_ctx.audio_codec_info.audio_codec_config.iFrameDuration;
  tCodecConfig.iPTime = codec_info_ctx.audio_codec_info.audio_codec_config.iPTime;
  tCodecConfig.bLipSyncEnable = codec_info_ctx.audio_codec_info.audio_codec_config.bLipSyncEnable;
  tCodecConfig.bSilenceSupp = codec_info_ctx.audio_codec_info.audio_codec_config.bSilenceSupp;
  tCodecConfig.bDTXEnable = codec_info_ctx.audio_codec_info.audio_codec_config.bDTXEnable;
  tCodecConfig.iDtxMax = codec_info_ctx.audio_codec_info.audio_codec_config.iDtxMax;
  tCodecConfig.iDtxMin = codec_info_ctx.audio_codec_info.audio_codec_config.iDtxMin;
  tCodecConfig.iHangover = codec_info_ctx.audio_codec_info.audio_codec_config.iHangover;
  codec_info_ctx.audio_codec_info.audio_codec_config.sCodecInfo.sAMRInfo.eCodecMode = (QVP_RTP_AUDIO_AMR_CODEC_MODE)mode;
  tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode = (QPE_AUDIO_AMR_CODEC_MODE)mode;

  {
    switch(codec_info_ctx.audio_codec_info.audio_codec_config.eCodec)
    {
    case QVP_RTP_PYLD_EVRC0:
      {
        tCodecConfig.eCodec = AUDIO_CODEC_EVRC0;
      }
      break;

    case QVP_RTP_PYLD_EVRCB0:
      {
        tCodecConfig.eCodec = AUDIO_CODEC_EVRCB0;
      }
      break;
    case QVP_RTP_PYLD_EVRCWB0:
      {
        tCodecConfig.eCodec = AUDIO_CODEC_EVRCWB0;
      }
      break;

    case QVP_RTP_PYLD_AMR:
      {
        tCodecConfig.eCodec = AUDIO_CODEC_AMR;
      }
      break;
			case QVP_RTP_PYLD_EVS:
				{
					tCodecConfig.eCodec = AUDIO_CODEC_EVS;
					//tCodecConfig.sCodecInfo.sEVSInfo.tx_evs_red_offset = media_config->audio_codec_config.sCodecInfo.sEVSInfo.tx_evs_red_offset;
					//tCodecConfig.sCodecInfo.sEVSInfo.rx_evs_red_offset = media_config->audio_codec_config.sCodecInfo.sEVSInfo.rx_evs_red_offset;
					tCodecConfig.sCodecInfo.sEVSInfo.eTxBW = (QPE_EVS_BW)event_data;;
					tCodecConfig.sCodecInfo.sEVSInfo.is_evs_primary = TRUE;//EVS TODO pick up dynamically
					if(tCodecConfig.sCodecInfo.sEVSInfo.is_evs_primary)
						tCodecConfig.sCodecInfo.sEVSInfo.eCodecMode.ePrmyMode = (QPE_EVS_PRIMARY_MODE)mode;;
					//else return error in PHASE 1 EVSTODO

				}
      break;
    case QVP_RTP_PYLD_G722_2:
      {
        tCodecConfig.eCodec = AUDIO_CODEC_AMR_WB;
      }
      break;
    default:
      {
        // Should be set to Default
        tCodecConfig.eCodec = AUDIO_CODEC_AMR;
        tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode = AUDIO_CODEC_MODE_7;
      }
      break;
    }
  }

  //do codec set.
  eError = qpAudioCodecSet(audioDesc, tCodecConfig);
  if(eError != AUDIO_ERROR_OK)
  {
    QVP_RTP_ERR_1("handle_codec_mode_change_request : audio codec set failed with error = %d",eError);
    return QVP_RTP_ERR_FATAL;
  }
  if(codec_info_ctx.audio_codec_info.biscmrreq < QVP_MAX_CMR_PENDING_REQ)
  {
  codec_info_ctx.audio_codec_info.biscmrreq++;
 }
 }
 else
 {
   QVP_RTP_ERR_0("handle_codec_mode_change_request :call to audio codec set failed ");
   return QVP_RTP_ERR_FATAL;
 }
 return QVP_RTP_SUCCESS;
}
uint16 qvp_rtp_get_audio_clk_rate(void)
{
  return codec_info_ctx.audio_codec_info.audio_codec_config.iClockRate;
}
QP_AUDIO_DESC qvp_rtp_get_audio_descp(void)
{
  return codec_info_ctx.audio_codec_info.audioDesc;
}

qvp_rtp_codec_state qvp_rtp_get_audio_state(void)
{
  return codec_info_ctx.audio_codec_info.audio_state;
}

qvp_rtp_status_type qvp_rtp_clear_audio_resource
(
  qvp_rtp_app_id_type    app_id      /* application which opened
                                       * the stream
                                     */
)
{
	QPE_AUDIO_ERROR     eError = AUDIO_ERROR_OK;
      QVP_RTP_MSG_HIGH_0("qvp_rtp_media_clear_audio_resource.");
#ifndef FEATURE_IMS_PC_TESTBENCH
      eError = qpAudioClearAudioResource(codec_info_ctx.audio_codec_info.audioDesc);
#endif/*FEATURE_IMS_PC_TESTBENCH*/
      codec_info_ctx.audio_codec_info.audio_state = QVP_RTP_CODEC_STATE_NONE;
      codec_info_ctx.audio_codec_info.audioDesc = QP_NULL;
      g_is_release_pending = FALSE;
      codec_info_ctx.audio_codec_info.app_id = (qvp_rtp_app_id_type)0xdeadbeef;
      codec_info_ctx.audio_codec_info.stream_id = (qvp_rtp_stream_id_type)0xdeadbeef;
  if(eError != AUDIO_ERROR_OK)
  {
    return QVP_RTP_ERR_FATAL;
  }
  else
      return QVP_RTP_SUCCESS;
}


void qvp_rtp_execute_pending_handoff(void)
{	
  if(g_handoffstate == HO_IN_PROGRESS)
  {
		QVP_RTP_MSG_LOW_0("qvp_rtp_codec_audio_cb() :Handoff in progress");
	qvp_rtp_mark_pending_task_done(codec_info_ctx.audio_codec_info.app_id);
	qvp_rtp_execute_pending_command(codec_info_ctx.audio_codec_info.app_id);
  } 
	return;
}

QPBOOL qvp_rtp_get_active_session(uint8* streamId, uint8* appId)
{
  if(QVP_RTP_CODEC_STATE_NONE != codec_info_ctx.audio_codec_info.audio_state )
  {
    *streamId = (uint8)codec_info_ctx.audio_codec_info.stream_id;
    *appId = (uint8)codec_info_ctx.audio_codec_info.app_id;
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

QPBOOL qvp_rtp_codec_set_qdj_threshold(qvp_rtp_stream_id_type streamId, 
                                     qvp_rtp_app_id_type appId,
                                     uint8 silence,
                                     uint8 speech,
                                     qp_audio_metirc_cb pCbfptr)
{
  if(QVP_RTP_CODEC_STATE_NONE == codec_info_ctx.audio_codec_info.audio_state )
  {
    QVP_RTP_ERR_0("metrics:qvp_rtp_codec_set_qdj_threshold: invalid state");
    return FALSE;
  }

  if(codec_info_ctx.audio_codec_info.stream_id != streamId &&
    codec_info_ctx.audio_codec_info.app_id != appId)
  {
    QVP_RTP_ERR("metrics:qvp_rtp_codec_set_qdj_threshold: invalid param s:%d a:%d s:%d",
                    (uint32)streamId, (uint32)appId, 
                    (uint32)codec_info_ctx.audio_codec_info.stream_id);
    return FALSE;
  }
  return(qpAudioSetQdjThresh(silence, speech, pCbfptr));
}
#endif /* end of FEATURE_QVPHONE_RTP */