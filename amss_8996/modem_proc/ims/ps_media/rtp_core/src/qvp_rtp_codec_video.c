/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

qvp_rtp_codec_video. c

GENERAL DESCRIPTION

This file contains functions which talks to DPL CVD for various codec operations.

Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is
regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==*=*/


#include "ims_variation.h"
#include "stdlib.h"

#include "comdef.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP

#ifdef  RTP_FILE_NUMBER 
  #undef RTP_FILE_NUMBER 
#endif 
#define RTP_FILE_NUMBER 14

/*===========================================================================
INCLUDE FILES FOR MODULE
===========================================================================*/
#include"qvp_app_common.h"
#include "string.h"
#include "qvp_rtp_log.h"
#include "qvp_rtp_codec.h"
#ifndef FEATURE_IMS_PC_TESTBENCH
#include "qvp_common.h"
#endif

extern qvp_rtp_codec_info_ctx codec_info_ctx;

extern void qvp_rtp_process_tx_pkt
(
 qvp_rtp_app_id_type    app_id,      /* application which opened
                                     * the stream
                                     */
 qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
 uint8* frame_data,
 uint16 frame_length,
 uint32 tstamp,
 uint64 avtimestamp
);

/*===========================================================================
FUNCTION qvp_rtp_codec_video_tx_cb

DESCRIPTION
This is the callback rtp uses to handle the various DPL Recorder video events

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
void qvp_rtp_codec_video_tx_cb(QPE_VIDEO_MSG tVideoMsg,
                               void* pParam1,
                               uint32 iParam2,
                               void* pUserData)
{
  qvp_rtp_codec_info_ctx * codec_info_cb_ptr = (qvp_rtp_codec_info_ctx *)pUserData;
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  
  if(codec_info_cb_ptr == NULL)
  {
    QVP_RTP_ERR_0("qvp_rtp_codec_video_tx_cb: codec_info_cb_ptr is NULL");
    return;
  }

  QVP_RTP_MSG_HIGH("qvp_rtp_codec_video_tx_cb() : Vid Message %d state = %d Dir: %d", 
                   tVideoMsg, codec_info_cb_ptr->video_codec_info.video_tx_state, codec_info_cb_ptr->video_codec_info.direction);


	switch(tVideoMsg)
	{
  case VIDEO_MSG_DEV_INITIALISED:
    {
      if(codec_info_cb_ptr->video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_INITIALIZING)
      {
        codec_info_cb_ptr->video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_INITDONE;
        if(codec_info_cb_ptr->video_codec_info.handle_media_res)
        {
          codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
                                                               codec_info_cb_ptr->video_codec_info.stream_id, 
                                                               QVP_RTP_MEDIA_CODEC_INIT,
                                                               QVP_RTP_SUCCESS,
                                                               QVP_RTP_DIR_TX);

          //Configure the codec
          /*status = qvp_rtp_media_codec_configure_video_pvt(codec_info_cb_ptr->video_codec_info.app_id,
                                                           codec_info_cb_ptr->video_codec_info.stream_id,
                                                           &codec_info_cb_ptr->media_config,
                                                           QVP_RTP_DIR_TX);*/

        }
        else
        {
          QVP_RTP_ERR_0("qvp_rtp_codec_video_tx_cb: APP CB is NULL");
          return;
        }
      }
      else
      {
        QVP_RTP_ERR_0("qvp_rtp_codec_video_tx_cb: Recorder state is wrong");
        if(codec_info_cb_ptr->video_codec_info.handle_media_res)
        {
          codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
                                                               codec_info_cb_ptr->video_codec_info.stream_id, 
                                                               QVP_RTP_MEDIA_CODEC_INIT,
                                                               status,
                                                               QVP_RTP_DIR_TX);

        }
        return;
      }
    }
		break;
  case VIDEO_MSG_CODEC_CHANGED:
    //codec changed
    {
      if((codec_info_cb_ptr->video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_INITDONE) ||
         (codec_info_cb_ptr->video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_CONFIGURING) ||
         (codec_info_cb_ptr->video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_RECONFIGURE))
      {
        if(codec_info_cb_ptr->video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_RECONFIGURE)
        {
          codec_info_cb_ptr->video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_STARTED;
        }
        else
        {
          codec_info_cb_ptr->video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_CONFIGUREDONE;
        }

        status = QVP_RTP_SUCCESS;

        if(codec_info_cb_ptr->video_codec_info.handle_media_res)
        {
          codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
                                                               codec_info_cb_ptr->video_codec_info.stream_id, 
                                                               QVP_RTP_MEDIA_CODEC_CONFIG,
                                                               status,
                                                               QVP_RTP_DIR_TX);
        }
        else
        {
          QVP_RTP_ERR_0("qvp_rtp_codec_video_tx_cb: APP CB is NULL");
          return;
        }
      }
      else
      {
        QVP_RTP_ERR_0("qvp_rtp_codec_video_tx_cb: Recorder state is wrong");
        if(codec_info_cb_ptr->video_codec_info.handle_media_res)
        {
          codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
                                                               codec_info_cb_ptr->video_codec_info.stream_id, 
                                                               QVP_RTP_MEDIA_CODEC_CONFIG,
                                                               status,
                                                               QVP_RTP_DIR_TX);

        }
        return;
      }
    }
    break;
	case VIDEO_MSG_DEV_UNINITIALISED:
		{
      //device uninitialized
      codec_info_cb_ptr->video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_NONE;
      codec_info_cb_ptr->video_codec_info.TxDesc = QP_NULL;
      QVP_RTP_MSG_LOW_0("qvp_rtp_codec_video_tx_cb() : Video Dev Recorder Un-initialize success.");
      //notify APP about un-init sucess?
      //notify to APP about config codec sucess
      if(codec_info_cb_ptr->video_codec_info.handle_media_res)
      {
        codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
          codec_info_cb_ptr->video_codec_info.stream_id, QVP_RTP_MEDIA_VIDEO_CODEC_RELEASE,QVP_RTP_SUCCESS, QVP_RTP_DIR_TX);  
      } 
      else
      {
        QVP_RTP_ERR_0("qvp_rtp_codec_video_tx_cb: APP CB is NULL");
        return;
      }
    }
    break;
	case VIDEO_MSG_RECORDER_STARTED:
		{
      if(codec_info_cb_ptr->video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_STARTING)
      {
        codec_info_cb_ptr->video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_STARTED;
        QVP_RTP_MSG_LOW_0("qvp_rtp_codec_video_tx_cb() : Recorder start success.");
        status = QVP_RTP_SUCCESS;
        //notify to APP about stream start  sucess
      }
      else
      {
        QVP_RTP_ERR_1("qvp_rtp_codec_video_tx_cb: Recorder in wrong state %d",codec_info_cb_ptr->video_codec_info.video_tx_state);
      }
      //notify to APP about config codec sucess
      if(codec_info_cb_ptr->video_codec_info.handle_media_res)
      {
        codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
        codec_info_cb_ptr->video_codec_info.stream_id, QVP_RTP_MEDIA_START_STREAM,status, QVP_RTP_DIR_TX);
      }
      else
      {
        QVP_RTP_ERR_0("qvp_rtp_codec_video_tx_cb: APP CB is NULL");
        return;
      }
		}
		break;
  case VIDEO_MSG_RECORDER_STOPPED:
    {
      //Recorder Stopped
      if(codec_info_cb_ptr->video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_STOPPING)
      {
        codec_info_cb_ptr->video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_INITDONE;
        QVP_RTP_MSG_LOW_0("qvp_rtp_codec_video_tx_cb() : Recorder stop success.");
        status = QVP_RTP_SUCCESS;
        //notify to APP about stream start  sucess
      }
      else
      {
        QVP_RTP_ERR_1("qvp_rtp_codec_video_tx_cb: Recorder in wrong state %d",codec_info_cb_ptr->video_codec_info.video_tx_state);
      }
      //notify to APP about config codec sucess
      if(codec_info_cb_ptr->video_codec_info.handle_media_res)
      {
        codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
          codec_info_cb_ptr->video_codec_info.stream_id, QVP_RTP_MEDIA_STOP_STREAM,status, QVP_RTP_DIR_TX);
      }
      else
      {
        QVP_RTP_ERR_0("qvp_rtp_codec_video_tx_cb: APP CB is NULL");
        return;
      }
    }
		break;
  case VIDEO_MSG_RECORDED_DATA:
    //TX Frame received
    {
      QP_MULTIMEDIA_FRAME* FrameData = QP_NULL;
      uint8				      *play_pkt_ptr = NULL;
      uint16 	           play_pkt_len = 0;
      uint32			       tstamp = 0;

      FrameData =  ((QP_MULTIMEDIA_FRAME*) pParam1);
      if(codec_info_cb_ptr->video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_STARTED)
      {
        QVP_RTP_MSG_LOW_0("qvp_rtp_codec_video_tx_cb() : Video data received.");
        if(FrameData != NULL)
        {
          /* Copy vocoder pkt ptr to the play pkt ptr */
          play_pkt_ptr = (uint8*)FrameData->pData;
          /* Copy pkt len*/
          play_pkt_len = (uint16)FrameData->iDataLen;
          /* copy pkt time stamp */
          tstamp = FrameData->sMediaPacketInfo.sMediaPktInfoTx.iTimeStamp;

          QVP_RTP_MSG_LOW_1("qvp_rtp_codec_video_tx_cb() : uplink pkt len  %d",play_pkt_len);
        }
        if((play_pkt_len == 0) || (play_pkt_ptr == QP_NULL))
        {
          QVP_RTP_MSG_LOW_2("qvp_rtp_codec_video_tx_cb() : Empty media pkt, app_id: %d stream_id: %d",
            codec_info_cb_ptr->video_codec_info.app_id, codec_info_cb_ptr->video_codec_info.stream_id);
          return;
        }
        qvp_rtp_process_tx_pkt(codec_info_cb_ptr->video_codec_info.app_id, codec_info_cb_ptr->video_codec_info.stream_id, play_pkt_ptr, play_pkt_len, tstamp,0 );
      }
      else
      {
        QVP_RTP_ERR_1("qvp_rtp_codec_video_tx_cb: VIDEO_MSG_RECORDED_DATA in wrong state %d",codec_info_cb_ptr->video_codec_info.video_tx_state);
      }
    }	
		break;
  case VIDEO_MSG_ERROR:
    //Video Error
    {
      //Video Error
      QVP_RTP_ERR_1("qvp_rtp_codec_video_tx_cb: Recorder Error in state %d",codec_info_cb_ptr->video_codec_info.video_tx_state);
      if(codec_info_cb_ptr->video_codec_info.handle_media_res)
      {
        codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
        codec_info_cb_ptr->video_codec_info.stream_id, QVP_RTP_MEDIA_ERROR,status, QVP_RTP_DIR_TX);
      }
      //TBD: Should I be changing Recorder state to Error or keep it whatever state it is in so that it will help at clean up time?
    }
    break;
	default:
		//Unknown case
		break;
	}
}


/*===========================================================================
FUNCTION qvp_rtp_codec_video_rx_cb

DESCRIPTION
This is the callback rtp uses to handle the various DPL Player video events

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
void qvp_rtp_codec_video_rx_cb(QPE_VIDEO_MSG tVideoMsg,
                            void* pParam1,
                            uint32 iParam2,
                            void* pUserData)
{
  qvp_rtp_codec_info_ctx * codec_info_cb_ptr = (qvp_rtp_codec_info_ctx *)pUserData;
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  
  if(codec_info_cb_ptr == NULL)
  {
    QVP_RTP_ERR_0("qvp_rtp_codec_video_rx_cb: codec_info_cb_ptr is NULL");
    return;
  }

  QVP_RTP_MSG_HIGH("qvp_rtp_codec_video_rx_cb() : Vid Message %d state = %d Dir: %d", 
                   tVideoMsg, codec_info_cb_ptr->video_codec_info.video_rx_state, codec_info_cb_ptr->video_codec_info.direction);

  switch(tVideoMsg)
	{
	case VIDEO_MSG_DEV_INITIALISED:
    //device initialized
    {
      if(codec_info_cb_ptr->video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_INITIALIZING)
      {
        codec_info_cb_ptr->video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_INITDONE;
        if(codec_info_cb_ptr->video_codec_info.handle_media_res)
        {
          codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
                                                               codec_info_cb_ptr->video_codec_info.stream_id, 
                                                               QVP_RTP_MEDIA_CODEC_INIT,
                                                               QVP_RTP_SUCCESS,
                                                               QVP_RTP_DIR_RX);

          //Configure the codec
         /* status = qvp_rtp_media_codec_configure_video_pvt(codec_info_cb_ptr->video_codec_info.app_id,
                                                           codec_info_cb_ptr->video_codec_info.stream_id,
                                                           &codec_info_cb_ptr->media_config,
                                                           QVP_RTP_DIR_RX);*/

        }
        else
        {
          QVP_RTP_ERR_0("qvp_rtp_codec_video_rx_cb: APP CB is NULL");
          return;
        }
      }
      else
      {
        QVP_RTP_ERR_0("qvp_rtp_codec_video_rx_cb: Player state is wrong");
        if(codec_info_cb_ptr->video_codec_info.handle_media_res)
        {
          codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
                                                               codec_info_cb_ptr->video_codec_info.stream_id, 
                                                               QVP_RTP_MEDIA_CODEC_INIT,
                                                               status,
                                                               QVP_RTP_DIR_RX);

        }
        return;
      }
    }
    break;
  case VIDEO_MSG_CODEC_CHANGED:
    //codec changed
		{
		  if((codec_info_cb_ptr->video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_INITDONE) ||
         (codec_info_cb_ptr->video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_CONFIGURING) ||
         (codec_info_cb_ptr->video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_RECONFIGURE))
      {
        if(codec_info_cb_ptr->video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_RECONFIGURE)
        {
          codec_info_cb_ptr->video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_STARTED;
        }
        else
        {
          codec_info_cb_ptr->video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_CONFIGUREDONE;
        }

        status = QVP_RTP_SUCCESS;

        if(codec_info_cb_ptr->video_codec_info.handle_media_res)
        {
          codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
                                                               codec_info_cb_ptr->video_codec_info.stream_id, 
                                                               QVP_RTP_MEDIA_CODEC_CONFIG,
                                                               status,
                                                               QVP_RTP_DIR_RX);
        }
        else
        {
          QVP_RTP_ERR_0("qvp_rtp_codec_video_rx_cb: APP CB is NULL");
          return;
        }
      }
      else
      {
        QVP_RTP_ERR_0("qvp_rtp_codec_video_rx_cb: Player state is wrong");
        if(codec_info_cb_ptr->video_codec_info.handle_media_res)
        {
          codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
                                                               codec_info_cb_ptr->video_codec_info.stream_id, 
                                                               QVP_RTP_MEDIA_CODEC_CONFIG,
                                                               status,
                                                               QVP_RTP_DIR_RX);

        }
        return;
      }	
		}
		break;
	case VIDEO_MSG_DEV_UNINITIALISED:
		{
      //device uninitialized
      codec_info_cb_ptr->video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_NONE;
      codec_info_cb_ptr->video_codec_info.RxDesc = QP_NULL;
      QVP_RTP_MSG_LOW_0("qvp_rtp_codec_video_rx_cb() : Video Dev Player Un-initialize success.");
      //notify APP about un-init sucess?
      //notify to APP about config codec sucess
      if(codec_info_cb_ptr->video_codec_info.handle_media_res)
      {
        codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
        codec_info_cb_ptr->video_codec_info.stream_id, QVP_RTP_MEDIA_VIDEO_CODEC_RELEASE,QVP_RTP_SUCCESS, QVP_RTP_DIR_RX);  
      }
    }
    break;
	case VIDEO_MSG_PLAYER_STARTED:
		{
      if(codec_info_cb_ptr->video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_STARTING)
      {
        codec_info_cb_ptr->video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_STARTED;
        QVP_RTP_MSG_LOW_0("qvp_rtp_codec_video_rx_cb() : Player start success.");
        status = QVP_RTP_SUCCESS;
        //notify to APP about stream start  sucess
      }
      else
      {
        QVP_RTP_ERR_1("qvp_rtp_codec_video_rx_cb: Player in wrong state %d",codec_info_cb_ptr->video_codec_info.video_rx_state);
      }
      //notify to APP about config codec sucess
      if(codec_info_cb_ptr->video_codec_info.handle_media_res)
      {
        codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
        codec_info_cb_ptr->video_codec_info.stream_id, QVP_RTP_MEDIA_START_STREAM,status, QVP_RTP_DIR_RX);
      }
		}
		break;
	case VIDEO_MSG_PLAYER_STOPPED:
		{
      //Player Stopped
      if(codec_info_cb_ptr->video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_STOPPING)
      {
        codec_info_cb_ptr->video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_INITDONE;
        QVP_RTP_MSG_LOW_0("qvp_rtp_codec_video_rx_cb() : Player stop success.");
        status = QVP_RTP_SUCCESS;
        //notify to APP about stream start  sucess
      }
      else
      {
        QVP_RTP_ERR_1("qvp_rtp_codec_video_rx_cb: Player in wrong state %d",codec_info_cb_ptr->video_codec_info.video_rx_state);
      }
      //notify to APP about config codec sucess
      if(codec_info_cb_ptr->video_codec_info.handle_media_res)
      {
        codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
        codec_info_cb_ptr->video_codec_info.stream_id, QVP_RTP_MEDIA_STOP_STREAM,status, QVP_RTP_DIR_RX);
      }
		}
		break;
  case VIDEO_MSG_ERROR:
    {
      //Video Error
      QVP_RTP_ERR_1("qvp_rtp_codec_video_rx_cb: Player Error in state %d",codec_info_cb_ptr->video_codec_info.video_rx_state);
      if(codec_info_cb_ptr->video_codec_info.handle_media_res)
      {
        codec_info_cb_ptr->video_codec_info.handle_media_res(codec_info_cb_ptr->video_codec_info.app_id,
        codec_info_cb_ptr->video_codec_info.stream_id, QVP_RTP_MEDIA_ERROR,status, QVP_RTP_DIR_RX);
      }
      //TBD: Should I be changing player state to Error or keep it whatever state it is in so that it will help at clean up time?
    }
		break;
	default:
		//Unknown case
		break;
	}
}


/*===========================================================================
FUNCTION qvp_rtp_media_nal_hdr_cb

DESCRIPTION
This is the callback rtp uses to handle NAL HDR generation

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
void qvp_rtp_media_nal_hdr_cb(QPCHAR* pNalHdr, QPUINT16 iNalHdrLen, void* nparam1, void* nparam2)
{
  qvp_rtp_codec_info_ctx * codec_info_cb_ptr = (qvp_rtp_codec_info_ctx *)nparam1;
  if (codec_info_cb_ptr == NULL)
  {
    QVP_RTP_ERR_0("qvp_rtp_media_nal_hdr_cb: codec_info_cb_ptr is NULL");
    return;
  }

  if(codec_info_cb_ptr->video_codec_info.handle_generate_nal)
  {
    if (pNalHdr == NULL)
    {
      QVP_RTP_ERR_0("qvp_rtp_media_nal_hdr_cb: pNalHdr is NULL");
      codec_info_cb_ptr->video_codec_info.handle_generate_nal(NULL, QVP_RTP_ERR_FATAL,nparam2);
      return;
    }
    codec_info_cb_ptr->video_codec_info.handle_generate_nal(pNalHdr, QVP_RTP_SUCCESS,nparam2);
  }
  else
  {
    QVP_RTP_ERR_0("qvp_rtp_media_nal_hdr_cb: NAL HDR CB is NULL");
  }
}

/*===========================================================================
FUNCTION qvp_rtp_media_init_video_pvt

DESCRIPTION
This function initializes media layer

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_init_video_pvt
( 
  qvp_rtp_app_id_type    app_id       /* application which opened
                                       * the stream
                                       */
)
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  QPE_VIDEO_ERROR     eErr   = VIDEO_ERROR_OK;

  eErr = qpVideoInitialize();

  if (eErr != VIDEO_ERROR_OK)
  {
    QVP_RTP_ERR_0("qvp_rtp_media_init_video_pvt: VideoInitialize failed");
  }
  else
  {
    codec_info_ctx.video_codec_info.media_inited = TRUE;
    status = QVP_RTP_SUCCESS;
  }

  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_deinit_video_pvt

DESCRIPTION
This function un-initializes media layer

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/

qvp_rtp_status_type qvp_rtp_media_deinit_video_pvt
( 
  qvp_rtp_app_id_type    app_id       /* application which opened
                                       * the stream
                                       */
)
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  QPE_VIDEO_ERROR     eErr   = VIDEO_ERROR_OK;

  eErr = qpVideoUnInitialize();

  if (eErr != VIDEO_ERROR_OK)
  {
    QVP_RTP_ERR_0("qvp_rtp_media_init_video_pvt: VideoUnInitialize failed");
  }
  else
  {
    codec_info_ctx.video_codec_info.media_inited = FALSE;
    status = QVP_RTP_SUCCESS;
  }

  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_generate_nal_pvt

DESCRIPTION
This function generates H.264 NAL header for out of band negotiation

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_generate_nal_pvt
(
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                       * the stream
                                       */
  uint16                 height, 
  uint16                 width, 
  qvp_rtp_media_h264_profile_type profile,
  qvp_rtp_media_h264_level_type level,
  void* puserdata,
  qvp_rtp_media_handle_generate_nal handle_generate_nal
)
{
  qvp_rtp_status_type        status = QVP_RTP_ERR_FATAL;
  QPE_VIDEO_ERROR            eErr   = VIDEO_ERROR_OK;
  QP_VIDEO_NAL_HDR_CONFIG    nal_hdr_cfg;

  codec_info_ctx.video_codec_info.handle_generate_nal = handle_generate_nal;

  memset(&nal_hdr_cfg, 0, sizeof(QP_VIDEO_NAL_HDR_CONFIG));

  nal_hdr_cfg.iHeight = height;
  nal_hdr_cfg.iWidth  = width;
  // Directly typecasting between two enum as vlues are same between RTP enum and DPL enum
  nal_hdr_cfg.eLevel  = (QPE_VIDEO_H264_LEVEL) level;
  nal_hdr_cfg.eProfile= (QPE_VIDEO_H264_PROFILE) profile;


  eErr = qpVideoGenerateNalHeaderAsynch(&nal_hdr_cfg, qvp_rtp_media_nal_hdr_cb, &codec_info_ctx,puserdata);

  if (eErr != VIDEO_ERROR_OK)
  {
    QVP_RTP_ERR_0("qvp_rtp_media_generate_nal_pvt: GenerateNalHdr failed");
  }
  else
  {
    status = QVP_RTP_SUCCESS;
  }

  return status;
}


/*===========================================================================
FUNCTION qvp_rtp_media_codec_initialize_video_pvt

DESCRIPTION
This function initializes media codec engine

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_codec_initialize_video_pvt(
                                      qvp_rtp_app_id_type            app_id,      /* application which opened
                                                                                   * the stream
                                                                                   */
                                      qvp_rtp_stream_id_type         stream_id,    /* stream which was opened */
                                      qvp_rtp_media_config*          media_config,
                                      qvp_rtp_media_handle_response  handle_media_res,
                                      qvp_rtp_stream_direction       direction   /* Direction in which configuration
                                                                                  * needs to be applied
                                                                                  */

                                      )
{
  QP_VIDEO_CONFIG     tCodecConfig;
  qvp_rtp_media_handle_response app_cb = handle_media_res;
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  QPE_VIDEO_ERROR     eError;
  qvp_rtp_codec_state* rx_state   = &codec_info_ctx.video_codec_info.video_rx_state;
  qvp_rtp_codec_state* tx_state   = &codec_info_ctx.video_codec_info.video_tx_state;
  qvp_rtp_codec_state* rxtx_state = &codec_info_ctx.video_codec_info.video_rxtx_state;

  QVP_RTP_MSG_MED("API qvp_rtp_media_codec_initialize_video_pvt entered\
                  with appid %d stream id %d dir: %d", app_id, stream_id, direction);
  if (((direction == QVP_RTP_DIR_TXRX) &&\
       ((*rxtx_state != QVP_RTP_CODEC_STATE_NONE) || (*rx_state != QVP_RTP_CODEC_STATE_NONE) ||\
        (*tx_state != QVP_RTP_CODEC_STATE_NONE))) ||\
      ((direction == QVP_RTP_DIR_RX) && (*rx_state != QVP_RTP_CODEC_STATE_NONE)) ||\
      ((direction == QVP_RTP_DIR_TX) && (*tx_state != QVP_RTP_CODEC_STATE_NONE))
     )
  {
    QVP_RTP_ERR("qvp_rtp_media_codec_initialize_video_pvt: Wrong state: RX: %d TX: %d RXTX: %d ",*rx_state, *tx_state, *rxtx_state);
    return status;
  }

  if ((direction != QVP_RTP_DIR_TX) && (direction != QVP_RTP_DIR_RX) && (direction != QVP_RTP_DIR_TXRX))
  {
    QVP_RTP_ERR_0("qvp_rtp_media_codec_initialize_video_pvt: Invalid Direction");
    // raise event to APP for error (WRG PARAM & Invalid Direction)
    if(app_cb)
    {	
      app_cb( app_id,
        stream_id,
        QVP_RTP_MEDIA_CODEC_INIT,
        QVP_RTP_WRONG_PARAM,
        QVP_RTP_INV_DIR);

    }
    return status;
  }

  memset(&tCodecConfig, 0, sizeof(QP_VIDEO_CONFIG));

  codec_info_ctx.video_codec_info.app_id = app_id;
  codec_info_ctx.video_codec_info.stream_id = stream_id;
  qvp_rtp_memscpy(&codec_info_ctx.video_codec_info.video_codec_config,sizeof(qvp_rtp_video_codec_config),&media_config->video_codec_config,sizeof(qvp_rtp_video_codec_config));
  codec_info_ctx.video_codec_info.handle_media_res = handle_media_res;
  
  if ((direction == QVP_RTP_DIR_RX) || (direction == QVP_RTP_DIR_TXRX) )
  {
    if (media_config->video_codec_config.player_config.eCodec == QVP_RTP_PYLD_H264)
    {
      tCodecConfig.eCodec = VIDEO_CODEC_H264;
      // Directly typecasting between two enum as vlues are same between RTP enum and DPL enum
      tCodecConfig.sH264Info.eLevel = (QPE_VIDEO_H264_LEVEL)media_config->video_codec_config.player_config.h264_config.level_type;
      tCodecConfig.sH264Info.eProfile = (QPE_VIDEO_H264_PROFILE)media_config->video_codec_config.player_config.h264_config.profile_type;
    }
    else
    {
      QVP_RTP_ERR_1("qvp_rtp_media_codec_initialize_video_pvt: Invalid codec: %x",media_config->video_codec_config.player_config.eCodec);
      if( handle_media_res )
      {	
        handle_media_res( app_id,
                          stream_id,
                          QVP_RTP_MEDIA_CODEC_INIT,
                          status,
                          QVP_RTP_DIR_RX);

      }
      status = QVP_RTP_ERR_FATAL;
      return status;
    }

    tCodecConfig.eDeviceType = VIDEO_PLAYER;
    tCodecConfig.iBitRate = media_config->video_codec_config.player_config.iBitRate;
    tCodecConfig.iClockRate = media_config->video_codec_config.player_config.iClockRate;
    tCodecConfig.iFrameRate = media_config->video_codec_config.player_config.iFrameRate;
    tCodecConfig.iHeight = media_config->video_codec_config.player_config.iHeight;
    tCodecConfig.iWidth = media_config->video_codec_config.player_config.iWidth;
    tCodecConfig.bLipSyncEnable = media_config->video_codec_config.player_config.bLipSyncEnable;
    //Set the Lip Synch Parameters
    QVP_RTP_MSG_MED("API qvp_rtp_media_codec_initialize_video_pvt\
                    clock rate %d FD: %d", codec_info_ctx.audio_codec_info.audio_codec_config.iClockRate,
                     codec_info_ctx.audio_codec_info.audio_codec_config.iFrameDuration, 0);
    tCodecConfig.sLipSyncParam.iAudioClockRate = codec_info_ctx.audio_codec_info.audio_codec_config.iClockRate;
    tCodecConfig.sLipSyncParam.iAudPktInterval = codec_info_ctx.audio_codec_info.audio_codec_config.iFrameDuration;


    if (media_config->video_codec_config.player_config.pNALHeader && (media_config->video_codec_config.player_config.iNALHeaderLen > 0))
    {
      tCodecConfig.iNALHeaderLen = media_config->video_codec_config.player_config.iNALHeaderLen;
      tCodecConfig.pNALHeader = (QPCHAR*) qvp_malloc(sizeof(QPCHAR)*tCodecConfig.iNALHeaderLen +1);
      if(tCodecConfig.pNALHeader)
      {
        memset(tCodecConfig.pNALHeader, 0, tCodecConfig.iNALHeaderLen + 1);
        qvp_rtp_memscpy(tCodecConfig.pNALHeader, (tCodecConfig.iNALHeaderLen /*+ 1*/), media_config->video_codec_config.player_config.pNALHeader, tCodecConfig.iNALHeaderLen); 
      }
    }
    else
    {
      tCodecConfig.iNALHeaderLen = 0;
      tCodecConfig.pNALHeader    = NULL;
    }

    codec_info_ctx.video_codec_info.RxDesc = qpVideoCodecInitialize(qvp_rtp_codec_video_rx_cb, &codec_info_ctx, VIDEO_PLAYER, &tCodecConfig);

    if (codec_info_ctx.video_codec_info.RxDesc == QP_NULL)
    {
      *rx_state = QVP_RTP_CODEC_STATE_ERROR;

      if( handle_media_res )
      {	
        handle_media_res( app_id,
                          stream_id,
                          QVP_RTP_MEDIA_CODEC_INIT,
                          status,
                          QVP_RTP_DIR_RX);

        if(direction == QVP_RTP_DIR_TXRX)
        {
          handle_media_res( app_id,
            stream_id,
            QVP_RTP_MEDIA_CODEC_INIT,
            status,
            QVP_RTP_DIR_TX);
        }

      }
      QVP_RTP_MSG_LOW_2("qvp_rtp_media_codec_initialize_video_pvt : Method call to initialize VIDEO_PLAYER_RECORDER on %d app_id for %d stream_id failed !!!",
        app_id,
        stream_id);
      status = QVP_RTP_ERR_FATAL;
      return status;
    }

    eError = qpVideoCodecSet(codec_info_ctx.video_codec_info.RxDesc, tCodecConfig);
    if(eError != VIDEO_ERROR_OK)
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_initialize_video_pvt : Method call to video codec set failed ");
      //TBD: Should we change state here as later we won't know what was last state of player just in case if it was started or stopped
      codec_info_ctx.video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_ERROR;
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_CODEC_INIT,
          QVP_RTP_ERR_FATAL,
          QVP_RTP_DIR_RX);
      }
      if(direction == QVP_RTP_DIR_TXRX && handle_media_res)
      {
        handle_media_res( app_id,
          stream_id,
          QVP_RTP_MEDIA_CODEC_INIT,
          status,
          QVP_RTP_DIR_TX);
      }
      return QVP_RTP_ERR_FATAL;
    }

    *rx_state = QVP_RTP_CODEC_STATE_INITIALIZING;
    QVP_RTP_MSG_LOW_2("qvp_rtp_media_codec_initialize_video_pvt : Method call to initialize VIDEO_PLAYER_RECORDER on %d app_id for %d stream_id success !!!",
      app_id,
      stream_id);
    codec_info_ctx.video_codec_info.direction = direction;
    codec_info_ctx.video_codec_info.current_codec = media_config->video_codec_config.player_config.eCodec;
    status = QVP_RTP_SUCCESS;
  }

  if ((direction == QVP_RTP_DIR_TX) || (direction == QVP_RTP_DIR_TXRX) )
  {
    if (media_config->video_codec_config.recorder_config.eCodec == QVP_RTP_PYLD_H264)
    {
      tCodecConfig.eCodec = VIDEO_CODEC_H264;
      // Directly typecasting between two enum as vlues are same between RTP enum and DPL enum
      tCodecConfig.sH264Info.eLevel = (QPE_VIDEO_H264_LEVEL)media_config->video_codec_config.recorder_config.h264_config.level_type;
      tCodecConfig.sH264Info.eProfile = (QPE_VIDEO_H264_PROFILE)media_config->video_codec_config.recorder_config.h264_config.profile_type;
    }
    else
    {
      QVP_RTP_ERR_1("qvp_rtp_media_codec_initialize_video_pvt: Invalid codec: %x",media_config->video_codec_config.recorder_config.eCodec);
      if( handle_media_res )
      {	
        handle_media_res( app_id,
                          stream_id,
                          QVP_RTP_MEDIA_CODEC_INIT,
                          status,
                          QVP_RTP_DIR_TX);

      }
      status = QVP_RTP_ERR_FATAL;
      return status;
    }

    tCodecConfig.eDeviceType = VIDEO_RECORDER;
    tCodecConfig.iBitRate = media_config->video_codec_config.recorder_config.iBitRate;
    tCodecConfig.iClockRate = media_config->video_codec_config.recorder_config.iClockRate;
    tCodecConfig.iFrameRate = media_config->video_codec_config.recorder_config.iFrameRate;
    tCodecConfig.iHeight = media_config->video_codec_config.recorder_config.iHeight;
    tCodecConfig.iWidth = media_config->video_codec_config.recorder_config.iWidth;

    tCodecConfig.iNALHeaderLen = 0;
    tCodecConfig.pNALHeader    = NULL;


    codec_info_ctx.video_codec_info.TxDesc = qpVideoCodecInitialize(qvp_rtp_codec_video_tx_cb, &codec_info_ctx, VIDEO_RECORDER, &tCodecConfig);

    if (codec_info_ctx.video_codec_info.TxDesc == QP_NULL)
    {
      *tx_state = QVP_RTP_CODEC_STATE_ERROR;

      if( handle_media_res )
      {	
        handle_media_res( app_id,
                          stream_id,
                          QVP_RTP_MEDIA_CODEC_INIT,
                          status,
                          QVP_RTP_DIR_TX);

      }
      QVP_RTP_MSG_LOW_2("qvp_rtp_media_codec_initialize_video_pvt : Video RECORDER on %d app_id for %d stream_id failed !!!",
        app_id,
        stream_id);
      status = QVP_RTP_ERR_FATAL;
      return status;
    }

    eError = qpVideoCodecSet(codec_info_ctx.video_codec_info.TxDesc, tCodecConfig);
    if(eError != VIDEO_ERROR_OK)
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_initialize_video_pvt : Method call to video codec set failed ");
      //TBD: Should we change state here as later we won't know what was last state of player just in case if it was started or stopped
      codec_info_ctx.video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_ERROR;
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_CODEC_INIT,
          QVP_RTP_ERR_FATAL,
          QVP_RTP_DIR_TX);
      }
      return QVP_RTP_ERR_FATAL;
    }

    *tx_state = QVP_RTP_CODEC_STATE_INITIALIZING;
    codec_info_ctx.video_codec_info.current_codec = media_config->video_codec_config.recorder_config.eCodec;
    QVP_RTP_MSG_LOW_2("qvp_rtp_media_codec_initialize_video_pvt : Method call to initialize VIDEO_PLAYER_RECORDER on %d app_id for %d stream_id success !!!",
      app_id,
      stream_id);
    codec_info_ctx.video_codec_info.direction = direction;
    status = QVP_RTP_SUCCESS;
  }
  
  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_codec_configure_video_pvt

DESCRIPTION
This function configures media codec engine

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_codec_configure_video_pvt(
                                      qvp_rtp_app_id_type       app_id,      /* application which opened
                                                                              * the stream
                                                                              */
                                      qvp_rtp_stream_id_type    stream_id,    /* stream which was opened */
                                      qvp_rtp_media_config*     media_config,
                                      qvp_rtp_stream_direction  direction     /* Direction in which configuration
                                                                               * needs to be applied
                                                                               */
                                      )
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  QP_VIDEO_CONFIG     tCodecConfig;
  QPE_VIDEO_ERROR     eError;
  qvp_rtp_codec_state* codec_state = NULL;
  qvp_rtp_media_handle_response app_cb = codec_info_ctx.video_codec_info.handle_media_res;
  QP_VIDEO_DESC       rx_desc  = NULL;
  QP_VIDEO_DESC       tx_desc  = NULL;

  QVP_RTP_MSG_MED_2("API qvp_rtp_media_codec_configure_video_pvt entered\
                  with appid %d and stream id %d for media_type", app_id, stream_id);

  if ((direction == QVP_RTP_DIR_RX) || (direction == QVP_RTP_DIR_TXRX))
  {
    codec_state   = &codec_info_ctx.video_codec_info.video_rx_state;
    rx_desc  = codec_info_ctx.video_codec_info.RxDesc;
    if (rx_desc == NULL)
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_configure_video_pvt: Invalid Rx vid desc");
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_CODEC_CONFIG,
          status,
          QVP_RTP_DIR_RX);

      }
    }
    if (((!codec_state) && ( (*codec_state == QVP_RTP_CODEC_STATE_ERROR) || \
      (*codec_state == QVP_RTP_CODEC_STATE_NONE) || \
      (*codec_state == QVP_RTP_CODEC_STATE_INITIALIZING) || \
      (*codec_state == QVP_RTP_CODEC_STATE_CONFIGURING) || \
      (*codec_state == QVP_RTP_CODEC_STATE_STARTING) || \
      (*codec_state == QVP_RTP_CODEC_STATE_STOPPING) || \
      (*codec_state == QVP_RTP_CODEC_STATE_RELEASING))))
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_configure_video_pvt: Invalid codec state for Config");
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_CODEC_CONFIG,
          status,
          QVP_RTP_DIR_RX);

      }
    }
  }

  if ((direction == QVP_RTP_DIR_TX) || (direction == QVP_RTP_DIR_TXRX))
  {
    codec_state   = &codec_info_ctx.video_codec_info.video_tx_state;
    tx_desc  = codec_info_ctx.video_codec_info.TxDesc;
    if (tx_desc == NULL)
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_configure_video_pvt: Invalid Tx vid desc:");
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_CODEC_CONFIG,
          status,
          QVP_RTP_DIR_TX);

      }
    }
    if (((!codec_state) && ( (*codec_state == QVP_RTP_CODEC_STATE_ERROR) || \
      (*codec_state == QVP_RTP_CODEC_STATE_NONE) || \
      (*codec_state == QVP_RTP_CODEC_STATE_INITIALIZING) || \
      (*codec_state == QVP_RTP_CODEC_STATE_CONFIGURING) || \
      (*codec_state == QVP_RTP_CODEC_STATE_STARTING) || \
      (*codec_state == QVP_RTP_CODEC_STATE_STOPPING) || \
      (*codec_state == QVP_RTP_CODEC_STATE_RELEASING))))
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_configure_video_pvt: Invalid codec state for Config");
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_CODEC_CONFIG,
          status,
          QVP_RTP_DIR_TX);

      }
    }
  }

  if ((direction != QVP_RTP_DIR_TX) && (direction != QVP_RTP_DIR_RX) && (direction != QVP_RTP_DIR_TXRX))
  {
    QVP_RTP_ERR_0("qvp_rtp_media_codec_configure_video_pvt: Invalid Direction");
    // raise event to APP for error (WRG PARAM & Invalid Direction)
    if( app_cb )
    {	
      app_cb( app_id,
        stream_id,
        QVP_RTP_MEDIA_CODEC_CONFIG,
        QVP_RTP_WRONG_PARAM,
        QVP_RTP_INV_DIR);

    }
    return status;
  }

  memset(&tCodecConfig, 0, sizeof(QP_VIDEO_CONFIG));

  //Assuming rx and tx local descriptors are initialized properly according to direction check above
  if (rx_desc)
  {
    // Putting code for H.264 only as of now
    if (media_config->video_codec_config.player_config.eCodec == QVP_RTP_PYLD_H264)
    {
      tCodecConfig.eCodec = VIDEO_CODEC_H264;
      tCodecConfig.sH264Info.eLevel = (QPE_VIDEO_H264_LEVEL)media_config->video_codec_config.player_config.h264_config.level_type;
      tCodecConfig.sH264Info.eProfile = (QPE_VIDEO_H264_PROFILE)media_config->video_codec_config.player_config.h264_config.profile_type;
    }
    else
    {
      QVP_RTP_ERR_1("qvp_rtp_media_codec_configure_video_pvt: Invalid codec: %x",media_config->video_codec_config.player_config.eCodec);
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_CODEC_CONFIG,
          QVP_RTP_WRONG_PARAM,
          QVP_RTP_DIR_RX);
      }
    }

    tCodecConfig.eDeviceType = VIDEO_PLAYER;
    tCodecConfig.iBitRate = media_config->video_codec_config.player_config.iBitRate;
    tCodecConfig.iClockRate = media_config->video_codec_config.player_config.iClockRate;
    tCodecConfig.iFrameRate = media_config->video_codec_config.player_config.iFrameRate;
    tCodecConfig.iHeight = media_config->video_codec_config.player_config.iHeight;
    tCodecConfig.iWidth = media_config->video_codec_config.player_config.iWidth;
    tCodecConfig.bLipSyncEnable = media_config->video_codec_config.player_config.bLipSyncEnable;
    tCodecConfig.sLipSyncParam.iAudioClockRate = codec_info_ctx.audio_codec_info.audio_codec_config.iClockRate;
    tCodecConfig.sLipSyncParam.iAudPktInterval = codec_info_ctx.audio_codec_info.audio_codec_config.iFrameDuration;
    QVP_RTP_MSG_HIGH("Video codec rx params: Level = %d, Profile = %d, devicetype = %d",tCodecConfig.sH264Info.eLevel,tCodecConfig.sH264Info.eProfile,tCodecConfig.eDeviceType);
    QVP_RTP_MSG_HIGH("Video codec rx params: Bitrate = %d, clockrate = %d, framerate = %d",tCodecConfig.iBitRate,tCodecConfig.iClockRate,tCodecConfig.iFrameRate);
    QVP_RTP_MSG_HIGH("Video codec rx params: Height = %d, width = %d, LipsyncEnable = %d",tCodecConfig.iHeight,tCodecConfig.iWidth,tCodecConfig.bLipSyncEnable);
    QVP_RTP_MSG_HIGH_2("Video codec rx params: AudioClockRate  %d, AudPktInterval = %d", tCodecConfig.sLipSyncParam.iAudioClockRate, tCodecConfig.sLipSyncParam.iAudPktInterval);
    if (media_config->video_codec_config.player_config.pNALHeader && (media_config->video_codec_config.player_config.iNALHeaderLen > 0))
    {
      tCodecConfig.iNALHeaderLen = media_config->video_codec_config.player_config.iNALHeaderLen;
      tCodecConfig.pNALHeader = (QPCHAR*)qvp_malloc(sizeof(QPCHAR)*tCodecConfig.iNALHeaderLen + 1);
      if(tCodecConfig.pNALHeader)
      {	  
        int i = 0;
        for(i = 0; i<tCodecConfig.iNALHeaderLen; i++)
        {
          QVP_RTP_MSG_LOW_2("pNALHeader[%d] = %d",i,media_config->video_codec_config.player_config.pNALHeader[i]);
        }
        memset(tCodecConfig.pNALHeader, 0, tCodecConfig.iNALHeaderLen + 1);
        qvp_rtp_memscpy(tCodecConfig.pNALHeader, (tCodecConfig.iNALHeaderLen /*+ 1*/), media_config->video_codec_config.player_config.pNALHeader, tCodecConfig.iNALHeaderLen); 
      }		
    }
    else
    {
      QVP_RTP_MSG_HIGH_0("pNALHeader is Null........");
      tCodecConfig.iNALHeaderLen = 0;
      tCodecConfig.pNALHeader    = NULL;
    }
    eError = qpVideoCodecSet(rx_desc, tCodecConfig);
    if(eError != VIDEO_ERROR_OK)
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_configure_video_pvt : Method call to video codec set failed ");
      //TBD: Should we change state here as later we won't know what was last state of player just in case if it was started or stopped
      codec_info_ctx.video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_ERROR;
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_CODEC_CONFIG,
          QVP_RTP_ERR_FATAL,
          QVP_RTP_DIR_RX);
      }
	  qvp_free(tCodecConfig.pNALHeader);
      return QVP_RTP_ERR_FATAL;
    }
    if (codec_info_ctx.video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_STARTED)
    {
      codec_info_ctx.video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_RECONFIGURE;
    }
    else
    {
      codec_info_ctx.video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_CONFIGURING;
    }

    codec_info_ctx.video_codec_info.current_codec = media_config->video_codec_config.player_config.eCodec;
    status = QVP_RTP_SUCCESS;
	qvp_free(tCodecConfig.pNALHeader);

  }

  if (tx_desc)
  {
    // Putting code for H.264 only as of now
    if (media_config->video_codec_config.recorder_config.eCodec == QVP_RTP_PYLD_H264)
    {
      tCodecConfig.eCodec = VIDEO_CODEC_H264;
      tCodecConfig.sH264Info.eLevel = (QPE_VIDEO_H264_LEVEL)media_config->video_codec_config.recorder_config.h264_config.level_type;
      tCodecConfig.sH264Info.eProfile = (QPE_VIDEO_H264_PROFILE)media_config->video_codec_config.recorder_config.h264_config.profile_type;
    }
    else
    {
      QVP_RTP_ERR_1("qvp_rtp_media_codec_initialize_video_pvt: Invalid codec: %x",media_config->video_codec_config.recorder_config.eCodec);
      // raise event to APP for error
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_CODEC_CONFIG,
          QVP_RTP_WRONG_PARAM,
          QVP_RTP_DIR_TX);
      }
    }

    tCodecConfig.eDeviceType = VIDEO_RECORDER;
    tCodecConfig.iBitRate = media_config->video_codec_config.recorder_config.iBitRate;
    tCodecConfig.iClockRate = media_config->video_codec_config.recorder_config.iClockRate;
    tCodecConfig.iFrameRate = media_config->video_codec_config.recorder_config.iFrameRate;
    tCodecConfig.iHeight = media_config->video_codec_config.recorder_config.iHeight;
    tCodecConfig.iWidth = media_config->video_codec_config.recorder_config.iWidth;

    tCodecConfig.iNALHeaderLen = 0;
    tCodecConfig.pNALHeader    = NULL;
    
    QVP_RTP_MSG_HIGH("Video codec tx params: Level = %d, Profile = %d, devicetype = %d",tCodecConfig.sH264Info.eLevel,tCodecConfig.sH264Info.eProfile,tCodecConfig.eDeviceType);
    QVP_RTP_MSG_HIGH("Video codec tx params: Bitrate = %d, clockrate = %d, framerate = %d",tCodecConfig.iBitRate,tCodecConfig.iClockRate,tCodecConfig.iFrameRate);
    QVP_RTP_MSG_HIGH_2("Video codec tx params: Height = %d, width = %d",tCodecConfig.iHeight,tCodecConfig.iWidth);    
    eError = qpVideoCodecSet(tx_desc, tCodecConfig);
    if(eError != VIDEO_ERROR_OK)
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_configure_video_pvt : Method call to video codec set failed ");
      //TBD: Should we change state here as later we won't know what was last state of player just in case if it was started or stopped
      codec_info_ctx.video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_ERROR;
      // raise event to APP for error
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_CODEC_CONFIG,
          QVP_RTP_ERR_FATAL,
          QVP_RTP_DIR_TX);
      }
      return QVP_RTP_ERR_FATAL;
    }
    if (codec_info_ctx.video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_STARTED)
    {
      codec_info_ctx.video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_RECONFIGURE;
    }
    else
    {
      codec_info_ctx.video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_CONFIGURING;
    }

    codec_info_ctx.video_codec_info.current_codec = media_config->video_codec_config.recorder_config.eCodec;
    status = QVP_RTP_SUCCESS;
  }

  if (status == QVP_RTP_SUCCESS)
  {
    //Preseve the current direction mode so that it can be used at the time of handling Video CB and raising event
    codec_info_ctx.video_codec_info.direction = direction;
    codec_info_ctx.video_codec_info.stream_id = stream_id;
  }

  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_codec_release_video_pvt

DESCRIPTION
This function releases media codec engine

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_codec_release_video_pvt(
                              qvp_rtp_app_id_type    app_id           /* application which opened
                                                                       * the stream
                                                                       */
                              )
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  qvp_rtp_media_handle_response app_cb = codec_info_ctx.video_codec_info.handle_media_res;

  QVP_RTP_MSG_MED_1("API qvp_rtp_media_codec_release_video_pvt entered with appid %d", app_id);

  // Manipulate the codec context direction based on descriptors
  if ((codec_info_ctx.video_codec_info.TxDesc != NULL) && (codec_info_ctx.video_codec_info.RxDesc != NULL))
  {
    codec_info_ctx.video_codec_info.direction = QVP_RTP_DIR_TXRX;
  }
  else if (codec_info_ctx.video_codec_info.TxDesc != NULL)
  {
    codec_info_ctx.video_codec_info.direction = QVP_RTP_DIR_TX;
  }
  else if (codec_info_ctx.video_codec_info.RxDesc != NULL)
  {
    codec_info_ctx.video_codec_info.direction = QVP_RTP_DIR_RX;
  }
  else
  {
    QVP_RTP_ERR_0("qvp_rtp_media_codec_release_video_pvt : Both Desc NULL ");
    //TBD: raise Error EVT to APP
    if( app_cb )
    {	
      app_cb( app_id,
        NULL,
        QVP_RTP_MEDIA_VIDEO_CODEC_RELEASE,
        QVP_RTP_ERR_FATAL,
        QVP_RTP_DIR_TXRX);
    }
    return status;
  }

  if (((codec_info_ctx.video_codec_info.direction == QVP_RTP_DIR_TX) || \
       (codec_info_ctx.video_codec_info.direction == QVP_RTP_DIR_TXRX)) && \
      (codec_info_ctx.video_codec_info.TxDesc != NULL))
  {
    if ( (codec_info_ctx.video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_INITDONE) || \
      (codec_info_ctx.video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_CONFIGUREDONE))
    {
      qpVideoCodecUnInitialize(codec_info_ctx.video_codec_info.TxDesc, VIDEO_RECORDER);
      codec_info_ctx.video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_RELEASING;
      status = QVP_RTP_SUCCESS;

    }
    else
    {
      QVP_RTP_ERR_1("qvp_rtp_media_codec_release_video_pvt : failed: TX State: %d ", codec_info_ctx.video_codec_info.video_tx_state);
      //raise event to APP as release is called in wrg state
      if( app_cb )
      {	
        app_cb( app_id,
          NULL,
          QVP_RTP_MEDIA_VIDEO_CODEC_RELEASE,
          QVP_RTP_ERR_FATAL,
          QVP_RTP_DIR_TX);
      }
    }
  }

  if (((codec_info_ctx.video_codec_info.direction == QVP_RTP_DIR_RX) || \
       (codec_info_ctx.video_codec_info.direction == QVP_RTP_DIR_TXRX)) && \
      (codec_info_ctx.video_codec_info.TxDesc != NULL))
  {
    if ( (codec_info_ctx.video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_INITDONE) || \
         (codec_info_ctx.video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_CONFIGUREDONE))
    {

      qpVideoCodecUnInitialize(codec_info_ctx.video_codec_info.RxDesc, VIDEO_PLAYER);
      codec_info_ctx.video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_RELEASING;
      status = QVP_RTP_SUCCESS;

    }
    else
    {
      QVP_RTP_ERR_1("qvp_rtp_media_codec_release_video_pvt : failed: RX State: %d ", codec_info_ctx.video_codec_info.video_rx_state);
      //raise event to APP as release is called in wrg state
      if( app_cb )
      {	
        app_cb( app_id,
          NULL,
          QVP_RTP_MEDIA_VIDEO_CODEC_RELEASE,
          QVP_RTP_ERR_FATAL,
          QVP_RTP_DIR_RX);
      }
    }
  }

  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_stream_start_video_pvt

DESCRIPTION
This function starts stream associated with stream id

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_stream_start_video_pvt
(
                                     qvp_rtp_app_id_type    app_id,      /* application which opened
                                                                          * the stream
                                                                          */
                                     qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
                                     qvp_rtp_stream_direction   direction
                                     )
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  QPE_VIDEO_ERROR     eError = VIDEO_ERROR_OK;
  qvp_rtp_media_handle_response app_cb = codec_info_ctx.video_codec_info.handle_media_res;

  QVP_RTP_MSG_MED_1("API qvp_rtp_media_stream_start_pvt entered with appid %d", app_id);

  if ((direction != QVP_RTP_DIR_TX) && (direction != QVP_RTP_DIR_RX) && (direction != QVP_RTP_DIR_TXRX))
  {
    QVP_RTP_ERR_1("qvp_rtp_media_codec_configure_video_pvt: Invalid Direction: %d",direction);
    // raise event to APP for error (WRG PARAM & Invalid Direction)
    if( app_cb )
    {	
      app_cb( app_id,
        stream_id,
        QVP_RTP_MEDIA_START_STREAM,
        QVP_RTP_WRONG_PARAM,
        QVP_RTP_INV_DIR);

    }
    return status;
  }

  if ((direction == QVP_RTP_DIR_TX) || (direction == QVP_RTP_DIR_TXRX))
  {
    if ( (codec_info_ctx.video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_INITDONE) || \
         (codec_info_ctx.video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_CONFIGUREDONE))
    {
      if (codec_info_ctx.video_codec_info.TxDesc != NULL)
      {
        eError = qpVideoRecordStart(codec_info_ctx.video_codec_info.TxDesc, 1);
      }
      else
      { 
        codec_info_ctx.video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_NONE;
        QVP_RTP_ERR_0("qvp_rtp_media_stream_start_pvt : Desc NULL ");
        if( app_cb )
        {	
          app_cb( app_id,
            stream_id,
            QVP_RTP_MEDIA_START_STREAM,
            QVP_RTP_ERR_FATAL,
            QVP_RTP_DIR_TX);

        }
      }

      if (eError != VIDEO_ERROR_OK)
      {
        codec_info_ctx.video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_INITDONE;
        QVP_RTP_ERR_0("qvp_rtp_media_stream_start_pvt : Recorder start failed ");
        if( app_cb )
        {	
          app_cb( app_id,
            stream_id,
            QVP_RTP_MEDIA_START_STREAM,
            QVP_RTP_ERR_FATAL,
            QVP_RTP_DIR_TX);

        }
      }
      else
      {
        codec_info_ctx.video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_STARTING;
        codec_info_ctx.video_codec_info.direction = direction;
        status = QVP_RTP_SUCCESS;
      }
    }
    else
    {
      QVP_RTP_ERR_1("qvp_rtp_media_stream_start_pvt : TX failed: State: %d ", codec_info_ctx.video_codec_info.video_tx_state);
      //raise event to APP as release is called in wrg state
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_START_STREAM,
          QVP_RTP_ERR_FATAL,
          QVP_RTP_DIR_TX);

      }
    }
  }

  if ((direction == QVP_RTP_DIR_RX) || (direction == QVP_RTP_DIR_TXRX))
  {
    if ( (codec_info_ctx.video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_INITDONE) || \
         (codec_info_ctx.video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_CONFIGUREDONE))
    {
      if (codec_info_ctx.video_codec_info.RxDesc != NULL)
      {
        eError = qpVideoPlayStart(codec_info_ctx.video_codec_info.RxDesc);
      }
      else
      { 
        codec_info_ctx.video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_NONE;
        QVP_RTP_ERR_0("qvp_rtp_media_stream_start_pvt : Desc NULL ");
        if( app_cb )
        {	
          app_cb( app_id,
            stream_id,
            QVP_RTP_MEDIA_START_STREAM,
            QVP_RTP_WRONG_PARAM,
            QVP_RTP_DIR_RX);

        }
      }

      if (eError != VIDEO_ERROR_OK)
      {
        codec_info_ctx.video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_INITDONE;
        QVP_RTP_ERR_0("qvp_rtp_media_stream_start_pvt : Player start failed ");
        if( app_cb )
        {	
          app_cb( app_id,
            stream_id,
            QVP_RTP_MEDIA_START_STREAM,
            QVP_RTP_ERR_FATAL,
            QVP_RTP_DIR_RX);

        }
      }
      else
      {
        codec_info_ctx.video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_STARTING;
        codec_info_ctx.video_codec_info.direction = direction;
        status = QVP_RTP_SUCCESS;
      }
    }
    else
    {
      QVP_RTP_ERR_1("qvp_rtp_media_stream_start_pvt : RX failed: State: %d ",codec_info_ctx.video_codec_info.video_rx_state);
      //TBD: raise event to APP as release is called in wrg state
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_START_STREAM,
          QVP_RTP_ERR_FATAL,
          QVP_RTP_DIR_RX);

      }
    }
  }

  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_media_stream_stop_video_pvt

DESCRIPTION
This function stops stream associated with stream id

DEPENDENCIES
None

RETURN VALUE
QVP_RTP_SUCESS or error code

SIDE EFFECTS
None
===========================================================================*/
qvp_rtp_status_type qvp_rtp_media_stream_stop_video_pvt
(
                                     qvp_rtp_app_id_type    app_id,      /* application which opened
                                                                          * the stream
                                                                          */
                                     qvp_rtp_stream_id_type  stream_id,   /* stream which was opened */
                                     qvp_rtp_stream_direction   direction
                                     )
{
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
  QPE_VIDEO_ERROR     eError = VIDEO_ERROR_OK;
  qvp_rtp_media_handle_response app_cb = codec_info_ctx.video_codec_info.handle_media_res;

  QVP_RTP_MSG_MED_1("API qvp_rtp_media_stream_stop_video_pvt entered with appid %d", app_id);

  //TBD: Is it possible for APP to do Init and Config with RXTX but Start with just RX or TX DIR?

  if ((direction != QVP_RTP_DIR_TX) && (direction != QVP_RTP_DIR_RX) && (direction != QVP_RTP_DIR_TXRX))
  {
    QVP_RTP_ERR_1("qvp_rtp_media_stream_stop_video_pvt: Invalid Direction: %d",direction);
    // raise event to APP for error (WRG PARAM & Invalid Direction)
    if( app_cb )
    {	
      app_cb( app_id,
        stream_id,
        QVP_RTP_MEDIA_STOP_STREAM,
        QVP_RTP_WRONG_PARAM,
        QVP_RTP_INV_DIR);

    }
    return status;
  }

  if ((direction == QVP_RTP_DIR_TX) || (direction == QVP_RTP_DIR_TXRX))
  {
    if (codec_info_ctx.video_codec_info.video_tx_state == QVP_RTP_CODEC_STATE_STARTED)
    {
      if (codec_info_ctx.video_codec_info.TxDesc != NULL)
      {
        eError = qpVideoRecordStop(codec_info_ctx.video_codec_info.TxDesc);
      }
      else
      { 
        codec_info_ctx.video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_NONE;
        QVP_RTP_ERR_0("qvp_rtp_media_stream_stop_video_pvt : Desc NULL ");
        if( app_cb )
        {	
          app_cb( app_id,
            stream_id,
            QVP_RTP_MEDIA_STOP_STREAM,
            QVP_RTP_WRONG_PARAM,
            QVP_RTP_DIR_TX);

        }
      }

      if (eError != VIDEO_ERROR_OK)
      {
        codec_info_ctx.video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_INITDONE;
        QVP_RTP_ERR_0("qvp_rtp_media_stream_stop_video_pvt : Recorder stop failed ");
        if( app_cb )
        {	
          app_cb( app_id,
            stream_id,
            QVP_RTP_MEDIA_STOP_STREAM,
            QVP_RTP_ERR_FATAL,
            QVP_RTP_DIR_TX);

        }
      }
      else
      {
        codec_info_ctx.video_codec_info.video_tx_state = QVP_RTP_CODEC_STATE_STOPPING;
        codec_info_ctx.video_codec_info.direction = direction;
        status = QVP_RTP_SUCCESS;
      }
    }
    else
    {
      QVP_RTP_ERR_1("qvp_rtp_media_stream_stop_video_pvt : failed: State: %d ", codec_info_ctx.video_codec_info.video_tx_state);
      //raise event to APP as release is called in wrg state
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_STOP_STREAM,
          QVP_RTP_ERR_FATAL,
          QVP_RTP_DIR_TX);

      }
    }
  }

  if ((direction == QVP_RTP_DIR_RX) || (direction == QVP_RTP_DIR_TXRX))
  {
    if (codec_info_ctx.video_codec_info.video_rx_state == QVP_RTP_CODEC_STATE_STARTED)
    {
      if (codec_info_ctx.video_codec_info.RxDesc != NULL)
      {
        eError = qpVideoPlayStop(codec_info_ctx.video_codec_info.RxDesc, TRUE);
      }
      else
      { 
        codec_info_ctx.video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_NONE;
        QVP_RTP_ERR_0("qvp_rtp_media_stream_stop_video_pvt : Desc NULL ");
        if( app_cb )
        {	
          app_cb( app_id,
            stream_id,
            QVP_RTP_MEDIA_STOP_STREAM,
            QVP_RTP_WRONG_PARAM,
            QVP_RTP_DIR_RX);

        }
      }

      if (eError != VIDEO_ERROR_OK)
      {
        codec_info_ctx.video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_INITDONE;
        QVP_RTP_ERR_0("qvp_rtp_media_stream_stop_video_pvt : Player stop failed ");
        if( app_cb )
        {	
          app_cb( app_id,
            stream_id,
            QVP_RTP_MEDIA_STOP_STREAM,
            QVP_RTP_ERR_FATAL,
            QVP_RTP_DIR_RX);

        }
      }
      else
      {
        codec_info_ctx.video_codec_info.video_rx_state = QVP_RTP_CODEC_STATE_STOPPING;
        codec_info_ctx.video_codec_info.direction = direction;
        status = QVP_RTP_SUCCESS;
      }
    }
    else
    {
      QVP_RTP_ERR_0("qvp_rtp_media_stream_stop_video_pvt : player stop failed ");
      //raise event to APP as release is called in wrg state
      if( app_cb )
      {	
        app_cb( app_id,
          stream_id,
          QVP_RTP_MEDIA_STOP_STREAM,
          QVP_RTP_ERR_FATAL,
          QVP_RTP_DIR_RX);

      }
    }
  }
  
  return status;
}

/*===========================================================================
FUNCTION qvp_rtp_handle_incoming_vid_pkt_cb_pvt

DESCRIPTION
This function handlesincomming rtp video packets and calls play start

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
void qvp_rtp_handle_incoming_vid_pkt_cb_pvt
(
 qvp_rtp_app_id_type     app_id,      /* RTP Application ID*/
 qvp_rtp_stream_id_type   stream_id,    /* stream id through which the
                                        * packet came
                                        */
 qvp_rtp_recv_pkt_type    *pkt       /* packet which is being delivered */
)
{
  QP_MULTIMEDIA_FRAME	 frame;
  QP_VIDEO_DESC       vidDesc = NULL;
  QPE_VIDEO_ERROR eError = VIDEO_ERROR_OK;

  if(pkt == QP_NULL )
  {
    QVP_RTP_ERR_0("qvp_rtp_handle_incoming_pkt_cb: Received NULL Packet from DS!!");
    return;
  }
  QVP_RTP_MSG_LOW("qvp_rtp_handle_incoming_pkt_cb: Rcvd RTP packet len = %d, seq = %u, t = %u",
    pkt->len, pkt->seq, pkt->tstamp );
  if(codec_info_ctx.video_codec_info.video_rx_state != QVP_RTP_CODEC_STATE_STARTED)
  {
    QVP_RTP_MSG_LOW_1("qvp_rtp_handle_incoming_pkt_cb: Dropping packets as we are in wrong state %d....!!",codec_info_ctx.video_codec_info.video_rx_state);
    return;    
  }
  if (codec_info_ctx.video_codec_info.stream_id != stream_id )
  {
    QVP_RTP_ERR_2("qvp_rtp_handle_incoming_pkt_cb: Incoming stream does not match active id (%d,%d)!", codec_info_ctx.video_codec_info.stream_id,
      stream_id);
    return;
  }
  if (codec_info_ctx.video_codec_info.current_codec != QVP_RTP_PYLD_H264)
  {
    QVP_RTP_ERR_1("qvp_rtp_handle_incoming_pkt_cb: current_codec is invalid = %d", codec_info_ctx.video_codec_info.current_codec);
    return;
  }

  if (codec_info_ctx.video_codec_info.current_codec != QVP_RTP_PYLD_H263)
  {
	  QVP_RTP_ERR_1("qvp_rtp_handle_incoming_pkt_cb: current_codec is invalid = %d", codec_info_ctx.video_codec_info.current_codec);
	  return;
  }

  frame.sMediaPacketInfo.sMediaPktInfoRx.iTimeStamp = pkt->tstamp;
  frame.sMediaPacketInfo.sMediaPktInfoRx.iSeqNum = pkt->seq;
  frame.sMediaPacketInfo.sMediaPktInfoRx.bMarkerBit = pkt->marker_bit;
  frame.sMediaPacketInfo.sMediaPktInfoRx.iPayloadType = VIDEO_CODEC_H264;
  
  frame.pData = pkt->data;

  frame.iDataLen = pkt->len;
  frame.iMaxBuffLen = pkt->len;

  vidDesc = codec_info_ctx.video_codec_info.RxDesc;
  if(vidDesc == NULL)
  {
    QVP_RTP_ERR_0("qvp_rtp_handle_incoming_pkt_cb: codec descriptor.");
    return ;
  }

  eError = qpVideoPlayFrame(vidDesc, &frame);
  if(eError == VIDEO_ERROR_OK)
  {
    QVP_RTP_MSG_LOW_0("qvp_rtp_handle_incoming_pkt_cb : Call to video play frame success.");
  }
  else
  {
    QVP_RTP_ERR_0("qvp_rtp_handle_incoming_pkt_cb : Call to video play frame failed.");
  }

}
void qvp_rtp_handle_video_rtcp_cb(
  qvp_rtp_app_id_type      app_id,    /* RTP Application ID*/
  qvp_rtp_stream_id_type   stream,    /* stream id to which the rtcp info belongs */
  qvp_rtcp_session_info_type *rtcp_info, /* RTCP info propogated to app */
  qvp_rtcp_local_param_type  *rtcp_stats,  /* RTP Local statistics */
  qvp_rtp_ip_ver_type       ip_ver_type 
  )
{
  QP_VIDEO_DESC       vidDesc = NULL;
  QPE_VIDEO_ERROR eError = VIDEO_ERROR_OK;
	uint32																		TMMBR_VALUE = 0;
  dword                                     hi_ntp; // the value for the high 32 bits 
  dword                                     lo_ntp; // the value for the high 32 bits 
  uint32                                     net_bitrate = 0;
  vidDesc = codec_info_ctx.video_codec_info.RxDesc;
  if(vidDesc == NULL)
  {
    QVP_RTP_ERR_0("qvp_rtp_handle_video_rtcp_cb: codec descriptor.");
    return ;
  }
  QVP_RTP_MSG_LOW_2("qvp_rtp_handle_video_rtcp_cb: app_id = %d, stream = %d",
    app_id, stream);
  hi_ntp = ( rtcp_info->info.sr.ntp >> 32);
  lo_ntp = (uint32)( rtcp_info->info.sr.ntp );
  if(rtcp_info->inf_type == QVP_RTCP_SR){
    eError = qpDplVideoReport(vidDesc, hi_ntp, lo_ntp, rtcp_info->info.sr.tstamp);
    if(eError != VIDEO_ERROR_OK)
    {
      QVP_RTP_ERR_0("qvp_rtp_handle_video_rtcp_cb : Method call to qpDplVideoReport failed ");
    }
  }
    else if(rtcp_info->inf_type == QVP_RTCP_PSFB){
      eError = qpDplVideoGenerateH264IdrFrame(vidDesc);
    if(eError != VIDEO_ERROR_OK)
    {
      QVP_RTP_ERR_0("qvp_rtp_handle_video_rtcp_cb : Method call to qpDplVideoReport failed ");
    }
  }
  else if(rtcp_info->inf_type == QVP_RTCP_RTPFB){
    if(rtcp_info->info.tp_fb.fmt == QVP_RTCP_TRANS_FB_TMMBR){

      TMMBR_VALUE = rtcp_info->info.tp_fb.fci.tmmbr_list.mxtbr_mantissa << \
	                           rtcp_info->info.tp_fb.fci.tmmbr_list.mxtbr_expo;
      // need to check the profile and than update the bitrate.
      if(TMMBR_VALUE)
      {
          if(codec_info_ctx.video_codec_info.video_codec_config.recorder_config.h264_config.level_type == QVP_RTP_AVC_LEVEL1)
          {
             if(TMMBR_VALUE <= 64000)
             {
 	        codec_info_ctx.video_codec_info.video_codec_config.recorder_config.iBitRate = TMMBR_VALUE;
             }
             else
             {
 	        QVP_RTP_ERR_0("qvp_rtp_handle_video_rtcp_cb : bitrate received in tmmbr is above the max value supported for QVP_RTP_AVC_LEVEL1 ");
 	        return;
             }
          }
          else if(codec_info_ctx.video_codec_info.video_codec_config.recorder_config.h264_config.level_type == QVP_RTP_AVC_LEVEL1B)
          {
             if(TMMBR_VALUE <= 128000)
             {
 	        codec_info_ctx.video_codec_info.video_codec_config.recorder_config.iBitRate = TMMBR_VALUE;
             }
             else
             {
 	        QVP_RTP_ERR_0("qvp_rtp_handle_video_rtcp_cb : bitrate received in tmmbr is above the max value supported for QVP_RTP_AVC_LEVEL1B ");
 	        return;
             }
          }
          else if(codec_info_ctx.video_codec_info.video_codec_config.recorder_config.h264_config.level_type == QVP_RTP_AVC_LEVEL11)
          {
             if(TMMBR_VALUE <= 192000)
             {
 	        codec_info_ctx.video_codec_info.video_codec_config.recorder_config.iBitRate = TMMBR_VALUE;
             }
             else
             {
 	        QVP_RTP_ERR_0("qvp_rtp_handle_video_rtcp_cb : bitrate received in tmmbr is above the max value supported for QVP_RTP_AVC_LEVEL11 ");
 	        return;
             }
          }
          else if(codec_info_ctx.video_codec_info.video_codec_config.recorder_config.h264_config.level_type == QVP_RTP_AVC_LEVEL12)
          {
             if(TMMBR_VALUE <= 384000)
             {
 	        codec_info_ctx.video_codec_info.video_codec_config.recorder_config.iBitRate = TMMBR_VALUE;
             }
             else
             {
 	        QVP_RTP_ERR_0("qvp_rtp_handle_video_rtcp_cb : bitrate received in tmmbr is above the max value supported for QVP_RTP_AVC_LEVEL12 ");
 	        return;
             }
          }
          else if(codec_info_ctx.video_codec_info.video_codec_config.recorder_config.h264_config.level_type == QVP_RTP_AVC_LEVEL13)
          {
             if(TMMBR_VALUE <= 768000)
             {
 	        codec_info_ctx.video_codec_info.video_codec_config.recorder_config.iBitRate = TMMBR_VALUE;
             }
             else
             {
 	        QVP_RTP_ERR_0("qvp_rtp_handle_video_rtcp_cb : bitrate received in tmmbr is above the max value supported for QVP_RTP_AVC_LEVEL13 ");
 	        return;
             }
          }
      }
      else
      {
          QVP_RTP_ERR_0("qvp_rtp_handle_video_rtcp_cb : bitrate received in tmmbr is 0 ");
          return;
      }	
      if(ip_ver_type == QVP_RTP_IPV4 )
      {
          if(QVP_RTCP_IPV4_OVERHEAD > rtcp_info->info.tp_fb.fci.tmmbr_list.mxtbr_overhead)
          {
 	     net_bitrate = TMMBR_VALUE - (codec_info_ctx.video_codec_info.video_codec_config.recorder_config.iFrameRate* QVP_RTCP_IPV4_OVERHEAD * 8);
          }
          else
          {
 	     net_bitrate = TMMBR_VALUE - (codec_info_ctx.video_codec_info.video_codec_config.recorder_config.iFrameRate * \
									rtcp_info->info.tp_fb.fci.tmmbr_list.mxtbr_overhead * 8);
          }
      }
      else if(ip_ver_type == QVP_RTP_IPV6 )
      {
          if(QVP_RTCP_IPV6_OVERHEAD > rtcp_info->info.tp_fb.fci.tmmbr_list.mxtbr_overhead)
          {
 	     net_bitrate = TMMBR_VALUE - (codec_info_ctx.video_codec_info.video_codec_config.recorder_config.iFrameRate* QVP_RTCP_IPV6_OVERHEAD * 8);
          }
          else
          {
	     net_bitrate = TMMBR_VALUE - (codec_info_ctx.video_codec_info.video_codec_config.recorder_config.iFrameRate * \
																	rtcp_info->info.tp_fb.fci.tmmbr_list.mxtbr_overhead * 8);
          }
      }

      QVP_RTP_MSG_HIGH_1("qvp_rtp_handle_video_rtcp_cb : Netbitrate calculated %d", net_bitrate);
      eError = qpDplVideoAdaptVideoBitRate(vidDesc,net_bitrate);
      if(eError != VIDEO_ERROR_OK)
      {
         QVP_RTP_ERR_0("qvp_rtp_handle_video_rtcp_cb : Method call to qpDplVideoReport failed ");
      }
    }
    if(rtcp_info->info.tp_fb.fmt == QVP_RTCP_TRANS_FB_TMMBN){

			/*need to check wht to do*/
      if(eError != VIDEO_ERROR_OK)
      {
         QVP_RTP_ERR_0("qvp_rtp_handle_video_rtcp_cb : Method call to qpDplVideoReport failed ");
      }
    }
  }
}
#endif /* end of FEATURE_QVPHONE_RTP */
