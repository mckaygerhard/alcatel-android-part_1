﻿/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                         QVP_RTP_APP_CMD . C

GENERAL DESCRIPTION

  This file contains extrnalized functions and implementation of command
  processing from the application. Appropriate call backs are called
  after execution of each command.

EXTERNALIZED FUNCTIONS

  qvp_rtp_process_app_cmd

    Process application messages by dispatching it to sub handlers. A
    call back is invoked for every message.

  qvp_rtp_pull_pkt_cmd

    Pulls a packet from inbound jitter buffer. This is a command which 
    is executed synchronously.

   

INITIALIZATION AND SEQUENCING REQUIREMENTS

  None of the externalized functions will function before the task is 
  spawned.


  Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==*=*/

/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/ps_media/rtp_core/src/qvp_rtp_app_cmd.c#7 $ $DateTime: 2016/08/01 11:30:44 $ $Author: pbyri $

                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
11-02-16   nik     CR 970834
20 Jun 15  pradeepg CR#:924154 - 8996-TH20- Jitter calculation by DUT is wrong 
19-10-15  vailani  CR910022 - no DL DTMF tone hear
25/09/15   vailani CR874293 - baseband crash during a TTY call 
27/07/15   vailani CR855666 - P1 kw error
23-07-15   vailani CR840018 - No audio noticed during G711 call
30/06/15   vailani CR857711 - Gap between events is not coming as 640 RTP timestamps
24/06/15   vailani CR844680 - G711 Interface changes which was bought in
23/06/15    Manju  CR768442  - FR 25100: DTMF event reception feature - Media changes
11-06-15  pradeepg CR789244    - FR 25455: Internal - Improvements to WiFi Interface selection criteria, Media metrics
08-06-15   vailani CR840427 - TTY call swap failing
25-03-15   Manju   CR807266 - DTMF option2 change for IMS media for non-deferrable option
13-08-13   vailani CR#: 518371 - Use secapi_get_random() for generating random numbers. 
26-07-13   vailani CR#: 484798 - SRTP Feature support
24/07/13   vailani  CR#: 513217 - support for ATT log packet 0x17F2
03/11/09    SG     Starting and stopping house keeping timer only when there is session
03/08/09    SG     G722 Support
26/06/09    SG     Changed the validate function to check pyld and Chdir only when valid
03/09/09    apr    Adding support for DSCP GAN feature
02/09/09   sujitd Implemented RIM requirement. 
                  1) changes done in qvp_rtp_init_cmd()
                  2) added qvp_rtp_configure_session_cmd() and added corresponding 
                  QVP_RTP_CONFIG_SESSION_CMD enum and changes in qvp_rtp_process_app_cmd()
07/01/09    sha    Changes for eHRPD cdma network profile id 
03/05/07    apr   Support for IPV6 - using a common data type for
                  IPV6 & IP4 addresses
12/04/08    mc    Fixing RVCT compile warnings for 7K
10/03/08    apr   KW critical warning fixes
08/08/08    mc    Make RTP logging independent from LOGGING module
06/25/08    mc    KW critical warning fixes
16/04/08    apr   KW critical warning fixes
02/05/08    apr   Fix in RTP config, for RX params
01/20/08    apr   Fixes from mainline - 
                  enable lipsync - always give callback
                  Call RTCP funcs only if RTCP hdl is valid
01/22/08    rk    Changes for call hold/resume
12/26/07    apr   Support for EVRC-WB
10/19/07    grk   Support for in-call DTMF as per RFC 4733.
10/26/07    rk    QDJ & VJB integration. Cleanup
10/18/07    rk    Changes for video JB reassembly and delay tail estimation
10/10/07    rk    Changes for QDJ integration
01/18/08    apr   Fix in configure during cleanup of ib socket
11/01/08    grk   Fix for CR 134629-fixes bugs in DTMF payload packing.
12/11/07    apr   Added code to propogate status of lipsync 
                  enable/disable in all cases
11/12/07    apr   Open RTCP only when required/enabled, 
                  use adaptive_jb flag from stream params
07/11/07    apr   Fix for adjb init
08/26/07    rkk   Support for disable LS
07/05/07    apr   Modified to fix 7200 warnings
06/25/07    apr   Fix for CR121528,CR122448 
06/14/07    apr   Code cleanup in handle_nw_packet
02/14/07    apr   Added code for setting jitter size in profile config
                  used to set output mode in profile
05/04/07    grk   Removed banned API's
04/24/07    grk   Removed banned API's
03/15/07    apr   Fix for CR114342 nw_init for allocation of right 
                  number of socket resources
02/28/07    grk   Modified handling of odd RTP port.(RFC 3550)
02/28/07    grk   Added support for channel upgrade/downgrade.
02/01/07    uma   Removed checks for connection ip
02/01/07    apr   Support for Adaptive De-Jitter Buffer
02/01/07    uma   Made connection IP optional while validating config
12/14/06    apr   Support for clock skew estimation & compensation
12/12/06    uma   Added rtcp tx enabled check before setting RTCP o/b port
                  in qvp_rtp_alloc_open_rtcp
11/22/06    uma   Addition of qos and shared handle during open
10/31/06    grk   Added valid user check in qvp_rtp_deregister_app_cmd().
10/31/06    grk   Removed function calls to set tx payload type 
                  during configure.
10/31/06    grk   Added qvp_rtp_open2_cmd().
10/31/06    grk   Addition of rtcp shutdown in qvp_rtp_shutdown_cmd().
10/31/06    grk   Reordering of arguments for nw layer primitives.
10/31/06    grk   Modification to pass appid in all callbacks.
10/25/06    uma   Fix for socket2 - obtaining iface through rtp open
                  Removing sock interface & having only sock2
10/20/06    uma   Support for socket2 interface FEATURE_QVP_RTP_SOCK2
10/17/06    grk   Fixed the check for valid ob stream in 
                  qvp_rtp_validate_stream_configure().
10/13/06    apr   Fix for CR102349 on RTP house keeping timer - Enable HK  
                  timer only when adaptive jitter buffer is used.
09/27/06    grk   Fix for condition check in qvp_rtp_alloc_open_stream()
09/27/06    grk   Fix for calling QOS function calls only for o/b streams
09/27/06    grk   Fixed the assignment of outbound local port to stream in 
                  qvp_rtp_alloc_open_stream()
08/31/06    grk   Added support for configuring RTCP outbound local port
08/31/06    grk   Added support for port reservation.
08/30/06    Uma   Added max jitter size.check in qvp_rtp_alloc_jitter_buffer
08/14/06    uma   Locked stream resources under critical section
07/13/06    uma   Added new functions for validating payload configuration 
07/24/06    grk   Added support for configuring jitter size
                  Modified qvp_rtp_configure_stream(),
                  Added qvp_rtp_configure_jitter_size().
05/24/06    apr   Added new payload support for H263_2000, H263_1998
                  in function qvp_rtp_get_nw_priority
06/21/06    uma   Support for EVRC0, EVRCB0, EVRCB, EVRC1, EVRCB1 & DTX
05/31/06    xz    Remvoed compiler warning for 7K
05/05/06    apr   Including RTP code in feature FEATURE_QVPHONE_RTP
02/21/06    apr   Modified qvp_rtp_handle_rtcp_info to give callback 
                  handle_rtcp_info to application
03/17/06    uma   Modified qvp_rtp_handle_nw_packet to reverse logging code
03/20/06    uma   Replaced all malloc and free with wrappers 
                  qvp_rtp_malloc & qvp_rtp_free 
02/28/06    uma   Modified qvp_rtp_get_nw_priority, qvp_rtp_alloc_open_stream
                  for DTMF support
02/27/06    uma   Modified qvp_rtp_send_cmd due to change in
                  qvp_rtp_send_msg_type
02/08/06    uma   Modifications due to change in rtcp data types
01/02/06    uma   Modified qvp_rtp_register_cmd to do RTP initialization
01/24/06    uma   Modified qvp_rtp_configure_stream,
                  qvp_rtp_alloc_open_stream for TOS support
12/26/05    uma   Modified qvp_rtp_configure_stream for PacketShaping support
12/04/05    srk   Added code to activate inboud stream socket on config
                  Fixed compile errors on removed fields on the qos 
                  plane. Fixes the integration issue on repeated calls.
11/18/05    uma   Added code to activate outbound stream socket on config
11/18/05    uma   Added code to reset inbound stream socket on stream reset
11/18/05    apr   RTP open cmd returns network interface app id through
                  open callback.
10/10/05    apr   Modifications to remove coverity warnings
09/06/05    srk   Added a support to reset session API.
08/15/05    srk   Added Queue occupancy poll API for Encoder rate control
07/26/05    srk   Added separate params in open for RTCP config
07/12/05    srk   Added support for 1) started cleaning
                  up hk timer in shutdown. 2) Added check for hk timer 
                  firing in the middle of initi or in any uninit state 
07/05/05    srk   Added the 1) housekeeping timer and 
                  2) adaptive dejitter
07/01/05    srk   Added check for profile accessor for configure  
                  Added check for port on send 
                  only RTCP ib_port
06/26/05    srk   Giving indication when close fails.
                  Added check for config cb before calling
                  it. Started storing payload type after
                  configure.
06/09/05    srk   Removing data queue from Send side
                  moving to common pool
06/06/05    srk   Making JB init in line qith rest.
                  Adding code change for H.263
                  Adding RAW profile with JB
05/11/05    srk   Added RTCP and lipsync changes
04/01/05    srk   Added changes for  priority based queueing.
03/23/05    srk   Added configure API limited support.
03/18/05    srk   Removed RV dependencies added our own
                  RTP
03/04/05    srk   Added Shutdown API support and cleanup on init failure
10/26/04    srk   Initial Creation.
===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP

#ifdef  RTP_FILE_NUMBER 
  #undef RTP_FILE_NUMBER 
#endif 
#define RTP_FILE_NUMBER 2

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <comdef.h>               /* common target/c-sim defines */
#include <string.h>               /* memory routines */
#include <stdlib.h>               /* for malloc */


#include <stdlib.h>
#include <string.h>

#ifdef WINDOWS_MAKE
#include <stdio.h>                /* for windows console */
//#include <Winsock.h>
#endif
#include "qvp_rtp_api.h"
#include "qvp_rtp_buf.h"
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
#include "qvp_rtp_jb_api.h"
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
#include "qvp_rtp_profile.h"
#include "qvp_rtp_msg.h"          /* for qvp rtp message data types */
#include "qvp_rtp_app_cmd.h"      /* app command internal apis */
#include "qvp_rtp_packet.h"
#include "qvp_rtp_nw.h"           /* for nw apis */
#include "qvp_rtcp_api.h"
#include "qvp_rtp.h"              /* for core data types */
#include "qvp_rtp_task.h"     /* for MSG_HIGH */
#include "list.h"
#include "qvp_rtp_timer_api.h"
#include "qvp_rtp_log.h"
#include "qvp_srtp.h"
#include "qvp_rtcp.h"
#include "qpdpl.h"

#include "qvp_app_common.h"
#include "time_svc.h"
#include "qvp_rtp_codec.h"
#include "qvp_rtp_service.h"
#include "qvp_rtp_codec_txt.h"
#include "bit.h"
#include "ran.h"
#include "secapi.h"
#include "qpConfigNVItem.h"
#include "rex.h"                    /* definition of REX data types       */
#include "qvp_timer.h"
#include "qvp_rtp_nw_ds.h"

#define QVP_RTP_AUDIO_OFFLOAD 2
#define QVP_RTP_AMR_NB_CLOCK_RATE 8000
#define QVP_RTP_AMR_WB_CLOCK_RATE 16000
#define QVP_RTP_AUDIO_IND_CNT	1
#define QVP_RTP_VIDEO_TX_IND_CNT	1
#define QVP_RTP_VIDEO_RX_IND_CNT	1
#define QVP_RTP_VIDEO_IND_CNT	(QVP_RTP_VIDEO_TX_IND_CNT + QVP_RTP_VIDEO_RX_IND_CNT)
#define QVP_RTP_METRIC_ALPHA 0.9
#define QVP_RTP_COMPUTE_METRIC(alpha, curr, prev) (((alpha)*(prev))+((1-alpha)*(curr)))
#define QVP_RTP_COMPARE_METRIC(x,b) ((x) >= (b) ? 1: 0)
#define QVP_RTP_UL_ACTIVE 0xF0
#define QVP_RTP_DL_ACTIVE 0x0F
#define QVP_RTP_ULDL_ACTIVE 0xFF
#define QVP_RTP_ULDL_INACTIVE 0xFF
#define QVP_SAMPLES_PER_SECOND 50 /*1000/20 */
#define QVP_PERCENTILE_METRIC 0.95
#define QVP_RTP_METRIC_95P(time,result) \
  do{\
  uint8 temp=0;\
  temp = (time) * QVP_SAMPLES_PER_SECOND;\
  (result) = (1-QVP_PERCENTILE_METRIC)*temp;\
  }while(0);

#define QVP_SET_METRIC_STATE(dst, state)\
do{\
   if(QVP_METRIC_NONE == dst)\
    dst|=state;\
   else if((QVP_METRIC_INIT|QVP_METRIC_CALCULATE )& dst)\
    dst|=state;\
   else\
     QVP_RTP_ERR_0("Illegal state");\
     }while(0);
   
#define QVP_RESET_METRIC_STATE(dst, state) dst &= ~(state)

rex_tcb_type                      qvp_rtp_tcb;

qvp_rtp_ctx rtp_ctx;    /* GLOBAL RTP CONTEXT */

LOCAL qvp_rtcp_config_srtcp_param_type srtcp_param;

static boolean qvp_rtp_initialized = FALSE;
LOCAL  boolean adaptive_jb = TRUE;
boolean dtmf_end_sent = FALSE;
extern uint64 g_Dec_RequestTime;
extern boolean g_isNetworkCommandPending;
extern qvp_rtp_codec_info_ctx codec_info_ctx;
extern qvp_rtp_codec_state g_current_audio_state_on_ap;
boolean g_cvd_ssr = FALSE;
uint8 dpl_init_proxy_timer = 0;
qvp_rtp_send_dtmf_pkt_type g_rx_dtmf;
boolean clear_all_session_req = FALSE;
boolean g_isStreamStarted = FALSE;
extern QPBOOL qpDplPostEventToEventQueue(QPUINT16 iMsgId, QPINT32 iParam, QPVOID *pParam, QPC_GLOBAL_DATA *pGlobalData); 
uint16  g_gps_nv = 0;
boolean srvcc_tear_down_req = FALSE;
static uint8 g_active_session = FALSE;
/*************
g_audio_offload
2: No Offload
3: Offload to modem only--> Phase 1
other values: Offload to APP-->Phase 2
***************/
uint8 g_audio_offload = QVP_RTP_AUDIO_OFFLOAD; 
qvp_rtp_handoff_state		g_handoffstate = HO_NONE;

/************************
Read from NV 67332 (iVideoEncodingBit), g_crash_recovery_timer is 
being used as workaround for crash recovery timer. Value 0 means use
default value of 6 sec. This will be in seconds.
***********************/
uint16 g_crash_recovery_timer = 6;

/*===========================================================================
                  LOCAL FORWARD DECLATIONS                     
===========================================================================*/
LOCAL int qvp_rtp_usr_ctx_pvt_update_config(qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type stream_id, qvp_rtp_msg_type *msg);

LOCAL void qvp_rtp_init_cmd
(
  qvp_rtp_init_msg_type *init_msg 
);
void qvp_rtp_adsp_ssr_cmd(void);
LOCAL void qvp_rtp_register_cmd
(
  qvp_rtp_register_msg_type *init_msg 
);
LOCAL void qvp_rtp_update_register_cb_cmd
(
  qvp_rtp_update_register_cb_msg_type *update_register_msg 
);
LOCAL void qvp_rtp_register2_cmd
(
  qvp_rtp_register2_msg_type *register2_msg 
);

LOCAL void qvp_rtp_open_cmd
(
  qvp_rtp_open_msg_type *open_msg
);

LOCAL void qvp_rtp_open2_cmd
(
  qvp_rtp_open2_msg_type *open_msg
);

LOCAL void qvp_rtp_config_cmd
(
  qvp_rtp_config_msg_type *config_msg
);

LOCAL void qvp_rtp_configure_session_cmd
(
  qvp_rtp_config_session_msg_type *config_session_msg  
);

LOCAL void qvp_rtp_reset_cmd
(
  qvp_rtp_reset_msg_type *reset_msg
);

LOCAL void qvp_rtp_close_cmd
(
  qvp_rtp_close_msg_type *close_msg
);

LOCAL void qvp_rtp_deregister_app_cmd
(
  qvp_rtp_deregister_msg_type *deregister_msg
);

LOCAL void qvp_rtp_send_cmd
(
  qvp_rtp_send_msg_type *send_msg 
);

LOCAL void qvp_rtp_send_dtmf_cmd
(
  qvp_rtp_send_msg_type *send_msg 
);

LOCAL void qvp_rtp_enable_ls_cmd
( 
  qvp_rtp_enable_ls_msg_type *ls_msg
);

LOCAL void qvp_rtp_disable_ls_cmd
( 
  qvp_rtp_disable_ls_msg_type *ls_msg
);

LOCAL qvp_rtp_status_type qvp_rtp_alloc_open_stream
(
  qvp_rtp_stream_type      *stream,
  qvp_rtp_open_msg_type    *open_msg,
  qvp_rtp_network_hdl_type nw_hdl
);

LOCAL qvp_rtp_status_type qvp_rtp_dealloc_close_stream
(
  qvp_rtp_stream_type       *stream,
  qvp_rtp_network_hdl_type  nw_hdl
);

LOCAL qvp_rtp_status_type qvp_rtp_configure_stream
(
  qvp_rtp_stream_type       *stream,
  qvp_rtp_config_msg_type   *config_msg,
  qvp_rtp_network_hdl_type  nw_hdl
);

LOCAL qvp_rtp_status_type qvp_rtp_configure_stream_direction
(
  qvp_rtp_stream_type       *stream,
  qvp_rtp_config_msg_type   *config_msg,
  qvp_rtp_network_hdl_type  nw_hdl,
  boolean                   *close_old_jb
);

LOCAL qvp_rtp_status_type qvp_rtp_configure_ob_socket
(
  qvp_rtp_stream_type         *stream,
  qvp_rtp_nw_open_param_type  *open_param,
  qvp_rtp_network_hdl_type    nw_hdl
);

LOCAL qvp_rtp_status_type qvp_rtp_configure_ib_socket
(
  qvp_rtp_stream_type             *stream,
  qvp_rtp_pyld_rtp_rx_param_type  rx_param,
  qvp_rtp_network_hdl_type        nw_hdl
);

LOCAL qvp_rtp_status_type qvp_rtp_close_unused_socket
(
  qvp_rtp_channel_type      current_stream_type,
  qvp_rtp_channel_type      desired_stream_type,
  qvp_rtp_stream_type       *stream,
  qvp_rtp_network_hdl_type  nw_hdl
);

LOCAL qvp_rtp_status_type qvp_rtp_configure_tx_path
(
  qvp_rtp_stream_type         *stream,
  qvp_rtp_nw_open_param_type  *open_param,
  qvp_rtp_pyld_rtp_tx_param_type  tx_param
);

LOCAL qvp_rtp_status_type qvp_rtp_configure_rx_path
(
  qvp_rtp_stream_type             *stream,
  qvp_rtp_pyld_rtp_rx_param_type  rx_param,
  boolean                         payload_valid,
  qvp_rtp_payload_type            payload_type,
  boolean                         *close_old_jb
);

LOCAL qvp_rtp_status_type qvp_rtp_configure2_stream
(
  qvp_rtp_stream_type          *stream,
  qvp_rtp_payload_config2_type *config_param
);

LOCAL qvp_rtp_status_type qvp_rtp_validate_configure
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config
);

LOCAL qvp_rtp_status_type qvp_rtp_validate_stream_configure
(
  qvp_rtp_stream_type          *stream,
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config
);

#ifdef FEATURE_IMS_RTP_JITTERBUFFER
LOCAL qvp_rtp_status_type qvp_rtp_alloc_jitter_buffer
(
  qvp_rtp_stream_type *stream,
  uint16               jitter_size
);
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
LOCAL qvp_rtp_traffic_priority_type qvp_rtp_get_nw_priority
(
  qvp_rtp_payload_type payload
);

LOCAL void qvp_rtp_shutdown_cmd( void );


LOCAL void qvp_rtp_handle_rtcp_info
(

  qvp_rtcp_session_handle_type,
  qvp_rtcp_app_handle_type,
  qvp_rtcp_session_info_type *,
  qvp_rtcp_local_param_type*
 
);

LOCAL void qvp_rtp_handle_nw_packet
(
  void                *stream_data,
  qvp_rtp_buf_type    *pkt_rcvd,
  QPNET_ADDR	remote_ip,
  uint16 remote_port 
);

LOCAL void qvp_rtp_handle_hk_tout
( 
  void *param
);

LOCAL qvp_rtp_status_type qvp_rtp_alloc_open_rtcp
(  
  qvp_rtp_open_msg_type    *open_msg,
  qvp_rtp_stream_type      *stream,
  qvp_rtp_network_hdl_type nw_hdl
);
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
LOCAL qvp_rtp_status_type qvp_rtp_configure_jitter_size 
(
  qvp_rtp_stream_type              *stream,
  qvp_rtp_pyld_rtp_rx_param_type   rx_param,
  boolean                          payload_valid,
  qvp_rtp_payload_type             payload_type,
  boolean                          *close_flag
);
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
LOCAL void qvp_rtp_send_rtcp_bye_cmd
(
  qvp_rtp_send_rtcp_bye_msg_type *send_msg 
);

LOCAL void qvp_rtp_send_rtcp_app_cmd
(
  qvp_rtp_send_rtcp_app_msg_type *send_msg 
);

LOCAL void qvp_rtp_send_rtcp_voip_xr_cmd
( 
  qvp_rtp_send_rtcp_voip_xr_msg_type  *voip_xr_msg 
);
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
LOCAL void qvp_rtcp_get_qdj_param
(
  qvp_rtp_jitter_hdl_type      hdl ,
  qvp_rtcp_session_handle_type  sess_handle 
);
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
LOCAL void qvp_rtp_set_rtcp_xr_signal_metrics_cmd
(
  qvp_rtp_set_rtcp_voip_xr_signal_metrics_type  *rtcp_signal_metrics_msg
);

LOCAL void qvp_rtp_set_rtcp_xr_tx_quality_cmd
(
  qvp_rtp_set_rtcp_voip_xr_tx_quality_metrics_type  *rtcp_tx_quality_msg
);

LOCAL void qvp_rtp_media_init_cmd
(
 qvp_rtp_media_init_msg_type *media_init_msg
 );

LOCAL void qvp_rtp_media_deinit_cmd
(
 qvp_rtp_media_deinit_msg_type *media_deinit_msg
 );

LOCAL void qvp_rtp_media_generate_nal_cmd
(
 qvp_rtp_media_generate_nal_header_msg_type *media_generate_nal_msg
 );


LOCAL void qvp_rtp_media_codec_init_cmd
(
 qvp_rtp_media_codec_init_msg_type *media_codec_init_msg
 );

LOCAL void qvp_rtp_media_codec_config_cmd
(
 qvp_rtp_media_codec_config_msg_type *media_codec_config_msg
 );

LOCAL void qvp_rtp_media_codec_release_cmd
(
 qvp_rtp_media_codec_release_msg_type *media_codec_release_msg
 );

LOCAL void qvp_rtp_media_start_stream_cmd
(
 qvp_rtp_media_start_stream_msg_type *media_start_stream_msg
 );
LOCAL void qvp_rtp_media_stop_stream_cmd
(
 qvp_rtp_media_stop_stream_msg_type *media_stop_stream_msg
 );
LOCAL void qvp_rtp_media_resume_stream_cmd
(
 qvp_rtp_media_resume_stream_msg_type *media_resume_stream_msg
 );
LOCAL void qvp_rtp_media_pause_stream_cmd
(
 qvp_rtp_media_pause_stream_msg_type *media_pause_stream_msg
 );
LOCAL void qvp_rtp_link_alive_timer_cmd
(
 qvp_rtp_link_alive_timer_msg_type *rtp_link_alive_timer_msg
 );
LOCAL void qvp_rtcp_link_alive_timer_cmd
(
 qvp_rtcp_link_alive_timer_msg_type *rtcp_link_alive_timer_msg
 );
LOCAL void qvp_rtp_handle_rtcp_link_aliveness_info
(

 qvp_rtcp_session_handle_type handle,
 qvp_rtcp_app_handle_type     app_hdl
 );
LOCAL void qvp_rtp_empty_jitter_buffer_cmd
(
 qvp_rtp_empty_jitter_buffer_msg_type *rtp_empty_jitter_buffer_msg
 );
LOCAL void qvp_start_keep_alive_timer_cmd
(
 qvp_start_keep_alive_timer_msg_type *start_keep_alive_timer_msg
 );
LOCAL void qvp_stop_keep_alive_timer_cmd
(
 qvp_stop_keep_alive_timer_msg_type *stop_keep_alive_timer_msg
 );
void qvp_rtp_process_tx_pkt
(
 qvp_rtp_app_id_type    app_id,      /* application which opened
                                     * the stream
                                     */
                                     qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
                                     uint8* frame_data,
                                     uint16 frame_length,
                                     uint32 tstamp,
                                     uint64 avtimestamp
                                     );
void qvp_rtp_get_dtmf_endbit
(
  qvp_rtp_app_id_type    app_id, 
  qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
  boolean end_bit
                                     );
LOCAL void qvp_rtp_handle_rtp_link_alive_timer ( void *param);
LOCAL void qvp_rtp_handle_rtp_keep_alive_timer ( void *param);
LOCAL void qvp_rtp_handle_rtcp_keep_alive_timer ( void *param);

int qvp_rtp_is_qmi_request(qvp_rtp_msg_type *msg);
void qvp_rtp_find_internal_streamid_type(qvp_rtp_msg_type *msg, int is_app, boolean is_req);

void qvp_rtp_process_qmi_request(qvp_rtp_msg_type *msg);
LOCAL void qvp_rtp_proxy_dpl_init (void *param);
extern qvp_rtp_status_type qvp_rtp_unpack_dtmf_payload
(
  uint8*         dtmf_payload,
  uint16         dtmf_payload_len,  
  qvp_rtp_send_dtmf_pkt_type *dtmf
);

extern void qvp_rtp_init_dpl(void);
extern void qvp_rtp_uninit_dpl(void);

extern void qvp_rtp_service_notify_dpl_deinit();
extern void qvp_rtp_imsrtp_service_notifier_init();
extern void qvp_rtp_imsrtp_service_notifier_de_init();
//extern qvp_rtp_status_type rtp_codec_txt_rx_ctx_init(qvp_rtp_stream_type* stream, qvp_rtp_text_ctx_type* text_context);
extern qvp_rtp_status_type rtp_codec_txt_rx_ctx_uninit(qvp_rtp_stream_type* stream);
void qvp_rtp_log_rx_rtp_pkt(qvp_rtp_stream_type *stream, uint8 direction, qvp_rtp_buf_type    *pkt_rcvd, uint16  head_room);
LOCAL void qvp_rtp_log_commit_voice_call_stat( qvp_rtp_stream_type *stream );
LOCAL void qvp_rtp_log_voice_call_stat(qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type stream_id);
extern qvp_rtp_status_type qvp_rtp_text_codec_configure_pvt(
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                      * the stream
                                      */
  qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
  qvp_rtp_media_config   *config,
  qvp_rtp_media_handle_response  handle_media_res,
  qvp_rtp_profile_hdl_type *handle,
  qvp_rtp_stream_direction  direction   
  );
extern qvp_rtp_status_type qvp_rtp_text_codec_release_pvt(
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                     * the stream
                                     */
  qvp_rtp_media_handle_response  handle_media_res
                                     );

extern boolean qvp_rtp_service_is_indication_pending(void);

LOCAL qvp_rtp_status_type qvp_rtp_configure_srtp(qvp_rtp_stream_type* stream, qvp_rtp_config_msg_type* config_msg);
LOCAL qvp_rtp_status_type qvp_rtp_authenticate_decrypt_pkt 
(
  qvp_rtp_buf_type *pkt,      /* packet profile decoded */ 
  qvp_rtp_stream_type   *stream
);

void qvp_rtp_media_uninitialize_all_cmd
(
	qvp_rtp_media_uninitialize_all_msg_type *media_uninitialize_all_msg
);
void qvp_rtp_srvcc_tear_down_cmd
(
	qvp_rtp_srvcc_tear_down_msg_type *srvcc_tear_down_msg
);
void qvp_rtp_per_req_cmd
(
	qvp_rtp_per_req_msg_type *per_req_msg
);

void qvp_rtp_response_handler
(
	qvp_rtp_app_id_type    app_id,		/* application which opened
										 * the stream
										 */
	qvp_rtp_stream_id_type	stream_id,	 /* stream which was opened */										 
	qvp_rtp_app_data_type  app_data,
	qvp_rtp_response_type rsp_type, /*  msg type for ehich response has to be sent out */	
	qvp_rtp_status_type  status,
	boolean				  is_modem,  /* Is the response form modem*/
	qvp_rtp_stream_direction direction
);
void qvp_rtp_video_quality_response
(
    qvp_rtp_app_id_type    app_id,		/* application which opened
										 the stream*/
	qvp_rtp_stream_id_type	stream_id,	 /* stream which was opened */	
	qvp_rtp_video_quality video_quality, /*video quality*/
    uint16 reason_code /*reason for video quality*/

);
void qvp_rtp_handoff_queue_commands
(
 qvp_rtp_app_id_type app_id,
 boolean is_modem_valid
);

void qvp_rtp_set_handoff_status_cmd
(
  qvp_rtp_set_handoff_status_msg_type         *set_handoff_status_msg
);
boolean qvp_rtp_push_pending_command
(
	qvp_rtp_app_id_type app_id,
	qvp_rtp_stream_id_type stream_id, 
	qvp_rtp_command cmd,
	boolean is_modem
);
void qvp_rtp_execute_pending_command
(
	qvp_rtp_app_id_type app_id
);

qvp_rtp_stream_id_type qvp_rtp_find_external_streamid 
(
	qvp_rtp_app_id_type app_id,
	qvp_rtp_stream_id_type stream_id,
	boolean is_modem
);


qvp_rtp_stream_id_type qvp_rtp_find_internal_streamid 
(
	qvp_rtp_app_id_type app_id,
	qvp_rtp_stream_id_type stream_id,
	int is_app
);

int qvp_rtp_return_free_usr_ctx_pvt
(
	qvp_rtp_app_id_type app_id,
	void* external_stream_id
);
int qvp_rtp_return_stream_id_type
(
	qvp_rtp_stream_id_type stream_id,
	qvp_rtp_app_id_type app_id
);

void qvp_rtp_mark_pending_task_done
(
	qvp_rtp_app_id_type app_id
);

qvp_rtp_stream_id_type qvp_rtp_update_stream_id_in_pvt_ctx
(
	qvp_rtp_app_id_type app_id,
	qvp_rtp_stream_id_type internal_stream_id,
	boolean is_modem,
	uint8 index
);

qvp_rtp_media_type	qvp_rtp_find_media_type_from_pvt_ctx
(
	qvp_rtp_stream_id_type internal_stream_id,
	qvp_rtp_app_id_type app_id,
	boolean is_modem,
	boolean is_force_check
);


void qvp_rtp_execute_pending_command_cb
(
	uint8 app_id
);

void qvp_rtp_post_execute_pending_command
(
	qvp_rtp_app_id_type app_id
);
void qvp_rtp_clear_pending_ref_cnt
(
	qvp_rtp_app_id_type app_id, 
	qvp_rtp_media_type media_type
);
void qvp_rtp_clear_pvt_ctx_on_error
(
	qvp_rtp_app_id_type app_id,
	qvp_rtp_app_data_type app_data
);
extern void qvp_rtp_service_send_dtmf_req(qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type stream_id, qvp_rtp_buf_type       *pkt);
qvp_rtp_stream_id_type qvp_rtp_find_new_stream_id(qvp_rtp_app_id_type app_id, boolean is_modem);
boolean qvp_rtp_is_stream_id_set(qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type external_stream_id);

/*===========================================================================

FUNCTION QVP_RTP_PROCESS_APP_CMD 

DESCRIPTION
  Processea the application initiated commmand passed. The call backs are 
  called if the processing is done in one shot.

DEPENDENCIES
  None

PARAMETERS
    msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void qvp_rtp_process_app_cmd ( qvp_rtp_msg_type *msg )
{

  int flag = FALSE;
/*------------------------------------------------------------------------*/

  
  /*------------------------------------------------------------------------
    Check for valid arguments
  ------------------------------------------------------------------------*/
  if( !msg )
  {
    return;
  }
  
  QVP_RTP_MSG_HIGH_1( "qvp_app_process_app_cmd :: Got app command %d \r\n ", msg->msg_type);

  flag = qvp_rtp_is_qmi_request(msg);

  qvp_rtp_find_internal_streamid_type(msg, flag, TRUE);

  if(flag == 1) /* This is a QMI Request */
  {
    qvp_rtp_process_qmi_request(msg);
  switch ( msg->msg_type )
  {
    case  QVP_RTP_INIT_CMD :
      {
        qvp_rtp_init_cmd( &msg->msg_param.init_msg );
        break;
      }
    case  QVP_RTP_REGISTER_CMD :
      {
        qvp_rtp_register_cmd( &msg->msg_param.register_msg );
        break;
      }
    case  QVP_RTP_UPDATE_REGISTER_CB_CMD :
      {
        qvp_rtp_update_register_cb_cmd( &msg->msg_param.update_register_msg );
        break;
      }
    case  QVP_RTP_REGISTER2_CMD :
      {
        qvp_rtp_register2_cmd( &msg->msg_param.register2_msg );
        break;
      }
    case  QVP_RTP_DEREGISTER_CMD :
      {
        qvp_rtp_deregister_app_cmd( &msg->msg_param.deregister_app_msg );
        break;
      }
    case QVP_RTP_SHUTDOWN_CMD :
      {
		qvp_rtp_service_clear_callbacks();
        qvp_rtp_shutdown_cmd( );
      }
      break;
    case QVP_RTP_MEDIA_INIT_CMD:
      {
        qvp_rtp_media_init_cmd(&msg->msg_param.media_init_msg);
        break;
      }
    case QVP_RTP_MEDIA_DEINIT_CMD:
      {
        qvp_rtp_media_deinit_cmd(&msg->msg_param.media_deinit_msg);
        break;
      }
    case QVP_RTP_MEDIA_CODEC_RELEASE_CMD:
      {
        if(msg->msg_param.media_codec_release_msg.media_type == QVP_RTP_AUDIO)
       	{
       	clear_all_session_req = FALSE;
        g_cvd_ssr = FALSE;
           qvp_rtp_media_codec_release_cmd( &msg->msg_param.media_codec_release_msg );
           
        }
		else if (msg->msg_param.media_codec_release_msg.media_type == QVP_RTP_TEXT)
		{
           qvp_rtp_media_codec_release_cmd( &msg->msg_param.media_codec_release_msg );
		}
        else
        {
            QVP_RTP_ERR_1("QVP_RTP_MEDIA_CODEC_RELEASE_CMD not processed for media_type %d",\
                        msg->msg_param.media_codec_release_msg.media_type);
        }
        break;
      }
    default:
      break;
    }
  }
  else if (flag == 0){

    /*------------------------------------------------------------------------
    Switch on the message code and act upon each message 
    ------------------------------------------------------------------------*/
    switch ( msg->msg_type )
    {
    case  QVP_RTP_OPEN_CMD :
      {
        qvp_rtp_open_cmd( &msg->msg_param.open_msg );
        break;
      }
    case  QVP_RTP_OPEN2_CMD :
      {
        qvp_rtp_open2_cmd( &msg->msg_param.open2_msg );
        break;
      }
    case QVP_RTP_CONFIG_CMD :
      {
        qvp_rtp_config_cmd( &msg->msg_param.config_msg );
        break;
      }      
    case QVP_RTP_CONFIG_SESSION_CMD :
      {
      	 qvp_rtp_configure_session_cmd( &msg->msg_param.config_session_msg);
        break;
      }	
    case QVP_RTP_RESET_CMD :
      {
        qvp_rtp_reset_cmd( &msg->msg_param.reset_msg );
        break;
      }
    case  QVP_RTP_SEND_CMD :
      {
        qvp_rtp_send_cmd( &msg->msg_param.send_msg );
        break;
      }
    case  QVP_RTP_SEND_DTMF_CMD :
      {
        qvp_rtp_send_dtmf_cmd( &msg->msg_param.send_msg );
        break;
      }
    case  QVP_RTP_CLOSE_CMD :
      {
	  	clear_all_session_req = FALSE;
        qvp_rtp_close_cmd( &msg->msg_param.close_msg );
        break;
      }
    case  QVP_RTP_ENABLE_LS_CMD :
      {
        qvp_rtp_enable_ls_cmd( &msg->msg_param.enable_ls_msg );
        break;
      }
    case  QVP_RTP_DISABLE_LS_CMD :
      {
        qvp_rtp_disable_ls_cmd( &msg->msg_param.disable_ls_msg );
        break;
      }
    case QVP_RTP_SEND_RTCP_BYE_CMD:
      {
        qvp_rtp_send_rtcp_bye_cmd( &msg->msg_param.rtcp_bye_msg );
        break;
      }
    case QVP_RTP_SEND_RTCP_APP_CMD:
      {
        qvp_rtp_send_rtcp_app_cmd( &msg->msg_param.rtcp_app_msg );
        break;
      }
    case QVP_RTP_SEND_RTCP_VOIP_XR_CMD:
      {
        qvp_rtp_send_rtcp_voip_xr_cmd( &msg->msg_param.rtcp_voip_xr_msg );
      }
    case QVP_RTP_SET_VOIP_XR_SIGNAL_METRICS_CMD:
      {
        qvp_rtp_set_rtcp_xr_signal_metrics_cmd( 
                                         &msg->msg_param.rtcp_signal_metrics_msg);
        break;
      }
    case QVP_RTP_SET_VOIP_XR_TX_QUALITY_CMD:
      {
        qvp_rtp_set_rtcp_xr_tx_quality_cmd( 
                                                    &msg->msg_param.rtcp_tx_metrics_msg );
        break;
      }
    case QVP_RTP_CODEC_INIT_CMD:
      {
      qvp_rtp_media_codec_init_cmd( &msg->msg_param.media_codec_init_msg );
      break;
    }
  case QVP_RTP_MEDIA_CODEC_CONFIG_CMD:
    {
      qvp_rtp_media_codec_config_cmd( &msg->msg_param.media_codec_config_msg );
      break;
    }
  case QVP_RTP_START_STREAM_CMD:
    {
      qvp_rtp_media_start_stream_cmd( &msg->msg_param.media_start_stream_msg );
      break;
    }
  case QVP_RTP_STOP_STREAM_CMD:
    {
	  clear_all_session_req = FALSE;
    g_cvd_ssr = FALSE;
	  qvp_rtp_log_voice_call_stat( msg->msg_param.media_stop_stream_msg.app_id,msg->msg_param.media_stop_stream_msg.stream_id);
      qvp_rtp_media_stop_stream_cmd( &msg->msg_param.media_stop_stream_msg );
      break;
    }
  case QVP_RTP_RESUME_STREAM_CMD:
    {
      qvp_rtp_media_resume_stream_cmd( &msg->msg_param.media_resume_stream_msg );
      break;
    }
  case QVP_RTP_PAUSE_STREAM_CMD:
    {
        QVP_RTP_MSG_HIGH_0( "QVP_RTP_PAUSE_STREAM_CMD \r\n");
      qvp_rtp_media_pause_stream_cmd( &msg->msg_param.media_pause_stream_msg );
      break;
    }
  case QVP_RTP_LINK_ALIVE_TIMER_CMD:
    {
      qvp_rtp_link_alive_timer_cmd( &msg->msg_param.rtp_link_alive_timer_msg );
      break;
    }
  case QVP_RTCP_LINK_ALIVE_TIMER_CMD:
    {
      qvp_rtcp_link_alive_timer_cmd( &msg->msg_param.rtcp_link_alive_timer_msg );
      break;
    }
  case QVP_RTP_EMPTY_JITTER_BUFFER_CMD:
    {
      qvp_rtp_empty_jitter_buffer_cmd(&msg->msg_param.rtp_empty_jitter_buffer_msg);
      break;
    }
  case QVP_START_KEEP_ALIVE_TIMER:
   {
      qvp_start_keep_alive_timer_cmd(&msg->msg_param.start_keep_alive_timer);
      break;
    }
  case QVP_STOP_KEEP_ALIVE_TIMER:
    {
      qvp_stop_keep_alive_timer_cmd(&msg->msg_param.stop_keep_alive_timer);
      break;
    }
  case QVP_RTP_UNINITIALIZE_ALL_RTP_SESSION_CMD:
  		{
			qvp_rtp_media_uninitialize_all_cmd( &(msg->msg_param.media_uninitialize_all_msg) );
  		}
    	break;
  case QVP_RTP_SRVCC_TEAR_DOWN_CMD:
  		{
			qvp_rtp_srvcc_tear_down_cmd( &(msg->msg_param.srvcc_tear_down_msg_type) );
  		}
    	break;
  case QVP_RTP_PER_REQUEST:
  		{
			qvp_rtp_per_req_cmd(&(msg->msg_param.per_req_msg_type));
  		}
  		break;
    default:
      {
        QVP_RTP_MSG_HIGH_0( "Unsupported app command in function qvp_app_process_app_cmd \r\n");
        break;
      }
  } /* end of switch msg->msg_type */
  }
  else if (flag == 2)
  {
    switch (msg->msg_type)
    {
    case QVP_RTP_SET_HANDOFF_STATUS:
      {
		QVP_RTP_ERR_0( "QVP_RTP_SET_HANDOFF_STATUS");
       	qvp_rtp_set_handoff_status_cmd( &(msg->msg_param.set_handoff_status_msg)); 
      }
      break;
    case  QVP_RTP_OPEN_CMD :
	  {
		  if ( msg->msg_param.open_msg.app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
			  && ( msg->msg_param.open_msg.app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users) )
		  {
			  if ( rtp_ctx.user_array[(uint32)msg->msg_param.open_msg.app_id ].\
				  call_backs.open_cb )
			  {
				  qvp_rtp_response_handler(msg->msg_param.open_msg.app_id, 0, msg->msg_param.open_msg.stream_params.app_data,\
					  QVP_RTP_OPEN_RSP, QVP_RTP_NORESOURCES, TRUE, 0);
			  }
		  }
		  break;
	  }
    case  QVP_RTP_OPEN2_CMD :
	  {
		  if ( msg->msg_param.open2_msg.app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
			  && ( msg->msg_param.open2_msg.app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users) )
		  {
			  if ( rtp_ctx.user_array[(uint32)msg->msg_param.open2_msg.app_id ].\
				  call_backs.open_cb )
			  {
				  qvp_rtp_response_handler(msg->msg_param.open2_msg.app_id, 0, msg->msg_param.open2_msg.open_config_param.stream_params.app_data,\
					  QVP_RTP_OPEN_RSP, QVP_RTP_NORESOURCES, TRUE, 0);
			  }
		  }
		  break;
	  }
    default:
      {
        QVP_RTP_MSG_HIGH_0( "Unsupported app command in function qvp_app_process_app_cmd \r\n");
      }
        break;
      }
  }
else
{
	QVP_RTP_ERR_1( "qvp_rtp_process_app_cmd: flag:%d",flag);
}
  return;
} /* end of function  qvp_rtp_process_app_cmd */

/*===========================================================================

FUNCTION QVP_RTP_PULL_PKT_CMD ()


DESCRIPTION
    Pulls a packet from inbound jitter buffer. This is a command which 
    is executed synchronously.

DEPENDENCIES
  None

ARGUMENTS IN

    app_id         - application which is sending packet
    stream_id      - stream through which packet is sent
    recv_pkt       - packet structure allocated by the app.
    max_len        - maximum len which can copied into the 
                     data pointer within the recv_pkt structure.


RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfully queued to RTP task.
  appropriate error code otherwise


SIDE EFFECTS
  None.

===========================================================================*/
qvp_rtp_status_type qvp_rtp_pull_pkt_cmd
( 
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                       * the stream
                                       */
  qvp_rtp_stream_id_type stream_id,   /* stream which was opened */
  qvp_rtp_recv_pkt_type  *recv_pkt,    /* preallocated recv packet
                                       * structure. This structure 
                                       * should contain a preallocated
                                       * data pointer as well.
                                       */ 
                                       
  uint16                 max_len       /* maximum len which can be copied */
)
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *usr_ctx; 
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range

    4) has a valid jitter buffer
  ------------------------------------------------------------------------*/
  if ( ( app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
       ( stream_id < ( qvp_rtp_stream_id_type )
         (uint32)rtp_ctx.user_array[ (uint32) app_id].num_streams) )
  {

    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ]; 

    /*----------------------------------------------------------------------
        extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream =  &( usr_ctx->stream_array[ (uint32) stream_id ] ); 

    
    /*----------------------------------------------------------------------
       Lock before updating audio clock
       Pull & RTP Rx happen in different tasks, and Pull is a synchronous 
       call. So to ensure that Rx clock is not read before all the values
       of audio reference clock are updated we lock here
    ----------------------------------------------------------------------*/
    if ( qvp_rtp_lock( ) != QVP_RTP_SUCCESS ) 
    {
      QVP_RTP_ERR_0( "qvp_rtp_pull_pkt_cmd: failed at lock");
      return ( QVP_RTP_ERR_FATAL );
    }

    /*----------------------------------------------------------------------
        If this is a audio stream pull, update the Rx audio clock
    ----------------------------------------------------------------------*/
    if( qvp_rtp_get_nw_priority( stream->payload_type ) == 
                                                  QVP_RTP_TRAFFIC_AUD_PRI )
    {
      qvp_rtcp_update_rx_audio_clk( stream->rtcp_hdl );
    }

    /*----------------------------------------------------------------------
        Unlock after clock updation
    ----------------------------------------------------------------------*/    
    if ( qvp_rtp_unlock( ) != QVP_RTP_SUCCESS ) /* unlock rtp resources  */
    {
      QVP_RTP_ERR_0( "qvp_rtp_pull_pkt_cmd: failed at unlock");
      return ( QVP_RTP_ERR_FATAL );
    }

    /*----------------------------------------------------------------------
       If there is a valid jitter buffer configured pull the packet out
    ----------------------------------------------------------------------*/
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
    if( stream->ib_stream.jitter_size && stream->ib_stream.jitter_queue )
    {
      return( qvp_rtp_jb_pull_pkt( stream->ib_stream.jitter_queue, recv_pkt, 
                           max_len ) );
    }
    else
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
    {
      return( QVP_RTP_ERR_FATAL );
    }
  }
  else
  {
    return( QVP_RTP_ERR_FATAL );
  }
} /* end of function qvp_rtp_pull_pkt_cmd */ 

/*===========================================================================

FUNCTION QVP_RTP_READ_Q_INFO


DESCRIPTION
  Reads the buffer occupancy and statistics inside RTP. This is done on a
  per session basis.

DEPENDENCIES
  This call may not be acurate or adequate if the session is operating on 
  REV0 mode.

ARGUMENTS IN

    app_id         - application which is sending packet
    stream_id      - stream through which packet is sent
    info           - structure pointer which will be populated with 
                     the current values.

ARGUMENTS OUT
    ifo            - pointer to the data structure which will store the info
                     be passed back to the app.


RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful; appropriate error code 
                     otherwise


SIDE EFFECTS
   The pointer info will be populated with the relavant information.

===========================================================================*/
qvp_rtp_status_type qvp_rtp_read_q_info_cmd 
(
  qvp_rtp_app_id_type      app_id,      /* application which opened
                                         * the stream
                                         */
  qvp_rtp_stream_id_type   stream_id,   /* stream which was opened */

  qvp_rtp_data_q_info_type *info        /* information regarding the q 
                                         * q occupancy and water marks
                                         */

)
{

  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *usr_ctx; 
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
    4) Good info pointer

  ------------------------------------------------------------------------*/
  if ( ( app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( app_id < ( qvp_rtp_app_id_type  ) (uint32)rtp_ctx.num_users ) &&
       ( stream_id < ( qvp_rtp_stream_id_type )
         (uint32)rtp_ctx.user_array[ (uint32) app_id].num_streams) &&
        info )
  {

    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ]; 

    /*----------------------------------------------------------------------
        extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream =  &( usr_ctx->stream_array[ (uint32) stream_id ] ); 

    
    /*----------------------------------------------------------------------
       If there is a valid jitter buffer configured pull the packet out
    ----------------------------------------------------------------------*/
    if( stream->ob_stream.valid )
    {
      return( qvp_rtp_get_nw_q_info( stream->ob_stream.sock_id, info ) );
    }
    else
    {
      return( QVP_RTP_ERR_FATAL );
    }
  }
  else
  {
    return( QVP_RTP_ERR_FATAL );
  }
} /* end of function qvp_rtp_read_q_info_cmd */

QPVOID qvp_qdj_qlty_cb(void *pParam)
{
  qvp_rtp_metrics *lmet = NULL;
  QPDPL_MEDIA_METRICS* thres = NULL;
  QP_MEDIA_METRIC_REPORT_STRUCT report;

  QVP_RTP_MSG_HIGH_1("metric:qvp_qdj_qlty_cb-active session state: %x",g_active_session);

  if(QVP_RTP_DL_ACTIVE & g_active_session)
  {
    lmet = (qvp_rtp_metrics*)rtp_ctx.metrics;
    if(NULL == lmet)
    {
      QVP_RTP_ERR_0("metrics:fatal -> this should not occur");
      return;
    }
    /* this means we received a bad quality indication from qdj throw the same*/
    lmet->edl_qlty = RAT_QUAL_BAD;  
    report.eMediaModuleID = QPE_METRIC_MEDIA_DL_MODULE;
    report.eMediaRATQuality = RAT_QUAL_BAD;
    qpDplProcessMediaMetricReportAPI(&report);
	  if(NULL != rtp_ctx.metricTh)
	  {
	    thres = (QPDPL_MEDIA_METRICS*)rtp_ctx.metricTh;
	    qvp_rtp_reset_timer(rtp_ctx.metrics->tHdl);
	    qvp_rtp_timer_start(rtp_ctx.metrics->tHdl, thres->m_iTimerInterval);
	  }
	  else
	  {
	    QVP_RTP_ERR_0("metric:qvp_qdj_qlty_cb: metric threshold NULL");
	  }
  }
  else
  {
    QVP_RTP_MSG_HIGH_0("metric: DL path not active");
  }
}


/* this takes care of activating or holding the required metrics based on the 
state of the active stream */
QPVOID qvp_rtp_metric_hdl_activate(uint8 pState)
{
  QP_MEDIA_METRIC_REPORT_STRUCT report;
  QPBOOL sTimer = FALSE;
  QPDPL_MEDIA_METRICS *thres = (QPDPL_MEDIA_METRICS*)(rtp_ctx.metricTh);
  qvp_rtp_metrics *metric = (qvp_rtp_metrics*)(rtp_ctx.metrics);
  QVP_RTP_MSG_HIGH_0("metric:qvp_rtp_metric_hdl_activate");
  QVP_RTP_MSG_HIGH_2("metric:cur state = %d newstate = %d",g_active_session, pState);
  if(pState == g_active_session || NULL == thres || NULL == metric)
  {
    QVP_RTP_ERR_0("metric: qvp_rtp_metric_hdl_activate - invalid param");
    return;
  }
  if((g_active_session >> 4) ^ (pState >> 4))
  {
    if(pState & QVP_RTP_UL_ACTIVE)
    {
      /* uplink has been activated */
      sTimer = TRUE;
    }
    else
    {
      metric->eul_qlty = RAT_QUAL_ON_HOLD;
    }
    g_active_session = (~(g_active_session & QVP_RTP_UL_ACTIVE) & 
                          (g_active_session | QVP_RTP_UL_ACTIVE));
  }
  if((g_active_session & QVP_RTP_DL_ACTIVE) ^ (pState & QVP_RTP_DL_ACTIVE))
  {
    if(pState & QVP_RTP_DL_ACTIVE)
    {
      /* uplink has been activated */
      sTimer = TRUE;
    }
    else
    {

      metric->edl_qlty = RAT_QUAL_ON_HOLD;
    }
    g_active_session = (~(g_active_session & QVP_RTP_DL_ACTIVE) & 
                          (g_active_session | QVP_RTP_DL_ACTIVE));
    
  }
  if(TRUE == sTimer)
  {
    QVP_RTP_MSG_HIGH_0("metric: re-arm the metric calculation timer");
    if(!(g_active_session  & QVP_RTP_UL_ACTIVE))
    {
      report.eMediaModuleID = QPE_METRIC_MEDIA_UL_MODULE;
      report.eMediaRATQuality = RAT_QUAL_GOOD;
      qpDplProcessMediaMetricReportAPI(&report);
    }
    if(!(g_active_session & QVP_RTP_DL_ACTIVE))
    {
      report.eMediaModuleID = QPE_METRIC_MEDIA_DL_MODULE;
      report.eMediaRATQuality = RAT_QUAL_GOOD;
      qpDplProcessMediaMetricReportAPI(&report);
    }
    qvp_rtp_timer_start(rtp_ctx.metrics->tHdl, thres->m_iTimerInterval);
  } 
  else 
  {
     QVP_RTP_MSG_HIGH_0("metric: cancelling metric calculation timer");
     qvp_rtp_reset_timer(rtp_ctx.metrics->tHdl);
  }
  QVP_RTP_MSG_HIGH_2("metric:@Exit cur state = %d newstate = %d",g_active_session, pState);
}

QPBOOL qvp_rtp_jitloss_metric(qvp_rtp_jitloss *metric, 
                              qvp_rtcp_session_handle_type hdl,
                              qvp_rtp_stream_direction dir)
{
  uint32 loss = 0;
  uint32 jitter = 0;
  uint32 jitter95p=0;

  if(NULL == metric || NULL == hdl
    ||(QVP_RTP_DIR_TX != dir && QVP_RTP_DIR_RX != dir))
  {
    QVP_RTP_ERR_0("metric:jitloss_metric Invalid Args");
    return FALSE;
  }
  if(QVP_RTP_SUCCESS != qvp_rtcp_get_jitter(hdl, &jitter, &jitter95p,dir))
  {
    QVP_RTP_ERR_0("metric:jitloss_metricget jitter failed");
    return FALSE;
  }
  
  if(QVP_RTP_SUCCESS != qvp_rtcp_get_loss(hdl, &loss, dir))
  {
    QVP_RTP_ERR_0("metric:jitloss_metricget loss failed");
    return FALSE;
  }

  QVP_RTP_MSG_HIGH("metric:loss=%d prevloss = %d dir = %d",loss,metric->prev_loss, dir);
  
  metric->loss = loss;
  metric->jitter = jitter;
  metric->jitter95p = jitter95p;


  QVP_RTP_MSG_HIGH("metric kpi:jitloss_metric jit - %d 95pj - %d loss - %d",\
          metric->jitter, metric->jitter95p, metric->loss);

  return TRUE;
}


QPBOOL qvp_rtp_compute_metrics(qvp_rtp_jitloss *metric,
                                           qvp_rtp_stream_direction dir,
                                           QP_DPL_RAT_QUALITY *quality)
{
  qvp_rtp_jitloss lJitloss;
  uint8 streamId =0xFF, appId =0xFF;
  qvp_rtp_usr_ctx *ctx = NULL;
  qvp_rtcp_session_handle_type hdl=NULL;
  QPBOOL status = TRUE;
  uint32 tempMetric=0;
  
  QVP_RTP_MSG_HIGH_1("metric:qvp_rtp_compute_metrics->dir : %d",dir);

  if(NULL == quality)
  {
    return FALSE;
  }
  *quality = RAT_QUAL_INVALID;
  if(FALSE == qvp_rtp_get_active_session(&streamId, &appId))
  {
    QVP_RTP_ERR_0("metric:compute_metrics -  failed to get active session");
    return FALSE;
  }
  ctx = &rtp_ctx.user_array[appId];

  if(NULL == ctx)
  {
    return FALSE;
  }
  hdl = ctx->stream_array[streamId].rtcp_hdl;
  if(hdl == NULL)
  {
    /* if RTCP is not enabled we send the metric as good from media */
    *quality = RAT_QUAL_GOOD;
    return TRUE;
  }

  if(NULL == metric ||(QVP_RTP_DIR_RX != dir && QVP_RTP_DIR_TX != dir) ||
    NULL == rtp_ctx.metricTh || NULL == quality)
  {
    QVP_RTP_ERR_0("metric:invalid input arguements");
    return FALSE;
  }
  do
  {
    QPDPL_MEDIA_METRICS *thres = (QPDPL_MEDIA_METRICS*)(rtp_ctx.metricTh);
    qvp_rtp_jitloss ltemp;
    *quality = RAT_QUAL_GOOD;
    if(QVP_RTP_DIR_RX == dir)
    {

      ltemp.jitter = thres->m_sDLJitter.iAccept;
      ltemp.loss = thres->m_sDLFrLoss.iAccept;
      ltemp.jitter95p = thres->m_sDL95Jitter.iAccept;
    }
    else
    {
      ltemp.jitter = thres->m_sULJitter.iAccept;
      ltemp.loss = thres->m_sULFrLoss.iAccept;

    }/* end of if for dir check*/
    memset(&lJitloss,0,sizeof(qvp_rtp_jitloss));
    lJitloss.prev_loss = metric->prev_loss;
    if(TRUE != qvp_rtp_jitloss_metric(&lJitloss, hdl, dir))
    {
      QVP_RTP_ERR_0("metric:failed to fetch the jitter and loss metric");
      /* Even though this computation failed set the quality to GOOD */
      *quality = RAT_QUAL_GOOD;
      break;
    }
    QVP_RTP_MSG_HIGH_0("metric: jitloss_metric success");
    switch(dir)
    {
    case QVP_RTP_DIR_RX:
      tempMetric=metric->jitter95p = QVP_RTP_COMPUTE_METRIC(thres->m_fAlphaRTP,lJitloss.jitter95p,metric->jitter95p);
      QVP_RTP_ERR_1("metric kpi:computed metric - jitter95p = %d",tempMetric);
      if(QVP_RTP_COMPARE_METRIC(tempMetric,ltemp.jitter95p))
      {
        QVP_RTP_MSG_HIGH_1("metric kpi:compute_metrics BAD due to 95p=%d",tempMetric);
        *quality = RAT_QUAL_BAD;
      }

    case QVP_RTP_DIR_TX:
     tempMetric = metric->loss =     QVP_RTP_COMPUTE_METRIC(thres->m_fAlphaRTP, lJitloss.loss, metric->loss);
      metric->prev_loss = lJitloss.prev_loss;
       QVP_RTP_ERR_1("metric kpi:computed metric - loss = %d",tempMetric);
      if(QVP_RTP_COMPARE_METRIC(tempMetric, ltemp.loss))
      {
        QVP_RTP_ERR_1("metric:compute_metrics BAD due to loss = %d",tempMetric);
        *quality = RAT_QUAL_BAD;
      }
      tempMetric = metric->jitter = QVP_RTP_COMPUTE_METRIC(thres->m_fAlphaRTP, lJitloss.jitter, metric->jitter);
      QVP_RTP_ERR_1("metric kpi:computed metric - jitter = %d",tempMetric);
      if(QVP_RTP_COMPARE_METRIC(tempMetric, ltemp.jitter))
      {
        QVP_RTP_ERR_1("metric:compute_metrics BAD due to jitter = %d",tempMetric);
        *quality = RAT_QUAL_BAD;
      }
      break;
    default:
      QVP_RTP_ERR_1("metric:this default should never happen - dir = %d", dir);
      break;
    }
    
  }while(0);/* end of DO WHILE */
  return status;
}

QPVOID qvp_rtp_reset_metrics(qvp_rtp_metrics* pParam)
{
  QVP_RTP_MSG_HIGH_0("metric:qvp_rtp_reset_metrics");
  if(NULL == pParam)
  {
    QVP_RTP_ERR_0("metric:qvp_rtp_reset_metrics - NULL param");
    return;
  }
  memset(&pParam->dl_metrics,0,sizeof(qvp_rtp_dl_metrics));
  memset(&pParam->ul_metrics,0,sizeof(qvp_rtp_jitloss));
  qvp_rtp_reset_timer(pParam->tHdl);
  pParam->edl_qlty = RAT_QUAL_INVALID;
  pParam->eul_qlty = RAT_QUAL_INVALID;
}

QPVOID qvp_rtp_metric_cb(void* pParam)
{
  qvp_rtp_metrics *lmet = NULL;
  QP_DPL_RAT_QUALITY ULqlty = RAT_QUAL_GOOD;
  QP_DPL_RAT_QUALITY DLqlty = RAT_QUAL_GOOD;
  QPDPL_MEDIA_METRICS* temp = NULL;
  QP_MEDIA_METRIC_REPORT_STRUCT report;
  
  QVP_RTP_MSG_HIGH_1("metric:qvp_rtp_metric_cb-active session state: %x",g_active_session);
  lmet = (qvp_rtp_metrics*)pParam;
  
  if(NULL == rtp_ctx.metricTh || NULL == lmet)
  {
    QVP_RTP_ERR_0("metric:context is NULL return");
    return;
  }
  report.eMediaModuleID = QPE_METRIC_MODULE_NONE;
  if(!g_active_session)
  {
    QVP_RTP_ERR_0("metric: Already on hold dont need to send again");
    //QVP_RTP_FATAL_0_STR(""); /* this should not happens since timer is reset*/
    return;
  }
  if(g_active_session & QVP_RTP_DL_ACTIVE)
  {
    if(!(lmet->dl_metrics.sDLJitLoss.compute & QVP_METRIC_SKIP))
    {
      QVP_RTP_MSG_HIGH_0("metric kpi: computing for DL/RX dir value:0");
      if(TRUE == qvp_rtp_compute_metrics(&lmet->dl_metrics.sDLJitLoss, QVP_RTP_DIR_RX, &DLqlty))
      {/* we have a valid metric to report */
        QVP_RTP_MSG_HIGH_2("metric kpi: computation for dir %d succeeded with quality %d",QVP_RTP_DIR_RX,DLqlty);
      }
      else
      {
        QVP_RTP_MSG_HIGH_0("metric: computation failed on DL not active");
      }
      if(DLqlty != lmet->edl_qlty)
      {
        QVP_RTP_MSG_MED_2("metric: prev - %d and curr - %d DLQ differ",lmet->edl_qlty,DLqlty);
      /* report metric received */
        lmet->edl_qlty = DLqlty;
        QVP_SET_METRIC_STATE(lmet->dl_metrics.sDLJitLoss.compute ,QVP_METRIC_CHANGED);
      }
      else
      {
        QVP_RTP_MSG_HIGH_1("metric:DL metric same as earlier nothing to report - %d", DLqlty);
        QVP_RESET_METRIC_STATE(lmet->dl_metrics.sDLJitLoss.compute ,QVP_METRIC_CHANGED);
      }
    }
    else
    {
      QVP_RTP_ERR_0("metric: set DL to good since we are skipping");  
      lmet->edl_qlty = RAT_QUAL_GOOD;
      QVP_RESET_METRIC_STATE(lmet->dl_metrics.sDLJitLoss.compute,QVP_METRIC_SKIP);
    }
  }
  else
  {
    QVP_RTP_ERR_0("metric: DL is not active set it to hold");
    DLqlty = RAT_QUAL_GOOD;
  }
  if(g_active_session & QVP_RTP_UL_ACTIVE)
  {
    if(!(lmet->ul_metrics.compute & (QVP_METRIC_SKIP|QVP_METRIC_SKIP_NEXT)))
    {
      QVP_RTP_MSG_HIGH_0("metric kpi: computing for UL/TX dir value:1");
      if(TRUE ==  qvp_rtp_compute_metrics(&lmet->ul_metrics, QVP_RTP_DIR_TX, &ULqlty))
      {
      /* we have a valid metric to report */
       QVP_RTP_MSG_HIGH_2("metric: computation for dir %d succeeded with quality %d",QVP_RTP_DIR_TX,ULqlty);
      }
      else
      {
        QVP_RTP_MSG_HIGH_0("metric: computation failed on UL not active");
      }
      if(ULqlty != lmet->eul_qlty)
      {
        QVP_RTP_MSG_MED_2("metric: prev - %d and curr - %d DLQ differ",lmet->eul_qlty,ULqlty);
      /* report metric received */
        lmet->eul_qlty = ULqlty;
        QVP_SET_METRIC_STATE(lmet->ul_metrics.compute,QVP_METRIC_CHANGED);
      }
      else
      {
        QVP_RTP_MSG_HIGH_1("metric:UL metric same as earlier nothing to report - %d", ULqlty);
        QVP_RESET_METRIC_STATE(lmet->ul_metrics.compute,QVP_METRIC_CHANGED);
      }
    }
    else
    {
      QVP_RTP_ERR_0("metric: set UL to good since we are skipping");
      QVP_SET_METRIC_STATE(lmet->ul_metrics.compute,QVP_METRIC_CHANGED);
      lmet->eul_qlty = RAT_QUAL_GOOD;
    }
  }
  else
  {
    QVP_RTP_ERR_0("metric: DL is not active set it to hold");
    QVP_SET_METRIC_STATE(lmet->ul_metrics.compute,QVP_METRIC_CHANGED);
    ULqlty = RAT_QUAL_GOOD;
  }
  
  /* restart timer only if we have any activity either on UL/DL*/
  if(g_active_session)
  {
    temp = (QPDPL_MEDIA_METRICS*)rtp_ctx.metricTh;
    qvp_rtp_timer_start(rtp_ctx.metrics->tHdl,temp->m_iTimerInterval);

    if(lmet->dl_metrics.sDLJitLoss.compute & QVP_METRIC_CHANGED ||
       lmet->ul_metrics.compute & QVP_METRIC_CHANGED)
    {
      report.eMediaRATQuality = ULqlty;
      report.eMediaModuleID = QPE_METRIC_MEDIA_UL_MODULE;
      qpDplProcessMediaMetricReportAPI(&report);
      report.eMediaRATQuality = DLqlty;
      report.eMediaModuleID = QPE_METRIC_MEDIA_DL_MODULE;
      qpDplProcessMediaMetricReportAPI(&report);
    }
    
  }
  else
  {
    QVP_RTP_MSG_HIGH_0("metric: no session active");
  }

}

LOCAL QPVOID qvp_rtp_metric_95P(uint32 pTimeInt, uint16* pJitter)
{
  uint32 temp=0;
  QVP_RTP_MSG_HIGH_2("metric:qvp_rtp_metric_95P - time = %d, percentile=%d%",pTimeInt,pJitter);
  temp = (pTimeInt) * QVP_SAMPLES_PER_SECOND;
  *pJitter = (1-QVP_PERCENTILE_METRIC)*temp;
}

LOCAL QPBOOL qvp_rtp_metric_init()
{
  uint8 status = 0;
  QPDPL_MEDIA_METRICS* temp = NULL;

  QVP_RTP_MSG_HIGH_0("metric:qvp_rtp_metric_init");
  do
  {
    if(NULL == rtp_ctx.metricTh)
    {
      rtp_ctx.metricTh = qvp_rtp_malloc(sizeof(QPDPL_MEDIA_METRICS));
      if(NULL == rtp_ctx.metricTh)
      {
        QVP_RTP_ERR_0("metric:memory allocation failed");
        status = 1;
        break;
      }
      temp = (QPDPL_MEDIA_METRICS*)rtp_ctx.metricTh;
      if(FALSE == qpDplReadMediaMetricThresholds(temp))
      {
        QVP_RTP_ERR_0("metric:Metric threshold read failed, continue");
        status = 2;
        break;
      }
      /* To be noted here it is Disabled == 0 -ve of -ve is +ve
         hence the feature is enabled */
      if(0 == temp->MediaMetricsDisabled)
      {
        rtp_ctx.metrics = (qvp_rtp_metrics*)qvp_rtp_malloc(sizeof(qvp_rtp_metrics));
        if(NULL == rtp_ctx.metrics)
        {
          QVP_RTP_ERR_0("metric:memory allocation failed return");
          status = 2;
          break;
        }
        memset(rtp_ctx.metrics,0,sizeof(qvp_rtp_metrics));
        if(QVP_RTP_SUCCESS != qvp_rtp_alloc_timer(qvp_rtp_metric_cb, 
                                                  (void*)(rtp_ctx.metrics), 
                                                   &rtp_ctx.metrics->tHdl))
        {
          QVP_RTP_ERR_0("metric:Mertic timer allocation failed - free memory");
          status = 3;
          break;
        }
        rtp_ctx.metrics->edl_qlty = RAT_QUAL_INVALID;
        rtp_ctx.metrics->eul_qlty = RAT_QUAL_INVALID;
        qvp_rtp_metric_95P(temp->m_iTimerInterval, &rtp_ctx.metrics->dl_metrics.rtp_jitter95p_cnt);
        QVP_RTP_MSG_HIGH_1("metric: initialization completed with flag - %d",status);
      }
      else
      {
        QVP_RTP_MSG_HIGH_0("metric:media metrics disabled no reporting required");
      }
    }
  }while(0);
  switch(status)
  {
    case 4:
      qvp_rtp_free_timer(rtp_ctx.metrics->tHdl);
    case 3:
      qvp_rtp_free(rtp_ctx.metrics);
    case 2:
      qvp_rtp_free(rtp_ctx.metricTh);
    case 1:
      status = FALSE;
      break;
    case 0:
      default:
      status = TRUE;
      break;
  }
  return status;
}


/*===========================================================================

FUNCTION QVP_RTP_INIT_CMD 

DESCRIPTION
  Processes the application command init().

DEPENDENCIES
  None

PARAMETERS
    init_msg - init_message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_init_cmd
(
  qvp_rtp_init_msg_type *init_msg 
)
{
  int32 i, j; /* index variables */
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
        First check the parameters 
  ------------------------------------------------------------------------*/
  if ( init_msg && ( ( init_msg->config.max_users ) && 
       ( !qvp_rtp_initialized ) ) )
  {
    if(FALSE == qvp_rtp_metric_init())
    {
      QVP_RTP_ERR_0("metric:Metric initialization failed with allocation error");
      return;
    }
    rtp_ctx.num_users = init_msg->config.max_users;
    rtp_ctx.user_array = ( qvp_rtp_usr_ctx *)  \
                         qvp_rtp_malloc( sizeof( qvp_rtp_usr_ctx ) *
                                 rtp_ctx.num_users  );

    /*----------------------------------------------------------------------
          See if the allocation succeeded. 
    ----------------------------------------------------------------------*/
    if ( rtp_ctx.user_array )
    {

      memset( rtp_ctx.user_array , 0, sizeof( qvp_rtp_usr_ctx ) * 
              rtp_ctx.num_users  ); 

      /*--------------------------------------------------------------------
        Initialize private variables for stream this avoids reverse
        serch from a stream pointer to context to find call back.
        
        Also stream id given to app. Looses a few byte but its good. 
      --------------------------------------------------------------------*/
      for ( i = 0; i < rtp_ctx.num_users ; i++ )
      {

        /*------------------------------------------------------------------
          Initialize all the streams for the user 
        ------------------------------------------------------------------*/
        for ( j = 0; j < QVP_RTP_MAX_STREAMS_DFLT; j++ )
        {
          rtp_ctx.user_array[i].stream_array[j].usr_ctx =
          &rtp_ctx.user_array[i];
          rtp_ctx.user_array[i].stream_array[j].str_id =
          ( qvp_rtp_stream_id_type ) j; 
          memset( &rtp_ctx.user_array[i].stream_array[j].rtp_log_pkt_ctx , 0, sizeof( rtp_ctx.user_array[i].stream_array[j].rtp_log_pkt_ctx ) );
          rtp_ctx.user_array[i].stream_array[j].stream_state = ACTIVE;
        } /* end of for j  = 0 */

        /*------------------------------------------------------------------
          Assign appid in user context.
        ------------------------------------------------------------------*/
        rtp_ctx.user_array[i].app_id = ( qvp_rtp_app_id_type ) i;

      } /* end of for i = 0 */

      /*--------------------------------------------------------------------
        Initialize profiles 
      --------------------------------------------------------------------*/
      if ( qvp_rtp_init_profiles ( (QVP_RTP_MAX_STREAMS_DFLT * QVP_RTP_MAX_USERS_DFLT) + 1, 
                                   QVP_RTP_MTU_DLFT ) != QVP_RTP_SUCCESS )
      {
        QVP_RTP_MSG_HIGH_0( " Could not initialize all profiles \r\n");
        goto init_failed;
      }

      /*--------------------------------------------------------------------
          Initialize NW interface layer 
          Here we reserve socket resources( ib & ob ) for max_streams.

          Each stream may also open a RTCP stream, thus NW layer needs to
          be initialized for max_streams * 2 
      --------------------------------------------------------------------*/
      if( qvp_rtp_nw_init(  QVP_RTP_MAX_STREAMS_DFLT * QVP_RTP_MAX_USERS_DFLT * 2 ) != 
          QVP_RTP_SUCCESS )
      {
        QVP_RTP_MSG_HIGH_0( " Could not initialize all network \r\n");
        goto init_failed;
      }
      
      /*--------------------------------------------------------------------
          Initialize jitter buffer 
      --------------------------------------------------------------------*/
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
      if( qvp_rtp_jb_init( QVP_RTP_MAX_STREAMS_DFLT ) != QVP_RTP_SUCCESS )
      {
        QVP_RTP_MSG_HIGH_0( " Could not initialize jitter buffer layer");
        goto init_failed;
      }
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
      
      /*--------------------------------------------------------------------
        Initialize RTCP sub-layer
      --------------------------------------------------------------------*/
      if( qvp_rtcp_init( QVP_RTP_MAX_STREAMS_DFLT * QVP_RTP_MAX_USERS_DFLT ) != QVP_RTP_SUCCESS )
      {
        goto init_failed;
      }

      /*--------------------------------------------------------------------
        Initialize our housekeeping timer if adaptive_jb is set
      --------------------------------------------------------------------*/
      if( qvp_rtp_alloc_timer( qvp_rtp_handle_hk_tout, NULL, 
                                &rtp_ctx.hk_timer ) != QVP_RTP_SUCCESS ) 
      {
        rtp_ctx.hk_timer = 0;
        goto init_failed;
      }

      /*--------------------------------------------------------------------
        Setting the HK timer reference count to zero
      --------------------------------------------------------------------*/
       rtp_ctx.hk_timer_ref_count = 0;

      /*--------------------------------------------------------------------
        Flag the module as initialized

        ...............NO INIT CODE BEYOND THIS POINT.............
      --------------------------------------------------------------------*/
      qvp_rtp_initialized = TRUE;
      return;

      /*--------------------------------------------------------------------
              ...............END OF INIT CODE .............
      --------------------------------------------------------------------*/
      
      /*--------------------------------------------------------------------
        Collection of code piece for the shutdown on error 
      --------------------------------------------------------------------*/
      init_failed :
        qvp_rtp_free( rtp_ctx.user_array );
        rtp_ctx.user_array = NULL;
        qvp_rtp_profiles_shutdown();
        qvp_rtp_shutdown_nw();
#ifdef FEATURE_IMS_RTP_JITTERBUFFER		
        qvp_rtp_jb_shutdown();
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
        qvp_rtcp_shutdown();
        /*------------------------------------------------------------------
          If we allocate a timer free it
        ------------------------------------------------------------------*/
        if( rtp_ctx.hk_timer )
        {
          qvp_rtp_free_timer( rtp_ctx.hk_timer );
          rtp_ctx.hk_timer = 0;
        }
        qvp_rtp_initialized = FALSE;
        return;
    }
    else
    {
      QVP_RTP_ERR_0(" qvp_rtp_init_cmd :: could not allocate user array\r\n");
      return;
    }
  }
  else
  {
    return;
  }
} /* end of function   qvp_rtp_init_cmd */

/*===========================================================================

FUNCTION QVP_RTP_REGISTER_CMD 

DESCRIPTION
  Processes the application command to register an app with RTP layer. 

DEPENDENCIES
  None

PARAMETERS
    register_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_register_cmd
(
  qvp_rtp_register_msg_type *register_msg 
)
{
  int i,index; /* lookup index */
  qvp_rtp_init_msg_type init_msg;
  qvp_rtp_register_param_type register_param;
  memset(&register_param,0,sizeof(register_param));
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Initialize the RTP layer and be ready for app 
    The init function is called directly and not through message posting.
    Even if app does it gain by posting a init message it does not hurt.
    Multiple inits will return success
  ------------------------------------------------------------------------*/
  init_msg.config.max_users = QVP_RTP_MAX_USERS_DFLT; 
  init_msg.config.max_streams = QVP_RTP_MAX_STREAMS_DFLT;
  qvp_rtp_init_cmd( &init_msg );

  /*------------------------------------------------------------------------
        First check the parameters 
  ------------------------------------------------------------------------*/
  if ( register_msg && qvp_rtp_initialized && 
       register_msg->call_backs.register_cb &&
       register_msg->call_backs.open_cb )
  {
  	{
		QPIMS_NV_CONFIG_DATA* pConfig;
		pConfig = (QPIMS_NV_CONFIG_DATA*) qpDplMalloc(MEM_IMS_DPL,sizeof(QPIMS_NV_CONFIG_DATA));
		if( !pConfig )
		{
		  QVP_RTP_MSG_HIGH_0("pConfig MALLOC RETURNED FAILED\n");
		  return;
		}
		else
		{
		  qpDplMemset(pConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
		 // QVP_RTP_MSG_HIGH_0("pConfig MALLOC Done\n");

		  pConfig->env_Hdr = eQP_IMS_MEDIA_CONFIG;
		  if (qpdpl_get_config_group_value(pConfig) == QP_IO_ERROR_OK)
		  {
			  /* Read audio Offload from NV*/
			  { 
				g_audio_offload = pConfig->uIMSConfigPayload.sMediaConfigItem.iAudio_Offload;
				QVP_RTP_MSG_HIGH_2("pConfig Free Done audio_offload = %d : %u\n",g_audio_offload,pConfig->uIMSConfigPayload.sMediaConfigItem.iAudio_Offload);
			  } 	
			  /* since NV sMediaConfigItem.iVideoEncodingBit is unused, use it to configure crash recovery timer */
              if(pConfig->uIMSConfigPayload.sMediaConfigItem.iVideoEncodingBit != 0)
              {
                 g_crash_recovery_timer = pConfig->uIMSConfigPayload.sMediaConfigItem.iVideoEncodingBit;
              }
              QVP_RTP_MSG_HIGH_1("Crash recovery timer = %d seconds\n",g_crash_recovery_timer);			  
		  }
		  qpDplFree(MEM_IMS_DPL,pConfig); 
		}
	}	
    /*----------------------------------------------------------------------
      Lookup in the user table and see we can accomodate one more free user
    ----------------------------------------------------------------------*/
    for ( i = 0; i < rtp_ctx.num_users; i++ )
    {
      /*--------------------------------------------------------------------
           If this is free user entry then mark it and use it 
      --------------------------------------------------------------------*/
      if ( !rtp_ctx.user_array[i].valid )
      {
        rtp_ctx.user_array[i].valid      = TRUE;
        /*------------------------------------------------------------------
            copy all the call backs to the internal data struct 
         -----------------------------------------------------------------*/
        qvp_rtp_memscpy( &rtp_ctx.user_array[i].call_backs,sizeof( rtp_ctx.user_array[i].call_backs ), &register_msg->call_backs,
                sizeof( rtp_ctx.user_array[i].call_backs ) );

		
		  for(index=0; index<QVP_RTP_MAX_USR_PVT_COUNT; index++)
		  {
			memset( &rtp_ctx.user_array[i].usr_ctx_pvt[index], 0, sizeof(qvp_rtp_usr_ctx_pvt));
			rtp_ctx.user_array[i].usr_ctx_pvt[index].apps_stream_id = (qvp_rtp_stream_id_type)DEAD_BEEF;
			rtp_ctx.user_array[i].usr_ctx_pvt[index].modem_stream_id = (qvp_rtp_stream_id_type)DEAD_BEEF;
		  }
		  
		rtp_ctx.user_array[i].prev_modem_ext_strem_id = (qvp_rtp_stream_id_type) QVP_RTP_MAX_STREAMS_DFLT;
		rtp_ctx.user_array[i].prev_app_ext_strem_id = (qvp_rtp_stream_id_type) (QVP_RTP_APPS_STREAM_ID_OFFSET + QVP_RTP_MAX_STREAMS_DFLT);	


        rtp_ctx.user_array[i].num_streams = QVP_RTP_MAX_STREAMS_DFLT; 

        /*------------------------------------------------------------------
          register the app with nw to obtain the nw handle.
          num_ports is 0 since port reservation is not done 
        ------------------------------------------------------------------*/
        register_param.num_ports = 0;
        if ( qvp_rtp_register_app_nw ( &register_param, 
              &rtp_ctx.user_array[i].nw_hdl ) != QVP_RTP_SUCCESS )
        {
          QVP_RTP_ERR_0( "qvp_rtp_register_cmd ::nw registration failed ");

          register_msg->call_backs.register_cb(
                                        QVP_RTP_NORESOURCES, 
                                        ( qvp_rtp_app_id_type ) DEAD_BEEF
                                        );
          rtp_ctx.user_array[i].valid      = FALSE;
          return;
        }
        /*------------------------------------------------------------------
          Flag success to the user , update the user ID and pass the index
          of the user array 
        ------------------------------------------------------------------*/
        if ( qvp_rtp_buf_init(QVP_RTP_AUDIO) != QVP_RTP_SUCCESS )
        {
          QVP_RTP_ERR_0( " could not init the buffer management \r\n");
          register_msg->call_backs.register_cb(
                                        QVP_RTP_NORESOURCES, 
                                        ( qvp_rtp_app_id_type ) DEAD_BEEF
                                        );
          return;
        }
        if ( qvp_rtp_buf_init(QVP_RTP_VIDEO) != QVP_RTP_SUCCESS )
        {
          QVP_RTP_ERR_0( " could not init the buffer management \r\n");
          register_msg->call_backs.register_cb(
                                        QVP_RTP_NORESOURCES, 
                                        ( qvp_rtp_app_id_type ) DEAD_BEEF
                                        );
          return;
        }
        if ( qvp_rtp_buf_init(QVP_RTP_TEXT) != QVP_RTP_SUCCESS )
          {
          QVP_RTP_ERR_0( " could not init the buffer management \r\n");
          register_msg->call_backs.register_cb(
                                        QVP_RTP_NORESOURCES, 
                                        ( qvp_rtp_app_id_type ) DEAD_BEEF
                                        );
          return;
          }
		/*Malloc for srtp master key */
		rtp_ctx.user_array[i].master_key = NULL;
		rtp_ctx.user_array[i].master_key = (char*)qvp_rtp_malloc(255);
		
        //QVP_RTP_MSG_HIGH_0("qvp_rtp_register_cmd calling dpl init");
        //qvp_rtp_init_dpl();
        register_msg->call_backs.register_cb( QVP_RTP_SUCCESS,
                                              ( qvp_rtp_app_id_type ) i );
		qvp_rtp_imsrtp_service_notifier_init();
        return;
      }
    } /* for i = 0 */

    /*----------------------------------------------------------------------
        if we got here that means we cannot accomodate more users 
        call the call back and let the user know.
    ----------------------------------------------------------------------*/
    QVP_RTP_ERR_0( "qvp_rtp_register_cmd :: not enough users allocated \
                  try increasing the number of users in the init_config");

    register_msg->call_backs.register_cb(
                                        QVP_RTP_NORESOURCES, 
                                        ( qvp_rtp_app_id_type ) DEAD_BEEF
                                        );
    return;
  }
  else
  {
    if ( register_msg && register_msg->call_backs.register_cb )
    {
      register_msg->call_backs.register_cb( QVP_RTP_WRONG_PARAM, 
                                     ( qvp_rtp_app_id_type ) DEAD_BEEF  );
      return;
    }
    QVP_RTP_ERR_0( "qvp_rtp_register_cmd ::  register failed because \
                  wrong params");
  }
  return;
} /* end of function  qvp_rtp_register_cmd */

void qvp_rtp_update_register_cb_cmd
(
  qvp_rtp_update_register_cb_msg_type *update_register_msg 
)
{

  if ( ( update_register_msg != NULL ) &&  
       ( update_register_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( update_register_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
  {
			if ( rtp_ctx.user_array[(uint32)update_register_msg->app_id].valid )
			{
				/*------------------------------------------------------------------
					copy all the call backs to the internal data struct 
				-----------------------------------------------------------------*/
				qvp_rtp_memscpy( &rtp_ctx.user_array[(uint32)update_register_msg->app_id].call_backs,sizeof( rtp_ctx.user_array[(uint32)update_register_msg->app_id].call_backs ), &update_register_msg->call_backs,
						sizeof( rtp_ctx.user_array[(uint32)update_register_msg->app_id].call_backs ) );
				update_register_msg->call_backs.register_cb( QVP_RTP_SUCCESS,
                                              update_register_msg->app_id );				
			}
  }
  else
  {
    if ( update_register_msg && update_register_msg->call_backs.register_cb )
    {
      update_register_msg->call_backs.register_cb( QVP_RTP_WRONG_PARAM, 
                                     ( qvp_rtp_app_id_type ) DEAD_BEEF  );
      return;
    }

    QVP_RTP_ERR_0( "qvp_rtp_update_register_cb_cmd ::  register failed because \
                  wrong params" );
  }

  return;

}

/*==========================================================================

FUNCTION QVP_RTP_REGISTER2_CMD 

DESCRIPTION
  Processes the application command to register an app with RTP layer. 

DEPENDENCIES
  None

PARAMETERS
  register2_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

==========================================================================*/
LOCAL void qvp_rtp_register2_cmd
(
  qvp_rtp_register2_msg_type *register2_msg 
)
{
  uint8 user_cnt = 0; /* lookup index */
  qvp_rtp_init_msg_type init_msg;
  QPIMS_NV_CONFIG_DATA* pConfig;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
        First check the parameters 
  ------------------------------------------------------------------------*/
  if ( !register2_msg ||
       !register2_msg->call_backs.register2_cb ||
       !register2_msg->call_backs.open_cb )
  {
    if ( register2_msg && register2_msg->call_backs.register2_cb )
    {
      register2_msg->call_backs.register2_cb ( QVP_RTP_WRONG_PARAM, 
                                     ( qvp_rtp_app_id_type ) DEAD_BEEF, 
                                      register2_msg->app_data );
    }

    QVP_RTP_ERR_0( "qvp_rtp_register2_cmd :: register failed because of \
                  wrong params");
     return;
  }

  /*------------------------------------------------------------------------
    Initialize the RTP layer and be ready for app 
    The init function is called directly and not through message posting.
    Even if app does it again by posting a init message it does not hurt.
    Multiple inits will return success
  ------------------------------------------------------------------------*/
  init_msg.config.max_users = QVP_RTP_MAX_USERS_DFLT; 
  init_msg.config.max_streams = QVP_RTP_MAX_STREAMS_DFLT;
  qvp_rtp_init_cmd( &init_msg );
  

		pConfig = (QPIMS_NV_CONFIG_DATA*) qpDplMalloc(MEM_IMS_DPL,sizeof(QPIMS_NV_CONFIG_DATA));
		if( !pConfig )
		{
		  QVP_RTP_MSG_HIGH_0("pConfig MALLOC RETURNED FAILED\n");
		  return;
		}
		else
		{
		  qpDplMemset(pConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
		  //QVP_RTP_MSG_HIGH_0("pConfig MALLOC Done\n");

		  pConfig->env_Hdr = eQP_IMS_MEDIA_CONFIG;
		  if (qpdpl_get_config_group_value(pConfig) == QP_IO_ERROR_OK)
		  {
				g_audio_offload = pConfig->uIMSConfigPayload.sMediaConfigItem.iAudio_Offload; 	
				QVP_RTP_MSG_HIGH_2("pConfig Free Done audio_offload = %d : %u\n",g_audio_offload,pConfig->uIMSConfigPayload.sMediaConfigItem.iAudio_Offload);

		  }
		  qpDplFree(MEM_IMS_DPL,pConfig); 
		}

  /*------------------------------------------------------------------------
    Bail out if initialisation failed
  ------------------------------------------------------------------------*/
  if ( !qvp_rtp_initialized )
  {
    QVP_RTP_ERR_0( "qvp_rtp_register2_cmd :: Initialisation failed \
                  ");
    register2_msg->call_backs.register2_cb(
                                  QVP_RTP_ERR_FATAL, 
                                  ( qvp_rtp_app_id_type ) DEAD_BEEF,
                                  register2_msg->app_data );
    return;
  }
  /*------------------------------------------------------------------------
    Lookup in the user table and see we can accomodate one more free user
  ------------------------------------------------------------------------*/
  for ( user_cnt = 0; user_cnt < rtp_ctx.num_users; user_cnt++ )
  {
    int i = 0;
    /*----------------------------------------------------------------------
         If this is free user entry then mark it and use it 
    ----------------------------------------------------------------------*/
    if ( !rtp_ctx.user_array[user_cnt].valid )
    {
      /*--------------------------------------------------------------------
          copy all the call backs to the internal data struct 
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( &rtp_ctx.user_array[user_cnt].call_backs,sizeof(qvp_rtp_app_call_backs_type),
               &register2_msg->call_backs,
              sizeof( rtp_ctx.user_array[user_cnt].call_backs ) );

      for(i=0; i<QVP_RTP_MAX_USR_PVT_COUNT; i++)
      {
      	memset( &rtp_ctx.user_array[user_cnt].usr_ctx_pvt[i], 0, sizeof(qvp_rtp_usr_ctx_pvt));
		rtp_ctx.user_array[user_cnt].usr_ctx_pvt[i].apps_stream_id = (qvp_rtp_stream_id_type)DEAD_BEEF;
		rtp_ctx.user_array[user_cnt].usr_ctx_pvt[i].modem_stream_id = (qvp_rtp_stream_id_type)DEAD_BEEF;
      }

	rtp_ctx.user_array[user_cnt].prev_modem_ext_strem_id = (qvp_rtp_stream_id_type) QVP_RTP_MAX_STREAMS_DFLT;
	rtp_ctx.user_array[user_cnt].prev_app_ext_strem_id = (qvp_rtp_stream_id_type) (QVP_RTP_APPS_STREAM_ID_OFFSET + QVP_RTP_MAX_STREAMS_DFLT);	
	
      rtp_ctx.user_array[user_cnt].num_streams = QVP_RTP_MAX_STREAMS_DFLT;
      /*--------------------------------------------------------------------
        register app to nw reserving ports requested by the app
      --------------------------------------------------------------------*/
      if ( qvp_rtp_register_app_nw ( &register2_msg->register_param, 
            &rtp_ctx.user_array[user_cnt].nw_hdl ) != QVP_RTP_SUCCESS )
      {
        QVP_RTP_ERR_0( "qvp_rtp_register2_cmd ::port reservation failed ");
         register2_msg->call_backs.register2_cb(
                                      QVP_RTP_NORESOURCES, 
                                      ( qvp_rtp_app_id_type ) DEAD_BEEF,
                                      register2_msg->app_data
                                      );
        return;
      }
      /*--------------------------------------------------------------------
        Flag success to the user , update the user ID and pass the index
        of the user array 
      --------------------------------------------------------------------*/
      if ( qvp_rtp_buf_init(QVP_RTP_AUDIO) != QVP_RTP_SUCCESS )
      {
	  QVP_RTP_ERR_0( " could not init the buffer management \r\n");
	  register2_msg->call_backs.register2_cb(
									  QVP_RTP_NORESOURCES, 
										  ( qvp_rtp_app_id_type ) DEAD_BEEF,
										  register2_msg->app_data
										  );
          return;
      }
	  if ( qvp_rtp_buf_init(QVP_RTP_VIDEO) != QVP_RTP_SUCCESS )
	  {
		  QVP_RTP_ERR_0( " could not init the buffer management \r\n");
		  register2_msg->call_backs.register2_cb(
										  QVP_RTP_NORESOURCES, 
										  ( qvp_rtp_app_id_type ) DEAD_BEEF,
										  register2_msg->app_data
										  );
          return;
      }
      if ( qvp_rtp_buf_init(QVP_RTP_TEXT) != QVP_RTP_SUCCESS )
	  {
		  QVP_RTP_ERR_0( " could not init the buffer management \r\n");
		  register2_msg->call_backs.register2_cb(
										  QVP_RTP_NORESOURCES, 
										  ( qvp_rtp_app_id_type ) DEAD_BEEF,
										  register2_msg->app_data
										  );
          return;
      }
	  
	  /*Malloc for srtp master key */
		rtp_ctx.user_array[user_cnt].master_key = NULL;
		rtp_ctx.user_array[user_cnt].master_key = (char*)qvp_rtp_malloc(255);
		
      rtp_ctx.user_array[user_cnt].valid      = TRUE;
      register2_msg->call_backs.register2_cb( QVP_RTP_SUCCESS,
                                       ( qvp_rtp_app_id_type ) (uint32)user_cnt,
                                         register2_msg->app_data );
      qvp_rtp_imsrtp_service_notifier_init();
      return;
    } /* if ( !rtp_ctx.user_array[user_cnt].valid )*/
  } /* for user_cnt = 0 */
  /*------------------------------------------------------------------------
    if we got here that means we cannot accomodate more users 
    call the call back and let the user know.
  ------------------------------------------------------------------------*/
  QVP_RTP_ERR_0( "qvp_rtp_register2_cmd :: not enough users allocated \
                try increasing the number of users in the init_config");

  register2_msg->call_backs.register2_cb(
                                  QVP_RTP_NORESOURCES, 
                                  ( qvp_rtp_app_id_type ) DEAD_BEEF,
                                  register2_msg->app_data );
  return;
} /* end of function  qvp_rtp_register2_cmd */

/*===========================================================================

FUNCTION QVP_RTP_OPEN_CMD 

DESCRIPTION
  Processes the application command to open an RTP channel. 

DEPENDENCIES
  None

PARAMETERS
    open_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_open_cmd
(
  qvp_rtp_open_msg_type *open_msg
)
{
  int i;                             /* for lookup in stream array */
  qvp_rtp_stream_type *stream_array; /* for inderection simplicity */
  qvp_rtp_usr_ctx *usr_ctx; 
  qvp_rtp_status_type status;
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
  Set the Profile ID in the DS before opening up any socket.This ID is set 
  by application if it has already opened a PDP context and wants the application 
  to share this 
------------------------------------------------------------------------*/
  if((open_msg != NULL ) && ( open_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ))
  {
    QVP_RTP_MSG_HIGH_1_MEM( "qvp_rtp_open_cmd:: Setting qvp_rtp_network_profile_id to \
                       %d",open_msg->stream_params.qvp_rtp_network_profile_id);
	QVP_RTP_MSG_HIGH_1_MEM( "qvp_rtp_open_cmd:: as_id \
                       %d",open_msg->stream_params.as_id);

    qvp_rtp_set_cdma_profile_id(open_msg->stream_params.qvp_rtp_network_profile_id);
  }

  /*------------------------------------------------------------------------
      Check for params before we act on message 

      1) good app id
      2) app id in range
      3) non zero local port .. Ravi had a good point we need to relax this
         to allow ephemeral port
      4) good pay load type
      5) we have a way to notify the app ie: good call back
  ------------------------------------------------------------------------*/
  if ( ( open_msg != NULL ) &&  
       ( open_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( open_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) && 
       ( open_msg->stream_params.local_port ) && 
       ( open_msg->stream_params.payload_type < QVP_RTP_PYLD_NIL ) &&
       ( rtp_ctx.user_array[ (uint32)open_msg->app_id ].call_backs.open_cb ) )
  {
    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the user context 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) open_msg->app_id ]; 
    /*----------------------------------------------------------------------
        extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream_array =  &( usr_ctx->stream_array[0] ); 

    /*----------------------------------------------------------------------
          lookup for free connections and go from there 
    ----------------------------------------------------------------------*/
    for ( i = 0; i < usr_ctx->num_streams; i++ )
    {
      /*--------------------------------------------------------------------
          See if there is a free channel 
      --------------------------------------------------------------------*/
      if ( !stream_array [i].valid )
      {
        /*------------------------------------------------------------------
            Following function will
            1) populate the channel variables
            2) Open the network socket
            3) sets up the remote IP and port for outbound and 
               bi directional  channel
        ------------------------------------------------------------------*/
        if ( ( status = qvp_rtp_alloc_open_stream( &stream_array [i], 
                   open_msg,
                   usr_ctx->nw_hdl ) )== QVP_RTP_SUCCESS ) 
        {

          /*----------------------------------------------------------------
              Flag the channel as occuppied 
          ----------------------------------------------------------------*/
          stream_array [i].valid = TRUE;
		  stream_array [i].send_dtmf = TRUE;

          /*----------------------------------------------------------------
             Set the stream as active stream
          ----------------------------------------------------------------*/
          stream_array[i].state =  QVP_RTP_SESS_ACTIVE;

          /*----------------------------------------------------------------
             Set the stream state as active
          ----------------------------------------------------------------*/
          stream_array[i].stream_state =  ACTIVE;
		  stream_array[i].rat_type = open_msg->stream_params.rat_type;
          memset( &stream_array[i].rtp_log_pkt_ctx , 0, sizeof(qvp_rtp_log_pkt_ctx) );
          /*--------------------------------------------------------------------
          Start the house keeping timer if this is the first session.
          If another session is already running just increment the reference count
          --------------------------------------------------------------------*/
          if(!rtp_ctx.hk_timer_ref_count)
          {
            if( qvp_rtp_timer_start( rtp_ctx.hk_timer, 500 /* one sec */ ) 
              != QVP_RTP_SUCCESS )
            {
              QVP_RTP_ERR_0( "qvp_rtp_open_cmd::Starting the house keeping timer failed \r\n");
            }
          }
          rtp_ctx.hk_timer_ref_count++;
          
          if(g_handoffstate != HO_IN_PROGRESS)
          {
		  	qvp_rtp_response_handler(open_msg->app_id, ( qvp_rtp_stream_id_type ) i, stream_array[i].app_data, QVP_RTP_OPEN_RSP, QVP_RTP_SUCCESS, TRUE, 0);
          }

          //QVP_RTP_MSG_HIGH_0_MEM( "qvp_rtp_open_cmd:: Successfully opened an RTP channel \r\n");
			  {
					QPIMS_NV_CONFIG_DATA* pConfig;
					pConfig = (QPIMS_NV_CONFIG_DATA*) qpDplMalloc(MEM_IMS_DPL,sizeof(QPIMS_NV_CONFIG_DATA));
					if( !pConfig )
					{
						QVP_RTP_MSG_HIGH_0("pConfig MALLOC RETURNED FAILED\n");
						return;
					}
					else
					{
						qpDplMemset(pConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
						//QVP_RTP_MSG_HIGH_0("pConfig MALLOC Done\n");
			  
						pConfig->env_Hdr = eQP_IMS_MEDIA_CONFIG;
						if (qpdpl_get_config_group_value(pConfig) == QP_IO_ERROR_OK)
						{
							//QVP_RTP_MSG_HIGH_1("[RTP_GPS_TIME]GPS READ TIME NV = %hu",pConfig->uIMSConfigPayload.sMediaConfigItem.igps_read_time_at_run);
			  
							if( pConfig->uIMSConfigPayload.sMediaConfigItem.igps_read_time_at_run != 0 )
							{
							  g_gps_nv = pConfig->uIMSConfigPayload.sMediaConfigItem.igps_read_time_at_run;   
							  QVP_RTP_MSG_HIGH_0("pConfig Free Done\n");
							}				
						}
						qpDplFree(MEM_IMS_DPL,pConfig); 
					}
			  }
        }
        else
        {
          /*----------------------------------------------------------------
            If we got here we could not open the stream 
          ----------------------------------------------------------------*/
          if(g_handoffstate != HO_IN_PROGRESS)
          {
              qvp_rtp_response_handler(open_msg->app_id, ( qvp_rtp_stream_id_type ) 0, open_msg->stream_params.app_data, QVP_RTP_OPEN_RSP, status, TRUE, 0);
           }
          QVP_RTP_MSG_HIGH_0( "qvp_rtp_open_cmd:: Unknown error while opening RTP channel \r\n");
        }
        return;
      }
    } /* end of for  i = 0 */

    /*----------------------------------------------------------------------
      If we got here we could not open the stream 
    ----------------------------------------------------------------------*/
    if(g_handoffstate != HO_IN_PROGRESS)
    {
        qvp_rtp_response_handler(open_msg->app_id, ( qvp_rtp_stream_id_type ) 0, open_msg->stream_params.app_data, QVP_RTP_OPEN_RSP, QVP_RTP_NORESOURCES, TRUE, 0);
    }
    return;
  }
  else
  {
    /*----------------------------------------------------------------------
      If we have a valid app id then we shoulc call the open_cb and let the
      user know the wrong doing. 
    ----------------------------------------------------------------------*/
    if ( open_msg && open_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
         && ( open_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users) )
    {
      /*--------------------------------------------------------------------
        If there is a registered open cb then call it. 
      --------------------------------------------------------------------*/
      if ( rtp_ctx.user_array[(uint32)open_msg->app_id ].call_backs.open_cb && (g_handoffstate != HO_IN_PROGRESS))
      {
         qvp_rtp_response_handler(open_msg->app_id, ( qvp_rtp_stream_id_type ) 0, open_msg->stream_params.app_data, QVP_RTP_OPEN_RSP, QVP_RTP_WRONG_PARAM, TRUE, 0);
      }
    }

    QVP_RTP_ERR_0( "qvp_rtp_open_cmd ::  open failed because wrong params");
    return;
  }
} /* end of function qvp_rtp_open_cmd */

/*===========================================================================

FUNCTION QVP_RTP_OPEN2_CMD 

DESCRIPTION
  Processes the application command to open and configure an RTP channel

DEPENDENCIES
  None

ARGUMENT IN
  open2_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_open2_cmd
	(
	qvp_rtp_open2_msg_type *open2_msg
	)
{
	int i;                             /* for lookup in stream array */
	qvp_rtp_stream_type *stream_array; /* for inderection simplicity */
	qvp_rtp_open_msg_type open_msg;
	qvp_rtp_stream_id_type streamid = ( qvp_rtp_app_id_type ) DEAD_BEEF;
	qvp_rtp_usr_ctx *usr_ctx; 
	qvp_rtp_status_type status = QVP_RTP_SUCCESS;
	boolean free_channel = FALSE;
	/*------------------------------------------------------------------------*/

	/*------------------------------------------------------------------------
	Set the Profile ID in the DS before opening up any socket.This ID is set 
	by application if it has already opened a PDP context and wants the application 
	to share this 
	------------------------------------------------------------------------*/
	memset(&open_msg,0,sizeof(open_msg));

	if((open2_msg != NULL ) && ( open2_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ))
	{
		QVP_RTP_MSG_HIGH_1_MEM( "qvp_rtp_open2_cmd:: Setting qvp_rtp_network_profile_id to \
							  %d",open2_msg->open_config_param.stream_params.qvp_rtp_network_profile_id);

		qvp_rtp_set_cdma_profile_id(open2_msg->open_config_param.stream_params.qvp_rtp_network_profile_id);
	}

	/*------------------------------------------------------------------------
	Check for params before we act on message 

	1) good app id
	2) app id in range
	3) non zero local port .. Ravi had a good point we need to relax this
	to allow ephemeral port
	4) good pay load type
	5) we have a way to notify the app ie: good call back
	------------------------------------------------------------------------*/
	if ( ( open2_msg != NULL ) &&  
		( open2_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
		( open2_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) && 
		( open2_msg->open_config_param.stream_params.local_port ) && 
		( open2_msg->open_config_param.stream_params.payload_type < \
		QVP_RTP_PYLD_NIL ) &&
		( rtp_ctx.user_array[ (uint32)open2_msg->app_id ].\
		call_backs.open_cb ) &&
		( rtp_ctx.user_array[ (uint32)open2_msg->app_id].valid ) )
	{
		/*----------------------------------------------------------------------
		Looks like we have a valid handle . extract the user context 
		----------------------------------------------------------------------*/
		usr_ctx = &rtp_ctx.user_array[ (uint32) open2_msg->app_id ]; 

		/*----------------------------------------------------------------------
		extract the stream array from the context 
		----------------------------------------------------------------------*/
		stream_array =  &( usr_ctx->stream_array[0] ); 

		/*----------------------------------------------------------------------
		lookup for free connections and go from there 
		----------------------------------------------------------------------*/
		for ( i = 0; i < usr_ctx->num_streams; i++ )
		{
			/*--------------------------------------------------------------------
			See if there is a free channel 
			--------------------------------------------------------------------*/
			if ( !stream_array [i].valid )
			{
				free_channel = TRUE;
				/*------------------------------------------------------------------
				Following function will
				1) populate the channel variables
				2) Open the network socket
				3) sets up the remote IP and port for outbound and 
				bi directional  channel
				------------------------------------------------------------------*/
				open_msg.app_id = open2_msg->app_id;
				open_msg.stream_params = open2_msg->open_config_param.stream_params;
				QVP_RTP_MSG_HIGH_1( "qvp_rtp_open2_cmd:: as_id \
							  %d",open_msg.stream_params.as_id);
				stream_array [i].is_srtp_enabled = FALSE;
				memset(&stream_array [i].srtp_ctx, 0, sizeof(qvp_rtp_srtp_context));
				if(open2_msg->open_config_param.stream_params.is_srtp_enabled)
				{
					stream_array [i].is_srtp_enabled = TRUE;

					if(open2_msg->open_config_param.stream_params.rx_srtp_info_valid && (open2_msg->open_config_param.stream_params.rx_srtp_info.master_key != NULL) )
					{
						stream_array [i].rx_srtp_info.sec_algo = open2_msg->open_config_param.stream_params.rx_srtp_info.sec_algo;
						stream_array [i].rx_srtp_info.hash_algo = open2_msg->open_config_param.stream_params.rx_srtp_info.hash_algo;
						stream_array [i].rx_srtp_info.is_authentication_enabled = open2_msg->open_config_param.stream_params.rx_srtp_info.is_authentication_enabled;			
						QVP_RTP_MSG_HIGH("SRTP enabled: RX security algo is %d RX hash algo is %d RX authentication = %d",stream_array [i].rx_srtp_info.sec_algo,stream_array [i].rx_srtp_info.sec_algo,stream_array [i].rx_srtp_info.is_authentication_enabled);
						//QVP_RTP_MSG_HIGH_1("SRTP enabled: RX security Master key length = %d", strlen(open2_msg->open_config_param.stream_params.rx_srtp_info.master_key));
						memset(stream_array [i].rx_master_key, 0, 255);
						qvp_rtp_memscpy(stream_array [i].rx_master_key, 255, open2_msg->open_config_param.stream_params.rx_srtp_info.master_key, strlen(open2_msg->open_config_param.stream_params.rx_srtp_info.master_key));
						stream_array [i].rx_srtp_info_valid = TRUE;			
					}

					if(open2_msg->open_config_param.stream_params.tx_srtp_info_valid && (open2_msg->open_config_param.stream_params.tx_srtp_info.master_key != NULL) )
					{
						stream_array [i].tx_srtp_info.sec_algo = open2_msg->open_config_param.stream_params.tx_srtp_info.sec_algo;
						stream_array [i].tx_srtp_info.hash_algo = open2_msg->open_config_param.stream_params.tx_srtp_info.hash_algo;
						stream_array [i].tx_srtp_info.is_authentication_enabled = open2_msg->open_config_param.stream_params.tx_srtp_info.is_authentication_enabled;
						QVP_RTP_MSG_HIGH("SRTP enabled: TX Hash algo is %d TX Hash algo is %d TX authentication = %d",stream_array [i].tx_srtp_info.hash_algo,stream_array [i].tx_srtp_info.hash_algo,stream_array [i].tx_srtp_info.is_authentication_enabled);
						memset(stream_array [i].tx_master_key, 0, 255);
						QVP_RTP_MSG_HIGH_1("SRTP enabled: TX security Master key length = %d", strlen(open2_msg->open_config_param.stream_params.tx_srtp_info.master_key));
						qvp_rtp_memscpy(stream_array [i].tx_master_key, 255, open2_msg->open_config_param.stream_params.tx_srtp_info.master_key, strlen(open2_msg->open_config_param.stream_params.tx_srtp_info.master_key));
						stream_array [i].tx_srtp_info_valid = TRUE;
					}
				}
				{
					QPIMS_NV_CONFIG_DATA* pConfig;
					pConfig = (QPIMS_NV_CONFIG_DATA*) qpDplMalloc(MEM_IMS_DPL,sizeof(QPIMS_NV_CONFIG_DATA));
					if( !pConfig )
					{
						QVP_RTP_MSG_HIGH_0("pConfig MALLOC RETURNED FAILED\n");
						return;
					}
					else
					{
						qpDplMemset(pConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
						//QVP_RTP_MSG_HIGH_0("pConfig MALLOC Done\n");

						pConfig->env_Hdr = eQP_IMS_MEDIA_CONFIG;
						if (qpdpl_get_config_group_value(pConfig) == QP_IO_ERROR_OK)
						{
							//QVP_RTP_MSG_HIGH_1("[RTP_GPS_TIME]GPS READ TIME NV = %hu",pConfig->uIMSConfigPayload.sMediaConfigItem.igps_read_time_at_run);

							if( pConfig->uIMSConfigPayload.sMediaConfigItem.igps_read_time_at_run != 0 )
							{
								g_gps_nv = pConfig->uIMSConfigPayload.sMediaConfigItem.igps_read_time_at_run; 	
								//QVP_RTP_MSG_HIGH_0("pConfig Free Done\n");
							} 			  
							/* Read audio Offload from NV*/
							{ 
                //ignoring NV value and configuring in code.
								g_audio_offload = pConfig->uIMSConfigPayload.sMediaConfigItem.iAudio_Offload;
								QVP_RTP_MSG_HIGH_2("pConfig Free Done audio_offload = %d : %u\n",g_audio_offload,pConfig->uIMSConfigPayload.sMediaConfigItem.iAudio_Offload);
							} 	
							/* Read video Offload from NV*/
							{ 
								uint8 video_offload = 0;
								video_offload = pConfig->uIMSConfigPayload.sMediaConfigItem.iVideo_Offload; 	
								QVP_RTP_MSG_HIGH_2("pConfig Free Done video_offload = %d : %d\n",video_offload,pConfig->uIMSConfigPayload.sMediaConfigItem.iVideo_Offload);
							} 		  
						}
						qpDplFree(MEM_IMS_DPL,pConfig); 
					}
				}

				if ( ( status = qvp_rtp_alloc_open_stream( &stream_array [i], 
					&open_msg, usr_ctx->nw_hdl ) ) != QVP_RTP_SUCCESS ) 
				{
					/*----------------------------------------------------------------
					If we got here we could not open the stream 
					----------------------------------------------------------------*/
					QVP_RTP_MSG_HIGH_0_MEM( "qvp_rtp_open2_cmd:: Unknown error while opening RTP channel \r\n");
					break;
				}
				/*------------------------------------------------------------------
				Flag the channel as occuppied 
				------------------------------------------------------------------*/
				stream_array [i].valid = TRUE;
				stream_array [i].send_dtmf = TRUE;
				stream_array [i].first_pckt_rcvd = FALSE;
				stream_array [i].dtmf_rx_pt = 0;
				/*------------------------------------------------------------------
				Set the stream as active stream
				------------------------------------------------------------------*/
				stream_array[i].state =  QVP_RTP_SESS_ACTIVE;

				/*----------------------------------------------------------------
				Set the stream state as active
				----------------------------------------------------------------*/
				stream_array[i].stream_state =  ACTIVE;
				stream_array [i].rat_type = open2_msg->open_config_param.stream_params.rat_type;
				memset( &stream_array[i].rtp_log_pkt_ctx , 0, sizeof(qvp_rtp_log_pkt_ctx) );
				/*------------------------------------------------------------------
				Set the stream ID
				------------------------------------------------------------------*/
				streamid = ( qvp_rtp_stream_id_type ) i;

				stream_array[i].dtmf_end_bit_pkt_count = 0;
				stream_array[i].target_tstamp = 0;
				stream_array[i].rtp_packet_source_check = FALSE;
				stream_array[i].srtp_ctx.cipher_handle = 0;
				stream_array[i].srtp_ctx.auth_handle = 0;

				/*--------------------------------------------------------------------
				Initialize our rtp pkt timer if is set
				--------------------------------------------------------------------*/
				if( qvp_rtp_alloc_timer( qvp_rtp_handle_rtp_keep_alive_timer, stream_array+i, 
					&stream_array[i].rtp_keep_alive_timer ) != QVP_RTP_SUCCESS ) 
				{
					stream_array->rtp_keep_alive_timer = 0;
					QVP_RTP_MSG_HIGH_0_MEM( "qvp_rtp_open2_cmd:: qvp_rtp_alloc_timer error while opening RTP channel \r\n");
					break;
				}
				if( qvp_rtp_alloc_timer( qvp_rtp_handle_rtcp_keep_alive_timer, stream_array+i, 
					&stream_array[i].rtcp_keep_alive_timer ) != QVP_RTP_SUCCESS ) 
				{
					stream_array->rtcp_keep_alive_timer = 0;
					QVP_RTP_MSG_HIGH_0_MEM( "qvp_rtp_open2_cmd:: qvp_rtp_alloc_timer error while opening RTP channel \r\n");
					break;
				}
				if( qvp_rtp_alloc_timer( qvp_rtp_handle_rtp_link_alive_timer, stream_array+i, 
					&stream_array[i].rtp_link_alive_timer ) != QVP_RTP_SUCCESS ) 
				{
					stream_array->rtp_link_alive_timer = 0;
					QVP_RTP_MSG_HIGH_0_MEM( "qvp_rtp_open2_cmd:: qvp_rtp_alloc_timer error while opening RTP channel \r\n");
					break;
				}
				stream_array[i].rtcp_keep_alive_time = 5000; // 5 sec
				stream_array[i].rtp_keep_alive_time = 20;  // 20 ms

				/*------------------------------------------------------------------
				Configure the stream if we have a valid configuration request.
				------------------------------------------------------------------*/

				if ( open2_msg->open_config_param.config_valid != TRUE )
				{
					QVP_RTP_MSG_HIGH_0_MEM( "qvp_rtp_open2_cmd:: Opening channel with default configuration \r\n");
					break;
				}

				if ( (status = qvp_rtp_configure2_stream( stream_array, 
					&open2_msg->open_config_param.config ) ) != QVP_RTP_SUCCESS )
				{
					/*----------------------------------------------------------------
					If we got here we could not configure the stream 
					----------------------------------------------------------------*/
					QVP_RTP_MSG_HIGH_0_MEM( "qvp_rtp_open2_cmd:: Unknown error while  \
										  opening RTP channel \r\n");
					break;

				}/*end of if (status = qvp_rtp_configure_stream() )*/

				QVP_RTP_MSG_HIGH_0_MEM( "qvp_rtp_open2_cmd:: Successfully opened an RTP \
									  channel \r\n");
				break;

			}/*end of if ( !stream_array [i].valid ) */

		}/*end of for (i = 0 ) */
		/*----------------------------------------------------------------------
		If we got here we could not open the stream 
		----------------------------------------------------------------------*/
		if ( !free_channel )
		{
			QVP_RTP_ERR_0( "qvp_rtp_open2_cmd:: no free channel available");
			status = QVP_RTP_NORESOURCES;
		}
		else
		{
			/*--------------------------------------------------------------------
			Start the house keeping timer if this is the first session.If another session 
			is already running just increment the reference count
			--------------------------------------------------------------------*/
			if(!rtp_ctx.hk_timer_ref_count)
			{
				if( qvp_rtp_timer_start( rtp_ctx.hk_timer, 500 /* one sec */ ) 
					!= QVP_RTP_SUCCESS )
				{
					QVP_RTP_ERR_0( "qvp_rtp_open2_cmd::Starting the house keeping timer failed \r\n");
				}
			}
			rtp_ctx.hk_timer_ref_count++;
		}
	}/*end of if (open2_msg != NULL)*/
	else
	{
		QVP_RTP_ERR_0( "qvp_rtp_open2_cmd ::  open failed because \
					 wrong params");
		status = QVP_RTP_WRONG_PARAM;
	}

	/*------------------------------------------------------------------------
	If we have a valid app id then we should call the open_cb and let the
	user know the wrong doing. 
	------------------------------------------------------------------------*/

	if ( open2_msg && open2_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
		&& ( open2_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users) )
	{

		/*----------------------------------------------------------------------
		If there is a registered open cb then call it. 
		----------------------------------------------------------------------*/
		if ( rtp_ctx.user_array[(uint32)open2_msg->app_id ].\
			call_backs.open_cb )
		{
			
			{
				int ret_val = -1;
				ret_val = qvp_rtp_return_free_usr_ctx_pvt(open2_msg->app_id, (void*)open2_msg->open_config_param.stream_params.app_data); //TBD, we need to change this place to a suitable position to handle WLAN to LTE

				//QVP_RTP_ERR( "qvp_rtp_open2_cmd::app_id:%d ret_val:%d streamid:%d\r\n", open2_msg->app_id,ret_val , streamid );

				if(ret_val != -1 && ret_val < QVP_RTP_MAX_USR_PVT_COUNT && (status == QVP_RTP_SUCCESS))
				{
					streamid = qvp_rtp_update_stream_id_in_pvt_ctx ( open2_msg->app_id, streamid, TRUE, ret_val);
					QVP_RTP_ERR_2( "qvp_rtp_open2_cmd::app_id:%d internal streamid:%d\r\n", open2_msg->app_id,streamid);
				}

				QVP_RTP_ERR_2( "qvp_rtp_open2_cmd::app_id:%d external_streamid:%d\r\n", open2_msg->app_id,streamid);

				if((g_handoffstate != HO_IN_PROGRESS))
				{
					qvp_rtp_response_handler(open2_msg->app_id, streamid, open2_msg->open_config_param.stream_params.app_data, QVP_RTP_OPEN_RSP, status, TRUE, 0);
				}
				else
				{
					qvp_rtp_post_execute_pending_command(open2_msg->app_id);
				}

			}

		}
	}

	return;

} /* end of function qvp_rtp_open2_cmd */

/*===========================================================================

FUNCTION QVP_RTP_READ_DEFAULT_CONFIG_CMD

DESCRIPTION

  This function SYNCHRONOUSLY executes payload configuration of an RTP 
  channel. The result is conveyed by populating the passed in structure on
  return. This is a command.

DEPENDENCIES
  None

ARGUMENTS IN
    app_id         - application which is requesting to open the channel
    payload        - payload type for which the default configuration 
                     is being read out.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
qvp_rtp_status_type qvp_rtp_read_default_config_cmd
( 
  qvp_rtp_app_id_type         app_id,
  qvp_rtp_payload_type        payload,
  qvp_rtp_payload_config_type *payld_config  /* where the configuration  
                                              *  should be copied
                                              */
)
{
  qvp_rtp_profile_type *profile;
/*------------------------------------------------------------------------*/


  
  /*------------------------------------------------------------------------
      Check for params before we act on message 

      1) good app id
      2) app id in range

      In the strict sense we do not need app id check. I am just using this
      now. This is because each app can specify its own defaults in future.
      
      4) good pay load type
  ------------------------------------------------------------------------*/
  if ( ( app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) && 
       ( payload < QVP_RTP_PYLD_NIL )  &&
       ( payld_config != NULL ) )
  {
    
    
    /*----------------------------------------------------------------------
      try and find an profile accessor table.
    ----------------------------------------------------------------------*/
    profile = qvp_rtp_get_payld_profile( payload ); 

    
    /*----------------------------------------------------------------------
      if the profile is not supported or the profile does not have 
      default params just return error.
    ----------------------------------------------------------------------*/
    if( !profile || !profile->read_dflt )
    {
      return( QVP_RTP_ERR_FATAL );
    }
    else
    {
      
      /*--------------------------------------------------------------------
        return the read default config
      --------------------------------------------------------------------*/
      return( profile->read_dflt(payload, payld_config ) );
    }
    
  }
  else
  {
    return( QVP_RTP_ERR_FATAL );
  }
  
} /* end of function qvp_rtp_read_default_config_cmd */

/*===========================================================================

FUNCTION QVP_RTP_CONFIG_CMD 

DESCRIPTION
  Processes the application command to configure an RTP channel. 

DEPENDENCIES
  None

PARAMETERS
    open_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_config_cmd
(
  qvp_rtp_config_msg_type *config_msg
)
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx; 
  qvp_rtp_status_type status;                            
  uint32 tx_ssrc;
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
    4) we want to switch off jiter,check if there is a packet callback
       to ship the packet to app
  ------------------------------------------------------------------------*/
  if ( ( config_msg != NULL ) && 
       ( config_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( config_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
       ( config_msg->stream_id < ( qvp_rtp_stream_id_type )
         (uint32)rtp_ctx.user_array[ (uint32)   config_msg->app_id].num_streams) &&
       ( rtp_ctx.user_array[ (uint32)config_msg->app_id].valid ) && 
       ( rtp_ctx.user_array[ (uint32)config_msg->app_id]. \
                 stream_array[ (uint32) config_msg->stream_id].valid ) )
  {


    user_ctx = &rtp_ctx.user_array[ (uint32)config_msg->app_id]; 
    stream   = &user_ctx->stream_array[ (uint32) config_msg->stream_id ];
    stream->dtmf_rx_pt = config_msg->config.config_rx_params.rx_rtp_param.dtmf_rx_pt;
    status = qvp_rtp_configure_stream( stream, config_msg, 
                           user_ctx->nw_hdl );

    /*----------------------------------------------------------------------
       If configuration was success and the stream was idle set it back 
       to active. Also restart RTP and RTCP.
    ----------------------------------------------------------------------*/
    if( status == QVP_RTP_SUCCESS )
    {
      if( stream->state == QVP_RTP_SESS_INACTIVE )
      {
        /*------------------------------------------------------------------
          Reinit RTP for new SSRC and Sequence number start
        ------------------------------------------------------------------*/
        qvp_rtp_ctx_init( &stream->rtp );

        /*------------------------------------------------------------------
          activate RTCP if the layer was there before
        ------------------------------------------------------------------*/
        if( stream->rtcp_hdl )
        {

          /*----------------------------------------------------------------
            get the newly generated TX SSRC
          ----------------------------------------------------------------*/
          qvp_rtp_get_rtp_tx_ssrc( &stream->rtp, &tx_ssrc );

          /*----------------------------------------------------------------
            Activate the session using the new SSRC
          ----------------------------------------------------------------*/
          qvp_rtcp_activate_session( stream->rtcp_hdl, tx_ssrc );
        }

        /*------------------------------------------------------------------
        If the outbound channel is valid activate the socket
        ------------------------------------------------------------------*/     
        if( stream->ob_stream.valid )
        {

          /*----------------------------------------------------------------
            Activate the network socket for an outbound transmission
          ----------------------------------------------------------------*/
          qvp_rtp_activate_nw_ob( stream->ob_stream.sock_id );
        }

        /*------------------------------------------------------------------
          If the inbound channel is valid activate the socket
        ------------------------------------------------------------------*/     
      if( stream->ib_stream.valid )
        {
          /*----------------------------------------------------------------
            Activate the network for  inbound 
          ----------------------------------------------------------------*/
          qvp_rtp_activate_nw_ib ( stream->ib_stream.sock_id );
          
        }

        stream->state = QVP_RTP_SESS_ACTIVE;
      }
    }
    
    /*----------------------------------------------------------------------
       If there is a uset call back installed just call it
    ----------------------------------------------------------------------*/
    if( user_ctx->call_backs.config_cb && g_handoffstate != HO_IN_PROGRESS)
    {
      qvp_rtp_response_handler(config_msg->app_id, config_msg->stream_id, stream->app_data, QVP_RTP_CONFIG_RSP, status, TRUE, 0);
      
    }
	else if(g_handoffstate == HO_IN_PROGRESS)
	{
		rtp_ctx.user_array[(uint32)config_msg->app_id].pending_queue[0].valid = FALSE;
	  	QVP_RTP_ERR_0("qvp_rtp_config_cmd: calling qvp_rtp_execute_pending_command");
		qvp_rtp_post_execute_pending_command(config_msg->app_id);
	}
                                    
  }
  else
  {
    
    /*----------------------------------------------------------------------
      If we have a valid app id then we shoulc call the close_cb and let the
      user know the wrong doing. 
    ----------------------------------------------------------------------*/
    if ( config_msg && 
          config_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
         && ( config_msg->app_id < ( qvp_rtp_app_id_type ) 
              (uint32)rtp_ctx.num_users) 
      && rtp_ctx.user_array[ (uint32) config_msg->app_id ].valid )
    {
      /*--------------------------------------------------------------------
        If there is a registered open cb then call it. 
      --------------------------------------------------------------------*/
      if ( rtp_ctx.user_array[ (uint32) config_msg->app_id ]. \
           call_backs.config_cb )
      {
       qvp_rtp_response_handler(config_msg->app_id, config_msg->stream_id, rtp_ctx.user_array[ (uint32) config_msg->app_id ]. \
             stream_array[ ( uint32 ) config_msg->stream_id].app_data, QVP_RTP_CONFIG_RSP, QVP_RTP_WRONG_PARAM, TRUE, 0);
      }
    }
  }
  

} /* end of function qvp_rtp_config_cmd */


/*===========================================================================

FUNCTION QVP_RTP_CONFIGURE_SESSION_CMD 

DESCRIPTION
  Processes the application command QVP_RTP_CONFIGURE_SESSION_CMD.

DEPENDENCIES
  None

PARAMETERS
    init_msg - init_message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_configure_session_cmd
(
  qvp_rtp_config_session_msg_type *config_session_msg 
)
{

  /*------------------------------------------------------------------------
      Check for params before we act on message 

      1) good app id
      2) app id in range
      3) we have a way to notify the app ie: good call back
      4) qvp_rtp_initialized is initialized
  ------------------------------------------------------------------------*/
 if ( ( config_session_msg != NULL ) 
       &&(config_session_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) 
       &&(config_session_msg->app_id < (qvp_rtp_app_id_type)(uint32)rtp_ctx.num_users)
       &&( rtp_ctx.user_array[(uint32)config_session_msg->app_id].valid )
       &&(qvp_rtp_initialized))
  {

    /*----------------------------------------------------------------------
      Looks like we have a valid handle . extract the user context 
    ----------------------------------------------------------------------*/
    if(qvp_rtcp_configure_session(config_session_msg->stream_id,\
      config_session_msg->config.rtcp_report_interval, config_session_msg->config.rtcp_xr_report_interval) == QVP_RTP_SUCCESS )
    {
      if (rtp_ctx.user_array[(uint32)config_session_msg->app_id ].\
        call_backs.handle_rtp_config_session_cb && g_handoffstate != HO_IN_PROGRESS)
      {
       qvp_rtp_response_handler(config_session_msg->app_id, ( qvp_rtp_stream_id_type ) 0, NULL, QVP_RTP_CONFIG_SESSION_RSP, QVP_RTP_SUCCESS, TRUE, 0);
      }
	  else if(g_handoffstate == HO_IN_PROGRESS)
	  {
		rtp_ctx.user_array[(uint32)config_session_msg->app_id].pending_queue[0].valid = FALSE;
	  	//QVP_RTP_ERR_0("qvp_rtp_configure_session_cmd: calling qvp_rtp_execute_pending_command");
	  	qvp_rtp_post_execute_pending_command(config_session_msg->app_id);
      }
    }
    else
    {    
      QVP_RTP_ERR_0(" qvp_rtp_configure_session_cmd :: \
                  could not configure session");
      if (rtp_ctx.user_array[(uint32)config_session_msg->app_id ].\
        call_backs.handle_rtp_config_session_cb)
      {                 
       qvp_rtp_response_handler(config_session_msg->app_id, ( qvp_rtp_stream_id_type ) 0, NULL, QVP_RTP_CONFIG_SESSION_RSP, QVP_RTP_WRONG_PARAM, TRUE, 0);
      }
    }
    return;
  }
  else 
  {
    QVP_RTP_ERR_0(" qvp_rtp_configure_session_cmd :: Failed beacuse \
                 wrong parameters");
    return;	
  }
} /* end of function   qvp_rtp_configure_session_cmd */


/*===========================================================================

FUNCTION QVP_RTP_VALIDATE_CONFIG_CMD

DESCRIPTION

  This function SYNCHRONOUSLY validates the payload configuration
  provided for the specified payload.
  
  This API is generally used when a stream is configured for the first time
  or to validate a standalone configuration
  
  RTP suggests a valid counter configuration for invalid parameters
  App can choose to use / ignore the counter configuration


DEPENDENCIES
  None

ARGUMENTS IN
  app_id       - application which is requesting for validation
  payload      - payload type to which the configuration pertains.
  payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the configuration is valid, 
                   counter_config is same as payld_config

                   If the given configuration is not successful,
                   and if an alternate value can be proposed by RTP
                   this contains the counter configuration proposed by RTP 

                   If the given configuration is not successful,
                   and if an alternate value cannot be proposed by RTP
                   counter configuration is set to invalid


RETURN VALUE
  QVP_RTP_SUCCESS   - The given Configuration is valid for the payload,
                      RTP configure will succeed for these settings
  
  QVP_RTP_ERR_FATAL - The given Configuration is not valid for the payload
                      RTP configure will fail for these settings
                      counter_config may conatin proposed valid config

SIDE EFFECTS
  None

===========================================================================*/
qvp_rtp_status_type qvp_rtp_validate_config_cmd
( 
  qvp_rtp_app_id_type          app_id,
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Check for params before we act on message 
    1) good app id and in range
    2) good pay load configuration
    3) good counter pay load configuration
  ------------------------------------------------------------------------*/
  if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
       ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) ||
       ( payld_config == NULL ) ||
       ( counter_config == NULL ) )
  {
    QVP_RTP_ERR_0( "qvp_rtp_validate_config_cmd:failed because \
                  wrong input params");

    return( QVP_RTP_WRONG_PARAM );
  }
  
  /*------------------------------------------------------------------------
    Prepare counter configuration. 

    Initially make this same as config proposed by app
    as you proceed overwrite invalid values proposed by app
    with default valid values

    counter_config has the basic capabilities.
    In future additional support can be added to come up with 
    an enhanced configuration
  ------------------------------------------------------------------------*/
  qvp_rtp_memscpy ( counter_config, sizeof ( qvp_rtp_payload_config_type ),payld_config, 
                      sizeof ( qvp_rtp_payload_config_type ) );

  status = qvp_rtp_validate_configure ( payld_config,counter_config ) ;

  return ( status ) ;

} /* end of function qvp_rtp_validate_config_cmd */


/*===========================================================================

FUNCTION QVP_RTP_VALIDATE_STREAM_CONFIG_CMD

DESCRIPTION

  This function SYNCHRONOUSLY validates the payload configuration provided &
  also validates if the current stream can support this configuration
  
  This API is used to validate configuration of an already configured stream
  where the configuration has to be validated with respect
  to the current stream configuration
  
  If the input configuration is not successful 
  then RTP suggests a valid counter configuration.
  
  App can choose to use / ignore the counter configuration

DEPENDENCIES

  None

ARGUMENTS IN

  app_id       - application which is requesting for validation
  stream_id    - stream which needs to be configured 
  payld_config - configuration which needs to be validated

ARGUMENTS OUT

  counter_config - If the configuration is valid, 
                   counter_config is same as payld_config

                   If the given configuration is not successful,
                   and if an alternate value can be proposed by RTP
                   this contains the counter configuration proposed by RTP 

                   If the given configuration is not successful,
                   and if an alternate value cannot be proposed by RTP
                   counter configuration is set to invalid


RETURN VALUE
  QVP_RTP_SUCCESS   - The given Configuration is valid for the payload,
                      RTP configure will succeed for these settings

  QVP_RTP_ERR_FATAL - The given Configuration is not valid for the payload
                      RTP configure will fail for these settings. 
                      counter_config conatins proposed valid configuration

SIDE EFFECTS
  None

===========================================================================*/
qvp_rtp_status_type  qvp_rtp_validate_stream_config_cmd
( 
  qvp_rtp_app_id_type          app_id,
  qvp_rtp_stream_id_type       stream_id, 
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config
)
{
  qvp_rtp_stream_type  *stream; 
  qvp_rtp_usr_ctx      *user_ctx; 
  qvp_rtp_status_type  status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Lock the stream resources .
    Do not let a asynchronous command message modify the stream values 
    while this validate synchronous api is  reading the stream values
  ------------------------------------------------------------------------*/
  if ( qvp_rtp_lock( ) != QVP_RTP_SUCCESS ) 
  {
    QVP_RTP_ERR_0( "qvp_rtp_validate_stream_config_cmd: failed at lock");
    return ( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Check for params before we act on message 
    1) good app id and in range
    2) good stream id for this app id 
    3) good pay load configuration
    4) good counter pay load configuration
  ------------------------------------------------------------------------*/
  if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
       ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) ||
       ( !rtp_ctx.user_array[ (uint32)app_id].valid ) ||
       
       ( stream_id > 
          ( qvp_rtp_stream_id_type )(uint32)rtp_ctx.user_array
                                   [ (uint32) app_id].num_streams) ||
       ( !rtp_ctx.user_array[ (uint32)app_id].
                             stream_array[ (uint32) stream_id].valid ) ||
  
       ( payld_config == NULL ) ||
       ( !payld_config->valid )  ||
       ( counter_config == NULL ) )
  {
    QVP_RTP_ERR_0( "qvp_rtp_validate_config_cmd:failed because \
                  wrong input params");

    
    if ( qvp_rtp_unlock( ) != QVP_RTP_SUCCESS ) /* unlock rtp resources  */
    {
      QVP_RTP_ERR_0( "qvp_rtp_validate_stream_config_cmd: failed at unlock");
      return ( QVP_RTP_ERR_FATAL );
    }


    return( QVP_RTP_WRONG_PARAM );
  }

  
  /*------------------------------------------------------------------------
    Prepare counter configuration. 

    Initially make this same as config proposed by app
    as you proceed overwrite invalid values proposed by app
    with default valid values

    counter_config has the basic capabilities.
    In future additional support can be added to come up with 
    an enhanced configuration
  ------------------------------------------------------------------------*/
  qvp_rtp_memscpy ( counter_config,sizeof ( qvp_rtp_payload_config_type ), payld_config, 
                      sizeof ( qvp_rtp_payload_config_type ) );

  /*------------------------------------------------------------------------
    Get stream reference from stream id
  ------------------------------------------------------------------------*/
  user_ctx = &rtp_ctx.user_array[ (uint32)app_id]; 
  stream = &user_ctx->stream_array[ (uint32) stream_id ];

  /*------------------------------------------------------------------------
    Validate the new configuration message with respect to the stream ctx
  ------------------------------------------------------------------------*/
  if ( qvp_rtp_validate_stream_configure( stream, 
         payld_config,counter_config ) != QVP_RTP_SUCCESS) 
  {
    QVP_RTP_ERR_0( "qvp_rtp_validate_stream_configure: failed ");
    status = QVP_RTP_ERR_FATAL;
  } 

  /*------------------------------------------------------------------------
    Populate the payload & channel direction, 
    these are mandatory to validate the message
  ------------------------------------------------------------------------*/
  if ( ! payld_config->payload_valid )
  {
    /*----------------------------------------------------------------------
      If the payload is not explicitly mentioned,
      it is assumed to be same as that of the stream
    ----------------------------------------------------------------------*/
    payld_config->payload_valid = TRUE;
    payld_config->payload = stream->payload_type;

    counter_config->payload_valid = TRUE;
    counter_config->payload = stream->payload_type;
  }

  if ( ! payld_config->chdir_valid )
  {
  /*------------------------------------------------------------------------
    If the payload is not explicitly mentioned,
    it is assumed to be same as that of the stream
  ------------------------------------------------------------------------*/
    payld_config->chdir_valid = TRUE;
    payld_config->ch_dir = stream->stream_type;

    counter_config->chdir_valid = TRUE;
    counter_config->ch_dir = stream->stream_type;
  }

  /*------------------------------------------------------------------------
     Validating this configuration message
  ------------------------------------------------------------------------*/
  if ( qvp_rtp_validate_configure ( payld_config, counter_config ) 
                                                 !=  QVP_RTP_SUCCESS ) 
  {
    QVP_RTP_ERR_0( "qvp_rtp_validate_configure: failed ");
    status = QVP_RTP_ERR_FATAL;
  }

  if ( qvp_rtp_unlock( ) != QVP_RTP_SUCCESS ) /* unlock rtp resources  */
  {
    QVP_RTP_ERR_0( "qvp_rtp_validate_stream_config_cmd: failed at unlock");
    return ( QVP_RTP_ERR_FATAL );
  }

  return ( status ) ;

} /* end of function qvp_rtp_validate_stream_config_cmd */


/*===========================================================================

FUNCTION QVP_RTP_VALIDATE_CONFIGURE

DESCRIPTION

  This function SYNCHRONOUSLY validates the payload configuration
  provided for the specified payload.
  
  This API is generally used when a stream is configured for the first time
  or to validate a standalone configuration (eg: to validate the config
  before opening the stream )
  
  RTP suggests a valid counter configuration for invalid parameters
  App can choose to use / ignore the counter configuration


DEPENDENCIES
  PT, connection IP, Channel direction are mandatory fields 

ARGUMENTS IN
  payload      - payload type to which the configuration pertains.
  payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the configuration is valid, 
                   counter_config is same as payld_config

                   If the given configuration is not successful,
                   and if an alternate value can be proposed by RTP
                   this contains the counter configuration proposed by RTP 

                   If the given configuration is not successful,
                   and if an alternate value cannot be proposed by RTP
                   counter configuration is set to invalid
                   

RETURN VALUE
  QVP_RTP_SUCCESS   - The given Configuration is valid for the payload,
                      RTP configure will succeed for these settings
  
  QVP_RTP_ERR_FATAL - The given Configuration is not valid for the payload
                      RTP configure will fail for these settings
                      counter_config may conatin proposed valid config

SIDE EFFECTS
  None

===========================================================================*/
qvp_rtp_status_type qvp_rtp_validate_configure
( 
  qvp_rtp_payload_config_type*  payld_config,
  qvp_rtp_payload_config_type*  counter_config
)
{
  qvp_rtp_profile_type *profile;
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  if ( !payld_config || !counter_config )
  {
    QVP_RTP_ERR_0( "validate_config:failed because \
                  wrong input params");

    return( QVP_RTP_WRONG_PARAM );
  }

  /*------------------------------------------------------------------------
    validate config message, payload, channel direction , connection ip
    Since this is a standalone config validation payload & 
    channel direction are mandatory
  ------------------------------------------------------------------------*/
  if ( !payld_config->valid )
  {
    status = QVP_RTP_ERR_FATAL;
    counter_config->valid = FALSE;
    QVP_RTP_ERR_0( "validate_config failed invalid config");
    return ( status ) ;
  }

  if ( ( payld_config->payload_valid ) &&
       ( payld_config->payload >= QVP_RTP_PYLD_NIL ) )
  {
    status = QVP_RTP_ERR_FATAL;
    QVP_RTP_ERR( "validate_config failed- invalid pyld params",
                 payld_config->valid, payld_config->payload_valid,
                 payld_config->payload );
    
  }


  if ( (payld_config->chdir_valid ) &&
       ( payld_config->ch_dir  > QVP_RTP_CHANNEL_FULL_DUPLEX ) ) 
  {
    status = QVP_RTP_ERR_FATAL;
    QVP_RTP_ERR_2( "validate_config:failed invalid ch params:\
                  flag:%d, dir:%d " ,
                  payld_config->chdir_valid , payld_config->ch_dir);
  }

  /*------------------------------------------------------------------------
    Without the payload & channel direction 
    there is no meaning in validating the message
    counter configuration cannot be proposed
  ------------------------------------------------------------------------*/
  if ( status != QVP_RTP_SUCCESS )
  {
    counter_config->valid = FALSE;
    return ( status ) ;
  }

  /*------------------------------------------------------------------------
     Validate Profile/Payload specific configuration 
  ------------------------------------------------------------------------*/
  profile = qvp_rtp_get_payld_profile( payld_config->payload ); 

  /*------------------------------------------------------------------------
    if the profile is not supported or the profile does not have validate 
    just return error.
  ------------------------------------------------------------------------*/
  if( !profile )
  {
    QVP_RTP_ERR_1( "validate_config:failed because \
                  invalid profile for pyld:%d",payld_config->payload);
    status = QVP_RTP_ERR_FATAL;
    counter_config->valid = FALSE;
    return ( status ) ;
  }


  if ( ( payld_config->rtcp_tx_config.rtcp_valid ) &&
       ( payld_config->rtcp_tx_config.rtcp_tx_enabled ) && 
       ( !payld_config->rtcp_tx_config.tx_rtcp_port ) )
  {
    counter_config->rtcp_tx_config.rtcp_tx_enabled = 0; 
    QVP_RTP_ERR_0( "validate_config fail:RTCP Tx but no port ");
    status = QVP_RTP_ERR_FATAL;
  }

  /*------------------------------------------------------------------------
    Validate the Rx plane settings
    check for any conflicting configuration requests
  ------------------------------------------------------------------------*/
  if ( payld_config->config_rx_params.valid ) 
  {
    if ( payld_config->ch_dir  == QVP_RTP_CHANNEL_OUTBOUND )
    {
      /*--------------------------------------------------------------------
        Rxr configuration provided for a sendonly channel, 
        throw an error & propose send only
      --------------------------------------------------------------------*/
      counter_config->config_rx_params.valid = 0;
      status = QVP_RTP_ERR_FATAL;
      QVP_RTP_ERR_0( "validate_config fail:Rx config requested \
                                                     for o/b ch ");
    }
    else 
    {
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
      if ( ( payld_config->config_rx_params.rx_rtp_param.jitter_valid ) && 
           ( payld_config->config_rx_params.rx_rtp_param.jitter_size 
                                              > QVP_RTP_MAX_JITTER_SIZE ) )
      {
        counter_config->config_rx_params.rx_rtp_param.jitter_size = 
                                                  QVP_RTP_MAX_JITTER_SIZE;
        QVP_RTP_ERR_2( "validate_config fail:large jitter size:%d,\
             proposed value:%d  ", 
             payld_config->config_rx_params.rx_rtp_param.jitter_size, 
             counter_config->config_rx_params.rx_rtp_param.jitter_size);
         status = QVP_RTP_ERR_FATAL;
      } /* end of jitter valid check*/
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
      /*--------------------------------------------------------------------
        validate profile specific configuration
      --------------------------------------------------------------------*/

       if ( !profile->validate_config_rx )
       {
         QVP_RTP_MSG_LOW_1( "validate_config:no fmtp-rx validate for PT:%d",
                                          payld_config->payload);
       } 
       else if ( profile->validate_config_rx ( payld_config,
                      counter_config ) != QVP_RTP_SUCCESS )
       {
         QVP_RTP_ERR_1( "validate_config profile rx fail for PT : %d",
                                     payld_config->payload);
         status = QVP_RTP_ERR_FATAL;
       } /* end of  if ( ( !profile->validate_config_rx ) */

    } /* end of else ( ch_dir == QVP_RTP_CHANNEL_OUTBOUND ) */

  }/* end of if ( payld_config->config_rx_params.valid ) */

  /*------------------------------------------------------------------------
    Validate the Tx plane settings
    check for any conflicting configuration requests
  ------------------------------------------------------------------------*/

  if ( payld_config->config_tx_params.valid ) 
  {
    if ( payld_config->ch_dir  == QVP_RTP_CHANNEL_INBOUND )
    {
      counter_config->config_tx_params.valid = 0;
      status = QVP_RTP_ERR_FATAL;
      QVP_RTP_ERR_0( "validate_config fail:Tx config requested for i/b ch ");
    }
    else 
    {
       if( payld_config->config_tx_params.tx_rtp_param.remote_port 
                                                                    & 0x1 )
       {
         counter_config->config_tx_params.tx_rtp_param.remote_port = 
             payld_config->config_tx_params.tx_rtp_param.remote_port + 1;
         status = QVP_RTP_ERR_FATAL;

         QVP_RTP_ERR_2( "validate_config fail:odd rem port:%d,\
             proposed port:%d ", 
             payld_config->config_tx_params.tx_rtp_param.remote_port, 
             counter_config->config_tx_params.tx_rtp_param.remote_port);
       } /* end of  if( ...remote_port */

       /*-------------------------------------------------------------------
         validate profile specific configuration .
       -------------------------------------------------------------------*/
       if ( !profile->validate_config_tx )
       {
         QVP_RTP_MSG_LOW_1( "validate_config:no fmtp rx validate for PT:%d",
                                           payld_config->payload);
       } 
       else if ( profile->validate_config_tx( payld_config, counter_config )
                                                   != QVP_RTP_SUCCESS ) 
       {
         status = QVP_RTP_ERR_FATAL;
         QVP_RTP_ERR_1( "validate_config:profile tx fail for PT : %d",
                                          payld_config->payload);
       } /* end of  if ( profile->validate_config_tx )*/

    }/* end of  else */
  } /* end of if ( payld_config->config_tx_params.valid ) */

  return ( status ) ;

} /* end of function qvp_rtp_validate_configure*/


/*===========================================================================

FUNCTION QVP_RTP_VALIDATE_STREAM_CONFIGURE

DESCRIPTION

  This function SYNCHRONOUSLY validates the payload configuration
  provided for the specified payload.
  
  This API is generally used when a  configuration has to be done 
  on top of an existing stream
  
  RTP suggests a valid counter configuration for invalid parameters
  App can choose to use / ignore the counter configuration


DEPENDENCIES
  None

ARGUMENTS IN
  payld_config - configuration which needs to be validated
  stream_id    - stream which needs to be configured 


ARGUMENTS OUT
  counter_config - If the configuration is valid, 
                   counter_config is same as payld_config

                   If the given configuration is not successful,
                   and if an alternate value can be proposed by RTP
                   this contains the counter configuration proposed by RTP 

                   If the given configuration is not successful,
                   and if an alternate value cannot be proposed by RTP
                   counter configuration is set to invalid
                   

RETURN VALUE
  QVP_RTP_SUCCESS   - The given Configuration is valid for the payload,
                      RTP configure will succeed for these settings
  
  QVP_RTP_ERR_FATAL - The given Configuration is not valid for the payload
                      RTP configure will fail for these settings
                      counter_config may conatin proposed valid config

SIDE EFFECTS
  None

===========================================================================*/
qvp_rtp_status_type qvp_rtp_validate_stream_configure
( 
  qvp_rtp_stream_type          *stream,
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  if ( ( stream == NULL ) ||
       ( payld_config == NULL ) ||
       ( counter_config == NULL ) )
  {
    QVP_RTP_ERR_0( "validate_stream:failed because \
                  wrong input params");

    return( QVP_RTP_WRONG_PARAM );
  }


  /*------------------------------------------------------------------------
    Throw an error if invalid channel direction is requested.
   -----------------------------------------------------------------------*/
  if ( ( payld_config->chdir_valid ) && 
       ( payld_config->ch_dir  > QVP_RTP_CHANNEL_FULL_DUPLEX ) )
  {
    counter_config->ch_dir  =  stream->stream_type;
    status = QVP_RTP_ERR_FATAL;
    QVP_RTP_ERR_2( "validate_stream fail:mismatch \
                            in config ch dir:%d & stream dir:%d ", 
                            payld_config->ch_dir ,stream->stream_type);
  }


  if ( ( payld_config->rtcp_tx_config.rtcp_valid ) &&
       ( payld_config->rtcp_tx_config.rtcp_tx_enabled ) && 
       ( ! stream->rtcp_hdl ) )
  {
    counter_config->rtcp_tx_config.rtcp_valid = FALSE;
    status = QVP_RTP_ERR_FATAL;
    QVP_RTP_ERR_0( "validate_stream fail:RTCP \
                        config rquested  for stream wout rtcp hdl");
  }

  /*------------------------------------------------------------------------
    If there are any configurations that we do not want to allow 
    when the stream is active or when the call is ongoing, add it here .
    These will be restrictions specific to our implementation
  ------------------------------------------------------------------------*/

  return ( status ) ;

} /* end of function qvp_rtp_validate_steam_configure*/




/*===========================================================================

FUNCTION QVP_RTP_RESET_CMD 

DESCRIPTION
  Processes the application command to reset an  RTP session. 

DEPENDENCIES
  None

PARAMETERS
    reset_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_reset_cmd
(
  qvp_rtp_reset_msg_type *reset_msg
)
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx; 
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/



  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
  ------------------------------------------------------------------------*/
  if ( ( reset_msg != NULL ) && 
       ( reset_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( reset_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
       ( reset_msg->stream_id < ( qvp_rtp_stream_id_type )
         (uint32)rtp_ctx.user_array[ (uint32)   reset_msg->app_id].num_streams) &&
       rtp_ctx.user_array[ (uint32)reset_msg->app_id].valid && 
       rtp_ctx.user_array[ (uint32)reset_msg->app_id]. \
       stream_array[ (uint32) reset_msg->stream_id].valid  )
       
  {
    user_ctx = &rtp_ctx.user_array[ (uint32)reset_msg->app_id]; 
    stream   = &user_ctx->stream_array[ (uint32) reset_msg->stream_id ];

    /*----------------------------------------------------------------------
      If there is an outbound component reset the stream
    ----------------------------------------------------------------------*/
    if( stream->ob_stream.valid )
    {
      qvp_rtp_reset_nw_ob( stream->ob_stream.sock_id );
    }

    /*----------------------------------------------------------------------
      If there is inbound component and there is a de-jitter buffer. Close 
      and reopen the de-jitter buffer. 
      Reset the inbound socket state
    ----------------------------------------------------------------------*/
    if( stream->ib_stream.valid )
    {
      qvp_rtp_reset_nw_ib( stream->ib_stream.sock_id );
    }
#ifdef FEATURE_IMS_RTP_JITTERBUFFER	
    if( stream->ib_stream.valid && stream->ib_stream.jitter_size &&
        stream->ib_stream.jitter_queue )
    {
      qvp_rtp_jb_close_jitter_buffer( stream->ib_stream.jitter_queue );
      if( qvp_rtp_alloc_jitter_buffer( stream, 
                                      stream->ib_stream.jitter_size) 
                                      != QVP_RTP_SUCCESS )
      {
        status = QVP_RTP_ERR_FATAL;
      }
    }
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
    /*----------------------------------------------------------------------
       If there is an RTCP handle reset it.
    ----------------------------------------------------------------------*/
    if( ( status == QVP_RTP_SUCCESS ) && ( stream->rtcp_hdl ) )
    {
      qvp_rtcp_reset_session( stream->rtcp_hdl );
    }

    /*----------------------------------------------------------------------
      Close and reopen the profile. So that profile is clean regardless 
      type of action which goes on.
    ----------------------------------------------------------------------*/
    if( ( status == QVP_RTP_SUCCESS ) && 
        ( stream->profile && stream->prof_handle ) )
    {
      stream->profile->close_profile( stream->prof_handle );

	  if (stream->payload_type == QVP_RTP_PYLD_TXT)
	  {
		  rtp_codec_txt_rx_ctx_uninit(stream);
	  }

      if( stream->profile->open_profile( &stream->prof_handle, 
                                            stream ) != QVP_RTP_SUCCESS ) 
      {
        status = QVP_RTP_ERR_FATAL;
      }
    }


    /*----------------------------------------------------------------------
      Reset the session state to inactive.
    ----------------------------------------------------------------------*/
    stream->state = QVP_RTP_SESS_INACTIVE;

    /*----------------------------------------------------------------------
      Reset the stream state to inactive.
    ----------------------------------------------------------------------*/
    stream->stream_state = INACTIVE;


    /*----------------------------------------------------------------------
      Call the call back with success
    ----------------------------------------------------------------------*/
    if( user_ctx->call_backs.reset_cb )
    {
      user_ctx->call_backs.reset_cb( reset_msg->app_id,
                                      stream->app_data, status,
                                      reset_msg->stream_id );
    }

  }
  else
  {

    /*----------------------------------------------------------------------
      We got an invalid message throw an error upwards
    ----------------------------------------------------------------------*/
    /*----------------------------------------------------------------------
      If we have a valid app id then we should call the close_cb and let the
      user know the wrong doing. 
    ----------------------------------------------------------------------*/
    if ( reset_msg && reset_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
         && ( reset_msg->app_id < ( qvp_rtp_app_id_type ) 
              (uint32)rtp_ctx.num_users) 
      && rtp_ctx.user_array[ (uint32) reset_msg->app_id ].valid )
    {
      /*--------------------------------------------------------------------
        If there is a registered open cb then call it. 
      --------------------------------------------------------------------*/
      if ( rtp_ctx.user_array[ (uint32) reset_msg->app_id ]. \
           call_backs.reset_cb )
      {
        rtp_ctx.user_array[ (uint32) reset_msg->app_id ]. 
                                call_backs.reset_cb( reset_msg->app_id,
             rtp_ctx.user_array[ (uint32) reset_msg->app_id ]. \
             stream_array[ ( uint32 ) reset_msg->stream_id].app_data,  
             QVP_RTP_WRONG_PARAM, reset_msg->stream_id );
      }
    }

  }


} /* end of function qvp_rtp_reset_cmd */

/*===========================================================================

FUNCTION QVP_RTP_CLOSE_CMD 

DESCRIPTION
  Processes the application command to close an already opened RTP channel. 

DEPENDENCIES
  None

PARAMETERS
    close_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_close_cmd
(
  qvp_rtp_close_msg_type *close_msg
)
{
  qvp_rtp_stream_type *stream_to_close=NULL; /* for inderection simplicity */
  qvp_rtp_status_type status;
  qvp_rtp_usr_ctx *usr_ctx=NULL; 
  qvp_rtp_red_end_event red_end_event;
  uint32 tx_ssrc=0,rx_ssrc=0,seq_no=0;int8 ret=0;
  qvp_rtp_ds_ob_sock_entry_type *sock_id=NULL;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
      Check for valid arguments
  ------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
  ------------------------------------------------------------------------*/
  if ( ( close_msg != NULL ) && 
       ( close_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( close_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
       ( close_msg->stream_id < ( qvp_rtp_stream_id_type )
         (uint32)rtp_ctx.user_array[ (uint32) close_msg->app_id].num_streams) )
  {

    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) close_msg->app_id ]; 

    /*----------------------------------------------------------------------
        extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream_to_close =  
    &( usr_ctx->stream_array[ (uint32) close_msg->stream_id ] ); 
	if(!stream_to_close)
	{
		QVP_RTP_ERR_0( "RTP_CLOSE stream is NULL ");
    return;
	}
	 sock_id = ( qvp_rtp_ds_ob_sock_entry_type * ) stream_to_close->ob_stream.sock_id; 
  if(sock_id!=NULL)
  {
	sock_id->first_red=FALSE;
  }

	if(stream_to_close->remote_rat_type==QVP_RTP_RAT_WLAN)
	{
	ret=qvp_rtp_get_rtp_tx_ssrc( &stream_to_close->rtp, &tx_ssrc );
	ret=qvp_rtp_get_rtp_rx_ssrc( &stream_to_close->rtp, &rx_ssrc );
	red_end_event.tx_ssrc=tx_ssrc;
        red_end_event.rx_ssrc=rx_ssrc;
        ret=qvp_rtp_get_rtp_seq_number( &stream_to_close->rtp, &seq_no );
        red_end_event.rtp_seq_no=seq_no;
        red_end_event.rtp_timestamp=stream_to_close->last_tstamp;
		   event_report_payload(
          (event_id_enum_type) EVENT_IMS_RTP_REDUN_END,
          sizeof(qvp_rtp_red_end_event),
          (void *)&red_end_event);
  }
    if(stream_to_close->rtp_link_alive_timer)
    {
      qvp_rtp_free_timer( stream_to_close->rtp_link_alive_timer );
      stream_to_close->rtp_link_alive_timer = 0;
    }
		
		
    if(stream_to_close->rtp_keep_alive_timer)
    {
      qvp_rtp_free_timer( stream_to_close->rtp_keep_alive_timer );
      stream_to_close->rtp_keep_alive_timer = 0;
    }
    if(stream_to_close->rtcp_keep_alive_timer)
    {
      qvp_rtp_free_timer( stream_to_close->rtcp_keep_alive_timer );
      stream_to_close->rtcp_keep_alive_timer = 0;
    }

	if(stream_to_close->is_srtp_enabled)
	{
	  qvp_srtp_deinit_crypto(stream_to_close->srtp_ctx.cipher_handle, stream_to_close->srtp_ctx.auth_handle);
	  stream_to_close->srtp_ctx.cipher_handle = 0;
	  stream_to_close->srtp_ctx.auth_handle = 0;
	}
    /*----------------------------------------------------------------------
        Try closing the stream 
    ----------------------------------------------------------------------*/
    status = qvp_rtp_dealloc_close_stream(  stream_to_close,
                                            usr_ctx->nw_hdl );
    
    if ( status == QVP_RTP_SUCCESS )
    {
          
      /*--------------------------------------------------------------------
          Flag the channel as free 
      --------------------------------------------------------------------*/
      stream_to_close->valid = FALSE;

      /*----------------------------------------------------------------------
      Reset the stream state to inactive.
      ----------------------------------------------------------------------*/
      stream_to_close->stream_state = INACTIVE;

      stream_to_close->dtmf_endbit = 0;
	  stream_to_close->last_dtmf_dur = 0;
      stream_to_close->dtmf_clock_rate = 0;
      stream_to_close->T_last = 0;
      stream_to_close->T_sent = 0;
	  stream_to_close->target_tstamp = 0;
      stream_to_close->rtp_link_alive_time = 0;
	    memset(&(stream_to_close->rtp),0,sizeof(qvp_rtp_ctx_type));
      stream_to_close->rtcp_hdl = NULL;
      /*--------------------------------------------------------------------
         Call the close call back if the function is registered with the
         RTP 
      --------------------------------------------------------------------*/
      if ( usr_ctx->call_backs.close_cb && !clear_all_session_req && (g_handoffstate != HO_IN_PROGRESS))
      {
        qvp_rtp_response_handler(close_msg->app_id, close_msg->stream_id,  stream_to_close->app_data, QVP_RTP_CLOSE_RSP, QVP_RTP_SUCCESS, TRUE, 0);
      }

      /*--------------------------------------------------------------------
      Decrement the reference count here of the HK timer, if this reached zero 
      than this timer will not be started again in the callback
      --------------------------------------------------------------------*/
      if(rtp_ctx.hk_timer_ref_count)
        rtp_ctx.hk_timer_ref_count--;
      
      QVP_RTP_MSG_HIGH_0_MEM( " Successfully closed an RTP channel \r\n");
	  if(g_handoffstate == HO_IN_PROGRESS)
	  {
	    usr_ctx->pending_queue[0].valid = FALSE;
	  	qvp_rtp_post_execute_pending_command(close_msg->app_id);
	  }
	  
      return;


    }
    else
    {
      
      if ( usr_ctx->call_backs.close_cb && !clear_all_session_req && (g_handoffstate != HO_IN_PROGRESS))
      {
        /*------------------------------------------------------------------
        Notify user if we could not close it
        ------------------------------------------------------------------*/
        qvp_rtp_response_handler(close_msg->app_id, close_msg->stream_id,  stream_to_close->app_data, QVP_RTP_CLOSE_RSP, status, TRUE, 0);
      }
    }

  }
  else
  {

    /*----------------------------------------------------------------------
      If we have a valid app id then we shoulc call the close_cb and let the
      user know the wrong doing. 
    ----------------------------------------------------------------------*/
    if ( close_msg && close_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
         && ( close_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users) )
    {
      /*--------------------------------------------------------------------
        If there is a registered open cb then call it. 
      --------------------------------------------------------------------*/
      if ( rtp_ctx.user_array[ (uint32) close_msg->app_id ]. \
           call_backs.close_cb && !clear_all_session_req && (g_handoffstate != HO_IN_PROGRESS) )
      {
           qvp_rtp_response_handler(close_msg->app_id, close_msg->stream_id,  rtp_ctx.user_array[ (uint32) close_msg->app_id ].stream_array[  \
          (uint32) close_msg->stream_id ].app_data, QVP_RTP_CLOSE_RSP, QVP_RTP_WRONG_PARAM, TRUE, 0);
      }
    }

    QVP_RTP_ERR_0( "qvp_rtp_close_cmd ::  open failed because \
                  wrong params \r\n");
    return;
  }

  return;

} /* end of function qvp_rtp_close_cmd */ 

/*===========================================================================

FUNCTION QVP_RTP_DEREGISTER_APP_CMD 

DESCRIPTION
  Processes the application command to deregister an app.

DEPENDENCIES
  None

PARAMETERS
  deregister_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_deregister_app_cmd
(
  qvp_rtp_deregister_msg_type  *deregister_msg
)
{
  uint8 stream_no = 0;
  qvp_rtp_usr_ctx *usr_ctx; 
  qvp_rtp_close_msg_type close_msg; /* to be used for closing channels */
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
  ------------------------------------------------------------------------*/

  if( ( deregister_msg == NULL ) || 
      ( deregister_msg->app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
      ( deregister_msg->app_id > 
                              ( qvp_rtp_app_id_type )(uint32)rtp_ctx.num_users ) )
  {
    QVP_RTP_ERR_0( "Deregister for invalid params ");
    return;
  }
  
  /*------------------------------------------------------------------------
      Looks like we have a valid handle . extract the user context 
  ------------------------------------------------------------------------*/
  usr_ctx = &rtp_ctx.user_array[ (uint32) deregister_msg->app_id ];

  /*----------------------------------------------------------------------
    check if this is a valid user
    Shutdown calls deregister for the max users supported by RTP
    out of which not all might be active.
  ----------------------------------------------------------------------*/
  if ( usr_ctx->valid != TRUE )
  {
    QVP_RTP_MSG_HIGH_1( "Deregister for an invalid app_id %d", 
                                      deregister_msg->app_id);
    return;
  }
  if ( qvp_rtp_buf_deinit(QVP_RTP_AUDIO) != QVP_RTP_SUCCESS )
  {
	QVP_RTP_ERR_0( " could not deinit the buffer management \r\n");
  }
  if ( qvp_rtp_buf_deinit(QVP_RTP_VIDEO) != QVP_RTP_SUCCESS )
  {
	QVP_RTP_ERR_0( " could not deinit the buffer management \r\n");
  }
   if ( qvp_rtp_buf_deinit(QVP_RTP_TEXT) != QVP_RTP_SUCCESS )
  {
	QVP_RTP_ERR_0( " could not deinit the buffer management \r\n");
  }

  
  /*free srtp master key */
		qvp_rtp_free(usr_ctx->master_key);		
		usr_ctx->master_key = NULL;
		
  /*------------------------------------------------------------------------
      close all the open streams of the app
  ------------------------------------------------------------------------*/  
  for( stream_no = 0; stream_no < usr_ctx->num_streams; stream_no++ )
  {
    /*----------------------------------------------------------------------
        See if this channel is open . If yes close it now.
    ----------------------------------------------------------------------*/
    if( usr_ctx->stream_array[ stream_no ].valid )
    {
      
      close_msg.app_id = deregister_msg->app_id;
      close_msg.stream_id  = usr_ctx->stream_array[stream_no].str_id;
      /*--------------------------------------------------------------------
        close the dynamically allocated sockets associated with the app
      --------------------------------------------------------------------*/
      qvp_rtp_close_cmd( &close_msg );
    }

  }/* end of for( stream_no = 0 )*/

  /*------------------------------------------------------------------------
    deregister nw closing the reserved sockets associated with the app
  ------------------------------------------------------------------------*/
  if ( qvp_rtp_deregister_app_nw( usr_ctx->nw_hdl ) !=
          QVP_RTP_SUCCESS )
  {
    QVP_RTP_ERR_1( "qvp_rtp_deregister_cmd ::failed in deregistering nw \
                          for appid %d ", deregister_msg->app_id);
  }

  /*------------------------------------------------------------------------
      free the user entry 
  ------------------------------------------------------------------------*/
  usr_ctx->valid = FALSE;
  qvp_rtp_service_notify_dpl_deinit();
  qvp_rtp_imsrtp_service_notifier_de_init();
  return;

}/* end of function qvp_rtp_deregister_app_cmd()*/


/*===========================================================================

FUNCTION QVP_RTP_SEND_CMD 

DESCRIPTION
  Processes the application command to RTP payload through an already 
  opened channel. 

DEPENDENCIES
  None

PARAMETERS
    send_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_send_cmd
(
  qvp_rtp_send_msg_type *send_msg 
)
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity    */
  qvp_rtp_usr_ctx *usr_ctx;   /* accessor for the user context */
  qvp_rtp_wall_clk_type wal_clk;
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
    4) stream is o/b or full duplex
    5) good packet
    6) session not in reset mode. 
  ------------------------------------------------------------------------*/
  if ( ( send_msg != NULL ) &&
       ( send_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( send_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
       ( send_msg->stream_id < ( qvp_rtp_stream_id_type )
         (uint32)rtp_ctx.user_array[ (uint32)   send_msg->app_id].num_streams) &&
       rtp_ctx.user_array[ (uint32)send_msg->app_id]. \
       stream_array[ (uint32) send_msg->stream_id].ob_stream.valid &&
       ( rtp_ctx.user_array[ (uint32)send_msg->app_id]. \
       stream_array[ (uint32) send_msg->stream_id].state == 
         QVP_RTP_SESS_ACTIVE )&&
       ( send_msg->pkt != NULL ) )
  {

    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) send_msg->app_id ]; 

    /*----------------------------------------------------------------------
      extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream    =  &( usr_ctx->stream_array[ (uint32) send_msg->stream_id ] ); 

    stream->T_sent = send_msg->pkt->tstamp;
	qvp_rtp_time_get_ms(&stream->TS_sent);
	//QVP_RTP_ERR_2(" qvp_rtp_send_cmd:: ts:%u rtp ts:%d \r\n",stream->TS_sent,stream->T_sent);
    stream->ob_stream.pkt_cnt++;

    /*----------------------------------------------------------------------
       When we get Wall clock we will trigger an SR into RTCP
    ----------------------------------------------------------------------*/
    if( send_msg->wl_cl.valid )
    {
      wal_clk.sys_time = send_msg->wl_cl.wl_clock;

      wal_clk.tstamp = send_msg->pkt->tstamp;
      qvp_rtcp_report_tx_wall_clock( stream->rtcp_hdl, &wal_clk, stream->RTP_sys_time );
    }

    /*----------------------------------------------------------------------
      storing the wall clock time in the packet
    ----------------------------------------------------------------------*/
    send_msg->pkt->sys_time = send_msg->wl_cl.wl_clock;



	 if(!stream->rtp_first_pkt_tstamp)
	{
		uint64 first_pkt_tstamp = 0;
		qvp_rtp_time_get_ms(&first_pkt_tstamp);
		stream->rtp_first_pkt_tstamp = (uint32)(first_pkt_tstamp);
		QVP_RTP_MSG_LOW_1("Rtp_first_packet_timestamp = %d\n",stream->rtp_first_pkt_tstamp);
	}
    /*----------------------------------------------------------------------
      Send it to the profile 
    ----------------------------------------------------------------------*/
    if ( stream->profile->send_profile( stream->prof_handle, stream, 
                                        send_msg->pkt ) != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_0(" qvp_rtp_send_cmd:: profile did not send pkt \r\n");

    }
  }
  else
  {
    /*----------------------------------------------------------------------
         Need to free the buffer .. We do it even for wrong parameters
    ----------------------------------------------------------------------*/
    if( send_msg && send_msg->pkt )
    {
      qvp_rtp_free_buf ( send_msg->pkt );
    }
    QVP_RTP_MSG_HIGH_0( " Send message had unacceptable params-pkt dropped");
  }


  return;

} /* end of function qvp_rtp_send_cmd */


/*===========================================================================

FUNCTION QVP_RTP_SEND_DTMF_CMD 

DESCRIPTION
  Processes the application command to RTP payload through an already 
  opened channel. 

DEPENDENCIES
  None

PARAMETERS
    send_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_send_dtmf_cmd
(
  qvp_rtp_send_msg_type *send_msg 
)
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity    */
  qvp_rtp_usr_ctx *usr_ctx;   /* accessor for the user context */
  qvp_rtp_wall_clk_type wal_clk;
  uint8 org_tx_payload;
  uint16 dtmf_dur = 0;
  uint64 current_time;
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
    4) stream is o/b or full duplex
    5) good packet
    6) session not in reset mode. 
  ------------------------------------------------------------------------*/
  if ( ( send_msg != NULL ) &&
       ( send_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( send_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
       ( send_msg->stream_id < ( qvp_rtp_stream_id_type )
         (uint32)rtp_ctx.user_array[ (uint32)   send_msg->app_id].num_streams) &&
       rtp_ctx.user_array[ (uint32)send_msg->app_id]. \
       stream_array[ (uint32) send_msg->stream_id].ob_stream.valid &&
       ( rtp_ctx.user_array[ (uint32)send_msg->app_id]. \
       stream_array[ (uint32) send_msg->stream_id].state == 
         QVP_RTP_SESS_ACTIVE )&&
       ( send_msg->pkt != NULL ) )
  {
    QVP_RTP_MSG_HIGH_0(" sending DTMF \r\n");
    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) send_msg->app_id ]; 

    /*----------------------------------------------------------------------
      extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream    =  &( usr_ctx->stream_array[ (uint32) send_msg->stream_id ] ); 

	{
		uint8 *temp_data = send_msg->pkt->data + send_msg->pkt->head_room;
		if(!temp_data)
		  return;
		temp_data++;
		stream->dtmf_endbit = b_unpackb(temp_data,0,1);
		temp_data++;
		dtmf_dur = b_unpackw(temp_data,0,16);
 
	}
 if(stream->send_dtmf == FALSE)
	{
      if( send_msg && send_msg->pkt )
      {
        qvp_rtp_free_buf ( send_msg->pkt );
      }
	  QVP_RTP_ERR_0("Cant send dtmf packets when TX is paused");
	  return;
	}

    stream->ob_stream.pkt_cnt++;
	qvp_rtp_time_get_ms(&current_time);

    if(stream->dtmf_clock_rate == QVP_RTP_AMR_WB_CLOCK_RATE)
    {
		
		if(stream->TS_sent && ((current_time - stream->TS_sent) > 40))
		{
		   QVP_RTP_ERR_1("send dtmf: last pkt was sent %d ms before. Adjusting time.", (current_time - stream->TS_sent));
		   stream->T_sent = stream->T_sent + ((int)(current_time - stream->TS_sent)/20) * 320;
		}
		else
		{    
      stream->T_sent += 320;
	  stream->target_tstamp = stream->T_sent + 320;
    }
		
		qvp_rtp_time_get_ms(&stream->TS_sent);
		//QVP_RTP_ERR_2(" qvp_rtp_send_cmd:: ts:%u rtp ts:%d \r\n",stream->TS_sent,stream->T_sent);
    }
    else if(stream->dtmf_clock_rate == QVP_RTP_AMR_NB_CLOCK_RATE)
    {
		if(stream->TS_sent && ((current_time - stream->TS_sent) > 40))
		{
		   QVP_RTP_ERR_1("send dtmf: last pkt was sent %d ms before. Adjusting time.", (current_time - stream->TS_sent));
		   stream->T_sent = stream->T_sent + ((int)(current_time - stream->TS_sent)/20) * 160;
		}    
		else
    {
      stream->T_sent += 160;
	  stream->target_tstamp = stream->T_sent + 160;
    }
	 	qvp_rtp_time_get_ms(&stream->TS_sent);
		//QVP_RTP_ERR_2(" qvp_rtp_send_cmd:: ts:%u rtp ts:%d \r\n",stream->TS_sent,stream->T_sent);
    }

	 /*----------------------------------------------------------------------
     Updating packet timestamp with T_sent
	 This will directly work in early media case.
	 ----------------------------------------------------------------------*/
    if(send_msg->pkt->marker_bit)
    {
      send_msg->pkt->tstamp = stream->T_sent;
      stream->dtmf_send_tstamp = stream->T_sent;
    }
    else
    {
      send_msg->pkt->tstamp = stream->dtmf_send_tstamp;
	  if(dtmf_dur && stream->last_dtmf_dur && dtmf_dur < stream->last_dtmf_dur)
	  {
        send_msg->pkt->tstamp = stream->T_sent;
        stream->dtmf_send_tstamp = stream->T_sent;
	  }
	  stream->last_dtmf_dur = dtmf_dur;
    }
    /*----------------------------------------------------------------------
       When we get Wall clock we will trigger an SR into RTCP
    ----------------------------------------------------------------------*/
    if( send_msg->wl_cl.valid )
    {
      wal_clk.sys_time = send_msg->wl_cl.wl_clock;
      wal_clk.tstamp = send_msg->pkt->tstamp;
      qvp_rtcp_report_tx_wall_clock( stream->rtcp_hdl, &wal_clk, stream->RTP_sys_time );
    }

    {
      if(stream->stream_state == ACTIVE)
        stream->stream_state = INACTIVE_TX;
      else if(stream->stream_state == INACTIVE_RX)
        stream->stream_state = INACTIVE;
    }

    QVP_RTP_MSG_HIGH( " Sending DTMF Len = %d, timestamp = %d, \
                       marker = %d", send_msg->pkt->len, send_msg->pkt->tstamp, 
                       send_msg->pkt->marker_bit );

    org_tx_payload = stream->rtp.tx_payload_type;
    stream->rtp.tx_payload_type = send_msg->pkt->rtp_pyld_type;
    /*----------------------------------------------------------------------
        Form the RTP header 
    ----------------------------------------------------------------------*/
    qvp_rtp_pack( &stream->rtp, send_msg->pkt );

    stream->rtp.tx_payload_type = org_tx_payload;

    /*------------------------------------------------------------------------
          Send the packet to the NW 
    ------------------------------------------------------------------------*/
    if( qvp_rtp_send_nw( stream->ob_stream.sock_id, 
                           send_msg->pkt ) != QVP_RTP_SUCCESS )
    {
      qvp_rtp_free_buf( send_msg->pkt );
      QVP_RTP_ERR_0(" &&& NW send failed on the packet \r\n");
      return; 

    }
    else
    {
      QVP_RTP_MSG_HIGH_0(" Pkt sent over the n/w \r\n");
      /*--------------------------------------------------------------------
        Report a successful transmission to RTCP
      --------------------------------------------------------------------*/
      qvp_rtcp_report_tx( stream->rtcp_hdl, send_msg->pkt->len, 
                  send_msg->pkt->tstamp );
    }

    if(stream->dtmf_endbit)
    {
		if(stream->dtmf_end_bit_pkt_count == 1)
		{
		  if(stream->stream_state == INACTIVE)
			stream->stream_state = INACTIVE_RX;
		  else if(stream->stream_state == INACTIVE_TX)
			stream->stream_state = ACTIVE;	
		  stream->last_dtmf_dur = 0;
		}
		if(stream->dtmf_end_bit_pkt_count)
			stream->dtmf_end_bit_pkt_count--;
	    else
			stream->dtmf_end_bit_pkt_count = 2;		
	}

  }/*end of if ( ( send_msg != NULL ) */
  else
  {
    /*----------------------------------------------------------------------
         Need to free the buffer .. We do it even for wrong parameters
    ----------------------------------------------------------------------*/
    if( send_msg && send_msg->pkt )
    {
      qvp_rtp_free_buf ( send_msg->pkt );
    }
    QVP_RTP_MSG_HIGH_0( " Send message had unacceptable params-pkt dropped");
  }

  return;

} /* end of function qvp_rtp_send_cmd */

/*===========================================================================

FUNCTION QVP_RTP_ENABLE_LS_CMD

DESCRIPTION

Process the application command  to lip sync between an already opened 
audio and vieo channel.

DEPENDENCIES
  None

PARAMETERS
    send_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_enable_ls_cmd
( 
  qvp_rtp_enable_ls_msg_type *ls_msg
)
{

  qvp_rtp_stream_type *aud_stream = NULL; /* for inderection simplicity */
  qvp_rtp_usr_ctx     *usr_ctx = NULL;    /* accessor for the user context
                                           */ 
  qvp_rtp_status_type
                  status = QVP_RTP_ERR_FATAL  ; /* used as status flag */
/*------------------------------------------------------------------------*/
  

  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id for video in range
    4) stream id for audio in range
  ------------------------------------------------------------------------*/
  if ( ( ls_msg != NULL ) &&
       ( ls_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( ls_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
       ( ls_msg->audio_stream < ( qvp_rtp_stream_id_type ) 
         (uint32)rtp_ctx.user_array[ (uint32)   ls_msg->app_id].num_streams) &&
       ( ls_msg->video_stream < ( qvp_rtp_stream_id_type ) 
         (uint32)rtp_ctx.user_array[ (uint32)   ls_msg->app_id].num_streams )
        )
  {

    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) ls_msg->app_id ]; 

    /*----------------------------------------------------------------------
      extract the video stream and audio stream from the user context 
    ----------------------------------------------------------------------*/


    aud_stream = 
      &( usr_ctx->stream_array[ (uint32) ls_msg->audio_stream ] ); 

    
#ifdef FEATURE_IMS_RTP_JITTERBUFFER   
{ 
    /*----------------------------------------------------------------------
      1) valid video ib handle
      2) valid jb size for video
      3) valid jb handle for video 
      4) valid audio ib handle
      5) valid jb size for audio
      6) valid jb handle for audio 
      7) jb sizes are equal for audio and video 
    ----------------------------------------------------------------------*/
	  qvp_rtp_stream_type *vid_stream = NULL; /* for inderection simplicity */
	      vid_stream    =  
      &( usr_ctx->stream_array[ (uint32) ls_msg->video_stream ] ); 
    if( vid_stream->ib_stream.valid &&  /* valid vid inboumd */
        vid_stream->ib_stream.jitter_size &&  /* valid vid jitter size */
        vid_stream->ib_stream.jitter_queue && /* valid vid jb handle */ 
        aud_stream->ib_stream.valid &&        /* valid aud inbound */
        aud_stream->ib_stream.jitter_size &&  /* valid aud jitter size */
        aud_stream->ib_stream.jitter_queue && /* valid aud jb handle */
        ( aud_stream->ib_stream.jitter_size ==  
          vid_stream->ib_stream.jitter_size  ) ) /* jb sizes are equal */
    {

      QVP_RTP_MSG_LOW_2(" enable_ls aud_q = %p, vid_q = %p", 
                       aud_stream->ib_stream.jitter_queue,
                       vid_stream->ib_stream.jitter_queue);

      /*--------------------------------------------------------------------
        Now we can try setting the lipsync inside the jb layer
      --------------------------------------------------------------------*/
      status =  qvp_rtp_jitter_buffer_enable_ls( 
                                      aud_stream->ib_stream.jitter_queue,
                                      vid_stream->ib_stream.jitter_queue );
    
    }
    else
    {
      status = QVP_RTP_WRONG_PARAM;
    }
}
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/ 

    
  }
  else
  {
    
    status = QVP_RTP_WRONG_PARAM;
    
  }

  /*------------------------------------------------------------------------
        Call the appropriate application call back
  ------------------------------------------------------------------------*/
  if( usr_ctx && usr_ctx->call_backs.lipsync_cb )
  {
    
    usr_ctx->call_backs.lipsync_cb( ls_msg->app_id,
                                    aud_stream->app_data, 
                                    status, 
                                    ls_msg->audio_stream, 
                                    ls_msg->video_stream );
    
  } 


  return;
    
  
} /* end of function qvp_rtp_enable_ls_cmd */ 

/*===========================================================================

FUNCTION QVP_RTP_DISABLE_LS_CMD

DESCRIPTION

Process the application command  to disable lip sync between an already 
opened audio and vieo channel.

DEPENDENCIES
  None

PARAMETERS
    send_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_disable_ls_cmd
( 
  qvp_rtp_disable_ls_msg_type *ls_msg
)
{

  qvp_rtp_stream_type *aud_stream = NULL; /* for inderection simplicity */
  qvp_rtp_usr_ctx     *usr_ctx = NULL;    /* accessor for the user context
                                           */ 
  qvp_rtp_status_type
                  status = QVP_RTP_ERR_FATAL  ; /* used as status flag */
/*------------------------------------------------------------------------*/
  

  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
  ------------------------------------------------------------------------*/
  if ( ( ls_msg != NULL ) &&
       ( ls_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( ls_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users )
        )
  {
    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the appid  
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) ls_msg->app_id ]; 

  }
  if (!usr_ctx)
  {
      QVP_RTP_ERR("qvp_rtp_disable_ls_cmd: usr_ctx is NULL", 0, 0, 0 );
      return;
  }

  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id for video in range
    4) stream id for audio in range
  ------------------------------------------------------------------------*/
  if ( ( ls_msg->audio_stream < ( qvp_rtp_stream_id_type ) 
         (uint32)rtp_ctx.user_array[ (uint32)   ls_msg->app_id].num_streams) &&
       ( ls_msg->video_stream < ( qvp_rtp_stream_id_type ) 
         (uint32)rtp_ctx.user_array[ (uint32)   ls_msg->app_id].num_streams )
        )
  {
    /*----------------------------------------------------------------------
      extract the video stream and audio stream from the user context 
    ----------------------------------------------------------------------*/


    aud_stream = 
      &( usr_ctx->stream_array[ (uint32) ls_msg->audio_stream ] ); 
  if (!aud_stream)
  {
      QVP_RTP_ERR("qvp_rtp_disable_ls_cmd: aud_stream is NULL", 0, 0, 0 );
      return;
  }

    
#ifdef FEATURE_IMS_RTP_JITTERBUFFER   
{  
    /*----------------------------------------------------------------------
      1) valid video ib handle
      2) valid jb handle for video 
      3) valid audio ib handle
      4) valid jb handle for audio 
    ----------------------------------------------------------------------*/
	  qvp_rtp_stream_type *vid_stream = NULL; /* for inderection simplicity */
	      vid_stream    =  
      &( usr_ctx->stream_array[ (uint32) ls_msg->video_stream ] ); 
    if( vid_stream->ib_stream.valid &&  /* valid vid inboumd */
        vid_stream->ib_stream.jitter_queue && /* valid vid jb handle */ 
        aud_stream->ib_stream.valid &&        /* valid aud inbound */
        aud_stream->ib_stream.jitter_queue )  /* valid aud jb handle */
    {

      QVP_RTP_MSG_LOW_2(" disable_ls aud_q = %p, vid_q = %p", 
                       aud_stream->ib_stream.jitter_queue,
                       vid_stream->ib_stream.jitter_queue);
      
      /*--------------------------------------------------------------------
        Now we can try setting the lipsync inside the jb layer
      --------------------------------------------------------------------*/
      status =  qvp_rtp_jitter_buffer_disable_ls( 
                                      aud_stream->ib_stream.jitter_queue,
                                      vid_stream->ib_stream.jitter_queue );
     
    }
    else
    {
      QVP_RTP_ERR_0("disable_jb jb parameters are wrong");
      status = QVP_RTP_WRONG_PARAM;
    }
}
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/ 

    
  }
  else
  {
    QVP_RTP_ERR_0("disable_jb msg parameters are bad");
  
    status = QVP_RTP_WRONG_PARAM;
    
  }

  /*------------------------------------------------------------------------
        Call the appropriate application call back
  ------------------------------------------------------------------------*/
  if( usr_ctx && usr_ctx->call_backs.lip_unsync_cb && aud_stream)
  {
    
    usr_ctx->call_backs.lip_unsync_cb( ls_msg->app_id,
                                    aud_stream->app_data, 
                                    status, 
                                    ls_msg->audio_stream, 
                                    ls_msg->video_stream );
    
  } 


  return;
    
  
} /* end of function qvp_rtp_disable_ls_cmd */ 

/*===========================================================================

FUNCTION QVP_RTP_ALLOC_OPEN_STREAM 

DESCRIPTION
  This routine allocates ans opens a stream for inbound or outbound or both
  traffic.
DEPENDENCIES
  None

PARAMETERS
    close_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_alloc_open_stream
(
  qvp_rtp_stream_type       *stream,
  qvp_rtp_open_msg_type     *open_msg,
  qvp_rtp_network_hdl_type  nw_hdl
)

{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  qvp_rtp_nw_open_param_type  open_param = {0};
  qvp_rtp_nw_open_ib_param_type  ib_open_param;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
     Check for valid params 
  ------------------------------------------------------------------------*/
  if( !stream || !open_msg )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    DTMF reception not supported.If there is a req for o/b raise an error.
    This is not the right place for this filter.
    This should be moved to profile specific sanity/pcd check funcs
  ------------------------------------------------------------------------*/
  if ( ( open_msg->stream_params.payload_type == QVP_RTP_PYLD_TEL_EVENT ) &&
       ( ( open_msg->stream_params.channel_type == \
                                       QVP_RTP_CHANNEL_INBOUND ) ||
          ( open_msg->stream_params.channel_type == \
                                           QVP_RTP_CHANNEL_FULL_DUPLEX ) ) )
  {
    QVP_RTP_MSG_HIGH_0(
       " qvp_rtp_alloc_open_stream:: DTMF reception not supported ");
    return( QVP_RTP_ERR_FATAL );
  }
  /*------------------------------------------------------------------------
     Do common things first. 
  ------------------------------------------------------------------------*/
  stream->app_data = open_msg->stream_params.app_data;


  /*------------------------------------------------------------------------
    Flag inbound and o/b both invalid 
  ------------------------------------------------------------------------*/
  stream->ob_stream.valid = FALSE;
  stream->ib_stream.valid = FALSE;

  /*------------------------------------------------------------------------
       Reset statstics
  ------------------------------------------------------------------------*/
  stream->ob_stream.pkt_cnt  = 0;
  stream->ib_stream.pkt_cnt  = 0;

  /*------------------------------------------------------------------------
      Setup the common things first
  ------------------------------------------------------------------------*/
  stream->stream_type =  open_msg->stream_params.channel_type;
  stream->payload_type = open_msg->stream_params.payload_type;

  /*------------------------------------------------------------------------
               Find the right profile for the stream 
  ------------------------------------------------------------------------*/
  if ( ( stream->profile = qvp_rtp_get_payld_profile(
                open_msg->stream_params.payload_type ) ) == NULL )
  {
    QVP_RTP_MSG_HIGH_0(" qvp_rtp_alloc_open_stream:: Profile not supported");
    return( QVP_RTP_ERR_FATAL );

  }
  else
  {
    if ( stream->profile->open_profile( &stream->prof_handle, 
              stream ) != QVP_RTP_SUCCESS )
    {
      QVP_RTP_MSG_HIGH_0(
       " qvp_rtp_alloc_open_stream:: Profile could not  be opened ");
      return( QVP_RTP_ERR_FATAL );
    }
  }
  
  /*------------------------------------------------------------------------
    Try and initialize the RTP packetization depacketization reoutines
  ------------------------------------------------------------------------*/
  if ( ( status = qvp_rtp_ctx_init( &stream->rtp ) ) != 
          QVP_RTP_SUCCESS ) 
  {
    QVP_RTP_MSG_HIGH_0(" RTP ctx init failed \r\n");
    return( QVP_RTP_ERR_FATAL );
  }
  
  /*------------------------------------------------------------------------
     Do things common for o/b stream and FULL DUPLEX stream. 
  ------------------------------------------------------------------------*/
  if ( ( open_msg->stream_params.channel_type == QVP_RTP_CHANNEL_OUTBOUND )
       || ( open_msg->stream_params.channel_type == 
            QVP_RTP_CHANNEL_FULL_DUPLEX ) )
  {

    
    /*----------------------------------------------------------------------
      If we have an OB component and we are trying to use an Odd RTP port
      take the next lower even port as RTP port.
      RFC 3550 section 11
      "RTP should use an even destination port number and the corresponding
       RTCP stream should use the next higher (odd) destination port number.
       If an odd number is supplied then the application should replace that
       number with the next lower (even) number to use as the base of the
       port pair."
    ----------------------------------------------------------------------*/
#if 0 
    if( open_msg->stream_params.remote_port & 0x1 )
    {
      QVP_RTP_MSG_HIGH_2(" Odd remote_port %d changed to %d\r\n", 
           open_msg->stream_params.remote_port,
           open_msg->stream_params.remote_port-1);

      stream->ob_stream.remote_port = 
                open_msg->stream_params.remote_port - 1;
    }
    else
#endif
    {
      stream->ob_stream.remote_port = open_msg->stream_params.remote_port;
    }

    /*----------------------------------------------------------------------
      1) Set local port for o/b stream. 
    ----------------------------------------------------------------------*/
    stream->ob_stream.local_port  = open_msg->stream_params.ob_lcl_port;
    qvp_rtp_memscpy( &stream->ob_stream.remote_ip,sizeof(qvp_rtp_ip_addr_type), 
            &open_msg->stream_params.remote_ip, 
            sizeof(qvp_rtp_ip_addr_type) );

    qvp_rtp_memscpy( &stream->ob_stream.local_ip,sizeof(qvp_rtp_ip_addr_type), 
			&open_msg->stream_params.ip_address, 
            sizeof(qvp_rtp_ip_addr_type) );

    /*----------------------------------------------------------------------
      Open outbound channel
    ----------------------------------------------------------------------*/

      memset( &open_param, 0 , sizeof( open_param ) );
      /*--------------------------------------------------------------------
        Setup QOS paramaeters
      --------------------------------------------------------------------*/
      open_param.qos = open_msg->stream_params.qos;
      if( open_msg->stream_params.remote_ip.ipaddr_type != QVP_RTP_IPV4 )
      {
         open_param.qos.use_this_tos = FALSE;
      }
      open_param.pri = qvp_rtp_get_nw_priority( 
                            open_msg->stream_params.payload_type  );

      /*--------------------------------------------------------------------
        Set up connection parameters
      --------------------------------------------------------------------*/
      open_param.iface_hdlr  = open_msg->stream_params.iface_hdl;
      qvp_rtp_memscpy( &open_param.remote_addr, sizeof( qvp_rtp_ip_addr_type ) ,
              &stream->ob_stream.remote_ip,
              sizeof( qvp_rtp_ip_addr_type ) );
      open_param.remote_port = stream->ob_stream.remote_port;
      open_param.ob_port     = open_msg->stream_params.ob_lcl_port;
	  qvp_rtp_memscpy((qvp_rtp_profile_id_type *)open_param.profile_number, sizeof(qvp_rtp_profile_id_type) * QVP_RTP_NUM_PROFILE_ID,open_msg->stream_params.profile_id, sizeof(qvp_rtp_profile_id_type) * QVP_RTP_NUM_PROFILE_ID); 
    open_param.stream = stream;
    open_param.lcl_ip_addr = open_msg->stream_params.ip_address;
    open_param.app_addr_type = open_msg->stream_params.app_addr_type;
	  open_param.iface_id = open_msg->stream_params.iface_id;
	memset(open_param.apn_name, 0, QVP_RTP_APN_NAME_LEN + 1);
	qvp_rtp_memscpy(open_param.apn_name,QVP_RTP_APN_NAME_LEN + 1, (char*)open_msg->stream_params.apn_name, QVP_RTP_APN_NAME_LEN+1);
	  open_param.rat_type = open_msg->stream_params.rat_type;
	  open_param.as_id = open_msg->stream_params.as_id;
    stream->ob_stream.as_id=open_msg->stream_params.as_id;
    QVP_RTP_MSG_HIGH_2("RTP open2 cmd Iface id is %u open_param.rat_type %d",open_param.iface_id,open_param.rat_type);
      /*--------------------------------------------------------------------
        Invoke the primitive open an outbound flow.
      --------------------------------------------------------------------*/
      status = qvp_rtp_open_nw_ob( nw_hdl,
                                   &open_param,
                                   &stream->ob_stream.sock_id );
      
    /*----------------------------------------------------------------------
        Open a generic RTP channel. Then add the remote address/port for
        O/B and Full Duplex channels 
    ----------------------------------------------------------------------*/
    if( status == QVP_RTP_SUCCESS ) 
    {
      
      /*--------------------------------------------------------------------
           Copy back the canonical name 
      --------------------------------------------------------------------*/

	  if (strlcpy( (char *)stream->ob_stream.cname,
		    (char *)open_msg->stream_params.cname,
            sizeof( stream->ob_stream.cname ) ) >=
            sizeof( stream->ob_stream.cname ) )
      {
        QVP_RTP_ERR_2("qvp_rtp_memscpy failed, src len %d, dst len %d", 
          strlen( ( const char *)open_msg->stream_params.cname ), 
          sizeof( stream->ob_stream.cname ));
        return( QVP_RTP_WRONG_PARAM );
      }

      /*--------------------------------------------------------------------
         Flag the o/b part as valid 
      --------------------------------------------------------------------*/
      stream->ob_stream.valid = TRUE;

    }
  }


  /*------------------------------------------------------------------------
    Now its time to open the inbound portion
  ------------------------------------------------------------------------*/
  if ( ( status == QVP_RTP_SUCCESS ) && 
       ( ( open_msg->stream_params.channel_type ==
                                 QVP_RTP_CHANNEL_INBOUND ) ||
         ( open_msg->stream_params.channel_type == 
              QVP_RTP_CHANNEL_FULL_DUPLEX ) ) ) 
  {

#if 0     
    /*----------------------------------------------------------------------
      If we have an IB component and we are trying to use an Odd RTP port
      take the next lower even port as RTP port.
    ----------------------------------------------------------------------*/
    if( open_msg->stream_params.local_port & 0x1 )
    {
      QVP_RTP_MSG_HIGH_2(" Odd ib_local_port %d changed to %d\r\n", 
           open_msg->stream_params.local_port,
           open_msg->stream_params.local_port -1);

      stream->ib_stream.local_port = 
                    open_msg->stream_params.local_port - 1;
    }
    else
#endif
    {
      stream->ib_stream.local_port = open_msg->stream_params.local_port;
    }
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
    /*----------------------------------------------------------------------
      Configure jitter buffer size 
    ----------------------------------------------------------------------*/
    stream->ib_stream.jitter_size = open_msg->stream_params.jitter_size;

    /*----------------------------------------------------------------------
        If the application is asking for a jitter buffer just do it
    ----------------------------------------------------------------------*/
    if( ( status == QVP_RTP_SUCCESS ) && stream->ib_stream.jitter_size  )
    {
      status =  qvp_rtp_alloc_jitter_buffer( stream, 
                                         stream->ib_stream.jitter_size) ;
                                    
    }
#endif/*FEATURE_IMS_RTP_JITTERBUFFER	*/
    /*----------------------------------------------------------------------
         Open socket for inbound channel 
    ----------------------------------------------------------------------*/
    memset(&ib_open_param,0,sizeof(ib_open_param));

    ib_open_param.iface_hdl      = open_msg->stream_params.iface_hdl;
    ib_open_param.ib_port        = open_msg->stream_params.local_port;
    ib_open_param.lcl_ip_addr    = open_msg->stream_params.ip_address;
    ib_open_param.qos_hdls       = open_msg->stream_params.qos.qos_handles;
    ib_open_param.stream         = stream;
    ib_open_param.app_addr_type  =  open_msg->stream_params.app_addr_type;
    ib_open_param.profile_number[0] = open_msg->stream_params.profile_id[0];
	ib_open_param.profile_number[1] = open_msg->stream_params.profile_id[1]; 
    ib_open_param.iface_id = open_msg->stream_params.iface_id;
    ib_open_param.rat_type = open_msg->stream_params.rat_type;
	
	memset(ib_open_param.apn_name, 0, QVP_RTP_APN_NAME_LEN + 1);
	qvp_rtp_memscpy(ib_open_param.apn_name,QVP_RTP_APN_NAME_LEN + 1, (char*)open_msg->stream_params.apn_name, QVP_RTP_APN_NAME_LEN+1);
	ib_open_param.as_id = open_msg->stream_params.as_id;
    stream->ib_stream.as_id=open_msg->stream_params.as_id;	
	QVP_RTP_MSG_MED_1("to call qvp_rtp_open_nw_ib status = %d",status);
    if( status == QVP_RTP_SUCCESS )
    {     
       status = qvp_rtp_open_nw_ib( nw_hdl,
        ib_open_param,
                                    &stream->ib_stream.sock_id,
        qvp_rtp_handle_nw_packet
        ); 
    }

    /*----------------------------------------------------------------------
          if the open is not success then we need flag it through return 
          code. 
    ----------------------------------------------------------------------*/
    if( status == QVP_RTP_SUCCESS ) 
    {

      /*--------------------------------------------------------------------
            Flag the i/b part as valid 
      --------------------------------------------------------------------*/
        stream->ib_stream.valid = TRUE;

    }
  }/*end of if( channel_type == ( ib || duplex ) )*/

#ifdef FEATURE_QVP_RTP_GAN_DSCP  
    /*----------------------------------------------------------------------
          Set the duplex handle
    ----------------------------------------------------------------------*/
  if ( ( status == QVP_RTP_SUCCESS ) &&
       ( open_msg->stream_params.channel_type == 
              QVP_RTP_CHANNEL_FULL_DUPLEX ) )
  {
    status = qvp_rtp_set_ob_duplex_hdl( stream->ob_stream.sock_id, 
                          stream->ib_stream.sock_id);
  }
#endif /*end of FEATURE_QVP_RTP_GAN_DSCP*/

  /*------------------------------------------------------------------------
    If we have been successfull this far we can open RTCP
  ------------------------------------------------------------------------*/
  if( status == QVP_RTP_SUCCESS && 
      open_msg->stream_params.rtcp_tx_config.rtcp_valid  /*&& 
      open_msg->stream_params.rtcp_tx_config.rtcp_tx_enabled*/)
  {
    status  = qvp_rtp_alloc_open_rtcp( open_msg, stream, nw_hdl);
  }

  /*------------------------------------------------------------------------
      If we failed in any step we need to flag the whole channel as false. 
  ------------------------------------------------------------------------*/
  if ( status != QVP_RTP_SUCCESS )
  {
    stream->valid = FALSE;
    stream->ib_stream.valid = FALSE;
    stream->ob_stream.valid = FALSE;

   /*-----------------------------------------------------------------------
      Do all the cleanup before we exit
   -----------------------------------------------------------------------*/

    
    /*----------------------------------------------------------------------
      Cleanup NW allocation O/B
    ----------------------------------------------------------------------*/
    if ( ( ( open_msg->stream_params.channel_type == 
           QVP_RTP_CHANNEL_FULL_DUPLEX ) ||  
           ( open_msg->stream_params.channel_type == 
                  QVP_RTP_CHANNEL_OUTBOUND ) ) && 
           stream->ob_stream.sock_id )
    {

      /*--------------------------------------------------------------------
               close the already opened o/b channel 
      --------------------------------------------------------------------*/
      qvp_rtp_close_nw_ob( nw_hdl, stream->ob_stream .sock_id );

    }
    
    /*----------------------------------------------------------------------
      Cleanup NW allocation I/B
    ----------------------------------------------------------------------*/
    if ( ( ( open_msg->stream_params.channel_type == 
           QVP_RTP_CHANNEL_FULL_DUPLEX ) ||  
           ( open_msg->stream_params.channel_type == 
                  QVP_RTP_CHANNEL_INBOUND ) ) && 
           stream->ib_stream.sock_id )
    {

      /*--------------------------------------------------------------------
               close the already opened i/b channel 
      --------------------------------------------------------------------*/
      qvp_rtp_close_nw_ib( nw_hdl,stream->ib_stream .sock_id );

    }


    /*----------------------------------------------------------------------
      Cleanup the profile if we have aleady opened it 
    ----------------------------------------------------------------------*/
    if( stream->prof_handle )
    {

      stream->profile->close_profile( stream->prof_handle );
	  if (stream->payload_type == QVP_RTP_PYLD_TXT)
	  {
		  rtp_codec_txt_rx_ctx_uninit(stream);
	  }
      stream->profile = NULL;
    }
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
    /*----------------------------------------------------------------------
      cleanup jitter related resources
    ----------------------------------------------------------------------*/
    if ( ( ( open_msg->stream_params.channel_type == 
           QVP_RTP_CHANNEL_FULL_DUPLEX ) ||  
           ( open_msg->stream_params.channel_type == 
                  QVP_RTP_CHANNEL_INBOUND ) ) &&
         ( open_msg->stream_params.jitter_size ) && 
           stream->ib_stream.jitter_queue )
    {
      qvp_rtp_jb_close_jitter_buffer(
                            stream->ib_stream.jitter_queue );
    }
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/    
    
    /*----------------------------------------------------------------------
       If the RTCP session is opened close it as well.
    ----------------------------------------------------------------------*/
    if( stream->rtcp_hdl )
    {
      qvp_rtcp_close( stream->rtcp_hdl );
    }
    
  }

  /*-----------------------------------------------------------------------
    *Try to store required QDJ parameters in the RTCP session
    *--------------------------------------------------------------------*/
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
  if( status == QVP_RTP_SUCCESS && 
    open_msg->stream_params.rtcp_tx_config.rtcp_valid && 
    open_msg->stream_params.rtcp_tx_config.rtcp_tx_enabled)
  {
    qvp_rtcp_get_qdj_param(stream->ib_stream.jitter_queue ,stream->rtcp_hdl );
  }
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
  return( status );

} /* end of function qvp_rtp_alloc_open_stream */ 




/*===========================================================================

FUNCTION QVP_RTP_DEALLOC_CLOSE_STREAM 

DESCRIPTION
  This routine allocates ans closes an already open stream for inbound or 
  outbound or both traffic.
  
DEPENDENCIES
  None

  ARGUEMENTS IN
    close_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_dealloc_close_stream
(
  qvp_rtp_stream_type       *stream,
  qvp_rtp_network_hdl_type  nw_hdl
)
{

  if( !stream || !stream->valid )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  /*------------------------------------------------------------------------
    If we have an outbound component close it now. 
  ------------------------------------------------------------------------*/
  if ( ( stream->stream_type == QVP_RTP_CHANNEL_OUTBOUND ) ||
       ( stream->stream_type == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
  {

    qvp_rtp_close_nw_ob( nw_hdl, stream->ob_stream.sock_id );
  }

  /*------------------------------------------------------------------------
    If we have an inbound component close it now. 
  ------------------------------------------------------------------------*/
  if ( ( stream->stream_type == QVP_RTP_CHANNEL_INBOUND ) ||
       ( stream->stream_type == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
  {

    qvp_rtp_close_nw_ib( nw_hdl, stream->ib_stream.sock_id );
  }

  /*------------------------------------------------------------------------
        Close the profile opened
  ------------------------------------------------------------------------*/

  if( stream->profile && stream->prof_handle )
  {
    stream->profile->close_profile( stream->prof_handle );
	if (stream->payload_type == QVP_RTP_PYLD_TXT)
	{
		rtp_codec_txt_rx_ctx_uninit(stream);
	}
    
  }
 #ifdef FEATURE_IMS_RTP_JITTERBUFFER 
  /*------------------------------------------------------------------------
    Cleanup the jitter buffer if opened one
  ------------------------------------------------------------------------*/
  if( stream->ib_stream.jitter_size 
      && stream->ib_stream.jitter_queue )
  {
    qvp_rtp_jb_close_jitter_buffer( 
                                  stream->ib_stream.jitter_queue );
    stream->ib_stream.jitter_size = 0;
    stream->ib_stream.jitter_queue = NULL;
  }
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
  /*------------------------------------------------------------------------
    Close the associated RTCP stream
  ------------------------------------------------------------------------*/
  if( stream->rtcp_hdl )
  {
    qvp_rtcp_close( stream->rtcp_hdl );
  }
  
  
  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_dealloc_close_stream */
/*===========================================================================

FUNCTION QVP_RTP_CLOSE_UNUSED_SOCKET

DESCRIPTION
  This routine closes the unused sockets ( only socket not the jitter 
  buffer ) after channel configuration.

DEPENDENCIES
  None

ARGUEMENTS IN
  current_stream_type - current stream type( stream type before 
                        configuration )
  desired_stream_type - desired stream type( stream type desired )
  stream              - stream being reconfigured
  nw_hdl              - network handle

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_close_unused_socket
(
  qvp_rtp_channel_type      current_stream_type,
  qvp_rtp_channel_type      desired_stream_type,
  qvp_rtp_stream_type       *stream,
  qvp_rtp_network_hdl_type  nw_hdl
)
{
  /*------------------------------------------------------------------------
    Check for valid arguments 
  ------------------------------------------------------------------------*/
  if( !stream )
  {
    QVP_RTP_ERR_0(" rtp_configure_stream_direction:Invalid i/p params\r\n");
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    No need to close any sockets here.
  ------------------------------------------------------------------------*/
  if ( current_stream_type == desired_stream_type  )
  {
    QVP_RTP_MSG_MED_2("No socks closed des_chl_type %d,curr_chl_type %d\r\n", 
         desired_stream_type, current_stream_type);
    return( QVP_RTP_SUCCESS );
  }
  /*------------------------------------------------------------------------
    we want an ib stream so close the ob socket.
  ------------------------------------------------------------------------*/
  else if ( desired_stream_type == QVP_RTP_CHANNEL_INBOUND ) 
  {
    qvp_rtp_close_nw_ob( nw_hdl,stream->ob_stream.sock_id );
  }
  /*------------------------------------------------------------------------
    we want an ob stream so close the ib socket.
  ------------------------------------------------------------------------*/
  else if ( desired_stream_type == QVP_RTP_CHANNEL_OUTBOUND )
  {
    qvp_rtp_close_nw_ib( nw_hdl,stream->ib_stream.sock_id );
  }

  return ( QVP_RTP_SUCCESS );

}/*end of qvp_rtp_close_unused_socket() */

/*===========================================================================

FUNCTION QVP_RTP_CONFIGURE_IB_SOCKET

DESCRIPTION

  This routine configures inbound socket for a stream.
  Opens the socket not jitter.

DEPENDENCIES
  None

ARGUEMENTS IN
  stream     - stream being reconfigured
  rx_param   - Open parameters to be used for configuring the rx path.
  nw_hdl     - network handle

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_configure_ib_socket
(
  qvp_rtp_stream_type             *stream,
  qvp_rtp_pyld_rtp_rx_param_type  rx_param,
  qvp_rtp_network_hdl_type        nw_hdl
)
{
  qvp_rtp_status_type        status = QVP_RTP_SUCCESS;
  qvp_rtp_pyld_qos_type  qos;
  qvp_rtp_if_hdl_type iface_hdl;
  qvp_rtp_nw_open_ib_param_type  ib_open_param;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Check for valid arguments 
  ------------------------------------------------------------------------*/
  if ( ( !stream ) || ( !stream->ob_stream.valid ) )
  {
    QVP_RTP_ERR_0(" qvp_rtp_configure_ib_socket:Invalid i/p params\r\n");
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Get the interface handle from an already opened ob socket.
  ------------------------------------------------------------------------*/
  status = qvp_rtp_get_nw_iface_hdl( stream->stream_type,
                                     stream->ob_stream.sock_id,
                                     &iface_hdl );

  if ( status != QVP_RTP_SUCCESS )
  {
    QVP_RTP_ERR_1("Failed to get interface handle status %d\r\n",
               status);
    return ( status );
  }

  /*------------------------------------------------------------------------
    Get the Qos handle from an already opened ob socket.
  ------------------------------------------------------------------------*/
  status = qvp_rtp_get_nw_qos_ob( stream->ob_stream.sock_id, &qos );

  if ( status != QVP_RTP_SUCCESS )
  {
    QVP_RTP_ERR_1("Failed to get QOS handle status %d\r\n", status);
    return ( status );
  }
#if 0 
  /*------------------------------------------------------------------------
    If we have an IB component and we are trying to use an Odd RTP port
    take the next lower even port as RTP port.
  ------------------------------------------------------------------------*/
  if( rx_param.ib_local_port & 0x1 )
  {
    QVP_RTP_MSG_HIGH_2(" Odd ib_local_port %d changed to %d\r\n", 
         rx_param.ib_local_port,
         rx_param.ib_local_port-1);
    rx_param.ib_local_port = rx_param.ib_local_port - 1;
  }
#endif
    memset(&ib_open_param,0,sizeof(ib_open_param));
    ib_open_param.iface_hdl      = iface_hdl;
    ib_open_param.ib_port        =  rx_param.ib_local_port;
    ib_open_param.qos_hdls       =  qos.qos_handles;
    ib_open_param.stream         =  stream;
	ib_open_param.as_id          = stream->ib_stream.as_id;

  /*------------------------------------------------------------------------
    Open inbound socket
  ------------------------------------------------------------------------*/
  if ( (status = qvp_rtp_open_nw_ib( nw_hdl,
   ib_open_param,
                          &stream->ib_stream.sock_id,
                          qvp_rtp_handle_nw_packet ) ) == QVP_RTP_SUCCESS )
  {

    /*----------------------------------------------------------------------
      Flag the i/b part as valid and update local port.
    ----------------------------------------------------------------------*/
    stream->ib_stream.local_port = rx_param.ib_local_port;
    stream->ib_stream.valid = TRUE;
	QVP_RTP_MSG_MED_1("registered qvp_rtp_handle_nw_packet() as call back , port = %d",stream->ib_stream.local_port);

  }
  else
  {
    QVP_RTP_ERR_1("Failed to open ib socket, status %d\r\n", status);
    return ( status );
  }

  return ( QVP_RTP_SUCCESS );

}/*end of qvp_rtp_configure_ib_socket() */

/*===========================================================================

FUNCTION QVP_RTP_CONFIGURE_OB_SOCKET

DESCRIPTION

  This routine configures outbound socket for a stream.

DEPENDENCIES
  None

ARGUEMENTS IN
  stream     - stream being reconfigured
  open_param - params for opening ob socket.
  nw_hdl     - network handle

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_configure_ob_socket
(
  qvp_rtp_stream_type         *stream,
  qvp_rtp_nw_open_param_type  *open_param,
  qvp_rtp_network_hdl_type    nw_hdl
)
{
  qvp_rtp_status_type  status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Check for valid arguments 
  ------------------------------------------------------------------------*/
  if( !stream || !open_param )
  {
    QVP_RTP_ERR_0(" qvp_rtp_configure_ob_socket:Invalid i/p params\r\n");
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Get the interface handle from an already open ib socket.
  ------------------------------------------------------------------------*/
  if ( stream->ib_stream.valid == TRUE )
  {
    status = qvp_rtp_get_nw_iface_hdl( stream->stream_type,
                                       stream->ib_stream.sock_id,
                                       &open_param->iface_hdlr );

    if ( status != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_1("Failed to get interface handle, status %d\r\n", 
                             status);
      return ( status );
    }

    /*----------------------------------------------------------------------
      Get the Qos handles from an already opened ib socket.
    ----------------------------------------------------------------------*/
    status = qvp_rtp_get_nw_qos_ib( stream->ib_stream.sock_id,
                  &open_param->qos.qos_handles );

    if ( status != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_1("Failed to get QOS handle, status %d\r\n", status);
      return ( status );
    }

  }/*end of if ( stream->ib_stream.valid == TRUE ) */
  else
  {
    QVP_RTP_ERR_0("Inval ib stream,retrieval of sock params not possible\r\n");
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    If the application is trying use an odd port take the next lower even
    port as RTP port.
  ------------------------------------------------------------------------*/
#if 0 
  if( open_param->remote_port & 0x1 )
  {
    QVP_RTP_MSG_HIGH_2(" Odd remote_port %d changed to %d\r\n",
         open_param->remote_port,
         open_param->remote_port-1);
    open_param->remote_port = open_param->remote_port - 1;
  }
#endif 

  /*------------------------------------------------------------------------
    Invoke the primitive to open an outbound flow.
  ------------------------------------------------------------------------*/
  open_param->as_id=stream->ob_stream.as_id;
  if ( ( status = qvp_rtp_open_nw_ob( nw_hdl,
                               open_param,
                               &stream->ob_stream.sock_id ) ) == 
                                       QVP_RTP_SUCCESS )
  {
    /*----------------------------------------------------------------------
      Flag the o/b part as valid and update local port.
    ----------------------------------------------------------------------*/
    stream->ob_stream.local_port = open_param->ob_port;
    stream->ob_stream.valid = TRUE;
  }
  else
  {
    QVP_RTP_ERR_1("Failed to open an ob socket, status %d\r\n", status);
    return ( status );
  }

  return ( QVP_RTP_SUCCESS );
}/*end of qvp_rtp_configure_ob_socket()*/
/*===========================================================================

FUNCTION QVP_RTP_CONFIGURE_RX_PATH

DESCRIPTION

  This routine configures a preallocated stream's rx path using the 
  parameters passed.

DEPENDENCIES
  None

ARGUEMENTS IN
  stream         - stream being reconfigured
  rx_param       - Open parameters to be used for configuring the rx path.
  payload_valid  - valid flag for payload type.
  payload_type   - RTP payload type.

ARGUEMENTS OUT
  close_old_jb - flag indicating if the the old jitter is to be closed
                 due to new allocation during configuration.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_configure_rx_path
(
  qvp_rtp_stream_type             *stream,
  qvp_rtp_pyld_rtp_rx_param_type  rx_param,
  boolean                         payload_valid,
  qvp_rtp_payload_type            payload_type,
  boolean                         *close_old_jb
)
{
  qvp_rtp_status_type        status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Check for valid arguments 
  ------------------------------------------------------------------------*/
  if( !stream || !close_old_jb )
  {
    QVP_RTP_ERR_0(" qvp_rtp_configure_rx_path:Invalid i/p params\r\n");
    return( QVP_RTP_ERR_FATAL );
  }
  /*------------------------------------------------------------------------
    Throw an error if we dont have a valid ib stream and we are trying to 
    configure it. 
  ------------------------------------------------------------------------*/
  if ( stream->ib_stream.valid != TRUE )
  {
    QVP_RTP_ERR_1(" qvp_rtp_configure_rx_path:Invalid ib stream flag %d\r\n",
                        stream->ib_stream.valid);
    return ( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
   Set RTP payload type for now
  ------------------------------------------------------------------------*/
  status = qvp_rtp_set_rtp_rx_payld_type( &stream->rtp, 
                                          rx_param.payload_type, stream->dtmf_rx_pt ) ;

  if ( status != QVP_RTP_SUCCESS )
  {
    QVP_RTP_ERR_1(" rx payload setting failed pyld_type %d\r\n",
        rx_param.payload_type);
  }
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
  /*------------------------------------------------------------------------
    If the application is asking for a jitter buffer just allocate it
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS ) && ( rx_param.jitter_valid == TRUE ) )
  {
    /*----------------------------------------------------------------------
      Configuring jitter buffer .
    ----------------------------------------------------------------------*/
    status = qvp_rtp_configure_jitter_size ( stream,
                                              rx_param, payload_valid, 
                                              payload_type,
                                              close_old_jb );

    if ( status != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_1(" Jitter allocation failed, jitter_size%d\r\n",
          rx_param.jitter_size);
    }
  }
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
  /*------------------------------------------------------------------------
    If its an ib stream with no jitter and no pkt call back throw an error.
  ------------------------------------------------------------------------*/
  /*if( ( status == QVP_RTP_SUCCESS ) && 
            ( ( !stream->ib_stream.jitter_size ) && 
         ( !( ( qvp_rtp_usr_ctx * )(stream->usr_ctx) )->\
                                        call_backs.packet_cb ) ) )
  {
    QVP_RTP_ERR(" No packet callback, how to ship the packet to app ??",
                                                       0, 0, 0 );
    status = QVP_RTP_ERR_FATAL;
  }*/

  return( status );

}/*end of qvp_rtp_configure_rx_path()*/
/*===========================================================================

FUNCTION QVP_RTP_CONFIGURE_TX_PATH

DESCRIPTION

  This routine configures a preallocated stream's directionality using the 
  parameters passed.

DEPENDENCIES
  None

ARGUEMENTS IN
  stream     - stream being reconfigured
  open_param - Parameters for configuring tx path.

ARGUEMENTS OUT
  NONE

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_configure_tx_path
(
  qvp_rtp_stream_type         *stream,
 qvp_rtp_nw_open_param_type  *open_param,
 qvp_rtp_pyld_rtp_tx_param_type  tx_param
)
{
  qvp_rtp_status_type        status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Check for valid arguments 
  ------------------------------------------------------------------------*/
  if( !stream || !open_param )
  {
    QVP_RTP_ERR_0(" qvp_rtp_configure_tx_path:Invalid i/p params\r\n");
    return( QVP_RTP_ERR_FATAL );
  }
  /*------------------------------------------------------------------------
    Throw an error if we dont have a valid ob stream and we are trying to 
    configure it. 
  ------------------------------------------------------------------------*/
  if ( stream->ob_stream.valid != TRUE )
  {
    QVP_RTP_ERR_1(" qvp_rtp_configure_tx_path:Invalid ob stream flag %d\r\n",
                        stream->ob_stream.valid);
    return ( QVP_RTP_ERR_FATAL );
  }
  /*------------------------------------------------------------------------
  set dtmf clock rate for now
  ------------------------------------------------------------------------*/
  stream->dtmf_clock_rate = tx_param.dtmf_clock_rate;
  /*------------------------------------------------------------------------
  Set RTP payload type for now
  ------------------------------------------------------------------------*/
  status = qvp_rtp_set_rtp_tx_payld_type( &stream->rtp, 
    tx_param.payload_type ) ;
  /*------------------------------------------------------------------------
    If the application is trying use an odd port take the next lower even
    port as RTP port.
  ------------------------------------------------------------------------*/
#if 0
  if( open_param->remote_port & 0x1 )
  {
    QVP_RTP_MSG_HIGH_2(" Odd remote_port %d changed to %d\r\n",
         open_param->remote_port,
         open_param->remote_port-1 );
    open_param->remote_port = open_param->remote_port - 1;
  }
  else
#endif 
  {
    
    /*----------------------------------------------------------------------
      Store the IP and port in the context....
    ----------------------------------------------------------------------*/
    qvp_rtp_memscpy( &stream->ob_stream.remote_ip,sizeof( qvp_rtp_ip_addr_type ),
            &open_param->remote_addr,
            sizeof( qvp_rtp_ip_addr_type ) );
    stream->ob_stream.remote_port = open_param->remote_port;
    
  }

  /*------------------------------------------------------------------------
     If we have an open ob stream configure ob port and ip.
  ------------------------------------------------------------------------*/
  if( status == QVP_RTP_SUCCESS )
  {
  //  uint8 i = 0;
    /*----------------------------------------------------------------------
      Configure the NW layer with the new ip and port numbers.
    ----------------------------------------------------------------------*/
    
    QVP_RTP_MED_1_STR(LOG_IMS_RTP,"remote_ip address = %s ",stream->ob_stream.remote_ip.ipaddr);
    QVP_RTP_MSG_HIGH_1("remote port = %d",stream->ob_stream.remote_port);
  /*  while(stream->ob_stream.remote_ip.ipaddr[i] != '\0')
    {
      QVP_RTP_MSG_HIGH("remote_ip address[%d] = %c remote port = %d",i,stream->ob_stream.remote_ip.ipaddr[i],stream->ob_stream.remote_port);
      i++;
    }*/
    stream->rtp_packet_source_check = FALSE;    	  
	if(0 == (stream->ob_stream.remote_port%2))
	{
	  if( QVP_RTP_SUCCESS != qvp_rtp_conver_to_netowrk_order(&stream->stream_remote_ipaddr, &stream->ob_stream.remote_ip))
	  {
	    stream->rtp_packet_source_check = FALSE;
		QVP_RTP_MSG_LOW_1("qvp_rtp_conver_to_netowrk_order failed for remote port %u",stream->ob_stream.remote_port);
	  }
	  else
	  {
	    stream->rtp_packet_source_check = TRUE;
		QVP_RTP_MSG_LOW_1("qvp_rtp_conver_to_netowrk_order success for remote port %u",stream->ob_stream.remote_port);
	  }	
	}
    status = qvp_rtp_set_nw_ob_dest( stream->ob_stream.sock_id, 
                                     &stream->ob_stream.remote_ip,
                                     stream->ob_stream.remote_port );
  }

  return ( status );

}/*end of qvp_rtp_configure_tx_path()*/
/*===========================================================================

FUNCTION QVP_RTP_CONFIGURE_STREAM_DIRECTION

DESCRIPTION

  This routine configures a preallocated stream's directionality using the 
  parameters passed.

DEPENDENCIES
  None

ARGUEMENTS IN
  stream     - stream being reconfigured
  config_msg - message which was posted by the API into the module.
  nw_hdl     - network handle

ARGUEMENTS OUT
  close_old_jb - If this flag is enabled the jitter buffer of 
                 the old configuration has to be closed 
                 after the new config is successful

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_configure_stream_direction
(
  qvp_rtp_stream_type       *stream,
  qvp_rtp_config_msg_type   *config_msg,
  qvp_rtp_network_hdl_type  nw_hdl,
  boolean                   *close_old_jb
)
{
  qvp_rtp_status_type        status = QVP_RTP_SUCCESS;
  qvp_rtp_nw_open_param_type  open_param;
  qvp_rtp_channel_type curr_dir, desired_dir;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Check for valid arguments 
  ------------------------------------------------------------------------*/
  if( !stream || !config_msg || !close_old_jb  ||
        ( stream->stream_type > QVP_RTP_CHANNEL_FULL_DUPLEX ) ||
        ( config_msg->config.ch_dir > QVP_RTP_CHANNEL_FULL_DUPLEX ) )
        
  {
    QVP_RTP_ERR_0(" rtp_configure_stream_direction:Invalid i/p params\r\n");
    return( QVP_RTP_ERR_FATAL );
  }

  memset( &open_param, 0 , sizeof( open_param ) );
  /*------------------------------------------------------------------------
    Extract params for configuring tx path.
  ------------------------------------------------------------------------*/
  if ( config_msg->config.config_tx_params.valid == TRUE )
  {
    open_param.ob_port = 
            config_msg->config.config_tx_params.tx_rtp_param.ob_local_port;
    open_param.remote_port = 
            config_msg->config.config_tx_params.tx_rtp_param.remote_port;
    qvp_rtp_memscpy( &open_param.remote_addr,sizeof(qvp_rtp_ip_addr_type),
            &config_msg->config.connection_ip,
            sizeof(qvp_rtp_ip_addr_type) );

    open_param.qos = config_msg->config.qos_config;

    if( open_param.remote_addr.ipaddr_type != QVP_RTP_IPV4 )
    {
      open_param.qos.use_this_tos = FALSE;
    }

    open_param.pri = qvp_rtp_get_nw_priority( 
                                     config_msg->config.payload );
  }

  curr_dir = stream->stream_type;

  if ( ( config_msg->config.chdir_valid == TRUE ) &&
          ( config_msg->config.ch_dir != stream->stream_type ) )
  {

    desired_dir = config_msg->config.ch_dir;

    /*----------------------------------------------------------------------
      we need to open an ob socket incase
      1. curr channel is ib and desired channel is ob
      2. curr channel is ib and desired channel is duplex
    ----------------------------------------------------------------------*/
    if ( curr_dir == QVP_RTP_CHANNEL_INBOUND ) 
    {
      /*--------------------------------------------------------------------
        Open an ob socket if we have a valid ib socket existing ( as we will
        be extracting the interface and QOS handles form the existing socket)
        and we have a set of valid tx params to be configured.
      --------------------------------------------------------------------*/
      if( ( stream->ib_stream.valid ) && 
             ( config_msg->config.config_tx_params.valid == TRUE ) )
      {

        status = qvp_rtp_configure_ob_socket( stream, &open_param, nw_hdl);

      }/*end of if ( stream->ib_stream.valid )*/
      else
      {
        /*------------------------------------------------------------------
          Invalid params for opening socket.
        ------------------------------------------------------------------*/
        QVP_RTP_ERR(" cant open ob sock,ib_stream_valid %d,tx_param_valid%d\
                 , status %d \r\n", 
                 config_msg->config.ch_dir, 
                 config_msg->config.config_tx_params.valid, status );
        status = QVP_RTP_ERR_FATAL;
      }

    }/*end of if ( curr_dir == QVP_RTP_CHANNEL_INBOUND )*/

    /*----------------------------------------------------------------------
      we need to open an ib socket incase
      1. curr channel is ob and desired channel is ib
      2. curr channel is ob and desired channel is duplex
    ----------------------------------------------------------------------*/
    else if ( curr_dir == QVP_RTP_CHANNEL_OUTBOUND )
    {
      /*--------------------------------------------------------------------
        Open an ib socket if we have a valid ob socket existing ( as we will
        be extracting the interface and QOS handles form the existing socket)
        and we have a set of valid rx params to be configured.
      --------------------------------------------------------------------*/
      if( ( stream->ob_stream.valid == TRUE ) && 
             ( config_msg->config.config_rx_params.valid == TRUE ) )
      {
        status = qvp_rtp_configure_ib_socket( stream, 
                config_msg->config.config_rx_params.rx_rtp_param, nw_hdl);

      }/*end of if ( stream->ob_stream.valid )*/
      else
      {
        /*------------------------------------------------------------------
          Invalid params for opening socket.
        ------------------------------------------------------------------*/
        QVP_RTP_ERR(" cant open ib sock,ob_stream_valid %d,rx_param_valid%d\
                  , status %d \r\n", 
                 config_msg->config.ch_dir, 
                 config_msg->config.config_rx_params.valid, status );
        status = QVP_RTP_ERR_FATAL;
      }

    }/*end of if ( curr_dir == QVP_RTP_CHANNEL_OUTBOUND )*/

    if ( status != QVP_RTP_SUCCESS )
    {
      /*--------------------------------------------------------------------
       socket opening failed.
      --------------------------------------------------------------------*/
      QVP_RTP_ERR_1("socket config failed status %d\r\n", status);
      return( status );
    }

    /*----------------------------------------------------------------------
      Set the ib stream as invalid incase desired channel type is ob.
    ----------------------------------------------------------------------*/
    if ( desired_dir == QVP_RTP_CHANNEL_OUTBOUND ) 
    {
      stream->ib_stream.valid = FALSE;
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
      /*--------------------------------------------------------------------
        Flag the closing of jb associated with the ib stream.
      --------------------------------------------------------------------*/
      if ( stream->ib_stream.jitter_queue != NULL )
      {
        *close_old_jb = TRUE;
        stream->ib_stream.jitter_size = 0;
        stream->ib_stream.jitter_queue = NULL;
      }
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
    }
    /*----------------------------------------------------------------------
      Set the ob stream as invalid incase desired channel type is ib.
    ----------------------------------------------------------------------*/
    else if ( desired_dir == QVP_RTP_CHANNEL_INBOUND )
    {
      stream->ob_stream.valid = FALSE;
    }

    /*----------------------------------------------------------------------
      Update the stream type.
    ----------------------------------------------------------------------*/
    stream->stream_type = config_msg->config.ch_dir;

  }/*end of if ( chl_dir == valid )*/


  /*------------------------------------------------------------------------
    configure the tx path params.
  ------------------------------------------------------------------------*/
  if ( ( stream->ob_stream.valid == TRUE ) &&
         ( config_msg->config.config_tx_params.valid == TRUE ) )
  {
    status = qvp_rtp_configure_tx_path( stream, &open_param,config_msg->config.config_tx_params.tx_rtp_param );

    if ( status != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_1("Failed in configuring tx params, status %d\r\n", 
                             status);
    }

  }/* end of if ( status == QVP_RTP_SUCCESS)*/

  /*------------------------------------------------------------------------
    configure the rx path params.
  ------------------------------------------------------------------------*/
  if ( ( status == QVP_RTP_SUCCESS ) && 
         ( stream->ib_stream.valid == TRUE ) )
  {
    if ( config_msg->config.config_rx_params.valid == TRUE )
    {
      status = qvp_rtp_configure_rx_path( stream, 
                    config_msg->config.config_rx_params.rx_rtp_param,
                    config_msg->config.payload_valid,
                    config_msg->config.payload, close_old_jb);

      if ( status != QVP_RTP_SUCCESS )
      {
        QVP_RTP_ERR_1("Failed in configuring rx params, status %d\r\n", 
                               status);
      }
    }
    /*----------------------------------------------------------------------
      If its an ib stream with no jitter and no pkt call back throw an error
    ----------------------------------------------------------------------*/
    /*if ( ( status == QVP_RTP_SUCCESS ) &&
        ( !stream->ib_stream.jitter_size ) && 
         ( !( ( qvp_rtp_usr_ctx * )(stream->usr_ctx) )->\
                                        call_backs.packet_cb ) )
    {
      QVP_RTP_ERR(" No packet callback, how to ship the packet to app ??",
                                                         0, 0, 0 );
      status = QVP_RTP_ERR_FATAL;
    }*/
  }/* end of if ( status == QVP_RTP_SUCCESS)*/

  return( status );

}/*end of qvp_rtp_configure_stream_direction()*/

/*===========================================================================

FUNCTION QVP_RTP_CONFIGURE_STREAM

DESCRIPTION

   This routine configures a preallocated streams using the parameters 
   passed.
  
DEPENDENCIES
  None

  ARGUEMENTS IN
    stream - stream being reconfigured
    config_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_configure_stream
(
  qvp_rtp_stream_type  *stream,
  qvp_rtp_config_msg_type *config_msg,
  qvp_rtp_network_hdl_type  nw_hdl
)
{
  qvp_rtp_stream_type        orig_stream;     /* backup of complete stream*/
  uint8                      orig_rx_payload; /* back up of rx payload    */
  uint8                      orig_tx_payload; /* back up of tx payload    */
  qvp_rtp_profile_hdl_type   old_hdl;         /* backup of profile handle */
  qvp_rtp_profile_type       *old_profile;    /* backkup of profile funcs */
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
  qvp_rtp_jitter_hdl_type    old_jb_hdl = NULL; /*backup of jitter handle */
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/	  
  qvp_rtcp_config_param_type orig_rtcp_config;/* backup of rtcp config    */
  qvp_rtp_pyld_qos_type      orig_tx_qos ;    /* backup of qos            */
  qvp_rtcp_config_param_type rtcp_config;     /* new rtcp configuration   */
  boolean                    close_old_jb = FALSE; /* set if old jitter
                                              * is to be closed 
                                              */
  qvp_rtp_payload_config_type profile_config;
  qvp_rtp_channel_type old_stream_type;
  qvp_rtp_pyld_appqoshdls_type  orig_rx_qos;
  qvp_rtp_status_type        status = QVP_RTP_SUCCESS;

/*------------------------------------------------------------------------*/
  

  /*------------------------------------------------------------------------
    Check for valid arguments 
  ------------------------------------------------------------------------*/
  if( !stream || !config_msg )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  if ( config_msg->config.valid != TRUE )
  {
    /*----------------------------------------------------------------------
      We dont have a request for configuring stream. Just return with
      success.
    ----------------------------------------------------------------------*/
    return ( QVP_RTP_SUCCESS );
  }

  old_hdl = stream->prof_handle;
  old_profile = stream->profile;
#ifdef FEATURE_IMS_RTP_JITTERBUFFER  
  old_jb_hdl = stream->ib_stream.jitter_queue;
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/	  
  old_stream_type = stream->stream_type;

  /*------------------------------------------------------------------------
    Back up the stream  before we attempt to configre anything
    In case we fail we reconfigure with the old value set.   
  ------------------------------------------------------------------------*/
  qvp_rtp_memscpy( &orig_stream, sizeof( orig_stream ),stream, sizeof( orig_stream ) );

  /*------------------------------------------------------------------------
    Back up the Rx payload, qos, RTCP and store them in a local variable.
    In case we fail we reconfigure with the old value set. 
    Stream backup will back up these values also
    but we want to repeat it again for the purpose of abstraction
  ------------------------------------------------------------------------*/
  orig_rx_payload = 0;
  status =  qvp_rtp_get_rx_payld_type( &stream->rtp, &orig_rx_payload );

  orig_tx_payload = 0;
  status =  qvp_rtp_get_tx_payld_type( &stream->rtp, &orig_tx_payload );

  QVP_RTP_MSG_LOW_1( "RTP configure get Tx and Rx payload types status %d", status);

  memset( &orig_tx_qos, 0 , sizeof( orig_tx_qos ) );
  if( ( status == QVP_RTP_SUCCESS ) && ( stream->ob_stream.valid ) )
  {
    status = qvp_rtp_get_nw_qos_ob( stream->ob_stream.sock_id, &orig_tx_qos);
  }
  QVP_RTP_MSG_LOW_1( "RTP configure get_nw_qos_ob status %d", status);
  memset( &orig_rx_qos, 0 , sizeof( orig_rx_qos ) );
  if( ( status == QVP_RTP_SUCCESS ) && ( stream->ib_stream.valid == TRUE ) )
  {
    status = qvp_rtp_get_nw_qos_ib( stream->ib_stream.sock_id,&orig_rx_qos);
  }
  QVP_RTP_MSG_LOW_1( "RTP configure get_nw_qos_ib status %d", status);
  if ( ( status == QVP_RTP_SUCCESS ) && ( stream->rtcp_hdl ) )
  { 
    status = qvp_rtcp_get_config( stream->rtcp_hdl, &orig_rtcp_config );
  }

  QVP_RTP_MSG_LOW_1( "RTCP get config status %d", status);
  /*------------------------------------------------------------------------
    If there is any error in stream back up bail out
  ------------------------------------------------------------------------*/

  if( status != QVP_RTP_SUCCESS )
  {
    QVP_RTP_MSG_LOW_1( "RTP configure status error %d", status);
    return( status );
  }

  if(config_msg->config.config_rx_params.rx_rtp_param.tx_payload_enable)
  {
	status = qvp_rtp_set_param(&stream->rtp, QVP_RTP_PARAM_ENABLE_TX_PYLD_FLTR, 0);
	if( status != QVP_RTP_SUCCESS )
	{
		return( status );
	}
  }
  /*------------------------------------------------------------------------
    Configure channel direction.
    During direction configuration we will not close any socket just set the
    socket as invalid for now.
    Sockets will be closed only if we succeed the whole configuration.
    This might lead to resource clash.
  ------------------------------------------------------------------------*/
  status = qvp_rtp_configure_stream_direction( stream,config_msg,
                      nw_hdl, &close_old_jb );


   QVP_RTP_MSG_LOW_1( "RTP configure stream direction status %d", status);
  
   /*-----------------------------------------------------------------------
     Configure SRTP parameters if enabled
   -------------------------------------------------------------------------*/
   if(config_msg->config.is_srtp_enabled)
   {
      status = qvp_rtp_configure_srtp(stream, config_msg);
	  stream->is_srtp_enabled = TRUE;
	  if(status != QVP_RTP_SUCCESS)
	  {
	    QVP_RTP_ERR_0("SRTP enabled: qvp_rtp_configure_srtp failed");
	    stream->is_srtp_enabled = FALSE;
	  }
	  
   }
   else
   {
     stream->is_srtp_enabled = FALSE;
   }
   
  /*------------------------------------------------------------------------
    If we are succeeding before then try and config RTCP
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS ) && 
                   stream->rtcp_hdl  && 
                   config_msg->config.rtcp_tx_config.rtcp_valid && 
                   config_msg->config.rtcp_tx_config.rtcp_tx_enabled)

  {
    qvp_rtp_profile_property_type property;
    /*----------------------------------------------------------------------
    Get the profile property
    ----------------------------------------------------------------------*/
    status = qvp_rtp_find_profile_property( config_msg->config.payload,
      &property );

    /*----------------------------------------------------------------------
      remote ip will be used from the remote ip in the rtcp plane
    ----------------------------------------------------------------------*/
    qvp_rtp_memscpy( &rtcp_config.tx_rtcp_addr,sizeof(qvp_rtp_ip_addr_type),
            &config_msg->config.connection_ip,
            sizeof(qvp_rtp_ip_addr_type) );

    /*----------------------------------------------------------------------
      Copy the RTCP configuration from config msg right into the RTCP
      config param
    ----------------------------------------------------------------------*/
    rtcp_config.rtcp_tx_config = config_msg->config.rtcp_tx_config; 
    rtcp_config.channel_dir = stream->stream_type;
    rtcp_config.maxptime = config_msg->config.config_rx_params.rx_rtp_param. \
                                                           maxptime;
    rtcp_config.rx_clk_rate = property.clk_rate;
    rtcp_config.tx_clk_rate = property.clk_rate;

    /*----------------------------------------------------------------------
      Configure the RTCP now. We have backed up the old settings.
      RTCP can be restored to the old settings if we fail
    ----------------------------------------------------------------------*/
    status = qvp_rtcp_configure( stream->rtcp_hdl, &rtcp_config  );
    QVP_RTP_MSG_LOW_1( "RTCP configure status %d", status);
	if(rtcp_config.rtcp_tx_config.rtcp_xr_blocks.xr_stat_summ.xr_stat_summ_hl_enabled)
	{
		qvp_rtp_enable_recv_ttl(stream->ib_stream.sock_id);
	}
	
	if(status == QVP_RTP_SUCCESS && config_msg->config.is_srtp_enabled)
	{
	  
	  status = qvp_rtcp_configure_srctp(stream->rtcp_hdl, &srtcp_param);
	  
	  if(status != QVP_RTP_SUCCESS)
	  {
	    QVP_RTP_ERR_0("SRTP enabled: qvp_rtcp_configure_srctp failed");
	    stream->is_srtp_enabled = FALSE;	    
	  }
	}
  }
      
      
  /*------------------------------------------------------------------------
    If the existing profile is different from lat profile then we need to 
    tentatively close this and reopen with the new one.
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS ) && 
      ( config_msg->config.payload_valid) &&
      ( config_msg->config.payload != stream->payload_type ) ) 
  {

    /*----------------------------------------------------------------------
                 Find the right profile for the stream 
    ----------------------------------------------------------------------*/
    if ( ( stream->profile = qvp_rtp_get_payld_profile(
                  config_msg->config.payload ) ) == NULL )
    {
      QVP_RTP_MSG_HIGH_0(" qvp_rtp_configure_stream :: Profile not supported");
      status = QVP_RTP_ERR_FATAL;

    }
    else if ( stream->profile->open_profile( &stream->prof_handle, 
                  stream ) != QVP_RTP_SUCCESS )
    {
      QVP_RTP_MSG_HIGH_0(
        " qvp_rtp_configure_stream :: Profile could not  be opened ");
       status = QVP_RTP_ERR_FATAL;
    }
    else
    {

      
      
      /*--------------------------------------------------------------------
        Update the payload with the new value
      --------------------------------------------------------------------*/
      stream->payload_type = config_msg->config.payload; 
      
    }

    
  }
  if(QVP_RTP_PYLD_TXT == stream->payload_type)
  {
    QVP_RTP_MSG_HIGH_2( "RTP text stream = %x stream paylaod = %d", stream, stream->payload_type );
    stream->rtp.is_txt_stream = TRUE;
  }

  QVP_RTP_MSG_LOW_1( "RTP open profile status %d", status);
  
  /*------------------------------------------------------------------------
    The profile configurations does not need to be backed. It is expected
    that if the profiile could not configure the current param. Continue 
    using the previous.
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS ) && ( stream->profile->configure ) )
  {
    /*----------------------------------------------------------------------
      We dont want to modify the original input structure here
      Copy the profile config structure into a temp variable
    ----------------------------------------------------------------------*/
    qvp_rtp_memscpy( &profile_config, sizeof( qvp_rtp_payload_config_type ),&config_msg->config, 
                                            sizeof( qvp_rtp_payload_config_type ) );
#ifdef FEATURE_IMS_RTP_JITTERBUFFER    
    /*----------------------------------------------------------------------
      Update jitetr size from the stream params ( always )
    ----------------------------------------------------------------------*/
    if( stream->ib_stream.valid )
    {
      profile_config.config_rx_params.rx_rtp_param.jitter_valid = TRUE;
      profile_config.config_rx_params.rx_rtp_param.jitter_size = 
                                    stream->ib_stream.jitter_size;
    }
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/	
    /*----------------------------------------------------------------------
      If the profile supports configure just call it
      else. Profile is not configured
    ----------------------------------------------------------------------*/
    status = stream->profile->configure( stream->prof_handle, 
                                         &profile_config ); 
	if( (QVP_RTP_PYLD_G711_U == config_msg->config.payload) || (QVP_RTP_PYLD_G711_A == config_msg->config.payload))
	{
		//storing G711 silence PT
		stream->rtp.rx_cn_payload_type = config_msg->config.config_rx_params.config_rx_payld_params.g711_rx_config.cn_pyld_num;
		
		//storing G711 silence PT
		stream->rtp.tx_cn_payload_type = config_msg->config.config_tx_params.config_tx_payld_params.g711_tx_config.cn_pyld_num;
		QVP_RTP_MSG_HIGH_2( "rx_cn_payload_type = %x tx_cn_payload_type = %d", stream->rtp.rx_cn_payload_type, stream->rtp.tx_cn_payload_type );
	}

	/*if(stream->payload_type == QVP_RTP_PYLD_TXT )
	{
		//init the codec_txt contexts
		QVP_RTP_MSG_HIGH_1("qvP-rtp_configure -> stream context set calling rx_ctx_init = %x",stream);
		rtp_codec_txt_rx_ctx_init(stream,stream->prof_handle);
  }*/

 }
  QVP_RTP_MSG_LOW_1( "RTP profile configure status %d", status);

  /*------------------------------------------------------------------------
    If by any reason we did not succeed we will revert the state as if 
    nothing happened and this configure was a bad nightmare... :)
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS )
  { 

    /*-------------------------------------------------------------------
      Config failed. If an alloc jitter was performed due to a jitter   
      config request close the newly allocated jitter buffer and restore  
      the old buffer.
      OR
      if we opened a new buffer trying to change the channel type form ob 
      to ib/duplex we need to close the buffer.
    --------------------------------------------------------------------*/
#ifdef FEATURE_IMS_RTP_JITTERBUFFER    
    if ( ( old_jb_hdl != stream->ib_stream.jitter_queue )&&
         ( stream->ib_stream.jitter_queue != NULL ) )
    {
      qvp_rtp_jb_close_jitter_buffer( stream->ib_stream.jitter_queue );
    }
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
    /*-------------------------------------------------------------------
      Config failed. Restore the stream type, closing any new 
      sockets opened during configuration and also flag the earlier 
      valid sockets as valid( in case they were made invalid ).
    --------------------------------------------------------------------*/

	/*----------------------------------------------------------------------
     If there was a change in the profile we need to close old one
    ----------------------------------------------------------------------*/

    if( stream->prof_handle != old_hdl )
    {
      stream->profile->close_profile( stream->prof_handle );

	  //TODO : verify this logic
	  /*if (stream->payload_type == QVP_RTP_PYLD_TXT)
	  {
		  rtp_codec_txt_rx_ctx_init(stream,stream->prof_handle);
	  }*/
    }
    if ( old_stream_type != stream->stream_type )
    {
      qvp_rtp_close_unused_socket( stream->stream_type,
                        old_stream_type, stream, nw_hdl );
      stream->stream_type = old_stream_type;
    }

    /*----------------------------------------------------------------------
       Copy back the stream state and then go from there
    ----------------------------------------------------------------------*/
    qvp_rtp_memscpy( stream, sizeof( orig_stream ), &orig_stream, sizeof( orig_stream ) ); 

    if ( stream->ob_stream.valid == TRUE )
    {
	  uint8 i = 0;
      if( stream->ob_stream.remote_ip.ipaddr_type != QVP_RTP_IPV4 )
      {
        orig_tx_qos.use_this_tos = FALSE;
      }
      qvp_rtp_set_nw_qos_ob( stream->ob_stream.sock_id, orig_tx_qos);

      while(stream->ob_stream.remote_ip.ipaddr[i] != '\0')
      {
        QVP_RTP_MSG_HIGH("remote_ip address[%d] = %c remote port = %d",i,stream->ob_stream.remote_ip.ipaddr[i],stream->ob_stream.remote_port);
        i++;
      }	  
      stream->rtp_packet_source_check = FALSE;
	  if( QVP_RTP_SUCCESS != qvp_rtp_conver_to_netowrk_order(&stream->stream_remote_ipaddr, &stream->ob_stream.remote_ip))
	  {
	    stream->rtp_packet_source_check = FALSE;
		QVP_RTP_MSG_LOW_0("qvp_rtp_conver_to_netowrk_order failed");
	  }
	  else
	  {
	    stream->rtp_packet_source_check = TRUE;
		//QVP_RTP_MSG_LOW_0("qvp_rtp_conver_to_netowrk_order success");
	  }
      qvp_rtp_set_nw_ob_dest( stream->ob_stream.sock_id, 
                              &stream->ob_stream.remote_ip,
                              stream->ob_stream.remote_port );
      qvp_rtp_set_rtp_tx_payld_type( &stream->rtp, orig_tx_payload ); 
    }

    if ( stream->ib_stream.valid == TRUE )
    {
      qvp_rtp_set_nw_qos_ib( stream->ib_stream.sock_id, orig_rx_qos);

      qvp_rtp_set_rtp_rx_payld_type( &stream->rtp, orig_rx_payload, stream->dtmf_rx_pt ); 
    }

    if ( stream->rtcp_hdl )
    {
      qvp_rtcp_configure( stream->rtcp_hdl, &orig_rtcp_config );
    }

    QVP_RTP_ERR_2( " RTP configuration failed for PT:%d Restored to PT:%d ",
                config_msg->config.payload, stream->payload_type);

    return( QVP_RTP_ERR_FATAL );
    
  }
  else
  {
    
    /*----------------------------------------------------------------------
      Config succeeded. 
    ----------------------------------------------------------------------*/

    /*----------------------------------------------------------------------
     If there was a change in the profile we need to close old one
    ----------------------------------------------------------------------*/
    if( stream->prof_handle != old_hdl )
    {
      old_profile->close_profile( old_hdl );

	  //TODO : verify this logic
	  /*if (stream->payload_type == QVP_RTP_PYLD_TXT)
	  {
		  rtp_codec_txt_rx_ctx_init(stream,stream->prof_handle);
	  }*/
    }

	if(stream->rtp_packet_source_check)
	{
	   stream->rtp_packet_source_check = config_msg->config.config_rx_params.rx_rtp_param.blocking_enable;
	}
   QVP_RTP_MSG_LOW_1("RX RTP packet blocking based on IP flag = %d", stream->rtp_packet_source_check);
	   
    /*----------------------------------------------------------------------
      If an alloc jitter was performed
      due to a jitter config request 
      Or 
      if no ib component is needed due to channel direction 
      configuration,
      close the old jitter buffer.
    ----------------------------------------------------------------------*/
    if ( close_old_jb == TRUE )
    {
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
      qvp_rtp_jb_close_jitter_buffer( old_jb_hdl );
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
    }

    /*----------------------------------------------------------------------
      If we changed the channel direction successfully just close the 
      unused sockets ( if any ).
    ----------------------------------------------------------------------*/
    qvp_rtp_close_unused_socket( old_stream_type, 
                      stream->stream_type, stream, nw_hdl );

    return( QVP_RTP_SUCCESS );
    
  }

  
} /* end of function qvp_rtp_configure_stream */
/*===========================================================================

FUNCTION QVP_RTP_CONFIGURE2_STREAM

DESCRIPTION

   This routine configures a preallocated streams using the parameters 
   passed.
  
DEPENDENCIES
  None

  ARGUEMENTS IN
    stream - stream being configured
    config_param - payload specific parameters.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_configure2_stream
(
  qvp_rtp_stream_type          *stream,
  qvp_rtp_payload_config2_type *config_param
)
{
  qvp_rtp_payload_config_type config_msg;
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  /*------------------------------------------------------------------------
    Check for valid arguments 
  ------------------------------------------------------------------------*/
  if( !stream || !config_param )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Initialize the config msg to be used during configure call. Only 
    the payload specific information is updated in this message.
  ------------------------------------------------------------------------*/
  memset( &config_msg, 0 , sizeof( config_msg ) );

  /*------------------------------------------------------------------------
    Copy stream payload type to config message
  ------------------------------------------------------------------------*/
  config_msg.payload = stream->payload_type;

  /*------------------------------------------------------------------------
    If the user is asking for the inbound stream configure it
  ------------------------------------------------------------------------*/
  if( ( stream->ib_stream.valid == TRUE ) && 
      ( config_param->config_rx_valid == TRUE ) )
  {
    status = qvp_rtp_set_rtp_rx_payld_type( &stream->rtp,
                                config_param->rx_payload_type, stream->dtmf_rx_pt );
	//Saving G711 silence PT
	stream->rtp.rx_cn_payload_type = config_param->config_rx.g711_rx_config.cn_pyld_num;

    if ( status == QVP_RTP_SUCCESS )
    {
      config_msg.config_rx_params.valid = TRUE;
      qvp_rtp_memscpy( &config_msg.config_rx_params.config_rx_payld_params,
              sizeof( qvp_rtp_config_rx_payld_params_type ),
              &config_param->config_rx, 
             sizeof( config_msg.config_rx_params.config_rx_payld_params ) );
    }
  }
  if( ( status == QVP_RTP_SUCCESS ) && ( stream->ob_stream.valid == TRUE) 
         && ( config_param->config_tx_valid == TRUE ) )
  {
    /*----------------------------------------------------------------------
      Now set the new payload type.
    ----------------------------------------------------------------------*/
    status = qvp_rtp_set_rtp_tx_payld_type( &stream->rtp,
      config_param->tx_payload_type );
	//storing G711 tx silence PT
	stream->rtp.tx_cn_payload_type = config_param->config_tx.g711_tx_config.cn_pyld_num;
	
    config_msg.config_tx_params.valid = TRUE;
    qvp_rtp_memscpy( &config_msg.config_tx_params.config_tx_payld_params,
      sizeof(qvp_rtp_config_tx_payld_params_type),
            &config_param->config_tx, 
            sizeof( config_msg.config_tx_params.config_tx_payld_params ) );
	QVP_RTP_ERR_2( " tx_cn_payload_type:%d rx_cn_payload_type:%d ",
                stream->rtp.tx_cn_payload_type, stream->rtp.rx_cn_payload_type);
  }
  /*------------------------------------------------------------------------
    If the profile supports configure just call it
    else. Profile is not configured
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS ) && (stream->profile->configure ) )
  {
    status = stream->profile->configure( stream->prof_handle, 
                                       &config_msg ); 

	/*if(stream->payload_type == QVP_RTP_PYLD_TXT)
	{
		//call codec_txt contexts initialize
		QVP_RTP_MSG_HIGH_1("qvP-rtp_configure -> stream context set calling rx_ctx_init = %x",stream);
		rtp_codec_txt_rx_ctx_init(stream,stream->prof_handle);
	}*/
  }
  return ( status );
}/* end of qvp_rtp_configure2_stream() */
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
/*===========================================================================

FUNCTION QVP_RTP_ALLOC_JITTER_BUFFER

DESCRIPTION

  This routine allocates a jitter buffer for an inbound channel
  
DEPENDENCIES
  None

PARAMETERS
    stream      -  to which jitter buffer should be attached
                   this should have a valid inbound stream
    jitter_size - size of de-jitter buffer

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_alloc_jitter_buffer
(
  qvp_rtp_stream_type *stream,
  uint16              jitter_size
)
{
  qvp_rtp_profile_property_type property;
  qvp_rtp_status_type  status;
/*------------------------------------------------------------------------*/

    /*----------------------------------------------------------------------
       Check for valid arguments 
    ----------------------------------------------------------------------*/
    if( !stream )
    {
      return( QVP_RTP_ERR_FATAL );
    }

    if ( ( !jitter_size ) || ( jitter_size >  QVP_RTP_MAX_JITTER_SIZE ) )
    {
      QVP_RTP_ERR_1(" qvp_rtp_alloc_jitter_buffer ::Invalid jitter \
                                 size:%d", jitter_size);
      return( QVP_RTP_ERR_FATAL );
    }

    /*----------------------------------------------------------------------
       Get the profile property
    ----------------------------------------------------------------------*/
    status = qvp_rtp_find_profile_property( stream->payload_type,
                                              &property );
  
    /*----------------------------------------------------------------------
      if we fail to recognise codec just bail out
    ----------------------------------------------------------------------*/
    if( status != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_1(" qvp_rtp_alloc_jitter_buffer : Find profile property \
                                 error:%d", status);
      return( status );
    }

    status = qvp_rtp_jb_open_jitter_buffer( 
                                   &stream->ib_stream.jitter_queue, 
                                   property.mode,
                                   stream,
                                   property.clk_rate,
                                   property.pkt_interval,
                                   jitter_size,
                                   jitter_size
                                   ); 

    if( status != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_1( "Could not open jitter buffer handle error %d ", 
                        status);
    }

    return( status );    

} /* end of function  qvp_rtp_alloc_jitter_buffer */
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
/*===========================================================================

FUNCTION QVP_RTP_SHUTDOWN_CMD 

DESCRIPTION

  This routine shuts down the RTP module and all the submodules to a 
  state they can be reinitialized afresh and start doing the normal oper-
  ations upon init.
  
DEPENDENCIES
  None

PARAMETERS

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_shutdown_cmd( void  )
{
  int user_cnt = 0; /* index variables */
  qvp_rtp_deregister_msg_type deregister_msg; /* to be used for 
                                               * deregistering the app
                                               */
/*------------------------------------------------------------------------*/



  /*------------------------------------------------------------------------
    If already shut down why care now
  ------------------------------------------------------------------------*/
  if( !qvp_rtp_initialized )
  {
    return;
  }
  
  /*------------------------------------------------------------------------
    Deregister each user.
  ------------------------------------------------------------------------*/
  for( user_cnt = 0 ; user_cnt < rtp_ctx.num_users ; user_cnt++ )
  {

    deregister_msg.app_id = ( qvp_rtp_app_id_type ) user_cnt;

    /*----------------------------------------------------------------------
      Deregister app closing all the sockets associated with the app
    ----------------------------------------------------------------------*/

    qvp_rtp_deregister_app_cmd( &deregister_msg );

  } /* end of for (user_cnt = 0 )*/

  /*------------------------------------------------------------------------
    Clear out the housekeeping timer
  ------------------------------------------------------------------------*/
  if( rtp_ctx.hk_timer )
  {
    qvp_rtp_free_timer( rtp_ctx.hk_timer );
  }

  /*------------------------------------------------------------------------
                   Shut down NW layer.
  ------------------------------------------------------------------------*/
  qvp_rtp_shutdown_nw();
  

  /*------------------------------------------------------------------------
                  Shutdown All profiles
  ------------------------------------------------------------------------*/
  qvp_rtp_profiles_shutdown( );

  /*------------------------------------------------------------------------
                  Shut down the Jitter buffer layer
  ------------------------------------------------------------------------*/
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
  qvp_rtp_jb_shutdown( );
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/

  /*------------------------------------------------------------------------
                Shutdown QVP_RTP memory management array
  ------------------------------------------------------------------------*/
  qvp_rtp_shutdown_buf();

  /*------------------------------------------------------------------------
    Shutdown RTCP context
  ------------------------------------------------------------------------*/
  qvp_rtcp_shutdown();

  /*------------------------------------------------------------------------
    Shut down the core RTP by freeing all taking out all the resources
    and flagging the resources as used
  ------------------------------------------------------------------------*/
  qvp_rtp_free( rtp_ctx.user_array );
  rtp_ctx.user_array = NULL;
  rtp_ctx.num_users = 0;
  qvp_rtp_initialized = FALSE;
  
  
  return;
  
  
} /* end of function qvp_rtp_shutdown_cmd */

/*===========================================================================

FUNCTION QVP_RTCP_ALLOC_OPEN_RTCP

DESCRIPTION
  This routine allocates and open an RTCP session. The handle is stored in 
  passed in pointer.


  
DEPENDENCIES
  None

ARGUMENTS IN
  open_msg - app  message which caused the open
  stream   - stream context to which open is attached.
  nw_hdl   - network handle of app.


RETURN VALUE
  NW priority appropriate for the given payload.


SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_alloc_open_rtcp
(
  qvp_rtp_open_msg_type    *open_msg,
  qvp_rtp_stream_type      *stream,
  qvp_rtp_network_hdl_type nw_hdl
)
{
  qvp_rtcp_session_param        rtcp_param;
  qvp_rtp_status_type           status;
  qvp_rtp_profile_property_type property;
/*------------------------------------------------------------------------*/



  /*------------------------------------------------------------------------
    Regarless of the directionality of the channel we will need Reciever
    Report/Sender report reception and transmission so rtcp is full 
    duplex. Application can disable transmissions by flipping the
    tx_enabled flag. And ignore reports by specifying thr i/b port to zero
  ------------------------------------------------------------------------*/
  if( !open_msg || !stream )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  
  /*------------------------------------------------------------------------
    Specify the interface we want to use
  ------------------------------------------------------------------------*/
  rtcp_param.iface_hdl = open_msg->stream_params.iface_hdl;
  

  /*------------------------------------------------------------------------
     Get the profile property
  ------------------------------------------------------------------------*/
  if( qvp_rtp_find_profile_property( stream->payload_type,
                                     &property ) != QVP_RTP_SUCCESS )
  {
    QVP_RTP_ERR_0("qvp_rtp_alloc_open_rtcp ->qvp_rtp_find_profile_property failed");
    return( QVP_RTP_ERR_FATAL );
  }
    
  rtcp_param.rx_clk_rate = property.clk_rate; 
  rtcp_param.rx_ptime    = property.pkt_interval;  

  /*------------------------------------------------------------------------
    Copy canonical name so we will advertise that in SDES
  ------------------------------------------------------------------------*/

  if (strlcpy( (char *)rtcp_param.tx_cname,
        (char *)open_msg->stream_params.cname,
        sizeof( rtcp_param.tx_cname ) ) >=
        sizeof( rtcp_param.tx_cname ) )
  {
    QVP_RTP_ERR_2("qvp_rtp_memscpy failed, src len %d, dst len %d", 
      strlen( ( const char *)open_msg->stream_params.cname ), 
      sizeof( rtcp_param.tx_cname ));
    return( QVP_RTP_WRONG_PARAM );
  }

  /*------------------------------------------------------------------------
    Place where to send SR and SDES
  ------------------------------------------------------------------------*/

  qvp_rtp_memscpy( &rtcp_param.tx_rtcp_addr,sizeof(qvp_rtp_ip_addr_type),
          &open_msg->stream_params.remote_ip,
         sizeof(qvp_rtp_ip_addr_type) );

  /*------------------------------------------------------------------------
    Copy the rtcp transmit configuration structure from open message
  ------------------------------------------------------------------------*/

  rtcp_param.rtcp_config = open_msg->stream_params.rtcp_tx_config;

  /*------------------------------------------------------------------------
    port where we need to listen for RTCP reports.
    If the local RTCP port is not passed take it as RTP local port + 1.
  ------------------------------------------------------------------------*/
  if ( open_msg->stream_params.rtcp_lcal_prt )
  {
    rtcp_param.rx_rtcp_port = open_msg->stream_params.rtcp_lcal_prt;
  }
  else if ( ( stream->ib_stream.valid ) && 
            ( stream->ib_stream.local_port ) )
  {
    rtcp_param.rx_rtcp_port = stream->ib_stream.local_port + 1;
  }

  if ( ( open_msg->stream_params.rtcp_tx_config.rtcp_valid ) && 
       ( open_msg->stream_params.rtcp_tx_config.rtcp_tx_enabled ) )

  {
    /*----------------------------------------------------------------------
      port from where we need to send RTCP reports.
      If we get a finite value for RTCP outbound local port we use it 
      else we check for RTP outbound local port and if it has a finite value
      we use the next higher port as RTCP outbound local port.
      If none of the above RTCP o/b local port is set to "0".
    ----------------------------------------------------------------------*/
    if ( open_msg->stream_params.rtcp_ob_lcal_prt )
    {
      rtcp_param.tx_rtcp_local_port = 
         open_msg->stream_params.rtcp_ob_lcal_prt;
    }
    else if ( open_msg->stream_params.ob_lcl_port )
    {
      rtcp_param.tx_rtcp_local_port = 
        open_msg->stream_params.ob_lcl_port + 1;
    }
    else
    {
      rtcp_param.tx_rtcp_local_port = 0;
    }

    /*----------------------------------------------------------------------
      port where we need to transmit RTCP reports.
      If the remote RTCP port is not passed take it as RTP remote port + 1.
    ----------------------------------------------------------------------*/
    if ( ( !open_msg->stream_params.rtcp_tx_config.tx_rtcp_port ) &&
             ( stream->ob_stream.valid == TRUE ) &&
             ( stream->ob_stream.remote_port ) )
    {
      rtcp_param.rtcp_config.tx_rtcp_port = 
                stream->ob_stream.remote_port + 1;
    }
    else
    {
      QVP_RTP_MSG_HIGH_1( " setting remote rtcp port %d",
                      open_msg->stream_params.rtcp_tx_config.tx_rtcp_port);
      rtcp_param.rtcp_config.tx_rtcp_port = 
                open_msg->stream_params.rtcp_tx_config.tx_rtcp_port;
      qvp_rtp_memscpy(&rtcp_param.rtcp_config.tx_rtcp_ip,sizeof(qvp_rtp_ip_addr_type),
                &open_msg->stream_params.rtcp_tx_config.tx_rtcp_ip,sizeof(qvp_rtp_ip_addr_type));
    }

  }/* if rtcp_tx_enabled */
  
  /*------------------------------------------------------------------------
    RTCP needs to know the directionality of the session
  ------------------------------------------------------------------------*/
  rtcp_param.channel = open_msg->stream_params.channel_type;


  /*------------------------------------------------------------------------
    Set our RTP TC SSRC in the RTCP context so it can use it
  ------------------------------------------------------------------------*/
  qvp_rtp_get_rtp_tx_ssrc( &stream->rtp, &rtcp_param.tx_ssrc );

  /*------------------------------------------------------------------------
    propogate the user's network hanlde to rtcp
  ------------------------------------------------------------------------*/
  rtcp_param.nw_hdl = nw_hdl;

  rtcp_param.is_srtp_enabled = open_msg->stream_params.is_srtp_enabled;

  /*------------------------------------------------------------------------
    Open a unicast session.
  ------------------------------------------------------------------------*/
  status = qvp_rtcp_open_unicast_session( &stream->rtcp_hdl, stream, 
                                          qvp_rtp_handle_rtcp_info,
    qvp_rtp_handle_rtcp_link_aliveness_info,
    &rtcp_param,open_msg->stream_params.ip_address,
    open_msg->stream_params.app_addr_type,
    open_msg->stream_params.profile_id,
	  open_msg->stream_params.iface_id,
	  open_msg->stream_params.apn_name,
	  open_msg->stream_params.rat_type,
open_msg->stream_params.as_id);

  /*------------------------------------------------------------------------
    If we did not succeed make sure we do not flag it as open
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS )
  {
    stream->rtcp_hdl = NULL;
  }
  else 
  {
    if(NULL != rtp_ctx.metrics && NULL != rtp_ctx.metricTh)
    {
      QPDPL_MEDIA_METRICS* temp = (QPDPL_MEDIA_METRICS*)rtp_ctx.metricTh;

      qvp_rtcp_set_jitter_metric(stream->rtcp_hdl,
        temp->m_sDL95Jitter.iAccept,
        rtp_ctx.metrics->dl_metrics.rtp_jitter95p_cnt
        ); 
    }
  }
                                          
  return( status );
    

} /* end of function qvp_rtcp_alloc_open_rtcp */ 

/*===========================================================================

FUNCTION QVP_RTP_GET_NW_PRIORITY

DESCRIPTION

  This routine gets the appropriate NW priority for a particular type
  of payload requested.

  
DEPENDENCIES
  None

PARAMETERS
  payload - type of payload requested

RETURN VALUE
  NW priority appropriate for the given payload.


SIDE EFFECTS
  None

===========================================================================*/
LOCAL qvp_rtp_traffic_priority_type qvp_rtp_get_nw_priority
(
  qvp_rtp_payload_type payload
)
{
    qvp_rtp_traffic_priority_type traffic_priority;
/*------------------------------------------------------------------------*/
  
    /*----------------------------------------------------------------------
        Switch based on payload type audio and video need to be treated 
        different.
    ----------------------------------------------------------------------*/
    switch( payload )
    {

      /*--------------------------------------------------------------------
          List all audio codecx there
      --------------------------------------------------------------------*/
      case QVP_RTP_PYLD_EVRC :
      case QVP_RTP_PYLD_AMR :
      case QVP_RTP_PYLD_G723 :
      case QVP_RTP_PYLD_EVRC0:
      case QVP_RTP_PYLD_EVRCB0:
      case QVP_RTP_PYLD_EVRCB:
      case QVP_RTP_PYLD_EVRC1:
      case QVP_RTP_PYLD_EVRCB1:
      case QVP_RTP_PYLD_EVRCWB:
      case QVP_RTP_PYLD_EVRCWB1:
      case QVP_RTP_PYLD_EVRCWB0:
      case QVP_RTP_PYLD_RAW_AUD :
      case QVP_RTP_PYLD_G722 :
      case QVP_RTP_PYLD_G711_A:
      case QVP_RTP_PYLD_G711_U:
      case QVP_RTP_PYLD_G729:
      case QVP_RTP_PYLD_G722_2:
	    case QVP_RTP_PYLD_EVS:
        traffic_priority = QVP_RTP_TRAFFIC_AUD_PRI; 
        break;
  case QVP_RTP_PYLD_TXT:
    traffic_priority = QVP_RTP_TRAFFIC_TXT_PRI;
        break;

      /*--------------------------------------------------------------------
          DTMF TELEPHONE EVENT has the same priority as audio codecs but had  
          a separate case so that if there any changes specific to DTMF we 
          know this is the place to change. 
      --------------------------------------------------------------------*/

      case QVP_RTP_PYLD_TEL_EVENT:
      {
        traffic_priority = QVP_RTP_TRAFFIC_AUD_PRI; 
        break;
      }
      /*--------------------------------------------------------------------
          List all video codecx there
      --------------------------------------------------------------------*/
      case QVP_RTP_PYLD_MP4_ES :
      case QVP_RTP_PYLD_MP4_GENERIC :
      case QVP_RTP_PYLD_H263        :
      case QVP_RTP_PYLD_H263_2000   :
      case QVP_RTP_PYLD_H263_1998   :
      case QVP_RTP_PYLD_RAW_VID     :
      case QVP_RTP_PYLD_H264        :
        traffic_priority = QVP_RTP_TRAFFIC_VID_PRI; 
        break;

      default:
        traffic_priority = QVP_RTP_TRAFFIC_LAST_PRI; 
        break;
        
    } /* end of switch */
  
    return( traffic_priority );
} /* end of function qvp_rtp_get_nw_priority */ 

/*===========================================================================

FUNCTION QVP_RTP_HANDLE_RTCP_INFO

DESCRIPTION

  This function is called from RTCP sub layer every time a SDES/SR or any 
  other report is received from the network. This routines funnels the 
  info to appropriate entities to take action

  
DEPENDENCIES
  None

PARAMETERS
  handle       - handle to RTCP session
  app_hdl      - handle we supplied to RTCP
  sess_info    - info we are getting

RETURN VALUE
  NW priority appropriate for the given payload.


SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_handle_rtcp_info
(

  qvp_rtcp_session_handle_type handle,
  qvp_rtcp_app_handle_type     app_hdl,
  qvp_rtcp_session_info_type   *sess_info,
  qvp_rtcp_local_param_type*   rtcp_stats
)
{
  qvp_rtp_stream_type *stream = ( qvp_rtp_stream_type *) app_hdl;
  qvp_rtp_usr_ctx *user_ctx; 
/*------------------------------------------------------------------------*/

  
  /*------------------------------------------------------------------------
    Ignore the info if some thing does not make sense.
  ------------------------------------------------------------------------*/
  if( !stream || !sess_info || ( stream->rtcp_hdl != handle ) ||
      !rtcp_stats || !stream->usr_ctx )
  {
    QVP_RTP_ERR_0( " Params in rtcp_info call back in error");
    return;
  }
#ifdef FEATURE_IMS_RTP_JITTERBUFFER  
  if( (sess_info->inf_type == QVP_RTCP_SR) &&
    ( stream->ib_stream.jitter_queue ) )
  {
    qvp_rtp_jitter_buffer_report_ntp_wl( stream->ib_stream.jitter_queue,
                                         sess_info->info.sr.ntp,
                                         sess_info->info.sr.tstamp ); 
  }
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/

  /*------------------------------------------------------------------------

    :ARR-APTO:TODO

    1) Recv vid frame rate has to be estimated here by calling
       qvp_rtp_jitter_buffer_vid_report_rx_frm_rate

       CRD had a hack to pass in the avg vid rx frame rate here
       They took the curr_frame_rate from the logging module and treated
       it as avg vid rx frame rate here 

    2) If this is a video RTCP_RR then RTT has to be estimated by calling
       qvp_rtp_jitter_buffer_vid_report_rtt
    
  ------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Notify application
  ------------------------------------------------------------------------*/
  user_ctx = (qvp_rtp_usr_ctx*)stream->usr_ctx;
  //if( !user_ctx || !user_ctx->call_backs.handle_rtcp_cb )
  if ((qvp_rtp_get_media_type(user_ctx->app_id,stream->str_id) == QVP_RTP_AUDIO))
  {
    qvp_rtp_handle_audio_rtcp_cb(user_ctx->app_id,stream->str_id,sess_info,rtcp_stats);
  }
  else if ((qvp_rtp_get_media_type(user_ctx->app_id,stream->str_id) == QVP_RTP_VIDEO))
  {
    //qvp_rtp_handle_video_rtcp_cb(user_ctx->app_id,stream->str_id,sess_info,rtcp_stats,stream->ob_stream.remote_ip.ipaddr_type);
  }
  else
  {
    QVP_RTP_ERR_0(" qvp_rtp_handle_rtcp_info: Invalid media type: Dropping RTCP report");
  }
} /* qvp_rtp_handle_rtcp_info */



int compareIpAddress(QPNET_ADDR ip1,QPNET_ADDR ip2)
{
	int returnValue = 0;
	
	if(ip1.ipAddrType!=ip2.ipAddrType)
		return returnValue;
	
	if(ip1.ipAddrType == IPV4)
	{
		if(ip1.ipAddress.iIPV4Address == ip2.ipAddress.iIPV4Address)
			{
			returnValue = 1;
			}
		else
			{
			returnValue = 0;
			}
	}
	else if(ip1.ipAddrType == IPV6)
	{
		if(strncmp((char *)ip1.ipAddress.iIPV6Address,(char*)ip2.ipAddress.iIPV6Address,(size_t)QP_IPV6_ADDR_NUM_OF_OCTATE) == 0)
			{
			returnValue = 1;
			}
		else
			{
			returnValue = 0;
			}
	}
	else
		returnValue = 0;
	
return returnValue;
}


/*===========================================================================

FUNCTION QVP_RTP_AUTHENTICATE_DECRYPT_PKT	 


DESCRIPTION
  If SRTP is ENABLED, it will authenticate and decrypt the packet  

DEPENDENCIES
  None

ARGUMENTS IN
  pkt - packet being received .
  user_data - the stream pointer to which the packet is associated. Passed
              in as void pointer.

RETURN VALUE
  QVP_RTP_SUCCESS  - on rsuccess or failure code. 
                    


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_authenticate_decrypt_pkt 
(
  qvp_rtp_buf_type *pkt,      /* packet profile decoded */ 
  qvp_rtp_stream_type   *stream
)
{
	uint32 OutDataLen = 0;
	uint64 index = 0;
	uint8 tx_auth_tag[10] = {0};
	uint8 rx_auth_tag[10] = {0};
	qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;
	/*------------------------------------------------------------------------
	Check for valid arguments
	------------------------------------------------------------------------*/
	if( !stream )
	{
		QVP_RTP_ERR_1(" SRTP enabled qvp_rtp_authenticate_decrypt_pkt: Invalid Stream", status);
		return( QVP_RTP_ERR_FATAL );
	}

	//Authentication validity
	if(stream->srtp_ctx.auth_handle)
	{ 
		uint8* temp_src = NULL;
		uint16 temp_len = 0;
		int i =0;
		if( stream->rx_srtp_info.hash_algo == QVP_RTP_HMAC_SHA1_80)
			OutDataLen = 10;
		else if (stream->rx_srtp_info.hash_algo == QVP_RTP_HMAC_SHA1_32)
			OutDataLen = 4;
		temp_len += pkt->len;
		temp_len += pkt->head_room;
		temp_len -= OutDataLen;
		QVP_RTP_MSG_LOW_1("pkt->data address %p ",pkt->data);
		temp_src = pkt->data +  temp_len;
		qvp_rtp_memscpy(tx_auth_tag, 10, temp_src, OutDataLen);

		temp_len = (pkt->len - OutDataLen);
		temp_len += pkt->head_room;
		qvp_rtp_memscpy((pkt->data + temp_len), OutDataLen, &stream->rtp.rx_ROC, 4);
		status =  qvp_srtp_hash(stream->srtp_ctx.auth_handle,
			pkt->data,
			rx_auth_tag,
			(temp_len + 4),
			OutDataLen,
			stream->srtp_ctx.rx_keys.K_a);
		for(i=0; i<OutDataLen; i++)
		{
			QVP_RTP_MSG_LOW("SRTP enabled: prfile_rx rx authentication Tag[%d] = %u RX roc = %lu",i,rx_auth_tag[i],stream->rtp.rx_ROC);
		}									
		if(status != QVP_RTP_SUCCESS || (memcmp(tx_auth_tag,rx_auth_tag,OutDataLen) != 0 ))
		{
			if( pkt->need_tofree )
			{
				qvp_rtp_free_buf( pkt );
			}
			QVP_RTP_ERR_1(" SRTP enabled: Authentication failed status = %d", status);
			return( QVP_RTP_ERR_FATAL  );	  
		}
		pkt->len -= OutDataLen;	  
		//QVP_RTP_ERR_1(" SRTP enabled: Authentication successful status = %d", status);
	} 

	QVP_RTP_MSG_LOW("qvp_rtp_profile_rx seq no = %u len = %u headroom = %u",pkt->seq, pkt->len, pkt->head_room);
	QVP_RTP_MSG_LOW("qvp_rtp_profile_rx rx_sl = %lu rx_roc = %u ssrc = %u",stream->rtp.rx_sl, stream->rtp.rx_ROC, stream->rtp.rx_ssrc);
	index = qvp_rtp_rx_srtp_index(stream->rtp.rx_sl, stream->rtp.rx_ROC, pkt->seq);
	//decryption 
	status =  qvp_srtp_decrypt(stream->srtp_ctx.cipher_handle,
								(pkt->data+pkt->head_room),
								(pkt->data+pkt->head_room),
								(pkt->len),
								&OutDataLen, 
								index,
								stream->rtp.rx_ssrc,
								stream->srtp_ctx.rx_keys.K_e,
								stream->srtp_ctx.rx_keys.K_s);

	QVP_RTP_MSG_LOW_2("SRTP enabled: qvp_srtp_decrypt output data length = %lu index = %lu",OutDataLen,index);		

	if(status != QVP_RTP_SUCCESS || ( pkt->len ) != OutDataLen)
	{
		if( pkt->need_tofree )
		{
			qvp_rtp_free_buf( pkt );
		}
		QVP_RTP_ERR(" SRTP enabled: Decryption length mismatch/Error pkt length %u out data len = %u status = %d", pkt->len, OutDataLen, status );
		return( QVP_RTP_ERR_FATAL  );	  
	}	
	QVP_RTP_ERR(" SRTP enabled: Decryption success length %u out data len = %u status = %d", pkt->len, OutDataLen, status );												 

	return( QVP_RTP_SUCCESS );

}

/*===========================================================================

FUNCTION QVP_RTP_HANDLE_NW_PACKET 

DESCRIPTION

    Deals with each nw packet arrived. RTP header is stripped down and
    the remaining data is passed to the profile handler.
     
DEPENDENCIES
  None

ARGUMENTS IN
   None

RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful
  appropriate error code otherwise


SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_handle_nw_packet
(
  void                *stream_data,
  qvp_rtp_buf_type    *pkt_rcvd,
  QPNET_ADDR	remote_ip,
  uint16 remote_port  
)
{
  qvp_rtp_stream_type *stream = ( qvp_rtp_stream_type *) stream_data;
  qvp_rtp_profile_property_type property;
  uint32  discard_pkt_cnt = 0;
  uint16  pkt_len=0;
  uint16  head_room=0;
  uint64 ts_val = 0;
  qvp_rtp_usr_ctx *user_ctx = NULL;  
/*------------------------------------------------------------------------*/
  if( !stream ||
      !stream->valid || ( stream->state != QVP_RTP_SESS_ACTIVE  ) )
  {
    QVP_RTP_MSG_HIGH_0( " Discard NW packet received in wrong state");
    if(  pkt_rcvd != NULL ) 
    {
      if( pkt_rcvd->need_tofree )
      {
        qvp_rtp_free_buf(pkt_rcvd );
      }
    }
    return;
  }
	// QVP_RTP_MSG_HIGH_1("qvp_rtp_handle_nw_packet for stream->payload_type = %d ", stream->payload_type);
  /*------------------------------------------------------------------------
     if the packet passed is NULL just return error.
  ------------------------------------------------------------------------*/
  if( !pkt_rcvd )
  {
    QVP_RTP_ERR_0(" Unexpected NULL pkt from the network");
    return;
  }
  user_ctx = ( qvp_rtp_usr_ctx * ) stream->usr_ctx;

  if(stream->stream_state == INACTIVE_RX || stream->stream_state == INACTIVE)
  {
    QVP_RTP_MSG_MED_0( "qvp_rtp_handle_nw_packet :: Pause called on RX/TXRX stream -> dropping pacckets \r\n ");
    if( pkt_rcvd->need_tofree )
    {
      qvp_rtp_free_buf(pkt_rcvd );
    }
    return;
  }
  /*-----------------------------------------------------------------------
      Log the frame we got
  ------------------------------------------------------------------------*/
  /*qvp_rtp_log_frames ( (void *) (pkt_rcvd->data + pkt_rcvd->head_room) , 
                   pkt_rcvd->len, DL_RTP ); */
  stream->cur_rx_pkt_len = pkt_len = pkt_rcvd->len;
  head_room = pkt_rcvd->head_room;

  /*------------------------------------------------------------------------
      Try strip off the RTP header
  ------------------------------------------------------------------------*/
  if( qvp_rtp_unpack( &discard_pkt_cnt, &stream->rtp, pkt_rcvd ) != 
      QVP_RTP_SUCCESS )
  {
    if( pkt_rcvd->need_tofree )
    {
      qvp_rtp_free_buf( pkt_rcvd );

    }
    return;
  }

  if(  (pkt_rcvd->rtp_pyld_type > MAX_STATIC_PT_VAL) && (pkt_rcvd->rtp_pyld_type == stream->dtmf_rx_pt))
  {
    QVP_RTP_ERR("qvp_rtp_handle_nw_packet dtmf packet received seq = %d, time %d",pkt_rcvd->seq,pkt_rcvd->tstamp,0);
	memset(&g_rx_dtmf, 0, sizeof(qvp_rtp_send_dtmf_pkt_type));
	if(QVP_RTP_SUCCESS == qvp_rtp_unpack_dtmf_payload(pkt_rcvd->data+pkt_rcvd->head_room, pkt_rcvd->len, &g_rx_dtmf))
	{
		qvp_rtp_dtmf_payload_type pkt;
		pkt.digit_count = g_rx_dtmf.num_digits;//support only one digit to be sent
		//initial implementation to send only one event to app
		pkt.volume = g_rx_dtmf.dtmf_array[(g_rx_dtmf.num_digits-1)].vol;
		if(pkt_rcvd->marker_bit)
		{
		  pkt.dtmf_evt_type = QVP_RTP_DTMF_START;
		  dtmf_end_sent = FALSE;
		}
		else if(g_rx_dtmf.dtmf_array[(g_rx_dtmf.num_digits-1)].end_bit)
		{
		  pkt.dtmf_evt_type = QVP_RTP_DTMF_STOP;
		  if(dtmf_end_sent)
		  {
			QVP_RTP_ERR("dtmf dont send to app evnt %d vol %d digit %d",pkt.dtmf_evt_type,pkt.volume,pkt.digit);
			QVP_RTP_ERR("dtmf dont send to app digit_count %d end bit %d",pkt.digit_count,g_rx_dtmf.dtmf_array[(g_rx_dtmf.num_digits-1)].end_bit,0);		  
			if( pkt_rcvd->need_tofree )
			{
			  qvp_rtp_free_buf( pkt_rcvd );

			}	
			return;		    
		  }
		  if(!dtmf_end_sent)
		    dtmf_end_sent = TRUE;		  
		}
		else
		{
			pkt.dtmf_evt_type = QVP_RTP_DTMF_PROGRESS;
			dtmf_end_sent = FALSE;
		}
		pkt.digit = g_rx_dtmf.dtmf_array[(g_rx_dtmf.num_digits-1)].dtmf;
		QVP_RTP_ERR("dtmf send to app evnt %d vol %d digit %d",pkt.dtmf_evt_type,pkt.volume,pkt.digit);
		QVP_RTP_ERR("dtmf send to app digit_count %d end bit %d",pkt.digit_count,g_rx_dtmf.dtmf_array[(g_rx_dtmf.num_digits-1)].end_bit,0);
		if(user_ctx->call_backs.dtmf_evt_cb && pkt.dtmf_evt_type != QVP_RTP_DTMF_PROGRESS)
			qvp_rtp_response_handler( user_ctx->app_id, stream->str_id, (qvp_rtp_app_data_type)&pkt, QVP_RTP_RX_DTMF, 0, TRUE, 0);
	}
    if( pkt_rcvd->need_tofree )
    {
      qvp_rtp_free_buf( pkt_rcvd );

    }	
	return;
  }

  if(stream->rtp_packet_source_check && (!compareIpAddress(stream->stream_remote_ipaddr, remote_ip)))
  {
    QVP_RTP_ERR_0("Dropping packet as it does not match configured IP address");
	if( pkt_rcvd->need_tofree )
	{
	// QVP_RTP_ERR_0("freeing the buffer as it does not match configured IP address");
	 qvp_rtp_free_buf( pkt_rcvd );
	}
	return;
  }  

  if(stream->is_srtp_enabled && stream->rx_srtp_info_valid)
  {
	 // QVP_RTP_MSG_LOW_0( "qvp_rtp_handle_nw_packet :: srtp enabled -> decrypting packet\n ");
	  if( qvp_rtp_authenticate_decrypt_pkt( pkt_rcvd, stream ) != QVP_RTP_SUCCESS )
		  return;
  }

  qvp_rtp_time_get_ms(&ts_val);
  stream->T_last = ((uint32)ts_val);
  
  if(stream->first_pckt_rcvd == FALSE)
  {
	qvp_rtp_response_handler(user_ctx->app_id, stream->str_id, NULL, QVP_RTP_MEDIA_FIRST_PKT_RCVD_RSP, QVP_RTP_SUCCESS, TRUE, 0);
	stream->first_pckt_rcvd = TRUE;
  }

  //First packet ?
  if(!stream->rtp_log_pkt_ctx.first_packet)
  {
    stream->rtp_log_pkt_ctx.first_packet = TRUE;
    stream->rtp_log_pkt_ctx.ssrc = stream->rtp.rx_ssrc;
    stream->rtp_log_pkt_ctx.seq = (uint16)pkt_rcvd->seq;
	stream->rtp_log_pkt_ctx.begin_seq = pkt_rcvd->seq;
	stream->rtp_log_pkt_ctx.end_seq = pkt_rcvd->seq;
	stream->rtp_log_pkt_ctx.tot_rx_rtp_cnt = 0;
	qvp_rtcp_update_rx_min_seq( stream->rtcp_hdl, pkt_rcvd->seq );
	QVP_RTP_MSG_HIGH_1( "update first sequence number in the session = %u",pkt_rcvd->seq);
  }
  //Verfify Source Identifier
  if (stream->rtp_log_pkt_ctx.ssrc != stream->rtp.rx_ssrc) 
  {
    QVP_RTP_ERR_2(" warn SSRC of packets from src=%d has changed %d.\n", stream->rtp_log_pkt_ctx.ssrc, stream->rtp.rx_ssrc);
	qvp_rtp_log_commit_voice_call_stat(stream);
	
    stream->rtp_log_pkt_ctx.ssrc = stream->rtp.rx_ssrc;
	stream->rtp_log_pkt_ctx.seq = pkt_rcvd->seq;  // to reset the loss
	/*reset the begin and end seq numbers*/
	stream->rtp_log_pkt_ctx.begin_seq = pkt_rcvd->seq;	
	stream->rtp_log_pkt_ctx.end_seq = pkt_rcvd->seq;
	stream->rtp_log_pkt_ctx.tot_rx_rtp_cnt = 0;

    qvp_rtcp_hdl_ssrc_chg(stream->rtcp_hdl, stream->rtp.rx_ssrc, pkt_rcvd);
  }

  /* log incomming rtp data packet and important parameters*/
  if(stream->payload_type == QVP_RTP_PYLD_H264 || stream->payload_type == QVP_RTP_PYLD_H263)
  {

		qvp_rtp_log_pkt_v2 log_pak;
		uint16 rtp_logged_payload_size = pkt_len;
		uint8 rat_type = LTE; /*Default assumption is LTE*/

		memset( &log_pak , 0, sizeof( qvp_rtp_log_pkt_v2 ) );
				log_pak.version = 6 ; // version 6 for RTP type change
		
		/* Find RAT type for logging */
		if(stream->rat_type == QVP_RTP_RAT_LTE)
		   rat_type = LTE;
		else if((stream->rat_type == QVP_RTP_RAT_WLAN) || (stream->rat_type == QVP_RTP_RAT_IWLAN))
		   rat_type = WLAN;
		
		log_pak.direction_and_RAT = (NETWORK_TO_UE & 0x03) | ( (rat_type << 3) & 0xF8);
    log_pak.ssrc = stream->rtp.rx_ssrc;
		log_pak.rtp_seqno = (uint16)pkt_rcvd->seq;
		log_pak.rtp_tstamp = pkt_rcvd->tstamp;

		//Set logged payload size to zero for Video till NV is obtained
		if(stream->payload_type == QVP_RTP_PYLD_H264) {
			log_pak.media_type = VIDEO;
			log_pak.payload_size = pkt_len;
			log_pak.codec_type = H264;
			rtp_logged_payload_size = 0;
		}

		//Set logged payload size to zero for Video till NV is obtained
		if(stream->payload_type == QVP_RTP_PYLD_H263) {
			log_pak.media_type = VIDEO;
			log_pak.payload_size = pkt_len;
			log_pak.codec_type = H263;
			rtp_logged_payload_size = 0;
		}

		//QVP_RTP_ERR_1(" HandleNWpkt Log RTP payload type : %d\r\n", stream->payload_type);

    qvp_rtp_log_packets_v2( &log_pak,(void *) (pkt_rcvd->data + head_room + RTP_HEADER_LEN + pkt_rcvd->xtn_hdr_len) , 
        rtp_logged_payload_size);


  }


  //Lost or OutOfOrder packet?
  stream->rtp_log_pkt_ctx.tot_rx_rtp_cnt++;
  stream->rtp_log_pkt_ctx.is_dup=FALSE;
  if (stream->rtp_log_pkt_ctx.seq != pkt_rcvd->seq) 
  {
    if (stream->rtp_log_pkt_ctx.seq-1 == pkt_rcvd->seq) 
    {
      // Duplicated
      stream->rtp_log_pkt_ctx.dup++;
      stream->rtp_log_pkt_ctx.is_dup=TRUE;
	  if(stream->rtp_log_pkt_ctx.tot_rx_rtp_cnt)
	  	stream->rtp_log_pkt_ctx.tot_rx_rtp_cnt--;
    }
    else if (stream->rtp_log_pkt_ctx.seq > pkt_rcvd->seq) 
    {
      // Out Of Order
      stream->rtp_log_pkt_ctx.late++;
      if(stream->rtp_log_pkt_ctx.lost)
        stream->rtp_log_pkt_ctx.lost--;
    }
    else 
    {// Lost
      qvp_rtp_log_pkt_loss rtp_loss_pkt;
      memset( &rtp_loss_pkt , 0, sizeof( qvp_rtp_log_pkt_loss ) );
      stream->rtp_log_pkt_ctx.lost += (uint16)pkt_rcvd->seq - stream->rtp_log_pkt_ctx.end_seq - 1;
      //version 5 for change in loss packet field size from 8bit to 16bit
      rtp_loss_pkt.version = 6; 
      rtp_loss_pkt.ssrc = stream->rtp_log_pkt_ctx.ssrc;
      rtp_loss_pkt.nlost = stream->rtp_log_pkt_ctx.lost;
      if(stream->payload_type == QVP_RTP_PYLD_AMR) 
      {
				rtp_loss_pkt.codectype = AMR;
	  }

      else if(stream->payload_type == QVP_RTP_PYLD_G722_2) 
      {
				rtp_loss_pkt.codectype = AMR_WB;
	  }
      else if(stream->payload_type == QVP_RTP_PYLD_G711_U) 
      {
				rtp_loss_pkt.codectype = G711_U;
	  }
      else if(stream->payload_type == QVP_RTP_PYLD_G711_A) 
      {
				rtp_loss_pkt.codectype = G711_A;
	  }
      else if(stream->payload_type == QVP_RTP_PYLD_EVS) 
      {
				rtp_loss_pkt.codectype = EVS;
	  }	  
      else if(stream->payload_type == QVP_RTP_PYLD_H264) 
      {
				rtp_loss_pkt.codectype = H264;
	  }

      else if(stream->payload_type == QVP_RTP_PYLD_H263)
      {
				rtp_loss_pkt.codectype = H263;
	  }
      stream->rtp_log_pkt_ctx.lost = 0;
      rtp_loss_pkt.seq = stream->rtp_log_pkt_ctx.seq-1;
      QVP_RTP_MSG_HIGH_1( " rtp_loss_pkt = %u",rtp_loss_pkt.nlost);
     if(rtp_loss_pkt.nlost)
      rtp_log(LOG_IMS_RTP_PACKET_LOSS_C, &rtp_loss_pkt, sizeof(qvp_rtp_log_pkt_loss)); 
    }

  }

  // Calculate next number in sequence
  if(stream->rtp_log_pkt_ctx.end_seq < pkt_rcvd->seq)
  	stream->rtp_log_pkt_ctx.end_seq = pkt_rcvd->seq;
  
  stream->rtp_log_pkt_ctx.seq = (uint16)pkt_rcvd->seq+1;

  
  /*------------------------------------------------------------------------
      Update the discard packet count in the RTCP context
  ------------------------------------------------------------------------*/
  if (stream->rtcp_hdl)
  {  
    qvp_rtcp_update_discard_pkt_cnt(stream->rtcp_hdl, discard_pkt_cnt); 
  }


  if( stream->rtcp_hdl )
  {
    qvp_rtcp_report_rx( stream->rtcp_hdl, pkt_rcvd->len, pkt_rcvd->tstamp, 
                      stream->rtp.rx_ssrc, pkt_rcvd->seq, pkt_rcvd->ttl );
  }
  
  /*------------------------------------------------------------------------
     Get the profile property
  ------------------------------------------------------------------------*/
  if( qvp_rtp_find_profile_property( stream->payload_type,
                                     &property ) != QVP_RTP_SUCCESS )
  {
    QVP_RTP_ERR_0( "Error finding profile property");

    if( pkt_rcvd->need_tofree )
    {
      qvp_rtp_free_buf( pkt_rcvd );
    }
    return;
  }

  //qvp_rtcp_set_profile_property(stream->rtcp_hdl, &property);

  /*------------------------------------------------------------------------
       pipe the packet to appropriate profile. 
  ------------------------------------------------------------------------*/
 // QVP_RTP_MSG_HIGH_0("TTY QVP_RTP_HANDLE_NW_PACKET=>PROFILE->RECV_PROFILE");
  stream->profile->recv_profile( stream->prof_handle, stream, pkt_rcvd );

  /*------------------------------------------------------------------------
    Calculate the Burst Metrics for RTCP VoIP-XR packet
  ------------------------------------------------------------------------*/
 if( stream->rtcp_hdl )
 {
   qvp_rtcp_generate_burst_metrics_report_blk(stream->rtcp_hdl);
 }

}  /* qvp_rtp_handle_nw_packet */

/*===========================================================================

FUNCTION QVP_RTP_HANDLE_HK_TOUT 

DESCRIPTION
  This function is called every time the  RTP house keeping timer fires.
  Housekeeping timer fires once in every second. When the timer fires RTP 
  will tidy up some of the internal states. This includes Jitter buffer 
  adaptation.  Periodic lipsync calculation. Clock Skew detection and 
  correction etc.

     
DEPENDENCIES
  None

ARGUMENTS IN
   None

RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful
  appropriate error code otherwise


SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_handle_hk_tout ( void *param)
{
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
  uint16 i, j;
  qvp_rtp_stream_type *stream = NULL;
  int16 skew;
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/ 
/*------------------------------------------------------------------------*/
  
  
  /*------------------------------------------------------------------------
    Bail out if we are not fully initialized
  ------------------------------------------------------------------------*/
  if( !qvp_rtp_initialized )
  {
    return;
  }
  /*------------------------------------------------------------------------
    1) Walk through all the users active
    2) Walk into all the sessions open
    3) Do the housekeeping chores for the session
       3.1) Do the jiter buffer adaptation
  ------------------------------------------------------------------------*/

  /*
  if the reference count is zero than there is no active session opened ,hence do not restart the timer
  */
  if(!rtp_ctx.hk_timer_ref_count)
  {
  
    QVP_RTP_MSG_HIGH_0("qvp_rtp_handle_hk_tout::Reference count zero..not starting the timer");
    return;
  }
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
  for( i = 0; i  < rtp_ctx.num_users ; i++ )
  {
    
    
    /*----------------------------------------------------------------------
       If the user is occupied then do the remaining
    ----------------------------------------------------------------------*/
    if( rtp_ctx.user_array[ i ].valid )
    {
      for( j = 0; j < QVP_RTP_MAX_STREAMS_DFLT; j++ )
      {

        stream = &rtp_ctx.user_array[ i ].stream_array[j ];

        /*------------------------------------------------------------------
          Compensate skew here if session is open
        ------------------------------------------------------------------*/
        if( stream->valid && stream->ib_stream.valid && 
            stream->ib_stream.jitter_queue ) 
        {
           if( qvp_rtcp_get_rx_skew( stream->rtcp_hdl, &skew )
			  != QVP_RTP_SUCCESS)
           {
            QVP_RTP_ERR_0( "Getting RTCP Rx Skew failed");
           }
        } /* end of if session valid */
        
      } /* end of for i = 0 */
      
    } /* end of if */
    
  } /* end of for j = 0 */
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/ 

#ifndef FEATURE_IMS_PC_TESTBENCH
  if( qvp_rtp_timer_start( rtp_ctx.hk_timer, 500 /* half sec */ ) 
      != QVP_RTP_SUCCESS )
  {
    QVP_RTP_ERR_0( " Rearming the housekeeping failed");
  }
#endif/*FEATURE_IMS_PC_TESTBENCH*/
  
} /* end of function qvp_rtp_handle_hk_tout */
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
/*===========================================================================

FUNCTION  QVP_RTP_CONFIGURE_JITTER_SIZE


DESCRIPTION

  This function will do one of the following configurations for 
  the jitter buffer size based on the input values :
  1. Switch off the existing jitter buffer.
  2. Change the jitter size for an existing jitter buffer.
  3. Allocate a new jitter buffer.
  Any new jitter allocation will result in release of the previously
  allocated jitter buffer by updating the close_flag
   

DEPENDENCIES
  None

ARGUEMENTS IN
  stream - stream ctx.
  rx_param - rx params for the stream.
  payload_valid - valid flag for payload type.
  payload_type - RTP payload type requested.

ARGUEMENTS OUT
   close_flag : If this flag is enabled the jitter buffer of 
                the old configuration has to be closed 
                after the new config is successful

RETURN VALUE
  status : success if jitter size config succeeds else error

SIDE EFFECTS
   None

===========================================================================*/

LOCAL qvp_rtp_status_type qvp_rtp_configure_jitter_size 
(
  qvp_rtp_stream_type              *stream,
  qvp_rtp_pyld_rtp_rx_param_type   rx_param,
  boolean                          payload_valid,
  qvp_rtp_payload_type             payload_type,
  boolean                          *close_flag
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  boolean config_jitter_valid = FALSE;
  uint16 config_jitter_size = rx_param.jitter_size;
  qvp_rtp_jitter_hdl_type  old_jb_hdl;
  uint16 old_jitter_size = 0;
  /*----------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Check for valid arguments 
  ------------------------------------------------------------------------*/
  if( !stream || !close_flag )
  {
    QVP_RTP_ERR_0(" qvp_rtp_configure_jitter_size:Invalid i/p params\r\n");
    return( QVP_RTP_ERR_FATAL );
  }

  old_jitter_size = stream->ib_stream.jitter_size;
  old_jb_hdl= stream->ib_stream.jitter_queue;
  *close_flag = FALSE;

  /*------------------------------------------------------------------------
    We need to reopen the dejitter buffer if both the following conditions
    are true.

    1) Application is asking for dejb size configure
    2) The deisred dejb size is not equal to currently configured value.
  ------------------------------------------------------------------------*/
  if( ( config_jitter_size != old_jitter_size ) &&
      ( rx_param.jitter_valid == TRUE ) ) 
  {
    config_jitter_valid = TRUE;
  }/* end of if ( config_jitter_size ) */
  else if( ( payload_valid == TRUE ) &&
           ( payload_type != stream->payload_type ) && 
           ( old_jb_hdl ) && ( old_jitter_size ) )
  {
    /*----------------------------------------------------------------------
      if jitter was there with the earlier profile and the new payload 
      type does not ask for any change in jitter allocation just set the 
      params to reallocate the jitter buffer with the same size as 
      previous. This will be treated as new jitter size allocation request.
    ----------------------------------------------------------------------*/
    config_jitter_valid = TRUE;
    config_jitter_size = old_jitter_size;
  } /* end of else if ( config_msg->config.payload != old_payload_type ) */

  if ( config_jitter_valid == TRUE )
  {
    /*----------------------------------------------------------------------
      reconfiguring the jitter with jitter valid = TRUE
    ----------------------------------------------------------------------*/
    if ( config_jitter_size == 0 ) /* clear jitter request */
    {
      /*--------------------------------------------------------------------
        only if previous jitter exists close , if previous config did not
        have jitter we do nothing
      --------------------------------------------------------------------*/
      if (old_jb_hdl &&  
          old_jitter_size )
      {
        /*------------------------------------------------------------------
          indication to close old configuration
        ------------------------------------------------------------------*/
        *close_flag = TRUE ;  
      }
      stream->ib_stream.jitter_size = 0;
      stream->ib_stream.jitter_queue = NULL;
      QVP_RTP_MSG_LOW_1(" closing  a  jb = %p, ", 
                        stream->ib_stream.jitter_queue);
    } /* end of if ( jitter_size == 0 ) */
    else if ( ( config_jitter_size > 0 ) && 
              ( config_jitter_size <= QVP_RTP_MAX_JITTER_SIZE ) )
    {

      /*--------------------------------------------------------------------
        different from previous jitter allocation -reallocate & 
        close old one if it exists
      --------------------------------------------------------------------*/
      if ( old_jb_hdl &&  
           old_jitter_size )
      {
        /*------------------------------------------------------------------
          indication to close old configuration
        ------------------------------------------------------------------*/
        *close_flag = TRUE ; 
      }

      if ( (status = qvp_rtp_alloc_jitter_buffer( stream,
            config_jitter_size)) == QVP_RTP_SUCCESS )
      {
        stream->ib_stream.jitter_size = config_jitter_size;
      }
      else
      {
        QVP_RTP_ERR_0(" qvp_rtp_configure_jitter_size ::jitter buffer\
                                      allocation failed");
      }
      QVP_RTP_MSG_HIGH_1(" alloced jb = %d, ", 
                         stream->ib_stream.jitter_queue);
    } /* else if  valid jitter range */

    else
    {
      QVP_RTP_ERR_1(" qvp_rtp_configure_jitter_size ::Invalid jitter \
                                 size:%d", config_jitter_size);
      status = QVP_RTP_ERR_FATAL ;
    } /* end of else - indicating invalid jitter size */
  }
  return ( status ); 
}/* end of function qvp_rtp_configure_jitter_size */ 
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
/*===========================================================================

FUNCTION QVP_RTP_GET_PLAYOUT_DELAY

DESCRIPTION
  Obtains the video delay estimate value, this can be used in
  calculation of APTO value

DEPENDENCIES
  None

ARGUEMENTS IN
  stream_hdl - vid stream handle

ARGUEMENTS OUT
  curr_vid_delay_est  - Current video delay estimate
  frm_arr_time - last frame arrival time
  no_frame_update - for emergency throttling


RETURN VALUE
  success or failiure code

SIDE EFFECTS
  None

===========================================================================*/
qvp_rtp_status_type qvp_rtp_get_playout_delay
(
  qvp_rtcp_app_handle_type   stream_hdl,
  int32                      *curr_vid_delay_est,
  uint64*              frm_arr_time,
  boolean*         no_frame_update
)
{
  qvp_rtp_stream_type *stream = ( qvp_rtp_stream_type *) stream_hdl;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
      Check for valid arguments
  ------------------------------------------------------------------------*/
  if( !stream || !curr_vid_delay_est || !frm_arr_time )
  {
    QVP_RTP_ERR_0( "Invalid params");
    return( QVP_RTP_ERR_FATAL );
  }
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
  if( stream->ib_stream.valid && stream->ib_stream.jitter_size )
  {
    return( qvp_rtp_jb_get_playout_delay( stream->ib_stream.jitter_queue, 
                            curr_vid_delay_est, frm_arr_time, no_frame_update ) );
  }
  else 
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
  {
    QVP_RTP_ERR_0( " Cannot send APTO without JB");
    return QVP_RTP_ERR_FATAL;
  }

} /* end of qvp_rtp_get_playout_delay */

/*===========================================================================

FUNCTION QVP_RTP_JB_GET_FRAMERATE

DESCRIPTION
  Obtains the video framerate value

DEPENDENCIES
  None

ARGUEMENTS IN
  stream_hdl - vid JB handle 

ARGUEMENTS OUT
  curr_vid_delay_est  - Current video delay estimate
  frm_arr_time - last frame arrival time
  no_frame_update - for emergency throttling

RETURN VALUE
  success or failiure code

SIDE EFFECTS
  None

===========================================================================*/
qvp_rtp_status_type qvp_rtp_get_framerate
(
  qvp_rtcp_app_handle_type   stream_hdl,
  float* frame_rate
)
{
    qvp_rtp_stream_type *stream = ( qvp_rtp_stream_type *) stream_hdl;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
      Check for valid arguments
  ------------------------------------------------------------------------*/
  if( !stream || !frame_rate )
  {
    QVP_RTP_ERR_0( "Invalid params");
    return( QVP_RTP_ERR_FATAL );
  }
#ifdef FEATURE_IMS_RTP_JITTERBUFFER
  if( stream->ib_stream.valid && stream->ib_stream.jitter_size )
  {
    return( qvp_rtp_jb_get_framerate( stream->ib_stream.jitter_queue, 
                            frame_rate ) );
  }
  else 
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
  {
    QVP_RTP_ERR_0( " Cannot send get framerate without JB");
    return QVP_RTP_ERR_FATAL;
  }

} /* end of qvp_rtp_get_framerate */

/*===========================================================================

FUNCTION QVP_RTP_UPDATE_REMOTE_RAT

DESCRIPTION
  Obtains the remote_rat type

DEPENDENCIES
  None

ARGUEMENTS IN
  stream_hdl -  handle 

ARGUEMENTS OUT
  remote rat type

RETURN VALUE
  success or failiure code

SIDE EFFECTS
  None

===========================================================================*/
qvp_rtp_status_type qvp_rtp_update_remote_rat
(
  qvp_rtcp_app_handle_type   stream_hdl,
    qvp_rtp_rat_type* remote_rat
)
{
    qvp_rtp_stream_type *stream = ( qvp_rtp_stream_type *) stream_hdl;
	 qvp_rtp_ds_ob_sock_entry_type *sock_id=NULL;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
      Check for valid arguments
  ------------------------------------------------------------------------*/
  if( !stream || !remote_rat )
  {
    QVP_RTP_ERR_0( "Invalid params");
    return( QVP_RTP_ERR_FATAL );
  }
  sock_id = ( qvp_rtp_ds_ob_sock_entry_type * ) stream->ob_stream.sock_id; 
  if(!sock_id)
  {
	QVP_RTP_ERR_0( "sockid NULL");
    return( QVP_RTP_ERR_FATAL );
  }
  stream->remote_rat_type=*remote_rat;
  sock_id->remote_rat=*remote_rat;
  QVP_RTP_ERR_2( "sock_id->remote_rat %d *remote_rat %d",sock_id->remote_rat,*remote_rat);
    return( QVP_RTP_SUCCESS );


} /* end of qvp_rtp_update_remote_rat */
/*===========================================================================

FUNCTION QVP_RTP_SEND_RTCP_BYE_CMD

DESCRIPTION
  Processes the application command to RTP payload through an already 
  opened channel. 

DEPENDENCIES
  None

PARAMETERS
    send_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtp_send_rtcp_bye_cmd
(
  qvp_rtp_send_rtcp_bye_msg_type *send_msg 
)
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity    */
  qvp_rtp_usr_ctx *usr_ctx;   /* accessor for the user context */
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
    4) stream is o/b or full duplex
    5) good packet
    6) session not in reset mode. 
  ------------------------------------------------------------------------*/
  if ( ( send_msg != NULL ) &&
       ( send_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( send_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
       ( send_msg->stream_id < ( qvp_rtp_stream_id_type )
         (uint32)rtp_ctx.user_array[ (uint32)   send_msg->app_id].num_streams) &&
       rtp_ctx.user_array[ (uint32)send_msg->app_id]. \
       stream_array[ (uint32) send_msg->stream_id].ob_stream.valid &&
       ( rtp_ctx.user_array[ (uint32)send_msg->app_id]. \
       stream_array[ (uint32) send_msg->stream_id].state == 
         QVP_RTP_SESS_ACTIVE ) )
  {

    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) send_msg->app_id ]; 

    /*----------------------------------------------------------------------
      extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream    =  &( usr_ctx->stream_array[ (uint32) send_msg->stream_id ] ); 

    if( qvp_rtcp_send_bye( stream->rtcp_hdl, &send_msg->bye_info )
              != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_0( "Error sending RTCP Bye");
    }
  }
  else
  {
    QVP_RTP_ERR_0("qvp_rtp_send_rtcp_bye_cmd:Invalid params ");
  }

  return;

} /* end of function qvp_rtp_send_rtcp_bye_cmd */

/*===========================================================================

FUNCTION QVP_RTP_SEND_RTCP_APP_CMD

DESCRIPTION
  Processes the application command to RTP payload through an already 
  opened channel. 

DEPENDENCIES
  None

PARAMETERS
    send_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

LOCAL void qvp_rtp_send_rtcp_app_cmd
(
  qvp_rtp_send_rtcp_app_msg_type* send_msg 
)
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity    */
  qvp_rtp_usr_ctx *usr_ctx;   /* accessor for the user context */
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
    4) stream is o/b or full duplex
    5) good packet
    6) session not in reset mode. 
  ------------------------------------------------------------------------*/
  if ( ( send_msg != NULL ) &&
       ( send_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( send_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
       ( send_msg->stream_id < ( qvp_rtp_stream_id_type )
         (uint32)rtp_ctx.user_array[ (uint32)   send_msg->app_id].num_streams) &&
       rtp_ctx.user_array[ (uint32)send_msg->app_id]. \
       stream_array[ (uint32) send_msg->stream_id].ob_stream.valid &&
       ( rtp_ctx.user_array[ (uint32)send_msg->app_id]. \
       stream_array[ (uint32) send_msg->stream_id].state == 
         QVP_RTP_SESS_ACTIVE ) )
  {

    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) send_msg->app_id ]; 

    /*----------------------------------------------------------------------
      extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream    =  &( usr_ctx->stream_array[ (uint32) send_msg->stream_id ] ); 

    if( qvp_rtcp_send_app( stream->rtcp_hdl, &send_msg->app_data )
              != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_0( "Error sending RTCP Bye");
    }
  }
  else
  {
    QVP_RTP_ERR_0("qvp_rtp_send_rtcp_bye_cmd:Invalid params ");
  }

  return;

} /* end of function qvp_rtp_send_rtcp_app_cmd */

/*===========================================================================

FUNCTION QVP_RTP_SEND_RTCP_VOIP_XR_CMD

DESCRIPTION
  Intiates RTCP VoIP XR packet

DEPENDENCIES
  None

PARAMETERS
    voip_xr_msg - message which was posted by the API into the module.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

LOCAL void qvp_rtp_send_rtcp_voip_xr_cmd
( 
  qvp_rtp_send_rtcp_voip_xr_msg_type  *voip_xr_msg 
)
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity    */
  qvp_rtp_usr_ctx *usr_ctx;   /* accessor for the user context */
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
    4) stream is o/b or full duplex
    5) good packet
    6) session not in reset mode. 
  ------------------------------------------------------------------------*/
  if ( ( voip_xr_msg != NULL ) &&
       ( voip_xr_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( voip_xr_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
       ( voip_xr_msg->stream_id < ( qvp_rtp_stream_id_type )
         (uint32)rtp_ctx.user_array[ (uint32)   voip_xr_msg->app_id].num_streams) &&
       rtp_ctx.user_array[ (uint32)voip_xr_msg->app_id]. \
       stream_array[ (uint32) voip_xr_msg->stream_id].ob_stream.valid &&
       ( rtp_ctx.user_array[ (uint32)voip_xr_msg->app_id]. \
       stream_array[ (uint32) voip_xr_msg->stream_id].state == 
         QVP_RTP_SESS_ACTIVE ) )
  {

    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) voip_xr_msg->app_id ]; 

    /*----------------------------------------------------------------------
      extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream    =  &( usr_ctx->stream_array[ (uint32) voip_xr_msg->stream_id ] ); 
   if( qvp_rtcp_send_voip_xr( stream->rtcp_hdl, voip_xr_msg->enable_voip_xr)
              != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_0( "Error sending RTCP  VoIP XR");
    }
  }
  else
  {
    QVP_RTP_ERR_0("qvp_rtp_send_rtcp_voip_xr_cmd:Invalid params ");
  }

  return;
}/* end of function qvp_rtp_send_rtcp_voip_xr_cmd */

#ifdef FEATURE_IMS_RTP_JITTERBUFFER
/*===========================================================================

FUNCTION QVP_RTCP_GET_QDJ_PARAM

DESCRIPTION
  Stores relevant QDJ parameters in the RTCP session structures

DEPENDENCIES
  None

PARAMETERS
    hdl - handle of the jitter buffer from where relevant  QDJ parameters will be retrieved
    sess_handle -  handle to the session where QDJ parameters will be stored
RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
LOCAL void qvp_rtcp_get_qdj_param
(
 qvp_rtp_jitter_hdl_type      hdl ,
  qvp_rtcp_session_handle_type  sess_handle 
)
{
  uint8   plc; /* PLC : 2 bits */
  uint8   jba; /* JBA : 2 bits*/
  uint8   jb_rate;  /* JB rate : 4 bits */ 
  uint16   jb_nominal; /* JB nominal */
  uint16   jb_max;     /* JB maximum*/
  uint16   jb_abs_max; /* JB abs max */ 

  if((NULL == hdl) || (NULL == sess_handle))
  {
    QVP_RTP_ERR_0("qvp_rtcp_get_qdj_param..invalid parameters ");
    return;
  }

  qvp_rtp_jb_retrieve_voip_xr_param(hdl, &plc , &jba, &jb_rate, &jb_nominal, 
                                                      &jb_max, &jb_abs_max);

  qvp_rtcp_store_jb_voip_xr_param(sess_handle, plc, jba, jb_rate, jb_nominal,
                                                     jb_max, jb_abs_max);
  return;

}/*end of function qvp_rtcp_get_qdj_param*/
#endif/*FEATURE_IMS_RTP_JITTERBUFFER*/
/*===========================================================================

FUNCTION QVP_RTP_SET_RTCP_XR_SIGNAL_METRICS_CMD

DESCRIPTION
 sets system signal metrics to RTCP session structure

DEPENDENCIES
  None

PARAMETERS
    rtcp_signal_metrics_msg - contains signal metrics values.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/


LOCAL void qvp_rtp_set_rtcp_xr_signal_metrics_cmd
(
       qvp_rtp_set_rtcp_voip_xr_signal_metrics_type  *signal_msg
)
{
   qvp_rtp_stream_type *stream; /* for inderection simplicity    */
  qvp_rtp_usr_ctx *usr_ctx;   /* accessor for the user context */
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
    4) stream is o/b or full duplex
    5) good packet
    6) session not in reset mode. 
  ------------------------------------------------------------------------*/
  if ( ( signal_msg != NULL ) &&
       ( signal_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF) && 
       ( signal_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
       ( signal_msg->stream_id < ( qvp_rtp_stream_id_type)
         (uint32)rtp_ctx.user_array[ (uint32) signal_msg->app_id].num_streams ) &&
       rtp_ctx.user_array[ (uint32)signal_msg->app_id]. \
       stream_array[ (uint32) signal_msg->stream_id].ob_stream.valid &&
       ( rtp_ctx.user_array[ (uint32)signal_msg->app_id]. \
       stream_array[ (uint32) signal_msg->stream_id].state == 
         QVP_RTP_SESS_ACTIVE ) )
  {

    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) signal_msg->app_id ]; 

    /*----------------------------------------------------------------------
      extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream    =  &( usr_ctx->stream_array[ (uint32) signal_msg->stream_id ] ); 

    qvp_rtcp_set_voip_xr_signal_metrics( stream->rtcp_hdl,
                                          &(signal_msg->signal_metrics_info ));
  }
  else
  {
    QVP_RTP_ERR_0("qvp_rtp_set_rtcp_xr_signal_metrics_cmd: Invalid params ");
  }
  return;
}/*end of function qvp_rtp_set_rtcp_xr_signal_metrics_cmd*/

/*===========================================================================

FUNCTION QVP_RTP_SET_RTCP_XR_TX_QUALITY_CMD

DESCRIPTION
 sets system tx quality metrics to RTCP session structure

DEPENDENCIES
  None

PARAMETERS
    rtcp_tx_quality_msg - contains system tx quality metrics values.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/


LOCAL void qvp_rtp_set_rtcp_xr_tx_quality_cmd
(
  qvp_rtp_set_rtcp_voip_xr_tx_quality_metrics_type  *tx_quality_msg
)
{
   qvp_rtp_stream_type *stream; /* for inderection simplicity    */
  qvp_rtp_usr_ctx *usr_ctx;   /* accessor for the user context */
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
    4) stream is o/b or full duplex
    5) good packet
    6) session not in reset mode. 
  ------------------------------------------------------------------------*/
  if ( (tx_quality_msg != NULL ) &&
       (tx_quality_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF) && 
       (tx_quality_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users) &&
       ( tx_quality_msg->stream_id < ( qvp_rtp_stream_id_type)
         (uint32)rtp_ctx.user_array[ (uint32) tx_quality_msg->app_id].num_streams ) &&
       rtp_ctx.user_array[ (uint32)tx_quality_msg->app_id]. \
       stream_array[ (uint32) tx_quality_msg->stream_id].ob_stream.valid &&
       ( rtp_ctx.user_array[ (uint32)tx_quality_msg->app_id]. \
       stream_array[ (uint32) tx_quality_msg->stream_id].state == 
         QVP_RTP_SESS_ACTIVE ) )
  {

    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) tx_quality_msg->app_id ]; 

    /*----------------------------------------------------------------------
      extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream = &( usr_ctx->stream_array[ (uint32) tx_quality_msg->stream_id ] );

    qvp_rtcp_set_voip_xr_tx_quality_metrics( stream->rtcp_hdl,
                                          &(tx_quality_msg->tx_quality_info));
  }
  else
  {
    QVP_RTP_ERR_0("qvp_rtp_set_rtcp_xr_tx_quality_cmd:Invalid params ");
  }
 
  return;
} /*end of function qvp_rtp_set_rtcp_xr_tx_quality_cmd*/

/*===========================================================================

FUNCTION QVP_RTP_MEDIA_INIT_CMD 

DESCRIPTION
Processes the application command to init the media layer. 

DEPENDENCIES
None

PARAMETERS
media_init_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_media_init_cmd
(
 qvp_rtp_media_init_msg_type *media_init_msg
 )
{
  qvp_rtp_usr_ctx *user_ctx; 
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;                            

  if(!media_init_msg)
  {
    QVP_RTP_ERR_0 ("qvp_rtp_media_init_cmd : media_init_msg is null");
    return;
  }
  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  ------------------------------------------------------------------------*/
  if (( media_init_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( media_init_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( rtp_ctx.user_array[ (uint32)media_init_msg->app_id].valid ))
  {
    user_ctx = &rtp_ctx.user_array[ (uint32)media_init_msg->app_id];

    // Check if Media for particular media type for this APP ID is already inited
    if (((media_init_msg->media_type == QVP_RTP_VIDEO) && (user_ctx->media_vid_inited != TRUE)) ||
        ((media_init_msg->media_type == QVP_RTP_AUDIO) && (user_ctx->media_aud_inited != TRUE)) ||
        ((media_init_msg->media_type == QVP_RTP_TEXT) && (user_ctx->media_txt_inited != TRUE))
        )
    {
      status = qvp_rtp_media_init_pvt(media_init_msg->app_id, media_init_msg->media_type);
    }
    else
    {
      QVP_RTP_ERR_2 ("media_init_cmd : media already inited: %x App ID: %x",media_init_msg->media_type, media_init_msg->app_id);
    }

    //[Vik]: If media init failed do we want to store the APP CB into user data?
    if( status == QVP_RTP_SUCCESS )
    {
      if(media_init_msg->media_type == QVP_RTP_AUDIO)
        user_ctx->media_aud_inited = TRUE;
      else if(media_init_msg->media_type == QVP_RTP_TEXT)
        user_ctx->media_txt_inited = TRUE;
      else
        user_ctx->media_vid_inited = TRUE;
      user_ctx->app_media_cb = media_init_msg->pfnotify;
    }

    /*----------------------------------------------------------------------
     Notify status back to APP.
    ----------------------------------------------------------------------*/

    /*----------------------------------------------------------------------
     If there is a user call back installed just call it
     ----------------------------------------------------------------------*/
    if( media_init_msg->pfnotify )
    {	
      media_init_msg->pfnotify(status, media_init_msg->media_type);
    }

  }
  else
  {
    /*----------------------------------------------------------------------
    If we have a valid app media cb then invoke the cb with error. 
    ----------------------------------------------------------------------*/
    if( media_init_msg->pfnotify )
    {	
      media_init_msg->pfnotify(status, media_init_msg->media_type);
    }
  }
}

/*===========================================================================

FUNCTION QVP_RTP_MEDIA_DEINIT_CMD 

DESCRIPTION
Processes the application command to uninitialize the media layer. 

DEPENDENCIES
None

PARAMETERS
media_deinit_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_media_deinit_cmd
(
 qvp_rtp_media_deinit_msg_type *media_deinit_msg
 )
{
  qvp_rtp_usr_ctx *user_ctx; 
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;                            

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  ------------------------------------------------------------------------*/
  if ( ( media_deinit_msg != NULL ) && 
    ( media_deinit_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( media_deinit_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( rtp_ctx.user_array[ (uint32)media_deinit_msg->app_id].valid ))
  {
    user_ctx = &rtp_ctx.user_array[ (uint32)media_deinit_msg->app_id];

    // Check if Media for particular media type for this APP ID is already inited
    if (((media_deinit_msg->media_type == QVP_RTP_VIDEO) && (user_ctx->media_vid_inited == TRUE)) ||
        ((media_deinit_msg->media_type == QVP_RTP_AUDIO) && (user_ctx->media_aud_inited == TRUE)) ||
        ((media_deinit_msg->media_type == QVP_RTP_TEXT) && (user_ctx->media_txt_inited == TRUE))
        )
    {
      status = qvp_rtp_media_deinit_pvt(media_deinit_msg->app_id, media_deinit_msg->media_type);
    }
    else
    {
      QVP_RTP_ERR_2 ("media_deinit : media already DeInited: %x App ID: %x",media_deinit_msg->media_type, media_deinit_msg->app_id);
    }
    
    /*----------------------------------------------------------------------
     Notify status back to APP.
    ----------------------------------------------------------------------*/

    /*----------------------------------------------------------------------
     If there is a user call back installed just call it
     ----------------------------------------------------------------------*/
    if( user_ctx->app_media_cb)
    {	
      user_ctx->app_media_cb(status, media_deinit_msg->media_type);
    }
  }
  else
  {
    QVP_RTP_ERR_1 ("media_deinit : wrong param on the function: msg: %x",media_deinit_msg);
  }
}

/*===========================================================================

FUNCTION QVP_RTP_GENERATE_NAL_CMD 

DESCRIPTION
Processes the application command to generate the H.264 NAL header. 

DEPENDENCIES
None

PARAMETERS
media_generate_nal_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_media_generate_nal_cmd
(
 qvp_rtp_media_generate_nal_header_msg_type *media_generate_nal_msg
 )
{
  qvp_rtp_usr_ctx *user_ctx; 
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;                            

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  ------------------------------------------------------------------------*/
  if ( ( media_generate_nal_msg != NULL ) && 
    ( media_generate_nal_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( media_generate_nal_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( rtp_ctx.user_array[ (uint32)media_generate_nal_msg->app_id].valid ))
  {
    user_ctx = &rtp_ctx.user_array[ (uint32)media_generate_nal_msg->app_id];

    if (user_ctx->media_vid_inited == TRUE)
    {
      status = qvp_rtp_media_generate_nal_pvt(media_generate_nal_msg->app_id, 
                                              media_generate_nal_msg->height,
                                              media_generate_nal_msg->width,
                                              media_generate_nal_msg->profile,
                                              media_generate_nal_msg->level,
                                              media_generate_nal_msg->puserdata,
                                              user_ctx->call_backs.handle_generate_nal);
    }
    else
    {
      QVP_RTP_ERR_1 ("qvp_rtp_media_generate_nal_cmd : media Not Inited: App ID: %x",media_generate_nal_msg->app_id);
    }

    /*----------------------------------------------------------------------
     Notify status back to APP.
    ----------------------------------------------------------------------*/
    if( status != QVP_RTP_SUCCESS )
    {
      if (user_ctx->call_backs.handle_generate_nal)
      {
        user_ctx->call_backs.handle_generate_nal(NULL, status, media_generate_nal_msg->puserdata);
      }
      else
      {
        QVP_RTP_ERR_0("qvp_rtp_media_generate_nal_cmd: NAL CB is NULL");
      }
    }
  }
  else
  {
    QVP_RTP_ERR_1 ("qvp_rtp_media_generate_nal_cmd : wrong param on the function: msg: %x",media_generate_nal_msg);
  }
}

/*===========================================================================

FUNCTION QVP_RTP_GET_MEDIA_TYPE

DESCRIPTION
Extract the media type using APP ID and Stream ID. 

DEPENDENCIES
None

PARAMETERS
qvp_rtp_app_id_type    - App id.
qvp_rtp_stream_id_type - stream id

RETURN VALUE
qvp_rtp_media_type  - media type

SIDE EFFECTS
None

===========================================================================*/

qvp_rtp_media_type qvp_rtp_get_media_type(qvp_rtp_app_id_type    app_id,
                                          qvp_rtp_stream_id_type  stream_id )
{
  qvp_rtp_stream_type *stream = NULL; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx = NULL;
  qvp_rtp_traffic_priority_type traffic_type;

  user_ctx = &rtp_ctx.user_array[ (uint32)app_id]; 
  stream   = &user_ctx->stream_array[ (uint32) stream_id ];
  traffic_type = qvp_rtp_get_nw_priority(stream->payload_type);
  if(traffic_type == QVP_RTP_TRAFFIC_VID_PRI)
  {
    //video code 
    return QVP_RTP_VIDEO;
  }
  else if(traffic_type == QVP_RTP_TRAFFIC_TXT_PRI)
  {
    //video code 
    return QVP_RTP_TEXT;
  }
  else if(traffic_type != QVP_RTP_TRAFFIC_LAST_PRI)
  {
    //audio code
    return QVP_RTP_AUDIO;
  }
  else
  {
    //error 
    return QVP_RTP_INVALID;
  }
}


/*===========================================================================

FUNCTION QVP_RTP_CODEC_INIT_CMD 

DESCRIPTION
Processes the application command to init the codec. 

DEPENDENCIES
None

PARAMETERS
media_codec_init_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_media_codec_init_cmd
(
 qvp_rtp_media_codec_init_msg_type *media_codec_init_msg
 )
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx; 
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;                            
  qvp_rtp_media_type media_type = QVP_RTP_INVALID;

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range
  ------------------------------------------------------------------------*/
  if ( ( media_codec_init_msg != NULL ) && 
    ( media_codec_init_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( media_codec_init_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( media_codec_init_msg->stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   media_codec_init_msg->app_id].num_streams) &&
    ( rtp_ctx.user_array[ (uint32)media_codec_init_msg->app_id].valid ) && 
    ( rtp_ctx.user_array[ (uint32)media_codec_init_msg->app_id]. \
    stream_array[ (uint32) media_codec_init_msg->stream_id].valid ) )
  {

    user_ctx = &rtp_ctx.user_array[ (uint32)media_codec_init_msg->app_id]; 
    stream   = &user_ctx->stream_array[ (uint32) media_codec_init_msg->stream_id ];

    if( user_ctx->call_backs.handle_media_res )
    {
       media_type = qvp_rtp_get_media_type(media_codec_init_msg->app_id, 
                                       media_codec_init_msg->stream_id);
       if(QVP_RTP_AUDIO == media_type)
      {
	    qvp_rtp_rat_type t_rat_type = QVP_RTP_RAT_LTE;
        g_cvd_ssr = FALSE;
		QVP_RTP_ERR_2("qvp_rtp_media_codec_init_cmd: stream id:%d rat type:%d",media_codec_init_msg->stream_id, stream->rat_type);
		if(g_audio_offload != 2)
			t_rat_type = stream->rat_type;
        status = qvp_rtp_media_codec_initialize_pvt(media_codec_init_msg->app_id,
                                                 media_codec_init_msg->stream_id,
                                              &media_codec_init_msg->media_config,
                                              t_rat_type,
                                             user_ctx->call_backs.handle_media_res);
        }
      else if ( QVP_RTP_TEXT == media_type )
      {
        status = qvp_rtp_text_codec_initialize_pvt(media_codec_init_msg->app_id,media_codec_init_msg->stream_id,
          &media_codec_init_msg->media_config,user_ctx->call_backs.handle_media_res, stream->prof_handle,media_codec_init_msg->direction);
      }
      else if ( QVP_RTP_VIDEO == media_type )
      {
        //status = qvp_rtp_media_codec_initialize_video_pvt(media_codec_init_msg->app_id,media_codec_init_msg->stream_id,
          //&media_codec_init_msg->media_config,user_ctx->call_backs.handle_media_res, media_codec_init_msg->direction);
      }
      else
      {
        QVP_RTP_ERR_2("qvp_rtp_media_codec_init_cmd: Invalid media type: App ID: %d, stream ID: %d ",
                    media_codec_init_msg->app_id, media_codec_init_msg->stream_id);
      }
    }
    else
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_init_cmd: APP CB is NULL");
    }

    /*----------------------------------------------------------------------
    If codec init failed notify back to APP.
    ----------------------------------------------------------------------*/
    if( status != QVP_RTP_SUCCESS )
    {

      QVP_RTP_ERR_0("qvp_rtp_media_codec_init_cmd: Failed");
     /*----------------------------------------------------------------------
      If there is a user call back installed just call it
      ----------------------------------------------------------------------*/
      if( user_ctx->call_backs.handle_media_res && g_handoffstate != HO_IN_PROGRESS)
      {	
          qvp_rtp_response_handler(media_codec_init_msg->app_id, media_codec_init_msg->stream_id, NULL, QVP_RTP_MEDIA_CODEC_INIT_RSP, status, TRUE, media_codec_init_msg->direction);
      }      
    }
  }
  else
  {

    /*----------------------------------------------------------------------
    If we have a valid app id then we should call the handle_media_res and let the
    user know the wrong doing. 
    ----------------------------------------------------------------------*/
    if ( media_codec_init_msg && 
      media_codec_init_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
      && ( media_codec_init_msg->app_id < ( qvp_rtp_app_id_type ) 
      (uint32)rtp_ctx.num_users) 
      && rtp_ctx.user_array[ (uint32) media_codec_init_msg->app_id ].valid )
    {
      /*--------------------------------------------------------------------
      If there is a registered handle_media_res cb then call it. 
      --------------------------------------------------------------------*/
      if ( rtp_ctx.user_array[ (uint32) media_codec_init_msg->app_id ]. \
        call_backs.handle_media_res )
      {
		qvp_rtp_response_handler(media_codec_init_msg->app_id, media_codec_init_msg->stream_id, NULL, QVP_RTP_MEDIA_CODEC_INIT_RSP, QVP_RTP_WRONG_PARAM, TRUE, media_codec_init_msg->direction);
      }
    }
  }
} /* end of function qvp_rtp_media_codec_init_cmd */

/*===========================================================================

FUNCTION QVP_RTP_MEDIA_CODEC_CONFIG_CMD 

DESCRIPTION
Processes the application command to configure an initialized codec. 

DEPENDENCIES
None

PARAMETERS
media_codec_config_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_media_codec_config_cmd
(
 qvp_rtp_media_codec_config_msg_type *media_codec_config_msg
 )
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx; 
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;                            
  qvp_rtp_media_type media_type = QVP_RTP_INVALID;

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range
  ------------------------------------------------------------------------*/
  if ( ( media_codec_config_msg != NULL ) && 
    ( media_codec_config_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( media_codec_config_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( media_codec_config_msg->stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   media_codec_config_msg->app_id].num_streams) &&
    ( rtp_ctx.user_array[ (uint32)media_codec_config_msg->app_id].valid ) && 
    ( rtp_ctx.user_array[ (uint32)media_codec_config_msg->app_id]. \
    stream_array[ (uint32) media_codec_config_msg->stream_id].valid ) )
  {
    user_ctx = &rtp_ctx.user_array[ (uint32)media_codec_config_msg->app_id]; 
    stream = &user_ctx->stream_array[ (uint32) media_codec_config_msg->stream_id ];
media_type = qvp_rtp_get_media_type(media_codec_config_msg->app_id, 
                                        media_codec_config_msg->stream_id);
    if(QVP_RTP_AUDIO == media_type)
        {	
		stream->clock_rate = media_codec_config_msg->media_config.audio_codec_config.iClockRate;
       status = qvp_rtp_media_codec_configure_pvt(media_codec_config_msg->app_id,
            media_codec_config_msg->stream_id,
                &media_codec_config_msg->media_config,
              media_codec_config_msg->direction);
        }
    else if ( QVP_RTP_TEXT == media_type)
        {
      status = qvp_rtp_text_codec_configure_pvt(media_codec_config_msg->app_id,
                                         media_codec_config_msg->stream_id,
                                      &media_codec_config_msg->media_config,
                                    user_ctx->call_backs.handle_media_res, 
                                    stream->prof_handle,
                                      media_codec_config_msg->direction);
        }
   else if ( QVP_RTP_VIDEO == media_type )
    {
      //stream->clock_rate = media_codec_config_msg->media_config.video_codec_config.recorder_config.iClockRate;
      //status = qvp_rtp_media_codec_configure_video_pvt(media_codec_config_msg->app_id,media_codec_config_msg->stream_id,
        //&media_codec_config_msg->media_config,media_codec_config_msg->direction);
    }
    else
    {
      QVP_RTP_ERR_2("qvp_rtp_media_codec_config_cmd: Invalid media type: App ID: %d, stream ID: %d ",
                    media_codec_config_msg->app_id, media_codec_config_msg->stream_id);
    }

    /*----------------------------------------------------------------------
    If codec configure failed notify back to APP.
    ----------------------------------------------------------------------*/
    if( status != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_config_cmd: Failed");
        /*----------------------------------------------------------------------
        If there is a user call back installed just call it
        ----------------------------------------------------------------------*/
        if( user_ctx->call_backs.handle_media_res && g_handoffstate != HO_IN_PROGRESS)
        {	
         qvp_rtp_response_handler(media_codec_config_msg->app_id, media_codec_config_msg->stream_id, NULL, QVP_RTP_MEDIA_CODEC_CONFIG_RSP, status, TRUE, media_codec_config_msg->direction);
        }
		else if(g_handoffstate == HO_IN_PROGRESS)
		{
			QVP_RTP_ERR_0("qvp_rtp_media_codec_config_cmd: HO in Progress.got error in codec config.Raise audio error to Qipcall");
			if(g_handoffstate != HO_NONE)
			{
				g_handoffstate = HO_NONE;
			}		
			qvp_rtp_response_handler( media_codec_config_msg->app_id, media_codec_config_msg->stream_id,(qvp_rtp_app_data_type)(NULL), QVP_RTP_MEDIA_AUDIO_ERROR_RSP, QVP_RTP_ERR_FATAL, TRUE, QVP_RTP_DIR_TXRX);
        }
        else
        {
          QVP_RTP_ERR_0("qvp_rtp_media_codec_config_cmd: APP CB is NULL");
        }
    }
  }
  else
  {

    /*----------------------------------------------------------------------
    If we have a valid app id then we should call the handle_media_res and let the
    user know the wrong doing. 
    ----------------------------------------------------------------------*/
    if ( media_codec_config_msg && 
      media_codec_config_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
      && ( media_codec_config_msg->app_id < ( qvp_rtp_app_id_type ) 
      (uint32)rtp_ctx.num_users) 
      && rtp_ctx.user_array[ (uint32) media_codec_config_msg->app_id ].valid )
    {
      /*--------------------------------------------------------------------
      If there is a registered handle_media_res cb then call it. 
      --------------------------------------------------------------------*/
      if ( rtp_ctx.user_array[ (uint32) media_codec_config_msg->app_id ]. \
        call_backs.handle_media_res )
      {
       qvp_rtp_response_handler(media_codec_config_msg->app_id, media_codec_config_msg->stream_id, NULL, QVP_RTP_MEDIA_CODEC_CONFIG_RSP, QVP_RTP_WRONG_PARAM, TRUE, media_codec_config_msg->direction);
      }
    }
  }
} /* end of function qvp_rtp_media_codec_config_cmd */

/*===========================================================================

FUNCTION QVP_RTP_MEDIA_CODEC_RELEASE_CMD 

DESCRIPTION
Processes the application command to release acquired media codec. 

DEPENDENCIES
None

PARAMETERS
media_codec_release_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_media_codec_release_cmd
(
 qvp_rtp_media_codec_release_msg_type *media_codec_release_msg
 )
{
  qvp_rtp_usr_ctx *user_ctx; 
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;  
if( media_codec_release_msg != NULL )     
  QVP_RTP_MSG_HIGH_1("qvp_rtp_media_codec_release_cmd called for media_type %d",media_codec_release_msg->media_type);
  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  ------------------------------------------------------------------------*/
  if ( ( media_codec_release_msg != NULL ) && 
    ( media_codec_release_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) )
  {


    user_ctx = &rtp_ctx.user_array[ (uint32)media_codec_release_msg->app_id]; 

    if(QVP_RTP_AUDIO == media_codec_release_msg->media_type)
    {
      status = qvp_rtp_media_codec_release_pvt(media_codec_release_msg->app_id);
        }
    else if(QVP_RTP_TEXT == media_codec_release_msg->media_type)
        {
      status = qvp_rtp_text_codec_release_pvt(media_codec_release_msg->app_id,
        user_ctx->call_backs.handle_media_res);
      }
    else if(QVP_RTP_VIDEO == media_codec_release_msg->media_type)
    {
      //status = qvp_rtp_media_codec_release_video_pvt(media_codec_release_msg->app_id);
    }
    else
    {
      QVP_RTP_ERR_1("qvp_rtp_media_codec_release_cmd: Invalid media type: %d",media_codec_release_msg->media_type);
    }

    /*----------------------------------------------------------------------
     If codec release failed notify back to APP.
    ----------------------------------------------------------------------*/
    if( status != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_0("qvp_rtp_media_codec_release_cmd: Failed");
if ( media_codec_release_msg && 
      media_codec_release_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
     )
    {
      /*--------------------------------------------------------------------
      If there is a registered handle_media_res cb then call it. 
      --------------------------------------------------------------------*/
      if ( rtp_ctx.user_array[ (uint32) media_codec_release_msg->app_id ]. \
        call_backs.handle_media_res && g_handoffstate != HO_IN_PROGRESS)
      {
          if (media_codec_release_msg->media_type == QVP_RTP_AUDIO)
          {
	        qvp_rtp_response_handler(media_codec_release_msg->app_id, ( qvp_rtp_stream_id_type ) 0, NULL, QVP_RTP_MEDIA_AUDIO_CODEC_RELEASE_RSP, status, TRUE, QVP_RTP_DIR_TXRX);
        }
        else if (media_codec_release_msg->media_type == QVP_RTP_VIDEO)
        {
         qvp_rtp_response_handler(media_codec_release_msg->app_id, ( qvp_rtp_stream_id_type ) 0, NULL, QVP_RTP_MEDIA_VIDEO_CODEC_RELEASE_RSP, status, TRUE, QVP_RTP_DIR_TXRX);
        }
      }
	  else if(g_handoffstate == HO_IN_PROGRESS)
	  {
	  	QVP_RTP_ERR_0("qvp_rtp_media_codec_release_cmd: HO in Progress.got error in codec release.Raise audio error to Qipcall");
		if(g_handoffstate != HO_NONE)
		{
			g_handoffstate = HO_NONE;
		}		
	  	qvp_rtp_response_handler( media_codec_release_msg->app_id, ( qvp_rtp_stream_id_type ) 0,(qvp_rtp_app_data_type)(NULL), QVP_RTP_MEDIA_AUDIO_ERROR_RSP, QVP_RTP_ERR_FATAL, TRUE, QVP_RTP_DIR_TXRX);
	  }
    }
    }
    if(NULL != rtp_ctx.metrics)
    {
      uint32 tempLs = 0;
      uint32 temp95pjcnt = 0;
      qvp_rtp_metic_state tstate = 0;
      qvp_rtp_metic_state tstate1 = 0;
      tempLs = rtp_ctx.metrics->dl_metrics.sDLJitLoss.prev_loss;
      temp95pjcnt = rtp_ctx.metrics->dl_metrics.rtp_jitter95p_cnt;
      tstate = rtp_ctx.metrics->dl_metrics.sDLJitLoss.compute;
      tstate1 = rtp_ctx.metrics->ul_metrics.compute;
        qvp_rtp_reset_timer(rtp_ctx.metrics->tHdl);
        qvp_rtp_reset_metrics(rtp_ctx.metrics);
      rtp_ctx.metrics->dl_metrics.sDLJitLoss.prev_loss = tempLs;
      rtp_ctx.metrics->dl_metrics.rtp_jitter95p_cnt=temp95pjcnt;
      QVP_RTP_ERR_2("metric: state at handoff DL - %d UL - %d",tstate,tstate1);
      QVP_SET_METRIC_STATE(tstate, QVP_METRIC_SKIP);
      QVP_SET_METRIC_STATE(tstate1, QVP_METRIC_SKIP);
      QVP_RTP_ERR_2("metric: state after set to SKIP DL - %d UL - %d",tstate,tstate1);      
      rtp_ctx.metrics->dl_metrics.sDLJitLoss.compute = tstate ;
      rtp_ctx.metrics->ul_metrics.compute = tstate1 ;
      }
  }
  else
  {

    /*----------------------------------------------------------------------
    If we have a valid app id then we should call the handle_media_res and let the
    user know the wrong doing. 
    ----------------------------------------------------------------------*/
    if ( media_codec_release_msg && 
      media_codec_release_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
      && ( media_codec_release_msg->app_id < ( qvp_rtp_app_id_type ) 
      (uint32)rtp_ctx.num_users) 
      && rtp_ctx.user_array[ (uint32) media_codec_release_msg->app_id ].valid )
    {
      /*--------------------------------------------------------------------
      If there is a registered handle_media_res cb then call it. 
      --------------------------------------------------------------------*/
      if ( rtp_ctx.user_array[ (uint32) media_codec_release_msg->app_id ]. \
        call_backs.handle_media_res )
      {
        if (media_codec_release_msg->media_type == QVP_RTP_AUDIO)
        {
           qvp_rtp_response_handler(media_codec_release_msg->app_id, ( qvp_rtp_stream_id_type ) 0, NULL, QVP_RTP_MEDIA_AUDIO_CODEC_RELEASE_RSP, QVP_RTP_WRONG_PARAM, TRUE, QVP_RTP_DIR_TXRX);
        }
        else if (media_codec_release_msg->media_type == QVP_RTP_VIDEO)
        {
	       qvp_rtp_response_handler(media_codec_release_msg->app_id, ( qvp_rtp_stream_id_type ) 0, NULL, QVP_RTP_MEDIA_VIDEO_CODEC_RELEASE_RSP, QVP_RTP_WRONG_PARAM, TRUE, QVP_RTP_DIR_TXRX);
        }
      }
    }
  }
} /* end of function qvp_rtp_media_codec_release_cmd */

/*===========================================================================

FUNCTION QVP_RTP_START_STREAM_CMD 

DESCRIPTION
Processes the application command to start the stream. 

DEPENDENCIES
None

PARAMETERS
media_start_stream_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_media_start_stream_cmd
(
 qvp_rtp_media_start_stream_msg_type *media_start_stream_msg
 )
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx; 
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;                            
  qvp_rtp_media_type media_type = QVP_RTP_INVALID;
  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range

  ------------------------------------------------------------------------*/
  if ( ( media_start_stream_msg != NULL ) && 
    ( media_start_stream_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( media_start_stream_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( media_start_stream_msg->stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   media_start_stream_msg->app_id].num_streams) &&
    ( rtp_ctx.user_array[ (uint32)media_start_stream_msg->app_id].valid ) && 
    ( rtp_ctx.user_array[ (uint32)media_start_stream_msg->app_id]. \
    stream_array[ (uint32) media_start_stream_msg->stream_id].valid ) )
  {


    user_ctx = &rtp_ctx.user_array[ (uint32)media_start_stream_msg->app_id]; 
    stream = &user_ctx->stream_array[ (uint32) media_start_stream_msg->stream_id ];
	/*Initialize the time stamp*/
	stream->prev_iGpsTimeMS = 0;
	stream->prev_iTxTimestampGps = 0;
	stream->prev_iRxTimestampGps = 0;
	g_isStreamStarted = TRUE;

media_type = qvp_rtp_get_media_type(media_start_stream_msg->app_id, 
                                        media_start_stream_msg->stream_id);
	qvp_rtp_time_get_ms( &stream->time_session_start );
	QVP_RTP_MSG_LOW_1("QVP_RTP_START_STREAM_CMD time_session_start[1] = %lu",stream->time_session_start);
 
   if( media_type == QVP_RTP_TEXT)
    {
      status = qvp_rtp_text_stream_start_pvt(media_start_stream_msg->app_id,
        media_start_stream_msg->stream_id, media_start_stream_msg->direction);
      stream->stream_state = ACTIVE;
      stream->state = QVP_RTP_SESS_ACTIVE;

        if( user_ctx->call_backs.handle_media_res )
        {	
          qvp_rtp_response_handler(media_start_stream_msg->app_id, 
                                   media_start_stream_msg->stream_id, 
                                   NULL, QVP_RTP_MEDIA_START_STREAM_RSP, 
                                   status, TRUE,media_start_stream_msg->direction);
        }

      /* Is this a right place to trigger the byteorder */
      qvp_rtp_text_stream_send_byteorder(media_start_stream_msg->app_id, media_start_stream_msg->stream_id);
    }
    else if(QVP_RTP_AUDIO == media_type)
    {
    status = qvp_rtp_media_stream_start_pvt(media_start_stream_msg->app_id, media_start_stream_msg->stream_id, media_start_stream_msg->direction);
      if(NULL == rtp_ctx.metrics || NULL == rtp_ctx.metricTh)
      {
        QVP_RTP_ERR_0("qvp_rtp_media_start_stream_cmd:metric timer not started");
      }
      else
      {
        QPDPL_MEDIA_METRICS * temp = (QPDPL_MEDIA_METRICS*)rtp_ctx.metricTh;
        uint16 tempalpha = 0;
        qvp_rtp_metic_state tstate = 0;
        if(rtp_ctx.metrics->tHdl && temp->m_iTimerInterval)
        {
          QVP_RTP_MSG_HIGH_1("start timer wth valued = %d",temp->m_iTimerInterval);
          QVP_RTP_MSG_HIGH("metric kpi:DL Thresh - jitter - %d 95pj - %d loss - %d",\
             temp->m_sDLJitter.iAccept, temp->m_sDL95Jitter.iAccept,temp->m_sDLFrLoss.iAccept);
          QVP_RTP_MSG_HIGH_2("metric kpi:DL Thresh - qdjsil - %d qjd speech - %d ",\
             temp->m_iDLQdjSilence, temp->m_iDLQdjSpeech);
          tempalpha = (uint16)(temp->m_fAlphaRTP*100);
          QVP_RTP_MSG_HIGH_1("metric kpi: alpha = %d",tempalpha);
          QVP_RTP_MSG_HIGH_2("metric kpi:UL Thresh - jitter - %d loss - %d",\
                            temp->m_sULJitter.iAccept, temp->m_sULFrLoss.iAccept);
          qvp_rtp_reset_timer(rtp_ctx.metrics->tHdl);
          qvp_rtp_codec_set_qdj_threshold(media_start_stream_msg->stream_id, 
                                      media_start_stream_msg->app_id, 
                                      temp->m_iDLQdjSilence, 
                                      temp->m_iDLQdjSpeech,
                                      qvp_qdj_qlty_cb);          
          tstate = rtp_ctx.metrics->dl_metrics.sDLJitLoss.compute;
            memset(&rtp_ctx.metrics->dl_metrics,0,sizeof(qvp_rtp_dl_metrics));
          rtp_ctx.metrics->dl_metrics.sDLJitLoss.compute = tstate;
          tstate= rtp_ctx.metrics->ul_metrics.compute;
            memset(&rtp_ctx.metrics->ul_metrics,0,sizeof(qvp_rtp_jitloss));
          rtp_ctx.metrics->ul_metrics.compute = tstate;
            rtp_ctx.metrics->dl_metrics.sDLJitLoss.prev_loss = 0;
            rtp_ctx.metrics->ul_metrics.prev_loss = 0;
          if(QVP_RTP_SESS_ACTIVE == stream->state)
          {
            /* we should set the metric calculation in both direction to true*/
            g_active_session |= (QVP_RTP_UL_ACTIVE|QVP_RTP_DL_ACTIVE);
            QVP_SET_METRIC_STATE(rtp_ctx.metrics->ul_metrics.compute,QVP_METRIC_INIT);
            QVP_SET_METRIC_STATE(rtp_ctx.metrics->dl_metrics.sDLJitLoss.compute,QVP_METRIC_INIT)
            qvp_rtp_timer_start(rtp_ctx.metrics->tHdl, (int32)(temp->m_iTimerInterval & 0x7FFFFFF));
          }
        }
      }
    }
    else if(QVP_RTP_VIDEO == media_type)
{
      //status = qvp_rtp_media_stream_start_video_pvt(media_start_stream_msg->app_id, media_start_stream_msg->stream_id, media_start_stream_msg->direction);

}
    /*----------------------------------------------------------------------
    If start stream failed notify APP
    ----------------------------------------------------------------------*/
    if( status != QVP_RTP_SUCCESS )
    {
      QVP_RTP_ERR_0("qvp_rtp_media_start_stream_cmd: Failed");
/*----------------------------------------------------------------------
      If there is a uset call back installed just call it
      ----------------------------------------------------------------------*/
      if( user_ctx->call_backs.handle_media_res )
      {	
		qvp_rtp_response_handler(media_start_stream_msg->app_id, media_start_stream_msg->stream_id, NULL, QVP_RTP_MEDIA_START_STREAM_RSP, status, TRUE,media_start_stream_msg->direction);
      }
    }
  }
  else
  {

    /*----------------------------------------------------------------------
    If we have a valid app id then we should call the handle_media_res and let the
    user know the wrong doing. 
    ----------------------------------------------------------------------*/
    if ( media_start_stream_msg && 
      media_start_stream_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
      && ( media_start_stream_msg->app_id < ( qvp_rtp_app_id_type ) 
      (uint32)rtp_ctx.num_users) 
      && rtp_ctx.user_array[ (uint32) media_start_stream_msg->app_id ].valid )
    {
      /*--------------------------------------------------------------------
      If there is a registered handle_media_res cb then call it. 
      --------------------------------------------------------------------*/
      if ( rtp_ctx.user_array[ (uint32) media_start_stream_msg->app_id ]. \
        call_backs.handle_media_res && g_handoffstate != HO_IN_PROGRESS)
      {
	      qvp_rtp_response_handler(media_start_stream_msg->app_id, media_start_stream_msg->stream_id, NULL, QVP_RTP_MEDIA_START_STREAM_RSP, QVP_RTP_WRONG_PARAM, TRUE, media_start_stream_msg->direction);
      }
	  else if(g_handoffstate == HO_IN_PROGRESS)
	  {
		  QVP_RTP_ERR_0("qvp_rtp_media_start_stream_cmd: HO in Progress.got error in codec start.Raise audio error to Qipcall");
		  if(g_handoffstate != HO_NONE)
		  {
			  g_handoffstate = HO_NONE;
		  } 	  
		  qvp_rtp_response_handler( media_start_stream_msg->app_id, media_start_stream_msg->stream_id,(qvp_rtp_app_data_type)(NULL), QVP_RTP_MEDIA_AUDIO_ERROR_RSP, QVP_RTP_ERR_FATAL, TRUE, QVP_RTP_DIR_TXRX);
      }
    }
  }
} /* end of function qvp_rtp_media_start_stream_cmd */

/*===========================================================================

FUNCTION QVP_RTP_STOP_STREAM_CMD 

DESCRIPTION
Processes the application command to stop stream. 

DEPENDENCIES
None

PARAMETERS
media_stop_stream_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_media_stop_stream_cmd
(
 qvp_rtp_media_stop_stream_msg_type *media_stop_stream_msg
 )
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx; 
  qvp_rtp_status_type status = QVP_RTP_ERR_FATAL;                            
  qvp_rtp_media_type media_type = QVP_RTP_INVALID;                           

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range
  ------------------------------------------------------------------------*/
  if ( ( media_stop_stream_msg != NULL ) && 
    ( media_stop_stream_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( media_stop_stream_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) /*&&
    ( media_stop_stream_msg->stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   media_stop_stream_msg->app_id].num_streams) &&
    ( rtp_ctx.user_array[ (uint32)media_stop_stream_msg->app_id].valid ) && 
    ( rtp_ctx.user_array[ (uint32)media_stop_stream_msg->app_id]. \
    stream_array[ (uint32) media_stop_stream_msg->stream_id].valid ) */)
  {


    user_ctx = &rtp_ctx.user_array[ (uint32)media_stop_stream_msg->app_id]; 
    stream = &user_ctx->stream_array[ (uint32) media_stop_stream_msg->stream_id ];
    media_type = qvp_rtp_get_media_type(media_stop_stream_msg->app_id, 
                                        media_stop_stream_msg->stream_id);
	g_isStreamStarted = FALSE;
		
    if( media_type == QVP_RTP_TEXT)
    {
      status = qvp_rtp_text_stream_stop_pvt(media_stop_stream_msg->app_id, media_stop_stream_msg->stream_id, media_stop_stream_msg->direction);
        if( user_ctx->call_backs.handle_media_res )
        {	
	      qvp_rtp_response_handler(media_stop_stream_msg->app_id, media_stop_stream_msg->stream_id, NULL, QVP_RTP_MEDIA_STOP_STREAM_RSP, status, TRUE, media_stop_stream_msg->direction);   

        }
       stream->state = QVP_RTP_SESS_INACTIVE;
    }
    else if(media_type == QVP_RTP_AUDIO)
    {
    status = qvp_rtp_media_stream_stop_pvt(media_stop_stream_msg->app_id, media_stop_stream_msg->stream_id, media_stop_stream_msg->direction);
    }
else if(media_type == QVP_RTP_VIDEO)
{
    //status = qvp_rtp_media_stream_stop_video_pvt(media_stop_stream_msg->app_id, media_stop_stream_msg->stream_id, media_stop_stream_msg->direction);
    
    }
    else
    {
      QVP_RTP_MSG_MED_0("unsupported media type ");
      status = QVP_RTP_ERR_FATAL;
    }
    /*----------------------------------------------------------------------
    If stop stream failed notify APP
    ----------------------------------------------------------------------*/
    if( status != QVP_RTP_SUCCESS )
    {
      /*----------------------------------------------------------------------
      If there is a uset call back installed just call it
      ----------------------------------------------------------------------*/
      if( user_ctx->call_backs.handle_media_res )
      {	
		  qvp_rtp_response_handler(media_stop_stream_msg->app_id, media_stop_stream_msg->stream_id, NULL, QVP_RTP_MEDIA_STOP_STREAM_RSP, status, TRUE,media_stop_stream_msg->direction);   

      }      
    }
  }
  else
  {

    /*----------------------------------------------------------------------
    If we have a valid app id then we should call the handle_media_res and let the
    user know the wrong doing. 
    ----------------------------------------------------------------------*/
    if ( media_stop_stream_msg && 
      media_stop_stream_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF 
      && ( media_stop_stream_msg->app_id < ( qvp_rtp_app_id_type ) 
      (uint32)rtp_ctx.num_users) 
      && rtp_ctx.user_array[ (uint32) media_stop_stream_msg->app_id ].valid )
    {
      /*--------------------------------------------------------------------
      If there is a registered handle_media_res cb then call it. 
      --------------------------------------------------------------------*/
      if ( rtp_ctx.user_array[ (uint32) media_stop_stream_msg->app_id ]. \
        call_backs.handle_media_res )
      {
	    qvp_rtp_response_handler(media_stop_stream_msg->app_id, media_stop_stream_msg->stream_id, NULL, QVP_RTP_MEDIA_STOP_STREAM_RSP, QVP_RTP_WRONG_PARAM, TRUE, media_stop_stream_msg->direction);   
      }
    }
  }
} /* end of function media_stop_stream_msg */

/*===========================================================================

FUNCTION QVP_RTP_RESUME_STREAM_CMD 

DESCRIPTION
Processes the application command to resume, already paused stream. 

DEPENDENCIES
None

PARAMETERS
media_resume_stream_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_media_resume_stream_cmd
(
 qvp_rtp_media_resume_stream_msg_type *media_resume_stream_msg
 )
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx;                     
  qvp_rtp_status_type status;

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range
  ------------------------------------------------------------------------*/
  if ( ( media_resume_stream_msg != NULL ) && 
    ( media_resume_stream_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( media_resume_stream_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( media_resume_stream_msg->stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   media_resume_stream_msg->app_id].num_streams) &&
    ( rtp_ctx.user_array[ (uint32)media_resume_stream_msg->app_id].valid ) && 
    ( rtp_ctx.user_array[ (uint32)media_resume_stream_msg->app_id]. \
    stream_array[ (uint32) media_resume_stream_msg->stream_id].valid ) )
  {


    user_ctx = &rtp_ctx.user_array[ (uint32)media_resume_stream_msg->app_id]; 
    stream = &user_ctx->stream_array[ (uint32) media_resume_stream_msg->stream_id ];
	/*Initialize the time stamp*/
	stream->prev_iGpsTimeMS = 0;
	stream->prev_iTxTimestampGps = 0;

    if(media_resume_stream_msg->direction == QVP_RTP_DIR_RX || media_resume_stream_msg->direction == QVP_RTP_DIR_TXRX)
    {
      status = qvp_rtp_empty_jitter_buffer_pvt(media_resume_stream_msg->app_id,media_resume_stream_msg->stream_id);
      if(status != QVP_RTP_SUCCESS)
      {
        QVP_RTP_ERR_0("qvp_rtp_empty_jitter_buffer_cmd: failed");
      }
    }

	if(!stream->time_session_start)
	{
		qvp_rtp_time_get_ms( &stream->time_session_start );
		QVP_RTP_MSG_LOW_1("QVP_RTP_RESUME_STREAM_CMD time_session_start = %lu",stream->time_session_start);
	}
 
    switch(media_resume_stream_msg->direction)
    {
    case  QVP_RTP_DIR_TX:
      if(stream->stream_state == INACTIVE)
        stream->stream_state = INACTIVE_RX;
      else if(stream->stream_state == INACTIVE_TX)
      {
        stream->stream_state = ACTIVE;
        if(qvp_rtp_send_fir_fb_msg(stream->rtcp_hdl)!=QVP_RTP_SUCCESS)
          QVP_RTP_MSG_HIGH_0("qvp_rtp_media_resume_stream_cmd: sending fir message failed....");

      }
  	  stream->send_dtmf = TRUE;	  
      break;
    case QVP_RTP_DIR_RX:
      if(stream->stream_state == INACTIVE)
        stream->stream_state = INACTIVE_TX;
      else if(stream->stream_state == INACTIVE_RX)
        stream->stream_state = ACTIVE;
      break;
    case QVP_RTP_DIR_TXRX:
      {
      stream->stream_state = ACTIVE;
        if(qvp_rtp_send_fir_fb_msg(stream->rtcp_hdl)!=QVP_RTP_SUCCESS)
          QVP_RTP_MSG_HIGH_0("qvp_rtp_media_resume_stream_cmd: sending fir message failed....");
				// send FIR
			  stream->send_dtmf = TRUE;				

      }
      break;
    default:
      QVP_RTP_MSG_LOW_0("qvp_rtp_media_resume_stream_cmd: Invalid case....");
      break;
    }
    if(NULL != rtp_ctx.metricTh && NULL != rtp_ctx.metrics &&
      codec_info_ctx.audio_codec_info.app_id == media_resume_stream_msg->app_id &&
      codec_info_ctx.audio_codec_info.stream_id == media_resume_stream_msg->stream_id)
    {
      uint8 sesnState = g_active_session;
      QVP_RTP_MSG_HIGH_0("metric:resume triggered change in state");
      switch(stream->stream_state)
      {
      case INACTIVE:/* this is an invalid case here */
        sesnState &= ~(QVP_RTP_UL_ACTIVE|QVP_RTP_DL_ACTIVE);
        break;
      case INACTIVE_TX:
        sesnState &= ~QVP_RTP_UL_ACTIVE;
        break;
      case INACTIVE_RX:
        sesnState &= ~QVP_RTP_DL_ACTIVE;
        break;
      case ACTIVE:
        sesnState |= (QVP_RTP_UL_ACTIVE | QVP_RTP_DL_ACTIVE);
        break;
      default:
        QVP_RTP_ERR_0("metric:this should not be triggered");
        break;
      }
      /* notify about the resumption and restart a timer */
      qvp_rtp_metric_hdl_activate(sesnState);

    }
	if(g_handoffstate == HO_IN_PROGRESS)
	{
		  qvp_rtp_post_execute_pending_command(media_resume_stream_msg->app_id);
  }

  }

} /* end of function qvp_rtp_media_resume_stream_cmd */

/*===========================================================================

FUNCTION QVP_RTP_PAUSE_STREAM_CMD 

DESCRIPTION
Processes the application command to pause, already started stream. 

DEPENDENCIES
None

PARAMETERS
media_pause_stream_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_media_pause_stream_cmd
(
 qvp_rtp_media_pause_stream_msg_type *media_pause_stream_msg
 )
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx; 

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range
  ------------------------------------------------------------------------*/
  if ( ( media_pause_stream_msg != NULL ) && 
    ( media_pause_stream_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( media_pause_stream_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( media_pause_stream_msg->stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   media_pause_stream_msg->app_id].num_streams) &&
    ( rtp_ctx.user_array[ (uint32)media_pause_stream_msg->app_id].valid ) && 
    ( rtp_ctx.user_array[ (uint32)media_pause_stream_msg->app_id]. \
    stream_array[ (uint32) media_pause_stream_msg->stream_id].valid ) )
  {
	g_Dec_RequestTime = 0;

	//g_isNetworkCommandPending = 0;

    user_ctx = &rtp_ctx.user_array[ (uint32)media_pause_stream_msg->app_id]; 
    stream = &user_ctx->stream_array[ (uint32) media_pause_stream_msg->stream_id ];
    if( g_isStreamStarted && (stream->stream_state == ACTIVE))
	  qvp_rtp_log_voice_call_stat( media_pause_stream_msg->app_id, media_pause_stream_msg->stream_id);
    switch(media_pause_stream_msg->direction)
    {
    case  QVP_RTP_DIR_TX:
      if(stream->stream_state == ACTIVE)
        stream->stream_state = INACTIVE_TX;
      else if(stream->stream_state == INACTIVE_RX)
        stream->stream_state = INACTIVE;
	  stream->send_dtmf = FALSE;
      break;
    case QVP_RTP_DIR_RX:
      if(stream->stream_state == ACTIVE)
        stream->stream_state = INACTIVE_RX;
      else if(stream->stream_state == INACTIVE_TX)
        stream->stream_state = INACTIVE;
      break;
    case QVP_RTP_DIR_TXRX:
      stream->stream_state = INACTIVE;
	  stream->send_dtmf = FALSE;
      break;
    default:
      QVP_RTP_MSG_LOW_0("qvp_rtp_media_pause_stream_cmd: Invalid case....");
      break;
    }
    if(NULL != rtp_ctx.metricTh && NULL != rtp_ctx.metrics &&
      codec_info_ctx.audio_codec_info.app_id == media_pause_stream_msg->app_id &&
      codec_info_ctx.audio_codec_info.stream_id == media_pause_stream_msg->stream_id)
    {
      uint8 sesnState = g_active_session;
      switch(stream->stream_state)
      {
      case INACTIVE:
        sesnState &= ~(QVP_RTP_UL_ACTIVE|QVP_RTP_DL_ACTIVE);
        break;
      case INACTIVE_TX:
        sesnState &= ~QVP_RTP_UL_ACTIVE;
        break;
      case INACTIVE_RX:
        sesnState &= ~QVP_RTP_DL_ACTIVE;
        break;
      case ACTIVE:/* this is an invalid case here */
        sesnState |= (QVP_RTP_UL_ACTIVE | QVP_RTP_DL_ACTIVE);
        break;
      default:
        QVP_RTP_ERR_0("this should not be triggered");
        break;
      }
      /* notify about the resumption and restart a timer */
      qvp_rtp_metric_hdl_activate(sesnState);
    }
  if(g_handoffstate == HO_IN_PROGRESS)
  {
		qvp_rtp_post_execute_pending_command(media_pause_stream_msg->app_id);
  }	
  }


} /* end of function qvp_rtp_media_pause_stream_cmd */

/*===========================================================================

FUNCTION qvp_rtp_process_tx_pkt

DESCRIPTION
This function is called every time there is a rtp packet to be sent out.


DEPENDENCIES
None

ARGUMENTS IN
app_id             -    app id
stream_id          -    stream which was opened
frame_data         -    frame data to be sent out
frame_length       -    frame length
tstamp             -    time stamp at which frame was generated

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
void qvp_rtp_process_tx_pkt
(
 qvp_rtp_app_id_type    app_id,      /* application which opened
                                     * the stream
                                     */
                                     qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
                                     uint8* frame_data,
                                     uint16 frame_length,
                                     uint32 tstamp,
                                     uint64 avtimestamp
                                     )
{
  qvp_rtp_send_pkt_type      pkt;
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_msg_type     *msg;          /* message pointer */
  qvp_rtp_usr_ctx *user_ctx;
  uint16               codec_dat_len = 0; 

  user_ctx = &rtp_ctx.user_array[ (uint32)app_id]; 
  stream = &user_ctx->stream_array[ (uint32) stream_id ];

  if(stream->stream_state == INACTIVE_TX || stream->stream_state == INACTIVE)
  {
    QVP_RTP_MSG_MED_0( "qvp_rtp_process_tx_pkt :: Pause called on TX/RXTX stream -> dropping pacckets \r\n ");
    return;
  }
  else if(stream->target_tstamp > tstamp )
  {
  	  QVP_RTP_MSG_MED_2( "qvp_rtp_process_tx_pkt ::target_tstamp = %u not reached current tstamp = %u-> dropping pacckets \r\n ", \
      stream->target_tstamp , tstamp);
    return;
  }

  /* Assign the buf ptr and buf len to the corresponding pkt parameters
  */
  stream->last_tstamp=tstamp;
  pkt.data = frame_data;
  pkt.len  = frame_length;
  pkt.codec_hdr.valid = FALSE;
  pkt.marker_bit = FALSE; 
  pkt.time_stamp = tstamp;
  pkt.rtp_pyld_type    = stream->payload_type;
  pkt.wall_clock.valid = TRUE;
  qvp_rtp_time_get_ms( &stream->RTP_sys_time);

  if(avtimestamp == 0)
  {
    pkt.wall_clock.wl_clock = stream->RTP_sys_time;
    QVP_RTP_MSG_HIGH_1( "qvp_rtp_service_send_av_sync_feed_first:: avtime from CVD is zero. Sending System time = %Lu ms",avtimestamp);
  }
  else
  {
    pkt.wall_clock.wl_clock = avtimestamp; 
  }

  pkt.aud_frame_type = QVP_RTP_AUDIO_SPEECH;//always good?

  if( ((stream->payload_type == QVP_RTP_PYLD_G711_A) || (stream->payload_type == QVP_RTP_PYLD_G711_U) ) && (frame_length == 11))
  {
      QVP_RTP_MSG_MED_2("G711 QVP_RTP_AUDIO_SID stream->payload_type = %d pkt.len:%d",stream->payload_type,pkt.len);
      pkt.aud_frame_type = QVP_RTP_AUDIO_SID;
 	}

 QVP_RTP_ERR_1 ("qvp_rtp_update_AVTime : wl_clock[0] = %Lu in ms ", pkt.wall_clock.wl_clock);
  /*------------------------------------------------------------------------
  Extract codec data from the packet. We need this ifo for len check
  ------------------------------------------------------------------------*/
  if( pkt.codec_hdr.valid && pkt.codec_hdr.codec_hdr)
  {
    codec_dat_len = pkt.codec_hdr.hdr_len;
  }
  else
  {
    codec_dat_len = 0;
  }

  /*------------------------------------------------------------------------
  Check the parameters first    
  ------------------------------------------------------------------------*/
  if( ( ( pkt.len + codec_dat_len ) > 
    ( QVP_RTP_MAX_PKT_SZ - QVP_RTP_HEAD_ROOM )  )  )
  {
    QVP_RTP_ERR_0 ("qvp_rtp_process_tx_pkt : wrong param on the function \r\n");
    return; 

  }
  /*------------------------------------------------------------------------
  Check if number of command buffers reached higher water mark
  ------------------------------------------------------------------------*/
  if( !qvp_rtp_is_safe_to_insert_buff() )
  {
    QVP_RTP_ERR_0 ( \
      "qvp_rtp_process_tx_pkt : Max buff limit reached,dropping RTP outgoing \
      packets \r\n");

    return;
  }

  /*------------------------------------------------------------------------
  Try to get a command buffer 
  ------------------------------------------------------------------------*/
  msg = qvp_rtp_get_msg_buf(); 

  if( !msg )
  {
    QVP_RTP_ERR_0 ( \
      "qvp_rtp_process_tx_pkt : Could not get a free command buffer\r\n");
    return;
  }

  /*------------------------------------------------------------------------
  we are just setting the value, we arent really posting     
  ------------------------------------------------------------------------*/
  msg->msg_type = QVP_RTP_SEND_CMD; 

  /*------------------------------------------------------------------------
  Copy the parameteters
  ------------------------------------------------------------------------*/
  msg->msg_param.send_msg.app_id  = app_id;
  msg->msg_param.send_msg.stream_id = stream_id;

  /*------------------------------------------------------------------------
  We need to Deep copy the packet structure 
  ------------------------------------------------------------------------*/
  if( ( msg->msg_param.send_msg.pkt = qvp_rtp_alloc_buf_by_len( pkt.len 
    +  QVP_RTP_HEAD_ROOM + codec_dat_len) )
    == NULL )
  {
    /*----------------------------------------------------------------------
    Free the message buffer before we leave here. 
    ----------------------------------------------------------------------*/
    qvp_rtp_free_msg_buf( msg );
    QVP_RTP_MSG_HIGH_0( "qvp_rtp_process_tx_pkt : could not get data packet for the stream \r\n");
    return;

  }


  /*------------------------------------------------------------------------
  Set up the RTP header related stuff in the respective fields. 
  ------------------------------------------------------------------------*/
  msg->msg_param.send_msg.pkt->marker_bit = pkt.marker_bit;
  msg->msg_param.send_msg.pkt->tstamp = pkt.time_stamp;
  msg->msg_param.send_msg.pkt->rtp_pyld_type= pkt.rtp_pyld_type;
  msg->msg_param.send_msg.pkt->frm_info.info.aud_info.frame_type 
    = pkt.aud_frame_type;



  /*------------------------------------------------------------------------
  Set up head room 
  ------------------------------------------------------------------------*/
  msg->msg_param.send_msg.pkt->head_room = QVP_RTP_HEAD_ROOM;


  /*------------------------------------------------------------------------
  Codec header is used for Mp4(3016) and H263(2190) temporarily as the
  payload header is packed by codec. Data will be copied into payload 
  data in the profile files.
  Copy the codec hdr to pkt's codec hdr
  ------------------------------------------------------------------------*/
  if( codec_dat_len )
  {
    msg->msg_param.send_msg.pkt->codec_hdr.valid = TRUE;
    msg->msg_param.send_msg.pkt->codec_hdr.hdr_len = codec_dat_len;
    qvp_rtp_memscpy( (uint8 *) msg->msg_param.send_msg.pkt->codec_hdr.codec_hdr,
      sizeof(qvp_rtp_send_pkt_codec_hdr_type),
      pkt.codec_hdr.codec_hdr, codec_dat_len );
    /*----------------------------------------------------------------------
    Codec header will be accomodated in the head room
    ----------------------------------------------------------------------*/    
    msg->msg_param.send_msg.pkt->head_room += codec_dat_len;

  }
  else
  {
    msg->msg_param.send_msg.pkt->codec_hdr.valid = FALSE;
  }

  /*------------------------------------------------------------------------
  Copy message to the buffer in the pakt 
  ------------------------------------------------------------------------*/
  /*------------------------------------------------------------------------
  There is no way to know the exact buffer allocated  by the caller , 
  hence we assume atleast equal to source buffer 
  ------------------------------------------------------------------------*/
  qvp_rtp_memscpy( (uint8 *) msg->msg_param.send_msg.pkt->data + 
    msg->msg_param.send_msg.pkt->head_room,pkt.len, pkt.data, pkt.len );

  /*------------------------------------------------------------------------
  Update the length to reflect the codec data and the packet len total
  ------------------------------------------------------------------------*/
  msg->msg_param.send_msg.pkt->len = pkt.len;

  /*------------------------------------------------------------------------
  We allocated a new buffer. need to free it 
  ------------------------------------------------------------------------*/
  msg->msg_param.send_msg.pkt->need_tofree = TRUE;

  /*------------------------------------------------------------------------
  Copy the wall clock struct back to the message we are posting
  ------------------------------------------------------------------------*/
  qvp_rtp_memscpy( &msg->msg_param.send_msg.wl_cl, sizeof(qvp_rtp_sample_wall_clock_type),&pkt.wall_clock, 
    sizeof( msg->msg_param.send_msg.wl_cl ) );
  QVP_RTP_MSG_LOW_1("qvp_rtp_process_tx_pkt : Sending audio of size %d", pkt.len);
  qvp_rtp_send_cmd( &msg->msg_param.send_msg );
  /*--------------------------------------------------------------------
  Put the message back in free queue after sending
  --------------------------------------------------------------------*/
  qvp_rtp_free_msg_buf( msg );
}

LOCAL void qvp_rtp_proxy_dpl_init (void *param)
{

  QVP_RTP_MSG_HIGH_0("qvp_rtp_proxy_dpl_init entered");
    if(dpl_init_proxy_timer)
    {
      qvp_rtp_free_timer( dpl_init_proxy_timer );
      dpl_init_proxy_timer = 0;
    }
  QVP_RTP_MSG_HIGH_0("qvp_rtp_proxy_dpl_init calling dpl init");
  qvp_rtp_init_dpl();
    
}
/*===========================================================================

FUNCTION QVP_RTP_HANDLE_RTP_LINK_ALIVE_TIMER

DESCRIPTION
This function is called every time the  RTP link alive timer fires.


DEPENDENCIES
None

ARGUMENTS IN
None

RETURN VALUE
QVP_RTP_SUCCESS  - operation was succesful
appropriate error code otherwise


SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_handle_rtp_link_alive_timer (void *param)
{
  qvp_rtp_stream_type *stream = (qvp_rtp_stream_type*)param;
  qvp_rtp_usr_ctx *user_ctx;

  if(!stream)
    return;

  user_ctx = ( qvp_rtp_usr_ctx * ) stream->usr_ctx;
  if( user_ctx && user_ctx->call_backs.handle_rtp_time_out && stream->rtp_link_alive_timer)
  {

    if(stream->stream_state != INACTIVE_RX && stream->stream_state != INACTIVE) 
    {
      uint64 ts_val = 0;
      uint32 T_current;

      qvp_rtp_time_get_ms(&ts_val);
      T_current = ((uint32)ts_val);
      if((T_current - stream->T_last) < ((stream->rtp_link_alive_time*1000) + QVP_TIMER_TICK_INTERVAL))
      {
        uint32 temp = ((stream->rtp_link_alive_time *1000) + QVP_TIMER_TICK_INTERVAL) - (T_current - stream->T_last);
        if(temp)
        {
          if( qvp_rtp_timer_start_in_millisecs( stream->rtp_link_alive_timer, temp ) 
            != QVP_RTP_SUCCESS )
          {
            QVP_RTP_ERR_0( "rtp_link_alive_timer: Rearming the rtp pkt timer failed");
          }
        }
      }
      else
      {
        QVP_RTP_ERR( "rtp_link_alive_timer: Failed T_current=%lu T_last=%lu rtp_link_alive_time=%lu", T_current, stream->T_last, (stream->rtp_link_alive_time*1000) );
      	/*user_ctx->call_backs.handle_rtp_time_out(user_ctx->app_id,stream->str_id);    */
		stream->first_pckt_rcvd = FALSE;
		qvp_rtp_response_handler(user_ctx->app_id, stream->str_id, NULL, QVP_RTP_LINK_ALIVE_TIMER_RSP, QVP_RTP_SUCCESS, TRUE, 0);
      }
    }
  }
}/*qvp_rtp_handle_rtp_link_alive_timer*/


void generate_dummy_packet(qvp_rtp_stream_type *stream)
{
	 qvp_rtp_usr_ctx *user_ctx;
	 qvp_rtp_buf_type       *pkt;

	 user_ctx = ( qvp_rtp_usr_ctx * ) stream->usr_ctx;

	 //QVP_RTP_ERR_0( "generate_dummy_packet");

	 pkt = qvp_rtp_alloc_buf_by_len(QVP_RTP_HEAD_ROOM);
	// stream->ob_stream.pkt_cnt++; // need too chek on this

	 if(pkt) 
	 {
		 //memset(send_msg.pkt,0
		 if ((qvp_rtp_get_media_type(user_ctx->app_id,stream->str_id) == QVP_RTP_AUDIO))
		 {
			 if(stream->clock_rate == 8)
			 {
				stream->T_sent = stream->T_sent + 160;
				pkt->tstamp = stream->T_sent;
			 }
			 else if(stream->clock_rate == 16)
			 {
				 stream->T_sent = stream->T_sent + 320;
				 pkt->tstamp = stream->T_sent;
			 }
		 }
		 else if ((qvp_rtp_get_media_type(user_ctx->app_id,stream->str_id) == QVP_RTP_VIDEO))
		 {
				stream->T_sent = stream->T_sent + (20*90);
				pkt->tstamp = stream->T_sent;
		 }
		 QVP_RTP_ERR_1( "generate_dummy_packet  pkt tstamp: %d",pkt->tstamp);

		 pkt->marker_bit = 0;
		 pkt->need_tofree = TRUE;
		 pkt->head_room = QVP_RTP_HEAD_ROOM;
		 pkt->len = 0;
		 stream->rtp.tx_payload_type = pkt->rtp_pyld_type = 20; // payload for empty ppkt in case of NAT
		 qvp_rtp_pack( &stream->rtp, pkt );
		 if( qvp_rtp_send_nw( stream->ob_stream.sock_id, 
														 pkt ) != QVP_RTP_SUCCESS )
			{
				qvp_rtp_free_buf( pkt );
				QVP_RTP_ERR_0(" &&& NW send failed on the packet \r\n");
				return; 

			}
	 }
}
LOCAL void qvp_rtp_handle_rtp_keep_alive_timer (void *param)
{
  qvp_rtp_stream_type *stream = (qvp_rtp_stream_type*)param;
  qvp_rtp_usr_ctx *user_ctx;

  if(!stream)
    return;

  user_ctx = ( qvp_rtp_usr_ctx * ) stream->usr_ctx;
  if( user_ctx && user_ctx->call_backs.handle_rtp_time_out && stream->rtp_keep_alive_timer)
  {
    {
      generate_dummy_packet(stream);

    }
    if(stream->rtp_keep_alive_time)
    {

			 QVP_RTP_ERR_1( "qvp_rtp_handle_rtp_keep_alive_timer for duration %d ",stream->rtp_keep_alive_time);
      if( qvp_rtp_timer_start_in_millisecs( stream->rtp_keep_alive_timer,stream->rtp_keep_alive_time  ) 
        != QVP_RTP_SUCCESS )
      {
        QVP_RTP_ERR_0( "rtp_keep_alive_timer: Rearming the rtp pkt timer failed");
      }
    }
	if(g_handoffstate == HO_IN_PROGRESS)
	{
		qvp_rtp_post_execute_pending_command(user_ctx->app_id);	
	}  
  }
}/*qvp_rtp_handle_rtp_link_alive_timer*/
LOCAL void qvp_rtp_handle_rtcp_keep_alive_timer (void *param)
{
  qvp_rtp_stream_type *stream = (qvp_rtp_stream_type*)param;
  qvp_rtp_usr_ctx *user_ctx;

  if(!stream)
    return;

  user_ctx = ( qvp_rtp_usr_ctx * ) stream->usr_ctx;
   if( user_ctx && user_ctx->call_backs.handle_rtp_time_out && stream->rtcp_keep_alive_timer)
  {
    {
      generate_dummy_packet(stream);

    }
		if(stream->rtcp_keep_alive_time)
    {
			 QVP_RTP_ERR_1( "qvp_rtp_handle_rtcp_keep_alive_timer for duration %d  ",stream->rtcp_keep_alive_time);
      if( qvp_rtp_timer_start_in_millisecs( stream->rtcp_keep_alive_timer, stream->rtcp_keep_alive_time ) 
        != QVP_RTP_SUCCESS )
      {
        QVP_RTP_ERR_0( "qvp_rtp_handle_rtp_keep_alive_timer: Rearming the rtp pkt timer failed");
      }
    }
	if(g_handoffstate == HO_IN_PROGRESS)
	{
		qvp_rtp_post_execute_pending_command(user_ctx->app_id);	
	}  	
  }
}/*qvp_rtp_handle_rtp_link_alive_timer*/


/*===========================================================================

FUNCTION QVP_RTP_LINK_ALIVE_TIMER_CMD 

DESCRIPTION
Processes the application command to start RTP link aliveness. 

DEPENDENCIES
None

PARAMETERS
rtp_link_alive_timer_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_link_alive_timer_cmd
(
 qvp_rtp_link_alive_timer_msg_type *rtp_link_alive_timer_msg
 )
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx; 
  int32 ms_time = 0;

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range
  ------------------------------------------------------------------------*/
  if ( ( rtp_link_alive_timer_msg != NULL ) && 
    ( rtp_link_alive_timer_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( rtp_link_alive_timer_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( rtp_link_alive_timer_msg->stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   rtp_link_alive_timer_msg->app_id].num_streams) &&
    ( rtp_ctx.user_array[ (uint32)rtp_link_alive_timer_msg->app_id].valid ) && 
    ( rtp_ctx.user_array[ (uint32)rtp_link_alive_timer_msg->app_id]. \
    stream_array[ (uint32) rtp_link_alive_timer_msg->stream_id].valid ) )
  {
    user_ctx = &rtp_ctx.user_array[ (uint32)rtp_link_alive_timer_msg->app_id]; 
    stream = &user_ctx->stream_array[ (uint32) rtp_link_alive_timer_msg->stream_id ];
    stream->rtp_link_alive_time = rtp_link_alive_timer_msg->rtp_link_alive_time;

    if(stream->rtp_link_alive_timer)
    {
      QVP_RTP_MSG_HIGH_1("start rtp link alive timer with %d value",stream->rtp_link_alive_time);
      if( qvp_rtp_reset_timer(stream->rtp_link_alive_timer) != QVP_RTP_SUCCESS )
      {
        QVP_RTP_MSG_LOW_0("link_alive_timer_cmd: Could not reset the RTP link aliveness timer( sigh.. ) ");
      }
      if(stream->rtp_link_alive_time)
      {
        ms_time = stream->rtp_link_alive_time *1000 + QVP_TIMER_TICK_INTERVAL;
        if( qvp_rtp_timer_start_in_millisecs( stream->rtp_link_alive_timer, ms_time) 
          != QVP_RTP_SUCCESS )
        {
          QVP_RTP_ERR_0( "qvp_rtp_link_alive_timer_cmd: Arming the rtp pkt timer failed");
        }
      }
    }
	
	if(g_handoffstate == HO_IN_PROGRESS)
	{
		qvp_rtp_post_execute_pending_command(rtp_link_alive_timer_msg->app_id);
	}
  }
} /* end of function qvp_rtp_link_alive_timer_cmd */

/*===========================================================================

FUNCTION QVP_RTCP_LINK_ALIVE_TIMER_CMD 

DESCRIPTION
Processes the application command to start rtcp link aliveness. 

DEPENDENCIES
None

PARAMETERS
rtcp_link_alive_timer_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtcp_link_alive_timer_cmd
(
 qvp_rtcp_link_alive_timer_msg_type *rtcp_link_alive_timer_msg
 )
{
  qvp_rtp_status_type status;
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx; 

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range
  ------------------------------------------------------------------------*/
  if ( ( rtcp_link_alive_timer_msg != NULL ) && 
    ( rtcp_link_alive_timer_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( rtcp_link_alive_timer_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( rtcp_link_alive_timer_msg->stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   rtcp_link_alive_timer_msg->app_id].num_streams) &&
    ( rtp_ctx.user_array[ (uint32)rtcp_link_alive_timer_msg->app_id].valid ) && 
    ( rtp_ctx.user_array[ (uint32)rtcp_link_alive_timer_msg->app_id]. \
    stream_array[ (uint32) rtcp_link_alive_timer_msg->stream_id].valid ) )
  {
    user_ctx = &rtp_ctx.user_array[ (uint32)rtcp_link_alive_timer_msg->app_id]; 
    stream = &user_ctx->stream_array[ (uint32) rtcp_link_alive_timer_msg->stream_id ];
    status = qvp_rtcp_start_link_alive_timer(stream->rtcp_hdl,rtcp_link_alive_timer_msg->rtcp_link_alive_time);
    if(status != QVP_RTP_SUCCESS)
    {
      QVP_RTP_ERR_0("qvp_rtcp_link_alive_timer_cmd: Starting rtcp link aliveness timer failed");
    }

	if(g_handoffstate == HO_IN_PROGRESS)
	{
		qvp_rtp_post_execute_pending_command(rtcp_link_alive_timer_msg->app_id);
	}
  }
} /* end of function qvp_rtcp_link_alive_timer_cmd */

/*===========================================================================

FUNCTION QVP_RTP_HANDLE_RTCP_LINK_ALIVENESS_INFO

DESCRIPTION

This function is called from RTCP sub layer every time a SDES/SR or any 
other report is received from the network. This routines funnels the 
info to appropriate entities to take action


DEPENDENCIES
None

PARAMETERS
handle       - handle to RTCP session
app_hdl      - handle we supplied to RTCP

RETURN VALUE
None


SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_handle_rtcp_link_aliveness_info
(

 qvp_rtcp_session_handle_type handle,
 qvp_rtcp_app_handle_type     app_hdl
 )
{
  qvp_rtp_stream_type *stream = ( qvp_rtp_stream_type *) app_hdl;
  qvp_rtp_usr_ctx *user_ctx; 
  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Ignore the info if some thing does not make sense.
  ------------------------------------------------------------------------*/
  if( !stream || ( stream->rtcp_hdl != handle ) ||
    !stream->usr_ctx )
  {
    QVP_RTP_ERR_0( " Params in qvp_rtp_handle_rtcp_link_aliveness_info call back in error");
    return;
  }


  user_ctx = (qvp_rtp_usr_ctx*)stream->usr_ctx;
  if( !user_ctx || !user_ctx->call_backs.handle_rtcp_time_out )
  {
    QVP_RTP_MSG_MED_0( "qvp_rtp_handle_rtcp_link_aliveness_info:cb info not found ");
    return;
  }
 /* user_ctx->call_backs.handle_rtcp_time_out ( user_ctx->app_id, stream->str_id);*/
 qvp_rtp_response_handler(user_ctx->app_id, stream->str_id, NULL, QVP_RTCP_LINK_ALIVE_TIMER_RSP, QVP_RTP_SUCCESS, TRUE, 0);

} /* qvp_rtp_handle_rtcp_link_aliveness_info */

/*===========================================================================

FUNCTION QVP_RTP_EMPTY_JITTER_BUFFER_CMD 

DESCRIPTION
Processes the application command to empty jitter buffer. 

DEPENDENCIES
None

PARAMETERS
rtp_empty_jitter_buffer_msg - message which was posted by the API into the module.

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_empty_jitter_buffer_cmd
(
 qvp_rtp_empty_jitter_buffer_msg_type *rtp_empty_jitter_buffer_msg
 )
{
  qvp_rtp_status_type status;
  //qvp_rtp_stream_type *stream; /* for inderection simplicity */
//  qvp_rtp_usr_ctx *user_ctx; 

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range
  ------------------------------------------------------------------------*/
  if ( ( rtp_empty_jitter_buffer_msg != NULL ) && 
    ( rtp_empty_jitter_buffer_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( rtp_empty_jitter_buffer_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( rtp_empty_jitter_buffer_msg->stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   rtp_empty_jitter_buffer_msg->app_id].num_streams) &&
    ( rtp_ctx.user_array[ (uint32)rtp_empty_jitter_buffer_msg->app_id].valid ) && 
    ( rtp_ctx.user_array[ (uint32)rtp_empty_jitter_buffer_msg->app_id]. \
    stream_array[ (uint32) rtp_empty_jitter_buffer_msg->stream_id].valid ) )
  {
    status = qvp_rtp_empty_jitter_buffer_pvt(rtp_empty_jitter_buffer_msg->app_id,rtp_empty_jitter_buffer_msg->stream_id);
    if(status != QVP_RTP_SUCCESS)
    {
      QVP_RTP_ERR_0("qvp_rtp_empty_jitter_buffer_cmd: failed");
    }
  }
} /* end of function qvp_rtcp_link_alive_timer_cmd */

void qvp_rtp_get_dtmf_endbit
(
 qvp_rtp_app_id_type    app_id, 
 qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
 boolean end_bit
 )
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity    */
  qvp_rtp_usr_ctx *usr_ctx;   /* accessor for the user context */
  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range
  4) stream is o/b or full duplex
  5) good packet
  6) session not in reset mode. 
  ------------------------------------------------------------------------*/
  if ( ( app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   app_id].num_streams) &&
    rtp_ctx.user_array[ (uint32)app_id]. \
    stream_array[ (uint32) stream_id].ob_stream.valid &&
    ( rtp_ctx.user_array[ (uint32)app_id]. \
    stream_array[ (uint32) stream_id].state == 
    QVP_RTP_SESS_ACTIVE ))
  {
    QVP_RTP_MSG_HIGH_0(" sending DTMF \r\n");
    /*----------------------------------------------------------------------
    Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ]; 

    /*----------------------------------------------------------------------
    extract the stream array from the context 
    ----------------------------------------------------------------------*/
    stream    =  &( usr_ctx->stream_array[ (uint32) stream_id ] ); 
    stream->dtmf_endbit = end_bit;
  }
}
LOCAL void qvp_start_keep_alive_timer_cmd
(
 qvp_start_keep_alive_timer_msg_type *start_keep_alive_timer_msg
 )
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx; 

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range
  ------------------------------------------------------------------------*/
	QVP_RTP_ERR_0( "qvp_start_keep_alive_timer_cmd: start");
	if ( ( start_keep_alive_timer_msg != NULL ) && 
    ( start_keep_alive_timer_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( start_keep_alive_timer_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( start_keep_alive_timer_msg->stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   start_keep_alive_timer_msg->app_id].num_streams) &&
    ( rtp_ctx.user_array[ (uint32)start_keep_alive_timer_msg->app_id].valid ) && 
    ( rtp_ctx.user_array[ (uint32)start_keep_alive_timer_msg->app_id]. \
    stream_array[ (uint32) start_keep_alive_timer_msg->stream_id].valid ) )
  {
    user_ctx = &rtp_ctx.user_array[ (uint32)start_keep_alive_timer_msg->app_id]; 
    stream = &user_ctx->stream_array[ (uint32) start_keep_alive_timer_msg->stream_id ];
/*PG: consider removing the duplication of code here*/		
		if(start_keep_alive_timer_msg->timer_type == QVP_RTP_KEEP_ALIVE_TIMER)
		{
			if(stream->rtp_keep_alive_timer)
			{

				if( qvp_rtp_reset_timer(stream->rtp_keep_alive_timer) != QVP_RTP_SUCCESS )
				{
					QVP_RTP_MSG_LOW_0("qvp_start_keep_alive_timer_cmd: Could not reset the RTP link aliveness timer( sigh.. ) ");
				}
				if(stream->rtp_keep_alive_time)
				{
						QVP_RTP_ERR_0( "qvp_start_keep_alive_timer_cmd: start rtp timer in ms");
					if( qvp_rtp_timer_start_in_millisecs( stream->rtp_keep_alive_timer, stream->rtp_keep_alive_time  ) 
						!= QVP_RTP_SUCCESS )
					{
						QVP_RTP_ERR_0( "qvp_start_keep_alive_timer_cmd: Arming the rtp pkt timer failed");
					}
				}
			}
		}
		else if(start_keep_alive_timer_msg->timer_type == QVP_RTCP_KEEP_ALIVE_TIMER)
		{
			if(stream->rtcp_keep_alive_timer)
			{

				if( qvp_rtp_reset_timer(stream->rtcp_keep_alive_timer) != QVP_RTP_SUCCESS )
				{
					QVP_RTP_MSG_LOW_0("qvp_start_keep_alive_timer_cmd: Could not reset the RTCP keep aliveness timer( sigh.. ) ");
				}
				if(stream->rtcp_keep_alive_time)
				{
					QVP_RTP_ERR_0( "qvp_start_keep_alive_timer_cmd: start rtcp timer in ms");
					if( qvp_rtp_timer_start_in_millisecs( stream->rtcp_keep_alive_timer, stream->rtcp_keep_alive_time  ) 
						!= QVP_RTP_SUCCESS )
					{
						QVP_RTP_ERR_0( "qvp_start_keep_alive_timer_cmd: Arming the rtcp pkt timer failed");
					}
				}
			}
		}
	if(g_handoffstate == HO_IN_PROGRESS)
	{
		qvp_rtp_post_execute_pending_command(start_keep_alive_timer_msg->app_id);	
	}		
  }
} /* end of function qvp_rtcp_link_alive_timer_cmd */
LOCAL void qvp_stop_keep_alive_timer_cmd
(
 qvp_stop_keep_alive_timer_msg_type *stop_keep_alive_timer_msg
 )
{
  qvp_rtp_stream_type *stream; /* for inderection simplicity */
  qvp_rtp_usr_ctx *user_ctx; 

  /*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
  Check forparams before we act on message 
  1) good app id
  2) app id in range
  3) stream id in range
  ------------------------------------------------------------------------*/
  if ( ( stop_keep_alive_timer_msg != NULL ) && 
    ( stop_keep_alive_timer_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( stop_keep_alive_timer_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) &&
    ( stop_keep_alive_timer_msg->stream_id < ( qvp_rtp_stream_id_type )
    (uint32)rtp_ctx.user_array[ (uint32)   stop_keep_alive_timer_msg->app_id].num_streams) &&
    ( rtp_ctx.user_array[ (uint32)stop_keep_alive_timer_msg->app_id].valid ) && 
    ( rtp_ctx.user_array[ (uint32)stop_keep_alive_timer_msg->app_id]. \
    stream_array[ (uint32) stop_keep_alive_timer_msg->stream_id].valid ) )
  {
    user_ctx = &rtp_ctx.user_array[ (uint32)stop_keep_alive_timer_msg->app_id]; 
    stream = &user_ctx->stream_array[ (uint32) stop_keep_alive_timer_msg->stream_id ];

    /*PG: consider removing duplication of code here */
    if(stop_keep_alive_timer_msg->timer_type == QVP_RTP_KEEP_ALIVE_TIMER)
		{
			if(stream->rtp_keep_alive_timer)
			{
				QVP_RTP_ERR_0( "qvp_stop_keep_alive_timer_cmd: start rtp timer in ms");
				if( qvp_rtp_reset_timer(stream->rtp_keep_alive_timer) != QVP_RTP_SUCCESS )
				{
					QVP_RTP_MSG_LOW_0("qvp_stop_keep_alive_timer_cmd: Could not reset the RTP keep aliveness timer( sigh.. ) ");
				}
			}
		}
		else if(stop_keep_alive_timer_msg->timer_type == QVP_RTCP_KEEP_ALIVE_TIMER)
		{
			if(stream->rtcp_keep_alive_timer)
			{
				QVP_RTP_ERR_0( "qvp_stop_keep_alive_timer_cmd: start rtcp timer in ms");
				if( qvp_rtp_reset_timer(stream->rtcp_keep_alive_timer) != QVP_RTP_SUCCESS )
				{
					QVP_RTP_MSG_LOW_0("qvp_stop_keep_alive_timer_cmd: Could not reset the RTCP keep aliveness timer( sigh.. ) ");
				}
			}
		}
  }
} /* end of function qvp_rtcp_link_alive_timer_cmd */

void qvp_rtp_find_internal_streamid_type(qvp_rtp_msg_type *msg, int is_app, boolean is_req)
{
	int offset = QVP_RTP_MAX_STREAMS_DFLT;
	qvp_rtp_app_id_type app_id = 0;
	qvp_rtp_stream_id_type stream_id = 0;
	
	if(!msg)
	{
	  QVP_RTP_ERR_0("qvp_rtp_find_internal_streamid_type msgs null");
	  return;
	}

	if(is_app) /* offset to Convert to Apps internal stream id*/
	{
		offset += QVP_RTP_APPS_STREAM_ID_OFFSET;
	}
	if(is_req)
	{
		offset *= -1;
	}

	switch ( msg->msg_type )
	{
	    QVP_RTP_MSG_MED_1("qvp_rtp_find_internal_streamid_type: msg_type:%d for modem",msg->msg_type);
		
	    case  QVP_RTP_OPEN_CMD :
	    case  QVP_RTP_OPEN2_CMD :
			return;

	    case QVP_RTP_CONFIG_CMD :
	      {
		  	app_id = msg->msg_param.config_msg.app_id;
		  	stream_id = msg->msg_param.config_msg.stream_id;
			msg->msg_param.config_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	      }      
	    case QVP_RTP_CONFIG_SESSION_CMD :
	      {
		  	app_id = msg->msg_param.config_session_msg.app_id;
		  	stream_id = msg->msg_param.config_session_msg.stream_id;
			msg->msg_param.config_session_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);		  	
			break;
	      }	
	    case QVP_RTP_RESET_CMD :
	      {
		  	app_id = msg->msg_param.reset_msg.app_id;
		  	stream_id = msg->msg_param.reset_msg.stream_id;	
			msg->msg_param.reset_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	      }
	    case  QVP_RTP_SEND_CMD :
		case  QVP_RTP_SEND_DTMF_CMD :
	      {
		  	app_id = msg->msg_param.send_msg.app_id;
		  	stream_id = msg->msg_param.send_msg.stream_id;	
			msg->msg_param.send_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	      }
	    case  QVP_RTP_CLOSE_CMD :
	      {
		  	app_id = msg->msg_param.close_msg.app_id;
		  	stream_id = msg->msg_param.close_msg.stream_id;	
			msg->msg_param.close_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	      }
	    case QVP_RTP_SEND_RTCP_BYE_CMD:
	      {
		  	app_id = msg->msg_param.rtcp_bye_msg.app_id;
		  	stream_id = msg->msg_param.rtcp_bye_msg.stream_id;	
			msg->msg_param.rtcp_bye_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	      }
	    case QVP_RTP_SEND_RTCP_APP_CMD:
	      {
		  	app_id = msg->msg_param.rtcp_app_msg.app_id;
		  	stream_id = msg->msg_param.rtcp_app_msg.stream_id;
			msg->msg_param.rtcp_app_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	      }
	    case QVP_RTP_SEND_RTCP_VOIP_XR_CMD:
	      {
		  	app_id = msg->msg_param.rtcp_voip_xr_msg.app_id;
		  	stream_id = msg->msg_param.rtcp_voip_xr_msg.stream_id;
			msg->msg_param.rtcp_voip_xr_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	      }
	    case QVP_RTP_SET_VOIP_XR_SIGNAL_METRICS_CMD:
	      {
		  	app_id = msg->msg_param.rtcp_signal_metrics_msg.app_id;
		  	stream_id = msg->msg_param.rtcp_signal_metrics_msg.stream_id;	
			msg->msg_param.rtcp_signal_metrics_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	      }
	    case QVP_RTP_SET_VOIP_XR_TX_QUALITY_CMD:
	      {
		  	app_id = msg->msg_param.rtcp_tx_metrics_msg.app_id;
		  	stream_id = msg->msg_param.rtcp_tx_metrics_msg.stream_id;
			msg->msg_param.rtcp_tx_metrics_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	      }
	    case QVP_RTP_CODEC_INIT_CMD:
	      {
		  	app_id = msg->msg_param.media_codec_init_msg.app_id;
		  	stream_id = msg->msg_param.media_codec_init_msg.stream_id;	
			msg->msg_param.media_codec_init_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_MEDIA_CODEC_CONFIG_CMD:
	    {
		  	app_id = msg->msg_param.media_codec_config_msg.app_id;
		  	stream_id = msg->msg_param.media_codec_config_msg.stream_id;	
			msg->msg_param.media_codec_config_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_START_STREAM_CMD:
	    {
		  	app_id = msg->msg_param.media_start_stream_msg.app_id;
		  	stream_id = msg->msg_param.media_start_stream_msg.stream_id;
			msg->msg_param.media_start_stream_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_STOP_STREAM_CMD:
	    {
		  	app_id = msg->msg_param.media_stop_stream_msg.app_id;
		  	stream_id = msg->msg_param.media_stop_stream_msg.stream_id;	
			//msg->msg_param.media_stop_stream_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_RESUME_STREAM_CMD:
	    {
		  	app_id = msg->msg_param.media_resume_stream_msg.app_id;
		  	stream_id = msg->msg_param.media_resume_stream_msg.stream_id;
			msg->msg_param.media_resume_stream_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_PAUSE_STREAM_CMD:
	    {
		  	app_id = msg->msg_param.media_pause_stream_msg.app_id;
		  	stream_id = msg->msg_param.media_pause_stream_msg.stream_id;
			msg->msg_param.media_pause_stream_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_LINK_ALIVE_TIMER_CMD:
	    {
		  	app_id = msg->msg_param.rtp_link_alive_timer_msg.app_id;
		  	stream_id = msg->msg_param.rtp_link_alive_timer_msg.stream_id;	
			msg->msg_param.media_pause_stream_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTCP_LINK_ALIVE_TIMER_CMD:
{
		  	app_id = msg->msg_param.rtcp_link_alive_timer_msg.app_id;
		  	stream_id = msg->msg_param.rtcp_link_alive_timer_msg.stream_id;
			msg->msg_param.rtcp_link_alive_timer_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_EMPTY_JITTER_BUFFER_CMD:
	    {
		  	app_id = msg->msg_param.rtp_empty_jitter_buffer_msg.app_id;
		  	stream_id = msg->msg_param.rtp_empty_jitter_buffer_msg.stream_id;				
 	      	msg->msg_param.rtp_empty_jitter_buffer_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_START_KEEP_ALIVE_TIMER:
	   {
		  	app_id = msg->msg_param.start_keep_alive_timer.app_id;
		  	stream_id = msg->msg_param.start_keep_alive_timer.stream_id;
			msg->msg_param.start_keep_alive_timer.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_STOP_KEEP_ALIVE_TIMER:
	    {
		  	app_id = msg->msg_param.stop_keep_alive_timer.app_id;
		  	stream_id = msg->msg_param.stop_keep_alive_timer.stream_id;	
			msg->msg_param.stop_keep_alive_timer.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_ENABLE_LS_CMD:
	    {
		  	app_id = msg->msg_param.enable_ls_msg.app_id;
		  	stream_id = msg->msg_param.enable_ls_msg.video_stream;	
			msg->msg_param.enable_ls_msg.video_stream= qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_DISABLE_LS_CMD:
	    {
		  	app_id = msg->msg_param.disable_ls_msg.app_id;
		  	stream_id = msg->msg_param.disable_ls_msg.video_stream;	
			msg->msg_param.disable_ls_msg.video_stream= qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_MEDIA_CAPABILITY_CMD:
	    {
		  	app_id = msg->msg_param.media_capability_msg.app_id;
		  	stream_id = msg->msg_param.media_capability_msg.stream_id;	
			msg->msg_param.media_capability_msg.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_GENERATE_INTRA_CODEC_FRAME_CMD:
	    {
		  	app_id = msg->msg_param.rtp_generate_intra_frame_msg_type.app_id;
		  	stream_id = msg->msg_param.rtp_generate_intra_frame_msg_type.stream_id;	
			msg->msg_param.rtp_generate_intra_frame_msg_type.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }
	  case QVP_RTP_CONFIGURE_GBR_MBR_CMD:
	    {
		  	app_id = msg->msg_param.rtp_configure_gbr_mbr_msg_type.app_id;
		  	stream_id = msg->msg_param.rtp_configure_gbr_mbr_msg_type.stream_id;	
			msg->msg_param.rtp_configure_gbr_mbr_msg_type.stream_id = qvp_rtp_find_internal_streamid( app_id, stream_id,is_app);
			break;
	    }	  
	  case QVP_RTP_PER_REQUEST:
	  	{
			break; //no stream id in per request
	  	}
	 default:
	   {
	        QVP_RTP_MSG_HIGH_0( "Unsupported app command in function qvp_app_process_app_cmd \r\n");

	        break;
	   }
	 } /* end of switch msg->msg_type */
	return;
 }

int qvp_rtp_is_qmi_request(qvp_rtp_msg_type *msg)
{
  int ret_val =0;
  if(!msg)
  {
    QVP_RTP_ERR_0("qvp_rtp_is_qmi_request msgs null");
    return FALSE;
  }

  QVP_RTP_MSG_LOW_1("qvp_rtp_is_qmi_request entered with msg = %d",msg->msg_type);
  
  if(msg->msg_type == QVP_RTP_INIT_CMD || msg->msg_type == QVP_RTP_UPDATE_REGISTER_CB_CMD || msg->msg_type == QVP_RTP_REGISTER_CMD || msg->msg_type == QVP_RTP_REGISTER2_CMD || msg->msg_type == QVP_RTP_DEREGISTER_CMD || msg->msg_type == QVP_RTP_SHUTDOWN_CMD || msg->msg_type == QVP_RTP_MEDIA_INIT_CMD || msg->msg_type == QVP_RTP_MEDIA_DEINIT_CMD || msg->msg_type == QVP_RTP_MEDIA_CODEC_RELEASE_CMD || msg->msg_type == QVP_RTP_MEDIA_GENERATE_NAL_CMD)
    return TRUE;

  if(msg->msg_type == QVP_RTP_SET_HANDOFF_STATUS)
    return 2;

  if(msg->msg_type == QVP_RTP_OPEN_CMD)
  {
    QVP_RTP_MSG_LOW_1("qvp_rtp_is_qmi_request: rtp open called with payload %d",msg->msg_param.open_msg.stream_params.payload_type);
    ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.open2_msg.app_id, NULL, msg);
    if(ret_val != 0)
    {
      QVP_RTP_ERR_0("qvp_rtp_is_qmi_request QVP_RTP_OPEN2_CMD failed................");
	  return QVP_RTP_CTX_FULL;
    }    
    if((msg->msg_param.open_msg.stream_params.payload_type == QVP_RTP_PYLD_H264) || (msg->msg_param.open_msg.stream_params.payload_type == QVP_RTP_PYLD_H263) || (msg->msg_param.open_msg.stream_params.payload_type == QVP_RTP_PYLD_RAW_VID))
    {
      return TRUE;
    }
  }
  /* this is for video capability message and it will definately be QMI request*/
  if(msg->msg_type == QVP_RTP_MEDIA_CAPABILITY_CMD)
  {
    QVP_RTP_MSG_LOW_1("qvp_rtp_is_qmi_request: rtp media capability called for stream id %d",msg->msg_param.media_capability_msg.stream_id);
    return TRUE;
  }

  if(msg->msg_type == QVP_RTP_OPEN2_CMD)
  {
    QVP_RTP_MSG_HIGH_2("qvp_rtp_is_qmi_request: rtp open called with payload %d rat_type:%d",msg->msg_param.open2_msg.open_config_param.stream_params.payload_type,msg->msg_param.open2_msg.open_config_param.stream_params.rat_type);
    ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.open2_msg.app_id, NULL, msg);
    if(ret_val != 0)
    {
      QVP_RTP_ERR_0("qvp_rtp_is_qmi_request QVP_RTP_OPEN2_CMD failed................");
	  return QVP_RTP_CTX_FULL;
    }

    if((msg->msg_param.open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_H264) || (msg->msg_param.open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_H263) || (msg->msg_param.open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_RAW_VID))
    {
      return TRUE;
    }    
	else if(((msg->msg_param.open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_EVS) || msg->msg_param.open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_AMR) || (msg->msg_param.open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_AMR_WB) || (msg->msg_param.open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_RAW_AUD))
	{
		if( ((msg->msg_param.open2_msg.open_config_param.stream_params.rat_type == QVP_RTP_RAT_WLAN) || (msg->msg_param.open2_msg.open_config_param.stream_params.rat_type == QVP_RTP_RAT_IWLAN) ) && (g_audio_offload != 2) && (g_audio_offload != 3))
		{
			//RAT is wlan and offload is enable i.e. value 2:no offload OR value 3:offload to modem3 is not set
			QVP_RTP_ERR_1("qvp_rtp_is_qmi_request QVP_RTP_OPEN2_CMD g_audio_offload:%d.. ret:true",g_audio_offload);
			return TRUE;
		}
		else
		{
			QVP_RTP_ERR_1("qvp_rtp_is_qmi_request QVP_RTP_OPEN2_CMD g_audio_offload:%d.. ret: false",g_audio_offload);
			return FALSE;
		}
	}	
	else
	{
		QVP_RTP_ERR_1("qvp_rtp_is_qmi_request QVP_RTP_OPEN2_CMD failed. unsupported payload type:%d..........",msg->msg_param.open_msg.stream_params.payload_type);
	}  
  }
  if(msg->msg_type == QVP_RTP_GENERATE_INTRA_CODEC_FRAME_CMD || msg->msg_type == QVP_RTP_CONFIGURE_GBR_MBR_CMD)
  {
    return TRUE;
  }

  if(msg->msg_type == QVP_RTP_UNINITIALIZE_ALL_RTP_SESSION_CMD)
  {
  	// clear the pending ref count
  	qvp_rtp_clear_pending_ref_cnt( msg->msg_param.media_uninitialize_all_msg.app_id, msg->msg_param.media_uninitialize_all_msg.media_type);
	
     if(	msg->msg_param.media_uninitialize_all_msg.media_type == QVP_RTP_VIDEO || 
	 		(
	 			msg->msg_param.media_uninitialize_all_msg.media_type == QVP_RTP_AUDIO && 
	 			(
	 				msg->msg_param.media_uninitialize_all_msg.rat_type == QVP_RTP_RAT_WLAN ||
	 				msg->msg_param.media_uninitialize_all_msg.rat_type == QVP_RTP_RAT_IWLAN
	 			)
	 		)
	 	)
     	{
	 		return TRUE;
     	}
   }
  if(msg->msg_type != QVP_RTP_OPEN2_CMD && msg->msg_type != QVP_RTP_OPEN_CMD && msg->msg_type != QVP_RTP_INIT_CMD && msg->msg_type != QVP_RTP_UPDATE_REGISTER_CB_CMD && msg->msg_type != QVP_RTP_REGISTER_CMD && msg->msg_type != QVP_RTP_REGISTER2_CMD)
  {
    qvp_rtp_stream_id_type stream_id = 0;
	qvp_rtp_app_id_type	   app_id = 0;
    switch ( msg->msg_type )
    {
    case QVP_RTP_CONFIG_CMD :
      {
        stream_id = msg->msg_param.config_msg.stream_id;
			app_id 	  = msg->msg_param.config_msg.app_id;
	        ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.config_msg.app_id, stream_id, msg);

        break;
      }      
    case QVP_RTP_CONFIG_SESSION_CMD :
      {
        stream_id = msg->msg_param.config_session_msg.stream_id;
			app_id	  = msg->msg_param.config_session_msg.app_id;
	        ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.config_session_msg.app_id, stream_id, msg);

        break;
      }	
    case QVP_RTP_RESET_CMD :
      {
        stream_id = msg->msg_param.reset_msg.stream_id;
			app_id = msg->msg_param.reset_msg.app_id;
        break;
      }
    case  QVP_RTP_CLOSE_CMD :
      {
        stream_id = msg->msg_param.close_msg.stream_id;
			app_id	  = msg->msg_param.close_msg.app_id;
        break;
      }
    case QVP_RTP_CODEC_INIT_CMD:
      {
        stream_id = msg->msg_param.media_codec_init_msg.stream_id;
			app_id = msg->msg_param.media_codec_init_msg.app_id;
	        ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.media_codec_init_msg.app_id, stream_id, msg);

        break;
      }
    case QVP_RTP_MEDIA_CODEC_CONFIG_CMD:
      {
        stream_id = msg->msg_param.media_codec_config_msg.stream_id;
			app_id = msg->msg_param.media_codec_config_msg.app_id;
	        ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.media_codec_init_msg.app_id, stream_id, msg);

        break;
      }
    case QVP_RTP_START_STREAM_CMD:
      {
        stream_id = msg->msg_param.media_start_stream_msg.stream_id;
			app_id   = msg->msg_param.media_start_stream_msg.app_id;
	        ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.media_start_stream_msg.app_id, stream_id, msg);			
        break;
      }
    case QVP_RTP_STOP_STREAM_CMD:
      {
	  	int index = 0;
		qvp_rtp_usr_ctx *usr_ctx; 
        stream_id = msg->msg_param.media_stop_stream_msg.stream_id;
		app_id = msg->msg_param.media_stop_stream_msg.app_id;
		for(;index <QVP_RTP_MAX_USR_PVT_COUNT;index++)
		{
			if(msg->msg_param.media_stop_stream_msg.stream_id == rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].external_stream_id)
			{
				if(rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].is_modem_stream_id_valid)
				{
					msg->msg_param.media_stop_stream_msg.stream_id = rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].modem_stream_id;
					
					if(( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
						return -1;

					usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ]; 
					if(g_handoffstate != HO_IN_PROGRESS )
					usr_ctx->usr_ctx_pvt[index].pending_ind_ref_cnt += QVP_RTP_AUDIO_IND_CNT; 
					QVP_RTP_ERR("qvp_rtp_is_qmi_request stop: pending_ind_ref_cnt:%d external stream id:%d app stream id:%d",usr_ctx->usr_ctx_pvt[index].pending_ind_ref_cnt,stream_id,msg->msg_param.media_stop_stream_msg.stream_id);					
					return 0;
				}
				else if(rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].is_apps_stream_id_valid)
				{
					msg->msg_param.media_stop_stream_msg.stream_id = rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].apps_stream_id;

					if(( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
						return -1;

					usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ]; 
					if(qvp_rtp_find_media_type_from_pvt_ctx( msg->msg_param.media_stop_stream_msg.stream_id, msg->msg_param.media_stop_stream_msg.app_id, FALSE, FALSE) == QVP_RTP_AUDIO)
					{
						if(g_handoffstate != HO_IN_PROGRESS )
					   usr_ctx->usr_ctx_pvt[index].pending_ind_ref_cnt += QVP_RTP_AUDIO_IND_CNT; 
					}
					else
					{
					   if(msg->msg_param.media_stop_stream_msg.direction == QVP_RTP_DIR_TXRX)
						   usr_ctx->usr_ctx_pvt[index].pending_ind_ref_cnt += QVP_RTP_VIDEO_IND_CNT; 
					   else if( (msg->msg_param.media_stop_stream_msg.direction == QVP_RTP_DIR_TX) || (msg->msg_param.media_stop_stream_msg.direction == QVP_RTP_DIR_RX) )
						   usr_ctx->usr_ctx_pvt[index].pending_ind_ref_cnt += QVP_RTP_VIDEO_TX_IND_CNT; 					}

					QVP_RTP_ERR("qvp_rtp_is_qmi_request stop: pending_ind_ref_cnt:%d external stream id:%d app stream id:%d",usr_ctx->usr_ctx_pvt[index].pending_ind_ref_cnt,stream_id,msg->msg_param.media_stop_stream_msg.stream_id);
					
					return 1;
				}
				break;
			}
		}		
		QVP_RTP_ERR_2("qvp_rtp_is_qmi_request stop: stream id:%d app id:%d",stream_id,app_id);
        break;
      }
    case QVP_RTP_RESUME_STREAM_CMD:
      {
        stream_id = msg->msg_param.media_resume_stream_msg.stream_id;
		app_id = msg->msg_param.media_resume_stream_msg.app_id;
		ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.media_resume_stream_msg.app_id, stream_id, msg);

        break;
      }
    case QVP_RTP_PAUSE_STREAM_CMD:
      {
        stream_id = msg->msg_param.media_pause_stream_msg.stream_id;
			app_id = msg->msg_param.media_pause_stream_msg.app_id;		
		ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.media_pause_stream_msg.app_id, stream_id, msg);

        break;
      }
    case QVP_RTP_LINK_ALIVE_TIMER_CMD:
      {
        stream_id = msg->msg_param.rtp_link_alive_timer_msg.stream_id;
			app_id = msg->msg_param.rtp_link_alive_timer_msg.app_id;		
	        ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.rtp_link_alive_timer_msg.app_id, stream_id, msg);

        break;
      }
    case QVP_RTCP_LINK_ALIVE_TIMER_CMD:
      {
        stream_id = msg->msg_param.rtcp_link_alive_timer_msg.stream_id;
			app_id = msg->msg_param.rtcp_link_alive_timer_msg.app_id;		
	        ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.rtcp_link_alive_timer_msg.app_id, stream_id, msg);

        break;
      }
  case QVP_START_KEEP_ALIVE_TIMER:
   {
      stream_id = msg->msg_param.start_keep_alive_timer.stream_id;
	  app_id = msg->msg_param.start_keep_alive_timer.app_id;	
        ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.start_keep_alive_timer.app_id, stream_id, msg);

      break;
    }
  case QVP_STOP_KEEP_ALIVE_TIMER:
    {
      stream_id = msg->msg_param.stop_keep_alive_timer.stream_id;
		app_id = msg->msg_param.stop_keep_alive_timer.app_id;	 
		ret_val = qvp_rtp_usr_ctx_pvt_update_config(msg->msg_param.stop_keep_alive_timer.app_id, stream_id, msg);
 
      break;
    }	  
    case  QVP_RTP_SEND_DTMF_CMD :
      {
		stream_id = msg->msg_param.send_msg.stream_id;
		app_id = msg->msg_param.send_msg.app_id;
        break;
      }	  
	case QVP_RTP_PER_REQUEST :
	  {
	  	app_id = msg->msg_param.per_req_msg_type.app_id;
		return 0; // return because this request is independednt of stream id and it is calculated based on pvt context stored in modem
	  }
	break;
    default:
      {
        QVP_RTP_MSG_HIGH_0( "Unsupported app command in function qvp_app_process_app_cmd \r\n");
         return 0;
      }
      QVP_RTP_MSG_LOW_2("qvp_rtp_is_qmi_request: called with msg = %d stream id %d",msg->msg_type,stream_id);
    } /* end of switch msg->msg_type */

    if(ret_val != 0)
	  {
      QVP_RTP_ERR_1("qvp_rtp_is_qmi_request : %d failed",msg->msg_type);
	  }
    QVP_RTP_ERR_2("qvp_rtp_is_qmi_request: stream id:%d app id:%d",stream_id,app_id);
    ret_val = qvp_rtp_return_stream_id_type(stream_id, app_id);

	if(ret_val == -1)
	{
	  	QVP_RTP_ERR("qvp_rtp_is_qmi_request: Unable to locate stream id in context. Rasing error",0,0,0);
		qvp_rtp_response_handler(app_id, stream_id, NULL,
			     QVP_RTP_MEDIA_ERROR_RSP, 0, TRUE, QVP_RTP_DIR_TXRX); 
	}	
	return ret_val;
  }

  return FALSE;
}

void qvp_rtp_process_qmi_request(qvp_rtp_msg_type *msg)
{
  if(!msg)
  {
    QVP_RTP_ERR_0("qvp_rtp_process_qmi_request msgs null");
    return;
  }

  QVP_RTP_MSG_LOW_1("qvp_rtp_process_qmi_request entered with msg = %d",msg->msg_type);
  switch(msg->msg_type)
  {
  case  QVP_RTP_REGISTER_CMD :
    {
      qvp_rtp_service_register_proxy_req_msg(&msg->msg_param.register_msg.call_backs);
      break;
    }
  case  QVP_RTP_REGISTER2_CMD :
    {
      qvp_rtp_service_register_proxy_req_msg(&msg->msg_param.register2_msg.call_backs);
      break;
    }
  case  QVP_RTP_OPEN_CMD :
    {
      qvp_rtp_service_open_req_msg( msg->msg_param.open_msg.app_id, &msg->msg_param.open_msg.stream_params );
      break;
    }
  case  QVP_RTP_OPEN2_CMD :
    {
      qvp_rtp_service_open2_req_msg( msg->msg_param.open2_msg.app_id, &msg->msg_param.open2_msg.open_config_param.stream_params );
      break;
    }
  case QVP_RTP_CONFIG_CMD :
    {
      qvp_rtp_service_configure_req_msg( msg->msg_param.config_msg.app_id, msg->msg_param.config_msg.stream_id, &msg->msg_param.config_msg.config );
      break;
    }      
  case QVP_RTP_CONFIG_SESSION_CMD:
    {
      qvp_rtp_service_configure_session_cmd(msg->msg_param.config_session_msg.app_id, msg->msg_param.config_session_msg.stream_id, msg->msg_param.config_session_msg.config.rtcp_report_interval);
    }
    break;
  case  QVP_RTP_CLOSE_CMD :
    {
      qvp_rtp_service_close_req_msg( msg->msg_param.close_msg.app_id, msg->msg_param.close_msg.stream_id);
      break;
    }
  case  QVP_RTP_DEREGISTER_CMD :
    {
      qvp_rtp_service_deregister_app_req_msg( msg->msg_param.deregister_app_msg.app_id );
      break;
    }
  case QVP_RTP_SHUTDOWN_CMD :
    {
      //qvp_rtp_service_deregister_app_req_msg( msg->msg_param.deregister_app_msg.app_id );
      qvp_rtp_service_clear_callbacks();
		qvp_rtp_service_media_uninitialize_all_req
		(
			(qvp_rtp_app_id_type)0,
	  		QVP_RTP_VIDEO,
	  		QVP_RTP_RAT_LTE
	  	);	  
      break;
    }
  case QVP_RTP_MEDIA_INIT_CMD:
    {
      qvp_rtp_service_media_init_req_msg(msg->msg_param.media_init_msg.app_id, msg->msg_param.media_init_msg.media_type, msg->msg_param.media_init_msg.pfnotify);
      break;
    }
  case QVP_RTP_MEDIA_DEINIT_CMD:
    {
      qvp_rtp_service_media_deinit_req_msg(msg->msg_param.media_deinit_msg.app_id, msg->msg_param.media_deinit_msg.media_type);
      break;
    }
  case QVP_RTP_MEDIA_GENERATE_NAL_CMD:
    {
      qvp_rtp_service_media_generate_nal_req_msg(msg->msg_param.media_generate_nal_hdr_msg.app_id, 
        msg->msg_param.media_generate_nal_hdr_msg.width,
        msg->msg_param.media_generate_nal_hdr_msg.height,		
        msg->msg_param.media_generate_nal_hdr_msg.level,
        msg->msg_param.media_generate_nal_hdr_msg.profile,
        msg->msg_param.media_generate_nal_hdr_msg.puserdata);
      break;
    }
  case QVP_RTP_CODEC_INIT_CMD:
    {
      qvp_rtp_service_media_codec_init_req_msg( msg->msg_param.media_codec_init_msg.app_id,  
        msg->msg_param.media_codec_init_msg.stream_id,
        msg->msg_param.media_codec_init_msg.direction,
        &msg->msg_param.media_codec_init_msg.media_config);
      break;
    }
  case QVP_RTP_MEDIA_CODEC_CONFIG_CMD:
    {
      qvp_rtp_service_media_codec_config_req_msg( msg->msg_param.media_codec_config_msg.app_id,
        msg->msg_param.media_codec_config_msg.stream_id,
        msg->msg_param.media_codec_config_msg.direction,
        &msg->msg_param.media_codec_config_msg.media_config);
      break;
    }
  case QVP_RTP_MEDIA_CAPABILITY_CMD:
   {
      qvp_rtp_service_media_capability_req_msg( msg->msg_param.media_capability_msg.app_id,
        msg->msg_param.media_capability_msg.stream_id,
        &msg->msg_param.media_capability_msg.video_capability);
      break;
   }
  case QVP_RTP_MEDIA_CODEC_RELEASE_CMD:
    {
    if(msg->msg_param.media_codec_release_msg.media_type == QVP_RTP_VIDEO)
    {
      qvp_rtp_service_media_codec_release_req_msg( msg->msg_param.media_codec_release_msg.app_id,
        msg->msg_param.media_codec_release_msg.media_type, TRUE);
    }
      break;
    }
  case QVP_RTP_START_STREAM_CMD:
    {
      qvp_rtp_service_media_start_stream_req_msg( msg->msg_param.media_start_stream_msg.app_id, 
        msg->msg_param.media_start_stream_msg.stream_id,
        msg->msg_param.media_start_stream_msg.direction);
      break;
    }
  case QVP_RTP_STOP_STREAM_CMD:
    {
      qvp_rtp_service_media_stop_stream_req_msg( msg->msg_param.media_stop_stream_msg.app_id,
        msg->msg_param.media_stop_stream_msg.stream_id,
        msg->msg_param.media_stop_stream_msg.direction);
      break;
    }
  case QVP_RTP_RESUME_STREAM_CMD:
    {
      qvp_rtp_service_media_resume_stream_req_msg( msg->msg_param.media_resume_stream_msg.app_id,
        msg->msg_param.media_resume_stream_msg.stream_id,
        msg->msg_param.media_resume_stream_msg.direction);
      break;
    }
  case QVP_RTP_PAUSE_STREAM_CMD:
    {
      qvp_rtp_service_media_pause_stream_req_msg( msg->msg_param.media_pause_stream_msg.app_id,
        msg->msg_param.media_pause_stream_msg.stream_id,
        msg->msg_param.media_pause_stream_msg.direction);
      break;
    }
  case QVP_RTCP_LINK_ALIVE_TIMER_CMD:
    {
      qvp_rtp_service_rtcp_link_alive_timer_req_msg( msg->msg_param.rtcp_link_alive_timer_msg.app_id, 
        msg->msg_param.rtcp_link_alive_timer_msg.stream_id,
        msg->msg_param.rtcp_link_alive_timer_msg.rtcp_link_alive_time);
      break;
    }
  case QVP_RTP_LINK_ALIVE_TIMER_CMD:
    {
      qvp_rtp_service_rtp_link_alive_timer_req_msg( msg->msg_param.rtp_link_alive_timer_msg.app_id, 
        msg->msg_param.rtp_link_alive_timer_msg.stream_id,
        msg->msg_param.rtp_link_alive_timer_msg.rtp_link_alive_time);	
	  break;
	}
  case QVP_RTP_GENERATE_INTRA_CODEC_FRAME_CMD:
  {
     qvp_rtp_service_generate_intra_codec_frame(msg->msg_param.rtp_generate_intra_frame_msg_type.app_id, msg->msg_param.rtp_generate_intra_frame_msg_type.stream_id);
  }
  break;
  case QVP_RTP_CONFIGURE_GBR_MBR_CMD:
  {
    qvp_rtp_service_configure_gbr_mbr(
	  msg->msg_param.rtp_configure_gbr_mbr_msg_type.app_id,
	  msg->msg_param.rtp_configure_gbr_mbr_msg_type.stream_id,
	  msg->msg_param.rtp_configure_gbr_mbr_msg_type.tx_gbr,
	  msg->msg_param.rtp_configure_gbr_mbr_msg_type.tx_mbr,
	  msg->msg_param.rtp_configure_gbr_mbr_msg_type.rx_gbr,
	  msg->msg_param.rtp_configure_gbr_mbr_msg_type.rx_mbr);
  }
  break;
  case QVP_START_KEEP_ALIVE_TIMER:
   {
      qvp_rtp_service_start_keep_alive_timer_cmd(
	  msg->msg_param.start_keep_alive_timer.app_id,
	  msg->msg_param.start_keep_alive_timer.stream_id,
	  msg->msg_param.start_keep_alive_timer.timer_type);
      break;
    }
  case QVP_STOP_KEEP_ALIVE_TIMER:
    {
      qvp_rtp_service_stop_keep_alive_timer_cmd(
	  msg->msg_param.stop_keep_alive_timer.app_id,
	  msg->msg_param.stop_keep_alive_timer.stream_id,
	  msg->msg_param.stop_keep_alive_timer.timer_type);
      break;
    }	  	
  case QVP_RTP_UNINITIALIZE_ALL_RTP_SESSION_CMD:
  	{
		qvp_rtp_service_media_uninitialize_all_req
		(
			msg->msg_param.media_uninitialize_all_msg.app_id,
	  		msg->msg_param.media_uninitialize_all_msg.media_type,
	  		msg->msg_param.media_uninitialize_all_msg.rat_type
	  	);
  	}
    break; 
    case  QVP_RTP_SEND_DTMF_CMD :
      {
        qvp_rtp_service_send_dtmf_req( msg->msg_param.send_msg.app_id, msg->msg_param.send_msg.stream_id, msg->msg_param.send_msg.pkt);
        break;
      }	
  default:
    {
      QVP_RTP_MSG_HIGH_0( "Unsupported app command in function qvp_app_process_app_cmd \r\n");
      break;
    }
  } /* end of switch msg->msg_type */
}

void qvp_rtp_log_rx_rtp_pkt(qvp_rtp_stream_type *stream, uint8 direction, qvp_rtp_buf_type    *pkt_rcvd, uint16  head_room)
{
  /* log incomming rtp data packet and important parameters*/
  //uint16  head_room=0;
  if(!stream || !pkt_rcvd)
  {
    QVP_RTP_ERR_0("qvp_rtp_log_rx_rtp_pkt failed");
    return;
  }
  if(stream->payload_type == QVP_RTP_PYLD_AMR || stream->payload_type == QVP_RTP_PYLD_G722_2 || stream->payload_type == QVP_RTP_PYLD_H264 || stream->payload_type == QVP_RTP_PYLD_H263 || stream->payload_type == QVP_RTP_PYLD_G711_U ||  stream->payload_type == QVP_RTP_PYLD_G711_A || stream->payload_type == QVP_RTP_PYLD_EVS )
  {

		qvp_rtp_log_pkt_v2 log_pak;
		uint16 rtp_logged_payload_size = pkt_rcvd->len;
		uint8 rat_type = LTE; /*Default RAT assumption is LTE*/

		memset( &log_pak , 0, sizeof( qvp_rtp_log_pkt_v2 ) );
		log_pak.version = 9; // version 9 for restructuring (EVS+G711 support)
		if(stream->rat_type == QVP_RTP_RAT_LTE)
			rat_type = LTE;
		else if ((stream->rat_type == QVP_RTP_RAT_WLAN) || (stream->rat_type == QVP_RTP_RAT_IWLAN) )
			rat_type = WLAN;
		
		log_pak.direction_and_RAT = (direction & 0x03) | ( (rat_type << 3) & 0xF8);
    log_pak.ssrc = stream->rtp.rx_ssrc;
		log_pak.rtp_seqno = (uint16)pkt_rcvd->seq;
		log_pak.rtp_tstamp = pkt_rcvd->tstamp;
		if(stream->rtp_log_pkt_ctx.is_dup==FALSE)
			log_pak.rtp_redundant_indicator=1;
		else
			log_pak.rtp_redundant_indicator=2;

		if((stream->payload_type == QVP_RTP_PYLD_AMR) ||(stream->payload_type == QVP_RTP_PYLD_G722_2)){
			log_pak.media_type = AUDIO;
			log_pak.payload_size = stream->cur_rx_pkt_len;
			if(stream->payload_type == QVP_RTP_PYLD_AMR )
			log_pak.codec_type = AMR;
			else if(stream->payload_type == QVP_RTP_PYLD_G722_2 )
			log_pak.codec_type = AMR_WB;
			log_pak.s_packet.amr_audio_packet.CMR =
				pkt_rcvd->frm_info.info.aud_info.profile_info.amr_info.amr_mode_request;
			log_pak.s_packet.amr_audio_packet.F =
				pkt_rcvd->frm_info.info.aud_info.profile_info.amr_info.F;
			log_pak.s_packet.amr_audio_packet.FT = 
				pkt_rcvd->frm_info.info.aud_info.profile_info.amr_info.FT;
			log_pak.s_packet.amr_audio_packet.Q = 
				pkt_rcvd->frm_info.info.aud_info.profile_info.amr_info.amr_fqi;
			log_pak.s_packet.amr_audio_packet.marker_m  = pkt_rcvd->marker_bit;
		}
		else if(stream->payload_type == QVP_RTP_PYLD_G711_U || stream->payload_type == QVP_RTP_PYLD_G711_A) {

			log_pak.media_type = AUDIO;
			log_pak.payload_size = stream->cur_rx_pkt_len;
			if(stream->payload_type == QVP_RTP_PYLD_G711_U )
			{
				log_pak.codec_type = G711_U;
				log_pak.s_packet.g711_audio_packet.CMR = 0;
			}
			else if(stream->payload_type == QVP_RTP_PYLD_G711_A )
			{
				log_pak.codec_type = G711_A;	
				log_pak.s_packet.g711_audio_packet.CMR = 1;
			}
			log_pak.s_packet.g711_audio_packet.F = 0; //last frame
			log_pak.s_packet.g711_audio_packet.FT = (g711_frame_mode)pkt_rcvd->frm_info.info.aud_info.profile_info.g711_info.g711_mode; // speech,SID,nodata
			log_pak.s_packet.g711_audio_packet.Q = 1;
			log_pak.s_packet.g711_audio_packet.marker_m = pkt_rcvd->marker_bit;
		}
		else if(stream->payload_type == QVP_RTP_PYLD_EVS) {

			log_pak.media_type = AUDIO;
			log_pak.payload_size = stream->cur_rx_pkt_len;
			log_pak.codec_type = EVS;
			log_pak.s_packet.evs_audio_packet.CMR = pkt_rcvd->frm_info.info.aud_info.profile_info.evs_info.evs_mode_request;
			log_pak.s_packet.evs_audio_packet.F = pkt_rcvd->frm_info.info.aud_info.profile_info.evs_info.F;
			if(pkt_rcvd->frm_info.info.aud_info.profile_info.evs_info.is_primary)
				log_pak.s_packet.evs_audio_packet.FT_mode = EVS_Primary_mode; //primary mode
			else
				log_pak.s_packet.evs_audio_packet.FT_mode = EVS_AMR_WB_IO_modes; //IO mode

			if(log_pak.s_packet.evs_audio_packet.FT_mode == 0)
				log_pak.s_packet.evs_audio_packet.FT.evs_pri_mode = (evs_primary_mode)pkt_rcvd->frm_info.info.aud_info.profile_info.evs_info.FT;
			else
				log_pak.s_packet.evs_audio_packet.FT.evs_amrwb_io_mode = (evs_AMR_WB_IO_mode)pkt_rcvd->frm_info.info.aud_info.profile_info.evs_info.FT; // mode 8

			log_pak.s_packet.evs_audio_packet.Q = pkt_rcvd->frm_info.info.aud_info.profile_info.evs_info.q;
			log_pak.s_packet.evs_audio_packet.marker_m = pkt_rcvd->marker_bit;
		}
		//Set logged payload size to zero for Video till NV is obtained
		else if(stream->payload_type == QVP_RTP_PYLD_H264) {
			log_pak.media_type = VIDEO;
			log_pak.payload_size = stream->cur_rx_pkt_len;
			log_pak.codec_type = H264;
			rtp_logged_payload_size = 0;
		}

		//Set logged payload size to zero for Video till NV is obtained
		else if(stream->payload_type == QVP_RTP_PYLD_H263) {
			log_pak.media_type = VIDEO;
			log_pak.payload_size = stream->cur_rx_pkt_len;
			log_pak.codec_type = H263;
			rtp_logged_payload_size = 0;
		}

		if(g_gps_nv)
		{
			if( !pkt_rcvd->xtn_enabled )
			{
				uint64 	transmit_gps_time_ms = 0;
				uint64 	arrival_gps_time_ms = 0;

				log_pak.latency_info_present = 3;

				transmit_gps_time_ms = (pkt_rcvd->tstamp - stream->prev_iRxTimestampGps)/stream->clock_rate;
				transmit_gps_time_ms += stream->prev_iGpsTimeMS ;
				QVP_RTP_MSG_HIGH_2("[GPS_RX_XTN] prev_iRxTimestampGps %lu: prev_iGpsTimeMS %lu",stream->prev_iRxTimestampGps,stream->prev_iGpsTimeMS );

				//storing in the GPS TOD format 		
				// First 10 LSB bits represents time in milliseconds ( 0 - 999)
				log_pak.latency.latency_block_all.Ts	=	((transmit_gps_time_ms % (60*60*1000))%1000);
				QVP_RTP_MSG_HIGH_2("[pkt_transmit_time] %u:%x",log_pak.latency.latency_block_all.Ts,log_pak.latency.latency_block_all.Ts);
				// Next 12 LSB bits represents time in seconds (0-3599)
				log_pak.latency.latency_block_all.Ts	+=	 (((transmit_gps_time_ms % (60*60*1000))/1000) << 10);
				QVP_RTP_MSG_HIGH_2("[pkt_transmit_time] %u:%x",log_pak.latency.latency_block_all.Ts,log_pak.latency.latency_block_all.Ts);
				// Next 5 MSB bits represents hours (0-23)
				log_pak.latency.latency_block_all.Ts	+=	 ((transmit_gps_time_ms / (60*60*1000)) << 22);
				QVP_RTP_MSG_HIGH_2("[pkt_transmit_time] %u:%x",log_pak.latency.latency_block_all.Ts,log_pak.latency.latency_block_all.Ts);

				// Remaining 5 bits are 0

				// Finding the arrival time
				if(QVP_RTP_SUCCESS == qvp_rtp_read_gps_and_parse(pkt_rcvd))
				{
					QVP_RTP_MSG_HIGH_1("[GPS_RX_XTN] received xtn hdr pkt for seq = %d\n",pkt_rcvd->seq);
					QVP_RTP_MSG_HIGH("[GPS_RX_XTN] hh:ss:mmm %u:%u:%lu",pkt_rcvd->gps_time.gps_hh, pkt_rcvd->gps_time.gps_ss,pkt_rcvd->gps_time.gps_mmm);
				}
				else
				{
					QVP_RTP_MSG_HIGH_0("GPS PARSE IS NOT SUCCESS");
				}
				
				//storing in the GPS TOD format 		
				// First 10 LSB bits represents time in milliseconds ( 0 - 999)
				QVP_RTP_MSG_HIGH("[pkt_arrival_time]hh:ss:mmm %u:%u: %u", pkt_rcvd->gps_time.gps_hh, pkt_rcvd->gps_time.gps_ss, pkt_rcvd->gps_time.gps_mmm);
				log_pak.latency.latency_block_all.Ta	=	 pkt_rcvd->gps_time.gps_mmm;
				QVP_RTP_MSG_HIGH_2("[pkt_arrival_time] %u:%x",log_pak.latency.latency_block_all.Ta,log_pak.latency.latency_block_all.Ta);			
				// Next 12 LSB bits represents time in seconds (0-3599)
				log_pak.latency.latency_block_all.Ta	+=	 (pkt_rcvd->gps_time.gps_ss << 10);
				QVP_RTP_MSG_HIGH_2("[pkt_arrival_time] %u:%x",log_pak.latency.latency_block_all.Ta,log_pak.latency.latency_block_all.Ta);			
				// Next 5 MSB bits represents hours (0-23)
				log_pak.latency.latency_block_all.Ta	+=	 (pkt_rcvd->gps_time.gps_hh << 22);
				QVP_RTP_MSG_HIGH_2("[pkt_arrival_time] %u:%x",log_pak.latency.latency_block_all.Ta,log_pak.latency.latency_block_all.Ta);			
				// Remaining 5 bits are 0
				
				arrival_gps_time_ms = pkt_rcvd->gps_time.gps_mmm + (pkt_rcvd->gps_time.gps_ss * 1000) + (pkt_rcvd->gps_time.gps_hh * 60 * 60 * 1000);
				
				log_pak.latency.latency_block_all.To = arrival_gps_time_ms - transmit_gps_time_ms;
				
				//storing in the GPS TOD format 		
				// First 10 LSB bits represents time in milliseconds ( 0 - 999)
				log_pak.latency.latency_block_all.To	=	((log_pak.latency.latency_block_all.To % (60*60*1000))%1000);
				// Next 12 LSB bits represents time in seconds (0-3599)
				log_pak.latency.latency_block_all.To	+=	 (((log_pak.latency.latency_block_all.To % (60*60*1000))/1000) << 10);
				// Next 5 MSB bits represents hours (0-23)
				log_pak.latency.latency_block_all.To	+=	 ((log_pak.latency.latency_block_all.To / (60*60*1000)) << 22);
				// Remaining 5 bits are 0
				
				QVP_RTP_MSG_HIGH("[GPS_RX_XTN] arrival_gps_time_ms %lu: transmit_gps_time_ms %lu latency = %lu",arrival_gps_time_ms,transmit_gps_time_ms ,log_pak.latency.latency_block_all.To);			
			
			}
			else
			{
				uint64 	transmit_gps_time_ms = 0;
				uint64 	arrival_gps_time_ms = 0;
				log_pak.latency_info_present = 4;
				QVP_RTP_MSG_HIGH("[pkt_transmit_time]hh:ss:mmm %u:%u: %u", pkt_rcvd->gps_time.gps_hh, pkt_rcvd->gps_time.gps_ss, pkt_rcvd->gps_time.gps_mmm);
				//storing in the GPS TOD format
				// First 10 LSB bits represents time in milliseconds ( 0 - 999)
				log_pak.latency.latency_block_all.Ts	=	 pkt_rcvd->gps_time.gps_mmm;
				QVP_RTP_MSG_HIGH_2("[pkt_transmit_time] %u:%x",log_pak.latency.latency_block_all.Ts,log_pak.latency.latency_block_all.Ts);
				// Next 12 LSB bits represents time in seconds (0-3599)
				log_pak.latency.latency_block_all.Ts	+=	 (pkt_rcvd->gps_time.gps_ss << 10);
				QVP_RTP_MSG_HIGH_2("[pkt_transmit_time] %u:%x",log_pak.latency.latency_block_all.Ts,log_pak.latency.latency_block_all.Ts);
				// Next 5 MSB bits represents hours (0-23)
				log_pak.latency.latency_block_all.Ts	+=	 (pkt_rcvd->gps_time.gps_hh << 22);
				QVP_RTP_MSG_HIGH_2("[pkt_transmit_time] %u:%x",log_pak.latency.latency_block_all.Ts,log_pak.latency.latency_block_all.Ts);
				// Remaining 5 bits are 0

				transmit_gps_time_ms = pkt_rcvd->gps_time.gps_mmm + (pkt_rcvd->gps_time.gps_ss * 1000) + (pkt_rcvd->gps_time.gps_hh * 60 * 60 * 1000);
				
				// Finding the arrival time
				if(QVP_RTP_SUCCESS == qvp_rtp_read_gps_and_parse(pkt_rcvd))
				{
					QVP_RTP_MSG_HIGH_1("[GPS_RX_XTN] received xtn hdr pkt = %d\n",pkt_rcvd->seq);
					QVP_RTP_MSG_HIGH("[GPS_RX_XTN] hh:ss:mmm %u:%u:%lu",pkt_rcvd->gps_time.gps_hh,pkt_rcvd->gps_time.gps_ss,pkt_rcvd->gps_time.gps_mmm);
				}
				else
				{
					QVP_RTP_MSG_HIGH_0("GPS PARSE IS NOT SUCCESS");
				}
				QVP_RTP_MSG_HIGH("[pkt_arrival_time]hh:ss:mmm %u:%u: %u", pkt_rcvd->gps_time.gps_hh, pkt_rcvd->gps_time.gps_ss, pkt_rcvd->gps_time.gps_mmm);
				
				//storing in the GPS TOD format			
				// First 10 LSB bits represents time in milliseconds ( 0 - 999)
				log_pak.latency.latency_block_all.Ta	=	 pkt_rcvd->gps_time.gps_mmm;
				QVP_RTP_MSG_HIGH_2("[pkt_arrival_time] %u:%x",log_pak.latency.latency_block_all.Ta,log_pak.latency.latency_block_all.Ta);			
				// Next 12 LSB bits represents time in seconds (0-3599)
				log_pak.latency.latency_block_all.Ta	+=	 (pkt_rcvd->gps_time.gps_ss << 10);
				QVP_RTP_MSG_HIGH_2("[pkt_arrival_time] %u:%x",log_pak.latency.latency_block_all.Ta,log_pak.latency.latency_block_all.Ta);			
				// Next 5 MSB bits represents hours (0-23)
				log_pak.latency.latency_block_all.Ta	+=	 (pkt_rcvd->gps_time.gps_hh << 22);
				QVP_RTP_MSG_HIGH_2("[pkt_arrival_time] %u:%x",log_pak.latency.latency_block_all.Ta,log_pak.latency.latency_block_all.Ta);			
				// Remaining 5 bits are 0

				arrival_gps_time_ms = pkt_rcvd->gps_time.gps_mmm + (pkt_rcvd->gps_time.gps_ss * 1000) + (pkt_rcvd->gps_time.gps_hh * 60 * 60 * 1000);

				log_pak.latency.latency_block_all.To = arrival_gps_time_ms - transmit_gps_time_ms;
				//Storing the GPS time for latency case 3
				stream->prev_iRxTimestampGps = pkt_rcvd->tstamp;
				stream->prev_iGpsTimeMS = transmit_gps_time_ms;
				QVP_RTP_MSG_HIGH("[GPS_RX_XTN] arrival_gps_time_ms %lu: transmit_gps_time_ms %lu latency = %lu",arrival_gps_time_ms,transmit_gps_time_ms ,log_pak.latency.latency_block_all.To);					

				
			}
		}
		else
		{
			log_pak.latency_info_present = 0;
		}

		QVP_RTP_ERR_1(" HandleNWpkt Log RTP payload type : %d\r\n", stream->payload_type);
        //head_room = (stream->cur_rx_pkt_len - pkt_rcvd->len);
		qvp_rtp_log_packets_v2( &log_pak,(void *) (pkt_rcvd->data + head_room  + RTP_HEADER_LEN + pkt_rcvd->xtn_hdr_len) , 
			rtp_logged_payload_size);


  }
}


/*===========================================================================

FUNCTION QVP_RTP_LOG_COMMIT_VOICE_CALL_STAT

DESCRIPTION
Log the voice call statistics parameters for the given session

DEPENDENCIES
None

PARAMETERS
None

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_log_commit_voice_call_stat( qvp_rtp_stream_type *stream )
{

	qvp_rtp_voice_call_stat_log_pkt			voice_call_stat_log_pkt;
	uint64									duration = 0;
	uint32									tx_byte_cnt = 0;
        uint32                                               rx_byte_cnt = 0;
	uint32									tx_rtp_bit_rate = 0;
	uint32									rx_rtp_bit_rate = 0;
	uint8									size = sizeof(qvp_rtp_voice_call_stat_log_pkt);
	uint32   max_Delta = 0;
	uint16   max_seq = 0;
	uint16   min_seq = 0;

	if(!stream)
	{
		QVP_RTP_ERR_0(" commit_voice_call_stat: Invalid stream \r\n");
		return;
	}
    memset( &voice_call_stat_log_pkt , 0, sizeof( qvp_rtp_voice_call_stat_log_pkt ) );

	//if(stream->stream_state == ACTIVE && stream->T_last)
		{

		   qvp_rtp_time_get_ms( &stream->time_session_end );
		QVP_RTP_MSG_LOW_1("commit_voice_call_stat time_session_end = %lu ",stream->time_session_end);
		   duration = stream->time_session_end - stream->time_session_start;
		   voice_call_stat_log_pkt.sip_call_duration = duration /1000;

		voice_call_stat_log_pkt.version = 5;

		   /* Get the params from RTCP layer */
		   if(stream->rtcp_hdl )
			   qvp_rtcp_get_voice_call_params( stream->rtcp_hdl, &voice_call_stat_log_pkt.rtcp_voice_call_stat_params, 
			   &tx_byte_cnt, &rx_byte_cnt, &max_Delta, &max_seq, &min_seq);

         voice_call_stat_log_pkt.tx_byte_cnt = tx_byte_cnt;
         voice_call_stat_log_pkt.rx_byte_cnt = rx_byte_cnt;
         voice_call_stat_log_pkt.max_delta = max_Delta;
         voice_call_stat_log_pkt.Imax_seq = max_seq;
         voice_call_stat_log_pkt.Imin_seq = min_seq;
		QVP_RTP_MSG_LOW("commit_voice_call_stat Max_delta = %u, Imax_seq = %d and Imin_seq=%d ",voice_call_stat_log_pkt.max_delta,voice_call_stat_log_pkt.Imax_seq,voice_call_stat_log_pkt.Imin_seq);
		QVP_RTP_MSG_HIGH("commit_voice_call_stat avg_IAJ = %u, max IAJ = %u and rx_loss_pkt_cnt=%u ",voice_call_stat_log_pkt.rtcp_voice_call_stat_params.avg_inter_arrival_jitter,voice_call_stat_log_pkt.rtcp_voice_call_stat_params.max_inter_arrival_jitter,voice_call_stat_log_pkt.rtcp_voice_call_stat_params.rx_lost_pkt_cnt);
		QVP_RTP_MSG_LOW_2("commit_voice_call_stat  rx_ssrc = %u and tx_ssrc=%u ",voice_call_stat_log_pkt.rtcp_voice_call_stat_params.rx_ssrc,voice_call_stat_log_pkt.rtcp_voice_call_stat_params.tx_ssrc);
		QVP_RTP_MSG_HIGH_2("commit_voice_call_stat tx_pkt_cnt=%u and rx_pkt_cnt = %u,",voice_call_stat_log_pkt.rtcp_voice_call_stat_params.tx_pkt_cnt,voice_call_stat_log_pkt.rtcp_voice_call_stat_params.rx_pkt_cnt);
		QVP_RTP_MSG_HIGH_1("commit_voice_call_stat cummulative_instantaneous_jitter=%u ",voice_call_stat_log_pkt.rtcp_voice_call_stat_params.cummulative_instantaneous_jitter);
		   voice_call_stat_log_pkt.rtcp_voice_call_stat_params.avg_inter_arrival_jitter /= 1000;
		   voice_call_stat_log_pkt.rtcp_voice_call_stat_params.max_inter_arrival_jitter /= 1000;

		   /* Calculating avg_instantaneous_jitter*/
		if(voice_call_stat_log_pkt.rtcp_voice_call_stat_params.rx_pkt_cnt)
		   voice_call_stat_log_pkt.rtcp_voice_call_stat_params.cummulative_instantaneous_jitter /= voice_call_stat_log_pkt.rtcp_voice_call_stat_params.rx_pkt_cnt;

		QVP_RTP_MSG_HIGH_1("commit_voice_call_stat avg_instantaneous_jitter=%u ",voice_call_stat_log_pkt.rtcp_voice_call_stat_params.cummulative_instantaneous_jitter);
		
		   /* Get the params from QDJ layer */
		   qvp_rtp_get_voice_call_params_from_QDJ(&voice_call_stat_log_pkt.QDJ_voice_call_stat_params, &voice_call_stat_log_pkt.codec_type);
		QVP_RTP_MSG_HIGH("commit_voice_call_stat avg_frame_dealy=%u avg qsize = %u and avg_target delay = %u ",voice_call_stat_log_pkt.QDJ_voice_call_stat_params.avg_frame_delay,voice_call_stat_log_pkt.QDJ_voice_call_stat_params.avg_q_size,voice_call_stat_log_pkt.QDJ_voice_call_stat_params.avg_target_delay);
		QVP_RTP_MSG_LOW("commit_voice_call_stat underflow rate = %u frames_not_enqueued = %u and frame_underflow_cnt= %d", voice_call_stat_log_pkt.QDJ_voice_call_stat_params.avg_underflow_rate, voice_call_stat_log_pkt.QDJ_voice_call_stat_params.frames_not_enq_cnt, voice_call_stat_log_pkt.QDJ_voice_call_stat_params.frames_underflow_cnt);
		QVP_RTP_MSG_LOW("commit_voice_call_stat max_frame_delay=%u rx_frame_cnt =%u and codec type = %d",voice_call_stat_log_pkt.QDJ_voice_call_stat_params.max_frame_delay,voice_call_stat_log_pkt.QDJ_voice_call_stat_params.rx_frames_cnt,voice_call_stat_log_pkt.codec_type);
   		
		   if(voice_call_stat_log_pkt.sip_call_duration)
		   {
		   QVP_RTP_MSG_LOW("commit_voice_call_stat tx_byte_cnt = %u and rx_byte_cnt=%u and sip_call_duration =%d",tx_byte_cnt,rx_byte_cnt,voice_call_stat_log_pkt.sip_call_duration);
			   tx_rtp_bit_rate = ( tx_byte_cnt * 8 ) / (voice_call_stat_log_pkt.sip_call_duration ); //bps
			   rx_rtp_bit_rate = ( rx_byte_cnt * 8 ) / (voice_call_stat_log_pkt.sip_call_duration ); //bps
		   QVP_RTP_MSG_LOW_2("commit_voice_call_stat tx_rtp_bit_rate = %u and rx_rtp_bit_rate=%u in bits per sec",tx_rtp_bit_rate,rx_rtp_bit_rate);
			   voice_call_stat_log_pkt.tx_rtp_bit_rate = tx_rtp_bit_rate / 1000; //kbps
			   voice_call_stat_log_pkt.rx_rtp_bit_rate = rx_rtp_bit_rate / 1000; //kbps
		   QVP_RTP_MSG_LOW_2("commit_voice_call_stat tx_rtp_bit_rate = %u and rx_rtp_bit_rate=%u in kilobits per sec",voice_call_stat_log_pkt.tx_rtp_bit_rate,voice_call_stat_log_pkt.rx_rtp_bit_rate);
		   }

		   voice_call_stat_log_pkt.Tx_PT = stream->rtp.tx_payload_type;
		   voice_call_stat_log_pkt.Rx_PT = stream->rtp.rx_payload_type;
		   voice_call_stat_log_pkt.IP_addr_Type = stream->ob_stream.remote_ip.ipaddr_type;
   		
		   if(voice_call_stat_log_pkt.IP_addr_Type == QVP_RTP_IPV4)
		   {	
			   QPNET_ADDR 		IP_address_NwOrdr;
   			
				   /* Rx IP */
				   if( QVP_RTP_SUCCESS != qvp_rtp_conver_to_netowrk_order(&IP_address_NwOrdr, &stream->ob_stream.remote_ip))
				   {
				     QVP_RTP_ERR_0("qvp_rtp_conver_to_netowrk_order failed");
				   }		
			qvp_rtp_memscpy( voice_call_stat_log_pkt.ip_addr.ipv4_addr.rx_addr, 4, &IP_address_NwOrdr.ipAddress.iIPV4Address, 4);

				   /* RX port */
			voice_call_stat_log_pkt.ip_addr.ipv4_addr.RxPort = stream->ob_stream.local_port;

				   /* TX IP */
				   if( QVP_RTP_SUCCESS != qvp_rtp_conver_to_netowrk_order(&IP_address_NwOrdr, &stream->ob_stream.local_ip))
				   {
				     QVP_RTP_ERR_0("qvp_rtp_conver_to_netowrk_order failed");
				   }				
			qvp_rtp_memscpy( voice_call_stat_log_pkt.ip_addr.ipv4_addr.tx_addr, 4, &IP_address_NwOrdr.ipAddress.iIPV4Address, 4);
				   /* TX Port */
			voice_call_stat_log_pkt.ip_addr.ipv4_addr.TxPort = stream->ob_stream.remote_port;

			   }

		   else if(voice_call_stat_log_pkt.IP_addr_Type == QVP_RTP_IPV6)
		   {
			   QPNET_ADDR 		IP_address_NwOrdr;

				   /* Rx IP */
				   if( QVP_RTP_SUCCESS != qvp_rtp_conver_to_netowrk_order(&IP_address_NwOrdr, &stream->ob_stream.remote_ip))
				   {
				     QVP_RTP_ERR_0("qvp_rtp_conver_to_netowrk_order failed");
				   }
			qvp_rtp_memscpy(voice_call_stat_log_pkt.ip_addr.ipv6_addr.rx_addr, 16, &IP_address_NwOrdr.ipAddress.iIPV6Address, 16);
				   /* RX port */
			voice_call_stat_log_pkt.ip_addr.ipv4_addr.RxPort = stream->ob_stream.local_port;

				   if( QVP_RTP_SUCCESS != qvp_rtp_conver_to_netowrk_order(&IP_address_NwOrdr, &stream->ob_stream.local_ip))
				   {
				     QVP_RTP_ERR_0("qvp_rtp_conver_to_netowrk_order failed");
				   }
				   /* TX IP */
			qvp_rtp_memscpy(voice_call_stat_log_pkt.ip_addr.ipv6_addr.tx_addr, 16, &IP_address_NwOrdr.ipAddress.iIPV6Address, 16);
				   /* TX Port */
			voice_call_stat_log_pkt.ip_addr.ipv4_addr.TxPort = stream->ob_stream.remote_port;

	   } 
	   else
	   {
			QVP_RTP_ERR_0(" commit_voice_call_stat: unknown IP type \r\n");
	   }

		/* Log the voice call statistics packet*/
		QVP_RTP_ERR_0(" Log the voice call statistics packet \r\n");
		rtp_log(LOG_VOICE_CALL_STATISTICS_C, &voice_call_stat_log_pkt, size);

	   /* Reset the parameters */
	   stream->time_session_start = 0;
	   stream->time_session_end = 0;
   	
	   }

	return;

	}


/*===========================================================================

FUNCTION QVP_RTP_LOG_VOICE_CALL_STAT

DESCRIPTION
Log the voice call statistics parameters for the given session

DEPENDENCIES
None

PARAMETERS
None

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
LOCAL void qvp_rtp_log_voice_call_stat( qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type stream_id)
	{
	qvp_rtp_stream_type						*stream = NULL; /* for inderection simplicity */
	qvp_rtp_usr_ctx							*user_ctx; 

	if( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) ||
		(stream_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) )
	 {
		  QVP_RTP_ERR( " qvp_rtp_return_stream_id_type app_id : %d stream_id:%d rtp_ctx.num_users:%d", app_id, stream_id, rtp_ctx.num_users);
		  return;
	 }


	user_ctx = &rtp_ctx.user_array[(uint32)app_id]; 
	stream = &user_ctx->stream_array[(uint32)stream_id];
		
	qvp_rtp_log_commit_voice_call_stat(stream);
	
	return;
}
/****************************************************************************/
/**
 * \fn qvp_rtp_status_type qvp_rtp_configure_srtp(
                                     qvp_rtp_stream_type* stream,
                                      qvp_rtp_config_msg_type* config_msg)
 * \brief Configures SRTP parameters
 * 
 * \param stream       stream through which packet is sent
 * \param config_msg   Configure message
 *
 * \return QVP_RTP_SUCCESS
 * \retval QVP_RTP_ERR_FATAL
 * \retval QVP_RTP_WRONG_PARAM
 * \remarks None
 */
/****************************************************************************/
qvp_rtp_status_type qvp_rtp_configure_srtp(qvp_rtp_stream_type* stream, qvp_rtp_config_msg_type* config_msg)
{
	 uint8 masterkey[20];
     uint8 mastersalt[20];
	 uint8 decoutput[64];
	 uint32 decode_len = 0;
	 qvp_rtp_status_type status = QVP_RTP_SUCCESS;
	 
     stream->is_srtp_enabled = TRUE;
     if(config_msg->config.config_rx_params.rx_srtp_info_valid && (config_msg->config.config_rx_params.rx_srtp_info.master_key != NULL))
	 {
	    stream->rx_srtp_info.sec_algo = config_msg->config.config_rx_params.rx_srtp_info.sec_algo;
		stream->rx_srtp_info.hash_algo = config_msg->config.config_rx_params.rx_srtp_info.hash_algo;
        stream->rx_srtp_info.is_authentication_enabled = config_msg->config.config_rx_params.rx_srtp_info.is_authentication_enabled;			
	    QVP_RTP_MSG_HIGH("SRTP enabled: RX security algo is %d RX hash algo is %d RX authentication = %d",stream->rx_srtp_info.sec_algo,stream->rx_srtp_info.sec_algo,stream->rx_srtp_info.is_authentication_enabled);
		QVP_RTP_MSG_HIGH_1("SRTP enabled: RX security Master key length = %d", strlen(config_msg->config.config_rx_params.rx_srtp_info.master_key));
		memset(stream->rx_master_key, 0, 255);
		qvp_rtp_memscpy(stream->rx_master_key, 255, config_msg->config.config_rx_params.rx_srtp_info.master_key, strlen(config_msg->config.config_rx_params.rx_srtp_info.master_key)+1);
		stream->rx_srtp_info_valid = TRUE;
	  }
		  
	  if(config_msg->config.config_tx_params.tx_srtp_info_valid && (config_msg->config.config_tx_params.tx_srtp_info.master_key != NULL))
	  {
		stream->tx_srtp_info.sec_algo = config_msg->config.config_tx_params.tx_srtp_info.sec_algo;
	    stream->tx_srtp_info.hash_algo = config_msg->config.config_tx_params.tx_srtp_info.hash_algo;
		stream->tx_srtp_info.is_authentication_enabled = config_msg->config.config_tx_params.tx_srtp_info.is_authentication_enabled;
		QVP_RTP_MSG_HIGH("SRTP enabled: TX Hash algo is %d TX Hash algo is %d TX authentication = %d",stream->tx_srtp_info.hash_algo,stream->tx_srtp_info.hash_algo,stream->tx_srtp_info.is_authentication_enabled);
		QVP_RTP_MSG_HIGH_1("SRTP enabled: TX security Master key length = %d", strlen(config_msg->config.config_tx_params.tx_srtp_info.master_key));
		memset(stream->tx_master_key, 0, 255);
		qvp_rtp_memscpy(stream->tx_master_key, 255, config_msg->config.config_tx_params.tx_srtp_info.master_key, strlen(config_msg->config.config_tx_params.tx_srtp_info.master_key)+1);
		stream->tx_srtp_info_valid = TRUE;
	  }
	  
	  if(stream->rx_srtp_info_valid && stream->tx_srtp_info_valid)
	  {
	    QVP_RTP_MSG_LOW_0("SRTP enabled: Starting Crypto configuration");
		if((stream->rx_srtp_info.sec_algo == stream->tx_srtp_info.sec_algo) &&
		   (stream->tx_srtp_info.hash_algo == stream->rx_srtp_info.hash_algo))
		{
		
		   if( (stream->srtp_ctx.cipher_handle != 0) || (stream->srtp_ctx.auth_handle != 0) )
			   qvp_srtp_deinit_crypto(stream->srtp_ctx.cipher_handle, stream->srtp_ctx.auth_handle);		
			   
		  status = qvp_srtp_init_crypto(&stream->srtp_ctx, &stream->tx_srtp_info);
		  
		  memset(&srtcp_param, 0, sizeof(qvp_rtcp_config_srtcp_param_type));
		  /**< Dervie keys for TX */
		  if(status == QVP_RTP_SUCCESS)
		  {
			srtcp_param.cipher_handle = stream->srtp_ctx.cipher_handle;
			srtcp_param.auth_handle = stream->srtp_ctx.auth_handle;
			if( stream->tx_srtp_info.hash_algo == QVP_RTP_HMAC_SHA1_80)
				srtcp_param.hash_tag_len =  10;
			else if (stream->tx_srtp_info.hash_algo == QVP_RTP_HMAC_SHA1_32)
				srtcp_param.hash_tag_len =  4;
			
		    decode_len = rtp_DecodeBase64(stream->tx_master_key, (QPCHAR*)decoutput);
            QVP_RTP_MSG_LOW_1("SRTP enabled: TX master key DecodeBase64 decode_len %u",decode_len);
			if(decode_len != 30)
			{
			  status = QVP_RTP_ERR_FATAL;
			  QVP_RTP_ERR_0("SRTP enabled: TX Master Key length Mis Match with crypto context");
			}
			if(status == QVP_RTP_SUCCESS)
			{
			  int i = 0;
			  memset(masterkey, 0, 20);
			  memset(mastersalt, 0, 20);
              qvp_rtp_memscpy(masterkey, 20, decoutput+0, 16);
              qvp_rtp_memscpy(mastersalt, 20, decoutput+16, 14);
			  
			  memset(srtcp_param.tx_masterkey, 0, 20);
			  memset(srtcp_param.tx_mastersalt, 0, 20);			  
			  qvp_rtp_memscpy(srtcp_param.tx_masterkey, 20, masterkey, 16);
			  qvp_rtp_memscpy(srtcp_param.tx_mastersalt, 20, mastersalt, 14);
			  
              //RTP key derivation
              decode_len = qvp_srtp_derive_keys(stream->srtp_ctx.cipher_handle, masterkey, mastersalt, 0x00, stream->srtp_ctx.tx_keys.K_e, 16);
              QVP_RTP_MSG_LOW_1("SRTP enabled: TX derive RTP Encryption key %u",decode_len);
			  i = 0;
			  while(stream->srtp_ctx.tx_keys.K_e[i] != '\0')
			  {
			    QVP_RTP_MSG_LOW_2("tx_keys.K_e %d of index %d",stream->srtp_ctx.tx_keys.K_e[i],i);
				i++;
			  }
			  if(decode_len != 16)
			  {
			    status = QVP_RTP_ERR_FATAL;
			    QVP_RTP_ERR_0("SRTP enabled: TX RTP Encryption Key length Mis Match with crypto context");
			  }			  
              decode_len = qvp_srtp_derive_keys(stream->srtp_ctx.cipher_handle, masterkey, mastersalt, 0x01, stream->srtp_ctx.tx_keys.K_a, 20);  
              QVP_RTP_MSG_LOW_1("SRTP enabled: TX derive RTP auth key %u",decode_len);
			  i = 0;
			  while(stream->srtp_ctx.tx_keys.K_a[i] != '\0')
			  {
			    QVP_RTP_MSG_LOW_2("tx_keys.K_a %d of index %d",stream->srtp_ctx.tx_keys.K_a[i],i);
				i++;
			  }			  
			  if(decode_len != 20)
			  {
			    status = QVP_RTP_ERR_FATAL;
			    QVP_RTP_ERR_0("SRTP enabled: TX RTP Authentication Key length Mis Match with crypto context");
			  }			  
              decode_len = qvp_srtp_derive_keys(stream->srtp_ctx.cipher_handle, masterkey, mastersalt, 0x02, stream->srtp_ctx.tx_keys.K_s, 14);  
              QVP_RTP_MSG_LOW_1("SRTP enabled: TX derive RTP salt key %u",decode_len);
			  if(decode_len != 14)
			  {
			    status = QVP_RTP_ERR_FATAL;
			    QVP_RTP_ERR_0("SRTP enabled: TX RTP Salting Key length Mis Match with crypto context");
			  }			  
			  i = 0;
			  while(stream->srtp_ctx.tx_keys.K_s[i] != '\0')
			  {
			    QVP_RTP_MSG_LOW_2("tx_keys.K_s %d of index %d",stream->srtp_ctx.tx_keys.K_s[i],i);
				i++;
			  }			  
			}
		  }
		  
		  /**< Dervie keys for RX */
		  if(status == QVP_RTP_SUCCESS)
		  {
		     int  i = 0;	
		    decode_len = rtp_DecodeBase64(stream->rx_master_key, (QPCHAR*)decoutput);
            QVP_RTP_MSG_LOW_1("SRTP enabled: RX master key DecodeBase64 decode_len %u",decode_len);
			if(decode_len != 30)
			{
			  status = QVP_RTP_ERR_FATAL;
			  QVP_RTP_ERR_0("SRTP enabled: RX Master Key length Mis Match with crypto context");
			}
			if(status == QVP_RTP_SUCCESS)
			{
              qvp_rtp_memscpy(masterkey, 20, decoutput+0, 16);
              qvp_rtp_memscpy(mastersalt, 20, decoutput+16, 14);
			  
			  qvp_rtp_memscpy(srtcp_param.rx_masterkey, 20, masterkey, 16);
			  qvp_rtp_memscpy(srtcp_param.rx_mastersalt, 20, mastersalt, 14);
			  
              //RTP key derivation
              decode_len = qvp_srtp_derive_keys(stream->srtp_ctx.cipher_handle, masterkey, mastersalt, 0x00, stream->srtp_ctx.rx_keys.K_e, 16);
              QVP_RTP_MSG_LOW_1("SRTP enabled: RX derive RTP Encryption key %u",decode_len);
			  if(decode_len != 16)
			  {
			    status = QVP_RTP_ERR_FATAL;
			    QVP_RTP_ERR_0("SRTP enabled: RX RTP Encryption Key length Mis Match with crypto context");
			  }			  
			  for(i=0;i<decode_len;i++)
			  {
			    QVP_RTP_MSG_LOW_2("rx_keys.K_e %d of index %d",stream->srtp_ctx.rx_keys.K_e[i],i);
			  }			  
              decode_len = qvp_srtp_derive_keys(stream->srtp_ctx.cipher_handle, masterkey, mastersalt, 0x01, stream->srtp_ctx.rx_keys.K_a, 20);  
              QVP_RTP_MSG_LOW_1("SRTP enabled: RX derive RTP auth key %u",decode_len);
			  if(decode_len != 20)
			  {
			    status = QVP_RTP_ERR_FATAL;
			    QVP_RTP_ERR_0("SRTP enabled: RX RTP Authentication Key length Mis Match with crypto context");
			  }			  
			  for(i=0;i<decode_len;i++)
			  {
			    QVP_RTP_MSG_LOW_2("rx_keys.K_a %d of index %d",stream->srtp_ctx.rx_keys.K_a[i],i);
			  }			  
              decode_len = qvp_srtp_derive_keys(stream->srtp_ctx.cipher_handle, masterkey, mastersalt, 0x02, stream->srtp_ctx.rx_keys.K_s, 14);  
              QVP_RTP_MSG_LOW_1("SRTP enabled: RX derive RTP salt key %u",decode_len);
			  if(decode_len != 14)
			  {
			    status = QVP_RTP_ERR_FATAL;
			    QVP_RTP_ERR_0("SRTP enabled: RX RTP Salting Key length Mis Match with crypto context");
			  }			  
			  for(i=0;i<decode_len;i++)
			  {
			    QVP_RTP_MSG_LOW_2("rx_keys.K_s %d of index %d",stream->srtp_ctx.rx_keys.K_s[i],i);
			  }			  
			}
		  }		  
		  
		}
		
	  }
	  else
	    status = QVP_RTP_WRONG_PARAM;
	return status;
}/*qvp_rtp_configure_srtp*/
/****************************************************************************/
/**
 * \fn char* qvp_rtp_generate_srtp_master_key(
                                     qvp_rtp_app_id_type app_id)
 * \brief Generates master key for SRTP
 * 
 * \param app_id application which is sending packet
 *
 * \return Pointer to master Key
 * \retval Master Key
 * \retval NULL
 * \remarks None
 */
/****************************************************************************/
char* qvp_rtp_generate_srtp_master_key(qvp_rtp_app_id_type app_id)
{
	char* temp_master_key = NULL;
  /*------------------------------------------------------------------------
    Check forparams before we act on message 
    1) good app id
    2) app id in range
    3) stream id in range
    4) Good info pointer

  ------------------------------------------------------------------------*/
  if ( ( app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
       ( app_id < ( qvp_rtp_app_id_type  ) (uint32)rtp_ctx.num_users )  )
  {
	qvp_rtp_usr_ctx *usr_ctx; 
	int i =0;
	dword random_key[16];
secerrno_enum_type rand_number_vaild = E_SUCCESS;
    /*----------------------------------------------------------------------
        Looks like we have a valid handle . extract the stream to close 
    ----------------------------------------------------------------------*/
    usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ]; 
	
	if(usr_ctx->master_key == NULL)
		return NULL;
	
	temp_master_key = (char*)qvp_rtp_malloc(255);
	
	if(temp_master_key == NULL)
		return NULL;
		
	memset(temp_master_key, 0, 255);
	memset(usr_ctx->master_key,0,255);

  rand_number_vaild = secapi_get_random( SECAPI_SECURE_RANDOM, (uint8*) &random_key, sizeof(random_key));

  if(rand_number_vaild != E_SUCCESS)
  {
QVP_RTP_MSG_HIGH_0("secapi_get_random: Not able to generate master key");
	qvp_rtp_free(temp_master_key);
return NULL;
}
	for(i=0; i<16; i++)
	{
       		temp_master_key[i] = ran_dist( random_key[i], 34, 127 );
	}
//master salt
	for(i=0; i<14; i++)
	{
		temp_master_key[i+16] = temp_master_key[i];
	}
	i = 0;
	while (usr_ctx->master_key[i] != '\0')
	{
		QVP_RTP_MSG_LOW_2("srtp enable temp_master_key[%d]: %d",i,temp_master_key[i]);
		i++;
	}	
	
	rtp_EncodeBase64(temp_master_key, 30, usr_ctx->master_key);
	
	i = 0;
	while (usr_ctx->master_key[i] != '\0')
	{
		QVP_RTP_MSG_LOW_2("srtp enable master_key[%d]: %d",i,usr_ctx->master_key[i]);
		i++;
	}
	qvp_rtp_free(temp_master_key);
	
	return usr_ctx->master_key;
  }
  else
  {
    QVP_RTP_ERR_0( "qvp_rtp_generate_srtp_master_key ::  master key generation failed because \
                  wrong params");
    return NULL;
  }
}/* End of qvp_rtp_generate_srtp_master_key*/

void qvp_rtp_media_uninitialize_all_cmd
(
	qvp_rtp_media_uninitialize_all_msg_type *media_uninitialize_all_msg
)
{
		uint8 stream_no = 0;
	  	qvp_rtp_usr_ctx *usr_ctx; 
	  	qvp_rtp_close_msg_type close_msg; /* to be used for closing channels */
        clear_all_session_req = TRUE;
		srvcc_tear_down_req = FALSE;

	if ( ( media_uninitialize_all_msg != NULL ) && 
		( media_uninitialize_all_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
		( media_uninitialize_all_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users )
		&& media_uninitialize_all_msg->media_type != QVP_RTP_INVALID 
		&& media_uninitialize_all_msg->rat_type != QVP_RTP_RAT_NONE)
		  	{
		  	usr_ctx = &rtp_ctx.user_array[ (uint32) media_uninitialize_all_msg->app_id ];

				if(usr_ctx->valid == FALSE)
		  		{
		  				QVP_RTP_MSG_HIGH_0("USER CTX corresponds to this app id is invalid");
						return;
		  		}

            QVP_RTP_MSG_HIGH_1("uninitializ_all entered g_cvd_ssr %d",g_cvd_ssr);
		    	for( stream_no = 0; stream_no < usr_ctx->num_streams; stream_no++ )
  				{
    				if( usr_ctx->stream_array[ stream_no ].valid )
    				{

      				close_msg.app_id = media_uninitialize_all_msg->app_id;
      				close_msg.stream_id  = usr_ctx->stream_array[stream_no].str_id;
      
      				qvp_rtp_close_cmd( &close_msg );
    				}
		    	}

        if(g_cvd_ssr)
        {
          if (QVP_RTP_SUCCESS != qvp_rtp_clear_audio_resource(media_uninitialize_all_msg->app_id))
          {
            QVP_RTP_MSG_HIGH_0( "clear_audio_resource failed to process\r\n");
          }          
        }                 
        else
        {
				if (QVP_RTP_SUCCESS != qvp_rtp_media_stream_stop_pvt(media_uninitialize_all_msg->app_id, NULL, QVP_RTP_DIR_TXRX))
				{
				    QVP_RTP_MSG_HIGH_0( "media_codec_release failed to process\r\n");
				}
        }		
		  
		memset( &rtp_ctx.user_array[(uint32) media_uninitialize_all_msg->app_id ].usr_ctx_pvt, 0, QVP_RTP_MAX_USR_PVT_COUNT * sizeof(qvp_rtp_usr_ctx_pvt));
		memset( &rtp_ctx.user_array[(uint32) media_uninitialize_all_msg->app_id ].pending_queue, 0, QVP_RTP_MAX_QUEUE_COUNT * sizeof(qvp_rtp_pending_queue));
		{
			int i = 0;
			
			for(i=0; i<QVP_RTP_MAX_USR_PVT_COUNT; i++)
		    {
				rtp_ctx.user_array[(uint32) media_uninitialize_all_msg->app_id].usr_ctx_pvt[i].apps_stream_id = (qvp_rtp_stream_id_type)DEAD_BEEF;
				rtp_ctx.user_array[(uint32) media_uninitialize_all_msg->app_id].usr_ctx_pvt[i].modem_stream_id = (qvp_rtp_stream_id_type)DEAD_BEEF;
				rtp_ctx.user_array[(uint32) media_uninitialize_all_msg->app_id].usr_ctx_pvt[i].is_modem_stream_id_valid = FALSE;
				rtp_ctx.user_array[(uint32) media_uninitialize_all_msg->app_id].usr_ctx_pvt[i].is_apps_stream_id_valid = FALSE;

		    }
		}

}
else
{

	          		QVP_RTP_ERR_0( " uninitializ_all could not be processed \r\n");
}
}
int qvp_rtp_usr_ctx_pvt_update_config(qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type stream_id, qvp_rtp_msg_type *msg)
{
  int ret_val = 0;
  qvp_rtp_usr_ctx *usr_ctx; 
  qvp_rtp_stream_id_type active_stream_id = (qvp_rtp_stream_id_type)DEAD_BEEF;
  
  if(( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) && ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
    return -1;

  
  usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ]; 

  QVP_RTP_MSG_HIGH_2( " qvp_rtp_usr_ctx_pvt_update_config msg_type : %d external_stream_id:%d\r\n", msg->msg_type, stream_id);

  if(g_handoffstate == HO_IN_PROGRESS)
  {
    QVP_RTP_MSG_HIGH_0( " qvp_rtp_usr_ctx_pvt_update_config HO_IN_PROGRESS RETRUN\r\n");
  	return 0;
  }

  switch(msg->msg_type)
  {
	  case QVP_RTP_OPEN2_CMD:
	  case QVP_RTP_OPEN_CMD:
	    for(;ret_val<QVP_RTP_MAX_USR_PVT_COUNT;ret_val++)
	    {
	      if(!usr_ctx->usr_ctx_pvt[ret_val].valid && !usr_ctx->usr_ctx_pvt[ret_val].is_open_pending && !usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt)
	      {
	      	usr_ctx->usr_ctx_pvt[ret_val].is_open_pending = TRUE;
			usr_ctx->usr_ctx_pvt[ret_val].stream_direction = QVP_RTP_DIR_TXRX;
	        break;
		}
		}
		QVP_RTP_MSG_HIGH_1( " qvp_rtp_usr_ctx_pvt_update_config ret_val : %d\r\n", ret_val);
	    break;
		
	  case QVP_RTP_CONFIG_CMD:
	  case QVP_RTP_MEDIA_CODEC_CONFIG_CMD:
	  case QVP_RTP_CODEC_INIT_CMD:
	  case QVP_RTP_CONFIG_SESSION_CMD:
	  case QVP_RTP_LINK_ALIVE_TIMER_CMD:
	  case QVP_RTCP_LINK_ALIVE_TIMER_CMD:
	  case QVP_RTP_START_STREAM_CMD:
      case QVP_RTP_RESUME_STREAM_CMD:
	  case QVP_START_KEEP_ALIVE_TIMER:
	  case QVP_STOP_KEEP_ALIVE_TIMER:
	  case QVP_RTP_PAUSE_STREAM_CMD:
	    for(;ret_val<QVP_RTP_MAX_USR_PVT_COUNT;ret_val++)
	    {
	      if(usr_ctx->usr_ctx_pvt[ret_val].valid && stream_id == usr_ctx->usr_ctx_pvt[ret_val].external_stream_id)
	      {
		    QVP_RTP_MSG_HIGH( " qvp_rtp_usr_ctx_pvt_update_config ret_val : %d stream_id:%u external_stream_id:%d", ret_val, stream_id, usr_ctx->usr_ctx_pvt[ret_val].external_stream_id ); 	
	      	break;
	      }

	    }
				  
	    break;
    default:
	   break;

  }
  

  if(ret_val < QVP_RTP_MAX_USR_PVT_COUNT)
  {
    if(msg->msg_type == QVP_RTP_OPEN2_CMD)
    {
      qvp_rtp_memscpy(&usr_ctx->usr_ctx_pvt[ret_val].open2_msg, sizeof(qvp_rtp_open2_msg_type), &msg->msg_param.open2_msg, sizeof(qvp_rtp_open2_msg_type));
	  usr_ctx->usr_ctx_pvt[ret_val].app_data = (uint32) msg->msg_param.open2_msg.open_config_param.stream_params.app_data;
    }
    else if(msg->msg_type == QVP_RTP_CONFIG_CMD)
    {
      qvp_rtp_memscpy(&usr_ctx->usr_ctx_pvt[ret_val].config_msg, sizeof(qvp_rtp_config_msg_type), &msg->msg_param.config_msg, sizeof(qvp_rtp_config_msg_type));
	  usr_ctx->usr_ctx_pvt[ret_val].is_config_msg = TRUE;
    }
    else if(msg->msg_type == QVP_RTP_CODEC_INIT_CMD)
    {
      qvp_rtp_memscpy(&usr_ctx->usr_ctx_pvt[ret_val].codec_init_msg, sizeof(qvp_rtp_media_codec_init_msg_type), &msg->msg_param.media_codec_init_msg, sizeof(qvp_rtp_media_codec_init_msg_type));
	  if(usr_ctx->usr_ctx_pvt[ret_val].is_modem_stream_id_valid)
	  {
	  	//if(qvp_rtp_find_media_type_from_pvt_ctx( usr_ctx->usr_ctx_pvt[ret_val].modem_stream_id, msg->msg_param.media_codec_init_msg.app_id, TRUE, FALSE) == QVP_RTP_AUDIO)
	  	{
	  active_stream_id = msg->msg_param.media_codec_init_msg.stream_id;
			usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += 2 * QVP_RTP_AUDIO_IND_CNT; // Since codec init will internally do codec set as well. so number of indications expected will be twice.
	  	}
	  }
	  else if(usr_ctx->usr_ctx_pvt[ret_val].is_apps_stream_id_valid)
	  {
		  if(qvp_rtp_find_media_type_from_pvt_ctx( usr_ctx->usr_ctx_pvt[ret_val].apps_stream_id, msg->msg_param.media_codec_init_msg.app_id, FALSE, FALSE) == QVP_RTP_AUDIO)
		  {
	  active_stream_id = msg->msg_param.media_codec_init_msg.stream_id;
			usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += 2 * QVP_RTP_AUDIO_IND_CNT; // Since codec init will internally do codec set as well. so number of indications expected will be twice.
		 }
		 else
		 {
		 	if(msg->msg_param.media_codec_init_msg.direction == QVP_RTP_DIR_TXRX)
		 		usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += 2 * QVP_RTP_VIDEO_IND_CNT; // Since codec init will internally do codec set as well. so number of indications expected will be twice.
		 	else if( (msg->msg_param.media_codec_init_msg.direction == QVP_RTP_DIR_TX) || (msg->msg_param.media_codec_init_msg.direction == QVP_RTP_DIR_RX) )
		 		usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += 2 * QVP_RTP_VIDEO_TX_IND_CNT; // Since codec init will internally do codec set as well. so number of indications expected will be twice.
		  }
	  }
	  usr_ctx->usr_ctx_pvt[ret_val].stream_direction = msg->msg_param.media_codec_init_msg.direction;
    }
    else if(msg->msg_type == QVP_RTP_MEDIA_CODEC_CONFIG_CMD)
    {
      qvp_rtp_memscpy(&usr_ctx->usr_ctx_pvt[ret_val].codec_init_msg, sizeof(qvp_rtp_media_codec_init_msg_type), &msg->msg_param.media_codec_config_msg, sizeof(qvp_rtp_media_codec_init_msg_type));
	  if(usr_ctx->usr_ctx_pvt[ret_val].is_modem_stream_id_valid)
	  {
	  	//if(qvp_rtp_find_media_type_from_pvt_ctx( usr_ctx->usr_ctx_pvt[ret_val].modem_stream_id, msg->msg_param.media_codec_config_msg.app_id, TRUE, FALSE) == QVP_RTP_AUDIO)
	  	{
	  active_stream_id = msg->msg_param.media_codec_config_msg.stream_id;
			usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += QVP_RTP_AUDIO_IND_CNT;
	  	}
	  }
	  else if(usr_ctx->usr_ctx_pvt[ret_val].is_apps_stream_id_valid)
	  {
		  if(qvp_rtp_find_media_type_from_pvt_ctx( usr_ctx->usr_ctx_pvt[ret_val].apps_stream_id, msg->msg_param.media_codec_config_msg.app_id, FALSE, FALSE) == QVP_RTP_AUDIO)
		  {
	  active_stream_id = msg->msg_param.media_codec_config_msg.stream_id;
			usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += QVP_RTP_AUDIO_IND_CNT;
		  }
		 else
		 {
		 	if(msg->msg_param.media_codec_config_msg.direction == QVP_RTP_DIR_TXRX)
		 		usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += QVP_RTP_VIDEO_IND_CNT; 
		 	else if( (msg->msg_param.media_codec_config_msg.direction == QVP_RTP_DIR_TX) || (msg->msg_param.media_codec_config_msg.direction == QVP_RTP_DIR_RX) )
		 		usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += QVP_RTP_VIDEO_TX_IND_CNT; 
		  }
	  }	  
	  usr_ctx->usr_ctx_pvt[ret_val].stream_direction = msg->msg_param.media_codec_config_msg.direction;
    }	
    else if(msg->msg_type == QVP_RTP_CONFIG_SESSION_CMD)
    {
      qvp_rtp_memscpy(&usr_ctx->usr_ctx_pvt[ret_val].config_session_msg, sizeof(qvp_rtp_config_session_msg_type), &msg->msg_param.config_session_msg, sizeof(qvp_rtp_config_session_msg_type));
    }
    else if(msg->msg_type == QVP_RTP_LINK_ALIVE_TIMER_CMD)
    {
      qvp_rtp_memscpy(&usr_ctx->usr_ctx_pvt[ret_val].rtp_link_alive_timer_msg, sizeof(qvp_rtp_link_alive_timer_msg_type), &msg->msg_param.rtp_link_alive_timer_msg, sizeof(qvp_rtp_link_alive_timer_msg_type));
    }
    else if(msg->msg_type == QVP_RTCP_LINK_ALIVE_TIMER_CMD)
    {
      qvp_rtp_memscpy(&usr_ctx->usr_ctx_pvt[ret_val].rtcp_link_alive_timer_msg, sizeof(qvp_rtcp_link_alive_timer_msg_type), &msg->msg_param.rtcp_link_alive_timer_msg, sizeof(qvp_rtcp_link_alive_timer_msg_type));      
    }
	else if(msg->msg_type == QVP_RTP_START_STREAM_CMD)
	{
	  if(usr_ctx->usr_ctx_pvt[ret_val].is_modem_stream_id_valid)
	  {
	  	//if(qvp_rtp_find_media_type_from_pvt_ctx( usr_ctx->usr_ctx_pvt[ret_val].modem_stream_id, msg->msg_param.media_start_stream_msg.app_id, TRUE, FALSE) == QVP_RTP_AUDIO)
	  	{
	  		active_stream_id = msg->msg_param.media_start_stream_msg.stream_id;
			usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += QVP_RTP_AUDIO_IND_CNT;
	  	}
	  }
	  else if(usr_ctx->usr_ctx_pvt[ret_val].is_apps_stream_id_valid)
	  {
		  if(qvp_rtp_find_media_type_from_pvt_ctx( usr_ctx->usr_ctx_pvt[ret_val].apps_stream_id, msg->msg_param.media_start_stream_msg.app_id, FALSE, FALSE) == QVP_RTP_AUDIO)
		  {
		active_stream_id = msg->msg_param.media_start_stream_msg.stream_id;
			usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += QVP_RTP_AUDIO_IND_CNT;
		  }
		 else
		 {
			 if(msg->msg_param.media_start_stream_msg.direction == QVP_RTP_DIR_TXRX)
				 usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += QVP_RTP_VIDEO_IND_CNT; 
			 else if( (msg->msg_param.media_start_stream_msg.direction == QVP_RTP_DIR_TX) || (msg->msg_param.media_start_stream_msg.direction == QVP_RTP_DIR_RX) )
				 usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt += QVP_RTP_VIDEO_TX_IND_CNT; 
		  }
	  }	  	
		QVP_RTP_MSG_HIGH_1( " qvp_rtp_usr_ctx_pvt_update_config QVP_RTP_START_STREAM_CMD: active stream id:%d\r\n", usr_ctx->active_stream_id);
		usr_ctx->usr_ctx_pvt[ret_val].stream_direction = msg->msg_param.media_start_stream_msg.direction;
	}
	else if(msg->msg_type == QVP_RTP_RESUME_STREAM_CMD)
	{
		//usr_ctx->usr_ctx_pvt[ret_val].stream_direction = msg->msg_param.media_resume_stream_msg.direction;
		switch(msg->msg_param.media_resume_stream_msg.direction)
	    {
	    case  QVP_RTP_DIR_TX:
	      if(usr_ctx->usr_ctx_pvt[ret_val].stream_direction == QVP_RTP_INV_DIR)
	        usr_ctx->usr_ctx_pvt[ret_val].stream_direction  = QVP_RTP_DIR_TX;
	      else if(usr_ctx->usr_ctx_pvt[ret_val].stream_direction == QVP_RTP_DIR_RX)
	        usr_ctx->usr_ctx_pvt[ret_val].stream_direction  = QVP_RTP_DIR_TXRX;
	      break;
	    case QVP_RTP_DIR_RX:
	      if(usr_ctx->usr_ctx_pvt[ret_val].stream_direction  == QVP_RTP_INV_DIR)
	        usr_ctx->usr_ctx_pvt[ret_val].stream_direction  = QVP_RTP_DIR_RX;
	      else if(usr_ctx->usr_ctx_pvt[ret_val].stream_direction == QVP_RTP_DIR_TX)
	        usr_ctx->usr_ctx_pvt[ret_val].stream_direction  = QVP_RTP_DIR_TXRX;
	      break;
	    case QVP_RTP_DIR_TXRX:
	      usr_ctx->usr_ctx_pvt[ret_val].stream_direction  = QVP_RTP_DIR_TXRX;
	      break;
	    default:
	      QVP_RTP_MSG_LOW_0("qvp_rtp_usr_ctx_pvt_update_config: Invalid direction....");
	      break;
	    }
		QVP_RTP_MSG_HIGH_2( " usr_ctx_pvt_update RESUME: stream direction:%d pvt ctx index:%d\r\n", usr_ctx->usr_ctx_pvt[ret_val].stream_direction, ret_val);		
	}	
	else if(msg->msg_type == QVP_RTP_PAUSE_STREAM_CMD)
	{
		//usr_ctx->usr_ctx_pvt[ret_val].stream_direction = msg->msg_param.media_pause_stream_msg.direction;
		switch(msg->msg_param.media_pause_stream_msg.direction)
	    {
	    case  QVP_RTP_DIR_TX:
	      if(usr_ctx->usr_ctx_pvt[ret_val].stream_direction == QVP_RTP_DIR_TXRX)
	        usr_ctx->usr_ctx_pvt[ret_val].stream_direction  = QVP_RTP_DIR_RX;
	      else if(usr_ctx->usr_ctx_pvt[ret_val].stream_direction == QVP_RTP_DIR_TX)
	        usr_ctx->usr_ctx_pvt[ret_val].stream_direction  = QVP_RTP_INV_DIR;
	      break;
	    case QVP_RTP_DIR_RX:
	      if(usr_ctx->usr_ctx_pvt[ret_val].stream_direction  == QVP_RTP_DIR_TXRX)
	        usr_ctx->usr_ctx_pvt[ret_val].stream_direction  = QVP_RTP_DIR_TX;
	      else if(usr_ctx->usr_ctx_pvt[ret_val].stream_direction == QVP_RTP_DIR_RX)
	        usr_ctx->usr_ctx_pvt[ret_val].stream_direction  = QVP_RTP_INV_DIR;
	      break;
	    case QVP_RTP_DIR_TXRX:
	      usr_ctx->usr_ctx_pvt[ret_val].stream_direction  = QVP_RTP_INV_DIR;
	      break;
	    default:
	      QVP_RTP_MSG_LOW_0("qvp_rtp_usr_ctx_pvt_update_config: Invalid direction....");
	      break;
	    }
		QVP_RTP_MSG_HIGH_2( " usr_ctx_pvt_update PAUSE: stream direction:%d pvt ctx index:%d\r\n", usr_ctx->usr_ctx_pvt[ret_val].stream_direction, ret_val);
	}
	else if(msg->msg_type == QVP_START_KEEP_ALIVE_TIMER)
	{
		QVP_RTP_MSG_HIGH_1( " qvp_rtp_usr_ctx_pvt_update_config QVP_START_KEEP_ALIVE_TIMER: Time_type id:%d\r\n", msg->msg_param.start_keep_alive_timer.timer_type);
		
	}
	else if(msg->msg_type == QVP_STOP_KEEP_ALIVE_TIMER)
	{
		QVP_RTP_MSG_HIGH_1( " qvp_rtp_usr_ctx_pvt_update_config QVP_STOP_KEEP_ALIVE_TIMER: Time_type:%d\r\n", msg->msg_param.stop_keep_alive_timer.timer_type);
		
	}	
	QVP_RTP_MSG_HIGH( " qvp_rtp_usr_ctx_pvt_update_config ret_val : %d stream_id:%u external_stream_id:%d", ret_val, stream_id, usr_ctx->usr_ctx_pvt[ret_val].external_stream_id ); 	
	QVP_RTP_ERR_1( " qvp_rtp_usr_ctx_pvt_update_config updating: pending_ind_ref_cnt:%d\r\n", usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt);	

	if(active_stream_id != (qvp_rtp_stream_id_type)DEAD_BEEF)
	{
				usr_ctx->active_stream_id = active_stream_id;
		QVP_RTP_MSG_HIGH_1( " qvp_rtp_usr_ctx_pvt_update_config updating: active_stream_id:%d\r\n", usr_ctx->active_stream_id);
	}

    ret_val = 0;    
  }
  else
  {
  	for(ret_val =0; ret_val < QVP_RTP_MAX_USR_PVT_COUNT; ret_val++)
  	{
  		QVP_RTP_MSG_HIGH( " qvp_rtp_usr_ctx_pvt_update_config. not able to find external streamid in pvt_ctx. ret_val : %d stream_id:%u external_stream_id:%d", ret_val, stream_id, usr_ctx->usr_ctx_pvt[ret_val].external_stream_id ); 	
  	}
    ret_val = -1;
  }
  
  return ret_val;
}
		
int qvp_rtp_return_free_usr_ctx_pvt(qvp_rtp_app_id_type app_id, void* param)
{
  qvp_rtp_usr_ctx *usr_ctx; 
  int ret_val = -1;
  if(( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	return ret_val;
  
  usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ]; 
  
  for(ret_val = 0;ret_val<QVP_RTP_MAX_USR_PVT_COUNT;ret_val++)
  {
    
	if(g_handoffstate == HO_IN_PROGRESS)
	{
		QVP_RTP_MSG_HIGH( " qvp_rtp_return_free_usr_ctx_pvt ret_val : %d vaild: %d param:%d", ret_val, usr_ctx->usr_ctx_pvt[ret_val].valid, param); 
		if(usr_ctx->usr_ctx_pvt[ret_val].valid && (qvp_rtp_stream_id_type)param == usr_ctx->usr_ctx_pvt[ret_val].external_stream_id)
	      break;		
	}
	else 
	{
		QVP_RTP_MSG_HIGH( " qvp_rtp_return_free_usr_ctx_pvt ret_val : %d vaild: %d param:%d", ret_val, usr_ctx->usr_ctx_pvt[ret_val].valid,param  ); 
		if(!usr_ctx->usr_ctx_pvt[ret_val].valid && usr_ctx->usr_ctx_pvt[ret_val].is_open_pending && usr_ctx->usr_ctx_pvt[ret_val].app_data == (uint32)param)
		{
			usr_ctx->usr_ctx_pvt[ret_val].is_open_pending = FALSE;
      break;
  }
  }
  }
  return ret_val;
}

int qvp_rtp_return_stream_id_type(qvp_rtp_stream_id_type stream_id, qvp_rtp_app_id_type app_id)
{
  qvp_rtp_usr_ctx *usr_ctx; 
  int ret_val = 0;
  
  if(( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
   {
     	QVP_RTP_ERR_2( " qvp_rtp_return_stream_id_type app_id : %d rtp_ctx.num_users:%d", app_id, rtp_ctx.num_users);
  		return -1;
  	}
		
  usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ];
  
  for(;ret_val<QVP_RTP_MAX_USR_PVT_COUNT;ret_val++)
  {
  	  	QVP_RTP_MSG_HIGH( " qvp_rtp_return_stream_id_type ret_val : %d valid:%d external_stream_id:%d", ret_val, usr_ctx->usr_ctx_pvt[ret_val].valid , usr_ctx->usr_ctx_pvt[ret_val].external_stream_id ); 	  
    if(usr_ctx->usr_ctx_pvt[ret_val].valid && stream_id == usr_ctx->usr_ctx_pvt[ret_val].external_stream_id)
    {
	      if(usr_ctx->usr_ctx_pvt[ret_val].is_modem_stream_id_valid && !usr_ctx->usr_ctx_pvt[ret_val].is_apps_stream_id_valid)
	      { 
	        ret_val = 0;
			    	  	QVP_RTP_MSG_HIGH_1( " qvp_rtp_return_stream_id_type ret_val : %d ", ret_val);
	        return ret_val;
	      }
	      if(!usr_ctx->usr_ctx_pvt[ret_val].is_modem_stream_id_valid && usr_ctx->usr_ctx_pvt[ret_val].is_apps_stream_id_valid)
	      { 
	        ret_val = 1;
			    	  	QVP_RTP_MSG_HIGH_1( " qvp_rtp_return_stream_id_type ret_val : %d ", ret_val);
	        return ret_val;
	      }
	      if(usr_ctx->usr_ctx_pvt[ret_val].is_modem_stream_id_valid && usr_ctx->usr_ctx_pvt[ret_val].is_apps_stream_id_valid)
	      { 
	      
	        QVP_RTP_ERR_0( " qvp_rtp_return_stream_id_type unhandled case. Post audio error ");
			if(g_handoffstate != HO_NONE)
			{
				g_handoffstate = HO_NONE;
			}

			qvp_rtp_response_handler( app_id, stream_id,(qvp_rtp_app_data_type)(NULL), QVP_RTP_MEDIA_AUDIO_ERROR_RSP, QVP_RTP_ERR_FATAL, TRUE, QVP_RTP_DIR_TXRX);
			
	        return ret_val;
	      }
    }
  }
  return -1;
		  
}
void qvp_rtp_handoff_queue_commands(qvp_rtp_app_id_type app_id,  boolean is_modem_valid)
{
	int index = 0,audio_idx = 0;
	qvp_rtp_usr_ctx *usr_ctx; 
	qvp_rtp_usr_ctx_pvt *pvt_ctx = NULL;
	boolean start_execution = FALSE;
	qvp_rtp_codec_state current_audio_state = is_modem_valid ? codec_info_ctx.audio_codec_info.audio_state : g_current_audio_state_on_ap;

	if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
	  ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	  {
		  QVP_RTP_ERR_1("qvp_rtp_handoff_queue_commands: Invalid AppId:%d",app_id);
	  return;
	}

	
	usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ];
	
	if(usr_ctx->valid == FALSE)
	{
	  QVP_RTP_ERR_0("USER CTX corresponds to this app id is invalid");
	  return;
	}
	
	if(g_audio_offload == 2)
	{
		
		QVP_RTP_MSG_HIGH_1("g_audio_offload:%d :No offload. Return",g_audio_offload);
		return;
	}
	if(g_audio_offload == 3)
	{
		is_modem_valid = TRUE;
		current_audio_state = codec_info_ctx.audio_codec_info.audio_state;
		QVP_RTP_MSG_HIGH_1("g_audio_offload:%d :modem offload only",g_audio_offload);
	}

	if(g_handoffstate ==HO_IN_PROGRESS)
	{
		QVP_RTP_MSG_HIGH_0("g_handoffstate in progress");
	}
		
	/* If awaiting for some APP indication, dont change status*/
	if(qvp_rtp_service_is_indication_pending() == TRUE )
	{
		QVP_RTP_ERR_0("qvp_rtp_handoff_queue_commands: Waiting for pending indication from APP");
		return;
	}
	

	/*Find  a valid audio index*/
	for(index = 0; index < QVP_RTP_MAX_USR_PVT_COUNT;index++)
	{
	    pvt_ctx = &usr_ctx->usr_ctx_pvt[index];
		QVP_RTP_MSG_HIGH("index:%d modem valid:%d app valid:%d",index, pvt_ctx->is_modem_stream_id_valid,pvt_ctx->is_apps_stream_id_valid);
		
		if(pvt_ctx->valid && pvt_ctx->is_apps_stream_id_valid && 
		   qvp_rtp_find_media_type_from_pvt_ctx( pvt_ctx->apps_stream_id,pvt_ctx->open2_msg.app_id, FALSE, FALSE) == QVP_RTP_AUDIO &&
		   pvt_ctx->external_stream_id == usr_ctx->active_stream_id)
		{
			QVP_RTP_MSG_HIGH_2("queue handoff commands: vaid audio on AP external stream id:%d and index:%d ",pvt_ctx->external_stream_id,index);
			
			current_audio_state = g_current_audio_state_on_ap;
			is_modem_valid = FALSE;
			break;
		}
		else if(pvt_ctx->valid && pvt_ctx->is_modem_stream_id_valid && 
		        qvp_rtp_find_media_type_from_pvt_ctx(pvt_ctx->modem_stream_id, pvt_ctx->open2_msg.app_id, TRUE, FALSE) == QVP_RTP_AUDIO &&
				pvt_ctx->external_stream_id == usr_ctx->active_stream_id)
		{
			QVP_RTP_MSG_HIGH_2("queue handoff commands: vaid audio on modem external stream id:%d and index:%d",pvt_ctx->external_stream_id, index);
			current_audio_state = codec_info_ctx.audio_codec_info.audio_state;
			is_modem_valid = TRUE;
			break;
		}		
	}
	/* do codec related stuff based on codec state*/
	QVP_RTP_MSG_HIGH_2("queue handoff commands: audio state:%d is_modem_valid:%d",current_audio_state,is_modem_valid);
	
	if(QVP_RTP_MAX_USR_PVT_COUNT <= index)
	{
		index = 0;
		current_audio_state = QVP_RTP_CODEC_STATE_NONE;
	}
	switch( current_audio_state)
	{
		case QVP_RTP_CODEC_STATE_INITIALIZING:
		case QVP_RTP_CODEC_STATE_INITDONE:
		case QVP_RTP_CODEC_STATE_CONFIGURING:
		case QVP_RTP_CODEC_STATE_RECONFIGURE:
		case QVP_RTP_CODEC_STATE_STARTING:
		case QVP_RTP_CODEC_STATE_STOPPING:
		case QVP_RTP_CODEC_STATE_RELEASING:
		{
		    QVP_RTP_MSG_HIGH_0("queue handoff commands: waiting for stable audio state");
			return;
		}
		case QVP_RTP_CODEC_STATE_STARTED:
		{
			g_handoffstate = HO_IN_PROGRESS;
			codec_info_ctx.audio_codec_info.audio_state_at_handoff = current_audio_state;		
			qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_STOP_CODEC,is_modem_valid);
		}
		case QVP_RTP_CODEC_STATE_CONFIGUREDONE:
		case QVP_RTP_CODEC_STATE_STOPPED:
		{
			g_handoffstate = HO_IN_PROGRESS;			
			codec_info_ctx.audio_codec_info.audio_state_at_handoff = current_audio_state;
			qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_CODEC_RELEASE,is_modem_valid);			
		}
		break;
		
		case QVP_RTP_CODEC_STATE_NONE:
		{
			g_handoffstate = HO_IN_PROGRESS;			
			codec_info_ctx.audio_codec_info.audio_state_at_handoff = current_audio_state;
			if(g_audio_offload == 3)
			{
					g_handoffstate = HO_NONE;
					QVP_RTP_MSG_HIGH_0("queue handoff commands: audio offload set to 3. Codec is not initialized so returning");
					( void ) rex_set_sigs( QVP_RTP_TASK_PTR, QVP_RTP_MSG_SIG );
			}
}
		break;
		
		default:
		{
		    QVP_RTP_MSG_HIGH_1("queue handoff commands: unhandled audio state: %d",codec_info_ctx.audio_codec_info.audio_state);
		}
	}

	audio_idx = index;
	
	if(g_handoffstate == HO_IN_PROGRESS)
	{
	    for(index = 0;index<QVP_RTP_MAX_USR_PVT_COUNT;index++)
	    {
	    	//QVP_RTP_MSG_HIGH("queue handoff commands: index:%d valid:%d is_modem_stream_id_valid:%d",index,usr_ctx->usr_ctx_pvt[index].valid,usr_ctx->usr_ctx_pvt[index].is_modem_stream_id_valid);

	    	if(g_audio_offload != 2 && g_audio_offload != 3) // If its APP offload
	    	{
				if(usr_ctx->usr_ctx_pvt[index].valid && usr_ctx->usr_ctx_pvt[index].is_modem_stream_id_valid && !usr_ctx->usr_ctx_pvt[index].is_apps_stream_id_valid)
				{

					qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_CLOSE_SESSION,is_modem_valid);
					qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_OPEN,!is_modem_valid);
						
						if(usr_ctx->usr_ctx_pvt[index].is_config_msg)
						{
						qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_CONFIGURE,!is_modem_valid);
						qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_CONFIGURE_SESSION,!is_modem_valid);
						//qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_PAUSE,!is_modem_valid);						

						qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_KEEP_ALIVE,!is_modem_valid);
						qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_LINK_ALIVE,!is_modem_valid);
						qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTCP_LINK_ALIVE,!is_modem_valid);
						}

						start_execution = TRUE;
				}
				
				if(usr_ctx->usr_ctx_pvt[index].valid && !usr_ctx->usr_ctx_pvt[index].is_modem_stream_id_valid && usr_ctx->usr_ctx_pvt[index].is_apps_stream_id_valid)
				{
					if(qvp_rtp_find_media_type_from_pvt_ctx( usr_ctx->usr_ctx_pvt[index].apps_stream_id,usr_ctx->usr_ctx_pvt[index].open2_msg.app_id, FALSE, FALSE) == QVP_RTP_AUDIO)
					{
						QVP_RTP_MSG_HIGH_0("qvp_rtp_handoff_queue_commands WLAN to LTE handoff case");
						qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_CLOSE_SESSION,is_modem_valid);
						qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_OPEN,!is_modem_valid);

						if(usr_ctx->usr_ctx_pvt[index].is_config_msg)
						{
							qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_CONFIGURE,!is_modem_valid);
							qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_CONFIGURE_SESSION,!is_modem_valid);
							//qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_PAUSE,!is_modem_valid);						
							
							qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_KEEP_ALIVE,!is_modem_valid);
							qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTP_LINK_ALIVE,!is_modem_valid);
							qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[index].external_stream_id,QVP_RTCP_LINK_ALIVE,!is_modem_valid);						
						}
					}
					start_execution = TRUE;					
				}
	    	 }
		}
	}

	if((codec_info_ctx.audio_codec_info.audio_state_at_handoff == QVP_RTP_CODEC_STATE_CONFIGUREDONE) || 
	   (codec_info_ctx.audio_codec_info.audio_state_at_handoff == QVP_RTP_CODEC_STATE_STARTED) || 
	   (codec_info_ctx.audio_codec_info.audio_state_at_handoff == QVP_RTP_CODEC_STATE_STOPPED) )
	{
	/*Update user context private*/
  		uint8 ret_val = 0;
				
		if(g_audio_offload != 2) // If need to offload, start execution
		{
			start_execution = TRUE;
		}
		
  		for(;ret_val<QVP_RTP_MAX_USR_PVT_COUNT;ret_val++)
	    {
	      if(usr_ctx->usr_ctx_pvt[ret_val].valid && usr_ctx->active_stream_id == usr_ctx->usr_ctx_pvt[ret_val].external_stream_id)
	        break;
	    }

		QVP_RTP_MSG_HIGH("qvp_rtp_handoff_queue_commands configuring codec for active active_stream_id:%d, audio_idx:%d active_audio_idx:%d",usr_ctx->active_stream_id,audio_idx,ret_val);	

		if(audio_idx < QVP_RTP_MAX_USR_PVT_COUNT)
		{
		
			boolean is_modem = !is_modem_valid;
			
			if(g_audio_offload == 3)
			{
				is_modem = TRUE;
			}
	//		qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[ret_val].external_stream_id,QVP_RTP_RTCP_CONFIGURE,is_modem);

		    //Codec Init --> internally codec set will happen
		    switch(codec_info_ctx.audio_codec_info.audio_state_at_handoff)
		    {
		    	case QVP_RTP_CODEC_STATE_CONFIGUREDONE:
				case QVP_RTP_CODEC_STATE_STOPPED:
		    	{
					qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[audio_idx].external_stream_id,QVP_RTP_CODEC_INIT,is_modem);
					qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[audio_idx].external_stream_id,QVP_RTP_CODEC_SET,is_modem);	
		    	}
				break;
				case QVP_RTP_CODEC_STATE_STARTED:
				{
					/*By default the stream will be in active state: Dir txrx. While HO, if direction is changed from TX_RX, we need to pause in the appropriate direction*/
					if(g_audio_offload != 2 && g_audio_offload != 3 ) // If its APP offload
					{
						if(usr_ctx->usr_ctx_pvt[audio_idx].stream_direction != QVP_RTP_DIR_TXRX)
							qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[audio_idx].external_stream_id,QVP_RTP_PAUSE,is_modem);					
					}

					qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[audio_idx].external_stream_id,QVP_RTP_CODEC_INIT,is_modem);
					qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[audio_idx].external_stream_id,QVP_RTP_CODEC_SET,is_modem);				
					qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[audio_idx].external_stream_id,QVP_RTP_START_CODEC,is_modem);	
				}
				break;
				
				default:
				{
					 QVP_RTP_MSG_HIGH_0("USER CTX corresponds to this app id is invalid");
				}
				break;
		    }	

			//call codec set for active stream id
			if((ret_val < QVP_RTP_MAX_USR_PVT_COUNT) && (ret_val != audio_idx))
			{
				if((g_audio_offload != 2 && g_audio_offload != 3 ) && (usr_ctx->usr_ctx_pvt[ret_val].stream_direction != QVP_RTP_DIR_TXRX))
					qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[ret_val].external_stream_id,QVP_RTP_PAUSE,is_modem);				
				
				qvp_rtp_push_pending_command(app_id, usr_ctx->usr_ctx_pvt[ret_val].external_stream_id,QVP_RTP_CODEC_SET,is_modem);
			}
		}
		else
		{
			QVP_RTP_MSG_HIGH_0("matching active stream id not found in private context");
		}
	}

	if(start_execution)
	{
		qvp_rtp_execute_pending_command(app_id);
	}
	else
	{
		if(current_audio_state == QVP_RTP_CODEC_STATE_NONE)
		{
			g_handoffstate = HO_NONE;
			QVP_RTP_MSG_HIGH_0("queue handoff commands:nothing to process so returning");
			( void ) rex_set_sigs( QVP_RTP_TASK_PTR, QVP_RTP_MSG_SIG );
		}
	}	
	return;
}

void qvp_rtp_set_handoff_status_cmd(
	qvp_rtp_set_handoff_status_msg_type         *set_handoff_status_msg
	)
{
	qvp_rtp_usr_ctx *usr_ctx; 
  qvp_rtp_stream_id_type strid;
  qvp_rtp_stream_type* stream;
  uint8 stream_no=0;

  QVP_RTP_MSG_HIGH("ENTER qvp_rtp_set_handoff_status_cmd on APPS",0,0,0);

	if ( !set_handoff_status_msg )
	{
		QVP_RTP_ERR_0( " qvp_rtp_set_handoff_status_cmd could not be processed \
					 because of wrong param\r\n");
		return;
	}
     usr_ctx = &rtp_ctx.user_array[ (intptr_t) set_handoff_status_msg->app_id ];
  if(usr_ctx->valid == FALSE)
  {
    QVP_RTP_MSG_HIGH("USER CTX corresponds to this app id is invalid",0,0,0);
    return;
  }
	if( ( set_handoff_status_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
		( set_handoff_status_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) )
	{  
		boolean b_is_in_call =FALSE;
		
		QVP_RTP_MSG_HIGH("qvp_rtp_set_handoff_status_cmd rat_type = %d HO_status: %d g_audio_offload:%d",
						 set_handoff_status_msg->rat_type,set_handoff_status_msg->ho_status_type,g_audio_offload);

		/*Update to AP about HO state and RAT*/
		if((set_handoff_status_msg->ho_status_type == QVP_RTP_HO_COMPLETE ) ||
           (set_handoff_status_msg->ho_status_type == QVP_RTP_HO_INIT ) ||
           (set_handoff_status_msg->ho_status_type == QVP_RTP_HO_FAIL ) )
		{
			qvp_rtp_service_set_handoff_status_req_msg(set_handoff_status_msg->app_id, 
													   set_handoff_status_msg->rat_type, 
													   set_handoff_status_msg->ho_status_type);
		}
  if(set_handoff_status_msg->ho_status_type == QVP_RTP_HO_COMPLETE )
		{
  	for( stream_no = 0; stream_no < usr_ctx->num_streams; stream_no++ )
  {
    /*----------------------------------------------------------------------
    See if this channel is open 
    ----------------------------------------------------------------------*/
	 if( usr_ctx->stream_array[ stream_no ].valid )
    {
		if ((qvp_rtp_get_media_type(set_handoff_status_msg->app_id ,usr_ctx->stream_array[stream_no].str_id) == QVP_RTP_AUDIO))
		{
			strid=usr_ctx->stream_array[stream_no].str_id;
			stream=&usr_ctx->stream_array[stream_no];
			stream->rat_type = set_handoff_status_msg->rat_type;
			qvp_rtp_send_rtcp_app_msg(set_handoff_status_msg->app_id,strid,set_handoff_status_msg->rat_type);
		}
	} 
  }

		  QVP_RTP_MSG_HIGH_0("qvp_rtp_set_handoff_status_cmd: metric - reset quality values ");
      if(rtp_ctx.metrics)
      {
		  rtp_ctx.metrics->edl_qlty = RAT_QUAL_INVALID;
		  rtp_ctx.metrics->eul_qlty = RAT_QUAL_INVALID;
		}
		}

		QVP_RTP_MSG_HIGH("qvp_rtp_set_handoff_status_cmd rat_type = %d HO_status: %d g_audio_offload:%d",
						 set_handoff_status_msg->rat_type,
						 set_handoff_status_msg->ho_status_type,
						 g_audio_offload);

		if( set_handoff_status_msg->rat_type == QVP_RTP_RAT_IWLAN && 
			set_handoff_status_msg->ho_status_type == QVP_RTP_HO_COMPLETE)
		{
			if(g_audio_offload != 2 && g_audio_offload != 3) // If its APP offload
			{
				boolean ret_val = FALSE;
				qvp_rtp_msgr_audio_rat_change_event_struct rat_change_evt;

				QVP_RTP_MSG_HIGH_0("Invoking qvp_rtp_ims_audio_rat_change_events_req");
				/*STOP LTE modem VSID*/
				rat_change_evt.eSubsID = QVP_RTP_MMODE_QMI_SYS_MODEM_AS_ID_1;
				rat_change_evt.ePassiveSessionTyp = QVP_RTP_MMODE_QMI_AUDIO_PASSIVE_SESSION_STOP;
				rat_change_evt.eVoiceSysMode = QVP_RTP_MMODE_QMI_VOICE_RAT_LTE;
				ret_val = qvp_rtp_ims_audio_rat_change_events_req( &rat_change_evt);

				if(!ret_val)
				{
					QVP_RTP_MSG_HIGH_0("qvp_rtp_set_handoff_status_cmd:failed to stop the AUDIO SESSION");
				}
				/*Start WLAN APPs VSID*/
				rat_change_evt.eSubsID = QVP_RTP_MMODE_QMI_SYS_MODEM_AS_ID_1;
				rat_change_evt.ePassiveSessionTyp = QVP_RTP_MMODE_QMI_AUDIO_PASSIVE_SESSION_START;
				rat_change_evt.eVoiceSysMode = QVP_RTP_MMODE_QMI_VOICE_RAT_WLAN;
				ret_val = qvp_rtp_ims_audio_rat_change_events_req(&rat_change_evt);


				if(!ret_val)
				{
					QVP_RTP_MSG_HIGH_0("qvp_rtp_set_handoff_status_cmd: failed to start the AUDIO SESSION");
				}

			}
			/**make  The RAT for all existing session as IWLAN**/
			{
				int index = 0;
				for(;index<QVP_RTP_MAX_USR_PVT_COUNT; index++)
				{
					if(usr_ctx->usr_ctx_pvt[index].valid)
					{
						usr_ctx->usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.rat_type = QVP_RTP_RAT_IWLAN;
						if(usr_ctx->usr_ctx_pvt[index].is_modem_stream_id_valid)
						{
							QVP_RTP_ERR_2("qvp_rtp_set_handoff_status_cmd: stream id:%d rat type:%d",usr_ctx->usr_ctx_pvt[index].modem_stream_id, QVP_RTP_RAT_IWLAN);
							usr_ctx->stream_array[(uint32) usr_ctx->usr_ctx_pvt[index].modem_stream_id].rat_type = QVP_RTP_RAT_IWLAN;
						}
						b_is_in_call = TRUE;
					}
				}

			}
			if(b_is_in_call  && g_audio_offload != 2)
			{
				QVP_RTP_MSG_HIGH_1("Hand off is complete, starting Call transfer. Current audio state: %d",codec_info_ctx.audio_codec_info.audio_state);
				g_handoffstate = HO_TRIGGERED;
				qvp_rtp_handoff_queue_commands(set_handoff_status_msg->app_id,TRUE);
			}
			else
			{
				QVP_RTP_MSG_HIGH_0("handoff_status_cmd: Hand off is complete,No call in progress. Return");
			}
		}
		else if( set_handoff_status_msg->rat_type == QVP_RTP_RAT_LTE && set_handoff_status_msg->ho_status_type == QVP_RTP_HO_COMPLETE)
		{
			if(g_audio_offload != 2  && g_audio_offload != 3)
			{
				boolean ret_val = FALSE;
				qvp_rtp_msgr_audio_rat_change_event_struct rat_change_evt;

				QVP_RTP_MSG_HIGH_0("Invoking qvp_rtp_ims_audio_rat_change_events_req");
				/*STOP LTE modem VSID*/
				rat_change_evt.eSubsID = QVP_RTP_MMODE_QMI_SYS_MODEM_AS_ID_1;
				rat_change_evt.ePassiveSessionTyp = QVP_RTP_MMODE_QMI_AUDIO_PASSIVE_SESSION_STOP;
				rat_change_evt.eVoiceSysMode = QVP_RTP_MMODE_QMI_VOICE_RAT_WLAN;
				ret_val = qvp_rtp_ims_audio_rat_change_events_req( &rat_change_evt);

				if(!ret_val)
				{
					QVP_RTP_MSG_HIGH_0("handoff_status:failed to stop the AUDIO SESSION");
					//qvp_rtp_response_handler( set_handoff_status_msg->app_id , (qvp_rtp_stream_id_type)0,(qvp_rtp_app_data_type)(NULL), QVP_RTP_MEDIA_AUDIO_ERROR_RSP, QVP_RTP_ERR_FATAL, TRUE, QVP_RTP_DIR_TXRX);
					//return;
				}
				/*Start WLAN APPs VSID*/
				rat_change_evt.eSubsID = QVP_RTP_MMODE_QMI_SYS_MODEM_AS_ID_1;
				rat_change_evt.ePassiveSessionTyp = QVP_RTP_MMODE_QMI_AUDIO_PASSIVE_SESSION_START;
				rat_change_evt.eVoiceSysMode = QVP_RTP_MMODE_QMI_VOICE_RAT_LTE;
				ret_val = qvp_rtp_ims_audio_rat_change_events_req(&rat_change_evt);

				if(!ret_val)
				{
					QVP_RTP_MSG_HIGH_0("handoff_status_cmd:failed to start the AUDIO SESSION");
					//qvp_rtp_response_handler( set_handoff_status_msg->app_id , (qvp_rtp_stream_id_type)0,(qvp_rtp_app_data_type)(NULL), QVP_RTP_MEDIA_AUDIO_ERROR_RSP, QVP_RTP_ERR_FATAL, TRUE, QVP_RTP_DIR_TXRX);
					//return;
				}
			}

			/**make  The RAT for all existing session as LTE**/
			{
				int index = 0;
				for(;index<QVP_RTP_MAX_USR_PVT_COUNT; index++)
				{
					if(usr_ctx->usr_ctx_pvt[index].valid)
					{
						usr_ctx->usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.rat_type = QVP_RTP_RAT_LTE;
						if(usr_ctx->usr_ctx_pvt[index].is_modem_stream_id_valid)
						{
							QVP_RTP_ERR_2("qvp_rtp_set_handoff_status_cmd: stream id:%d rat type:%d",usr_ctx->usr_ctx_pvt[index].modem_stream_id, QVP_RTP_RAT_LTE);
							usr_ctx->stream_array[(uint32) usr_ctx->usr_ctx_pvt[index].modem_stream_id].rat_type = QVP_RTP_RAT_LTE;
						}
						b_is_in_call = TRUE;
					}
				}
			}

			if(b_is_in_call  && g_audio_offload != 2)
			{
			QVP_RTP_MSG_HIGH_1("handoff_status_cmd: Hand off complete,starting Call transfer from AP to modem. Current audio state: %d",g_current_audio_state_on_ap);
			g_handoffstate = HO_TRIGGERED;
			qvp_rtp_handoff_queue_commands(set_handoff_status_msg->app_id,FALSE);			
			}
			else
			{
				QVP_RTP_MSG_HIGH_0("handoff_status: Hand off is complete,No call in progress. Return");
			}			

		}
		else
		{
			QVP_RTP_MSG_HIGH_2("qvp_rtp_set_handoff_status_cmd rat type %d, hand off status %d",set_handoff_status_msg->rat_type,set_handoff_status_msg->ho_status_type);
		}
	}

	return;
}

boolean qvp_rtp_push_pending_command(qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type stream_id, 
	qvp_rtp_command cmd, boolean is_modem)
{
	uint8 iQueue_count = 0;
	qvp_rtp_usr_ctx *usr_ctx; 

	//QVP_RTP_MSG_HIGH("qvp_rtp_push_pending_command: stream_id:%d, cmd:%d app_id %d", stream_id, cmd, app_id);

	if (( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
		( app_id > ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	{
		QVP_RTP_ERR_0( " qvp_rtp_push_pending_command could not be processed \
					 because of wrong param\r\n");
		return FALSE;
	}

	usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ];

	if(usr_ctx->valid == FALSE)
	{

		QVP_RTP_MSG_HIGH_0("USER CTX corresponds to this app id is invalid");
		return FALSE;
	}


	for(;iQueue_count < QVP_RTP_MAX_QUEUE_COUNT; iQueue_count++)
	{
		//QVP_RTP_MSG_HIGH_2("qvp_rtp_push_pending_command:iQueue_count %d--> cmd:%d", iQueue_count,usr_ctx->pending_queue[iQueue_count].external_stream_id);
		if(!usr_ctx->pending_queue[iQueue_count].valid)
		{
			//QVP_RTP_MSG_HIGH("qvp_rtp_push_pending_command: stream_id:%d, cmd:%d iQueue_count %d", stream_id, cmd, iQueue_count);
			break;
		}
	}

	if(iQueue_count == QVP_RTP_MAX_QUEUE_COUNT) 
	{
		QVP_RTP_ERR_0("qvp_rtp_push_pending_command: pending Queue not empty");
		return FALSE;
	}

	if(!usr_ctx->pending_queue[iQueue_count].valid)
	{
		memset(&usr_ctx->pending_queue[iQueue_count], 0, sizeof(qvp_rtp_pending_queue));
		usr_ctx->pending_queue[iQueue_count].external_stream_id = ( qvp_rtp_stream_id_type )stream_id;
		usr_ctx->pending_queue[iQueue_count].valid = TRUE;
		usr_ctx->pending_queue[iQueue_count].cmd = cmd;
		usr_ctx->pending_queue[iQueue_count].is_modem = is_modem;
		QVP_RTP_MSG_MED("qvp_rtp_push_pending_command: pushed stream_id:%d, cmd:%d iQueue_count %d", stream_id, cmd, iQueue_count);
		return FALSE;
	}
	return FALSE;
}

void qvp_rtp_execute_pending_command(qvp_rtp_app_id_type app_id)
{
	uint8 iCount = 0;
	uint8 iQueue_count = 0;
	uint8 iUsr_ctx_count = 0;
	qvp_rtp_command cmd;
	boolean codec_set = FALSE;
	qvp_rtp_usr_ctx *usr_ctx; 

	//QVP_RTP_MSG_HIGH_1("qvp_rtp_execute_pending_command: app_id %d", app_id);

	if (( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
		( app_id > ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	{
		QVP_RTP_ERR_2( " qvp_rtp_execute_pending_command could not be processed \
					 because of wrong param app_id:%d and num_users:%d\r\n", app_id, rtp_ctx.num_users);
		return;
	}

	usr_ctx = &rtp_ctx.user_array[(uint32)app_id ];

	if(usr_ctx->valid == FALSE)
	{
		QVP_RTP_MSG_HIGH_0("USER CTX corresponds to this app id is invalid");
		return;
	}

	if(!usr_ctx->pending_queue[iQueue_count].valid && usr_ctx->pending_queue[iQueue_count].cmd == QVP_RTP_CODEC_INIT)
	{
		codec_set = TRUE;
	}


	if(!usr_ctx->pending_queue[iQueue_count].valid)
	{
		int temp_index=0;


		for(iQueue_count = 1; iQueue_count < QVP_RTP_MAX_QUEUE_COUNT; iQueue_count++)
		{
			if(usr_ctx->pending_queue[iQueue_count].valid == TRUE)
			{        
				qvp_rtp_memscpy(&usr_ctx->pending_queue[temp_index], sizeof(qvp_rtp_pending_queue), &usr_ctx->pending_queue[iQueue_count], sizeof(qvp_rtp_pending_queue));
				memset(&usr_ctx->pending_queue[iQueue_count], 0, sizeof(qvp_rtp_pending_queue));
				temp_index++;
			}
		}
	}
	if(codec_set)
	{
		QVP_RTP_MSG_HIGH_1("execute_pending_command: dummy codec set. app_id %d", app_id);				
		return;
	}

	iQueue_count = 0;
	if(usr_ctx->pending_queue[iQueue_count].valid )
	{	
		cmd = usr_ctx->pending_queue[iQueue_count].cmd;
		{
			QVP_RTP_MSG_HIGH("execute_pending_command: pending cmd = %d, external stream id: %d is_modem = %d",cmd, usr_ctx->pending_queue[iQueue_count].external_stream_id,usr_ctx->pending_queue[iQueue_count].is_modem);

			for( iUsr_ctx_count = 0; iUsr_ctx_count < QVP_RTP_MAX_USR_PVT_COUNT; iUsr_ctx_count++)
			{
				if( usr_ctx->usr_ctx_pvt[iUsr_ctx_count].valid  && usr_ctx->pending_queue[iQueue_count].external_stream_id == usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id )
					break;
			}
      		QVP_RTP_MSG_MED_2("execute_pending_command: modem_stream_id = %d,  apps_stream id: %d",usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);
			switch(cmd)
			{

				qvp_rtp_msg_type	rtp_msg;      

			case QVP_RTP_OPEN:
				{
					rtp_msg.msg_type = QVP_RTP_OPEN2_CMD;
					qvp_rtp_memscpy(&rtp_msg.msg_param.open2_msg, sizeof(qvp_rtp_open2_msg_type), &usr_ctx->usr_ctx_pvt[iUsr_ctx_count].open2_msg, sizeof(qvp_rtp_open2_msg_type));
			
					rtp_msg.msg_param.open2_msg.open_config_param.stream_params.app_data = (qvp_rtp_app_data_type) usr_ctx->pending_queue[iQueue_count].external_stream_id;
					if(usr_ctx->pending_queue[iQueue_count].is_modem)
					{
						qvp_rtp_process_app_cmd(&rtp_msg);
					}
					else
					{
						qvp_rtp_process_qmi_request(&rtp_msg);
					}
				}
				break;
			case QVP_RTP_CONFIGURE:
				{
					rtp_msg.msg_type = QVP_RTP_CONFIG_CMD;
					qvp_rtp_memscpy(&rtp_msg.msg_param.config_msg, sizeof(qvp_rtp_config_msg_type), &usr_ctx->usr_ctx_pvt[iUsr_ctx_count].config_msg, sizeof(qvp_rtp_config_msg_type)); 

					if(usr_ctx->pending_queue[iQueue_count].is_modem)
					{
						//QVP_RTP_MSG_HIGH("execute_pending_command : sending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);
						rtp_msg.msg_param.config_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id;
						qvp_rtp_process_app_cmd(&rtp_msg);
					}
					else
					{
						//QVP_RTP_MSG_HIGH("execute_pending_command : sending cmd = %d,  external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);
						rtp_msg.msg_param.config_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id;
						qvp_rtp_process_qmi_request(&rtp_msg);

					}

				}
				break;

			case QVP_RTP_CONFIGURE_SESSION:
				{
					if(usr_ctx->usr_ctx_pvt[iUsr_ctx_count].config_session_msg.config.rtcp_report_interval)
					{
						rtp_msg.msg_type = QVP_RTP_CONFIG_SESSION_CMD;
						qvp_rtp_memscpy(&rtp_msg.msg_param.config_session_msg, sizeof(qvp_rtp_config_msg_type), &usr_ctx->usr_ctx_pvt[iUsr_ctx_count].config_session_msg, sizeof(qvp_rtp_config_msg_type)); 
						QVP_RTP_MSG_HIGH_1("execute_pending_command: Calling QVP_RTP_CONFIGURE_SESSION . rtcp interval:%d", usr_ctx->usr_ctx_pvt[iUsr_ctx_count].config_session_msg.config.rtcp_report_interval );

						if(usr_ctx->pending_queue[iQueue_count].is_modem)
						{
							//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);
							rtp_msg.msg_param.config_session_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id;
							qvp_rtp_process_app_cmd(&rtp_msg);
						}
						else
						{
							//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d,  external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);
							rtp_msg.msg_param.config_session_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id;
							qvp_rtp_process_qmi_request(&rtp_msg);
						}
					}
					else
					{
						QVP_RTP_MSG_HIGH_0("qvp_rtp_execute_pending_command: not calling QVP_RTP_CONFIGURE_SESSION . rtcp interval is 0");
						rtp_ctx.user_array[(uint32)app_id].pending_queue[0].valid = FALSE;
						qvp_rtp_execute_pending_command(app_id);				
					}			
				}
				break;		

			case QVP_RTP_CODEC_INIT:
				{
					rtp_msg.msg_type = QVP_RTP_CODEC_INIT_CMD;
					rtp_msg.msg_param.media_codec_init_msg.app_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].open2_msg.app_id;

					qvp_rtp_memscpy(&rtp_msg.msg_param.media_codec_init_msg, sizeof(qvp_rtp_media_codec_init_msg_type), &usr_ctx->usr_ctx_pvt[iUsr_ctx_count].codec_init_msg, sizeof(qvp_rtp_media_codec_init_msg_type)); 

					if(usr_ctx->pending_queue[iQueue_count].is_modem)
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);			
						rtp_msg.msg_param.media_codec_init_msg.stream_id= usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id;
						qvp_rtp_process_app_cmd(&rtp_msg);
					}
					else
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);				
						rtp_msg.msg_param.media_codec_init_msg.stream_id= usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id;
						qvp_rtp_process_qmi_request(&rtp_msg);
					}

				}
				break;
			case QVP_RTP_CODEC_SET:
				{
					rtp_msg.msg_type = QVP_RTP_MEDIA_CODEC_CONFIG_CMD;
					rtp_msg.msg_param.media_codec_config_msg.app_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].open2_msg.app_id; 
					qvp_rtp_memscpy(&rtp_msg.msg_param.media_codec_config_msg, sizeof(qvp_rtp_media_codec_config_msg_type), &usr_ctx->usr_ctx_pvt[iUsr_ctx_count].codec_init_msg, sizeof(qvp_rtp_media_codec_init_msg_type)); 

					if(usr_ctx->pending_queue[iQueue_count].is_modem)
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);			
						rtp_msg.msg_param.media_codec_config_msg.stream_id= usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id;
						qvp_rtp_process_app_cmd(&rtp_msg);
					}
					else
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);				
						rtp_msg.msg_param.media_codec_config_msg.stream_id= usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id;
						qvp_rtp_process_qmi_request(&rtp_msg);
					}

				}
				break;
			case QVP_RTP_START_CODEC:
				{
					rtp_msg.msg_type = QVP_RTP_START_STREAM_CMD;

					rtp_msg.msg_param.media_start_stream_msg.app_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].open2_msg.app_id; 

					/*TBD to take the direction from stream state*/
					rtp_msg.msg_param.media_start_stream_msg.direction = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].stream_direction;
					QVP_RTP_MSG_HIGH_1("qvp_rtp_execute_pending_command:codec QVP_RTP_START_CODEC. direction %d", usr_ctx->usr_ctx_pvt[iUsr_ctx_count].stream_direction);
					rtp_msg.msg_param.media_start_stream_msg.direction = QVP_RTP_DIR_TXRX;

					if(usr_ctx->pending_queue[iQueue_count].is_modem)
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);			
						rtp_msg.msg_param.media_start_stream_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id;
						qvp_rtp_process_app_cmd(&rtp_msg);
					}
					else
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);				
						rtp_msg.msg_param.media_start_stream_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id;
						qvp_rtp_process_qmi_request(&rtp_msg);
					}

				}
				break;
			case QVP_RTP_STOP_CODEC:
				{
					rtp_msg.msg_type = QVP_RTP_STOP_STREAM_CMD;
					rtp_msg.msg_param.media_stop_stream_msg.app_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].open2_msg.app_id; 

					rtp_msg.msg_param.media_stop_stream_msg.direction = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].stream_direction;

					//QVP_RTP_MSG_HIGH_1("qvp_rtp_execute_pending_command:codec QVP_RTP_STOP_STREAM_CMD. direction %d", usr_ctx->usr_ctx_pvt[iUsr_ctx_count].stream_direction);

					/*TBD to take the direction from stream state*/
					rtp_msg.msg_param.media_stop_stream_msg.direction = QVP_RTP_DIR_TXRX;
					if(usr_ctx->pending_queue[iQueue_count].is_modem)
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);						
						rtp_msg.msg_param.media_stop_stream_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id;
						//QVP_RTP_MSG_HIGH_1("qvp_rtp_sl_execute_pending_command: stream_id :%d", rtp_msg.msg_param.media_stop_stream_msg.stream_id);
						qvp_rtp_process_app_cmd(&rtp_msg);
					}
					else
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);							
						rtp_msg.msg_param.media_stop_stream_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id;
						//QVP_RTP_MSG_HIGH_1("qvp_rtp_sl_execute_pending_command: cmd unhandled :%d", usr_ctx->pending_queue[iCount].cmd);
						qvp_rtp_process_qmi_request(&rtp_msg);
					}

				}
				break;
			case QVP_RTP_CODEC_RELEASE:
				{
					rtp_msg.msg_type = QVP_RTP_MEDIA_CODEC_RELEASE_CMD;
					rtp_msg.msg_param.media_codec_release_msg.app_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].open2_msg.app_id; 
					if(usr_ctx->pending_queue[iQueue_count].is_modem)
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);			
						rtp_msg.msg_param.media_codec_release_msg.media_type = QVP_RTP_AUDIO;
						qvp_rtp_process_app_cmd(&rtp_msg);
					}
					else
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);				
						rtp_msg.msg_param.media_codec_release_msg.media_type = QVP_RTP_AUDIO; /*This APi will only be called only in case of HO and we are moving only audio stream from AP to modem or modem to AP*/
      					qvp_rtp_service_media_codec_release_req_msg( rtp_msg.msg_param.media_codec_release_msg.app_id,
						        rtp_msg.msg_param.media_codec_release_msg.media_type, TRUE);						
						//qvp_rtp_process_qmi_request(&rtp_msg);
					}

				}
				break;
			case QVP_RTP_CLOSE_SESSION:
				{
					rtp_msg.msg_type = QVP_RTP_CLOSE_CMD;
					rtp_msg.msg_param.close_msg.app_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].open2_msg.app_id; 
					if(usr_ctx->pending_queue[iQueue_count].is_modem)
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);			
						rtp_msg.msg_param.close_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id;
						qvp_rtp_process_app_cmd(&rtp_msg);
					}
					else
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);				
						rtp_msg.msg_param.close_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id;
						qvp_rtp_process_qmi_request(&rtp_msg);
					}				
				}
				break;
			case QVP_RTP_PAUSE:
				{
					rtp_msg.msg_type = QVP_RTP_PAUSE_STREAM_CMD;
					rtp_msg.msg_param.media_pause_stream_msg.app_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].open2_msg.app_id; 
					switch(usr_ctx->usr_ctx_pvt[iUsr_ctx_count].stream_direction)
					{
						case QVP_RTP_DIR_TX:
							rtp_msg.msg_param.media_pause_stream_msg.direction = QVP_RTP_DIR_RX;
							break;
						case QVP_RTP_DIR_RX:
							rtp_msg.msg_param.media_pause_stream_msg.direction = QVP_RTP_DIR_TX;
							break;
						case QVP_RTP_INV_DIR:
							rtp_msg.msg_param.media_pause_stream_msg.direction = QVP_RTP_DIR_TXRX;
							break;
						default:
							QVP_RTP_MSG_HIGH_1("qvp_rtp_execute_pending_command:codec QVP_RTP_PAUSE_STREAM_CMD. invalid direction %d. Not pausing", usr_ctx->usr_ctx_pvt[iUsr_ctx_count].stream_direction);
							return;							
					}
					QVP_RTP_MSG_HIGH_1("qvp_rtp_execute_pending_command:codec QVP_RTP_PAUSE_STREAM_CMD. direction %d", rtp_msg.msg_param.media_pause_stream_msg.direction);


					if(usr_ctx->pending_queue[iQueue_count].is_modem)
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : pending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);			
						rtp_msg.msg_param.media_pause_stream_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id;
						qvp_rtp_process_app_cmd(&rtp_msg);
					}
					else
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : pending cmd = %d, external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);				
						rtp_msg.msg_param.media_pause_stream_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id;
						qvp_rtp_process_qmi_request(&rtp_msg);
					}

				}
				break;

			case QVP_RTP_RESUME:
				{
					uint8 active_queue_index = 0;
					rtp_msg.msg_type = QVP_RTP_RESUME_STREAM_CMD;
					rtp_msg.msg_param.media_resume_stream_msg.app_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].open2_msg.app_id; 

					for(;active_queue_index<QVP_RTP_MAX_USR_PVT_COUNT;active_queue_index++)
					{
						if(usr_ctx->usr_ctx_pvt[active_queue_index].valid && usr_ctx->active_stream_id == usr_ctx->usr_ctx_pvt[active_queue_index].external_stream_id)
							break;
					}

					rtp_msg.msg_param.media_resume_stream_msg.direction = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].stream_direction;
					QVP_RTP_MSG_HIGH_1("qvp_rtp_execute_pending_command:codec QVP_RTP_RESUME_STREAM_CMD. direction %d", usr_ctx->usr_ctx_pvt[iUsr_ctx_count].stream_direction);


					if(usr_ctx->pending_queue[iQueue_count].is_modem)
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : pending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);			
						rtp_msg.msg_param.media_resume_stream_msg.stream_id = usr_ctx->usr_ctx_pvt[active_queue_index].external_stream_id;
						qvp_rtp_process_app_cmd(&rtp_msg);
					}
					else
					{
						//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : pending cmd = %d, external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);			
						rtp_msg.msg_param.media_resume_stream_msg.stream_id = usr_ctx->usr_ctx_pvt[active_queue_index].apps_stream_id;
						qvp_rtp_process_qmi_request(&rtp_msg);
					}

				}
				break;


			case QVP_RTP_LINK_ALIVE:
				{
					if(usr_ctx->usr_ctx_pvt[iUsr_ctx_count].rtp_link_alive_timer_msg.rtp_link_alive_time)
					{
						rtp_msg.msg_type = QVP_RTP_LINK_ALIVE_TIMER_CMD;
						rtp_msg.msg_param.rtp_link_alive_timer_msg.app_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].rtp_link_alive_timer_msg.app_id; 
						rtp_msg.msg_param.rtp_link_alive_timer_msg.rtp_link_alive_time = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].rtp_link_alive_timer_msg.rtp_link_alive_time; 
						QVP_RTP_MSG_MED_1("qvp_rtp_sl_execute_pending_command: QVP_RTP_LINK_ALIVE_TIME: %d", usr_ctx->usr_ctx_pvt[iUsr_ctx_count].rtp_link_alive_timer_msg.rtp_link_alive_time);

						if(usr_ctx->pending_queue[iQueue_count].is_modem)
						{
							//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);			
							rtp_msg.msg_param.rtp_link_alive_timer_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id;
							qvp_rtp_process_app_cmd(&rtp_msg);
						}
						else
						{
							//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);				
							rtp_msg.msg_param.rtp_link_alive_timer_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id;
							qvp_rtp_process_qmi_request(&rtp_msg);
						}		
					}
					else
					{
						QVP_RTP_MSG_MED_0("qvp_rtp_sl_execute_pending_command: QVP_RTP_LINK_ALIVE_TIME is 0");
						rtp_ctx.user_array[(uint32)app_id].pending_queue[0].valid = FALSE;
						qvp_rtp_execute_pending_command(app_id);					
					}
				}
				break;

			case QVP_RTCP_LINK_ALIVE:
				{
					if(usr_ctx->usr_ctx_pvt[iUsr_ctx_count].rtcp_link_alive_timer_msg.rtcp_link_alive_time)
					{
						rtp_msg.msg_type = QVP_RTCP_LINK_ALIVE_TIMER_CMD;
						rtp_msg.msg_param.rtcp_link_alive_timer_msg.app_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].rtcp_link_alive_timer_msg.app_id; 
						rtp_msg.msg_param.rtcp_link_alive_timer_msg.rtcp_link_alive_time = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].rtcp_link_alive_timer_msg.rtcp_link_alive_time; 
						QVP_RTP_MSG_MED_1("qvp_rtp_sl_execute_pending_command: QVP_RTCP_LINK_ALIVE_TIME: %d", usr_ctx->usr_ctx_pvt[iUsr_ctx_count].rtcp_link_alive_timer_msg.rtcp_link_alive_time);

						if(usr_ctx->pending_queue[iQueue_count].is_modem)
						{
							//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d modem_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].modem_stream_id);			
							rtp_msg.msg_param.rtcp_link_alive_timer_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id;
							qvp_rtp_process_app_cmd(&rtp_msg);
						}
						else
						{
							//QVP_RTP_MSG_HIGH("qvp_rtp_execute_pending_command : sending cmd = %d, external stream id: %d apps_stream_id = %d",cmd, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].external_stream_id, usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id);				
							rtp_msg.msg_param.rtcp_link_alive_timer_msg.stream_id = usr_ctx->usr_ctx_pvt[iUsr_ctx_count].apps_stream_id;
							qvp_rtp_process_qmi_request(&rtp_msg);
						}		
					}
					else
					{
						QVP_RTP_MSG_MED_0("qvp_rtp_sl_execute_pending_command: QVP_RTCP_LINK_ALIVE_TIME is 0");
						rtp_ctx.user_array[(uint32)app_id].pending_queue[0].valid = FALSE;
						qvp_rtp_execute_pending_command(app_id);					
					}
				}
				break;

			default:
				{
					QVP_RTP_MSG_HIGH_1("qvp_rtp_sl_execute_pending_command: cmd unhandled :%d", usr_ctx->pending_queue[iCount].cmd);
					rtp_ctx.user_array[(uint32)app_id].pending_queue[0].valid = FALSE;
					qvp_rtp_post_execute_pending_command(app_id);			  
				}
			}
		}

		QVP_RTP_MSG_HIGH_2("qvp_rtp_execute_pending_command: executed  queue valid:%d cmd:%d", usr_ctx->pending_queue[iQueue_count].valid, cmd);
	}
	else
	{
		g_handoffstate = HO_NONE;
		( void ) rex_set_sigs( QVP_RTP_TASK_PTR, QVP_RTP_MSG_SIG );
		QVP_RTP_MSG_MED_0("qvp_rtp_sl_execute_pending_command: executed all commands");
	}
}
void qvp_rtp_video_quality_response
	(
	                                   qvp_rtp_app_id_type    app_id,		/* application which opene the stream*/
										qvp_rtp_stream_id_type	stream_id,	 /* stream which was opened */	
										qvp_rtp_video_quality video_quality, /*video quality*/
                                        uint16 reason_code /*reason for video quality*/
										)
{
	qvp_rtp_stream_id_type streamid = stream_id;
	qvp_rtp_usr_ctx *usr_ctx; 
	QVP_RTP_MSG_HIGH("qvp_rtp_video_quality_response: sending video_quality:%d reason_code:%d", video_quality, reason_code, 0);
	 if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
    ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) )
  	{
  		QVP_RTP_ERR_1("qvp_rtp_response_handler: Invalid App id:%d",app_id);
		return;
  	}

	usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ];
	streamid = qvp_rtp_find_external_streamid ( app_id, stream_id, FALSE);
	QVP_RTP_MSG_HIGH_1("qvp_rtp_video_quality_response: external stream id: %d", streamid);	
if(usr_ctx->call_backs.video_quality_cb)
			usr_ctx->call_backs.video_quality_cb(
                                app_id,
				streamid,
				video_quality,
				reason_code);	  
}
void qvp_rtp_response_handler
	(
	qvp_rtp_app_id_type    app_id,		/* application which opened
										* the stream
										*/
										qvp_rtp_stream_id_type	stream_id,	 /* stream which was opened */										 
										qvp_rtp_app_data_type  app_data,
										qvp_rtp_response_type rsp_type, /* Status of Handover */ 	
										qvp_rtp_status_type  status,	
										boolean				  is_modem,  /* Is the response form modem*/
										qvp_rtp_stream_direction direction
										)
{
	qvp_rtp_stream_id_type streamid = stream_id;
	qvp_rtp_usr_ctx *usr_ctx; 
	int index = 0;

	QVP_RTP_MSG_MED("qvp_rtp_response_handler: sending rsp:%d internal_stream_id:%d staus: %d", rsp_type, streamid, status);
	QVP_RTP_MSG_HIGH("qvp_rtp_response_handler: sending app_id:%d direction:%d is_modem: %d", app_id, direction, is_modem);

  if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
    ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) )
  	{
  		QVP_RTP_ERR_1("qvp_rtp_response_handler: Invalid App id:%d",app_id);
		return;
  	}
  
	usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ];
	
	if(!usr_ctx)
	{
		QVP_RTP_ERR_0("invalid usr_ctx");
		return;		
	}
	
	if( stream_id == ( qvp_rtp_stream_id_type ) DEAD_BEEF ) 
  	{
  		/*Raise error to Qipcall*/
  		QVP_RTP_ERR_0("qvp_rtp_response_handler: Invalid stream id");
		usr_ctx->call_backs.handle_media_res(app_id, streamid, QVP_RTP_MEDIA_AUDIO_ERROR, status, direction);	  
				
		/*Handoff in progress. Delete the Pending queue entries and reset the flag*/
		if(g_handoffstate == HO_IN_PROGRESS)
    	{
			uint8 iQueue_count = 0;	
			g_handoffstate = HO_NONE;
	  
			for(iQueue_count = 0; iQueue_count < QVP_RTP_MAX_QUEUE_COUNT; iQueue_count++)
	  		{
				memset(&usr_ctx->pending_queue[iQueue_count], 0, sizeof(qvp_rtp_pending_queue));
			}
	  	}
		return;
  	}
		
	//QVP_RTP_MSG_HIGH_1("qvp_rtp_response_handler: sending direction %d", direction);
	if( rsp_type == QVP_RTP_MEDIA_AUDIO_CODEC_RELEASE_RSP
		|| rsp_type == QVP_RTP_MEDIA_VIDEO_CODEC_RELEASE_RSP
		|| rsp_type == QVP_RTP_MEDIA_VIDEO_ERROR_RSP
		|| rsp_type == QVP_RTP_MEDIA_AUDIO_ERROR_RSP
		|| rsp_type == QVP_RTP_MEDIA_TEXT_CODEC_RELEASE_RSP
		|| rsp_type == QVP_RTP_MEDIA_ERROR_RSP
		||rsp_type == QVP_RTP_MEDIA_STOP_STREAM_RSP
		|| rsp_type == QVP_RTP_MEDIA_CODEC_CONFIG_RSP
		|| rsp_type == QVP_RTP_MEDIA_CODEC_INIT_RSP
		|| rsp_type == QVP_RTP_MEDIA_START_STREAM_RSP
		)
	{
		for(index = 0; index < QVP_RTP_MAX_USR_PVT_COUNT; index ++)
		{
			if(is_modem && rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].modem_stream_id == stream_id &&
				rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].pending_ind_ref_cnt)
			{
				QVP_RTP_ERR_2("qvp_rtp_response_handler: external stream id: %d pending_ind_ref_cnt:%d", rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].external_stream_id, rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].pending_ind_ref_cnt);
				usr_ctx->usr_ctx_pvt[index].pending_ind_ref_cnt -= QVP_RTP_AUDIO_IND_CNT; // modem indication assumed to be audio only
				streamid = rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].external_stream_id;
				break;
			}
			else if(!is_modem && rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].apps_stream_id == stream_id &&
				rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].pending_ind_ref_cnt)
			{
				QVP_RTP_ERR_2("qvp_rtp_response_handler: external stream id: %d pending_ind_ref_cnt:%d", rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].external_stream_id, rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].pending_ind_ref_cnt);
				 if(rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_RAW_AUD ||
					rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_AMR ||
					rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_EVS ||
					rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_AMR_WB)
			{
					usr_ctx->usr_ctx_pvt[index].pending_ind_ref_cnt -= QVP_RTP_AUDIO_IND_CNT; 
				 }
				 else if(rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_RAW_VID ||
						rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_H264 ||
						rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_H263)
				 {
				 	if(direction == QVP_RTP_DIR_TXRX)
				 		usr_ctx->usr_ctx_pvt[index].pending_ind_ref_cnt -= QVP_RTP_VIDEO_IND_CNT; 
				 	else if( (direction == QVP_RTP_DIR_TX) || (direction == QVP_RTP_DIR_RX) )
				 		usr_ctx->usr_ctx_pvt[index].pending_ind_ref_cnt -= QVP_RTP_VIDEO_TX_IND_CNT; 
				 }		
				streamid = rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].external_stream_id;
				break;
			}			 
		}
		QVP_RTP_MSG_HIGH_2("qvp_rtp_response_handler: external stream id: %d for rsp:%d", streamid, rsp_type);
	}

	else if(rsp_type!= QVP_RTP_OPEN_RSP)
	{
		streamid = qvp_rtp_find_external_streamid ( app_id, stream_id, is_modem);
	QVP_RTP_MSG_HIGH_2("qvp_rtp_response_handler: external stream id: %d for rsp:%d", streamid, rsp_type);
	}

	switch(rsp_type)
	{
		QVP_RTP_MSG_MED("qvp_rtp_response_handler: sending rsp:%d external_stream_id:%d staus: %d", rsp_type, streamid, status);
	case	QVP_RTP_OPEN_RSP:
		{
			if(usr_ctx->call_backs.open_cb)
			{
			usr_ctx->call_backs.open_cb(
				app_id,
				app_data,
				status, 
				streamid); 
		}
			if( status != QVP_RTP_SUCCESS)
			{
				/* Clear the pvt context for the failed extry */
				qvp_rtp_clear_pvt_ctx_on_error(app_id, app_data);
			}
		}
		break;

	case	QVP_RTP_CLOSE_RSP:
		{
			int index = 0;
			if(usr_ctx->call_backs.close_cb)
			{
      usr_ctx->call_backs.close_cb (
        app_id,
        app_data,
        status,
        streamid );
			}
			/*clear user context private for this stream id*/
			for(;index<QVP_RTP_MAX_USR_PVT_COUNT;index++)
			{
				if( usr_ctx->usr_ctx_pvt[index].valid && streamid == usr_ctx->usr_ctx_pvt[index].external_stream_id )
				{
					//memset( &rtp_ctx.user_array[index].usr_ctx_pvt, 0, sizeof(qvp_rtp_usr_ctx_pvt));
					usr_ctx->usr_ctx_pvt[index].valid = FALSE;
					usr_ctx->usr_ctx_pvt[index].is_config_msg = FALSE;
					break;
				}
			}

		}
		break;


	case	QVP_RTP_CONFIG_RSP:
		{
			if(usr_ctx->call_backs.config_cb)
			{
			usr_ctx->call_backs.config_cb(
				app_id,
				app_data,  
				status, 
				streamid );
		}
		}
		break;

	case QVP_RTP_CONFIG_SESSION_RSP:
		{
			if(usr_ctx->call_backs.handle_rtp_config_session_cb)
			{
			usr_ctx->call_backs.handle_rtp_config_session_cb(
				app_id,
				status);
		}
		}
		break;		

	case	QVP_RTP_LINK_ALIVE_TIMER_RSP:
		{
			if(usr_ctx->call_backs.handle_rtp_time_out)
			{
			usr_ctx->call_backs.handle_rtp_time_out(
				app_id,
				streamid);	  
		}
                }
		break;

	case	QVP_RTCP_LINK_ALIVE_TIMER_RSP:
		{
			if(usr_ctx->call_backs.handle_rtcp_time_out)
			{
			usr_ctx->call_backs.handle_rtcp_time_out(
				app_id,
				streamid);	  
		}
		}
		break;


	case	QVP_RTP_MEDIA_CODEC_INIT_RSP:
		{
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				app_id,
				streamid,
				QVP_RTP_MEDIA_CODEC_INIT,
				status,
				direction);	  
		}
		}
		break;		

	case	QVP_RTP_MEDIA_CODEC_CONFIG_RSP:
		{
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				app_id,
				streamid,
				QVP_RTP_MEDIA_CODEC_CONFIG,
				status,
				direction);	  
		}
		}
		break;		

	case	QVP_RTP_MEDIA_AUDIO_CODEC_RELEASE_RSP:
		{
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				app_id,
				NULL,
				QVP_RTP_MEDIA_AUDIO_CODEC_RELEASE,
				status,
				direction);	  
		}
		}
		break;		

	case	QVP_RTP_MEDIA_VIDEO_CODEC_RELEASE_RSP:
		{
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				NULL,
				NULL,
				QVP_RTP_MEDIA_VIDEO_CODEC_RELEASE,
				status,
				direction);	  
		}
		}
		break;

	case	QVP_RTP_MEDIA_START_STREAM_RSP:
		{
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				app_id,
				streamid,
				QVP_RTP_MEDIA_START_STREAM,
				status,
				direction);	
			}
			QVP_RTP_MSG_HIGH_1("qvp_rtp_response_handler: QVP_RTP_MEDIA_START_STREAM_RSP external stream id: %d", streamid);
		}
		break;

	case	QVP_RTP_MEDIA_STOP_STREAM_RSP:
		{
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				app_id,
				streamid,
				QVP_RTP_MEDIA_STOP_STREAM,
				status,
				direction);	  
		}
		}
		break;

	case	QVP_RTP_MEDIA_ERROR_RSP:
		{
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				app_id,
				streamid,
				QVP_RTP_MEDIA_ERROR,
				status,
				direction);	  
		}
		}
		break;

	case	QVP_RTP_MEDIA_FIRST_PKT_RCVD_RSP:
		{
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				app_id,
				streamid,
				QVP_RTP_MEDIA_FIRST_PKT_RCVD,
				status,
				direction);	  
		}
		}
		break;

	case	QVP_RTCP_MEDIA_FIRST_PKT_RCVD_RSP:
		{
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				app_id,
				streamid,
				QVP_RTCP_MEDIA_FIRST_PKT_RCVD,
				status,
				direction);	  
		}
		}
		break;

	case	QVP_RTP_MEDIA_TEXT_CODEC_RELEASE_RSP:
		{
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				app_id,
				streamid,
				QVP_RTP_MEDIA_TEXT_CODEC_RELEASE,
				status,
				direction);	  
		}
		}
		break;

	case	QVP_RTP_MEDIA_AUDIO_ERROR_RSP:
		{
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				app_id,
				streamid,
				QVP_RTP_MEDIA_AUDIO_ERROR,
				status,
				direction);	  
			}
				
			/*Handoff in progress. Delete the Pending queue entries and reset the flag*/
			if(g_handoffstate == HO_IN_PROGRESS)
	    	{
				uint8 iQueue_count = 0;	
				g_handoffstate = HO_NONE;
		  
				for(iQueue_count = 0; iQueue_count < QVP_RTP_MAX_QUEUE_COUNT; iQueue_count++)
		  		{
					memset(&usr_ctx->pending_queue[iQueue_count], 0, sizeof(qvp_rtp_pending_queue));
				}
		  	}				
		}
		break;

	case	QVP_RTP_MEDIA_VIDEO_ERROR_RSP:
		{
				
				uint8 iQueue_count = 0;	
				g_handoffstate = HO_NONE;
		  
				for(iQueue_count = 0; iQueue_count < QVP_RTP_MAX_QUEUE_COUNT; iQueue_count++)
		  		{
					memset(&usr_ctx->pending_queue[iQueue_count], 0, sizeof(qvp_rtp_pending_queue));
				}
			if(usr_ctx->call_backs.handle_media_res)
			{
			usr_ctx->call_backs.handle_media_res(
				0,
				0,
				QVP_RTP_MEDIA_VIDEO_ERROR,
				status,
				direction);			
			}
		}
		break;
	case QVP_RTP_MEDIA_PER_RSP:
		{
			if(usr_ctx->call_backs.handle_per_cb)
			{
			usr_ctx->call_backs.handle_per_cb(app_id,0,0);
			}	
			QVP_RTP_ERR_2("qvp_rtp_response_handler: tot_pkt_cnt:%lu and cum_lost:%lu",0,0);
		}
		break;
	case QVP_RTP_RX_DTMF:
		{
			if(usr_ctx->call_backs.dtmf_evt_cb)
			{
			usr_ctx->call_backs.dtmf_evt_cb(app_id, streamid, (qvp_rtp_dtmf_payload_type*)app_data);
		}
		}
		break;
	default:
		{
			QVP_RTP_ERR_0("qvp_rtp_response_handler: unhandled response");
		}

		return;
	}
}


qvp_rtp_stream_id_type qvp_rtp_find_external_streamid ( qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type stream_id, boolean is_modem)
{
	uint8 ret_val = 0;
	qvp_rtp_usr_ctx *usr_ctx; 

	//QVP_RTP_MSG_HIGH("qvp_rtp_find_external_streamid: app_id:%d stream_id:%d is_modem: %d", app_id, stream_id, is_modem);

	if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
	  ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	  {
		  QVP_RTP_ERR_1("qvp_rtp_find_external_streamid: Invalid AppId:%d",app_id);
		  return (qvp_rtp_stream_id_type)DEAD_BEEF;
	  }
	usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ];

	if(is_modem)
	{
		for(;ret_val<QVP_RTP_MAX_USR_PVT_COUNT;ret_val++)
		{
			QVP_RTP_MSG_LOW("qvp_rtp_find_external_streamid: valid:%d is_modem_stream_id_valid:%d modem_stream_id: %d", usr_ctx->usr_ctx_pvt[ret_val].valid, usr_ctx->usr_ctx_pvt[ret_val].is_modem_stream_id_valid, usr_ctx->usr_ctx_pvt[ret_val].modem_stream_id);
			if(usr_ctx->usr_ctx_pvt[ret_val].valid && usr_ctx->usr_ctx_pvt[ret_val].is_modem_stream_id_valid && stream_id == usr_ctx->usr_ctx_pvt[ret_val].modem_stream_id)
				break;
		}
	}
	else
	{
		for(;ret_val<QVP_RTP_MAX_USR_PVT_COUNT;ret_val++)
		{
			QVP_RTP_MSG_LOW("qvp_rtp_find_external_streamid: valid:%d is_apps_stream_id_valid:%d apps_stream_id: %d", usr_ctx->usr_ctx_pvt[ret_val].valid, usr_ctx->usr_ctx_pvt[ret_val].is_apps_stream_id_valid, usr_ctx->usr_ctx_pvt[ret_val].apps_stream_id);
			if(usr_ctx->usr_ctx_pvt[ret_val].valid && usr_ctx->usr_ctx_pvt[ret_val].is_apps_stream_id_valid && stream_id == usr_ctx->usr_ctx_pvt[ret_val].apps_stream_id)
				break;
		}	
	}
	if(ret_val<QVP_RTP_MAX_USR_PVT_COUNT)
	{
		QVP_RTP_MSG_HIGH("qvp_rtp_find_external_streamid: external_stream_id:%d internal stream_id:%d is_modem: %d", usr_ctx->usr_ctx_pvt[ret_val].external_stream_id, stream_id, is_modem);
		return (usr_ctx->usr_ctx_pvt[ret_val].external_stream_id);
	}
	else
	{
		QVP_RTP_ERR("qvp_rtp_find_external_streamid: Could not find matching stream id app_id:%d internal stream_id:%d is_modem: %d", app_id, stream_id, is_modem);
		return 0;
	}
}

qvp_rtp_stream_id_type qvp_rtp_find_internal_streamid ( qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type stream_id, int is_app)
{
	uint8 ret_val = 0;
	qvp_rtp_stream_id_type internal_stream_id = 0;
	qvp_rtp_usr_ctx *usr_ctx; 

	//QVP_RTP_MSG_HIGH("qvp_rtp_find_internal_streamid: app_id:%d stream_id:%d is_app: %d", app_id, stream_id, is_app);

	if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
	  ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	  {
		  QVP_RTP_ERR_1("qvp_rtp_find_internal_streamid: Invalid app id:%d",app_id);
		  return (qvp_rtp_stream_id_type)DEAD_BEEF;
	  }	

	usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ];

	for(;ret_val<QVP_RTP_MAX_USR_PVT_COUNT;ret_val++)
	{
		if(usr_ctx->usr_ctx_pvt[ret_val].valid && stream_id == usr_ctx->usr_ctx_pvt[ret_val].external_stream_id)
		{
			//QVP_RTP_MSG_MED_2("qvp_rtp_find_internal_streamid: is_apps_stream_id_valid:%d is_modem_stream_id_valid:%d ", usr_ctx->usr_ctx_pvt[ret_val].is_apps_stream_id_valid, usr_ctx->usr_ctx_pvt[ret_val].is_modem_stream_id_valid);	  
			if(is_app && usr_ctx->usr_ctx_pvt[ret_val].is_apps_stream_id_valid)
			{
				internal_stream_id =  usr_ctx->usr_ctx_pvt[ret_val].apps_stream_id;
				QVP_RTP_MSG_HIGH("qvp_rtp_find_internal_streamid: internal_stream_id:%d for external stream_id:%d is_modem:%d", internal_stream_id, stream_id, !is_app);	  
				break;
			}
			else if(!is_app && usr_ctx->usr_ctx_pvt[ret_val].is_modem_stream_id_valid)
			{  
				internal_stream_id = usr_ctx->usr_ctx_pvt[ret_val].modem_stream_id;
				QVP_RTP_MSG_HIGH("qvp_rtp_find_internal_streamid: internal_stream_id:%d for external stream_id:%d is_modem:%d", internal_stream_id, stream_id, !is_app);	
				break;
			}
		}
	}

	return (internal_stream_id);
}

void qvp_rtp_mark_pending_task_done(qvp_rtp_app_id_type app_id)
{
	QVP_RTP_ERR_2("qvp_rtp_mark_pending_task_done: for app_id:%d cmd:%d", app_id,rtp_ctx.user_array[ (uint32) app_id ].pending_queue[0].cmd);
	
	if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
	  ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	  {
		  QVP_RTP_ERR_1("qvp_rtp_mark_pending_task_done: Invalid app id:%d",app_id);
		  return;
	  }	
			
	rtp_ctx.user_array[ (uint32) app_id ].pending_queue[0].valid = FALSE;
	return;
}

qvp_rtp_stream_id_type qvp_rtp_update_stream_id_in_pvt_ctx(qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type internal_stream_id, boolean is_modem, uint8 index)
{
	qvp_rtp_stream_id_type external_stream_id = internal_stream_id;
	
	QVP_RTP_MSG_LOW("qvp_rtp_update_stream_id_in_pvt_ctx internal_stream_id = %d is_modem:%d index:%d",internal_stream_id,is_modem,index);  
	
	if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
	  ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	  {
		  QVP_RTP_ERR_1("qvp_rtp_update_stream_id_in_pvt_ctx: Invalid app id:%d",app_id);
		  return NULL;
	  }	
		

	if(is_modem) //if response is from modem
	{
		rtp_ctx.user_array[(uint32)app_id ].usr_ctx_pvt[index].modem_stream_id= internal_stream_id;		
		rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].is_modem_stream_id_valid = TRUE;	
		rtp_ctx.user_array[(uint32)app_id ].usr_ctx_pvt[index].apps_stream_id = ( qvp_rtp_stream_id_type )DEAD_BEEF;
		rtp_ctx.user_array[(uint32)app_id ].usr_ctx_pvt[index].is_apps_stream_id_valid = FALSE;
		
		external_stream_id = qvp_rtp_find_new_stream_id(app_id, TRUE);
	}
	else
	{
		rtp_ctx.user_array[(uint32)app_id ].usr_ctx_pvt[index].modem_stream_id= ( qvp_rtp_stream_id_type )DEAD_BEEF;		
		rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].is_modem_stream_id_valid = FALSE;	
		rtp_ctx.user_array[(uint32)app_id ].usr_ctx_pvt[index].apps_stream_id = internal_stream_id;
		rtp_ctx.user_array[(uint32)app_id ].usr_ctx_pvt[index].is_apps_stream_id_valid = TRUE;
		
		external_stream_id = qvp_rtp_find_new_stream_id(app_id, FALSE);
	}
	
	if(!rtp_ctx.user_array[(uint32)app_id ].usr_ctx_pvt[index].valid)
	{
		if(g_handoffstate != HO_IN_PROGRESS)
		{
			rtp_ctx.user_array[(uint32)app_id ].usr_ctx_pvt[index].external_stream_id = external_stream_id;
		}

		rtp_ctx.user_array[(uint32)app_id ].usr_ctx_pvt[index].valid = TRUE;
		QVP_RTP_MSG_LOW("qvp_rtp_update_stream_id_in_pvt_ctx external_stream_id = %d internal_stream_id:%d index:%d",external_stream_id,internal_stream_id,index); 
	}	
	
	return external_stream_id;
}

void qvp_rtp_adsp_ssr_cmd(void)
{
	g_cvd_ssr = TRUE;
}

qvp_rtp_media_type	qvp_rtp_find_media_type_from_pvt_ctx(qvp_rtp_stream_id_type internal_stream_id, qvp_rtp_app_id_type app_id, boolean is_modem,	boolean is_force_check)
{
	int index = 0;
	qvp_rtp_media_type media_type = QVP_RTP_INVALID;
	
	if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
	  ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	  {
		  QVP_RTP_ERR_1("qvp_rtp_find_media_type_from_pvt_ctx: Invalid app id:%d",app_id);
		  return media_type;
	  }	
	
	QVP_RTP_MSG_MED("qvp_rtp_find_media_type_from_pvt_ctx: internal_stream_id:%d app_id:%d is_modem:%d", internal_stream_id,app_id, is_modem);
	for(;index < QVP_RTP_MAX_USR_PVT_COUNT; index++)
	{
		if(rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].valid || is_force_check)
		{
			if(is_modem && rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].is_modem_stream_id_valid && internal_stream_id == rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].modem_stream_id)
			{
				break;
			}
			if(!is_modem && rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].is_apps_stream_id_valid && internal_stream_id == rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].apps_stream_id)
			{
				break;
			}
		}
	}
	if(QVP_RTP_MAX_USR_PVT_COUNT <= index)
	{
		return media_type;
	}
	QVP_RTP_MSG_MED("qvp_rtp_find_media_type_from_pvt_ctx: index:%d valid:%d PT:%d", index,rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].valid, rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type);
	
	if(rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].valid || is_force_check)
	{
		if(rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_RAW_AUD ||
			rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_AMR ||
			rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_EVS ||
			rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_AMR_WB)
		{
			media_type = QVP_RTP_AUDIO;
		}
		else if(rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_RAW_VID ||
			rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_H264 ||
			rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_H263)
		{
			media_type = QVP_RTP_VIDEO;
		}
		else if(rtp_ctx.user_array[(uint32)app_id].usr_ctx_pvt[index].open2_msg.open_config_param.stream_params.payload_type == QVP_RTP_PYLD_TXT)
		{
			media_type = QVP_RTP_TEXT;
		}
	}

	QVP_RTP_MSG_HIGH("qvp_rtp_find_media_type_from_pvt_ctx: internal_stream_id:%d app_id:%d media_type:%d", internal_stream_id,app_id, media_type);

	return media_type;
}

/******************************
qvp_rtp_execute_pending_command_cb

Post the signal to execute the pending commands

*******************************/

void qvp_rtp_execute_pending_command_cb(uint8 app_id)
{	
	if ( ( app_id == ( uint8 ) DEAD_BEEF ) || 
	  ( app_id >= (uint8)rtp_ctx.num_users ))
	  {
		  QVP_RTP_ERR_1("qvp_rtp_execute_pending_command_cb: Invalid app id:%d",app_id);
		  return;
	  }

  if(g_handoffstate == HO_IN_PROGRESS)
  {
			QVP_RTP_MSG_LOW_0("qvp_rtp_execute_pending_command_cb() :Handoff in progress");
		qvp_rtp_mark_pending_task_done((qvp_rtp_app_id_type)(uint32)app_id);
		qvp_rtp_execute_pending_command((qvp_rtp_app_id_type)(uint32)app_id);
  } 
	return;
}

/******************************
qvp_rtp_post_execute_pending_command

Post the signal to execute the pending commands

*******************************/
void qvp_rtp_post_execute_pending_command(qvp_rtp_app_id_type app_id)
{	

  if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
    ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
  	{
  		QVP_RTP_ERR_1("qvp_rtp_post_execute_pending_command: Invalid app id:%d",app_id);
		return;
  	}

  if(g_handoffstate == HO_IN_PROGRESS)
  {
		QVP_RTP_MSG_LOW_0("qvp_rtp_post_execute_pending_command() :Handoff in progress");
		qpDplPostEventToEventQueue(QPCMSG_DPL_AUDIO_MSG, (int32)app_id,(QPVOID*)qvp_rtp_execute_pending_command_cb, QP_NULL);

  } 
	return;
}
void qvp_rtp_srvcc_tear_down_cmd
(
	qvp_rtp_srvcc_tear_down_msg_type *srvcc_tear_down_msg
)
{
	qvp_rtp_usr_ctx *usr_ctx; 

	if ( ( srvcc_tear_down_msg != NULL ) && 
    ( srvcc_tear_down_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( srvcc_tear_down_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	{
        //clear_all_session_req = TRUE;
		srvcc_tear_down_req	= TRUE;

		usr_ctx = &rtp_ctx.user_array[ (uint32) srvcc_tear_down_msg->app_id ];
		//log voice call statistics log packet
		{
			uint8 index= 0;
  
  			for(;index<QVP_RTP_MAX_USR_PVT_COUNT;index++)
  			{
  				if( usr_ctx->usr_ctx_pvt[index].valid && (usr_ctx->active_stream_id == usr_ctx->usr_ctx_pvt[index].external_stream_id))
				{
					if(usr_ctx->usr_ctx_pvt[index].is_modem_stream_id_valid)
					{
						qvp_rtp_log_voice_call_stat(srvcc_tear_down_msg->app_id, usr_ctx->usr_ctx_pvt[index].modem_stream_id);
						break;
					}
  				}
  			}
			if(index >= QVP_RTP_MAX_USR_PVT_COUNT)
			{
				QVP_RTP_ERR("srvcc_tear_down :Not able to locate active audio stream id on modem: %d", usr_ctx->active_stream_id, 0, 0);
			}
		}

		if (QVP_RTP_SUCCESS != qvp_rtp_media_stream_stop_pvt(srvcc_tear_down_msg->app_id, NULL, QVP_RTP_DIR_TXRX))
		{
			//if(qvp_rtp_clear_all_media_resource(srvcc_tear_down_msg->app_id, QVP_RTP_AUDIO, QVP_RTP_RAT_LTE) != QVP_RTP_SUCCESS)
				//QVP_RTP_ERR("srvcc_tear_down :clear all QVP_RTP_AUDIO failed.", 0, 0, 0);
		    //if(qvp_rtp_clear_all_media_resource(srvcc_tear_down_msg->app_id, QVP_RTP_VIDEO, QVP_RTP_RAT_LTE) != QVP_RTP_SUCCESS)
				//QVP_RTP_ERR("srvcc_tear_down : clear all QVP_RTP_VIDEO failed.", 0, 0, 0);
			if (QVP_RTP_SUCCESS != qvp_rtp_media_codec_release_pvt(srvcc_tear_down_msg->app_id))
			{
				
			}
			
		 QVP_RTP_MSG_HIGH( " qvp_rtp_media_codec_release_pvt failed to process\r\n", 0, 0, 0 );  
		}
		qvp_rtp_service_media_codec_release_req_msg(srvcc_tear_down_msg->app_id, QVP_RTP_VIDEO, FALSE);
	}	

}

void qvp_rtp_per_req_cmd
(
	qvp_rtp_per_req_msg_type *per_req_msg
)
{
	if ( ( per_req_msg != NULL ) && 
    ( per_req_msg->app_id != ( qvp_rtp_app_id_type ) DEAD_BEEF ) && 
    ( per_req_msg->app_id < ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	{
		qvp_rtp_usr_ctx *usr_ctx;   
		uint64				tot_pkt_cnt = 0,cum_lost = 0;
		uint8				index = 0;
		usr_ctx = &rtp_ctx.user_array[ (uint32) (per_req_msg->app_id) ];

		for(index = 0; index < QVP_RTP_MAX_USR_PVT_COUNT; index++)
		{
			// find the valid user context
			if(usr_ctx->usr_ctx_pvt[index].valid)
			{
				uint64 loss_cnt =0;
				qvp_rtp_log_pkt_ctx* rtp_log;
				//if modem is valid
				QVP_RTP_MSG_HIGH("qvp_rtp_per_req_cmd: enter modem_valid:%d apps_valid:%d",usr_ctx->usr_ctx_pvt[index].is_modem_stream_id_valid,usr_ctx->usr_ctx_pvt[index].is_apps_stream_id_valid,0);
				if(usr_ctx->usr_ctx_pvt[index].is_modem_stream_id_valid)
				{
					rtp_log = &usr_ctx->stream_array[(uint32)(usr_ctx->usr_ctx_pvt[index].modem_stream_id)].rtp_log_pkt_ctx;
					
					loss_cnt =  (rtp_log->end_seq - rtp_log->begin_seq + 1) - rtp_log->tot_rx_rtp_cnt;
					cum_lost += loss_cnt;
					tot_pkt_cnt += rtp_log->tot_rx_rtp_cnt;
					QVP_RTP_MSG_HIGH("qvp_rtp_per_req_cmd: end_seq:%lu  begin_seq:%lu tot_rx_rtp_cnt:%lu",rtp_log->end_seq,rtp_log->begin_seq,rtp_log->tot_rx_rtp_cnt);					
				}
				//if apps is valid. currently no support for AP. extend if needed
				/*
				else if(usr_ctx->usr_ctx_pvt[index].is_apps_stream_id_valid )
				{
					rtp_log = &usr_ctx->stream_array[(uint32)(usr_ctx->usr_ctx_pvt[index].apps_stream_id)].rtp_log_pkt_ctx;
					
					loss_cnt =  (rtp_log->end_seq - rtp_log->begin_seq + 1) - rtp_log->tot_rx_rtp_cnt;
					cum_lost += loss_cnt;
					tot_pkt_cnt += rtp_log->tot_rx_rtp_cnt;
					QVP_RTP_MSG_HIGH("qvp_rtp_per_req_cmd: end_seq:%lu  begin_seq:%lu tot_rx_rtp_cnt:%lu",rtp_log->end_seq,rtp_log->begin_seq,rtp_log->tot_rx_rtp_cnt);					
				}			
				*/
			}
		}
		QVP_RTP_MSG_MED("qvp_rtp_per_req_cmd: tot_pkt_cnt:%lu and cum_lost:%lu",tot_pkt_cnt,cum_lost,0);
		if(usr_ctx->call_backs.handle_per_cb)
		{
			//QVP_RTP_MSG_HIGH("qvp_rtp_per_req_cmd: handle_per_cb ",0,0,0);
			usr_ctx->call_backs.handle_per_cb(per_req_msg->app_id,tot_pkt_cnt,cum_lost);
		}
		else
		{
			QVP_RTP_MSG_HIGH("qvp_rtp_per_req_cmd: handle_per_cb is NULL ",0,0,0);
		}
	}	
	return;
}

/******************************
qvp_rtp_clear_pending_ref_cnt


Clears the pending indication ref count from 
private context related to media type

*******************************/

void qvp_rtp_clear_pending_ref_cnt(qvp_rtp_app_id_type app_id, qvp_rtp_media_type media_type)
{	
	qvp_rtp_usr_ctx *usr_ctx; 
	qvp_rtp_payload_type payload_type = QVP_RTP_PYLD_NIL;
	int ret_val = 0;
	
  if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
    ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
  	{
  		QVP_RTP_ERR_1("qvp_rtp_clear_pending_ref_cnt: Invalid app id:%d",app_id);
		return;
  	}
  
    usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ];

	
	for(;ret_val<QVP_RTP_MAX_USR_PVT_COUNT;ret_val++)
	{
		if(usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt)
		{
			payload_type =usr_ctx->usr_ctx_pvt[ret_val].open2_msg.open_config_param.stream_params.payload_type;

			if( (media_type == QVP_RTP_AUDIO) && (payload_type == QVP_RTP_PYLD_RAW_AUD || payload_type == QVP_RTP_PYLD_AMR || payload_type == QVP_RTP_PYLD_EVS || payload_type == QVP_RTP_AMR_WB 
												|| payload_type == QVP_RTP_PYLD_G711_A || payload_type == QVP_RTP_PYLD_G711_U))
			{
				usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt = 0;
			}
			else if( (media_type == QVP_RTP_VIDEO) && (payload_type == QVP_RTP_PYLD_RAW_VID || payload_type == QVP_RTP_PYLD_H264 || payload_type == QVP_RTP_PYLD_H263 ))
			{
				usr_ctx->usr_ctx_pvt[ret_val].pending_ind_ref_cnt = 0;
			}
		}
		if( usr_ctx->usr_ctx_pvt[ret_val].valid && (usr_ctx->usr_ctx_pvt[ret_val].is_apps_stream_id_valid ))
		{
			usr_ctx->usr_ctx_pvt[ret_val].valid = FALSE;
		}
    } 
	return;
}
char * qvp_rtp_media_generate_nal_sync_cmd
(
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                       * the stream
                                       */
  uint16 height,
  uint16 width,
  qvp_rtp_media_h264_profile_type profile,
  qvp_rtp_media_h264_level_type level
)
{
 return qvp_rtp_service_media_generate_nal_req_msg_sync(app_id,height,width,level,profile);
}
/*===========================================================================

FUNCTION qvp_rtp_send_rtcp_app_msg

DESCRIPTION

This function is used to send the RTCP APP PKT of RTP session.

DEPENDENCIES
None

ARGUMENTS IN
app_id             -    app id
stream_id          -    stream which was opened

RETURN VALUE
None.

SIDE EFFECTS
None.

===========================================================================*/
qvp_rtp_status_type qvp_rtp_send_rtcp_app_msg(qvp_rtp_app_id_type app_id,qvp_rtp_stream_id_type stream_id,qvp_rtp_rat_type rat_type)
{
     qvp_rtp_stream_type *stream; /* for inderection simplicity */
     qvp_rtp_usr_ctx *user_ctx;

    QVP_RTP_MSG_MED("inside qvp_rtp_rtcp_app_msg....",0,0,0);


    user_ctx = &rtp_ctx.user_array[ (uint32)app_id];
    stream = &user_ctx->stream_array[ (uint32)stream_id ];

     if(qvp_rtp_send_rtcp_app_pkt_msg(stream->rtcp_hdl,rat_type)!=QVP_RTP_SUCCESS)
          QVP_RTP_MSG_HIGH("qvp_rtp_send_rtcp_app_msg: sending rtcp app message failed....",0,0,0);

   return( QVP_RTP_SUCCESS );
}

/*===========================================================================

FUNCTION qvp_rtp_is_stream_id_set

DESCRIPTION

This function is used to check if external stream id is already present in the pvt ctx

DEPENDENCIES
None

ARGUMENTS IN
app_id             -    app id
external_stream_id          -   one selected external stream id

RETURN VALUE
None.

SIDE EFFECTS
None.

===========================================================================*/
boolean qvp_rtp_is_stream_id_set(qvp_rtp_app_id_type app_id, qvp_rtp_stream_id_type external_stream_id)
{
	qvp_rtp_usr_ctx *usr_ctx; 
	int ret_val = 0;
	boolean status = FALSE;
	  
	if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
	  ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	  {
		  QVP_RTP_ERR_1("qvp_rtp_clear_pending_ref_cnt: Invalid app id:%d",app_id);
		  return status;
	  }
	
	usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ];

	for(;ret_val<QVP_RTP_MAX_USR_PVT_COUNT;ret_val++)
	{
	 	if(usr_ctx->usr_ctx_pvt[ret_val].external_stream_id == external_stream_id)
	 	{
         QVP_RTP_ERR("qvp_rtp_find_new_stream_id: pvt ctx external_stream_id:%d external_stream_id: %d ret_val:%d",usr_ctx->usr_ctx_pvt[ret_val].external_stream_id,external_stream_id, ret_val);
         status = TRUE;
	 		break;
	 	}
	}

   if(status == FALSE)
      QVP_RTP_ERR_1("qvp_rtp_is_stream_id_set: external_stream_id: %d not found",external_stream_id);

	return status;
}

/*===========================================================================

FUNCTION qvp_rtp_find_new_stream_id

DESCRIPTION

This function is used to find an unused stream id

DEPENDENCIES
None

ARGUMENTS IN
app_id             -    app id
is_modem          -    flag to check if modem is valid

RETURN VALUE
an unused external stream id

SIDE EFFECTS
None.

===========================================================================*/
qvp_rtp_stream_id_type qvp_rtp_find_new_stream_id(qvp_rtp_app_id_type app_id, boolean is_modem)
{
	qvp_rtp_stream_id_type external_stream_id = (qvp_rtp_stream_id_type)(is_modem ? QVP_RTP_MAX_STREAMS_DFLT : (QVP_RTP_APPS_STREAM_ID_OFFSET + QVP_RTP_MAX_STREAMS_DFLT));
	qvp_rtp_usr_ctx *usr_ctx; 
	int count =0;
	  
	if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
	  ( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ))
	  {
		  QVP_RTP_ERR_1("qvp_rtp_find_new_stream_id: app id:%d",app_id);
		  return NULL;
	  }
	
	usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ];


	if(is_modem )
	{	
		count =0;
		
		do{
			external_stream_id = (qvp_rtp_stream_id_type)((uint32)usr_ctx->prev_modem_ext_strem_id + 1 + count);
			if((uint32)external_stream_id >= QVP_RTP_APPS_STREAM_ID_OFFSET)
			{
				external_stream_id = (qvp_rtp_stream_id_type) (QVP_RTP_MAX_STREAMS_DFLT);
				usr_ctx->prev_modem_ext_strem_id = external_stream_id;
			}			
			count++;
			QVP_RTP_ERR("qvp_rtp_find_new_stream_id: external_stream_id:%d prev_modem_ext_strem_id: %d count:%d",external_stream_id, usr_ctx->prev_modem_ext_strem_id, count);
		}while(qvp_rtp_is_stream_id_set(app_id, external_stream_id));

		usr_ctx->prev_modem_ext_strem_id = external_stream_id;
	}
	else if(!is_modem )
	{
		count =0;		
		do{
			external_stream_id = (qvp_rtp_stream_id_type)((uint32)usr_ctx->prev_app_ext_strem_id + 1 + count);
			if((uint32)external_stream_id >= QVP_RTP_APPS_STREAM_ID_OFFSET*2)
			{
				external_stream_id = (qvp_rtp_stream_id_type) (QVP_RTP_APPS_STREAM_ID_OFFSET + QVP_RTP_MAX_STREAMS_DFLT);
				usr_ctx->prev_app_ext_strem_id = external_stream_id;
			}
			count++;
			QVP_RTP_ERR("qvp_rtp_find_new_stream_id: external_stream_id:%d prev_app_ext_strem_id: %d count:%d",external_stream_id, usr_ctx->prev_app_ext_strem_id, count);
		}while(qvp_rtp_is_stream_id_set(app_id, external_stream_id));

		usr_ctx->prev_app_ext_strem_id = external_stream_id;
	} 	
	QVP_RTP_ERR_1("qvp_rtp_find_new_stream_id: external_stream_id:%d",external_stream_id);
	return external_stream_id;
}

void qvp_rtcp_metric_set_nxtstate()
{
  qvp_rtp_metic_state temp =QVP_METRIC_NONE;

  if(NULL != rtp_ctx.metrics)
  {
    temp=rtp_ctx.metrics->ul_metrics.compute;
    QVP_RTP_ERR_1("metrics: UL state B4 - 0x%x",temp);
    if(QVP_METRIC_SKIP & temp)
    {
      if(!(QVP_METRIC_SKIP_NEXT & temp))
      {
        QVP_SET_METRIC_STATE(temp, QVP_METRIC_SKIP_NEXT);
      }
      else
      {
        QVP_RESET_METRIC_STATE(temp,(QVP_METRIC_SKIP|QVP_METRIC_SKIP_NEXT));
      }
    }
    QVP_RTP_ERR_1("metric: UL state A$ - 0x%x",temp);
    rtp_ctx.metrics->ul_metrics.compute = temp;
  }
}


void qvp_rtp_clear_pvt_ctx_on_error(
	qvp_rtp_app_id_type app_id,
	qvp_rtp_app_data_type app_data)
{
	int idx = 0;
	qvp_rtp_usr_ctx *usr_ctx = NULL;

	if ( ( app_id == ( qvp_rtp_app_id_type ) DEAD_BEEF ) || 
		( app_id >= ( qvp_rtp_app_id_type ) (uint32)rtp_ctx.num_users ) )
	{
		return;
	}
	usr_ctx = &rtp_ctx.user_array[ (uint32) app_id ];

	for(idx = 0; idx < QVP_RTP_MAX_USR_PVT_COUNT; idx++)
	{
		if( usr_ctx->usr_ctx_pvt[idx].app_data ==  (uint32) app_data)
		{
			usr_ctx->usr_ctx_pvt[idx].valid = FALSE;
			usr_ctx->usr_ctx_pvt[idx].is_open_pending = FALSE;
			usr_ctx->usr_ctx_pvt[idx].pending_ind_ref_cnt = 0;
			break;
		}
	}

	return;
}
#endif /* end of FEATURE_QVPHONE_RTP */
