
#ifndef _QVP_SRTP_H_
#define _QVP_SRTP_H_
/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                         QVP_SRTP . h

GENERAL DESCRIPTION

  This file contains the interface to SRTP. Provides apis to encrypt, decrypt
  and authenticate if SRTP is enabled.

EXTERNALIZED FUNCTIONS

  qvp_srtp_init_crypto

    Initializes the cryptographic context

  qvp_srtp_encrypt
    
    Encrypts the payload and authenticates RTP header + encrypted payload before transmitting.
	Return error if failed to do encryption or authentication.
  
  qvp_srtp_decrypt
 
     While receiving a packet, check if authentication is proper. If yes, decrypt the packet.
	 If not, send error message.

  qvp_srtp_hash
 
	authenticates RTP header + encrypted payload before transmitting.
	authenticates the RTP header + encrypted payload before decrypting in Rx side
	
  qvp_srtp_deinit_crypto

    Deinitializes the cryptographic context
	
  qvp_srtp_derive_keys
 
	This function will keys for encryption and authentication from the master keys
 
  qvp_srtp_create_iv
 
	This function will create IV for packet encryption
  
  qvp_rtp_rx_srtp_index
  
	This function will find index in rx side

INITIALIZATION AND SEQUENCING REQUIREMENTS

  None of the externalized functions will function before the task is 
  spawned.


  Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/**
 * \file qvp_srtp.h
 * \brief   Contains the interface to SRTP. Provides apis to encrypt, decrypt
 * and authenticate if SRTP is enabled.
 *
 * \ingroup RTP_CORE
 * This file contains the interface to SRTP. Provides apis to encrypt, decrypt
 * and authenticate if SRTP is enabled.
 */
 /** @defgroup RTP_CORE_SRTP  Contains APIS and callbacks required to access SRTP functionality
 * \ingroup RTP_CORE
 *  @{
 */
/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/ps_media/rtp_core/src/qvp_srtp.h#1 $ $DateTime: 2016/03/28 23:03:22 $ $Author: mplcsds1 $

                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
27/05/13    vaishali    SRTP support
===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP


/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#define WINDOWS_MAKE 1
#include <comdef.h>               /* common target/c-sim defines */
#include <string.h>               /* memory routines */



#include <stdlib.h>
#include <string.h>
#ifdef WINDOWS_MAKE
#include <stdio.h>                /* for windows console */
//#include <Winsock.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * \fn QVP_RTP_STATUS_TYPE qvp_srtp_init_crypto(qvp_rtp_srtp_context* srtp_ctx,qvp_rtp_srtp_info* srtp_info);
 * 
 *
 * This function will create a cipher handle for the specifed cipher algo
 * srtp_ctx					- srtp context
 * auth_handle			 	- handle to the srtp algo information
 * \return QVP_RTP_STATUS_TYPE - operation was succesful
 *  appropriate error code otherwise
 * \remarks : The cipher handle to be used for subsequent cipher operations.
 */
 qvp_rtp_status_type qvp_srtp_init_crypto
 (
 qvp_rtp_srtp_context* srtp_ctx,
 qvp_rtp_srtp_info* srtp_info
 );
/*--------------------------------------------------------------------------*/
 /**
 * \fn QVP_RTP_STATUS_TYPE  qvp_srtp_encrypt(uint32	cipher_handle, uint* indata, uint8* outdata, uint16 InDataLen, uint32* OutDataLen, uint64 index, uint32 ssrc, uint8* encryption_key,  uint8* salting_key)
 *
 * Encrypts the payload a before authentication.
 * 
 * cipher_handle        	- Cipher handle 
 * indata					- pointer to data needs to be encrypted
 * outdata					- pointer to output data
 * InDataLen				- input data length
 * OutDataLen				- data length after encryption
 * index					- combination of rtp seq and ROC
 * ssrc						- ssrc of the source
 * encryption_key      		- encryption keys
 * salting_key				- session salting key
 * \return QVP_RTP_SUCCESS  - operation was succesful
 *  appropriate error code otherwise
 * \remarks : The cipher handle to be used for subsequent cipher operations.
 */
 qvp_rtp_status_type qvp_srtp_encrypt
 ( 
 uint32	cipher_handle,
 uint8* indata,
 uint8* outdata,
 uint16 InDataLen,
 uint32 *OutDataLen, 
 uint64 index,
 uint32 ssrc,
 uint8* encryption_key,
 uint8* salting_key
 );
 /*--------------------------------------------------------------------------*/ 
 /**
 * \fn QVP_RTP_STATUS_TYPE  qvp_srtp_decrypt( uint32 cipher_handle, uint* encoutput, uint8* decoutput, uint16 InDataLen,uint32* OutDataLen, uint64 index, uint32 ssrc, uint8* encryption_key, uint8* salting_key)
 *
 * While receiving a packet, if authentication is successful, decrypt the packet.
 * 
 * cipher_handle        	- Cipher handle 
 * encoutput				- pointer to data needs to be encrypted
 * decoutput				- pointer to decrypted data
 * InDataLen				- input data length
 * OutDataLen				- data length after decryption
 * index					- combination of rtp seq and ROC
 * ssrc						- ssrc of the source
 * decryption_key      		- decryption keys
 * salting_key				- session salting key
 * \return QVP_RTP_SUCCESS  - operation was succesful
 *  appropriate error code otherwise
 * \remarks : The cipher handle to be used for subsequent cipher operations.
 */
 qvp_rtp_status_type qvp_srtp_decrypt
 (
 uint32	decryp_handle,
 uint8* encoutput,
 uint8* decoutput,
 uint16 InDataLen,
 uint32 *OutDataLen, 
 uint64 index,
 uint32 ssrc,
 uint8* decryption_key,
 uint8* salting_key
 );
 /*--------------------------------------------------------------------------*/
 /**
 * \fn QVP_RTP_STATUS_TYPE  qvp_srtp_hash( uint32	auth_handle, uint* indata, uint8* outdata, uint16 InDataLen, uint32 OutDataLen, uint8* auth_key)
 *
 * authenticates RTP header + encrypted payload before transmitting.
 * authenticates the RTP header + encrypted payload before decrypting in Rx side
 * 
 * auth_handle        		- handle to authentication 
 * indata					- pointer to data needs to be encrypted
 * outdata					- pointer to output data
 * InDataLen				- input data length
 * OutDataLen				- hashed data length
 * auth_key        			- authentication keys
 *
 * \return QVP_RTP_SUCCESS  - operation was succesful
 *  appropriate error code otherwise
 * \remarks : The cipher handle to be used for subsequent cipher operations.
 */
 qvp_rtp_status_type qvp_srtp_hash
 ( 
 uint32	auth_handle,
 uint8* indata,
 uint8* outdata,
 uint16 InDataLen,
 uint32 OutDataLen, 
 uint8* auth_key
 );
 /*--------------------------------------------------------------------------*/
 /**
 * \fn QVP_RTP_STATUS_TYPE qvp_srtp_deinit_crypto(uint32* cipher_handle, uint32* auth_handle);
 * 
 *
 * This function will free the given cipher handle 
 * cipher_handle        	- Cipher handle which we need to free
 * auth_handle				- authentication handle which we need to free
 * \return QVP_RTP_SUCCESS  - operation was succesful
 * appropriate error code otherwise
 * \remarks : None
 */
 qvp_rtp_status_type qvp_srtp_deinit_crypto
 (
 uint32 cipher_handle,
 uint32 auth_handle
 );

 /*--------------------------------------------------------------------------*/
/**
 * \fn uint32 qvp_srtp_derive_keys(uint32 cipher_handle,const uint8 *master_key, const uint8 *master_salt, int label, uint8 *out, int outlen);
 * 
 * 
 * This function will keys for encryption and authentication from the master keys
 * cipher_handle      		- Cipher handle 
 * master_key        		- master key used for key derivation
 * master_salt        		- master salt used for key derivation
 * label					- label specific for each key
 * out						- output key
 * outlen					- length of the required key
 * \return length of the derived key
 * \remarks : Need to call this api to generate all the keys
 */
 uint32 qvp_srtp_derive_keys
 (
 uint32 cipher_handle,
 const uint8 *master_key,
 const uint8 *master_salt,
 int label,
 uint8 *out,
 int outlen
 );
  /*--------------------------------------------------------------------------*/
/**
 * \fn uint64 qvp_rtp_rx_srtp_index
								  ( uint16 rx_sl,
 								    uint32 rx_ROC,
									uint32 rx_seq );									   
 * 
 *
 * This function will find index in rx side
 * rx_sl      				-	Maximum sequence number received
 * rx_ROC        			- 	Roll over counter
 * rx_seq          			- 	Sequence number of the received packet
 * \return uint64 index    	-   index for decryption of data
 * \remarks : None
 */  
 uint64 qvp_rtp_rx_srtp_index(uint16 rx_sl, uint32 rx_ROC, uint16 rx_seq );
/*--------------------------------------------------------------------------*/
#endif /* end of FEATURE_QVPHONE_RTP */
#endif /*  end of function _QVP_RTP_API_H_ */
 /** @} */ // end of group RTP_CORE_SRTP
