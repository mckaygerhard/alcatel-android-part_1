#ifndef _QVP_RTP_SVS_IVA_H_
#define _QVP_RTP_SVS_IVA_H_
/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==*

                         QVP_RTP_SVS_IVA.H

GENERAL DESCRIPTION

  THis file contains internal functions used within RTP core module

Copyright (c) 2007 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP
#include "qvp_rtcp_api.h"
#include "iva_if.h"

/*============================================================================= 
  STRUCT qvp_rtp_iva_vocoder_ctx

  DESRIPTION
  This data structure mantains all the information RTP needs to know about IVA
 =============================================================================*/
typedef struct qvp_rtp_iva_vocoder_ctx_s
{
  qvp_rtp_iva_state                        iva_state;
  iva_ivoice_cmd_open_t                    iva_open_cmd;
  qvp_rtp_media_codec_init_msg_type        *msg_codec_init;
  boolean                                  iva_revoke_pending;
  boolean                                  iva_revoke_req;
}qvp_rtp_iva_vocoder_ctx;




#endif /* end of FEATURE_QVPHONE_RTP */
#endif /* end of _QVP_RTP_SVS_IVA_H_ */