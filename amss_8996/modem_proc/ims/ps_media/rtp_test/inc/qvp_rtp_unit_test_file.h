#ifndef _IMS_VT_MEDIA_TEST_FRAMEWORK_FILE
#define _IMS_VT_MEDIA_TEST_FRAMEWORK_FILE

#include "ims_variation.h"
#include "customer.h"
#include "comdef.h"

#include "qpPlatformConfig.h"
#include "qpdplCommonMisc.h"
#include "qplog.h"
#include "qpdpl.h"
#include "qpdcm.h"
#include "qpConfigNVItem.h"
#include "qpnet.h"
#include "qpIO.h"


typedef struct TestPlanParam
{
  char* name;
  char* value;
  struct TestPlanParam *next_param;
}TestPlanParam;


typedef struct TestPlanComponent
{
  char* action;
  TestPlanParam* params;
  struct TestPlanComponent *next_test;
}TestPlanComponent;



QPVOID GenerateTestPlan(QPVOID);

#endif