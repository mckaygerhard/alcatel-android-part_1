#ifndef IMS_VT_MEDIA_TEST_FRAMEWORK
#define IMS_VT_MEDIA_TEST_FRAMEWORK


#include "comdef.h"
#include "qvp_rtp_api.h"
#include "qpPlatformConfig.h"
#include "qpdplCommonMisc.h"
#include "qplog.h"
#include "qvp_rtp_api.h"
#include "qpdpl.h"
#include "qpdcm.h"
#include "qpConfigNVItem.h"
#include "qpnet.h"
#include "qvp_rtp_unit_test_file.h"


typedef enum
{
  /* combinations of audio and video streams
   * 
   * The values defined by this enumeration are used as arguments
   * to test case functions which operate on one or more streams.
   */
  STREAMS_NONE,
  STREAMS_AUDIO,
  STREAMS_VIDEO,
  STREAMS_AUDIO_VIDEO
} streams_t;


typedef struct call_params
{
  /* structure to hold parameters specific to a call */
  qvp_rtp_stream_id_type audio_stream_id;
  qvp_rtp_stream_id_type video_stream_id;
  uint16 rtp_audio_port;
  uint16 rtcp_audio_port;
  uint16 rtp_video_port;
  uint16 rtcp_video_port;
  struct call_params *next;
  struct call_params *prev;
} call_params;


typedef enum
{
  /* type used by 'test_monitor_data_t' to specify
   * the monitor type i.e. RTP or RTCP.
   */
  MONITOR_RTP,
  MONITOR_RTCP,
} monitor_type_t;


typedef struct
{
  /* data type for arguments to the TestMonitor function */
  monitor_type_t type;
  streams_t streams;
  uint16 time;
} test_monitor_data_t;


typedef struct
{
  /* data type for arguments to the TestPause and TestResume functions */
  streams_t streams;
  qvp_rtp_stream_direction direction;
} test_pause_resume_data_t;


typedef enum
{
  /* parameters to execute_next_step_of_test_case */
  PARAM_NULL,
  STOP_RTP_MONITOR_A,
  STOP_RTP_MONITOR_V,
  STOP_RTP_MONITOR_AV,
  STOP_RTCP_MONITOR_A,
  STOP_RTCP_MONITOR_V,
  STOP_RTCP_MONITOR_AV
} execute_next_step_of_test_case_param_t;


typedef struct
{
  /* atomic unit of a test case */
  QPVOID (*function)(QPVOID *);
  #ifndef FEATURE_IMS_PC_TESTBENCH
  QPVOID *data;
  #else
  QPVOID *data[10];
  #endif
} test_case_step_t;


typedef enum
{
  /* all events that can be received by the QIPCALL */
  TEST_RTP_REG_FAIL,
  TEST_RTP_REGISTER_DONE,
  TEST_RTP_OPEN_DONE,
  TEST_RTP_CLOSE_DONE,
  TEST_RTP_CONFIGURE_DONE,
  TEST_RTP_OPEN_FAIL,
  TEST_RTP_RESET_DONE,
  TEST_RTP_RESET_FAIL,
  TEST_RTP_CONFIGURE_FAIL,
  TEST_RTP_INACTIVITY_TIMEOUT,
  TEST_RTCP_INACTIVITY_TIMEOUT,
  TEST_RTP_MEDIA_EVENT,
  TEST_GENERATE_NAL_EVENT,
  TEST_GENERATE_IDR_EVENT,
  TEST_RTP_CONFIG_SESSION_FAIL,
  TEST_RTP_CONFIG_SESSION_DONE,
  TEST_EVENT_MAX = 0x7FFFFFFF
} test_event_type;


typedef enum { 
  TEST_SQCIF_WIDTH = 128,
  TEST_QCIF_WIDTH = 176,
  TEST_CIF_WIDTH = 352,
  TEST_QQVGA_WIDTH = 160,
  TEST_QVGA_WIDTH = 320,
  TEST_VGA_WIDTH  = 640,
  TEST_HD_720_WIDTH = 720
} TEST_RESOLUTION_WIDTH;


typedef enum { 
  TEST_SQCIF_HEIGHT = 96,
  TEST_QCIF_HEIGHT = 144,
  TEST_CIF_HEIGHT = 288,
  TEST_QQVGA_HEIGHT = 120,
  TEST_QVGA_HEIGHT = 240,
  TEST_VGA_HEIGHT  = 480,
  TEST_HD_720_HEIGHT = 1280
} TEST_RESOLUTION_HEIGHT;


typedef struct
{
  QPUINT8 iH264_preferred_frame_rate;
  QPUINT16 iH264_preferred_bit_rate;
  QPUINT32 iH264_preferred_width;
  QPUINT32 iH264_preferred_height;
  qvp_rtp_media_h264_level_type iH264_preferred_profile_level;
  QPUINT8 iH264_min_frame_rate;
  QPUINT8 iH264_max_frame_rate;
  QPUINT16 iH264_min_bit_rate;
  QPUINT16 iH264_max_bit_rate;
  QPUINT32 iH264_resolutions_supported;
  QPUINT32 iH264_profile_levels_supported;
  qvp_rtp_media_h264_profile_type iH264_profile_type;
} test_capability_H264;


typedef struct
{
  QPUINT8 iH263_preferred_frame_rate;
  QPUINT16 iH263_preferred_bit_rate;
  QPUINT32 iH263_preferred_width;
  QPUINT32 iH263_preferred_height;
  qvp_rtp_media_h263_level_type iH263_preferred_profile_level;
  QPUINT8 iH263_min_frame_rate;
  QPUINT8 iH263_max_frame_rate;
  QPUINT16 iH263_min_bit_rate;
  QPUINT16 iH263_max_bit_rate;
  QPUINT32 iH263_resolutions_supported;
  QPUINT32 iH263_profile_levels_supported;
  qvp_rtp_media_h263_profile_type iH263_profile_type;
} test_capability_H263;


typedef struct 
{
  test_capability_H264 icodecH264;
  test_capability_H263 icodecH263;
} test_media_capability;


typedef struct
{
  test_media_capability VT_4G;
} test_nv_params;


typedef struct
{
  /* media param info that will be used to switch the context */
  qvp_rtp_media_events media_event_id;
  qvp_rtp_status_type status;
  qvp_rtp_stream_direction direction;
} test_rtp_media_event_info;


typedef struct
{
  /* param that will be sent with a callback from RTP */
  test_event_type event_id;
  qvp_rtp_status_type status;
  qvp_rtp_app_id_type app_id;
  qvp_rtp_app_data_type app_data;
  qvp_rtp_stream_id_type stream;
  qvp_rtcp_session_info_type rtcp_pkt_info;      
  test_rtp_media_event_info media_event_info;
  QPINT nw_iface_app_id;
  QPCHAR *nal_info;
} test_rtp_param;


void qvp_rtp_get_my_cb(qvp_rtp_app_call_backs_type *call_backs);

/* functions for getting IP address */
void rtptest_dcm_cb(QPE_DCM_MSG dcmMsg, QPDCM_CONN_PROFILE *pConProfile, void *pMsgData);
QPDCM_CONN_PROFILE *rtptestcreate_profile(QPUINT32 *iPDPID);

/* entry point for rtp_test */
QPVOID VT_Media_Test_Call(int param);

/* test case 'steps' */
QPVOID TestRegister(QPVOID *data);
QPVOID TestRTPOpen(QPVOID *data);
QPVOID TestRTPConfig(QPVOID *data);
QPVOID TestRTPSessionConfig(QPVOID *data);
QPVOID TestCodecInit(QPVOID *data);
QPVOID TestCodecInitTXConfigRx(QPVOID *data);

QPVOID TestCodecInitTx(QPVOID *data);
QPVOID TestCodecInitRx(QPVOID *data);



QPVOID TestCodecConfig(QPVOID *data);
QPVOID TestCodecConfigTx(QPVOID *data);
QPVOID TestCodecConfigRx(QPVOID *data);

QPVOID TestCodecStartStream(QPVOID *data);
QPVOID TestCodecStartStreamTx(QPVOID *data);
QPVOID TestCodecStartStreamRx(QPVOID *data);

QPVOID TestCodecStopStream(QPVOID *data);
QPVOID TestCodecStopStreamTx(QPVOID *data);
QPVOID TestCodecStopStreamRx(QPVOID *data);


QPVOID TestCodecRelease(QPVOID *data);
QPVOID TestRTPClose(QPVOID *data);
QPVOID TestPause(QPVOID *data);
QPVOID TestResume(QPVOID *data);
QPVOID TestCallUpdateStart(QPVOID *data);
QPVOID TestCallUpdateEnd(QPVOID *data);
QPVOID TestMonitor(QPVOID *data);
QPVOID TestHandoff(QPVOID *data);
QPVOID TestDTMFSend(QPVOID *data);

QPVOID TestSrvccTearDown(QPVOID *data);
QPVOID TestConfigGbrMbr(QPVOID *data);
QPVOID TestSwapCall(QPVOID *data);
QPVOID TestUnswapCall(QPVOID *data);
QPVOID TestCaseComplete(QPVOID *data);
QPVOID ClearAllResources(QPVOID *data);

/* test case step helper functions */
QPVOID CodecInitHelper(qvp_rtp_media_type media_type, qvp_rtp_stream_direction direction);
QPVOID open_helper(qvp_rtp_media_type media_type);
QPVOID config_helper(qvp_rtp_media_type media_type);
void CodecConfigHelper(qvp_rtp_media_type media_type, qvp_rtp_stream_direction direction);

/* misc functions */
QPVOID readNV(void);
test_rtp_param *test_rtp_get_cmd(void);
QPVOID test_rtp_free_cmd(test_rtp_param *cmd);
QPVOID test_process_rtp_msg(test_rtp_param *test_rtp_msg);
QPVOID test_rtp_cmd_handle(QPVOID *cmd_ptr);
QPVOID test_rtp_cmd (QPVOID *msg);
QPVOID media_cb(qvp_rtp_status_type status, qvp_rtp_media_type media_type);
void execute_next_step_of_test_case(uint32 param);

/* test case step data */
extern test_monitor_data_t monitor_rtp_av;
extern test_monitor_data_t monitor_rtp_a;
extern test_monitor_data_t monitor_rtp_v;

extern test_monitor_data_t monitor_rtcp_av;
extern test_monitor_data_t monitor_rtcp_a;
extern test_monitor_data_t monitor_rtcp_v;

extern test_pause_resume_data_t pause_resume_av_txrx;
extern test_pause_resume_data_t pause_resume_av_tx;
extern test_pause_resume_data_t pause_resume_av_rx;

extern test_pause_resume_data_t pause_resume_a_txrx;
extern test_pause_resume_data_t pause_resume_a_tx;
extern test_pause_resume_data_t pause_resume_a_rx;

extern test_pause_resume_data_t pause_resume_v_txrx;
extern test_pause_resume_data_t pause_resume_v_tx;
extern test_pause_resume_data_t pause_resume_v_rx;

/* simplified macro definitions for test case steps */
#ifndef FEATURE_IMS_PC_TESTBENCH

#define RTP_REGISTER (test_case_step_t){TestRegister, NULL}

#define RTP_OPEN_AV (test_case_step_t){TestRTPOpen, (QPVOID *)STREAMS_AUDIO_VIDEO}
#define RTP_OPEN_A (test_case_step_t){TestRTPOpen, (QPVOID *)STREAMS_AUDIO}
#define RTP_OPEN_V (test_case_step_t){TestRTPOpen, (QPVOID *)STREAMS_VIDEO}

#define RTP_CONFIG_AV (test_case_step_t){TestRTPConfig, (QPVOID *)STREAMS_AUDIO_VIDEO}
#define RTP_CONFIG_A (test_case_step_t){TestRTPConfig, (QPVOID *)STREAMS_AUDIO}
#define RTP_CONFIG_V (test_case_step_t){TestRTPConfig, (QPVOID *)STREAMS_VIDEO}

#define CODEC_INIT_AV (test_case_step_t){TestCodecInit, (QPVOID *)STREAMS_AUDIO_VIDEO}
#define CODEC_INIT_A (test_case_step_t){TestCodecInit, (QPVOID *)STREAMS_AUDIO}
#define CODEC_INIT_V (test_case_step_t){TestCodecInit, (QPVOID *)STREAMS_VIDEO}
#define CODEC_INIT_V_TX (test_case_step_t){TestCodecInitTx, (QPVOID *)STREAMS_VIDEO}
#define CODEC_INIT_V_RX (test_case_step_t){TestCodecInitRx, (QPVOID *)STREAMS_VIDEO}
#define CODEC_V_INIT_TX_CONFIG_RX (test_case_step_t){TestCodecInitTXConfigRx, (QPVOID *)STREAMS_VIDEO}

#define CODEC_START_STREAM_AV (test_case_step_t){TestCodecStartStream, (QPVOID *)STREAMS_AUDIO_VIDEO}
#define CODEC_START_STREAM_A (test_case_step_t){TestCodecStartStream, (QPVOID *)STREAMS_AUDIO}
#define CODEC_START_STREAM_V (test_case_step_t){TestCodecStartStream, (QPVOID *)STREAMS_VIDEO}
#define CODEC_START_STREAM_V_TX (test_case_step_t){TestCodecStartStreamTx, (QPVOID *)STREAMS_VIDEO}
#define CODEC_START_STREAM_V_RX (test_case_step_t){TestCodecStartStreamRx, (QPVOID *)STREAMS_VIDEO}



#define CODEC_STOP_STREAM_AV (test_case_step_t){TestCodecStopStream, (QPVOID *)STREAMS_AUDIO_VIDEO}
#define CODEC_STOP_STREAM_A (test_case_step_t){TestCodecStopStream, (QPVOID *)STREAMS_AUDIO}
#define CODEC_STOP_STREAM_V (test_case_step_t){TestCodecStopStream, (QPVOID *)STREAMS_VIDEO}
#define CODEC_STOP_STREAM_V_TX (test_case_step_t){TestCodecStopStreamTx, (QPVOID *)STREAMS_VIDEO}
#define CODEC_STOP_STREAM_V_RX (test_case_step_t){TestCodecStopStreamRx, (QPVOID *)STREAMS_VIDEO}


#define CODEC_RELEASE_AV (test_case_step_t){TestCodecRelease, (QPVOID *)STREAMS_AUDIO_VIDEO}
#define CODEC_RELEASE_A (test_case_step_t){TestCodecRelease, (QPVOID *)STREAMS_AUDIO}
#define CODEC_RELEASE_V (test_case_step_t){TestCodecRelease, (QPVOID *)STREAMS_VIDEO}

#define RTP_CLOSE_AV (test_case_step_t){TestRTPClose, (QPVOID *)STREAMS_AUDIO_VIDEO}
#define RTP_CLOSE_A (test_case_step_t){TestRTPClose, (QPVOID *)STREAMS_AUDIO}
#define RTP_CLOSE_V (test_case_step_t){TestRTPClose, (QPVOID *)STREAMS_VIDEO}

#define RTP_MONITOR_AV (test_case_step_t){TestMonitor, (QPVOID *)(&monitor_rtp_av)}
#define RTP_MONITOR_A (test_case_step_t){TestMonitor, (QPVOID *)(&monitor_rtp_a)}
#define RTP_MONITOR_V (test_case_step_t){TestMonitor, (QPVOID *)(&monitor_rtp_v)}

#define RTCP_MONITOR_AV (test_case_step_t){TestMonitor, (QPVOID *)(&monitor_rtcp_av)}
#define RTCP_MONITOR_A (test_case_step_t){TestMonitor, (QPVOID *)(&monitor_rtcp_a)}
#define RTCP_MONITOR_V (test_case_step_t){TestMonitor, (QPVOID *)(&monitor_rtcp_v)}

#define PAUSE_AV_TXRX (test_case_step_t){TestPause, (QPVOID *)(&pause_resume_av_txrx)}
#define PAUSE_AV_TX (test_case_step_t){TestPause, (QPVOID *)(&pause_resume_av_tx)}
#define PAUSE_AV_RX (test_case_step_t){TestPause, (QPVOID *)(&pause_resume_av_rx)}

#define PAUSE_A_TXRX (test_case_step_t){TestPause, (QPVOID *)(&pause_resume_a_txrx)}
#define PAUSE_A_TX (test_case_step_t){TestPause, (QPVOID *)(&pause_resume_a_tx)}
#define PAUSE_A_RX (test_case_step_t){TestPause, (QPVOID *)(&pause_resume_a_rx)}

#define PAUSE_V_TXRX (test_case_step_t){TestPause, (QPVOID *)(&pause_resume_v_txrx)}
#define PAUSE_V_TX (test_case_step_t){TestPause, (QPVOID *)(&pause_resume_v_tx)}
#define PAUSE_V_RX (test_case_step_t){TestPause, (QPVOID *)(&pause_resume_v_rx)}

#define RESUME_AV_TXRX (test_case_step_t){TestResume, (QPVOID *)(&pause_resume_av_txrx)}
#define RESUME_AV_TX (test_case_step_t){TestResume, (QPVOID *)(&pause_resume_av_tx)}
#define RESUME_AV_RX (test_case_step_t){TestResume, (QPVOID *)(&pause_resume_av_rx)}

#define RESUME_A_TXRX (test_case_step_t){TestResume, (QPVOID *)(&pause_resume_a_txrx)}
#define RESUME_A_TX (test_case_step_t){TestResume, (QPVOID *)(&pause_resume_a_tx)}
#define RESUME_A_RX (test_case_step_t){TestResume, (QPVOID *)(&pause_resume_a_rx)}

#define RESUME_V_TXRX (test_case_step_t){TestResume, (QPVOID *)(&pause_resume_v_txrx)}
#define RESUME_V_TX (test_case_step_t){TestResume, (QPVOID *)(&pause_resume_v_tx)}
#define RESUME_V_RX (test_case_step_t){TestResume, (QPVOID *)(&pause_resume_v_rx)}

#define CODEC_CONFIG_AV (test_case_step_t){TestCodecConfig, (QPVOID *)STREAMS_AUDIO_VIDEO}
#define CODEC_CONFIG_A (test_case_step_t){TestCodecConfig, (QPVOID *)STREAMS_AUDIO}
#define CODEC_CONFIG_V (test_case_step_t){TestCodecConfig, (QPVOID *)STREAMS_VIDEO}
#define CODEC_CONFIG_V_TX (test_case_step_t){TestCodecConfigTx, (QPVOID *)STREAMS_VIDEO}
#define CODEC_CONFIG_V_RX (test_case_step_t){TestCodecConfigRx, (QPVOID *)STREAMS_VIDEO}

#define RTP_SESSION_CONFIG_AV (test_case_step_t){TestRTPSessionConfig, (QPVOID *)STREAMS_AUDIO_VIDEO}
#define RTP_SESSION_CONFIG_A (test_case_step_t){TestRTPSessionConfig, (QPVOID *)STREAMS_AUDIO}
#define RTP_SESSION_CONFIG_V (test_case_step_t){TestRTPSessionConfig, (QPVOID *)STREAMS_VIDEO}

#define CLEAR_ALL_RESOURCES_AV (test_case_step_t){ClearAllResources, (QPVOID *)STREAMS_AUDIO_VIDEO}
#define CLEAR_ALL_RESOURCES_A (test_case_step_t){ClearAllResources, (QPVOID *)STREAMS_AUDIO}
#define CLEAR_ALL_RESOURCES_V (test_case_step_t){ClearAllResources, (QPVOID *)STREAMS_VIDEO}

#define CALL_UPDATE_START (test_case_step_t){TestCallUpdateStart, NULL}

#define CALL_UPDATE_END (test_case_step_t){TestCallUpdateEnd, NULL}

#define HANDOFF (test_case_step_t){TestHandoff, NULL}

#define DTMFSend (test_case_step_t){TestDTMFSend, NULL}

#define TEST_CASE_COMPLETE (test_case_step_t){TestCaseComplete, NULL}

#define SRVCC_TEAR_DOWN (test_case_step_t){TestSrvccTearDown, NULL}
#define CONFIG_GBR_MBR (test_case_step_t){TestConfigGbrMbr, NULL}


#define SWAP_CALL (test_case_step_t){TestSwapCall, NULL}

#define UNSWAP_CALL (test_case_step_t){TestUnswapCall, NULL}

#define TEST_CASE_NULL (test_case_step_t){NULL, NULL}

#else

#define RTP_REGISTER (test_case_step_t){TestRegister, NULL}

#define RTP_OPEN_AV (test_case_step_t){TestRTPOpen, {(QPVOID *)STREAMS_AUDIO_VIDEO}}
#define RTP_OPEN_A (test_case_step_t){TestRTPOpen, {(QPVOID *)STREAMS_AUDIO}}
#define RTP_OPEN_V (test_case_step_t){TestRTPOpen, {(QPVOID *)STREAMS_VIDEO}}

#define RTP_CONFIG_AV (test_case_step_t){TestRTPConfig, {(QPVOID *)STREAMS_AUDIO_VIDEO}}
#define RTP_CONFIG_A (test_case_step_t){TestRTPConfig, {(QPVOID *)STREAMS_AUDIO}}
#define RTP_CONFIG_V (test_case_step_t){TestRTPConfig, {(QPVOID *)STREAMS_VIDEO}}

#define CODEC_INIT_AV (test_case_step_t){TestCodecInit, {(QPVOID *)STREAMS_AUDIO_VIDEO}}
#define CODEC_INIT_A (test_case_step_t){TestCodecInit, {(QPVOID *)STREAMS_AUDIO}}
#define CODEC_INIT_V (test_case_step_t){TestCodecInit, {(QPVOID *)STREAMS_VIDEO}}
#define CODEC_INIT_V_TX (test_case_step_t){TestCodecInitTx, (QPVOID *)STREAMS_VIDEO}
#define CODEC_INIT_V_RX (test_case_step_t){TestCodecInitRx, (QPVOID *)STREAMS_VIDEO}
#define CODEC_V_INIT_TX_CONFIG_RX (test_case_step_t){TestCodecInitTXConfigRx, (QPVOID *)STREAMS_VIDEO}


#define CODEC_START_STREAM_AV (test_case_step_t){TestCodecStartStream, {(QPVOID *)STREAMS_AUDIO_VIDEO}}
#define CODEC_START_STREAM_A (test_case_step_t){TestCodecStartStream, {(QPVOID *)STREAMS_AUDIO}}
#define CODEC_START_STREAM_V (test_case_step_t){TestCodecStartStream, {(QPVOID *)STREAMS_VIDEO}}
#define CODEC_START_STREAM_V_TX (test_case_step_t){TestCodecStartStreamTx, (QPVOID *)STREAMS_VIDEO}
#define CODEC_START_STREAM_V_RX (test_case_step_t){TestCodecStartStreamRx, (QPVOID *)STREAMS_VIDEO}

#define CODEC_STOP_STREAM_AV (test_case_step_t){TestCodecStopStream, {(QPVOID *)STREAMS_AUDIO_VIDEO}}
#define CODEC_STOP_STREAM_A (test_case_step_t){TestCodecStopStream, {(QPVOID *)STREAMS_AUDIO}}
#define CODEC_STOP_STREAM_V (test_case_step_t){TestCodecStopStream, {(QPVOID *)STREAMS_VIDEO}}
#define CODEC_STOP_STREAM_V_TX (test_case_step_t){TestCodecStopStreamTx, (QPVOID *)STREAMS_VIDEO}
#define CODEC_STOP_STREAM_V_RX (test_case_step_t){TestCodecStopStreamRx, (QPVOID *)STREAMS_VIDEO}

#define CODEC_RELEASE_AV (test_case_step_t){TestCodecRelease, {(QPVOID *)STREAMS_AUDIO_VIDEO}}
#define CODEC_RELEASE_A (test_case_step_t){TestCodecRelease, {(QPVOID *)STREAMS_AUDIO}}
#define CODEC_RELEASE_V (test_case_step_t){TestCodecRelease, {(QPVOID *)STREAMS_VIDEO}}

#define RTP_CLOSE_AV (test_case_step_t){TestRTPClose, {(QPVOID *)STREAMS_AUDIO_VIDEO}}
#define RTP_CLOSE_A (test_case_step_t){TestRTPClose, {(QPVOID *)STREAMS_AUDIO}}
#define RTP_CLOSE_V (test_case_step_t){TestRTPClose, {(QPVOID *)STREAMS_VIDEO}}

#define RTP_MONITOR_AV (test_case_step_t){TestMonitor, {(QPVOID *)(&monitor_rtp_av)}}
#define RTP_MONITOR_A (test_case_step_t){TestMonitor, {(QPVOID *)(&monitor_rtp_a)}}
#define RTP_MONITOR_V (test_case_step_t){TestMonitor, {(QPVOID *)(&monitor_rtp_v)}}

#define RTCP_MONITOR_AV (test_case_step_t){TestMonitor, {(QPVOID *)(&monitor_rtcp_av)}}
#define RTCP_MONITOR_A (test_case_step_t){TestMonitor, {(QPVOID *)(&monitor_rtcp_a)}}
#define RTCP_MONITOR_V (test_case_step_t){TestMonitor, {(QPVOID *)(&monitor_rtcp_v)}}

#define PAUSE_AV_TXRX (test_case_step_t){TestPause, {(QPVOID *)(&pause_resume_av_txrx)}}
#define PAUSE_AV_TX (test_case_step_t){TestPause, {(QPVOID *)(&pause_resume_av_tx)}}
#define PAUSE_AV_RX (test_case_step_t){TestPause, {(QPVOID *)(&pause_resume_av_rx)}}

#define PAUSE_A_TXRX (test_case_step_t){TestPause, {(QPVOID *)(&pause_resume_a_txrx)}}
#define PAUSE_A_TX (test_case_step_t){TestPause, {(QPVOID *)(&pause_resume_a_tx)}}
#define PAUSE_A_RX (test_case_step_t){TestPause, {(QPVOID *)(&pause_resume_a_rx)}}

#define PAUSE_V_TXRX (test_case_step_t){TestPause, {(QPVOID *)(&pause_resume_v_txrx)}}
#define PAUSE_V_TX (test_case_step_t){TestPause, {(QPVOID *)(&pause_resume_v_tx)}}
#define PAUSE_V_RX (test_case_step_t){TestPause, {(QPVOID *)(&pause_resume_v_rx)}}

#define RESUME_AV_TXRX (test_case_step_t){TestResume, {(QPVOID *)(&pause_resume_av_txrx)}}
#define RESUME_AV_TX (test_case_step_t){TestResume, {(QPVOID *)(&pause_resume_av_tx)}}
#define RESUME_AV_RX (test_case_step_t){TestResume, {(QPVOID *)(&pause_resume_av_rx)}}

#define RESUME_A_TXRX (test_case_step_t){TestResume, {(QPVOID *)(&pause_resume_a_txrx)}}
#define RESUME_A_TX (test_case_step_t){TestResume, {(QPVOID *)(&pause_resume_a_tx)}}
#define RESUME_A_RX (test_case_step_t){TestResume, {(QPVOID *)(&pause_resume_a_rx)}}

#define RESUME_V_TXRX (test_case_step_t){TestResume, {(QPVOID *)(&pause_resume_v_txrx)}}
#define RESUME_V_TX (test_case_step_t){TestResume, {(QPVOID *)(&pause_resume_v_tx)}}
#define RESUME_V_RX (test_case_step_t){TestResume, {(QPVOID *)(&pause_resume_v_rx)}}

#define CODEC_CONFIG_AV (test_case_step_t){TestCodecConfig, {(QPVOID *)STREAMS_AUDIO_VIDEO}}
#define CODEC_CONFIG_A (test_case_step_t){TestCodecConfig, {(QPVOID *)STREAMS_AUDIO}}
#define CODEC_CONFIG_V (test_case_step_t){TestCodecConfig, {(QPVOID *)STREAMS_VIDEO}}
#define CODEC_CONFIG_V_TX (test_case_step_t){TestCodecConfigTx, (QPVOID *)STREAMS_VIDEO}
#define CODEC_CONFIG_V_RX (test_case_step_t){TestCodecConfigRx, (QPVOID *)STREAMS_VIDEO}


#define RTP_SESSION_CONFIG_AV (test_case_step_t){TestRTPSessionConfig, {(QPVOID *)STREAMS_AUDIO_VIDEO}}
#define RTP_SESSION_CONFIG_A (test_case_step_t){TestRTPSessionConfig, {(QPVOID *)STREAMS_AUDIO}}
#define RTP_SESSION_CONFIG_V (test_case_step_t){TestRTPSessionConfig, {(QPVOID *)STREAMS_VIDEO}}

#define CALL_UPDATE_START (test_case_step_t){TestCallUpdateStart, NULL}

#define CALL_UPDATE_END (test_case_step_t){TestCallUpdateEnd, NULL}

#define HANDOFF (test_case_step_t){TestHandoff, NULL}

#define DTMFSend (test_case_step_t){TestDTMFSend, NULL}

#define SRVCC_TEAR_DOWN (test_case_step_t){TestSrvccTearDown, NULL}
#define CONFIG_GBR_MBR (test_case_step_t){TestConfigGbrMbr, NULL}

#define SWAP_CALL (test_case_step_t){TestSwapCall, NULL}

#define UNSWAP_CALL (test_case_step_t){TestUnswapCall, NULL}

#define TEST_CASE_NULL (test_case_step_t){NULL, NULL}

#endif


/* combinations of common test case steps */
#define START_VT_CALL RTP_OPEN_AV, RTP_CONFIG_AV, CODEC_INIT_AV, RESUME_AV_TXRX, CODEC_START_STREAM_AV
#define START_VOLTE_CALL RTP_OPEN_A, RTP_CONFIG_A, CODEC_INIT_A, RESUME_A_TXRX, CODEC_START_STREAM_A
#define STOP_VT_CALL CODEC_STOP_STREAM_AV, CODEC_RELEASE_AV, RTP_CLOSE_AV
#define STOP_VOLTE_CALL CODEC_STOP_STREAM_A, CODEC_RELEASE_A, RTP_CLOSE_A
#define UPGRADE_CALL CALL_UPDATE_START, RTP_OPEN_V, RTP_CONFIG_V, CODEC_INIT_V, CODEC_START_STREAM_V, CALL_UPDATE_END
#define DOWNGRADE_CALL CALL_UPDATE_START, CODEC_STOP_STREAM_V, CODEC_RELEASE_V, RTP_CLOSE_V, CALL_UPDATE_END
#define VT_CALL RTP_OPEN_AV, RTP_CONFIG_AV,CODEC_INIT_A, CODEC_INIT_V_RX, RESUME_A_TXRX, CODEC_START_STREAM_A, CODEC_V_INIT_TX_CONFIG_RX, RESUME_V_TXRX, CODEC_START_STREAM_V



/* single entry in mapping from step names to steps*/
typedef struct test_case_step_map_item_t
{
  char *name;
  test_case_step_t step;
  struct test_case_step_map_item_t *next;
} test_case_step_map_item_t;

#define TEST_CASE_STEP_MAP_SIZE 1001


#endif // IMS_VT_MEDIA_TEST_FRAMEWORK
