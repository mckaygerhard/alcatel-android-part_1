/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                         QVP_RTP_H264_PROFILE . C

GENERAL DESCRIPTION

  This file contains the implementation of MP4 profile. This file takes 
  care of payload fragmentation, reassebmly etc.

EXTERNALIZED FUNCTIONS
  None. Exposed through a profile table. All the functions are static
  but exposed through function pointers.


INITIALIZATION AND SEQUENCING REQUIREMENTS

  None of the externalized functions will function before the task is 
  spawned.


  Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/ps_media/rtp_profile/src/qvp_rtp_h264_profile.c#1 $ $DateTime: 2016/03/28 23:03:22 $ $Author: mplcsds1 $

                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
01/aug/11   sha    Initial Creation.
===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP

#ifdef  RTP_FILE_NUMBER 
  #undef RTP_FILE_NUMBER 
#endif 
#define RTP_FILE_NUMBER 26

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <comdef.h>               /* common target/c-sim defines */
#include <string.h>               /* memory routines */
#include <stdlib.h>               /* for malloc */
#include <comdef.h>               /* common target/c-sim defines */
#include "qvp_rtp_api.h"          /* return type propagation */
#include "qvp_rtp_buf.h"
#include "qvp_rtp_profile.h"      /* profile template data type */
#include "qvp_rtp_h264_profile.h"
#include "qvp_rtp_buf.h"          /* for memory management */
#include "qvp_rtp_log.h"
#include "stdio.h"
#include <bit.h>
#include <list.h>
#include "qpConfigNVItem.h"

/*===========================================================================
                    H264 DEFINITIONS 
===========================================================================*/
#define H264_FU_HDR_SIZE  2
#define H264_FU_INDICATOR  0x1C
#define H264_MASK_PAYLOAD  0x1F
#define H264_SET_START_BIT 0x80
#define H264_SET_END_BIT   0x40
#define H264_SET_SUBSEQ_BIT 0x00
#define H264_MASK_NRI_VAL 0x60
#define H264_NAL_UNIT_OFFSET 4
#define H264_NALUNIT_TYPE_24 24 //Single-time Aggregation Packet Type A (STAP-A)
#define H264_NALUNIT_TYPE_27 27
#define H264_NALUNIT_TYPE_28 28 //Fragmentation Unit type A (FU-A)
#define H264_NALUNIT_TYPE_23 23
#define H264_PAYLOAD_HDR_SIZE  2 
#define QP_MASK_P_BIT             0x04
#define QP_SHIFT_P_BIT            0x02
#define H264_SPS_OCTET_VALUE  0x60// 0x65
#define H264_PPS_OCTET_VALUE  0x60// 0x67
#define MAX_PAYLOAD_BUFFER_SIZE_H264FRAG 1200
#define MAX_PAYLOAD_BUFFER_SIZE 1400 - QVP_RTP_HEAD_ROOM
/*===========================================================================
                    LOCAL STATIC FUNCTIONS 
===========================================================================*/

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_init
(
  qvp_rtp_profile_config_type *config
);

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl     /* user handle for any cb */
  
);


LOCAL void qvp_rtp_h264_reset_stream
( 
  qvp_rtp_h264_ctx_type *stream 
);

LOCAL void qvp_rtp_h264_reset_tx_ctx
( 
  qvp_rtp_h264_ctx_type *stream 
);

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_copy_config
(
  qvp_rtp_payload_config_type *payld_config,
  qvp_rtp_h264_config_type    *h264_config
);

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_config_tx_param
(
  qvp_rtp_h264_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_config_rx_param
(
  qvp_rtp_h264_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_read_deflt_config
(
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_validate_config_rx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_validate_config_tx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
);

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * the profile formatting
                                              */

);

LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

);

LOCAL void qvp_rtp_h264_profile_shutdown( void );


LOCAL uint16 qvp_rtp_h264_fragment_and_send( 

uint16 iDataLen, 
uint16 iSentByte, 
qvp_rtp_buf_type              *pkt,
boolean  firstbit
);


LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_recv_3016
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * the profile formatting
                                              */

);
LOCAL int32 qvp_rtp_find_nal_unit(uint8* buf, int32 buf_len, int32* nal_start, int32* nal_end);
LOCAL int32 power(int32 x, uint32 y);

/*===========================================================================
                    LOCAL STATIC DATA 
===========================================================================*/
/*--------------------------------------------------------------------------
    Table of accessors for the profile. This need to be populated in
    the table entry at qvp_rtp_profole.c. All these functions will 
    automatically link to the RTP stack by doing so.
--------------------------------------------------------------------------*/
qvp_rtp_profile_type qvp_rtp_h264_profile_table = 
{
  qvp_rtp_h264_profile_init,            /* LOACAL initialization routine  */
  qvp_rtp_h264_profile_open,            /* LOCAL function to open channel */
  qvp_rtp_h264_profile_send,            /* LOCAL function to send a pkt   */
  qvp_rtp_h264_profile_recv,            /* LOCAL function to rx a nw pkt  */
  qvp_rtp_h264_profile_close,           /* LOCAL function to close channel */
  qvp_rtp_h264_profile_read_deflt_config, /* read the defaults */
  NULL,                            /* read config not supported for now  */
  qvp_rtp_h264_profile_validate_config_rx,
  qvp_rtp_h264_profile_validate_config_tx,
  qvp_rtp_h264_profile_configure,       /* configure the payld format */ 
  qvp_rtp_h264_profile_shutdown         /* LOCAL function to shut down  */

};


/*===========================================================================
                     LOCAL STATIC DATA 
===========================================================================*/

LOCAL boolean h264_initialized = FALSE;
/*--------------------------------------------------------------------------
        Stream context array.  Responsible for each bidirectional 
        connections
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_h264_ctx_type *qvp_rtp_h264_array = NULL;
/*--------------------------------------------------------------------------
     Stores the configuration requested by the app.                        
--------------------------------------------------------------------------*/
LOCAL  qvp_rtp_profile_config_type h264_profile_config;

/*--------------------------------------------------------------------------
     Configuration pertaining payload formatting for H264 
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_h264_config_type h264_stream_config;

/*--------------------------------------------------------------------------
     Local variable to know if we are in fragmenting mode
--------------------------------------------------------------------------*/
LOCAL boolean g_bIsFragment;

/*--------------------------------------------------------------------------
     Local variable to know if we are in fragmenting mode
--------------------------------------------------------------------------*/
LOCAL uint16 g_rtp_mtu_size;
/*===========================================================================

FUNCTION QVP_RTP_H264_PROFILE_INIT 


DESCRIPTION
  Initializes data structures and resources used by the H264 profile.

DEPENDENCIES
  None

ARGUMENTS IN
  config - pointer to profile configuration requested.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could initialize the profile. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_init
(
  qvp_rtp_profile_config_type *config
)
{

 QPIMS_NV_CONFIG_DATA pConfig;
/*------------------------------------------------------------------------*/


  if( h264_initialized )
  {
    return( QVP_RTP_SUCCESS );
  }
   
  /*------------------------------------------------------------------------
    Init if we get a valid config  and appropriate call backs 
    for the basic operation of the profile.
  ------------------------------------------------------------------------*/
  if( config && config->rx_cb && config->tx_cb )
  {
    qvp_rtp_memscpy( &h264_profile_config,sizeof(qvp_rtp_profile_config_type), config, sizeof( h264_profile_config ) );
  }
  else
  {
    return( QVP_RTP_WRONG_PARAM );
  }

  /*------------------------------------------------------------------------
    Allocate the number of streams first
  ------------------------------------------------------------------------*/
  qvp_rtp_h264_array  = qvp_rtp_malloc( config->num_streams * 
                                sizeof( qvp_rtp_h264_ctx_type ) ); 

  /*------------------------------------------------------------------------
      See if we could get the memory
  ------------------------------------------------------------------------*/
  if( !qvp_rtp_h264_array )
  {
    //QVP_RTP_ERR( "Could not allocate qvp_rtp_h264_array during init ", 
      //                          0, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }
  else
  {
    memset( qvp_rtp_h264_array, 0, config->num_streams * 
                    sizeof( qvp_rtp_h264_ctx_type ) );
    
    /*----------------------------------------------------------------------
        Set up the default configuration 
    ----------------------------------------------------------------------*/
    
    
    //TODO

  }
    
  /*------------------------------------------------------------------------
    Flag initialization
  ------------------------------------------------------------------------*/
  h264_initialized  = TRUE;
  

  memset(&pConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
  pConfig.env_Hdr = eQP_IMS_MEDIA_CONFIG;

  g_rtp_mtu_size = MAX_PAYLOAD_BUFFER_SIZE;
  if (qpdpl_get_config_group_value(&pConfig) == QP_IO_ERROR_OK)
  {
    if(pConfig.uIMSConfigPayload.sMediaConfigItem.iRTPMTUSz > 700 && pConfig.uIMSConfigPayload.sMediaConfigItem.iRTPMTUSz < 1500)
    {
      QVP_RTP_MSG_MED_1( "qvp_rtp_h264_profile_init :: RTP MTU Size set to  %d", 0);
      g_rtp_mtu_size = pConfig.uIMSConfigPayload.sMediaConfigItem.iRTPMTUSz;
    }
  }

  return( QVP_RTP_SUCCESS );

}/* end of function qvp_rtp_h264_profile_init */

/*===========================================================================

FUNCTION  QVP_RTP_H264_PROFILE_SEND


DESCRIPTION
  request to send specified buffer through this profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl     - handle into the profile. 
  usr_hdl - handle to use when we call send function using the registered
            tx callback
  pkt     - packet of data to be sent.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_send
(
 qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
 qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
 qvp_rtp_buf_type              *pkt          /* packet to be formatted 
                                             * through the profile
                                             */
                                             )
{

  qvp_rtp_h264_ctx_type *stream  = ( qvp_rtp_h264_ctx_type *) hdl;
  qvp_rtp_buf_type *nw_buf       = NULL;
  uint16   iCurrentNALUSize = 0;
  uint16   len = pkt->len;
  uint8*   src = pkt->data+QVP_RTP_HEAD_ROOM;
  int32 nal_start;
  int32 nal_end;
  int32 ret;

  /*------------------------------------------------------------------------
  If no tx cb or uninitialized bail out
  ------------------------------------------------------------------------*/
  if(!stream ||  !h264_profile_config.tx_cb || !h264_initialized || !pkt)
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
  If there is nothing to be sent ignore it
  ------------------------------------------------------------------------*/
  if( !pkt || !pkt->len || !pkt->data)
  {
    QVP_RTP_ERR( "qvp_rtp_h264_profile_send.. There is nothing to be sent ...ignoring  ", 
      0, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }
  QVP_RTP_MSG_LOW_2( "qvp_rtp_h264_profile_send.. packetization mode = %d and AU lenght is = %d", stream->stream_config.packetization_mode, pkt->len, 
    0 );   

  /* handle single nalu packetization mode */
  if(stream->stream_config.packetization_mode == 0) 
  {
    /* From RFC 6184 single nalu layout */
    /*---------------------------------------------------------------
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |F|NRI|  Type   |                                               |
    +-+-+-+-+-+-+-+-+                                               |
    |                                                               |
    |               Bytes 2..n of a single NAL unit                 |
    |                                                               |
    |                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                               :...OPTIONAL RTP padding        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    ----------------------------------------------------------------*/


    do
    {

      ret = qvp_rtp_find_nal_unit(src,len,&nal_start,&nal_end);
      if(ret <=0)
      {
        //TBD what to do in error case
        QVP_RTP_ERR("qvp_rtp_h264_profile_send.. Failed to find nal start/end and nal size, error = %d",ret,0,0);
        //free the source rtp buf
        if(pkt->need_tofree)
        {
          qvp_rtp_free_buf(pkt);
        }
        return QVP_RTP_ERR_FATAL;
      }
      iCurrentNALUSize = (uint16)ret;
      src += nal_start;//jump to nal start
      len -= nal_start;
      len -= iCurrentNALUSize;
      QVP_RTP_MSG_LOW_2( "qvp_rtp_h264_profile_send.. current nalu size = %d & Reduced AU lenght = %d", iCurrentNALUSize, ((len-iCurrentNALUSize)-nal_start), 
        0 );

      if(stream->stream_config.reliable_txmit)
      {
        uint8 type = b_unpackb(src,3,5);
        QVP_RTP_MSG_LOW_0( "qvp_rtp_h264_profile_send.. sps/pps retransmission on..");

        switch(type)
        {
        case 7://sps
          QVP_RTP_MSG_LOW_1( "qvp_rtp_h264_profile_send.. sps cached len is %d..", iCurrentNALUSize);

          qvp_rtp_memscpy( stream->stream_config.cached_sps,40, src,
            iCurrentNALUSize);
          stream->stream_config.cached_sps_len = iCurrentNALUSize;
          stream->stream_config.ps_cnt = 0;
          if(len == 0){
            stream->stream_config.reliable_txmit = FALSE;
            stream->stream_config.reliable_txmit_timer = TRUE;
            stream->stream_config.PS_timer = pkt->tstamp;
            QVP_RTP_MSG_LOW_0( "qvp_rtp_h264_profile_send.. sps/pps retransmission off..");

          }
          break;
        case 8://pps
          QVP_RTP_MSG_LOW_1( "qvp_rtp_h264_profile_send.. pps cached len is %d..", iCurrentNALUSize);

          qvp_rtp_memscpy( stream->stream_config.cached_pps,40, src,
            iCurrentNALUSize);
          stream->stream_config.cached_pps_len = iCurrentNALUSize;
          if(len == 0){
            stream->stream_config.reliable_txmit = FALSE;
            stream->stream_config.reliable_txmit_timer = TRUE;
            stream->stream_config.PS_timer = pkt->tstamp;
            QVP_RTP_MSG_LOW_0( "qvp_rtp_h264_profile_send.. sps/pps retransmission off..");

          }
          stream->stream_config.ps_cnt = 0;
          break;
        default:
          QVP_RTP_MSG_LOW_1( "qvp_rtp_h264_profile_send.. retransmission on but default case %d..", type);

          break;
        }
      }
      if(stream->stream_config.reliable_txmit_timer)
      {
        QVP_RTP_MSG_LOW_0( "qvp_rtp_h264_profile_send.. retransmission timer is on");

        if(stream->stream_config.PS_timer <= pkt->tstamp)
        {
          if(stream->stream_config.ps_cnt)
          {
            uint8 i=0;
            uint16 temp_len;
            uint8 * temp_src;
            QVP_RTP_MSG_LOW_1( "qvp_rtp_h264_profile_send.. retransmitting sps/pps %d ", stream->stream_config.ps_cnt);

            for(;i<2;i++)
            {
              if(i==0)//send sps
              {
                temp_len = stream->stream_config.cached_sps_len;
                temp_src = stream->stream_config.cached_sps;
              }
              else//send pps
              {
                temp_len = stream->stream_config.cached_pps_len;
                temp_src =stream->stream_config.cached_pps;
              }
              nw_buf  = qvp_rtp_alloc_buf_by_len( temp_len + QVP_RTP_HEAD_ROOM );
              if( !nw_buf )
              {
                QVP_RTP_ERR("qvp_rtp_h264_profile_send.. Failed allocate a buffer arrggggggg....", 0, 0, 0 );
                //free the source rtp buf
                if(pkt->need_tofree)
                {
                  qvp_rtp_free_buf(pkt);
                }
                qvp_rtp_h264_reset_tx_ctx( stream );
                return( QVP_RTP_ERR_FATAL );
              }
              memset(nw_buf->data,0,temp_len + QVP_RTP_HEAD_ROOM);
              /*----------------------------------------------------------------------
              Set up room for RTP headers
              ----------------------------------------------------------------------*/
              nw_buf->head_room = QVP_RTP_HEAD_ROOM;      

              nw_buf->need_tofree = TRUE;
              qvp_rtp_memscpy( nw_buf->data + QVP_RTP_HEAD_ROOM,temp_len, temp_src,
                temp_len);

              nw_buf->len = temp_len;
              /*----------------------------------------------------------------------
              set up other RTP fields
              ----------------------------------------------------------------------*/
              nw_buf->tstamp  = pkt->tstamp;
              nw_buf->rtp_pyld_type = pkt->rtp_pyld_type;
              h264_profile_config.tx_cb( nw_buf, usr_hdl );
              QVP_RTP_MSG_MED( " Sending H264 bundle len = %d, marker = %d, \
                                timestamp = %d", nw_buf->len, nw_buf->marker_bit, 
                                nw_buf->tstamp );
              nw_buf = NULL;
            }
          }
          stream->stream_config.ps_cnt++;
          if(stream->stream_config.ps_cnt < 6/*PS_cnt_threshold*/)
          {
            int32 temp = power(2,stream->stream_config.ps_cnt-1);
            temp *= 500*90;/* clock rate */
            stream->stream_config.PS_timer = pkt->tstamp;
            stream->stream_config.PS_timer += temp;
          }
          else
          {
            stream->stream_config.reliable_txmit_timer = FALSE;
            stream->stream_config.ps_cnt = 0;
            QVP_RTP_MSG_LOW_0("qvp_rtp_h264_profile_send.. retransmission timer is off");
          }
        }
      }
      nw_buf  = qvp_rtp_alloc_buf_by_len( iCurrentNALUSize + QVP_RTP_HEAD_ROOM );

      if( !nw_buf )
      {
        QVP_RTP_ERR("qvp_rtp_h264_profile_send.. Failed allocate a buffer arrggggggg....", 0, 0, 0 );
        //free the source rtp buf
        if(pkt->need_tofree)
        {
          qvp_rtp_free_buf(pkt);
        }
        qvp_rtp_h264_reset_tx_ctx( stream );
        return( QVP_RTP_ERR_FATAL );
      }
      memset(nw_buf->data,0,iCurrentNALUSize + QVP_RTP_HEAD_ROOM);
      /*----------------------------------------------------------------------
      Set up room for RTP headers
      ----------------------------------------------------------------------*/
      nw_buf->head_room = QVP_RTP_HEAD_ROOM;      

      nw_buf->need_tofree = TRUE;
      qvp_rtp_memscpy( nw_buf->data + QVP_RTP_HEAD_ROOM,iCurrentNALUSize, src,
        iCurrentNALUSize);

      nw_buf->len = iCurrentNALUSize;
      src += iCurrentNALUSize;
      /*----------------------------------------------------------------------
      set up other RTP fields
      ----------------------------------------------------------------------*/
      nw_buf->tstamp  = pkt->tstamp;
      nw_buf->rtp_pyld_type = pkt->rtp_pyld_type;
      if(len == 0)
      {
        nw_buf->marker_bit = 1;
        //free the source rtp buf
        if(pkt->need_tofree)
        {
          qvp_rtp_free_buf(pkt);
        }
      }
      else
        nw_buf->marker_bit = 0;

      h264_profile_config.tx_cb( nw_buf, usr_hdl );
      QVP_RTP_MSG_MED_2( " Sending H264 bundle len = %d, marker = %d, \
                        timestamp = %d", nw_buf->len, nw_buf->marker_bit, 
                        nw_buf->tstamp );
    }while(len > 2);//discuss with shankar about this check

  }/*end of single nalu mode */
  else if(stream->stream_config.packetization_mode == 1) /* handle non interleaved mode */
  {
    uint16   iRTPPayloadSize = 0;
    uint16   stapAsize = 0;
    uint8    iNumOfNalus = 0;
    uint8 *  first_nalu_temp = NULL;
    uint16   first_nalu_size = 0;
    //int32    first_nalu_start = 0;
    boolean first_nalu = FALSE;
    boolean pkt_assembled = FALSE;
    uint8 forbidden_zero_bit = 0;
    uint16  nw_buf_offset = QVP_RTP_HEAD_ROOM;
    uint8 nal_ref_idc = 0;
    uint16 SN = (uint16)pkt->seq;
    nw_buf = NULL;

    QVP_RTP_MSG_LOW_0( "qvp_rtp_h264_profile_send.. non-interleaved mode selected");

    do
    {
      ret = qvp_rtp_find_nal_unit(src,len,&nal_start,&nal_end);
      if(ret <=0)
      {
        //TBD what to do in error case
        QVP_RTP_ERR("qvp_rtp_h264_profile_send.. Failed to find nal start/end and nal size, error = %d",ret,0,0);
        //free the source rtp buf
        if(pkt->need_tofree)
        {
          qvp_rtp_free_buf(pkt);
        }
        return QVP_RTP_ERR_FATAL;
      }

      iCurrentNALUSize = (uint16)ret;
      QVP_RTP_MSG_LOW_2( "qvp_rtp_h264_profile_send.. current nalu size = %d & Reduced AU lenght = %d", iCurrentNALUSize, ((len-iCurrentNALUSize)-nal_start), 
        0 );
      //TBD +2 nal size to be added for the size check?
      stapAsize = iCurrentNALUSize + iRTPPayloadSize;
      stapAsize += (iNumOfNalus+1)*2;
      stapAsize += 1;
      if((!iNumOfNalus && (iCurrentNALUSize <= g_rtp_mtu_size))|| (iNumOfNalus && (stapAsize <= g_rtp_mtu_size)))
      {

        /* From RFC 6184  STAP-A layout */
        /*---------------------------------------------------------------

        0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                          RTP Header                           |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |STAP-A NAL HDR |         NALU 1 Size           | NALU 1 HDR    |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                         NALU 1 Data                           |
        :                                                               :
        +               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |               | NALU 2 Size                   | NALU 2 HDR    |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                         NALU 2 Data                           |
        :                                                               :
        |                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                               :...OPTIONAL RTP padding        |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

        ----------------------------------------------------------------*/
        src += nal_start;//jump to nal start
        len -= nal_start;
        len -= iCurrentNALUSize;
        if(stream->stream_config.reliable_txmit)
        {
          uint8 type = b_unpackb(src,3,5);
          QVP_RTP_MSG_LOW_0( "qvp_rtp_h264_profile_send.. sps/pps retransmission on..");

          switch(type)
          {
          case 7://sps
            QVP_RTP_MSG_LOW_1( "qvp_rtp_h264_profile_send.. sps cached len is %d..", iCurrentNALUSize);

            qvp_rtp_memscpy( stream->stream_config.cached_sps,40, src,
              iCurrentNALUSize);
            stream->stream_config.cached_sps_len = iCurrentNALUSize;
            stream->stream_config.ps_cnt = 0;
            if(len == 0){
              stream->stream_config.reliable_txmit = FALSE;
              stream->stream_config.reliable_txmit_timer = TRUE;
              stream->stream_config.PS_timer = pkt->tstamp;
            }
            break;
          case 8://pps
            QVP_RTP_MSG_LOW_1( "qvp_rtp_h264_profile_send.. pps cached len is %d..", iCurrentNALUSize);

            qvp_rtp_memscpy( stream->stream_config.cached_pps,40, src,
              iCurrentNALUSize);
            stream->stream_config.cached_pps_len = iCurrentNALUSize;
            if(len == 0){
              stream->stream_config.reliable_txmit = FALSE;
              stream->stream_config.reliable_txmit_timer = TRUE;
              stream->stream_config.PS_timer = pkt->tstamp;
            }
            stream->stream_config.ps_cnt = 0;
            break;
          default:
            break;
          }
        }
        if(stream->stream_config.reliable_txmit_timer)
        {
          QVP_RTP_MSG_LOW_0( "qvp_rtp_h264_profile_send.. retransmission timer is on");

          if(stream->stream_config.PS_timer <= pkt->tstamp)
          {
            if(stream->stream_config.ps_cnt)
            {
              uint8 i=0;
              uint16 temp_len;
              uint8 * temp_src;
              uint8 offset = QVP_RTP_HEAD_ROOM;
              QVP_RTP_MSG_LOW_1( "qvp_rtp_h264_profile_send.. retransmitting sps/pps %d ", stream->stream_config.ps_cnt);

              if(!nw_buf){
                nw_buf  = qvp_rtp_alloc_buf_by_len( stream->stream_config.cached_sps_len + stream->stream_config.cached_pps_len + 5 + QVP_RTP_HEAD_ROOM );
                memset(nw_buf->data,0,stream->stream_config.cached_sps_len + stream->stream_config.cached_pps_len + 5 + QVP_RTP_HEAD_ROOM);
              }
              if( !nw_buf )
              {
                QVP_RTP_ERR("qvp_rtp_h264_profile_send.. Failed allocate a buffer arrggggggg....", 0, 0, 0 );
                //free the source rtp buf
                if(pkt->need_tofree)
                {
                  qvp_rtp_free_buf(pkt);
                }
                qvp_rtp_h264_reset_tx_ctx( stream );
                return( QVP_RTP_ERR_FATAL );
              }
              for(;i<2;i++)
              {
                if(i==0)//send sps
                {
                  temp_len = stream->stream_config.cached_sps_len;
                  temp_src = stream->stream_config.cached_sps;
                  //stap-a nal header
                  offset++;
                }
                else//send pps
                {
                  temp_len = stream->stream_config.cached_pps_len;
                  temp_src =stream->stream_config.cached_pps;
                }
                forbidden_zero_bit |= b_unpackb(temp_src,0,1);
                if(nal_ref_idc < (b_unpackb(temp_src,1,2)))
                  nal_ref_idc = b_unpackb(temp_src,1,2);
                /*------------------------------------------------------------------------
                pack the 16 bit len , length is in words
                ------------------------------------------------------------------------*/
                b_packw( temp_len, nw_buf->data + offset, 0, 16 );
                offset += 2;

                qvp_rtp_memscpy( nw_buf->data + offset, temp_len,temp_src,
                  temp_len);
                offset += temp_len;


              }
              b_packb(forbidden_zero_bit,nw_buf->data+QVP_RTP_HEAD_ROOM,0,1); /*pack F bit*/
              b_packb(nal_ref_idc,nw_buf->data+QVP_RTP_HEAD_ROOM,1,2); /*pack NRI*/
              b_packb(H264_NALUNIT_TYPE_24,nw_buf->data+QVP_RTP_HEAD_ROOM,3,5); /*Pack STAP-A type - 24*/
              /*----------------------------------------------------------------------
              Set up room for RTP headers
              ----------------------------------------------------------------------*/
              nw_buf->head_room = QVP_RTP_HEAD_ROOM;  
              nw_buf->need_tofree = TRUE;
              nw_buf->len = stream->stream_config.cached_sps_len + stream->stream_config.cached_pps_len + 5;
              /*----------------------------------------------------------------------
              set up other RTP fields
              ----------------------------------------------------------------------*/
              nw_buf->tstamp  = pkt->tstamp;
              nw_buf->rtp_pyld_type = pkt->rtp_pyld_type;
              h264_profile_config.tx_cb( nw_buf, usr_hdl );
              QVP_RTP_MSG_MED( " Sending H264 bundle len = %d, marker = %d, \
                                timestamp = %d", nw_buf->len, nw_buf->marker_bit, 
                                nw_buf->tstamp );
              nw_buf = NULL;
              forbidden_zero_bit = 0;
              nal_ref_idc = 0;
            }
            stream->stream_config.ps_cnt++;
            if(stream->stream_config.ps_cnt < 6/*PS_cnt_threshold*/)
            {
              int32 temp = power(2,stream->stream_config.ps_cnt-1);
              temp *= 500*90;/* clock rate */
              stream->stream_config.PS_timer = pkt->tstamp;
              stream->stream_config.PS_timer += temp;
            }
            else
            {
              stream->stream_config.reliable_txmit_timer = FALSE;
              stream->stream_config.ps_cnt = 0;
              QVP_RTP_MSG_LOW_0("qvp_rtp_h264_profile_send.. retransmission timer is off");
            }
          }
        }
        if(!nw_buf)
        {
          nw_buf  = qvp_rtp_alloc_buf_by_len( g_rtp_mtu_size + QVP_RTP_HEAD_ROOM );

          if( !nw_buf )
          {
            QVP_RTP_ERR(" Failed allocate a buffer arrggggggg....", 0, 0, 0 );
            //free the source rtp buf
            if(pkt->need_tofree)
            {
              qvp_rtp_free_buf(pkt);
            }
            qvp_rtp_h264_reset_tx_ctx( stream );
            return( QVP_RTP_ERR_FATAL );
          }
          memset(nw_buf->data,0,g_rtp_mtu_size + QVP_RTP_HEAD_ROOM);
          /*----------------------------------------------------------------------
          Set up room for RTP headers
          ----------------------------------------------------------------------*/
          nw_buf->head_room = QVP_RTP_HEAD_ROOM;      

          nw_buf->need_tofree = TRUE;
        }

        pkt_assembled = TRUE;
        forbidden_zero_bit |= b_unpackb(src,0,1);
        if(nal_ref_idc < (b_unpackb(src,1,2)))
          nal_ref_idc = b_unpackb(src,1,2);
        if(!first_nalu)
        {
          first_nalu = TRUE;
          first_nalu_temp = src;
          first_nalu_size = iCurrentNALUSize;
          //first_nalu_start = nal_start;
        }
        else
        {
          if(first_nalu_temp)
          {
            /* we are adding STAP-A header so we can increment offset readily */
            nw_buf_offset ++;
            /*------------------------------------------------------------------------
            pack the 16 bit len , length is in words
            ------------------------------------------------------------------------*/
            b_packw( first_nalu_size, nw_buf->data + nw_buf_offset, 0, 16 );

            nw_buf_offset +=2;
            /*----------------------------------------------------------------------
            Copy the payload data
            ----------------------------------------------------------------------*/
            qvp_rtp_memscpy( nw_buf->data + nw_buf_offset,first_nalu_size, first_nalu_temp,
              first_nalu_size);
            nw_buf_offset += first_nalu_size;
            first_nalu_temp = NULL;
          }
          /*------------------------------------------------------------------------
          pack the 16 bit len , length is in words
          ------------------------------------------------------------------------*/
          b_packw( iCurrentNALUSize, nw_buf->data + nw_buf_offset, 0, 16 );

          nw_buf_offset +=2;
          /*----------------------------------------------------------------------
          Copy the payload data
          ----------------------------------------------------------------------*/
          qvp_rtp_memscpy( nw_buf->data + nw_buf_offset,iCurrentNALUSize, src,
            iCurrentNALUSize);
          nw_buf_offset += iCurrentNALUSize;

        }
        iRTPPayloadSize = iRTPPayloadSize + iCurrentNALUSize;
        iNumOfNalus++;
        if(len == 0)
        {
          if(first_nalu_temp)
          {
            /*----------------------------------------------------------------------
            Copy the payload data
            ----------------------------------------------------------------------*/
            qvp_rtp_memscpy( nw_buf->data + nw_buf->head_room, first_nalu_size,first_nalu_temp,
              first_nalu_size);
            nw_buf->len = first_nalu_size;
            QVP_RTP_MSG_LOW_0( "sending h264 frame as a single nalu.. only one nalu in this AU");

          }
          else
          {
            /*----------------------------------------------------------------------
            set up STAP-A NAL HDR fields
            ----------------------------------------------------------------------*/
            b_packb(forbidden_zero_bit,nw_buf->data+QVP_RTP_HEAD_ROOM,0,1); /*pack F bit*/
            b_packb(nal_ref_idc,nw_buf->data+QVP_RTP_HEAD_ROOM,1,2); /*pack NRI*/
            b_packb(H264_NALUNIT_TYPE_24,nw_buf->data+QVP_RTP_HEAD_ROOM,3,5); /*Pack STAP-A type - 24*/
            nw_buf->len = nw_buf_offset - QVP_RTP_HEAD_ROOM;
            QVP_RTP_MSG_LOW_0( "sending h264 frame as a STAP-A.....");

          }
          /*----------------------------------------------------------------------
          set up other RTP fields
          ----------------------------------------------------------------------*/
          nw_buf->marker_bit = 1;
          nw_buf->tstamp  = pkt->tstamp;
          nw_buf->rtp_pyld_type = pkt->rtp_pyld_type;
          nw_buf->seq = SN++;

          /*----------------------------------------------------------------------*/      
          h264_profile_config.tx_cb( nw_buf, usr_hdl );
          QVP_RTP_MSG_MED( " Sending H264 bundle len = %d, timestamp = %d, \
                            marker = %d", nw_buf->len, nw_buf->tstamp, 
                            nw_buf->marker_bit );

          //free the source rtp buf
          if(pkt->need_tofree)
          {
            qvp_rtp_free_buf(pkt);
          }
          //TBD to be removed?
          //iRTPPayloadSize = 0;
          //pkt_assembled = FALSE;
          //nw_buf_offset = 0;
          //forbidden_zero_bit = 0;
          //nal_ref_idc = 0;
          //first_nalu = FALSE;
          //first_nalu_temp = NULL;
          //first_nalu_size = 0;
          //first_nalu_start = 0;
          //nw_buf = NULL;
        }
        src += iCurrentNALUSize;
      }
      else//CurrentNALU  does not fit in the current RTP payload
      {
        //TBD, need to handle assembled and not sent handle packet send 
        if(pkt_assembled)
        {
          if(first_nalu_temp)//its the first nalu we should send
          {
            /*----------------------------------------------------------------------
            Copy the payload data
            ----------------------------------------------------------------------*/
            qvp_rtp_memscpy( nw_buf->data + nw_buf->head_room,first_nalu_size, first_nalu_temp,
              first_nalu_size);
            nw_buf->len = first_nalu_size;
            QVP_RTP_MSG_LOW_0( "sending h264 frame as a single nalu.. only one nalu in this AU");

          }
          else
          {
            /*----------------------------------------------------------------------
            set up STAP-A NAL HDR fields
            ----------------------------------------------------------------------*/
            b_packb(forbidden_zero_bit,nw_buf->data + QVP_RTP_HEAD_ROOM,0,1); /*pack F bit*/
            b_packb(nal_ref_idc,nw_buf->data + QVP_RTP_HEAD_ROOM,1,2); /*pack NRI*/
            b_packb(H264_NALUNIT_TYPE_24,nw_buf->data + QVP_RTP_HEAD_ROOM,3,5); /*Pack STAP-A type - 24*/
            nw_buf->len = nw_buf_offset - QVP_RTP_HEAD_ROOM;
            QVP_RTP_MSG_LOW_0( "sending h264 frame as a STAP-A.....");

          }
          /*----------------------------------------------------------------------
          set up other RTP fields
          ----------------------------------------------------------------------*/
          nw_buf->marker_bit = 0;
          nw_buf->tstamp  = pkt->tstamp;
          nw_buf->rtp_pyld_type = pkt->rtp_pyld_type;
          nw_buf->seq = SN++;
          iRTPPayloadSize = 0;
          iNumOfNalus = 0;
          /*----------------------------------------------------------------------*/      
          h264_profile_config.tx_cb( nw_buf, usr_hdl );     
          QVP_RTP_MSG_MED( " Sending H264 bundle len = %d, timestamp = %d, \
                            marker = %d", nw_buf->len, nw_buf->tstamp, 
                            nw_buf->marker_bit );
          pkt_assembled = FALSE;
          nw_buf_offset = 0;
          forbidden_zero_bit = 0;
          nal_ref_idc = 0;
          first_nalu = FALSE;
          first_nalu_temp = NULL;
          first_nalu_size = 0;
          //first_nalu_start = 0;
          nw_buf = NULL;
        }

        //handle FU-A
        if(iCurrentNALUSize > g_rtp_mtu_size)
        {

          /* From RFC 6184 FU-A Layout */
          /*---------------------------------------------------------------

          0                   1                   2                   3
          0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
          | FU indicator  |   FU header   |                               |
          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                               |
          |                                                               |
          |                         FU payload                            |
          |                                                               |
          |                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
          |                               :...OPTIONAL RTP padding        |
          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

          ----------------------------------------------------------------*/

          qvp_rtp_buf_type *frag_nw_buf       = NULL;
          uint16 offset = 0;
          uint16 size = iCurrentNALUSize;
          uint8 type;
          uint8 nri;
          boolean first_fragment = TRUE;

          src += nal_start;//jump to nal start
          len -= nal_start;
          len -= iCurrentNALUSize;

          type = b_unpackb(src,3,5);
          nri = b_unpackb(src,1,2);

          offset += 1;
          size -= 1;
          while (size + 2 > g_rtp_mtu_size) {

            frag_nw_buf  = qvp_rtp_alloc_buf_by_len( g_rtp_mtu_size + QVP_RTP_HEAD_ROOM );

            if( !frag_nw_buf )
            {
              QVP_RTP_ERR(" Failed allocate a buffer arrggggggg....", 0, 0, 0 );
              if(pkt->need_tofree)
              {
                qvp_rtp_free_buf(pkt);
              }
              qvp_rtp_h264_reset_tx_ctx( stream );
              return( QVP_RTP_ERR_FATAL );
            }
            memset(frag_nw_buf->data,0,g_rtp_mtu_size + QVP_RTP_HEAD_ROOM);
            /*----------------------------------------------------------------------
            Set up room for RTP headers
            ----------------------------------------------------------------------*/
            frag_nw_buf->head_room = QVP_RTP_HEAD_ROOM;      

            b_packb(nri,frag_nw_buf->data + QVP_RTP_HEAD_ROOM,1,2); /*pack NRI*/
            b_packb(H264_NALUNIT_TYPE_28,frag_nw_buf->data + QVP_RTP_HEAD_ROOM,3,5); /* FU Indicator; Type = 28 ---> FU-A */
            b_packb(type,frag_nw_buf->data + QVP_RTP_HEAD_ROOM+1,3,5); //mark nalu type
            if(first_fragment)
            {
              first_fragment = FALSE;
              b_packb(1,frag_nw_buf->data + QVP_RTP_HEAD_ROOM+1,0,1); //mark start bit
            }
            qvp_rtp_memscpy(frag_nw_buf->data+QVP_RTP_HEAD_ROOM+2,g_rtp_mtu_size - 2, src+offset, g_rtp_mtu_size - 2);
            frag_nw_buf->seq = SN++;
            frag_nw_buf->len = g_rtp_mtu_size;
            frag_nw_buf->marker_bit = 0;
            frag_nw_buf->tstamp  = pkt->tstamp;
            frag_nw_buf->rtp_pyld_type = pkt->rtp_pyld_type;
            h264_profile_config.tx_cb( frag_nw_buf, usr_hdl );
            QVP_RTP_MSG_MED( " Sending H264 as FU-A len = %d, timestamp = %d, \
                              marker = %d", frag_nw_buf->len, frag_nw_buf->tstamp, 
                              frag_nw_buf->marker_bit );
            offset += g_rtp_mtu_size - 2;
            size -= g_rtp_mtu_size - 2;

          }
          frag_nw_buf  = qvp_rtp_alloc_buf_by_len( g_rtp_mtu_size + QVP_RTP_HEAD_ROOM );

          if( !frag_nw_buf )
          {
            QVP_RTP_ERR(" Failed allocate a buffer arrggggggg....", 0, 0, 0 );
            if(pkt->need_tofree)
            {
              qvp_rtp_free_buf(pkt);
            }
            qvp_rtp_h264_reset_tx_ctx( stream );
            return( QVP_RTP_ERR_FATAL );
          }
          memset(frag_nw_buf->data,0,g_rtp_mtu_size + QVP_RTP_HEAD_ROOM);
          /*----------------------------------------------------------------------
          Set up room for RTP headers
          ----------------------------------------------------------------------*/
          frag_nw_buf->head_room = QVP_RTP_HEAD_ROOM;      

          b_packb(nri,frag_nw_buf->data + QVP_RTP_HEAD_ROOM,1,2); /*pack NRI*/
          b_packb(H264_NALUNIT_TYPE_28,frag_nw_buf->data + QVP_RTP_HEAD_ROOM,3,5); /* FU Indicator; Type = 28 ---> FU-A */
          b_packb(type,frag_nw_buf->data + QVP_RTP_HEAD_ROOM+1,3,5); //mark nalu type
          b_packb(1,frag_nw_buf->data + QVP_RTP_HEAD_ROOM+1,1,1);//mark end bit
          qvp_rtp_memscpy(frag_nw_buf->data + QVP_RTP_HEAD_ROOM+2,size, src+offset, size);
          frag_nw_buf->seq = SN++;
          frag_nw_buf->len = size+2;
          if(len == 0)
          {
            frag_nw_buf->marker_bit = 1;
            //free the source rtp buf
            if(pkt->need_tofree)
            {
              qvp_rtp_free_buf(pkt);
            }
          }
          else
            frag_nw_buf->marker_bit = 0;
          frag_nw_buf->tstamp  = pkt->tstamp;
          frag_nw_buf->rtp_pyld_type = pkt->rtp_pyld_type;
          frag_nw_buf->need_tofree = TRUE;
          h264_profile_config.tx_cb( frag_nw_buf, usr_hdl );
          QVP_RTP_MSG_MED( " Sending H264 as FU-A len = %d, timestamp = %d, \
                            marker = %d", frag_nw_buf->len, frag_nw_buf->tstamp, 
                            frag_nw_buf->marker_bit );
          iRTPPayloadSize = 0;
          iNumOfNalus = 0;
          src += iCurrentNALUSize;
        }
      }
    }while(len > 2);//discuss with shankar about this check

  }
  else
  {
    QVP_RTP_MSG_HIGH_1( " unsupported h264 packetization mode = %d", stream->stream_config.packetization_mode);

    //free the source rtp buf
    if(pkt->need_tofree)
    {
      qvp_rtp_free_buf(pkt);
    }
  }

  //TODO Fragmentation support
#if ZERO  
  iDataLen = pkt->len;

  g_bIsFragment = FALSE;

  while (iSentByte < iDataLen)
  {
    iSentByte = qvp_rtp_h264_fragment_and_send(iDataLen,iSentByte,pkt,stream->first_frame);
  }
#endif  

  return( QVP_RTP_SUCCESS );
} /* end of function qvp_rtp_h264_profile_send */

/*===========================================================================

FUNCTION  QVP_RTP_H264_PROFILE_FRAGMENT_AND_SEND


DESCRIPTION
  request to send specified buffer through this profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl     - handle into the profile. 
  usr_hdl - handle to use when we call send function using the registered
            tx callback
  pkt     - packet of data to be sent.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
/* Fill up the payload data with the header and buffer pointers */
uint16 qvp_rtp_h264_fragment_and_send( uint16 iDataLen, uint16 iSentByte, qvp_rtp_buf_type *pkt,boolean firstbit)
{
  uint16   iDataLenToSend          = 0;
  uint8    iFUIndicator            = 0;
  uint8    iFUHeader               = 0;  
  uint8    iNalUnitType            = 0;
  uint8    iNRIVal                 = 0; 
  uint8*   pPayloadBuffer          = pkt->data + pkt->head_room;
  
  
  if(!iDataLen || !pPayloadBuffer)
  {
    QVP_RTP_ERR( "There is nothing to be sent ...ignoring  ", 
      0, 0, 0 );
    return FALSE;
  }
  
  if(iSentByte >= iDataLen)
  {
    QVP_RTP_ERR( "TiSentByte cannot be greater than total data len  ", 
      0, 0, 0 );
    return FALSE;
  }
  
  QVP_RTP_MSG_MED_2( " iDataLen = %d, SentByte = %d\r\n", iDataLen, iSentByte ); 
  
  iDataLenToSend = (uint16) (iDataLen - iSentByte);
  /**********************************************************/
  
  iNalUnitType            = pPayloadBuffer[H264_NAL_UNIT_OFFSET];
  iNRIVal                 = iNalUnitType & H264_MASK_NRI_VAL;
  
  iNalUnitType &= H264_MASK_PAYLOAD;
  if(iNalUnitType >= H264_NALUNIT_TYPE_24 && iNalUnitType <= H264_NALUNIT_TYPE_27)
  {
    if(iDataLenToSend > MAX_PAYLOAD_BUFFER_SIZE_H264FRAG)
    {
      QVP_RTP_ERR( "Aggregation packets with size < 1200 is not yet supported", 0, 0, 0 );      
      return FALSE;
    }
    else
    {
      QVP_RTP_MSG_MED_1( "  Aggregation packet type received and sending the packet as it is without fragmenting& packet type = %x", iNalUnitType);
      //psPayloadData->iBufLen = iDataLenToSend;
      //psPayloadData->MarkerBit = n_bMarkerBit;;
    }
  }
  else if(iNalUnitType >= 1 && iNalUnitType <= H264_NALUNIT_TYPE_23)
  {
    //initialize FU
    iFUIndicator = 0;
    iFUHeader = 0;
    if (iDataLenToSend >MAX_PAYLOAD_BUFFER_SIZE_H264FRAG)
    {
      iFUIndicator |= H264_FU_INDICATOR;
      iFUIndicator |= iNRIVal;
      if(firstbit == FALSE)
      {
        iFUHeader |= H264_SET_START_BIT;
        firstbit = TRUE;
      }
      else
      {
        iFUHeader |= H264_SET_SUBSEQ_BIT;
      }
      iFUHeader |= iNalUnitType;
      g_bIsFragment = FALSE;
      //psPayloadData->iBufLen = MAX_PAYLOAD_BUFFER_SIZE_MP4FRAG;
      //psPayloadData->MarkerBit = 0;
    }
    else
    {
      if(g_bIsFragment == TRUE)
      {
        iFUIndicator |= H264_FU_INDICATOR;
        iFUIndicator |= iNRIVal;
        iFUHeader |= H264_SET_END_BIT;
        iFUHeader |= iNalUnitType;
      }
      //psPayloadData->iBufLen = iDataLenToSend;
      //psPayloadData->MarkerBit = n_bMarkerBit;
       g_bIsFragment = FALSE;
    }
  }
  else
  {
    QVP_RTP_MSG_MED_0( " Nal unit type has to be ignored since it odes not belong to either single nal unit or non- interleaved");
    return FALSE;
  }
  //psPayloadData->pPayloadBuffer = pPayloadBuffer + iSentByte;
  /**********************************************************/
  
  //qpDplMemset(psPayloadData->pPayloadHeader, 0, H264_FU_HDR_SIZE);
  
  
  if (iFUIndicator != 0 &&  iFUHeader != 0)
  {
    //psPayloadData->pPayloadHeader[0] |= iFUIndicator;
    //psPayloadData->pPayloadHeader[1] |= iFUHeader;
    //psPayloadData->iHeaderLen = H264_FU_HDR_SIZE;
  }
  else
  {
    //psPayloadData->pPayloadHeader = QP_NULL;
    //psPayloadData->iHeaderLen = 0;
  }
  
  
  QVP_RTP_MSG_MED_1( " Fragmenting h264 frame of len %d", 0);
  return TRUE;
  
}

/*===========================================================================

FUNCTION  QVP_RTP_H264_PROFILE_OPEN 


DESCRIPTION
    Requests to open a bi directional channel within the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  usr_hdl       - handle to use when we call send function using the 
                  registered tx callback

ARGUMENTS OUT
  hdl           - on success we write the handle to the profiile into 
                  the  passed double pointer
  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl      /* user handle for any cb */
)
{
  uint32 i;   /* index variable */
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Bail out if we are not initialized yet
  ------------------------------------------------------------------------*/
  if( !h264_initialized )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
              look at an idle stream 
  ------------------------------------------------------------------------*/
  for( i = 0;  i < h264_profile_config.num_streams; i++ )
  {
    
    
    /*----------------------------------------------------------------------
              Fish for free entries
    ----------------------------------------------------------------------*/
    if( !qvp_rtp_h264_array[ i ].valid )
    {
      qvp_rtp_h264_array[ i ].valid = TRUE;
      *hdl = &qvp_rtp_h264_array[ i ]; 
      
      /*--------------------------------------------------------------------
            copy the H264 configuration to the  stream_ctx 
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( &qvp_rtp_h264_array[i].stream_config,sizeof(qvp_rtp_h264_config_type),
        &h264_stream_config,sizeof( qvp_rtp_h264_array[i].stream_config ));

      /*--------------------------------------------------------------------
          Reset the stream context - if any stale values in there.
      --------------------------------------------------------------------*/
      qvp_rtp_h264_reset_stream( &qvp_rtp_h264_array[i] );
      
      /*--------------------------------------------------------------------
        Initialize reassembly context for this stream ( FRAME mode )
        ( always open it since we dont know if jitter size at this stage )
      --------------------------------------------------------------------*/
      qvp_rtp_h264_array[i].rx_ctx.reassem_hdl = NULL;      
      
      return( QVP_RTP_SUCCESS );

    }
    
  } /* maximum no of streams */

  return( QVP_RTP_NORESOURCES );


} /* end of function qvp_rtp_h264_profile_open */


/*==================================================================

FUNCTION QVP_RTP_H264_PROFILE_VALIDATE_CONFIG 


DESCRIPTION

  This function checks the configuration of H264 specified in the input 
  against the valid fmtp values for H264

DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute  is valid, counter_config is not modified
                   If the attribute  is invalid,
                   counter_config is updated with proposed value

RETURN VALUE
  QVP_RTP_SUCCESS  - The given Configuration is  valid for the given payload,
                     RTP configure will succeed for these settings
                     
  QVP_RTP_ERR_FATAL   - The given Configuration is not valid for the given 
                        payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_validate_config_rx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  /*----------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if ( ( !payld_config ) || ( !counter_config ) )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Validating rx configuration
   -----------------------------------------------------------------------*/
  
  
  //TODO

  
  return ( status ); 


}/* end of qvp_rtp_h264_profile_validate_config_rx */


/*===========================================================================

FUNCTION QVP_RTP_H264_PROFILE_VALIDATE_CONFIG_TX 


DESCRIPTION

  This function checks the configuration of H264 specified in the input 
  against the valid fmtp values for H264

DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute  is valid, counter_config is not modified
                   If the attribute  is invalid, 
                   counter_config is updated with proposed value


RETURN VALUE
  QVP_RTP_SUCCESS  - The given Configuration is  valid for the given payload,
                     RTP configure will succeed for these settings
  QVP_RTP_ERR_FATAL   - The given Configuration is not valid for the given 
                        payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_validate_config_tx
(
  qvp_rtp_payload_config_type*  payld_config,
  qvp_rtp_payload_config_type*  counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {
    return( QVP_RTP_ERR_FATAL );
  }
   
  //TODO
  
  return ( status ); 

}/* end of qvp_rtp_h264_profile_validate_config_tx */


/*===========================================================================

FUNCTION QVP_RTP_H264_PROFILE_READ_DEFLT_CONFIG 


DESCRIPTION

  This function reads out the default configuration of H264
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is not used by H264. Some 
                     other profiles uses it as they support multiple 
                     payload formats.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_read_deflt_config
(
  
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
)
{

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  
  return( qvp_rtp_h264_profile_copy_config( payld_config, 
                                            &h264_stream_config  ) );

  
} /* end of function qvp_rtp_h264_profile_read_deflt_config */ 

/*===========================================================================

FUNCTION QVP_RTP_H264_PROFILE_CONFIGURE

DESCRIPTION

  This function payload configuration of a previously opened H264  
  channel.

DEPENDENCIES
  None

ARGUMENTS IN
    hdl            - handle to the opend channel
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
)
{
  qvp_rtp_h264_ctx_type *stream = ( qvp_rtp_h264_ctx_type *) hdl; 
  qvp_rtp_h264_config_type prev_config;
  qvp_rtp_status_type      status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/


  
  /*------------------------------------------------------------------------
                Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !h264_initialized || !stream || !stream->valid || 
      !payld_config ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Backup the current configuration before we proceed from here.
  ------------------------------------------------------------------------*/
  prev_config = stream->stream_config;
  
  /*------------------------------------------------------------------------
      Configuration steps.

      1) Try and configure RX.
         If RX fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model as 
          given in RFC3264.
      2) Try and configure TX
         If TX  configure fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model 
          as given in RFC3264.
  ------------------------------------------------------------------------*/

  if( payld_config->config_rx_params.valid )
  {
    status = qvp_rtp_h264_profile_config_rx_param( stream, payld_config );
  }


  /*------------------------------------------------------------------------
      Try and configure Tx part now....
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS )&&
      payld_config->config_tx_params.valid )
  {
    status = qvp_rtp_h264_profile_config_tx_param(stream, payld_config );
    
  }

  /*------------------------------------------------------------------------
    If we did not succeed to a struct to reset to preivous value and 
    return failure
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS )
  {
    stream->stream_config = prev_config ;
  }
  
  return( status );

} /* end of function qvp_rtp_h264_profile_configure */

/*===========================================================================

FUNCTION QVP_RTP_H264_PROFILE_CONFIG_RX_PARAM

DESCRIPTION

  This function payload will configuration of Rx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - H264 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_config_rx_param
(
  
  qvp_rtp_h264_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
)
{
  if( !payld_config || !stream ) 
  {
    QVP_RTP_ERR ( " Invalid params", 0, 0, 0);
    return( QVP_RTP_ERR_FATAL );
  }

  //TODO

  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_h264_profile_config_rx_param */ 

/*===========================================================================

FUNCTION QVP_RTP_H264_PROFILE_CONFIG_TX_PARAM

DESCRIPTION

  This function payload will configuration of Tx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - H264 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_config_tx_param
(
  
  qvp_rtp_h264_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
)
{
  if( !payld_config || !stream ) 
  {
    QVP_RTP_ERR ( " Invalid params", 0, 0, 0);
    return( QVP_RTP_ERR_FATAL );
  }

  stream->stream_config.reliable_txmit = payld_config->config_tx_params.config_tx_payld_params.h264_tx_config.reliable_txmit;
  stream->stream_config.packetization_mode = (uint8)payld_config->config_tx_params.config_tx_payld_params.h264_tx_config.packetization_mode;
  return( QVP_RTP_SUCCESS );
        
} /* end of function qvp_rtp_h264_profile_config_tx_param */
  
/*===========================================================================

FUNCTION  QVP_RTP_H264_PROFILE_RECV 


DESCRIPTION
   The function which the RTP framework will call upon arrival of data from 
   NW. H264 header is stripped but parameters are passed.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl           - hdl into the profile.
  usr_hdl       - handle to use when we call send function using the 
                  registered rx callback
  pkt           - actual data packet to be parsed.

  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed*/

)
{
  qvp_rtp_h264_ctx_type *stream = ( qvp_rtp_h264_ctx_type *) hdl; 

  /*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
                Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !h264_initialized || !stream || !stream->valid || 
      !h264_profile_config.rx_cb  ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  //TODO


  /*----------------------------------------------------------------------
              pass the packet up
  ----------------------------------------------------------------------*/
  
  return( h264_profile_config.rx_cb( pkt, usr_hdl ) ); 
  
  
} /* end of function qvp_rtp_h264_profile_recv */

/*===========================================================================

FUNCTION  QVP_RTP_H264_PROFILE_CLOSE


DESCRIPTION
  Closes an already open bi directional channel inside the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl - handle of channel to close.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

)
{
  qvp_rtp_h264_ctx_type *stream = ( qvp_rtp_h264_ctx_type *) hdl; 
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
              If we get  a valid handle invalidate the index 

              Also check are we initialized yet ..?
  ------------------------------------------------------------------------*/
  if( hdl && h264_initialized )
  {
    
    /*----------------------------------------------------------------------
        We cannot afford to loose buffers so when we are in the middle of
        reassmbling we need to free the whole chain
    ----------------------------------------------------------------------*/
    stream->valid = FALSE;
    /*----------------------------------------------------------------------
        We cannot afford to loose buffers so when we are in the middle of
        reassmbling we need to free the whole chain
    ----------------------------------------------------------------------*/
    qvp_rtp_close_reassem_ctx( stream->rx_ctx.reassem_hdl );
    
    
    
    /*----------------------------------------------------------------------
        Reset the stream context - if any stale values in there.
    ----------------------------------------------------------------------*/
    qvp_rtp_h264_reset_stream( stream );
    
    return( QVP_RTP_SUCCESS );
  }
  else
  {
    return( QVP_RTP_ERR_FATAL );
  }


} /* end of function qvp_rtp_h264_profile_close */ 

/*===========================================================================

FUNCTION  QVP_RTP_H264_RESET_TX_CTX


DESCRIPTION
  Reset the trasmitter context inside the stream

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  None


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_h264_reset_tx_ctx
( 
  qvp_rtp_h264_ctx_type *stream 
)
{
/*------------------------------------------------------------------------*/

 //TODO
  
} /* end of function qvp_rtp_h264_reset_tx_ctx */
    
/*===========================================================================

FUNCTION  QVP_RTP_H264_RESET_STREAM 


DESCRIPTION
  Reset the context inside the stream

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_h264_reset_stream
( 
  qvp_rtp_h264_ctx_type *stream 
)
{


/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
          Reset the transmit side
  ------------------------------------------------------------------------*/

  qvp_rtp_h264_reset_tx_ctx( stream );

  /*------------------------------------------------------------------------
          Reset the receiving  side
  ------------------------------------------------------------------------*/
  //TODO
  
   /*------------------------------------------------------------------------
          Reset the first frame flag
  ------------------------------------------------------------------------*/ 
   stream->first_frame = FALSE;
  
} /* end of function qvp_rtp_h264_reset_stream */

/*===========================================================================

FUNCTION QVP_RTP_H264_PROFILE_COPY_CONFIG 


DESCRIPTION

  This function reads out the default configuration of H264
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is not used by H264. Some 
                     other profiles uses it as they support multiple 
                     payload formats.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h264_profile_copy_config
(
  qvp_rtp_payload_config_type *payld_config,
  qvp_rtp_h264_config_type    *h264_config
)
{
  qvp_rtp_h264_sdp_config_type  *h264_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.h264_tx_config;
  qvp_rtp_h264_sdp_config_type  *h264_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.h264_rx_config;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Memset the whole configuration before we proceed
  ------------------------------------------------------------------------*/
  memset( h264_rx_config, 0, 
          sizeof( qvp_rtp_h264_sdp_config_type ) ); 
  memset( h264_tx_config, 0, 
          sizeof( qvp_rtp_h264_sdp_config_type ) );
  
  /*------------------------------------------------------------------------
      Copy each and every FMTP and flag them as TRUE
  ------------------------------------------------------------------------*/
  
  //TODO
  
  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_h264_profile_copy_config */ 

/*===========================================================================

FUNCTION  QVP_RTP_H264_PROFILE_SHUTDOWN


DESCRIPTION
  Shuts this module down and flag intialization as false

DEPENDENCIES
  None

ARGUMENTS IN
  len - length of the parload

RETURN VALUE
  toc type


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_h264_profile_shutdown( void )
{
  uint32 i;
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Lets not do multiple shutdown
  ------------------------------------------------------------------------*/
  if( !h264_initialized )
  {
    return;
  }
  
  /*------------------------------------------------------------------------
      Walk through the stream array and close all channels
  ------------------------------------------------------------------------*/
  for( i = 0; i < h264_profile_config.num_streams; i ++ )
  {

    
    /*----------------------------------------------------------------------
        If this stream is open close it now
    ----------------------------------------------------------------------*/
    if ( qvp_rtp_h264_array[ i ].valid )
    {
      qvp_rtp_h264_profile_close( ( qvp_rtp_profile_hdl_type ) i );
    }
    
  } /* end of for i = 0  */
  
  /*------------------------------------------------------------------------
    free the array of streams
  ------------------------------------------------------------------------*/
  if(qvp_rtp_h264_array)
    qvp_rtp_free( qvp_rtp_h264_array);

  /*------------------------------------------------------------------------
    Flag array as NULL and init as FALSE
  ------------------------------------------------------------------------*/
  qvp_rtp_h264_array = NULL; 
  h264_initialized  = FALSE;
  
  
} /* end of function qvp_rtp_h264_profile_shutdown  */

/*===========================================================================

FUNCTION  QVP_RTP_FIND_NAL_UNIT


DESCRIPTION
  Find the beginning and end of a NAL (Network Abstraction Layer) unit in a byte buffer containing H264 bitstream data.

DEPENDENCIES
  None

ARGUMENTS IN
  buf        the buffer
  size       the size of the buffer

ARGUMENTS OUT
 nal_start  the beginning offset of the nal
 nal_end    the end offset of the nal

RETURN VALUE
  the length of the nal, or 0 if did not find start of nal, or -1 if did not find end of nal


SIDE EFFECTS
  None.

===========================================================================*/

LOCAL int32 qvp_rtp_find_nal_unit(uint8* buf, int32 buf_len, int32* nal_start, int32* nal_end)
{
  int32 nal_offset;
  // find start
  *nal_start = 0;
  *nal_end = 0;

  nal_offset = 0;
  while (   //( next_bits( 24 ) != 0x000001 && next_bits( 32 ) != 0x00000001 )
    (buf[nal_offset] != 0 || buf[nal_offset+1] != 0 || buf[nal_offset+2] != 0x01) && 
    (buf[nal_offset] != 0 || buf[nal_offset+1] != 0 || buf[nal_offset+2] != 0 || buf[nal_offset+3] != 0x01) 
    )
  {
    nal_offset++; // skip leading zero
    if (nal_offset+4 >= buf_len) { 
      return 0; 
    } // did not find nal start
  }

  if  (buf[nal_offset] != 0 || buf[nal_offset+1] != 0 || buf[nal_offset+2] != 0x01) // ( next_bits( 24 ) != 0x000001 )
  {
    nal_offset++;
  }

  nal_offset+= 3;
  *nal_start = nal_offset;

  while (   //( next_bits( 24 ) != 0x000000 && next_bits( 24 ) != 0x000001 )
    (buf[nal_offset] != 0 || buf[nal_offset+1] != 0 || buf[nal_offset+2] != 0) && 
    (buf[nal_offset] != 0 || buf[nal_offset+1] != 0 || buf[nal_offset+2] != 0x01) 
    )
  {
    nal_offset++;
    if (nal_offset+3 >= buf_len) { 
      *nal_end = buf_len; 
      return (*nal_end - *nal_start);
    } // did not find nal end, Access unit ended first
  }

  *nal_end = nal_offset;
  return (*nal_end - *nal_start);
}

LOCAL int32 power(int32 x, uint32 y)
{
    if( y == 0)
        return 1;
    else if (y%2 == 0)
        return power(x, y/2)*power(x, y/2);
    else
        return x*power(x, y/2)*power(x, y/2);

}

#endif /* end of FEATURE_QVPHONE_RTP */
