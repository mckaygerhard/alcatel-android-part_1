/*=*====*====*====*====*====*====*===*====*====*====*====*====*====*====*====*


                         QVP_RTP_G723_PROFILE . C

GENERAL DESCRIPTION

  This file contains the implementation of G723 profile. G723 profile acts 
  as conduite inside RTP layer. The RFC which is based on is RFC3551.

EXTERNALIZED FUNCTIONS
  None.


INITIALIZATION AND SEQUENCING REQUIREMENTS

  Need to init and configure the profile before it becomes usable.


  Copyright (c) 3004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/ps_media/rtp_profile/src/qvp_rtp_g723_profile.c#1 $ $DateTime: 2016/03/28 23:03:22 $ $Author: mplcsds1 $

                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
26/06/09    sha    Changed the validate function to check pyld and Chdir only when valid
08/01/08    grk    Updated frm_present flag for received frames.
12/31/07    grk    Moving maxptime/ptime to media level.
10/18/07    rk     QDJ integration. Marking silence frames
06/09/06    apr    RTP payload type number is copied into the network
                   buffer, as part of fix for CR 95499
05/25/06    xz     Added an EOL at the end of file to remove a 7k warning
05/05/06    apr    Including RTP code in feature FEATURE_QVPHONE_RTP
03/20/06    uma    Replaced all malloc and free with wrappers 
                   qvp_rtp_malloc & qvp_rtp_free 
09/09/05    srk    Started adjusting our bundle size with the maxptime in 
                   config. Error check on maxptime config. Adding buf 
                   abstraction on send side all the way.
08/23/05    srk    Adding frame check in the tx side.
07/18/05    srk    Initial Creation.
===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP


#ifdef  RTP_FILE_NUMBER 
  #undef RTP_FILE_NUMBER 
#endif 
#define RTP_FILE_NUMBER 28
/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <comdef.h>               /* common target/c-sim defines */
#include <string.h>               /* memory routines */
#include <stdlib.h>               /* for malloc */
#include "qvp_rtp_api.h"          /* return type propagation */
#include "qvp_rtp_profile.h"      /* profile template data type */
#include "qvp_rtp_msg.h"
#include "qvp_rtp_log.h"
#include "qvp_rtp_g723_profile.h" /* profile template data type */
#include <bit.h>                  /* for bit parsing/manipulation */


/*===========================================================================
                     LOCAL STATIC FUNCTIONS 
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_init
(
  qvp_rtp_profile_config_type *config
);

LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl     /* user handle for any cb */
);

LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_read_deflt_config
(
  
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_validate_config_rx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_validate_config_tx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
);

LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * header 
                                              */

);

LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

);

LOCAL void qvp_rtp_g723_profile_shutdown( void );

/*--------------------------------------------------------------------------
      FUNCTIONS APART FROM PROFILE TEMPLATE 
--------------------------------------------------------------------------*/
LOCAL void qvp_rtp_g723_reset_tx_ctx
( 
  qvp_rtp_g723_ctx_type *stream 
);

LOCAL void qvp_rtp_g723_reset_stream
( 
  qvp_rtp_g723_ctx_type *stream 
);

LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_copy_config
(
  qvp_rtp_payload_config_type *payld_config,
  qvp_rtp_g723_config_type    *g723_config
);

LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_config_tx_param
(
  qvp_rtp_g723_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_config_rx_param
(
  qvp_rtp_g723_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL int16  qvp_rtp_g723_find_num_frames 
(
  qvp_rtp_buf_type *buf
);

/*--------------------------------------------------------------------------
    Table of accessors for the profile. This need to be populated in
    the table entry at qvp_rtp_profole.c. All these functions will 
    automatically link to the RTP stack by doing so.
--------------------------------------------------------------------------*/
qvp_rtp_profile_type qvp_rtp_g723_profile_table = 
{
  qvp_rtp_g723_profile_init,      /* LOACAL initialization routine  */
  qvp_rtp_g723_profile_open,      /* LOCAL function to open channel */
  qvp_rtp_g723_profile_send,      /* LOCAL function to send a pkt   */
  qvp_rtp_g723_profile_recv,      /* LOCAL function to rx a nw pkt  */
  qvp_rtp_g723_profile_close,     /* LOCAL function to close channel*/
  qvp_rtp_g723_profile_read_deflt_config,
  NULL,
  qvp_rtp_g723_profile_validate_config_rx,
  qvp_rtp_g723_profile_validate_config_tx,
  qvp_rtp_g723_profile_configure,
  qvp_rtp_g723_profile_shutdown   /* LOCAL function to shut down */

};

/*===========================================================================
                     LOCAL STATIC DATA 
===========================================================================*/

LOCAL boolean g723_initialized = FALSE;

/*--------------------------------------------------------------------------
     Stores the configuration requested by the app.                        
--------------------------------------------------------------------------*/
LOCAL  qvp_rtp_profile_config_type g723_profile_config;

/*--------------------------------------------------------------------------
        Stream context array.  Responsible for each bidirectional 
        connections
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_g723_ctx_type *qvp_rtp_g723_array = NULL;

/*--------------------------------------------------------------------------
     Configuration pertaining payload formatting for G723 
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_g723_config_type g723_stream_config;

/*--------------------------------------------------------------------------
    LOCAL table to parse the length
     bits  content                      octets/frame
     00    high-rate speech (6.3 kb/s)            24
     01    low-rate speech  (5.3 kb/s)            20
     10    SID frame                               4
     11    reserved
--------------------------------------------------------------------------*/
LOCAL int16 qvp_rtp__g723_toc_len_table[ 4 ] =
{
  24,     /* High rate speech  */
  20,     /* low rate speech */
  4,      /* SID frame */
  -1,     /*  reserved or invalid */
};


/*===========================================================================

FUNCTION QVP_RTP_G723_PROFILE_INIT 


DESCRIPTION
  Initializes data structures and resources used by the G723 profile.

DEPENDENCIES
  None

ARGUMENTS IN
  config - pointer to profile configuration requested.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could initialize the profile. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_init
(
  qvp_rtp_profile_config_type *config
)
{
/*------------------------------------------------------------------------*/


  if( g723_initialized )
  {
    return( QVP_RTP_SUCCESS );
  }
   
  /*------------------------------------------------------------------------
    Init if we get a valid config  and appropriate call backs 
    for the basic operation of the profile.
  ------------------------------------------------------------------------*/
  if( config && config->rx_cb && config->tx_cb )
  {
    qvp_rtp_memscpy( &g723_profile_config,sizeof( g723_profile_config ), config, sizeof( g723_profile_config ) );
  }
  else
  {
    return( QVP_RTP_WRONG_PARAM );
  }

  /*------------------------------------------------------------------------
    Allocate the number of streams first
  ------------------------------------------------------------------------*/
  qvp_rtp_g723_array  = qvp_rtp_malloc( config->num_streams * 
                                sizeof( qvp_rtp_g723_ctx_type ) ); 

  /*------------------------------------------------------------------------
      See if we could get the memory
  ------------------------------------------------------------------------*/
  if( !qvp_rtp_g723_array )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  else
  {
    memset( qvp_rtp_g723_array, 0, config->num_streams * 
                    sizeof( qvp_rtp_g723_ctx_type ) );
    
    /*----------------------------------------------------------------------
        Set up the default configuration
    ----------------------------------------------------------------------*/
    g723_stream_config.rx_ptime = QVP_RTP_G723_DFLT_PKT_INTERVAL;
    g723_stream_config.rx_max_ptime = QVP_RTP_DFLT_BUNDLE_SIZE *
                                      QVP_RTP_G723_DFLT_PKT_INTERVAL;
    g723_stream_config.rx_g723_pkt_interval = QVP_RTP_G723_DFLT_PKT_INCR;  
    
    /*----------------------------------------------------------------------
      We do not support interleaving yet...
      So easy
    ----------------------------------------------------------------------*/
    g723_stream_config.tx_bundle_size = QVP_RTP_DFLT_BUNDLE_SIZE;
    g723_stream_config.tx_ptime = QVP_RTP_G723_DFLT_PKT_INTERVAL;
    g723_stream_config.tx_max_ptime = QVP_RTP_DFLT_BUNDLE_SIZE *
                                      QVP_RTP_G723_DFLT_PKT_INTERVAL;

  }
    
  /*------------------------------------------------------------------------
    Flag initialization
  ------------------------------------------------------------------------*/
  g723_initialized  = TRUE;
  
  return( QVP_RTP_SUCCESS );

}/* end of function qvp_rtp_g723_profile_init */

/*===========================================================================

FUNCTION  QVP_RTP_G723_PROFILE_SEND


DESCRIPTION
  request to send specified buffer through this profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl     - handle into the profile. 
  usr_hdl - handle to use when we call send function using the registered
            tx callback
  pkt     - packet of data to be sent.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type              *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
)
{
  qvp_rtp_g723_ctx_type *stream = ( qvp_rtp_g723_ctx_type *) hdl; 
  uint8   fr_type;
  int16 fr_len;
  qvp_rtp_buf_type *nw_buf = NULL;
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  uint32 cur_tstamp;
  uint8  rtp_pyld_type;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
                    Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !g723_initialized || !stream || !stream->valid || 
      !g723_profile_config.tx_cb  || !pkt ) 
  {
    
    /*----------------------------------------------------------------------
      Free the packet before we bail out
    ----------------------------------------------------------------------*/
    if( pkt )
    {
      qvp_rtp_free_buf( pkt );
    }
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Store the payload type number
  ------------------------------------------------------------------------*/
   rtp_pyld_type = pkt->rtp_pyld_type;

  /*------------------------------------------------------------------------
      Send packet and return if we are in header less mode.

      I doubt we will ever use this
  ------------------------------------------------------------------------*/
  if( stream->stream_config.tx_bundle_size == 1)
  {
    
    /*----------------------------------------------------------------------
                    Ship the packet out 
    ----------------------------------------------------------------------*/
    return( g723_profile_config.tx_cb( pkt, usr_hdl ) );
    
  }

  /*------------------------------------------------------------------------
     store the current time stamp
  ------------------------------------------------------------------------*/
  cur_tstamp = pkt->tstamp;
  
  /*------------------------------------------------------------------------
               If packet len is zero this is a silent packet 
  ------------------------------------------------------------------------*/
  if( pkt->len )
  {
    
    
    /*----------------------------------------------------------------------
      Extract frame type from the first byte in the packet
    ----------------------------------------------------------------------*/
    fr_type = ( *( pkt->data + pkt->head_room ) & 0x3 ); 

    
    /*----------------------------------------------------------------------
      Lookup the length from the table
    ----------------------------------------------------------------------*/
    fr_len = qvp_rtp__g723_toc_len_table[ fr_type ];

    
    /*----------------------------------------------------------------------
      if we are getting a negative len bail out and return
    ----------------------------------------------------------------------*/
    if( fr_len < 0 )
    { 
      QVP_RTP_ERR("Bad frame passed in G723 fr_type = %d", fr_type, 0, 0 );
      stream->tx_ctx.frame_cnt++;
      
      /*----------------------------------------------------------------------
        Set the marker bit in the bit
      ----------------------------------------------------------------------*/
      stream->tx_ctx.marker |= TRUE;
      
      qvp_rtp_free_buf( pkt );
      return( QVP_RTP_ERR_FATAL );
    }
    
    /*----------------------------------------------------------------------
            concatenate the frame into our n/w buffer
    ----------------------------------------------------------------------*/
    qvp_rtp_memscpy( stream->tx_ctx.op_packet + stream->tx_ctx.data_len, QVP_RTP_MTU_DLFT - stream->tx_ctx.data_len,
              pkt->data  + pkt->head_room, pkt->len );
    
    /*----------------------------------------------------------------------
          Add to the data len for the newly copied frame
    ----------------------------------------------------------------------*/
    stream->tx_ctx.data_len += pkt->len;
            
  }
  else
  {
    
    /*----------------------------------------------------------------------
      Set the marker bit in the bit
    ----------------------------------------------------------------------*/
    stream->tx_ctx.marker |= TRUE;
  }
  
  /*------------------------------------------------------------------------
      Frame count should be incrementd on each frame

      We will ship the bundle when frame count equals bundle size
  ------------------------------------------------------------------------*/
  stream->tx_ctx.frame_cnt++;

  /*------------------------------------------------------------------------
    Free the current buffer
  ------------------------------------------------------------------------*/
  qvp_rtp_free_buf( pkt );


  /*------------------------------------------------------------------------
              If we reached the bundle size lets ship it out
  ------------------------------------------------------------------------*/
  if( stream->tx_ctx.frame_cnt == stream->stream_config.tx_bundle_size )
  {
    
    QVP_RTP_MSG_MED_1( " frame cnt %d \r\n", stream->tx_ctx.frame_cnt);
    /*----------------------------------------------------------------------
      If there is some data for this bundle then send it out 
    ----------------------------------------------------------------------*/
    if( stream->tx_ctx.data_len )
    {
      /*--------------------------------------------------------------------
        Try and allocate a buffer of appropriate length
      --------------------------------------------------------------------*/
      nw_buf = qvp_rtp_alloc_buf_by_len( stream->tx_ctx.data_len + 
                                         QVP_RTP_HEAD_ROOM );


      /*--------------------------------------------------------------------
         If we cannot allocate bail out
      --------------------------------------------------------------------*/
      if( !nw_buf )
      {
        qvp_rtp_g723_reset_tx_ctx( stream );
        return( QVP_RTP_ERR_FATAL );
      }

      
      /*--------------------------------------------------------------------
          Set up the network buffer
      --------------------------------------------------------------------*/
      nw_buf->head_room = QVP_RTP_HEAD_ROOM;
      qvp_rtp_memscpy( nw_buf->data + nw_buf->head_room,
              stream->tx_ctx.data_len, 
              stream->tx_ctx.op_packet, 
              stream->tx_ctx.data_len ); 
      nw_buf->len = stream->tx_ctx.data_len; 
      nw_buf->marker_bit = stream->tx_ctx.marker; 
      nw_buf->tstamp = cur_tstamp;
      nw_buf->rtp_pyld_type = rtp_pyld_type;

      /*--------------------------------------------------------------------
                      Ship the packet out 
      --------------------------------------------------------------------*/
      QVP_RTP_MSG_HIGH_1(" Sending G723 Bundle with tstamp = %d", 
                        pkt->tstamp, 0, 0 ); 
      if( g723_profile_config.tx_cb( nw_buf, usr_hdl ) != QVP_RTP_SUCCESS )
      {
        
        /*------------------------------------------------------------------
           If the callee did not free it lets free and bail out
        ------------------------------------------------------------------*/
        if( nw_buf->need_tofree )
        {
          qvp_rtp_free_buf( nw_buf );
        }
        status = QVP_RTP_ERR_FATAL;
      }
      
    }

    
    /*----------------------------------------------------------------------
                We have sent the packet so lets reset it for next bundle
    ----------------------------------------------------------------------*/
    qvp_rtp_g723_reset_tx_ctx( stream );
    
  }

  return( status );
  
} /* end of function qvp_rtp_g723_profile_send */

/*===========================================================================

FUNCTION  QVP_RTP_G723_PROFILE_OPEN 


DESCRIPTION
    Requests to open a bi directional channel within the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  usr_hdl       - handle to use when we call send function using the 
                  registered tx callback

ARGUMENTS OUT
  hdl           - on success we write the handle to the profiile into 
                  the  passed double pointer
  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl      /* user handle for any cb */
)
{
  uint32 i;   /* index variable */
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Bail out if we are not initialized yet
  ------------------------------------------------------------------------*/
  if( !g723_initialized )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
              look at an idle stream 
  ------------------------------------------------------------------------*/
  for( i = 0;  i < g723_profile_config.num_streams; i++ )
  {
    
    
    /*----------------------------------------------------------------------
              Fish for free entries
    ----------------------------------------------------------------------*/
    if( !qvp_rtp_g723_array[ i ].valid )
    {
      qvp_rtp_g723_array[ i ].valid = TRUE;
      *hdl = &qvp_rtp_g723_array[ i ]; 
      
      /*--------------------------------------------------------------------
            copy the G723 configuration to the  stream_ctx 
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( &qvp_rtp_g723_array[i].stream_config,sizeof(qvp_rtp_g723_config_type), 
        &g723_stream_config,sizeof( qvp_rtp_g723_array[i].stream_config ));

      /*--------------------------------------------------------------------
          Reset the stream context - if any stale values in there.
      --------------------------------------------------------------------*/
      qvp_rtp_g723_reset_stream( &qvp_rtp_g723_array[i] );
      
      
      
      return( QVP_RTP_SUCCESS );

    }
    
  } /* maximum no of streams */

  return( QVP_RTP_NORESOURCES );


} /* end of function qvp_rtp_g723_profile_open */


/*==================================================================

FUNCTION QVP_RTP_G723_PROFILE_VALIDATE_CONFIG 


DESCRIPTION

  This function checks the configuration of G723 specified in the input 
  against the valid fmtp values for G723

DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute  is valid, counter_config is not modified
                   If the attribute  is invalid,
                   counter_config is updated with proposed value

RETURN VALUE
  QVP_RTP_SUCCESS  - The given Configuration is  valid for the given payload,
                     RTP configure will succeed for these settings
                     
  QVP_RTP_ERR_FATAL   - The given Configuration is not valid for the given 
                        payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_validate_config_rx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  /*----------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if ( ( !payld_config ) || ( !counter_config ) )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Validating rx configuration
   -----------------------------------------------------------------------*/
  if ( ( payld_config->config_rx_params.valid ) && 
       ( payld_config->config_rx_params.
             config_rx_payld_params.g723_rx_config.valid ) )
  {
    if ( ( payld_config->ch_dir == QVP_RTP_CHANNEL_INBOUND ) || 
              ( payld_config->ch_dir == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
    {
      /*--------------------------------------------------------------------
        Validate ptime. Throw an error if we cant receive packets with 
        the ptime proposed.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_rx_params.rx_rtp_param.ptime_valid ) && 
           ( payld_config->config_rx_params.rx_rtp_param.ptime  != 
                       QVP_RTP_G723_DFLT_PKT_INTERVAL ) )
      {
        counter_config->config_rx_params.rx_rtp_param.ptime = 
                        QVP_RTP_G723_DFLT_PKT_INTERVAL ; 
        status = QVP_RTP_ERR_FATAL;
      }
      /*--------------------------------------------------------------------
        Validate maxptime. Throw an error if we cant receive packets with 
        the maxptime proposed.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_rx_params.rx_rtp_param.maxptime_valid ) && 
           ( ( payld_config->config_rx_params.rx_rtp_param.maxptime  < 
                       QVP_RTP_G723_DFLT_PKT_INTERVAL ) ||
             ( payld_config->config_rx_params.rx_rtp_param.maxptime > 
                    QVP_RTP_G723_DFLT_PKT_INTERVAL *
                                              QVP_RTP_DFLT_BUNDLE_SIZE ) )  )
      {
        counter_config->config_rx_params.rx_rtp_param.maxptime = 
                  QVP_RTP_G723_DFLT_PKT_INTERVAL *
                                                   QVP_RTP_DFLT_BUNDLE_SIZE ; 
        status = QVP_RTP_ERR_FATAL;
      }

    }/* end of if ( ( payld_config->ch_dir */
    else
    {
      QVP_RTP_ERR ( " Attempting to config rx for an o/b only ch", 0, 0, 0);
      status = QVP_RTP_ERR_FATAL ;
    }
  } /* end of   if ( ( payld_config->config_rx_params.valid )*/

  
  return ( status ); 


}/* end of qvp_rtp_g723_profile_validate_config_rx */


/*===========================================================================

FUNCTION QVP_RTP_G723_PROFILE_VALIDATE_CONFIG_TX 


DESCRIPTION

  This function checks the configuration of G723 specified in the input 
  against the valid fmtp values for G723

DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute  is valid, counter_config is not modified
                   If the attribute  is invalid, 
                   counter_config is updated with proposed value


RETURN VALUE
  QVP_RTP_SUCCESS  - The given Configuration is  valid for the given payload,
                     RTP configure will succeed for these settings
  QVP_RTP_ERR_FATAL   - The given Configuration is not valid for the given 
                        payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_validate_config_tx
(
  qvp_rtp_payload_config_type*  payld_config,
  qvp_rtp_payload_config_type*  counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Validating tx configuration
   -----------------------------------------------------------------------*/
  if ( ( payld_config->config_tx_params.valid ) && 
       ( payld_config->config_tx_params.
             config_tx_payld_params.g723_tx_config.valid ) )
  {
    if ( ( payld_config->ch_dir == QVP_RTP_CHANNEL_OUTBOUND ) || 
         ( payld_config->ch_dir == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
    {
      /*--------------------------------------------------------------------
        Validate ptime. Throw an error if peer wants us to send 
        packets with ptime lesser than our minimum supported ptime.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_tx_params.tx_rtp_param.ptime_valid ) && 
           ( payld_config->config_tx_params.tx_rtp_param.ptime < 
                       QVP_RTP_G723_DFLT_PKT_INTERVAL ) )
      {
        counter_config->config_tx_params.tx_rtp_param.ptime = 
                        QVP_RTP_G723_DFLT_PKT_INTERVAL ; 
        status = QVP_RTP_ERR_FATAL;
      }
      /*--------------------------------------------------------------------
        Validate maxptime. Throw an error if peer wants us to send 
        packets with maxptime lesser than our minimum supported maxptime.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_tx_params.tx_rtp_param.maxptime_valid ) && 
           ( payld_config->config_tx_params.tx_rtp_param.maxptime  < 
                       QVP_RTP_G723_DFLT_PKT_INTERVAL )  )
      {
        counter_config->config_tx_params.tx_rtp_param.maxptime = 
                  QVP_RTP_G723_DFLT_PKT_INTERVAL *
                                                   QVP_RTP_DFLT_BUNDLE_SIZE ; 
        status = QVP_RTP_ERR_FATAL;
      }

    }/* end of if ( ( payld_config->ch_dir */
    else
    {
      QVP_RTP_ERR ( " Attempting to config tx for an i/b only ch", 0, 0, 0);
      status = QVP_RTP_ERR_FATAL ;
    }
  } /* end of if ( ( payld_config->config_tx_params.valid )*/
  
  return ( status ); 

}/* end of qvp_rtp_g723_profile_validate_config_tx */


/*===========================================================================

FUNCTION QVP_RTP_G723_PROFILE_READ_DEFLT_CONFIG 


DESCRIPTION

  This function reads out the default configuration of G723
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is not used by G723. Some 
                     other profiles uses it as they support multiple 
                     payload formats.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_read_deflt_config
(
  
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
)
{

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {

    return( QVP_RTP_ERR_FATAL );
  }
  

  return( qvp_rtp_g723_profile_copy_config( payld_config, 
                                            &g723_stream_config  ) );

    
  
} /* end of function qvp_rtp_g723_profile_read_deflt_config */ 

/*===========================================================================

FUNCTION QVP_RTP_G723_PROFILE_CONFIGURE

DESCRIPTION

  This function payload configuration of a previously opened G723  
  channel.

DEPENDENCIES
  None

ARGUMENTS IN
    hdl            - handle to the opend channel
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
)
{
  qvp_rtp_g723_ctx_type *stream = ( qvp_rtp_g723_ctx_type *) hdl; 
  qvp_rtp_g723_config_type prev_config;
  qvp_rtp_status_type      status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/


  
  /*------------------------------------------------------------------------
                Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !g723_initialized || !stream || !stream->valid || 
      !payld_config ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Backup the current configuration before we proceed from here.
  ------------------------------------------------------------------------*/
  prev_config = stream->stream_config;
  
  /*------------------------------------------------------------------------
      Configuration steps.

      1) Try and configure RX.
         If RX fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model as 
          given in RFC3264.
      2) Try and configure TX
         If TX  configure fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model 
          as given in RFC3264.
  ------------------------------------------------------------------------*/

  if( payld_config->config_rx_params.valid )
  {
    status = qvp_rtp_g723_profile_config_rx_param( stream, payld_config );
  }


  /*------------------------------------------------------------------------
      Try and configure Tx part now....
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS )&&
      payld_config->config_tx_params.valid )
  {
    status = qvp_rtp_g723_profile_config_tx_param(stream, payld_config );
    
  }

  /*------------------------------------------------------------------------
    If we did not succeed to a struct to reset to preivous value and 
    return failure
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS )
  {
    stream->stream_config = prev_config ;
  }
  
  return( status );

} /* end of function qvp_rtp_g723_profile_configure */

/*===========================================================================

FUNCTION QVP_RTP_G723_PROFILE_CONFIG_RX_PARAM

DESCRIPTION

  This function payload will configuration of Rx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - G723 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_config_rx_param
(
  
  qvp_rtp_g723_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
)
{
  if( !payld_config || !stream ) 
  {
    QVP_RTP_ERR ( " Invalid params", 0, 0, 0);
    return( QVP_RTP_ERR_FATAL );
  }
  /*------------------------------------------------------------------------
    See if the pttime matches with what we have
  ------------------------------------------------------------------------*/
  if( payld_config->config_rx_params.rx_rtp_param.ptime_valid && 
      ( payld_config->config_rx_params.rx_rtp_param.ptime != 
                                 QVP_RTP_G723_DFLT_PKT_INTERVAL ) )
  {
    QVP_RTP_MSG_MED_1( "G723 Invalid Rx ptime %d ",
        payld_config->config_rx_params.rx_rtp_param.ptime, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    We will cache the max ptime ... If this exceeds too much this will
    be bad for our jitter buffer. But then the other end is asking 
    for trouble.
  ------------------------------------------------------------------------*/
  if( payld_config->config_rx_params.rx_rtp_param.maxptime_valid ) 
  {

    /*----------------------------------------------------------------------
      if maxptime is LT ptime or greater than maximum maxptime we can 
      support bail out with error.
    ----------------------------------------------------------------------*/
    if( ( payld_config->config_rx_params.rx_rtp_param.maxptime < 
        QVP_RTP_G723_DFLT_PKT_INTERVAL ) || 
        ( payld_config->config_rx_params.rx_rtp_param.maxptime > 
           QVP_RTP_DFLT_BUNDLE_SIZE*QVP_RTP_G723_DFLT_PKT_INTERVAL ) )
    {
      QVP_RTP_MSG_MED_1( "G723 Invalid Rx maxptime %d ",
        payld_config->config_rx_params.rx_rtp_param.maxptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
    stream->stream_config.rx_max_ptime = 
          payld_config->config_rx_params.rx_rtp_param.maxptime; 
  }
  QVP_RTP_MSG_HIGH_2( "G723 Rx maxptime %d Rx ptime %d",
        stream->stream_config.rx_max_ptime,
         stream->stream_config.rx_ptime, 0 );
  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_g723_profile_config_rx_param */ 

/*===========================================================================

FUNCTION QVP_RTP_G723_PROFILE_CONFIG_TX_PARAM

DESCRIPTION

  This function payload will configuration of Tx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - G723 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_config_tx_param
(
  
  qvp_rtp_g723_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
)
{
  if( !payld_config || !stream ) 
  {
    QVP_RTP_ERR ( " Invalid params", 0, 0, 0);
    return( QVP_RTP_ERR_FATAL );
  }

  if( payld_config->config_tx_params.tx_rtp_param.ptime_valid )
  {
    /*----------------------------------------------------------------------
      This means peer can receive pkts with ptime lesser 
      than our minimum ptime pkts we can send. Throw an error.
    ----------------------------------------------------------------------*/
    if ( payld_config->config_tx_params.tx_rtp_param.ptime < 
                QVP_RTP_G723_DFLT_PKT_INTERVAL )
    {
      QVP_RTP_MSG_MED_1( "G723 Invalid Tx ptime %d ",
        payld_config->config_tx_params.tx_rtp_param.ptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
    /*----------------------------------------------------------------------
      This means peer can receive pkts with ptime greater 
      than our maximum ptime. Just set the transmit ptime to maximum 
      ptime we can send.
    ----------------------------------------------------------------------*/
    else if ( payld_config->config_tx_params.tx_rtp_param.ptime > 
                QVP_RTP_G723_DFLT_PKT_INTERVAL )
    {
      stream->stream_config.tx_ptime = QVP_RTP_G723_DFLT_PKT_INTERVAL;
    }
    /*----------------------------------------------------------------------
      Proposed ptime is within the limits, we can send packets with the 
      ptime proposed.
    ----------------------------------------------------------------------*/
    else
    {
      stream->stream_config.tx_ptime = 
          payld_config->config_tx_params.tx_rtp_param.ptime;
    }
  }

  if( payld_config->config_tx_params.tx_rtp_param.maxptime_valid ) 
  {
    /*----------------------------------------------------------------------
      if max ptime is less than ptime then we have a problem. 
    ----------------------------------------------------------------------*/
    if( payld_config->config_tx_params.tx_rtp_param.maxptime < 
        QVP_RTP_G723_DFLT_PKT_INTERVAL )
    {
      QVP_RTP_MSG_MED_1( "G723 Invalid Tx maxptime %d ",
        payld_config->config_tx_params.tx_rtp_param.maxptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
    /*----------------------------------------------------------------------
      This means peer can receive pkts with maxptime greater 
      than our maximum maxptime. Just set the transmit maxptime to 
      maximum maxptime we can send.
    ----------------------------------------------------------------------*/
    if( payld_config->config_tx_params.tx_rtp_param.maxptime > 
        QVP_RTP_G723_DFLT_PKT_INTERVAL )
    {
      stream->stream_config.tx_max_ptime   = 
        QVP_RTP_G723_DFLT_PKT_INTERVAL*QVP_RTP_DFLT_BUNDLE_SIZE;
    }
    /*----------------------------------------------------------------------
      Proposed maxptime is within the limits, we can send packets with the 
      maxptime proposed.
    ----------------------------------------------------------------------*/
    else
    {
      stream->stream_config.tx_max_ptime = 
            payld_config->config_tx_params.tx_rtp_param.maxptime;
    }

  }

  /*------------------------------------------------------------------------
    bundle size should be derived off the maxptime
  ------------------------------------------------------------------------*/
  stream->stream_config.tx_bundle_size = 
    stream->stream_config.tx_max_ptime/stream->stream_config.tx_ptime;

  QVP_RTP_MSG_HIGH( "G723 Tx maxptime %d Tx ptime %d bundle size %d",
        stream->stream_config.tx_max_ptime,
     stream->stream_config.tx_ptime, stream->stream_config.tx_bundle_size );
  return( QVP_RTP_SUCCESS );
        
} /* end of function qvp_rtp_g723_profile_config_tx_param */
  
/*===========================================================================

FUNCTION  QVP_RTP_G723_PROFILE_RECV 


DESCRIPTION
   The function which the RTP framework will call upon arrival of data from 
   NW. G723 header is stripped but parameters are passed.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl           - hdl into the profile.
  usr_hdl       - handle to use when we call send function using the 
                  registered rx callback
  pkt           - actual data packet to be parsed.

  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed*/

)
{
  qvp_rtp_g723_ctx_type *stream = ( qvp_rtp_g723_ctx_type *) hdl; 
  uint16                  toc;        /* to parse toc field */
  qvp_rtp_buf_type       *aud_buf;
  uint16                 len;
  uint8                  *aud_data;
  uint16                 aud_len;
  int16                 num_frames;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
                Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !g723_initialized || !stream || !stream->valid || 
      !g723_profile_config.rx_cb  ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
              If the configuration says bundle size is 1 just  
  ------------------------------------------------------------------------*/
  if( stream->stream_config.tx_bundle_size == 1 )
  {
    
    /*----------------------------------------------------------------------
              pass the packet up
    ----------------------------------------------------------------------*/
    return( g723_profile_config.rx_cb( pkt, usr_hdl ) ); 
    
  }

  QVP_RTP_MSG_MED_2( "RX G723 packet tstamp =  %d, Seq = %d", 
                      pkt->tstamp , pkt->seq, 0 );
  
  if( ( stream->rx_ctx.last_seq + 1 ) != pkt->seq ) 
  {
    QVP_RTP_ERR( " Missisg Seq inside RTP", 0, 0, 0 ); 
  }
 
  stream->rx_ctx.last_seq = pkt->seq;


  /*------------------------------------------------------------------------
      Advance to audio data inside the packet 
  ------------------------------------------------------------------------*/
  aud_data = pkt->data + pkt->head_room;
  
  /*------------------------------------------------------------------------
      Preacalculate the audio data length inside the packet
  ------------------------------------------------------------------------*/
  aud_len = pkt->len;

  /*------------------------------------------------------------------------
    Find the number of pkts in the bundle
  ------------------------------------------------------------------------*/
  num_frames = qvp_rtp_g723_find_num_frames(  pkt );

  /*------------------------------------------------------------------------
    Try and find the number of frames
  ------------------------------------------------------------------------*/
  if( num_frames <= 0 )
  {
    qvp_rtp_free_buf( pkt );
    return( QVP_RTP_ERR_FATAL );
  }

  
  /*------------------------------------------------------------------------
      iterate through the packet until we have parsed all the frames...

      Or we get erraneous PDU.
  ------------------------------------------------------------------------*/
  while( aud_len)
  {

    /*----------------------------------------------------------------------
                    read the toc
    ----------------------------------------------------------------------*/
    toc = *aud_data & 0x3; 

    
    /*----------------------------------------------------------------------
                validate the toc first
    ----------------------------------------------------------------------*/
    if( qvp_rtp__g723_toc_len_table[ toc ] < 0 )
    {
      
      /*--------------------------------------------------------------------
                  Free the buffer if needed
      --------------------------------------------------------------------*/
      if( pkt->need_tofree )
      {
        qvp_rtp_free_buf( pkt );
      }
      
      QVP_RTP_ERR( " got unknown toc dont  support this \r\n", 0, 0, 0 ); 
      return( QVP_RTP_ERR_FATAL );
      
    }

    /*----------------------------------------------------------------------
                find the pkt len
    ----------------------------------------------------------------------*/
    len = qvp_rtp__g723_toc_len_table[ toc ];

    
    /*----------------------------------------------------------------------
       Double check for the len remaining
    ----------------------------------------------------------------------*/
    if( aud_len < len )
    {
      
      
      /*--------------------------------------------------------------------
        Malformed or corrupt packet
      --------------------------------------------------------------------*/
      QVP_RTP_ERR( " Insufficient data len inside the G723 \r\n", \
                    0, 0, 0 ); 
      
      /*--------------------------------------------------------------------
                  Free the buffer if needed
      --------------------------------------------------------------------*/
      if( pkt->need_tofree )
      {
        qvp_rtp_free_buf( pkt );
      }
      
      return( QVP_RTP_ERR_FATAL );
    }
    

    /*--------------------------------------------------------------------
              try and alloc an audio buffer
    --------------------------------------------------------------------*/
    aud_buf = qvp_rtp_alloc_buf( QVP_RTP_POOL_AUDIO );
  
  
    /*--------------------------------------------------------------------
              See if we got a buffer
    --------------------------------------------------------------------*/
    if ( !aud_buf )
    {
  
      QVP_RTP_ERR( " Could not get an audio buffer \r\n", 0, 0, 0 );
  
      /*------------------------------------------------------------------
              Free if needed
      ------------------------------------------------------------------*/
      if ( pkt->need_tofree )
      {
        qvp_rtp_free_buf( pkt );
      }
      return( QVP_RTP_ERR_FATAL );
  
    }
    else
    {
      aud_buf->head_room = 0;
      aud_buf->len = len;
      
      /*--------------------------------------------------------------------

      --------------------------------------------------------------------*/
      
      aud_buf->tstamp = pkt->tstamp - ( ( num_frames - 1 ) * 
                       stream->stream_config.rx_g723_pkt_interval );
      aud_buf->need_tofree = TRUE;
      aud_buf->seq = pkt->seq;
  
      qvp_rtp_memscpy( aud_buf->data,len, aud_data, len ); 
  
      
      /*--------------------------------------------------------------------
        If this is a silence frame or 1/8th frame mark it 
      --------------------------------------------------------------------*/
      if( toc ==  QVP_RTP_G723_TOC_SID )
      {
        aud_buf->silence = TRUE;
      }
      else
      {
        aud_buf->silence = FALSE;
      }
      
      /*--------------------------------------------------------------------
           Mark frm_present bit
      --------------------------------------------------------------------*/
      aud_buf->frm_info.info.aud_info.frm_present = TRUE;
  
      /*------------------------------------------------------------------
                Ship the packet up

                If we could not ship it break;
      ------------------------------------------------------------------*/
      if( g723_profile_config.rx_cb(aud_buf, usr_hdl ) != QVP_RTP_SUCCESS )
      {
        break;
      }
    }


    /*----------------------------------------------------------------------
              Decrement the frame count next no
    ----------------------------------------------------------------------*/
    num_frames--;

    
    /*----------------------------------------------------------------------
            decrement the remaining audion len accordingly
    ----------------------------------------------------------------------*/
    aud_len -= len;
    
      
   /*-----------------------------------------------------------------------
    Advance the audio buffer
   -----------------------------------------------------------------------*/
   aud_data += len;
    
    
  } /* end of while aud_len */

  
  /*----------------------------------------------------------------------
            Free the buffer if needed
  ----------------------------------------------------------------------*/
  if( pkt->need_tofree )
  {
    qvp_rtp_free_buf( pkt );
  }

  /*------------------------------------------------------------------------
     If at this moment hdr.count is not zero there is something wrong
  ------------------------------------------------------------------------*/
  if( aud_len )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  else
  {
    return( QVP_RTP_SUCCESS );
  }
  
} /* end of function qvp_rtp_g723_profile_recv */

/*===========================================================================

FUNCTION  QVP_RTP_G723_PROFILE_CLOSE


DESCRIPTION
  Closes an already open bi directional channel inside the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl - handle of channel to close.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

)
{
  qvp_rtp_g723_ctx_type *stream = ( qvp_rtp_g723_ctx_type *) hdl; 
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
              If we get  a valid handle invalidate the index 

              Also check are we initialized yet ..?
  ------------------------------------------------------------------------*/
  if( hdl && g723_initialized )
  {
    
    /*----------------------------------------------------------------------
        We cannot afford to loose buffers so when we are in the middle of
        reassmbling we need to free the whole chain
    ----------------------------------------------------------------------*/
    stream->valid = FALSE;
    
    /*----------------------------------------------------------------------
        Reset the stream context - if any stale values in there.
    ----------------------------------------------------------------------*/
    qvp_rtp_g723_reset_stream( stream );
    
    return( QVP_RTP_SUCCESS );
  }
  else
  {
    return( QVP_RTP_ERR_FATAL );
  }


} /* end of function qvp_rtp_g723_profile_close */ 

/*===========================================================================

FUNCTION  QVP_RTP_G723_RESET_TX_CTX


DESCRIPTION
  Reset the trasmitter context inside the stream

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  None


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_g723_reset_tx_ctx
( 
  qvp_rtp_g723_ctx_type *stream 
)
{
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
      Reset the packet to packet to packet context
  ------------------------------------------------------------------------*/
  stream->tx_ctx.data_len = 0;
  stream->tx_ctx.frame_cnt = 0;
  stream->tx_ctx.marker = 0;

  
} /* end of function qvp_rtp_g723_reset_tx_ctx */
    
/*===========================================================================

FUNCTION  QVP_RTP_G723_RESET_STREAM 


DESCRIPTION
  Reset the context inside the stream

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_g723_reset_stream
( 
  qvp_rtp_g723_ctx_type *stream 
)
{
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
          Reset the transmit side
  ------------------------------------------------------------------------*/
  qvp_rtp_g723_reset_tx_ctx( stream );

  /*------------------------------------------------------------------------
      add code to reset rx_ctx when needed if and when we do 
      interleaving 
  ------------------------------------------------------------------------*/
  
} /* end of function qvp_rtp_g723_reset_stream */

/*===========================================================================

FUNCTION QVP_RTP_G723_PROFILE_COPY_CONFIG 


DESCRIPTION

  This function reads out the default configuration of G723
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is not used by G723. Some 
                     other profiles uses it as they support multiple 
                     payload formats.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g723_profile_copy_config
(
  qvp_rtp_payload_config_type *payld_config,
  qvp_rtp_g723_config_type    *g723_config
)
{
  qvp_rtp_g723_sdp_config_type  *g723_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.g723_tx_config;
  qvp_rtp_g723_sdp_config_type  *g723_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.g723_rx_config;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Memset the whole configuration before we proceed
  ------------------------------------------------------------------------*/
  memset( g723_rx_config, 0, 
          sizeof( qvp_rtp_g723_sdp_config_type ) ); 
  memset( g723_tx_config, 0, 
          sizeof( qvp_rtp_g723_sdp_config_type ) );
  
  /*------------------------------------------------------------------------
      Copy each and every FMTP and flag them as TRUE
  ------------------------------------------------------------------------*/
  
  payld_config->config_tx_params.tx_rtp_param.maxptime_valid = 
  payld_config->config_rx_params.rx_rtp_param.maxptime_valid = TRUE;

  payld_config->config_tx_params.tx_rtp_param.maxptime = 
                                  g723_config->tx_max_ptime;
  payld_config->config_rx_params.rx_rtp_param.maxptime = 
                                  g723_config->rx_max_ptime;

  payld_config->config_tx_params.tx_rtp_param.ptime_valid = 
  payld_config->config_rx_params.rx_rtp_param.ptime_valid = TRUE;

  payld_config->config_tx_params.tx_rtp_param.ptime = 
                                  g723_config->tx_ptime;
  payld_config->config_rx_params.rx_rtp_param.ptime = 
                                  g723_config->rx_ptime;
  /*------------------------------------------------------------------------
    Flag the entire rx  and tx config as valid

    Just make sure we stick in our paylod format in the pointer.

    If you got here by accident you get what you get.
  ------------------------------------------------------------------------*/
  payld_config->config_rx_params.valid = TRUE;
  payld_config->config_tx_params.valid = TRUE;
  
  payld_config->config_tx_params.config_tx_payld_params.g723_tx_config.valid 
                                              = TRUE;
  payld_config->config_rx_params.config_rx_payld_params.g723_rx_config.valid 
                                      = TRUE; 
  payld_config->payload  = QVP_RTP_PYLD_G723;

  payld_config->chdir_valid = TRUE;
  payld_config->ch_dir = QVP_RTP_CHANNEL_FULL_DUPLEX;
  payld_config->valid = TRUE;


  payld_config->valid = TRUE;
  
  
  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_g723_profile_copy_config */ 

/*===========================================================================

FUNCTION  QVP_RTP_G723_PROFILE_SHUTDOWN


DESCRIPTION
  Shuts this module down and flag intialization as false

DEPENDENCIES
  None

ARGUMENTS IN
  len - length of the parload

RETURN VALUE
  toc type


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_g723_profile_shutdown( void )
{
  uint32 i;
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Lets not do multiple shutdown
  ------------------------------------------------------------------------*/
  if( !g723_initialized )
  {
    return;
  }
  
  /*------------------------------------------------------------------------
      Walk through the stream array and close all channels
  ------------------------------------------------------------------------*/
  for( i = 0; i < g723_profile_config.num_streams; i ++ )
  {

    
    /*----------------------------------------------------------------------
        If this stream is open close it now
    ----------------------------------------------------------------------*/
    if ( qvp_rtp_g723_array[ i ].valid )
    {
      qvp_rtp_g723_profile_close( ( qvp_rtp_profile_hdl_type ) i );
    }
    
  } /* end of for i = 0  */
  
  /*------------------------------------------------------------------------
    free the array of streams
  ------------------------------------------------------------------------*/
  qvp_rtp_free( qvp_rtp_g723_array   );

  /*------------------------------------------------------------------------
    Flag array as NULL and init as FALSE
  ------------------------------------------------------------------------*/
  qvp_rtp_g723_array = NULL; 
  g723_initialized  = FALSE;
  
  
} /* end of function qvp_rtp_g723_profile_shutdown  */

/*===========================================================================

FUNCTION  QVP_RTP_G723_FIND_NUM_FRAMES


DESCRIPTION
  This function walks through a bundled frame

DEPENDENCIES
  None

ARGUMENTS IN
  len - length of the parload

RETURN VALUE
  toc type


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL int16  qvp_rtp_g723_find_num_frames 
(
  qvp_rtp_buf_type *buf
)
{
  uint8 *data = buf->data + buf->head_room;
  uint16 len = buf->len;
  uint8  fr_type;
  int16  fr_cnt = 0;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
        iterate through the frame to count number of frames
  ------------------------------------------------------------------------*/
  while( len )
  {
    /*----------------------------------------------------------------------
      Extract the G723 frame type
    ----------------------------------------------------------------------*/
    fr_type = *data & 0x3;
    fr_cnt++;

    /*----------------------------------------------------------------------
      if there is an error bail out
    ----------------------------------------------------------------------*/
    if( ( qvp_rtp__g723_toc_len_table[ fr_type ] < 0 ) || 
      ( qvp_rtp__g723_toc_len_table[ fr_type ] > len  ) )
    {
      return( 0 );
    }

    data += qvp_rtp__g723_toc_len_table[ fr_type ]; 
    len  -= qvp_rtp__g723_toc_len_table[ fr_type ]; 

  } /* end of while  */


  return( fr_cnt );

} /* end of function qvp_rtp_g723_find_num_frames */ 

#endif /* end of FEATURE_QVPHONE_RTP */
