/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                         QVP_RTP_MP4_PROFILE . C

GENERAL DESCRIPTION

  This file contains the implementation of MP4 profile. This file takes 
  care of payload fragmentation, reassebmly etc.

EXTERNALIZED FUNCTIONS
  None. Exposed through a profile table. All the functions are static
  but exposed through function pointers.


INITIALIZATION AND SEQUENCING REQUIREMENTS

  None of the externalized functions will function before the task is 
  spawned.


  Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/ps_media/rtp_profile/src/qvp_rtp_mp4_profile.c#1 $ $DateTime: 2016/03/28 23:03:22 $ $Author: mplcsds1 $

                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
16/04/08   apr    KW critical warning fixes
01/03/08   grk    Set profile reassembly handle for stream only if rx 
                  configuration is requested.
12/31/07   grk    Moving maxptime/ptime to media level.
06/14/07   apr    Removed reset reassembly in open profile
01/11/07   apr    Move reassembly part into a separate module
03/26/07   apr    Removing compiler warnings
03/21/07   apr    Removing rate parameter in config
12/20/06   apr    Fix for wrap around reassembling 
07/13/06   uma    Added new functions for validating payload configuration
06/14/06   apr    Modifying send func copy payload header
06/09/06   apr    RTP payload type number is copied into the network
                  buffer, as part of fix for CR 95499
05/25/06   xz     Remove 7k compiler warning
05/05/06   apr    Including RTP code in feature FEATURE_QVPHONE_RTP
25/04/06   apr    Fixing bugs related to feature partial frames
03/07/06   apr    Including error concealment code
                  in QVP_RTP_ERROR_CONCEAL_PATTERN
03/07/06   apr    Added support for handling partial frames based on 
                  timestamp 
03/20/06   uma    Replaced all malloc and free with wrappers 
                  qvp_rtp_malloc & qvp_rtp_free 
11/18/05   uma    Added support for MP4 slicing during transmission
05/20/05   srk    Added Configure API support
11/03/04   srk    Initial Creation.
===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP

#ifdef  RTP_FILE_NUMBER 
  #undef RTP_FILE_NUMBER 
#endif 
#define RTP_FILE_NUMBER 25
/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <comdef.h>               /* common target/c-sim defines */
#include <string.h>               /* memory routines */
#include <stdlib.h>               /* for malloc */
#include <comdef.h>               /* common target/c-sim defines */
#include "qvp_rtp_api.h"          /* return type propagation */
#include "qvp_rtp_buf.h"
#include "qvp_rtp_profile.h"      /* profile template data type */
#include "qvp_rtp_mp4_profile.h"
#include "qvp_rtp_buf.h"          /* for memory management */
#include "qvp_rtp_log.h"
#include "stdio.h"
#include <bit.h>
#include <list.h>


/*===========================================================================
                    LOCAL STATIC FUNCTIONS 
===========================================================================*/

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_init
(
  qvp_rtp_profile_config_type *config
);

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl     /* user handle for any cb */
  
);

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_read_deflt_config
(
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_validate_config_rx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_validate_config_tx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
);

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * the profile formatting
                                              */

);

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

);

LOCAL void qvp_rtp_mp4_profile_shutdown( void );

LOCAL qvp_rtp_status_type qvp_rtp_mp4_fragment_and_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
);


LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_recv_3016
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * the profile formatting
                                              */

);

LOCAL qvp_rtp_status_type  qvp_rtp_mp4_stuff_single_au_header
( 
  qvp_rtp_profile_hdl_type    hdl,          /* handle created while open */
  qvp_rtp_buf_type            *pkt, 
  uint16                      au_len,
  boolean                     marker
);


LOCAL uint16 qvp_rtp_mp4_parse_au_hdr
( 
  qvp_rtp_mp4_ctx_type *stream, 
  uint8 *au_hdr, 
  uint32 au_hdr_len, 
  qvp_rtp_mp4_au_param_type *au_param,
  uint16 *parse_offset
);

LOCAL void qvp_rtp_mp4_abort_reassembly
( 
  qvp_rtp_mp4_ctx_type *stream
);


LOCAL int qvp_rtp_mp4_compare_fgmt
( 
  void *item, 
  void *seq_no
);

LOCAL void qvp_rtp_mp4_reset_reassem_ctx
( 
  qvp_rtp_mp4_ctx_type *stream
);

LOCAL void qvp_rtp_mp4_reset_stream
(
  qvp_rtp_mp4_ctx_type *stream
);

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_copy_config
(
  qvp_rtp_payload_config_type *payld_config,
  qvp_rtp_mp4_config_type    *mp4_config
);

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_config_rx_param
(
  
  qvp_rtp_mp4_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_config_tx_param
(
  
  qvp_rtp_mp4_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
);


/*===========================================================================
                    LOCAL STATIC DATA 
===========================================================================*/
/*--------------------------------------------------------------------------
    Table of accessors for the profile. This need to be populated in
    the table entry at qvp_rtp_profole.c. All these functions will 
    automatically link to the RTP stack by doing so.
--------------------------------------------------------------------------*/
qvp_rtp_profile_type qvp_rtp_mp4_profile_table = 
{
  qvp_rtp_mp4_profile_init,            /* LOACAL initialization routine  */
  qvp_rtp_mp4_profile_open,            /* LOCAL function to open channel */
  qvp_rtp_mp4_profile_send,            /* LOCAL function to send a pkt   */
  qvp_rtp_mp4_profile_recv,            /* LOCAL function to rx a nw pkt  */
  qvp_rtp_mp4_profile_close,           /* LOCAL function to close channel */
  qvp_rtp_mp4_profile_read_deflt_config, /* read the defaults */
  NULL,                            /* read config not supported for now  */
  qvp_rtp_mp4_profile_validate_config_rx,
  qvp_rtp_mp4_profile_validate_config_tx,
  qvp_rtp_mp4_profile_configure,       /* configure the payld format */ 
  qvp_rtp_mp4_profile_shutdown         /* LOCAL function to shut down  */

};

/*--------------------------------------------------------------------------
  Flags module initialization  
--------------------------------------------------------------------------*/ 
LOCAL boolean mp4_initialized;

/*--------------------------------------------------------------------------
        Stores the configuration requested by the app. 

        This is generic fconfiguration which hooks the profile to some
        user (RTP stack context ). Created off a template for all
        profiles.
--------------------------------------------------------------------------*/
LOCAL  qvp_rtp_profile_config_type mp4_profile_config;

/*--------------------------------------------------------------------------
        Stream context array.  Responsible for each bidirectional 
        connections
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_mp4_ctx_type *qvp_rtp_mp4_array = NULL;

/*--------------------------------------------------------------------------
     Configuration pertaining payload formatting for MP4 
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_mp4_config_type mp4_stream_config;


/*===========================================================================

FUNCTION QVP_RTP_MP4_PROFILE_INIT 


DESCRIPTION
  Initializes data structures and resources used by the MP4 profile.

DEPENDENCIES
  None

ARGUMENTS IN
  config - pointer to profile configuration requested.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could initialize the profile. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_init
(
  qvp_rtp_profile_config_type *config
)
{

/*------------------------------------------------------------------------*/


  
  /*------------------------------------------------------------------------
    If this module is already inited then just return success
  ------------------------------------------------------------------------*/
  if( mp4_initialized )
  {
    return( QVP_RTP_SUCCESS );
  }

  /*------------------------------------------------------------------------
    Init if we get a valid config  and appropriate call backs 
    for the basic operation of the profile.
  ------------------------------------------------------------------------*/
  if( config && config->rx_cb && config->tx_cb )
  {
    qvp_rtp_memscpy( &mp4_profile_config,sizeof( mp4_profile_config ), config, sizeof( mp4_profile_config ) );
  }
  else
  {
    QVP_RTP_ERR( "qvp_rtp_mp4_profile_init:: wrong parameters ", 0, 0, 0 );
    return( QVP_RTP_WRONG_PARAM );
  }

  qvp_rtp_mp4_array = qvp_rtp_malloc(  sizeof( qvp_rtp_mp4_ctx_type  )
                                  * config->num_streams );

  if( qvp_rtp_mp4_array == NULL )
  {
   // QVP_RTP_ERR( "Could not allocate qvp_rtp_mp4_array during init ", 
     //                           0, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }
  else
  {
    memset( qvp_rtp_mp4_array, 0, sizeof( qvp_rtp_mp4_ctx_type  )
                              * config->num_streams );
  }


  

  /*------------------------------------------------------------------------
    Initial default configuration  
  ------------------------------------------------------------------------*/
  mp4_stream_config.rx_inteleave_support = FALSE;
  mp4_stream_config.rx_au_hdr_on = FALSE;
  mp4_stream_config.rx_CTS_size = 0;
  mp4_stream_config.rx_de_interleave_buf_size = 0;
  mp4_stream_config.rx_DTS_size = 0;
  mp4_stream_config.rx_index_len = 8;  /* 8 bit index 0 - 255 and
                                        * wrap around 
                                        */
  mp4_stream_config.rx_rap_fag_present = TRUE;
  mp4_stream_config.rx_aux_data_len = 0;
  mp4_stream_config.rx_max_displacement = 0;
  mp4_stream_config.rx_stream_state_size = 0;
  mp4_stream_config.rx_AU_size_len = 16;  /* 16 bit AU size */
  mp4_stream_config.rx_err_concl_support = FALSE; /* Error concealment 
                                                   * support is not present
                                                   * by default 
                                                   */
  
  mp4_stream_config.tx_inteleave_support = FALSE;
  mp4_stream_config.tx_au_hdr_on = FALSE;
  mp4_stream_config.tx_CTS_size = 0;
  mp4_stream_config.tx_de_interleave_buf_size = 0;
  mp4_stream_config.tx_DTS_size = 0;
  mp4_stream_config.tx_index_len = 8;  /* 8 bit index 0 - 255 and
                                        * wrap around 
                                        */
  mp4_stream_config.tx_max_displacement = 0;
  mp4_stream_config.tx_stream_state_size = 0;
  mp4_stream_config.tx_AU_size_len = 16;  /* 16 bit AU size */
  mp4_stream_config.tx_aux_data_len = 0;
  mp4_stream_config.tx_slicing_enabled = TRUE; 



  /*------------------------------------------------------------------------
    Flag the module initialization
  ------------------------------------------------------------------------*/
  mp4_initialized = TRUE;

  return( QVP_RTP_SUCCESS );

}/* end of function qvp_rtp_mp4_profile_init */

/*===========================================================================

FUNCTION  QVP_RTP_MP4_PROFILE_SEND


DESCRIPTION
  request to send specified buffer through this profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl     - handle into the profile. 
  usr_hdl - handle to use when we call send function using the registered
            tx callback
  pkt     - packet of data to be sent.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
)
{
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    If no tx cb or uninitialized bail out
  ------------------------------------------------------------------------*/
  if( !mp4_profile_config.tx_cb || !mp4_initialized )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  
  /*------------------------------------------------------------------------
      Copy payload header
  ------------------------------------------------------------------------*/
  if( pkt->codec_hdr.valid && pkt->codec_hdr.hdr_len > 0 )
  {
    if( pkt->head_room < pkt->codec_hdr.hdr_len ||
        pkt->codec_hdr.hdr_len > sizeof( pkt->codec_hdr.codec_hdr) )
    {
      QVP_RTP_ERR( " qvp_rtp_mp4_profile_send : \
                      not enough head room \r\n", 0, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
    pkt->len += pkt->codec_hdr.hdr_len;
    pkt->head_room -= pkt->codec_hdr.hdr_len;
    qvp_rtp_memscpy( pkt->data + pkt->head_room , pkt->codec_hdr.hdr_len ,pkt->codec_hdr.codec_hdr, 
            pkt->codec_hdr.hdr_len );
  }

  /*------------------------------------------------------------------------
    If slicing is enabled the data can be sent as it is after 
    stuffing AU header. Otherwise data is fragmented based on size
  ------------------------------------------------------------------------*/
  if( !mp4_stream_config.tx_slicing_enabled )
  {
  
    /*----------------------------------------------------------------------
      See if we can fit the whole packet in one segment. 
    ----------------------------------------------------------------------*/
    if( pkt->len + QVP_RTP_MP4_SINGLE_AU_HDR_SZ > mp4_profile_config.rtp_mtu )
    {

      /*----------------------------------------------------------------------
        We need to fragment and send the packets to the network 
      ----------------------------------------------------------------------*/
      return( qvp_rtp_mp4_fragment_and_send( hdl, usr_hdl, pkt   ) );
      
    }
    else
    {
      pkt->marker_bit = TRUE;
    }
  }
    
  /*----------------------------------------------------------------------
    For now we are not concatenating the AUs. So we need to stick 
    in a MP4 Header  and send this down the stack. 
  ----------------------------------------------------------------------*/
  if( qvp_rtp_mp4_stuff_single_au_header( hdl, pkt, pkt->len, 
                                                      pkt->marker_bit ) 
                                                      != QVP_RTP_SUCCESS )
  {
    return( QVP_RTP_ERR_FATAL );
  }


  QVP_RTP_MSG_MED( " Sending MP4 Len = %d, tstamp = %d, marker = %d",
                                  pkt->len, pkt->tstamp, TRUE );

  
  /*----------------------------------------------------------------------
      reurn the return value of the tx function 
  ----------------------------------------------------------------------*/
  return( mp4_profile_config.tx_cb( pkt, usr_hdl  ) );

} /* end of function qvp_rtp_mp4_profile_send */

/*===========================================================================

FUNCTION  QVP_RTP_MP4_PROFILE_OPEN 


DESCRIPTION
    Requests to open a bi directional channel within the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  usr_hdl       - handle to use when we call send function using the 
                  registered tx callback
  stream_param  - stream parameters applies to profiles with transport 
                  integrated. Its not planning to use the RTP.

ARGUMENTS OUT
  hdl - on success we write the handle to the profiile into the ** passed
  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl     /* user handle for any cb */
)
{
  uint32 i;   /* index variable */
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    look at an idle stream 
  ------------------------------------------------------------------------*/
  for( i = 0;  i < mp4_profile_config.num_streams; i++ )
  {
    if( !qvp_rtp_mp4_array[ i ].valid )
    {
      qvp_rtp_mp4_array[ i ].valid = TRUE;
      *hdl = &qvp_rtp_mp4_array[ i ]; 
      
      /*--------------------------------------------------------------------
        copy the MP4 configuration to the  stream_ctx 
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( &qvp_rtp_mp4_array[i].stream_config,sizeof(qvp_rtp_mp4_config_type), &mp4_stream_config,
               sizeof( qvp_rtp_mp4_array[i].stream_config ));

      /*--------------------------------------------------------------------
        Initialize reassembly context for this stream ( FRAME mode )
        ( always open it since we dont know if jitter size at this stage )
      --------------------------------------------------------------------*/
      qvp_rtp_mp4_array[i].rx_ctx.reassem_hdl = NULL;
      
      return( QVP_RTP_SUCCESS );

    }
  } /* maximum no of streams */

  return( QVP_RTP_NORESOURCES );

} /* end of function qvp_rtp_mp4_profile_open */

/*===========================================================================

FUNCTION  QVP_RTP_MP4_PROFILE_RECV 


DESCRIPTION
   The function which the RTP framework will call upon arrival of data from 
   NW. RTP header is stripped but parameters are passed.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl           - hdl into the profile.
  usr_hdl       - handle to use when we call send function using the 
                  registered rx callback
  stream_param  - stream parameters applies to profiles with transport 
                  integrated. Its not planning to use the RTP.

  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * the profile formatting
                                              */

)
{
  uint16 au_hdrs_len;
  qvp_rtp_mp4_ctx_type *stream = ( qvp_rtp_mp4_ctx_type *) hdl; 
  qvp_rtp_mp4_au_param_type au_param;
  qvp_rtp_buf_type          *buf = NULL;
  uint8                     *au_data;
  uint8                     *au_hdr = pkt->data + pkt->head_room;
  uint16 rem_data_len = pkt->len;
  uint16 offset = 0;
  uint16 parsed_len;
  uint16 aux_data_len = 0;
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    If the module is not initialized bail out
  ------------------------------------------------------------------------*/
  if ( !mp4_initialized ) 
  {
      return( QVP_RTP_ERR_FATAL );
    
  }

  /*------------------------------------------------------------------------
    parse the AU headers length 

    Taken from RFC 3640
    "
    
    If the AU-header is not configured empty, then the AU-
   headers-length is a two octet field that specifies the length in bits
   of the immediately following AU-headers, excluding the padding bits. 

   "
  ------------------------------------------------------------------------*/
  if( stream->stream_config.rx_au_hdr_on )
  {
    
    au_hdrs_len = b_unpackw(pkt->data + pkt->head_room, offset, 16);
    offset += 16;
  }
  else
  {
    
      /*--------------------------------------------------------------------
          Receiving 3016 type payload is completely offloaded into a 
        different call flow. We will need to parse VOL header and all in
        the future.
      --------------------------------------------------------------------*/
     return( qvp_rtp_mp4_profile_recv_3016(hdl, usr_hdl, pkt ) );
  }

  /*------------------------------------------------------------------------
      Parse and skip the auxilliary length

      Per RFC 3640
      "

      auxiliary-data-size: specifies the length in bits of the immediately
            following auxiliary-data field;
      
         auxiliary-data: the auxiliary-data field contains data of a format
            not defined by this specification.

      "


      I am assuming length does not count for itself...
  ------------------------------------------------------------------------*/
  if( stream->stream_config.rx_aux_data_len ) 
  {
    aux_data_len = b_unpackw(pkt->data + pkt->head_room, offset, 
                           stream->stream_config.rx_aux_data_len); 

  }
  else
  {
    aux_data_len = 0;
  }

  /*------------------------------------------------------------------------
    Initialize start of data  
         +---------+-----------+-----------+---------------+
         | RTP     | AU Header | Auxiliary | Access Unit   |
         | Header  | Section   | Section   | Data Section  |
         +---------+-----------+-----------+---------------+
  ------------------------------------------------------------------------*/
  au_data = ( pkt->data + pkt->head_room + 
                ( ( au_hdrs_len  + 7 ) / 8 ) ) +  /* au headers section */
                ( ( aux_data_len + 7 ) / 8 ) /* aux data */ ;

  
  
  /*------------------------------------------------------------------------
     the ramaining data ( pay load ) in the packet is total len -
     the au_hdrs_len 

         +---------+-----------+-----------+---------------+
         | RTP     | AU Header | Auxiliary | Access Unit   |
         | Header  | Section   | Section   | Data Section  |
         +---------+-----------+-----------+---------------+

  ------------------------------------------------------------------------*/
  rem_data_len -= ( ( au_hdrs_len + 7 )  / 8 ) /* au headers */ + 
                  ( ( aux_data_len + 7 ) / 8 )  /* aux data section */;

  
  
  /*------------------------------------------------------------------------
      If we are parsing the au_hdr then we need to adjust the data
  ------------------------------------------------------------------------*/
  au_data += 2;
  rem_data_len -= 2;
  
  /*------------------------------------------------------------------------
     parse the first header 
     if there is au_hdr present then we do it
  ------------------------------------------------------------------------*/
  if( au_hdrs_len  != 0 )
  {
    
    parsed_len =  qvp_rtp_mp4_parse_au_hdr( stream, au_hdr, au_hdrs_len, 
                  &au_param, &offset );
  }
  else
  {
    au_param.au_index = 0;
    au_param.au_len = rem_data_len;
    au_param.cts = 0; 
    au_param.dts = 0; 
    au_param.rap = pkt->marker_bit; 
    parsed_len = 0;
  }
  
  if( (!parsed_len ) && ( au_hdrs_len != 0 ) )
  {
    if( pkt->need_tofree )
    {
      qvp_rtp_free_buf( pkt );
    }
    
    /*----------------------------------------------------------------------
      update the statistics counter
    ----------------------------------------------------------------------*/
    stream->rx_ctx.drop_cnt++;
    
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
     Check for bad formed paskets 
  ------------------------------------------------------------------------*/
  if( ( pkt->len * 8 ) < au_hdrs_len )
  {
    QVP_RTP_ERR( " Not enough data to parse \r\n", 0 , 0, 0 );
    
    /*----------------------------------------------------------------------
        Free the packet and return
    ----------------------------------------------------------------------*/
    if( pkt->need_tofree )
    {
      qvp_rtp_free_buf( pkt );
    }
    
    /*----------------------------------------------------------------------
      update the statistics counter
    ----------------------------------------------------------------------*/
    stream->rx_ctx.drop_cnt++;
    
    return( QVP_RTP_ERR_FATAL );
  }


  /*------------------------------------------------------------------------
    Pass the packet to next level if the stream is in fragment mode
    ---  We do reassembly based on timestamp & marker in JB --
  ------------------------------------------------------------------------*/
  if( stream->rx_ctx.output_mode == QVP_RTP_OUTPUT_FRAG_MODE )
  {
    return( mp4_profile_config.rx_cb( pkt, usr_hdl ) );
  }

  /*------------------------------------------------------------------------
       See if we are getting  an fragmented frame
  ------------------------------------------------------------------------*/
  if( au_param.au_len > rem_data_len ) 
  {
    /*----------------------------------------------------------------------
       Set the packet paramters so that they are ready to chain it    
    ----------------------------------------------------------------------*/
    pkt->head_room += ( ( au_hdrs_len + 
                          7 /* extra bits + padding */) / 8 ) + 2; 
    pkt->len = rem_data_len;


    /*----------------------------------------------------------------------
       If we have a reassmbly chain  and this is a new one lets abort the 
       existing one. We need to be smarter than this later we need to do
       some error concealed frame. For now lets abort
    ----------------------------------------------------------------------*/
    if( qvp_rtp_get_reasm_frame_length( stream->rx_ctx.reassem_hdl  ) )
    {
      if( stream->rx_ctx.curr_au_len != au_param.au_len )
      {
        qvp_rtp_abort_reassembly( stream->rx_ctx.reassem_hdl );
        stream->rx_ctx.curr_au_len = au_param.au_len;
      
        /*------------------------------------------------------------------
          update the statistics counter
        ------------------------------------------------------------------*/
        stream->rx_ctx.drop_cnt++;
      }
    }
    else
    {

      /*-------------------------------------------------------------------
        We are starting a new reassembly chain so lets reset the state and 
        start 
      --------------------------------------------------------------------*/
      qvp_rtp_reset_reassem_ctx( stream->rx_ctx.reassem_hdl );

      stream->rx_ctx.curr_au_len = au_param.au_len;
    }

    status = qvp_rtp_input_frag_in_reassembly( 
                                  stream->rx_ctx.reassem_hdl,
                                  pkt );

    /*------------------------------------------------------------------------
      check the reassembly array for complete frame 
    ------------------------------------------------------------------------*/
    if( ( status == QVP_RTP_SUCCESS ) && 
        ( au_param.au_len <= 
          qvp_rtp_get_reasm_frame_length( stream->rx_ctx.reassem_hdl  ) ) )
    {
      /*----------------------------------------------------------------------
       regroup the chain in one VIDEO buffer 
       allocate a buffer to the list
      ----------------------------------------------------------------------*/
      buf = qvp_rtp_alloc_buf( QVP_RTP_POOL_VIDEO );

      /*----------------------------------------------------------------------
           Bail out if we do not have a video buffer 
      ----------------------------------------------------------------------*/
      if( buf )
      {
        buf->head_room = 0;
        buf->seq = pkt->seq;
        buf->tstamp = pkt->tstamp + au_param.cts;
        buf->marker_bit = pkt->marker_bit;
        buf->len = au_param.au_len;
        buf->need_tofree = TRUE;
      }
      else
      {
        status = QVP_RTP_NORESOURCES;
      }
      
      if( status == QVP_RTP_SUCCESS )
      {
        status = qvp_rtp_get_reassembled_frame( stream->rx_ctx.reassem_hdl,
                                                buf, QVP_RTP_PKT_VIDEO_SIZE,
                                                stream->rx_ctx.last_seq_sent );
        qvp_rtp_abort_reassembly( stream->rx_ctx.reassem_hdl );
      }
      /*--------------------------------------------------------------------
          pass the buffer back to the application 
      --------------------------------------------------------------------*/
      if( status == QVP_RTP_SUCCESS )
      {
        qvp_rtp_reset_reassem_ctx( stream->rx_ctx.reassem_hdl );
        
        mp4_profile_config.rx_cb( buf, usr_hdl );
      } 

    } /* if complete frame */

    if( status != QVP_RTP_SUCCESS )
    {
       qvp_rtp_abort_reassembly( stream->rx_ctx.reassem_hdl );
       if( buf && buf->need_tofree )
       {
         qvp_rtp_free_buf( buf );
       }
       return( status );
    }

  } /* If fragmented frame */
  else
  {

    /*----------------------------------------------------------------------
      If we are sitting too long on a reassemby lets abort it 
    ----------------------------------------------------------------------*/
    if( qvp_rtp_get_reasm_frame_length( stream->rx_ctx.reassem_hdl  ) )
    {

        if( ( pkt->seq - stream->rx_ctx.last_seq )
            > QVP_RTP_MAX_REASS_WAIT )
        {
          
          /*----------------------------------------------------------------
            update the statistics counter
          ----------------------------------------------------------------*/
          stream->rx_ctx.drop_cnt++;
          qvp_rtp_abort_reassembly( stream->rx_ctx.reassem_hdl );
        }
    }


    /*----------------------------------------------------------------------
      iteratively parse all the AUs 

      no interleaving assumed

      if we have the min header len continue.
    ----------------------------------------------------------------------*/
    do
    {

      /*--------------------------------------------------------------------
         Are we still sane here ..? Data should match up the headers 
      --------------------------------------------------------------------*/
      if( rem_data_len < au_param.au_len )
      {
        
        /*------------------------------------------------------------------
            Free the packet and return
        ------------------------------------------------------------------*/
        if( pkt->need_tofree )
        {
          qvp_rtp_free_buf( pkt );
        }
        
        QVP_RTP_ERR( "Not enough data in buffer parsing error\r\n", 0, 0, 
                     0 );
        
        /*------------------------------------------------------------------
          update the statistics counter
        ------------------------------------------------------------------*/
        stream->rx_ctx.drop_cnt++;
        
        return( QVP_RTP_ERR_FATAL );
      }

      /*--------------------------------------------------------------------
         If this is the last segment. Reuse the existing buffer passed from 
         n/w 
      --------------------------------------------------------------------*/
      if( au_param.au_len == rem_data_len   )
      {
        pkt->head_room = au_data /* place where AU points */
                         - pkt->data /* place where data starts */;  

        /*------------------------------------------------------------------
             adjust packet variable 
        ------------------------------------------------------------------*/
        pkt->marker_bit = au_param.rap;
        pkt->tstamp = pkt->tstamp + au_param.cts; 
        pkt->len = au_param.au_len;

        /*------------------------------------------------------------------
           ship this packet to RTP profile user 
        ------------------------------------------------------------------*/
        mp4_profile_config.rx_cb( pkt, usr_hdl );

        /*------------------------------------------------------------------
            This is the last AU so break out of the loop 
        ------------------------------------------------------------------*/
        break;
        
      }



      if( au_param.au_len > QVP_RTP_PKT_VIDEO_SIZE )
      {
        QVP_RTP_ERR( " The MP3 pay load bigger than max \r\n", 
          0, 0, 0 );
        
        /*------------------------------------------------------------------
            Free the packet and return
        ------------------------------------------------------------------*/
        if( pkt->need_tofree )
        {
          qvp_rtp_free_buf( pkt );
        }
        
        /*------------------------------------------------------------------
          update the statistics counter
        ------------------------------------------------------------------*/
        stream->rx_ctx.drop_cnt++;
        
        return( QVP_RTP_ERR_FATAL );

      }
      /*--------------------------------------------------------------------
         Read AU data and dispatch it to app_call _back 
      --------------------------------------------------------------------*/
      buf = qvp_rtp_alloc_buf( QVP_RTP_POOL_VIDEO );

      /*--------------------------------------------------------------------
          Se if we got a free buffer 
      --------------------------------------------------------------------*/
      if( !buf )
      {
        
        /*------------------------------------------------------------------
            Free the packet and return
        ------------------------------------------------------------------*/
        if( pkt->need_tofree )
        {
          qvp_rtp_free_buf( pkt );
        }
        
        /*------------------------------------------------------------------
          update the statistics counter
        ------------------------------------------------------------------*/
        stream->rx_ctx.drop_cnt++;
        
        QVP_RTP_ERR( "Could not find a buffer \r\n", 0, 0, 0 );
        return( QVP_RTP_ERR_FATAL );
      }


      buf->marker_bit = au_param.rap;
      buf->tstamp = pkt->tstamp + au_param.cts; 
      buf->len = au_param.au_len;
      buf->need_tofree = TRUE;

      /*--------------------------------------------------------------------
         Copy this AU to the buffer 
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( buf->data, au_param.au_len,au_data, au_param.au_len );

   
      /*--------------------------------------------------------------------
         ship this packet to RTP profile user 
      --------------------------------------------------------------------*/
      mp4_profile_config.rx_cb( buf, usr_hdl );

      
      /*--------------------------------------------------------------------
        Now that we fully parsed and packaged this au.
        lets decrement the remaining heades by the length we parsed
      --------------------------------------------------------------------*/
      au_hdrs_len -= parsed_len;
      
      /*--------------------------------------------------------------------
         parse the next header 
      --------------------------------------------------------------------*/
      if( au_hdrs_len )
      {
        parsed_len =  qvp_rtp_mp4_parse_au_hdr( stream, au_hdr, au_hdrs_len, 
                        &au_param, &offset );
        if(! parsed_len )
        {
          
          /*----------------------------------------------------------------
              Free the packet and return
          ----------------------------------------------------------------*/
          if( pkt->need_tofree )
          {
            qvp_rtp_free_buf( pkt );
          }
          
          /*----------------------------------------------------------------
            update the statistics counter
          ----------------------------------------------------------------*/
          stream->rx_ctx.drop_cnt++;
          
          return( QVP_RTP_ERR_FATAL );
        }

      }
      else
      {
        break;
      }

      
      /*--------------------------------------------------------------------
          AU Index will be non zero only for interleaved AUs
      --------------------------------------------------------------------*/
      if( au_param.au_index )
      {
        /*------------------------------------------------------------------
          We are getting interleaved frame. 
          Currently do not support
          We will soon support this
        ------------------------------------------------------------------*/
        
        /*------------------------------------------------------------------
            Free the packet and return
        ------------------------------------------------------------------*/
        if( pkt->need_tofree )
        {
          qvp_rtp_free_buf( pkt );
        }
        
        /*------------------------------------------------------------------
          update the statistics counter
        ------------------------------------------------------------------*/
        stream->rx_ctx.drop_cnt++;
        
        return( QVP_RTP_ERR_FATAL );

      }

      /*--------------------------------------------------------------------
        We parsed one more AU reduce the data len 
      --------------------------------------------------------------------*/
      rem_data_len -= au_param.au_len;
      au_data      += au_param.au_len;


    }while( au_hdrs_len > 
          stream->stream_config.rx_AU_size_len /* min auhdr len */+
          stream->stream_config.rx_aux_data_len /* aux data len */ );

  } /* end of else */

  return( status );
} /* end of qvp_rtp_mp4_profile_recv */

/*===========================================================================

FUNCTION  QVP_RTP_MP4_PROFILE_RECV_316


DESCRIPTION
   This function will handle parsing and reassembly of 3016 based format. 
   This aligns to 3640 when the configuration of the header is turned OFF.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl           - hdl into the profile.
  usr_hdl       - handle to use when we call send function using the 
                  registered rx callback
  stream_param  - stream parameters applies to profiles with transport 
                  integrated. Its not planning to use the RTP.

  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_recv_3016
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * the profile formatting
                                              */

)
{
  qvp_rtp_mp4_ctx_type *stream = ( qvp_rtp_mp4_ctx_type *) hdl;
  qvp_rtp_status_type status          = QVP_RTP_SUCCESS;
  qvp_rtp_buf_type        *app_buf    = NULL;
/*------------------------------------------------------------------------*/

  QVP_RTP_MSG_MED( "qvp_rtp_mp4_profile_recv_3016: SEQ=%d, TS=%lu, Mark=%d",
                pkt->seq, pkt->tstamp, pkt->marker_bit );

  /*------------------------------------------------------------------------
    Pass the packet to next level if the stream is in fragment mode
  ------------------------------------------------------------------------*/
  if( stream->rx_ctx.output_mode == QVP_RTP_OUTPUT_FRAG_MODE )
  {
    stream->rx_ctx.last_ts = pkt->tstamp;
    stream->rx_ctx.last_seq = stream->rx_ctx.last_seq_sent = pkt->seq;

    return( mp4_profile_config.rx_cb( pkt, usr_hdl ) );

  }

  /*------------------------------------------------------------------------
    Check for timestamp wrap around and Seq no wrap around
    If it doesnt fall in either of these cases its a delayed packet.
  ------------------------------------------------------------------------*/
  if( ( ( pkt->tstamp < stream->rx_ctx.last_ts ) && 
        ( stream->rx_ctx.last_ts - pkt->tstamp < 
                 QVP_RTP_MP4_TS_WRAP_LIMIT )  /* !( TS wrap around )*/
      )  ||
      ( ( pkt->seq < stream->rx_ctx.last_seq_sent ) &&
        ( stream->rx_ctx.last_seq_sent - pkt->seq <
               ( QVP_RTP_POOL_NW_SIZE * 10 ) ) /* !( Seqno wrap around )*/
      ) 
    )
  {
    /*----------------------------------------------------------------------
      update the statistics counter
    ----------------------------------------------------------------------*/
    stream->rx_ctx.drop_cnt++;

    QVP_RTP_MSG_MED_2( "Delayed Mp4 packet dropped. Last SEQ: %d, TS: %d",
               stream->rx_ctx.last_seq_sent , stream->rx_ctx.last_ts, 0 );

    /*----------------------------------------------------------------------
       Drop the packet for now, Cleanup and return success
    ----------------------------------------------------------------------*/
    if( pkt->need_tofree )
    {
      qvp_rtp_free_buf( pkt );
    }
    
    return( QVP_RTP_SUCCESS );

  }

  /*------------------------------------------------------------------------
      In 3016 the marker bit will flag a complete frame or part of the 
      frame or fragment

      1) is this a fragment ..?
      
      2) is this really an end of frame ( most possible while we are
      reassembling.
      
      3) We need to try and confirm this by try and parsing the first few 
      bytes. A layer violation but this is the only way we can get 
      a robust 3016 receiver.
  ------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
      Check if it belongs to the same frame or
       if we are starting a new reassembly
          Insert the packet received in the reassembly chain, 
  ------------------------------------------------------------------------*/
  if( ( pkt->tstamp == stream->rx_ctx.last_ts ) ||
      ( stream->rx_ctx.rx_state == QVP_RTP_MP4_RX_IDLE ) )
  {
    QVP_RTP_MSG_LOW_1( "Insert pkt in reassembly TS %d", pkt->tstamp);
    status = qvp_rtp_input_frag_in_reassembly( stream->rx_ctx.reassem_hdl, 
                                               pkt );

    stream->rx_ctx.rx_state = QVP_RTP_MP4_RX_REASSMBLING;
    stream->rx_ctx.last_ts = pkt->tstamp;

  }
  
  /*------------------------------------------------------------------------
    Check if we reached end of frame - 
    If the marker bit of the packet is set. 
    If packet timestamp changes from currently reassembly timestamp
    Then Regroup the current reassembly 
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS || status == QVP_RTP_NORESOURCES ) &&
      ( ( pkt->marker_bit ) ||
        ( ( stream->rx_ctx.rx_state == QVP_RTP_MP4_RX_REASSMBLING ) && 
          ( pkt->tstamp != stream->rx_ctx.last_ts ) ) ) )
  {

    /*----------------------------------------------------------------------
       Update all statistcs before aborting the reassembly
    ----------------------------------------------------------------------*/
    stream->rx_ctx.drop_cnt += 
           qvp_rtp_get_reasm_drop_cnt( stream->rx_ctx.reassem_hdl );

    stream->rx_ctx.missing_pkt_cnt += 
           qvp_rtp_get_reasm_missng_cnt( stream->rx_ctx.reassem_hdl );


    /*----------------------------------------------------------------------
        regroup the chain in one VIDEO buffer 
        allocate a buffer to the list
    ----------------------------------------------------------------------*/
    app_buf = qvp_rtp_alloc_buf( QVP_RTP_POOL_VIDEO );

    if( app_buf )
    {
      QVP_RTP_MSG_LOW_1( "Get MP4 frame TS %d", stream->rx_ctx.last_ts);
      status = qvp_rtp_get_reassembled_frame( stream->rx_ctx.reassem_hdl,
                                            app_buf, QVP_RTP_PKT_VIDEO_SIZE,
                                            stream->rx_ctx.last_seq_sent );

      if( ( status != QVP_RTP_SUCCESS ) && ( app_buf->need_tofree ) )
      {
         qvp_rtp_free_buf( app_buf );
         app_buf = NULL;
      }
    }
    else
    {
      status = QVP_RTP_NORESOURCES;

    } /* if no app_buf */

    stream->rx_ctx.last_seq_sent = 
             qvp_rtp_get_reasm_last_seq( stream->rx_ctx.reassem_hdl );
    
    /*--------------------------------------------------------------------
         Do not abort the reassembly here since there is no need to start a new 
         reassembly chain. 
         Hoping that we can recover from previous errors we will continue here 
         by sending a NULL frame to application
    --------------------------------------------------------------------*/
    if( ( status != QVP_RTP_SUCCESS ) && 
         ( pkt->tstamp == stream->rx_ctx.last_ts ) )
    {
      QVP_RTP_ERR( "Ignore error, sending NULL frame at TS %d ",\
                   stream->rx_ctx.last_ts,0,0 );
      return( mp4_profile_config.rx_cb( app_buf, usr_hdl ) );
    }

    /*----------------------------------------------------------------------
       If we reach here, ignore other errors and move to next frame 
    ----------------------------------------------------------------------*/
    status = QVP_RTP_SUCCESS;

    /*----------------------------------------------------------------------
      Check if we have to start a new reassembly for next frame
      Then input the packet in a new reassembly chain
    ----------------------------------------------------------------------*/
    if( pkt->tstamp != stream->rx_ctx.last_ts )
    {
      /*--------------------------------------------------------------------
         Stop any previous reassembly here
      --------------------------------------------------------------------*/
      qvp_rtp_abort_reassembly( stream->rx_ctx.reassem_hdl );

      QVP_RTP_MSG_LOW_1( "Insert pkt in reassembly TS %d", pkt->tstamp);
      stream->rx_ctx.last_ts = pkt->tstamp;
      stream->rx_ctx.rx_state = QVP_RTP_MP4_RX_REASSMBLING;
      status = qvp_rtp_input_frag_in_reassembly( stream->rx_ctx.reassem_hdl, 
                                                 pkt );
    }

  }  

  /*------------------------------------------------------------------------
    Update receiver context 
  ------------------------------------------------------------------------*/  
  stream->rx_ctx.last_ts = pkt->tstamp;

  /*------------------------------------------------------------------------
    Return for any fatal errors here, other errors like NORESOURCES can be skipped
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS && 
      status != QVP_RTP_NORESOURCES )
  {
    return( status );
  }
  
  /*------------------------------------------------------------------------
    If we formed a frame send it to application
  ------------------------------------------------------------------------*/
  if( app_buf )
  {
    QVP_RTP_MSG_MED_1( "MP4 frame TS %d", stream->rx_ctx.last_ts);

    /*----------------------------------------------------------------------
       Update the state so that new frame can be started with next pkt
    ----------------------------------------------------------------------*/
    stream->rx_ctx.rx_state = QVP_RTP_MP4_RX_IDLE;
    qvp_rtp_abort_reassembly( stream->rx_ctx.reassem_hdl );
    return( mp4_profile_config.rx_cb( app_buf, usr_hdl ) );
  }

   return( QVP_RTP_SUCCESS );

  
} /* qvp_rtp_mp4_profile_recv_3016 */
/*===========================================================================

FUNCTION  QVP_RTP_MP4_PROFILE_CLOSE


DESCRIPTION
  Closes an already open bi directional channel inside the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl - handle of channel to close.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

)
{
  qvp_rtp_mp4_ctx_type *stream = ( qvp_rtp_mp4_ctx_type *) hdl; 
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If the module is not initialized bail out
  ------------------------------------------------------------------------*/
  if ( !mp4_initialized ) 
  {
      return( QVP_RTP_ERR_FATAL );
    
  }

  /*------------------------------------------------------------------------
    If we get  a valid hadle invalidate the index 
  ------------------------------------------------------------------------*/
  if( hdl )
  {
    /*----------------------------------------------------------------------
        We cannot afford to loose buffers so when we are in the middle of
        reassmbling we need to free the whole chain
    ----------------------------------------------------------------------*/
    qvp_rtp_close_reassem_ctx( stream->rx_ctx.reassem_hdl );
    stream->valid = FALSE;
  }
  return( QVP_RTP_SUCCESS );

} /* end of function qvp_rtp_mp4_profile_close */ 


/*===========================================================================

FUNCTION QVP_RTP_MP4_PROFILE_VALIDATE_CONFIG 


DESCRIPTION

  This function checks the configuration of MP4 specified in the input 
  against the valid fmtp values for MP4

DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute is valid, counter_config is not modified
                   If the attribute  is invalid,
                   counter_config is updated with proposed value

RETURN VALUE
  QVP_RTP_SUCCESS  - The given Configuration is  valid for the given payload,
                     RTP configure will succeed for these settings
                     
  QVP_RTP_ERR_FATAL   - The given Configuration is not valid for the given 
                        payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_validate_config_rx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if ( ( !payld_config ) || ( !counter_config ) )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Validating rx configuration
   -----------------------------------------------------------------------*/
  if ( ( payld_config->config_rx_params.valid ) && 
       ( payld_config->config_rx_params.
             config_rx_payld_params.mp4_es_rx_config.valid ) )
  {
    if ( ( payld_config->ch_dir == QVP_RTP_CHANNEL_INBOUND ) || 
         ( payld_config->ch_dir == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
    {
      /*--------------------------------------------------------------------
        Validate ptime. Throw an error if we cant receive packets with 
        the ptime proposed.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_rx_params.rx_rtp_param.ptime_valid ) && 
           ( payld_config->config_rx_params.rx_rtp_param.ptime != 
                      QVP_RTP_MP4_PKT_INTVL ) )
      {
        counter_config->config_rx_params.rx_rtp_param.ptime = 
                     QVP_RTP_MP4_PKT_INTVL;
        status = QVP_RTP_ERR_FATAL;
      }

    } /* end of if ( ( payld_config->ch_dir */
    else
    {
      QVP_RTP_ERR ( " Attempting to config rx for an o/b only ch", 0, 0, 0);
      status = QVP_RTP_ERR_FATAL ;
    }

  } /* end of   if ( ( payld_config->config_rx_params.valid )*/
  
  return ( status ); 


}/* end of qvp_rtp_mp4_profile_validate_config_rx */


/*===========================================================================

FUNCTION QVP_RTP_MP4_PROFILE_VALIDATE_CONFIG_TX 


DESCRIPTION

  This function checks the configuration of MP4 specified in the input 
  against the valid fmtp values for MP4

DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute is valid, counter_config is not modified
                   If the attribute  is invalid, 
                   counter_config is updated with proposed value


RETURN VALUE
  QVP_RTP_SUCCESS  - The given Configuration is  valid for the given payload,
                     RTP configure will succeed for these settings
  QVP_RTP_ERR_FATAL   - The given Configuration is not valid for the given 
                        payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_validate_config_tx
(
  qvp_rtp_payload_config_type*  payld_config,
  qvp_rtp_payload_config_type*  counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Validating tx configuration
   -----------------------------------------------------------------------*/
  if ( ( payld_config->config_tx_params.valid ) && 
       ( payld_config->config_tx_params.
             config_tx_payld_params.mp4_es_tx_config.valid ) )
  {
    if ( ( payld_config->ch_dir == QVP_RTP_CHANNEL_OUTBOUND ) || 
           ( payld_config->ch_dir == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
    {
      /*--------------------------------------------------------------------
        Validate ptime. Throw an error if peer wants us to send 
        packets with ptime lesser than our minimum supported ptime.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_tx_params.tx_rtp_param.ptime_valid ) && 
           ( payld_config->config_tx_params.tx_rtp_param.ptime < 
                      QVP_RTP_MP4_PKT_INTVL ) )
      {
        counter_config->config_tx_params.tx_rtp_param.ptime = 
                     QVP_RTP_MP4_PKT_INTVL;
        status = QVP_RTP_ERR_FATAL;
      }

    }/* end of if ( ( payld_config->ch_dir */
    else
    {
      QVP_RTP_ERR ( " Attempting to config tx for an i/b only ch", 0, 0, 0);
      status = QVP_RTP_ERR_FATAL ;
    }
  } /* end of if ( ( payld_config->config_tx_params.valid )*/
  
  return ( status ); 

}/* end of qvp_rtp_mp4_profile_validate_config_tx */


/*===========================================================================

FUNCTION QVP_RTP_MP4_PROFILE_READ_DEFLT_CONFIG 


DESCRIPTION

  This function reads out the default configuration of MP4
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is not used by MP4. Some 
                     other profiles uses it as they support multiple 
                     payload formats.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_read_deflt_config
(
  
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
)
{

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {

    return( QVP_RTP_ERR_FATAL );
  }


  payld_config->payload = payld;

  return( qvp_rtp_mp4_profile_copy_config( payld_config, 
                                            &mp4_stream_config  ) );



} /* end of function qvp_rtp_mp4_profile_read_deflt_config */ 



/*===========================================================================

FUNCTION QVP_RTP_MP4_PROFILE_CONFIGURE

DESCRIPTION

  This function payload configuration of a previously opened MP4  
  channel.

DEPENDENCIES
  None

ARGUMENTS IN
    hdl            - handle to the opend channel
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
)
{
  qvp_rtp_mp4_ctx_type *stream = ( qvp_rtp_mp4_ctx_type *) hdl; 
  qvp_rtp_mp4_config_type prev_config;
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/



  /*------------------------------------------------------------------------
                Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !mp4_initialized || !stream || !stream->valid || 
      !payld_config ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Backup the current configuration before we proceed from here.
  ------------------------------------------------------------------------*/
  prev_config = stream->stream_config;
  
  /*------------------------------------------------------------------------
      Configuration steps.

      1) Try and configure RX.
         If RX fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model as 
          given in RFC3264.
      2) Try and configure TX
         If TX  configure fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model 
          as given in RFC3264.
  ------------------------------------------------------------------------*/
  if( payld_config->config_rx_params.valid )
  {
    status = qvp_rtp_mp4_profile_config_rx_param( stream, payld_config );
  }


  /*------------------------------------------------------------------------
      Try and configure Tx part now....
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS )&&
      payld_config->config_tx_params.valid )
  {
    status = qvp_rtp_mp4_profile_config_tx_param(stream, payld_config );
  }
  /*------------------------------------------------------------------------
    Reassembly ctx not required for outbound stream.
  ------------------------------------------------------------------------*/
  if ( ( status == QVP_RTP_SUCCESS ) &&
      ( payld_config->chdir_valid == TRUE ) && 
      ( payld_config->ch_dir == QVP_RTP_CHANNEL_OUTBOUND ) &&
      ( stream->rx_ctx.reassem_hdl ) )
  {

      QVP_RTP_MSG_LOW_0( "Closing reassembly contxt not reqd");
      qvp_rtp_close_reassem_ctx( stream->rx_ctx.reassem_hdl ); 

  }
  /*------------------------------------------------------------------------
    If we did not succeed to a struct to reset to preivous value and 
    return failure
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS )
  {
    stream->stream_config = prev_config ;
  }
  
  return( status );

} /* end of function qvp_rtp_mp4_profile_configure */

/*===========================================================================

FUNCTION  QVP_RTP_MP4_STUFF_SINGLE_AU_HEADER 


DESCRIPTION
      This function will form a single single AU header for the  packaging
      of generic MP4 profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl - handle of channel to close.
  pkt - packet strucutre where this header need to be stuffed in
  au_len - length of AU which will be added in the pay load
  marker - I-Frame/ silence on/off etc.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type  qvp_rtp_mp4_stuff_single_au_header
(
  qvp_rtp_profile_hdl_type   hdl,      /* handle created while open */
  qvp_rtp_buf_type           *pkt,     /* packet with head room */
  uint16                     au_len,   /* length of AU */
  boolean                    marker    /* marker bit */
)
{
  qvp_rtp_mp4_ctx_type *stream = ( qvp_rtp_mp4_ctx_type *) hdl; 
  uint8 header[QVP_RTP_MP4_SINGLE_AU_HDR_SZ];
  uint16 offset  = 0;
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
      If we have not negotiated the au_hdr_ln no au hdr is stuck in
  ------------------------------------------------------------------------*/
  if( !stream->stream_config.tx_au_hdr_on )
  {
    return( QVP_RTP_SUCCESS );
  }

  /*------------------------------------------------------------------------
    If we did not get the stream bail out 
  ------------------------------------------------------------------------*/
  if( !hdl )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    pack the AU header with size = 1
    
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-
      |AU-headers-length|AU-header|
      |                 |   (1)   |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  ------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Reset the header back to zero first 
  ------------------------------------------------------------------------*/
  memset( header, 0, sizeof( header ) );



  /*------------------------------------------------------------------------
     We will form the AU headers len in the end lets leave the size for it
     and go forward. 
  ------------------------------------------------------------------------*/
  offset += 16;


  /*------------------------------------------------------------------------
     NW byte order and into the header 
  ------------------------------------------------------------------------*/
  /*------------------------------------------------------------------------
    Pack the header fields
    AU header len
    and then add offset to advance for next field.
  ------------------------------------------------------------------------*/
  b_packw( au_len,
           header, 
           offset, 
           stream->stream_config.tx_AU_size_len);
  offset += stream->stream_config.tx_AU_size_len;

  /*------------------------------------------------------------------------
    Pack the header fields
    AU index field  
    and then add offset to advance for next field.
  ------------------------------------------------------------------------*/
  b_packw( (uint16) stream->tx_ctx.tx_idx,
           header, 
           offset, 
           stream->stream_config.tx_index_len );
  offset += stream->stream_config.tx_index_len;
  /*------------------------------------------------------------------------
    inrement index  
  ------------------------------------------------------------------------*/
  stream->tx_ctx.tx_idx++;

  /*------------------------------------------------------------------------
     If we negotiated for CTS we will flag it FALSE
  ------------------------------------------------------------------------*/
  if( stream->stream_config.tx_CTS_size )
  {
  
    /*----------------------------------------------------------------------
      We do not encode CTS this is only for contatenated PDUs
    ----------------------------------------------------------------------*/
    b_packb( 0, header, offset, 1 );
    offset += 1;
  }

  /*------------------------------------------------------------------------
    If configured to send DTS we will flag it FALSE 
  ------------------------------------------------------------------------*/
  if( stream->stream_config.tx_DTS_size )
  {

    /*----------------------------------------------------------------------
      We will never ever send this out.
    ----------------------------------------------------------------------*/
    b_packb( 0,
              header,
              offset,
              1 );
    offset += 1;
  }


  /*------------------------------------------------------------------------
    Per Jun Marker can be same as RAP flag. We need to honor it.
    So bit pack the booloen. 
  ------------------------------------------------------------------------*/
  if( marker )
  {
    b_packb( 1,
              header,
              offset,
              1 );
    
  }
  else
  {
    b_packb( 0,
              header,
              offset,
              1 );
  }
  offset += 1;

  /*------------------------------------------------------------------------
     If this is not byte aligned we need to make with padding zeros. 

     THIS CODE NEED TO BE MOVED OUT FOR THE CONCATENATED PDUS.
  ------------------------------------------------------------------------*/
  if( offset % 8 )
  {
    b_packb( 0, 
              header,
              offset,
             ( 8 - ( offset % 8 ) ) );
  }


  /*------------------------------------------------------------------------
    If we do not have enough head room cry and exit 
  ------------------------------------------------------------------------*/
  if( pkt->head_room < ( ( ( offset + 7 ) / 8 ) ) )
  {
    QVP_RTP_ERR( " No head room for header ", 
                                  0, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
                  
  }
  else

  {
    /*----------------------------------------------------------------------
      offset is the AU headers len 
      NW byte order not including the au_hdrs_len field itself.

      Per RFC 3640
      
      "
      if the AU-header is not configured empty, then the AU-
      headers-length is a two octet field that specifies the length in bits
      of the immediately following AU-headers, excluding the padding bits.
      "
    ----------------------------------------------------------------------*/
    header[0] = (uint8) (( offset - 16 /* hdr_len field */ ) & 0xff00);
    header[1] = ( offset - 16 /* hdr_len field */ ) & 0x00ff;
  }

  
  /*------------------------------------------------------------------------
     Recalculate offset in terms of no of bytes 
  ------------------------------------------------------------------------*/
  offset = (offset + 7 ) / 8;
  
  /*------------------------------------------------------------------------
    Copy the header we created to the data offset. 
  ------------------------------------------------------------------------*/
  if(offset > QVP_RTP_MP4_SINGLE_AU_HDR_SZ)
    offset = QVP_RTP_MP4_SINGLE_AU_HDR_SZ;
  qvp_rtp_memscpy( ( (uint8 *) pkt->data + pkt->head_room - offset ) ,offset, 
            &header[0], offset );

  /*------------------------------------------------------------------------
    We need to reduce head room 
  ------------------------------------------------------------------------*/
  pkt->head_room -= offset;

  /*------------------------------------------------------------------------
    Need to update the packet length 
  ------------------------------------------------------------------------*/
  pkt->len += offset;

  return( QVP_RTP_SUCCESS );

} /* end of function qvp_rtp_mp4_stuff_single_au_header */


/*===========================================================================

FUNCTION  QVP_RTP_MP4_FRAGMENT_AND_SEND 


DESCRIPTION
  This function will fragment a large AU into small pieces for 
  MTU requirement of the N/W stack. Also IP fragmentation is not a 
  good thing. We need to be smarted here to align it to encoder slices
  and so on.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl - handle of channel to close.
  usr_hdl - user context for calling txmit function
  pkt - packet strucutre where this header need to be stuffed in

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_fragment_and_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
)
{
  uint16 len_rem = pkt->len;
  qvp_rtp_buf_type  *nw_buf = NULL;
  uint16 len_to_send = 0;
  boolean rap_flag = FALSE;
  boolean first_fragment = TRUE;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
     If handle is bad return 
  ------------------------------------------------------------------------*/
  if( !hdl )
  {
    return( QVP_RTP_ERR_FATAL );
  }


  /*------------------------------------------------------------------------
      fragment and send packets in a loop 
  ------------------------------------------------------------------------*/
  while( len_rem )
  {

    /*----------------------------------------------------------------------
      length which can be sent is the minimum of the fragment size and 
      length remaining. Take the AU header size into account. 
    ----------------------------------------------------------------------*/
    len_to_send = ( len_rem > ( mp4_profile_config.rtp_mtu - 
                   QVP_RTP_MP4_SINGLE_AU_HDR_SZ ) ) ? 
                   mp4_profile_config.rtp_mtu - 
                   QVP_RTP_MP4_SINGLE_AU_HDR_SZ : len_rem;

    /*----------------------------------------------------------------------
       Set the marker to one when we reach the last fragment.

       Taken from  RFC3640

       "
        Marker (M) bit: The M bit is set to 1 to indicate that the RTP 
        packet payload contains either the final fragment of a fragmented 
        Access Unit or one or more complete Access Units.
      "
    ----------------------------------------------------------------------*/
    if( len_rem == len_to_send )
    {
      pkt->marker_bit = TRUE;
      pkt->len = len_to_send;
      /*--------------------------------------------------------------------
        For the last fragment we will not use a different buffer we will 
        use the existing one.
      --------------------------------------------------------------------*/
      nw_buf = pkt;
    }
    else
    {
      
      
      /*--------------------------------------------------------------------
         Allocate buffers by length
      --------------------------------------------------------------------*/
      nw_buf = qvp_rtp_alloc_buf_by_len( len_to_send + QVP_RTP_HEAD_ROOM 
                                         + QVP_RTP_MP4_SINGLE_AU_HDR_SZ );
      
      /*--------------------------------------------------------------------
        If we could not allocate packet just bail out
      --------------------------------------------------------------------*/
      if( !nw_buf )
      {
        qvp_rtp_free_buf( pkt );
        QVP_RTP_ERR( "Could not allocate the buffer ", 0, 0, 0 );
        return( QVP_RTP_ERR_FATAL );
      }
      else
      {
        nw_buf->head_room = QVP_RTP_HEAD_ROOM + 
                            QVP_RTP_MP4_SINGLE_AU_HDR_SZ;
        qvp_rtp_memscpy( nw_buf->data + nw_buf->head_room, len_to_send,pkt->data + 
                pkt->head_room, len_to_send ); 


        /*------------------------------------------------------------------
          set up the network buffer
        ------------------------------------------------------------------*/
        nw_buf->tstamp  = pkt->tstamp;
        nw_buf->marker_bit = FALSE;
        nw_buf->len = len_to_send;
        nw_buf->rtp_pyld_type = pkt->rtp_pyld_type;
      }
    }


   /*-----------------------------------------------------------------------
      If this is first fragment and we have RAP just flag it accordingly 
   -----------------------------------------------------------------------*/
   if( first_fragment )
   {
     if( pkt->marker_bit /* relate to RAP in MP4 */)
     {
       rap_flag = TRUE;  
     }
     first_fragment = FALSE;
   }
   else
   {
     rap_flag = FALSE;  
   }


    /*----------------------------------------------------------------------
         Add the header. If the header is not negotiated it returns
         right away.
    ----------------------------------------------------------------------*/
    if( qvp_rtp_mp4_stuff_single_au_header(hdl, nw_buf, 
              len_to_send, rap_flag ) != QVP_RTP_SUCCESS )
    {

      /*--------------------------------------------------------------------
        We need to free the  buffer for this fragment and buffer for the
        entire packet
      --------------------------------------------------------------------*/
      qvp_rtp_free_buf( nw_buf );
      if( nw_buf != pkt )
      {
        qvp_rtp_free_buf( pkt );
      }
      return( QVP_RTP_ERR_FATAL );
    }
    
    QVP_RTP_MSG_MED( " Sending MP4 Len = %d, tstamp = %d, marker = %d",
                     nw_buf->len, nw_buf->tstamp, nw_buf->marker_bit );

    /*----------------------------------------------------------------------
           ship this fragment 
    ----------------------------------------------------------------------*/
    if( mp4_profile_config.tx_cb( nw_buf, usr_hdl  ) != QVP_RTP_SUCCESS )
    {

      /*--------------------------------------------------------------------
        if this is not the last segment we need to free the orignal buffer
        passed. This is because callee frees the buffer passed. If this 
        is the last fragment we do not free anything else we need to free 
        the packet buffer passed.
      --------------------------------------------------------------------*/
      if( nw_buf != pkt )
      {
        if( nw_buf->need_tofree )
        {
          qvp_rtp_free_buf( nw_buf );
        }
      }
      if( pkt->need_tofree )
      {
        qvp_rtp_free_buf( pkt );
      }
      return( QVP_RTP_ERR_FATAL );
    }

    /*----------------------------------------------------------------------
                Adjust the len_rem to the new value. 
    ----------------------------------------------------------------------*/
    len_rem -= len_to_send;

    /*----------------------------------------------------------------------
      update the head room if this is not the last packet

      If we update in case of last packet we will mangle packet which is 
      just scheduled into the network sub layer.
    ----------------------------------------------------------------------*/
    if( nw_buf != pkt )
    {
      pkt->head_room += len_to_send;
    }


  } /* end of while len_rem */

  return( QVP_RTP_SUCCESS );

} /* end of function qvp_rtp_mp4_fragment_and_send */

/*===========================================================================

FUNCTION  QVP_RTP_MP4_PARSE_AU_HDR 


DESCRIPTION
  This function will parse a full AU header and pack the information 
  into the au_param structure.

DEPENDENCIES
  None

ARGUMENTS IN
  stream - mp4 stream pointer
  au_hdr - au_hdr from which the parsing offset is applied
  au_hdr_len - remaining len of header info in bits ( not bytes)
  parse_offset - no bits already parsed starting from strem
  
ARGUMENTS OUT
  au_pram - the parameters which are parsed out of the header
  

RETURN VALUE
  0 on failure or no of bits this function chewed on failure.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL uint16 qvp_rtp_mp4_parse_au_hdr
( 
  qvp_rtp_mp4_ctx_type *stream, 
  uint8                *au_hdr, 
  uint32               au_hdr_len, 
  qvp_rtp_mp4_au_param_type *au_param,
  uint16               *parse_offset
)
{
  uint16 offset = *parse_offset;
  boolean cts_present = FALSE ;
  boolean dts_present = FALSE ;
  uint32 rem_len = au_hdr_len;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    check len before parsing 

    parse the AU len 
  ------------------------------------------------------------------------*/
  if( rem_len < stream->stream_config.rx_AU_size_len )
  {
    return ( 0 );
  }
  else
  {

    /*----------------------------------------------------------------------
      unpack length and length check on the total header len  
    ----------------------------------------------------------------------*/
    au_param->au_len = b_unpackw(au_hdr, offset, 
                                     stream->stream_config.rx_AU_size_len );
    offset += stream->stream_config.rx_AU_size_len;

    rem_len -= stream->stream_config.rx_AU_size_len; 

  }

  /*------------------------------------------------------------------------
     Parse the AU index 
  ------------------------------------------------------------------------*/
  if( stream->stream_config.rx_index_len )
  {

    /*----------------------------------------------------------------------
      unpack length and length check on the total header len  
    ----------------------------------------------------------------------*/
    if( rem_len < stream->stream_config.rx_index_len  )
    {
      return ( 0 );
    }
    else
    {

      au_param->au_index = b_unpackd(au_hdr, offset, 
                                      stream->stream_config.rx_index_len );
      offset += stream->stream_config.rx_index_len ;

      rem_len -= stream->stream_config.rx_index_len; 

    }
  }
  else
  {
    au_param->au_index = 0;
  }


  /*------------------------------------------------------------------------
     if receiver is configured for CTS.
     
     Try and parse it  
  ------------------------------------------------------------------------*/
  if( stream->stream_config.rx_CTS_size )
  {

    if( rem_len < 1 )
    {
      return( 0 );
    }

    cts_present = b_unpackb( au_hdr, offset, 
                             1 );
    offset += 1;
    rem_len -= 1;

    if( !cts_present )
    {
     au_param->cts = 0; 
    }
    else if ( rem_len < stream->stream_config.rx_CTS_size ) 
    {
      /*--------------------------------------------------------------------
        not enough bits to parse so return error  
      --------------------------------------------------------------------*/
      return ( 0 );
    }
    else
    {

      /*--------------------------------------------------------------------
        unpack length and length check on the total header len  
      --------------------------------------------------------------------*/
      au_param->cts = b_unpackw(au_hdr, offset, 
                                        stream->stream_config.rx_CTS_size ) ;
      offset += stream->stream_config.rx_CTS_size ;
      rem_len -= stream->stream_config.rx_CTS_size; 

    }

  }
  else
  {
    au_param->cts = 0; 
  }

  /*------------------------------------------------------------------------
     If we are configured to parse DTS 
     Try and parse it 
  ------------------------------------------------------------------------*/
  if( stream->stream_config.rx_DTS_size )
  {

    if( rem_len < 1 )
    {
      return( 0 );
    }

    dts_present = b_unpackb( au_hdr, offset, 
                             1 );
    offset += 1;
    rem_len -= 1;
    if( !dts_present )
    {
     au_param->dts = 0; 
    }
    if( dts_present && ( rem_len < stream->stream_config.rx_DTS_size ) )
    {
      return ( 0 );
    }
    else
    {


      /*--------------------------------------------------------------------
        unpack length and length check on the total header len  
      --------------------------------------------------------------------*/
      au_param->cts = b_unpackw(au_hdr, offset, 
                                        stream->stream_config.rx_DTS_size ) ;
      offset += stream->stream_config.rx_DTS_size ;

      rem_len -= stream->stream_config.rx_DTS_size; 

    }

  }
  else
  {
    au_param->dts = 0; 
  }

  /*------------------------------------------------------------------------
     Parse the RAP flag now 
  ------------------------------------------------------------------------*/
  if( stream->stream_config.rx_rap_fag_present )
  {

    if( rem_len < 1 )
    {
      return( 0 );
    }

    au_param->rap = b_unpackb( au_hdr, offset, 
                             1 );
    offset += 1;
    rem_len -= 1;
  }
  else
  {
    au_param->rap = 0; 
  }

  /*------------------------------------------------------------------------
      pase stream state if present 
  ------------------------------------------------------------------------*/
  if( stream->stream_config.rx_stream_state_size )
  {

    if(  rem_len < stream->stream_config.rx_stream_state_size )
    {
      return ( 0 );
    }
    else
    {


      /*--------------------------------------------------------------------
        unpack length and length check on the total header len  
      --------------------------------------------------------------------*/
      au_param->cts = b_unpackw(au_hdr, offset, 
                              stream->stream_config.rx_stream_state_size );
      offset += stream->stream_config.rx_stream_state_size;

      rem_len -= stream->stream_config.rx_stream_state_size; 

    }
  }
  else
  {
    au_param->ss_present = FALSE;
  }

  /*------------------------------------------------------------------------
     remember the bit offset where we stopped parsing 

     To be used for the next call to this function. Very useful for 
     iterative parsing.
  ------------------------------------------------------------------------*/
  *parse_offset = offset;

  /*------------------------------------------------------------------------
     return the length of bytes we chewed on this one.
  ------------------------------------------------------------------------*/
  return ( (uint16 )( au_hdr_len - rem_len ) );
  
} /* end of function  qvp_rtp_mp4_parse_au_hdr */


/*===========================================================================

FUNCTION qvp_rtp_mp4_reset_stream 


DESCRIPTION
  Any time this function can be called to reset the context of an MP4 
  stream. This function does not free the memory associated.

DEPENDENCIES
  None

ARGUMENTS IN
 stream - the stream for which the context should be reset 
 
ARGUMENTS OUT
  None
  

RETURN VALUE
  True on success FALSE on failure.

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_mp4_reset_stream
(
  qvp_rtp_mp4_ctx_type *stream
)
{

  /*------------------------------------------------------------------------
       reset the reassmbly context
  ------------------------------------------------------------------------*/
  qvp_rtp_reset_reassem_ctx( stream->rx_ctx.reassem_hdl );

  
  /*------------------------------------------------------------------------
    Will need to add interleaving and other things as well later
  ------------------------------------------------------------------------*/
    
  /*------------------------------------------------------------------------
      reset statstics counters
  ------------------------------------------------------------------------*/
  stream->rx_ctx.drop_cnt = 0;
  stream->rx_ctx.missing_pkt_cnt = 0;
  stream->rx_ctx.last_seq_sent = 0;
  stream->rx_ctx.last_seq = 0;
  stream->rx_ctx.last_ts = 0;   
  stream->rx_ctx.curr_au_len = 0;
  stream->rx_ctx.rx_state = QVP_RTP_MP4_RX_IDLE;
  stream->rx_ctx.output_mode = QVP_RTP_OUTPUT_FRAG_MODE;
  
} /* end of function qvp_rtp_mp4_reset_stream */


/*===========================================================================

FUNCTION QVP_RTP_MP4_PROFILE_CONFIG_RX_PARAM

DESCRIPTION

  This function payload will configuration of Rx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - MP4 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_config_rx_param
(
  
  qvp_rtp_mp4_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
)
{                                                     
 qvp_rtp_status_type ret_val = QVP_RTP_SUCCESS;

/*------------------------------------------------------------------------*/

  /*----------------------------------------------------------------------
     Get output mode from stream jitter size
  ----------------------------------------------------------------------*/
  if( payld_config->config_rx_params.rx_rtp_param.jitter_valid )
  {
    stream->rx_ctx.output_mode = qvp_rtp_get_profile_output_mode( 
              payld_config->config_rx_params.rx_rtp_param.jitter_size );
  }

  /*------------------------------------------------------------------------
    Create a ressembly context if in Frame mode
  ------------------------------------------------------------------------*/
  if( stream->rx_ctx.output_mode == QVP_RTP_OUTPUT_FRAME_MODE )
  {
    if( !stream->rx_ctx.reassem_hdl )
    {
      if( ( stream->rx_ctx.reassem_hdl = qvp_rtp_create_reassem_ctx() ) == 
                             NULL )
      {
        return( QVP_RTP_NORESOURCES );
      }
    }
  }
  /*------------------------------------------------------------------------
    In fragment mode profile does not require a reassembly context, so 
    close it if it is already present
  ------------------------------------------------------------------------*/
  else if( stream->rx_ctx.output_mode == QVP_RTP_OUTPUT_FRAG_MODE )
  {
    if( stream->rx_ctx.reassem_hdl )
    {
      QVP_RTP_MSG_LOW_0( "Closing reassembly contxt not reqd");
      qvp_rtp_close_reassem_ctx( stream->rx_ctx.reassem_hdl ); 
    }
  }
  else
  {
    QVP_RTP_ERR( "Error in output mode ", 0, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Switch based on the Payload format 
  ------------------------------------------------------------------------*/
  switch( payld_config->payload )
  {


    case QVP_RTP_PYLD_MP4_ES :
      break;
    case QVP_RTP_PYLD_MP4_GENERIC  :
    default:
      ret_val =  QVP_RTP_ERR_FATAL;
      break;
  }

  return( ret_val );

  
  
} /* end of function qvp_rtp_mp4_profile_config_rx_param */ 

/*===========================================================================

FUNCTION QVP_RTP_MP4_PROFILE_CONFIG_TX_PARAM

DESCRIPTION

  This function payload will configuration of Tx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - MP4 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_config_tx_param
(
  
  qvp_rtp_mp4_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
)
{
 qvp_rtp_status_type ret_val = QVP_RTP_SUCCESS;

/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Switch based on the Payload format 
  ------------------------------------------------------------------------*/
  switch( payld_config->payload )
  {

    case QVP_RTP_PYLD_MP4_GENERIC  :
      ret_val = QVP_RTP_ERR_FATAL ;
      break;

    case QVP_RTP_PYLD_MP4_ES :
      break;

    default:
      ret_val = QVP_RTP_ERR_FATAL ;
      break;
  }

  return( ret_val );
        
} /* end of function qvp_rtp_mp4_profile_config_tx_param */

/*===========================================================================

FUNCTION QVP_RTP_MP4_PROFILE_COPY_CONFIG 


DESCRIPTION

  This function reads out the default configuration of MP4
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is used by MP4. This is 
                     because if someone specifies the ES we will just 
                     configure the GENERIC to operate in header less 
                     mode.
                    
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_mp4_profile_copy_config
(
  qvp_rtp_payload_config_type *payld_config,
  qvp_rtp_mp4_config_type    *mp4_config
)
{
  qvp_rtp_status_type ret_val;
  qvp_rtp_mp4_es_sdp_config_type *mp4_es_rx_config =
    &payld_config->config_rx_params.config_rx_payld_params.mp4_es_rx_config;
  qvp_rtp_mp4_es_sdp_config_type *mp4_es_tx_config =
  &payld_config->config_tx_params.config_tx_payld_params.mp4_es_tx_config;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Switch on payload
  ------------------------------------------------------------------------*/
  switch( payld_config->payload )
  {
    
    case QVP_RTP_PYLD_MP4_ES :
      memset( payld_config, 0, sizeof( qvp_rtp_payload_config_type ) );
      payld_config->valid = TRUE;
      payld_config->payload  = QVP_RTP_PYLD_MP4_ES;
      payld_config->config_rx_params.valid = TRUE;
      mp4_es_rx_config->valid = TRUE;
      payld_config->config_tx_params.valid = TRUE;
      
      mp4_es_tx_config->valid = TRUE;
      ret_val = QVP_RTP_SUCCESS ;
      break;
      
    case QVP_RTP_PYLD_MP4_GENERIC :
      /*--------------------------------------------------------------------
        We need to add this code when we actually support MP4 generic
      --------------------------------------------------------------------*/
      ret_val = QVP_RTP_ERR_FATAL;
      break;

    default:
      ret_val = QVP_RTP_ERR_FATAL ;
      break;
      
  } /* end of switch */

  return( ret_val );
  
} /* end of function qvp_rtp_mp4_profile_copy_config */

/*===========================================================================

FUNCTION  QVP_RTP_MP4_PROFILE_SHUTDOWN


DESCRIPTION
  Shuts this module down and flag intialization as false

DEPENDENCIES
  None

ARGUMENTS IN
  len - length of the parload

RETURN VALUE
  toc type


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_mp4_profile_shutdown( void )
{
  uint32 i;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
     If not inited or already shut down bail out
  ------------------------------------------------------------------------*/
  if( !mp4_initialized )
  {
    return;
  }
  
  /*------------------------------------------------------------------------
      Walk through the stream array and close all channels
  ------------------------------------------------------------------------*/
  for( i = 0; i < mp4_profile_config.num_streams; i ++ )
  {

    
    /*----------------------------------------------------------------------
        If this stream is open close it now
    ----------------------------------------------------------------------*/
    if ( qvp_rtp_mp4_array[ i ].valid )
    {
      qvp_rtp_mp4_profile_close( ( qvp_rtp_profile_hdl_type  ) i );
    }
    
  } /* end of for i = 0  */
  
  /*------------------------------------------------------------------------
    free the array of streams
  ------------------------------------------------------------------------*/
  qvp_rtp_free( qvp_rtp_mp4_array   );

  /*------------------------------------------------------------------------
    Flag array as NULL and init as FALSE
  ------------------------------------------------------------------------*/
  qvp_rtp_mp4_array = NULL; 
  mp4_initialized  = FALSE;
  
} /* end of function qvp_rtp_mp4_profile_shutdown  */

#endif /* end of FEATURE_QVPHONE_RTP */
