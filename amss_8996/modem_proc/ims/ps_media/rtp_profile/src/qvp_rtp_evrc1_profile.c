/*=*====*====*====*====*====*====*===*====*====*====*====*====*====*====*====*


                         QVP_RTP_EVRC1_PROFILE . C

GENERAL DESCRIPTION

  This file contains the implementation of EVRC1 & EVRCB1 profile. 
  This is based on draft-ietf-avt-compact-bundled-evrc-04.txt

EXTERNALIZED FUNCTIONS
  None.


INITIALIZATION AND SEQUENCING REQUIREMENTS

  Need to init and configure the profile before it becomes usable.


  Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/ps_media/rtp_profile/src/qvp_rtp_evrc1_profile.c#1 $ $DateTime: 2016/03/28 23:03:22 $ $Author: mplcsds1 $

                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
26/06/09    sha    Changed the validate function to check pyld and Chdir only when valid
08/01/08    grk    Updated frm_present flag for received frames.
16/04/08    apr    KW critical warning fixes
01/14/08    apr    Fixes in EVRCB1 support
12/24/07    apr    Support for EVRC-WB
12/31/07    grk    Moving maxptime/ptime to media level.
10/26/07    rk     Marker bit doesn't need to be set
10/18/07    rk     QDJ integration. Reseting silence flag
09/14/07    apr    Modified handling zero len packets
07/28/06    uma    Fixed bug in qvp_rtp_evrc1_profile_copy_config
06/14/06    uma    Initial Creation.
===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP

#ifdef  RTP_FILE_NUMBER 
  #undef RTP_FILE_NUMBER 
#endif 
#define RTP_FILE_NUMBER 21

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <comdef.h>               /* common target/c-sim defines */
#include <string.h>               /* memory routines */
#include <stdlib.h>               /* for malloc */
#include "qvp_rtp_api.h"          /* return type propagation */
#include "qvp_rtp_profile.h"      /* profile template data type */
#include "qvp_rtp_msg.h"
#include "qvp_rtp_log.h"
#include "qvp_rtp_evrc1_profile.h"/* profile template data type */
#include <bit.h>                  /* for bit parsing/manipulation */


/*===========================================================================
                     LOCAL STATIC FUNCTIONS 
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_init
(
  qvp_rtp_profile_config_type *config
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_open
(
  qvp_rtp_profile_hdl_type      *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl      /* user handle for any cb */
);

LOCAL qvp_rtp_status_type qvp_rtp_evrcwb1_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl      /* user handle for any cb */
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_read_deflt_config
(
  
  qvp_rtp_payload_type        payld,
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_validate_config_rx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_validate_config_tx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_evrcwb1_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted */ 
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed & removed hdr*/
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_close
(
  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */
);

LOCAL void qvp_rtp_evrc1_profile_shutdown( void );

/*--------------------------------------------------------------------------
      FUNCTIONS APART FROM PROFILE TEMPLATE 
--------------------------------------------------------------------------*/

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_validate_fmtp
(
  qvp_rtp_channel_type                    ch_dir,
  qvp_rtp_evrc_comp_bund_sdp_config_type*  evrc1_config,
  qvp_rtp_evrc_comp_bund_sdp_config_type  *counter_evrc1_config
);


LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_copy_config
(
  qvp_rtp_payload_type         payld,
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_tx_config,
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_rx_config,
  qvp_rtp_evrc1_config_type    *evrc_config
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_config_tx_param
(
  qvp_rtp_evrc1_ctx_type       *stream,
    qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_tx_config
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_config_rx_param
(
  qvp_rtp_evrc1_ctx_type       *stream,
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_rx_config
);

LOCAL void qvp_rtp_evrc1_set_default_config_rx_ctx
( 
  qvp_rtp_evrc1_config_type *stream_config 
);

LOCAL void qvp_rtp_evrc1_set_default_config_tx_ctx
( 
  qvp_rtp_evrc1_config_type *stream_config 
);

LOCAL void qvp_rtp_evrc1_reset_rx_ctx
( 
  qvp_rtp_evrc1_ctx_type *stream 
);

LOCAL void qvp_rtp_evrc1_reset_tx_ctx
( 
  qvp_rtp_evrc1_ctx_type *stream 
);

LOCAL void qvp_rtp_evrc1_reset_stream
( 
  qvp_rtp_evrc1_ctx_type *stream 
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc1_config_pkt_itvl
(
  
  qvp_rtp_evrc1_ctx_type       *stream,
  qvp_rtp_payload_config_type   *payld_config 
);


/*--------------------------------------------------------------------------
    Table of accessors for the EVRC1 profile. This need to be populated in
    the table entry at qvp_rtp_profile.c. All these functions will 
    automatically link to the RTP stack by doing so.
--------------------------------------------------------------------------*/
qvp_rtp_profile_type qvp_rtp_evrc1_profile_table = 
{
  qvp_rtp_evrc1_profile_init,      /* LOACAL initialization routine  */
  qvp_rtp_evrc1_profile_open,      /* LOCAL function to open channel */
  qvp_rtp_evrc1_profile_send,      /* LOCAL function to send a pkt   */
  qvp_rtp_evrc1_profile_recv,      /* LOCAL function to rx a nw pkt  */
  qvp_rtp_evrc1_profile_close,     /* LOCAL function to close channel*/
  qvp_rtp_evrc1_profile_read_deflt_config,
  NULL,                            /* read current configuration     */
  qvp_rtp_evrc1_profile_validate_config_rx,
  qvp_rtp_evrc1_profile_validate_config_tx,
  qvp_rtp_evrc1_profile_configure,
  qvp_rtp_evrc1_profile_shutdown   /* LOCAL function to shut down    */
};

/*--------------------------------------------------------------------------
   Table of APIs for EVRCWB1
--------------------------------------------------------------------------*/
qvp_rtp_profile_type qvp_rtp_evrcwb1_profile_table = 
{
  qvp_rtp_evrc1_profile_init,      /* LOACAL initialization routine  */
  qvp_rtp_evrcwb1_profile_open,      /* LOCAL function to open channel */
  qvp_rtp_evrc1_profile_send,      /* LOCAL function to send a pkt   */
  qvp_rtp_evrc1_profile_recv,      /* LOCAL function to rx a nw pkt  */
  qvp_rtp_evrc1_profile_close,     /* LOCAL function to close channel*/
  qvp_rtp_evrc1_profile_read_deflt_config,
  NULL,                            /* read current configuration     */
  qvp_rtp_evrc1_profile_validate_config_rx,
  qvp_rtp_evrc1_profile_validate_config_tx,
  qvp_rtp_evrcwb1_profile_configure,
  qvp_rtp_evrc1_profile_shutdown   /* LOCAL function to shut down    */
};



/*===========================================================================
                     LOCAL STATIC DATA 
===========================================================================*/

LOCAL boolean evrc1_initialized = FALSE;

/*--------------------------------------------------------------------------
  Stores the configuration requested by the app.                        
--------------------------------------------------------------------------*/
LOCAL  qvp_rtp_profile_config_type evrc1_profile_config;

/*--------------------------------------------------------------------------
  Stream context array.  Responsible for each bidirectional 
  connections
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_evrc1_ctx_type *qvp_rtp_evrc1_array = NULL;

/*--------------------------------------------------------------------------
  Configuration pertaining payload formatting for EVRC 
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_evrc1_config_type evrc1_stream_config;

/*--------------------------------------------------------------------------
  LOCAL table to parse the length
  0     Blank      0    (0 bit)
  1     1/8        2    (16 bits not valid for EVRC1 & EVRCB1 )
  2     1/4        5    (40 bits; not valid for EVRC1 & EVRCB1 )
  3     1/2       10    (80 bits)
  4     1         22    (171 bits; 5 padded at end with zeros)
--------------------------------------------------------------------------*/
LOCAL int16 qvp_rtp_evrc1_rate_len_table[ QVP_RTP_EVRC1_RATE_1 + 1 ] =
{
  
  0,  /* Blank */
  10, /* 1/2 rate */
  22, /* full rate */
  
};


/*===========================================================================

FUNCTION QVP_RTP_EVRC1_PROFILE_INIT 


DESCRIPTION
  Initializes data structures and resources used by the EVRC1,EVRCB1 profile.

DEPENDENCIES
  None

ARGUMENTS IN
  config - pointer to profile configuration requested.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could initialize the profile. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_init
(
  qvp_rtp_profile_config_type *config
)
{
/*------------------------------------------------------------------------*/

  if( evrc1_initialized )
  {
    return( QVP_RTP_SUCCESS );
  }
   
  /*------------------------------------------------------------------------
    Init if we get a valid config  and appropriate call backs 
    for the basic operation of the profile.
  ------------------------------------------------------------------------*/
  if( config && config->rx_cb && config->tx_cb )
  {
    qvp_rtp_memscpy( &evrc1_profile_config, sizeof( evrc1_profile_config ),config, sizeof( evrc1_profile_config ) );
  }
  else
  {
    return( QVP_RTP_WRONG_PARAM );
  }

  /*------------------------------------------------------------------------
    Allocate the number of streams first
  ------------------------------------------------------------------------*/
  qvp_rtp_evrc1_array  = qvp_rtp_malloc( config->num_streams * 
                                sizeof( qvp_rtp_evrc1_ctx_type ) ); 

  /*------------------------------------------------------------------------
      See if we could get the memory
  ------------------------------------------------------------------------*/
  if( !qvp_rtp_evrc1_array )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  else
  {
    memset( qvp_rtp_evrc1_array, 0, config->num_streams * 
                    sizeof( qvp_rtp_evrc1_ctx_type ) );

    /*----------------------------------------------------------------------
      Set up the default configuration
    ----------------------------------------------------------------------*/
    memset ( &evrc1_stream_config,0, sizeof ( qvp_rtp_evrc1_config_type ) );
    qvp_rtp_evrc1_set_default_config_rx_ctx ( & evrc1_stream_config );
    qvp_rtp_evrc1_set_default_config_tx_ctx ( & evrc1_stream_config );
  }
    
  /*------------------------------------------------------------------------
    Flag initialization
  ------------------------------------------------------------------------*/
  evrc1_initialized  = TRUE;
  
  return( QVP_RTP_SUCCESS );

}/* end of function qvp_rtp_evrc1_profile_init */



/*===========================================================================

FUNCTION  QVP_RTP_EVRC1_PROFILE_SEND


DESCRIPTION
  request to send specified buffer through this profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl     - handle into the profile. 
  usr_hdl - handle to use when we call send function using the registered
            tx callback
  pkt     - packet of data to be sent.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted */
)
{
  qvp_rtp_evrc1_ctx_type *stream = ( qvp_rtp_evrc1_ctx_type *) hdl; 
  qvp_rtp_buf_type   *nw_buf = NULL;
  static uint8  rtp_pyld_type;
  static uint32 cur_tstamp;

/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !evrc1_initialized || !stream || !stream->valid || 
      !evrc1_profile_config.tx_cb || !pkt   ||
      ( ( stream->stream_config.tx_fixedrate !=  QVP_RTP_EVRC1_RATE_1_2 ) && 
        ( stream->stream_config.tx_fixedrate !=  QVP_RTP_EVRC1_RATE_1 ) ) )
  {
    /*----------------------------------------------------------------------
      If the packet is there just free it
    ----------------------------------------------------------------------*/
    QVP_RTP_ERR( "qvp_rtp_evrc1_profile_send:pkt dropped invalid i/p",0,0,0);

    if( pkt )
    {
      qvp_rtp_free_buf( pkt );
    }
    return( QVP_RTP_ERR_FATAL );
  }

  
  /*------------------------------------------------------------------------
    It has to be either a full rate ( 22bytes) or half rate ( 10bytes) pkt
    Silent frames( of hangover frames ) are also encoded at fixed rate
  ------------------------------------------------------------------------*/
  if ( (pkt->len ==
          qvp_rtp_evrc1_rate_len_table[stream->stream_config.tx_fixedrate] ) && (stream->tx_ctx.data_len < QVP_RTP_MTU_DLFT) )
  {
    /*----------------------------------------------------------------------
      concatenate the frame into our n/w buffer
    ----------------------------------------------------------------------*/
	  if( pkt->len < QVP_RTP_MTU_DLFT )
	  {
    qvp_rtp_memscpy( stream->tx_ctx.op_packet + stream->tx_ctx.data_len,(QVP_RTP_MTU_DLFT - stream->tx_ctx.data_len),
                           pkt->data  + pkt->head_room, pkt->len );
	  }

    /*----------------------------------------------------------------------
      Add to the data len for the newly copied frame
    ----------------------------------------------------------------------*/
    stream->tx_ctx.data_len += pkt->len;

  /*------------------------------------------------------------------------
    Frame count should be incrementd on each frame
    We will ship the bundle when frame count equals bundle size
  ------------------------------------------------------------------------*/
    stream->tx_ctx.frame_cnt++;

  /*------------------------------------------------------------------------
    Store the payload type number
  ------------------------------------------------------------------------*/
  rtp_pyld_type = pkt->rtp_pyld_type;
 
  /*------------------------------------------------------------------------
    Store the tstamp of the packet
  ------------------------------------------------------------------------*/
  cur_tstamp = pkt->tstamp;


  }
  
  else
  {
    /*----------------------------------------------------------------------
     invalid packet length
     For fixed rate EVRC1 payload silence suppression does not make sense, 
     since there is no way of indicating a silence frame. All the EVRC1
     frames should be of same size. 
   -----------------------------------------------------------------------*/
    QVP_RTP_ERR( "Evrc1 pkt dropped Rate:%d- pkt len:%d mismatch for PT %d",
       stream->stream_config.tx_fixedrate, pkt->len,pkt->rtp_pyld_type);

    qvp_rtp_free_buf( pkt );
    return( QVP_RTP_ERR_FATAL );
  }
 
  /*------------------------------------------------------------------------
    Free the buffer
  ------------------------------------------------------------------------*/
  qvp_rtp_free_buf( pkt );

  /*------------------------------------------------------------------------
    If we reached the bundle size or if we received ZLP and 
    we are in the middle of a bundle ship the bundle
  ------------------------------------------------------------------------*/
  if( stream->tx_ctx.frame_cnt == stream->stream_config.tx_bundle_size )
  {
    QVP_RTP_MSG_MED_1( " frame cnt %d \r\n", stream->tx_ctx.frame_cnt);
    /*----------------------------------------------------------------------
      Allocate a suitable len buffer
    ----------------------------------------------------------------------*/
    nw_buf = qvp_rtp_alloc_buf_by_len( QVP_RTP_HEAD_ROOM + 
                                                stream->tx_ctx.data_len );

    /*----------------------------------------------------------------------
      Bail out and cry if we could not allocate.
    ----------------------------------------------------------------------*/
    if( !nw_buf )
    {
      QVP_RTP_ERR(" Cannot allocate buffer to send EVRC1 pkt bundle.. ",
                  0, 0, 0 );
      qvp_rtp_evrc1_reset_tx_ctx( stream );
      return( QVP_RTP_ERR_FATAL );
    }
    
    nw_buf->head_room = QVP_RTP_HEAD_ROOM,
    nw_buf->len = stream->tx_ctx.data_len;
    
    /*----------------------------------------------------------------------
     Marker has no significance here 
     Silence frames should be identified by decoder using DTX
     or by decoding the packet
    ----------------------------------------------------------------------*/
    nw_buf->marker_bit = FALSE;
    nw_buf->tstamp =  cur_tstamp;
    nw_buf->rtp_pyld_type = rtp_pyld_type;
    
    qvp_rtp_memscpy( nw_buf->data + nw_buf->head_room, (QVP_RTP_HEAD_ROOM + 
                                                stream->tx_ctx.data_len) - nw_buf->head_room,/*stream->tx_ctx.data_len,*/
      stream->tx_ctx.op_packet,stream->tx_ctx.data_len);
    
    QVP_RTP_MSG_MED_2( " Sending EVRC1 Bundle Len = %d, timestamp = %d, \
                       marker = %d", nw_buf->len, nw_buf->tstamp, 
                       nw_buf->marker_bit );

    /*----------------------------------------------------------------------
      Ship the packet out 
    ----------------------------------------------------------------------*/
    evrc1_profile_config.tx_cb( nw_buf, usr_hdl );
 
    /*----------------------------------------------------------------------
       We have sent the packet so lets reset it for next bundle
    ----------------------------------------------------------------------*/
    qvp_rtp_evrc1_reset_tx_ctx( stream );
    
  }

  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_evrc1_profile_send */

/*===========================================================================

FUNCTION  QVP_RTP_EVRC1_PROFILE_OPEN 


DESCRIPTION
    Requests to open a bi directional channel within the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  usr_hdl       - handle to use when we call send function using the 
                  registered tx callback

ARGUMENTS OUT
  hdl           - on success we write the handle to the profiile into 
                  the  passed double pointer
  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl      /* user handle for any cb */
)
{
  uint32 i;   /* index variable */
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Bail out if we are not initialized yet
  ------------------------------------------------------------------------*/
  if( !evrc1_initialized )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    look for an idle stream 
  ------------------------------------------------------------------------*/
  for( i = 0;  i < evrc1_profile_config.num_streams; i++ )
  {
    /*----------------------------------------------------------------------
      Fish for free entries
    ----------------------------------------------------------------------*/
    if( !qvp_rtp_evrc1_array[ i ].valid )
    {
      qvp_rtp_evrc1_array[ i ].valid = TRUE;
      *hdl = &qvp_rtp_evrc1_array[ i ]; 
      
      /*--------------------------------------------------------------------
        copy the EVRC1 configuration to the  stream_ctx 
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( &qvp_rtp_evrc1_array[i].stream_config,sizeof(qvp_rtp_evrc1_config_type), &evrc1_stream_config,
               sizeof( qvp_rtp_evrc1_array[i].stream_config ));

      /*--------------------------------------------------------------------
        Reset the stream context - if any stale values in there.
      --------------------------------------------------------------------*/
      qvp_rtp_evrc1_reset_stream( &qvp_rtp_evrc1_array[i] );
      
      return( QVP_RTP_SUCCESS );
    }
    
  } /* maximum no of streams */

  return( QVP_RTP_NORESOURCES );


} /* end of function qvp_rtp_evrc1_profile_open */

/*===========================================================================

FUNCTION  QVP_RTP_EVRCWB1_PROFILE_OPEN 


DESCRIPTION
    Requests to open a bi directional channel within the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  usr_hdl       - handle to use when we call send function using the 
                  registered tx callback

ARGUMENTS OUT
  hdl           - on success we write the handle to the profiile into 
                  the  passed double pointer
  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrcwb1_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl      /* user handle for any cb */
)
{
  uint32 i;   /* index variable */
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Bail out if we are not initialized yet
  ------------------------------------------------------------------------*/
  if( !evrc1_initialized )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    look for an idle stream 
  ------------------------------------------------------------------------*/
  for( i = 0;  i < evrc1_profile_config.num_streams; i++ )
  {
    /*----------------------------------------------------------------------
      Fish for free entries
    ----------------------------------------------------------------------*/
    if( !qvp_rtp_evrc1_array[ i ].valid )
    {
      qvp_rtp_evrc1_array[ i ].valid = TRUE;
      *hdl = &qvp_rtp_evrc1_array[ i ]; 
      
      /*--------------------------------------------------------------------
        copy the EVRC1 configuration to the  stream_ctx 
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( &qvp_rtp_evrc1_array[i].stream_config,sizeof(qvp_rtp_evrc1_config_type), &evrc1_stream_config,
               sizeof( qvp_rtp_evrc1_array[i].stream_config ));

      /*--------------------------------------------------------------------
        Reset the stream context - if any stale values in there.
      --------------------------------------------------------------------*/
      qvp_rtp_evrc1_reset_stream( &qvp_rtp_evrc1_array[i] );
      qvp_rtp_evrc1_array[ i ].rx_ctx.rx_evrc_tstamp_inc = 
	  	                     QVP_RTP_EVRCWB1_DFLT_TSTAMP_INCR;
      
      return( QVP_RTP_SUCCESS );
    }
    
  } /* maximum no of streams */

  return( QVP_RTP_NORESOURCES );


} /* end of function qvp_rtp_evrcwb1_profile_open */

/*===========================================================================

FUNCTION QVP_RTP_EVRC1_PROFILE_READ_DEFLT_CONFIG 


DESCRIPTION

  This function reads out the default configuration of EVRC1
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is not used by EVRC1. Some 
                     other profiles uses it as they support multiple 
                     payload formats.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    

RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_read_deflt_config
(
  
  qvp_rtp_payload_type        payld, 
  qvp_rtp_payload_config_type *payld_config 
)
{
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_tx_config = NULL;
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_rx_config = NULL;
/*------------------------------------------------------------------------*/
  
  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  switch( payld ) 
  {
    case QVP_RTP_PYLD_EVRC1:
    {
      evrc1_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.evrc1_tx_config;
      evrc1_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.evrc1_rx_config;
      break;
    }
    case QVP_RTP_PYLD_EVRCB1:
    {
      evrc1_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.evrcb1_tx_config;
      evrc1_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.evrcb1_rx_config;
      break;
    }	
    case QVP_RTP_PYLD_EVRCWB1:
    {
      evrc1_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.evrcwb1_tx_config;
      evrc1_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.evrcwb1_rx_config;
	break;
    }
    default:
    {
      QVP_RTP_ERR( "Not a EVRC1 payload %d ", payld, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
  } /* end of switch pyld */

  /*------------------------------------------------------------------------
     Bundling interval
  ------------------------------------------------------------------------*/
  payld_config->config_rx_params.rx_rtp_param.maxptime_valid = TRUE;
  payld_config->config_tx_params.tx_rtp_param.maxptime_valid = TRUE;

  payld_config->config_rx_params.rx_rtp_param.maxptime = 
                                         evrc1_stream_config.rx_max_ptime;
  payld_config->config_tx_params.tx_rtp_param.maxptime =
                                          evrc1_stream_config.tx_max_ptime;
  
  /*------------------------------------------------------------------------
      Framing interval 20 ms for EVRC
  ------------------------------------------------------------------------*/
  payld_config->config_rx_params.rx_rtp_param.ptime_valid = TRUE;
  payld_config->config_tx_params.tx_rtp_param.ptime_valid = TRUE;

  payld_config->config_rx_params.rx_rtp_param.ptime = 
                                          evrc1_stream_config.rx_ptime;
  payld_config->config_tx_params.tx_rtp_param.ptime = 
                                         evrc1_stream_config.tx_ptime;

  /*------------------------------------------------------------------------
    Flag the entire rx  and tx config as valid

    Just make sure we stick in our paylod format in the pointer.

    If you got here by accident you get what you get.
  ------------------------------------------------------------------------*/
  payld_config->config_rx_params.valid = TRUE;
  payld_config->config_tx_params.valid = TRUE;
  
  evrc1_tx_config->valid = TRUE;
  evrc1_rx_config->valid = TRUE; 
 
  payld_config->chdir_valid = TRUE;
  payld_config->ch_dir = QVP_RTP_CHANNEL_FULL_DUPLEX;
  payld_config->valid = TRUE;
 
  payld_config->payload  = payld;
  payld_config->valid = TRUE;

  /*------------------------------------------------------------------------
  read the default configuration set in evrc1_stream_config set during
  profile-init or the current value if configure has happened
  ------------------------------------------------------------------------*/
  return( qvp_rtp_evrc1_profile_copy_config(payld, evrc1_tx_config, 
                  evrc1_rx_config ,&evrc1_stream_config  ) );

} /* end of function qvp_rtp_evrc1_profile_read_deflt_config */ 



/*==================================================================

FUNCTION QVP_RTP_EVRC1_PROFILE_VALIDATE_CONFIG 


DESCRIPTION

  This function checks the configuration of EVRC1 specified in the input 
  against the valid fmtp values for EVRC1

DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute  is valid, counter_config is not modified
                   If the attribute  is invalid,
                   counter_config is updated with proposed value

RETURN VALUE
  QVP_RTP_SUCCESS  - The given Configuration is  valid for the given payload,
                     RTP configure will succeed for these settings
                     
  QVP_RTP_ERR_FATAL   - The given Configuration is not valid for the given 
                        payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_validate_config_rx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  qvp_rtp_evrc_comp_bund_sdp_config_type*  evrc1_config = NULL;
  /*----------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if ( ( !payld_config ) || ( !counter_config ) )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Validating rx configuration
  ------------------------------------------------------------------------*/
  if ( ( payld_config->config_rx_params.valid ) && 
       ( payld_config->config_rx_params.
             config_rx_payld_params.evrc1_rx_config.valid ) )
  {
    if ( ( payld_config->ch_dir == QVP_RTP_CHANNEL_INBOUND ) || 
         ( payld_config->ch_dir == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
    {
      /*--------------------------------------------------------------------
        Validate ptime. Throw an error if we cant receive packets with 
        the ptime proposed.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_rx_params.rx_rtp_param.ptime_valid ) && 
           ( payld_config->config_rx_params.rx_rtp_param.ptime !=
                                QVP_RTP_EVRC1_DEF_PTIME ) )
      {
        counter_config->config_rx_params.rx_rtp_param.ptime = 
                            QVP_RTP_EVRC1_DEF_PTIME ; 
        status = QVP_RTP_ERR_FATAL;
      }
      /*--------------------------------------------------------------------
        Validate maxptime. Throw an error if we cant receive packets with 
        the maxptime proposed.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_rx_params.rx_rtp_param.maxptime_valid ) && 
           ( ( payld_config->config_rx_params.rx_rtp_param.maxptime < 
                             QVP_RTP_EVRC1_DEF_PTIME ) ||
             ( payld_config->config_rx_params.rx_rtp_param.maxptime >
                             QVP_RTP_EVRC1_DEF_MAXPTIME) ) )
      {
        counter_config->config_rx_params.rx_rtp_param.maxptime = 
                        QVP_RTP_EVRC1_DEF_MAXPTIME ; 
        status = QVP_RTP_ERR_FATAL;
      }

      switch( payld_config->payload )
       {
         case QVP_RTP_PYLD_EVRC1:
         {
           evrc1_config = 
           &payld_config->config_rx_params.config_rx_payld_params.
                                                     evrc1_rx_config;
           break;
         }
         case QVP_RTP_PYLD_EVRCB1:
         {
           evrc1_config = 
           &payld_config->config_rx_params.config_rx_payld_params.
                                                  evrcb1_rx_config;
           break;
         }
	 case QVP_RTP_PYLD_EVRCWB1:
         {
           evrc1_config = 
           &payld_config->config_rx_params.config_rx_payld_params.
                                                   evrcwb1_rx_config;
           break;
         }
        default:
        {
         QVP_RTP_ERR ( " Not a EVRC payload %d ", payld_config->payload, 0, 0);
         status = QVP_RTP_ERR_FATAL ;
        }

      }/* end of switch payload */

      /*--------------------------------------------------------------------
        Attempting to configure rx for an inbound/ duplex channel
      --------------------------------------------------------------------*/
      if( status == QVP_RTP_SUCCESS )
      {
        status = qvp_rtp_evrc1_profile_validate_fmtp (
                payld_config->ch_dir, evrc1_config ,
                & ( counter_config->config_rx_params.config_rx_payld_params.
                                                         evrc1_rx_config ) );
      }
    } /* end of if ( ( payld_config->ch_dir */
    else
    {
      QVP_RTP_ERR ( " Attempting to config rx for an o/b only ch", 0, 0, 0);
      status = QVP_RTP_ERR_FATAL ;
    }
  } /* end of if ( ( payld_config->config_rx_params.valid )*/
  
  return ( status ); 

}/* end of qvp_rtp_evrc1_profile_validate_config_rx */


/*===========================================================================

FUNCTION QVP_RTP_EVRC1_PROFILE_VALIDATE_CONFIG_TX 


DESCRIPTION

  This function checks the configuration of EVRC1 specified in the input 
  against the valid fmtp values for EVRC1

DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute  is valid, counter_config is not modified
                   If the attribute  is invalid, 
                   counter_config is updated with proposed value


RETURN VALUE
  QVP_RTP_SUCCESS   - The given Config is  valid for the given payload,
                      RTP configure will succeed for these settings
  QVP_RTP_ERR_FATAL - The given Configuration is not valid for the given 
                      payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_validate_config_tx
(
  qvp_rtp_payload_config_type*  payld_config,
  qvp_rtp_payload_config_type*  counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  qvp_rtp_evrc_comp_bund_sdp_config_type*  evrc1_config = NULL;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Validating tx configuration
  ------------------------------------------------------------------------*/
  if ( ( payld_config->config_tx_params.valid ) && 
       ( payld_config->config_tx_params.
             config_tx_payld_params.evrc1_tx_config.valid ) )
  {
    if ( ( payld_config->ch_dir == QVP_RTP_CHANNEL_OUTBOUND ) || 
         ( payld_config->ch_dir == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
    {
      /*--------------------------------------------------------------------
        Validate ptime. Throw an error if peer wants us to send 
        packets with ptime lesser than our minimum supported ptime.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_tx_params.tx_rtp_param.ptime_valid ) && 
           ( payld_config->config_tx_params.tx_rtp_param.ptime <
                                QVP_RTP_EVRC1_DEF_PTIME ) )
      {
        counter_config->config_tx_params.tx_rtp_param.ptime = 
                            QVP_RTP_EVRC1_DEF_PTIME ; 
        status = QVP_RTP_ERR_FATAL;
      }
      /*--------------------------------------------------------------------
        Validate maxptime. Throw an error if peer wants us to send 
        packets with maxptime lesser than our minimum supported maxptime.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_tx_params.tx_rtp_param.maxptime_valid ) && 
           ( payld_config->config_tx_params.tx_rtp_param.maxptime  < 
                            QVP_RTP_EVRC1_DEF_PTIME ) )
      {
        counter_config->config_tx_params.tx_rtp_param.maxptime = 
                              QVP_RTP_EVRC1_DEF_MAXPTIME ; 
        status = QVP_RTP_ERR_FATAL;
      }

      switch( payld_config->payload )
       {
         case QVP_RTP_PYLD_EVRC1:
         {
           evrc1_config = 
           &payld_config->config_tx_params.config_tx_payld_params.
                                                     evrc1_tx_config;
           break;
         }
         case QVP_RTP_PYLD_EVRCB1:
         {
           evrc1_config = 
           &payld_config->config_tx_params.config_tx_payld_params.
                                                  evrcb1_tx_config;
           break;
         }
         case QVP_RTP_PYLD_EVRCWB1:
         {
           evrc1_config = 
           &payld_config->config_rx_params.config_rx_payld_params.
                                                   evrcwb1_rx_config;
           break;
         }
        default:
        {
         QVP_RTP_ERR ( " Not a EVRC1 payload %d ", payld_config->payload, 0, 0);
         status = QVP_RTP_ERR_FATAL ;
        }

      }/* end of switch payload */

      /*--------------------------------------------------------------------
        Attempting to configure tx for an outbound/ duplex channel
      --------------------------------------------------------------------*/
      if( status == QVP_RTP_SUCCESS )
      {
        status = qvp_rtp_evrc1_profile_validate_fmtp (
                payld_config->ch_dir, evrc1_config,
                & ( counter_config->config_tx_params.config_tx_payld_params.
                                                        evrc1_tx_config ) );
      }
    }/* end of if ( ( payld_config->ch_dir */
    else
    {
      QVP_RTP_ERR ( " Attempting to config tx for an i/b only ch", 0, 0, 0);
      status = QVP_RTP_ERR_FATAL ;
    }
  }/* end of if ( ( payld_config->config_tx_params.valid )*/

  return ( status ); 

}/* end of qvp_rtp_evrc1_profile_validate_config_tx */


/*===========================================================================

FUNCTION QVP_RTP_EVRC1_PROFILE_VALIDATE_FMTP


DESCRIPTION

  This function checks the configuration of EVRC1 specified in the input
  against the valid fmtp values for EVRC1
  
  This is the generic function used for both tx & rx params since most of  
  the parameters have the same valid range in both the directions.
  
  In case of any assymetry,the differences have to be accomodated 
  using channel direction 
  or this can be split into separate functions on tx&rx plane


DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated
    ch_dir       - This gives the information whether the Tx or Rx plane
                   have to be configured
                   Currently not used since  Tx & Rx plane are symmetrical

ARGUMENTS OUT
  counter_config - If the attribute is valid, counter_config is not modified
                   If the attribute  is invalid, 
                   counter_config is updated with proposed value
                   
                   counter_config has the basic capabilities.
                   In future additional support can be added to come up with
                   an enhanced configuration


RETURN VALUE
  QVP_RTP_SUCCESS   - The given Config is  valid for the given payload,
                      RTP configure will succeed for these settings
  QVP_RTP_ERR_FATAL - The given Configuration is not valid for the given 
                      payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_validate_fmtp
(
  qvp_rtp_channel_type ch_dir,  /* not used for now*/
  qvp_rtp_evrc_comp_bund_sdp_config_type*      evrc1_config,
  qvp_rtp_evrc_comp_bund_sdp_config_type*     counter_evrc1_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  if ( !counter_evrc1_config ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  if ( !evrc1_config || evrc1_config->valid )
  {
    return (status );
  }

  if ( ( evrc1_config->fixedrate_valid ) && 
       ( (evrc1_config->fixedrate != QVP_RTP_EVRC1_RATE_1_2 ) &&
         ( evrc1_config->fixedrate != QVP_RTP_EVRC1_RATE_1 ) ) )
  {
    counter_evrc1_config->fixedrate = QVP_RTP_EVRC1_RATE_1_2; 
    status = QVP_RTP_ERR_FATAL;
  }

  /*------------------------------------------------------------------------
    Fmtp related to DTX parameters are not used by RTP , 
   so validation is not done
  ------------------------------------------------------------------------*/
  
  return ( status );
  
} /* end of function qvp_rtp_evrc1_profile_validate_fmtp */ 


/*===========================================================================

FUNCTION QVP_RTP_EVRC1_PROFILE_CONFIGURE

DESCRIPTION

  This function payload configuration of a previously opened EVRC1  
  channel.

  If new values are specified and they are valid
  they take precedence over previous configuration

  If new values are specified and they are invalid, 
  previous configuration is retained

  If new values are not specified for any attribute , 
  previous configuration value is retained
  & if previous configuration is absent default values are set.

DEPENDENCIES
  None

ARGUMENTS IN
    hdl            - handle to the opend channel
    payld_config   - pointer to configuration structure
    

RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,
  qvp_rtp_payload_config_type   *payld_config 
)
{
  qvp_rtp_evrc1_ctx_type     *stream = ( qvp_rtp_evrc1_ctx_type *) hdl; 
  qvp_rtp_evrc1_config_type  prev_config;
  qvp_rtp_status_type        status = QVP_RTP_SUCCESS;
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_rx_config = NULL;
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_tx_config = NULL;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !evrc1_initialized || !stream || !stream->valid || 
      !payld_config ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
      Since we are using the same code for EVRC & EVRCB we need to get the
      right pyld config structure here
  ------------------------------------------------------------------------*/
    if( payld_config->payload == QVP_RTP_PYLD_EVRCB1)
    {
      evrc1_rx_config = 
    	&payld_config->config_rx_params.config_rx_payld_params.evrcb1_rx_config;
      evrc1_tx_config = 
    	&payld_config->config_tx_params.config_tx_payld_params.evrcb1_tx_config;
    }
    else if( payld_config->payload == QVP_RTP_PYLD_EVRC1 )
    {
    
      evrc1_rx_config = 
    	&payld_config->config_rx_params.config_rx_payld_params.evrc1_rx_config;
      evrc1_tx_config = 
    	&payld_config->config_tx_params.config_tx_payld_params.evrc1_tx_config;	

    }
    else
    {
      QVP_RTP_ERR( "Not a EVRC1 Payload %d ", payld_config->payload ,0,0 );
      status =  QVP_RTP_ERR_FATAL;
    }
 
  /*------------------------------------------------------------------------
    Backup the current configuration before we proceed from here.
  ------------------------------------------------------------------------*/
  prev_config = stream->stream_config;
  
  /*------------------------------------------------------------------------
    Configuration steps.
    1) Set default values for values that are not set already
      , so that if any param is not configured the def values are retained
    2) Try and configure RX.If it fails.reset it to prev state & return err.
       This way application can easily implement OFFER/ANSWER as in RFC3264
    3) Try and configure TX If it fails.reset it to prev state & return err
    4) Set the default TS increment value
  ------------------------------------------------------------------------*/

  qvp_rtp_evrc1_set_default_config_tx_ctx ( & ( stream->stream_config ) );
  qvp_rtp_evrc1_set_default_config_rx_ctx ( & ( stream->stream_config ) );
  
  stream->rx_ctx.rx_evrc_tstamp_inc = QVP_RTP_EVRC1_DFLT_TSTAMP_INCR;
  
  /*------------------------------------------------------------------------
     Configure ptime & maxptime for tx & rx
  ------------------------------------------------------------------------*/
  status = qvp_rtp_evrc1_config_pkt_itvl( stream, payld_config );

  if( ( status == QVP_RTP_SUCCESS ) && 
        payld_config->config_rx_params.valid )
  {
    status = qvp_rtp_evrc1_profile_config_rx_param( stream, evrc1_rx_config );
  }

  /*------------------------------------------------------------------------
      Try and configure Tx part now....
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS )&&
      payld_config->config_tx_params.valid )
  {
    status = qvp_rtp_evrc1_profile_config_tx_param(stream, evrc1_tx_config );
  }

  /*------------------------------------------------------------------------
    If we did not succeed to a struct to reset to preivous value and 
    return failure
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS )
  {
    stream->stream_config = prev_config ;
  }
  
  return( status );

} /* end of function qvp_rtp_evrc1_profile_configure */


/*===========================================================================

FUNCTION QVP_RTP_EVRCWB1_PROFILE_CONFIGURE

DESCRIPTION

  This function payload configuration of a previously opened EVRCWB1  
  channel.

  If new values are specified and they are valid
  they take precedence over previous configuration

  If new values are specified and they are invalid, 
  previous configuration is retained

  If new values are not specified for any attribute , 
  previous configuration value is retained
  & if previous configuration is absent default values are set.

DEPENDENCIES
  None

ARGUMENTS IN
    hdl            - handle to the opend channel
    payld_config   - pointer to configuration structure
    

RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrcwb1_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,
  qvp_rtp_payload_config_type   *payld_config 
)
{
  qvp_rtp_evrc1_ctx_type     *stream = ( qvp_rtp_evrc1_ctx_type *) hdl; 
  qvp_rtp_evrc1_config_type  prev_config;
  qvp_rtp_status_type        status = QVP_RTP_SUCCESS;
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrcwb1_rx_config = NULL;
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrcwb1_tx_config = NULL;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !evrc1_initialized || !stream || !stream->valid || 
      !payld_config ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Backup the current configuration before we proceed from here.
  ------------------------------------------------------------------------*/
  prev_config = stream->stream_config;
  
  /*------------------------------------------------------------------------
    Configuration steps.
    1) Set default values for values that are not set already
      , so that if any param is not configured the def values are retained
    2) Try and configure RX.If it fails.reset it to prev state & return err.
       This way application can easily implement OFFER/ANSWER as in RFC3264
    3) Try and configure TX If it fails.reset it to prev state & return err
    4) Set the default TS increment value
  ------------------------------------------------------------------------*/

  qvp_rtp_evrc1_set_default_config_tx_ctx ( & ( stream->stream_config ) );
  qvp_rtp_evrc1_set_default_config_rx_ctx ( & ( stream->stream_config ) );
  
  stream->rx_ctx.rx_evrc_tstamp_inc = QVP_RTP_EVRCWB1_DFLT_TSTAMP_INCR;

  /*------------------------------------------------------------------------
     Configure ptime & maxptime for tx & rx
  ------------------------------------------------------------------------*/
  status = qvp_rtp_evrc1_config_pkt_itvl( stream, payld_config );

  evrcwb1_rx_config = 
    	&payld_config->config_rx_params.config_rx_payld_params.evrcwb1_rx_config;
  evrcwb1_tx_config = 
    	&payld_config->config_tx_params.config_tx_payld_params.evrcwb1_tx_config;

  if( payld_config->config_rx_params.valid )
  {
    status = qvp_rtp_evrc1_profile_config_rx_param( stream, evrcwb1_rx_config );
  }

  /*------------------------------------------------------------------------
      Try and configure Tx part now....
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS )&&
      payld_config->config_tx_params.valid )
  {
    status = qvp_rtp_evrc1_profile_config_tx_param(stream, evrcwb1_tx_config );
  }

  /*------------------------------------------------------------------------
    If we did not succeed to a struct to reset to preivous value and 
    return failure
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS )
  {
    stream->stream_config = prev_config ;
  }
  
  return( status );

} /* end of function qvp_rtp_evrc1_profile_configure */

/*===========================================================================

FUNCTION qvp_rtp_evrc1_config_pkt_itvl

DESCRIPTION

  To configure ptime & maxptime

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - EVRC1 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_config_pkt_itvl
(
  
  qvp_rtp_evrc1_ctx_type       *stream,
  qvp_rtp_payload_config_type   *payld_config 
)
{
  if( !stream || !payld_config )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    See if the pttime is sent does it  match with what we have
  ------------------------------------------------------------------------*/
  if( payld_config->config_rx_params.rx_rtp_param.ptime_valid && 
      ( payld_config->config_rx_params.rx_rtp_param.ptime != 
                  QVP_RTP_EVRC1_DEF_PTIME ) )
  {
    QVP_RTP_MSG_MED_1( "EVRC1 Invalid Rx ptime %d ",
        payld_config->config_rx_params.rx_rtp_param.ptime, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    We will cache the max ptime ... 
  ------------------------------------------------------------------------*/
  if( payld_config->config_rx_params.rx_rtp_param.maxptime_valid ) 
  {
    /*----------------------------------------------------------------------
      if max ptime is less than ptime then we have a problem. 
      If this exceeds too much this will be bad for our jitter buffer.
    ----------------------------------------------------------------------*/
    if ( ( payld_config->config_rx_params.rx_rtp_param.maxptime < 
                     QVP_RTP_EVRC1_DEF_PTIME ) ||
         ( payld_config->config_rx_params.rx_rtp_param.maxptime >
                     QVP_RTP_EVRC1_MAX_MAXPTIME) )
    {
      QVP_RTP_MSG_MED_1( "EVRC1 Invalid Rx maxptime %d ",
        payld_config->config_rx_params.rx_rtp_param.maxptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
    /*----------------------------------------------------------------------
      Store maxptime value.
    ----------------------------------------------------------------------*/
    stream->stream_config.rx_max_ptime = 
              payld_config->config_rx_params.rx_rtp_param.maxptime; 
  }

  QVP_RTP_MSG_HIGH_2( "EVRC1 Rx maxptime %d Rx ptime %d",
        stream->stream_config.rx_max_ptime,
          stream->stream_config.rx_ptime, 0 );


/*------------------------------------------------------------------------
    See if the pttime is sent & does it match with what we have
  ------------------------------------------------------------------------*/
  if( payld_config->config_tx_params.tx_rtp_param.ptime_valid == TRUE )
  {
    /*----------------------------------------------------------------------
      This means peer can receive pkts with ptime lesser 
      than minimum ptime pkts we can send. Throw an error.
    ----------------------------------------------------------------------*/
    if ( payld_config->config_tx_params.tx_rtp_param.ptime < 
                     QVP_RTP_EVRC1_DEF_PTIME )
    {
      QVP_RTP_MSG_MED_1( "EVRC1 Invalid Tx ptime %d ",
        payld_config->config_tx_params.tx_rtp_param.ptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
    /*----------------------------------------------------------------------
      This means peer can receive pkts with ptime greater 
      than our minimum ptime. Just set the transmit ptime to maximum 
      ptime we can support.
    ----------------------------------------------------------------------*/
    else if ( payld_config->config_tx_params.tx_rtp_param.ptime > 
                     QVP_RTP_EVRC1_DEF_PTIME )
    {
      stream->stream_config.tx_ptime = QVP_RTP_EVRC1_DEF_PTIME;
    }
    /*----------------------------------------------------------------------
      Proposed ptime is within the limits, we can send packets with the 
      ptime proposed.
    ----------------------------------------------------------------------*/
    else
    {
      stream->stream_config.tx_ptime = 
                  payld_config->config_tx_params.tx_rtp_param.ptime;
    }

  }/*end of if(ptime_valid == TRUE)*/
  
  /*------------------------------------------------------------------------
    We will cache the max ptime ... 
  ------------------------------------------------------------------------*/
  if( payld_config->config_tx_params.tx_rtp_param.maxptime_valid == TRUE ) 
  {
    /*----------------------------------------------------------------------
      if max ptime is less than ptime then we have a problem. 
    ----------------------------------------------------------------------*/
    if ( payld_config->config_tx_params.tx_rtp_param.maxptime < 
                        QVP_RTP_EVRC1_DEF_PTIME )
    {
      QVP_RTP_MSG_MED_1( "EVRC1 Invalid Tx maxptime %d ",
        payld_config->config_tx_params.tx_rtp_param.maxptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
    /*----------------------------------------------------------------------
      If this exceeds too much this will be bad for our jitter buffer.
      Cap it to the max value we can send.
    ----------------------------------------------------------------------*/
    else if ( payld_config->config_tx_params.tx_rtp_param.maxptime > 
                        QVP_RTP_EVRC1_DEF_MAXPTIME )
    {
      stream->stream_config.tx_max_ptime = QVP_RTP_EVRC1_DEF_MAXPTIME;
    }
    /*----------------------------------------------------------------------
      Proposed maxptime is within the limits, we can send packets with the 
      maxptime proposed.
    ----------------------------------------------------------------------*/
    else
    {
      stream->stream_config.tx_max_ptime = 
                     payld_config->config_tx_params.tx_rtp_param.maxptime; 
    }

  }/*end of if(maxptime_valid ==TRUE)*/

  /*--------------------------------------------------------------------
    maxptime & ptime are validated we dont have to validate bundle size
    too
  --------------------------------------------------------------------*/
  stream->stream_config.tx_bundle_size =
   ( stream->stream_config.tx_max_ptime /stream->stream_config.tx_ptime );

  QVP_RTP_MSG_HIGH( "EVRC1 Tx maxptime %d Tx ptime %d bundle size %d",
          stream->stream_config.tx_max_ptime,
          stream->stream_config.tx_ptime, 
          stream->stream_config.tx_bundle_size );

  return( QVP_RTP_SUCCESS );


} /* end of qvp_rtp_evrc1_config_pkt_itvl */

/*===========================================================================

FUNCTION QVP_RTP_EVRC1_PROFILE_CONFIG_RX_PARAM

DESCRIPTION

  This function payload will configuration of Rx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - EVRC1 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_config_rx_param
(
  
  qvp_rtp_evrc1_ctx_type       *stream,
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_rx_config
)
{
/*-------------------------------------------------------------------------*/
  
  if( !evrc1_rx_config || !stream ) 
  {
    QVP_RTP_ERR ( " Invalid params", 0, 0, 0);
    return( QVP_RTP_ERR_FATAL );
  }

  
  /*------------------------------------------------------------------------
    We will cache the rate.
    If rate is not specified the default is taken and we dont complain since
    the rate is an optional parameter
    BUt if a wrong rate is specificied then we throw an error and exit
  ------------------------------------------------------------------------*/
  if( evrc1_rx_config->fixedrate_valid ) 
  {
    /*----------------------------------------------------------------------
      only half rate & full rate are supported for compact bundled frames
    ----------------------------------------------------------------------*/
    if ( ( evrc1_rx_config->fixedrate !=  QVP_RTP_EVRC1_RATE_1_2 ) && 
         ( evrc1_rx_config->fixedrate !=  QVP_RTP_EVRC1_RATE_1 ) )
    {
      QVP_RTP_ERR( "qvp_rtp_evrc1_profile_config_rx_param:Invalid rate:%d",\
                                     evrc1_rx_config->fixedrate, 0,0 );
      return( QVP_RTP_ERR_FATAL );
    }

    stream->stream_config.rx_fixedrate = evrc1_rx_config->fixedrate; 
  }

  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_evrc1_profile_config_rx_param */ 

/*===========================================================================

FUNCTION QVP_RTP_EVRC1_PROFILE_CONFIG_TX_PARAM

DESCRIPTION

  This function payload will configuration of Tx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - EVRC stream which needs to be configured.
    payld_config   - pointer to configuration structure
    


RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_config_tx_param
(
  
  qvp_rtp_evrc1_ctx_type       *stream,
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_tx_config
)
{
/*------------------------------------------------------------------------*/
  if( !evrc1_tx_config || !stream ) 
  {
    QVP_RTP_ERR ( " Invalid params", 0, 0, 0);
    return( QVP_RTP_ERR_FATAL );
  }
  /*------------------------------------------------------------------------
  Did not see any need in updating dtx params as they are not used by RTP
  ------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    We will cache the rate
  ------------------------------------------------------------------------*/
  if( evrc1_tx_config->fixedrate_valid ) 
  {
    /*----------------------------------------------------------------------
      only half rate & full rate are supported for compact bundled frames
    ----------------------------------------------------------------------*/
    if ( ( evrc1_tx_config->fixedrate !=  QVP_RTP_EVRC1_RATE_1_2 ) && 
         ( evrc1_tx_config->fixedrate !=  QVP_RTP_EVRC1_RATE_1 ) )
    {
      return( QVP_RTP_ERR_FATAL );
    }
    stream->stream_config.tx_fixedrate = evrc1_tx_config->fixedrate; 
  }

  return( QVP_RTP_SUCCESS );
        
} /* end of function qvp_rtp_evrc1_profile_config_tx_param */
  

/*===========================================================================

FUNCTION  QVP_RTP_EVRC1_PROFILE_RECV 


DESCRIPTION
   The function which the RTP framework will call upon arrival of data from 
   NW. EVRC header is stripped but parameters are passed.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl           - hdl into the profile.
  usr_hdl       - handle to use when we call send function using the 
                  registered rx callback
  pkt           - actual data packet to be parsed.

  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed*/

)
{
  qvp_rtp_evrc1_ctx_type *stream   = ( qvp_rtp_evrc1_ctx_type *) hdl; 
  qvp_rtp_buf_type       *aud_buf;
  uint8                  frame_len = 0;
  uint8                  *aud_data;
  uint16                 aud_len;
  uint8                  bundle_size = 0;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !evrc1_initialized || !stream || !stream->valid || 
      !evrc1_profile_config.rx_cb  ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  QVP_RTP_MSG_HIGH_0( " Got an EVRC1 packet ");

  if( stream->stream_config.rx_fixedrate > QVP_RTP_EVRC1_RATE_1 )
  {
    QVP_RTP_ERR( " Wrong length/oversized pkt of len :%d bytes for rate:%d",
                         pkt->len,stream->stream_config.rx_fixedrate, 0 ); 
    if ( pkt->need_tofree )
    {
      qvp_rtp_free_buf( pkt );
    }
    return( QVP_RTP_ERR_FATAL );
  }

  frame_len = 
          qvp_rtp_evrc1_rate_len_table[ stream->stream_config.rx_fixedrate];
  bundle_size = ( pkt->len / frame_len );

  if ( ( pkt->len % frame_len != 0 )  ||
       ( bundle_size == 0 ) ||
       ( bundle_size >
          (stream->stream_config.rx_max_ptime /
               stream->stream_config.rx_ptime) ) ||
       ( bundle_size > QVP_RTP_EVRC1_DEF_BUNDLE_SIZE ) )

  /*------------------------------------------------------------------------
    Check if the pkt len is an exact multiple (bunlde) of this frame rate 
    Atleast one frame should be present
    Bundle size should not be greater than configured value,
    bundle size should not greater than what we support
  ------------------------------------------------------------------------*/
  {
    QVP_RTP_ERR( " Wrong length/oversized pkt of len :%d bytes for rate:%d",
                         pkt->len,stream->stream_config.rx_fixedrate, 0 ); 
    if ( pkt->need_tofree )
    {
      qvp_rtp_free_buf( pkt );
    }
    return( QVP_RTP_ERR_FATAL );
  }

  if( ( stream->rx_ctx.last_seq + 1 ) <  pkt->seq ) 
  {
    QVP_RTP_ERR( " Missing Seq inside RTP", 0, 0, 0 ); 
  }
  else if ( ( stream->rx_ctx.last_seq + 1 ) >  pkt->seq ) 
  {
    /* you wil see this for pkt with seq no.0 also */
    QVP_RTP_MSG_LOW_0( " Delayed/Reordered pkt  inside RTP");
  }
  stream->rx_ctx.last_seq = pkt->seq;

  /*------------------------------------------------------------------------
    Advance to audio data inside the packet 
  ------------------------------------------------------------------------*/
  aud_data = pkt->data + pkt->head_room;
  aud_len = pkt->len;

  /*------------------------------------------------------------------------
    iterate through the packet until we have parsed all the frames...
    Or we get erraneous PDU.
  ------------------------------------------------------------------------*/
  while( bundle_size )
  {

    /*----------------------------------------------------------------------
      try and alloc an audio buffer
    ----------------------------------------------------------------------*/
    aud_buf = qvp_rtp_alloc_buf( QVP_RTP_POOL_AUDIO );

    /*----------------------------------------------------------------------
      See if we got a buffer
    ----------------------------------------------------------------------*/
    if ( !aud_buf )
    {
      QVP_RTP_ERR( "Could not get an audio buf for EVRC1 \r\n", 0, 0, 0 );
      if ( pkt->need_tofree )
      {
        qvp_rtp_free_buf( pkt );
      }
      return( QVP_RTP_ERR_FATAL );
    }
    else
    {
      aud_buf->head_room = 0;
      aud_buf->len = frame_len;
      
      /*--------------------------------------------------------------------
        Taken from RFC3558

        "
        The RTP timestamp is in 1/8000 of a second units
        for EVRC and SMV.  For any other vocoders that use this packet
        format, the timestamp unit needs to be defined explicitly. 

        "
        
        "
        When multiple codec data frames are present in a single RTP packet,
        the timestamp is that of the oldest data represented in the RTP
        packet. 
        "

        Remaining time stamps are calculated using packetization 
        interval ( 20 ms fixed for EVRC ) in this implementation

        The timestamp reflects the sampling instant of the first octet in
        the RTP data packet.
        Eg : an audio frame with three bundles will contain the timestamp of 
        the first octet of the third bundle.
        This will be 3 RTP packets with timestamp 0,160 & 320 
     ---------------------------------------------------------------------*/

      aud_buf->tstamp = pkt->tstamp - ( ( bundle_size - 1 ) * 
                       stream->rx_ctx.rx_evrc_tstamp_inc );
      aud_buf->need_tofree = TRUE;
      aud_buf->seq = pkt->seq;
      aud_buf->silence = FALSE;
  
      qvp_rtp_memscpy( aud_buf->data,aud_buf->len, aud_data,aud_buf->len ); 
 
      /*--------------------------------------------------------------------
           Mark frm_present bit
      --------------------------------------------------------------------*/
      aud_buf->frm_info.info.aud_info.frm_present = TRUE;

      QVP_RTP_MSG_MED(" RX EVRC1 Bundle Len = %d, timestamp = %d,seq= %d",\
                          aud_buf->len, aud_buf->tstamp, aud_buf->seq  );
  
      /*--------------------------------------------------------------------
        Ship the packet up
        If we could not ship it break;
      --------------------------------------------------------------------*/
      if( evrc1_profile_config.rx_cb(aud_buf, usr_hdl ) != QVP_RTP_SUCCESS )
      {
        break;
      }

    } /* end of else for if ( !aud_buf ) */

    /*----------------------------------------------------------------------
      Decrement the frame count 
    ----------------------------------------------------------------------*/
    bundle_size --;

    /*----------------------------------------------------------------------
      Decrement the remaining audion len accordingly
    ----------------------------------------------------------------------*/
    aud_len -= frame_len;
    
    /*----------------------------------------------------------------------
      If there is still life in this loop advance data pointer
    ----------------------------------------------------------------------*/
    if( bundle_size )
    {
      /*--------------------------------------------------------------------
        Advance the audio buffer
      --------------------------------------------------------------------*/
      aud_data += frame_len;
     }
    
    
  } /* end of while hdr.count */

  /*------------------------------------------------------------------------
    Free the buffer if needed
  ------------------------------------------------------------------------*/
  if( pkt->need_tofree )
  {
    qvp_rtp_free_buf( pkt );
  }

  return( QVP_RTP_SUCCESS );

} /* end of function qvp_rtp_evrc1_profile_recv */


/*===========================================================================

FUNCTION  QVP_RTP_EVRC1_PROFILE_CLOSE


DESCRIPTION
  Closes an already open bi directional channel inside the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl - handle of channel to close.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

)
{
  qvp_rtp_evrc1_ctx_type *stream = ( qvp_rtp_evrc1_ctx_type *) hdl; 
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If we get  a valid handle invalidate the index 
    Also check are we initialized yet ..?
  ------------------------------------------------------------------------*/
  if( hdl && evrc1_initialized )
  {
    /*----------------------------------------------------------------------
      We cannot afford to loose buffers so when we are in the middle of
      reassmbling we need to free the whole chain
    ----------------------------------------------------------------------*/
    stream->valid = FALSE; 
    /*----------------------------------------------------------------------
      Reset the stream context - if any stale values in there.
    ----------------------------------------------------------------------*/
    qvp_rtp_evrc1_reset_stream( stream );
    return( QVP_RTP_SUCCESS );
  }
  else
  {
    return( QVP_RTP_ERR_FATAL );
  }

} /* end of function qvp_rtp_evrc1_profile_close */ 


/*===========================================================================

FUNCTION  QVP_RTP_EVRC1_SET_DEFAULT_CONFIG_TX_CTX


DESCRIPTION
  Set the profile config context inside the stream to default values 

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  None


SIDE EFFECTS
  This function will set the default values only if
  the values are not set already.
  If you .want to force all the values to be set to default values, 
  clear the argument before calling this function 
  

===========================================================================*/
LOCAL void qvp_rtp_evrc1_set_default_config_tx_ctx
( 
  qvp_rtp_evrc1_config_type *stream_config 
)
{
/*------------------------------------------------------------------------*/

    /*----------------------------------------------------------------------
    Set up the default configuration
    ----------------------------------------------------------------------*/
    if ( !stream_config->tx_fixedrate )
    {
      stream_config->tx_fixedrate      = QVP_RTP_EVRC1_RATE_1_2;
    }
    if ( !stream_config->tx_bundle_size )
    {
      stream_config->tx_bundle_size    = QVP_RTP_EVRC1_DEF_BUNDLE_SIZE;
    }
    if ( !stream_config->tx_ptime )
    {
      stream_config->tx_ptime          = QVP_RTP_EVRC1_DEF_PTIME;
    }
    if ( !stream_config->tx_max_ptime )
    {
      stream_config->tx_max_ptime      = QVP_RTP_EVRC1_DEF_MAXPTIME;
    }

} /* end of function qvp_rtp_evrc1_set_default_config_tx_ctx */

/*===========================================================================

FUNCTION  QVP_RTP_EVRC1_SET_DEFAULT_CONFIG_RX_CTX


DESCRIPTION
  Set the profile config context inside the stream to default values 

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  None


SIDE EFFECTS
  This function will set the default values only if
  the values are not set already.
  If you .want to force all the values to be set to default values, 
  clear the argument before calling this function 

===========================================================================*/
LOCAL void qvp_rtp_evrc1_set_default_config_rx_ctx
( 
  qvp_rtp_evrc1_config_type *stream_config 
)
{
/*------------------------------------------------------------------------*/

    /*----------------------------------------------------------------------
      Set up the default configuration
    ----------------------------------------------------------------------*/
    if ( !stream_config->rx_fixedrate )
    {
      stream_config->rx_fixedrate      = QVP_RTP_EVRC1_RATE_1_2;
    }
    if ( !stream_config->rx_ptime )
    {
      stream_config->rx_ptime          = QVP_RTP_EVRC1_DEF_PTIME;
    }
    if ( !stream_config->rx_max_ptime )
    {
      stream_config->rx_max_ptime      = QVP_RTP_EVRC1_DEF_MAXPTIME;
    }

} /* end of function qvp_rtp_evrc1_set_default_config_rx_ctx */



/*===========================================================================

FUNCTION  QVP_RTP_EVRC1_RESET_TX_CTX


DESCRIPTION
  Reset the trasmitter context inside the stream

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  None


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_evrc1_reset_tx_ctx
( 
  qvp_rtp_evrc1_ctx_type *stream 
)
{
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
      Reset the packet to packet to packet context
  ------------------------------------------------------------------------*/
  stream->tx_ctx.data_len     = 0;
  stream->tx_ctx.frame_cnt    = 0;
  stream->tx_ctx.marker       = 0;
  stream->tx_ctx.first_tstamp = 0;

} /* end of function qvp_rtp_evrc1_reset_tx_ctx */

/*===========================================================================

FUNCTION  QVP_RTP_EVRC1_RESET_RX_CTX


DESCRIPTION
  Reset the receiver context inside the stream

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  None


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_evrc1_reset_rx_ctx
( 
  qvp_rtp_evrc1_ctx_type *stream 
)
{
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Reset the packet to packet to packet context
  ------------------------------------------------------------------------*/
  stream->rx_ctx.last_seq = 0;

} /* end of function qvp_rtp_evrc1_reset_tx_ctx */


    
/*===========================================================================

FUNCTION  QVP_RTP_EVRC1_RESET_STREAM 


DESCRIPTION
  Reset the context inside the stream

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_evrc1_reset_stream
( 
  qvp_rtp_evrc1_ctx_type *stream 
)
{
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
   Reset the transmit side
  ------------------------------------------------------------------------*/
  qvp_rtp_evrc1_reset_tx_ctx( stream );

  qvp_rtp_evrc1_reset_rx_ctx( stream );
  
} /* end of function qvp_rtp_evrc1_reset_stream */



/*===========================================================================

FUNCTION QVP_RTP_EVRC1_PROFILE_COPY_CONFIG 


DESCRIPTION

  This function reads out the default configuration of EVRC1
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is not used by EVRC1. Some 
                     other profiles uses it as they support multiple 
                     payload formats.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc1_profile_copy_config
(
  qvp_rtp_payload_type         payld,
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_tx_config,
  qvp_rtp_evrc_comp_bund_sdp_config_type  *evrc1_rx_config,
  qvp_rtp_evrc1_config_type   *evrc1_config
)
{
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Memset the whole configuration before we proceed
  ------------------------------------------------------------------------*/
  memset( evrc1_rx_config, 0, 
          sizeof( qvp_rtp_evrc_comp_bund_sdp_config_type ) ); 
  memset( evrc1_tx_config, 0, 
          sizeof( qvp_rtp_evrc_comp_bund_sdp_config_type ) );
  
  /*------------------------------------------------------------------------
    Copy generic attributes and flag them as TRUE
  ------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Copy  FMTP and flag them as TRUE
  ------------------------------------------------------------------------*/

 /*------------------------------------------------------------------------
    RX codec rate
  ------------------------------------------------------------------------*/
  evrc1_rx_config->fixedrate_valid = TRUE;
  evrc1_rx_config->fixedrate = evrc1_config->rx_fixedrate;

  evrc1_rx_config->valid = TRUE;

 /*------------------------------------------------------------------------
    TX codec rate
  ------------------------------------------------------------------------*/
  evrc1_tx_config->fixedrate_valid = TRUE;
  evrc1_tx_config->fixedrate = evrc1_config->tx_fixedrate;

  evrc1_tx_config->valid = TRUE;  
  
  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_evrc1_profile_copy_config */ 

/*===========================================================================

FUNCTION  QVP_RTP_EVRC1_PROFILE_SHUTDOWN


DESCRIPTION
  Shuts this module down and flag intialization as false

DEPENDENCIES
  None

ARGUMENTS IN
  len - length of the parload

RETURN VALUE
  toc type


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_evrc1_profile_shutdown( void )
{
  uint32 count = 0;
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Lets not do multiple shutdown
  ------------------------------------------------------------------------*/
  if( !evrc1_initialized )
  {
    return;
  }
  
  /*------------------------------------------------------------------------
      Walk through the stream array and close all channels
  ------------------------------------------------------------------------*/
  for( count = 0; count < evrc1_profile_config.num_streams; count ++ )
  {

    
    /*----------------------------------------------------------------------
        If this stream is open close it now
    ----------------------------------------------------------------------*/
    if ( qvp_rtp_evrc1_array[ count ].valid )
    {
      qvp_rtp_evrc1_profile_close( ( qvp_rtp_profile_hdl_type ) count );
    }
    
  } /* end of for count = 0  */
  
  /*------------------------------------------------------------------------
    free the array of streams
  ------------------------------------------------------------------------*/
  qvp_rtp_free( qvp_rtp_evrc1_array   );

  /*------------------------------------------------------------------------
    Flag array as NULL and init as FALSE
  ------------------------------------------------------------------------*/
  qvp_rtp_evrc1_array = NULL; 
  evrc1_initialized  = FALSE;
  
  
} /* end of function qvp_rtp_evrc1_profile_shutdown  */


#endif /* end of FEATURE_QVPHONE_RTP */
