#ifndef _H_QVP_RTP_TEXT_H_
#define _H_QVP_RTP_TEXT_H_
/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                         QVP_RTP_TEXT_PROFILE . H

GENERAL DESCRIPTION

  This file contains the implementation of text profile. text profile acts 
  as conduite to RTP layer. The RFC which is based on is RFC4301.

EXTERNALIZED FUNCTIONS
  None.


INITIALIZATION AND SEQUENCING REQUIREMENTS
  Need to init and configure the profile before it becomes usable.



  Copyright (c) 2013 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/ps_media/rtp_profile/src/qvp_rtp_text_profile.h#1 $ $DateTime: 2016/03/28 23:03:22 $ $Author: mplcsds1 $

                            EDIT HISTORY FOR FILE


when        who          what, where, why
--------    --------    ----------------------------------------------------------
03/08/09    pradeepg    Initial Creation.
===========================================================================*/
#include "customer.h"
#include "qvp_rtp.h"
#include "qvp_rtp_profile.h"
#ifdef FEATURE_QVPHONE_RTP
#define QVP_RTP_MAX_T140_BUFF 100
#define QVP_RTP_MAX_NUM_CHAR 15
#define QVP_RTP_TEXT_DFLT_RED_PKT 2       /* BY default this is considered 
                                              to be 2*****DO NOT CHANGE THIS VALUE****/
#define	BOM			0xFEFF
#define	BOM_R		0xFFFE
#define MSB_CHECK	0xFF00
#define	MISS_TXT	0xFFFD
#define TTY_NEW_LINE 0X2028
#define TTY_NEW_LINE_UTF8 0xa880e200
#define	TTY_CR			0x000D
#define	TTY_LF			0x000A


#define QVP_RTP_TXT_DFLT_NUM_RED 0
#define QVP_RTP_TXT_PYLD_MIN	96
#define QVP_RTP_TXT_PYLD_MAX	128

/*--------------------------------------------------------------------------
    TYPEDEF STRUCT QVP_RTP_TEXT_CONFIG_TYPE

    This strcture stores the configuration for a particular channel 
    the text channel.
--------------------------------------------------------------------------*/
typedef struct
{
  
  /* receiver settings */
  uint8		cps;                /* character transmission rate */
  uint8		num_red_pkt;        /* Maximum number of redundant blocks */
  uint8  red_payload;		/* dynamic payload number for redundant*/
  uint8  t140_payload;		/* dynamic payload number for t140 block*/
  uint8  new_red_pkt; /* new redundacy level set which will be set 
                        after the idle period */
  
} qvp_rtp_text_config_type;

typedef struct 
{
 uint32 ts;
 uint8 count;
 uint8 t140_char[QVP_RTP_MAX_NUM_CHAR];
}rtp_t140_data;

//This is the stucture that contains the sequence number
//and the t140 character details an rtp pkt
typedef struct
{
	uint32	seqid;		/* The sequence number of the packet */
	uint16*	t140Char;	/* The actual t140 character array sent in the packet.*/
	uint8	numChars;
  uint8 block_size;
}rtp_txt_pkt_data;

//This struct represents the Node contained in the
//queue to CVD or the buffer queue while waiting to 
//recover a lost pkt
struct txt_queue_node
{
	rtp_txt_pkt_data*			pkt;
	struct txt_queue_node*		next;
};

typedef struct txt_queue_node rtp_txt_queue_node;


struct range
	{
		uint16			min;
		uint16			max;
		struct range	*next;
	};
/*--------------------------------------------------------------------------
    TYPEDEF STRUCT QVP_RTP_TEXT_RX_CTX

    This strcture stores the receiver context for the text channel.
--------------------------------------------------------------------------*/
typedef struct
{
	
	int prev_seq;
	rtp_txt_queue_node* buff_head;
	rtp_txt_pkt_data	red_blocks[QVP_RTP_TEXT_DFLT_RED_PKT];
	boolean skip_char;
	boolean first_pkt;
	boolean byte_order;
	struct range		*missing_pkts_head;
	uint16				missing_pkts_id;
	uint16				marker_index;
	uint8				num_red_pkts;
	uint8				timer_id;
	
} qvp_rtp_text_rx_ctx_type;

typedef struct range qvp_rtp_text_seq_range;
/*--------------------------------------------------------------------------
    TYPEDEF STRUCT QVP_RTP_TEXT_TX_CTX

    This strcture stores the receiver context for the g722 channel.
--------------------------------------------------------------------------*/
typedef struct
{
  
  boolean valid;              /* indicates if the ctx is valid  */
  boolean firstPkt;           /* set to true for first pkt type */
  boolean marker;                      /* marker bit turned on */
  boolean moveIndex;          /*Need to insert into next block  */
  uint8 redIndex;             /* index for redundant blocks     */
  uint8 emptyIndex;           /* insertion index for new block  */
  uint8 text_tx_timer;				/* Timer used to control transmission 
                                     *  based on the cps*/
  uint8 max_elements;        /*Number of elements per t140 block*/
  uint8 num_emt_pkts;        /*count of num MT pkts transmitted */
  uint32 lastPkt_ts;         /*user for inserting the MT pkt    */
  uint32 num_tx_pkts;        /*used for pkts tx'ed between idle */
  boolean mt_t140;           /*flag when ever MT is transmitted */
  rtp_t140_data *t140_txBuff[QVP_RTP_MAX_T140_BUFF];/*tx buffer incase the
                                                     transmission rate is 
                                                   less than 160ms/char */
} qvp_rtp_text_tx_ctx_type;


/*--------------------------------------------------------------------------
    TYPEDEF STRUCT QVP_RTP_TEXT_CTX_TYPE

  The total Text context maintained in each channel
--------------------------------------------------------------------------*/
typedef struct
{
  
  boolean valid;
  qvp_rtp_profile_usr_hdl_type *usr_hdl;    /* user handle to be sent in CB*/
  qvp_rtp_text_config_type rx_stream_config;   /* the way the profile is 
                                             * configured
                                             */
  qvp_rtp_text_config_type tx_stream_config;   /* the way the profile is */ 
  qvp_rtp_text_rx_ctx_type  rx_ctx;          /* receiver context */
  qvp_rtp_text_tx_ctx_type  tx_ctx;          /* transmitter context */
  
} qvp_rtp_text_ctx_type;

/*---------------------------------------------------------------------------
	This structure contains the rx_ctx for a give app_id - stream_id
 --------------------------------------------------------------------------*/
struct codec_text_ctx_type
{
	qvp_rtp_app_id_type					app_id;
	qvp_rtp_stream_id_type				stream_id;
	qvp_rtp_text_rx_ctx_type*			rx_ctx;
	qvp_rtp_text_config_type*			stream_config;
	qvp_rtp_stream_type*				stream;
	struct codec_text_ctx_type*			next;
};

typedef struct codec_text_ctx_type qvp_rtp_codec_text_ctx_type;

void qvp_rtp_handle_incoming_text_pkt_cb
(
 qvp_rtp_app_id_type     app_id,      /* RTP Application ID*/
 qvp_rtp_stream_id_type   stream_id,    /* stream id through which the
                                        * packet came
                                        */
 qvp_rtp_recv_pkt_type    *pkt       /* packet which is being delivered */
 );

void qvp_rtp_text_process_buffq(uint16 iseq_upto);

qvp_rtp_codec_text_ctx_type* qvp_rtp_codec_text_get_ctx(qvp_rtp_app_id_type app_id,	qvp_rtp_stream_id_type stream_id);

void qvp_rtp_text_buffq_add(qvp_rtp_text_rx_ctx_type* rx_ctx, rtp_txt_queue_node* pNode);
rtp_txt_queue_node* qvp_rtp_text_buffq_getpkt(qvp_rtp_text_rx_ctx_type* rx_ctx);
void qvp_rtp_text_update_missing_pkts(qvp_rtp_text_rx_ctx_type* rx_ctx, qvp_rtp_text_seq_range* pNewList,qvp_rtp_text_seq_range* pNodeToReplace );
void qvp_rtp_text_profile_recv_process
(                                        
 qvp_rtp_recv_pkt_type    *pkt       /* packet which is being delivered */
 );

boolean qvp_rtp_text_unpack_pkt(rtp_txt_pkt_data* pData, qvp_rtp_recv_pkt_type* pkt);
void qvp_rtp_text_recover_pkt(qvp_rtp_recv_pkt_type* pkt, rtp_txt_pkt_data* pData);
void qvp_rtp_text_timeout_exp(void* param);
//qvp_rtp_status_type rtp_codec_txt_rx_ctx_init(qvp_rtp_stream_type* stream, qvp_rtp_text_ctx_type* txt_ctx);
//qvp_rtp_status_type rtp_codec_txt_rx_ctx_uninit(qvp_rtp_stream_type* stream);
void qvp_rtp_text_get_t140_block(uint16* pdest, uint8* psource, uint8 sizeof_t140_block, boolean byte_order);
uint8 qvp_rtp_unpack_t140_block(uint8* t140Block, uint8* header, uint16* parse_offset, uint8 num_chars);
void qvp_rtp_text_get_byte_order(qvp_rtp_text_rx_ctx_type *rx_ctx, uint8* t140_primary);
void qvp_rtp_text_stream_send_byteorder
(
 qvp_rtp_app_id_type app_id, 
 qvp_rtp_stream_id_type stream_id
);
void qvp_rtp_text_send_rx_pkt(rtp_txt_queue_node* pNode);
qvp_rtp_status_type qvp_rtp_txt_rx_ctx_init(qvp_rtp_text_ctx_type* text_context);
qvp_rtp_status_type qvp_rtp_txt_rx_ctx_uninit(qvp_rtp_text_ctx_type* text_context);
#endif /* end of FEATURE_QVPHONE_RTP */
#endif /* end of  _H_QVP_RTP_text_H_ */
