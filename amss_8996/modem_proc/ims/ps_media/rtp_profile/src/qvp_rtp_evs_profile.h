#ifndef _H_QVP_RTP_EVS_H_
#define _H_QVP_RTP_EVS_H_
/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                         QVP_RTP_EVS_PROFILE . H

GENERAL DESCRIPTION

  This file contains the implementation of EVS profile. EVS profile acts 
  as conduite to RTP layer.

EXTERNALIZED FUNCTIONS
  None.


INITIALIZATION AND SEQUENCING REQUIREMENTS
  Need to init and configure the profile before it becomes usable.



  Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/ps_media/rtp_profile/src/qvp_rtp_evs_profile.h#1 $ $DateTime: 2016/03/28 23:03:22 $ $Author: mplcsds1 $

                            EDIT HISTORY FOR FILE


when        who         what, where, why
--------    ---         ----------------------------------------------------------
05/08/2015  manjunat    Initial creation.

===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP

#define QVP_RTP_EVS_PKT_INTERVAL 320  /* 1/16000 of a second. so 20 ms =
                                       * 20 / ( 1/16 ) = 320 units
                                       */

#define QVP_RTP_DFLT_BUNDLE_SIZE       3 /* Default bundle size */
#define QVP_RTP_MAX_BUNDLE_SIZE       12  /* Max bundle size */
#define QVP_RTP_EVS_DFLT_PTIME       20

#define QVP_RTP_FRAME_BLOCKS_1					1
#define	QVP_RTP_FRAME_BLOCKS_2					2

#define QVP_RTP_NUM_MS_PER_FRAME				20			
#define QVP_RTP_EVS_HL_FIXED_HDR_SIZE  1 /* QC fixed header size in EVS
                                          * HF packing
                                          */
/*--------------------------------------------------------------------------

  TYPEDEF ENUM QVP_RTP_EVS_PRIM_TOC_TYPE
  
EVS mode bit 
(1 bit)	Unused
(1 bit)	EVS bit rate	Indicated EVS mode and bit rate
0	0	0000	Primary 2.8 kbps
0	0	0001	Primary 7.2 kbps
0	0	0010	Primary 8.0 kbps
0	0	0011	Primary 9.6 kbps
0	0	0100	Primary 13.2 kbps
0	0	0101	Primary 16.4 kbps
0	0	0110	Primary 24.4 kbps
0	0	0111	Primary 32.0 kbps
0	0	1000	Primary 48.0 kbps
0	0	1001	Primary 64.0 kbps
0	0	1010	Primary 96.0 kbps
0	0	1011	Primary 128.0 kbps
0	0	1100	Primary 2.4kbps SID
0	0	1101	For future use
0	0	1110	SPEECH_LOST
0	0	1111	NO_DATA

  
--------------------------------------------------------------------------*/
typedef enum
{
  
  QVP_RTP_EVS_PRIM_FT_RATE_2_8   = 0, /* 2.8 kbps */ 
  QVP_RTP_EVS_PRIM_FT_RATE_7_2   = 1, /* 7.2 kbps */ 
  QVP_RTP_EVS_PRIM_FT_RATE_8_0   = 2, /* 8.0 kbps  */
  QVP_RTP_EVS_PRIM_FT_RATE_9_6   = 3, /* 9.6 kbps  */ 
  QVP_RTP_EVS_PRIM_FT_RATE_13_2  = 4, /* 13.2 kbps  */ 
  QVP_RTP_EVS_PRIM_FT_RATE_16_4  = 5, /* 16.4 kbps */ 
  QVP_RTP_EVS_PRIM_FT_RATE_24_4  = 6, /* 24.4 kbps */ 
  QVP_RTP_EVS_PRIM_FT_RATE_32_0  = 7, /* 32.0 kbps */ 
  QVP_RTP_EVS_PRIM_FT_RATE_48_0  = 8, /* 48.0 kbps */   
  QVP_RTP_EVS_PRIM_FT_RATE_64_0  = 9, /* 64.0 kbps */     
  QVP_RTP_EVS_PRIM_FT_RATE_96_0  = 10, /* 96.0 kbps */       
  QVP_RTP_EVS_PRIM_FT_RATE_128_0 = 11, /* 128.0 kbps */         
  QVP_RTP_EVS_PRIM_FT_RATE_SID   = 12, /* silence ind - 1.95 kbps */ 
  QVP_RTP_EVS_PRIM_FT_RATE_RSRVD = 13, /* Future use */   
  QVP_RTP_EVS_PRIM_FT_RATE_LOST  = 14, /* Speech Lost */     
  QVP_RTP_EVS_PRIM_FT_RATE_NOD   = 15, /* No data zero len packet 0kbps */ 
  QVP_RTP_EVS_PRIM_FT_INVALID    = 0xff /* notifies invalid */
  
} qvp_rtp_evs_toc_type;

typedef enum bwinfo
{
  PROFILE_BW_UNK = -1,
  PROFILE_BW_NB,
  PROFILE_BW_WB,
  PROFILE_BW_SWB,
  PROFILE_BW_FB,
}qvp_rtp_profile_bw_info;

/*--------------------------------------------------------------------------
    TYPEDEF STRUCT QVP_RTP_EVS_CONFIG_TYPE

    This strcture stores the configuration for a particular channel 
    the EVS channel.

    Not all the configuration options are supported for the time being.
    All the capabilities of the layer is listed here. Any signalling
    entity tries misconfiguration will be dealt with error codes/exceptions. 
--------------------------------------------------------------------------*/
typedef struct
{
  
  boolean is_evs_primary;        
  boolean is_tx_hf;
  qvp_rtp_profile_bw_info tx_bw_info;
  qvp_rtp_profile_bw_info rx_bw_info;
  boolean is_rx_hf;

  uint16 rx_ptime;
  uint16 rx_max_ptime;


  boolean tx_header_on;                /* header less payload */
  boolean tx_crc_on;                   /* flags presence of TX crc */
  uint8   tx_interleave_on;            /* flags transmit interleaving */
  uint8   tx_bundle_size;              /* bundle size in no of 20 ms 
                                        * frames 
                                        */ 

  uint16 tx_ptime;
  uint16 tx_max_ptime;

  uint8 tx_cmr;                         /* CMR to be set in tx packet, recievers capability */

  uint32 ob_codec_modes;                      /* bit mask representaion of modes negotiated in SDP in call set up */
  uint8  prev_rx_D;
  uint8  cur_rx_D;
  boolean first_rx_frame;

  uint8 highest_mode;                   /* Variable for maintaining the highest mode */										  
  uint16 rx_mode_change_period;
  

   boolean                      ch_aw_mode_enabled;
   int8                         tx_red_offset;/*channel-aware mode*/

   uint8                        cmr_hdr_sz;
   
} qvp_rtp_evs_config_type;

/*--------------------------------------------------------------------------
    TYPEDEF STRUCT QVP_RTP_EVS_RX_CTX

    This strcture stores the receiver context for the EVS channel.
--------------------------------------------------------------------------*/
typedef struct
{
  boolean valid;
  uint16  pkt_interval;
  uint8 codecrate;
  boolean firstspeechframe;             /*First speech frame in rx context*/
  uint32  redtstamp;                    /* last redundant tstamp*/  
} qvp_rtp_evs_rx_ctx_type;

/*--------------------------------------------------------------------------
    TYPEDEF STRUCT QVP_RTP_EVS_TX_CTX

    This strcture stores the receiver context for the EVS channel.
--------------------------------------------------------------------------*/
typedef struct
{
  
  boolean valid;
  uint8 frame_cnt;         /* number of frames which got queued up in the
                            * stream when frame_cnt becomes bundle size we 
                            * send a packet out
                            */
  uint8 op_packet[ QVP_RTP_MTU_DLFT ]; /* tx headr s preconfigured 
                                        * statically * and then stuffed 
                                        * in every packet 
                                        */
  boolean marker;                      /* marker bit turned on */

  uint16 data_len;                     /* accumulated data len */
 
  uint32 first_tstamp;                 /* since the profile sends the time
                                        * stamp of the first packt in a 
                                        * bundle we need to store it
                                        */
  boolean prev_silence_frame;          /* Indicates if the previous frame
                                        * sent is a silence frame 
                                        */
  boolean all_silence_frames;          /* Indicates if the RTP packet 
                                        * contains all silence frames 
                                        */
} qvp_rtp_evs_tx_ctx_type;


/*--------------------------------------------------------------------------
    TYPEDEF STRUCT QVP_RTP_EVS_CTX_TYPE

  The total EVS context maintained in each channel
--------------------------------------------------------------------------*/
typedef struct
{
  
  boolean valid;
  qvp_rtp_evs_config_type stream_config;    /* the way the profile is 
                                             * configured
                                             */
  qvp_rtp_evs_rx_ctx_type  rx_ctx;          /* receiver context */
  qvp_rtp_evs_tx_ctx_type  tx_ctx;          /* transmitter context */
  
} qvp_rtp_evs_ctx_type;

#endif /* end of FEATURE_QVPHONE_RTP */
#endif /* end of  _H_QVP_RTP_EVS_H_ */
