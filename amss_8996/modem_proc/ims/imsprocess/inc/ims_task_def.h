/*============================================================================
FILE: ims_task_def.h

DESCRIPTION:
   This file contains IMS task definitions.

INITIALIZATION AND SEQUENCING REQUIREMENTS:  Not Applicable

        Copyright � 2014 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
 Revision History
 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 26-Feb-2010    sameer                 -     - Moving the task code out of fwapi and
                                               into process directory
                                             - Featurizing code
--------------------------------------------------------------------------------
 25-Jul-2011    Girish                 -     Domain selection API's for qipcall
--------------------------------------------------------------------------------
 23-Jan-2012    sameer              332423   Added new signal to handle events from the
                                             QMI framework
--------------------------------------------------------------------------------
 20-Feb-2012    Shashank            338129   Added new signal to handle events from the
                                             QMI framework
--------------------------------------------------------------------------------
 23-Apr-2012    Shashank            354739   Changing task signal numbers
--------------------------------------------------------------------------------
 01-Apr-2012    Shashank            351889   Added new signal to handle events from the
                                             QMI framework
--------------------------------------------------------------------------------
 19-Feb-2013    Girish              443912   DS:READY changes as part of RCINIT changes in IMS
--------------------------------------------------------------------------------------------------
 20-Mar-2014    Girish              605791   ATP/IMS co-existence
-------------------------------------------------------------------------
 11-Feb-2015    Dipak               766106   FR 24984: SIM Swap without RESET
-------------------------------------------------------------------------------------------
 15-May-2015    Dipak               819564   FR 27276: IMS/DDS switch across subs in 7-modes DSDS
--------------------------------------------------------------------------
 20-May-2015    Shashank            826013   Added support for LOC client
-----------------------------------------------------------------------------------------------
05-may-2014     chinmay             831360    changed signum for IMS_QMI_REGISTRATION_APPS_SERVICE_HANDLE_SIG
============================================================================*/
#ifndef _IMS_TASK_DEF_H_
#define _IMS_TASK_DEF_H_


#ifdef __cplusplus
extern "C"
{
#endif

/*===========================================================================
 
                     INCLUDE FILES FOR MODULE
 
===========================================================================*/
#include "rex.h"

typedef enum
{
  IMSTASK_STATE_INITIAL          = 0, /* initial task creation */
  IMSTASK_STATE_STARTED          = 1, /* TASK_START_SIG recieved */
  IMSTASK_STATE_STOPPED          = 2, /* TASK_STOP_SIG recieved */
  IMSTASK_STATE_OFFLINE          = 3, /* TASK_OFFLINE_SIG received */
  IMSTASK_STATE_FAILED           = 4, /* task encountered unrecoverable error*/
  IMSTASK_STATE_MAX
} imstask_state_enum_type;

typedef int (*imstask_cmd_handler_fn_type)(void *);

typedef void (*imstask_state_cb)
( 
   imstask_state_enum_type state,
   uint32 user_data
);

typedef void (*imstask_media_event_handler_fn_type)
(
  void* session_ptr,
  void* stream_ptr,
  int   event_id
);

/* rex signals */
/* TODO: jjg- this is a totally arbitrary non-zero signal. */
#define IMS_CMD_DONE_SIG                                   0x01000000
#define IMS_RPT_TIMER_SIG                                  0x0001
#define IMS_CLIENT_CMD_Q_SIG                               0x0080
#define IMS_DS_READY_SIG                                   0x0008
#define IMS_MSGR_Q_SIG                                     0x02000000
#define IMS_QMI_SETTINS_SERVICE_HANDLE_SIG                 0x04000000
#define IMS_QMI_PRESENCE_SERVICE_HANDLE_SIG                0x08000000
/*
note: this is workaround. crypto team needs to fix. Fix is only for requested PL/CPL
please contact ims dpl before propagating to other pls.
*/
#define IMS_QMI_REGISTRATION_APPS_SERVICE_HANDLE_SIG       0x800000
#define IMS_QMI_DCM_SERVICE_AVAILABLE_SIG                  0x20000000
#define IMS_QMI_DCM_SEND_MSG_WAIT_SIG                      0x40000000
#define IMS_QMI_DCM_SRV_UP_SIG                             0x80000000
#define IMS_QMI_DCM_TIMER_WAIT_SIG                         0x0004
#define IMS_QMI_LOC_SERVICE_AVAILABLE_SIG                  0x00001000
#define IMS_QMI_LOC_SEND_MSG_WAIT_SIG                      0x00002000
#define IMS_QMI_LOC_SRV_UP_SIG                             0x00004000
#define IMS_QMI_LOC_TIMER_WAIT_SIG                         0x00008000
#ifdef FEATURE_MODEM_CONFIG_REFRESH
#define IMS_MCFG_REFRESH_SIG                               0x0010
#endif//FEATURE_MODEM_CONFIG_REFRESH
#define IMS_DPL_SLOT_CHANGE                                0x0020  // reserved for slot change sig IMS_DPL_SLOT_CHANGE_SIG

#define IMS_DPL_ID_REQUIRED   0x01

typedef void* imstask_dpl_id_type;
#define IMSTASK_INVALID_DPL_ID NULL

typedef void* imstask_state_cb_handle_type;
#define IMSTASK_INVALID_CB_HANDLE NULL

typedef int (*imstask_cmd_fn_type)
(
  imstask_cmd_handler_fn_type   cmd_fn,
  void*                         data_ptr,
  rex_sigs_type                 response_sig,
  imstask_dpl_id_type           dpl_id
);

#ifdef __cplusplus
}
#endif

#endif /* _IMS_TASK_DEF_H_ */
