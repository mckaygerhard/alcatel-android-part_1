/***********************************************************************/
/* $Author: mplcsds1 $ */
/* $DateTime: 2016/03/28 23:03:22 $ */
/* $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/imsqmi/inc/ims_qmi_loc_client.h#1 $ */

/* ims_qmi_loc_client.h
 * The IMS Settings Service Module for remote apps settings manipulation client.
 *
 * Copyright (C) 2011 Qualcomm Technologies, Inc.
 *
 * This file provides implementation for service module that processes ims settings
 * requests from the client module running on Apps proc. 
 * It does this by remoting these calls to the modem proc over QMI using QCCI framework.
 * With each request from the client the service task gets signalled which then invokes the service module.
 * The service proceses requests on the modem and sends responses to the client through the QCSI framework.
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

 Revision History
========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
========================================================================
 20-May-2015    Shashank           826013      Added support for LOC client
===========================================================================*/

#ifndef __IMS_QMI_LOC_CLIENT_H__
#define __IMS_QMI_LOC_CLIENT_H__

#include "ims_variation.h"
#include "customer.h"

#ifdef FEATURE_IMS_QMI_LOC_QCCI_CLIENT

#include "qmi_csi.h"
#include "qpdpl_qmi_defs.h"
#include "qmi_cci_target_ext.h"

typedef enum
{
  QMI_LOC_CLIENT_REG_SUCCESS,
  QMI_LOC_CLIENT_REG_PENDING,
  QMI_LOC_CLIENT_REG_FAILURE,
  QMI_LOC_CLIENT_REG_SERVICE_LOST
} QMI_LOC_CLIENT_REG_STATUS;

/*
   Call back function that needs to be registered to get the QCCI events notified.
   Depending on the response type, application should typecast the evt_info_ptr.
*/
typedef void (*ims_qmi_loc_ev_notify_f_type) (unsigned int msg_id, uint8 bIsResp, void* evt_info_ptr, void* cb_user_data, void* cb_qmi_data);

/*===========================================================================
  FUNCTION IMS_QMI_REG_SETTINGS_EV_CALLBACK()

  DESCRIPTION
    API to register application callback for notifying QMI events
    
  PARAMETERS
    qmi_settings_msg_cb_fn_ptr  : callback function
    cb_settings_user_data       : user data

  RETURN VALUE
    none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
QMI_LOC_CLIENT_REG_STATUS ims_qmi_reg_loc_ev_callback(ims_qmi_loc_ev_notify_f_type qmi_loc_msg_cb_fn_ptr, void* cb_loc_user_data);

/*===========================================================================
  FUNCTION IMS_QMI_DEREG_SETTINGS_EV_CALLBACK()

  DESCRIPTION
    API to register application callback for notifying QMI events
    
  PARAMETERS
    None

  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void ims_qmi_dereg_loc_ev_callback(void);

/*===========================================================================
  FUNCTION  IMS_QMI_SETTINGS_SERVICE_SEND_MSG_TO_AP()

  DESCRIPTION
    Send the response/indication requested by modem to AP
    
  PARAMETERS
    eMsgId   : msg information, as received from Presence
    pData    : data associated with msg

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
extern int ims_qmi_loc_client_send_msg_to_ap(QCE_QMI_MESSAGE_IDS eMsgId, void* pData);

/**************************************************************************//**
 * @par Name:
 *   ims_qmi_settings_service_init - Initialize the ims_qmi_settings service module.
 *
 * @par Synopsis:
 *  @code
 *    #include "ims_qmi_settings_service.h"
 *    qmi_csi_os_params os_params;
 *    os_params.tcb = rex_self ();
 *    os_params.sigs = IMS_QMI_SETTINS_SERVICE_HANDLE_SIG
 *    void ims_qmi_settings_service_init (os_params);
 *  @endcode
 *
 * @par Description:
 *  Initializes static objects and registers callbacks with the QCSI framework.
 *  Initilazed by the service task at startup.
 *
 * @param os_params
 *  qmi_csi_os_params structure to hold the service task's tcb and signals used
 *  to signal any event from clients like connection request, disconnection
 *  request and process message request.
 *
 * @return
 *  Returns 0 on success and -1 on error.
 *
******************************************************************************/
int ims_qmi_loc_client_init(qmi_csi_os_params os_params);

/**************************************************************************//**
 * @par Name:
 *  ims_qmi_settings_service_handle_event - Handler called when service task receives the
 *   handle event signal registered with the QCSI framework at initialization.
 *
 * @par Synopsis:
 *  @code
 *    #include "ims_qmi_settings_service.h"
 *    void ims_qmi_settings_service_handle_event (void);
 *  @endcode
 *
 * @par Description:
 *  Calls the handle_event function on the framework to get appropriate
 *  callback invoked based on client action.
 *
 * @param os_params
 *  os_param registered during initialization.
 *
 * @return
 *  Returns 0 on success and -1 on error.
 *
******************************************************************************/
int ims_qmi_loc_client_handle_event(qmi_client_os_params os_params);

/**************************************************************************//**
 * @par Name:
 *   ims_qmi_settings_service_close - Closes connection with the QCSI framwork.
 *
 * @par Synopsis:
 *  @code
 *    #include "ims_qmi_settings_service.h"
 *    void ims_qmi_settings_service_close (void);
 *  @endcode
 *
 * @par Description:
 *  Releases any handle provided by the framwork and unregisters all signals
 *  and callbacks registered at initialization. No more events from the clients
 *  will invoke the service.
 *
 * @return
 *  Returns 0 on success and -1 on error.
******************************************************************************/
int ims_qmi_loc_client_close(void);

#endif /* FEATURE_IMS_QMI_LOC_QCCI_CLIENT */

#endif /* not __IMS_QMI_LOC_CLIENT_H__ */
