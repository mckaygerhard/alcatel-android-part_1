/******************************************************************************
Copyright (c) 2013 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.


File Name      : qpIMSPolicyManager.h
Description    : Interface file to be used by application
                 to get the configuration which PM will
                 read from multiple sources and return it
                 to applications
				 
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/policymanager/inc/qpIMSPolicyManager.h#1 $
$DateTime: 2016/03/28 23:03:22 $
$Author: mplcsds1 $

Revision History
==============================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
==============================================================================
 12-Jun-2013   Sankalp            481175         First version
----------------------------------------------------------------------------------------------
 14-Sep-2013   ggrewal            448374         Add support for emergency transaction timers
------------------------------------------------------------------------------
 13-Sep-2013   Priyank            543915         FR 3104: IMS on Wifi
 -------------------------------------------------------------------------------
 02-Jan-2014   Sankalp            584251         FR 18905: Camping on the network not 
                                                 supporting IMS PS voice
 ----------------------------------------------------------------------------------------------
 02-Jan-2014   Priyank            553180         All FT support to be implemented.
------------------------------------------------------------------------------- 
08-May-2014    Rohit                587830     Providing for Timer D to be configurable using NV Item
-------------------------------------------------------------------------------
28-Aug-14      Purna              FR22881       IMS PDN and IMS Registration Management for CMCC
-------------------------------------------------------------------------------
29-Aug-14      Purna              FR22955      User Based WiFi Calling Enable/Disable Handling
-------------------------------------------------------------------------------
05-Sep-14      Purna              FR21423      TMO Wi-Fi Preference and Wi-Fi calling impact on IMS Registration and Policy Manager
---------------------------------------------------------------------------
01-26-14  Manasi     779236   Support for TDSCDMA RAT
-------------------------------------------------------------------------------
02-14-15     c_snaras              767931     FR 24213: VzW - Handling APN related parameter change when connected to IMS APN
--------------------------------------------------------------------------------------------------
04-30-15       c_snaras          827644      FR 27427: [T-Mobile Wifi Calling Requirement] Handling of non-T-Mobile SIM or a non-GBA capable SIM card 
--------------------------------------------------------------------------------------------------
13-07-15    Sreenidhi     865526   Sub Oprt 7 - DUT is getting eregistered services = 0 after completion of registration in IPv6
--------------------------------------------------------------------------------------------------
05-20-15      c_snaras        838306      FR 26384: ATT - VoLTE, VT, RCS Messaging Enable/Disable requirements
-------------------------------------------------------------------------------
25-Aug-15   Priyank       895110      IMS - RM to pass resolved APN information to all its Client
--------------------------------------------------------------------------------------------------
21-Sep-15     Priyank    907270       After IP fall back, UE is registering with the previously registering IP type during LPM on/off
************************************************************************/


#ifndef __QP_IMS_POLICY_MANAGER__
#define __QP_IMS_POLICY_MANAGER__

#include <qpSipCommon.h>
#include <qpdcm.h>
#include <qpdefines.h>
#include <qpdefinesCpp.h>
#include <qpConfigNVItem.h>

/******************************************/
/*               ENUMS                    */
/*           IMS SERVICES                 */
/* The below enums to be used as an index
   to know which service is enable or disable
*/
typedef enum qpServiceType
{
  QP_VOLTE         = 0,        /*VOLTE service indication*/
  QP_VT            = 1,        /*VT service indication*/
  QP_SMS           = 2,        /*SMS service indication*/
  QP_IM            = 3,        /*Instant message service indication*/
  QP_VS            = 4,        /*VS service indication*/
  QP_IS            = 5,        /*Image share service indication*/
  QP_MSRP          = 6,        /*MSRP service indication*/
  QP_GL            = 7,        /*Geo Location service indication*/
  QP_PRESENCE      = 8,        /*Presence service indication*/
  QP_FT            = 9,        /*File transfer service indication*/
  QP_RCS_ALL       = 10,       /*All RCS enable service indication*/
  QP_SM            = 11,
  QP_DEFAULT_SRV   = 15,       /*If default service is enable in that case operator mode will take a preference*/
  QP_MAX_SERVICE   = 16
}QP_SERVICE_TYPE;

/*
** Store the location from where
** any reg parameter is picked.
*/
typedef enum qpRegLoc
{
  ePM_ISIM              = 1,
  ePM_NV                = 2,
  ePM_ACS               = 3,
  ePM_PCO               = 4
}QP_IMS_REG_LOC;

/*
** Security Type
*/
typedef enum eQp_SecurityType
{
  QPE_SEC_TYPE_NONE            = 0,
  QPE_SEC_TYPE_TLS             = 1,
  QPE_SEC_TYPE_TLS_SEC_AGREE   = 2,
}QPE_SECURITY_TYPE;

/*
** IMS Priority for different source of data
** like ACS/ISIM/NV/PCO
*/
typedef struct qpIMSParamPriority
{
  QPUINT8 iACSPriority;     /* Priority sequence of ACS values */
  QPUINT8 iISIMPriority;    /* Priority sequence of ISIM values */
  QPUINT8 iNVPriority;      /* Priority sequence of Pre Config NV */
  QPUINT8 iPCOPriority;     /* Priority sequence of PCO */
}QP_IMS_PARAM_PRIORITY;

/* DEFINES */
#define QP_MAX_APN_TYPE  6
#define QP_GWL_RATS_SIZE  8
/* DEFINES END*/

/* STRUCTS */
/*
  Desc: Description of the structure
  Request Id: Application need to pass
              the req. id to PM and
              based on that PM will perform
              any action
  IN/OUT struct : This tells that the given structure
                  is what app need to provide i.e IN
                  or what PM will return i.e OUT
  OUT struct    : If the structure is IN then what
                  app structure application will get

  IN  struct    : If the structure is of type OUT then
                  wha IN struct app needs to pass
*/

/******************************************/
/*Desc:
  Application will pass the RAT and this is the
  Out structure which applcation will get in return

  REQUEST ID     : QP_IMS_GET_IMS_APN_PRIORITY or
                   QP_IMS_GET_CONFIG_APN_PRIORITY

  IN/OUT struct  : OUT

  IN structure   : QPE_DCM_RAT RAT info
*/
/******************************************/
typedef struct qpAPNPriority
{
  QPE_APN_TYPE       eAPNType[QP_MAX_APN_TYPE];
}QP_APN_PRIORITY_INFO;

/*
  Desc:
  Once application gets the apn fallback
  This is the input param which application
  needs to provide to PM and provide the
  RAT and apn combination to get rest of the
  profile attributes

  REQUEST ID     : QP_IMS_GET_APN_PARAM

  IN/OUT struct  : IN

  OUT structure   : QP_APN_ATTR_INFO
*/
typedef struct qpAPNRAT
{
  QPE_DCM_RAT        eRat;
  QPE_APN_TYPE       eAPNType;
}QP_APN_RAT;

/*
  Desc:
  Once PM gets the rat and apn type it will
  search in configuration and return the
  IP addr type , fall back option , allowed
  services , apn name

  REQUEST ID     : QP_IMS_GET_APN_PARAM

  IN/OUT struct  : OUT

  IN structure   : QP_APN_RAT
*/
typedef struct qpAPNAttr
{
  QPE_IPADDR_TYPE         eIPAddrType;
  QPBOOL                  bIsFallBackEnable;
  QPCHAR                  arrAPNName[QP_IMS_PM_APN_NAME_SIZE];
  QPE_AUTH_SCHEME_CONFIG  eAuthScheme;
  QPE_REGISTRATION_MODES  eRegistrationMode;
  /*QPE_SECURITY_TYPE     eSecurityType;*/
  QPBOOL                  bPriorityIPTypeToBeUsed;
}QP_APN_ATTR_INFO;

/*
  Desc:
  Any given point of time application gets WWAN and WLAN both
  the indicaiton, it can query to PM passing the current rat
  where any given service is present and the rat mask on which
  it wants to check whether the same service needs to be offloaded
  or not.

  REQUEST ID     : QP_IMS_QUERY_WLAN_OFFLOAD_INFO

  IN/OUT struct  : IN

  OUT            : QPE_DCM_RAT
*/
typedef struct qpQueryOffLoadInfo
{
  QPE_DCM_RAT        eCurrRat;
  QPE_DCM_RAT_MASK   iRATMask;
  QP_SERVICE_TYPE    eSrvToCheckFor;
}QP_IMS_QUERY_OFFLOAD_INFO;

/*
  Desc:
  Once PDP is up then applications will get the rat and apn where the
  PDP is currently connected. Applications will use this information
  again using "QP_APN_RAT" as IN param and then the below structure
  will be return to tell application about the allowed services

  REQUEST ID     : QP_IMS_GET_ALLOWED_SERVICES/QP_IMS_GET_ALLOWED_SERVICES_WLAN

  IN/OUT struct  : OUT

  IN structure   : QP_APN_RAT
*/
typedef struct qpAllowedServices
{
  QPBOOL             bAllowedServices[QP_MAX_SERVICE];
}QP_ALLOWED_SERVICE_ON;

/*
  Desc:
  Any given point of time application gets  WWAN and WLAN both
  the indicaiton , it can query all the service priority list
  on WWAN and WLAN and based on that take an action

  REQUEST ID     : QP_IMS_GET_SERVICE_PRIORITY

  IN/OUT struct  : OUT

  IN structure   : NONE
*/
typedef struct qpServicePriority
{
  QPBOOL             bPrioritySrvOnWWAN[QP_MAX_SERVICE];
  QPBOOL             bPrioritySrvOnWLAN[QP_MAX_SERVICE];
}QP_SERVICE_PRIORITY;

/*
  Desc:
  Once services are added and before triggering the registration
  the below parameters needs to be read. RM will call this
  to get the below reg parameters

  REQUEST ID     : QP_IMS_GET_REG_PARAM

  IN/OUT struct  : OUT

  IN structure   : NONE
*/
typedef struct qpRegParam
{
  QPCHAR                 arrSipPublicUserId[QP_DPL_CONFIG_SIP_USER_URI_LEN];
  QPCHAR                 arrSipPrivateUserId[QP_DPL_CONFIG_SIP_USER_URI_LEN];
  QPCHAR                 arrSipUserPassword[QP_DPL_CONFIG_SIP_PROXY_NAME_LEN];
  QPCHAR                 arrSipHomeDomain[QP_DPL_CONFIG_SIP_USER_URI_LEN];
  QPCHAR                 arrSipRealm[QP_DPL_CONFIG_SIP_USER_URI_LEN];
  QPCHAR                 arrSipUserName[QP_DPL_CONFIG_SIP_USER_URI_LEN];
  QPE_REGISTRATION_MODES eRegMode;
  QPE_AUTH_SCHEME        eAuthScheme;
  QP_IMS_REG_LOC         eAttrLocation; /*Location from where the reg. parameters are picked*/
}QP_REG_PARAM;

/*
  Desc:
  This will have data which be filled based on the
  nv/isim/acs priority

  REQUEST ID     : QP_IMS_GET_SIP_PARAM

  IN/OUT struct  : OUT

  IN structure   : NONE
*/
typedef struct qpSipParam
{
  /*ALL timers related data*/
  QPUINT32 iTimer_T1;
  QPUINT32 iTimer_T2;
  QPUINT32 iTimer_T4;
  QPUINT32 iTimer_A;
  QPUINT32 iTimer_B;
  QPUINT32 iTimer_C;
  QPUINT32 iTimer_D;
  QPUINT32 iTimer_E;
  QPUINT32 iTimer_F;
  QPUINT32 iTimer_G;
  QPUINT32 iTimer_H;
  QPUINT32 iTimer_I;
  QPUINT32 iTimer_J;
  QPUINT32 iTimer_K;
  QPUINT32 iTimerEmergency_T1;
  QPUINT32 iTimerEmergency_T2;
  QPUINT32 iTimerEmergency_T4;
  QPUINT32 iTimerEmergency_F;
  QPUINT32 iTimerEmergency_J;
	QPUINT32 iTimerEmergency_D;
}QP_SIP_PARAM;

/*
Desc:
  Any given point of time application can set the service update 
  on defined RATs. This application needs to call for the cases
  where the services are getting updated dynamically

  REQUEST ID     : QP_IMS_SET_SERVICE_STATUS

  IN/OUT struct  : IN

  OUT            : None
*/
typedef struct qpUpdateServiceStatus
{
  QPE_DCM_RAT_MASK   iRATMask;                    /*Provide the RAT ,mask where all these services  
                                                    needs to be modified
                                                  */
  QPBOOL             bIsModified[QP_MAX_SERVICE]; /*If any srvice needs to be modified 
                                                    then that particular service status has
                                                    to set to TRUE
                                                  */
  QPBOOL             bSrvStatus[QP_MAX_SERVICE];  /*Set the service status to TRUE or FALSE*/
}QP_IMS_SRVICE_STATUS_INFO;

/*
  Desc:
  Any given point of time application can query the allowed FTs.
  PM will check which all services are allowed as per the
  device configuration and provide the allowed FTs in the out param.
  The ownership of the malloc'd data is passed to the calling API.

  REQUEST ID     : QP_IMS_GET_ALL_ALLOWED_FT

  IN/OUT struct  : OUT

  IN structure   : NONE
*/
typedef struct qpAllAllowedFTs
{
  QPCHAR*             pServiceFT;
}QP_ALL_ALLOWED_FT;

/*
  Desc:
  Applications query for wifi preference when they receive a
  settings change event.

  REQUEST ID     : QP_IMS_GET_WIFI_PREFS

  IN/OUT struct  : OUT

  IN structure   : NONE
*/
typedef struct qpWifiPrefs
{
  QPE_WFC_STATUS                       wFCStatus;
  QPE_CALL_MODE_PREFERENCE             callModePreference;
}QP_WIFI_PREFS;

/*
  Desc:
  Once services are added and before triggering the registration
  the below parameters needs to be read. RM will call this
  to get the below reg parameters

  REQUEST ID     : QP_IMS_GET_CONFIGURED_DEVICE_TYPE

  IN/OUT struct  : OUT

  IN structure   : None
*/
typedef enum QP_DEVICE_TYPE
{
  QP_DEVICE_TYPE_NONE       = 0,
  QP_DEVICE_TYPE_RCS_E      = 1,
  QP_DEVICE_TYPE_VOLTE      = 2,
  QP_DEVICE_TYPE_RCSE_VOLTE = 3,
  QP_DEVICE_TYPE_DEFAULT    = 4
}QPE_CONFIGURED_DEVICE_TYPE;

typedef struct qpResolvedAPNParams
{
  QPCHAR                 arrResolvedAPNName[QP_APN_NAME_LEN];
}QPE_RESOLVED_APN_PARAMS;

/*Below IDs need to be used by application to query any configuration*/
typedef enum qpRequestId
{
  QP_IMS_GET_IMS_APN_PRIORITY        = 1,  /*Will return the IMS apn fall back based on the input RAT*/
  QP_IMS_GET_CONFIG_APN_PRIORITY     = 2,  /*Will return the Config apn fall back based on the input RAT*/
  QP_IMS_GET_APN_PARAM               = 3,  /*Will return all the params for a profile creation based on the input RAT and APN type*/
  QP_IMS_QUERY_WLAN_OFFLOAD_INFO     = 4,  /*Will return a RAT on which the input service is on priority the rat will be selected from the current rat or rat mask*/
  QP_IMS_GET_ALLOWED_SERVICES        = 5,  /*Will return all the allowed services based on input RAT and APN Type*/
  QP_IMS_GET_ALLOWED_SERVICES_WLAN   = 6,  /*Will return all the allowed services based on input RAT = WLAN*/
  QP_IMS_GET_SERVICE_PRIORITY        = 7,  /*Will return all the service priority on WWAN over WLAN based on the input WWAN RAT*/
  QP_IMS_GET_REG_PARAM               = 8,  /*Will return the reg parameters based on the config priority (ACS NV ISIM)*/
  QP_IMS_GET_SIP_PARAM               = 9,  /*Will return the SIP params which can come from different location based on priority*/
  QP_IMS_GET_RCS_CONFIG_APN_PRIORITY = 10, /*Will return the RCS Http enabler apn*/
  QP_IMS_GET_CONFIGURED_DEVICE_TYPE  = 11, /*Will return the configured device type*/
  QP_IMS_GET_PARAM_PRIORITY          = 12, /*Will return the priority between ISIM/PCO/NV/CONFIG*/
  QP_IMS_SET_SERVICE_STATUS          = 13,  /*Will Set the specific servce status*/
  QP_IMS_GET_ALL_ALLOWED_FT          = 14,  /*Will return all the allowed FTs*/
  QP_IMS_QUERY_WLAN_WWAN_INFO        = 15, /*Will return a RAT on which the input service is on priority the rat will be selected from the current rat or rat mask*/
  QP_IMS_SET_APN_PARAM               = 16, /*Will set the APN specific parameters*/
  QP_IMS_GET_ALL_ALLOWED_RCS_FT      = 17, /*Will set the RCS specific FTS if IPME enabled */
  QP_IMS_SET_RESOLVED_APN_PARAMS     = 18, /*Will set the Resolved APN against NULL APN */
  QP_IMS_GET_RESOLVED_APN_PARAMS     = 19  /*Will return the Resolved APN against NULL APN */
}QP_PM_REQUEST_ID;

/**
*** Class QPConfigurationHandler
*** This is a singleton class.
**/
class QPConfigurationHandler
{
public:
  static  QPConfigurationHandler* GetInstance();
  static  QPVOID Destroy();
  /*application will query the configuration they want to fetch*/
  QPBOOL  qpGetConfiguration(const QP_PM_REQUEST_ID &n_eRequestId, const QPVOID *n_pInputParam, QPVOID *n_pOutParam);

  /* If there is a settings update then it will be notified to PM also
  ** On this call, the stored PM will be updated.
  */
  QPVOID  UpdateConfiguration(const QP_IMS_REG_LOC &n_eUpdatedLoc);
  QPBOOL  qpSetConfiguration(const QP_PM_REQUEST_ID &n_eRequestId, const QPVOID *n_pInputParam, QPVOID *n_pOutParam = QP_NULL);

  QPVOID qpGetPreferredRAT(const QP_WIFI_PREFS &wifiPreferences,const QPE_DCM_RAT_MASK& ratMask,QPE_DCM_RAT& n_eRAT);
  QPVOID SetUESIMGBAStatus(QPBOOL b_pUESIMGBAEnabled);
  QPVOID SwapIMSAPNNameAndIPType(QPCHAR* n_pAPNName, QPUINT8 n_iIPType);
  QPBOOL IsPMAddAllFTsEnabled();
  QPBOOL IsReRegShouldBlockOn3G_2G();
protected: /* Member Functions */
  QPConfigurationHandler();
  QPConfigurationHandler(const QPConfigurationHandler&);
  QPConfigurationHandler& operator= (const QPConfigurationHandler&);
  virtual ~QPConfigurationHandler();

private: /* Member Functions */
  QPVOID Init();
  QPVOID GetPMRATMapped();
  QPVOID GetPMRATFBMapped();
  QPBOOL GetIMSAPNPriority( QPE_DCM_RAT*, QP_APN_PRIORITY_INFO* );
  QPBOOL GetIMSAPNParams( QP_APN_RAT*, QP_APN_ATTR_INFO* );
  QPBOOL CheckIMSServiceToOffLoadOnWLAN( QP_IMS_QUERY_OFFLOAD_INFO*, QPE_DCM_RAT* );
  QPBOOL CheckWifiPrefSettings( QPE_DCM_RAT, QPE_DCM_RAT* );
  QPBOOL CheckWWANandWLAN( QP_IMS_QUERY_OFFLOAD_INFO*, QPE_DCM_RAT* );
  QPBOOL GetIMSConfiguredServices( QP_APN_RAT*, QP_ALLOWED_SERVICE_ON* );
  QPBOOL GetIMSAllowedServices( QP_APN_RAT*, QP_ALLOWED_SERVICE_ON* );
  QPBOOL GetIMSAllowedServicesOnWLAN( QPE_DCM_RAT*, QP_ALLOWED_SERVICE_ON* );
  QPBOOL GetIMSServicePriority( QPE_DCM_RAT*, QP_SERVICE_PRIORITY* );
  QPBOOL GetRCSConfigAPNPriority( QP_APN_PRIORITY_INFO*);
  QPBOOL GetDeviceType(QPE_CONFIGURED_DEVICE_TYPE*);
  QPBOOL GetIMSParamPriority(QP_IMS_PARAM_PRIORITY*);
  QPBOOL GetGenericConfig(const QP_PM_REQUEST_ID &n_eRequestId, const QPVOID *n_pInputParam, QPVOID *n_pOutParam);
  QPBOOL GetIMSSIPParam(QP_SIP_PARAM *n_pOutRes);
  QPBOOL UpdateIMSAPNFBlistBasedOnDeviceType(QPE_DCM_RAT* n_pInputParam,QP_APN_PRIORITY_INFO* n_pOutParam);
  QPBOOL GetWWANRATFromRATMask(const QPE_DCM_RAT_MASK, QPE_DCM_RAT&);
  QPBOOL UpdateIMSSrvStatus(const QP_IMS_SRVICE_STATUS_INFO *n_pSrvStatusInfo);
  QPBOOL GetIMSAllowedFTs(QP_ALL_ALLOWED_FT*);
  QPBOOL GetIMSAllowedRCSFTs(QP_ALL_ALLOWED_FT*);
  QPBOOL IsLTEVOPSEnabled();
  QPE_DCM_RAT extractGWTOrLRAT(const QPE_DCM_RAT_MASK ratMask);
  QPE_DCM_RAT extractIWLANRAT(const QPE_DCM_RAT_MASK ratMask);
  QPBOOL UpdateIMSAPNParams( QP_APN_RAT* n_pApnRat, QP_APN_ATTR_INFO* n_pApnAttrInfo );
  QPUINT16 UpdateRCSSrvBasedOnNV();  
  QPCHAR* ParseNVReadFTs(QPCHAR *n_pInStr);
  QPBOOL SetResolvedAPNParams(QPE_RESOLVED_APN_PARAMS* n_pResolvedAPNParams);
  QPBOOL GetResolvedAPNParams(QPE_RESOLVED_APN_PARAMS* n_pResolvedAPNParams);

private: /* Data Members */
  static QPConfigurationHandler*            m_pConfigHandler;   /*Store the different values coming from different location based on priority*/
  QPIMS_NV_CONFIG_DATA                      m_iPMconfig;         /* Reads the NV and stores the values */
  QPUINT32                                  m_iRAT[QP_IMS_PM_RAT_APN_SIZE];
  QPE_DCM_RAT                               m_iRATAPNFB[QP_IMS_PM_RAT_APN_FB_SIZE];
  QPBOOL                                    m_bGenericHandling;
  QPUINT8                                   m_iOprtMode;
  QPBOOL                                    m_bServiceStatusInfo[DCM_RAT_MAX][QP_MAX_SERVICE];/*double array to store the service status for each rat*/
  QPBOOL                                    m_bLteNoVopsConfigured; 
  QPBOOL                                    m_bUESIMGBAEnabled;
  QPCHAR*                                   m_pResolvedAPNName;
};

#endif //__QP_IMS_POLICY_MANAGER__
