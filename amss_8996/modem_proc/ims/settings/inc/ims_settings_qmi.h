/*==============================================================================
Copyright (C) 2010 Qualcomm Technologies, Incorporated. All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.

File Name
   ims_settings_qmi.h

Description
   This file contains the definition of the public interfaces
   to update IMS settings QMI Interface configuration values

================================================================================
Date    |   Author's Name    |  BugID  |        Change Description
================================================================================
01-12-2012    Murali Anand      331135   FR#1510 - QMI Setting Changes
14-04-2014    Prashanth M E     577990   FR#17768 - AT command Setting Changes
09-05-2014    Prashanth M E     659885   IMS Settings Enhancements
==============================================================================*/

#ifndef __IMS_SETTINGS_QMI_H
#define __IMS_SETTINGS_QMI_H

#include "ims_common_defs.h"
#include "qpPlatformConfig.h"

#ifdef FEATURE_IMS_QMI_SETTINGS_SERVICE

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
/************************************************************************
Function ims_settings_qmi_send_indication()

Description
API to Sends the QMI Indication messages for NV Item Group name

Parameters
eConfigItem - NV config Item type.

Dependencies
None

Return Value
AEE_IMS_SUCCESS if the send indication success, otherwise AEE_IMS_EFAILED is returned.

Side Effects
None
************************************************************************/
QPINT ims_settings_qmi_send_indication(QPE_IMS_CONFIG_ITEMS eConfigItem);

/************************************************************************
Function ims_settings_qmi_uninitialize()

Description
Initialize the QMI settings Interface. Registers the callback with DPL

Dependencies
None

Return Value
AEE_IMS_SUCCESS if the initialization success, otherwise AEE_IMS_EFAILED is returned.

Side Effects
None
************************************************************************/
QPINT ims_settings_qmi_initialize(QPVOID);

/************************************************************************
Function ims_settings_qmi_uninitialize()

Description
Un-Initialize the settings FW. De-Registers the callback with DPL

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID ims_settings_qmi_uninitialize(QPVOID);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // FEATURE_IMS_QMI_SETTINGS_SERVICE

#endif // __IMS_SETTINGS_QMI_H