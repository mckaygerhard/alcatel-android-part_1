/*****************************************************************************
 Copyright (c)2013-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential.


 File: ims_oma_dm_commpon.h
 Description: IPC common for both settings IPC handler and service

 Revision History
===============================================================================
   Date           |   Author's Name    |  BugID  |        Change Description
===============================================================================
06-Nov-2012      amurali             -                    First revision.
-------------------------------------------------------------------------------

*****************************************************************************/


#ifndef __IMS_OMA_DM_COMMON__
#define __IMS_OMA_DM_COMMON__

/**
	Settings IPC Command Type
 */
typedef enum
{
  QSETTINGS_SET_MO_REQ_IPC = 1,
  /**SET MO REQ   */
  QSETTINGS_GET_MO_REQ_IPC,
  /**GET MO REQ */ 
  QSETTINGS_SET_MO_RESP_IPC,
  /**SET MO RESP  */
  QSETTINGS_GET_MO_RESP_IPC,
  /**GET MO RESP   */
  QSETTINGS_IPC_MAX,
} QSETTINGS_IPC_ID;

/**
	Settings IPC Response Status Type
 */
typedef enum
{
  QSETTINGS_RESPONSE_SUCCESS = 1,
  /**REQUEST PROCESSED SUCCESSFULLY   */
  QSETTINGS_RESPONSE_FAILURE,
  /**REQUEST PROCESSING FAILED */ 
  QSETTINGS_RESPONSE_MAX,
} QSETTINGS_RESPONSE_ID;

typedef struct
{
  QPUINT16    requestID;
  QPUINT8      settingsIPCId;
  QPUINT8     responseStatus;
  QPUINT8      settingsIPCObjectType;
  QPUINT16    settingsIPCObjectLen;
  QPCHAR*    pSettingsIPCObjectBuf;
}QSETTINGS_IPC_INFO;


#endif  // __IMS_OMA_DM_COMMON__
