/*****************************************************************************
 Copyright (c)2013-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential.


 File: ims_oma_dm_service.h
 Description: IPC Service for the QSettings service.

 Revision History
===============================================================================
   Date           |   Author's Name    |  BugID  |        Change Description
===============================================================================
06-Nov-2012      amurali             -                    First revision.
-------------------------------------------------------------------------------

*****************************************************************************/


#ifndef __IMS_OMA_DM_SERVICE__
#define __IMS_OMA_DM_SERVICE__

#include <qpnet.h>
#include <networkBasehandler.h>
#include "ims_oma_dm_common.h"
#include <IMSRcsConfigMonitorHandler.h>

class ImsOmaDmService
{
public:
  // Instance of a class
  static ImsOmaDmService* getInstance();
  static QPVOID delInstance();
  QPVOID Init( QPVOID );
  
	static QPVOID handleIPCMessage(QPNET_CONN_PROFILE* pNetConnProfile, QPNET_REMOTE_ADDR* pAddr, QPVOID* pBuff, QPVOID* pUserData);
  
private:
  ImsOmaDmService();
  virtual ~ImsOmaDmService();
  QPVOID processGetMORequest(QPUINT16 requestID,QPUINT8 settingsIPCObjectType);
  QPVOID processSetMORequest(QPUINT16 requestID,QPUINT8 settingsIPCObjectType,QPUINT16 settingsIPCObjectLen,QPCHAR* pSettingsIPCObjectBuf);
  QPBOOL sendSettingsIPCMessage(QSETTINGS_IPC_INFO* pIPCInfo);
  QPVOID processIPCMessage(QPCHAR* pPacket);
  QPVOID constructMOFileName(QPUINT8 settingsIPCObjectType);
  QPBOOL sendConfigDataIPCMessage(QPCHAR* pMessage, QPUINT16 dwMessageLength);
  QPBOOL parseConfigXMLData(QPCHAR* pXmlMsgBody);

private:
  QPNET_CONN_PROFILE*              m_pConnProfile;
  QPCHAR                           m_configFileName[75];
  static ImsOmaDmService*          m_pImsOmaDmService;
  ImsRcsConfigMonitorHandler*      m_pImsRcsConfigMonitorHandler;
};

#endif  // __IMS_OMA_DM_SERVICE__
