/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : qpIMSServConfigGBAUIMDefines.h
 Description    : IMS Service Config realted all the define/struct/enum
 

 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 -------------------------------------------------------------------------------
 12-May-2015   Sreenidhi     789667      FR25901: Ut changes for migrating IMS over to UIM GBA solution
************************************************************************/ 

#ifndef __QPIMSSERVCONFIGGBAUIMDEFINES__
#define __QPIMSSERVCONFIGGBAUIMDEFINES__

#include "ims_variation.h"
#include <qpDplGba.h>


/*
** Function Pointer for the callback from IMS Service Config GBA utility to 
** IMS Service Config. This is needed as Challenege calculation is asynchronous from DPL
** typedef QPVOID (*QP_HANDLE_UB_AUTH_CALLBACK)(QPVOID* pUserData, QPVOID* pAuthHeader);
**                                        
** QP_HANDLE_UB_AUTH_CALLBACK is function pointer to the AUTH callback function.
** pUserData is the this pointer
** pChallengeParams is pointer to QP_IMS_SERV_CONFIG_CHALLENGE_PARAMS sent while calling the function.
*/
typedef QPVOID (*QP_HANDLE_UB_AUTH_CALLBACK)(QP_GBA_RESULT_ENUM_TYPE sGbaResultType,QP_BOOTSTRAP_RESULT* n_pGBAResult, 
																		QPVOID* pUserData);

/**
** This is the user data infromation that IMS Service config passes
** to the internal utility to receive the CB.
**/
typedef struct qpIMSServConfigUserData
{
  QPVOID*                              pthis;
  QP_HANDLE_UB_AUTH_CALLBACK           pHandleUbAuthCallBack;
}QP_IMS_SERV_CONFIG_USER_DATA;

/**
** qpIMSServConfigUaOnReqParams is the structure used
** when Mode is set as On Req and challenge is received 
** for the first time. Authorization header is parsed and 
** relevant data is extracted and filled in this structure.
**/
typedef struct qpIMSServConfigUaOnReqChallengeParams
{
  QPBOOL                                bNoncePresent;
  QPBOOL                                bPerformUbAuth;
  QPBOOL                                bIsParamIncorrect;
}QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS;

/**
** qpIMSServConfigGBAUIMUbAttributes is the structure used 
** to store the values of BTID, Ks Life Time and storing the 
** Life time value in Julian Format
**/
typedef struct qpIMSServConfigGBAUIMUbAttributes
{
  QPUINT8                               iKsNAF[QP_GBA_KS_NAF_LEN];
  QPCHAR*                               pBTIDValue;
  QPCHAR*                               pKsLifeTime;
}QP_IMS_SERV_CONFIG_GBA_UIM_UB_ATTRIBURES;


/**
** qpIMSServConfigNextChallengeParams is a structure used
** to store the value of Next Nonce and other parameters like Algorithm and 
** Qop associated with it.
**/
typedef struct qpIMSServConfigNextChallengeParams
{
  QPCHAR*                               pAlgorithm;
  QPCHAR*                               pNextNonce;
  QPCHAR*                               pQoP;
  QPCHAR*                               pRealm;
  QPCHAR*                               pOpaque;
  QPCHAR*                               pNC;
}QP_IMS_SERV_CONFIG_NEXT_CHALLENGE_PARAMS;

#endif