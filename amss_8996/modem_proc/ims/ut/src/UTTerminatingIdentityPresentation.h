/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : UTTerminatingIdentityPresentation.h
 Description    : TIP class for UT operations
 

 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 19-11-2013     Sreenidhi          549221     FR 17157: XML Configuration Access Protocol (XCAP) / Ut support for KDDI
--------------------------------------------------------------------------------------------------
 02-12-2013     Priyank            539082     Perform a GET operationof the entire server document initially to avoid rule id mismatch
************************************************************************/

#ifndef __UTTERMINATINGIDENTITYPRESENTATION_H__
#define __UTTERMINATINGIDENTITYPRESENTATION_H__

#include "UTXMLSSHandler.h"
#include <qpPlatformConfig.h>

class qp_terminating_identity_presentation;
class UTTerminatingIdentityPresentation : public UTXMLSSHandler
{
public:
  UTTerminatingIdentityPresentation();
  ~UTTerminatingIdentityPresentation();
  /* Procees the SS request. Returns object of class QP_SS_REQPARAMS*/
  QP_SS_REQPARAMS* ProcessSSRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  /* Procees the SS response received from the N/w.
  Returns bool for response parsing and indication data in QP_SUPS_IND_DATA
  */
  QPBOOL ProcessSSResponse(QPCHAR*, QP_SUPS_IND_DATA*, QPBOOL& n_bCompleteXML);
  /* Returns class name for the Class*/
  QPCHAR* ClassName();

private:

  //Copy Constructor and Operator = 
  UTTerminatingIdentityPresentation(const UTTerminatingIdentityPresentation&);
  UTTerminatingIdentityPresentation& operator=(const UTTerminatingIdentityPresentation &);

  
  QPVOID RegistrationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*){}
  QPVOID ActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID DeActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID InterrogationRequest(QP_SUPP_SRV_CB_DATA*);
  QPVOID ErasureRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*){}
  QPVOID Init();
  

private:
  qp_terminating_identity_presentation*      m_pRequestElement;
  qp_terminating_identity_presentation*      m_pResponseElement;
};

#endif // __UTTERMINATINGIDENTITYPRESENTATION_H__