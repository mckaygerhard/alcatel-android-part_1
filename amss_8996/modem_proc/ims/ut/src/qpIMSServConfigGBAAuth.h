/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : qpIMSServConfigGBAAuth.h
 Description    : IMS Sevice Configuration 

 Revision History
 ========================================================================
   Date      |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 21-Jun-2013     Priyank              494609     Application ( UT ) changes for FR 3236: GBA/GAA Support
-------------------------------------------------------------------------------
 04-Oct-2013     Priyank              541854     On receiving 401 with invalid BTID; UE doesn't reinitiate Ub procedure
-------------------------------------------------------------------------------
 18-Oct-2013     Sreenidhi            562035     For the initial Ub failure, device doesn't recover to handle subsequent Ut requests 
-------------------------------------------------------------------------------
 10-Dec-2013     Sreenidhi            579590     USIM Support required for Ut interface
-------------------------------------------------------------------------------
 23-Dec-2013     Sreenidhi            584663     In ON_REQUEST mode, if nonce is received during the bootup inital request;
                                                 UE doesn't do Ub procedure
-------------------------------------------------------------------------------
 23-Dec-2013     Sreenidhi            589936     When TMPI is returned with 403 on Ub procedure; UE does not fall back to IMPI based retry
 ------------------------------------------------------------------------------- 
 07-Jan-2014     Priyank              562518     FR 17764 : GBA_U Support for Ut Interface.
 ------------------------------------------------------------------------------- 
 07-Jan-2014     Sreenidhi            596126     Digest username and URI details not getting populated on subsequent Ua requests
 ------------------------------------------------------------------------------- 
 10-Jan-2014     Priyank              605099     GBA - On response mismatch, if server keeps sending new 401 with different nonce, loop occurs
 ------------------------------------------------------------------------------- 
 20-Feb-2014     Sreenidhi            614122     UE to use TMPI only when server announces its support for the same
 ------------------------------------------------------------------------------- 
 29-Apr-2014     Priyank              625112     FR 20013: TLS for XCAP
 -------------------------------------------------------------------------------
 22-Jul-2014     Priyank              695312     Cert contains IPv6 address but realm validation fails;ipv6 address format issue
-----------------------------------------------------------------------------------------------------------------------
10-Aug-2014      asharm             696761     UE should handle two realm values in WWW Authenticate header in GBA for TMO 
--------------------------------------------------------------------------------------------------
04-Dec-2014      Sreenidhi            738799     For GBA over TLS, Ut Interface doesn't use correct Security Identifier Protocol
-------------------------------------------------------------------------------
12-May-2015      Sreenidhi            789667      FR25901: Ut changes for migrating IMS over to UIM GBA solution
--------------------------------------------------------------------------------------------------
29-Jun-2014      asharm           855226     TMO-UCE:FR:24894/27504: Aggregate TMO UCE customization in GBA module
************************************************************************/

#ifndef __QPIMSSERVCONFIGGBAAUTH_H__
#define __QPIMSSERVCONFIGGBAAUTH_H__

#include <qpPlatformConfig.h>
#include "qpIMSServConfigDefines.h"
#include "qpIMSServConfigGBADefines.h"

class IMSServConfigGBAAuth
{
public:
  IMSServConfigGBAAuth();
  ~IMSServConfigGBAAuth();

  QPBOOL GenerateUbAuthMessage(QP_HTTP_MESSAGE_CONTENT*, QPCHAR*, QPBOOL&);
  QPBOOL GenerateUaAuthHeader(QP_IMS_SERV_CONFIG_GBA_UA_AUTH_PARAMS*);
  QPBOOL CalculateAuthResponse(const QP_IMS_SERV_CONFIG_USER_DATA&, const QP_IMS_SERV_CONFIG_CHALLENGE_PARAMS&, QPUINT16 n_iSecProtocolID = 0);
  QPBOOL ValidateGBA2XXResponse(QPCHAR*, QPCHAR*, QPE_GBA_AUTH);
  QPBOOL ParseHttpContent(QP_IMS_SERV_CONFIG_GBA_UB_ATTRIBURES&, QPCHAR*);  
  QPBOOL ValidateAuthenticationInterface(QP_IMS_SERV_CONFIG_UA_ON_REQ_AUTH_PARAMS&, const QPCHAR*, HttpConnection*);
  QPBOOL IsQopTypeAuthInt(QPE_GBA_AUTH);
  QPBOOL IsNextNoncePresent(QPE_GBA_AUTH);
  QPBOOL IsChallengeCaluculationInProgress();
  QPVOID  CancelOngoingAuthenticationInfo();
  QPVOID ResetPreviousAuthParams(QPE_GBA_AUTH);
  QPVOID ResetTMPI();
  static QPVOID qpGBAAuthCallBack(QPE_AUTH_MSG , QPVOID*, const QPVOID*);
  QPVOID SetGBABootStrappingParameters(QP_IMS_SERV_CONFIG_GBA_UB_ATTRIBURES&, QP_CARD_WRITE_USERDATA&);
  QPVOID SetTMPISupport(QPBOOL);
  QPVOID SetGBATLSMode(QPE_IMS_SERV_CONFIG_GBA_TLS_MODE);

private:
  //Copy Constructor and Operator = 
  IMSServConfigGBAAuth(const IMSServConfigGBAAuth&);
  IMSServConfigGBAAuth& operator=(const IMSServConfigGBAAuth&);

  QPVOID  GenerateAuthorizationHeader(const QP_GBA_CHALLENGE_PARAMS&, QPCHAR*, QPUINT32);
  QPBOOL  ParseAuthHeaderAndFillChallengeStruct(const QPCHAR*, QP_GBA_CHALLENGE_PARAMS&);
  QPVOID  ResetChallengeStruct(QP_GBA_CHALLENGE_PARAMS&);
  QPVOID  HandleGBAAuthCallBack(QPE_AUTH_MSG, const QP_GBA_CHALLENGE_PARAMS*);
  QPVOID  ParseRspAuthParam(QPCHAR*, QPCHAR*, QP_GBA_CHALLENGE_PARAMS&, QPE_GBA_AUTH);
  QPVOID  UpdateChallengeParams(QPCHAR*, QP_GBA_CHALLENGE_PARAMS&, QP_SIP_DIGEST_AUTH_PARAM);
  QPCHAR* ExtractParamsFromHeader(QPCHAR*);
  QPCHAR* RemoveQoutes(const SipString*);
  QPVOID  ResetNextChallengeStruct(QP_IMS_SERV_CONFIG_NEXT_CHALLENGE_PARAMS&);
  QPVOID  UpdateNextChallengeParams(QPCHAR* n_pParam, QPCHAR** n_pInput);
  QPCHAR* GenerateIMPIFromUSIM();
  QPVOID  CopyReceivedChallengeParams();  
  QPBOOL  ValidateServersRealm(QPCHAR*, HttpConnection*);
  QPCHAR* ExpandInIPv6Format(QPCHAR*);
  QPUINT QpIsAlphaNum(QPUINT c);

private:
  QPBOOL                                    m_bUaOnReqNoncePresent;
  QP_IMS_SERV_CONFIG_NEXT_CHALLENGE_PARAMS  m_stUbNNonceParams;
  QP_IMS_SERV_CONFIG_NEXT_CHALLENGE_PARAMS  m_stUaNNonceParams;
  QP_IMS_SERV_CONFIG_NEXT_CHALLENGE_PARAMS  m_stAKAv1NNonceParams;
  GAAGBAAuth*                               m_pAKAv1Auth;
  GAAGBAAuth*                               m_pGAAGBAAuth;
  QP_GBA_AUTH_USERDATA                      m_stUserData;                       /* This is User data and hold a "this" object's parameters */
  QP_GBA_CHALLENGE_PARAMS                   m_stGBAUbAuthParams;
  QP_GBA_CHALLENGE_PARAMS                   m_stGBAUaAuthParams;
  QP_GBA_CHALLENGE_PARAMS                   m_stAKAv1AuthParams;
  QP_GBA_CHALLENGE_PARAMS                   m_stOnReqUaAuthParams;
  QP_IMS_SERV_CONFIG_USER_DATA              m_stCBUserData;
  QP_IMS_SERV_CONFIG_CHALLENGE_PARAMS       m_stCBChallengeParams;
  QPBOOL                                    m_bAKAv1AuthCalcInProgress;
  QPBOOL                                    m_bGAAGBAAuthCalcInProgress;
  QPBOOL                                    m_bServerSupportsTMPI;
  QPE_IMS_SERV_CONFIG_GBA_TLS_MODE          m_eGBATLSMode;
  QPINT8                                    m_iOprtMode ;
};

#endif //__QPIMSSERVCONFIGGBAAUTH_H__