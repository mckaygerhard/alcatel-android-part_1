/************************************************************************
 Copyright (C) 2012 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : IMSServiceConfigGBAUIM.h
 Description    :

 Revision History
 ========================================================================
   Date      |   Author's Name    |  BugID  |        Change Description
=========================================================================
-------------------------------------------------------------------------------
12-May-2015   Sreenidhi     789667      FR25901: Ut changes for migrating IMS over to UIM GBA solution
-------------------------------------------------------------------------------
11-Jun-15        asharm      827203   FR 27504: support Ub over HTTPS and redirection
-------------------------------------------------------------------------------
29-Jul-2015      Sreenidhi    872090      FR 29150: If the APN in NV doesn't match profile then DUT should do CSFB
************************************************************************/
#ifndef __IMSSERVICECONFIGGBAUIM_H__
#define __IMSSERVICECONFIGGBAUIM_H__

/*----------------------------------------------------------------------------
 Header Includes
----------------------------------------------------------------------------*/
#include "imsserviceconfig.h"
#include "qpIMSServConfigGBAUIMDefines.h"
#include "qpDplGba.h"

//Forward Declaration
class IMSServConfigGBAAuthUIM;

class IMSServiceConfigGBAUIM : public IMSServiceConfig
{
public:
  IMSServiceConfigGBAUIM();
  virtual ~IMSServiceConfigGBAUIM();  
  QPVOID Init(QP_IMS_SERVICE_CONFIG_NV*, PDNBRINGUPTYPE = AlwaysOnPDN);
  /* From base class HttpListener */
  QPVOID NotifyHttpResponse(HttpConnection* pHttpConnection);
  /* This is the API which APP calls to send the message out */
  QPUINT8 SendMessage(QP_HTTP_MESSAGE_CONTENT*, QPE_HTTP_CONNECTION_TYPE n_eHttpConType = QP_HTTP_CONNECTION );

  /**
  ** QP_HANDLE_GBA_AUTH_CALLBACK is function pointer to the AUTH callback function
  **/
  static QPVOID qpHandleUBAuthCallBack(QP_GBA_RESULT_ENUM_TYPE n_eGbaResultType,QP_BOOTSTRAP_RESULT* n_pGBAResult, 
																		QPVOID* pUserData);
  /* This API triggers HTTP connection cleanup */
  QPVOID CloseHttpConnection();

  QPVOID ReInitGBASupport();

  
private:

  //Copy Constructor and Operator = 
  IMSServiceConfigGBAUIM(const IMSServiceConfigGBAUIM&);
  IMSServiceConfigGBAUIM& operator=(const IMSServiceConfigGBAUIM &);
  
  QPBOOL SetUpHttpConnection(QP_HTTP_MESSAGE_CONTENT& n_stHttpMessageContent);
  QPUINT8 Send(QP_HTTP_MESSAGE_CONTENT&);
  QPVOID ProcessGBAUBAuthCallBack(QP_GBA_RESULT_ENUM_TYPE n_eGbaResultType,QP_BOOTSTRAP_RESULT* n_pGBAResult);
  QPVOID HandleGBAResponse();
  QPVOID ProcessUa2XXResponse();
  QPVOID ResetGBAUbAttributes();
  QPVOID InitGBASupport();
  QPVOID DeInitGBASupport();
  QPUINT8 InitiateGBAUbProcedure(QPBOOL n_bForceUb = QP_FALSE);
  QPUINT8 InitiateGBAUaProcedure();
  QPBOOL ContinueAfterRspAuthFailure();
  QPVOID ParseServerAddress(QPCHAR*, QPCHAR*);
  QPBOOL IsBTIDValid(QPE_CARD_DATA n_eCardType);
  QPVOID Cleanup();
  QPBOOL IsStalePresent();
  QPCHAR* RemoveQoutes(const SipString*);
  QPUINT32 GetCSList();
  QPVOID RemoveAuthorizationHeader();
  QPVOID FillNAFSecProtocolType(QPUINT8* n_pUaSecProtocolType);
  
  QPVOID ResetUaAuthParams(QP_AUTH_PARAMS& n_stChallengeParams);
  QPUINT8 ProcessUa3XXResponse();
  
private:
  QPBOOL                                    m_bUaAuthInProgress;
  QPBOOL                                    m_bUbAuthInProgress;
  QPCHAR*                                   m_pRealm;
  QPCHAR*                                   m_pNafFqdn;
  QPCHAR*                                   m_pHttpContent;
  QPUINT8                                   m_iRspAuthFailureCount;
  QPDPL_IMSI_STRUCT*                        m_pImsi;
  QPE_IMS_SERV_CONFIG_GBA_UB_MODE           m_eGBAUbMode;                                       /* GBA Mode : Always On, When Needed */
  QPE_IMS_SERV_CONFIG_GBA_UB_TYPE           m_eGBAUbType;                                       /* GBA Type : GBA_ME, GBA_U, Both */
  QP_IMS_SERV_CONFIG_GBA_UIM_UB_ATTRIBURES  m_stGBAUbAttributes;
  QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS  m_stUaOnReqAuthParams;
  IMSServConfigGBAAuthUIM*                  m_pGBAAuth;                                         /* Handle to an object of IMSServConfigGBAAuth class */
  QP_IMS_SERV_CONFIG_USER_DATA              m_stUserData;                                       /* This is the CB details with which this class registers */
  QPBOOL                                    m_bUaAuthRespIncluded;
  QPUINT8                                   m_iPerformGBAUbCount;                               /* This counts number of time NAF server redirects UE to BSF server */
  QPE_IMS_SERV_CONFIG_GBA_TLS_MODE          m_eGBATLSMode;
  QPUINT16                                  m_iUaSecProtocolId;
  QPUINT32                                  m_iCSList;
  QP_AUTH_PARAMS                            m_stUaAuthParams;
  QPCHAR                                    m_strApnName[QP_APN_NAME_LEN];
  QPCHAR                                    m_strApn2Name[QP_APN_NAME_LEN];
  QPE_IMS_SERV_CONFIG_GBA_TLS_MODE_OVER_UB  m_eGBAUbTLSMode;
};

#endif //__IMSSERVICECONFIG_H__

