/************************************************************************
 Copyright (C) 2012 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : IMSServiceConfigGBA.h
 Description    :

 Revision History
 ========================================================================
   Date      |   Author's Name    |  BugID  |        Change Description
=========================================================================
-------------------------------------------------------------------------------
12-May-2015   Sreenidhi     789667      FR25901: Ut changes for migrating IMS over to UIM GBA solution
--------------------------------------------------------------------------------------------------
29-Jun-2014      asharm           855226     TMO-UCE:FR:24894/27504: Aggregate TMO UCE customization in GBA module
--------------------------------------------------------------------------------------------------
13-July-15     Sreenidhi        851945     BTID should be persistent across power cycle for GBA_U enabled card
-------------------------------------------------------------------------------
29-Jul-2015      Sreenidhi    872090      FR 29150: If the APN in NV doesn't match profile then DUT should do CSFB
************************************************************************/
#ifndef __IMSSERVICECONFIGGBA_H__
#define __IMSSERVICECONFIGGBA_H__

/*----------------------------------------------------------------------------
 Header Includes
----------------------------------------------------------------------------*/
	
#include "imsserviceconfig.h"
#include "qpIMSServConfigGBADefines.h"
#include "qpGAAGBAAuth.h"
#include <qpXCAPUri.h>

#define QP_UB_COOKIE_FILE_NAME            (QPCHAR*)"ims_ub_cookie.bin"
#define QP_GBA_MAX_COOKIE_SIZE            1024  
#define QP_GBA_MAX_COOKIE_FILE_SIZE       256

#define QP_GBA_COOKIE_DELIMITER           "#"   
#define QP_GBA_COOKIE_SEPERATER           ";"

//Forward Declaration
class IMSServConfigGBAAuth;

class IMSServiceConfigGBA : public IMSServiceConfig
{
public:
  IMSServiceConfigGBA();
  virtual ~IMSServiceConfigGBA();  
  /* From base class HttpListener */
  QPVOID NotifyHttpResponse(HttpConnection* pHttpConnection);
  /* This is the API which APP calls to send the message out */
  QPUINT8 SendMessage(QP_HTTP_MESSAGE_CONTENT*, QPE_HTTP_CONNECTION_TYPE n_eHttpConType = QP_HTTP_CONNECTION );
  QPVOID Init(QP_IMS_SERVICE_CONFIG_NV*, PDNBRINGUPTYPE = AlwaysOnPDN);
  /*
  ** Application will set the BSF Address.
  */
  QPBOOL SetBSFAddress(const QPCHAR*, QPUINT16);

  /**
  ** QP_HANDLE_GBA_AUTH_CALLBACK is function pointer to the AUTH callback function
  **/
  static QPVOID qpHandleGBAAuthCallBack(QPVOID* pUserData, QPVOID* pChallengeParams);

  /*QP_GBA_SET_CALLBACK is function pointer to the CARD WRITE callback function */
  static QPVOID qpHandleGBASetCallBack(QPE_WRITE_RESULT n_eWriteResult, QPVOID* pUserData);

  /* This API triggers HTTP connection cleanup */
  QPVOID CloseHttpConnection();

  QPVOID ReInitGBASupport();

  QPCHAR** GetHeadersValue(QPCHAR* pHeaderName);
  /*Verify if cookie value is valid*/
  QPBOOL  IsCookieExpired(QPCHAR* pCookieString);
  QPVOID SetCookieFileName(QPCHAR* n_pFileName);
  
private:

  //Copy Constructor and Operator = 
  IMSServiceConfigGBA(const IMSServiceConfigGBA&);
  IMSServiceConfigGBA& operator=(const IMSServiceConfigGBA &);
  QPUINT8 Send(QP_HTTP_MESSAGE_CONTENT&);
  QPVOID ProcessGBAAuthCallBack(QP_IMS_SERV_CONFIG_CHALLENGE_PARAMS*);
  QPVOID HandleGBAResponse();
  QPVOID ProcessUb2XXResponse();
  QPVOID ProcessUa2XXResponse();
  QPVOID ResetGBAUbAttributes();
  QPVOID InitGBASupport();
  QPVOID DeInitGBASupport();
  QPUINT8 InitiateGBAUbProcedure(QPBOOL n_bForceUb = QP_FALSE);
  QPUINT8 InitiateGBAUaProcedure();
  QPBOOL ContinueAfterRspAuthFailure(QPE_GBA_AUTH);
  QPVOID ParseServerAddress(QPCHAR*, QPCHAR*);
  QPVOID ConvertValidityInJulianFormat();
  QPBOOL IsBTIDValid();
  QPVOID CheckIfCardChanged();
  QPUINT8 ProcessCalculatedResponseReceived(QP_IMS_SERV_CONFIG_CHALLENGE_PARAMS*);
  QPVOID Cleanup();
  QPBOOL IsStalePresent();
  QPCHAR* RemoveQoutes(const SipString*);
  QPVOID SetGBABootStrappingParameters();
  QPVOID ProcessGBACardWrite(QPE_WRITE_RESULT);
  QPBOOL IsTMPIProductTokenPresent();
  QPUINT32 GetCSList();
  QPVOID RemoveAuthorizationHeader();

  QPUINT8 ProcessUa3XXResponse();   

   /*Store cookie value in file format in efs*/
  QPBOOL WriteCookieValueFile(QPCHAR*, QPCHAR**, QPUINT16);
  /*Retrieve cookie value in file format in efs*/
  QPBOOL ReadCookieValueFile(QPCHAR*, QPCHAR*, QPUINT16);
  QPVOID FormatCookieHeaderVal(QPCHAR* pCookieValfromFile, QPCHAR* pOutCookieVal);

  QPVOID AddCookieHeader();
  QPVOID StoreCookieHeader();
  QPVOID GetBTIDCredentialsFromCard();

private:
  static QP_IMS_SERV_CONFIG_GBA_UB_ATTRIBURES      m_stGBAUbAttributes;
  
  QPBOOL                                    m_bUbAuthInProgress;
  QPBOOL                                    m_bUaAuthInProgress;
  QPCHAR*                                   m_pOutAuthorizationHeader;
  QPCHAR*                                   m_pRealm;
  QPCHAR*                                   m_pHttpContent;
  QPDPL_IMSI_STRUCT*                        m_pImsi;
  QPUINT8                                   m_iRspAuthFailureCount;
  QPCHAR                                    m_strBSFUri[QP_IMS_SERV_CONFIG_BSF_URI_LEN];
  QP_HTTP_MESSAGE_CONTENT*                  m_pGBAUbHttpContent;
  QPE_IMS_SERV_CONFIG_GBA_UB_MODE           m_eGBAUbMode;                                       /* GBA Mode : Always On, When Needed */
  QPE_IMS_SERV_CONFIG_GBA_UB_TYPE           m_eGBAUbType;                                       /* GBA Type : GBA_ME, GBA_U, Both */
  QP_IMS_SERV_CONFIG_CHALLENGE_PARAMS       m_stChallengeParams;                                /* Struct containing the details of Challenge received */
  QP_IMS_SERV_CONFIG_GBA_UA_AUTH_PARAMS*    m_pUaAuthHeaderParams;                              /* Struct to contain Authorization credentials on Ua Interface */
  QP_IMS_SERV_CONFIG_UA_ON_REQ_AUTH_PARAMS  m_stUaOnReqAuthParams;
  IMSServConfigGBAAuth*                     m_pGBAAuth;                                         /* Handle to an object of IMSServConfigGBAAuth class */
  QP_IMS_SERV_CONFIG_BSF_HEAD_AUTH_PARAMS   m_stBSFAuthHeadParams;
  QP_IMS_SERV_CONFIG_USER_DATA              m_stUserData;                                       /* This is the CB details with which this class registers */
  QPBOOL                                    m_bUaAuthRespIncluded;
  QPUINT8                                   m_iPerformGBAUbCount;                               /* This counts number of time NAF server redirects UE to BSF server */
  QPE_IMS_SERV_CONFIG_GBA_TLS_MODE          m_eGBATLSMode;
  QPUINT16                                  m_iUaSecProtocolId;
  QPUINT32                                  m_iCSList;

  QPE_IMS_SERV_CONFIG_GBA_TLS_MODE_OVER_UB  m_eGBAUbTLSMode;
  QPCHAR*                                  m_strUaCookie;  
  QPCHAR*                                  m_strUbCookie;  
  QPCHAR                                   m_strUaCookieFileName[QP_GBA_MAX_COOKIE_FILE_SIZE];
};

#endif //__IMSSERVICECONFIGGBA_H__

