/************************************************************************
 Copyright (C) 2011 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : KTAutoConfig.cpp
 Description    :

 Revision History
 ========================================================================
   Date      |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 20-Jan-2013   Murali Anand                    Initial Version
 -------------------------------------------------------------------------------
 29-Apr-2013   Priyank              479646     KT ACS - Version -1 across the different 
                                               sim cards is not handled properly
-------------------------------------------------------------------------------
 29-Apr-2013   Priyank              478351     KT ACS - UE is not sending the Authorization
                                               header in the case of 500 HTTPS error re-try
-------------------------------------------------------------------------------
 05-Jun-2013   Priyank              494609     If all retransmissions fail and there is no previously retrieved configuration,
                                               UE is not retrieving the information from ISIM
-------------------------------------------------------------------------------
 06-Jun-2013   Priyank              494087     Incorrect behavior in "UE receives Validity -1 with SIM1 and then 
                                               insert SIM2 and then insert back SIM1" usecase
-------------------------------------------------------------------------------
 08-Jul-2013   Priyank              509611     Replacing DPL Memcpy with Memscpy (Related to FR 15386)                                               
-------------------------------------------------------------------------------
 25-Jul-2013   Priyank              494087     Part-II - Incorrect behavior in "UE receives Validity -1 with SIM1 and then 
                                               insert SIM2 and then insert back SIM1" usecase
-------------------------------------------------------------------------------
 07-Oct-2013   Priyank              552274     ACS connect does not happen when getting out of LPM
-------------------------------------------------------------------------------
 20-Nov-2013   Sreenidhi            576767     Addressing KW issues in RM/PM modules 
-------------------------------------------------------------------------------------------------
 11-Mar-2013   Priyank              621266     FR 20134 : User-Agent Information : Set Net_Type value based on access network
-------------------------------------------------------------------------------
 07-04-2014    Sankalp              617708     FR20074: IMS NV Settings Change Notification to Applications from DPL Instead of RM
-------------------------------------------------------------------------------------------------
 09-04-2014    Sreenidhi            625130     Request for print full message between UE and ACS
 -------------------------------------------------------------------------------
11-02-15   Priyank       792302      FR 24984: SIM Swap without RESET - Memory leaks in RM
-------------------------------------------------------------------------------
31-Dec-15  Sreenidhi    652675     SingTel: Automatic population of username with P-associated URI from 200OK
************************************************************************/

#include "math.h"
#include "KTAutoConfig.h"
#include <qpdefines.h>
#include <qpdefinesCpp.h>
#include <qpdpl.h>
#include <ims_common_defs.h>
#include <imsserviceconfig.h>
#include <SingoIMSServConfigEvManager.h>
#include <IMSConfigMgrApi.h>
#include <AutoConfigMgr.h>
#include "RegFunctions.h"
#include <ims_task.h>
#include <singoConfig.h>

#define PARM_MAX_LEN            50
#define KT_CLIENT               (QPCHAR*)"KT-client/"
#define KT_DEVICE_TYPE          (QPCHAR*)"Device_Type="
#define KT_NET_TYPE             (QPCHAR*)"Net_Type="
#define KT_CLOSE                (QPCHAR*)"close"
#define KT_ZERO                 (QPCHAR*)"0"
#define KT_LOG_BUFFER_SIZE      96

// Singleton Instance of a class
KTAutoConfig* KTAutoConfig::m_pKTAutoConfig = QP_NULL;

extern "C"
{
// Regmanager function to notify of settings change
extern void ImsRegmanager_GenericEventNotify(QPE_GENERIC_TASK_EVENT iEvent);

}

/************************************************************************
Function KTAutoConfig::getInstance()

Description
Access method to get the singleton instance of the class

Dependencies
None

Return Value
Singleton Object

Side Effects
None
************************************************************************/
KTAutoConfig* KTAutoConfig::getInstance() 
{
  if (m_pKTAutoConfig == QP_NULL) 
  {
    IMS_NEW(KTAutoConfig, MEM_IMS_HTTPS,m_pKTAutoConfig, ());
  }
  return m_pKTAutoConfig;
}

/************************************************************************
Function KTAutoConfig::delInstance()

Description
Access method to delete the singleton instance of the class

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::delInstance() 
{
  if (m_pKTAutoConfig != QP_NULL) 
  {
    IMS_DELETE(m_pKTAutoConfig,MEM_IMS_HTTPS);
    m_pKTAutoConfig = QP_NULL;
  }
}
/************************************************************************
Function KTAutoConfig::KTAutoConfig()

Description
Constructor

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
KTAutoConfig::KTAutoConfig()
{
  m_pUri = QP_NULL;
  m_pUrl = QP_NULL;
  m_pIMSServiceConfig = QP_NULL;
  m_pXmlMsgBody = QP_NULL;
  m_pImsi = QP_NULL;
  m_iHttpRespWaitCount = 0;
  m_iHttpRespErrCount = 0 ;
  m_i503WithRetryAfter = 0 ;
  m_currentValidityTime = 0;
  m_isCardReady = QP_FALSE;
  m_isPDPActive = QP_FALSE;
  m_pHttpContent = QP_NULL;
  m_pUserAgent = QP_NULL;
  m_pIMEInumber = QP_NULL;
  for ( int index = 0; index < QP_KT_TIMER_TYPE_MAX; index++ )
  {
    m_pConfigTimer[index] = QP_NULL;
  }
  IMS_NEW(IMSServiceConfig, MEM_IMS_HTTPS,m_pIMSServiceConfig, ());
  //Copy the default xml version.
  (QPVOID)qpDplStrlcpy(m_currentXmlVersion, "0.0.0.0", sizeof(m_currentXmlVersion));
  m_pAuthResponse = QP_NULL;
  m_pAutoConfigMgr = QP_NULL;
  m_pAutoConfigStruct = QP_NULL;
  qpDplMemset((QPVOID*) &m_strMSISDN,0,sizeof(QP_KT_MSISDN_MAX_LEN));
  m_callCtrlHdl   = QP_NULL;
}

/************************************************************************
Function KTAutoConfig::~KTAutoConfig()

Description
Destructor

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
KTAutoConfig::~KTAutoConfig()
{
  if(m_pXmlMsgBody)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pXmlMsgBody);
    m_pXmlMsgBody = QP_NULL;
  }
  if(m_pIMSServiceConfig)
  {
    IMS_DELETE(m_pIMSServiceConfig,MEM_IMS_HTTPS);
    m_pIMSServiceConfig = QP_NULL;
  }
  if(m_pUri)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pUri);
    m_pUri = QP_NULL;
  }
  if(m_pUrl)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pUrl);	
    m_pUrl = QP_NULL;
  }
  if(m_pImsi)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pImsi);
    m_pImsi = QP_NULL;
  }

  if(m_pAuthResponse)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pAuthResponse);
    m_pAuthResponse = QP_NULL;
  }
  // Stop and delete all timers running
  deleteAllTimers();

  if(m_pAutoConfigStruct)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pAutoConfigStruct);
    m_pAutoConfigStruct = QP_NULL;
  }
  if(m_pHttpContent)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pHttpContent);
    m_pHttpContent = QP_NULL;
  }
  if(m_pUserAgent)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pUserAgent);
    m_pUserAgent = QP_NULL;
  }
  if(m_pIMEInumber)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pIMEInumber);
    m_pIMEInumber = QP_NULL;
  }
  if(QP_NULL != m_callCtrlHdl)
  {
    qpDplCallCtrlUnInitialize(m_callCtrlHdl);
    m_callCtrlHdl = QP_NULL;
  }
}

/************************************************************************
Function KTAutoConfig::Init()

Description
Initialize and bring up the PDN for Https traffic

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::Init()
{
  SingoIMSServConfigEvManager* pEvMgr = QP_NULL;
  QPIMS_NV_CONFIG_DATA         sConfig;
  QP_IMS_SERVICE_CONFIG_NV     sImsConfig;
  QPIMS_NV_CONFIG_DATA         nvDPLConfig;
  QPBOOL                       m_bIsIPv6Enabled;
  IMS_LOC_STRUCT               sOprtInfo;

  pEvMgr = SINGLETON_OBJECT(SingoIMSServConfigEvManager);
  qpDplMemset((QPVOID*) &sConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
  qpDplMemset((QPVOID*) &sImsConfig,0,sizeof(QP_IMS_SERVICE_CONFIG_NV));
  qpDplMemset((QPVOID*) &nvDPLConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
  qpDplMemset((QPVOID*) &sOprtInfo,0,sizeof(IMS_LOC_STRUCT));

  m_pAutoConfigMgr = AutoConfigMgr::GetInstance();

  //Create NV config DATA.
  sConfig.env_Hdr = eQP_IMS_RCS_AUTO_CONFIG;

  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::Init qpdpl_get_config_group_value failed");
    return;
  }
  nvDPLConfig.env_Hdr = eQP_IMS_DPL_CONFIG;
  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&nvDPLConfig))
  {
    IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::Init DPL qpdpl_get_config_group_value failed");
    return ;
  }
  m_bIsIPv6Enabled = nvDPLConfig.uIMSConfigPayload.sDplConfigItem.iIPV6Enabled;

  if(pEvMgr != QP_NULL)
  {
    pEvMgr->addEventListener(IMS_SERV_CONFIG_EVENT_LISTENER, this, (QPVOID*)m_pIMSServiceConfig);
  }

  sImsConfig.imsServConfigProfileInfo.eDcmRat = DCM_RAT_LTE;
  sImsConfig.imsServConfigProfileInfo.eAPNType = DCM_APN_IMS;
  sImsConfig.imsServConfigProfileInfo.eIPAddrType = (QPE_IMS_SERV_CONFIG_IPADDR_TYPE)((m_bIsIPv6Enabled)? IPV6 : IPV4);

  if(qpDplStrlen(sConfig.uIMSConfigPayload.sRcsAutoConfigItem.InternetPdpProfilename)!= 0)
  {
    (QPVOID)qpDplStrlcpy(sImsConfig.imsServConfigProfileInfo.strApnName, sConfig.uIMSConfigPayload.sRcsAutoConfigItem.InternetPdpProfilename, QP_APN_NAME_LEN);
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::Init() - utPdpProfileName = %s", sImsConfig.imsServConfigProfileInfo.strApnName);
  }
  if ( qpDplStrstr(sImsConfig.imsServConfigProfileInfo.strApnName,"internet") )
  {
    sImsConfig.imsServConfigProfileInfo.eAPNType = DCM_APN_INTERNET;  
    IMS_MSG_LOW_0_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::Init() - Changing the APN type to Internet");
  }
  if ( m_pIMSServiceConfig )
  {
    m_pIMSServiceConfig->Init(&sImsConfig);
   	sOprtInfo = DplGetOperatorMode();
	if (IMS_OPRT_MODE_SINGTEL != sOprtInfo.eOperatorMode)
	{
      m_pIMSServiceConfig->SetAppAuthorizationBehaviour(QP_TRUE);
	}
  }

  QP_CMIPAPP_CB_REGISTER_MASK_TYPE eRegMask   =  QP_CALL_CTRL_REG_OPRT_MODE_CB_MASK;
  if (QP_NULL == (m_callCtrlHdl = qpDplCallCtrlInitialize( eCALL_TYPE_REG,eSYS_MODE_NO_SRV,eRegMask,KTAutoConfig::HandleCallCtrlCB,(QPVOID*)this)))
  {
    IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::Init() - qpDplCallCtrlInitialize failed!!");
    return;
  }
}

/************************************************************************
Function KTAutoConfig::HandleCallCtrlCB()

Description
Call-Back function to handle the Phone Operation mode.

Dependencies
None

Return Value
None.

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::HandleCallCtrlCB( QPE_CALL_CTRL_CB_TYPE eCallCbType, QP_CALL_CTRL_CB_DATA* pCallCtrlCbData, QPVOID* pUserData ) 
{
  if(QP_NULL == pCallCtrlCbData || QP_NULL == pUserData)
  {
    IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG, "KTAutoConfig::handleCallCtrlCB - QP_CALL_CTRL_CB_DATA,pUserData is NULL");
    return;
  }
  KTAutoConfig* pKTAutoConfig = (KTAutoConfig*)pUserData;
  IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG, "KTAutoConfig::handleCallCtrlCB -Call type %d", eCallCbType);

  switch (eCallCbType)
  {
  case eINFORM_OPRT_MODE_CB_TYPE:
    {
      QP_INFORM_OPRT_MODE_CB_DATA *pOptCbData =  (QP_INFORM_OPRT_MODE_CB_DATA*) pCallCtrlCbData;

      if ( eOPRT_MODE_PWROFF       == pOptCbData->eOprtMode  ||
           eOPRT_MODE_LPM          == pOptCbData->eOprtMode  ||
           eOPRT_MODE_OFFLINE      == pOptCbData->eOprtMode  ||
           eOPRT_MODE_OFFLINE_CDMA == pOptCbData->eOprtMode 
        )
      {
        IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG, "KTAutoConfig::handleCallCtrlCB - Oprt Mode %d", pOptCbData->eOprtMode);
        pKTAutoConfig->HandleLPMEvent();
      }      
      qpDplCallCtrlAckEvent(pKTAutoConfig->GetCallCtrlHandle(),eINFORM_OPRT_MODE_CB_TYPE,pOptCbData->iTransID);
    }
    break;
  default:
    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG, "KTAutoConfig::HandleCallCtrlCB default ignore the eCallCbType = %d notification", eCallCbType);
    break;
  }
}

/************************************************************************
Function KTAutoConfig::HandleLPMEvent()

Description
Handle LPM Event

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::HandleLPMEvent()
{
  IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG, "KTAutoConfig::HandleLPMEvent");
  if((m_pConfigTimer[QP_KT_CONFIG_VALIDITY_TIMER] != QP_NULL && m_pConfigTimer[QP_KT_CONFIG_VALIDITY_TIMER]->IsTimerRunning() == QP_TRUE))
  {
    IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG, "KTAutoConfig::HandleLPMEvent | Validity Timer is running, stop it!");
    ResetCardRelatedParams();
  }
}

/************************************************************************
Function KTAutoConfig::deleteAllTimers()

Description
Delete all the existing timers.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::deleteAllTimers()
{
  QPUINT8 iTmrCnt = 0;
  for(; iTmrCnt < QP_KT_TIMER_TYPE_MAX; iTmrCnt++)
  {
    if(m_pConfigTimer[iTmrCnt] != QP_NULL)
    {
      IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::qpDeleteAllTimers | Timer Type %d", m_pConfigTimer[iTmrCnt]->GetTimerType());
      if(m_pConfigTimer[iTmrCnt]->IsTimerRunning() == QP_TRUE)
      {
        m_pConfigTimer[iTmrCnt]->StopTimer();
      }
      IMS_DELETE(m_pConfigTimer[iTmrCnt], MEM_IMS_HTTPS)
      m_pConfigTimer[iTmrCnt] = QP_NULL;
    }
  }
}

/************************************************************************
Function KTAutoConfig::deleteTimer()

Description
Delete all the timer matching the timerType

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::deleteTimer(QPINT timerType)
{
  IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"deleteTimer | Deleting Timer type %d", timerType);

  if(m_pConfigTimer[timerType] != QP_NULL)
  {
    if(m_pConfigTimer[timerType]->IsTimerRunning() == QP_TRUE)
    {
      IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"deleteTimer | Timer type %d Running, stopping it", timerType);
      m_pConfigTimer[timerType]->StopTimer();
    }
    IMS_DELETE(m_pConfigTimer[timerType], MEM_IMS_HTTPS)
      m_pConfigTimer[timerType] = QP_NULL;
  }
  else
  {
    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"deleteTimer | m_pConfigTimer is NULL Timer type %d", timerType);
  }
}

/************************************************************************
Function KTAutoConfig::startTimer()

Description
Start the timer with timerType for iTimerValue msecs.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::startTimer(QPUINT32 iTimerValue, QPUINT32 iTimerId,QPE_KT_CONFIG_TIMERS_TYPES timerType)
{
  if(iTimerValue == 0)
  {
    IMS_MSG_ERR_1(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::startTimer | Could not start Timer, Timer value is %d", iTimerValue);
    return;
  }

  if(m_pConfigTimer[timerType] == QP_NULL)
  {
    IMS_NEW(MafTimer, MEM_IMS_HTTPS, m_pConfigTimer[timerType], (0, 0));
    if(m_pConfigTimer[timerType] == QP_NULL)
    {
      IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::startTimer | New Failed");
      return;
    }

    (QPVOID)m_pConfigTimer[timerType]->SetMafTimerCallBackData(configTimerCallBack, this);
    (QPVOID)m_pConfigTimer[timerType]->StartTimer(iTimerValue);
    m_pConfigTimer[timerType]->SetTimerID(iTimerId);
    m_pConfigTimer[timerType]->SetTimerType(timerType);

    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::startTimer ID: %d",iTimerId);
    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::startTimer Value: %u",iTimerValue);
    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::startTimer Type: %d",timerType);
  }
  else
  {
    IMS_MSG_FATAL_1(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::startTimer | Timer type-%d already running. Impossible situation!!!", timerType);
  }
}

/************************************************************************
Function KTAutoConfig::configTimerCallBack()

Description
Callback function which gets invoked when the existing timers expire.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::configTimerCallBack(QPINT iTimerType, QPUINT32 iTimerId, QPVOID* pUserData)
{
  IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::configTimerCallBack | Enter");

  if(pUserData == QP_NULL)
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::configTimerCallBack | pUserData is NULL");
    return;
  }

  ((KTAutoConfig*)pUserData)->processTimerExpy(iTimerId,iTimerType);
  IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::configTimerCallBack | Exit");
}

/************************************************************************
Function KTAutoConfig::processTimerExpy()

Description
Handler function to take care of the timer expiry

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::processTimerExpy(QPUINT32 iTimerId,QPINT timerType)
{
  IMS_MSG_MED_2(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::processTimerExpy | Enter Expired Timerid %d TimerType - %d", iTimerId, timerType);

  if ( timerType == QP_KT_RETRY_TIMER && (m_iHttpRespWaitCount == QP_KT_CONFIG_MAX_TOTAL_ATTEMPT || m_iHttpRespErrCount == QP_KT_CONFIG_MAX_TOTAL_ATTEMPT))
  {
    deleteTimer(timerType);
    InformACSStatusToApp();
    return;
  }

  if ( timerType == QP_KT_RETRY_TIMER || timerType == QP_KT_CONFIG_VALIDITY_TIMER)
  {
    IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::processTimerExpy | Current configuration or Retry Timer has expired");
    //Delete the expired timer
    deleteTimer(timerType);
    //Send configuration request
    sendConfigRequest();
  }
  else // Should not happen
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::processTimerExpy | Un-handled Timer Type");
  }
}

/************************************************************************
Function KTAutoConfig::update()

Description
Listener for the PDP events from IMSServiceConfig

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::update(EventObject& n_Ev)
{
  IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::update");

  switch(n_Ev.getEventType())
  {
  case QPE_IMS_SERV_CONFIG_PDP_ACTIVATED_EV:
    {
      m_isPDPActive = QP_TRUE;
      IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::update QPE_IMS_SERV_CONFIG_PDP_ACTIVATED_EV Card Status %d m_isPDPActive %d ", m_isCardReady, m_isPDPActive);
      if((m_pConfigTimer[QP_KT_RETRY_TIMER] != QP_NULL && m_pConfigTimer[QP_KT_RETRY_TIMER]->IsTimerRunning() == QP_TRUE) ||
         (m_pConfigTimer[QP_KT_CONFIG_VALIDITY_TIMER] != QP_NULL && m_pConfigTimer[QP_KT_CONFIG_VALIDITY_TIMER]->IsTimerRunning() == QP_TRUE))
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::update Timer Running, Do nothing ");
      }
      else
      {
        /* If RAT change has happened, across PDP Activation/failure,
        ** try with updated RAT value.
        */
        if(m_pHttpContent)
        {
          qpDplFree(MEM_IMS_HTTPS,m_pHttpContent);
          m_pHttpContent = QP_NULL;
        }
        if(m_pUserAgent)
        {
          qpDplFree(MEM_IMS_HTTPS,m_pUserAgent);
          m_pUserAgent = QP_NULL;
        }
        sendConfigRequest();
      }
      break;
    }
  case QPE_IMS_SERV_CONFIG_PDP_FAILURE_EV:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::update QPE_IMS_SERV_CONFIG_PDP_FAILURE_EV");
      m_isPDPActive = QP_FALSE;
      break;
    }
  case QPE_IMS_SERV_CONFIG_IP_ADDRESS_CHANGED_EV:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::update QPE_IMS_SERV_CONFIG_IP_ADDRESS_CHANGED_EV ");
    }
    break;  
  case QPE_IMS_SERV_CONFIG_PDP_DEACTIVATED_EV:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::update QPE_IMS_SERV_CONFIG_PDP_DEACTIVATED_EV");
      m_isPDPActive = QP_FALSE;
    }
    break;
  case QPE_IMS_SERV_CONFIG_HTTP_RESPONSE:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::update QPE_IMS_SERV_CONFIG_HTTP_RESPONSE");
      if ( m_pIMSServiceConfig )
        handleHttpResponse( m_pIMSServiceConfig->GetStatusCode());
    }
    break; 
  default:
    IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::update: Unhandled event  %d", n_Ev.getEventType());
    break;
  }
}

/************************************************************************
Function KTAutoConfig::handleHttpResponse()

Description
Handle the Http response

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::handleHttpResponse(QPUINT32 ihttpStatusCode )
{
  /* First thing is to delete the timer as a response is received */
  deleteTimer(QP_KT_RETRY_TIMER);
  IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse Http Response Code %d", ihttpStatusCode);

  QPBOOL bParserResult = QP_FALSE ;
  const QP_AUTO_CONFIG_STRUCT* pConfigStruct = QP_NULL;
  /* After receiving any response reset all the Http Response wait counter */
  m_iHttpRespWaitCount = 0;
  /* Reset if response if 503 and doesn't have Retry Header */
  m_i503WithRetryAfter = 0;

  switch(ihttpStatusCode)
  {
  case 200:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse 200_OK");
      QPCHAR* pTmpXML = QP_NULL;
      QPUINT32 iXmlLength = 0;
      //Reset the Http Error retry count
      m_iHttpRespErrCount = 0 ;
      if(m_pAuthResponse) /* Delete the previous stored Auth Header */
      {
        qpDplFree(MEM_IMS_HTTPS,m_pAuthResponse);
        m_pAuthResponse = QP_NULL;
      }
      if(m_pHttpContent) /* Delete the previous stored HTTP Message*/
      {
        qpDplFree(MEM_IMS_HTTPS,m_pHttpContent);
        m_pHttpContent = QP_NULL;
      }
      if(m_pUserAgent) /* Delete the previous stored User-Agent */
      {
        qpDplFree(MEM_IMS_HTTPS,m_pUserAgent);
        m_pUserAgent = QP_NULL;
      }
      //Stop the validity timer after getting a 200-OK response
      deleteTimer(QP_KT_CONFIG_VALIDITY_TIMER);
      if ( ! m_pIMSServiceConfig )
        break;      
      pTmpXML = m_pIMSServiceConfig->GetContent(iXmlLength);
      iXmlLength++;// For "\0"
      if(m_pXmlMsgBody)
      {
        qpDplFree(MEM_IMS_HTTPS,m_pXmlMsgBody);
        m_pXmlMsgBody = QP_NULL;
      }
      m_pXmlMsgBody = (QPCHAR*)qpDplMalloc(MEM_IMS_HTTPS, iXmlLength);
      if ( !m_pXmlMsgBody || !pTmpXML  )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse: m_pXmlMsgBody,pTmpXML is NULL");
        break;
      }
      qpDplMemset(m_pXmlMsgBody, 0, iXmlLength);
      (void)qpDplStrlcpy(m_pXmlMsgBody, pTmpXML, iXmlLength);
      QPUINT8 iLogHttpContent = 0;
	  QPIMS_NV_CONFIG_DATA_PTR sRMConfig ;
      qpDplMemset((QPVOID*) &sRMConfig,0,sizeof(QPIMS_NV_CONFIG_DATA_PTR));
      sRMConfig.env_Hdr = eQP_IMS_REG_CONFIG;
      if(QP_IO_ERROR_OK != qpdpl_get_config_group_value_ptr(&sRMConfig))
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse - iRegResponseforOptions not defined, setting to 0");
	    iLogHttpContent = 0;
      }
      else
      {
        iLogHttpContent = sRMConfig.puIMSConfigPayload->sRMConfigItem.iRegResponseforOptions;
        IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse - iRegResponseforOptions = %d", iLogHttpContent);
      }
	  qpdpl_release_config_group_value_ptr(&sRMConfig);
	  
	  if (iLogHttpContent == 1)
	  {
	    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse: XML body:");
	    LogMessageString(m_pXmlMsgBody);
	  }
      //Parse the XML
      bParserResult = updateNVFromXML();
      IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse: XML Parser Result %d", bParserResult);
    }
    break;
  case 401:
    {
      if(m_pHttpContent) /* Delete the previous stored HTTP Message, so that the new Request has Auth header also */
      {
        qpDplFree(MEM_IMS_HTTPS,m_pHttpContent);
        m_pHttpContent = QP_NULL;
      }
      if(m_pUserAgent) /* Delete the previous stored User-Agent */
      {
        qpDplFree(MEM_IMS_HTTPS,m_pUserAgent);
        m_pUserAgent = QP_NULL;
      }
      if ( calculateAuthResponse() )
      {
        sendConfigRequest();
      }
      else
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse, 401 calculateAuthResponse failed ");
      }
    }
    break;
  case 404:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse: 404_SERVER_NOT_FOUND");
      //Have to notify the user to subscribe to service.
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"Please subscribe to service");
      if ( m_pAutoConfigMgr )
        m_pAutoConfigMgr->setConfigSuccessStatus( QP_FALSE );
      ImsRegmanager_GenericEventNotify( eAutoConfigFailureNoSubscription );
      IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse: Informed RM ConfigSuccessStatus( QP_FALSE ), eAutoConfigFailureNoSubscription");
    }
    break;
  case 503:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse: 503_SERV_UNAVAILABLE");
      if ( m_pIMSServiceConfig )
      {
        QPCHAR* pRetryAfter = m_pIMSServiceConfig->GetHeaderValue( (QPCHAR *)("Retry-After") );
        if ( pRetryAfter )
        {
          m_i503WithRetryAfter = qpDplAtoi(pRetryAfter);
          if ( m_pAutoConfigMgr && (pConfigStruct = m_pAutoConfigMgr->GetAutoConfigStruct()) )
          {
            if ( pConfigStruct->validityTime > 0 )
            {
              IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," KTAutoConfig::handleHttpResponse | 503 Retry after Received using existing config ");
              //Notify everyone of the configuration success
              ImsRegmanager_GenericEventNotify( eAutoConfigSuccess );
              m_pAutoConfigMgr->setConfigSuccessStatus( QP_TRUE );
              IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse: Inform RM ConfigSuccessStatus( QP_TRUE ), eAutoConfigSuccess");
            }
            else
            {
              IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," KTAutoConfig::handleHttpResponse | 503 Retry-After Received, Valdity of existing config Expired ");
              m_pAutoConfigMgr->setConfigSuccessStatus( QP_FALSE );
              ImsRegmanager_GenericEventNotify( eAutoConfigFailure );
              IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse: Inform RM ConfigSuccessStatus( QP_FALSE ), eAutoConfigFailure");
            }
            startTimer((m_i503WithRetryAfter*1000), 0, QP_KT_RETRY_TIMER);
            return;
          }
        }
        else
        {
          IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse: Retry After header is not present");
          IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"Your plan does not provide HDVoice/HDVT service");
          if ( m_pAutoConfigMgr )
            m_pAutoConfigMgr->setConfigSuccessStatus( QP_FALSE );
          ImsRegmanager_GenericEventNotify( eAutoConfigFailureNoSubscription );
          IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse: Informed RM ConfigSuccessStatus( QP_FALSE ), eAutoConfigFailureNoSubscription");

          // Reset the cache and update the config from the cache to the EFS
          if ( m_pAutoConfigMgr && m_pAutoConfigStruct)
          {
            qpDplMemset(m_pAutoConfigStruct,0,sizeof(QP_AUTO_CONFIG_STRUCT));
            if ( m_pAutoConfigMgr->UpdateConfig( m_pAutoConfigStruct ) )
            {
              IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"updateNVFromXML | Reset SIM EFS success");
            }
          }
        }
      }
    }
    break;
  default:
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse: Other Error");
    break;
  }

  if ( ( ihttpStatusCode >= 300  && ihttpStatusCode < 700 ) &&
         ihttpStatusCode != 401  && ihttpStatusCode != 503 && ihttpStatusCode != 404)
  {	
    m_iHttpRespErrCount++;
    IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::handleHttpResponse m_iHttpRespErrCount %d", m_iHttpRespErrCount);
    startRetryMechanism();
  }
}

 /************************************************************************
Function KTAutoConfig::calculateAuthResponse()

Description
Calculate the authentication response.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPBOOL KTAutoConfig::calculateAuthResponse( QPVOID )
{
  QPCHAR* pAuthChallenge = QP_NULL;
  SipHeader* pSipHeader  = QP_NULL;
  QPCHAR*    pNonce      = QP_NULL;
  QPCHAR     md5Key[16];
  QPCHAR     hashKey[100];
  QPCHAR     shaKey[32];
  QPCHAR     strmd5Key[65];
  QPCHAR*    pResNonce      = QP_NULL;
  QPCHAR*    pResRealm      = QP_NULL;
  QPUINT8    hashKeyLen = 0;

  qpDplMemset((QPVOID*) &md5Key,0,sizeof(md5Key));
  qpDplMemset((QPVOID*) &hashKey,0,sizeof(hashKey));
  qpDplMemset((QPVOID*) &shaKey,0,sizeof(shaKey));
  qpDplMemset((QPVOID*) &strmd5Key,0,sizeof(strmd5Key));

  if ( m_pIMSServiceConfig )
  {
    pAuthChallenge = m_pIMSServiceConfig->GetHeaderValue( (QPCHAR*)HttpHeadersStrings[QP_AUTHENTICATE] );
    if ( (QP_NULL == pAuthChallenge) || (QP_NULL == m_pImsi) )
    {
      IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::calculateAuthResponse | pAuthChallenge or IMSI is NULL");
      return QP_FALSE;
    }

    SipString strName((QPCHAR*)HttpHeadersStrings[QP_AUTHENTICATE]);
    SipString strValue(pAuthChallenge);
    IMS_NEW(SipHeader, MEM_IMS_HTTPS, pSipHeader, (strName, strValue));
    if(pSipHeader == QP_NULL)
    {
      IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::calculateAuthResponse | new SipHeader failed");
      return QP_FALSE;
    }

    pNonce = removeQoutes(pSipHeader->getParameter((SipString)QP_SIP_NONCE_STR));
    if ( QP_NULL == pNonce )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::calculateAuthResponse pNonce|m_pImsi is NULL");
      IMS_DELETE(pSipHeader,MEM_IMS_HTTPS);
      return QP_FALSE;
    }
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::calculateAuthResponse pNonce %s", pNonce);
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::calculateAuthResponse Imsi Value %s", m_pImsi->pIMSIValue);

    if ( qpDplHashCreateDigest(QP_DPL_SECAPI_SHA256,(QPUINT8*)(m_pImsi->pIMSIValue),qpDplStrlen(m_pImsi->pIMSIValue),(QPUINT8*)shaKey) )
    {
      hashKeyLen = 	qpDplStrlen( pNonce);
	  if ( (qpDplStrlen( pNonce) + qpDplStrlen(QP_KT_COLON_STR) + sizeof(strmd5Key) ) >  sizeof(hashKey)  )
	  {
    	  IMS_DELETE(pSipHeader,MEM_IMS_HTTPS);
 	  	  qpDplFree(MEM_IMS_HTTPS,pNonce);
		  return QP_FALSE ; 
	  }
	 
	  qpDplMemscpy(hashKey,sizeof(hashKey),pNonce,qpDplStrlen( pNonce)); 
	  qpDplMemscpy(hashKey+hashKeyLen,(sizeof(hashKey)-hashKeyLen),QP_KT_COLON_STR,qpDplStrlen(QP_KT_COLON_STR));
      hashKeyLen++;
      ConvertHexToString( shaKey, 32, strmd5Key );
	  qpDplMemscpy(hashKey+hashKeyLen,(sizeof(hashKey)-hashKeyLen),strmd5Key,sizeof(strmd5Key));
	  hashKeyLen += 64;
	  if ( qpDplHashCreateDigest(QP_DPL_SECAPI_MD5,(QPUINT8*)hashKey,hashKeyLen,(QPUINT8*)md5Key) )
	  {
        IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::calculateAuthResponse Size of MD5 hash key %d md5[0] 0x%x", sizeof(md5Key), md5Key[0]);
        ConvertHexToString( md5Key, 16, strmd5Key );
      }
    }

    if(QP_NULL != m_pAuthResponse)
    {
      qpDplFree(MEM_IMS_HTTPS,m_pAuthResponse);
      m_pAuthResponse = QP_NULL;
    }

    m_pAuthResponse = (QPCHAR *)qpDplMalloc(MEM_IMS_HTTPS, QP_MAX_HEADER_LEN * sizeof(QPCHAR));

    if ( QP_NULL == m_pAuthResponse )
    {
      IMS_DELETE(pSipHeader,MEM_IMS_HTTPS);
      qpDplFree(MEM_IMS_HTTPS,pNonce);
      return QP_FALSE ;
    }
    qpDplMemset(m_pAuthResponse, 0, QP_MAX_HEADER_LEN * sizeof(QPCHAR));

    (QPVOID)qpDplStrlcpy(m_pAuthResponse, "Digest ", QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "realm=\"", QP_MAX_HEADER_LEN);
    pResRealm = removeQoutes(pSipHeader->getParameter((SipString)QP_SIP_REALM_STR));
    if ( pResRealm )
      (QPVOID)qpDplStrlcat(m_pAuthResponse, pResRealm, QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "\",nonce=\"", QP_MAX_HEADER_LEN);
    pResNonce = removeQoutes(pSipHeader->getParameter((SipString)QP_SIP_NONCE_STR));
    if ( pResNonce )
      (QPVOID)qpDplStrlcat(m_pAuthResponse, pResNonce, QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "\",username=\"", QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse,m_strMSISDN, QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "\",uri=\"", QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse,QP_KT_FWDSLASH_STR,QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse,QP_KT_URL_PRE,QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "\",response=\"", QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse,  strmd5Key, QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "\"", QP_MAX_HEADER_LEN);
  }
  IMS_DELETE(pSipHeader,MEM_IMS_HTTPS);
  qpDplFree(MEM_IMS_HTTPS,pNonce);
  if ( pResRealm ) 
    qpDplFree(MEM_IMS_HTTPS,pResRealm);
  if ( pResNonce )
    qpDplFree(MEM_IMS_HTTPS,pResNonce);
  return QP_TRUE;
}

/************************************************************************
Function KTAutoConfig::updateNVFromXML()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPBOOL KTAutoConfig::updateNVFromXML( QPVOID )
{
  QPUINT16                  charListSize = 0;
  XMLContext*               pContext  = QP_NULL ;
  qp_wap_provisioning_doc*  pObj;
  XMLUnMarshaller*          pUnMarshaller;
  QPBOOL bRetValue = QP_FALSE ;
  const QP_AUTO_CONFIG_STRUCT* pConfigStruct = QP_NULL;

  if ( !m_pAutoConfigMgr )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," KTAutoConfig::updateNVFromXML, m_pAutoConfigMgr is NULL ");
    return bRetValue;
  }

  if(m_pXmlMsgBody)
  {
    //XMLFactory test.
    pContext = XMLFactory::newInstance(XML_SCHEMA_CONFIG_WAP_PROVISIONING_DOC);

    if ( pContext )
    {
      //XMLContext test.
      pUnMarshaller = pContext->createUnMarshaller();
      if ( pUnMarshaller )
      {    
        pObj = (qp_wap_provisioning_doc*)pUnMarshaller->UnMarshall(m_pXmlMsgBody);	
        if ( pObj )
        {
          QpSingleElementList* pCharList = pObj->getCharacteristicList();
          if ( pCharList )
          {
            charListSize = pCharList->Size();
            IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"updateNVFromXML | XML Charateristic List Size %d",charListSize );
            bRetValue = QP_TRUE ;
            for ( QPUINT index = 0; index < charListSize; index ++ )
            {
              qp_characteristic* pChar = (qp_characteristic*)pCharList->GetKeyAtIndex(index);
              if ( pChar )
              {
                IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"updateNVFromXML | parseCharList, XML Charateristic Name %s at index %d", pChar->getType(), index);
                QpSingleElementList* pParmList = pChar->getParamList();
                if ( pParmList && qpDplStricmp( pChar->getType(),"VERS") == 0 )
                  parseVersion( pParmList );
                QpSingleElementList* pInnerCharList = pChar->getCharacteristicList();
                if ( pInnerCharList && qpDplStricmp( pChar->getType(),"APPLICATION") == 0 )
                  parseApplication(pInnerCharList,pParmList);
                if ( pParmList && qpDplStricmp( pChar->getType(),"EMBEDDED_SERVICE") == 0 )
                  parseEmbeddedService( pParmList );
              }
            }
          }
        }
      }
    }
  }

  if ( bRetValue == QP_FALSE )
  {
    if ((pConfigStruct = m_pAutoConfigMgr->GetAutoConfigStruct()) && pConfigStruct->validityTime > 0)
    {
      m_pAutoConfigMgr->setConfigSuccessStatus( QP_TRUE );
      ImsRegmanager_GenericEventNotify( eAutoConfigSuccess );
      IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::updateNVFromXML: Informed RM ConfigSuccessStatus( QP_TRUE ), eAutoConfigSuccess");
    }
    else
    {
      m_pAutoConfigMgr->setConfigSuccessStatus( QP_FALSE );
      ImsRegmanager_GenericEventNotify( eAutoConfigFailure );
      IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::updateNVFromXML: Informed RM ConfigSuccessStatus( QP_FALSE ), eAutoConfigFailure");
    }
    startTimer((QP_KT_LONG_RETRY_TIMER*1000),0,QP_KT_RETRY_TIMER);
    ResetRetryBehaviour();
    XMLFactory::deleteContext(pContext);
    return bRetValue;
  }

  if ( (qpDplStrcmp(m_currentXmlVersion, "-1") == 0 ) || ( m_currentValidityTime <= 0 ) )
  {
    bRetValue = QP_FALSE ;
    if (m_pAutoConfigStruct)
    {
      qpDplMemset(m_pAutoConfigStruct,0,sizeof(QP_AUTO_CONFIG_STRUCT));
      qpDplStrlcpy(m_pAutoConfigStruct->xmlVersion,m_currentXmlVersion,QP_AUTO_CONFIG_VER_LEN);
      m_pAutoConfigStruct->validityTime = m_currentValidityTime;
    }
    ImsRegmanager_GenericEventNotify( eAutoConfigFailureNoSubscription );
    m_pAutoConfigMgr->setConfigSuccessStatus( QP_FALSE );
    IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::updateNVFromXML: Informed RM ConfigSuccessStatus( QP_FALSE ), eAutoConfigFailure");
  }
  else if ( ( charListSize >= 1 ) && (m_currentValidityTime > 0 ) )
  {
    ImsRegmanager_GenericEventNotify( eAutoConfigSuccess );
    m_pAutoConfigMgr->setConfigSuccessStatus( QP_TRUE );
    IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::updateNVFromXML: Informed RM ConfigSuccessStatus( QP_TRUE ), eAutoConfigSuccess");
  }

  // Update the config from the cache to the EFS
  if ( m_pAutoConfigStruct )
  {
    if ( m_pAutoConfigMgr->UpdateConfig( m_pAutoConfigStruct ) )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"updateNVFromXML | EFS has been updated successfully");
    }
  }
  XMLFactory::deleteContext(pContext);
  return bRetValue;
}

/************************************************************************
Function KTAutoConfig::parseVersion()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::parseVersion(QpSingleElementList* pParmList)
{
  QPCHAR parmValue[PARM_MAX_LEN];
  QPCHAR parmName[PARM_MAX_LEN];
  QPCHAR  currentXmlVersion[QP_AUTO_CONFIG_VER_LEN];
  QPCHAR *configXMLVersion = QP_NULL ;
  QPCHAR *commonConfigVersion = QP_NULL ;
  QPCHAR *userConfigVersion = QP_NULL ;
  QPCHAR *IMSConfigVersion = QP_NULL ;

  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseVersion, m_pAutoConfigStruct is NULL ");
    return;
  }

  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseVersion, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);

      if ( qpDplStricmp( parmName,"version") == 0 )
      {
        qpDplStrlcpy(m_currentXmlVersion,parmValue,QP_AUTO_CONFIG_VER_LEN);
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseVersion, version is %s ",m_currentXmlVersion );
        qpDplStrlcpy(m_pAutoConfigStruct->xmlVersion,parmValue,QP_AUTO_CONFIG_VER_LEN);
        (QPVOID)qpDplStrlcpy(currentXmlVersion, parmValue, sizeof(m_currentXmlVersion));

        configXMLVersion = qpDplStrtok( currentXmlVersion,"." );
        if (configXMLVersion)
          commonConfigVersion = qpDplStrtok( QP_NULL,"." );
        if (commonConfigVersion)
          userConfigVersion = qpDplStrtok( QP_NULL,"." );
        if (userConfigVersion)
          IMSConfigVersion = qpDplStrtok( QP_NULL,"." );
        if ( IMSConfigVersion )
        {
          if (qpDplStrstr(IMSConfigVersion, "-1"))
          {
            qpDplMemset(m_pAutoConfigStruct,0,sizeof(QP_AUTO_CONFIG_STRUCT));
            qpDplStrlcpy(m_pAutoConfigStruct->xmlVersion,"-1",QP_AUTO_CONFIG_VER_LEN); 
            qpDplStrlcpy(m_currentXmlVersion,"-1",QP_AUTO_CONFIG_VER_LEN);
          }
        }
      }
      else if ( qpDplStricmp( pParm->getParmName(),"validity") == 0 )
      {
        m_currentValidityTime = qpDplAtoi(parmValue);
        m_pAutoConfigStruct->validityTime = m_currentValidityTime;
        IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"parseVersion, validity is %d ",m_currentValidityTime );
        if(m_currentValidityTime > 0)
        {
          startTimer(m_currentValidityTime*1000,0,QP_KT_CONFIG_VALIDITY_TIMER);
        }
      }
    }//ParamList For Loop
  }
}

/************************************************************************
Function KTAutoConfig::parseApplication()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::parseApplication(QpSingleElementList* pCharList,QpSingleElementList* pParmList)
{
  QPCHAR charName[PARM_MAX_LEN];
  QPCHAR parmValue[PARM_MAX_LEN];
  QPCHAR parmName[PARM_MAX_LEN];
  QPIMS_NV_CONFIG_DATA         sConfig;

  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, m_pAutoConfigStruct is NULL ");
    return;
  }
  qpDplMemset((QPVOID*) &sConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));

  //Create NV config DATA.
  sConfig.env_Hdr = eQP_IMS_PARAM_CONFIG;

  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication qpdpl_get_config_group_value failed");
    return;
  }

  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);
      if ( qpDplStricmp( parmName,"Private_User_Identity") == 0 )
      {       
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, Private URI is %s ",parmValue );
        qpDplStrlcpy(sConfig.uIMSConfigPayload.sUserConfigItem.regConfigPrivateURI,parmValue,QP_REG_CONFIG_PRIVATE_URI_LEN);
      }
      else if ( qpDplStricmp( parmName,"Home_network_domain_name") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, home domain is %s ",parmValue );
        qpDplStrlcpy(sConfig.uIMSConfigPayload.sUserConfigItem.regConfigDomainName,parmValue,QP_REG_CONFIG_DOMAIN_NAME);
      }
    }//ParamList For Loop  
  }
  if ( qpdpl_set_config_group_value(&sConfig) == QP_IO_ERROR_OK )
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication | Config Updated Successfully ");
  if ( pCharList )
  {
    QPUINT charListSize = pCharList->Size();
    IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, XML Charateristic List Size %d",charListSize );
    for ( QPUINT index = 0; index < charListSize; index ++ )
    {
      qp_characteristic* pChar = (qp_characteristic*)pCharList->GetKeyAtIndex(index);
      if ( pChar )
      {
        qpDplStrlcpy(charName,pChar->getType(),sizeof(charName));
        IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, XML Charateristic Name %s at index %d", charName, index);
        QpSingleElementList* pParmList = pChar->getParamList();

        if ( qpDplStricmp( charName,"LBO_P-CSCF_Address") == 0 )
          parsePcscf( pParmList );
        else if ( qpDplStricmp( charName,"APPAUTH") == 0 )
          parseAppAuth( pParmList );
        else if ( qpDplStricmp( charName,"Public_User_Identity_List") == 0 )
          parsePublicUserIDList( pParmList );
        else
          IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, Un-handled XML Charateristic Name %s at index %d", charName, index);
      }
    }
  }
}

/************************************************************************
Function KTAutoConfig::parsePcscf()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::parsePcscf(QpSingleElementList* pParmList)
{
  QPCHAR                parmValue[PARM_MAX_LEN];
  QPCHAR                parmName[PARM_MAX_LEN];
  QPCHAR                pcscfAddress[QP_REG_PRE_CONFIG_SERVER_BASE_LEN];
  QPIMS_NV_CONFIG_DATA  sDplConfig;
  QPIMS_NV_CONFIG_DATA  sRegConfig;

  qpDplMemset((QPVOID*) &sDplConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
  qpDplMemset((QPVOID*) &sRegConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));

  //Create NV config DATA.
  sDplConfig.env_Hdr = eQP_IMS_DPL_CONFIG;
  sRegConfig.env_Hdr = eQP_IMS_REG_CONFIG;

  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sDplConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf | DPL qpdpl_get_config_group_value failed");
    return;
  }
  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sRegConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf | RM qpdpl_get_config_group_value failed");
    return;
  }
  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);
      if ( qpDplStricmp( parmName,"Address") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf, Address is %s ",parmValue);
        qpDplStrlcpy(pcscfAddress,pParm->getParmValue(),QP_REG_PRE_CONFIG_SERVER_BASE_LEN);
      }
      else if ( qpDplStricmp( parmName,"AddressType") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf, AddressType is %s ",parmValue);
        if ( qpDplStricmp( parmValue,"FQDN") == 0 )
        {
          qpDplStrlcpy(sRegConfig.uIMSConfigPayload.sRMConfigItem.regManagerPreConfigServerBase,pcscfAddress,QP_REG_PRE_CONFIG_SERVER_BASE_LEN);
        }
        else if ( qpDplStricmp( parmValue,"IPv4") == 0  && sDplConfig.uIMSConfigPayload.sDplConfigItem.iIPV6Enabled == 0 )
        {
          qpDplStrlcpy(sRegConfig.uIMSConfigPayload.sRMConfigItem.regManagerPreConfigServerBase,pcscfAddress,QP_REG_PRE_CONFIG_SERVER_BASE_LEN);
        }
        else if ( qpDplStricmp( parmValue,"IPv6") == 0  && sDplConfig.uIMSConfigPayload.sDplConfigItem.iIPV6Enabled == 1 )
        {
          qpDplStrlcpy(sRegConfig.uIMSConfigPayload.sRMConfigItem.regManagerPreConfigServerBase,pcscfAddress,QP_REG_PRE_CONFIG_SERVER_BASE_LEN);
        }
        else
        {
          IMS_MSG_LOW_2_STR(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf, AddressType %s and Config %d Mis-match ",parmValue,sDplConfig.uIMSConfigPayload.sDplConfigItem.iIPV6Enabled);
        }
      }
    }//ParamList For Loop  
  }
  if ( qpdpl_set_config_group_value(&sRegConfig) == QP_IO_ERROR_OK )
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf | Config Updated Successfully ");
}

/************************************************************************
Function KTAutoConfig::parseAppAuth()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::parseAppAuth(QpSingleElementList* pParmList)
{
  QPCHAR parmValue[PARM_MAX_LEN];
  QPCHAR parmName[PARM_MAX_LEN];
  QPIMS_NV_CONFIG_DATA  sParmConfig;
  QPIMS_NV_CONFIG_DATA  sSipConfig;

  qpDplMemset((QPVOID*) &sParmConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
  qpDplMemset((QPVOID*) &sSipConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));

  //Create NV config DATA.
  sParmConfig.env_Hdr = eQP_IMS_PARAM_CONFIG;
  sSipConfig.env_Hdr = eQP_IMS_SIP_CONFIG;

  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sParmConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth | qpdpl_get_config_group_value Param config failed");
    return;
  }
  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sSipConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth | qpdpl_get_config_group_value SIP config failed");
    return;
  }
  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, m_pAutoConfigStruct is NULL ");
    return;
  }
  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);
      if ( qpDplStricmp( parmName,"AuthType") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, AuthType is %s ",parmValue);
        if ( ( qpDplStricmp( parmValue,"EarlyIMS") == 0 ))
        {
          sSipConfig.uIMSConfigPayload.sSipConfigItem.eAuthScheme = QPE_CONFIG_AUTH_SCHEME_NONE;
        }
        else if (qpDplStrstr(parmValue, "AKA"))
        {
          sSipConfig.uIMSConfigPayload.sSipConfigItem.eAuthScheme = QPE_CONFIG_AUTH_SCHEME_AKA;
        }
        else
        {
          sSipConfig.uIMSConfigPayload.sSipConfigItem.eAuthScheme = QPE_CONFIG_AUTH_SCHEME_DIGEST;
        }
        qpDplStrlcpy(m_pAutoConfigStruct->reg_struct.authType,parmValue,QP_AUTO_CONFIG_AUTH_TYPE_LEN);
      }
      else if ( qpDplStricmp( parmName,"Realm") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, Realm is %s ",parmValue);
        qpDplStrlcpy(m_pAutoConfigStruct->reg_struct.authRealm,parmValue,QP_AUTO_CONFIG_AUTH_REALM_LEN);
      }
      else if ( qpDplStricmp( parmName,"UserName") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, UserName is %s ",parmValue);
        qpDplStrlcpy(sParmConfig.uIMSConfigPayload.sUserConfigItem.regConfigUserName,parmValue,QP_REG_CONFIG_USER_NAME_LEN);
      }
      else if ( qpDplStricmp( parmName,"UserPwd") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, UserPwd is %s ",parmValue);
        qpDplStrlcpy(sParmConfig.uIMSConfigPayload.sUserConfigItem.regConfigPassword,parmValue,QP_REG_CONFIG_PASSWORD_LEN);
      }
    }//ParamList For Loop  
  }
  if ( qpdpl_set_config_group_value(&sParmConfig) == QP_IO_ERROR_OK && qpdpl_set_config_group_value(&sSipConfig) == QP_IO_ERROR_OK)
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth | Config Updated Successfully ");
}


/************************************************************************
Function KTAutoConfig::parseEmbeddedService()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::parseEmbeddedService(QpSingleElementList* pParmList)
{
  QPCHAR parmValue[PARM_MAX_LEN];
  QPCHAR parmName[PARM_MAX_LEN];

  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseEmbeddedService, m_pAutoConfigStruct is NULL ");
    return;
  }
  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseEmbeddedService, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);
      if ( qpDplStricmp( parmName,"RCS_OEM") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseEmbeddedService, RCS_OEM Service Status is %s ",parmValue );
        m_pAutoConfigStruct->service_struct.RcsOemServiceStatus= qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"VOLTE") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseEmbeddedService, VOLTE Service Status is %s ",parmValue );
        m_pAutoConfigStruct->service_struct.VolteServiceStatus= qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"PSVT") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseEmbeddedService, PSVT Service Status is %s ",parmValue );
        m_pAutoConfigStruct->service_struct.PsvtServiceStatus = qpDplAtoi(parmValue);
      }  
    }//ParamList For Loop  
  }  
}

/************************************************************************
KTAutoConfig::parsePublicUserIDList


Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::parsePublicUserIDList(QpSingleElementList* pParmList)
{
  QPCHAR parmValue[QP_AUTO_CONFIG_PUBLIC_USER_ID_LEN];
  QPCHAR parmName[PARM_MAX_LEN];
  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePublicUserIDList, m_pAutoConfigStruct is NULL ");
    return;
  }
  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      if ( index >= QP_AUTO_CONFIG_PUBLIC_USER_ID_MAX )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePublicUserIDList, reached the max list size count");
        break;
      }
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePublicUserIDList, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),QP_AUTO_CONFIG_PUBLIC_USER_ID_LEN);

      if ( qpDplStricmp( parmName,"Public_User_Identity") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parsePublicUserIDList, Public_User_Identity is %s ",parmValue);
        qpDplStrlcpy(m_pAutoConfigStruct->reg_struct.publicUserId,parmValue,QP_AUTO_CONFIG_PUBLIC_USER_ID_LEN);
      }      
    }//ParamList For Loop  
  }
}



QPVOID KTAutoConfig::InformACSStatusToApp()
{
  const QP_AUTO_CONFIG_STRUCT* pConfigStruct = QP_NULL;

  IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," KTAutoConfig::ProcessMaxRetry | Reached Max Retry count, checking if there is existing config ");
  if ( m_pAutoConfigMgr && (pConfigStruct = m_pAutoConfigMgr->GetAutoConfigStruct()) )
  {
    if((qpDplStrcmp(pConfigStruct->xmlVersion, "-1") != 0) && (pConfigStruct->validityTime > 0))
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," KTAutoConfig::ProcessMaxRetry | Reached Max Retry count, using existing config ");
      //Notify everyone of the configuration success
      ImsRegmanager_GenericEventNotify( eAutoConfigSuccess );
      m_pAutoConfigMgr->setConfigSuccessStatus( QP_TRUE );		
      IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::ProcessMaxRetry: Informed RM ConfigSuccessStatus( QP_TRUE ), eAutoConfigSuccess");
      /* This will inform RM and start a 2 hour timer and reset all the previous retries */
      startTimer(QP_KT_LONG_RETRY_TIMER*1000,0,QP_KT_RETRY_TIMER);
      ResetRetryBehaviour();
    }
    else
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," KTAutoConfig::ProcessMaxRetry | Reached Max Retry count, Valdity of existing config Expired ");
      m_pAutoConfigMgr->setConfigSuccessStatus( QP_FALSE );
      ImsRegmanager_GenericEventNotify( eAutoConfigFailure);
      IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::ProcessMaxRetry: Informed RM ConfigSuccessStatus( QP_FALSE ), eAutoConfigFailure");
      /* This will inform RM to attempt on ISIM if possible and start a 2 hour timer and reset all the previous retries */
      startTimer(QP_KT_LONG_RETRY_TIMER*1000,0,QP_KT_RETRY_TIMER);
      ResetRetryBehaviour();
    }
  }
}


/************************************************************************
QPVOID KTAutoConfig::startRetryMechanism( QPVOID )


Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::startRetryMechanism()
{
  QPINT32 timerValue = 0;
  IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," KTAutoConfig::startRetryMechanism Entered ");

  /* This if will be true only when a HTTP message has been sent on the N/W
  ** When this is called from handleHttpResponse, m_iHttpRespWaitCount will be 0
  */
  if ( m_iHttpRespWaitCount > 0 )
  {
    if(1 == m_iHttpRespWaitCount)
    {
      timerValue = pow(2.0,m_iHttpRespWaitCount);
    }
    else if ( 1 < m_iHttpRespWaitCount && m_iHttpRespWaitCount <= QP_KT_CONFIG_MAX_TOTAL_ATTEMPT )
    {
      timerValue = pow(2.0,2);
    }
    IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::startRetryMechanism | m_iHttpRespWaitCount %d timervalue %d ", m_iHttpRespWaitCount, timerValue);
  }
  /** It will come here when this function is called from handleHttpResponse
  */
  else if(m_iHttpRespErrCount > 0 )
  {
    if(1== m_iHttpRespErrCount)
    {
      timerValue = pow(2.0,m_iHttpRespErrCount);
    }
    else if ( 1 < m_iHttpRespErrCount && m_iHttpRespErrCount <= QP_KT_CONFIG_MAX_TOTAL_ATTEMPT )
    {
      timerValue = pow(2.0,2);
    }
    IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::startRetryMechanism | m_iHttpRespErrCount %d timervalue %d ", m_iHttpRespErrCount, timerValue);
  }
  startTimer(timerValue*1000,0,QP_KT_RETRY_TIMER);
  IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG," KTAutoConfig::startRetryMechanism | timerValue = %d", timerValue);
}

/************************************************************************
Function KTAutoConfig::constructURI()

Description
Construct the URI for configuration request message.

Dependencies
None

Return Value
True -  If request constructed and sent successfully.

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::constructURI()
{
  QPIMS_NV_CONFIG_DATA    sConfig ;
  if(QP_NULL != m_pUri)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pUri);
    m_pUri = QP_NULL;
  }
  m_pUri = (QPCHAR*)qpDplMalloc(MEM_IMS_HTTPS, QP_KT_PUBLIC_ID_LEN);

  if(QP_NULL == m_pUri )
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::constructURI() | Failure in m_pUri  malloc");
    return ;
  }
  qpDplMemset(m_pUri, 0, QP_KT_PUBLIC_ID_LEN);

  if(QP_NULL != m_pImsi)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pImsi);
    m_pImsi = QP_NULL;
  }
  m_pImsi = (QPDPL_IMSI_STRUCT*)qpDplMalloc(MEM_IMS_HTTPS, sizeof(QPDPL_IMSI_STRUCT));

  if(QP_NULL == m_pImsi)
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"constructURI | Failure in m_pImsi malloc");
    qpDplFree(MEM_IMS_HTTPS,m_pUri);
    m_pUri = QP_NULL;
    return;
  }
  qpDplMemset(m_pImsi, 0, sizeof(QPDPL_IMSI_STRUCT));

  qpDplGetIMSI(m_pImsi);

#ifdef FEATURE_IMS_PC_TESTBENCH
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy(m_pImsi->pIMSIValue, "123456789",sizeof(m_pImsi->pIMSIValue)));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy(m_pImsi->pMCC, "001",sizeof(m_pImsi->pMCC)));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy(m_pImsi->pMNC, "010",sizeof(m_pImsi->pMNC)));
#endif // FEATURE_IMS_PC_TESTBENCH

  if (m_pImsi && qpDplStrlen(m_pImsi->pIMSIValue))
  {
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::constructURI() - imsi = %s",m_pImsi->pIMSIValue);
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::constructURI() - mcc = %s", m_pImsi->pMCC);
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::constructURI() - mnc = %s", m_pImsi->pMNC);
  }
  else
  {
    IMS_MSG_LOW_0_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::constructURI() - IMSI Value is not valid ");
    return ;
  }
  qpDplMemset((QPVOID*) &sConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));

  sConfig.env_Hdr = eQP_IMS_RCS_AUTO_CONFIG;

  if( qpdpl_get_config_group_value(&sConfig) == QP_IO_ERROR_OK && qpDplStrlen(sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSConfigServerAddress)!= 0)
  {
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy(m_pUri,sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSConfigServerAddress,QP_KT_PUBLIC_ID_LEN));
  }
  else
  {
    //Start constructing the URI
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlncpy(m_pUri,QP_KT_URI_PRE,QP_KT_PUBLIC_ID_LEN,qpDplStrlen(QP_KT_URI_PRE)));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_KT_PERIOD_STR,QP_KT_PUBLIC_ID_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_KT_URI_MCC,QP_KT_PUBLIC_ID_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,m_pImsi->pMCC,QP_KT_PUBLIC_ID_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_KT_PERIOD_STR,QP_KT_PUBLIC_ID_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_KT_URI_MNC,QP_KT_PUBLIC_ID_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,m_pImsi->pMNC,QP_KT_PUBLIC_ID_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_KT_PERIOD_STR,QP_KT_PUBLIC_ID_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_KT_URI_END,QP_KT_PUBLIC_ID_LEN));
  }

  if( qpDplStrlen(sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSConfigServerPort)!= 0)
  {
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_KT_COLON_STR,QP_KT_PUBLIC_ID_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSConfigServerPort,QP_KT_PUBLIC_ID_LEN));
  }
  else
  {
    //Port is not configured, use the default HTTPS port, "443"
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_KT_COLON_STR,QP_KT_PUBLIC_ID_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_HTTPS_DEFAULT_PORT,QP_KT_PUBLIC_ID_LEN));
  }

  IMS_MSG_MED_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::constructURI()| HTTP URI :\n%s\n", m_pUri);
}

/************************************************************************
Function KTAutoConfig::constructURL()

Description
Construct the URL for configuration request message.

Dependencies
None

Return Value
True -  If request constructed and sent successfully.

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::constructURL()
{ 
  QPIMS_NV_CONFIG_DATA    sConfig ;
  if ( !m_pUrl )
    m_pUrl = (QPCHAR*)qpDplMalloc(MEM_IMS_HTTPS, QP_KT_URL_LEN);

  if(QP_NULL == m_pUrl)
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::constructURL() | Failure in m_pUrl malloc");
    return;
  }
  qpDplMemset(m_pUrl, 0, QP_KT_URL_LEN);

  //  GET /Config?vers=1.1.0.0&client_vendor=SAMSUNG&client_version=1.1.0&terminal_vendor=SAMSUNG&terminal_model=SCH-M810&terminal_sw_version=1.1.1.1 HTTP/1.1
  //Start constructing the URL
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlncpy(m_pUrl,QP_KT_FWDSLASH_STR,QP_KT_URL_LEN,qpDplStrlen(QP_KT_FWDSLASH_STR)));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_URL_PRE,QP_KT_URL_LEN));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_QUES_STR,QP_KT_URL_LEN));
  //Version
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_URI_VERS,QP_KT_URL_LEN));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_EQUAL_STR,QP_KT_URL_LEN));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,m_currentXmlVersion,QP_KT_URL_LEN));

  sConfig.env_Hdr = eQP_IMS_RCS_AUTO_CONFIG;
  // Get the KT Auto config
  if(qpdpl_get_config_group_value(&sConfig) == QP_IO_ERROR_OK)
  {
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_ADDR_STR,QP_KT_URL_LEN));
    //Client_Vendor
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_URI_CLIENTVEN,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_EQUAL_STR,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSClientVendor,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_ADDR_STR,QP_KT_URL_LEN));
    //Client_Version
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_URI_CLIENTVER,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_EQUAL_STR,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSClientVersion,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_ADDR_STR,QP_KT_URL_LEN));
    //Terminal_Vendor
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_URI_TERVEN,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_EQUAL_STR,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSTerminalVendor,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_ADDR_STR,QP_KT_URL_LEN));
    //Terminal_Model
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_URI_TERMOD,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_EQUAL_STR,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSTerminalModel,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_ADDR_STR,QP_KT_URL_LEN));
    //Terminal_SW_Version
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_URI_TERSWVER,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_KT_EQUAL_STR,QP_KT_URL_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSTerminalSWVersion,QP_KT_URL_LEN));
  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_SETTINGS,"sendConfigRequest| qpdpl_get_config_group_value failed");
  }
  IMS_MSG_MED_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::constructURL()| KT URL :\n%s\n", m_pUrl);
}

/************************************************************************
Function KTAutoConfig::sendConfigRequest()

Description
Construct the configuration request message and send it.

Dependencies
None

Return Value
True -  If request constructed and sent successfully.

Side Effects
None
************************************************************************/
QPBOOL KTAutoConfig::sendConfigRequest( QPVOID )
{
  IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest | Enter");

  //Check PDP Status and card status before triggering request
  if ( !m_isPDPActive || !m_isCardReady)
  {
    IMS_MSG_FATAL_2(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest, not sending request | m_isPDPActive %d m_isCardReady %d ", m_isPDPActive, m_isCardReady);
    return QP_FALSE;
  }

  QPIMS_NV_CONFIG_DATA    sConfig ;
  qpDplMemset((QPVOID*) &sConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
  sConfig.env_Hdr = eQP_IMS_RCS_AUTO_CONFIG;
  if(qpdpl_get_config_group_value(&sConfig) != QP_IO_ERROR_OK)
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::sendConfigRequest NV Read Failed");
    return QP_FALSE;
  }

  if(QP_NULL == m_pHttpContent)
  {
    const QP_AUTO_CONFIG_STRUCT* pConfigStruct = QP_NULL;
    QPCHAR *strIMEInumber = QP_NULL ;
    qpDplGetMSISDN((QPCHAR*)&m_strMSISDN,QP_KT_MSISDN_MAX_LEN);

#ifdef FEATURE_IMS_PC_TESTBENCH
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy((QPCHAR*)&m_strMSISDN, "001010123456789",sizeof(m_strMSISDN)));
#endif // FEATURE_IMS_PC_TESTBENCH

    if (qpDplStrlen(m_strMSISDN))
    {
      IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::sendConfigRequest - Msisdn = %s",m_strMSISDN );
    }
    else
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest - Msisdn Value is not valid ");
      return QP_FALSE;
    }

    constructURI(); 

    if ( !m_pAutoConfigStruct )
    {
      if(QP_NULL == (m_pAutoConfigStruct = (QP_AUTO_CONFIG_STRUCT*)qpDplMalloc(MEM_IMS_HTTPS, sizeof(QP_AUTO_CONFIG_STRUCT))))
      {
        IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest | malloc failed on m_pAutoConfigStruct");
        return QP_FALSE;
      }
      qpDplMemset(m_pAutoConfigStruct,0,sizeof(QP_AUTO_CONFIG_STRUCT));
    }

    if( m_pAutoConfigMgr && (pConfigStruct = m_pAutoConfigMgr->GetAutoConfigStruct()) )
    {
      IMS_MSG_HIGH_1_STR(LOG_IMS_RCS_AUTO_CONFIG," sendConfigRequest | Existing IMS config version is %s", pConfigStruct->xmlVersion);
      IMS_MSG_HIGH_1(LOG_IMS_RCS_AUTO_CONFIG," sendConfigRequest | Existing IMS config validty %d",pConfigStruct->validityTime);
      if(  (qpDplStrcmp(pConfigStruct->xmlVersion, "-1") == 0 ) || ( pConfigStruct->validityTime < 0 ) )
      {
        IMS_MSG_HIGH_2_STR(LOG_IMS_RCS_AUTO_CONFIG," sendConfigRequest | Existing IMS config version is %s validty %d", pConfigStruct->xmlVersion,pConfigStruct->validityTime);
        IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG," sendConfigRequest | Exiting ");
        ImsRegmanager_GenericEventNotify( eAutoConfigFailureNoSubscription );
        m_pAutoConfigMgr->setConfigSuccessStatus( QP_FALSE );
        return QP_FALSE;
      }
      if( pConfigStruct->validityTime > 0 )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," sendConfigRequest | valid config exists in efs ");
        qpDplMemset(m_pAutoConfigStruct,0,sizeof(QP_AUTO_CONFIG_STRUCT)); 
        qpDplMemscpy(m_pAutoConfigStruct,sizeof(QP_AUTO_CONFIG_STRUCT),pConfigStruct,sizeof(QP_AUTO_CONFIG_STRUCT)); 
        qpDplStrlcpy(m_currentXmlVersion,pConfigStruct->xmlVersion,QP_AUTO_CONFIG_VER_LEN);		
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest Retrieved version is %s ",m_currentXmlVersion );
      } 
    }	

    constructURL();

    if(QP_NULL != m_pIMEInumber)
    {
      qpDplFree(MEM_IMS_HTTPS,m_pIMEInumber);
      m_pIMEInumber = QP_NULL;
    }
    strIMEInumber = qpDplGetDeviceIMEI();
    if(QP_NULL == (m_pIMEInumber = qpDplStrdup(MEM_IMS_HTTPS, strIMEInumber)))
    {
      IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest | malloc failed on m_pIMEInumber");
      return QP_FALSE;
    }

    m_pHttpContent = (QP_HTTP_MESSAGE_CONTENT*)qpDplMalloc(MEM_IMS_HTTPS, sizeof(QP_HTTP_MESSAGE_CONTENT));
    if(QP_NULL == m_pHttpContent)
    {
      IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest | Failure in pHttpContent malloc");
      return QP_FALSE;
    }
    qpDplMemset(m_pHttpContent, 0, sizeof(QP_HTTP_MESSAGE_CONTENT));

    //User-Agent: KT-client/service name;terminal model;Device_Type=xxx;Net_Type=yyy;RCS identifier
    //e.g., User-Agent: KT-client/RCS-e1.2;iPhone4S;Device_Type=iPhone;Net_Type=LTE;RCS.kor

    if ( m_pHttpContent && m_pUri && m_pUrl )
    {
      (void)qpDplStrlcpy(m_pHttpContent->n_Uri, m_pUri, QP_MAX_SERVER_URI_SIZE);
      (void)qpDplStrlcpy(m_pHttpContent->n_MethodName, METHOD[QP_GET], MAX_METHOD_STR_LEN);
      (void)qpDplStrlcpy(m_pHttpContent->n_Url, m_pUrl, QP_MAX_SERVER_URI_SIZE);
      m_pHttpContent->n_AdditionalHdrsCount = 7;

      SingoConfig* pSingoConfig = SINGLETON_OBJECT(SingoConfig);
      if(pSingoConfig)
      {
        m_pUserAgent = pSingoConfig->GetUserAgentHeader();
      }
      if (QP_NULL == m_pUserAgent || 0 == qpDplStrlen(m_pUserAgent))
      {
        IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest | m_pUserAgent is NULL");
        return QP_FALSE;
      }

      m_pHttpContent->n_AdditionalHeaders[1].hName  = (QPCHAR*)(HttpHeadersStrings[QP_USER_AGENT]);
      m_pHttpContent->n_AdditionalHeaders[1].hValue  = (QPCHAR*)(m_pUserAgent);
      m_pHttpContent->n_AdditionalHeaders[2].hName  = (QPCHAR*)(HttpHeadersStrings[QP_RCS_USER_ID]); 
      m_pHttpContent->n_AdditionalHeaders[2].hValue  = (QPCHAR*)(m_strMSISDN);	
      m_pHttpContent->n_AdditionalHeaders[3].hName  = (QPCHAR*)(HttpHeadersStrings[QP_RCS_SERVICE_TYPE]);
      m_pHttpContent->n_AdditionalHeaders[3].hValue  = (QPCHAR*)(QP_KT_RCS_SERVICE_TYPE);
      m_pHttpContent->n_AdditionalHeaders[4].hName  = (QPCHAR*)(HttpHeadersStrings[QP_RCS_SERVICE_TYPE_IMEI]);
      m_pHttpContent->n_AdditionalHeaders[4].hValue  = (QPCHAR*)(m_pIMEInumber);
      m_pHttpContent->n_AdditionalHeaders[5].hName  = (QPCHAR*)(HttpHeadersStrings[QP_CONNECTION]);
      m_pHttpContent->n_AdditionalHeaders[5].hValue  = KT_CLOSE;
      m_pHttpContent->n_AdditionalHeaders[6].hName  = (QPCHAR*)(HttpHeadersStrings[QP_CONTENT_LENGTH]);
      m_pHttpContent->n_AdditionalHeaders[6].hValue  = KT_ZERO;
      if ( m_pAuthResponse )
      {
        m_pHttpContent->n_AdditionalHdrsCount += 1;
        m_pHttpContent->n_AdditionalHeaders[7].hName  = (QPCHAR*)(HttpHeadersStrings[QP_AUTHORIZATION]);
        m_pHttpContent->n_AdditionalHeaders[7].hValue  = (QPCHAR*)(m_pAuthResponse );
      }
    }
  }

  if ( m_pHttpContent && m_pUri && m_pUrl )
  {
    QPUINT8 iLogHttpContent = 0;
	QPIMS_NV_CONFIG_DATA_PTR sRMConfig ;
    qpDplMemset((QPVOID*) &sRMConfig,0,sizeof(QPIMS_NV_CONFIG_DATA_PTR));
    sRMConfig.env_Hdr = eQP_IMS_REG_CONFIG;
    if(QP_IO_ERROR_OK != qpdpl_get_config_group_value_ptr(&sRMConfig))
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest - iRegResponseforOptions not defined, setting to 0");
	  iLogHttpContent = 0;
    }
    else
    {
      iLogHttpContent = sRMConfig.puIMSConfigPayload->sRMConfigItem.iRegResponseforOptions;
      IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest - iRegResponseforOptions = %d", iLogHttpContent);
    }
	qpdpl_release_config_group_value_ptr(&sRMConfig);
	
	if (iLogHttpContent == 1)
	{
      LogHttpContent(); 
	}  
    if( sConfig.uIMSConfigPayload.sRcsAutoConfigItem.iDisableAutoConfig  == 2 )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest | RCS Auto-Configuration Disabled Flag is set to 2, triggering http request");
      m_pIMSServiceConfig->SendMessage(m_pHttpContent);
    }
    else
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest | triggering https request");
      m_pIMSServiceConfig->SendMessage(m_pHttpContent, QP_HTTPS_CONNECTION);
    }
  }
  else
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::sendConfigRequest | Failed to Send the Request");
    return QP_FALSE;
  }

  m_iHttpRespWaitCount++;
  startRetryMechanism();
  return QP_TRUE;
}

/************************************************************************
Function KTAutoConfig::SipQosStatus()

Description
Pure Virtual function from regmonitor class 

Dependencies
None

Return Value
None.

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::SipQosStatus(QPE_SIPQOS_STATUS iSipQosStatus)
{
  QP_UNUSED(iSipQosStatus);
}

/************************************************************************
Function KTAutoConfig::RegistrationCardStatus()

Description
Virtual function from regmonitor class 

Dependencies
None

Return Value
None.

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::RegistrationCardStatus(QPBOOL n_iCardStatus)
{
  IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"RegistrationCardStatus Called Card Status %d", n_iCardStatus);
  QPBOOL iFileRead = QP_FALSE;
  m_isCardReady = n_iCardStatus;

  IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"RegistrationCardStatus | PDP Status %d", m_isPDPActive);
  //To Remove
  if ( m_isCardReady && m_pIMSServiceConfig)
    m_pIMSServiceConfig->TriggerEstablishPDP();

  if(m_isCardReady && m_pAutoConfigMgr)
  {
    iFileRead = m_pAutoConfigMgr->InitConfigFileInfo();
    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"RegistrationCardStatus | iFileRead %d", iFileRead);

    /* Ideally card ready status will come after the card becomes ready after boot up. 
    ** Otherwise it can even come when w/o reboot SIM has been swaped, eg. lpm-online
    ** else some card event has occurred, re-read all the values.
    */
    QPDPL_IMSI_STRUCT iImsi;
    qpDplMemset((QPVOID*)&iImsi,0,sizeof(QPDPL_IMSI_STRUCT));
    qpDplGetIMSI(&iImsi);
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::RegistrationCardStatus iImsi Value %s", iImsi.pIMSIValue);
    if(m_pImsi && qpDplStrcmp(m_pImsi->pIMSIValue, iImsi.pIMSIValue) != 0)
    {
      ResetCardRelatedParams();
    }
    sendConfigRequest();
  }
}

/************************************************************************
Function KTAutoConfig::RemoveQoutes

Description
Utility function to remove the preceeding and trailing quotes

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPCHAR *KTAutoConfig::removeQoutes(const SipString *n_pStrParam)
{
  QPCHAR*  m_pParam = QP_NULL;
  if(n_pStrParam)
  {
    if(m_pParam)
    {
      qpDplFree(MEM_IMS_HTTPS, m_pParam);
      m_pParam = QP_NULL;
    }
    QPCHAR *pTemp = (QPCHAR *)n_pStrParam->c_str();
    //Remove Quote if present at beginning
    if(pTemp[0] == '\"')
      pTemp++;
    QPINT i = 0;
    while(pTemp[i] != '\0')
    {
      i++;
    }
    //Remove Quote if present at end
    if(pTemp[--i] == '\"')
      pTemp[i] = '\0';

    m_pParam = (QPCHAR *)qpDplMalloc(MEM_IMS_HTTPS, n_pStrParam->length() + 1);
    if(m_pParam == QP_NULL)
    {
      IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"SipConnection::qpSipRemoveQoutes | new failed");
      return QP_NULL;
    }  
    (QPVOID)qpDplStrlcpy(m_pParam, pTemp, (n_pStrParam->length() + 1));
    return m_pParam;
  }
  return QP_NULL;
}

/************************************************************************
Function KTAutoConfig::ResetRetryBehaviour

Description
Reset all the retry behavious counts/flags

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::ResetRetryBehaviour()
{
  m_iHttpRespWaitCount = 0;
  m_iHttpRespErrCount = 0 ; 
}

/************************************************************************
Function KTAutoConfig::ResetCardRelatedParams

Description
Consider this as card swap and reset all the card related params

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::ResetCardRelatedParams()
{
  IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::ResetCardRelatedParams");
  deleteAllTimers();
  ResetRetryBehaviour();
  (QPVOID)qpDplStrlcpy(m_currentXmlVersion, "0.0.0.0", sizeof(m_currentXmlVersion));
  m_currentValidityTime = 0;
  if(m_pAutoConfigStruct)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pAutoConfigStruct);
    m_pAutoConfigStruct = QP_NULL;
  }
  if(m_pAuthResponse) /* Delete the previous stored Auth Header */
  {
    qpDplFree(MEM_IMS_HTTPS,m_pAuthResponse);
    m_pAuthResponse = QP_NULL;
  }
  if(m_pHttpContent) /* Delete the previous stored HTTP Message*/
  {
    qpDplFree(MEM_IMS_HTTPS,m_pHttpContent);
    m_pHttpContent = QP_NULL;
  }
  if(m_pUserAgent) /* Delete the previous stored User-Agent */
  {
    qpDplFree(MEM_IMS_HTTPS,m_pUserAgent);
    m_pUserAgent = QP_NULL;
  }
}

/************************************************************************
Function KTAutoConfig::LogHttpContent

Description
Logs the http content 

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::LogHttpContent()
{
  IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::LogHttpContent:");
  if (m_pHttpContent)
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"URI:");
    LogMessageString(m_pHttpContent->n_Uri);
	IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"URL:");
    LogMessageString(m_pHttpContent->n_Url);
	for(QPUINT32 iHeader = 1;  iHeader < m_pHttpContent->n_AdditionalHdrsCount; iHeader++)
	{
	  IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"Header name: %s", m_pHttpContent->n_AdditionalHeaders[iHeader].hName);
	  LogMessageString(m_pHttpContent->n_AdditionalHeaders[iHeader].hValue);
	}
  }
}
/************************************************************************
Function KTAutoConfig::LogMessageString

Description
Logs the message

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KTAutoConfig::LogMessageString(QPCHAR* n_pMsg)
{
  if (n_pMsg && qpDplStrlen(n_pMsg) > 0)
  {
    QPUINT32 iMsgLength = 0;
	QPCHAR tempLogBuffer[KT_LOG_BUFFER_SIZE+1];
	while (iMsgLength < qpDplStrlen(n_pMsg))
	{
	  qpDplMemset(tempLogBuffer,0,KT_LOG_BUFFER_SIZE+1);
      qpDplMemscpy(tempLogBuffer,KT_LOG_BUFFER_SIZE,n_pMsg+iMsgLength,KT_LOG_BUFFER_SIZE);
      IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"%s",tempLogBuffer);
	  iMsgLength += KT_LOG_BUFFER_SIZE;
	}
  }
  else
  {
	IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KTAutoConfig::LogMessageString - Msg length 0");
  }
}

/************************************************************************
Function KT_autoconfig_initialize() 

Description
Initialize the Enabler. Get the singleton instance 

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/      
QPINT KT_autoconfig_initialize( QPVOID )
{
  QPINT8 iOprtMode = (QPINT8) QP_CONFIG_IMS_OPRT_UNKNOWN_MODE;
  /*Reading the operation mode*/ 
  QPE_IO_ERROR error = QP_IO_ERROR_UNKNOWN;
  /* Reading and storing the oprt mode */
  if(QP_IO_ERROR_OK != (error = qpDplIODeviceGetItem(IMS_OPRT_MODE, (QPVOID*) &iOprtMode, sizeof(iOprtMode))))
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_initialize -no oprt mode defined");
  }
  else
  {
    IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_initialize -oprt mode = %d", iOprtMode);
  }
  /*If operation mode is KT then only APCS needs to be performed*/
  if(QP_CONFIG_IMS_OPRT_KT_MODE == iOprtMode)
  {
    KTAutoConfig* pKTAutoConfig = QP_NULL;
    QPIMS_NV_CONFIG_DATA    sConfig ;

    qpDplMemset((QPVOID*) &sConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
    sConfig.env_Hdr = eQP_IMS_RCS_AUTO_CONFIG;

    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_initialize Called");

    if( qpdpl_get_config_group_value(&sConfig) == QP_IO_ERROR_OK && sConfig.uIMSConfigPayload.sRcsAutoConfigItem.iDisableAutoConfig == 1 )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_initialize | KT Auto-Configuration Disabled");
      return AEE_IMS_EFAILED;
    }

    pKTAutoConfig = KTAutoConfig::getInstance();

    if ( pKTAutoConfig )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_initialize | pKTAutoConfig Instance is Valid");
      pKTAutoConfig->Init();
      //To get the SIM Status
      if ( pKTAutoConfig->SubscribeRegistrationStatus( ) == QP_FALSE )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_initialize | SubscribeRegistrationStatus FAILED");
      }
      else
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_initialize | SubscribeRegistrationStatus SUCCESS");
      }
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_initialize | pKTAutoConfig Instance is not Valid");
      return AEE_IMS_EFAILED;
    }
  }
  return AEE_IMS_SUCCESS;
}


/************************************************************************
Function KT_autoconfig_uninitialize()

Description
Un-Initialize the Enabler. Delete the singleton instance.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID KT_autoconfig_uninitialize( QPVOID )
{
  IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_uninitialize Called");
  QPINT8 iOprtMode = (QPINT8) QP_CONFIG_IMS_OPRT_UNKNOWN_MODE;
  /*Reading the operation mode*/ 
  QPE_IO_ERROR error = QP_IO_ERROR_UNKNOWN;
  /* Reading and storing the oprt mode */
  if(QP_IO_ERROR_OK != (error = qpDplIODeviceGetItem(IMS_OPRT_MODE, (QPVOID*) &iOprtMode, sizeof(iOprtMode))))
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_initialize -no oprt mode defined");
  }
  else
  {
    IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_initialize -oprt mode = %d", iOprtMode);
  }
  /*If operation mode is KT then only APCS needs to be performed*/
  if(QP_CONFIG_IMS_OPRT_KT_MODE == iOprtMode)
  {
    KTAutoConfig* pKTAutoConfig = QP_NULL;
    pKTAutoConfig = KTAutoConfig::getInstance();

    if ( pKTAutoConfig )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_uninitialize | pKTAutoConfig Instance is Valid");
      if ( pKTAutoConfig->UnsubscribeRegistrationStatus( ) == QP_FALSE )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_uninitialize | UnSubscribeRegistrationStatus FAILED");
      }
      else
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_uninitialize | UnSubscribeRegistrationStatus SUCCESS");
      }
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG,"KT_autoconfig_uninitialize | pKTAutoConfig Instance is not Valid");
    }
    AutoConfigMgr::DelInstance();
    KTAutoConfig::delInstance();
  }
}