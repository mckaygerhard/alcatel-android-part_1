/************************************************************************
 Copyright (C) 2011 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : VolteAutoConfig.cpp
 Description    :

 Revision History
 ========================================================================
   Date      |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 20-Jan-2013   Murali Anand                   Initial Version
 -------------------------------------------------------------------------------
 20-04-13      Sankalp            478079      Handling new oprt:6 requirement
 -------------------------------------------------------------------------------
 17-05-13      Priyank            488225      UE is not sending the Authorization 
                                              header in the case of 500 HTTPS error re-try
 -------------------------------------------------------------------------------
 17-05-13      Priyank            488364      Operator 6 Registration using USIM - Domain and P-CSCF values 
                                              are supposed to be read from NV instead of XML file in the case of USIM
 -------------------------------------------------------------------------------
 20-05-13      Priyank            488633      When we insert a new sim card in the UE, UE is not able to differentiate 
                                              this as new sim card and sending the HTTP request with the version of 
                                              the old sim card instead of 0.0.0.0
-------------------------------------------------------------------------------
 24-05-13      Priyank            488227      Timer started with an incorrect duration the case of error scenarios
 -------------------------------------------------------------------------------
 31-05-13      Priyank            493893      After a successful APCS for SIM1, if APCS for SIM2 fails 
                                              then UE is using SIM1 specific config for SIM2 instead of 
                                              throwing "no valid config exist" error for SIM2
-------------------------------------------------------------------------------
 06-06-13      Sankalp            495981      Getting the ICCID value from USIM and then setting it to the AKA header
 -------------------------------------------------------------------------------
 08-07-13      Priyank            509611      Replacing DPL Memcpy with Memscpy (Related to FR 15386)  
-------------------------------------------------------------------------------
 25-07-13      Priyank            494087      Incorrect behavior in "UE receives Validity -1 with SIM1 
                                              and then insert SIM2 and then insert back SIM1" usecase
-------------------------------------------------------------------------------
 20-11-13      Sreenidhi          576767      Addressing KW issues in RM/PM modules
-------------------------------------------------------------------------------
 07-04-14      Sankalp            617708      FR20074: IMS NV Settings Change Notification to Applications from DPL Instead of RM
-------------------------------------------------------------------------------------------------
 09-04-14      Sreenidhi          625130      Request for print full message between UE and ACS
 -------------------------------------------------------------------------------
11-02-15   Priyank       792302      FR 24984: SIM Swap without RESET - Memory leaks in RM
************************************************************************/

#include "math.h"
#include "VolteAutoConfig.h"
#include <qpdefines.h>
#include <qpdefinesCpp.h>
#include <qpdpl.h>
#include <ims_common_defs.h>
#include <SingoIMSServConfigEvManager.h>
#include <IMSConfigMgrApi.h>
#include <AutoConfigMgr.h>
#include "RegFunctions.h"
#include <ims_task.h>

#define PARM_MAX_LEN            50
#define IMS_AKA_AUTH_TYPE         (QPCHAR*)"AKA;"
#define IMS_AKA_ICCID_LEN          30
#define IMS_ICCID_LEN              25
#define QP_SERVICE_ID_HDR_VAL     (QPCHAR*)"VOLTE"
#define QP_CONNECTION_HDR_VAL     (QPCHAR*)"close"
#define QP_CONTENT_HDR_VAL        (QPCHAR*)"0"
#define QP_QXDM_LOG_BUFFER_SIZE   96


// Singleton Instance of a class
VolteAutoConfig* VolteAutoConfig::m_pVolteAutoConfig = QP_NULL;

extern "C"
{
  // Regmanager function to notify of settings change
  extern void ImsRegmanager_GenericEventNotify(QPE_GENERIC_TASK_EVENT iEvent);
}

/************************************************************************
Function VolteAutoConfig::getInstance()

Description
Access method to get the singleton instance of the class

Dependencies
None

Return Value
Singleton Object

Side Effects
None
************************************************************************/
VolteAutoConfig* VolteAutoConfig::getInstance() 
{
  if (m_pVolteAutoConfig == QP_NULL) 
  {
    IMS_NEW(VolteAutoConfig, MEM_IMS_HTTPS,m_pVolteAutoConfig, ());
  }
  return m_pVolteAutoConfig;
}

/************************************************************************
Function VolteAutoConfig::delInstance()

Description
Access method to delete the singleton instance of the class

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::delInstance() 
{
  if (m_pVolteAutoConfig != QP_NULL) 
  {
    IMS_DELETE(m_pVolteAutoConfig,MEM_IMS_HTTPS);
    m_pVolteAutoConfig = QP_NULL;
  }
}
/************************************************************************
Function VolteAutoConfig::VolteAutoConfig()

Description
Constructor

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
VolteAutoConfig::VolteAutoConfig()
{
  m_pUri = QP_NULL;
  m_pUrl = QP_NULL;
  m_pIMSServiceConfig = QP_NULL;
  m_pXmlMsgBody = QP_NULL;
  m_pImsi = QP_NULL;
  m_iHttpRespWaitCount = 0;
  m_iHttpRespErrCount = 0 ;
  m_iLongRetryCount = 0;
  m_bAllHttpRespWaitCountFailureFlag = QP_FALSE;
  m_bAllHttpReTransmissionFailureFlag = QP_FALSE;
  m_i503WithRetryAfter = 0 ;
  m_currentValidityTime = 0;
  m_isCardReady = QP_FALSE;
  m_isPDPActive = QP_FALSE;
  m_isRegPDPActive = QP_TRUE;
  m_isInitial403RegFailure = QP_TRUE;
  m_callCtrlHdl   = QP_NULL;
  m_pHttpContent = QP_NULL;
  for ( int index = 0; index < QP_VOLTE_TIMER_TYPE_MAX; index++ )
  {
    m_pConfigTimer[index] = QP_NULL;
  }
  IMS_NEW(IMSServiceConfig, MEM_IMS_HTTPS,m_pIMSServiceConfig, ());
  //Copy the default xml version.
  (QPVOID)qpDplStrlcpy(m_currentXmlVersion, "0.0.0.0", sizeof(m_currentXmlVersion));
  m_pAuthResponse = QP_NULL;
  m_pAutoConfigMgr = QP_NULL;
  m_pAutoConfigStruct = QP_NULL;
  m_pAKAICCIDVal = QP_NULL;
  m_pUserAgent = QP_NULL;
}

/************************************************************************
Function VolteAutoConfig::~VolteAutoConfig()

Description
Destructor

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
VolteAutoConfig::~VolteAutoConfig()
{
  if(m_pXmlMsgBody)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pXmlMsgBody);
    m_pXmlMsgBody = QP_NULL;
  }
  if(m_pIMSServiceConfig)
  {
    IMS_DELETE(m_pIMSServiceConfig,MEM_IMS_HTTPS);
    m_pIMSServiceConfig = QP_NULL;
  }
  if(m_pUri)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pUri);
    m_pUri = QP_NULL;
  }
  if(m_pUrl)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pUrl);	
    m_pUrl = QP_NULL;
  }
  if(m_pImsi)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pImsi);
    m_pImsi = QP_NULL;
  }

  if(QP_NULL != m_callCtrlHdl)
  {
    qpDplCallCtrlUnInitialize(m_callCtrlHdl);
    m_callCtrlHdl = QP_NULL;
  }
  // Stop and delete all timers running
  deleteAllTimers();

  if(m_pAuthResponse)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pAuthResponse);
    m_pAuthResponse = QP_NULL;
  }
  if(m_pAutoConfigStruct)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pAutoConfigStruct);
    m_pAutoConfigStruct = QP_NULL;
  }
  if(m_pHttpContent)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pHttpContent);
    m_pHttpContent = QP_NULL;
  }
  if(m_pAKAICCIDVal)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pAKAICCIDVal);
    m_pAKAICCIDVal = QP_NULL;
  }
  if(m_pUserAgent)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pUserAgent);
    m_pUserAgent = QP_NULL;
  }
}

/************************************************************************
Function VolteAutoConfig::Init()

Description
Initialize and bring up the PDN for Https traffic

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::Init()
{
  SingoIMSServConfigEvManager* pEvMgr = QP_NULL;
  QPIMS_NV_CONFIG_DATA         sConfig;
  QP_IMS_SERVICE_CONFIG_NV     sImsConfig;
  QPIMS_NV_CONFIG_DATA         nvDPLConfig;
  QPBOOL                       m_bIsIPv6Enabled;
  IMS_LOC_STRUCT               sOprtInfo;

  pEvMgr = SINGLETON_OBJECT(SingoIMSServConfigEvManager);
  qpDplMemset((QPVOID*) &sConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
  qpDplMemset((QPVOID*) &sImsConfig,0,sizeof(QP_IMS_SERVICE_CONFIG_NV));
  qpDplMemset((QPVOID*) &nvDPLConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
  qpDplMemset((QPVOID*) &sOprtInfo,0,sizeof(IMS_LOC_STRUCT));

  m_pAutoConfigMgr = AutoConfigMgr::GetInstance();

  //Create NV config DATA.
  sConfig.env_Hdr = eQP_IMS_RCS_AUTO_CONFIG;

  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::Init qpdpl_get_config_group_value failed");
    return;
  }
  nvDPLConfig.env_Hdr = eQP_IMS_DPL_CONFIG;
  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&nvDPLConfig))
  {
    IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::Init DPL qpdpl_get_config_group_value failed");
    return ;
  }
  m_bIsIPv6Enabled = nvDPLConfig.uIMSConfigPayload.sDplConfigItem.iIPV6Enabled;

  if(pEvMgr != QP_NULL)
  {
    pEvMgr->addEventListener(IMS_SERV_CONFIG_EVENT_LISTENER, this, (QPVOID*)m_pIMSServiceConfig);
  }

  sImsConfig.imsServConfigProfileInfo.eDcmRat = DCM_RAT_LTE;
  sImsConfig.imsServConfigProfileInfo.eAPNType = DCM_APN_INTERNET;
  sImsConfig.imsServConfigProfileInfo.eIPAddrType = (QPE_IMS_SERV_CONFIG_IPADDR_TYPE)((m_bIsIPv6Enabled)? IPV6 : IPV4);

  if(qpDplStrlen(sConfig.uIMSConfigPayload.sRcsAutoConfigItem.InternetPdpProfilename)!= 0)
  {
    (QPVOID)qpDplStrlcpy(sImsConfig.imsServConfigProfileInfo.strApnName, sConfig.uIMSConfigPayload.sRcsAutoConfigItem.InternetPdpProfilename, QP_APN_NAME_LEN);
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::Init() - utPdpProfileName = %s", sImsConfig.imsServConfigProfileInfo.strApnName);
  }
  /*if ( qpDplStrnicmp(sImsConfig.imsServConfigProfileInfo.strApnName,"vzwims",qpDplStrlen("vzwims")) == 0 )
  sImsConfig.imsServConfigProfileInfo.eAPNType = DCM_APN_IMS;*/
  if ( qpDplStrstr(sImsConfig.imsServConfigProfileInfo.strApnName,"ims") )
  {
    sImsConfig.imsServConfigProfileInfo.eAPNType = DCM_APN_IMS;  
    IMS_MSG_LOW_0_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::Init() - Changing the APN type to IMS");
  }
  if ( m_pIMSServiceConfig )
  {
    m_pIMSServiceConfig->Init(&sImsConfig);
    sOprtInfo = DplGetOperatorMode();
	if (IMS_OPRT_MODE_SINGTEL != sOprtInfo.eOperatorMode)
	{
      m_pIMSServiceConfig->SetAppAuthorizationBehaviour(QP_TRUE);
	}
  }

  QP_CMIPAPP_CB_REGISTER_MASK_TYPE eRegMask   =  QP_CALL_CTRL_REG_OPRT_MODE_CB_MASK;

  qpDplMemset((QPVOID*)&m_strMSISDN, 0, sizeof(QP_VOLTE_MSISDN_MAX_LEN));

  if (QP_NULL == (m_callCtrlHdl = qpDplCallCtrlInitialize( eCALL_TYPE_REG,eSYS_MODE_NO_SRV,eRegMask,VolteAutoConfig::HandleCallCtrlCB,(QPVOID*)this)))
  {
    IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::Init() - qpDplCallCtrlInitialize failed!!");
    return;
  }  

  m_pAKAICCIDVal  = (QPCHAR*) qpDplMalloc(MEM_IMS_HTTPS,sizeof(QPCHAR) * IMS_AKA_ICCID_LEN);
  if(QP_NULL == m_pAKAICCIDVal)
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::Init malloc failed for ICCID");
  }
  else
  {
    qpDplMemset(m_pAKAICCIDVal,0,sizeof(QPCHAR) * IMS_AKA_ICCID_LEN);
  }
}

/************************************************************************
Function VolteAutoConfig::deleteAllTimers()

Description
Delete all the existing timers.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::deleteAllTimers()
{
  QPUINT8 iTmrCnt = 0;
  for(; iTmrCnt < QP_VOLTE_TIMER_TYPE_MAX; iTmrCnt++)
  {
    if(m_pConfigTimer[iTmrCnt] != QP_NULL)
    {
      IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::qpDeleteAllTimers | Timer Type %d", m_pConfigTimer[iTmrCnt]->GetTimerType());
      if(m_pConfigTimer[iTmrCnt]->IsTimerRunning() == QP_TRUE)
      {
        m_pConfigTimer[iTmrCnt]->StopTimer();
      }
      IMS_DELETE(m_pConfigTimer[iTmrCnt], MEM_IMS_HTTPS);
      m_pConfigTimer[iTmrCnt] = QP_NULL;
    }
  }
}

/************************************************************************
Function VolteAutoConfig::deleteTimer()

Description
Delete all the timer matching the timerType

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::deleteTimer(QPINT timerType)
{
  IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::deleteTimer | Deleting Timer type %d", timerType);
  
  if(m_pConfigTimer[timerType] != QP_NULL)
  {
    if(m_pConfigTimer[timerType]->IsTimerRunning() == QP_TRUE)
    {
      IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::deleteTimer | Timer type %d Running, stopping it", timerType);
      m_pConfigTimer[timerType]->StopTimer();
    }
    IMS_DELETE(m_pConfigTimer[timerType], MEM_IMS_HTTPS)
      m_pConfigTimer[timerType] = QP_NULL;
  }
  else
  {
    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::deleteTimer | m_pConfigTimer is NULL Timer type %d", timerType);
  }
}

/************************************************************************
Function VolteAutoConfig::startTimer()

Description
Start the timer with timerType for iTimerValue msecs.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::startTimer(QPUINT32 iTimerValue, QPUINT32 iTimerId,QPE_VOLTE_CONFIG_TIMERS_TYPES timerType)
{
  if(iTimerValue == 0)
  {
    IMS_MSG_ERR_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::startTimer | Timer value is %d Not starting the timer", iTimerValue);
    return;
  }

  if(m_pConfigTimer[timerType] == QP_NULL)
  {
    IMS_NEW(MafTimer, MEM_IMS_HTTPS, m_pConfigTimer[timerType], (0, 0));
    if(m_pConfigTimer[timerType] == QP_NULL)
    {
      IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::startTimer | New Failed");
      return;
    }

    (QPVOID)m_pConfigTimer[timerType]->SetMafTimerCallBackData(configTimerCallBack, this);
    (QPVOID)m_pConfigTimer[timerType]->StartTimer(iTimerValue);
    m_pConfigTimer[timerType]->SetTimerID(iTimerId);
    m_pConfigTimer[timerType]->SetTimerType(timerType);

    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::startTimer ID: %d",iTimerId);
    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::startTimer Value: %u",iTimerValue);
    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::startTimer Type: %d",timerType);
  }
  else
  {
    IMS_MSG_FATAL_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::startTimer | Timer type-%d already running. Impossible situation!!!", timerType);
  }
}

/************************************************************************
Function VolteAutoConfig::configTimerCallBack()

Description
Callback function which gets invoked when the existing timers expire.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::configTimerCallBack(QPINT iTimerType, QPUINT32 iTimerId, QPVOID* pUserData)
{
  IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::configTimerCallBack | Enter");
    
  if(pUserData == QP_NULL)
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::configTimerCallBack | pUserData is NULL");
    return;
  }
  
  ((VolteAutoConfig*)pUserData)->processTimerExpy(iTimerId,iTimerType);

  IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::configTimerCallBack | Exit");
}

/************************************************************************
Function VolteAutoConfig::processTimerExpy()

Description
Handler function to take care of the timer expiry

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::processTimerExpy(QPUINT32 iTimerId,QPINT timerType)
{
  IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::processTimerExpy | Enter Expired TimerType - %d", timerType);

  if ( ( timerType == QP_VOLTE_RETRY_TIMER) &&  QP_TRUE == m_bAllHttpRespWaitCountFailureFlag && m_iHttpRespWaitCount == QP_CONFIG_MAX_RETRY_COUNT)
  {
    deleteTimer(timerType);
    m_iHttpRespWaitCount++;
    InformAPCSStatusToApp();
    return;
  }
  if ( timerType == QP_VOLTE_RETRY_TIMER || timerType == QP_VOLTE_CONFIG_VALIDITY_TIMER)
  {
    IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::processTimerExpy | Current configuration or Retry Timer has expired");
    //Delete the expired timer
    deleteTimer(timerType);
    //Send configuration request
    sendConfigRequest();
  }
  else // Should not happen
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::processTimerExpy | Un-handled Timer Type");
  }
}

/************************************************************************
Function VolteAutoConfig::update()

Description
Listener for the PDP events from IMSServiceConfig

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::update(EventObject& n_Ev)
{
	IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::update");
  
	switch(n_Ev.getEventType())
	{
	case QPE_IMS_SERV_CONFIG_PDP_ACTIVATED_EV:
		{
			IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::update QPE_UT_PDP_ACTIVATED_EV Card Status %d", m_isCardReady);
      m_isPDPActive = QP_TRUE;
        sendConfigRequest();
      break;
		}
	case QPE_IMS_SERV_CONFIG_PDP_FAILURE_EV:
		{
			IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::update QPE_IMS_SERV_CONFIG_PDP_FAILURE_EV");
			m_isPDPActive = QP_FALSE;
      break;
		}
	case QPE_IMS_SERV_CONFIG_IP_ADDRESS_CHANGED_EV:
		{
			IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::update QPE_IMS_SERV_CONFIG_IP_ADDRESS_CHANGED_EV ");
		}
		break;  
	case QPE_IMS_SERV_CONFIG_PDP_DEACTIVATED_EV:
		{
			IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::update QPE_IMS_SERV_CONFIG_PDP_DEACTIVATED_EV");
      m_isPDPActive = QP_FALSE;
		}
		break;
 	case QPE_IMS_SERV_CONFIG_HTTP_RESPONSE:
		{
			IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::update QPE_IMS_SERV_CONFIG_HTTP_RESPONSE");
      if ( m_pIMSServiceConfig )
        handleHttpResponse( m_pIMSServiceConfig->GetStatusCode());
		}
		break; 
  default:
		IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::update: Unhandled event  %d", n_Ev.getEventType());
		break;
	}
}

/************************************************************************
Function VolteAutoConfig::handleHttpResponse()

Description
Handle the Http response

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::handleHttpResponse(QPUINT32 ihttpStatusCode )
{
  /* First thing is to delete the timer as a response is received */
  deleteTimer(QP_VOLTE_RETRY_TIMER);
  IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse Http Response Code %d", ihttpStatusCode);

  QPBOOL bParserResult = QP_FALSE ;
  const QP_AUTO_CONFIG_STRUCT* pConfigStruct = QP_NULL;
  /* After receiving any response reset all the Http Response wait counter and their flag */
  m_iHttpRespWaitCount = 0;
  m_bAllHttpRespWaitCountFailureFlag = QP_FALSE;
  /* Reset if response if 503 and doesn't have Retry Header */
  m_i503WithRetryAfter = 0;

  switch(ihttpStatusCode)
  {
  case 200:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse 200_OK");
      QPCHAR* pTmpXML = QP_NULL;
      QPUINT32 iXmlLength = 0; 
      //Reset the Http Error retry count and the corresponding flag
      m_iHttpRespErrCount = 0 ;
      m_bAllHttpReTransmissionFailureFlag = QP_FALSE;
      //Reset the long Retry Count also
      m_iLongRetryCount = 0;
      if(m_pAuthResponse) /* Delete the previous stored Auth Header */
      {
        qpDplFree(MEM_IMS_HTTPS,m_pAuthResponse);
        m_pAuthResponse = QP_NULL;
      }
      if(m_pHttpContent) /* Delete the previous stored HTTP Message*/
      {
        qpDplFree(MEM_IMS_HTTPS,m_pHttpContent);
        m_pHttpContent = QP_NULL;
      }
      //Stop the validity timer after getting a 200-OK response
      deleteTimer(QP_VOLTE_CONFIG_VALIDITY_TIMER);
      if ( ! m_pIMSServiceConfig )
        break;      
      pTmpXML = m_pIMSServiceConfig->GetContent(iXmlLength);
      iXmlLength++;// For "\0"
      if(m_pXmlMsgBody)
      {
        qpDplFree(MEM_IMS_HTTPS,m_pXmlMsgBody);
        m_pXmlMsgBody = QP_NULL;
      }
      m_pXmlMsgBody = (QPCHAR*)qpDplMalloc(MEM_IMS_HTTPS, iXmlLength);
      if ( !m_pXmlMsgBody || !pTmpXML  )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse: m_pXmlMsgBody,pTmpXML is NULL");
        break;
      }
      qpDplMemset(m_pXmlMsgBody, 0, iXmlLength);
      (void)qpDplStrlcpy(m_pXmlMsgBody, pTmpXML, iXmlLength);

      QPUINT8 iLogHttpContent = 0;
	  QPIMS_NV_CONFIG_DATA_PTR sRMConfig ;
      qpDplMemset((QPVOID*) &sRMConfig,0,sizeof(QPIMS_NV_CONFIG_DATA_PTR));
      sRMConfig.env_Hdr = eQP_IMS_REG_CONFIG;
      if(QP_IO_ERROR_OK != qpdpl_get_config_group_value_ptr(&sRMConfig))
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse - iRegResponseforOptions not defined, setting to 0");
	    iLogHttpContent = 0;
      }
      else
      {
        iLogHttpContent = sRMConfig.puIMSConfigPayload->sRMConfigItem.iRegResponseforOptions;
        IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse - iRegResponseforOptions = %d", iLogHttpContent);
      }
	  qpdpl_release_config_group_value_ptr(&sRMConfig);
	  if (iLogHttpContent == 1)
	  {
	    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse, XML body: ");
	    LogMessageString(m_pXmlMsgBody);
	  }
      //Parse the XML
      bParserResult = updateNVFromXML();
      if ( QP_TRUE == bParserResult )
      {
        //Notify everyone of the configuration success
        ImsRegmanager_GenericEventNotify( eAutoConfigSuccess );
      }
      else if ( ( m_pAutoConfigStruct )  && ( m_pAutoConfigStruct->validityTime > 0 ) )
      {
        ImsRegmanager_GenericEventNotify( eAutoConfigSuccess );
      }
      else
        return ;
      // Updating the status in auto-config so that applications can query until they register with RegMonitor
      if ( !m_pAutoConfigMgr )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse, m_pAutoConfigMgr is NULL ");
        return;
      }
      m_pAutoConfigMgr->setConfigSuccessStatus(QP_TRUE);	  
    }
    break;
  case 401:
    {
      if(m_pHttpContent) /* Delete the previous stored HTTP Message, so that the new Request has Auth header also */
      {
        qpDplFree(MEM_IMS_HTTPS,m_pHttpContent);
        m_pHttpContent = QP_NULL;
      }

      if ( calculateAuthResponse() )
      {
        sendConfigRequest();
      }
      else
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse, 401 calculateAuthResponse failed ");
      }
    }
    break;
  case 400:
  case 403:
  case 404:
  case 500:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse: ReTry Request");
    }
    break;
  case 503:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse: 503_SERV_UNAVAILABLE");
      if ( m_pIMSServiceConfig )
      {
        QPCHAR* pRetryAfter = m_pIMSServiceConfig->GetHeaderValue( (QPCHAR *)("Retry-After") );
        if ( pRetryAfter )
        {
          m_i503WithRetryAfter = qpDplAtoi(pRetryAfter);
          if ( m_pAutoConfigMgr && (pConfigStruct = m_pAutoConfigMgr->GetAutoConfigStruct()) )
          {
            if ( pConfigStruct->validityTime > 0 )
            {
              IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::handleHttpResponse | 503 Retry after Received using existing config ");
              //Notify everyone of the configuration success
              ImsRegmanager_GenericEventNotify( eAutoConfigSuccess );
              m_pAutoConfigMgr->setConfigSuccessStatus( QP_TRUE );	      
            }
            else
            {
              IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::handleHttpResponse | 503 Retry after Received , no existing config ");
              m_pAutoConfigMgr->setConfigSuccessStatus( QP_FALSE );
              ImsRegmanager_GenericEventNotify( eAutoConfigFailure );
            }
          }
          IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse Retry After Value is %s",pRetryAfter);
          IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::handleHttpResponse | Retry Header count m_i503WithRetryAfter = %d ", m_i503WithRetryAfter);
          //Start retry after timer.
          startTimer(m_i503WithRetryAfter*1000,0,QP_VOLTE_RETRY_TIMER);
        }
        else
        {
          IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse Retry After header is not present");
        }
      }
    }
    break;
  default:
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse: Other Error");
    break;
  }

  if ( ( ihttpStatusCode >= 300 ) && ( ihttpStatusCode < 700 ) && ( ihttpStatusCode != 401 ) && (m_i503WithRetryAfter == 0 ) )
  {
    m_iHttpRespErrCount++;
    IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::handleHttpResponse m_iHttpRespErrCount %d", m_iHttpRespErrCount);
    startRetryMechanism();
  }
}

 /************************************************************************
Function VolteAutoConfig::calculateAuthResponse()

Description
Calculate the authentication response.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPBOOL VolteAutoConfig::calculateAuthResponse( QPVOID )
{
  QPCHAR* pAuthChallenge = QP_NULL;
  SipHeader* pSipHeader  = QP_NULL;
  QPCHAR*    pNonce      = QP_NULL;
  QPCHAR     md5Key[16];
  QPCHAR     hashKey[100];
  QPCHAR     shaKey[32];
  QPCHAR     strmd5Key[65];
  QPCHAR*    pResNonce      = QP_NULL;
  QPCHAR*    pResRealm      = QP_NULL;
  QPUINT8    hashKeyLen = 0;
  qpDplMemset((QPVOID*) &md5Key,0,sizeof(md5Key));
  qpDplMemset((QPVOID*) &hashKey,0,sizeof(hashKey));
  qpDplMemset((QPVOID*) &shaKey,0,sizeof(shaKey));
  qpDplMemset((QPVOID*) &strmd5Key,0,sizeof(strmd5Key));

  if ( m_pIMSServiceConfig )
  {
    pAuthChallenge = m_pIMSServiceConfig->GetHeaderValue( (QPCHAR*)HttpHeadersStrings[QP_AUTHENTICATE] );
    if ( (QP_NULL == pAuthChallenge) || (QP_NULL == m_pImsi) )
    {
      IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::calculateAuthResponse | pAuthChallenge or IMSI is NULL");
      return QP_FALSE;
    }

    SipString strName((QPCHAR*)HttpHeadersStrings[QP_AUTHENTICATE]);
    SipString strValue(pAuthChallenge);
    IMS_NEW(SipHeader, MEM_IMS_HTTPS, pSipHeader, (strName, strValue));
    if(pSipHeader == QP_NULL)
    {
      IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::calculateAuthResponse | new SipHeader failed");
      return QP_FALSE;
    }
    pNonce = removeQoutes(pSipHeader->getParameter((SipString)QP_SIP_NONCE_STR));
    if ( QP_NULL == pNonce )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::calculateAuthResponse pNonce|m_pImsi is NULL");
      IMS_DELETE(pSipHeader,MEM_IMS_HTTPS);
      return QP_FALSE;
    }
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::calculateAuthResponse pNonce %s", pNonce);
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::calculateAuthResponse Imsi Value %s", m_pImsi->pIMSIValue);

    if ( qpDplHashCreateDigest(QP_DPL_SECAPI_SHA256,(QPUINT8*)(m_pImsi->pIMSIValue),qpDplStrlen(m_pImsi->pIMSIValue),(QPUINT8*)shaKey) )
    {
      hashKeyLen = 	qpDplStrlen( pNonce);
      if ( (qpDplStrlen( pNonce) + qpDplStrlen(QP_VOLTE_COLON_STR) + sizeof(strmd5Key) ) >  sizeof(hashKey)  )
      {
        IMS_DELETE(pSipHeader,MEM_IMS_HTTPS);
        qpDplFree(MEM_IMS_HTTPS,pNonce);
        return QP_FALSE ; 
      }

      qpDplMemscpy(hashKey,sizeof(hashKey), pNonce,qpDplStrlen( pNonce)); 
      qpDplMemscpy(hashKey+hashKeyLen,(sizeof(hashKey)- hashKeyLen), QP_VOLTE_COLON_STR,qpDplStrlen(QP_VOLTE_COLON_STR));
      hashKeyLen++;
      ConvertHexToString( shaKey, 32, strmd5Key );
      qpDplMemscpy(hashKey+hashKeyLen,(sizeof(hashKey)- hashKeyLen), strmd5Key,sizeof(strmd5Key));
      hashKeyLen += 64;

      if ( qpDplHashCreateDigest(QP_DPL_SECAPI_MD5,(QPUINT8*)hashKey,hashKeyLen,(QPUINT8*)md5Key) )
      {
        IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::calculateAuthResponse Size of MD5 hash key %d md5[0] 0x%x", sizeof(md5Key), md5Key[0]);
        ConvertHexToString( md5Key, 16, strmd5Key );
      }
    }

    if(QP_NULL != m_pAuthResponse)
    {
      qpDplFree(MEM_IMS_HTTPS,m_pAuthResponse);
      m_pAuthResponse = QP_NULL;
    }

    m_pAuthResponse = (QPCHAR *)qpDplMalloc(MEM_IMS_HTTPS, QP_MAX_HEADER_LEN * sizeof(QPCHAR));

    if ( QP_NULL == m_pAuthResponse )
    {
      IMS_DELETE(pSipHeader,MEM_IMS_HTTPS);
      qpDplFree(MEM_IMS_HTTPS,pNonce);
      return QP_FALSE ;
    }
    qpDplMemset(m_pAuthResponse, 0, QP_MAX_HEADER_LEN * sizeof(QPCHAR));

    (QPVOID)qpDplStrlcpy(m_pAuthResponse, "Digest ", QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "realm=\"", QP_MAX_HEADER_LEN);
    pResRealm = removeQoutes(pSipHeader->getParameter((SipString)QP_SIP_REALM_STR));
    if ( pResRealm )
    {
      IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::calculateAuthResponse realm %s",pResRealm );
      (QPVOID)qpDplStrlcat(m_pAuthResponse, pResRealm, QP_MAX_HEADER_LEN);
    }
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "\",nonce=\"", QP_MAX_HEADER_LEN);
    pResNonce = removeQoutes(pSipHeader->getParameter((SipString)QP_SIP_NONCE_STR));
    if ( pResNonce )
    {
      IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::calculateAuthResponse nonce %s",pResNonce );
      (QPVOID)qpDplStrlcat(m_pAuthResponse, pResNonce, QP_MAX_HEADER_LEN);
    }
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "\",username=\"", QP_MAX_HEADER_LEN);
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::calculateAuthResponse username %s",m_sktUserName );
    (QPVOID)qpDplStrlcat(m_pAuthResponse,m_sktUserName, QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "\",uri=\"", QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse,QP_VOLTE_FWDSLASH_STR,QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse,QP_VOLTE_URL_PRE,QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "\",response=\"", QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse,  strmd5Key, QP_MAX_HEADER_LEN);
    (QPVOID)qpDplStrlcat(m_pAuthResponse, "\"", QP_MAX_HEADER_LEN);
  }
  IMS_DELETE(pSipHeader,MEM_IMS_HTTPS);
  qpDplFree(MEM_IMS_HTTPS,pNonce);
  if ( pResRealm ) 
    qpDplFree(MEM_IMS_HTTPS,pResRealm);
  if ( pResNonce )
    qpDplFree(MEM_IMS_HTTPS,pResNonce);
  return QP_TRUE;
}

/************************************************************************
Function VolteAutoConfig::updateNVFromXML()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPBOOL VolteAutoConfig::updateNVFromXML( QPVOID )
{
  QPUINT16                  charListSize = 0;

  //XMLFactory test.
  XMLContext*               pContext = QP_NULL ;
  qp_wap_provisioning_doc*  pObj = QP_NULL;
  XMLUnMarshaller*          pUnMarshaller = QP_NULL;
  QPBOOL bRetValue = QP_FALSE ;

  if(m_pXmlMsgBody)
  {
    //XMLFactory test.

    pContext = XMLFactory::newInstance(XML_SCHEMA_CONFIG_WAP_PROVISIONING_DOC);

    if ( pContext )
    {
      //XMLContext test.
      pUnMarshaller = pContext->createUnMarshaller();
      if ( pUnMarshaller )
      {    
        //qp_volte_config_info* pObj = (qp_volte_config_info*)pUnMarshaller->UnMarshall(m_pXmlMsgBody);
        pObj = (qp_wap_provisioning_doc*)pUnMarshaller->UnMarshall(m_pXmlMsgBody);	
        if ( pObj )
        {
          QpSingleElementList* pCharList = pObj->getCharacteristicList();
          if ( pCharList )
          {
            charListSize = pCharList->Size();
            IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"updateNVFromXML | XML Charateristic List Size %d",charListSize );
            bRetValue = QP_TRUE ;
            for ( QPUINT index = 0; index < charListSize; index ++ )
            {
              qp_characteristic* pChar = (qp_characteristic*)pCharList->GetKeyAtIndex(index);
              if ( pChar )
              {
                IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"updateNVFromXML | parseCharList, XML Charateristic Name %s at index %d", pChar->getType(), index);
                QpSingleElementList* pParmList = pChar->getParamList();
                if ( pParmList && qpDplStricmp( pChar->getType(),"VERS") == 0 )
                  parseVersion( pParmList );
                QpSingleElementList* pInnerCharList = pChar->getCharacteristicList();
                if ( pInnerCharList && qpDplStricmp( pChar->getType(),"APPLICATION") == 0 )
                  parseApplication(pInnerCharList,pParmList);
              }
            }
          }
        }
      }
    }
  }

  // Update the config from the cache to the EFS
  if ( m_pAutoConfigMgr && m_pAutoConfigStruct)
  {
    if ( m_pAutoConfigMgr->UpdateConfig( m_pAutoConfigStruct ) )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"updateNVFromXML | EFS has been updated successfully");
    }
  }
  XMLFactory::deleteContext(pContext);
  return bRetValue;
}

/************************************************************************
Function VolteAutoConfig::parseVersion()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::parseVersion(QpSingleElementList* pParmList)
{
  QPCHAR parmValue[PARM_MAX_LEN];
  QPCHAR parmName[PARM_MAX_LEN];

  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseVersion, m_pAutoConfigStruct is NULL ");
    return;
  }

  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseVersion, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);

      if ( qpDplStricmp( parmName,"version") == 0 )
      {
        qpDplStrlcpy(m_currentXmlVersion,parmValue,QP_AUTO_CONFIG_VER_LEN);
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseVersion, version is %s ",m_currentXmlVersion );
        qpDplStrlcpy(m_pAutoConfigStruct->xmlVersion,parmValue,QP_AUTO_CONFIG_VER_LEN);
      }
      else if ( qpDplStricmp( pParm->getParmName(),"validity") == 0 )
      {
        m_currentValidityTime = qpDplAtoi(parmValue);
        m_pAutoConfigStruct->validityTime = m_currentValidityTime;
        IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"parseVersion, validity is %d ",m_currentValidityTime );
        startTimer(m_currentValidityTime*1000,0,QP_VOLTE_CONFIG_VALIDITY_TIMER);
      }
    }//ParamList For Loop
  }
}

/************************************************************************
Function VolteAutoConfig::parseApplication()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::parseApplication(QpSingleElementList* pCharList,QpSingleElementList* pParmList)
{
  QPCHAR charName[PARM_MAX_LEN];
  QPCHAR parmValue[PARM_MAX_LEN];
  QPCHAR parmName[PARM_MAX_LEN];
  QPIMS_NV_CONFIG_DATA         sConfig;

  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, m_pAutoConfigStruct is NULL ");
    return;
  }
  qpDplMemset((QPVOID*) &sConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));

  //Create NV config DATA.
  sConfig.env_Hdr = eQP_IMS_PARAM_CONFIG;

  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication qpdpl_get_config_group_value failed");
    return;
  }

  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);
      if ( qpDplStricmp( parmName,"ServiceStatus") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, Service Status is %s ",parmValue );
        if ( qpDplStricmp( parmValue,"ON") == 0 )
          m_pAutoConfigStruct->service_struct.VolteServiceStatus = QP_TRUE;
        else
          m_pAutoConfigStruct->service_struct.VolteServiceStatus = QP_FALSE;
      }
      /* With the change in Requirement, XML shouldn't update Domain Name, IMPI */
      /*else if ( qpDplStricmp( parmName,"Private_User_Identity") == 0 )
      {       
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, Private URI is %s ",parmValue );
        qpDplStrlcpy(sConfig.uIMSConfigPayload.sUserConfigItem.regConfigPrivateURI,parmValue,QP_REG_CONFIG_PRIVATE_URI_LEN);
      }
      else if ( qpDplStricmp( parmName,"Home_network_domain_name") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, home domain is %s ",parmValue );
        qpDplStrlcpy(sConfig.uIMSConfigPayload.sUserConfigItem.regConfigDomainName,parmValue,QP_REG_CONFIG_DOMAIN_NAME);
      }*/
    }//ParamList For Loop  
  }
  if ( qpdpl_set_config_group_value(&sConfig) == QP_IO_ERROR_OK )
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication | Config Updated Successfully ");
  if ( pCharList )
  {
    QPUINT charListSize = pCharList->Size();
    IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, XML Charateristic List Size %d",charListSize );
    for ( QPUINT index = 0; index < charListSize; index ++ )
    {
      qp_characteristic* pChar = (qp_characteristic*)pCharList->GetKeyAtIndex(index);
      if ( pChar )
      {
        qpDplStrlcpy(charName,pChar->getType(),sizeof(charName));
        IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, XML Charateristic Name %s at index %d", charName, index);
        QpSingleElementList* pParmList = pChar->getParamList();
        if ( qpDplStricmp( charName,"SIPTimer") == 0 )
          parseSIPTimer( pParmList );
        else if ( qpDplStricmp( charName,"RTPPort") == 0 )
          parseRTPPort( pParmList );
        else if ( qpDplStricmp( charName,"RegRetry") == 0 )
          parseRegRetry( pParmList );
        /* With the change in Requirement, XML shouldn't update P-CSCF, AUTH Scheme and IMPU */
        /*else if ( qpDplStricmp( charName,"LBO_P-CSCF_Address") == 0 )
          parsePcscf( pParmList );
        else if ( qpDplStricmp( charName,"APPAUTH") == 0 )
          parseAppAuth( pParmList );
        else if ( qpDplStricmp( charName,"Public_User_Identity_List") == 0 )
          parsePublicUserIDList( pParmList );*/
        else
          IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"parseApplication, Un-handled XML Charateristic Name %s at index %d", charName, index);
      }
    }
  }
}

/************************************************************************
Function VolteAutoConfig::parseSIPTimer()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::parseSIPTimer(QpSingleElementList* pParmList)
{
  QPCHAR parmValue[PARM_MAX_LEN];
  QPCHAR parmName[PARM_MAX_LEN];
  QPIMS_NV_CONFIG_DATA         sConfig;

  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, m_pAutoConfigStruct is NULL ");
    return;
  }
  qpDplMemset((QPVOID*) &sConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));

  //Create NV config DATA.
  sConfig.env_Hdr = eQP_IMS_SIP_CONFIG;

  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer qpdpl_get_config_group_value failed");
    return;
  }

  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);

      if ( qpDplStricmp( parmName,"Timer_T1") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_T1 is %s ",parmValue );
        sConfig.uIMSConfigPayload.sSipConfigItem.iTimer_T1Value = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_T2") == 0 )
      {       
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_T2 is %s ",parmValue );
        sConfig.uIMSConfigPayload.sSipConfigItem.iTimer_T2Value = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_T4") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_T4 is %s ",parmValue );
        sConfig.uIMSConfigPayload.sSipConfigItem.iTimer_T4Value = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_A") == 0 )
      {       
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_A is %s ",parmValue );
        m_pAutoConfigStruct->sip_struct.iTimer_A = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_B") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_B is %s ",parmValue );
        m_pAutoConfigStruct->sip_struct.iTimer_B = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_C") == 0 )
      {       
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_C is %s ",parmValue );
        m_pAutoConfigStruct->sip_struct.iTimer_C = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_D") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_D is %s ",parmValue );
        m_pAutoConfigStruct->sip_struct.iTimer_D = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_E") == 0 )
      {       
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_E is %s ",parmValue );
        m_pAutoConfigStruct->sip_struct.iTimer_E = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_F") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_F is %s ",parmValue );
        sConfig.uIMSConfigPayload.sSipConfigItem.iTimer_TfValue = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_G") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_G is %s ",parmValue );
        m_pAutoConfigStruct->sip_struct.iTimer_G = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_H") == 0 )
      {       
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_H is %s ",parmValue );
        m_pAutoConfigStruct->sip_struct.iTimer_H = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_I") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_I is %s ",parmValue );
        m_pAutoConfigStruct->sip_struct.iTimer_I = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_J") == 0 )
      {       
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_J is %s ",parmValue );
        sConfig.uIMSConfigPayload.sSipConfigItem.iTimer_TJValue = qpDplAtoi(parmValue);
      }
      else if ( qpDplStricmp( parmName,"Timer_K") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer, Timer_K is %s ",parmValue );
        m_pAutoConfigStruct->sip_struct.iTimer_K = qpDplAtoi(parmValue);
      }
    }//ParamList For Loop  
  }
  if ( qpdpl_set_config_group_value(&sConfig) == QP_IO_ERROR_OK )
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseSIPTimer | Config Updated Successfully ");
}

/************************************************************************
Function VolteAutoConfig::parseRTPPort()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::parseRTPPort(QpSingleElementList* pParmList)
{
  QPCHAR parmValue[PARM_MAX_LEN];
  QPCHAR parmName[PARM_MAX_LEN];

  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseRTPPort, m_pAutoConfigStruct is NULL ");
    return;
  }
  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      if ( index >= QP_AUTO_CONFIG_RTP_PORT_MAX )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseRTPPort, reached the max port entry");
        break;
      }
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseRTPPort, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);
      if ( qpDplStricmp( parmName,"Port") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseRTPPort, Port is %s ",parmValue);
        m_pAutoConfigStruct->service_struct.iRTPPort[index] = qpDplAtoi(parmValue);
      }
    }//ParamList For Loop  
  }
}
/************************************************************************
Function VolteAutoConfig::parseRegRetry()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::parseRegRetry(QpSingleElementList* pParmList)
{
  QPCHAR parmValue[PARM_MAX_LEN];
  QPCHAR parmName[PARM_MAX_LEN];
  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseRegRetry, m_pAutoConfigStruct is NULL ");
    return;
  }
  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      if ( index >= QP_AUTO_CONFIG_REG_RETRY_MAX )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseRegRetry, reached the max reg retry count");
        break;
      }
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseRegRetry, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);
      m_pAutoConfigStruct->reg_struct.iRegRetryInterval[index] = qpDplAtoi(parmValue);
      if ( qpDplStricmp( parmName,"Interval1") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseRegRetry, Interval1 is %s ",parmValue);
      }
      else if ( qpDplStricmp( parmName,"Interval2") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseRegRetry, Interval2 is %s ",parmValue);
      }
      else if ( qpDplStricmp( parmName,"Interval3") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseRegRetry, Interval3 is %s ",parmValue);
      }
    }//ParamList For Loop  
  }
}
/************************************************************************
Function VolteAutoConfig::parsePcscf()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::parsePcscf(QpSingleElementList* pParmList)
{
  QPCHAR                parmValue[PARM_MAX_LEN];
  QPCHAR                parmName[PARM_MAX_LEN];
  QPCHAR                pcscfAddress[QP_REG_PRE_CONFIG_SERVER_BASE_LEN];
  QPIMS_NV_CONFIG_DATA  sDplConfig;
  QPIMS_NV_CONFIG_DATA  sRegConfig;

  qpDplMemset((QPVOID*) &sDplConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
  qpDplMemset((QPVOID*) &sRegConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));

  //Create NV config DATA.
  sDplConfig.env_Hdr = eQP_IMS_DPL_CONFIG;
  sRegConfig.env_Hdr = eQP_IMS_REG_CONFIG;

  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sDplConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf | DPL qpdpl_get_config_group_value failed");
    return;
  }
  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sRegConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf | RM qpdpl_get_config_group_value failed");
    return;
  }
  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);
      if ( qpDplStricmp( parmName,"Address") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf, Address is %s ",parmValue);
        qpDplStrlcpy(pcscfAddress,pParm->getParmValue(),QP_REG_PRE_CONFIG_SERVER_BASE_LEN);
      }
      else if ( qpDplStricmp( parmName,"AddressType") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf, AddressType is %s ",parmValue);
        if ( qpDplStricmp( parmValue,"IPv4") == 0  && sDplConfig.uIMSConfigPayload.sDplConfigItem.iIPV6Enabled == 0 )
        {
          qpDplStrlcpy(sRegConfig.uIMSConfigPayload.sRMConfigItem.regManagerPreConfigServerBase,pcscfAddress,QP_REG_PRE_CONFIG_SERVER_BASE_LEN);
        }
        else
        {
          IMS_MSG_LOW_2_STR(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf, AddressType %s and Config %d Mis-match ",parmValue,sDplConfig.uIMSConfigPayload.sDplConfigItem.iIPV6Enabled);
        }
      }
    }//ParamList For Loop  
  }
  if ( qpdpl_set_config_group_value(&sRegConfig) == QP_IO_ERROR_OK )
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePcscf | Config Updated Successfully ");
}
/************************************************************************
Function VolteAutoConfig::parseAppAuth()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::parseAppAuth(QpSingleElementList* pParmList)
{
  QPCHAR parmValue[PARM_MAX_LEN];
  QPCHAR parmName[PARM_MAX_LEN];
  QPIMS_NV_CONFIG_DATA  sParmConfig;
  QPIMS_NV_CONFIG_DATA  sSipConfig;

  qpDplMemset((QPVOID*) &sParmConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
  qpDplMemset((QPVOID*) &sSipConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));

  //Create NV config DATA.
  sParmConfig.env_Hdr = eQP_IMS_PARAM_CONFIG;
  sSipConfig.env_Hdr = eQP_IMS_SIP_CONFIG;

  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sParmConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth | qpdpl_get_config_group_value Param config failed");
    return;
  }
  if(QP_IO_ERROR_OK != qpdpl_get_config_group_value(&sSipConfig))
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth | qpdpl_get_config_group_value SIP config failed");
    return;
  }
  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, m_pAutoConfigStruct is NULL ");
    return;
  }
  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),PARM_MAX_LEN);
      if ( qpDplStricmp( parmName,"AuthType") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, AuthType is %s ",parmValue);
        if ( ( qpDplStricmp( parmValue,"EarlyIMS") == 0 ))
        {
          sSipConfig.uIMSConfigPayload.sSipConfigItem.eAuthScheme = QPE_CONFIG_AUTH_SCHEME_NONE;
        }
        else if (qpDplStrstr(parmValue, "AKA"))
        {
          sSipConfig.uIMSConfigPayload.sSipConfigItem.eAuthScheme = QPE_CONFIG_AUTH_SCHEME_AKA;
        }
        else
        {
          sSipConfig.uIMSConfigPayload.sSipConfigItem.eAuthScheme = QPE_CONFIG_AUTH_SCHEME_DIGEST;
        }
        qpDplStrlcpy(m_pAutoConfigStruct->reg_struct.authType,parmValue,QP_AUTO_CONFIG_AUTH_TYPE_LEN);
      }
      else if ( qpDplStricmp( parmName,"Realm") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, Realm is %s ",parmValue);
        qpDplStrlcpy(m_pAutoConfigStruct->reg_struct.authRealm,parmValue,QP_AUTO_CONFIG_AUTH_REALM_LEN);
      }
      else if ( qpDplStricmp( parmName,"UserName") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, UserName is %s ",parmValue);
        qpDplStrlcpy(sParmConfig.uIMSConfigPayload.sUserConfigItem.regConfigUserName,parmValue,QP_REG_CONFIG_USER_NAME_LEN);
      }
      else if ( qpDplStricmp( parmName,"UserPwd") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth, UserPwd is %s ",parmValue);
        qpDplStrlcpy(sParmConfig.uIMSConfigPayload.sUserConfigItem.regConfigPassword,parmValue,QP_REG_CONFIG_PASSWORD_LEN);
      }
    }//ParamList For Loop  
  }
  if ( qpdpl_set_config_group_value(&sParmConfig) == QP_IO_ERROR_OK && qpdpl_set_config_group_value(&sSipConfig) == QP_IO_ERROR_OK)
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parseAppAuth | Config Updated Successfully ");
}


/************************************************************************
Function VolteAutoConfig::parsePublicUserIDList()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::parsePublicUserIDList(QpSingleElementList* pParmList)
{
  QPCHAR parmValue[QP_AUTO_CONFIG_PUBLIC_USER_ID_LEN];
  QPCHAR parmName[PARM_MAX_LEN];
  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePublicUserIDList, m_pAutoConfigStruct is NULL ");
    return;
  }
  if ( pParmList )
  {
    for ( QPUINT index = 0; index < pParmList->Size(); index ++ )
    {
      if ( index >= QP_AUTO_CONFIG_PUBLIC_USER_ID_MAX )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePublicUserIDList, reached the max list size count");
        break;
      }
      qp_parm* pParm = (qp_parm*)pParmList->GetKeyAtIndex(index);
      if ( ! pParm || !pParm->getParmName() || !pParm->getParmValue() )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"parsePublicUserIDList, parm|ParmName|ParmValue is NULL ");
        break;
      }
      qpDplStrlcpy(parmName,pParm->getParmName(),PARM_MAX_LEN);
      qpDplStrlcpy(parmValue,pParm->getParmValue(),QP_AUTO_CONFIG_PUBLIC_USER_ID_LEN);

      if ( qpDplStricmp( parmName,"Public_User_Identity") == 0 )
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"parsePublicUserIDList, Public_User_Identity is %s ",parmValue);
        qpDplStrlcpy(m_pAutoConfigStruct->reg_struct.publicUserId,parmValue,QP_AUTO_CONFIG_PUBLIC_USER_ID_LEN);

      }      
    }//ParamList For Loop  
  }
}

QPVOID VolteAutoConfig::InformAPCSStatusToApp()
{
  QPINT32 timerValue = 0;
  const QP_AUTO_CONFIG_STRUCT* pConfigStruct = QP_NULL;

  IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::InformAPCSStatusToApp | Reached Max Retry count, checking if there is existing config ");
  if ( m_pAutoConfigMgr && (pConfigStruct = m_pAutoConfigMgr->GetAutoConfigStruct()) && pConfigStruct->validityTime > 0 )
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," startRetryVolteAutoConfig::InformAPCSStatusToApp | Reached Max Retry count, using existing config ");
    //Notify everyone of the configuration success
    ImsRegmanager_GenericEventNotify( eAutoConfigSuccess );
    m_pAutoConfigMgr->setConfigSuccessStatus( QP_TRUE );
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," startRetryVolteAutoConfig::InformAPCSStatusToApp | No further retry after this ");
    return;
  }
  else if(m_pAutoConfigMgr)
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::InformAPCSStatusToApp | Reached Max Retry count, no existing config ");
    m_pAutoConfigMgr->setConfigSuccessStatus( QP_FALSE );
    ImsRegmanager_GenericEventNotify( eAutoConfigFailure );
    timerValue = QP_VOLTE_CHECK_APCS_CONN_RETRY_TIMER_VALUE1;
    IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::InformAPCSStatusToApp | m_iLongRetryCount %d timervalue %d ", m_iLongRetryCount, timerValue);
    startTimer(timerValue*1000,0,QP_VOLTE_RETRY_TIMER);
  }
}

/************************************************************************
Function VolteAutoConfig::startRetryMechanism()

Description
Parse the XML and store the configuration data if required.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::startRetryMechanism( QPVOID )
{
  QPINT32 timerValue = 0;

  /* This if will be true only when a HTTP message has been sent on the N/W
  ** When this is called from handleHttpResponse m_iHttpRespWaitCount will be 0
  ** m_bAllHttpRespWaitCountFailureFlag will be false till be have reached max retransmission
  ** and then it will continue to be true till any response is received 
  */
  if ( m_iHttpRespWaitCount > 0 &&  QP_FALSE == m_bAllHttpRespWaitCountFailureFlag )
  {
    if(m_iHttpRespWaitCount < QP_CONFIG_MAX_RETRY_COUNT)
    {
      timerValue = pow(2.0,m_iHttpRespWaitCount);
    }
    else if ( m_iHttpRespWaitCount == QP_CONFIG_MAX_RETRY_COUNT )
    {
      timerValue = QP_VOLTE_MAX_RESPONSE_WAIT_TIMER;
      m_bAllHttpRespWaitCountFailureFlag = QP_TRUE;
    }
    IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::startRetryMechanism | m_iHttpRespWaitCount %d timervalue %d ", m_iHttpRespWaitCount, timerValue);
  }
  /** It will come here when this function is called from handleHttpResponse
  **  m_bAllHttpReTransmissionFailureFlag will be false till be have reached max retransmission
  **  and then it will continue to be true till we receive 200 OK
  */
  else if(m_iHttpRespErrCount > 0 && QP_FALSE == m_bAllHttpReTransmissionFailureFlag )
  {
   if(m_iHttpRespErrCount < QP_CONFIG_MAX_RETRY_COUNT)
    {
      timerValue = pow(2.0,m_iHttpRespErrCount);
    }
    else if ( m_iHttpRespErrCount == QP_CONFIG_MAX_RETRY_COUNT)
    {
      InformAPCSStatusToApp();
      m_bAllHttpReTransmissionFailureFlag = QP_TRUE;
      return;
    }
    IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::startRetryMechanism | m_iHttpRespErrCount %d timervalue %d ", m_iHttpRespErrCount, timerValue);
  }
  /* It will come here when either of m_bAllHttpRespWaitCountFailureFlag or 
  ** m_bAllHttpReTransmissionFailureFlag has become true and now long 
  ** recovery timer will continue 
  */
  else if ( QP_TRUE == m_bAllHttpRespWaitCountFailureFlag || QP_TRUE == m_bAllHttpReTransmissionFailureFlag)
  {
    m_iLongRetryCount++;
    if(m_iLongRetryCount == 1)
    {
      timerValue = QP_VOLTE_CHECK_APCS_CONN_RETRY_TIMER_VALUE2 ;
    }
    else if(m_iLongRetryCount > 1)
    {
      timerValue = QP_VOLTE_CHECK_APCS_CONN_RETRY_TIMER_VALUE3 ;
    }
    IMS_MSG_LOW_2(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::startRetryMechanism | m_iLongRetryCount %d timervalue %d ", m_iLongRetryCount, timerValue);
  }
  startTimer(timerValue*1000,0,QP_VOLTE_RETRY_TIMER);
}

/************************************************************************
Function VolteAutoConfig::constructURI()

Description
Construct the URI for configuration request message.

Dependencies
None

Return Value
True -  If request constructed and sent successfully.

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::constructURI( QPVOID )
{
  QPIMS_NV_CONFIG_DATA    sConfig ;

  if(QP_NULL != m_pUri)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pUri);
    m_pUri = QP_NULL;
  } 
  m_pUri = (QPCHAR*)qpDplMalloc(MEM_IMS_HTTPS, QP_VOLTE_PUBLIC_ID_LEN);

  if(QP_NULL == m_pUri )
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::constructURI() | Failure in m_pUri  malloc");
    return ;
  }
  qpDplMemset(m_pUri, 0, QP_VOLTE_PUBLIC_ID_LEN);

  if(QP_NULL != m_pImsi)
  {
    qpDplFree(MEM_IMS_HTTPS,m_pImsi);
    m_pImsi = QP_NULL;
  }
  m_pImsi = (QPDPL_IMSI_STRUCT*)qpDplMalloc(MEM_IMS_HTTPS, sizeof(QPDPL_IMSI_STRUCT));

  if(QP_NULL == m_pImsi)
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::constructURI | Failure in m_pImsi malloc");
    qpDplFree(MEM_IMS_HTTPS,m_pUri);
    m_pUri = QP_NULL;
    return;
  }
  qpDplMemset(m_pImsi, 0, sizeof(QPDPL_IMSI_STRUCT));

  qpDplGetIMSI(m_pImsi);

#ifdef FEATURE_IMS_PC_TESTBENCH
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy(m_pImsi->pIMSIValue, "123456789",sizeof(m_pImsi->pIMSIValue)));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy(m_pImsi->pMCC, "001",sizeof(m_pImsi->pMCC)));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy(m_pImsi->pMNC, "010",sizeof(m_pImsi->pMNC)));
#endif // FEATURE_IMS_PC_TESTBENCH

  if (m_pImsi && qpDplStrlen(m_pImsi->pIMSIValue))
  {
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::constructURI() - imsi = %s",m_pImsi->pIMSIValue);
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::constructURI() - mcc = %s", m_pImsi->pMCC);
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::constructURI() - mnc = %s", m_pImsi->pMNC);
  }
  else
  {
    IMS_MSG_LOW_0_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::constructURI() - IMSI Value is not valid ");
    return ;
  }
  qpDplMemset((QPVOID*) &sConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));

  sConfig.env_Hdr = eQP_IMS_RCS_AUTO_CONFIG;

  if( qpdpl_get_config_group_value(&sConfig) == QP_IO_ERROR_OK && qpDplStrlen(sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSConfigServerAddress)!= 0)
  {
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy(m_pUri,sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSConfigServerAddress,QP_VOLTE_PUBLIC_ID_LEN));
  }
  else
  {
    (QPVOID)qpDplStrlcpy(m_pUri,QP_VOLTE_SKT_APCS_URL,qpDplStrlen(QP_VOLTE_SKT_APCS_URL));
  }

  if( qpDplStrlen(sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSConfigServerPort)!= 0)
  {
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_VOLTE_COLON_STR,QP_VOLTE_PUBLIC_ID_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSConfigServerPort,QP_VOLTE_PUBLIC_ID_LEN));
  }
  else
  {
    //Post is not configured, use the default HTTPS port, "443"
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_VOLTE_COLON_STR,QP_VOLTE_PUBLIC_ID_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUri,QP_HTTPS_DEFAULT_PORT,QP_VOLTE_PUBLIC_ID_LEN));
  }

  IMS_MSG_MED_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::constructURI()| HTTP URI :\n%s\n", m_pUri);
}

/************************************************************************
Function VolteAutoConfig::constructURL()

Description
Construct the URL for configuration request message.

Dependencies
None

Return Value
True -  If request constructed and sent successfully.

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::constructURL()
{
  if ( !m_pUrl )
    m_pUrl = (QPCHAR*)qpDplMalloc(MEM_IMS_HTTPS, QP_VOLTE_URL_LEN);

  if(QP_NULL == m_pUrl)
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::constructURL() | Failure in m_pUrl malloc");
    return;
  }
  qpDplMemset(m_pUrl, 0, QP_VOLTE_URL_LEN);

  //Start constructing the URL
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlncpy(m_pUrl,QP_VOLTE_FWDSLASH_STR,QP_VOLTE_URL_LEN,qpDplStrlen(QP_VOLTE_FWDSLASH_STR)));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_VOLTE_URL_PRE,QP_VOLTE_URL_LEN));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_VOLTE_QUES_STR,QP_VOLTE_URL_LEN));
  //Version
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_VOLTE_URL_VERS,QP_VOLTE_URL_LEN));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,QP_VOLTE_EQUAL_STR,QP_VOLTE_URL_LEN));
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_pUrl,m_currentXmlVersion,QP_VOLTE_URL_LEN));

  IMS_MSG_MED_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::constructURL()| HTTP URL :\n%s\n", m_pUrl);
}

/************************************************************************
Function VolteAutoConfig::sendConfigRequest()

Description
Construct the configuration request message and send it.

Dependencies
None

Return Value
True -  If request constructed and sent successfully.

Side Effects
None
************************************************************************/
QPBOOL VolteAutoConfig::sendConfigRequest( QPVOID )
{
  IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest | Enter");
  //Check PDP Status and card status before triggering request
  if ( !m_isPDPActive || !m_isCardReady)
  {
    IMS_MSG_FATAL_2(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest, not sending request | m_isPDPActive %d m_isCardReady %d ", m_isPDPActive, m_isCardReady);
    return QP_FALSE;
  }

  QPIMS_NV_CONFIG_DATA    sConfig ;
  qpDplMemset((QPVOID*) &sConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
  sConfig.env_Hdr = eQP_IMS_RCS_AUTO_CONFIG;
  if(qpdpl_get_config_group_value(&sConfig) != QP_IO_ERROR_OK)
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest NV Read Failed");
    return QP_FALSE;
  }
  if(QP_NULL == m_pUserAgent)
  {
    m_pUserAgent = (QPCHAR*)qpDplMalloc(MEM_IMS_HTTPS, QP_IMS_RCS_GET_REQ_PARAM_LEN);
  }
  if(QP_NULL == m_pUserAgent)
  {
    IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest | Failure in m_pUserAgent malloc");
    return QP_FALSE;
  }
  qpDplMemset(m_pUserAgent, 0, QP_IMS_RCS_GET_REQ_PARAM_LEN);
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy(m_pUserAgent,sConfig.uIMSConfigPayload.sRcsAutoConfigItem.RCSTerminalModel,QP_IMS_RCS_GET_REQ_PARAM_LEN));

  if(QP_NULL == m_pHttpContent)
  {
    const QP_AUTO_CONFIG_STRUCT* pConfigStruct = QP_NULL;
    qpDplGetMSISDN((QPCHAR*)&m_strMSISDN,QP_VOLTE_MSISDN_MAX_LEN);

#ifdef FEATURE_IMS_PC_TESTBENCH
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy((QPCHAR*)&m_strMSISDN, "001010123456789",sizeof(m_strMSISDN)));
#endif // FEATURE_IMS_PC_TESTBENCH

    if (qpDplStrlen(m_strMSISDN))
    {
      IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest - Msisdn = %s",m_strMSISDN );
    }
    else
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest - Msisdn Value is not valid ");
      return QP_FALSE;
    }
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy((QPCHAR*)&m_sktUserName,"82",QP_VOLTE_MSISDN_MAX_LEN));
    QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcat(m_sktUserName,&m_strMSISDN[1],QP_VOLTE_MSISDN_MAX_LEN));
    IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest - SKT Username = %s",m_sktUserName );

    constructURI(); 

    if ( !m_pAutoConfigStruct )
    {
      if(QP_NULL == (m_pAutoConfigStruct = (QP_AUTO_CONFIG_STRUCT*)qpDplMalloc(MEM_IMS_HTTPS, sizeof(QP_AUTO_CONFIG_STRUCT))))
      {
        IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest | malloc failed on m_pAutoConfigStruct");
        return QP_FALSE;
      }
      qpDplMemset(m_pAutoConfigStruct,0,sizeof(QP_AUTO_CONFIG_STRUCT)); 
    }

      if ( m_pAutoConfigMgr && (pConfigStruct = m_pAutoConfigMgr->GetAutoConfigStruct()) )
      {
        if( pConfigStruct && m_pImsi && qpDplStrcmp(pConfigStruct->iImsi.pIMSIValue, m_pImsi->pIMSIValue) == 0  )
        {
          if (  pConfigStruct && ((qpDplStrcmp(pConfigStruct->xmlVersion, "-1") == 0 ) || ( pConfigStruct->validityTime < 0 )) )
          {
            IMS_MSG_HIGH_2_STR(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::sendConfigRequest | Existing IMS config version is %s validity %d", pConfigStruct->xmlVersion,pConfigStruct->validityTime);
            return QP_FALSE;
          }
          else if ( pConfigStruct && pConfigStruct->validityTime > 0 )
          {
            IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::sendConfigRequest | valid config exists in efs ");
            qpDplMemscpy(m_pAutoConfigStruct,sizeof(QP_AUTO_CONFIG_STRUCT), pConfigStruct,sizeof(QP_AUTO_CONFIG_STRUCT)); 
            qpDplStrlcpy(m_currentXmlVersion,pConfigStruct->xmlVersion,QP_AUTO_CONFIG_VER_LEN);		
            IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest Retrieved version is %s ",m_currentXmlVersion );
          } 
        }
        else if(pConfigStruct && m_pImsi && qpDplStrcmp(pConfigStruct->iImsi.pIMSIValue, m_pImsi->pIMSIValue) != 0) // IMSI value stored in bin is different from present card
        {
          qpDplMemscpy(&m_pAutoConfigStruct->iImsi,sizeof(QPDPL_IMSI_STRUCT), m_pImsi,sizeof(QPDPL_IMSI_STRUCT));
          IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::sendConfigRequest | Change in ISIM detected ");
        }
        else
        {
          IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest | pConfigStruct || m_pImsi NULL, shouldn't happen");
        }
      }	

    constructURL();

    m_pHttpContent = (QP_HTTP_MESSAGE_CONTENT*)qpDplMalloc(MEM_IMS_HTTPS, sizeof(QP_HTTP_MESSAGE_CONTENT));
    if(QP_NULL == m_pHttpContent)
    {
      IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest | Failure in pHttpContent malloc");
      return QP_FALSE;
    }
    qpDplMemset(m_pHttpContent, 0, sizeof(QP_HTTP_MESSAGE_CONTENT));

    if ( m_pHttpContent && m_pUri && m_pUrl )
    {
      QPCHAR cICCIDVal[IMS_ICCID_LEN];
      
      qpDplMemset(cICCIDVal,0,sizeof(cICCIDVal)); 
      
      qpDplGetICCID(cICCIDVal,sizeof(cICCIDVal));
 
#ifdef FEATURE_IMS_PC_TESTBENCH
      QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy((QPCHAR*)&cICCIDVal, "00101012345678965432",sizeof(cICCIDVal)));
#endif // FEATURE_IMS_PC_TESTBENCH

      if(0 == qpDplStrlen(cICCIDVal))
      {
        IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::sendConfigRequest ICCID not found ");
      }
      else      
      {
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::sendConfigRequest ICCID : %s ", cICCIDVal);
      }
      if(QP_NULL != m_pAKAICCIDVal)
	  {
        qpDplMemset(m_pAKAICCIDVal,0,sizeof(QPCHAR) * IMS_AKA_ICCID_LEN);      
        qpDplStrlcpy(m_pAKAICCIDVal,IMS_AKA_AUTH_TYPE,sizeof(QPCHAR) * IMS_AKA_ICCID_LEN);
        qpDplStrlcat(m_pAKAICCIDVal,cICCIDVal,sizeof(QPCHAR) * IMS_AKA_ICCID_LEN); 
        IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::sendConfigRequest AKA_ICCID : %s ", m_pAKAICCIDVal);
      }
	  else
	  {
	    IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG," VolteAutoConfig::sendConfigRequest aka iccid err");
	  }
      (void)qpDplStrlcpy(m_pHttpContent->n_Uri, m_pUri, QP_MAX_SERVER_URI_SIZE);
      (void)qpDplStrlcpy(m_pHttpContent->n_MethodName, METHOD[QP_GET], MAX_METHOD_STR_LEN);
      (void)qpDplStrlcpy(m_pHttpContent->n_Url, m_pUrl, QP_MAX_SERVER_URI_SIZE);
      m_pHttpContent->n_AdditionalHdrsCount = 6;

      m_pHttpContent->n_AdditionalHeaders[0].hName  = (QPCHAR*)(HttpHeadersStrings[QP_VOLTE_USER_ID]); 
      m_pHttpContent->n_AdditionalHeaders[0].hValue  = (QPCHAR*)(m_strMSISDN);
      m_pHttpContent->n_AdditionalHeaders[1].hName  = (QPCHAR*)(HttpHeadersStrings[QP_SERVICE_ID]);
      m_pHttpContent->n_AdditionalHeaders[1].hValue  = (QPCHAR*)QP_SERVICE_ID_HDR_VAL;
      m_pHttpContent->n_AdditionalHeaders[2].hName  = (QPCHAR*)(HttpHeadersStrings[QP_AUTH_TYPE]);

      m_pHttpContent->n_AdditionalHeaders[2].hValue  = (QPCHAR*)m_pAKAICCIDVal;
      m_pHttpContent->n_AdditionalHeaders[3].hName  = (QPCHAR*)(HttpHeadersStrings[QP_CONNECTION]);
      m_pHttpContent->n_AdditionalHeaders[3].hValue  = (QPCHAR*)QP_CONNECTION_HDR_VAL;
      m_pHttpContent->n_AdditionalHeaders[4].hName  = (QPCHAR*)(HttpHeadersStrings[QP_CONTENT_LENGTH]);
      m_pHttpContent->n_AdditionalHeaders[4].hValue  = (QPCHAR*)QP_CONTENT_HDR_VAL;
      m_pHttpContent->n_AdditionalHeaders[5].hName  = (QPCHAR*)(HttpHeadersStrings[QP_USER_AGENT]);
      m_pHttpContent->n_AdditionalHeaders[5].hValue  = m_pUserAgent;
      if ( m_pAuthResponse )
      {
        m_pHttpContent->n_AdditionalHdrsCount += 1;
        m_pHttpContent->n_AdditionalHeaders[6].hName  = (QPCHAR*)(HttpHeadersStrings[QP_AUTHORIZATION]);
        m_pHttpContent->n_AdditionalHeaders[6].hValue  = (QPCHAR*)(m_pAuthResponse );
      }
    }
  }

  if ( m_pHttpContent && m_pUri && m_pUrl )
  {
    QPUINT8 iLogHttpContent = 0;
	QPIMS_NV_CONFIG_DATA_PTR sRMConfig ;
    qpDplMemset((QPVOID*) &sRMConfig,0,sizeof(QPIMS_NV_CONFIG_DATA_PTR));
    sRMConfig.env_Hdr = eQP_IMS_REG_CONFIG;
    if(QP_IO_ERROR_OK != qpdpl_get_config_group_value_ptr(&sRMConfig))
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest - iRegResponseforOptions not defined, setting to 0");
	  iLogHttpContent = 0;
    }
    else
    {
      iLogHttpContent = sRMConfig.puIMSConfigPayload->sRMConfigItem.iRegResponseforOptions;
      IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"sendConfigRequest - iRegResponseforOptions = %d", iLogHttpContent);
    }
	qpdpl_release_config_group_value_ptr(&sRMConfig);
	if (iLogHttpContent == 1)
	{
      LogHttpContent();
	}
    if( sConfig.uIMSConfigPayload.sRcsAutoConfigItem.iDisableAutoConfig  == 2 )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest | RCS Auto-Configuration Disabled Flag is set to 2, triggering http request");
      m_pIMSServiceConfig->SendMessage(m_pHttpContent);
    }
    else
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest | triggering https request");
      m_pIMSServiceConfig->SendMessage(m_pHttpContent, QP_HTTPS_CONNECTION);
    }
  }
  else
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::sendConfigRequest | Failed to Send the Request");
    return QP_FALSE;
  }

  m_iHttpRespWaitCount++;
  startRetryMechanism();
  return QP_TRUE;
}

/*----------------------------------------------------------------------------
 Function  : VolteAutoConfig::ServiceAvailable
 Returns   : QPVOID
 Comments  : Virtual function from regmonitor class to listen to serviceAvailable
             events
----------------------------------------------------------------------------*/
QPVOID VolteAutoConfig::ServiceAvailable(QPE_SERVICE_STATUS eServiceStatus, QPCHAR* n_pFeatureTags, QPUINT16 iFeatureTagsLen)
{
  IMS_MSG_HIGH_1(LOG_IMS_SETTINGS,"VolteAutoConfig::ServiceAvailable [%d] ", eServiceStatus);
  m_isInitial403RegFailure = QP_TRUE;
}
/************************************************************************
Function VolteAutoConfig::RegistrationStatus()

Description
Pure Virtual function from regmonitor class 

Dependencies
None

Return Value
QPVOID

Side Effects
None
***********************************************************************/
QPVOID VolteAutoConfig::RegistrationStatus(eQpRegistrationStatus iRegistrationStatus)
{
  IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationStatus| Registration status is %d", iRegistrationStatus);
  switch(iRegistrationStatus)
  {
  case eQpStatusPDPActivated:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationStatus| IMS PDP is UP");
      if ( ! m_isRegPDPActive )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationStatus| IMS PDP re-connection, sending config request");
        sendConfigRequest();
        m_isRegPDPActive = QP_TRUE;
        m_isInitial403RegFailure = QP_TRUE;
      }
    }
    break;
  default:
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationStatus| Un-handled scenario");
  }
}

/************************************************************************
Function VolteAutoConfig::SipQosStatus()

Description
Pure Virtual function from regmonitor class 

Dependencies
None

Return Value
None.

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::SipQosStatus(QPE_SIPQOS_STATUS iSipQosStatus)
{
  QP_UNUSED(iSipQosStatus);
}

/************************************************************************
Function VolteAutoConfig::RegistrationCardStatus()

Description
Virtual function from regmonitor class 

Dependencies
None

Return Value
None.

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::RegistrationCardStatus(QPBOOL n_iCardStatus)
{
  IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationCardStatus Called Card Status %d", n_iCardStatus);
  QPBOOL iFileRead = QP_FALSE;

  m_isCardReady = n_iCardStatus;

  IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationCardStatus | PDP Status %d", m_isPDPActive);
  //To Remove
  if ( m_isCardReady && m_pIMSServiceConfig)
    m_pIMSServiceConfig->TriggerEstablishPDP();

  if(m_isCardReady && m_pAutoConfigMgr)
  {
    iFileRead = m_pAutoConfigMgr->InitConfigFileInfo();
    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationCardStatus | iFileRead %d", iFileRead);
    sendConfigRequest();
  }
}

/************************************************************************
Function VolteAutoConfig::RegistrationFailure()

Description
Virtual function from regmonitor class 

Dependencies
None

Return Value
None.

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::RegistrationFailure(QPE_REG_FAILURE_REASON iRegistrationFailure, QPUINT16 iStatusCode)
{
  IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationFailure Reason %d", iRegistrationFailure);
  QP_UNUSED(iStatusCode);
  switch(iRegistrationFailure)
  {
  case eQpRegManagerFailureNetwork:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationFailure| IMS PDP is down");
      m_isRegPDPActive = QP_FALSE;
    }
    break;
  case eQpRegManagerFailureIpChange:
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationFailure| IP Address has changed.");
      if( m_isRegPDPActive )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationFailure| IMS PDN Active,IP Address has changed, sending config request");
        sendConfigRequest();
      }
    }
    break;
  default:
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::RegistrationFailure| Un-handled scenario");
  }

}

/*----------------------------------------------------------------------------
 Function  : VolteAutoConfig::ServiceUnAvailable
 Returns   : QPVOID
 Comments  : Virtual function from regmonitor class to listen to 
             serviceUnAvailable event
----------------------------------------------------------------------------*/
	
QPVOID VolteAutoConfig::ServiceUnAvailable(QPE_REG_FAILURE_REASON iFailureReason, QPUINT16 iStatusCode, QPCHAR* ReasonStr, QPUINT16 ReasonStrLen)
{
  IMS_MSG_HIGH_1(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::ServiceUnAvailable Status Code [%d] ", iStatusCode);
  //Registration failed,triggering Configuration request.
  if(iStatusCode >= 400 && iStatusCode < 500 && iStatusCode != 408)
  {
    if ( iStatusCode == 403 && !m_isInitial403RegFailure)
    {
      IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::ServiceUnAvailable subsequent 403 failure, *NOT* triggering config request ");
      return;
    }
    IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::ServiceUnAvailable Registration Failure, triggering config request ");
    sendConfigRequest();
    if ( iStatusCode == 403 && m_isInitial403RegFailure)
    {
      IMS_MSG_HIGH_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::ServiceUnAvailable Initial 403 failure, resetting the flag ");
      m_isInitial403RegFailure = QP_FALSE;
    }
  }
}
/************************************************************************
Function VolteAutoConfig::handleCallCtrlCB()

Description
Call-Back function to handle the Phone Operation mode.

Dependencies
None

Return Value
None.

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::HandleCallCtrlCB( QPE_CALL_CTRL_CB_TYPE eCallCbType, QP_CALL_CTRL_CB_DATA* pCallCtrlCbData, QPVOID* pUserData ) 
{
  static QPBOOL m_isPhoneOnline = QP_TRUE;

  if(QP_NULL == pCallCtrlCbData || QP_NULL == pUserData)
  {
    IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG, "VolteAutoConfig::handleCallCtrlCB - QP_CALL_CTRL_CB_DATA,pUserData is NULL");
    return;
  }
  VolteAutoConfig* pVolteAutoConfig = (VolteAutoConfig*)pUserData;

  IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG, "VolteAutoConfig::handleCallCtrlCB -Call type %d", eCallCbType);

  switch (eCallCbType)
  {
  case eINFORM_OPRT_MODE_CB_TYPE:
    {
      QP_INFORM_OPRT_MODE_CB_DATA *pOptCbData =  (QP_INFORM_OPRT_MODE_CB_DATA*) pCallCtrlCbData;

      if ( eOPRT_MODE_PWROFF       == pOptCbData->eOprtMode  ||
           eOPRT_MODE_LPM          == pOptCbData->eOprtMode  ||
           eOPRT_MODE_OFFLINE      == pOptCbData->eOprtMode  ||
           eOPRT_MODE_OFFLINE_CDMA == pOptCbData->eOprtMode 
        )
      {
        IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG, "VolteAutoConfig::handleCallCtrlCB - Oprt Mode %d", pOptCbData->eOprtMode);
        m_isPhoneOnline = QP_FALSE;
        pVolteAutoConfig->m_isInitial403RegFailure = QP_TRUE;
      }
      else
      {
        if(eOPRT_MODE_ONLINE == pOptCbData->eOprtMode)
        {
          IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG, "VolteAutoConfig::handleCallCtrlCB - Mode Online");
          if ( ! m_isPhoneOnline )
          {          
            IMS_MSG_MED_0(LOG_IMS_RCS_AUTO_CONFIG, "VolteAutoConfig::handleCallCtrlCB - Triggering Config Request");
            pVolteAutoConfig->sendConfigRequest();
            m_isPhoneOnline = QP_TRUE;
          }
        }
      }
      qpDplCallCtrlAckEvent(pVolteAutoConfig->GetCallCtrlHandle(),eINFORM_OPRT_MODE_CB_TYPE,pOptCbData->iTransID);
    }
    break;
  default:
    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG, "VolteAutoConfig::handleCallCtrlCB default ignore the eCallCbType = %d notification", eCallCbType);
    break;
  }
}

/************************************************************************
Function VolteAutoConfig::RemoveQoutes

Description
Utility function to remove the preceeding and trailing quotes

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPCHAR* VolteAutoConfig::removeQoutes(const SipString *n_pStrParam)
{
  QPCHAR*  m_pParam = QP_NULL;
  if(n_pStrParam)
  {
    if(m_pParam)
    {
      qpDplFree(MEM_IMS_HTTPS, m_pParam);
      m_pParam = QP_NULL;
    }
    QPCHAR *pTemp = (QPCHAR *)n_pStrParam->c_str();
    //Remove Quote if present at beginning
    if(pTemp[0] == '\"')
      pTemp++;
    QPINT i = 0;
    while(pTemp[i] != '\0')
    {
      i++;
    }
    //Remove Quote if present at end
    if(pTemp[--i] == '\"')
      pTemp[i] = '\0';

    m_pParam = (QPCHAR *)qpDplMalloc(MEM_IMS_HTTPS, n_pStrParam->length() + 1);
    if(m_pParam == QP_NULL)
    {
      IMS_MSG_FATAL_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::removeQoutes | new failed");
      return QP_NULL;
    }  
    (QPVOID)qpDplStrlcpy(m_pParam, pTemp, (n_pStrParam->length() + 1));
    return m_pParam;
  }
  return QP_NULL;
}
/************************************************************************
Function VolteAutoConfig::LogHttpContent

Description
Logs the http content 

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::LogHttpContent()
{
  IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::LogHttpContent");
  if (m_pHttpContent)
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"URI:");
    LogMessageString(m_pHttpContent->n_Uri);
	IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"URL:");
    LogMessageString(m_pHttpContent->n_Url);
	for(QPUINT32 iHeader = 1;  iHeader < m_pHttpContent->n_AdditionalHdrsCount; iHeader++)
	{
	  IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"Header name: %s", m_pHttpContent->n_AdditionalHeaders[iHeader].hName);
      LogMessageString(m_pHttpContent->n_AdditionalHeaders[iHeader].hValue);
	}
  }
}
/************************************************************************
Function VolteAutoConfig::LogMessageString

Description
Logs the message

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID VolteAutoConfig::LogMessageString(QPCHAR* n_pMsg)
{
  if (n_pMsg && qpDplStrlen(n_pMsg) > 0)
  {
    QPUINT32 iMsgLength = 0;
	QPCHAR tempLogBuffer[QP_QXDM_LOG_BUFFER_SIZE+1];
	while (iMsgLength < qpDplStrlen(n_pMsg))
	{
	  qpDplMemset(tempLogBuffer,0,QP_QXDM_LOG_BUFFER_SIZE+1);
      qpDplMemscpy(tempLogBuffer,QP_QXDM_LOG_BUFFER_SIZE,n_pMsg+iMsgLength,QP_QXDM_LOG_BUFFER_SIZE);
      IMS_MSG_LOW_1_STR(LOG_IMS_RCS_AUTO_CONFIG,"%s",tempLogBuffer);
	  iMsgLength += QP_QXDM_LOG_BUFFER_SIZE;
	}
  }
  else
  {
	IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"VolteAutoConfig::LogMessageString - Msg length 0");
  }
}

/************************************************************************
Function volte_autoconfig_initialize()

Description
Initialize the Enabler. Get the singleton instance 

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPINT volte_autoconfig_initialize( QPVOID )
{
  QPINT8 iOprtMode = (QPINT8) QP_CONFIG_IMS_OPRT_UNKNOWN_MODE;
  /*Reading the operation mode*/ 
  QPE_IO_ERROR error = QP_IO_ERROR_UNKNOWN;
  /* Reading and storing the oprt mode */
  if(QP_IO_ERROR_OK != (error = qpDplIODeviceGetItem(IMS_OPRT_MODE, (QPVOID*) &iOprtMode, sizeof(iOprtMode))))
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_initialize -no oprt mode defined");
  }
  else
  {
    IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_initialize -oprt mode = %d", iOprtMode);
  }
  /*If operation mode is SKT then only APCS needs to be performed*/
  if(QP_CONFIG_IMS_OPRT_SKT_MODE == iOprtMode)
  {
    VolteAutoConfig* pVolteAutoConfig = QP_NULL;
    QPIMS_NV_CONFIG_DATA    sConfig ;

    qpDplMemset((QPVOID*) &sConfig,0,sizeof(QPIMS_NV_CONFIG_DATA));
    sConfig.env_Hdr = eQP_IMS_RCS_AUTO_CONFIG;

    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_initialize Called");

    if( qpdpl_get_config_group_value(&sConfig) == QP_IO_ERROR_OK && sConfig.uIMSConfigPayload.sRcsAutoConfigItem.iDisableAutoConfig == 1 )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_initialize | Volte Auto-Configuration Disabled");
      return AEE_IMS_EFAILED;
    }

    pVolteAutoConfig = VolteAutoConfig::getInstance();

    if ( pVolteAutoConfig )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_initialize | pVolteAutoConfig Instance is Valid");
      pVolteAutoConfig->Init();
      //To get the SIM Status
      if ( pVolteAutoConfig->SubscribeRegistrationStatus( ) == QP_FALSE )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_initialize | SubscribeRegistrationStatus FAILED");
      }
      else
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_initialize | SubscribeRegistrationStatus SUCCESS");
      }
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_initialize | pVolteAutoConfig Instance is not Valid");
      return AEE_IMS_EFAILED;
    }
  }
  return AEE_IMS_SUCCESS;
}


/************************************************************************
Function volte_autoconfig_uninitialize()

Description
Un-Initialize the Enabler. Delete the singleton instance.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID volte_autoconfig_uninitialize( QPVOID )
{
  IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_uninitialize Called");
  QPINT8 iOprtMode = (QPINT8) QP_CONFIG_IMS_OPRT_UNKNOWN_MODE;
  /*Reading the operation mode*/ 
  QPE_IO_ERROR error = QP_IO_ERROR_UNKNOWN;
  /* Reading and storing the oprt mode */
  if(QP_IO_ERROR_OK != (error = qpDplIODeviceGetItem(IMS_OPRT_MODE, (QPVOID*) &iOprtMode, sizeof(iOprtMode))))
  {
    IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_initialize -no oprt mode defined");
  }
  else
  {
    IMS_MSG_LOW_1(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_initialize -oprt mode = %d", iOprtMode);
  }
  /*If operation mode is SKT then only APCS needs to be performed*/
  if(QP_CONFIG_IMS_OPRT_SKT_MODE == iOprtMode)
  {
    VolteAutoConfig* pVolteAutoConfig = QP_NULL;
    pVolteAutoConfig = VolteAutoConfig::getInstance();

    if ( pVolteAutoConfig )
    {
      IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_uninitialize | pVolteAutoConfig Instance is Valid");
      if ( pVolteAutoConfig->UnsubscribeRegistrationStatus( ) == QP_FALSE )
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_uninitialize | UnSubscribeRegistrationStatus FAILED");
      }
      else
      {
        IMS_MSG_LOW_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_uninitialize | UnSubscribeRegistrationStatus SUCCESS");
      }
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_RCS_AUTO_CONFIG,"volte_autoconfig_uninitialize | pVolteAutoConfig Instance is not Valid");
    }
    AutoConfigMgr::DelInstance();
    VolteAutoConfig::delInstance();
  }
}
