//
// Copyright (c) 2004-2006 Qualphone, Inc. All rights reserved.
//
//
// XmlDecoderSchema.h
//   Defines Xml Decoder Schema APIs 
//

#ifndef __XmlDecoderSchema_h__
#define __XmlDecoderSchema_h__

/* User Defines Header Files */
#include "ims_variation.h"
#include <XmlCommon.h>
#include <qpdpl.h>

#define QP_XDM_SUPPORT

/**
 * Base class
 * Before intiating the parser, first set the parser to receive a response of type
 * based on the expected response. i.e. Use the appropriate response instance for
 * receiving the parsed data.
 */
class XmlDecoderSchema
{
public:
	 XmlDecoderSchema() {}
	 virtual ~XmlDecoderSchema() {}

	 /**
	  * Validation function to see if you obtained all the necessary data when parsing is
	  * complete
	  */
	 virtual QPINT Validate() = 0;
	 /**
	  * Attribute parsing for tag format <tag attr="value" attr2="value2">
	  */
	 virtual QPINT ProcessAttribute(QPCHAR *n_strElement, QPCHAR *n_strAttr, QPCHAR *n_strVal) = 0;
	 /**
	  * Parsing a tag value of the form <tag>value</tag>
	  */
	 virtual QPINT ProcessElement(QPCHAR *n_strElement, QPCHAR *n_strValue) = 0;
	#ifdef QP_XDM_SUPPORT
	 /**
	  * Change state of Parser when pushElement.
	  */
	 virtual QPINT ProcessState(QPCHAR *n_strElement) = 0;
	#endif //QP_XDM_SUPPORT

	 virtual QPVOID* GetSessionData() = 0;

   virtual QPVOID SetSchemaListener(QPVOID* pObj){QP_UNUSED(pObj)};
};

#endif //__XmlDecoderSchema_h__
