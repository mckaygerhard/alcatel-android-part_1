#ifndef __OIR_H__
#define __OIR_H__

#include<XMLOIR.h>
#include<XMLSimservsXcap.h>
#include<XMLCommonPolicy.h>
#include<XMLList.h>
#include<XMLStack.h>
#include <XmlDecoderSchema.h>
#include <XMLSchemaNameSpaceRetrieval.h>
#include <XMLCommonDef.h>

class XMLOIRDecoder : public XmlDecoderSchema
{

public:
	XMLOIRDecoder();
	~XMLOIRDecoder();

XmlCodeGenStack					m_codeGenStack; 
qp_oir*							m_poir;
XMLElement*						m_pIntermediateElement;
QPCHAR*							m_PassName;
XMLContext*						mContext;

QPINT ProcessElement(QPCHAR* n_strElement,QPCHAR* n_strVal);
QPINT ProcessAttribute(QPCHAR* n_strElement,QPCHAR* n_strAttr,QPCHAR* n_strVal);
QPINT Validate() {return 0;};
XMLElement* ProcessAnyElement(QPCHAR* n_strElement,QPCHAR* n_strVal){return QP_NULL;};
QPINT ProcessState(QPCHAR *n_strElement) {return 0;}; 
QPVOID* GetSessionData();
QPBOOL XMLRemoveNameSpaceTag(QPCHAR* n_strElement);   
}; 

#endif
  
  