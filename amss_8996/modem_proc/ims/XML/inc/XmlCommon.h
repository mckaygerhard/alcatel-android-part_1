//
// Copyright (c) 2004-2006 Qualphone, Inc. All rights reserved.
//
//
//
// XmlCommon.h
//   Defines Xml Common APIs 
//

#ifndef __XmlCommon_h__
#define __XmlCommon_h__

#include "qpdpl.h"

// char constants
#define QP_XML_NULL_CHAR     '\0'
#define QP_XML_BEGIN_TAG     '<'
#define QP_XML_END_TAG       '>'
#define QP_XML_DBLQUOTE_CHAR '\"'
#define QP_XML_SLASH_CHAR    '/'
#define QP_XML_BLANK_SPACE   ' '
#define QP_XML_TAB           '\t'
#define QP_XML_LINE_FEED     '\r'
#define QP_XML_NEW_LINE      '\n'
#define QP_XML_QUESTION_CHAR '?'
#define QP_XML_SNGLQUOTE_CHAR '\''

#define QP_XML_EQUALS        "="
#define QP_XML_QUOTE         "\""
#define QP_XML_SNGL_QUOTE	 "\'"	
#define QP_XML_QUOTES        "\"\'"
#define QP_XML_WHITESPACES   " \t\r\n"

typedef QPVOID (*freedata)(QPVOID *data);

class BuddyLinkList
{
public:
	 BuddyLinkList(QPVOID *n_pData) { m_pData = n_pData; m_pNext = QP_NULL; }
	 ~BuddyLinkList() {}

	 BuddyLinkList *Append( QPVOID *n_nxt );
	 BuddyLinkList *RemoveLast();
	 QPVOID FreeList(freedata fn);
	 BuddyLinkList *Next() { return m_pNext; }
	 QPVOID SetLast() { m_pNext = QP_NULL; }
	 QPVOID *Data() { return m_pData; }
	 BuddyLinkList *SeekEnd();

private:
	 QPVOID *m_pData;
	 BuddyLinkList *m_pNext;
};


class XmlCommon
{
public:
	 static QPCHAR *RemoveQuotes(QPCHAR *n_strValue);
	 static QPCHAR *LocateBeginElement(QPCHAR *n_strDecode);
	 static QPCHAR *LocateEndElement(QPCHAR *n_strDecode);
	 static QPCHAR *SkipSpaces(QPCHAR *n_strBegin);
	 static QPCHAR *TrimSpaces(QPCHAR *n_str);
  static QPBOOL IsEmailFormat(QPCHAR *n_str);
  static QPVOID TrimRightSingle(QPCHAR* n_pStr);
};

#endif //__XmlCommon_h__
