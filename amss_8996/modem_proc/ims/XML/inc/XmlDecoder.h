/*****************************************************************************
  Copyright (c) 2004-2006 Qualphone, Inc. All rights reserved.
  File  : XmlDecoder.h
  Desc  : Defines Xml Decoder APIs

  Notes :
  ===============================================================================
   Date    |   Author's Name    |  BugID  |        Change Description
  ===============================================================================
  22-06-06   Manjunath Gundurao    907           Increase in the Buffer Len for handling
                                                 TCP data
  -------------------------------------------------------------------------------
  25-09-06   Gautam Adlakha        1111          Replaced DecoderState from private to 
                                                 public variable - needed to be reset after
                                                 parsing each segment in the rich presence
                                                 notification.
-------------------------------------------------------------------------------
  29-09-06   Michele                -            Fixed memory leak caused by
                                                 multiple calls of InitXmlDecoder.
-------------------------------------------------------------------------------
  18-01-07   Michele                -            Added UnescapeXml to replace escape
                                                 sequences with correct characters.
-------------------------------------------------------------------------------
  09-08-07   Michele                -            Replaced tabs with spaces.
                                                 Moved to private some methods.
****************************************************************************/

#ifndef __XmlDecoder_h__
#define __XmlDecoder_h__

#define QP_XML_BUFFER_LEN  5000
#define QP_XML_INCOMPLETE  1
#define QP_XML_SUCCESS     0
#define QP_XML_ERROR       -1

/* User Defines Header Files */
#include <qpdpl.h>
#include <XmlCommon.h>

enum DecoderState
{
  QP_XML_PARSE_INIT,
  QP_XML_PARSE_CONTINUE,
  QP_XML_PARSE_ERROR,
  QP_XML_PARSE_COMPLETE
};

#define QP_SCHEMA_BUDDYLIST "buddylist"
#define QP_SCHEMA_BUDDYLISTS "buddylists"
#define QP_SCHEMA_BUDDIES "buddies"


#define SET_DELIM(m, c)         ((m)[(c) >> 3] |= (1 << ((c) & 7)))
#define IS_DELIM(m, c)          ((m)[(c) >> 3] & (1 << ((c) & 7)))
#define DELIM_TABLE_SIZE        32


class XmlDecoderSchema;
class BuddyLinkList;

/**
 * XmlDecoder Notes: This Decoder is specific to XML schema specified in LM document
 * PoC-List Management-1.1.3.doc
 * The parser is -
 * a. targeted to work with the http stack tightly
 * b. written with slow data links in mind < 19200 kbps link
 * c. written with low memory usage < 2k
 *
 * Decode takes max of QP_XML_BUFFER_LEN length of data at a time.
 * Refer section 7.5 for supported XML schema
 */

class XmlDecoder
{
public:
  QP_IMPORT_C XmlDecoder();
  ~XmlDecoder() { ResetXmlDecoder(); }

  QP_IMPORT_C QPINT Decode(QPCHAR *n_strData, QPINT n_count);

  QPVOID SetXmlDecoderSchema(XmlDecoderSchema *n_pXmlDecoderSchema) { m_pXmlDecoderSchema = n_pXmlDecoderSchema; }

  QP_IMPORT_C QPVOID ResetXmlDecoder();
  QP_IMPORT_C QPVOID InitXmlDecoder();
  
private: // Private methods
  QPINT              DecodeAttributes(QPCHAR *n_strAttrList);
  QPCHAR*            N_strtok(QPCHAR *n_pString,const QPCHAR *n_pDelims, QPCHAR **n_ppNewStr);

  BuddyLinkList*     PushElement(QPCHAR *n_strElement);
  BuddyLinkList*     PopElement();
  QPINT              CheckForElementStack() { return (m_pElementStack->Next() == QP_NULL ? 0 : 1); }

  BuddyLinkList*     PushValue(QPVOID *n_pValue);
  BuddyLinkList*     PopValue();

  QPVOID             UnescapeXml(QPCHAR* n_pString);
  QPVOID             UnescapeXmlString(QPCHAR* n_pString, QPCHAR* n_pSequence, QPCHAR n_iCharacter);
  QPVOID             UnescapeXmlString(QPCHAR* haystack, QPUINT haystack_length, QPCHAR* needle, QPCHAR replacement);

  QPINT				 ValidateAttributeList(QPCHAR* attrList);
  QPCHAR*			 TrimSpaces(QPCHAR* n_str);
  QPCHAR*            bufnstr(const QPCHAR* haystack, QPUINT haystack_length, const QPCHAR* needle, QPUINT needle_length);
  QPUINT             contractBuffer(QPCHAR* s, QPUINT slen);

private: // Private data
  DecoderState       m_DecodeState;
  BuddyLinkList*     m_pElementStack;
  QPCHAR*            m_strDecode;
  XmlDecoderSchema*  m_pXmlDecoderSchema;
  QPBOOL             m_bIsTagSelfEnding;
};

#endif // __XmlDecoder_h__
