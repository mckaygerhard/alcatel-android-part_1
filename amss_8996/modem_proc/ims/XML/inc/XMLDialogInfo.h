/*****************************************************************************
Copyright (c) 2015 Qualcomm Technologies, Inc. All rights reserved.
File  : XMLDialogInfo.h
Desc  :
Author: 
Date  : 08/06/2015

Revision History
===============================================================================
Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
08/06/2015                           ----      Defining the methods.
*****************************************************************************/
#ifndef __XMLDIALOGINFOPRIV_H__
#define __XMLDIALOGINFOPRIV_H__
#include <qpXMLDocGenerator.h>
#include <qplog.h>
#include "XMLCommonDef.h"
#include "XMLOmaPres.h"
#include "XMLCaps.h"

typedef enum
{
  XMLNS_DIALOG_DEFAULT,
  XMLNS_DIALOG_INFO,
  XMLNS_DIALOG_MAX
}XML_NAMESPACES_DIALOGINFO;


class DialogInfo :public XMLObject {
private: 
  QpSingleElementList*  mDialog;  //Element1
  xs_unsignedLong       mVersoin; //Attribute1
  xs_string             mStatus;  //Attribute2
  xs_anyURI             mEntity;  //Attribute3
  xs_string             mXmlns;   //Attribute4

  DialogInfo(const DialogInfo&);
  DialogInfo& operator=(const DialogInfo&){ return *this; }

public:
  DialogInfo();
  virtual ~DialogInfo();

  QPBOOL setDialog(QpSingleElementList* Dialog) {
    mDialog = Dialog;
    return QP_TRUE;
  }
  QpSingleElementList* getDialog() {
    return mDialog;
  }

  QPBOOL setVersionValue(xs_unsignedLong version) {
    mVersoin = version;
    return QP_TRUE;
  }
  xs_unsignedLong getVersionValue() {
    return mVersoin;
  }

  QPBOOL setStatusValue(xs_string status) {
    if (status != QP_NULL)
    {
      if(mStatus)
      {
         qpDplFree(MEM_IMS_XML, mStatus);
      }
      mStatus = qpDplStrdup(MEM_IMS_XML, status);
    }
    else
    {
      return QP_FALSE;
    }

    return QP_TRUE;
  }
  xs_string   getStatusValue() {
    return mStatus;
  }


  QPBOOL      setEntity(xs_anyURI entity) {
    if (entity != QP_NULL)
    {
      if(mEntity)
      {
            qpDplFree(MEM_IMS_XML, mEntity);
      }
      mEntity = qpDplStrdup(MEM_IMS_XML, entity);
    } 
    return QP_TRUE;
  }
  xs_anyURI   getEntity() {
    return mEntity;
  }


  QPBOOL      setXmlnsValue(xs_string xmlns) {
    if (xmlns != QP_NULL)
    {
      if(mXmlns)
        {
              qpDplFree(MEM_IMS_XML, mXmlns);
        }
      mXmlns = qpDplStrdup(MEM_IMS_XML, xmlns);
    } 
    return QP_TRUE;
  }
  xs_anyURI   getXmlnsValue() {
    return mXmlns;
  }

  QPBOOL IsRootElement(){
    return QP_TRUE;
  }

  QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

  QPCHAR* ClassName(){ return (QPCHAR*)"dialogInfo"; };
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
  QPCHAR* TagName(){ return (QPCHAR*)"dialogInfo"; }
  QPUINT8 Valiadate(){ return 0; }
};


class Duration :public XMLElement {
  private:

    xs_unsignedLong       mDuration;

  
    Duration(const Duration&);
    Duration& operator=(const Duration&){ return *this; }
  
  public:
    Duration();
    virtual ~Duration();
  
    QPBOOL      setDurationValue(xs_unsignedLong duration) {
      mDuration = duration;
      return QP_TRUE;
    }
    xs_unsignedLong   getDurationValue() {
      return mDuration;
    }

    QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
    QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);
  
    QPCHAR* ClassName(){ return (QPCHAR*)"duration"; }
    QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
    QPCHAR* TagName(){ return (QPCHAR*)"duration"; }
    QPUINT8 Valiadate(){ return 0; }
};

class Mediatype :public XMLElement {
  private:

    xs_string       mstrMediatype;
  
    Mediatype(const Mediatype&);
    Mediatype& operator=(const Mediatype&){ return *this; }
  
  public:
    Mediatype();
    virtual ~Mediatype();
  
    QPBOOL      setMediatype(xs_string  mediatype) {
      if (mediatype != QP_NULL)
      {
        if(mstrMediatype)
        {
           qpDplFree(MEM_IMS_XML, mstrMediatype);
        }
        mstrMediatype = qpDplStrdup(MEM_IMS_XML, mediatype);
      }
      return QP_TRUE;
    }
    xs_string   getMediatype() {
      return mstrMediatype;
    }
  
    QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
    QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);
  
    QPCHAR* ClassName(){ return (QPCHAR*)"mediatype"; }
    QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
    QPCHAR* TagName(){ return (QPCHAR*)"mediatype"; }
    QPUINT8 Valiadate(){ return 0; }
};


class Mediadirection :public XMLElement {
  private:

    xs_string       mstrMediadirection;
  
    Mediadirection(const Mediadirection&);
    Mediadirection& operator=(const Mediadirection&){ return *this; }
  
  public:
    Mediadirection();
    virtual ~Mediadirection();
  
    QPBOOL      setMediadirection(xs_string  mediadirection) {
      if (mediadirection != QP_NULL)
      {
        if(mstrMediadirection)
        {
           qpDplFree(MEM_IMS_XML, mstrMediadirection);
        }
        mstrMediadirection = qpDplStrdup(MEM_IMS_XML, mediadirection);
      }
      return QP_TRUE;
    }
    xs_string   getMediadirection() {
      return mstrMediadirection;
    }
  
    QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
    QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);
  
    QPCHAR* ClassName(){ return (QPCHAR*)"mediadirection"; };
    QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
    QPCHAR* TagName(){ return (QPCHAR*)"mediadirection"; }
    QPUINT8 Valiadate(){ return 0; }
};


class MediaAttributes :public XMLElement {
  private:

    Mediatype*      mMediaType;
    Mediadirection* mMediadirection;
    QPBOOL          mPort0;
    
  
    MediaAttributes(const MediaAttributes&);
    MediaAttributes& operator=(const MediaAttributes&){ return *this; }
  
  public:
    MediaAttributes();
    virtual ~MediaAttributes();
  
    QPBOOL      setMediatype(Mediatype* mediatype) {
      mMediaType = mediatype;
      return QP_TRUE;
    }
    Mediatype*  getMediatype() {
      return mMediaType;
    }

    QPBOOL      setMediadirection(Mediadirection* mediadirection) {
      mMediadirection = mediadirection;
      return QP_TRUE;
    }
    Mediadirection*   getMediadirection() {
      return mMediadirection;
    }

    QPBOOL      setPort0value(QPBOOL port0) {
      mPort0 = port0;
      return QP_TRUE;
    }
    QPBOOL  getPort0value() {
      return mPort0;
    }

    QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
    QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);
  
    QPCHAR* ClassName(){ return (QPCHAR*)"mediaAttributes"; }
    QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
    QPCHAR* TagName(){ return (QPCHAR*)"mediaAttributes"; }
    QPUINT8 Valiadate(){ return 0; }
};


class Saexclusive :public XMLElement {
  private:

    xs_string       mstrExclusive;
  
    Saexclusive(const Saexclusive&);
    Saexclusive& operator=(const Saexclusive&){ return *this; }
  
  public:
    Saexclusive();
    virtual ~Saexclusive();
  
    QPBOOL      setSaExclusiveValue(xs_string saexclusive) {
      if (saexclusive != QP_NULL)
      {
        if(mstrExclusive)
        {
           qpDplFree(MEM_IMS_XML, mstrExclusive);
        }
        mstrExclusive = qpDplStrdup(MEM_IMS_XML, saexclusive);
      }
      return QP_TRUE;
    }
    xs_string   getSaExclusiveValue() {
      return mstrExclusive;
    }
  
    QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
    QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);
  
    QPCHAR* ClassName(){ return (QPCHAR*)"saexclusive"; }
    QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
    QPCHAR* TagName(){ return (QPCHAR*)"sa:exclusive"; }
    QPUINT8 Valiadate(){ return 0; }
};


class RouteSet :public XMLElement {
  private:

    QpSingleElementList* mhoplist; //xs_string  mstrRouteset;
  
    RouteSet(const RouteSet&);
    RouteSet& operator=(const RouteSet&){ return *this; }
  
  public:
    RouteSet();
    virtual ~RouteSet();

    QPBOOL      setHoplist(QpSingleElementList* hoplist) {
      mhoplist = hoplist;
      return QP_TRUE;
    }
    QpSingleElementList* getHoplist() {
      return mhoplist;
    }
  
    QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
    QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);
  
    QPCHAR* ClassName(){ return (QPCHAR*)"routeset"; }
    QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
    QPCHAR* TagName(){ return (QPCHAR*)"route-set"; }
    QPUINT8 Valiadate(){ return 0; }
};


class Hop :public XMLElement {
  private:

    xs_string       mstrHop;
  
    Hop(const Hop&);
    Hop& operator=(const Hop&){ return *this; }
  
  public:
    Hop();
    virtual ~Hop();
  
    QPBOOL      setHopValue(xs_string hop) {
      if (hop != QP_NULL)
      {
        if(mstrHop)
        {
          qpDplFree(MEM_IMS_XML, mstrHop);
        }
        mstrHop = qpDplStrdup(MEM_IMS_XML, hop);
      }
      return QP_TRUE;
    }
    xs_string   gethopValue() {
      return mstrHop;
    }
  
    QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
    QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);
  
    QPCHAR* ClassName(){ return (QPCHAR*)"hop"; }
    QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
    QPCHAR* TagName(){ return (QPCHAR*)"hop"; }
    QPUINT8 Valiadate(){ return 0; }
};


class Replaces :public XMLElement {
  private:

    xs_string       mstrCallid;   //call-id
    xs_string       mstrLocalTag; //local-tag
    xs_string       mstrRemoteTag;//remote-tag
  
    Replaces(const Replaces&);
    Replaces& operator=(const Replaces&){ return *this; }
  
  public:
    Replaces();
    virtual ~Replaces();
  
    QPBOOL      setCallidValue(xs_string callid) {
      if (callid != QP_NULL)
      {
        if(mstrCallid)
        {
          qpDplFree(MEM_IMS_XML, mstrCallid);
        }
        mstrCallid = qpDplStrdup(MEM_IMS_XML, callid);
      }
      return QP_TRUE;
    }
    xs_string   getCallidValue() {
      return mstrCallid;
    }
  
    QPBOOL  setLocaltagValue(xs_string localtag) {
      if (localtag != QP_NULL)
      {
        if(mstrLocalTag)
        {
          qpDplFree(MEM_IMS_XML, mstrLocalTag);
        }
        mstrLocalTag = qpDplStrdup(MEM_IMS_XML, localtag);
      }
      return QP_TRUE;
    }
    xs_string   getLocaltagValue() {
      return mstrLocalTag;
    }

    QPBOOL  setRemotetagValue(xs_string remotetag) {
      if (remotetag != QP_NULL)
      {
        if(mstrRemoteTag)
        {
          qpDplFree(MEM_IMS_XML, mstrRemoteTag);
        }
        mstrRemoteTag = qpDplStrdup(MEM_IMS_XML, remotetag);
      }
      return QP_TRUE;
    }
    xs_string   getRemotetagValue() {
      return mstrRemoteTag;
    } 

  
    QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
    QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);
  
    QPCHAR* ClassName(){ return (QPCHAR*)"replaces"; }
    QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
    QPCHAR* TagName(){ return (QPCHAR*)"replaces"; }
    QPUINT8 Valiadate(){ return 0; }
};


/*
<xs:element name="state">
  <xs:complexType>
    <xs:simpleContent>
      <xs:extension base="xs:string">
        <xs:attribute name="event" use="optional">
          <xs:simpleType>
            <xs:restriction base="xs:string">
              <xs:enumeration value="cancelled"/>
              <xs:enumeration value="rejected"/>
              <xs:enumeration value="replaced"/>
              <xs:enumeration value="local-bye"/>
              <xs:enumeration value="remote-bye"/>
              <xs:enumeration value="error"/>
              <xs:enumeration value="timeout"/>
            </xs:restriction>
          </xs:simpleType>
        </xs:attribute>
        <xs:attribute name="code" use="optional">
          <xs:simpleType>
            <xs:restriction base="xs:positiveInteger">
              <xs:minInclusive value="100"/>
              <xs:maxInclusive value="699"/>
            </xs:restriction>
          </xs:simpleType>
        </xs:attribute>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
</xs:element>
*/

class State :public XMLElement {
private:
  xs_string       mState;
  xs_string       mEvent;    //Attribute1
  xs_unsignedInt  mCode;     //Attribute2  between 100 and 699

  State(const State&);
  State& operator=(const State&){ return *this; }

public:
  State();
  virtual ~State();

  QPBOOL      setStateValue(xs_string state) {
    if (state != QP_NULL)
    {
      if(mState)
      {
        qpDplFree(MEM_IMS_XML, mState);
      }
      mState = qpDplStrdup(MEM_IMS_XML, state);
    }
    return QP_TRUE;
  }
  xs_string   getStateValue() {
    return mState;
  }

  QPBOOL      setEventValue(xs_string Event) {
    if (Event != QP_NULL)
    {
      if(mEvent)
      {
        qpDplFree(MEM_IMS_XML, mEvent);
      }
      mEvent = qpDplStrdup(MEM_IMS_XML, Event);
    }
    return QP_TRUE;
  }
  xs_string   getEventValue() {
    return mEvent;
  }

  QPBOOL         setCodeValue(xs_unsignedInt Code) {
    mCode = Code;
    return QP_TRUE;
  }
  xs_unsignedInt getCodeValue() {
    return mCode;
  }

  QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

  QPCHAR* ClassName(){ return (QPCHAR*)"state"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"state"; }
  QPUINT8 Valiadate(){ return 0; }
};

/*
<xs:complexType name="nameaddr">
  <xs:simpleContent>
    <xs:extension base="xs:anyURI">
      <xs:attribute name="display-name" type="xs:string"
        use="optional"/>
    </xs:extension>
  </xs:simpleContent>
</xs:complexType>
*/

class Referredby :public XMLElement {
private:
  xs_anyURI       mAnyUri;          //xs:anyURI
  xs_string       mDisplayName;

  Referredby(const Referredby&);
  Referredby& operator=(const Referredby&){ return *this; }

public:
  Referredby();
  virtual ~Referredby();


  QPBOOL      setAnyUri(xs_anyURI anyUri) {
    if (anyUri != QP_NULL)
    {
      if(mAnyUri)
      {
        qpDplFree(MEM_IMS_XML, mAnyUri);
      }
      mAnyUri = qpDplStrdup(MEM_IMS_XML, anyUri);
    }
    return QP_TRUE;
  }
  xs_string   getAnyUri() {
    return mAnyUri;
  }


  QPBOOL      setDisplayName(xs_string displayName) {
    if (displayName != QP_NULL)
    {
      if(mDisplayName)
      {
        qpDplFree(MEM_IMS_XML, mDisplayName);
      }
      mDisplayName = qpDplStrdup(MEM_IMS_XML, displayName);
    }
    return QP_TRUE;
  }
  xs_string   getDisplayName() {
    return mDisplayName;
  }

  QPBOOL  ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL  ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

  QPCHAR* ClassName(){ return (QPCHAR*)"referredby"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"referred-by"; }
  QPUINT8 Valiadate(){ return 0; }
};


class Cseq :public XMLElement {
private:
  xs_unsignedLong       mCseq;

  Cseq(const Cseq&);
  Cseq& operator=(const Cseq&){ return *this; }

public:
  Cseq();
  virtual ~Cseq();

  QPBOOL      setCseqValue(xs_unsignedLong cseq) {
      mCseq = cseq;
      return QP_TRUE;
  }
  xs_unsignedLong   getCseqValue() {
    return mCseq;
  }

  QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

  QPCHAR* ClassName(){ return (QPCHAR*)"cseq"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"cseq"; }
  QPUINT8 Valiadate(){ return 0; }
};

class Target :public XMLElement {
private:
  xs_string       mUri;                //Attribute
  QpSingleElementList*  m_pParamList;  //list of Param element

  Target(const Target&);
  Target& operator=(const Target&){ return *this; }

public:
  Target();
  virtual ~Target();

  QPBOOL      setUri(xs_string uri) {
    if (uri != QP_NULL)
    {
      if(mUri)
      {
        qpDplFree(MEM_IMS_XML, mUri);
      }
      mUri = qpDplStrdup(MEM_IMS_XML, uri);
    }
    return QP_TRUE;
  }
  xs_string   getUri() {
    return mUri;
  }

    //QpSingleElementList* m_pParamList;  //param
  QPBOOL      setParamlist(QpSingleElementList* paramList) {
    m_pParamList = paramList;
    return QP_TRUE;
  }
  QpSingleElementList*  getParamlist() {
    return m_pParamList;
  }


  QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

  QPCHAR* ClassName(){ return (QPCHAR*)"target"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"target"; }
  QPUINT8 Valiadate(){ return 0; }
};




class Param :public XMLElement {
private:
  xs_string     mPname;      //attribute1
  xs_string     mPval;       //attribute2

  Param(const Param&);
  Param& operator=(const Param&){ return *this; }

public:
  Param();
  virtual ~Param();

  QPBOOL      setPnameValue(xs_string pname) {
    if (pname != QP_NULL)
    {
      if(mPname)
      {
        qpDplFree(MEM_IMS_XML, mPname);
      }
      mPname = qpDplStrdup(MEM_IMS_XML, pname);
    }
    return QP_TRUE;
  }
  xs_string   getPnameValue() {
    return mPname;
  }

  QPBOOL      setPvalValue(xs_string pval) {
    if (pval != QP_NULL)
    {
      if(mPval)
      {
        qpDplFree(MEM_IMS_XML, mPval);
      }
      mPval = qpDplStrdup(MEM_IMS_XML, pval);
    }
    return QP_TRUE;
  }
  xs_string   getPvalValue() {
    return mPval;
  }

  QPBOOL  ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL  ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

  QPCHAR* ClassName(){ return (QPCHAR*)"param"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"param"; }
  QPUINT8 Valiadate(){ return 0; }
};


class Sessd :public XMLElement {
private:
  xs_string       mType;     //Attribute only, xs:string
  xs_string       mSessionDescription;
  Sessd(const Sessd&);
  Sessd& operator=(const Sessd&){ return *this; }

public:
  Sessd();
  virtual ~Sessd();

  QPBOOL      setTypeValue(xs_string type) {
    if (type != QP_NULL)
    {
      if(mType)
      {
        qpDplFree(MEM_IMS_XML, mType);
      }
      mType = qpDplStrdup(MEM_IMS_XML, type);
    }
    return QP_TRUE;
  }
  xs_string   getTypeValue() {
    return mType;
  }
  QPBOOL    setSessDescriptionValue(xs_string sessdescription) {
    if (sessdescription != QP_NULL)
    {
      if(mSessionDescription)
      {
        qpDplFree(MEM_IMS_XML, mSessionDescription);
      }
      mSessionDescription = qpDplStrdup(MEM_IMS_XML, sessdescription);
    }
    return QP_TRUE;
  }
  xs_string   getSessDescriptionValue() {
    return mSessionDescription;
  }

  QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

  QPCHAR* ClassName(){ return (QPCHAR*)"sessd"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"session-description"; }
  QPUINT8 Valiadate(){ return 0; }
};

class Identity :public XMLElement {
private:
  xs_anyURI       mAnyUri;          //xs:anyURI
  xs_string       mDisplayName;     //Attribute only, display name

  Identity(const Identity&);
  Identity& operator=(const Identity&){ return *this; }

public:
  Identity();
  virtual ~Identity();


  QPBOOL      setAnyUri(xs_anyURI anyUri) {
    if (anyUri != QP_NULL)
    {
      if(mAnyUri)
      {
        qpDplFree(MEM_IMS_XML, mAnyUri);
      }
      mAnyUri = qpDplStrdup(MEM_IMS_XML, anyUri);
    }
    return QP_TRUE;
  }
  xs_string   getAnyUri() {
    return mAnyUri;
  }


  QPBOOL      setDisplayName(xs_string displayName) {
    if (displayName != QP_NULL)
    {
      if(mDisplayName)
      {
        qpDplFree(MEM_IMS_XML, mDisplayName);
      }
      mDisplayName = qpDplStrdup(MEM_IMS_XML, displayName);
    }
    return QP_TRUE;
  }
  xs_string   getDisplayName() {
    return mDisplayName;
  }

  QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

  QPCHAR* ClassName(){ return (QPCHAR*)"identity"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"identity"; }
  QPUINT8 Valiadate(){ return 0; }
};

class Local :public XMLElement {
private:
  Identity *mpIdentity;
  Target   *mpTarget;
  Sessd    *mpSessd;
  Cseq     *mpCseq;
  QpSingleElementList* mpMediaAttributelist; 
  QpSingleElementList*  mAny;

  Local(const Local&);
  Local& operator=(const Local&){ return *this; }

public:
  Local();
  virtual ~Local();

  QPBOOL      setIdentityValue(Identity* identity) {
    mpIdentity = identity;
    return QP_TRUE;
  }
  Identity*   getIdentityValue() {
    return mpIdentity;
  }

  QPBOOL      setTargetValue(Target* Target) {
    mpTarget = Target;
    return QP_TRUE;
  }
  Target*   getTargetValue() {
    return mpTarget;
  }

  QPBOOL      setSessdValue(Sessd* Sessd) {
    mpSessd = Sessd;
    return QP_TRUE;
  }
  Sessd*    getSessdValue() {
    return mpSessd;
  }

  QPBOOL      setCseqValue(Cseq* Cseq) {
    mpCseq = Cseq;
    return QP_TRUE;
  }
  Cseq*   getCseqValue() {
    return mpCseq;
  }

  QPBOOL      setMediaAttributeList(QpSingleElementList* mediaattribute) {
    mpMediaAttributelist = mediaattribute;
    return QP_TRUE;
  }
  QpSingleElementList*  getMediaAttributeList()
  {
    return mpMediaAttributelist;
  }

  QPBOOL      setAnyElementValue(QpSingleElementList* Any) {
    mAny = Any;
    return QP_TRUE;
  }
  QpSingleElementList*    getAnyElementValue() {
    return mAny;
  }

  QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

  QPCHAR* ClassName(){ return (QPCHAR*)"local"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"local"; }
  QPUINT8 Valiadate(){ return 0; }
};

class Remote :public XMLElement {
private:
  Identity *mpIdentity;
  Target   *mpTarget;
  Sessd    *mpSessd;
  Cseq     *mpCseq;
  QpSingleElementList* mpMediaAttributelist; 
  QpSingleElementList*  mAny;

  Remote(const Remote&);
  Remote& operator=(const Remote&){ return *this; }

public:
  Remote();
  virtual ~Remote();

  QPBOOL      setIdentityValue(Identity* identity) {
    mpIdentity = identity;
    return QP_TRUE;
  }
  Identity*   getIdentityValue() {
    return mpIdentity;
  }

 
  QPBOOL      setTargetValue(Target* Target) {
    mpTarget = Target;
    return QP_TRUE;
  }
  Target*   getTargetValue() {
    return mpTarget;
  }


  QPBOOL      setSessdValue(Sessd* Sessd) {
    mpSessd = Sessd;
    return QP_TRUE;
  }
  Sessd*    getSessdValue() {
    return mpSessd;
  }

  
  QPBOOL      setCseqValue(Cseq* Cseq) {
    mpCseq = Cseq;
    return QP_TRUE;
  }
  Cseq*   getCseqValue() {
    return mpCseq;
  }

  QPBOOL      setMediaAttributelist(QpSingleElementList* mediaattribute) {
    mpMediaAttributelist  = mediaattribute;
    return QP_TRUE;
  }
  QpSingleElementList*    getMediaAttributelist() {
    return mpMediaAttributelist;
  }

  QPBOOL      setAnyElementValue(QpSingleElementList* Any) {
    mAny = Any;
    return QP_TRUE;
  }
  QpSingleElementList*    getAnyElementValue() {
    return mAny;
  }

  QPBOOL          ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL          ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

  QPCHAR* ClassName(){ return (QPCHAR*)"remote"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"remote"; }
  QPUINT8 Valiadate(){ return 0; }
};

class Dialog :public XMLElement {
private:

  // 8 elements
  State* mpState;
  Saexclusive* mpSaexclusive;
  Duration *mpDuration;
  Replaces *mpReplaces;
  Referredby *mpReferredby;
  RouteSet* mpRouteset;
  Local* mpLocal;
  Remote* mpRemote;
  QpSingleElementList*  mAny;

  // 5 attributes
  xs_string       mID;        //Attribute1  id
  xs_string       mCallID;    //Attribute2  call-id
  xs_string       mLocaltag;  //Attribute3  local-tag
  xs_string       mRemotetag; //Attribute4  remote-tag
  xs_string       mDirection; //Attribute5  direction


  Dialog(const Dialog&);
  Dialog& operator=(const Dialog&){ return *this; }

public:
  Dialog();
  virtual ~Dialog();

  QPBOOL  setStateValue(State* state) {
        mpState = state;
    return QP_TRUE;
  }
  State* getStateValue() {
    return mpState;
  }

  QPBOOL      setDurationValue(Duration* duration) {
    mpDuration = duration;
    return QP_TRUE;
  }
  Duration* getDurationValue() {
    return mpDuration;
  }

  QPBOOL      setReplacesValue(Replaces* replaces) {
    mpReplaces = replaces;
    return QP_TRUE;
  }
  Replaces* getReplacesValue() {
    return mpReplaces;
  }

  QPBOOL      setReferredbyValue(Referredby* nameaddr) {
    mpReferredby = nameaddr;
    return QP_TRUE;
  }
  Referredby* getReferredbyValue() {
    return mpReferredby;
  }

  QPBOOL      setRoutesetValue(RouteSet* routeset) {
    mpRouteset = routeset;
    return QP_TRUE;
  }
  RouteSet*  getRoutesetValue() {
    return mpRouteset;
  }

  QPBOOL  setLocalValue(Local* local) {
        mpLocal = local;
    return QP_TRUE;
  }
  Local*  getLocalValue() {
    return mpLocal;
  }

  QPBOOL      setRemoteValue(Remote* remote) {
        mpRemote = remote;
    return QP_TRUE;
  }
  Remote* getRemoteValue() {
    return mpRemote;
  }

  QPBOOL      setSaexclusiveValue(Saexclusive* saexclusive) {
        mpSaexclusive = saexclusive;
    return QP_TRUE;
  }
  Saexclusive*  getSaexclusiveValue() {
    return mpSaexclusive;
  }

  QPBOOL      setAnyElementValue(QpSingleElementList* Any) {
    mAny = Any;
    return QP_TRUE;
  }
  QpSingleElementList*    getAnyElementValue() {
    return mAny;
  }

  QPBOOL      setIDValue(xs_string id) {
    if (id != QP_NULL)
    {
      if(mID)
      {
        qpDplFree(MEM_IMS_XML, mID);
      }
      mID = qpDplStrdup(MEM_IMS_XML, id);
    }
    return QP_TRUE;
  }
  xs_string   getIDValue() {
    return mID;
  }

  QPBOOL      setCallIDValue(xs_string callID) {
    if (callID != QP_NULL)
    {
      if(mCallID)
      {
        qpDplFree(MEM_IMS_XML, mCallID);
      }
      mCallID = qpDplStrdup(MEM_IMS_XML, callID);
    }
    return QP_TRUE;
  }
  xs_string   getCallIDValue() {
    return mCallID;
  }
  
  QPBOOL      setLocaltagValue(xs_string localtag) {
    if (localtag != QP_NULL)
    {
      if(mLocaltag)
      {
        qpDplFree(MEM_IMS_XML, mLocaltag);
      }
      mLocaltag = qpDplStrdup(MEM_IMS_XML, localtag);
    }
    return QP_TRUE;
  }
  xs_string   getLocaltagValue() {
    return mLocaltag;
  }

  QPBOOL      setRemotetagValue(xs_string remotetag) {
    if (remotetag != QP_NULL)
    {
      if(mRemotetag)
      {
        qpDplFree(MEM_IMS_XML, mRemotetag);
      }
      mRemotetag = qpDplStrdup(MEM_IMS_XML, remotetag);
    }
    return QP_TRUE;
  }
  xs_string   getRemotetagValue() {
    return mRemotetag;
  }

  QPBOOL      setDirectiontypeValue(xs_string direction) {
    if (direction != QP_NULL)
    {
      if(mDirection)
      {
        qpDplFree(MEM_IMS_XML, mDirection);
      }
      mDirection = qpDplStrdup(MEM_IMS_XML, direction);
    }
    return QP_TRUE;
  }
  xs_string   getDirectiontypeValue() {
    return mDirection;
  }


  QPBOOL  ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL  ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

  QPCHAR* ClassName(){ return (QPCHAR*)"dialog"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName()  { return (QPCHAR*)"dialog"; }
  QPUINT8 Valiadate(){ return 0; }
};



//////////////////////////////////////////////////
//
//  Marshaller is not required for DialogInfo.
//
//////////////////////////////////////////////////
class DialogInfoMarshaller : public XMLMarshaller, QpXMLDocGenerator
{
private:
  QPCHAR*                     mDialogInfoXml;
  qp_xml_common_array_definition*   m_pCommonArray;

public:

  DialogInfoMarshaller();
  ~DialogInfoMarshaller();

  const QPCHAR* Marshall(XMLElement* pObj);

  QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){}
};


class DialogInfoContext : public XMLContext
{
  static QPCHAR *defaultNameSpacePrefix[(XML_NAMESPACES_DIALOGINFO)XMLNS_DIALOG_MAX];
  
public:
  static QPCHAR *NamespaceUri[(XML_NAMESPACES_DIALOGINFO)XMLNS_DIALOG_MAX];
  QPCHAR* getNamespacePrefix(XML_NAMESPACES_DIALOGINFO ns);
  XMLMarshaller* createMarshaller();
  XMLUnMarshaller* createUnMarshaller();
  XMLObjectFactory* createBinderFactory();
};


class DialogInfoDataBinder : public XMLObjectFactory
{

public:

  DialogInfoDataBinder(){};
  ~DialogInfoDataBinder(){};

  XMLObject*  CreateRootElement();
  XMLElement* CreateStateElement();
  XMLElement* CreateSessdElement();
  XMLElement* CreateReferredbyElement();
  XMLElement* CreateLocalElement();
  XMLElement* CreateRemoteElement();
  XMLElement* CreateParamElement();
  XMLElement* CreateTargetElement();
  XMLElement* CreateCseqElement();
  XMLElement* CreateDialogElement();
  XMLElement* CreateReplacesElement();
  XMLElement* CreateHopElement();
  XMLElement* CreateRoutesetElement();
  XMLElement* CreateSaexclusiveElement();
  XMLElement* CreateMediatypeElement();
  XMLElement* CreateMediadirectionElement();
  XMLElement* CreateMediaAttributeElement();
};

class DialogInfoUnMarshaller : public XMLUnMarshaller
{

public:

  DialogInfoUnMarshaller(){};
  ~DialogInfoUnMarshaller(){};

  XMLObject* UnMarshall(const QPCHAR* pstring);
  XMLObject* UnMarshall(XMLFile* pfile);
};
#endif
