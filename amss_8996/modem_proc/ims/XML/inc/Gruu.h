#ifndef __GRUU_H__
#define __GRUU_H__

#include<XMLGruu.h>
#include<XMLList.h>
#include<XMLStack.h>
#include <XmlDecoderSchema.h>

class XMLGruuDecoder : public XmlDecoderSchema
{
public:
  XMLGruuDecoder();
  ~XMLGruuDecoder();
  XmlCodeGenStack	m_codeGenStack; 
  XMLObject*		m_pGruuObject;

  QPINT ProcessElement(QPCHAR* n_strElement,QPCHAR* n_strVal);
  QPINT ProcessAttribute(QPCHAR* n_strElement,QPCHAR* n_strAttr,QPCHAR* n_strVal);
  QPINT Validate() {return 0;};
  XMLElement* ProcessAnyElement(QPCHAR* n_strElement,QPCHAR* n_strVal){return QP_NULL;};
  QPINT ProcessState(QPCHAR *n_strElement) {return 0;};
  QPVOID* GetSessionData() { return m_pGruuObject; };
}; 

#endif

