/*****************************************************************************
  Copyright (c) 2013 Qualcomm Technologies, Inc. All rights reserved.
  File  : XMLOriginatingIdentityPresentation.h
  Desc  : 
  Author: 
  Date  : 

  Revision History 
===============================================================================
  Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
 19-11-2013     Sreenidhi          549221     FR 17157: XML Configuration Access Protocol (XCAP) / Ut support for KDDI    
*****************************************************************************/
#ifndef __XMLORIGINATINGIDENTITYPRESENTATION_H__
#define __XMLORIGINATINGIDENTITYPRESENTATION_H__

#include <qpXMLDocGenerator.h>
#include <qplog.h>

#include "XMLCommonDef.h"
#include "XMLSimservsXcap.h"


class qp_originating_identity_presentation : public qp_ns_xcap_simservType
{
private:
  
  XML_NAMESPACES_CB_CD								uriPrefix;
  qp_originating_identity_presentation(const qp_originating_identity_presentation&){};
  qp_originating_identity_presentation& operator=(const qp_originating_identity_presentation&){return *this;}

public:
  qp_originating_identity_presentation(){ uriPrefix = (XML_NAMESPACES_CB_CD)XMLNS_SS; };
  ~qp_originating_identity_presentation(){};
		

  QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element,QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element,QPCHAR* n_Value,XMLAttribute* n_Object);

  QPBOOL IsRootElement(){
  return QP_TRUE;
  }
  
  XML_NAMESPACES_CB_CD		getUriPrefix()
  {
	  return uriPrefix;
  }
  QPBOOL					toggleUriPrefix();
  QPCHAR* ClassName();
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
  QPCHAR* TagName(){ return (QPCHAR*)"OriginatingIdentityPresentation"; }
  QPUINT8 Valiadate(){ return 0; }
};

class OIPMarshaller : public XMLMarshaller, QpXMLDocGenerator
{

private:
  QPCHAR*		            		mOIPXml;
  qp_xml_common_array_definition*   m_pCommonArray;

public:
  OIPMarshaller();  
  ~OIPMarshaller();

  const QPCHAR* Marshall(XMLElement* pObj);


  QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){}

  QPBOOL AddOIPElementToXml(qp_originating_identity_presentation* originatingIdentityPresentation);
};

class OIPContext : public XMLContext
{
private:

	OIPContext(const OIPContext&){};
    OIPContext& operator=(const OIPContext&){return *this;}
	
	static QPCHAR *initNameSpacePrefix[(XML_NAMESPACES_CB_CD)XMLNS_MAX];
	static QPCHAR *NamespaceUri[3];
	static QPUINT uriListSize;
	QPCHAR** NSPrefix;
	QPVOID initNamespacePrefix();
public:
	XMLObject*		m_pNameSpaceinfo;
  OIPContext();
  ~OIPContext();

  using XMLContext::setNamespacePrefix;

  XMLMarshaller* createMarshaller();
  XMLUnMarshaller* createUnMarshaller();
  XMLObjectFactory* createBinderFactory();

  QPBOOL setNamespacePrefix(XML_NAMESPACES_CB_CD ns, QPCHAR* prefix);
  QPBOOL setNamespacePrefix(QPUINT ns, QPCHAR* prefix);
  QPCHAR* getNamespacePrefix(XML_NAMESPACES_CB_CD ns);
  QPCHAR* getNamespaceUri(QPUINT iIndex);
  QPUINT  getUriListSize();
};

class OIPDataBinder : public XMLObjectFactory
{

public:
	OIPDataBinder(){};  
	~OIPDataBinder(){};

  XMLObject* CreateRootElement();
};

class OIPUnMarshaller : public XMLUnMarshaller
{
public:
	OIPUnMarshaller();  
	~OIPUnMarshaller();

  XMLObject* UnMarshall(const QPCHAR* pstring);
  XMLObject* UnMarshall(XMLFile* pfile);
};

  QPVOID TestOIPMarshaller(QPVOID);

#endif // end of __XMLORIGINATINGIDENTITYPRESENTATION_H__