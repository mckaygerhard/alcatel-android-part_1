/*****************************************************************************
  Copyright (c) 2011-2012 Qualcomm Technologies, Inc. All rights reserved.
  File  : XMLSimservs.h
  Desc  : 
  Author: Likith
  Date  : 25.10.2013

  Revision History 
  Rohit Initial Revision
===============================================================================
  Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
25-Oct_2013		Likith				 -------		Defining Methods.
-------------------------------------------------------------------------------
*****************************************************************************/

#ifndef __XMLSIMSERVS_H__
#define __XMLSIMSERVS_H__

#include <qplog.h>
#include <qpXMLDocGenerator.h>

#include "XMLCommonDef.h"

#include "XMLCommunicationBarring.h"
#include "XMLCommunicationDiversion.h"
#include "XMLCommunicationWaiting.h"
#include "XMLOriginatingIdentityPresentation.h"
#include "XMLTerminatingIdentityPresentation.h"
#include "XMLOIR.h"
#include "XMLTIR.h"

class qp_Simservs : public qp_ns_xcap_simservType 
{
private:
  QpSingleElementList*	   m_anyElement;

  qp_Simservs(const qp_Simservs&){};
  qp_Simservs& operator=(const qp_Simservs&){return *this;}
  
public:
  qp_Simservs();
  ~qp_Simservs();
  

  QPBOOL					setAnyElementValue( QpSingleElementList* any) {
		m_anyElement = any;
	return QP_TRUE;
  }
  QpSingleElementList*		getAnyElementValue() {
	  return m_anyElement;
  }
  
  QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element,QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element,QPCHAR* n_Value,XMLAttribute* n_Object);

  QPBOOL IsRootElement(){
  return QP_TRUE;
  }
  
  QPCHAR* ClassName() { return (QPCHAR*)"qp_Simservs"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"simservs"; }
  QPUINT8 Valiadate(){ return 0; }

};

class SimservsMarshaller : public XMLMarshaller, QpXMLDocGenerator
{
private:
  QPCHAR*		            		mSimservsXml;
  qp_xml_common_array_definition*   m_pCommonArray;

  QPBOOL AddSimservsElementToXml(qp_Simservs* simservs);
  QPCHAR* InsertString(QPCHAR* pSimservsXml, QPCHAR* pInsertString);

public:
  SimservsMarshaller();  
  ~SimservsMarshaller();

  const QPCHAR* Marshall(XMLElement* pObj);
  QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){};

};

class SimservsContext : public XMLContext
{
public:
	SimservsContext();
	~SimservsContext();

  XMLMarshaller* createMarshaller();
  XMLUnMarshaller* createUnMarshaller();
  XMLObjectFactory* createBinderFactory();
};

class SimservsDataBinder : public XMLObjectFactory
{
public:

	SimservsDataBinder() {};
	~SimservsDataBinder() {};

	XMLObject* CreateRootElement();
};

class SimservsUnMarshaller : public XMLUnMarshaller
{
private:
	QPCHAR*		replaceSubStringWithNull(QPCHAR* pString,QPCHAR** pSubString,QPCHAR* pTempStart,QPUINT32 iLen);
	QPBOOL		validateXML(QPCHAR* pstring);
public:
	SimservsUnMarshaller();
	~SimservsUnMarshaller();

  XMLObject* UnMarshall(const QPCHAR* pstring);
  XMLObject* UnMarshall(XMLFile* pfile){ return QP_NULL;};
};

#endif // end of __XMLSIMSERVS_H__
