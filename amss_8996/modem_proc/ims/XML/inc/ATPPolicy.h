#ifndef __ATPPOLICY_H__
#define __ATPPOLICY_H__

#include<XMLATPPolicy.h>
#include<XMLList.h>
#include<XMLStack.h>
#include <XmlDecoderSchema.h>
class XMLATPPolicyDecoder : public XmlDecoderSchema
{

public:
  XMLATPPolicyDecoder();
  ~XMLATPPolicyDecoder();
  XmlCodeGenStack m_codeGenStack; 
  qp_ATPPolicy *m_eATPPolicy;

  QPINT ProcessElement(QPCHAR* n_strElement,QPCHAR* n_strVal);
  QPINT ProcessAttribute(QPCHAR* n_strElement,QPCHAR* n_strAttr,QPCHAR* n_strVal);
  QPINT Validate() {return 0;};
  XMLElement* ProcessAnyElement(QPCHAR* n_strElement,QPCHAR* n_strVal){return QP_NULL;};
  QPINT ProcessState(QPCHAR *n_strElement) {return 0;}; 
  QPVOID* GetSessionData() { return m_eATPPolicy; };

}; 

#endif