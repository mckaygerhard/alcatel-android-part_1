#ifndef __REGINFO_H__
#define __REGINFO_H__

#include <XMLReginfo.h>
#include <XMLList.h>
#include <XMLStack.h>
#include <XmlDecoderSchema.h>
#include <XMLGruu.h>

class XMLReginfoDecoder : public XmlDecoderSchema
{
private: 
  qp_reginfo*         m_preginfo;
  QPCHAR*							m_PassName;
  XmlCodeGenStack     m_codeGenStack; 
  
public:
	XMLReginfoDecoder();
  ~XMLReginfoDecoder();  

  QPINT               ProcessElement(QPCHAR* n_strElement,QPCHAR* n_strVal);
  QPINT               ProcessAttribute(QPCHAR* n_strElement,QPCHAR* n_strAttr,QPCHAR* n_strVal);
  QPINT               Validate() {return 0;};
  XMLElement*         ProcessAnyElement(QPCHAR* n_strElement,QPCHAR* n_strVal);
  QPINT               ProcessState(QPCHAR *n_strElement) {return 0;};
  QPBOOL              XMLRemoveNameSpaceTag(QPCHAR* n_strElement);
  QPVOID*             GetSessionData() {return m_preginfo;}; 
}; 

#endif
  
  