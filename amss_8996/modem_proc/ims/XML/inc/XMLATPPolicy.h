/*****************************************************************************
  Copyright (c) 2011-2012 Qualcomm Technologies, Inc. All rights reserved.
  File  : XMLATPPolicy.h
  Desc  : 
  Author: Likith(likithym)
  Date  : 07.08.2013

  Revision History 
===============================================================================
  Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
 07.08.2013   Likith				    ----      Defining the methods.
-------------------------------------------------------------------------------
*****************************************************************************/
#ifndef __XMLATPPOLICY_H__
#define __XMLATPPOLICY_H__

#include <qpXMLDocGenerator.h>
#include <qplog.h>

#include "XMLCommonDef.h"

class qp_Package : public XMLElement
{
  private:
  xs_string					 mName;
  xs_string					 mHashCode;
  xs_nonNegativeInt			 mMaxAckTimeOut;
  xs_string					 mFrameworkUID;
  QpSingleElementList*		 m_pAnyAttribute;

  qp_Package(const qp_Package&){};
  qp_Package& operator=(const qp_Package&){return *this;}

  public:
	  qp_Package(){
	  mName					=	QP_NULL;
	  mHashCode				=	QP_NULL;
	  mMaxAckTimeOut		=	0;
	  mFrameworkUID			=	QP_NULL;
  	  m_pAnyAttribute		=	QP_NULL;
	  }
	  ~qp_Package();

  QPBOOL				  setNameValue(xs_string name) {
	if(name != QP_NULL)
	{
	  mName = qpDplStrdup(MEM_IMS_XML,name);
	}
	return QP_TRUE;
  }
  xs_string				  getNameValue() {
	  return mName;
  }

  QPBOOL				  setHashCodeValue(xs_string hash_code) {
	if(hash_code != QP_NULL)
	{
	  mHashCode = qpDplStrdup(MEM_IMS_XML,hash_code);
	}
	return QP_TRUE;
  }
  xs_string				  getHashCodeValue() {
	  return mHashCode;
  }
  
  QPBOOL				  setMaxAckTimeOutValue(xs_nonNegativeInt max_ack_timeout) {
	
	mMaxAckTimeOut = max_ack_timeout;
	return QP_TRUE;
  }
  xs_nonNegativeInt				  getMaxAckTimeOutValue() {
	  return mMaxAckTimeOut;
  }

  QPBOOL				  setFrameworkUIDValue(xs_string framework_uid) {
	if(framework_uid != QP_NULL)
	{
	  mFrameworkUID = qpDplStrdup(MEM_IMS_XML,framework_uid);
	}
	return QP_TRUE;
  }
  xs_string				  getFrameworkUIDValue() {
	  return mFrameworkUID;
  }

  
  QPBOOL				  setAnyAttributeValue( QpSingleElementList* anyAttribute) {
	  m_pAnyAttribute = anyAttribute;
	return QP_TRUE;
  }
  QpSingleElementList*	  getAnyAttributeValue() {
	  return m_pAnyAttribute;
  }

  QPBOOL				  ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
  QPBOOL				  ProcessUnMarshallAttribute(QPCHAR* n_Element,QPCHAR* n_Value,XMLAttribute* n_Object);

  QPCHAR* ClassName() {return (QPCHAR*)"qp_Package"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"Package"; }
  QPUINT8 Valiadate(){ return 0; }
};

class qp_Service : public XMLElement
{
  private:
  xs_string					 mID;
  QpSingleElementList*		 m_pPackage;

  qp_Service(const qp_Service&){};
  qp_Service& operator=(const qp_Service&){return *this;}

  public:
	  qp_Service(){
	  mID					=	QP_NULL;
  	  m_pPackage			=	QP_NULL;
	  }
	  ~qp_Service();

  QPBOOL				  setIDValue(xs_string id) {
	if(id != QP_NULL)
	{
	  mID = qpDplStrdup(MEM_IMS_XML,id);
	}
	return QP_TRUE;
  }
  xs_string				  getIDValue() {
	  return mID;
  }
  
  QPBOOL				  setPackageListValue( QpSingleElementList* package) {
	  m_pPackage = package;
	return QP_TRUE;
  }
  QpSingleElementList*	  getPackageListValue() {
	  return m_pPackage;
  }

  QPBOOL				  ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
  QPBOOL				  ProcessUnMarshallAttribute(QPCHAR* n_Element,QPCHAR* n_Value,XMLAttribute* n_Object);

  QPCHAR* ClassName() {return (QPCHAR*)"qp_Service"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"Service"; }
  QPUINT8 Valiadate(){ return 0; }
};


class qp_APN : public XMLElement
{
  private:
  xs_string					 mAPNName;
  xs_string					 mPolicyServer;
  xs_string					 mReportServer;
  xs_nonNegativeInt			 mPolicyRefreshTime;
  QpSingleElementList*		 m_pService;

  qp_APN(const qp_APN&){};
  qp_APN& operator=(const qp_APN&){return *this;}

  public:
	  qp_APN(){
	  mAPNName				=	QP_NULL;
  	  mPolicyServer			=	QP_NULL;
	  mReportServer			=	QP_NULL;
	  mPolicyRefreshTime	=	0;
	  m_pService		=	QP_NULL;
	  }
	  ~qp_APN();

  QPBOOL				  setAPNNameValue(xs_string apn_name) {
	if(apn_name != QP_NULL)
	{
	  mAPNName = qpDplStrdup(MEM_IMS_XML,apn_name);
	}
	return QP_TRUE;
  }
  xs_string				  getAPNNameValue() {
	  return mAPNName;
  }

  QPBOOL				  setPolicyServerValue(xs_string policy_server) {
	if(policy_server != QP_NULL)
	{
	  mPolicyServer = qpDplStrdup(MEM_IMS_XML,policy_server);
	}
	return QP_TRUE;
  }
  xs_string				  getPolicyServerValue() {
	  return mPolicyServer;
  }

  QPBOOL				  setReportServerValue(xs_string report_server) {
	if(report_server != QP_NULL)
	{
	  mReportServer = qpDplStrdup(MEM_IMS_XML,report_server);
	}
	return QP_TRUE;
  }
  xs_string			  getReportServerValue() {
	  return mReportServer;
  }

  QPBOOL				  setPolicyRefreshTimeValue(xs_nonNegativeInt policy_refresh_time) {
	
	mPolicyRefreshTime = policy_refresh_time;	
	return QP_TRUE;
  }
  xs_nonNegativeInt		   getPolicyRefreshTimeValue() {
	  return mPolicyRefreshTime;
  }

  QPBOOL				  setServiceListValue( QpSingleElementList* service) {
	  m_pService = service;
	return QP_TRUE;
  }
  QpSingleElementList*	  getServiceListValue() {
	  return m_pService;
  }

  QPBOOL				  ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
  QPBOOL				  ProcessUnMarshallAttribute(QPCHAR* n_Element,QPCHAR* n_Value,XMLAttribute* n_Object);

  QPCHAR* ClassName() {return (QPCHAR*)"qp_APN"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"APN"; }
  QPUINT8 Valiadate(){ return 0; }
};


class qp_ATPPolicy : public XMLObject
{
  private:
  xs_string mVersion;
  QpSingleElementList* m_pAPN;

  public:
	  qp_ATPPolicy();
	  ~qp_ATPPolicy();

  QPBOOL			  setVersionValue(xs_string version){
	  if( version != QP_NULL)
	  {
		mVersion = qpDplStrdup(MEM_IMS_XML,version);
	  }
	  return QP_TRUE;
  }
  xs_string				  getVersionValue() {
	  return mVersion;
  }

  QPBOOL				setAPNListValue(QpSingleElementList* apn_list){	  
	  m_pAPN = apn_list;
	  return QP_TRUE;
  }
  QpSingleElementList*  getAPNListValue() {
	  return m_pAPN;
  }

  QPBOOL				ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
  QPBOOL				ProcessUnMarshallAttribute(QPCHAR* n_Element,QPCHAR* n_Value,XMLAttribute* n_Object);
  
  QPBOOL IsRootElement();

  QPCHAR* ClassName();
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
  QPCHAR* TagName(){ return (QPCHAR*)"ATP_Policy"; }
  QPUINT8 Valiadate(){ return 0; }
};

class ATPPolicyMarshaller : public XMLMarshaller, QpXMLDocGenerator
{

private:
  QPCHAR*		            		mATPPolicyXml;
  qp_xml_common_array_definition*   m_pCommonArray;

  QPBOOL AddATPPolicyElementToXml(qp_ATPPolicy* 	atp_policy);
  QPBOOL AddAPNElementToXml(qp_APN* 	apn);
  QPBOOL AddServiceElementToXml(qp_Service* 	service);
  QPBOOL AddPackageElementToXml(qp_Package* 	package);

public:

  ATPPolicyMarshaller();  
  ~ATPPolicyMarshaller();

  const QPCHAR* Marshall(XMLElement* pObj);


  QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){}

 

};

class ATPPolicyContext : public XMLContext
{
public:
  XMLMarshaller* createMarshaller();
  XMLUnMarshaller* createUnMarshaller();
  XMLObjectFactory* createBinderFactory();
};

class ATPPolicyDataBinder : public XMLObjectFactory
{

public:

	ATPPolicyDataBinder(){};  
	~ATPPolicyDataBinder(){};

  XMLObject* CreateRootElement();
  XMLElement* CreateAPNElement();
  XMLElement* CreateServiceElement();
  XMLElement* CreatePackageElement();
};

class ATPPolicyUnMarshaller : public XMLUnMarshaller
{

public:

	ATPPolicyUnMarshaller(){};  
	~ATPPolicyUnMarshaller(){};

  XMLObject* UnMarshall(const QPCHAR* pstring);
  XMLObject* UnMarshall(XMLFile* pfile);
};

#endif  //__XMLATPPOLICY_H__