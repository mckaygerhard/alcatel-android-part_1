#ifndef QIPCALLEXT_H
#define QIPCALLEXT_H
/*===========================================================================

Q I P C A L L E X T
H E A D E R   F I L E

DESCRIPTION
This file contains the mapping of external functions to internal names
This is used for replacing these external functions with mock versions
for unit testing.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/


/*===========================================================================

EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/qipcall/inc/qipcallext.h#1 $
$DateTime: 2016/03/28 23:03:22 $
$Author: mplcsds1 $
$Change: 10156183 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
01/19/15   aa     CR - 658583 - FR 21559: SMS over IMS: Support for RP-SMMA
11/14/13   sr       CR 564101 Changes in qipcall code for implementing UTF
09/30/13   mj       Fix for CR 532353: UE is not sending E911 DAN after emergency call ends 
06/02/13   nar      Initial Version.
===========================================================================*/

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "ims_variation.h"

#ifdef FEATURE_IMS_PC_TESTBENCH
#include "qipcallext_test.h"
#else


/*---------------------------------------------------------------------------
These are platform-layer function declarations that should be used across
qipcall instead of other external functions such as qpDpl*()
This layer allows mock implementations for unit-testing end-to-end scenarios
to be implemented internally rather than replacing external functions
which are owned by other modules.
---------------------------------------------------------------------------*/

#define qfDplGetGlobalData                                qpDplGetGlobalData
#define qfDplPostEventToEventQueue                        qpDplPostEventToEventQueue
#define qfDplGetMSISDN                                    qpDplGetMSISDN
#define qfDplGetPublicServiceIdentity                     qpDplGetPublicServiceIdentity
#define qfDplGetPublicUID                                 qpDplGetPublicUID
#define qfGetSMSP_SCAddress                               qpGetSMSP_SCAddress
#define qfDplGetHNDomain                                  qpDplGetHNDomain
#define qfDplGetLocalHost                                 qpDplGetLocalHost
#define qfDplGetRSRP                                      qpDplGetRSRP
#define qfDplGetIMSI                                      qpDplGetIMSI
#define qfDplGetGrantedQoSFlowSpec                        qpDplGetGrantedQoSFlowSpec
#define qfDplNetOpen                                      qpDplNetOpen
#define qfDplNetClose                                     qpDplNetClose
#define qfDplNetSendData                                  qpDplNetSendData
#define qfDplCallInitialize                               qpDplCallInitialize
#define qfDplCallUnInitialize                             qpDplCallUnInitialize
#define qfDplCallCtrlInitialize                           qpDplCallCtrlInitialize
#define qfDplCallCtrlUnInitialize                         qpDplCallCtrlUnInitialize
#define qfDplCallCtrlCapReg                               qpDplCallCtrlCapReg
#define qfDplCallCtrlInformStatus                         qpDplCallCtrlInformStatus
#define qfDcmInformIMSRegStatus                           qpDcmInformIMSRegStatus
#define qfDplCallCtrlReportInd                            qpDplCallCtrlReportInd
#define qfDplCallCtrlAckEvent                             qpDplCallCtrlAckEvent
#define qfDplCallCtrlGetCallId                            qpDplCallCtrlGetCallId
#define qfDplCallCtrlFreeCallId                           qpDplCallCtrlFreeCallId
#define qfDplCallCtrlInitCallNum                          qpDplCallCtrlInitCallNum
#define qfDplCallCtrlFillCallNum                          qpDplCallCtrlFillCallNum
#define qfDplCallCtrlMMCallCmdOrig                        qpDplCallCtrlMMCallCmdOrig
#define qfDplCallCtrlDeRegUSSCB                           qpDplCallCtrlDeRegUSSCB
#define qfDplQoSEventReg                                  qpDplQoSEventReg
#define qfDplQoSEventDeReg                                qpDplQoSEventDeReg
#define qfDplReleaseQoSInstance                           qpDplReleaseQoSInstance
#define qfDplPhoneInitialize                              qpDplPhoneInitialize
#define qfDplIsMCCInWhiteList                             qpDplIsMCCInWhiteList

#define qfSmsCreateProfile                                qpSmsCreateProfile
#define qfdpl_get_config_group_value                      qpdpl_get_config_group_value
#define qfSmsSendIndication                               qpSmsSendIndication
#define qfSmsConvertDtmfToAscii                           qpSmsConvertDtmfToAscii
#define qfSmsUpdateTransportCapability                    qpSmsUpdateTransportCapability
#define qfLogEventPayloadRecord                           qpLogEventPayloadRecord
#define qfSmsSendDomainNotificationSms                    qpSmsSendDomainNotificationSms
#define qfDcmRegSysInfo                                   qpDcmRegSysInfo
#define qfSmsRegisterForMoSms                             qpSmsRegisterForMoSms
#define qfSmsSetNwRegStatus                               qpSmsSetNwRegStatus
#define qfSmsInitDomainNotification                       qpSmsInitDomainNotification

#define mock_ims_update_service_status					  ims_update_service_status

#define qfDplEcmpReg									  qpDplEcmpReg
#define qfDplCallCtrlCapRegAppend						  qpDplCallCtrlCapRegAppend
#define qfSmsUpdateTransportFTimer						            qpSmsUpdateTransportFTimer
#define qvf_rtp_register                                  qvp_rtp_register
#define qvf_rtp_shut_down                                 qvp_rtp_shut_down
#define qvf_rtp_open2                                     qvp_rtp_open2
#define qvf_rtp_close                                     qvp_rtp_close
#define qvf_rtp_link_alive_timer_start                    qvp_rtp_link_alive_timer_start
#define qvf_rtp_pause_stream                              qvp_rtp_pause_stream
#define qvf_rtp_resume_stream                             qvp_rtp_resume_stream
#define qvf_rtp_configure                                 qvp_rtp_configure
#define qvf_rtp_configure_session                         qvp_rtp_configure_session
#define qvf_rtp_send_dtmf                                 qvp_rtp_send_dtmf
#define qvf_rtp_media_capability                          qvp_rtp_media_capability
#define qvf_rtp_generate_intra_coded_frame                qvp_rtp_generate_intra_coded_frame
#define qvf_rtp_media_start_stream                        qvp_rtp_media_start_stream
#define qvf_rtp_media_stop_stream                         qvp_rtp_media_stop_stream
#define qvf_rtp_media_codec_init                          qvp_rtp_media_codec_init
#define qvf_rtp_media_config_codec                        qvp_rtp_media_config_codec
#define qvf_rtp_media_init                                qvp_rtp_media_init
#define qvf_rtp_media_deinit                              qvp_rtp_media_deinit
#define qvf_rtp_media_generate_nal                        qvp_rtp_media_generate_nal
#define qvf_rtp_media_generate_nal_sync                   qvp_rtp_media_generate_nal_sync
#define qvf_rtp_empty_jitter_buffer                       qvp_rtp_empty_jitter_buffer
#define qvf_media_release_codec                           qvp_media_release_codec
#define qvf_rtp_configure_gbr_mbr                         qvp_rtp_configure_gbr_mbr
#define qvf_rtp_media_srtp_generate_master_key            qvp_rtp_media_srtp_generate_master_key
#define qvf_rtp_clear_all_media_resource                  qvp_rtp_clear_all_media_resource
#define qvf_rtcp_link_alive_timer_start                   qvp_rtcp_link_alive_timer_start
#define qvf_start_keep_alive_timer                        qvp_start_keep_alive_timer
#define qvf_stop_keep_alive_timer                         qvp_stop_keep_alive_timer
#define qvf_rtp_srvcc_tear_down                           qvp_rtp_srvcc_tear_down

// SIP Class Instance Allocation
#define qf_create_request_processor                       qp_create_request_processor
#define qf_create_session_service                         qp_create_session_service
#define qf_create_subscribe_service                       qp_create_subscribe_service
#define qf_create_im_service                              qp_create_im_service

// SIP Class Definitions
#define ICommonComponentsBase                             CommonComponentsBase
#define IRequestHandler                                   RequestHandler
#define IRequestProcessor                                 RequestProcessor
#define ISessionService                                   SessionService
#define ISubscribeService                                 SubscribeService
#define IInstantMessageService                            InstantMessageService
#define IRequestListener                                  RequestListener
#define IResponseListener                                 ResponseListener

// RegManager Class Definitions
#define IRegisterServiceMonitor                           RegisterServiceMonitor
#define IQPConfigurationHandler                           QPConfigurationHandler

#define IMafTimer                                         MafTimer

#endif // FEATURE_IMS_PC_TESTBENCH

#endif /* QIPCALLEXT_H */
