#ifndef QIPCALLUSSD_H
#define QIPCALLUSSD_H
/*===========================================================================

                        Q I P C A L L  Conference Handler
                        I M P L E M E N T A T I O N


DESCRIPTION
  This file contains the implementation for QIPCALL Conference call handler.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.

===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/qipcall/inc/qipcallussd.h#1 $
$DateTime: 2016/03/28 23:03:22 $
$Author: mplcsds1 $
$Change: 10156183 $

when       who     what, where, why
--------   ---    ----------------------------------------------------------
06/18/13   mj     CR 682518 - USSD requests are failing after mode lpm/online during USSD running
09/27/13   mj     CR 551855 - [IMS USSD] BYE is not sent to AS when UE receives 469 response to USSD INFO response
08/01/13   mj     CR 522006- Additional USSD changes 
07/09/13   mj     CR 510570- pending USSD changes 
06/09/13   mj     Initial Version
===========================================================================*/



/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/

#include "ims_variation.h"
#include "customer.h"

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef FEATURE_VOIP

#include "qpDplCallCtrl.h"
#include "qipcalliface.h"
#include "qipcallsdp.h"
#include "qimfif_api.h"

/*===========================================================================

LOCAL DATA DECLARATIONS

===========================================================================*/

#define QIPCALLUSSD_MAX_CALLS                      1
#define QIPCALLUSSD_INVALID_CALLID                 0xFF
#define QIPCALLUSSD_ERROR_TAG                      0x91


typedef enum
{
  QIPCALL_USSD_STATE_IDLE,                        
  QIPCALL_USSD_STATE_INVITE_ORIG,       
  QIPCALL_USSD_STATE_CONNECTED, //200OK for INVITE
  QIPCALL_USSD_STATE_INFO_RCVD,
  QIPCALL_USSD_STATE_WAIT_FOR_200OK_FOR_INFO_RESP,
  QIPCALL_USSD_STATE_INFO_SUCCESS,
  QIPCALL_USSD_STATE_MAX
} qipcallussd_state;

/*---------------------------------------------------------------------------
TYPEDEF qipcallussd_obj_type

DESCRIPTION
This structure contains all the information for one QV Phone session
---------------------------------------------------------------------------*/
typedef struct
{
  qipcall_iface_e_type                                            iface;  
  qipcallussd_state                                                   state;
  QP_CALL_ID                                                              call_id;  
  QPUINT8                                                                    sip_body_buffer[QIPCALL_SIP_BODY_SIZE];
  qimfif_call_handle_objs_s                                 qimf_objs;          /* qimf objects associated with this call */
  qimfif_sip_hdrs_type                                           sip_session_cfg;    /* session info */
  QPCHAR                                                                     *dialog_id;         /*SIP dialog id*/
  qvp_rtp_ip_addr_type                                         local_ip;  
  qipcallcodec_media_info_s                               media_info;      /* MJ: Contain rtp_stream, codec_info, curr_time_aud, dtmf_tx_pyld_type, 
                                                                                                                        ** audio_tx_pyld_type, Added video_tx_pyld_type */
  qipcallh_sdp_info_s                                              sdp_info;           /* MJ: Contain my_sdp_container, my_m_lines, peer_sdp_container, 
                                                                                                                       ** pre_condition_table, precondition_enabled */
  QPUINT8                                                                    invoke_id;
  QPUINT8                                                                    ss_ref;
  QPUINT8                                                                    coding_scheme;
  QPCHAR                                                                     *language;
  QPCHAR                                                                     *ussd_string;
  QPINT32                                                                      error_code;
  QPUINT16               sdp_session_id_seed;    

} qipcallussd_obj_type;

qipcallussd_obj_type*     qipcallussd_obj[QIPCALLUSSD_MAX_CALLS];

/* Object to save the ussd status */
//static qipcall_ussd qipcallussd_obj_type;

/* USSD call id */
typedef QPUINT8 qipcallussd_call_id_type;


/*
typedef enum 
{
  QIPCALLUSSD_ERROR_INFO,
  QIPCALLUSSD_DATA_INFO
}qipcallussd_ConfEType;

typedef struct 
{
   QPBOOL present;
   QPINT8 error_code_tag;
   QPINT8 error_code;
} qipcallussdErrorType;

typedef struct 
{
   QPBOOL         bPresent;
   QPUINT8        iUSSDataCodingScheme;
   QPUINT8        iSize;
   QPUINT8        iUSSData[QP_MAX_USS_CHAR];
} qipcallussdData;

typedef struct 
{
  qipcallussd_ConfEType     eDataType;
  union 
  {
    qipcallussdData                            sUSSData;
    qipcallussdErrorType                  sError;
  }data;
}qipcallussdProcessResSType; 

*/



/*===========================================================================
FUNCTION      qipcallussd_init_ussd_obj

DESCRIPTION
Initializes a ussd Object.

PARAMETERS
None

RETURN VALUE
NONE

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_init_ussd_obj(qipcallussd_obj_type* ussd_ptr);


/*===========================================================================
FUNCTION      qipcallussd_cleanup_ussd_obj

DESCRIPTION
Destroys everything related to a USSD Object.

PARAMETERS
None

RETURN VALUE
NONE

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_cleanup_ussd_obj(qipcallussd_obj_type* ussd_ptr);


/*===========================================================================
FUNCTION      qipcallussd_allocate_call_idx

DESCRIPTION
Allocate a QIPCALL Call Idx and associated call object.

PARAMETERS
None

RETURN VALUE
NONE

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
qipcallussd_call_id_type qipcallussd_allocate_call_idx(QPVOID);


/*===========================================================================
FUNCTION      qipcallussd_deallocate_call_id

DESCRIPTION
DeAllocate a USSD Call Id and associated call object.

PARAMETERS
None

RETURN VALUE
NONE

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_deallocate_call_id(QP_CALL_ID call_id);


/*===========================================================================
FUNCTION qipcallussd_getussdptr_by_ss_ref

DESCRIPTION
This is an accessor function that returns the pointer to qipcallussd_obj
used by the state machine. This pointer is still owned by the state machine
and cannot be deleted by the caller.

PARAMETERS
None

RETURN VALUE
qipcallussd_obj_type * : pointer to qipcallussd_obj instance used by
the state machine

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
qipcallussd_obj_type *qipcallussd_getussdptr_by_ss_ref(QPUINT8   ss_ref);

/*===========================================================================
FUNCTION qipcallussd_getussdptr_by_invoke_id

DESCRIPTION
This is an accessor function that returns the pointer to qipcallussd_obj
used by the state machine. This pointer is still owned by the state machine
and cannot be deleted by the caller.

PARAMETERS
None

RETURN VALUE
qipcallussd_obj_type * : pointer to qipcallussd_obj instance used by
the state machine

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
qipcallussd_obj_type *qipcallussd_getussdptr_by_invoke_id(QPUINT8   invoke_id);

/*===========================================================================
FUNCTION qipcall_getussdptr_bycallid

DESCRIPTION
This is an accessor function that returns the pointer to qipcallussd_obj
used by the state machine. This pointer is still owned by the state machine
and cannot be deleted by the caller.

PARAMETERS
None

RETURN VALUE
qipcallussd_obj_type * : pointer to qipcallussd_obj_type instance used by
the state machine

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
qipcallussd_obj_type *qipcallussd_getussdptr_bycallid(QP_CALL_ID call_id);


/*===========================================================================
FUNCTION      qipcallussd_get_ussdptr_from_sip_callid

DESCRIPTION
Returns the pointer to ussd object with this call id.

PARAMETERS

RETURN VALUE
USSD obj ptr

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
qipcallussd_obj_type * qipcallussd_get_ussdptr_from_sip_callid(const QPCHAR *call_id);


/*===========================================================================
FUNCTION qipcallussd_store_cm_request_params(ussd_invite);

DESCRIPTION
Stores data passed by CM USSD request

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_store_cm_request_params(QP_SUPP_SRV_CB_DATA *ussd_invite, qipcallussd_obj_type* ussd_ptr );

/*===========================================================================
FUNCTION qipcallussd_create_ussd_xml

DESCRIPTION
Interfaces with the XML to generate the ussd xml

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPCHAR* qipcallussd_create_ussd_xml(qipcallussd_obj_type* ussd_ptr);

/*=========================================================================*/
/**
*   Description: Parse xml received from network
*   
*   @param  QPCHAR * content_body
*   @return TRUE - If yes, FALSE - Otherwise
*
*/
QPBOOL qipcallussd_parse_ussd_xml(QPCHAR * content_body, QPCHAR * dialog_id);

/*===========================================================================
FUNCTION       qipcallussd_chg_state_to_invite_orig

DESCRIPTION
Move the ussd object state to INVITE ORIG state from current state.

PARAMETERS
None

RETURN VALUE
NONE

DEPENDENCIES
NONE

SIDE EFFECTS

===========================================================================*/
QPVOID qipcallussd_chg_state_to_invite_orig(qipcallussd_obj_type  *ussd_ptr);

/*===========================================================================
FUNCTION       qipcallussd_chg_state_to_connected

DESCRIPTION
Move the ussd object state to CONNECTED state from current state.

PARAMETERS
None

RETURN VALUE
NONE

DEPENDENCIES
NONE

SIDE EFFECTS

===========================================================================*/
QPVOID qipcallussd_chg_state_to_connected(qipcallussd_obj_type  *ussd_ptr);


/*===========================================================================
FUNCTION       qipcallussd_chg_state_to_idle

DESCRIPTION
Move the call object state to IDLE state from current state.

PARAMETERS
None

RETURN VALUE
NONE

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_chg_state_to_idle(qipcallussd_obj_type  *ussd_ptr);


/*===========================================================================
FUNCTION   qipcallussd_process_connected_msg

DESCRIPTION
This function processes the session connected message - 200ok for ussd invite.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_connected_msg(const qimfif_msg_type *p_qimf_param);


/*===========================================================================
FUNCTION  qipcallussd_process_disconnect_msg

DESCRIPTION
This function processes the disconnect message

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_disconnect_msg( const qimfif_msg_type *p_qimf_param );


/*===========================================================================
FUNCTION    qipcallussd_release_rpt_ind

DESCRIPTION
Sends RELEASE indication report to CM.

PARAMETERS
None

RETURN VALUE
NONE

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_release_rpt_ind(QPUINT8  ss_ref, QPINT32 error_code);


/*===========================================================================
FUNCTION    qipcallussd_conf_rpt_ind

DESCRIPTION
Sends USSD SS_CONF indication report to CM.

PARAMETERS
None

RETURN VALUE
NONE

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_conf_rpt_ind(QPUINT8  ss_ref, QPINT32 error_code);


/*===========================================================================
FUNCTION     qipcallussd_get_local_ip_addr_info

DESCRIPTION
Gets the ip address info corresponding to the interface

PARAMETERS
None

RETURN VALUE
A status indicating if ip address info could be updated 

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
qipcallsdp_status_type qipcallussd_get_local_ip_addr_info(  qipcallussd_obj_type *ussd_ptr );


/*===========================================================================

FUNCTION QIPCALLUSSD_INITILIZE_M_LINE

DESCRIPTION
This function is called to initialize a media line in the SDP offer

DEPENDENCIES
None

PARAMETERS
ussd_ptr               -> pointer to the ussd object

RETURN VALUE
A status indicating whether the media line could be generated

SIDE EFFECTS
None

===========================================================================*/
qipcallsdp_status_type qipcallussd_initialize_offer_m_line( qipcallussd_obj_type *ussd_ptr);


/*===========================================================================

FUNCTION qipcallussd_initialize_m_line_RTCP_bandwidth

DESCRIPTION
This function is called to initialize AS bandwiths in media line in the
SDP offer

DEPENDENCIES
None

PARAMETERS
ussd_ptr             -> pointer to the ussd object

RETURN VALUE
A status indicating whether the media line bandwidth could be initialised

SIDE EFFECTS
None

===========================================================================*/
qipcallsdp_status_type qipcallussd_initialize_m_line_RTCP_bandwidth( qipcallussd_obj_type *ussd_ptr);


/*===========================================================================

FUNCTION QIPCALLUSSD_GENERATE_SDP_CONTAINER

DESCRIPTION
This function will generate an SDP offer container

DEPENDENCIES
None

PARAMETERS
ussd_ptr              -> pointer to the ussd object

RETURN VALUE
A status indicating whether the SDP offer container could be generated

SIDE EFFECTS
None

===========================================================================*/
qipcallsdp_status_type qipcallussd_generate_container(qipcallussd_obj_type *ussd_ptr);


/*===========================================================================

FUNCTION      qipcallussd_create_sdp

DESCRIPTION
Creates sdp text 

PARAMETERS
call_ptr     <- pointer to call object
is_offer        QPBOOL
DEPENDENCIES
None

RETURN VALUE
A status indicating whether the SDP offer container could be generated

SIDE EFFECTS
none

===========================================================================*/
qipcallsdp_status_type qipcallussd_create_sdp(  qipcallussd_obj_type *ussd_ptr );

/*===========================================================================
FUNCTION       qipcallussd_create_and_send_invite

DESCRIPTION
Creates the SDP packet containg USSD information and sends the
SIP INVITE.

PARAMETERS
ussd_ptr

RETURN VALUE
NONE

DEPENDENCIES
NONE

SIDE EFFECTS
===========================================================================*/
QPVOID qipcallussd_create_and_send_invite( qipcallussd_obj_type *ussd_ptr );

/*===========================================================================
FUNCTION   qipcallussd_process_orig

DESCRIPTION
Process USSD Call Origination Command

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_invite(qipcall_msg_type *ussd_invite);

/*===========================================================================
FUNCTION   qipcallussd_process_info_resp

DESCRIPTION
Process USSD INFO response command

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_info_resp(qipcall_msg_type *ussd_info_resp);


/*===========================================================================
FUNCTION   qipcallussd_process_recd_info

DESCRIPTION
Process USSD Call Origination Command

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_recd_info( const qimfif_msg_type *p_qimf_param );


/*===========================================================================
FUNCTION   qipcallussd_process_recd_200ok_for_info

DESCRIPTION
This function processes the 200ok for response INFO.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_recd_200ok_for_info(const qimfif_msg_type *p_qimf_param);


/*===========================================================================
FUNCTION  qipcallussd_process_end

DESCRIPTION


DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_end(qipcall_msg_type *ussd_end);

/*===========================================================================
FUNCTION   QIPCALLUSSD_PROCESS_RINGBACKMSG

DESCRIPTION
This function processes the ringback message. This function will inform the
user that the remote party has been located

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_ringback_msg(const qimfif_msg_type *p_qimfif_msg);

/*===========================================================================
FUNCTION   QIPCALLUSSD_PROCESS_PRACK_SUCCESS_EVENT

DESCRIPTION
This function processes the prack 200ok.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_prack_success_event(const qimfif_msg_type *p_qimf_param);

/*===========================================================================
FUNCTION   QIPCALLUSSD_PROCESS_PRACK_FAILED_EVENT

DESCRIPTION
This function processes the prack fail event.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_prack_failed_event(const qimfif_msg_type *p_qimf_param);

/*===========================================================================
FUNCTION   QIPCALLUSSD_PROCESS_INCOMING_SESSION_PROGRESS_EVENT

DESCRIPTION
This function processes the 183 session in progress event.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_incoming_session_progress_event(const qimfif_msg_type *p_qimf_param);

/*===========================================================================
FUNCTION   qipcallussd_process_graceful_exit_event

DESCRIPTION
Performs clean up of SS / RH & other resources.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_graceful_exit_event(qimfif_msg_type  *p_qimf_param);

/*===========================================================================
FUNCTION  qipcallussd_process_timeout_msg

DESCRIPTION
Handle QIMF timeout event.

QIMF gives the app a time out event under the following conditions:
a)  QPE_SIP_SESSION_OUTGOING_TIMEOUT_EV:
Internal Timeout for a Invite. In this case, the application has to close
the SessionService. This event is raised when either Invite Times out without
getting a  response OR some mid-dialog request such as Prack/Update does not
receive a response.

b) QPE_SIP_SESSION_INCOMING_TIMEOUT_EV:
Internal Timeout for a 3xx-6xx Response sent for an Invite
In this case application will have a RequestHandler for INIVTE
and maybe PRACK/UPDATE RequestHandlers also and all that needs to be cleaned up.
This event is raised when no ACK is received for the non-2xx response sent to
an incoming INVITE.

c) QPE_SIP_SESSION_EXPIRED_EV:
Session timer expired without receiving a session refresh request

d) QPE_SIP_SESSION_REFRESH_TIMEDOUT_EV:
Session timer timedout without receiving a response for the session refresh request sent.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_timeout_msg( const qimfif_msg_type *p_qimf_param );


/*===========================================================================
FUNCTION   qipcallussd_process_request_failed_event

DESCRIPTION
Handles request failure events.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_process_request_failed_event(qimfif_msg_type  *p_qimf_param);

/*===========================================================================
FUNCTION   qipcallussd_handle_oprt_mode_change

DESCRIPTION
Handles the powerdown/offline/lpm cases

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_handle_oprt_mode_change(QPE_OPRT_MODE eSystemOprtMode);

/*===========================================================================
FUNCTION qipcallussd_cleanup_all_ussd_reqs

DESCRIPTION
This is an accessor function that returns the pointer to qipcallussd_obj
used by the state machine. This pointer is still owned by the state machine
and cannot be deleted by the caller.

PARAMETERS
None

RETURN VALUE
qipcallussd_obj_type * : pointer to qipcallussd_obj instance used by
the state machine

DEPENDENCIES
NONE

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallussd_cleanup_all_ussd_reqs(QPVOID);

#ifdef __cplusplus
}
#endif

#endif

#endif