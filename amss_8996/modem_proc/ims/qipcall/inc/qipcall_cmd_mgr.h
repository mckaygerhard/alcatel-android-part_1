#ifndef QIPCALL_UI_CMD_MGR
#define QIPCALL_UI_CMD_MGR
/*===========================================================================

                            Q I P C A L L I
                       H E A D E R   F I L E

DESCRIPTION
  This file contains the declaration of many internal datatypes and fucntions
  used by QIPCALL.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/qipcall/inc/qipcall_cmd_mgr.h#1 $
$DateTime: 2016/03/28 23:03:22 $
$Author: mplcsds1 $
$Change: 10156183 $

  when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/04/13    ma    CR649669:Handling HOld Failure. Introducing ANSWER REJECT command.
10/07/13   ma       Initial Version.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#include "comdef.h"
#include "sys.h"
#include "stdio.h"
#include "string.h"
#include "math.h"
#include "rex.h"

#include "qipcallext.h"

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef FEATURE_VOIP
#include "qvp_rtp_api.h"
#endif  //\\FEATURE_VOIP

#include <qpdpl.h>
#include "cmipapp.h"
#include "nv.h"
#include "qimfif_sipdefs.h"
#include "qipcallcodec.h"

#ifdef FEATURE_VOIP
#include "qvp_sdp_api.h"
#endif  //\\FEATURE_VOIP

#include "qpDplCallCtrl.h"
#include "qpdcm.h"
#include "qpIO.h"
#include "ims_task.h"

/*=========================================================================*/
/**
*   Description: init_qipcallcmd_Queue_element . 
*   Malloc queue element. copies  command 
*   @param  qipcall_msg_type* source ,  qipcallcmd_Queue_element *destination buffer
*   @return None 
*	
*/
QPVOID init_qipcallcmd_Queue_element(const qipcall_msg_type* qipcall_msg);
/*===========================================================================
FUNCTION  qipcallcmd_process_or_queue

DESCRIPTION
UI commands are either queued/ or processed immediately based on prerequisites state,

DEPENDENCIES
None

RETURN VALUE
None.

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallcmd_process_or_queue(const qipcall_msg_type* qipcall_msg);


/*===========================================================================
FUNCTION  qipcallcmd_try_next_async

DESCRIPTION
This function queues a callback to trigger the next qipcall cmd

DEPENDENCIES
None

RETURN VALUE
None.

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallcmd_try_next_async(QPVOID);

/*=========================================================================*/
/**
*   Description: 
*   
*   @param  None
*   @return None
*	
*/

/*===========================================================================
FUNCTION  qipcallcmd_clear_stale_commands_from_queue

DESCRIPTION
API used to indicate command complete from qipcallh to qipcall_cmd

DEPENDENCIES
None

RETURN VALUE
None.

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcallcmd_clear_stale_commands_from_queue(QP_CALL_ID call_id);
 /*===========================================================================
FUNCTION  qipcallcmd_process_linked_commands_from_queue

DESCRIPTION
API to process the second command linked with HOLD

DEPENDENCIES
None

RETURN VALUE
None.

SIDE EFFECTS
None
===========================================================================*/
 QPVOID qipcallcmd_process_linked_commands_from_queue(QP_CALL_ID call_id);


#ifdef __cplusplus
}
#endif

#endif /* QIPCALL_UI_CMD_MGR */
