#ifndef QIPCALLIFACEHOMGR_H
#define QIPCALLIFACEHOMGR_H
/*===========================================================================

Q I P C A L L I F A C E
H E A D E R   F I L E

DESCRIPTION
This file contains the definition of the public interfaces that are needed
for the QIPCALL interface.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/

/*===========================================================================

EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/qipcall/inc/qipcalliface_ho_mgr.h#1 $
$DateTime: 2016/03/28 23:03:22 $
$Author: mplcsds1 $
$Change: 10156183 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/30/15   hs     FR 28487: CR 844257 Adding VzW VoWiFi support 

===========================================================================*/

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/

#include "ims_variation.h"
#include "customer.h"
#include "nv.h"     /* for NV access */
#include "qipcall.h"
#include "qipcallh.h"
#include "rex.h"
#ifdef FEATURE_VOIP
#include "qvp_app_oa_api.h"
#endif  /*FEATURE_VOIP*/


/*===========================================================================

                      ENUM DECLARATIONS

===========================================================================*/

/*===========================================================================

                      FUNCTION DECLARATIONS

===========================================================================*/



QPBOOL qipcalliface_is_voip_service_enabled(QPVOID);
QPBOOL qipcalliface_is_qualified_voip_rat(QPUINT32 pref_rat);

/*==========================================================================
  FUNCTION qipcalliface_initiate_handover

  DESCRIPTION
  Initiates the Inter-rat Handover

  DEPENDENCIES
  None

  PARAMETERS
  pref_sys: The preferred Rat to handover to 

  RETURN VALUE
  QP_TRUE: Handover requested to preferred system
  QP_FALSE: Handover can't be requested to preferred system

  SIDE EFFECTS
  None
  ===========================================================================*/

  QPBOOL qipcalliface_set_APN_pref_sys(QPDCM_HO_INFO* pref_sys);

/*===========================================================================
FUNCTION qipcalliface_cleanup_handover_context

DESCRIPTION
	This method is called to reset the the Handover state
DEPENDENCIES
  None

PARAMETERS
  Sets the pref APN

RETURN VALUE
  current handover state

SIDE EFFECTS
  None
===========================================================================*/
QPVOID qipcalliface_cleanup_handover_context(QPE_DCM_APN_PREF_SYS pref_apn);
/*==========================================================================
  FUNCTION qipcalliface_get_handover_state

  DESCRIPTION
  Gets the Handover state

  DEPENDENCIES
  None

  PARAMETERS
  None

  RETURN VALUE
  QIPCALL_STATE_HANDOVER_IDLE           No Handover in progress. Stable state
  QIPCALL_STATE_HANDOVER_IN_PROGRESS    Handover in progress awaiting completion. Transition state
  QIPCALL_STATE_HANDOVER_INVALID        Handover Invalid state

  SIDE EFFECTS
  None
  ===========================================================================*/
qipcalliface_irat_handover_state_type qipcalliface_get_handover_state(QPVOID);

/*==========================================================================
  FUNCTION qipcalliface_set_handover_state

  DESCRIPTION
  Sets the Handover state

  DEPENDENCIES
  None

  PARAMETERS
  state: The new handover state

  RETURN VALUE
  QP_TRUE: Handover state set successfully
  QP_FALSE: Handover state set operation failed

  SIDE EFFECTS
  None
  ===========================================================================*/
QPBOOL qipcalliface_set_handover_state(qipcalliface_irat_handover_state_type  state);


/*=========================================================================*/
/**
*   Description: Returns the preferrred RAT as per priority order.
*                  1. LTE  2. UMTS  3. GSM 4. WiFi
*   @param  LTE
*   @return QP_TRUE or QP_FALSE
*	
*/
QPUINT32 qipcalliface_get_preferred_rat_for_ims_pdn(qimfif_service_id_mask_e_type service_mask);

/*===========================================================================
FUNCTION qipcalliface_is_wwan_iwlan_handover_required

DESCRIPTION
Evaluates if Handover required between WWAN (LTE,WCDMA) and IWLAN


DEPENDENCIES
None

PARAMETERS
curr_srv_status currently available services. Also indicate which network IFACE is currently attached to
HO_RAT OUT parameter. When ret is true, its set to the preferred system 

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPBOOL qipcalliface_is_wwan_iwlan_handover_required(qipcall_srv_status_param  curr_srv_status,
                                                    QPDCM_HO_INFO *handover_sys);
/*===========================================================================
FUNCTION qipcalliface_ho_mgr_is_handover_required_if_vowifi_off

DESCRIPTION
Check if Handover to WLAN/WWAN is required 
Needs to be checked at:
WLAN lost
WLAN available
Call established
call end
Upgrade successful


DEPENDENCIES
None

PARAMETERS
curr_srv_status currently available services. Also indicate which network IFACE is currently attached to
HO_RAT OUT parameter. When ret is true, its set to the handover network 

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPBOOL qipcalliface_ho_mgr_is_handover_required_if_vowifi_off(qipcall_srv_status_param  curr_srv_status,
                                                          QPDCM_HO_INFO *HO_RAT);
/*=========================================================================*/
/**
*   Description: This function performs generic IMS PDN management based of different parameters
*
*   @param  
*   @return PDN activity
*	
*/
qipcall_sys_pdn_activity qipcalliface_perform_handover_management( const qipcall_iface_e_type   iface);

/*===========================================================================
FUNCTION qipcalliface_get_pref_system

DESCRIPTION
Checks to get best preferred system


DEPENDENCIES
None

PARAMETERS
curr_srv_status currently available services. Also indicate which network IFACE is preferred
for IMS PDN. 

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
 QPBOOL qipcalliface_get_pref_system(qipcall_srv_status_param  curr_srv_status,
                                          QPDCM_HO_INFO *HO_RAT, QPUINT32 *handover_rat);

 /*===========================================================================
FUNCTION qipcallh_is_deregistration_required

DESCRIPTION
Evaluates if de-registration is required


DEPENDENCIES
None

PARAMETERS
curr_srv_status currently available services. Also indicate which network IFACE is currently attached to
HO_RAT OUT parameter. When ret is true, its set to the preferred system 

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
 QPBOOL qipcallface_is_deregistration_required(qipcall_srv_status_param  curr_srv_status,
                                                      QPDCM_HO_INFO *handover_sys);

 QPVOID qipcalliface_register_E911_Mode();

/*===========================================================================
FUNCTION qipcalliface_update_E911_capability

DESCRIPTION
This function would update E911 capabilities for TMO

DEPENDENCIES
None

PARAMETERS
srv_status
sys_mode
initRequired

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/ 
 QPVOID qipcalliface_update_E911_capability(qipcall_srv_status_param* srv_status, 
											QPE_SYS_MODE_MASK sys_mode, 
											boolean initRequired, 
											qipcall_iface_e_type   iface
);
/*===========================================================================
FUNCTION qipcalliface_set_wwan_iwlan_handover_measurement_params

DESCRIPTION
This function would set the parameters for starting the measurements for LTE and WiFi

DEPENDENCIES
None

PARAMETERS
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
 QPVOID qipcalliface_set_wwan_iwlan_handover_measurement_params(QPVOID);

/*===========================================================================
FUNCTION qipcalliface_is_voice_registered

DESCRIPTION
This function would set the parameters for starting the measurements for LTE and WiFi

DEPENDENCIES
None

PARAMETERS
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPBOOL qipcalliface_is_voice_registered(QPVOID);

/*===========================================================================
FUNCTION qipcalliface_is_wifi_quality_good_for_voip_call

DESCRIPTION
This function evaluates if wifi quality is good enough for VOIP calls

DEPENDENCIES
None

PARAMETERS
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPBOOL qipcalliface_is_wifi_quality_good_for_voip_call(QPVOID);

/*===========================================================================
FUNCTION qipcalliface_manage_incoming_call_counter

DESCRIPTION
This function manages the call counter and increases it by one for every MT call if 
IWLAN measurements criteria suggests to do a CS repoint for the calls

DEPENDENCIES
None

PARAMETERS
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcalliface_manage_incoming_call_counter(QPVOID);

/*===========================================================================
FUNCTION qipcalliface_reset_incoming_call_counter

DESCRIPTION
This function resets the incoming call counter upon De-reg or PDN disconnect

DEPENDENCIES
None

PARAMETERS
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcalliface_reset_incoming_call_counter(QPVOID);

/*===========================================================================
FUNCTION   qipcalliface_handle_ims_pdn_cleanup

DESCRIPTION
This function performs clean up of IMS PDN context. 

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcalliface_handle_ims_pdn_cleanup(const qipcall_iface_e_type   iface);

/*===========================================================================
FUNCTION qipcalliface_get_preferred_ims_rat_and_required_feature_tags

DESCRIPTION
Retrieves the preferred system RAT for FTs and provides FTs to be registered on it. 


DEPENDENCIES
None

PARAMETERS
1. OUT. When ret value is TRUE then it provides preferred RAT 
2. OUT. When ret value is TRUE then it provides FTs which needs to be registered on preferred RAT. 

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPBOOL qipcalliface_get_preferred_ims_rat_and_required_feature_tags( QPUINT32 *ims_srv_pref_rat, 
                                                                    qimfif_enabled_feature_tag_list *feature_tags);

/*=========================================================================
FUNCTION qipcalliface_is_ims_pdn_existing_on_iwlan

DESCRIPTION
Checks if IMS PDN is on IWLAN.
 
   @param  None

	
RETURN VALUE
QP_TRUE or QP_FALSE

SIDE EFFECTS
None
========================================================================*/
QPBOOL qipcalliface_is_ims_pdn_existing_on_iwlan(QPVOID);

/*=========================================================================
FUNCTION qipcalliface_is_ims_pdn_existing_on_wwan

DESCRIPTION
Checks if IMS PDN is on WWAN.
 
   @param  None
   
	
RETURN VALUE
QP_TRUE or QP_FALSE

SIDE EFFECTS
None
========================================================================*/
QPBOOL qipcalliface_is_ims_pdn_existing_on_wwan(qipcall_sys_srv_status_wwan_e_type *wwan_rat_type);
/*===========================================================================
FUNCTION qipcalliface_is_wwan_iwlan_handover_supported

DESCRIPTION
This function checls if WWAN IWLAN handover is supported

DEPENDENCIES
None

PARAMETERS
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPBOOL qipcalliface_is_wwan_iwlan_handover_supported();

QPVOID qipcalliface_process_handover_events(QPE_DCM_MSG dcmMsg, QPVOID* pMsgData);

QPVOID qipcalliface_log_handover_criteria(qipcall_sys_handover_state ho_state, qipcall_sys_wwan_iwlan_handover_direction ho_dir);
QPBOOL qipcalliface_get_pref_system_idle_mode(qipcall_srv_status_param  curr_srv_status,
                                              QPDCM_HO_INFO *handover_sys, QPUINT32 *handover_rat);

/*===========================================================================
FUNCTION qipcalliface_is_feature_tag_change_required

DESCRIPTION

It evaluates if Feature Tag change is required based on preferred RAT and
Feature tag which need to be registeted


DEPENDENCIES
None

PARAMETERS
1. OUT. When ret value is TRUE then it provides preferred RAT 
2. OUT. When ret value is TRUE then it provides FTs which needs to be registered on preferred RAT. 

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPBOOL qipcalliface_is_feature_tag_change_required(QPVOID);

/*===========================================================================
FUNCTION qipcalliface_is_voice_switch_reqd_from_iwlan_to_1x

DESCRIPTION
This function evaluates if voice service needs to swicth to 1x from WiFi

DEPENDENCIES
None

PARAMETERS
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPBOOL qipcalliface_is_voice_switch_possible_from_iwlan_to_1x( qipcall_iface_struct_type  *iface_ptr);

/*===========================================================================
FUNCTION qipcalliface_manage_epdg_1x_timer

DESCRIPTION
This function evaluates and starts epdg_1x timer once voice moves to WiFi. 

DEPENDENCIES
None

PARAMETERS
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcalliface_manage_epdg_1x_timer( qipcall_iface_struct_type  *iface_ptr);

/*===========================================================================
FUNCTION qipcalliface_is_ft_change_possible_on_iwlan

DESCRIPTION
This function evaluates if FT change required on iwlan for VzW

DEPENDENCIES
None

PARAMETERS
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPBOOL qipcalliface_is_ft_change_possible_on_iwlan( qimfif_enabled_feature_tag_list *tags);

/*===========================================================================
FUNCTION qipcalliface_get_ims_services_allowed

DESCRIPTION
Queries Policy Manager for services enabled on RATs such as IWLAN, LTE


DEPENDENCIES
None

PARAMETERS
eHORAT: Input RAT

RETURN VALUE
Service Mask on given RAT

SIDE EFFECTS
None
===========================================================================*/
qimfif_service_id_mask_e_type qipcalliface_get_ims_services_allowed(QPUINT32 eHORat);

//timer related
QPVOID qipcalliface_ho_epdg_lte_timer_start( QPUINT32 timer_value );
QPVOID qipcalliface_ho_epdg_lte_timer_stop( QPVOID );
QPVOID qipcalliface_ho_epdg_lte_timer_expiry_handler( QPVOID );
QPBOOL qipcalliface_ho_epdg_lte_timer_is_running( QPVOID );

QPVOID qipcalliface_ho_epdg_iwlan_timer_start( QPUINT32 timer_value);
QPVOID qipcalliface_ho_epdg_iwlan_timer_stop( QPVOID );
QPVOID qipcalliface_ho_epdg_iwlan_timer_expiry_handler( QPVOID );
QPBOOL qipcalliface_ho_epdg_iwlan_timer_is_running( QPVOID );

QPVOID qipcalliface_ho_epdg_1x_timer_start( QPUINT32 timer_value );
QPVOID qipcalliface_ho_epdg_1x_timer_stop( QPVOID );
QPVOID qipcalliface_ho_epdg_1x_timer_expiry_handler( QPVOID );
QPBOOL qipcalliface_ho_epdg_1x_timer_is_running( QPVOID );
QPVOID qipcalliface_stop_all_epdg_handoff_timers( QPVOID );

#endif /* QIPCALLIFACEHOMGR_H */

