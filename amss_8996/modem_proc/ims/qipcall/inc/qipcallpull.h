#ifndef QIPCALLPULL_H
#define QIPCALLPULL_H
/*===========================================================================

Q I P C A L L    DTMF


DESCRIPTION
This file contains the implementation of DTMF related functions

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2006 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/

/*===========================================================================

EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/qipcall/inc/qipcallpull.h#1 $
$DateTime: 2016/03/28 23:03:22 $
$Author: 
$Change: 10156183 $

when       who     what, where, why
--------   ---    ----------------------------------------------------------
09/03/15   lw     First Version
===========================================================================*/
#include <qipcallh.h>



/*==========================================================================

                          FUNCTION DECLARATIONS

===========================================================================*/

/*=========================================================================*/
/**
*   Description: Handles the 4xx/5xx/6xx response for subscribe, No op for 2xx
*
*   @param  None
*   @return None
*
*/
QPVOID qipcallpull_process_dialog_event_subscribe_response(qimfif_msg_type  *p_qimf_param);

/*=========================================================================*/
/**
*   Description: Handles the 4xx/5xx/6xx response for subscribe, No op for 2xx
*
*   @param  None
*   @return None
*
*/
QPVOID qipcallpull_dialog_event_subscribe_response(qimfif_msg_type  *p_qimf_param);

/*===========================================================================
FUNCTION   qipcallpull_add_dialog_to_list

DESCRIPTION
Adds new dialog to cached list

RETURN VALUE
None
===========================================================================*/
QPVOID qipcallpull_add_dialog_to_list(qipcall_vice_dialog* vice_dialog_data);

/*===========================================================================
FUNCTION   qipcallpull_reset_vice_dialogs

DESCRIPTION
Reset the complete cachced VICE dialogs in qipcall

RETURN VALUE
None

===========================================================================*/
QPVOID qipcallpull_reset_vice_dialogs(QPVOID);

/*===========================================================================
FUNCTION   qipcallpull_get_vice_dialog_by_dialog_id

DESCRIPTION
Get cached dialog by dialog id
Note: this dialog ID is a unique number from XML in dialog-info NOITY

PARAMETERS
QPCHAR* dialog ID

RETURN VALUE
QP_CALL_VICE_DIALOG pointer

SIDE EFFECTS
None
===========================================================================*/
qipcall_vice_dialog* qipcallpull_get_vice_dialog_by_dialog_id(QPCHAR* dialog_id);

/*===========================================================================
FUNCTION   qipcallpull_get_vice_dialog_by_remote_uri

DESCRIPTION
Get cached dialog by remote URI

PARAMETERS
QPCHAR* remote URI

RETURN VALUE
QP_CALL_VICE_DIALOG pointer

SIDE EFFECTS
None
===========================================================================*/
qipcall_vice_dialog* qipcallpull_get_vice_dialog_by_remote_uri(QPCHAR* remote_uri);

/*===========================================================================
FUNCTION   qipcallpull_setup_session_cfg

DESCRIPTION
Construct Replaces header for call pulling

PARAMETERS
qipcallh_obj_type     call ptr to be set with error code
qimfif_sip_hdrs_type
QP_CALL_NUM           the number from upper layer

RETURN VALUE
QP_TRUE   if Replaces header is correctly set
QP_FALSE  otherwise
===========================================================================*/
QPBOOL qipcallpull_setup_session_cfg(qipcallh_obj_type *call_ptr,
                                     qimfif_sip_hdrs_type *sip_session_cfg,
                                    QP_CALL_NUM call_number);
/*=========================================================================*/
/**
*   Description: Handle dialog-info VICE feature change
*
*   @param  eRegisteredServices enabled features
*   @param  app_id              App ID
*   @return None
===========================================================================*/									
QPVOID qipcallpull_handle_vice_capability_change(qimfif_service_id_mask_e_type  eRegisteredServices,
                                                 qimfif_app_id_type             app_id);

/*=========================================================================*/
/**
*   Description: Valiate the target URI with P-Associated-URI
*   P-Associated-URI comes from 200OK of Registration
*
*   @param  pEntity target URI
*   @return True if URI is among the P-Associated-URI list
*           False otherwise
*/
QPBOOL qipcallpull_validate_uri_against_p_associated_uri(const QPCHAR* pEntity);
#endif