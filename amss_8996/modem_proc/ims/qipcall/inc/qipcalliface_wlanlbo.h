#ifndef _QIPCALLH_IFACE_WLANLBO_H
#define _QIPCALLH_IFACE_WLANLBO_H
/*===========================================================================

Q I P C A L L   I F A C E W L A N L B O
H E A D E R  F I L E


DESCRIPTION
This file contains the implementation of wlan serv handling processed by QIPCALL
Application.

EXTERNALIZED FUNCTIONS
TBD

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2006 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/

/*===========================================================================

EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/qipcall/inc/qipcalliface_wlanlbo.h#1 $
$DateTime: 2016/03/28 23:03:22 $
$Author: 
$Change: 10156183 $

when       who     what, where, why
--------   ---    ----------------------------------------------------------
05/26/14   ma      CR :653864 Device is activate the IMS PDN over LTE during the active voice call in WIFI
9/14/13    ma      WLAN_LBO support:initial version of the file 
===========================================================================*/
#include <qpdpl.h>
QPVOID qipcalliface_wlanlbo_handle_srv_status_event( const qipcall_iface_e_type   iface,
                                             const qipcall_srv_status_param  srv_status );
QPVOID qipcalliface_wlanlbo_handle_service_change(qipcall_iface_e_type iface);
QPVOID qipcalliface_wlanlbo_update_domain(qipcall_iface_e_type iface);
QPVOID qipcalliface_wlanlbo_call_end_rpt_ind(qipcall_iface_e_type iface);
QPVOID qipcalliface_wlanlbo_try_qimf_register_app(qipcall_iface_e_type iface);
QPVOID qipcalliface_wlanlbo_dynamic_nvupdate_handler(QPVOID);
#endif