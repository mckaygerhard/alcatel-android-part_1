﻿/************************************************************************
Copyright (C) 2012 Qualcomm Technologies Incorporation .All Rights Reserved.

File Name      : qpGAAGBAAuth.h
Description    : GBA/GAA Utility class  

Revision History
========================================================================
Date      |   Author's Name    |  BugID  |        Change Description
========================================================================
20-05-2013     Swati                                  Initial Check-In
========================================================================
4-June-2013    ggrewal                                  Initial Check-In
------------------------------------------------------------------------
26-08-2013     Swati             530218               Password with trailing 0's should be copied by memcpy
------------------------------------------------------------------------
11-09-2013     Swati             542000               GBA Type should be determined from the DPL API
----------------------------------------------------------------------------
10-Dec-2013     Annapoorna             587489               Warning Fixes in Dime 3.0 
-------------------------------------------------------------------------------
23-Dec-2013  Swati            588637     Ks_NAF should be encoded as Base64 & IMPI should be taken from Realm
------------------------------------------------------------------------
07-01-2014     Swati             537800               GBA_U support
------------------------------------------------------------------------
24-01-2014     Swati                                 Ks_ext_NAF should be base64 encoded 
                                                     Proper card type should be passed to DPL
------------------------------------------------------------------------
10-01-2014     Swati              607788           Avoiding the situation of calling the NAF context simultaneously by
                                                    creating separate call back function for TMPI & Ks_ext_NAF calc.
------------------------------------------------------------------------
24-03-2014     Swati              636627           To validate mandatory params for response calculation.
------------------------------------------------------------------------
12-04-2014     Swati              659126           Handling use case when nonce repeats but URI/Method or other parama set by app
                                                   can change . Need to refresh the internal structures with these
                                                   new values .
------------------------------------------------------------------------
31-10-2014    Swati               747463          Supporting GBA o TLS
------------------------------------------------------------------------
07-11-2014    Swati               752387          Every Ua auth should calculate the Ks_NAF
-------------------------------------------------------------------------------------
08-05-2015    Swati              826267     Supporting Multiple Applications which use the same BSF URL
************************************************************************/

#ifndef __QPGAAGBAAuth_H__
#define __QPGAAGBAAuth_H__

/*----------------------------------------------------------------------------
Header Includes
----------------------------------------------------------------------------*/
#include<qpDigestAuth.h>
#include<qpDplIPSecAKA.h>
#include<qpdplsec.h>


typedef enum eGBA_UB_TYPE
{
  /** For the case where GBA is NULL */
  QC_GBA_NULL = 0,
  /** GBA_ME */
  QC_GBA_ME,
  /** GBA_U */
  QC_GBA_U,

}QPE_GBA_UB_TYPE;

typedef enum eGBA_AUTH
{
  /** default */
  QC_AKAV1 = 0,
  /** GBA_UB procedure */
  QC_GBA_UB,
  /** GBA_UA procedure */
  QC_GBA_UA,

}QPE_GBA_AUTH;

/**
* Function Pointer for the callback from GAA utility needed for AKA call back as it is asynchronous
* \var typedef QPVOID (*QP_GBA_AUTH_CALLBACK)(QPE_AUTH_MSG eAuthMsg,  
*                                          QPVOID* pUserData1, QPVOID* pUserData2);
*                                          
* QP_GBA_AUTH_CALLBACK is function pointer to the AUTH callback function.
* pUserData1 is the this pointer
* pUserData2 is the structure data , ownership lies with the Utility.
*/
typedef QPVOID (*QP_GBA_AUTH_CALLBACK)(QPE_AUTH_MSG eAuthMsg, QPVOID* pUserData1,const QPVOID* pUserData2);

/**
* \struct QpGBAAUTHUserData
* Callback parameter for AUTH APIs
*
* Structure holds UserData and a User Callback to be called by DPL for authentication operations.
*/
typedef struct QpGBAAUTHUserData
{

  QPVOID*                  pUserData;
  QP_GBA_AUTH_CALLBACK     GBAAUTHCallBack;

}QP_GBA_AUTH_USERDATA;


typedef struct QpChallengeParameters
{
  //Already defined in qpDigestAuth.h & this will be extended to have an AUTS parameter also.
  //This will be filled with necessary parameters to calculate the response.
  //TBD --> qpDigestAuth.cpp will be changed in future to use the DPL APIs directly instead of qpMd5.cpp

  DigestChallenge m_pDigestChallenge[QP_SIP_MAX_DIGEST_CLN_CNT];  

  QPE_GBA_AUTH m_eAuth;

  QPE_GBA_UB_TYPE m_eUbType;
  //Security Protocol Identifier
  QPUINT16 m_SecProtocolID;
}QP_GBA_CHALLENGE_PARAMS;    


class GAAGBAAuth
{
  //Here QPBOOL returns true if the structure was filled properly with all the necessary parameters.
  //ChallengeParameters has the structure filled by Application
  //QP_GBA_AUTH_USERDATA contains the function pointer & class reference needed for a asynchronous call to Application.
public :

  GAAGBAAuth();

  ~GAAGBAAuth();

  //Apps will have ownership of n_pAuthUserData & n_pDigestParameters
  QPBOOL qpCalculateChallengeResponse(const QP_GBA_AUTH_USERDATA *n_pAuthUserData ,const QP_GBA_CHALLENGE_PARAMS& n_pDigestParameters);

  //Apps will call this API to cancel any request.
  QPVOID qpGBACancelAuthenticationInfo(QP_GBA_AUTH_USERDATA* n_pAuthUserData);

  //API returns TMPI value calculated by the GBA Utility
  QPCHAR* qpGetTMPI();

  QPVOID qpResetTMPI();

  QPVOID qpSetIMPI(QPCHAR *n_pIMPI);

  //QPBOOL returns true if rspauth matches with the response calculated by GBA utility
  QPBOOL qpValidateRspAuth(const QP_GBA_CHALLENGE_PARAMS *n_pParams);

  //Callback API for processing the AKA response from DPL
  static QPVOID  qpImsProcessAuthCallBack(QPE_AUTH_MSG eAKAMsg, QP_AUTH_RESULT* pParam1, QPVOID* pUserData);

  //Callback API for processing the NAF context response from DPL
  static QPVOID  qpImsProcessNAFCallBackKs(QPE_AUTH_MSG eAKAMsg, QP_AUTH_RESULT* pParam1, QPVOID* pUserData);

  //Callback API for processing the NAF context response from DPL
  static QPVOID  qpImsProcessNAFCallBackTMPI(QPE_AUTH_MSG eAKAMsg, QP_AUTH_RESULT* pParam1, QPVOID* pUserData);
                                            
  //Callback API for to the CARD WRITE callback function. 
  static QPVOID qpImsCardWriteCallBack(QPE_WRITE_RESULT eWriteResult,QPVOID* pUserData);

  QPVOID qpUpdateStructureParam(QP_GBA_CHALLENGE_PARAMS *n_pParamNew,QP_SIP_DIGEST_AUTH_PARAM n_eName,QPCHAR *n_pValue);

  /**
  * Set the Bootstrapping parameters on to the UICC card 
  * \param Pointer to GBA parameter which has B-TID and Lifetime
  * \return QPBOOL - Success/Failure.
  * \remarks None.
  */

  QPBOOL qpGBASetBootStrappingParameters(QP_GBA_PARAM *n_pGBAParam);

  static QPCHAR* m_pTempKs;
  static QPUINT32 m_pTempKsLength;

  // To Store the RAND
  static QPUINT8* m_pPreviousRAND;

  static QPCHAR* m_pTMPI;

private:

  QPCHAR* GetCnonceStr();

  QPBOOL qpValidateStructureParam(const QP_GBA_CHALLENGE_PARAMS *n_pParam);

  QPVOID qpRefreshStructureParam(const QP_GBA_CHALLENGE_PARAMS *n_AppStruct,QP_GBA_CHALLENGE_PARAMS *n_toRefreshStruct);

  QPUINT8* qpGenerateSString(const QPCHAR* callerName, const QP_GBA_CHALLENGE_PARAMS* n_pDigestParameters, QPUINT32* n_sStringLen, const QPCHAR* n_P0);

  QPVOID qpGenerateKey(QP_AUTH_RESULT* pParam);

  QPBOOL  qpGBACopyStructure(QP_GBA_CHALLENGE_PARAMS *dest,const QP_GBA_CHALLENGE_PARAMS *source);

  QPBOOL  qpDeleteStructure(QP_GBA_CHALLENGE_PARAMS *n_pDel);

  QPBOOL  qpCheckIfNonceRepeated(const QP_GBA_CHALLENGE_PARAMS *n_pParamNew,QP_GBA_CHALLENGE_PARAMS *n_pParamOld);

  QPVOID  IncrementNcByOne(QPCHAR *n_pNc, QPCHAR *n_pNewNc);

  QPBOOL qpImsCalculateAkaResponse(QP_GBA_CHALLENGE_PARAMS* n_pDigestParameters);

  QPBOOL qpImsInitiateAKA(QP_GBA_CHALLENGE_PARAMS* n_pDigestParameters, QP_AUTH_USERDATA *n_pAuthUserData,QPE_AUTH_ALGO n_eAuthAlgo);

  QPCHAR* qpCalculateMD5Digest(QP_GBA_CHALLENGE_PARAMS* n_pDigestParameters,QPCHAR *n_pPassword,QPUINT32 n_pPwdLength,QPBOOL n_bFresh);

  QPVOID qpCalculateTMPI(QP_GBA_CHALLENGE_PARAMS* n_pDigestParameters);

  QPVOID qpCalculateKsNAF(QP_GBA_CHALLENGE_PARAMS* n_pDigestParameters);

  QPBOOL qpProcessAuthSuccess(QP_AUTH_RESULT* n_pParam);

  QPBOOL qpProcessAutsError(QP_AUTH_RESULT* n_pParam);

  QPVOID qpGetGBAUEnabled();

  QPVOID qpProcessCardWriteEvent(QPE_WRITE_RESULT eWriteResult);

  QPBOOL qpImsInitiateNAFContext(QPBOOL bNafData);

  QPVOID qpProcessNAFCallBack(QPE_AUTH_MSG eAKAMsg, QP_AUTH_RESULT* pParam,QPBOOL n_isTMPI);

  QPBOOL qpImsCalculateTMPIfromTEMP(QPUINT8 *n_pInput);

  QPBOOL qpPopulateIDFromRealm(QP_GBA_CHALLENGE_PARAMS *n_pDigestParameters);

  QPBOOL qpEncodeKeyToBase64(QPUINT8 *n_toEnc, QPUINT32 n_len);

private :
  QP_GBA_CHALLENGE_PARAMS *m_pUbParams;
  QP_GBA_CHALLENGE_PARAMS *m_pUaParams;
  QP_GBA_CHALLENGE_PARAMS *m_pAKAParams;
  QPBOOL m_bPlainAKAFlag;

  QPCHAR* m_pKsNAF;
  QPUINT16 m_pKsNAFLength;
  QPCHAR* m_pRES;
  QPUINT32 m_pRESLength;
  QPBOOL m_bFirstNCUa;

  QPUINT8* m_pBSFID;
  QPUINT16 m_pBSFIDLen;
  QPUINT8* m_pNAFID;
  QPUINT16 m_pNAFIDLen;
  QPCHAR*  m_pBTID;
  
  //Auth user data to store the call back values and to use while cancelling it.
  //This is between DPL & GBA Utility.
  QP_AUTH_USERDATA               *m_sDPLUserData;

  //Contains (this pointer & callback API) of the Application
  //Will be used while calling the callback API of the App
  QP_GBA_AUTH_USERDATA           *m_pAppUserData; 
  //Call back data of Application for the Card Write Event
  QP_CARD_WRITE_USERDATA         *m_pAppCardWriteData;
  //Flag to indicate RAND was repeated
  QPBOOL m_bRANDRepeated;

  // Cnone str val required during authorization
  QPUINT32         m_iPsCnonce;
  QPCHAR           strBuffer[50];
  QPCHAR*          m_IMPI;
  QPBOOL           m_bGetGBAUEnabled;
   
};


#endif //__QPGAAGBAAuth_H__

