/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

QPEVSPREVIEWFRAME . h

GENERAL DESCRIPTION

This file contains the header forqpEvsPreviewFrame.c.

EXTERNALIZED FUNCTIONS
None.

INITIALIZATION AND SEQUENCING REQUIREMENTS

Need to init and configure the profile before it becomes usable.

Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is
regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/
/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/dpl_media/src/qpEvsPreviewFrame.h#1 $ $DateTime: 2016/03/28 23:03:22 $ $Author: mplcsds1 $

EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
25/Jun/15   pradeepg    Initial Creation.   

===========================================================================*/
#ifndef QDJ_PREVIEW_FRAME_H
#define QDJ_PREVIEW_FRAME_H

#include "customer.h"
#include <assert.h>
//#include "stl.h"


/*----------------------------------------------------------------------------------*
 * All macro definitions
 *----------------------------------------------------------------------------------*/


#define ACELP_13k20                           13200     /* ACELP core layer at 13.20 kbps                                */
#define MAX_ACELP_SIG 194
#define NB                                    0         /* Indicator of 4 kHz bandwidth */
#define WB                                    1         /* Indicator of 8 kHz bandwidth */
#define SWB                                   2         /* Indicator of 14 kHz bandwidth */
#define FB                                    3         /* Indicator of 20 kHz bandwidth */
#define RF_FRAME_LEN                        264         /* length of the frame in CA mode */
/*----------------------------------------------------------------------------------*
 * Bitrates
 *----------------------------------------------------------------------------------*/

#define FRAME_NO_DATA                         0         /* Frame with no data */
#define SID_1k75                              1750      /* SID at 1.75 kbps               (used only in AMR-WB IO mode   */
#define SID_2k40                              2400      /* SID at 2.40 kbps                                              */
#define PPP_NELP_2k80                         2800      /* PPP and NELP at 2.80 kbps      (used only for SC-VBR)         */

#define ACELP_3k60                            3600      /* ACELP core layer at 3.60 kbps (used only for channel-aware mode) */
#define ACELP_3k70                            3700      /* ACELP core layer at 3.60 kbps (used only for channel-aware mode) */
#define ACELP_5k90                            5900      /* ACELP core layer at average bitrate of 5.90 kbps (used only in SC-VBR mode)      */
#define ACELP_6k60                            6600      /* ACELP core layer at 6.60  kbps (used only in AMR-WB IO mode)  */
#define ACELP_7k20                            7200      /* ACELP core layer at 7.20  kbps                                */
#define ACELP_8k00                            8000      /* ACELP core layer at 8     kbps                                */
#define ACELP_8k85                            8850      /* ACELP core layer at 8.85  kbps (used only in AMR-WB IO mode)  */
#define ACELP_9k25                            9250      /* ACELP core layer at 9.25  kbps (used for WB BWE)              */
#define ACELP_9k60                            9600      /* ACELP core layer at 9.60  kbps                                */
#define ACELP_11k60                           11600     /* ACELP core layer at 11.60 kbps (used for SWB TBE)             */
#define ACELP_12k15                           12150     /* ACELP core layer at 12.20 kbps                                */
#define ACELP_12k65                           12650     /* ACELP core layer at 12.65 kbps (used only in AMR-WB IO mode)  */
#define ACELP_12k85                           12850     /* ACELP core layer at 12.85 kbps (used for WB BWE)              */
#define ACELP_13k20                           13200     /* ACELP core layer at 13.20 kbps                                */
#define ACELP_14k25                           14250     /* ACELP core layer at 14.25 kbps (used only in AMR-WB IO mode)  */
#define ACELP_14k80                           14800     /* ACELP core layer at 14.80 kbps (used for SWB TBE )            */
#define ACELP_15k85                           15850     /* ACELP core layer at 15.85 kbps (used only in AMR-WB IO mode)  */
#define ACELP_16k40                           16400     /* ACELP core layer at 16.40 kbps                                */
#define ACELP_18k25                           18250     /* ACELP core layer at 18.25 kbps (used only in AMR-WB IO mode)  */
#define ACELP_19k85                           19850     /* ACELP core layer at 19.85 kbps (used only in AMR-WB IO mode)  */
#define ACELP_22k60                           22600     /* ACELP core layer at 22.60 kbps (used for FB + SWB TBE)        */
#define ACELP_22k80                           22800     /* ACELP core layer at 22.80 kbps (used for SWB TBE)             */
#define ACELP_23k05                           23050     /* ACELP core layer at 23.05 kbps (used only in AMR-WB IO mode)  */
#define ACELP_23k85                           23850     /* ACELP core layer at 23.85 kbps (used only in AMR-WB IO mode)  */
#define ACELP_24k40                           24400     /* ACELP core layer at 24.40 kbps                                */
#define ACELP_29k00                           29000     /* ACELP core layer at 29.00 kbps (used for FB + SWB TBE)        */
#define ACELP_29k20                           29200     /* ACELP core layer at 29.20 kbps (used for SWB TBE)             */
#define ACELP_30k20                           30200     /* ACELP core layer at 30.20 kbps (used for FB + SWB BWE)        */
#define ACELP_30k40                           30400     /* ACELP core layer at 30.40 kbps (used for SWB BWE)             */
#define ACELP_32k                             32000     /* ACELP core layer at 32    kbps                                */
#define ACELP_48k                             48000     /* ACELP core layer at 48    kbps                                */
#define ACELP_64k                             64000     /* ACELP core layer at 64    kbps                               */  

/* Coder Types */

#define INACTIVE                              0         /* inactive      */
#define UNVOICED                              1         /* unvoiced      */
#define VOICED                                2         /* purely voiced */
#define GENERIC                               3         /* generic       */
#define TRANSITION                            4         /* transition    */
#define AUDIO                                 5         /* audio (GSC)   */
#define LR_MDCT                               6         /* low-rate MDCT core */

#define MAX_BITS_PER_FRAME                  2560
#define G192_BIT0                           (int16)0x007F
#define G192_BIT1                           (int16)0x0081

#define MAX_16 (int16)0x7fff
#define MIN_16 (int16)0x8000

/*----------------------------------------------------------------------------------*
 * Function definitions
 *----------------------------------------------------------------------------------*/


void evs_dec_previewFrame_QDJ(
                 uint8 *bitstream,
                 int16 bitstreamSize,
                 int16 *partialCopyFrameType,
                 int16 *partialCopyOffset,
                 int16 *rf_flag
  );

uint16 get_indice_preview_QDJ(
                                int16 *bitstream,
                                int16 bitstreamSize,
                                int16 pos,
                                int16 nb_bits
  );
int16 qpSub (int16 var1, int16 var2);    /* Short sub,           1   */

#endif 