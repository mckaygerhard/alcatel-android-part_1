/* $Author: mplcsds1 $ */
/* $DateTime: 2016/03/28 23:03:22 $ */
/* $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/dpl_media/src/qpVideo.c#1 $ */


/*****************************************************************************
Copyright (c) 2004-2006 Qualphone, Inc. All rights reserved.

File: qpVideo.c
Description: Dpl implementation of video module

Revision History
===============================================================================
Date    |   Author's Name    |  BugID  |        Change Description
===============================================================================
*******************************************************************************************/


/*-------------------------------------------------------------------
Video implementation uses one single "object" for both player and
recorder and keep track of what was initialized. This is required
bacause only one ICodec object can be instantiated and thus this
is instantiated always using TxRx capabilities.

All callbacks are sent to the core asyncronously: when a callback
is received, dpl posts a message to itself (using ISHELL_Resume)
and then invokes the callback from the Applet context. The only
exception to this is when an encoded frame is ready: in this case
the callback is invoked directly to avoid an additional copy of 
the memory.

Currenlty video module only supports QCIF size. Preview is always
available in QCIF size and then scaled before blitting to screen.

A dummy implementation is available using the QP_DUMMY_VIDEO macro.
This is required to avoid the ICodec interface and run the video
module also on the Simulator. In the dummy implementation, some
coloured boxes are displayed as preview and as decoded frame.

It is possible to rotate preview and encoded frame by 180 degrees
using the QP_VIDEO_ROTATE_180 define. This is required because the
camera is mounted upside down on the FFA.

-------------------------------------------------------------------*/

//#ifndef FEATURE_IMS_NO_VIDEO

#include "ims_variation.h"
#include <qpdpl.h>
#include <qpVideo.h>
#include <qplog.h>
#include <qpVideoCommon.h>
#include <qpdpl_qmi_handler.h> 
#include <qpdpl_qmi_defs.h>
#include <qpdplCommonMisc.h>
#include <qpnet.h>


// Functions for the Simulator

#define QPVIDEO_DATA_PORT 9096
#define QPVIDEO_IP_ADDR_MAX 40

#define QPVIDEO_MAX_TX_BUFF_SZ 2500
#define QPVIDEO_MAX_RX_BUFF_SZ 2500

static QPCHAR gNwTxBuff[QPVIDEO_MAX_TX_BUFF_SZ];
static QPCHAR gNwRxBuff[QPVIDEO_MAX_RX_BUFF_SZ];

QPC_GLOBAL_DATA* global_GlobalData = QP_NULL;


typedef struct spsppsInfo_s {
  QPCHAR data[22];
  QPUINT8 length;
  QPUINT width;
  QPUINT height;
  QPE_VIDEO_H264_LEVEL profilelevel;
}spsppsinfo;

#define SPS_PPS_PREAMBLE_LEN 4
spsppsinfo spsppsdata[] = { 
  {{0,0,0,1,103,66,192,10,233,5,137,200,0,0,0,1,104,206,6,226},
  20,
  176,
  144,
  VIDEO_AVC_LEVEL1
  },
  {{0,0,0,1,103,66,208,11,233,5,137,200,0,0,0,1,104,206,6,226},
  20,
  176,
  144,
  VIDEO_AVC_LEVEL1B
  },
  {{0,0,0,1,103,66,192,11,233,5,137,200,0,0,0,1,104,206,6,226},
  20,
  176,
  144,
  VIDEO_AVC_LEVEL11
  },
  {{0,0,0,1,103,66,192,12,233,5,137,200,0,0,0,1,104,206,6,226},
  20,
  176,
  144,
  VIDEO_AVC_LEVEL12
  },
  {{0,0,0,1,103,66,192,13,233,5,137,200,0,0,0,1,104,206,6,226},
  20,
  176,
  144,
  VIDEO_AVC_LEVEL13
  },
  {{0,0,0,1,103,66,192,11,233,2,131,242,0,0,0,1,104,206,6,226},
  20,
  320,
  240,
  VIDEO_AVC_LEVEL11
  },
  {{0,0,0,1,103,66,192,11,233,2,193,44,128,0,0,0,1,104,206,6,226},
  21,
  352,
  288,
  VIDEO_AVC_LEVEL11
  },
  {{0,0,0,1,103,66,192,12,233,2,131,242,0,0,0,1,104,206,6,226},
  20,
  320,
  240,
  VIDEO_AVC_LEVEL12
  },
  {{0,0,0,1,103,66,192,12,233,2,193,44,128,0,0,0,1,104,206,6,226},
  21,
  352,
  288,
  VIDEO_AVC_LEVEL12
  },
  {{0,0,0,1,103,66,192,13,233,2,131,242,0,0,0,1,104,206,6,226},
  20,
  320,
  240,
  VIDEO_AVC_LEVEL13
  },
  {{0,0,0,1,103,66,192,13,233,2,193,44,128,0,0,0,1,104,206,6,226},
  21,
  352,
  288,
  VIDEO_AVC_LEVEL13
  }
};


typedef struct stVideoFrameInfo
{
  QPUINT8  iMarkerBit;
  QPUINT16 iSeqNum;
  QPUINT32 iTimestamp;
  QPUINT16 iLen;
  QPUINT8* pFrame;
}qpVideoFrameInfo;

typedef struct stVideoData
{
  QPC_GLOBAL_DATA*            pGlobalData;
  QPUINT32                    iRecordDescriptor;
  QPUINT32                    iPlayDescriptor;
  // Object state/configuration
  eVideoState            iRecorderState;
  eVideoState            iPlayerState;

  QP_MULTIMEDIA_FRAME         iVolFrame;

  // Frame counters
  QPUINT32                    iPlayerFrameCount;
  QPUINT32                    iRecorderFrameCount;

  // User parameters
  int                         iVideoDevice;
  QP_VIDEO_NAL_HDR_CALLBACK   pNalHdrCallback;
  QP_VIDEO_CALLBACK           pRecorderCallback;
  QP_VIDEO_CALLBACK           pPlayerCallback;
  QPVOID*                     pRecorderUserData;
  QPVOID*                     pPlayerUserData;

  QPUINT32                    iBaseTimeStamp;
  QPUINT32                    iTimeStamp;
  QPUINT32                    iClockRate;
  QP_DPL_QMI_PROFILE*         pQProfile;
  QPUINT8                     iIsQmiResRecvd;
  QPVOID*                     pNalHdrUserData;
  QPNET_CONN_PROFILE*         pNetConnProfile;
  QPBOOL                      bDataInitDone;
  QPE_IPADDR_TYPE             eIPType;
  QPCHAR                      LocalAddr[QPVIDEO_IP_ADDR_MAX];
  QPE_IPADDR_TYPE             eRemoteIPType;
  QPCHAR                      PeerAddr[QPVIDEO_IP_ADDR_MAX];
  QPUINT16                    iPeerPort;
} qpVideoData;

//static QPUINT8 nal_info[][] = { {},};

#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE
static ims_qmi_msg_info_u_type sQmiMsg;
#endif
void MediaInitRetryTCallback(unsigned long arg);


/**
* \fn QP_IMPORT_C QPBOOL IsRecorderActive(void* clientData);
*
* \brief Internally used to check if the recoder is active.
*
* This function will return TRUE if recorder active else returns FALSE.
*
* \param clientData Video Data.\n
* See \ref qpVideoData.
*
* \return \ref QPBOOL
* \retval QP_TRUE
* \retval QP_FALSE
*/
QPBOOL IsRecorderActive(void* clientData);

QPBOOL qpVideoDataInit(qpVideoData* n_pVideoData);
QPBOOL qpVideoDataDeInit(qpVideoData* n_pVideoData);
QPBOOL qpVideoDataSendRXFrame(qpVideoData* n_pVideoData, qpVideoFrameInfo* pVideoFrameInfo);

QPVOID qpVideoQmiHandler(QCE_QMI_MESSAGE_IDS eQmiMsgID, QPVOID* pMsgDataStruct, QPVOID* pUsedData);

// Base64 alphabet
static const QPCHAR basis64[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
QPVOID Base64DecodeInit( const QPCHAR *ca, QPINT ia[256] )
{
  /*lint --e{713}  suppress s */
  QPINT i, len = qpDplStrlen(ca);
  qpDplMemset(ia, -1, 256);

  for (i = 0; ((i < len) && (i < 256)); i++)
  {
    uint8 temp = (uint8)ca[i];
    ia[temp] = i;
  }
  ia['='] = 0;
}

void printFrames(char* frame,QPINT length)
{

  if(length > 10)
  {
    IMS_MSG_LOW_1(LOG_IMS_DPL,"FRAME::length = %d",length);
    IMS_MSG_LOW_3(LOG_IMS_DPL,"First 3 frames %x %x %x ",frame[0],frame[1],frame[2]);
    IMS_MSG_LOW_3(LOG_IMS_DPL,"Last 3 frames %x %x %x",frame[length-3],frame[length-2],frame[length-1]);
  }

}

QPINT32 DecodeBase64(const QPCHAR *cpcIn, QPCHAR *pcOut)
{
  QPINT pSize;
  QPINT delimCnt, len, pad, i, s, d, j;
  QPINT IA[256];
  const QPCHAR * charMap = basis64;
  // check parameters)   
  if ( !cpcIn || !pcOut )
    return 0;   
  /*lint --e{713}  suppress s */
  pSize = qpDplStrlen(cpcIn);
  if ( pSize == 0 )
    return 0;

  Base64DecodeInit(charMap, IA);

  // Count illegal characters and linefeeds to determine the size of the returned.
  delimCnt = 0;
  for (i = 0; i < pSize; i++)
  {
    uint8 temp = (uint8)cpcIn[i];
    if (IA[temp] < 0)
      delimCnt++;
  }

  // RFC 2045 says that non-delimiters must occur in multiples of 4.
  if ((pSize - delimCnt) % 4 != 0)
    return 0;

  pad = 0;
  for (i = pSize; i > 1 && IA[(uint8)cpcIn[--i]] <= 0;)
  {
    if (cpcIn[i] == '=')
      pad++;
  }
  /*lint --e{702}  suppress s */
  len = (((pSize - delimCnt) * 6) >> 3) - pad;

  for (s = 0, d = 0; d < len;) {
    // Assemble three bytes into an int from four "valid" characters.
    i = 0;
    for (j = 0; j < 4; j++) {   // j only increased if a valid char was found.
      QPINT c = IA[(uint8)cpcIn[s++]];
      if (c >= 0)
      {
        /*lint --e{701}  suppress s */
        i |= c << (18 - j * 6);
      }
      else
        j--;
    }
    // Add the bytes
    pcOut[d++] = (QPUINT8) (i >> 16);
    if (d < len) {
      pcOut[d++]= (QPUINT8) (i >> 8);
      if (d < len)
        pcOut[d++] = (QPUINT8) i;
    }
  }

  return d;
}
QPCHAR* EncodeBase64(const QPCHAR *cpcIn, QPINT32 nInLen, QPCHAR *pcOut)
{   

  QPCHAR *pDest;
  QPCHAR *pSrc;
  QPUINT8 c1,c2,c3;
  QPINT32   nOutLen = ((nInLen + 2) / 3) * 4;

  if(pcOut == 0) {
    return 0;
  }

  /* move the data to the end */
  pSrc = pcOut + nOutLen - nInLen;
  qpDplMemscpy(pSrc,(size_t)nInLen, cpcIn, (size_t)nInLen);
  pDest = pcOut;

  while (nInLen) {

    // extract first char
    c1 = *pSrc++;
    *pDest++ = basis64[c1>>2];  // encode first char
    nInLen--;

    if (!nInLen){
      c2 = 0;
    } else {
      c2 = *pSrc++;  // second char
    }

    // encode second char
    *pDest++ = basis64[((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4)];

    // append '=' to make encoded string size is multiple of 4
    if (!nInLen){
      *pDest++ = '=';
      *pDest++ = '=';
      break;
    }

    nInLen--;

    if (!nInLen){
      c3 = 0;
    } else {
      c3 = *pSrc++;    // third char
    }

    // encode third char
    *pDest++ = basis64[((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6)];

    if (!nInLen){
      *pDest++ = '=';
      break;
    }
    --nInLen;

    // fourth char
    *pDest++ = basis64[c3 & 0x3F];
  }

  *pDest = '\0';

  return pcOut;
}


QPVOID qpVideoDataMsgCB(QPE_NET_MSG  eNetMsg,
                        QPNET_CONN_PROFILE*   pNetConnProfile,
                        QPVOID*               pMsgData)
{
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
  pGlobalData = qpDplGetGlobalData();

  if (pNetConnProfile == NULL || pMsgData == NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataMsgCB - Invalid Param");
    return;
  }

  if (pGlobalData == NULL || pGlobalData->pVideo == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataMsgCB - Global Data is NULL");
    return;
  }

  if (eNetMsg == NET_MSG_SOCK_DATA_READY)
  {
    QPE_NET_ERROR       eRetval = NET_ERROR_OK;
    QPNET_REMOTE_ADDR   sRemAddr;
    QPCHAR* pNwBuff = QP_NULL;
    QPUINT16 iFrameLen    =  0;
    QPUINT32 iTimeStamp = 0;
    QPUINT32 iWTimeStamp = 0;
    QPUINT32 iTimeStampIncrement = 0;
	QPBOOL bIs_free = FALSE;

    QPUINT16 iPktLen = *((QPUINT16*)pMsgData);
    QPUINT8 iOffset = 0;
    qpVideoData* pVideoData = (qpVideoData*)pGlobalData->pVideo;

    qpDplMemset(&sRemAddr, 0, sizeof(QPNET_REMOTE_ADDR));

    if (iPktLen == 0)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataMsgCB - Invalid Pkt Len");
      return;
    }

    if (iPktLen < QPVIDEO_MAX_RX_BUFF_SZ)
    {
      pNwBuff = &gNwRxBuff[0];
    }
    else
    {
      pNwBuff = (QPCHAR*)qpDplMalloc(MEM_IMS_DPL, (iPktLen * sizeof(QPCHAR)));
      if (pNwBuff == QP_NULL)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataMsgCB - Malloc Failed");
        return;
      }
	  bIs_free = TRUE;
    }

    eRetval = qpDplNetReceiveData(pNetConnProfile, pNwBuff, &iPktLen, &sRemAddr);
    if (eRetval != NET_ERROR_OK)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataMsgCB: Failed to Recv Video Data!");
      if (iPktLen >= QPVIDEO_MAX_RX_BUFF_SZ)
      {
        qpDplFree(MEM_IMS_DPL, pNwBuff);
		bIs_free = FALSE;
      }
	  if(bIs_free)
	  {
        qpDplFree(MEM_IMS_DPL, pNwBuff);
		bIs_free = FALSE;	    
      }
      return;
    }

    qpDplMemscpy(&iWTimeStamp,sizeof(QPUINT32), pNwBuff, sizeof(QPUINT32));
    iWTimeStamp = qpDplNetNtohl(iWTimeStamp);
    iOffset += sizeof(QPUINT32);

    qpDplMemscpy(&iFrameLen,sizeof(QPUINT16), (pNwBuff + iOffset), sizeof(QPUINT16));
    iFrameLen = qpDplNetNtohs(iFrameLen);
    iOffset += sizeof(QPUINT16);
    IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoDataMsgCB: iWTimeStamp = %d!", iWTimeStamp);

    IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoDataMsgCB: iFrameLen = %d!", iFrameLen);

    if ((QP_NULL != pVideoData->pRecorderCallback) && (pVideoData->iRecorderState == eVideoStateActive))
    {
      QP_MULTIMEDIA_FRAME iQpFrame;			
      qpDplMemset(&iQpFrame, 0, sizeof(iQpFrame));

      IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpVideoDataMsgCB: iDataLen - %d", iFrameLen);

      if (iFrameLen > 0)
      {
        //RTP Timestamp generation:
        iTimeStamp =  qpDplGetTimeInMilliSeconds();
        //Calculation of timestamp increment
        if (iTimeStamp > pVideoData->iBaseTimeStamp)
        {
          iTimeStampIncrement = (iTimeStamp - pVideoData->iBaseTimeStamp) * (pVideoData->iClockRate/1000) ;
        }
        else
        {
          iTimeStampIncrement = (iTimeStamp - pVideoData->iBaseTimeStamp + 0xFFFFFFFF) * (pVideoData->iClockRate/1000) ;
        }
        pVideoData->iTimeStamp += iTimeStampIncrement; //new
        //Setting the BaseTimeStamp to GetTimeinMS
        pVideoData->iBaseTimeStamp = iTimeStamp;  

        // Pack frame
        iQpFrame.pData = (QPUINT8 *)(pNwBuff + iOffset);
        iQpFrame.iDataLen = iFrameLen;
        iQpFrame.sMediaPacketInfo.sMediaPktInfoTx.iTimeStamp = pVideoData->iTimeStamp;
        iQpFrame.sMediaPacketInfo.sMediaPktInfoTx.iFrameWallClock = iWTimeStamp;
        IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoDataMsgCB - Received Frame");
        printFrames((char *)iQpFrame.pData,iQpFrame.iDataLen);

        // Send frame directly to core (to avoid additional copy)
        (*pVideoData->pRecorderCallback)(VIDEO_MSG_RECORDED_DATA, (void*)(&iQpFrame), 1, pVideoData->pRecorderUserData);
        pVideoData->iRecorderFrameCount++;
      }
      else
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataMsgCB - Invalid frame");
      }
    }
    else
    {
      IMS_MSG_ERR_2(LOG_IMS_DPL,"qpVideoDataMsgCB - Recorder CB is NULL: CB %x %d", pVideoData->pRecorderCallback, pVideoData->iRecorderState);
    }

    if (iPktLen > QPVIDEO_MAX_RX_BUFF_SZ)
    {
      qpDplFree(MEM_IMS_DPL, pNwBuff);
      pNwBuff = QP_NULL;
	  bIs_free = FALSE;
    }
      if(bIs_free)
	  {
        qpDplFree(MEM_IMS_DPL, pNwBuff);
		bIs_free = FALSE;	    
    }
  }
  else
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoDataMsgCB - Unhandled Net Msg: %d", eNetMsg);
  }
}


QPBOOL qpVideoDataInit(qpVideoData* n_pVideoData)
{
  if (n_pVideoData == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataInit - Invalid Param");
    return QP_FALSE;
  }

  if (n_pVideoData->bDataInitDone != QP_FALSE)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataInit - Already Inited");
    return QP_FALSE;
  }
  if (n_pVideoData->pNetConnProfile != QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataInit - NetConn Profile not NULL");
    return QP_FALSE;
  }

  n_pVideoData->pNetConnProfile = (QPNET_CONN_PROFILE*)qpDplMalloc(MEM_IMS_DPL,sizeof(QPNET_CONN_PROFILE));
  if (n_pVideoData->pNetConnProfile)
  {
    QPE_IPADDR_TYPE eIPType;

    //Init the NetConn Profile
    qpDplMemset(n_pVideoData->pNetConnProfile, 0, sizeof(QPNET_CONN_PROFILE));
    n_pVideoData->pNetConnProfile->eProfileType = NET_PROFILE_RTP;
    n_pVideoData->pNetConnProfile->eProtoInet = NET_INET_UDP;
    n_pVideoData->pNetConnProfile->eSockType = NET_SOCK_DGRAM;
    n_pVideoData->pNetConnProfile->iMaxRecvDataSize = 30000;
    n_pVideoData->pNetConnProfile->iNetworkHdl = (QPUINT32)0;
    n_pVideoData->pNetConnProfile->iPrivateInt = 0;
    //n_pVideoData->pNetConnProfile->pQosParams = QP_NULL;
    n_pVideoData->pNetConnProfile->pDplNetAppSockEvCB = qpVideoDataMsgCB;
    n_pVideoData->pNetConnProfile->iLocalPort = QPVIDEO_DATA_PORT;

    //Indicate to DPL Net that it is tethered socket and what IFACE it needs to bind on
    n_pVideoData->pNetConnProfile->eIfaceType = NET_IFACE_LINK_LOCAL;
    n_pVideoData->pNetConnProfile->iTetheredSocket = 1;
    n_pVideoData->pNetConnProfile->iIsRoutable = 1;

    if (qpDplNetOpen(n_pVideoData->pNetConnProfile) != NET_ERROR_OK)
    {
      IMS_MSG_MED_0(LOG_IMS_DPL,"qpVideoDataInit::Video Data Port open failed");
      return QP_FALSE;
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataInit: Video Data Socket Created");
    }
    n_pVideoData->LocalAddr[0] = '\0';

    eIPType = qpDplGetLocalHostByIFACE(n_pVideoData->pNetConnProfile, &n_pVideoData->LocalAddr[0], QPVIDEO_IP_ADDR_MAX);
    if (n_pVideoData->LocalAddr[0] == 0x0)
    {
      IMS_MSG_LOW_1_STR(LOG_IMS_DPL,"qpVideoDataInit:: Failed to get IP Address: %s", n_pVideoData->LocalAddr);
      return QP_FALSE;
    }

    n_pVideoData->eIPType = eIPType;
    IMS_MSG_LOW_1_STR(LOG_IMS_DPL,"qpVideoDataInit:: IP Address: %s", n_pVideoData->LocalAddr);
  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataInit - Malloc failed");
    return QP_FALSE;
  }

  return QP_TRUE;
}

QPBOOL qpVideoDataDeInit(qpVideoData* n_pVideoData)
{
  if (n_pVideoData == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataDeInit - Invalid Param");
    return QP_FALSE;
  }

  if (n_pVideoData->bDataInitDone != QP_TRUE)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataDeInit - Not Inited");
    return QP_FALSE;
  }
  if (n_pVideoData->pNetConnProfile == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataDeInit - NetConn Profile is NULL");
    return QP_FALSE;
  }

  if (qpDplNetClose(n_pVideoData->pNetConnProfile) != NET_ERROR_OK)
  {
    IMS_MSG_MED_0(LOG_IMS_DPL,"qpVideoDataInit:: Failed to close Video Data Port");
  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataInit: Video Data Socket Closed");
  }

  n_pVideoData->LocalAddr[0] = '\0';
  n_pVideoData->bDataInitDone = QP_FALSE;

  if (n_pVideoData->pNetConnProfile)
  {
    qpDplFree(MEM_IMS_DPL, n_pVideoData->pNetConnProfile);
    n_pVideoData->pNetConnProfile = QP_NULL;
  }

  return QP_TRUE;
}

QPBOOL qpVideoDataSendRXFrame(qpVideoData* n_pVideoData, qpVideoFrameInfo* pVideoFrameInfo)
{
  QPCHAR* pNwBuff = QP_NULL;
  QPUINT16 iNwLen = 0;
  QPUINT8 iFrameHdrLen = 0;
  QPUINT16 iFrameLen    =  0;
  QPUINT32 iNwTimeStamp = 0;
  QPUINT16 iNwSeqNum = 0;
  QPE_NET_ERROR eErr = NET_ERROR_OK;
  uint16 iLen = 0;

  IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoDataSendRXFrame - ENTRY");

  if (n_pVideoData == QP_NULL || pVideoFrameInfo == QP_NULL || pVideoFrameInfo->pFrame == QP_NULL || pVideoFrameInfo->iLen == 0)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataSendRXFrame - Invalid Param");
    return QP_FALSE;
  }

  if (n_pVideoData->bDataInitDone != QP_TRUE)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataSendRXFrame - Not Inited");
    return QP_FALSE;
  }
  if (n_pVideoData->pNetConnProfile == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataSendRXFrame - NetConn Profile is NULL");
    return QP_FALSE;
  }

  //TBD: Discuss with Shankar and Sameer how to handle memory for this? Via global buffer for TX and RX?
  //     We also need to see if we can use lazy func to avoid DPL doing malloc and copying this buffer.

  n_pVideoData->pNetConnProfile->iRemoteIpAddr= qpDplInetPton(&n_pVideoData->PeerAddr[0]);
  n_pVideoData->pNetConnProfile->iRemotePort = n_pVideoData->iPeerPort;


  //Check with vikram .. what is iLen here 
  iLen = pVideoFrameInfo->iLen;
  iFrameHdrLen = (sizeof(QPUINT8) + sizeof(QPUINT16) + sizeof(QPUINT32) + sizeof(QPUINT16));
  iNwLen = (iFrameHdrLen + iLen + 1);

  if (iLen < (QPVIDEO_MAX_TX_BUFF_SZ - iFrameHdrLen))
  {
    pNwBuff = &gNwTxBuff[0];
  }
  else
  {
    pNwBuff = (QPCHAR*)qpDplMalloc(MEM_IMS_DPL, (iNwLen * sizeof(QPCHAR)));
  }
  /**************************************************************************
  | Marker Bit (8 bit) | Sequence Number (16 bit) |
  |                TimeStamp (32 bit)                 |
  |  Frame Length (16 bit) | Frame                    |
  */

  if (pNwBuff)
  {
    QPUINT16 iOffset = 0;

    qpDplMemset(pNwBuff, 0, iNwLen);

    iFrameLen    =  qpDplNetHtons(pVideoFrameInfo->iLen);
    iNwTimeStamp = qpDplNetHtonl(pVideoFrameInfo->iTimestamp);
    iNwSeqNum    = qpDplNetHtons(pVideoFrameInfo->iSeqNum);

    IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoDataSendRXFrame - pVideoFrameInfo->iMarkerBit = %d", pVideoFrameInfo->iMarkerBit);
    IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoDataSendRXFrame - pVideoFrameInfo->iLen = %d", pVideoFrameInfo->iLen);
    IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoDataSendRXFrame - pVideoFrameInfo->iTimestamp = %ld", pVideoFrameInfo->iTimestamp);
    IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoDataSendRXFrame - pVideoFrameInfo->iSeqNum = %d", pVideoFrameInfo->iSeqNum);

    qpDplMemscpy(pNwBuff,iNwLen,&pVideoFrameInfo->iMarkerBit, sizeof(QPUINT8));
    iOffset += sizeof(QPUINT8);
    qpDplMemscpy((pNwBuff + iOffset),iNwLen-iOffset,&iNwSeqNum, sizeof(QPUINT16));
    iOffset += sizeof(QPUINT16);
    qpDplMemscpy((pNwBuff + iOffset),iNwLen-iOffset, &iNwTimeStamp, sizeof(QPUINT32));
    iOffset += sizeof(QPUINT32);
    qpDplMemscpy((pNwBuff + iOffset), iNwLen-iOffset, &iFrameLen, sizeof(QPUINT16));
    iOffset += sizeof(QPUINT16);
    qpDplMemscpy((pNwBuff + iOffset), iNwLen-iOffset, pVideoFrameInfo->pFrame, pVideoFrameInfo->iLen);

    IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoDataSendRXFrame - Send Frame");
    printFrames((char *)pVideoFrameInfo->pFrame,pVideoFrameInfo->iLen);

    iOffset += iLen;
    IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoDataSendRXFrame - qpDplNetSendData >");

    eErr = qpDplNetSendData(n_pVideoData->pNetConnProfile, pNwBuff, pVideoFrameInfo->iLen + 9);
    if (eErr != NET_ERROR_OK)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataSendRXFrame - Failed to send Video Frame to AP");
      if (iLen > (QPVIDEO_MAX_TX_BUFF_SZ - (sizeof(QPUINT32) + sizeof(QPUINT16))))
      {
        qpDplFree(MEM_IMS_DPL, pNwBuff);
      }
      pNwBuff = QP_NULL;
      return QP_FALSE;
    }
  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoDataSendRXFrame - Malloc failed");
    return QP_FALSE;
  }

  if (iLen > (QPVIDEO_MAX_TX_BUFF_SZ - iFrameHdrLen))
  {
    qpDplFree(MEM_IMS_DPL, pNwBuff);
    pNwBuff = QP_NULL;
  }
  IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoDataSendRXFrame - EXIT");

  return QP_TRUE;
}


QPE_VIDEO_ERROR qpVideoInitialize()
{
  #ifdef  FEATURE_IMS_QMI_VIDEO_SERVICE

  qpVideoData*     pVideoData  = QP_NULL;
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;

  pGlobalData = qpDplGetGlobalData();
  if (pGlobalData != NULL)
  {
    // A video object was already instantiated
    // In this case we are re-using the same video object and
    // there is no need to instantiate a new one
    if (pGlobalData->pVideo != NULL)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoInitialize - Media is already instantiated");
    }
    else
    {
      pVideoData = (qpVideoData*)qpDplMalloc(MEM_IMS_DPL,sizeof(qpVideoData));
      if (pVideoData)
      {
        qpDplMemset(pVideoData, 0, sizeof(qpVideoData));

        // Register with QMI module
        pVideoData->pQProfile = qpDplQmiProxyRegisterCallBack(QC_IMS_QMI_VT_MODULE, pVideoData, qpVideoQmiHandler);
        if (QP_NULL == pVideoData->pQProfile)
        {
          IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoInitialize - Registration with QMI failed");
          qpDplFree(MEM_IMS_DPL,pVideoData);
          pVideoData = QP_NULL;
          return VIDEO_ERROR_UNKNOWN;
        }

        pVideoData->bDataInitDone = QP_FALSE;
        pVideoData->iRecorderState = eVideoStateNotReady;
        pVideoData->iPlayerState   = eVideoStateNotReady;

        pVideoData->pGlobalData = pGlobalData;

        pGlobalData->pVideo = (QPVOID*)pVideoData;

      }
      else
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoInitialize - Malloc Failed");
        return VIDEO_ERROR_UNKNOWN;
      }
    }
  }
#endif
  return VIDEO_ERROR_OK;
}

QPE_VIDEO_ERROR qpVideoGenerateNalHeaderAsynch(QP_VIDEO_NAL_HDR_CONFIG* pNalHdrCfg, QP_VIDEO_NAL_HDR_CALLBACK pCB, QPVOID* nparam1, QPVOID* nparam2)
{
  qpVideoData*     pVideoData  = QP_NULL;
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
  QPE_VIDEO_ERROR status = VIDEO_ERROR_UNKNOWN;
  //  const QPCHAR* pNal = "Z0IACpZTBYmI,aMljiA==";
  //  uint16 iLen = strlen(pNal);
  QPCHAR pNal[255];

  if (QP_NULL == pNalHdrCfg || QP_NULL == pCB)
  {
    IMS_MSG_ERR_2(LOG_IMS_DPL,"qpVideoGenerateNalHeaderAsynch - Invalide Param: Cfg: %x, CB: %x", pNalHdrCfg, pCB);
    return VIDEO_ERROR_UNKNOWN;
  }

  pGlobalData = qpDplGetGlobalData();
  if (pGlobalData != NULL)
  {
    // A video object was already instantiated
    // In this case we are re-using the same video object and
    // there is no need to instantiate a new one
    if (pGlobalData->pVideo != NULL)
    {
      QPUINT8 i;
      QPINT numberofelements = sizeof(spsppsdata)/sizeof(spsppsdata[0]);
      pVideoData = (qpVideoData*)pGlobalData->pVideo;
      if(!pVideoData){
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoGenerateNalHeaderAsynch - pVideoData is null");
        return VIDEO_ERROR_UNKNOWN;
      }
      for(i=0;i<numberofelements;i++) {
        if((pNalHdrCfg->iWidth == spsppsdata[i].width) && (pNalHdrCfg->iHeight == spsppsdata[i].height) && (pNalHdrCfg->eLevel == spsppsdata[i].profilelevel))
        {
          QPCHAR sps[20],pps[20];
          int spsLen, ppsLen,iEncodedNalLen,index;

          /*get the sps data skip the first 4 characters "0001"*/
          for (index=4;index<spsppsdata[i].length;index++)
          {
            if(spsppsdata[i].data[index] == 0)
				break;
          }
          spsLen = index-SPS_PPS_PREAMBLE_LEN;
          qpDplStrlncpy(sps,&spsppsdata[i].data[SPS_PPS_PREAMBLE_LEN],20,spsLen);
          EncodeBase64((const QPCHAR *)sps,spsLen,&pNal[0]);
		  		
          /*insert the comma*/
          pNal[((spsLen + 2) / 3) * 4] = ',';
		
          ppsLen = spsppsdata[i].length-(spsLen + SPS_PPS_PREAMBLE_LEN + SPS_PPS_PREAMBLE_LEN); /*substract the sps len n 8 bytes of 0001*/
      
          qpDplStrlncpy(pps,&spsppsdata[i].data[index+SPS_PPS_PREAMBLE_LEN],20,ppsLen);
          EncodeBase64((const QPCHAR *)pps,ppsLen, &pNal[(((spsLen + 2) / 3) * 4)+1]);

          iEncodedNalLen = (((spsLen + 2) / 3) * 4) + (((ppsLen + 2) / 3) * 4) + 1;
	  
        //  EncodeBase64((const QPCHAR *)spsppsdata[i].data,spsppsdata[i].length,pNal);
          pVideoData->pNalHdrCallback = pCB;
          pVideoData->pNalHdrUserData = nparam1;
          pCB((QPCHAR*)pNal,iEncodedNalLen,nparam1, nparam2);
          status = VIDEO_ERROR_OK;
          break;
        }

      }

#if 0
      pVideoData->pNalHdrCallback = pCB;
      pVideoData->pNalHdrUserData = pUserData;
      //TBD: Sending Dummy NAL header to APP.
      pCB((QPCHAR*)pNal, iLen, pUserData); 
#endif

    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoGenerateNalHeaderAsynch - VideoData is NULL");
      return VIDEO_ERROR_UNKNOWN;
    }
  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoGenerateNalHeaderAsynch - GLOBAL Data is NULL");
    return VIDEO_ERROR_UNKNOWN;
  }

  return status;
}

QPE_VIDEO_ERROR qpVideoUnInitialize()
{
  qpVideoData*     pVideoData  = QP_NULL;
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;

  pGlobalData = qpDplGetGlobalData();
  if (pGlobalData != NULL)
  {
    if (pGlobalData->pVideo != NULL)
    {

      pVideoData = (qpVideoData*)pGlobalData->pVideo;

      if ((QP_NULL != pVideoData->pRecorderCallback) || (QP_NULL != pVideoData->pPlayerCallback))
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"qpVideoUnInitialize - Codec Resources still not released: RCB: %x  PCB: %x", pVideoData->pRecorderCallback, pVideoData->pPlayerCallback);
        return VIDEO_ERROR_UNKNOWN;
      }

      if (QP_NULL == pVideoData->pQProfile)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoUnInitialize - Registration with QMI Not Available");
      }
      else
      {
        qpDplQmiProxyDeRegisterCallBack(pVideoData->pQProfile);
        pVideoData->pQProfile = QP_NULL;
      }

      qpDplFree(MEM_IMS_DPL,pVideoData);

      if (pGlobalData)
      {
        pGlobalData->pVideo = QP_NULL;
      }
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoUnInitialize - VideoData is NULL");
      return VIDEO_ERROR_UNKNOWN;
    }
  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoUnInitialize - GLOBAL Data is NULL");
    return VIDEO_ERROR_UNKNOWN;
  }

  return VIDEO_ERROR_OK;
}

// Initialization
QP_VIDEO_DESC qpVideoCodecInitialize(QP_VIDEO_CALLBACK tVideoCallBack, QPVOID* pUserData,
                                     QPE_VIDEO_DEV eVDev, QP_VIDEO_CONFIG* pCodecConfig)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE
  qpVideoData*     pVideoData  = QP_NULL;
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
  QPUINT8          iDecodedNalLen = 0;
  QPCHAR sps[20],pps[20];
  int index;


  if(pCodecConfig == QP_NULL || tVideoCallBack == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoCodecInitialize - Invalid Param");
    return (QP_VIDEO_DESC)QP_NULL;
  }

  pGlobalData = qpDplGetGlobalData();
  if (pGlobalData != NULL)
  {
    // A video object was already instantiated
    // In this case we are re-using the same video object and
    // there is no need to instantiate a new one
    if (pGlobalData->pVideo != NULL)
    {
      QPBOOL res = QP_FALSE;
      pVideoData = (qpVideoData*)pGlobalData->pVideo;
      if(!pVideoData){
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoCodecInitialize - pVideoData is null");
        return pVideoData;
      }
      qpDplMemset(&sQmiMsg, 0, sizeof(sQmiMsg));

      global_GlobalData = pGlobalData;
      if (QP_NULL == pVideoData->pQProfile)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoCodecInitialize - QMI Profile is not instantiated");
        return (QP_VIDEO_DESC)QP_NULL;
      }

      if (VIDEO_PLAYER == eVDev && (QP_NULL != pVideoData->pPlayerCallback))
      { 
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoCodecInitialize - Device is already instantiated");
        return (QP_VIDEO_DESC)QP_NULL;
      }

      if (VIDEO_RECORDER == eVDev && (QP_NULL != pVideoData->pRecorderCallback))
      { 
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoCodecInitialize - Device is already instantiated");
        return (QP_VIDEO_DESC)QP_NULL;
      }

      if (pCodecConfig->eCodec == VIDEO_CODEC_H264)
      {
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.video_codec_config.codec = QCE_QMI_VIDEO_H264;
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.iIncludeH264Config = 1;

        if (pCodecConfig->sH264Info.eProfile == VIDEO_AVC_PROFILE_BASELINE)
        {
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.h264_config.h264_config_parameters.profile_type = QCE_QMI_VIDEO_H264_PROFILE_BASELINE;
        }
        else
        {
          IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoCodecInitialize - H.264 Profile not supported: %d", pCodecConfig->sH264Info.eProfile);
          return (QP_VIDEO_DESC)QP_NULL;
        }

        switch(pCodecConfig->sH264Info.eLevel)
        {
        case VIDEO_AVC_LEVEL1:
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.h264_config.h264_config_parameters.profile_level = QCE_QMI_VIDEO_H264_LEVEL1;
          break;
        case VIDEO_AVC_LEVEL1B:
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.h264_config.h264_config_parameters.profile_level = QCE_QMI_VIDEO_H264_LEVEL1B;
          break;
        case VIDEO_AVC_LEVEL11:
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.h264_config.h264_config_parameters.profile_level = QCE_QMI_VIDEO_H264_LEVEL11;
          break;
        case VIDEO_AVC_LEVEL12:
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.h264_config.h264_config_parameters.profile_level = QCE_QMI_VIDEO_H264_LEVEL12;
          break;
        case VIDEO_AVC_LEVEL13:
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.h264_config.h264_config_parameters.profile_level = QCE_QMI_VIDEO_H264_LEVEL13;
          break;
        default:
          IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoCodecInitialize - H.264 Level not supported: %d", pCodecConfig->sH264Info.eLevel);
          return (QP_VIDEO_DESC)QP_NULL;
        }
      }
      else
      {
        IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoCodecInitialize failed!!: Codec Not Supported: %d", pCodecConfig->eCodec);
        return (QP_VIDEO_DESC)QP_NULL;
      }

      if (pVideoData->bDataInitDone != QP_TRUE)
      {
        res = qpVideoDataInit(pVideoData);
        if (res != QP_TRUE)
        {
          IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoCodecInitialize - Failed to Initialize Video Socket");
          return (QP_VIDEO_DESC)QP_NULL;
        }
        pVideoData->bDataInitDone = QP_TRUE;
      }

      if (VIDEO_RECORDER == eVDev)
      {
        IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoCodecInitialize - eVDev = %d", eVDev);
        pVideoData->iVideoDevice |= VIDEO_DEV_RECORDER;
        pVideoData->iRecorderState = eVideoStateIdle;

        pVideoData->iTimeStamp     = 0;
        pVideoData->iBaseTimeStamp = 0;
        pVideoData->iClockRate     = 0;

        /*
        if (pVideoData->iPlayerState != eVideoStateNotReady && pVideoData->iPlayerState != eVideoStateAcquiring)
        {
        pVideoData->iRecorderState = eVideoStateIdle;
        }
        else
        {
        pVideoData->iRecorderState = pVideoData->iPlayerState;
        }*/

        pVideoData->pRecorderUserData = pUserData;
        pVideoData->pRecorderCallback = tVideoCallBack;
        pVideoData->iRecordDescriptor = VIDEO_RECORDER;
        pVideoData->iTimeStamp     = 0;
        pVideoData->iBaseTimeStamp = 0;
        pVideoData->iClockRate     = 0;

      }

      if (VIDEO_PLAYER == eVDev)
      {
        pVideoData->iVideoDevice |= VIDEO_DEV_PLAYER;
        pVideoData->iPlayerState = eVideoStateIdle;

        /*
        if (pVideoData->iRecorderState != eVideoStateNotReady && pVideoData->iRecorderState != eVideoStateAcquiring)
        {
        pVideoData->iPlayerState = eVideoStateIdle;
        }
        else
        {
        pVideoData->iPlayerState = pVideoData->iRecorderState;
        }
        */
        pVideoData->pPlayerUserData = pUserData;
        pVideoData->pPlayerCallback = tVideoCallBack;
        pVideoData->iPlayDescriptor = VIDEO_PLAYER;
        IMS_MSG_LOW_2(LOG_IMS_DPL,"qpVideoCodecInitialize - eVDev = %x, DeviceTypeMask: %x", eVDev, pVideoData->iVideoDevice);

      }
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoCodecInitialize - VideoData is NULL in Global Data");
      return (QP_VIDEO_DESC)QP_NULL;
    }

    if (QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
    {
      QPINT iRes = 0;

      // Fill in some mandatory parameters
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.ip_address.iPort = QPVIDEO_DATA_PORT;

      qpDplStrlcpy(sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.ip_address.ipaddr,pVideoData->LocalAddr,(QPUINT8)QCE_QMI_VIDEO_IP_ADDR_MAX_LEN);

      sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.ip_address.iIpaddr_len = qpDplStrlen(sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.ip_address.ipaddr);

      // Fill in the Audio related Info for Lip Sync
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.audio_param.iAudio_clock_rate      = pCodecConfig->sLipSyncParam.iAudioClockRate;
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.audio_param.iAudio_packet_interval = pCodecConfig->sLipSyncParam.iAudPktInterval;

      IMS_MSG_MED_1(LOG_IMS_DPL,"qpVideoCodecInitialize:: %d Audio clk rte", sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.audio_param.iAudio_clock_rate);
      IMS_MSG_MED_1(LOG_IMS_DPL,"qpVideoCodecInitialize:: %d Audio pkt interval ", sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.audio_param.iAudio_packet_interval);

      if (VIDEO_RECORDER == eVDev)
      {
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.video_device = QCE_QMI_VIDEO_RECORDER_V01;
      }
      else
      {
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.video_device = QCE_QMI_VIDEO_PLAYER_V01;
        if (pCodecConfig->eCodec == VIDEO_CODEC_H264)
        {
          if ((pCodecConfig->pNALHeader != QP_NULL) && (pCodecConfig->iNALHeaderLen > 0) && (pCodecConfig->iNALHeaderLen < QCE_QMI_VIDEO_NAL_HEADER_MAX_LEN))
          {
            // Copy the NAL header
            sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.iIncludeNalHeader = 1;
            sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.iNalheader_len = pCodecConfig->iNALHeaderLen;

            /*Add the preamble 0001 */
            sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[0] = 0;
            sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[1] = 0;
            sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[2] = 0;
            sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[3] = 1;

            iDecodedNalLen = SPS_PPS_PREAMBLE_LEN; // preamble length added
            /*search the ,*/
            for (index=0;index<pCodecConfig->iNALHeaderLen;index++)
            {
            	if(pCodecConfig->pNALHeader[index] == ',')
            	{
            	 IMS_MSG_MED_1(LOG_IMS_DPL,"[sps/pps]!: got the comma %d ", index);
            	 break;
            	}
            }
           
            IMS_MSG_MED_2(LOG_IMS_DPL,"[sps/pps]!: %d comma, %d HdrLen", index, pCodecConfig->iNALHeaderLen);
            	
            qpDplStrlncpy(sps,pCodecConfig->pNALHeader,20,index); /*copy sps and decode*/
          
            iDecodedNalLen += DecodeBase64(sps,&sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[SPS_PPS_PREAMBLE_LEN]);
            	
            IMS_MSG_MED_1(LOG_IMS_DPL,"[sps/pps]!: %d iDecodedNalLen, ", iDecodedNalLen);
				
            	/*remove the comma and add 0001*/
            sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[iDecodedNalLen++] = 0;
            sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[iDecodedNalLen++] = 0;
            sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[iDecodedNalLen++] = 0;
            sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[iDecodedNalLen++] = 1;

            	
            qpDplStrlncpy(pps,&pCodecConfig->pNALHeader[index+1],20,pCodecConfig->iNALHeaderLen-index-1); /*copy pps and decode*/
            	  
            iDecodedNalLen += DecodeBase64(pps,&sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[iDecodedNalLen]);
  
            //TBD: Check with Rakesh/Shankar if we need to convert ASCII to Hex (Base64 encoded??)
          // iDecodedNalLen = DecodeBase64(pCodecConfig->pNALHeader,&sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[0]);
           sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.iNalheader_len = iDecodedNalLen;
           IMS_MSG_MED_1(LOG_IMS_DPL,"qpVideoCodecSet:: %d iDecodedNalLen", iDecodedNalLen);
           // qpDplMemscpy(&sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.nal_header.nalheader[0], pCodecConfig->pNALHeader, pCodecConfig->iNALHeaderLen);
          }
        }
      }

      // Hardcoding value right now for H.264 only with assumption of No Lip Synch and No NAL header
      if (QP_NULL != pCodecConfig)
      { 
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.video_codec_config.iWidth         = pCodecConfig->iWidth;
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.video_codec_config.iHeight        = pCodecConfig->iHeight;
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.video_codec_config.iBit_rate       = pCodecConfig->iBitRate;
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.video_codec_config.iFrame_rate     = pCodecConfig->iFrameRate;
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.video_codec_config.iClock_rate     = pCodecConfig->iClockRate;
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.initialize_req.video_codec_config.iLip_sync_enable = pCodecConfig->bLipSyncEnable;

        pVideoData->iClockRate     = pCodecConfig->iClockRate;
      }
      // Send VideoInitialize QMI Indication to AP IMS Video Engine
      iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_INITIALIZE_REQ, &sQmiMsg);


      return (QP_VIDEO_DESC)pVideoData;
    }
  }
  IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoCodecInitialize failed!!: %d", eVDev);
#endif
  return (QP_VIDEO_DESC)QP_NULL;
}

QPVOID qpVideoCodecUnInitialize(QP_VIDEO_DESC tVideoDescriptor, QPE_VIDEO_DEV eVDev)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE

  qpVideoData*      pVideoData          = (qpVideoData*)tVideoDescriptor;

  IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoCodecUnInitialize - dev = %d", eVDev);
  if (QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
  {
    QPINT iRes = 0;

    qpDplMemset(&sQmiMsg, 0, sizeof(sQmiMsg));
    if (VIDEO_RECORDER == eVDev && (pVideoData->iVideoDevice & VIDEO_DEV_RECORDER) == VIDEO_DEV_RECORDER)
    {
      pVideoData->iRecorderState = eVideoStateReleasing;
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.uninitialize_req.video_device = QCE_QMI_VIDEO_RECORDER_V01;
    }
    if (VIDEO_PLAYER == eVDev && (pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER)
    {
      QPBOOL bRes = QP_TRUE;

      pVideoData->iPlayerState = eVideoStateReleasing;
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.uninitialize_req.video_device = QCE_QMI_VIDEO_PLAYER_V01;

      bRes = qpVideoDataDeInit(pVideoData);
      if (bRes != QP_TRUE)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoCodecUnInitialize - Video Data DeInit Failed");
      }
    }

    // Release resources if both player and recorder are released
    if (pVideoData->iVideoDevice == 0)
    {
      // Set device type for current de-init, so callback can be properly sent
      if (VIDEO_RECORDER == eVDev)
      {
        pVideoData->iVideoDevice = VIDEO_DEV_RECORDER;
      }
      else
      {
        pVideoData->iVideoDevice = VIDEO_DEV_PLAYER;
      }

      // Release VOL
      if (pVideoData->iVolFrame.pData)
      {
        qpDplFree(MEM_IMS_DPL,pVideoData->iVolFrame.pData);
        pVideoData->iVolFrame.pData       = NULL;
        pVideoData->iVolFrame.iDataLen    = 0;
        pVideoData->iVolFrame.iMaxBuffLen = 0;
      }
    }

    iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_UNINITIALIZE_REQ, &sQmiMsg);

    //Do the error checking based on return value
  }
#endif
}

// Player
QPE_VIDEO_ERROR qpVideoPlayStart(QP_VIDEO_DESC tVideoDescriptor)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE
  qpVideoData* pVideoData = (qpVideoData*)tVideoDescriptor;

  if (QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
  {
    IMS_MSG_HIGH_2(LOG_IMS_DPL,"qpVideoPlayStart: device = %d - state = %d", pVideoData->iVideoDevice, pVideoData->iPlayerState);
    if (((pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER) &&
      pVideoData->iPlayerState == eVideoStateIdle)
    {
      QPINT iRes = 0;

      iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_PLAY_START_REQ, QP_NULL);
      if (!iRes)
      {
        pVideoData->iPlayerState      = eVideoStateStarting;
        pVideoData->iPlayerFrameCount = 0;
        IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpVideoPlayStart: state = %d", pVideoData->iPlayerState);
        return VIDEO_ERROR_OK;
      }
      else
      {
        IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoPlayStart: QMI send failed: %d", iRes);
      }
    }
  }
  IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoPlayStart - Unable to start player: %x", pVideoData);
#endif
  return VIDEO_ERROR_UNKNOWN;

}

QPE_VIDEO_ERROR qpVideoPlayStop(QP_VIDEO_DESC tVideoDescriptor, QPBOOL bPurge)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE
  QPINT iRes = 0;
  qpVideoData* pVideoData = (qpVideoData*)tVideoDescriptor;

  QP_UNUSED(bPurge);

  IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoPlayStop");
  if (QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
  {
    if ((pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER &&
      (pVideoData->iPlayerState == eVideoStateActive || pVideoData->iPlayerState == eVideoStateStarting))
    {
      iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_PLAY_STOP_REQ, &sQmiMsg);

      if (!iRes)
      {
        pVideoData->iPlayerState = eVideoStateStopping;
        return VIDEO_ERROR_OK;
      }
    }
  }
  IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoPlayStop - Unable to stop player");
#endif
  return VIDEO_ERROR_UNKNOWN;
}

QPE_VIDEO_ERROR qpVideoPlayFrame(QP_VIDEO_DESC tVideoDescriptor, QP_MULTIMEDIA_FRAME* pCodecFrame )
{
  qpVideoFrameInfo sFrameInfo;
  qpVideoData* pVideoData = (qpVideoData*)tVideoDescriptor;

  IMS_MSG_LOW_2(LOG_IMS_DPL,"qpVideoPlayFrame | pVideoData=%x pCodecFrame=%x", pVideoData, pCodecFrame);
  if (QP_NULL != pVideoData)
  {
    IMS_MSG_LOW_3(LOG_IMS_DPL,"qpVideoPlayFrame | iPlayerState=%d iVideoDevice=%d pQProfile=%x", pVideoData->iPlayerState, pVideoData->iVideoDevice, pVideoData->pQProfile);
  }

  if (QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile) && (QP_NULL != pCodecFrame))
  {
    if (((pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER) &&
      pVideoData->iPlayerState == eVideoStateActive)
    {
      QPBOOL bRes = QP_TRUE;
      //qpDplMemset(&sQmiMsg, 0, sizeof(sQmiMsg));
      IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoPlayFrame - proper state");

      if (QP_NULL == pCodecFrame->pData)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoPlayFrame - Video Frame is NULL");
        return VIDEO_ERROR_RECORDER_DOWN;
      }
#if 1
      qpDplMemset(&sFrameInfo, 0, sizeof(qpVideoFrameInfo));
      sFrameInfo.iMarkerBit = (QPUINT8)pCodecFrame->sMediaPacketInfo.sMediaPktInfoRx.bMarkerBit;
      sFrameInfo.iSeqNum = pCodecFrame->sMediaPacketInfo.sMediaPktInfoRx.iSeqNum;
      sFrameInfo.iTimestamp = pCodecFrame->sMediaPacketInfo.sMediaPktInfoRx.iTimeStamp;
      sFrameInfo.iLen = (QPUINT16) pCodecFrame->iDataLen;
      sFrameInfo.pFrame = (QPUINT8*)pCodecFrame->pData;

      bRes  = qpVideoDataSendRXFrame(pVideoData, &sFrameInfo);

      if (bRes != QP_TRUE)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoPlayFrame - Failed to send Video Frame");
        return VIDEO_ERROR_RECORDER_DOWN;
      }
#else
      memset(gNwTxBuff,3,2000);
      pVideoData->pNetConnProfile->iRemoteIpAddr= qpDplInetPton(&pVideoData->PeerAddr[0]);
      pVideoData->pNetConnProfile->iRemotePort = pVideoData->iPeerPort;
      err = qpDplNetSendData(pVideoData->pNetConnProfile, gNwTxBuff, 2000);
      IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoPlayFrame - qpDplNetSendData err = %d",err);
#endif	  
      pVideoData->iPlayerFrameCount++;


      return VIDEO_ERROR_OK;
    }
  }

  IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoPlayFrame - Unable to play frame");
  return VIDEO_ERROR_RECORDER_DOWN;
}

// Recorder
QPE_VIDEO_ERROR qpVideoRecordStart(QP_VIDEO_DESC tVideoDescriptor, QPUINT8 iFrameBundlingVal)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE
  QPINT iRes = 0;
  qpVideoData* pVideoData = (qpVideoData*)tVideoDescriptor;

  if (QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
  {
    IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoRecordStart");
    if (pVideoData && (pVideoData->iVideoDevice & VIDEO_DEV_RECORDER) == VIDEO_DEV_RECORDER &&
      pVideoData->iRecorderState == eVideoStateIdle)
    {
      QPUINT32 iTimeStampMultiplyFactor = 0;

      //initialize the timestamp
      iTimeStampMultiplyFactor = (pVideoData->iClockRate/1000);
      pVideoData->iBaseTimeStamp = qpDplGetTimeInMilliSeconds();
      pVideoData->iBaseTimeStamp = (pVideoData->iBaseTimeStamp >> 16);
      pVideoData->iTimeStamp = 0;

      pVideoData->iBaseTimeStamp = (pVideoData->iBaseTimeStamp * iTimeStampMultiplyFactor);

      pVideoData->iTimeStamp += pVideoData->iBaseTimeStamp;
      //Reseting base time stamp
      pVideoData->iBaseTimeStamp = qpDplGetTimeInMilliSeconds();

      iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_RECORD_START_REQ, QP_NULL);
      IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoRecordStart  qpDplSendQMIMessage iRes = %x", iRes);
      if (!iRes)
      {
        pVideoData->iRecorderState    = eVideoStateStarting;
        pVideoData->iRecorderFrameCount = 0;
        IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoRecordStart  pVideoData->iRecorderState = %x", pVideoData->iRecorderState);
        IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpVideoRecordStart pVideoData->iVideoDevice = %x",pVideoData->iVideoDevice);
        return VIDEO_ERROR_OK;
      }
    }
  }

  IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoRecordStart - Unable to start recorder");
#endif
  return VIDEO_ERROR_UNKNOWN;

}

QPE_VIDEO_ERROR qpVideoRecordStop(QP_VIDEO_DESC tVideoDescriptor)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE
  QPINT iRes = 0;
  qpVideoData* pVideoData = (qpVideoData*)tVideoDescriptor;
  if(QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
  {
    IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoRecordStop");

    if (pVideoData && (pVideoData->iVideoDevice & VIDEO_DEV_RECORDER) == VIDEO_DEV_RECORDER &&
      (pVideoData->iRecorderState == eVideoStateActive || pVideoData->iRecorderState == eVideoStateStarting))
    {

      IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoRecordStop: going to stop the recorder");

      iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_RECORD_STOP_REQ, QP_NULL);

      if (!iRes)
      {
        IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoRecordStop: recorder stopped successful");
        pVideoData->iRecorderState = eVideoStateStopping;
        return VIDEO_ERROR_OK;
      }
    }
  }
  IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoRecordStop - Unable to stop recorder");
#endif
  return VIDEO_ERROR_UNKNOWN;
}

// Generic functions
QPE_VIDEO_ERROR qpVideoCodecSet(QP_VIDEO_DESC tVideoDescriptor, QP_VIDEO_CONFIG tCodecConfig)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE
  qpVideoData* pVideoData = (qpVideoData*)tVideoDescriptor;
  QPUINT8 iDecodedNalLen = 0;
  QPCHAR sps[20],pps[20];
  int index;

  if (tCodecConfig.eCodec != VIDEO_CODEC_H263 && tCodecConfig.eCodec != VIDEO_CODEC_MPEG4_ISO &&
    tCodecConfig.eCodec != VIDEO_CODEC_MPEG4_XVID && tCodecConfig.eCodec != VIDEO_CODEC_H264)
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoCodecSet - Invalid codec (%d)", tCodecConfig.eCodec);
    return VIDEO_ERROR_UNKNOWN;
  }

  if (tCodecConfig.iFrameRate < 2 || tCodecConfig.iFrameRate > 30 ||
    tCodecConfig.iBitRate < 2 || tCodecConfig.iBitRate > 720)
  {
    IMS_MSG_ERR_2(LOG_IMS_DPL,"qpVideoCodecSet - Invalid bitrate (%d) or framerate (%d) parameters",
      tCodecConfig.iBitRate, tCodecConfig.iFrameRate, 0);
    return VIDEO_ERROR_UNKNOWN;
  }

  if (QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
  {
    QPINT iRes = 0;

    qpDplMemset(&sQmiMsg, 0, sizeof(sQmiMsg));

    if (pVideoData->iVideoDevice == 0)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoCodecSet - No device is set!");
      return VIDEO_ERROR_UNKNOWN;
    }

    if (tCodecConfig.eCodec == VIDEO_CODEC_H264)
    {
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.video_codec_config.codec = QCE_QMI_VIDEO_H264;
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.iIncludeH264Config = 1;

      if (tCodecConfig.sH264Info.eProfile == VIDEO_AVC_PROFILE_BASELINE)
      {
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.h264_config.h264_config_parameters.profile_type = QCE_QMI_VIDEO_H264_PROFILE_BASELINE;
      }
      else
      {
        IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoCodecSet - H.264 Profile not supported: %d", tCodecConfig.sH264Info.eProfile);
        return VIDEO_ERROR_UNKNOWN;
      }

      switch(tCodecConfig.sH264Info.eLevel)
      {
      case VIDEO_AVC_LEVEL1:
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.h264_config.h264_config_parameters.profile_level = QCE_QMI_VIDEO_H264_LEVEL1;
        break;
      case VIDEO_AVC_LEVEL1B:
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.h264_config.h264_config_parameters.profile_level = QCE_QMI_VIDEO_H264_LEVEL1B;
        break;
      case VIDEO_AVC_LEVEL11:
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.h264_config.h264_config_parameters.profile_level = QCE_QMI_VIDEO_H264_LEVEL11;
        break;
      case VIDEO_AVC_LEVEL12:
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.h264_config.h264_config_parameters.profile_level = QCE_QMI_VIDEO_H264_LEVEL12;
        break;
      case VIDEO_AVC_LEVEL13:
        sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.h264_config.h264_config_parameters.profile_level = QCE_QMI_VIDEO_H264_LEVEL13;
        break;
      default:
        IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoCodecSet - H.264 Level not supported: %d", tCodecConfig.sH264Info.eLevel);
        return VIDEO_ERROR_UNKNOWN;
      }
    }
    else
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoCodecSet failed!!: Codec Not Supported: %d", tCodecConfig.eCodec);
      return VIDEO_ERROR_UNKNOWN;
    }

    if (tCodecConfig.eDeviceType == VIDEO_PLAYER)
    {
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.video_device = QCE_QMI_VIDEO_PLAYER_V01;
      if (tCodecConfig.eCodec == VIDEO_CODEC_H264)
      {
        if ((tCodecConfig.pNALHeader != QP_NULL) && (tCodecConfig.iNALHeaderLen > 0) && (tCodecConfig.iNALHeaderLen < QCE_QMI_VIDEO_NAL_HEADER_MAX_LEN))
        {
          // Copy the NAL header
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.iIncludeNalHeader = 1;
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.iNalheader_len = tCodecConfig.iNALHeaderLen;
	  
          /*Add the preamble 0001 */
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[0] = 0;
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[1] = 0;
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[2] = 0;
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[3] = 1;

          iDecodedNalLen = SPS_PPS_PREAMBLE_LEN; // preamble length added
          /*search the ,*/
          for (index=0;index<tCodecConfig.iNALHeaderLen;index++)
          {
			
            	if(tCodecConfig.pNALHeader[index] == ',')
            	{
            	 IMS_MSG_MED_1(LOG_IMS_DPL,"[sps/pps]!: got the comma %d ", index);
            	 break;
            	}
          }
          
          
          IMS_MSG_MED_2(LOG_IMS_DPL,"[sps/pps]!: %d comma, %d HdrLen", index, tCodecConfig.iNALHeaderLen);
          qpDplStrlncpy(sps,tCodecConfig.pNALHeader,20,index); /*copy sps and decode*/

          iDecodedNalLen += DecodeBase64(sps,&sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[SPS_PPS_PREAMBLE_LEN]);
          /*remove the comma and add 0001*/
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[iDecodedNalLen++] = 0;
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[iDecodedNalLen++] = 0;
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[iDecodedNalLen++] = 0;
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[iDecodedNalLen++] = 1;
         
          qpDplStrlncpy(pps,&tCodecConfig.pNALHeader[index+1],20,tCodecConfig.iNALHeaderLen-index-1); /*copy pps and decode*/
			
          iDecodedNalLen += DecodeBase64(pps,&sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[iDecodedNalLen]);
			

          //TBD: Check with Rakesh/Shankar if we need to convert ASCII to Hex (Base64 encoded??)
         // iDecodedNalLen = DecodeBase64(tCodecConfig.pNALHeader,&sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[0]);
          sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.iNalheader_len = iDecodedNalLen;
          IMS_MSG_MED_1(LOG_IMS_DPL,"qpVideoCodecSet:: %d iDecodedNalLen", iDecodedNalLen);
          //qpDplMemscpy(&sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.nal_header.nalheader[0], tCodecConfig.pNALHeader, tCodecConfig.iNALHeaderLen);

        }
      }
    }
    else if (tCodecConfig.eDeviceType == VIDEO_RECORDER)
    {
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.video_device = QCE_QMI_VIDEO_RECORDER_V01;
    }
    else
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoCodecSet - Invalid device type: %d", tCodecConfig.eDeviceType);
      return VIDEO_ERROR_UNKNOWN;
    }


    sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.video_codec_config.iWidth           = tCodecConfig.iWidth;
    sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.video_codec_config.iHeight          = tCodecConfig.iHeight;
    sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.video_codec_config.iBit_rate        = tCodecConfig.iBitRate;
    sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.video_codec_config.iFrame_rate      = tCodecConfig.iFrameRate;
    sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.video_codec_config.iClock_rate      = tCodecConfig.iClockRate;
    sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.video_codec_config.iLip_sync_enable = tCodecConfig.bLipSyncEnable;    
    sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.iIncludeAudioParam = 1;
    sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.audio_param.iAudio_clock_rate			 = tCodecConfig.sLipSyncParam.iAudioClockRate;
    sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.audio_param.iAudio_packet_interval  = tCodecConfig.sLipSyncParam.iAudPktInterval; 
    IMS_MSG_MED_1(LOG_IMS_DPL,"qpVideoCodecSet:: %d Audio clk rte", sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.audio_param.iAudio_clock_rate);
    IMS_MSG_MED_1(LOG_IMS_DPL,"qpVideoCodecSet:: %d Audio pkt interval ", sQmiMsg.ims_qmi_data.video_info_struct.video_data.codec_config_req.audio_param.iAudio_packet_interval);

    pVideoData->iClockRate = tCodecConfig.iClockRate;
    iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_CODEC_CONFIG_REQ, &sQmiMsg);

    if (!iRes)
    {
      return VIDEO_ERROR_OK;
    }
  }
  IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoCodecSet - ");
#endif
  return VIDEO_ERROR_UNKNOWN;
}


QP_IMPORT_C QPE_VIDEO_ERROR qpVideoGenerateNalHeader(QP_VIDEO_CONFIG tCodecConfig, QPCHAR* pNalHeader, QPUINT16* pNalHeaderLen)
{
  return VIDEO_ERROR_UNKNOWN;
}

QPE_VIDEO_ERROR qpDplVideoReport(QP_VIDEO_DESC tVideoDescriptor, QPUINT32 n_iMSNtpTime, QPUINT32 n_iLSNtpTime, QPUINT32 n_iRtpTimeStamp)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE

  QPINT iRes = 0;
  qpVideoData* pVideoData = (qpVideoData*)tVideoDescriptor;
  if(QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
  {
    IMS_MSG_LOW_0(LOG_IMS_DPL,"qpDplVideoReport");

    if ((pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER &&
      (pVideoData->iPlayerState == eVideoStateActive))
    {
      IMS_MSG_LOW_1(LOG_IMS_DPL,"qpDplVideoReport n_imsntp_time = %u",n_iMSNtpTime);
      IMS_MSG_LOW_1(LOG_IMS_DPL,"qpDplVideoReport n_ilsntp_time = %u",n_iLSNtpTime);
      IMS_MSG_LOW_1(LOG_IMS_DPL,"qpDplVideoReport n_irtp_time_stamp = %u",n_iRtpTimeStamp);
      qpDplMemset(&sQmiMsg, 0, sizeof(sQmiMsg));
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.video_report_req.time_info.iMsntp_time = n_iMSNtpTime;
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.video_report_req.time_info.iLsntp_time = n_iLSNtpTime;
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.video_report_req.time_info.iRtp_time = n_iRtpTimeStamp;

      iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_VIDEO_REPORT_REQ, &sQmiMsg);

      if (!iRes)
      {
        IMS_MSG_LOW_0(LOG_IMS_DPL,"qpDplVideoReport: Report Sent successfuly");
        return VIDEO_ERROR_OK;
      }
      else
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpDplVideoReport - Failed to send QMI Msg");
      }
    }
  }
  else
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpDplVideoReport - Invalid Param: VideoData: %x", pVideoData);
  }
#endif
  return VIDEO_ERROR_UNKNOWN;
}

QPE_VIDEO_ERROR qpDplVideoGenerateH264IdrFrame(QP_VIDEO_DESC tVideoDescriptor)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE
  QPINT iRes = 0;
  qpVideoData* pVideoData = (qpVideoData*)tVideoDescriptor;
  if(QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
  {
    IMS_MSG_LOW_0(LOG_IMS_DPL,"qpDplVideoGenerateH264IdrFrame");

    if ((pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER &&
      (pVideoData->iPlayerState == eVideoStateActive))
    {
      qpDplMemset(&sQmiMsg, 0, sizeof(sQmiMsg));

      iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_H264_IDR_GENERATE_REQ, &sQmiMsg);

      if (!iRes)
      {
        IMS_MSG_LOW_0(LOG_IMS_DPL,"qpDplVideoGenerateH264IdrFrame: Report Sent successfuly");
        return VIDEO_ERROR_OK;
      }
      else
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpDplVideoGenerateH264IdrFrame - Failed to send QMI Msg");
      }
    }
  }
  else
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpDplVideoGenerateH264IdrFrame - Invalid Param: VideoData: %x", pVideoData);
  }
#endif
  return VIDEO_ERROR_UNKNOWN;
}
QPE_VIDEO_ERROR qpDplVideoAdaptVideoBitRate(QP_VIDEO_DESC tVideoDescriptor,QPUINT32 n_iBitRate)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE
  QPINT iRes = 0;
  qpVideoData* pVideoData = (qpVideoData*)tVideoDescriptor;
  if(QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
  {
    IMS_MSG_LOW_0(LOG_IMS_DPL,"qpDplVideoAdaptVideoBitRate");

    if ((pVideoData->iVideoDevice & VIDEO_RECORDER) == VIDEO_RECORDER &&
      (pVideoData->iRecorderState == eVideoStateActive))
    {
      qpDplMemset(&sQmiMsg, 0, sizeof(sQmiMsg));
			sQmiMsg.ims_qmi_data.video_info_struct.video_data.bitrate_adapt_req.iBitRate = n_iBitRate;

      iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_VIDEO_BIT_RATE_ADAPT_REQ, &sQmiMsg);

      if (!iRes)
      {
        IMS_MSG_LOW_0(LOG_IMS_DPL,"qpDplVideoAdaptVideoBitRate: Report Sent successfuly");
        return VIDEO_ERROR_OK;
      }
      else
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpDplVideoAdaptVideoBitRate - Failed to send QMI Msg");
      }
    }
  }
  else
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpDplVideoAdaptVideoBitRate - Invalid Param: VideoData: %x", pVideoData);
  }
#endif
  return VIDEO_ERROR_UNKNOWN;
}

QPVOID qpVideoSendAudioReport(QPUINT32 n_imsntp_time, /* Most significant NTP Timestamp */
                              QPUINT32 n_ilsntp_time, /* Least significant NTP Timestamp */
                              QPUINT32 n_irtp_time_stamp /* RTP Timestamp */
                              )
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE
  QPINT iRes = 0;
  qpVideoData* pVideoData = QP_NULL;
  QPC_GLOBAL_DATA* pGlobalData = qpDplGetGlobalData();

  if (pGlobalData == NULL || pGlobalData->pVideo == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoSendAudioReport - Global Data is NULL");
    return;
  }

  pVideoData = (qpVideoData*)pGlobalData->pVideo;

  if(QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
  {
    IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoSendAudioReport n_imsntp_time = %u",n_imsntp_time);
    IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoSendAudioReport n_ilsntp_time = %u",n_ilsntp_time);
    IMS_MSG_LOW_1(LOG_IMS_DPL,"qpVideoSendAudioReport n_irtp_time_stamp = %u",n_irtp_time_stamp);

    if ((pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER &&
      (pVideoData->iPlayerState == eVideoStateActive))
    {
      qpDplMemset(&sQmiMsg, 0, sizeof(sQmiMsg));
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.video_report_req.time_info.iMsntp_time = n_imsntp_time;
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.video_report_req.time_info.iLsntp_time = n_ilsntp_time;
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.video_report_req.time_info.iRtp_time = n_irtp_time_stamp;

      iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_AUDIO_REPORT_REQ, &sQmiMsg);

      if (!iRes)
      {
        IMS_MSG_LOW_0(LOG_IMS_DPL,"qpVideoSendAudioReport: Report Sent successfuly");
        return;
      }
      else
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoSendAudioReport - Failed to send QMI Msg");
      }
    }
  }
  else
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoSendAudioReport - Invalid Param: VideoData: %x", pVideoData);
  }
#endif
}

QPVOID qpProcessAudioPlayTS(QPUINT32 n_pTimestamp)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE
  QPINT iRes = 0;
  qpVideoData* pVideoData = QP_NULL;
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;

  //pGlobalData = qpDplGetGlobalData();
  pGlobalData = global_GlobalData;//Hack for now, should be removed when the CV comes in 

  if (pGlobalData == NULL || pGlobalData->pVideo == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpProcessAudioPlayTS - Global Data is NULL");
    return;
  }

  pVideoData = (qpVideoData*)pGlobalData->pVideo;

  if(QP_NULL != pVideoData && (QP_NULL != pVideoData->pQProfile))
  {
    IMS_MSG_LOW_1(LOG_IMS_DPL,"qpProcessAudioPlayTS %u", n_pTimestamp);

    if ((pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER &&
      (pVideoData->iPlayerState == eVideoStateActive))
    {
      qpDplMemset(&sQmiMsg, 0, sizeof(sQmiMsg));
      sQmiMsg.ims_qmi_data.video_info_struct.video_data.last_audio_play_time_req.last_play_time_stamp = n_pTimestamp;

      iRes = qpDplSendQMIMessage(pVideoData->pQProfile, QC_IMS_QMI_VIDEO_LAST_AUDIO_PLAY_TIME_REQ, &sQmiMsg);

      if (!iRes)
      {
        IMS_MSG_LOW_0(LOG_IMS_DPL,"qpProcessAudioPlayTS: Report Sent successfuly");
        return;
      }
      else
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpProcessAudioPlayTS - Failed to send QMI Msg");
      }
    }
  }
  else
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpProcessAudioPlayTS - Invalid Param: VideoData: %x", pVideoData);
  }
#endif
}

QPVOID qpVideoSendLastPlayedAudTimeStamp(QPUINT32 n_iTimestamp)
{
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
  QPUINT32* pTS = QP_NULL;
  //pGlobalData = qpDplGetGlobalData();
  pGlobalData = global_GlobalData;

  if (pGlobalData == QP_NULL || pGlobalData->pVideo == QP_NULL)
  {
    if(pGlobalData && pGlobalData->pVideo == QP_NULL)
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoSendLastPlayedAudTimeStamp - Global Data is NULL");
    return;
  }
  pTS = qpDplMalloc(MEM_IMS_DPL, sizeof(QPUINT32));
  if (pTS)
  {
    *pTS = n_iTimestamp;
    qpDplPostEventToEventQueue(QPCMSG_DPL_AUDIO_TS_MSG, 0, pTS, global_GlobalData);
  }
}

/*-------------------------------------------------------------------
qpVideoQmiHandler:
This callback is invoked when a frame is ready to be streamed or when
there is asynchronous event from AP IMS engine.
The buffer is contained in stream_data and length is max_bytes.
We don't need to do any operation with pMedHdr
-------------------------------------------------------------------*/

QPVOID qpVideoQmiHandler(QCE_QMI_MESSAGE_IDS eQmiMsgID, QPVOID* pMsgDataStruct, QPVOID* pUsedData)
{
#ifdef FEATURE_IMS_QMI_VIDEO_SERVICE

  QPE_VIDEO_MSG iVideoMsg             = VIDEO_MSG_ERROR;    // Message to send to recorder/player
  QP_VIDEO_CALLBACK pRecorderCallback = NULL;               // Callback function for recorder (NULL is no event)
  QP_VIDEO_CALLBACK pPlayerCallback   = NULL;               // Callback function for player (NULL is no event)
  qpVideoData* pVideoData             = (qpVideoData*)pUsedData;
  QPVOID* pRecorderUserData           = NULL;               // RecorderData (we need to store for release case)
  QPVOID* pPlayerUserData             = NULL;               // PlayerData (we need to store for release case)
  QPBOOL  bIsCleanup                  = QP_FALSE;
  QPC_GLOBAL_DATA* pGlobalData        = QP_NULL;

  if (QP_NULL == pMsgDataStruct || QP_NULL == pUsedData)
  {
    IMS_MSG_ERR_2(LOG_IMS_DPL,"qpVideoQmiHandler - Invalid param: %x, %x", pMsgDataStruct, pUsedData);
    return;
  }

  pGlobalData = qpDplGetGlobalData();

  if (pGlobalData == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - FATAL: Global Data is NULL");
    return;
  }

  if (pGlobalData->pVideo == QP_NULL)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - IGNORING MSG: Global Video Data is NULL");
    return;
  }

  pRecorderUserData = pVideoData->pRecorderUserData;
  pPlayerUserData   = pVideoData->pPlayerUserData;
  IMS_MSG_HIGH_2(LOG_IMS_DPL,"qpVideoQmiHandler pVideoData = %x QmiMsg = %x",pVideoData,eQmiMsgID);
  IMS_MSG_HIGH_3(LOG_IMS_DPL,"qpVideoQmiHandler pVD->VidDev = %x RCB: %x PCB: %x",
    pVideoData->iVideoDevice,pVideoData->pRecorderCallback,pVideoData->pPlayerCallback);
  IMS_MSG_HIGH_2(LOG_IMS_DPL,"qpVideoQmiHandler RS = %x PS = %x",pVideoData->iRecorderState,pVideoData->iPlayerState);

  switch(eQmiMsgID)
  {
  case QC_IMS_QMI_VIDEO_INITIALIZE_RESP:
    {
      ims_video_initialize_resp* pResp = (ims_video_initialize_resp*)(&(((ims_qmi_msg_info_u_type*)pMsgDataStruct)->ims_qmi_data.video_info_struct.video_data.initialize_resp));

      IMS_MSG_HIGH_0(LOG_IMS_DPL, "qpVideoQmiHandler: QC_IMS_QMI_VIDEO_INITIALIZE_RESP");

      IMS_MSG_HIGH_3(LOG_IMS_DPL,"qpVideoQmiHandler:QMI ResType: %d VDev:%d-%d",pResp->eQmiResponseType,pResp->iIncludeVideoDevice,pResp->video_device);

      if (pResp->eQmiResponseType == QC_QMI_RESPONSE_SUCCESS)
      {
        iVideoMsg = VIDEO_MSG_DEV_INITIALISED;
      }
      else
      {
        iVideoMsg = VIDEO_MSG_ERROR;
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - VIDEO_INTIALIZED FAILED");
      }

      if (pResp->iIncludeVideoDevice == 0)
      {
        iVideoMsg = VIDEO_MSG_ERROR;
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - FATAL: No Video Device Indication from AP");
        break;
      }

      if ((pResp->video_device == QCE_QMI_VIDEO_RECORDER_V01) && ((pVideoData->iVideoDevice & VIDEO_DEV_RECORDER) == VIDEO_DEV_RECORDER))
      {
        IMS_MSG_HIGH_0(LOG_IMS_DPL,"qpVideoQmiHandler set pRecorderCallback");
        pVideoData->iRecorderState = eVideoStateIdle;
        pRecorderCallback = (QP_VIDEO_CALLBACK)pVideoData->pRecorderCallback;
      }
      if ((pResp->video_device == QCE_QMI_VIDEO_PLAYER_V01) && ((pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER))
      {
        IMS_MSG_HIGH_0(LOG_IMS_DPL,"qpVideoQmiHandler set pPlayerCallback");
        pVideoData->iPlayerState = eVideoStateIdle;
        pPlayerCallback = (QP_VIDEO_CALLBACK)pVideoData->pPlayerCallback;

        //Store the Peer IP Address and Port
        if (pResp->iIncludeIpAddress == 0)
        {
          iVideoMsg = VIDEO_MSG_ERROR;
          IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - FATAL: No IP Address from AP");
        }
        else
        { 
          pVideoData->iPeerPort = pResp->ip_address.iPort; //TBD: Check if it is in NW order
          IMS_MSG_LOW_1_STR(LOG_IMS_DPL,"qpVideoQmiHandler Peer IP %s",pResp->ip_address.ipaddr);
          IMS_MSG_LOW_1_STR(LOG_IMS_DPL,"qpVideoQmiHandler iIpaddr_len IP %d",pResp->ip_address.iIpaddr_len);
          if (pResp->ip_address.iIpaddr_len < QPVIDEO_IP_ADDR_MAX)
          {
            qpDplMemset(&pVideoData->PeerAddr[0], 0, QPVIDEO_IP_ADDR_MAX);
            qpDplMemscpy(&pVideoData->PeerAddr[0], sizeof(QPCHAR), &pResp->ip_address.ipaddr[0], pResp->ip_address.iIpaddr_len);
            IMS_MSG_LOW_1_STR(LOG_IMS_DPL,"qpVideoQmiHandler Peer IP Address: %s", pVideoData->PeerAddr);

            if (pResp->ip_address.iptype == QCE_QMI_VIDEO_IPV6_V01)
            {
              pVideoData->eRemoteIPType = IPV6;
            }
            else if (pResp->ip_address.iptype == QCE_QMI_VIDEO_IPV4_V01)
            {
              pVideoData->eRemoteIPType = IPV4;
            }
            else
            {
              iVideoMsg = VIDEO_MSG_ERROR;
              IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoQmiHandler - FATAL: INVALID IP Address Type: %d",pResp->ip_address.iptype);
            }
          }
          else
          {
            iVideoMsg = VIDEO_MSG_ERROR;
            IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoQmiHandler - FATAL: INVALID IP Address Len: %d",pResp->ip_address.iIpaddr_len);
          }
        }
      }
      break;
    }
  case QC_IMS_QMI_VIDEO_RECORD_START_RESP:
    { 
      ims_video_standard_resp* pResp = (ims_video_standard_resp*)(&(((ims_qmi_msg_info_u_type*)pMsgDataStruct)->ims_qmi_data.video_info_struct.video_data.standard_resp));

      IMS_MSG_HIGH_0(LOG_IMS_DPL, "qpVideoQmiHandler: QC_IMS_QMI_VIDEO_RECORD_START_RESP");

      IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpVideoQmiHandler:QMI ResType: %d",pResp->eQmiResponseType);

      if ((pVideoData->iVideoDevice & VIDEO_DEV_RECORDER) == VIDEO_DEV_RECORDER)
      {
        if (pVideoData->iRecorderState != eVideoStateStarting)
        {
          IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoQmiHandler: Ignoring Recorder Start Response (Invalid State) - %d", pVideoData->iRecorderState);
          break;
        }

        if (pResp->eQmiResponseType == QC_QMI_RESPONSE_SUCCESS)
        {
          iVideoMsg = VIDEO_MSG_RECORDER_STARTED;
          pVideoData->iRecorderState = eVideoStateActive;
        }
        else
        {
          iVideoMsg = VIDEO_MSG_ERROR;
          pVideoData->iRecorderState = eVideoStateIdle;
          IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - RECORDER_START FAILED");
        }

        pRecorderCallback = (QP_VIDEO_CALLBACK)pVideoData->pRecorderCallback;
      }
      break;
    }
  case QC_IMS_QMI_VIDEO_PLAY_START_RESP:
    {
      ims_video_standard_resp* pResp = (ims_video_standard_resp*)(&(((ims_qmi_msg_info_u_type*)pMsgDataStruct)->ims_qmi_data.video_info_struct.video_data.standard_resp));

      IMS_MSG_HIGH_0(LOG_IMS_DPL, "qpVideoQmiHandler: QC_IMS_QMI_VIDEO_PLAY_START_RESP");

      IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpVideoQmiHandler:QMI ResType: %d",pResp->eQmiResponseType);

      if ((pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER)
      {
        if (pVideoData->iPlayerState != eVideoStateStarting)
        {
          IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoQmiHandler: Ignoring Player Start Response (Invalid State) - %d", pVideoData->iPlayerState);
          break;
        }

        if (pResp->eQmiResponseType == QC_QMI_RESPONSE_SUCCESS)
        {
          iVideoMsg = VIDEO_MSG_PLAYER_STARTED;
          pVideoData->iPlayerState = eVideoStateActive;
        }
        else
        {
          iVideoMsg = VIDEO_MSG_ERROR;
          pVideoData->iPlayerState = eVideoStateIdle;
          IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - PLAYER_START FAILED");
        }

        pPlayerCallback = (QP_VIDEO_CALLBACK)pVideoData->pPlayerCallback; 
      }
      break;
    }
  case QC_IMS_QMI_VIDEO_RECORD_STOP_RESP:
    {
      ims_video_standard_resp* pResp = (ims_video_standard_resp*)(&(((ims_qmi_msg_info_u_type*)pMsgDataStruct)->ims_qmi_data.video_info_struct.video_data.standard_resp));

      IMS_MSG_HIGH_0(LOG_IMS_DPL, "qpVideoQmiHandler: QC_IMS_QMI_VIDEO_RECORD_STOP_RESP");

      IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpVideoQmiHandler:QMI ResType: %d",pResp->eQmiResponseType);

      if (pVideoData->iRecorderState != eVideoStateActive && pVideoData->iRecorderState != eVideoStateStopping)
      {
        IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoQmiHandler: Ignoring Recorder Stop Response (Invalid State) - %d", pVideoData->iRecorderState);
        break;
      }

      if (pResp->eQmiResponseType == QC_QMI_RESPONSE_SUCCESS)
      {
        iVideoMsg = VIDEO_MSG_RECORDER_STOPPED;
        pVideoData->iRecorderState = eVideoStateIdle;
      }
      else
      {
        iVideoMsg = VIDEO_MSG_ERROR;
        pVideoData->iRecorderState = eVideoStateIdle;
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - RECORDER_STOP FAILED");
      }
      pRecorderCallback = (QP_VIDEO_CALLBACK)pVideoData->pRecorderCallback;

      break;
    }
  case QC_IMS_QMI_VIDEO_PLAY_STOP_RESP:
    {
      ims_video_standard_resp* pResp = (ims_video_standard_resp*)(&(((ims_qmi_msg_info_u_type*)pMsgDataStruct)->ims_qmi_data.video_info_struct.video_data.standard_resp));

      IMS_MSG_HIGH_0(LOG_IMS_DPL, "qpVideoQmiHandler: QC_IMS_QMI_VIDEO_PLAY_STOP_RESP");

      IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpVideoQmiHandler:QMI ResType: %d",pResp->eQmiResponseType);

      if (pVideoData->iPlayerState != eVideoStateActive && pVideoData->iPlayerState != eVideoStateStopping)
      {
        IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoQmiHandler: Ignoring Player Stop Response (Invalid State) - %d", pVideoData->iPlayerState);
        break;
      }

      if (pResp->eQmiResponseType == QC_QMI_RESPONSE_SUCCESS)
      {
        iVideoMsg = VIDEO_MSG_PLAYER_STOPPED;
        pVideoData->iPlayerState = eVideoStateIdle;
      }
      else
      {
        iVideoMsg = VIDEO_MSG_ERROR;
        pVideoData->iPlayerState = eVideoStateIdle;
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - PLAYER_STOP FAILED");
      }
      pPlayerCallback = (QP_VIDEO_CALLBACK)pVideoData->pPlayerCallback;

      break;
    }
  case QC_IMS_QMI_VIDEO_UNINITIALIZE_RESP:
    {
      ims_video_uninitialize_resp* pResp = (ims_video_uninitialize_resp*)(&(((ims_qmi_msg_info_u_type*)pMsgDataStruct)->ims_qmi_data.video_info_struct.video_data.uninitialize_resp));

      IMS_MSG_HIGH_0(LOG_IMS_DPL, "qpVideoQmiHandler: QC_IMS_QMI_VIDEO_UNINITIALIZE_RESP");

      IMS_MSG_HIGH(LOG_IMS_DPL,"qpVideoQmiHandler:QMI ResType: %d VDev:%d-%d",pResp->eQmiResponseType,pResp->iIncludeVideoDevice,pResp->video_device);

      if (pResp->eQmiResponseType == QC_QMI_RESPONSE_SUCCESS)
      {
        iVideoMsg = VIDEO_MSG_DEV_UNINITIALISED;
      }
      else
      {
        iVideoMsg = VIDEO_MSG_ERROR;
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - VIDEO_INTIALIZED FAILED");
      }

      if (pResp->iIncludeVideoDevice == 0)
      {
        iVideoMsg = VIDEO_MSG_ERROR;
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - FATAL: No Video Device Indication from AP");
        break;
      }

      //TBD: State check to ignore response

      if ((pResp->video_device == QCE_QMI_VIDEO_RECORDER_V01) && ((pVideoData->iVideoDevice & VIDEO_DEV_RECORDER) == VIDEO_DEV_RECORDER))
      {
        pVideoData->iVideoDevice -= VIDEO_DEV_RECORDER;
        pVideoData->iRecorderState = eVideoStateNotReady;
        pRecorderCallback = (QP_VIDEO_CALLBACK)pVideoData->pRecorderCallback;
      }
      if ((pResp->video_device == QCE_QMI_VIDEO_PLAYER_V01) && ((pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER))
      {
        pVideoData->iVideoDevice -= VIDEO_DEV_PLAYER;
        pVideoData->iPlayerState = eVideoStateNotReady;
        pPlayerCallback = (QP_VIDEO_CALLBACK)pVideoData->pPlayerCallback;
      }

      if ((pVideoData->iRecorderState == eVideoStateNotReady) && (pVideoData->iPlayerState == eVideoStateNotReady))
      {
        bIsCleanup = QP_TRUE;
      }
      break;
    }
  case QC_IMS_QMI_VIDEO_CODEC_CONFIG_RESP:
    {
      ims_video_codec_config_resp* pResp = (ims_video_codec_config_resp*)(&(((ims_qmi_msg_info_u_type*)pMsgDataStruct)->ims_qmi_data.video_info_struct.video_data.codec_config_resp));

      IMS_MSG_HIGH_0(LOG_IMS_DPL, "qpVideoQmiHandler: QC_IMS_QMI_VIDEO_CODEC_CONFIG_RESP");

      IMS_MSG_HIGH(LOG_IMS_DPL,"qpVideoQmiHandler:QMI ResType: %d VDev:%d-%d",pResp->eQmiResponseType,pResp->iIncludeVideoDevice,pResp->video_device);

      if (pResp->eQmiResponseType == QC_QMI_RESPONSE_SUCCESS)
      {
        iVideoMsg = VIDEO_MSG_CODEC_CHANGED;
      }
      else
      {
        iVideoMsg = VIDEO_MSG_ERROR;
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - VIDEO_INTIALIZED FAILED");
      }

      if (pResp->iIncludeVideoDevice == 0)
      {
        iVideoMsg = VIDEO_MSG_ERROR;
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler - FATAL: No Video Device Indication from AP");
        break;
      }

      IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpVideoQmiHandler pVideoData->iVideoDevice = %x",pVideoData->iVideoDevice);
      IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpVideoQmiHandler pVideoData->pRecorderCallback = %x",pVideoData->pRecorderCallback);
      IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpVideoQmiHandler pVideoData->pPlayerCallback = %x",pVideoData->pPlayerCallback);

      if ((pResp->video_device == QCE_QMI_VIDEO_RECORDER_V01) && ((pVideoData->iVideoDevice & VIDEO_DEV_RECORDER) == VIDEO_DEV_RECORDER))
      {
        IMS_MSG_HIGH_0(LOG_IMS_DPL,"qpVideoQmiHandler set pRecorderCallback");

        if ((pVideoData->iRecorderState == eVideoStateIdle) || (pVideoData->iRecorderState == eVideoStateActive))
        {
          pRecorderCallback = (QP_VIDEO_CALLBACK)pVideoData->pRecorderCallback;
        }
        else
        {
          IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoQmiHandler - Wrong Rec state: %x", pVideoData->iRecorderState);
        }
      }
      if ((pResp->video_device == QCE_QMI_VIDEO_PLAYER_V01) && ((pVideoData->iVideoDevice & VIDEO_DEV_PLAYER) == VIDEO_DEV_PLAYER))
      {
        IMS_MSG_HIGH_0(LOG_IMS_DPL,"qpVideoQmiHandler set pPlayerCallback");

        if ((pVideoData->iPlayerState == eVideoStateIdle) || (pVideoData->iPlayerState == eVideoStateActive))
        {
          pPlayerCallback = (QP_VIDEO_CALLBACK)pVideoData->pPlayerCallback;
        }
        else
        {
          IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoQmiHandler - Wrong Player state: %x", pVideoData->iPlayerState);
        }
      }
    }
    break;
  case QC_IMS_QMI_VIDEO_VIDEO_BIT_RATE_ADAPT_RESP:
    {
      IMS_MSG_HIGH_0(LOG_IMS_DPL, "qpVideoQmiHandler: QC_IMS_QMI_VIDEO_VIDEO_BIT_RATE_ADAPT_RESP");
    }
    break;
  case QC_IMS_QMI_VIDEO_H264_IDR_GENERATE_RESP:
    {
      IMS_MSG_HIGH_0(LOG_IMS_DPL, "qpVideoQmiHandler: QC_IMS_QMI_VIDEO_H264_IDR_GENERATE_RESP");
    }
    break;
  default:
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoQmiHandler - Unhandled VideoMsg: %x", eQmiMsgID);
      return;
    }	  

  }

  if (QP_NULL != pRecorderCallback)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpVideoQmiHandler call pRecorderCallback");
    (*pVideoData->pRecorderCallback)(iVideoMsg, QP_NULL, 0, pRecorderUserData);
  }
  if (QP_NULL != pPlayerCallback)
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpVideoQmiHandler call pPlayerCallback: Msg: %x",iVideoMsg);
    (*pVideoData->pPlayerCallback)(iVideoMsg, QP_NULL, 0, pPlayerUserData);
  }

  if (QP_TRUE == bIsCleanup)
  {
    if (pVideoData->iVolFrame.pData)
    {
      qpDplFree(MEM_IMS_DPL,pVideoData->iVolFrame.pData);
      pVideoData->iVolFrame.pData       = NULL;
      pVideoData->iVolFrame.iDataLen    = 0;
      pVideoData->iVolFrame.iMaxBuffLen = 0;
    }
    pVideoData->pRecorderCallback = QP_NULL;
    pVideoData->pPlayerCallback   = QP_NULL;
  }
#endif
}


/*-------------------------------------------------------------------
IsRecorderActive:
This function will check the state of the recorder and if the recorder is
is active then it will return QP_TRUE else it will return QP_FALSE.
-------------------------------------------------------------------*/
QPBOOL IsRecorderActive(void* clientData)
{
  qpVideoData* pVideoData = (qpVideoData*)clientData;

  if(pVideoData != QP_NULL)
  {
    if(pVideoData->iRecorderState == eVideoStateActive)
    {
      return QP_TRUE;
    }
  }
  return QP_FALSE;

}

QPVOID qpVideoReleaseBuffer(QPVOID* n_pCodecFrame, QPUINT32 n_iEncodeFramesAvailable)
{
  // Not required in Brew implementation
  QP_UNUSED(n_pCodecFrame);
  QP_UNUSED(n_iEncodeFramesAvailable);
  return;
}

QP_EXPORT_C QPE_VIDEO_ERROR qpVideoInitFrameCallback(QP_VIDEO_DESC tVideoDescriptor,
                                                     QP_VIDEO_CALLBACK tVideoCallback, 
                                                     QPVOID* pUserData,
                                                     QPE_VIDEO_DEV eVDev)
{
  return VIDEO_ERROR_UNKNOWN;
}

QP_IMPORT_C QPE_VIDEO_ERROR qpVideoGenerateVolHeader(QP_VIDEO_CONFIG tCodecConfig, QPCHAR* pVolHeader, QPUINT16* pVolHeaderLen)
{
  return VIDEO_ERROR_UNKNOWN;
}


QPE_VIDEO_ERROR qpVideoStartPreview(QP_VIDEO_DESC tVideoDescriptor, QPE_VIDEO_PREV_SIZE eVPrevSize,
                                    QPINT32 iXPos, QPINT32 iYPos)
{
  return VIDEO_ERROR_UNKNOWN;
}

QPE_VIDEO_ERROR qpVideoStopPreview(QP_VIDEO_DESC tVideoDescriptor)
{
  return VIDEO_ERROR_UNKNOWN;
}


/*-------------------------------------------------------------------
qpVideoStartSaving:
This function will first see if the required resource is acquired, if it is
acquired then it will start saving the video to local file system, else will
call the acquire resource method and then on acquiring the resource will start
saving.
-------------------------------------------------------------------*/
QPE_VIDEO_ERROR qpVideoStartSaving(QP_VIDEO_DESC tVideoDescriptor, QPCHAR* pFileName, QPUINT32 iTimeDuration)
{
  return VIDEO_ERROR_UNKNOWN;
}

/*-------------------------------------------------------------------
qpVideoStopSaving:
This function will be used to stop the saving of video to local file.
this can be called by the end user of this api or when duration of
saving as specified by the user is reached or when the local storage
area is full.
-------------------------------------------------------------------*/
QPE_VIDEO_ERROR qpVideoStopSaving(QP_VIDEO_DESC tVideoDescriptor)
{
  return VIDEO_ERROR_UNKNOWN;
}

QPE_VIDEO_ERROR qpDplVideoSetCameraAttributes(QP_VIDEO_DESC tVideoDescriptor, QP_CAMERA_SETTINGS* pCameraAttributes)
{
  return VIDEO_ERROR_UNKNOWN;
}

QPE_VIDEO_ERROR qpDplVideoGetCameraAttributes(QP_VIDEO_DESC tVideoDescriptor, QP_CAMERA_SETTINGS* pCameraAttributes)
{
  return VIDEO_ERROR_UNKNOWN;
}

QPE_VIDEO_ERROR qpDplVideoSetZoom(QP_VIDEO_DESC tVideoDescriptor, QPUINT8 iZoomValue)
{
  return VIDEO_ERROR_UNKNOWN;
}

QPE_VIDEO_ERROR qpDplVideoGetZoom(QP_VIDEO_DESC tVideoDescriptor, QPUINT8* pZoomValue)
{
  return VIDEO_ERROR_UNKNOWN;
}

QPE_VIDEO_ERROR qpDplSetAmbientLighting(QP_VIDEO_DESC tVideoDescriptor, QPUINT16 eAmbientValue)
{
  return VIDEO_ERROR_UNKNOWN;
}


QPE_VIDEO_ERROR qpDplSetActiveCamera(QPE_ACTIVE_CAMERA eActiveCamera)
{
  QPE_VIDEO_ERROR retVal = VIDEO_ERROR_UNKNOWN;
  return retVal;
}

QPE_VIDEO_ERROR qpDplGetActiveCamera(QPE_ACTIVE_CAMERA* peActiveCamera)
{
  QPE_VIDEO_ERROR retVal = VIDEO_ERROR_UNKNOWN;
  return retVal;
}

QPBOOL qpDplVideoSuspendCamera(QP_VIDEO_DESC tVideoDescriptor)
{
  return QP_FALSE;
}

QPBOOL qpDplVideoResumeCamera(QP_VIDEO_DESC tVideoDescriptor)
{
  return QP_FALSE;
}

QPVOID qpDplVideoSetQpValue(QPUINT8 iQpValue)
{
}


//#endif //FEATURE_IMS_NO_VIDEO

