/**
	Auto Generated File
	File: custom_audproc_appi_topo.h

	Header file for adding custom static modules into database.
	custom_appi_integration.py in <ROOT>\hap\integration\audproc\build
	will generate this file when OEM_ROOT is defined.
*/

/* =======================================================================

Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==========================================================================*/
#ifndef __CUSTOM_AUDPROC_APPI_TOPO_H__
#define __CUSTOM_AUDPROC_APPI_TOPO_H__

/*------------------------------------------------------------------------
 * Includes
 *------------------------------------------------------------------------*/
#include "audproc_topology_db.h"
#include "AudDynaPPSvc.h"
#include "adsp_amdb_static.h"

/*------------------------------------------------------------------------
 * Include files of APPI modules
 *------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * Function Declarations
 *------------------------------------------------------------------------*/
 ADSPResult hap_AddCustomStaticTopologiesAndModules(void);
 uint32_t hap_topo_init_get_custom_KCPS(const uint32_t module_id);

/*------------------------------------------------------------------------
 * Define KCPS of custom static APPI module
 *------------------------------------------------------------------------*/


#endif // #ifdef __CUSTOM_AUDPROC_APPI_TOPO_H__ 
