/**
	Auto Generated File
	File: custom_audproc_db_if.h

	Header file contains all the modules and topologies information.
	custom_appi_integration.py in <ROOT>\hap\integration\audproc\build
	will generate this file when OEM_ROOT is defined.
*/

/*==========================================================================
Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==========================================================================*/
#ifndef __CUSTOM_AUDPROC_DB_IF_H__
#define __CUSTOM_AUDPROC_DB_IF_H__

/*--------------------------------------------------------------------------
 * Includes 
 * -------------------------------------------------------------------------*/
#include "custom_audproc_appi_topo.h"

/*--------------------------------------------------------------------------
 * Module Definition Structure 
 * -------------------------------------------------------------------------*/
typedef struct _module_definition_t
{
	uint32_t module_id;
	appi_getsize_f getsize_f;
	appi_init_f init_f;
}module_definition_t;

/*--------------------------------------------------------------------------
 * Array Of Module Definition For All Custom Static Modules 
 * -------------------------------------------------------------------------*/
const module_definition_t audproc_custom_module_definitions[] =
{
};

/*--------------------------------------------------------------------------
 * KCPS lookup table structure 
 * -------------------------------------------------------------------------*/
struct CUSTOM_KCPS_LUT_t
{
	uint32_t module_id;
	uint32_t KCPS;
};

/*
 * KCPS lookup table for all custom static audio processing modules
 */
static const CUSTOM_KCPS_LUT_t Custom_KCPS_info[] =
{
};

/*-------------------------------------------------------------------------- 
 * Custom Static POPP Topologies
 *--------------------------------------------------------------------------*/ 
static const audproc_topology_definition_t audproc_custom_static_topos_popp[] = 
{
};

/*-------------------------------------------------------------------------- 
 * Custom Static POPREP Topologies
 *--------------------------------------------------------------------------*/ 
static const audproc_topology_definition_t audproc_custom_static_topos_poprep[] = 
{
};

/*-------------------------------------------------------------------------- 
 * Custom Static COPP Topologies
 *--------------------------------------------------------------------------*/ 
static const audproc_topology_definition_t audproc_custom_static_topos_copp[] = 
{
};

/*-------------------------------------------------------------------------- 
 * Custom Static COPREP Topologies
 *--------------------------------------------------------------------------*/ 
static const audproc_topology_definition_t audproc_custom_static_topos_coprep[] = 
{
};

#endif //#ifdef __CUSTOM_AUDPROC_DB_IF_H__ 
