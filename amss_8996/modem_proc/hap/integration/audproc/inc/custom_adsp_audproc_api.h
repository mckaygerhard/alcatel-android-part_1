/**
	Auto Generated File
	File: custom_adsp_audproc_api.h

	Header file for defining the Topology ID's, Module Id's.
	custom_appi_integration.py in <ROOT>\hap\integration\audproc\build
	will generate this file when OEM_ROOT is defined.
*/

/* =======================================================================

Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==========================================================================*/
#ifndef __CUSTOM_ADSP_AUDPROC_API_H__
#define __CUSTOM_ADSP_AUDPROC_API_H__

/**
 * Custom Static Audio Topologies, Maximum supported topologies: 128
 * To acquire range of topology ID's, email support.cdmatech@qualcomm.com
 */

/**
 * Define Module ID for the custom static audio processing modules.
 * Define Param ID  for the processing modules in respective APPI header file.
 * Param IDs are needed by unit testing also hence these values need to be defined in the
 * APPI header file.
 * To acquire range of unique Module ID's and Param ID's, email support.cdmatech@qualcomm.com
 */


#endif // #ifdef __CUSTOM_ADSP_AUDPROC_API_H__ 
