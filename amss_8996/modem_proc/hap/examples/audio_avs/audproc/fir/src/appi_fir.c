/*==============================================================================
  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include <stdio.h>
#include <string.h>
#include <hexagon_protos.h>

#include "appi_fir.h"
#include "appi_util.h"
#include "fir_block.h"
#include "Elite_pcm_ch_defs.h"
#include "HAP_farf.h"

#define s32_min_s32_s32(var1, var2)  Q6_R_min_RR(var1,var2)

#define TEMP_BUF_SIZE_IN_SAMPLES 160

typedef struct {
  const appi_vtbl_t* vtbl;
  uint32_t num_channels;
  uint32_t enable_flag;
  int16_t* coeff_ptr;
  int16_t* temp_lin_ptr;
  int16_t* temp_rin_ptr;
  uint32_t num_taps;
  uint32_t max_taps;
  uint32_t shift_fac;
} appi_fir_struct;

/*------------------------------------------------------------------------
  Function name: is_supported_media_type
  Verifies the supported media type of a module
 * -----------------------------------------------------------------------*/
static bool_t is_supported_media_type(const appi_format_t* format_ptr)
{
  if (format_ptr->bits_per_sample != 16) {
    FARF(ERROR, "<<is_supported_media_type>> Only 16 bit data supported. Received %lu",
         format_ptr->bits_per_sample);
    return FALSE;
  }

  if (format_ptr->data_is_interleaved) {
    FARF(ERROR, "<<is_supported_media_type>> Interleaved data not supported");
    return FALSE;
  }

  if (!format_ptr->data_is_signed) {
    FARF(ERROR, "<<is_supported_media_type>> Unsigned data not supported");
    return FALSE;
  }

  switch(format_ptr->num_channels) {
    case 0:
      FARF(ERROR, "<<is_supported_media_type>> Zero channels passed");
      return FALSE;
    case 1:
      if (PCM_CHANNEL_C != format_ptr->channel_type[0]) {
        FARF(ERROR, "<<is_supported_media_type>> Unsupported channel mapping.");
        return FALSE;
      }
      break;
    case 2:
      if ((PCM_CHANNEL_L != format_ptr->channel_type[0]) ||
          (PCM_CHANNEL_R != format_ptr->channel_type[1])) {
        FARF(ERROR, "<<is_supported_media_type>> Unsupported channel mapping.");
        return FALSE;
      }
      break;
    default:
      FARF(ERROR, "<<is_supported_media_type>> Only upto 2 channels supported. Received %lu.",
           format_ptr->num_channels);
      return FALSE;
  }

  return TRUE;
}

/*------------------------------------------------------------------------
  Function name: appi_fir_reinit
  Reinitialize the APPI library. This function can optionally allocate
  memory. The state is dependent on the media type information, such as the
  sampling rate.
 * -----------------------------------------------------------------------*/
static ADSPResult appi_fir_reinit(
   appi_t* appi_ptr,
   const appi_format_t* in_format_ptr,
   appi_format_t* out_format_ptr,
   appi_buf_t* info_ptr)
{
  if (NULL == appi_ptr || NULL == in_format_ptr || NULL == out_format_ptr) {
    FARF(ERROR, "<<reinit>> Received bad pointer, 0x%lx, 0x%lx, 0x%lx                ",
         (uint32_t)appi_ptr, (uint32_t)in_format_ptr,
         (uint32_t)out_format_ptr);
    return ADSP_EUNEXPECTED;
  }

  if (!is_supported_media_type(in_format_ptr)) {
    return ADSP_EUNSUPPORTED;
  }

  appi_fir_struct* me = (appi_fir_struct*)(appi_ptr);

  *out_format_ptr = *in_format_ptr;
  // Set output data format (determined by internal algorithm)
  out_format_ptr->num_channels = in_format_ptr->num_channels;
  out_format_ptr->bits_per_sample = in_format_ptr->bits_per_sample;
  out_format_ptr->sampling_rate = in_format_ptr->sampling_rate;
  out_format_ptr->data_is_signed = in_format_ptr->data_is_signed;
  out_format_ptr->data_is_interleaved = in_format_ptr->data_is_interleaved;

  // Copy format to internal structure
  me->num_channels = in_format_ptr->num_channels;

  // Check for support
  if (1 == in_format_ptr->data_is_interleaved ||
      2 < in_format_ptr->num_channels) {
    FARF(HIGH, "<<reinit>> Data format unsupported                                  ");
    return ADSP_EUNSUPPORTED;
  }

  FARF(HIGH, "--------------------------------------------------------------------");
  FARF(HIGH, "<<reinit>> Output Format set to %6lu,%6lu,%6lu,%6lu,%6lu  ",
       out_format_ptr->num_channels, out_format_ptr->bits_per_sample,
       out_format_ptr->sampling_rate, out_format_ptr->data_is_signed,
       out_format_ptr->data_is_interleaved);
  FARF(HIGH, "--------------------------------------------------------------------");

  return ADSP_EOK;
}

/*------------------------------------------------------------------------
  Function name: appi_fir_process
  Processes an input buffer and generates an output buffer.
  Notes;
  - No steady state debug messages (prints) allowed in this function
 * -----------------------------------------------------------------------*/
static ADSPResult appi_fir_process(
   appi_t* appi_ptr,
   const appi_buflist_t* input,
   appi_buflist_t* output,
   appi_buf_t* info_ptr)
{
  ADSPResult result = ADSP_EOK;
  appi_fir_struct* me = (appi_fir_struct*)(appi_ptr);

  // First verify that all pointers sent in are good.
  // The macro used does this only on simulation and not
  // on target, to not affect performance
  QURT_ELITE_ASSERT(me);
  QURT_ELITE_ASSERT(input);
  QURT_ELITE_ASSERT(output);
  QURT_ELITE_ASSERT(me->num_channels <= input->bufs_num);
  QURT_ELITE_ASSERT(me->num_channels <= output->bufs_num);

  // Data pointers and sizes
  // Sample count from one channel should be sufficient
  int16_t* lin_ptr = NULL;
  int16_t* rin_ptr = NULL;
  int16_t* lout_ptr = NULL;
  int16_t* rout_ptr = NULL;

  uint32_t num_samples = s32_min_s32_s32(input->buf_ptr[0].actual_data_len >> 1,
                                         output->buf_ptr[0].max_data_len >> 1);

  lin_ptr  = (int16_t*)input->buf_ptr[0].data_ptr;
  lout_ptr = (int16_t*)output->buf_ptr[0].data_ptr;

  if (2 == me->num_channels) {
    rin_ptr  = (int16_t*)input->buf_ptr[1].data_ptr;
    rout_ptr = (int16_t*)output->buf_ptr[1].data_ptr;
  }

  if (1 == me->enable_flag) {
    // Primary processing
    uint32_t offset_into_inbuf = 0;
    uint32_t num_samples_for_this_iteration = 0;
    uint32_t rem_num_samples = num_samples;

    do {
      num_samples_for_this_iteration = s32_min_s32_s32(rem_num_samples,
                                                       TEMP_BUF_SIZE_IN_SAMPLES);
      memmove((me->temp_lin_ptr + me->num_taps - 1),
              (lin_ptr + offset_into_inbuf),
              num_samples_for_this_iteration * sizeof(int16_t));

      fir_block(me->temp_lin_ptr, me->coeff_ptr,
                me->num_taps, num_samples_for_this_iteration,
                me->shift_fac, lout_ptr + offset_into_inbuf);
      // Copy Filter States
      memmove(me->temp_lin_ptr,
              me->temp_lin_ptr + num_samples_for_this_iteration,
              ((me->num_taps - 1) * sizeof(int16_t)));

      if (2 == output->bufs_num) {
        memmove((me->temp_rin_ptr + me->num_taps - 1),
                (rin_ptr + offset_into_inbuf),
                num_samples_for_this_iteration * sizeof(int16_t));

        fir_block(me->temp_rin_ptr, me->coeff_ptr,
                  me->num_taps, num_samples_for_this_iteration,
                  me->shift_fac, rout_ptr + offset_into_inbuf);

        // Copy Filter States
        memmove(me->temp_rin_ptr,
                me->temp_rin_ptr + num_samples_for_this_iteration,
                ((me->num_taps - 1) * sizeof(int16_t)));

      }

      // Check if any more input data is left to be processed
      // Update Offset into Input buffer, so we begin reading from that
      // point in the next iteration (if any).
      offset_into_inbuf += num_samples_for_this_iteration;
      rem_num_samples -= num_samples_for_this_iteration;
    }
    while (rem_num_samples);

  } else {
    if (input != output) {
      memcpy(output->buf_ptr[0].data_ptr, input->buf_ptr[0].data_ptr,
             num_samples << 1);
      if (2 == output->bufs_num) {
        memcpy(output->buf_ptr[1].data_ptr, input->buf_ptr[1].data_ptr,
               num_samples << 1);
      }
    }
  }

  // Book keeping variables
  output->buf_ptr[0].actual_data_len = num_samples << 1;
  if (2 == output->bufs_num) {
    output->buf_ptr[1].actual_data_len = num_samples << 1;
  }

  return result;
}

/*------------------------------------------------------------------------
  Function name: appi_fir_end
  Returns the library to the uninitialized state and frees the memory that
  was allocated by Init(). This function also frees the virtual function
  table.
 * -----------------------------------------------------------------------*/
static ADSPResult appi_fir_end(
   appi_t* appi_ptr)
{
  if (NULL == appi_ptr) {
    FARF(ERROR, "<<end>> received bad pointer, 0x%lx                                 ",
         (uint32_t)appi_ptr);
    return ADSP_EUNEXPECTED;
  }
  appi_fir_struct* me = (appi_fir_struct*)(appi_ptr);

  me->coeff_ptr = NULL;
  me->temp_lin_ptr = NULL;
  me->temp_rin_ptr = NULL;
  me->vtbl = NULL;

  FARF(HIGH, "--------------------------------------------------------------------");
  FARF(HIGH, "<<end>> Free memory and vtbl                                        ");
  FARF(HIGH, "--------------------------------------------------------------------");

  return ADSP_EOK;
}

/*------------------------------------------------------------------------
  Function name: appi_fir_set_param
  Sets either a parameter value or a parameter structure containing multiple
  parameters. In the event of a failure, the appropriate error code is
  returned.
 * -----------------------------------------------------------------------*/
static ADSPResult appi_fir_set_param(
   appi_t* appi_ptr,
   uint32_t param_id,
   const appi_buf_t* params_ptr)
{
  if (NULL == appi_ptr) {
    FARF(ERROR, "<<end>> received bad pointer, 0x%lx                                 ",
         (uint32_t)appi_ptr);
    return ADSP_EUNEXPECTED;
  }

  ADSPResult result = ADSP_EOK;
  appi_fir_struct* me = (appi_fir_struct*)(appi_ptr);

  switch(param_id) {
    case APPI_PARAM_ID_ALGORITHMIC_RESET:
    {
      uint32_t procblock_size = TEMP_BUF_SIZE_IN_SAMPLES + me->max_taps;
      memset(me->temp_lin_ptr, 0, align_to_8_byte(procblock_size *
                                                  sizeof(int16_t)));
      memset(me->temp_rin_ptr, 0, align_to_8_byte(procblock_size *
                                                  sizeof(int16_t)));

      FARF(HIGH, "--------------------------------------------------------------------");
      FARF(HIGH, "<<set_param>> ID = 0x%08lx Reset Algorithm                       ", param_id);
      break;
    }

    case PARAM_ID_FIR_FILTER_ENABLE:
    {
      if (params_ptr->actual_data_len >= sizeof(fir_filter_enable_cfg_t)) {
        fir_filter_enable_cfg_t* cfg_ptr = (fir_filter_enable_cfg_t*)(params_ptr->data_ptr);
        //set FIR enable flag
        me->enable_flag = cfg_ptr->enable_flag;
        FARF(HIGH, "--------------------------------------------------------------------");
        FARF(HIGH, "<<set_param>> Enable/Disable %lu                                      ", me->enable_flag);
      } else {
        FARF(ERROR, "<<set_param>> Bad param size %lu                                    ",
             params_ptr->actual_data_len);
        result = ADSP_ENEEDMORE;
      }
      break;
    }


    case PARAM_ID_FIR_FILTER_SHIFT:
    {
      if (params_ptr->actual_data_len >= sizeof(fir_filter_shift_cfg_t)) {
        fir_filter_shift_cfg_t* cfg_ptr = (fir_filter_shift_cfg_t*)(params_ptr->data_ptr);
        //set FIR enable flag
        me->shift_fac = cfg_ptr->shift_fac;
        FARF(HIGH, "--------------------------------------------------------------------");
        FARF(HIGH, "<<set_param>> Shift factor %12lu                             ",
             me->shift_fac);
      } else {
        FARF(ERROR,"<<set_param>> Bad param size %12lu                           ",
             params_ptr->actual_data_len);
        result = ADSP_ENEEDMORE;
      }
      break;
    }

    case PARAM_ID_FIR_FILTER_PARAMS:
    {
      if (params_ptr->actual_data_len >= sizeof(fir_filter_cfg_params_t)) {
        fir_filter_cfg_params_t* cfg_ptr = (fir_filter_cfg_params_t*)(params_ptr->data_ptr);

        if (cfg_ptr->num_taps > me->max_taps) {
          FARF(HIGH, "--------------------------------------------------------------------");
          FARF(HIGH,  "APPI Example FIR Requesting %d bands, memory allocated only for %lu bands",
               cfg_ptr->num_taps, me->max_taps);
          return ADSP_EUNSUPPORTED;
        }

        me->num_taps = cfg_ptr->num_taps;
        int16_t* cfg_coeff_ptr = (int16_t*)(((char*)params_ptr->data_ptr) +
                                            sizeof(fir_filter_cfg_params_t));
        memcpy(me->coeff_ptr, cfg_coeff_ptr, sizeof(int16_t) * cfg_ptr->num_taps);
        FARF(HIGH, "--------------------------------------------------------------------");
        FARF(HIGH, "<<set_param>> Filter Params for %8lu taps                       ", me->num_taps);
        FARF(HIGH, "Printing first 5 coeffs: %6d, %6d, %6d, %6d, %6d     ",
             me->num_taps, me->coeff_ptr[0], me->coeff_ptr[1],
             me->coeff_ptr[2], me->coeff_ptr[3], me->coeff_ptr[4]);
      } else {
        FARF(ERROR, "<<set_param>> Bad param size %lu                                    ",
             params_ptr->actual_data_len);
        result = ADSP_ENEEDMORE;
      }
      break;
    }


    default:
    {
      FARF(ERROR, "<<set_param>> Unsupported param ID 0x%lx                            ",
           param_id);
      result = ADSP_EUNSUPPORTED;
    }
      break;
  }

  return result;
}

/*------------------------------------------------------------------------
  Function name: appi_fir_get_param
  Gets either a parameter value or a parameter structure containing multiple
  parameters. In the event of a failure, the appropriate error code is
  returned.
 * -----------------------------------------------------------------------*/
static ADSPResult appi_fir_get_param(
   appi_t* appi_ptr,
   uint32_t param_id,
   appi_buf_t* params_ptr)
{
  if (NULL == appi_ptr) {
    FARF(ERROR, "<<end>> received bad pointer, 0x%lx                                 ",
         (uint32_t)appi_ptr);
    return ADSP_EUNEXPECTED;
  }

  ADSPResult result = ADSP_EOK;
  appi_fir_struct* me = (appi_fir_struct*)(appi_ptr);

  switch(param_id) {
    case APPI_PARAM_ID_ALGORITHMIC_DELAY:
    {
      uint32_t* delay_ptr = (uint32_t*)(params_ptr->data_ptr);
      *delay_ptr = me->num_taps - 1;
      params_ptr->actual_data_len = sizeof(uint32_t);

      FARF(HIGH, "--------------------------------------------------------------------");
      FARF(HIGH, "<<get_param>> ID = 0x%08lx Delay = %6ld                        ", param_id, *delay_ptr);
      FARF(HIGH, "--------------------------------------------------------------------");
      break;
    }

    case PARAM_ID_FIR_FILTER_ENABLE:
    {
      if (params_ptr->max_data_len >= sizeof(fir_filter_enable_cfg_t)) {
        fir_filter_enable_cfg_t* cfg_ptr = (fir_filter_enable_cfg_t*)(params_ptr->data_ptr);
        cfg_ptr->enable_flag = me->enable_flag;
        params_ptr->actual_data_len = sizeof(fir_filter_enable_cfg_t);
        FARF(HIGH, "<<get_param>> Enable/Disable %lu                                    ",
             cfg_ptr->enable_flag);
      } else {
        FARF(ERROR, "<<get_param>> Bad param size %lu                                    ",
             params_ptr->max_data_len);
        result = ADSP_ENEEDMORE;
      }
      break;
    }

    case PARAM_ID_FIR_FILTER_SHIFT:
    {
      if (params_ptr->max_data_len >= sizeof(fir_filter_shift_cfg_t)) {
        fir_filter_shift_cfg_t* cfg_ptr = (fir_filter_shift_cfg_t*)(params_ptr->data_ptr);
        cfg_ptr->shift_fac = me->shift_fac;
        params_ptr->actual_data_len = sizeof(fir_filter_shift_cfg_t);
        FARF(HIGH, "<<get_param>> shift factor %lu                                              ",
             cfg_ptr->shift_fac);
      } else {
        FARF(ERROR, "<<get_param>> Bad param size %lu                                    ",
             params_ptr->max_data_len);
        result = ADSP_ENEEDMORE;
      }
      break;
    }

    case PARAM_ID_FIR_FILTER_PARAMS:
    {
      if (params_ptr->max_data_len >= sizeof(fir_filter_cfg_params_t)) {
        fir_filter_cfg_params_t* cfg_ptr = (fir_filter_cfg_params_t*)(params_ptr->data_ptr);

        cfg_ptr->num_taps = me->num_taps;
        int32_t* cfg_coeff_ptr = (int32_t*)(((char*)params_ptr->data_ptr) +
                                            sizeof(fir_filter_cfg_params_t));
        memcpy(cfg_coeff_ptr, me->coeff_ptr, sizeof(int16_t) * cfg_ptr->num_taps);
        params_ptr->actual_data_len = sizeof(fir_filter_cfg_params_t) +
           (sizeof(int16_t) * cfg_ptr->num_taps);
      } else {
        FARF(ERROR, "<<get_param>> Bad param size %lu                                    ",
             params_ptr->max_data_len);
        result = ADSP_ENEEDMORE;
      }
      break;
    }

    case APPI_PARAM_ID_PROCESS_CHECK:
    {
      if (params_ptr->max_data_len >= sizeof(uint32_t)) {
        uint32_t* process_check_ptr = (uint32_t*)(params_ptr->data_ptr);
        *process_check_ptr = 0;
        // No need to do processing for unity gain
        if (1 == me->enable_flag) {
          *process_check_ptr = 1;
        }
        params_ptr->actual_data_len = sizeof(uint32_t);

        FARF(HIGH, "<<get_param>> ID = 0x%08lx Process check                         ", param_id);
        FARF(HIGH, "--------------------------------------------------------------------");
      } else {
        FARF(ERROR, "<<get_param>> Process check, Bad param size %6lu                  ",
             params_ptr->max_data_len);
        result = ADSP_ENEEDMORE;
      }
      break;
    }

    default:
    {
      FARF(ERROR, "<<get_param>> Get, unsupported param ID 0x%08lx                   ",
           param_id);
      result = ADSP_EUNSUPPORTED;
      break;
    }
  }

  FARF(HIGH, "APPI Example FIR Get param for 0x%8lx done                      ", param_id);
  return result;
}

/*------------------------------------------------------------------------
  Function name: appi_fir_get_input_req
  Gets the number of input samples needed to produce a certain number of
  output samples. If the library has internal buffering, it can take into
  account the state of its buffers and return the appropriate size.
  Notes;
  - No steady state debug messages (prints) allowed in this function
 * -----------------------------------------------------------------------*/
static ADSPResult appi_fir_get_input_req(
   appi_t* appi_ptr,
   const uint32_t output_size,
   uint32_t* input_size_ptr)
{
  QURT_ELITE_ASSERT(appi_ptr);
  QURT_ELITE_ASSERT(input_size_ptr);

  // This algorithm does not need any buffering
  *input_size_ptr = output_size;

  return ADSP_EOK;
}

static const appi_vtbl_t vtbl = {
  appi_fir_reinit,
  appi_fir_process,
  appi_fir_end,
  appi_fir_set_param,
  appi_fir_get_param,
  appi_fir_get_input_req
};

/*------------------------------------------------------------------------
  Function name: appi_fir_getsize
  Returns the memory required by this associated structure/algorithm.
 * -----------------------------------------------------------------------*/
ADSPResult appi_fir_getsize(
   const appi_buf_t* params_ptr,
   uint32_t* size_ptr)
{

  if (NULL == size_ptr || NULL == params_ptr) {
    FARF(ERROR, "APPI Example FIR Getsize received bad pointer, 0x%lx, 0x%lx",
         (uint32_t)params_ptr, (uint32_t)size_ptr);
    return ADSP_EUNEXPECTED;
  }

  // Indicate maximum number of coefficients that might ever be supported.
  uint32_t max_taps = *((uint32_t*)(params_ptr->data_ptr));
  uint32_t procblock_size = TEMP_BUF_SIZE_IN_SAMPLES + max_taps;

  // Each block must begin with 8-byte aligned memory. Here we have 3 blocks
  *size_ptr = align_to_8_byte(sizeof(appi_fir_struct))
     + align_to_8_byte(max_taps * sizeof(int16_t))
     + align_to_8_byte(procblock_size * sizeof(int16_t))   // For L-channel
     + align_to_8_byte(procblock_size * sizeof(int16_t));  // For R-channel


#ifdef __V_DYNAMIC__
  FARF(HIGH, "APPI FIR Dynamic Getsize done, requires %8d bytes              ", *size_ptr);
#else
  FARF(HIGH, "APPI FIR Static Getsize done, requires %8d bytes               ", *size_ptr);
#endif

  return ADSP_EOK;
}

/*------------------------------------------------------------------------
  Function name: appi_fir_init
  Initialize the APPI library. This function can allocate memory.
 * -----------------------------------------------------------------------*/
ADSPResult appi_fir_init(
   appi_t* appi_ptr,
   bool_t* is_inplace_ptr,
   const appi_format_t* in_format_ptr,
   appi_format_t* out_format_ptr,
   appi_buf_t* info_ptr)
{
  if (NULL == appi_ptr || NULL == is_inplace_ptr || NULL == in_format_ptr ||
      NULL == out_format_ptr) {
    FARF(ERROR, "APPI Example FIR Init received bad pointer, 0x%lx, 0x%lx, 0x%lx, 0x%lx",
         (uint32_t)appi_ptr, (uint32_t)is_inplace_ptr,
         (uint32_t)in_format_ptr, (uint32_t)out_format_ptr);
    return ADSP_EUNEXPECTED;
  }

  if (!is_supported_media_type(in_format_ptr)) {
    return ADSP_EUNSUPPORTED;
  }

  // Based on what has been requested by getsize function, allocate memory
  int8_t* ptr = (int8_t*)appi_ptr;

  // appi_fir_struct
  appi_fir_struct* me = (appi_fir_struct*)ptr;
  me->max_taps = *((uint32_t*)(info_ptr->data_ptr));
  me->num_taps = *((uint32_t*)(info_ptr->data_ptr));
  uint32_t procblock_size = TEMP_BUF_SIZE_IN_SAMPLES + me->max_taps;

  // Memory for Filter coefficients
  ptr += align_to_8_byte(sizeof(appi_fir_struct));
  me->coeff_ptr = (int16_t*)ptr;
  memset(me->coeff_ptr, 0, me->max_taps * sizeof(int16_t));

  // Memory for Processing block for L-channel
  ptr += align_to_8_byte(me->max_taps * sizeof(int16_t));
  me->temp_lin_ptr = (int16_t*)ptr;
  memset(me->temp_lin_ptr, 0, align_to_8_byte(procblock_size * sizeof(int16_t)));

  // Memory for Processing block for R-channel
  ptr += align_to_8_byte(procblock_size * sizeof(int16_t));
  me->temp_rin_ptr = (int16_t*)ptr;
  memset(me->temp_rin_ptr, 0, align_to_8_byte(procblock_size * sizeof(int16_t)));

  me->vtbl = &vtbl;
  me->enable_flag = 0;
  me->shift_fac = 0;

  *out_format_ptr = *in_format_ptr;
  // Set default output data format (determined by internal algorithm)
  out_format_ptr->num_channels = in_format_ptr->num_channels;
  out_format_ptr->bits_per_sample = in_format_ptr->bits_per_sample;
  out_format_ptr->sampling_rate = in_format_ptr->sampling_rate;
  out_format_ptr->data_is_signed = in_format_ptr->data_is_signed;
  out_format_ptr->data_is_interleaved = in_format_ptr->data_is_interleaved;

  // Copy format to internal structure
  me->num_channels                = in_format_ptr->num_channels;

  // Check for support
  if (1 == in_format_ptr->data_is_interleaved ||
      2 < in_format_ptr->num_channels) {
    FARF(HIGH, "<<init>> Data format unsupported                                    ");
    return ADSP_EUNSUPPORTED;
  }

  // Report if in-place is supported by this algorithm
  // If it is supported, the same buffer may be sent down
  // for optimal performance
  *is_inplace_ptr = FALSE;

#ifdef __V_DYNAMIC__
  FARF(HIGH, "APPI Passthru Dynamic Init done with outformat %3lu,%3lu,%5lu,%3lu,%3lu",
       out_format_ptr->num_channels, out_format_ptr->bits_per_sample,
       out_format_ptr->sampling_rate, out_format_ptr->data_is_signed,
       out_format_ptr->data_is_interleaved);
#else
  FARF(HIGH, "APPI Passthru Static Init done with outformat %3lu,%3lu,%5lu,%3lu,%3lu",
       out_format_ptr->num_channels, out_format_ptr->bits_per_sample,
       out_format_ptr->sampling_rate, out_format_ptr->data_is_signed,
       out_format_ptr->data_is_interleaved);
#endif

  return ADSP_EOK;
}

