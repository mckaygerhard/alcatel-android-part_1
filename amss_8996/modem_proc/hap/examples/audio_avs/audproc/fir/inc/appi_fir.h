#ifndef APPI_FIR_H
#define APPI_FIR_H
/*============================================================================*/
/**
@file appi_FIR.h

   Header file to apply an Audio Post Processor Interface compliant
   processing feature to the input signal
*/
/*==============================================================================
  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include "Elite_APPI.h"

#ifdef __cplusplus
extern "C"
{
#endif

// The numbers corresponding to these macros are just placeholders
#define PARAM_ID_FIR_FILTER_ENABLE	0x00012D03
#define PARAM_ID_FIR_FILTER_SHIFT	0x00012D04
#define PARAM_ID_FIR_FILTER_PARAMS	0x00012D05

/** @brief Structure for enabling the configuration parameter for the
    FIR filter on the Tx path.
*/
typedef struct fir_filter_enable_cfg_t fir_filter_enable_cfg_t;
struct fir_filter_enable_cfg_t {
  uint32_t enable_flag;
  /**< Enable flag: 0 = disabled; nonzero = enabled. */
};

/** \brief Structure for FIR filter coefficients for FIR module */
typedef struct fir_filter_cfg_params_t fir_filter_cfg_params_t;
struct fir_filter_cfg_params_t {
  /** \ Sequence of int16 filter coefficients */
  uint16_t num_taps;
  /** \brief Reserved field. Client must set this to zero */
  uint16_t reserved;
};

/** @brief Structure for enabling the configuration parameter for the
    FIR filter on the Tx path.
*/
typedef struct fir_filter_shift_cfg_t fir_filter_shift_cfg_t;
struct fir_filter_shift_cfg_t {
  uint32_t shift_fac;
  /**< Enable flag: 0 = disabled; nonzero = enabled. */
};

ADSPResult appi_fir_getsize(
   const appi_buf_t* params_ptr,
   uint32_t* size_ptr);

ADSPResult appi_fir_init(
   appi_t* _pif,
   bool_t* is_inplace_ptr,
   const appi_format_t* in_format_ptr,
   appi_format_t* out_format_ptr,
   appi_buf_t* info_ptr);

#ifdef __cplusplus
}
#endif

#endif // APPI_FIR_H

