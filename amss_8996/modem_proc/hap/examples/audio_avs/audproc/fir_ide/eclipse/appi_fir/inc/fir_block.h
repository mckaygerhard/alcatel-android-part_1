#ifndef FIR_BLOCK_H
#define FIR_BLOCK_H
/*============================================================================*/
/**
@file fir_block.h

   Header file to implement the Audio Post Processor Interface for 
   applying Block FIR to input signal
*/
/*==============================================================================
  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include "mmdefs.h"

#ifdef __cplusplus
extern "C" {
#endif

void fir_block(
   int16_t* in_ptr, 
   int16_t *coeff_ptr,
   int32_t num_taps,
   int32_t num_samples,
   int32_t shift_fac,
   int16_t *out_ptr);

#ifdef __cplusplus
}
#endif

#endif // FIR_BLOCK_H

