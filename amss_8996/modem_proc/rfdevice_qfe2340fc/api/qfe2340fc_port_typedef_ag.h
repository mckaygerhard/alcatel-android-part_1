#ifndef QFE2340FC_TYPEDEF_AG_H
#define QFE2340FC_TYPEDEF_AG_H


#include "comdef.h"

/*----------------------------------------------------------------------------*/
/*!
   It defines the QFE2340FC pa_port enum.
*/
typedef enum 
{
  QFE2340FC_LTE_BAND7_PORT_TX_HB1, 
  QFE2340FC_LTE_BAND30_PORT_TRX_HB2, 
  QFE2340FC_LTE_BAND38_PORT_TRX_HB3, 
  QFE2340FC_LTE_BAND40_PORT_TRX_HB2, 
  QFE2340FC_LTE_BAND41_PORT_TRX_HB3, 
  QFE2340FC_LTE_BAND41_B_PORT_TRX_HB3, 
  QFE2340FC_LTE_BAND41_C_PORT_TRX_HB3, 
  QFE2340FC_PORT_NUM, 
  QFE2340FC_PORT_INVALID, 
} qfe2340fc_pa_port_data_type;


#endif