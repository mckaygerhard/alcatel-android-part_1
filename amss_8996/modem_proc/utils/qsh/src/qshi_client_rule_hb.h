/*!
  @file
  qshi_client_rule_hb.h

  @brief
  QSH internal header file for hb functionality.
*/

/*==============================================================================

  Copyright (c) 2015 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/utils/qsh/src/qshi_client_rule_hb.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
07/11/15   ca      Initial check in.               
==============================================================================*/

#ifndef QSHI_CLIENT_RULE_HB_H
#define QSHI_CLIENT_RULE_HB_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/
#include <qshi.h>
#include <qsh_client.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/* Max number of entries in hash table */
#define QSH_CLIENT_RULE_HB_MAX_SIZE 8

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/
/*==============================================================================
	
                    INTERNAL DEFINITIONS AND TYPES
	
==============================================================================*/
/*==============================================================================
	
                    INTERNAL FUNCTIONS	
==============================================================================*/
/*==============================================================================

  FUNCTION:  qsh_client_rule_hb_find

==============================================================================*/
/*!
  @brief: 1. To allocate the mem for hash bucket if it is not allocated.
          2. To find rule in hash table. 
  @return: TRUE: if the rule is already present
           FALSE: if the rule is not present.
*/
/*============================================================================*/
boolean qsh_client_rule_hb_exists
(
  /* A pointer to rule */
  qsh_client_rule_s*     client_rule_ptr
);

/*==============================================================================

  FUNCTION:  qsh_client_rule_hb_add

==============================================================================*/
/*!
  @brief: To add rule in hash table.  
*/
/*============================================================================*/
void qsh_client_rule_hb_add
(
  /* A pointer to rule link */
  qsh_client_rule_link_s* client_rule_link_ptr
);

/*==============================================================================

  FUNCTION:  qsh_client_rule_hb_delete

==============================================================================*/
/*!
  @brief: To delete rule in hash table.
  @return: A pointer to rule.
*/
/*============================================================================*/
qsh_client_rule_link_s* qsh_client_rule_hb_delete
( 
  qsh_client_rule_link_s** head_client_rule,
  qsh_client_rule_s*  client_rule
);

/*==============================================================================

  FUNCTION:  qsh_client_rule_hb_get

==============================================================================*/
/*!
  @brief: Find the hash bucket which has the same event id 
*/
/*============================================================================*/
qsh_client_rule_link_s** qsh_client_rule_hb_get
(
  /*! client id */
  qsh_clt_e               client,

  /*! event id */
  qsh_event_id_t          id
);


#endif /* QSHI_CLIENT_RULE_HB_H */
