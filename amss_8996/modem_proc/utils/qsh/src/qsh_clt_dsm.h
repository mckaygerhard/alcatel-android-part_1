/*!
  @file
  qsh_clt_dsm.h

  @brief
  Header file for DSM client implementation.
*/

/*==============================================================================

  Copyright (c) 2014 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/utils/qsh/src/qsh_clt_dsm.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
12/17/14   mm      Initial check-in                
==============================================================================*/

#ifndef QSH_CLT_DSM_H
#define QSH_CLT_DSM_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <qsh.h>
#include <dsm.h>
#include <dsmstats.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*==============================================================================

                                FUNCTIONS

==============================================================================*/

/*==============================================================================

  FUNCTION:  qsh_clt_dsm_init

==============================================================================*/
/*!
  @brief
  Initializes client.
*/
/*============================================================================*/
void qsh_clt_dsm_init
(
  void
);

#endif /* QSH_CLT_DSM_H */
