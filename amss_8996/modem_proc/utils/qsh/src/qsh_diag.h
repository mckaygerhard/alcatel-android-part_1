#ifndef QSH_DIAG_H
  #define QSH_DIAG_H
/*!
  @file qsh_diag.h

  @brief
   QSH diag parameters.

*/

/*==============================================================================

  Copyright (c) 2015 Qualcomm Technologies, Inc. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies, Inc. and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/utils/qsh/src/qsh_diag.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
08/02/15   ca      Initial check in 
==============================================================================*/


/*==============================================================================

                           INCLUDE FILES

==============================================================================*/
#include <comdef.h>
#include <msg.h>
#include <log.h>
#include <diagpkt.h>
#include <diagcmd.h>
#include <diagdiag.h>
#include <qsh.h>
#include <qsh_client_rule.h>
#include <qsh_client.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/*! @brief
     QSH Diag action types currently supported
*/
typedef enum
{
  /*! rule action add */
  QSH_DIAG_ACTION_TYPE_ADD    = 0x1,
  /*! rule action delete */
  QSH_DIAG_ACTION_TYPE_DELETE = 0x2,
  /*! rule action cfg */
  QSH_DIAG_ACTION_TYPE_CFG    = 0x3,
  
#ifdef FEATURE_QSH_TEST_STUB  
  /* to send a notification to QSH */
  QSH_DIAG_ACTION_TYPE_TEST_NOTIFY = 0x4,
#endif /* FEATURE_QSH_TEST_STUB */
  /* action Max */
  QSH_DIAG_ACTION_TYPE_MAX    = 0xFFFFFFFF,
}qsh_diag_action_type_e;

/*!
  @brief A struct to hold the client rule info.
*/
typedef struct
{    
  /*! Generator client event info based on which the analysis needs to be run */
  qsh_client_event_subs_id_s  generator_info; 
  
  /*! Time interval in usec after which the action to be taken.
    0 implies instantaneously. Have a max value of 5 mins */
  uint32                      time_to_action_us;
  
  qsh_client_rule_params_u    params;
  
} qsh_diag_client_rule_common_s;

/*!
  @brief A struct to hold the client rule info.
*/
typedef struct
{    
  qsh_client_rule_type           type; 
  /*! No. of times this rule is in affect. No-op for delete */
  uint32                         ref_count;  
  /* Common params between add / delete */
  qsh_diag_client_rule_common_s  common_params;
  
} qsh_diag_client_rule_add_s;

/*!
  @brief A struct to hold the client rule info.
*/
typedef struct
{    
  qsh_client_rule_type           type; 
  /* Common params between add / delete */
  qsh_diag_client_rule_common_s  common_params;
  
} qsh_diag_client_rule_delete_s;

/*!
  @brief: A struct to hold the cfg data */
typedef struct
{
  /*! name of the client */
  qsh_clt_e               client;
	
  /*! client cfg data */
  qsh_client_cfg_s        cfg;
}qsh_diag_client_cfg_s;


/*! @brief
     A union to hold QSH Diag "RequestItem" request action rule */
typedef PACK(union)
{  
  /*!client rule add */
  qsh_diag_client_rule_add_s           rule_add;
  /*!client rule delete */
  qsh_diag_client_rule_delete_s        rule_delete;
  /*! client cfg */
  qsh_diag_client_cfg_s                diag_cfg; 
	
#ifdef FEATURE_QSH_TEST_STUB	
  /*! qsh notify params */
  qsh_client_event_notify_params_s     test_notify;
#endif /* FEATURE_QSH_TEST_STUB */
}qsh_diag_req_item_u;

/*! @brief
     QSH Diag "RequestItem" request payload structure
*/
typedef PACK(struct)
{
  /*! Number of args in the qxdm command */
  uint32                      num_args;
  /*! QSH diag action type: Add/Delete*/
  qsh_diag_action_type_e      action;
  /* rule / cfg */
  qsh_diag_req_item_u         req_item;
  
} qsh_diag_req_reqitem_s;

/*! @brief Prototype for callback registered by QSH */
typedef void (*qsh_diag_cb_t)(qsh_diag_req_reqitem_s *);

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*==============================================================================

  FUNCTION:  qsh_diag_init

==============================================================================*/
/*!
  @brief
  init function for qsh_diag.c file. Should not be called by clients.

  @detail
  Function registers the diagnostic call back functions for qsh

  @caller
  qsh_task_main()

*/
/*============================================================================*/
void qsh_diag_init
(
  void
);
/* qsh_diag_init() */

#endif /* QSH_DIAG_H */
