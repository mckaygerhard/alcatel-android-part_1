/*!
  @file
  qshi.h

  @brief
  Internal header file for top-level QSH state.
*/

/*==============================================================================

  Copyright (c) 2015 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/utils/qsh/src/qshi.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
10/25/15   ca      CR:922311: Enabling cmapi metric collection.
09/08/15   ca      CR:904326: Fixing compilation warnings
08/28/15   mm      CR 898854: Refactoring QSH_SNAPSHOT_ENABLED() to strip code
08/28/15   ca      CR 898746: QSH rule management feature.
08/27/15   mm      CR 895325: Fixing SNAPSHOT check introduced by CR 864729
08/13/15   mm      CR 852085: Changing default EFS mask to 0
03/30/15   mm      CR 814959: Fixing MOB compilation
02/16/14   mm      Moved qshi.h -> qshi_util.h, added top-level data structs
06/20/14   ar      Moved qsh_crit_sect_enter() and qsh_crit_sect_leave() to 
                   qsh.c file
05/23/14   mm      Added standardized analysis summary
03/12/14   vd      CR 630063: initial version           
==============================================================================*/

#ifndef QSHI_H
#define QSHI_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <limits.h>
#include <qshi_util.h>
#include <qurt.h>
#include <qsh_client_rule.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/* -----------------------------------------------------------------------------

  Generic definitions
 
----------------------------------------------------------------------------- */

#define QSH_VERSION "2.0"

/*!
  @brief
  Strings printed to display category/message type config
  
  @note
  Update the following whenever there is new category and qsh_ulog_print_info(...)
*/
#define QSH_CAT_MASK_STR "QSH cfg::category::mask:: TPUT:0x1|" \
                         "CFG:0x2|DSM:0x4|PERF:0x8|OTHER:0x10"
#define QSH_CAT_ENABLED_STR "QSH cfg::category::enabled:: TPUT:%d|" \
                         "CFG:%d|DSM:%d|PERF:%d|OTHER:%d"
#define QSH_MSG_TYPE_MASK_STR "QSH cfg::msg type::mask:: HIGH:0x1|" \
                              "MEDIUM:0x2|INFO:0x4"
#define QSH_MSG_TYPE_ENABLED_STR "QSH cfg::msg type::enabled:: HIGH:%d|" \
                                 "MED:%d|INFO:%d"

/*! 
  @brief
  Internal cookie values.
*/
#define QSH_COOKIE_8    0xDE
#define QSH_COOKIE_32   0xDEADC0DE

/*!
  @brief
  Macro to represent all SNAPSHOT-related dbg bits.
*/
#define QSH_SNAPSHOT_ENABLED_BMSK \
  ( QSH_DBG_MASK(QSH_DBG_MASK_METRIC_START) | \
    QSH_DBG_MASK(QSH_DBG_MASK_METRIC_RESTART) | \
    QSH_DBG_MASK(QSH_DBG_MASK_DUMP_ENABLE))

/*!
  @brief
  Macro to check whether or not SNAPSHOT features are enabled given current
  EFS mask.

  @note
  This should only be used after dbg EFS mask has been captured in qsh_init().
*/
#define QSH_SNAPSHOT_ENABLED() \
  ((qsh.state_int.dbg_mask.value & (QSH_SNAPSHOT_ENABLED_BMSK)) != 0)

/*!
  @brief
  EFS file used for dbg mask.
*/
#define QSH_DBG_MASK_EFS_FILE \
  "/nv/item_files/modem/utils/qsh/qsh_dbg_mask"

/*!
  @brief
  Default value of dbg mask.
*/
#define QSH_DBG_MASK_DEFAULT  \
  (0)
  
/*!
  @brief
  Enum for bits in EFS configuration.
*/
typedef enum
{
  /*! allocate memory for metrics and enable metric start */
  QSH_DBG_MASK_METRIC_START,
  
  /*! enable metric stop/start cycles (only if QSH_DBG_MASK_METRIC_START 
    is set) */
  QSH_DBG_MASK_METRIC_RESTART,
  
  /*! allocate memory for dumps and collect at err_fatal() post-flush */
  QSH_DBG_MASK_DUMP_ENABLE,
  
  /*! enable metric collection through cm api */
  QSH_DBG_MASK_CM_API_ENBALE = 0x8,
  
} qsh_dbg_mask_e;

/*!
  @brief
  Macro to check if a given bit is set in the mask.
*/
#define QSH_DBG_MASK_CHECK(e) \
  ((qsh.state_int.dbg_mask.value & QSH_DBG_MASK(e)) != 0)
  
/*!
  @brief
  Macro to get bitmask given enum.
*/
#define QSH_DBG_MASK(e) \
  (1 << (e))
  
/*!
  @brief
  Sets the bit in the mask.
*/
#define QSH_DBG_MASK_SET(mask, e) \
  (mask | QSH_DBG_MASK(e))
  
/*!
  @brief
  Clears the bit in the mask.
*/
#define QSH_DBG_MASK_CLEAR(mask, e) \
  (mask & (~QSH_DBG_MASK(e)))

/*!
  @brief
  Struct for dbg mask for readability.
*/
typedef union
{
  struct
  {
    /*! enables memory allocation for metrics and start */
    uint32  metric_start      : 1;
    
    /*! enables stop/restart for metrics */
    uint32  metric_restart    : 1;
    
    /*! enables memory allocation for dumps and collection */
    uint32  dump_enable       : 1;
    
    /*! Reserved for QSH Snap shot */
    uint32  reserved          : 5;

    /*! enables CM API for metric collection */
    uint32  cm_api            : 1;
  }       mask;
  
  /*! to pad to 32 bits */
  uint32  value;
} qsh_dbg_mask_u;

/*!
  @brief
  Struct to represent a ratio (%d/%d).
*/
typedef struct
{
  /*! numerator */
  uint16  num;

  /*! denominator */
  uint16  denom;
} qsh_ratio_s;

/*!
  @brief
  Struct to represent a 32-bit ratio (%d/%d).
*/
typedef struct
{
  /*! numerator */
  uint32  num;

  /*! denominator */
  uint32  denom;
} qsh_ratio_32_s;

/*!
  @brief
  Struct to represent latency in us (total/maximum).
*/
typedef struct
{
  qsh_timetick_t  total;
  qsh_timetick_t  max;
} qsh_latency_us_s;

/* -----------------------------------------------------------------------------

  State per client
 
----------------------------------------------------------------------------- */

/*!
  @brief
  Callback stats per client.
*/
typedef struct
{
  /*! # checks */
  qsh_ratio_s   ratio;
  
  /*! # checks per category */
  qsh_ratio_s   ratio_cat[QSH_CAT_MAX_IDX];
  
  /*! # checks which failed with msg_type = HIGH (for picking suspect) */
  uint8         count_high_failed;
  
  /*! count_high_failed per category */
  uint8         count_high_failed_cat[QSH_CAT_MAX_IDX];
} qsh_state_client_cb_stats_check_s;

/*! 
  @brief
  Callback state per client. 
  
  @note
  This is re-initialized prior to each callback.
*/
typedef struct
{
  /*! start time in timeticks per action */
  qsh_timetick_t                      time_start[QSH_ACTION_MAX_IDX];

  /*! total time in ns */
  qsh_timetick_t                      time_run_ns[QSH_ACTION_MAX_IDX];
  
  /*! check stats per cb */
  qsh_state_client_cb_stats_check_s   stats_check;
  
  /*! pointer to action done to set, or NULL */
  qsh_client_action_done_params_u *   done_params_ptr;
  
  /*! metric logs requested for this callback */
  uint32                              metric_log_req_count;
  
  /*! number of times metric_log_done() has been called for TIMER_EXPIRY */
  uint32                              metric_log_done_timer_expiry_count;
} qsh_state_client_cb_s;

/*!
  @brief
  Global dump stats per client.
*/
typedef struct
{
  /*! total size written by this client in bytes */
  uint32  size_written_bytes_total;
} qsh_state_client_dump_stats_s;

/*!
  @brief
  Global metric stats per client.
  
  @note
  This is initialized during qsh_init() and persistent indefinitely.
*/
typedef struct
{
  /*! num of times QSH_CLIENT_METRIC_TIMER_EXPIRY_ARR_MAX was exceeded */
  uint16  timer_expiry_arr_max_count_exceed;

  /*! current num of metric cfg that are periodic

    @note
    type must resolve to unsigned int or atomic op will throw an exception */
  uint32  metric_count_periodic;

  /*! current num of metric cfg that are event based 
  
    @note
    type must resolve to unsigned int or atomic op will throw an exception */
  uint32  metric_count_event;

  /*! running total of metric id count every timer expiry */
  /* @todo: come up with a better name */
  // uint32  total_periodic_metric_id_log_req;
  
  /*! num of times client was requested to log with reason:
    QSH_CLIENT_METRIC_LOG_REASON_TIMER_EXPIRY */
  uint32  log_count_periodic_req;

  /*! num of times client logs with reason:
    QSH_CLIENT_METRIC_LOG_REASON_TIMER_EXPIRY 
    
    @note
    type must resolve to unsigned int or atomic op will throw an exception */
  uint32  log_count_periodic;

  /*! num of times client logs with reason:
    QSH_CLIENT_METRIC_LOG_REASON_EVENT_INTERNAL
    
    @note
    type must resolve to unsigned int or atomic op will throw an exception */
  uint32  log_count_event;
} qsh_state_client_metric_stats_s;

/*!
  @brief
  Global stats per client for events.
*/
typedef struct
{
  /*! num of events configured for this client */
  uint32  count_cfg;

  /*! num of times events received by this client */
  uint32  count_rcvd;
  
  /*! num of times the event is logged in client's code */
  unsigned int  count_logged;
  
  /*! bitmask of which events are currently configured */
  uint32  mask;
} qsh_state_client_event_stats_s;

/*!
  @brief
  Global stats per client.
  
  @note
  This is initialized during qsh_init() and persistent indefinitely.
*/
typedef struct
{
  /*! number of words logged */
  uint16                            words_logged_count;
  
  /*! number of words dropped */
  uint16                            words_dropped_count;

  /*! ratio of # times done / # times invoked per action */
  // todo: only for 3 done required
  qsh_ratio_32_s                    cb_invoke_count[QSH_ACTION_MAX_IDX];
  
  /*! latency per action */
  qsh_latency_us_s                  cb_latency_us[QSH_ACTION_MAX_IDX];
  
  /*! dump stats */
  qsh_state_client_dump_stats_s     dump;

  /*! metric stats */
  qsh_state_client_metric_stats_s   metric;

  /*! events stats */
  qsh_state_client_event_stats_s    event;
} qsh_state_client_stats_s;

/*! An enum type to describe existance of a rule in internal data structures 
    This is a bit mask enum type. Different values should alwyas be power of 2*/
typedef enum
{ 
  /*! Rule is present in timer heap only */
  QSH_CLIENT_STATE_RULE_EXIST_NONE = 0x0,
  /*! Rule is present in timer heap only */
  QSH_CLIENT_STATE_RULE_EXIST_HEAP = 0x1,
  /*! Rule is present in HB only */
  QSH_CLIENT_STATE_RULE_EXIST_HB = 0x2,
}qsh_client_state_rule_exist_e;

/*! @brief struct for maintaing rule related info of a queue. 
*/
typedef struct qsh_client_rule_link_s qsh_client_rule_link_s;

struct qsh_client_rule_link_s
{
  /*! rule information */
  qsh_client_rule_s              rule;  

  /*! a pointer to the next rule in the hash bucket */
  qsh_client_rule_link_s  *rule_next_ptr;
};

/*!
  @brief
  State per client.
*/
typedef struct
{
  /*! registration params */
  qsh_client_reg_s            reg;
  
  /*! callback state (re-initialized prior to cb) */
  qsh_state_client_cb_s       cb;
  
  /*! stats */
  qsh_state_client_stats_s    stats;
  
#ifdef FEATURE_QSH_LEGACY
  /*! callback for legacy client */
  qsh_analysis_cb_type        legacy_cb_ptr;
#endif /* FEATURE_QSH_LEGACY */
  /*! todo: malloc the HB when the first rule is added 
    for the client */
  qsh_client_rule_link_s      **rule_hb;

} qsh_state_client_s;

/* -----------------------------------------------------------------------------

  Internal state
 
----------------------------------------------------------------------------- */

/*!
  @brief
  Internal top-level stats per callback.
*/
typedef struct
{
  qsh_ratio_s  ratio;
  qsh_ratio_s  ratio_cat[QSH_CAT_MAX_IDX];
} qsh_stats_cb_s;

/*!
  @brief
  Internal top-level stats.
*/
typedef struct
{
  /*! stats per callback */
  qsh_stats_cb_s  cb;
  
  /*! number of words dropped before QSH initialized */
  uint32          words_dropped_before_init_count;
  
  /*! number of words dropped for invalid client */
  uint32          words_dropped_invalid_client_count; 
  
  /*! number of words dropped for invalid category */
  uint32          words_dropped_invalid_category_count; 
  
  /*! number of words dropped for invalid message type */
  uint32          words_dropped_invalid_msg_type_count; 

  /*! Num of rules which are active */
  uint32          num_rules;   
} qsh_stats_s;

/*!
  @brief
  Internal state.
*/
typedef struct
{
  /*! whether or not QSH is initialized (uint32 for atomic ops) */
  uint32                initialized;
  
  /*! whether or not current context is err_fatal() */
  boolean               context_err_fatal;

  /*! whether or not current context is err_fatal_postflush */
  boolean               context_err_fatal_postflush;
  
  /*! pointer to config data structure */
  qsh_cfg_s *           cfg_ptr;
  
  /*! timetick handle */
  DalDeviceHandle *     timetick_handle;
  
  /*! next context id to assign (use qsh_context_id_get()) */
  qsh_context_id_t      context_id_next;
  
  /*! top-level mutex */
  qsh_crit_sect_s       crit_sect;
  
  /*! top-level flag for invoke state */
  boolean               invoked;
  
  /*! statistics */
  qsh_stats_s           stats;
  
  /*! dbg mask */
  qsh_dbg_mask_u        dbg_mask;
} qsh_state_int_s;

/* -----------------------------------------------------------------------------

  External state
 
----------------------------------------------------------------------------- */

/*!
  @brief
  Stats for metrics.
*/
typedef struct
{
  /*! max time taken in timer callback */
  qsh_timetick_t  timer_cb_time_max_ns;
} qsh_ext_metric_stats_s;

/*!
  @brief
  Top-level state for metrics.
*/
typedef struct
{
  /*! metric callback */
  qsh_ext_metric_stop_cb_t  stop_cb;
  
  /*! metric stats */
  qsh_ext_metric_stats_s    stats;
} qsh_state_ext_metric_s;

/*!
  @brief
  Top-level state for events.
*/
typedef struct
{
  /*! event callback */
  qsh_ext_event_cb_t  cb;
} qsh_state_ext_event_s;

/*!
  @brief
  Top-level ext state.
*/
typedef struct
{
  /*! metrics */
  qsh_state_ext_metric_s  metric;

  /*! events */
  qsh_state_ext_event_s   event;
} qsh_state_ext_s;

/* -----------------------------------------------------------------------------

  Top-level structures
 
----------------------------------------------------------------------------- */

/*!
  @brief
  Top-level data structure.
*/
typedef struct
{
  /*! state per client */
  qsh_state_client_s    state_client[QSH_CLT_MAX];
  
  /*! internal state */
  qsh_state_int_s       state_int;
  
  /*! external state */
  qsh_state_ext_s       state_ext;
} qsh_s;

/*==============================================================================

                         EXTERNAL VARIABLES

==============================================================================*/

/*! 
  @brief 
  Structure containing QSH global data.
*/
extern qsh_s qsh;

/*! ULog handle for summary prints: will point to either main QSH ulog or
  snapshot ulog */
extern ULogHandle * qsh_ulog_summary_handle_ptr;

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*==============================================================================

  FUNCTION:  qsh_init

==============================================================================*/
/*!
  @brief
  Init QSH.

  @return
  None
*/
/*============================================================================*/
void qsh_init
( 
  void
);

/*==============================================================================

  FUNCTION:  qsh_deinit

==============================================================================*/
/*!
  @brief
  Deinit QSH (needs to be called for off-target tests to pass).
  
  @return
  None
*/
/*============================================================================*/
void qsh_deinit
( 
  void
);

/*==============================================================================

  FUNCTION:  qsh_ulog_print_info

==============================================================================*/
/*!
  @brief
  Log all QSH configuration, bit mask, ...

  @return
  None
*/
/*============================================================================*/
void qsh_ulog_print_info
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_ulog_print_stats

==============================================================================*/
/*!
  @brief
  To print the QSH stats.
  
  @return
  None
*/
/*============================================================================*/
void qsh_ulog_print_stats
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_ulog_print_summary

==============================================================================*/
/*!
  @brief
  Prints a summary of the analysis.
*/
/*============================================================================*/
void qsh_ulog_print_summary
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_ulog_print_dump_header

==============================================================================*/
/*!
  @brief
  Prints dump table header.
*/
/*============================================================================*/
void qsh_ulog_print_dump_header
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_ulog_print_dump_row

==============================================================================*/
/*!
  @brief
  Prints dump table row.
*/
/*============================================================================*/
void qsh_ulog_print_dump_row
(
  qsh_state_client_s *                client_ptr,
  qsh_ext_dump_collect_s *            dump_collect_ptr,
  qsh_client_action_done_params_u *   action_done_params_ptr
);

/*==============================================================================

  FUNCTION:  qsh_ulog_print_client_reg

==============================================================================*/
/*!
  @brief
  Writes a log with the client and callback info.
*/
/*============================================================================*/
void qsh_ulog_print_client_reg
(
  qsh_clt_e   client
);

/*==============================================================================

  FUNCTION:  qsh_context_id_get

==============================================================================*/
/*!
  @brief
  Returns a unique value for this invocation.
  
  @return
  Context id
*/
/*============================================================================*/
qsh_context_id_t qsh_context_id_get
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_err_fatal_cb

==============================================================================*/
/*!
  @brief
  Callback that needs to be called during error fatal.

  @return
  None
*/
/*============================================================================*/
void qsh_err_fatal_cb
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_err_fatal_postflush_cb

==============================================================================*/
/*!
  @brief
  Callback that needs to be called during error fatal, post flush.

  @return
  None
*/
/*============================================================================*/
void qsh_err_fatal_postflush_cb
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_mask_to_idx

==============================================================================*/
/*!
  @brief
  Returns index corresponding to a single bit set in a mask.
  
  @note
  If more than one bit is set, the index corresponding to the least 
  sigificant bit set will be returned.

  @return
  Index of bit in mask, or 0 if there is no bit set.
*/
/*============================================================================*/
static inline uint32 qsh_mask_to_idx
(
  uint32  mask
)
{
  uint32 idx;
  
  for (idx = 0; idx < sizeof(uint32) * CHAR_BIT; idx ++)
  {
    if ((mask & (1 << idx)) != 0)
    {
      break;
    }
  }
  
  if (idx == sizeof(uint32) * CHAR_BIT)
  {
    return 0;
  }
  else
  {
    return idx;
  }
}

/*==============================================================================

  FUNCTION:  qsh_client_latency_calc

==============================================================================*/
/*!
  @brief
  Recalculates latency stats for this client/action. Can only be called once
  per callback.
*/
/*============================================================================*/
static inline void qsh_client_latency_calc
(
  qsh_state_client_s *  client_ptr,
  uint32                action_idx
)
{
  qsh_latency_us_s * latency_ptr;
  qsh_timetick_t latency_ns;

  QSH_ASSERT(action_idx < QSH_ACTION_MAX_IDX);

  latency_ptr = &client_ptr->stats.cb_latency_us[action_idx];
  
  latency_ns = qsh_timetick_diff(
    client_ptr->cb.time_start[action_idx],
    qsh_timetick_get());
  
  /* set new max */
  if (latency_ns > latency_ptr->max)
  {
    latency_ptr->max = latency_ns;
  }
  
  /* add this latency to total */
  latency_ptr->total += latency_ns;
  
  /* store latency */
  client_ptr->cb.time_run_ns[action_idx] = latency_ns;
}

/*==============================================================================

  FUNCTION:  qsh_client_latency_get_avg

==============================================================================*/
/*!
  @brief
  Returns average latency.
*/
/*============================================================================*/
static inline qsh_timetick_t qsh_client_latency_get_avg
(
  qsh_state_client_s *  client_ptr,
  uint32                action_idx
)
{
  qsh_latency_us_s * latency_ptr = &client_ptr->stats.
    cb_latency_us[action_idx];
    
  if (client_ptr->stats.cb_invoke_count[action_idx].num == 0)
  {
    return 0;
  }
  else
  {
    return latency_ptr->total / client_ptr->stats.
      cb_invoke_count[action_idx].num;
  }
}

/*==============================================================================

  FUNCTIONS:  qsh_crit_sect_enter
              qsh_crit_sect_leave

==============================================================================*/
/*!
  @brief
  Functions to enter/leave a critical section.

  @return
  None
*/
/*============================================================================*/
static inline void qsh_crit_sect_enter
(
  qsh_crit_sect_s *   crit_sect_ptr
)
{
  QSH_ASSERT(crit_sect_ptr != NULL);
  
  /* Do not acquire the mutex in error fatal case, since error fatal is a 
    single threaded execution. If CFM triggered the invoke analysis before
    error fatal, CFM may hold on to the mutex forever, thus resulting in a
    deadlock in error fatal handler */
    
  if (QSH_LIKELY(qsh.state_int.context_err_fatal == FALSE))
  {
#ifdef FEATURE_POSIX
    QSH_ASSERT(pthread_mutex_lock(crit_sect_ptr) == 0);
#else
    rex_enter_crit_sect((rex_crit_sect_type *) crit_sect_ptr);
#endif
  }
} /* qsh_crit_sect_enter() */

static inline void qsh_crit_sect_leave
(
  qsh_crit_sect_s *   crit_sect_ptr
)
{
  QSH_ASSERT(crit_sect_ptr != NULL);
  
  /* Do not acquire the mutex in error fatal case, since error fatal is a 
  single threaded execution. If CFM triggered the invoke analysis before
  error fatal, CFM may hold on to the mutex forever, thus resulting in a
  deadlock in error fatal handler */
  if (QSH_UNLIKELY(qsh.state_int.context_err_fatal == FALSE))
  {
#ifdef FEATURE_POSIX
    QSH_ASSERT(pthread_mutex_unlock(crit_sect_ptr) == 0);
#else
    rex_leave_crit_sect((rex_crit_sect_type *) crit_sect_ptr);
#endif
  }
} /* qsh_crit_sect_leave() */

/*==============================================================================

  FUNCTION:  qsh_align_up

==============================================================================*/
/*!
  @brief
  Aligns given memory to the given alignment in bytes, rounding up.
  
  @note
  This function is called in a loop so there is no validation of inputs.

  @return
  Aligned address
*/
/*============================================================================*/
static inline uint8 * qsh_align_up
(
  /*! pointer to memory to align */
  const uint8 *   mem_ptr,
  
  /*! alignment in bytes (should be power of 2) */
  uint32          align_bytes
)
{
  return (uint8 *) (((uint32) (mem_ptr + align_bytes - 1)) & 
    (~(align_bytes - 1)));
}

/*==============================================================================

  FUNCTION:  qsh_align_offset

==============================================================================*/
/*!
  @brief
  Aligns given memory to the given alignment in bytes and returns offset.
  
  @note
  This function is called in a loop so there is no validation of inputs.

  @return
  Offset to next boundary of alignment
*/
/*============================================================================*/
static inline uint32 qsh_align_offset
(
  /*! pointer to memory to align */
  const uint8 *   mem_ptr,
  
  /*! alignment in bytes (should be power of 2) */
  uint32          align_bytes
)
{
  return qsh_align_up(mem_ptr, align_bytes) - mem_ptr;
}

/*==============================================================================

  FUNCTION:  qsh_dump_get_next_client

==============================================================================*/
/*!
  @brief
  Returns index of next client to collect a dump, or MAX if this is the last.

  @return
  Client enum
*/
/*============================================================================*/
static inline uint32 qsh_dump_get_next_client
(
  qsh_state_client_s *  client_ptr
)
{
  uint32 next = QSH_CLT_MAX;
  
  /* increment client_ptr first to start with next client */
  for (client_ptr ++; client_ptr < &qsh.state_client[QSH_CLT_MAX]; 
    client_ptr ++)
  {
    if (client_ptr->reg.dump_info.max_size_bytes != 0)
    {
      next = client_ptr->reg.client;
      break;
    }
  }
  
  return next;
}

/*==============================================================================*/
/*!
  @brief
  Returns the pointer to the client state.

  @return
  ptr to client state
*/
/*============================================================================*/
inline qsh_state_client_s* qsh_get_client_state_pointer
(
  qsh_clt_e  client_id
)
{
  /* Validate the args */
  QSH_ASSERT(client_id > QSH_CLT_NONE && client_id < QSH_CLT_MAX);
  
  return &qsh.state_client[client_id];  
}

#endif /* QSHI_H */
