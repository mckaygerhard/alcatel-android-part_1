/*!
  @file
  qshi_client_rule_timer.h

  @brief
  QSH internal header file for timer functionality.
*/

/*==============================================================================

  Copyright (c) 2015 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/utils/qsh/src/qshi_client_rule_timer.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
09/08/15   ca      CR:904326: Fixing compilation warnings
07/11/15   ca      Initial check in.               
==============================================================================*/

#ifndef QSHI_CLIENT_RULE_TIMER_H
#define QSHI_CLIENT_RULE_TIMER_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/
#include <timer.h>
#include <qshi.h>

/*==============================================================================

                           EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/*! A structure for the timer heap info  */
typedef struct
{
  /*! ptr to the start of the heap array */
  qsh_client_rule_link_s *start_ptr;
  /*! Size of the heap array. */
  uint32                  size;
}qsh_client_rule_timer_heap_s;

/*! QSH client rule timer data struct */
/* todo: error handler when the # of rule exceed the array size */
typedef struct
{ 
  /*! A structure to hold the info about timer heap*/
  void*                         heap_inst_ptr; 
  /*! This contains the last timer expiry time 
      Time is an absolute time in micro secs */
  uint32                        last_timer_expiry_us;
  /*! timer datatype for QSH timers used primarily for 
  non deferrable timers */
  timer_group_type              timer_group;
	/*! timer object */
  timer_type                    timer;
  /* todo: Add stats */
}qsh_client_rule_timer_s;

/*! An object to maintain qsh rule timer info */
extern qsh_client_rule_timer_s qsh_client_rule_timer;


/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/
/*==============================================================================
	
                    INTERNAL DEFINITIONS AND TYPES
	
==============================================================================*/
/*==============================================================================
	
                    INTERNAL FUNCTION PROTOTYPES
	
==============================================================================*/
/*==============================================================================

FUNCTION:  qsh_client_rule_timer_expiry_process

==============================================================================*/
/*!
  @brief
  Process timer expiry.
  
  @return
  None

*/
/*============================================================================*/
void qsh_client_rule_timer_expiry_process(void);

/*==============================================================================

FUNCTION:  qsh_client_rule_timer_add

==============================================================================*/
/*!
  @brief: To add rule in timer heap.  
*/
/*============================================================================*/
void qsh_client_rule_timer_add
(
  qsh_client_rule_link_s* rule_link_ptr
);

/*==============================================================================

FUNCTION:  qsh_client_rule_timer_delete

==============================================================================*/
/*!
  @brief: To delete a rule from timer heap.  
*/
/*============================================================================*/
void qsh_client_rule_timer_delete
( 
  qsh_client_rule_link_s* rule_link_ptr
);

#endif /* QSHI_CLIENT_RULE_TIMER_H */
