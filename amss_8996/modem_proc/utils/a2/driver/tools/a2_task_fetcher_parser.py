import sys, getopt
  
# represents a bitfield of arbitrary complexity
class Field:
  name = None     # field name
  offset = None   # offset relative to parent field
  length = None   # field length in bits
  desc = None     # short description
  children = None # list of child Field objects
  values = None   # list of FieldValue objects
  
  def __init__(self):
    self.name = ""
    self.offset = 0
    self.length = 0
    self.desc = ""
    self.children = list()
    self.values = list()
    
  def parse(self, input_value, depth = 0):
    output = ""
    
    # indent according to current depth
    output += indent(depth)
    output += "[%d:%d] %s = %s (%s)" % (self.offset + self.length - 1,
                                        self.offset, self.name,
                                        hex(input_value), bin(input_value))
    
    # iterate through all possible values and see if there is a match
    for value_obj in self.values:
      if value_obj.value == input_value:
        # if there is a match, display the associated name
        output += " -> " + value_obj.name
        break
        
    # print description if set
    if self.desc != "":
      output += "\n" + indent(depth + 1) + "Desc: " + self.desc
    
    # iterate through all children and print values
    for child in self.children:
      # mask input value so the child receives relative value
      child_value = input_value >> child.offset
      child_value &= ((1 << child.length) - 1)
      output += "\n\n" + child.parse(child_value, depth + 1)
    return output
  
# represents a possible value that a field may have
class FieldValue:
  name = None     # field value name
  value = None    # actual value
  
  def __init__(self, name, value):
    self.name = name
    self.value = value

def indent(n):
  output = ""
  for i in range(n): output += "    "
  return output

# function parses the A2_DBG_TASK_FETCHER register value
def parse_a2_dbg_task_fetcher_reg(reg_val):
  # truncate the 'x' or '0x'
  if reg_val[0] == 'x':
    reg_val = reg_val[1:]

  # convert the string to integer for bit manipulation
  int_val = int(reg_val, 16)

  # create structure of a2_dbg_task_fetcher
  a2_dbg_task_fetcher = Field()
  a2_dbg_task_fetcher.name = "A2_DBG_TASK_FETCHER"
  a2_dbg_task_fetcher.length = 32
  
  # tf_cur_state
  tf_cur_state = Field()
  tf_cur_state.name = "TF_CUR_STATE"
  tf_cur_state.offset = 0
  tf_cur_state.length = 4
  tf_cur_state.desc = "When task fetcher is servicing HW thread it will have a value of 0x7. " + \
                      "When it services SW thread, it remains in other states. " + \
                      "When it does not service any thread it remains in Idle state."
  tf_cur_state.values = [ FieldValue("TF_IDLE", 0),
                          FieldValue("TF_ARB", 1),
                          FieldValue("TF_FETCH_LINK", 2),
                          FieldValue("TF_LINK1", 3),
                          FieldValue("TF_LINK2", 4),
                          FieldValue("TF_BATCH_REQ", 5),
                          FieldValue("TF_BATCH_WAIT", 6),
                          FieldValue("TF_HW_THRD", 7),
                          FieldValue("TF_RE_FETCH_LINK", 8),
                          FieldValue("TF_FETCH_LINK_ADDR", 9),
                          FieldValue("TF_LINK_ADDR_LD", 10),
                          FieldValue("TF_LD_LINK_WRD2", 11),
                          FieldValue("TF_FLOW_CHK", 12) ]

  a2_dbg_task_fetcher.children.append(tf_cur_state)
  
  # td_cur_state
  td_cur_state = Field()
  td_cur_state.name = "TD_CUR_STATE"
  td_cur_state.offset = 4
  td_cur_state.length = 3
  td_cur_state.values = [ FieldValue("TD_IDLE", 0),
                          FieldValue("TD_SEND_WORD_0", 1),
                          FieldValue("TD_SEND_WORD_N", 2),
                          FieldValue("TD_WORD_0_DISPATCH", 3) ]
  a2_dbg_task_fetcher.children.append(td_cur_state)
  
  # cur_arb_state
  cur_arb_state = Field()
  cur_arb_state.name = "CUR_ARB_STATE"
  cur_arb_state.offset = 7
  cur_arb_state.length = 1
  cur_arb_state.values = [ FieldValue("ARB_IDLE", 0),
                           FieldValue("ARB_LOCK", 1) ]
  a2_dbg_task_fetcher.children.append(cur_arb_state)
  
  # all_thrds_avail: need to add all individual bits as children
  all_thrds_avail = Field()
  all_thrds_avail.name = "ALL_THRDS_AVAIL"
  all_thrds_avail.offset = 8
  all_thrds_avail.length = 8
  
  all_thrds_avail_0 = Field()
  all_thrds_avail_0.name = "sw_thrds_avail['A2_SW_TQ_UL_PHY0']"
  all_thrds_avail_0.offset = 0
  all_thrds_avail_0.length = 1
  all_thrds_avail.children.append(all_thrds_avail_0)
  
  all_thrds_avail_1 = Field()
  all_thrds_avail_1.name = "sw_thrds_avail['A2_SW_TQ_UL_PHY1']"
  all_thrds_avail_1.offset = 1
  all_thrds_avail_1.length = 1
  all_thrds_avail.children.append(all_thrds_avail_1)
  
  all_thrds_avail_2 = Field()
  all_thrds_avail_2.name = "sw_thrds_avail['A2_SW_TQ_DL_PHY0']"
  all_thrds_avail_2.offset = 2
  all_thrds_avail_2.length = 1
  all_thrds_avail.children.append(all_thrds_avail_2)
  
  all_thrds_avail_3 = Field()
  all_thrds_avail_3.name = "lte_dl_phy_hw_thrd_en[0] & decob_trb_arb_lock[0] & ~decob_trb_last_xfr[0]"
  all_thrds_avail_3.offset = 3
  all_thrds_avail_3.length = 1
  all_thrds_avail.children.append(all_thrds_avail_3)
  
  all_thrds_avail_4 = Field()
  all_thrds_avail_4.name = "sw_thrds_avail['A2_SW_TQ_DL_PHY1']"
  all_thrds_avail_4.offset = 4
  all_thrds_avail_4.length = 1
  all_thrds_avail.children.append(all_thrds_avail_4)
  
  all_thrds_avail_5 = Field()
  all_thrds_avail_5.name = "lte_dl_phy_hw_thrd_en[1] & decob_trb_arb_lock[1] & ~decob_trb_last_xfr[1]"
  all_thrds_avail_5.offset = 5
  all_thrds_avail_5.length = 1
  all_thrds_avail.children.append(all_thrds_avail_5)
  
  all_thrds_avail_6 = Field()
  all_thrds_avail_6.name = "sw_thrds_avail['A2_SW_TQ_DL_SEC0']"
  all_thrds_avail_6.offset = 6
  all_thrds_avail_6.length = 1
  all_thrds_avail.children.append(all_thrds_avail_6)
  
  all_thrds_avail_7 = Field()
  all_thrds_avail_7.name = "sw_thrds_avail['A2_SW_TQ_DL_SEC1']"
  all_thrds_avail_7.offset = 7
  all_thrds_avail_7.length = 1
  all_thrds_avail.children.append(all_thrds_avail_7)
  
  a2_dbg_task_fetcher.children.append(all_thrds_avail)
  
  # winner
  winner = Field()
  winner.name = "WINNER"
  winner.offset = 16
  winner.length = 4
  winner.values = [ FieldValue("A2_WINNER_UL_PHY_0", 0),
                    FieldValue("A2_WINNER_UL_PHY_1", 1),
                    FieldValue("A2_WINNER_SW_DL_PHY_0", 2),
                    FieldValue("A2_WINNER_HW_DL_PHY_0", 3),
                    FieldValue("A2_WINNER_SW_DL_PHY_1", 4),
                    FieldValue("A2_WINNER_HW_DL_PHY_1", 5),
                    FieldValue("A2_WINNER_SEC_0", 6),
                    FieldValue("A2_WINNER_SEC_1", 7),
                    FieldValue("A2_WINNER_IDLE", 15) ]
  a2_dbg_task_fetcher.children.append(winner)
  
  # mcdma_rd_ready
  mcdma_rd_ready = Field()
  mcdma_rd_ready.name = "MCDMA_RD_READY"
  mcdma_rd_ready.offset = 20
  mcdma_rd_ready.length = 1
  a2_dbg_task_fetcher.children.append(mcdma_rd_ready)
  
  # mch_dma_req
  mch_dma_req = Field()
  mch_dma_req.name = "MCH_DMA_REQ"
  mch_dma_req.offset = 21
  mch_dma_req.length = 1
  a2_dbg_task_fetcher.children.append(mch_dma_req)
  
  # decob_trb_arb_lock[0]
  decob_trb_arb_lock_0 = Field()
  decob_trb_arb_lock_0.name = "DECOB_TRB_ARB_LOCK[0]"
  decob_trb_arb_lock_0.offset = 22
  decob_trb_arb_lock_0.length = 1
  a2_dbg_task_fetcher.children.append(decob_trb_arb_lock_0)
  
  # decob_trb_arb_lock[1]
  decob_trb_arb_lock_1 = Field()
  decob_trb_arb_lock_1.name = "DECOB_TRB_ARB_LOCK[1]"
  decob_trb_arb_lock_1.offset = 23
  decob_trb_arb_lock_1.length = 1
  a2_dbg_task_fetcher.children.append(decob_trb_arb_lock_1)
  
  # batch_len_bytes
  batch_len_bytes = Field()
  batch_len_bytes.name = "BATCH_LEN_BYTES"
  batch_len_bytes.offset = 24
  batch_len_bytes.length = 8
  a2_dbg_task_fetcher.children.append(batch_len_bytes)

  # print readable structure of a2_dbg_task_fetcher
  print a2_dbg_task_fetcher.parse(int_val)

# main function for looping over the command line arguments
def main(argv):

  # register list
  a2_dbg_tf_reg_val = ''

  try:
    opts, args = getopt.getopt(argv,"ht:",["help","dtf="])
  except getopt.GetoptError:
    print "\n options:\n\
   -h or --help for help with script usage and options\n\
   -t or --dtf to parse the A2_DBG_TASK_FETCHER reg value.\n\
     this option requires the A2_DBG_TASK_FETCHER register value as the input\n\
     for ex: a2_dime_reg_parser.py -t <A2_DBG_TASK_FETCHER reg value> OR\n\
             a2_dime_reg_parser.py --dtf <A2_DBG_TASK_FETCHER reg value>\n\
   \n general usage:\n\
   a2_task_fetcher_parser.py <option> <param>"
    sys.exit(2)
  
  for opt, arg in opts:
    if opt in ("-h", "--help"):
      print "\n options:\n\
   -h or --help for help with script usage and options\n\
   -t or --dtf to parse the A2_DBG_TASK_FETCHER reg value.\n\
     this option requires the A2_DBG_TASK_FETCHER register value as the input\n\
     for ex: a2_task_fetcher_parser.py -t <A2_DBG_TASK_FETCHER reg value> OR\n\
             a2_task_fetcher_parser.py --dtf <A2_DBG_TASK_FETCHER reg value>\n\
   \n general usage:\n\
   a2_task_fetcher_parser.py <option> <param>"
      sys.exit()
    elif opt in ("-t", "--dtf"):
      a2_dbg_tf_reg_val = arg
      parse_a2_dbg_task_fetcher_reg(a2_dbg_tf_reg_val.lower())

# calling main function here
if __name__ == "__main__":
  main(sys.argv[1:])
  
