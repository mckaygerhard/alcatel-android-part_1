/*!
  @file
  a2_poweri.h

  @brief
  The internal interface to the A2 power collapse feature implementation file

  @author
  araina
*/

/*==============================================================================

  Copyright (c) 2015 Qualcomm Technologies, Inc. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies, Inc. and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/utils/a2/driver/src/a2_poweri.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
10/15/15   ca      CR:924224, Implementing try lock function for a2 power 
                   mutex.
01/13/14   ca      DSDA feature is implemented.
11/21/13   ca      Added a2 power request processing callback decleration
10/08/13   ar      initial version
==============================================================================*/

#ifndef A2_POWERI_H
#define A2_POWERI_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <comdef.h>
#include <a2_power.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/*!
   @brief Structure to hold a2 power shut fown call backs and user data
*/
typedef struct
{
  /* to store the registered call back function*/
  a2_power_req_proc_cb     req_proc_cb;
  /* to store user data which is passed along with cb funciton  */
  uint32                   user_data;
}a2_power_req_proc_cb_info_s;

/*!
   @brief Structure to hold a2 instance id information such as,
   the corresponding sys sub id and demback ids.
*/
typedef struct
{
  /*! variable to store instance bf for a given sub id 
    the set bits represent hardware resources allocated for this sub id */
  a2_hw_inst_bf_t              hw_inst_used_sub_id;

  /*! a2 power process call backs array of structs*/
  a2_power_req_proc_cb_info_s  req_proc_cb_info[A2_POWER_CLIENT_MAX];
  
  /*! a variable to store the reference count of number of hard ware 
      resources used by a particular sub id */ 
  uint8                        hw_inst_ref_count;
}a2_power_sub_id_info_s;



/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*==============================================================================

  FUNCTION:  a2_power_init

==============================================================================*/
/*!
    @brief
    initializes the a2 power module
    
    @return
    none
*/
/*============================================================================*/
void a2_power_init
(
  void
);
/* a2_power_init() */

/*==============================================================================

  FUNCTION:  a2_power_deinit

==============================================================================*/
/*!
  @brief
  De-initializes the power module.

*/
/*============================================================================*/
void a2_power_deinit
(
  void
);
/* a2_power_deinit() */

/*==============================================================================

  FUNCTION:  a2_power_process_req

==============================================================================*/
/*!
    @brief
    function processes the A2_POWER_REQ
 
    @detail
    this function runs under DL PER task context

    @return
    none
*/
/*============================================================================*/
void a2_power_process_req
(
  a2_sub_id_t       sub_id, /*!< system subsrciber id */
  a2_power_client_e client, /*!< client making the request */
  a2_power_req_e    req,     /*!< type of request */
  a2_demback_bf_t   demback_bf /*!< dem back id applicable for only dl phy*/
);

/*==============================================================================

  FUNCTION:  a2_power_log_new_req_receive_time

==============================================================================*/
/*!
    @brief
    logs the ustmr time for the newly received power request

    @return
    none
*/
/*============================================================================*/
void a2_power_log_new_req_receive_time
(
  void
);
/* a2_power_log_new_req_receive_time() */

/*==============================================================================

  FUNCTION:  a2_power_enter_crit_sect

==============================================================================*/
/*!
  @brief
  This function is called to enter the A2 PC critical section

*/
/*============================================================================*/
void a2_power_enter_crit_sect
(
  void
);

/*==============================================================================

  FUNCTION:  a2_power_try_enter_crit_sect

==============================================================================*/
/*!
  @brief
  This function is called to enter the try lock for A2 PC critical section
  @return
  A boolean to represent if the try lock is succeful or not.

*/
/*============================================================================*/
boolean a2_power_try_enter_crit_sect
(
  void
);

/*==============================================================================

  FUNCTION:  a2_power_exit_crit_sect

==============================================================================*/
/*!
  @brief
  This function is called to exit the A2 PC critical section

*/
/*============================================================================*/
void a2_power_exit_crit_sect
(
  void
);

/*==============================================================================

  FUNCTION:  a2_power_config_modem_for_peak_data_rates2

==============================================================================*/
/*!
  @brief
  Function configures the needed npa resources like q6 cpu, buses and memories
  to run for peak data rates
  
  @detail
  This function is expected to be called only in A2 PER loopback mode
  
  @return
  none

*/
/*============================================================================*/
void a2_power_config_modem_for_peak_data_rates2
(
   uint8 enable_peak_q6_frequency
);

/*==============================================================================

  FUNCTION:  a2_power_config_modem_for_peak_offline

==============================================================================*/
/*!
  @brief
  vote for max mss config bus frequency 
  
  @return
  boolean

*/
/*============================================================================*/
void a2_power_config_modem_for_peak_offline
(
 void
);

/*==============================================================================

  FUNCTION:  a2_power_call_req_proc_cb

==============================================================================*/
/*!
  @brief
  This function is called to send the acknowledgement to the clients

*/
/*============================================================================*/
void a2_power_call_req_proc_cb
(
  a2_sub_id_t sub_id /* system subscriber id */
);

/*==============================================================================

  FUNCTION:  a2_power_alloc_hw_inst

==============================================================================*/
/*!
  @brief
  This function is called to allocate  a2 inst id from sub id.

*/
/*============================================================================*/
a2_hw_inst_bf_t a2_power_alloc_hw_inst
(
  a2_sub_id_t      sub_id, /* system subscriber id */
  a2_demback_bf_t  demback_bf  /* demback ID */
);



/*==============================================================================

  FUNCTION:  a2_test_power_set_hw_availability

==============================================================================*/
/*!
  @brief
  This funciton is called by the test frame work to set the hw
  availability to test the 3,4,5 taskqs.

  
  @return
  

*/
/*============================================================================*/
void a2_test_power_set_hw_availability
(
  uint8 val //value wth which the hw resource availablility is set.
);

/*==============================================================================

  FUNCTION:  a2_power_get_hw_inst_used

==============================================================================*/
/*!
  @brief
  This funciton is called to get the hw resources used 
  in the system.
  
  @return
  Returns the hw inst used in the system.

*/
/*============================================================================*/
uint8 a2_power_get_hw_inst_used
(
  void
);

#endif /* A2_POWERI_H */

