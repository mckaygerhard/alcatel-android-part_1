/*
  Copyright (C) 2010 - 2012 QUALCOMM Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - QUALCOMM Technologies, Inc.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/apr/custom/user/src/aprv2_ipc.c#1 $
  $Author: mplcsds1 $
*/

#include "aprv2_ids_domains.h"
#include "aprv2_api_inline.h"
#include "apr_errcodes.h" /*TODO: remove this once moved to latest aprv2_api_inline.h. */
#include "aprv2_ipc.h"
#include "aprv2_mpd_i.h" /* for sending and receiving messages from kernel */
#include "msg.h"

#include "sys_m_messages.h"
#include "rcecb.h"

/*****************************************************************************
 * Defines                                                                   *
 ****************************************************************************/

/*****************************************************************************
 * Variables                                                                 *
 ****************************************************************************/

/*****************************************************************************
 * Core Routine Implementations                                              *
 ****************************************************************************/

static int32_t aprv2_mpd_cb ( 
  aprv2_mpd_event_t ev, 
  void* payload, 
  uint32_t sz
)
{
  int32_t rc = APR_EOK;
  int32_t handle;
  aprv2_packet_t* new_packet;
  
  switch(ev)
  {
  case APRV2_MPD_EV_RX_DATA: /* received rx data */
    new_packet = ( ( aprv2_packet_t* ) payload );

    handle = ( ( uint32_t ) ( ( ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, new_packet->dst_addr ) ) << 8 ) |
                              ( APR_GET_FIELD( APRV2_PKT_SERVICE_ID, new_packet->dst_addr ) ) ) );

    ( void ) __aprv2_cmd_forward( handle, new_packet );
    break;

  case APRV2_MPD_EV_SEND_FAILED:
      rc = APR_EFAILED;
      break;
  }

  if( rc )
  {
    MSG_ERROR( "ADSP aprv2_mpd_cb failed, rc = %d." , rc, 0, 0 );
  }
  return rc;
}

APR_INTERNAL int32_t aprv2_ipc_init ( void )
{
  int32_t rc;

  MSG_HIGH( "ADSP aprv2_ipc_init starting initialization sequence.", 0, 0, 0 );

  for ( ;; )
  {
    /* register MPD call-back */
    aprv2_mpd_set_rx_cb(aprv2_mpd_cb);
    MSG_HIGH( "ADSP aprv2_ipc_init succeed.", 0, 0, 0 );
    return APR_EOK;
  }

  return rc;
}

APR_INTERNAL int32_t aprv2_ipc_deinit ( void )
{
  MSG_HIGH( "ADSP aprv2_ipc_deinit start.", 0, 0 ,0 );

  return APR_EOK;
}

APR_INTERNAL int32_t aprv2_ipc_is_domain_local (
  uint16_t domain_id
)
{
  switch ( domain_id )
  {
  case APRV2_IDS_DOMAIN_ID_ADSP_V:
    return APR_EOK;

  default:
    return APR_EFAILED;
  }
}

APR_INTERNAL int32_t aprv2_ipc_send (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  uint16_t dst_domain_id;
  uint16_t dst_service_id;
  uint32_t packet_len;

  if ( packet == NULL )
  {
    return APR_EBADPARAM;
  }
  packet_len =  APRV2_PKT_GET_PACKET_BYTE_SIZE( packet->header );

  dst_domain_id  = APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, packet->dst_addr );
  dst_service_id = APR_GET_FIELD( APRV2_PKT_SERVICE_ID, packet->dst_addr );

  switch ( dst_domain_id )
  {
  case APRV2_IDS_DOMAIN_ID_PC_V:
    /*-fallthru */
  case APRV2_IDS_DOMAIN_ID_MODEM_V:
    rc = aprv2_mpd_send(packet);
    break;

  case APRV2_IDS_DOMAIN_ID_APPS2_V:
  case APRV2_IDS_DOMAIN_ID_APPS_V:
    rc = aprv2_mpd_send(packet); // in user-space if dst domain is remote send 
                                 // to kernel
    break;

  case APRV2_IDS_DOMAIN_ID_ADSP_V:
    rc = APR_ELPC;
    break;

  default:
    MSG_ERROR( "ADSP apr_ipc_send(0x%08X) sending to invaild domain.", 
               packet, 0, 0 );
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;
}

