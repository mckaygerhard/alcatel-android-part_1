#pragma once
/**
   @file q6zip_ipa.h 
   @author anandj 
   @brief Implementation of Q6ZIP interface using algorithms implemented in HW
    
   Copyright (c) 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
#include "q6zip_iface.h"

/**
 * @brief Implementation of Q6ZIP interface on IPA
 */
extern q6zip_iface_t const Q6ZIP_IPA;
