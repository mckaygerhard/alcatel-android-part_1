/**
   @file q6zip_latency_meas.h 
   @brief Q6Zip related latency measurement module. Experimental feature that, 
          when enabled, measures latency, currently for a word read from PCNOC.
    
   Copyright (c) 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
#pragma once
#include "atomic_ops.h"
#include "comdef.h"
#include "q6zip_types.h"

/** @brief Power-of-2 value for number of samples. For example, 10 means 2^10 */
#define Q6ZIP_LATENCY_MEAS_NUM_SAMPLES_ORDER 10

/** @brief Number of samples */
#define Q6ZIP_LATENCY_MEAS_NUM_SAMPLES ( 1 << Q6ZIP_LATENCY_MEAS_NUM_SAMPLES_ORDER )

#define Q6ZIP_LATENCY_MEAS_MASK ( Q6ZIP_LATENCY_MEAS_NUM_SAMPLES - 1 )

typedef struct
{
    /** @brief Interval (in milliseconds) at which to measure latency */
    uint32 interval;

    /** @brief Number of times (2^order) to measure */
    uint8 order;

    boolean is_enabled;

} q6zip_latency_meas_conf_t;

typedef struct
{
    /** @brief Lower 32b of tick */
    uint32 tick;

    /** @brief PCNOC (word) read latency in units of ticks. Maximum value of
               65535 ticks = ~3.413ms. Well within the range for expected latency
               of word reads off PCNOC */
    uint16 pcnoc_read_latency_ticks;

    /** @brief Was this measurement from DL pager (as opposed to periodic)? */
    boolean is_dlpager;

} q6zip_latency_meas_sample_t;

typedef struct
{
    /** @brief Currently used index  */
    atomic_word_t idx;

    /** @brief Latency measurement samples */
    q6zip_latency_meas_sample_t samples[ Q6ZIP_LATENCY_MEAS_NUM_SAMPLES ];

} q6zip_latency_meas_history_t;

/** @brief Data-structure for latency measurement module */
typedef struct
{
    /** @brief Configuration */
    q6zip_latency_meas_conf_t conf;

    /** @brief Sample history */
    q6zip_latency_meas_history_t history;

    /** @brief Value of the PCNOC register that was read. Making it a part of
               this data-structure to prevent it from getting optimized out if it
               were a local variable */
    uint32 pcnoc_register_value;

} q6zip_latency_meas_module_t;

/** @brief Pointer to module for external usage */
extern q6zip_latency_meas_module_t const * Q6ZIP_LATENCY_MEAS_MODULE;

/**
 * @brief RC init hook for latency measurement 
 * @warning This is being invoked in the context of a RC init worker task. 
 *          Please keep work to absolute minimal
 */
extern void q6zip_latency_meas_rcinit (void);

/**
 * @brief Run latency measurement task 
 * @warning If latency measurement is enabled, this call *DOES NOT* return 
 */
extern void q6zip_latency_meas_run (void);

/**
 * @brief Measure and record a sample
 * 
 * @param [in] is_dlpager Is the function being invoked from DL pager?
 */
extern void q6zip_latency_meas_record (boolean is_dlpager);

/**
 * @brief Inlined variant of q6zip_latency_meas_record()
 * 
 * @param [in] is_dlpager Is the function being invoked from DL pager?
 */
static inline void q6zip_latency_meas_record_inlined (boolean is_dlpager)
{
    if ( Q6ZIP_UNLIKELY( Q6ZIP_LATENCY_MEAS_MODULE->conf.is_enabled ) )
    {
        q6zip_latency_meas_record( is_dlpager );
    }
}

