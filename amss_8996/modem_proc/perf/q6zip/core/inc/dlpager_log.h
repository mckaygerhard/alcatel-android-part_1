#ifndef DLPAGER_LOG_H
#define DLPAGER_LOG_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               DL PAGER TOP LEVEL HEADER FILE

GENERAL DESCRIPTION

  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/perf/q6zip/core/inc/dlpager_log.h#1 $ $DateTime: 2016/03/28 23:03:55 $ $Author: mplcsds1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/09/14   rc,rr,cp     Header file for logging
===========================================================================*/
#pragma once
#include <stdio.h>
#include <qurt.h>
#include <stdlib.h>
#include <dlpager_types.h>


#define DLPAGER_LOG_TICKS_BITS  15
#define DLPAGER_LOG_TICKS_MASK ((1 << DLPAGER_LOG_TICKS_BITS) - 1)
#define DLPAGER_LOG_DELTA_PCYCLES_BITS 19 // XXX cporter: doublecheck with raghu, ric.
                                          // this is an overestmiate.
                                          // tasklog of boot sequence used max of 17.
                                          // see \\harv-cporter\d\dumps\Port_COM15-chris-pcycles-bitmask
#define DLPAGER_LOG_DELTA_PCYCLES_MASK ((1 << DLPAGER_LOG_DELTA_PCYCLES_BITS) - 1)

/** @brief DL pager task-fault related events */
typedef enum
{
    DLP_FAULT,            /**< Pagefault occured       */
    DLP_REMOVE_CLK_VOTE,  /**< Clocks were devoted     */
    DLP_FIRST_CLK_VOTE,   /**< Initial clock vote      */
    DLP_MAX_CLK_VOTE,     /**< Vote for maximum clocks */

    DLP_RESTART_TIMER,
    DLP_FIRST_CLK_REQ,
    DLP_MAX_CLK_REQ,
    DLP_TIMER_REQ,

    DLP_EVENT_UNUSED_8,
    DLP_EVENT_UNUSED_9,
    DLP_EVENT_UNUSED_A,
    DLP_EVENT_UNUSED_B,
    DLP_EVENT_UNUSED_C,
    DLP_EVENT_UNUSED_D,
    DLP_EVENT_UNUSED_E,
    DLP_EVENT_UNUSED_F,

    DLP_EVENT_MAX = 0xF

    /* @warning Only 4 bits are allocated for dlpager_tasklog_event_t in
       dlpager_tasklog_entry_t (below). */

} dlpager_tasklog_event_t;

/** @brief Loader used for decompression/compression */
typedef enum
{
    DLP_LOADER_Q6ZIP_SW,  /**< SW  */
    DLP_LOADER_Q6ZIP_IPA, /**< IPA */
    DLP_LOADER_NA         /**< Not applicable. Shows up as 0xFE in Trace32! */

} dlpager_loader_t;

/** Log entry that logs the pager activity. The entries are logged when 
    when event_t happens. */
typedef struct {
    uint64_t        end_ticks;       /**< ticks, divide by 19200 to get usec */
    uint64_t        start_pcycles;   /**< all 64 bits of pcycles */
    uint32_t        fault_addr;      /**< Faulting address */

    uint32_t        page_event:4;    /**< Event passed by QuRT */
    uint32_t        clk:10;          /**< MIPS from DCVS */
    uint32_t        evicted_page:16; /**< Evicted page */
    dlpager_loader_t loader:2;       /**< Loader used  */

    uint32_t                thread_id:9;     /**< Fault thread Id */
    uint32_t                delta_pcycles:DLPAGER_LOG_DELTA_PCYCLES_BITS; /**< Amount of pcycles for this operation */
    dlpager_tasklog_event_t log_event:4;   /**< dlpager_tasklog_event_t, extra bit since enum may be signed */

} dlpager_tasklog_entry_t;

/*===========================================================================

             FUNCTION DEFINITIONS

===========================================================================*/
extern unsigned dlpager_pagelog_start(dlpager_page_state_t start_state, dlpager_page_state_t end_state, dlpager_event_t event, void* addr, qurt_thread_t thread_id);
extern void dlpager_pagelog_end(unsigned log_idx);

// TODO: take page_event as a param
extern unsigned dlpager_tasklog_start(dlpager_tasklog_event_t log_event, unsigned int fault_addr,  dlpager_event_t page_event, qurt_thread_t thread_id, uint32_t evicted_page);
extern void dlpager_tasklog_set_loader (unsigned int idx, dlpager_loader_t loader);
extern void dlpager_tasklog_end(unsigned log_idx, int clk);

#endif /* DLPAGER_LOG_MAIN_H */


