#include "q6zip_context.h"
#include "q6zip_params.h"
#include "q6zip_request.h"
#include "q6zip_request_private.h"
#include "q6zip_sw_worker.h"
#include "q6zip_worker.h"
#include "qurt.h"

/** @brief Handy macro for all signals to a worker */
#define Q6ZIP_SW_WORKER_SIGNALS \
    ( Q6ZIP_SW_WORKER_SIGNAL_WORK | \
      Q6ZIP_SW_WORKER_SIGNAL_QUIT )

/** @brief Various signals to a worker */
typedef enum
{
    Q6ZIP_SW_WORKER_SIGNAL_WORK = ( 1 << 0 ), /**< Work to do, get cracking */
    Q6ZIP_SW_WORKER_SIGNAL_QUIT = ( 1 << 1 )  /**< You are fired! */

} q6zip_sw_worker_signal_t;

/** @brief Kind of SW workers */
typedef enum
{
    Q6ZIP_SW_WORKER_HI,  /**< Worker for high priority requests  */
    Q6ZIP_SW_WORKER_LO,  /**< Worker for low priority requests   */
    Q6ZIP_SW_WORKER_MAX, /**< Dont use. For sizing purposes only */

} q6zip_sw_worker_kind_t;

/** @brief Abstract datatype for a SW worker */
typedef struct
{
    /** @brief Worker object */
    q6zip_worker_t worker;

    /** @brief Name of this worker */
    char const *   name;

    /** @brief Task priority of this worker */
    unsigned short priority;

    /** @brief Worker's queue */
    q_type         queue;

    /** @brief Worker's signal */
    qurt_signal_t  signal;

} q6zip_sw_worker_t;

/** @brief List of workers. Typically one per priority */
static q6zip_sw_worker_t q6zip_sw_workers[ Q6ZIP_SW_WORKER_MAX ] = {
    {
        .name = "q6zip_sw_hi",
        .priority = Q6ZIP_PARAM_SW_WORKER_HI_TASK_PRIORITY
    },
    {
        .name = "q6zip_sw_lo",
        .priority = Q6ZIP_PARAM_SW_WORKER_LO_TASK_PRIORITY
    }
};

/** @brief Pointer to function that does the work */
static q6zip_sw_worker_fn_t q6zip_sw_work;

static void * worker (void * argument)
{
    q6zip_sw_worker_t * worker = (q6zip_sw_worker_t *)argument;
    unsigned int        signals;

    while ( 1 )
    {
        signals = qurt_signal_wait_any( &worker->signal, Q6ZIP_SW_WORKER_SIGNALS );
        qurt_signal_clear( &worker->signal, signals );

        if ( signals & Q6ZIP_SW_WORKER_SIGNAL_WORK )
        {
            q6zip_context_t * context;

            while ( ( context = (q6zip_context_t *)q_get( &worker->queue ) ) )
            {
                q6zip_sw_work( &context->request );
            }
        }

        if ( signals & Q6ZIP_SW_WORKER_SIGNAL_QUIT )
        {
            break;
        }
    }

    return NULL;
}

static inline q6zip_sw_worker_t * q6zip_sw_worker_get (q6zip_request_t const * request)
{
    /* If high priority is being requested explicitly *OR* if this is a 
       decompression request, use high priority worker */
    if ( ( request->priority == Q6ZIP_FIXED_PRIORITY_HIGH ) ||
         ( Q6ZIP_IS_UNZIP_ALGO( request->algo ) ) )
    {
        return &q6zip_sw_workers[ Q6ZIP_SW_WORKER_HI ];
    }

    /* For everything else, use low priority worker */
    return &q6zip_sw_workers [ Q6ZIP_SW_WORKER_LO ];
}

q6zip_error_t q6zip_sw_worker_init (q6zip_sw_worker_fn_t work)
{
    unsigned int i;

    q6zip_sw_work = work;

    /* Initialize and start workers */
    for ( i = 0; i < Q6ZIP_ARRAY_SIZE( q6zip_sw_workers ); i++ )
    {
        q_init( &q6zip_sw_workers[ i ].queue );
        qurt_signal_init( &q6zip_sw_workers[ i ].signal );
        q6zip_worker_fork( 
            &q6zip_sw_workers[ i ].worker, 
            q6zip_sw_workers[ i ].name,
            Q6ZIP_PARAM_SW_WORKER_STACK_SIZE,
            worker, 
            q6zip_sw_workers[ i ].priority, 
            &q6zip_sw_workers[ i ] );
    }

    return Q6ZIP_ERROR_NONE;
}

q6zip_error_t q6zip_sw_worker_submit (q6zip_request_t * request)
{
    q6zip_context_t * context;
    q6zip_sw_worker_t * worker;

    Q6ZIP_ASSERT( request );

    context = q6zip_request_private_get_context( request );

    /* Select the worker */
    worker = q6zip_sw_worker_get( request );
    q_put( &worker->queue, &context->link );
    qurt_signal_set( &worker->signal, Q6ZIP_SW_WORKER_SIGNAL_WORK );

    return Q6ZIP_ERROR_NONE;
}

void q6zip_sw_worker_fini (void)
{
    unsigned int i;

    /* Ensure that all workers have finished their work */
    for ( i = 0; i < Q6ZIP_ARRAY_SIZE( q6zip_sw_workers ); i++ )
    {
        qurt_signal_set( &q6zip_sw_workers[ i ].signal, Q6ZIP_SW_WORKER_SIGNAL_QUIT );
        q6zip_worker_join( &q6zip_sw_workers[ i ].worker );
        Q6ZIP_ASSERT( q_cnt( &q6zip_sw_workers[ i ].queue ) == 0 );
    }
}
