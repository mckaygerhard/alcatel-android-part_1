#include "err.h"
#include "msg_diag_service.h"
#if !defined( Q6ZIP_ENABLE_ASSERTS )
#define Q6ZIP_ENABLE_ASSERTS
#endif
#include "q6zip_assert.h"
#include "q6zip_test_scenario_unzip_rw.h"
#include "q6zip_test_vectors_rw.h"
#include "q6zip_waiter.h"

/** @brief Destination/uncompressed page
    @warning *MUST* be aligned on cache line */
static uint8 unzipped_page[ 4096 ] __attribute__(( aligned( 64 ) ));

/** @brief Waiter to wait for completion of compression/decompression request */
static q6zip_waiter_t waiter;

/** @brief Count to track the number of times this test was run */
static uint8 test_iteration = 0;

static uint8 dma_src[ 3 * 1024 ] __attribute__(( aligned( 32 ) ));
static uint8 dma_dst[ 3 * 1024 ] __attribute__(( aligned( 32 ) ));

static void initialize (q6zip_test_scenario_t * scenario)
{
    q6zip_waiter_init( &waiter );

    memset( dma_src, 0xA5, sizeof( dma_src ) );
    (void)dma_dst;
}

static void configure (
    q6zip_test_scenario_t * scenario,
    q6zip_test_copyd_knobs_t const * knobs,
    uint8 const * msg,
    uint16 len
)
{
}

static boolean unzip_rw (uint8 vector_idx)
{
    #if 1
    q6zip_test_unzip_rw(
        (void* )unzipped_page,
        sizeof( unzipped_page ),
        (void *)RW_VECTORS_INDEX[ vector_idx ].zipped,
        RW_VECTORS_INDEX[ vector_idx ].zipped_size,
        ( void * )&RW_VECTORS_INDEX[ vector_idx ],
        &waiter );

    if ( memcmp( RW_VECTORS_INDEX[ vector_idx ].unzipped, unzipped_page, RW_VECTORS_INDEX[ vector_idx ].unzipped_size ) == 0 )
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
    #else
    q6zip_iovec_t src;
    q6zip_iovec_t dst;
    q6zip_iovec_t exp;

    dst.ptr = unzipped_page;
    dst.len = sizeof( unzipped_page );
    src.ptr = ( void * )RW_VECTORS_INDEX[ vector_idx ].zipped;
    src.len = RW_VECTORS_INDEX[ vector_idx ].zipped_size;
    exp.ptr = ( void * )RW_VECTORS_INDEX[ vector_idx ].zipped;
    exp.len = RW_VECTORS_INDEX[ vector_idx ].zipped_size;

    return q6zip_test_dma_verify( &dst, &src, &exp, &waiter );
    #endif
}

static void start (q6zip_test_scenario_t * scenario)
{
    unsigned int i;
    boolean bit_exact_match = FALSE;
    unsigned int repeat_count = 0;

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "ENTER: RW unzip tests. dst=0x%x iteration=%u", unzipped_page, test_iteration, 0 );

    for ( i = 0; i < Q6ZIP_TEST_VECTORS_RW_MAX; i++ )
    {
        /* Paint destination buffer with a pattern (number of the vector) */
        /* memset( unzipped_page, 0xA5, sizeof( unzipped_page ) );*/

        bit_exact_match = unzip_rw( i );

        #if 0
        if ( !bit_exact_match )
        {
            /* Repeat this vector */
            bit_exact_match = unzip_rw( i );

            if ( bit_exact_match )
            {
                repeat_count++;
            }
        }
        #endif

        if ( !bit_exact_match )
        {
            break;
        }
    }

    if ( bit_exact_match )
    {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "PASS: %u RW unzip vectors. repeat_count=%u", i, repeat_count, 0 );
    }
    else
    {
        ERR_FATAL( "FAIL: RW unzip of vector %u dst_ptr=0x%x repeat_count=%u", i, unzipped_page, repeat_count );
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "LEAVE: RW unzip tests", 0, 0, 0 );

    test_iteration++;
}

static void response (
    q6zip_test_scenario_t * scenario,
    q6zip_request_t const * request,
    q6zip_response_t const * response
)
{
    Q6ZIP_ASSERT( !response->error );
    Q6ZIP_ASSERT( response->len == 4096 );
    q6zip_waiter_wake( &waiter );
}

static void stop (q6zip_test_scenario_t * scenario)
{
}

q6zip_test_scenario_t q6zip_test_scenario_unzip_rw =
{
    .ops = 
    {
        initialize,
        configure,
        start,
        response,
        stop
    }
};
