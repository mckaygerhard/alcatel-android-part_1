/*#include "ttl_map.h"*/
#include "err.h"
#include "histogram.h"
#include "q6zip_assert.h"
#include "q6zip_clk.h"
#include "q6zip_ipa_conf.h"
#include "q6zip_context.h"
#include "q6zip_ipa.h"
#include "q6zip_ipa_initializer.h"
#include "q6zip_latency_meas.h"
#include "q6zip_log.h"
#include "q6zip_params.h"
#include "q6zip_request.h"
#include "q6zip_request_private.h"
#include "q6zip_types.h"
#include "qurt.h"
#include "ipa_api.h"
#include "npa.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/** @brief Define this (below) to enable q6zip logs */
#undef Q6ZIP_IPA_LOGGING_ENABLED

/** @brief Enable (#define) this to collect SNOC sample on a request */
#undef Q6ZIP_IPA_SNOC_SAMPLING_ENABLED

/** @brief Enable (#define) this if you want to capture a histogram of danger
           durations */
#undef Q6ZIP_IPA_DANGER_DURATION_HISTOGRAM_ENABLED

/** @brief Enable (#define) this if you want to enable SW workaround when
           partial/full EOB (0x1A) is present in last word. When enabled, if
           last word only contains partial/full EOB, it is clipped */
#define Q6ZIP_IPA_WORKAROUND_RO_UNZIP_EOB_IN_LAST_WORD

/** @brief Macro to check for IPA error */
#define IS_IPA_ERROR( ipa_error ) ( ( ipa_error ) != IPA_ZIP_SUCCESS )

#if !defined( Q6ZIP_IPA_SNOC_THRESHOLD_KHZ )
/** @brief SNOC threshold (KHz) */
#define Q6ZIP_IPA_SNOC_THRESHOLD_KHZ 200000
#endif

/** @brief Various IPA queues */
typedef enum
{
    Q6ZIP_IPA_QUEUE_LO, /**< Low/default priority queue */
    Q6ZIP_IPA_QUEUE_HI, /**< High priority queue        */
    Q6ZIP_IPA_QUEUE_MAX

} q6zip_ipa_queue_t;

/** @brief Statistics on an IPA queue */
typedef struct
{
    unsigned int responded; /**< Responses received */
    unsigned int submitted; /**< Requests submitted */
    unsigned int dropped;   /**< Requests dropped */
} q6zip_ipa_queue_stats_t;

/** @brief IPA statistics */
typedef struct
{
    q6zip_ipa_queue_stats_t hi;
    q6zip_ipa_queue_stats_t lo;

} q6zip_ipa_stats_t;

typedef struct
{
    /** @brief Error code indicated by engine */
    uint8 code;

    uint16 bytes_left;

    /** @brief Context that caused the error */
    volatile q6zip_context_t const * context;

    /** @brief Count of engine errors that were ignored. Only false-positives
        are ignored. @see q6zip_ipa_local_error_callback */
    uint8 num_ignored;

} q6zip_ipa_engine_error_t;

typedef struct
{
    /** @brief Value of SNOC query */
    unsigned int value;

    /** @brief Index into DL pager tasklog */
    unsigned int tasklog_idx;

} q6zip_ipa_snoc_clock_sample_t;

typedef struct
{
    struct
    {
        /** @brief Queues (used in a list fashion) that shadow IPA's queue */
        q_type queue[ Q6ZIP_IPA_QUEUE_MAX ];

        struct
        {
            ipa_zip_err_code_e registration;

            /** @brief Details on engine error */
            q6zip_ipa_engine_error_t engine;

        } errors;

        /** @brief IPA statistics */
        q6zip_ipa_stats_t stats;

        struct
        {
            /** @brief Number of times modem shutdown indication was notified by IPA ZIP
                       driver */
            uint16 num_asserted;

            /** @brief Number of requests re-routed to SW */
            uint32 num_rerouted;

            /** @brief Tick when last modem shutdown was asserted */
            uint64 tick;

        } shutdown;

        /** @brief *NEXT* sequence number to be used */
        uint8 next_seqno;

        /* Is IPA initialized? */
        boolean initialized;

    } ipa;

    /** @brief Clock control related data */
    struct
    {
        struct
        {
            /** @brief NPA handle to query /clk/snoc */
            npa_query_handle query_client;

            #if defined( Q6ZIP_IPA_SNOC_SAMPLING_ENABLED )

            /** @brief Buffer to hold SNOC query samples */
            q6zip_ipa_snoc_clock_sample_t samples[ 1024 ];

            /** @brief Current index into SNOC query sample buffer */
            unsigned int sample_idx;
            #endif

        } snoc;

        /** @brief Number of Q6Zip operations re-routed due pending clock boost */
        uint32 num_rerouted;

    } clocks;

    struct
    {
        struct
        {
            /** @brief Number of times danger was asserted */
            uint32 num_asserted;

            /** @brief Number of times danger was de-asserted */
            uint32 num_de_asserted;

            /** @brief Number of Q6Zip operations re-routed due to danger */
            uint32 num_rerouted;

            #if defined( Q6ZIP_IPA_DANGER_DURATION_HISTOGRAM_ENABLED )
            /** @brief Tick when danger was *assert* recently */
            uint64 danger_start_tick;

            uint32 recent_danger_duration;

            /** @brief Bins for danger duration histogram */
            histogram_bin_t duration_bins[ 130 ];

            /** @brief Danger duration histogram */
            histogram_t duration_histogram;
            #endif

        } stats;

        /** @brief Has FW asserted danger? */
        volatile boolean is_asserted;

    } danger;

    /** @brief Configuration */
    q6zip_ipa_conf_t const * conf;

    /** @brief I/O vector for dictionary */
    q6zip_iovec_t dict;

    /** @brief Client's callback for notification of completion of request */
    q6zip_completed_cb_t callback;

    /** @brief Semaphore to flow-control caller. If maximum number of requests
        have been submitted to IPA, this will block caller until a response for
        one of those requests is received */
    qurt_sem_t sem;

} q6zip_ipa_module_t;

static q6zip_ipa_module_t q6zip_ipa_module;

static ipa_zip_cmd_type_e const ALGO_TO_IPA_ZIP_CMD[ Q6ZIP_ALGO_MAX ] = {
    /* Q6ZIP_ALGO_UNZIP_RO */ IPA_ZIP_CMD_DECOMP_RO,
    /* Q6ZIP_ALGO_ZIP_RW   */ IPA_ZIP_CMD_COMP_RW,
    /* Q6ZIP_ALGO_UNZIP_RW */ IPA_ZIP_CMD_DECOMP_RW,
    /* Q6ZIP_ALGO_MEMCPY   */ IPA_ZIP_CMD_DMA
};

static void q6zip_ipa_callback (
    ipa_zip_cb_event_type_e  cb_evt,
    ipa_zip_cb_status_type_e cb_status,
    ipa_zip_cb_tag_type      cmd_cb_tag,
    uint32                   cmd_dst_buf_size
)
{
    q6zip_context_t * context = (q6zip_context_t *)cmd_cb_tag;

    /* @warning If context == NULL, it is an IPA bug. We *NEVER* give them a
       NULL context and therefore are not expecting one in return */
    ASSERT( context != NULL );

    #if defined( Q6ZIP_IPA_LOGGING_ENABLED )
    context->stats.sclock.stop = qurt_sysclock_get_hw_ticks();
    q6zip_log_put(
        context->stats.sclock.stop,
        context->meta.ipa.seqno,
        q_cnt( &q6zip_ipa_module.ipa.queue[ Q6ZIP_IPA_QUEUE_LO ] ),
        q_cnt( &q6zip_ipa_module.ipa.queue[ Q6ZIP_IPA_QUEUE_HI ] ),
        context->meta.ipa.queue_id & 0x1,
        Q6ZIP_LOG_TYPE_RSP,
        context->request.algo,
        cmd_dst_buf_size >> 2 );
    #endif

    /* Now that IPA is done, remove this request out of whatever queue it was
       submitted into */
    q_delete( &q6zip_ipa_module.ipa.queue[ context->meta.ipa.queue_id ], &context->link );

    if ( context->meta.ipa.queue_id == Q6ZIP_IPA_QUEUE_HI )
    {
        q6zip_ipa_module.ipa.stats.hi.responded++;
    }
    else
    {
        q6zip_ipa_module.ipa.stats.lo.responded++;
    }

    /* @todo anandj ASSERTion to catch over-incrementing of counting semaphore 
       closer to IPA. CR 846794 */
    ASSERT( qurt_sem_get_val( &q6zip_ipa_module.sem ) < Q6ZIP_PARAM_IPA_MAX_HI_PRIO_REQS );

    qurt_sem_up( &q6zip_ipa_module.sem );

    if ( Q6ZIP_LIKELY( q6zip_ipa_module.callback != NULL ) )
    {
        context->response.len = cmd_dst_buf_size;
        context->response.error = ( cb_status == IPA_ZIP_CB_STATUS_PASS ) ? 
            Q6ZIP_ERROR_NONE : Q6ZIP_ERROR_GENERIC;

        q6zip_ipa_module.callback( &context->request, &context->response );
    }
}

static void q6zip_ipa_local_callback (
    ipa_zip_cb_event_type_e  cb_evt,
    ipa_zip_cb_status_type_e cb_status,
    ipa_zip_cb_tag_type      cmd_cb_tag,
    uint32                   cmd_dst_buf_size
)
{
    switch ( cb_evt )
    {
        case IPA_ZIP_CB_EVENT_REGISTER:
        case IPA_ZIP_CB_EVENT_DICT_LOAD:

            if ( cb_evt == IPA_ZIP_CB_EVENT_DICT_LOAD )
            {
                /* Dictionary load is the last stage of initialization. If that
                   has completed successfully, Q6ZIP IPA module has been
                   initialized */
                q6zip_ipa_module.ipa.initialized = ( cb_status == IPA_ZIP_CB_STATUS_PASS );

                q6zip_ipa_module.clocks.snoc.query_client = npa_create_query_handle( "/clk/snoc" );

                q6zip_ipa_module.conf = &q6zip_ipa_conf_singleton;
            }

            /* These are handled by the Q6ZIP IPA initializer */
            q6zip_ipa_initializer_notify( cb_evt, cb_status );
            break;

        case IPA_ZIP_CB_EVENT_CMD:
            q6zip_ipa_callback( cb_evt, cb_status, cmd_cb_tag, cmd_dst_buf_size );
            break;

        case IPA_ZIP_CB_EVENT_MODEM_SHUTDOWN:
            q6zip_ipa_module.ipa.shutdown.num_asserted++;
            q6zip_ipa_module.ipa.shutdown.tick = qurt_sysclock_get_hw_ticks();
            break;

        default:
            break;
    }
}

static void q6zip_ipa_local_error_callback (
    uint8               code,
    ipa_zip_cb_tag_type tag,
    uint16              bytes_left
)
{
    q6zip_ipa_module.ipa.errors.engine.code = code;
    q6zip_ipa_module.ipa.errors.engine.context = (q6zip_context_t const *)tag;
    q6zip_ipa_module.ipa.errors.engine.bytes_left = bytes_left;

    if ( ( bytes_left == 0 ) && ( code == 1 ) )
    {
        /* @warning In IPAv2.6, due to a HW bug, an error IRQ is incorrectly
           triggered even when DCMP has successfully completed an ROD
           operation. Here's the breakdown of the conditions that detect this...
              * bytes_left = 0. Indicates that DCMP has consumed input
              * code = 1. Indicates that operation is ROD and DCMP has
                (incorrectly) decompressed junk

           ...in such a case, we ignore the error and move on. */

        q6zip_ipa_module.ipa.errors.engine.num_ignored++;
    }
    else
    {
        /* Anything else is legitimate failure. Abort! */
        ERR_FATAL( "IPA DCMP engine failure. code=%u", q6zip_ipa_module.ipa.errors.engine.code, 0, 0 );
    }
}

static void q6zip_ipa_init (
    void *                 ro_dict,
    unsigned int           ro_dict_len,
    q6zip_initialized_cb_t initialized,
    q6zip_completed_cb_t   completed
)
{
    ipa_zip_cfg_type_s cfg;

    memset( &q6zip_ipa_module, 0, sizeof( q6zip_ipa_module ) );

    /* Initialize this module */
    q6zip_ipa_module.callback = completed;

    q_init( &q6zip_ipa_module.ipa.queue[ Q6ZIP_IPA_QUEUE_LO ] );
    q_init( &q6zip_ipa_module.ipa.queue[ Q6ZIP_IPA_QUEUE_HI ] );

    q6zip_ipa_module.ipa.next_seqno = 0;

    /* Tune semaphore to sum of depths of IPA queues*/
    /* @todo anandj Fix this! Add priority argument to get_request() */
    qurt_sem_init_val( 
        &q6zip_ipa_module.sem, 
        ( /*Q6ZIP_PARAM_IPA_MAX_DEF_PRIO_REQS +*/ Q6ZIP_PARAM_IPA_MAX_HI_PRIO_REQS ) );

    /* @warning Initialize IPA asynchronous initializer *BEFORE* registering
       with IPA. This way, it will be ready when IPA responds */
    q6zip_ipa_module.dict.ptr = ro_dict;
    q6zip_ipa_module.dict.len = ro_dict_len;
    q6zip_ipa_initializer_init( &q6zip_ipa_module.dict, initialized );

    /* Register with IPA. This is an asynchronous process and Q6ZIP IPA
       initializer will handle it for us */
    cfg.req_cb_fn_ptr     = q6zip_ipa_local_callback;
    cfg.err_cb_fn_ptr     = q6zip_ipa_local_error_callback;
    cfg.max_def_prio_reqs = Q6ZIP_PARAM_IPA_MAX_DEF_PRIO_REQS;
    cfg.max_hi_prio_reqs  = Q6ZIP_PARAM_IPA_MAX_HI_PRIO_REQS;

    q6zip_ipa_module.ipa.errors.registration = ipa_zip_register( &cfg );

    #if defined( Q6ZIP_IPA_DANGER_DURATION_HISTOGRAM_ENABLED )
    histogram_init(
        &q6zip_ipa_module.danger.stats.duration_histogram,
        2048,
        262144,
        q6zip_ipa_module.danger.stats.duration_bins,
        HISTOGRAM_ARRAY_SIZE( q6zip_ipa_module.danger.stats.duration_bins ),
        11 );
    #endif
}

static boolean q6zip_ipa_are_clocks_as_expected (void)
{
    npa_query_type query;

    query.data.value = 0;

    #if defined( Q6ZIP_IPA_SNOC_SAMPLING_ENABLED )
    q6zip_ipa_module.clocks.snoc.sample_idx = ( q6zip_ipa_module.clocks.snoc.sample_idx + 1 ) & 0x3FF;
    #endif

    if ( Q6ZIP_LIKELY( q6zip_ipa_module.clocks.snoc.query_client != NULL ) )
    {
        npa_query( q6zip_ipa_module.clocks.snoc.query_client, NPA_QUERY_CURRENT_STATE, &query );
    }

    #if defined( Q6ZIP_IPA_SNOC_SAMPLING_ENABLED )
    q6zip_ipa_module.clocks.snoc.samples[ q6zip_ipa_module.clocks.snoc.sample_idx ].value = query.data.value;
    q6zip_ipa_module.clocks.snoc.samples[ q6zip_ipa_module.clocks.snoc.sample_idx ].tasklog_idx = ~0U;
    #endif

    return query.data.value >= Q6ZIP_IPA_SNOC_THRESHOLD_KHZ;
}

void q6zip_notify_danger_zone(uint32 uLevel)
{
    q6zip_ipa_module.danger.is_asserted = ( uLevel >= 1 );
    q6zip_ipa_module.danger.is_asserted ? 
        q6zip_ipa_module.danger.stats.num_asserted++ : q6zip_ipa_module.danger.stats.num_de_asserted++;

    #if defined( Q6ZIP_IPA_DANGER_DURATION_HISTOGRAM_ENABLED )
    if ( Q6ZIP_UNLIKELY( !q6zip_ipa_module.ipa.initialized ) )
    {
        return;
    }
    if ( uLevel >= 1 )
    {
        q6zip_ipa_module.danger.stats.danger_start_tick = qurt_sysclock_get_hw_ticks();
    }
    else
    {
        uint64 now_tick = qurt_sysclock_get_hw_ticks();

        if ( Q6ZIP_LIKELY( now_tick >= q6zip_ipa_module.danger.stats.danger_start_tick ) )
        {
            q6zip_ipa_module.danger.stats.recent_danger_duration = 
                 now_tick - q6zip_ipa_module.danger.stats.danger_start_tick;

            histogram_put( 
                &q6zip_ipa_module.danger.stats.duration_histogram, 
                q6zip_ipa_module.danger.stats.recent_danger_duration );
        }
    }
    #endif
}

static inline boolean q6zip_ipa_can_service_request (void)
{
    if ( q6zip_ipa_module.conf->flags.q6zip_sw_during_clock_boost )
    {
        /* Are clocks as expected? */
        if ( Q6ZIP_UNLIKELY( !q6zip_ipa_are_clocks_as_expected() ) )
        {
            q6zip_ipa_module.clocks.num_rerouted++;

            /* Clocks are not at expected level. Stay away from IPA usage */
            return FALSE;
        }
    }

    if ( q6zip_ipa_module.conf->flags.q6zip_sw_during_firmware_danger )
    {
        if ( Q6ZIP_UNLIKELY( q6zip_ipa_module.danger.is_asserted ) )
        {
            q6zip_ipa_module.danger.stats.num_rerouted++;

            /* Firmware danger is asserted. Stay away from IPA usage */
            return FALSE;
        }
    }

    return TRUE;
}

static q6zip_request_t * q6zip_ipa_get_request (void)
{
    q6zip_request_t * request = NULL;

    /* Vote for relevant clocks */
    q6zip_clk_check_vote();

    if ( Q6ZIP_UNLIKELY( !q6zip_ipa_can_service_request() ) )
    {
        /* @warning Returning NULL here is an indication to pager that it must
           use another implementation */
        return NULL;
    }

    qurt_sem_down( &q6zip_ipa_module.sem );

    if ( Q6ZIP_UNLIKELY( q6zip_ipa_module.ipa.shutdown.num_asserted > 0 ) )
    {
        /* @warning *IMPORTANT* Since we are not using IPA, need to increment
           the counting semaphore to reflect this. CR 897019 */
        qurt_sem_up( &q6zip_ipa_module.sem );

        q6zip_ipa_module.ipa.shutdown.num_rerouted++;

        /* @warning Returning NULL here is an indication to pager that it must
           use another implementation */
        return NULL;
    }

    request = q6zip_request_alloc();

    /* @warning Request must *NEVER* be NULL. If it is, then it is a
                configuration error */
    if ( Q6ZIP_UNLIKELY( !request ) )
    {
        ERR_FATAL( "Failed to allocate Q6Zip request", 0, 0, 0 );
    }

    return request;
}

static q6zip_error_t q6zip_ipa_submit (q6zip_request_t ** request)
{
    ipa_zip_err_code_e ipa_error;
    q6zip_context_t *  context;
    q6zip_error_t      error = Q6ZIP_ERROR_GENERIC;
    q6zip_ipa_queue_stats_t * qstats;

    Q6ZIP_ASSERT( q6zip_ipa_module.ipa.initialized );
    Q6ZIP_ASSERT( *request );

#if defined( Q6ZIP_UNIT_TEST) || defined( ENABLE_Q6ZIP_IPA )
    /* Allow all Q6ZIP operations on unit-test environment or on target if
       Q6ZIP in IPA is enabled */
#else
    /* For others, Q6ZIP IPA will only support memcpy */
    if ( (*request)->algo != Q6ZIP_ALGO_MEMCPY )
    {
        return Q6ZIP_ERROR_NOT_IMPLEMENTED;
    }
#endif

    context = q6zip_request_private_get_context( *request );
    context->impl = Q6ZIP_IMPL_IPA;

    #if defined( Q6ZIP_IPA_SNOC_SAMPLING_ENABLED )
    q6zip_ipa_module.clocks.snoc.samples[ q6zip_ipa_module.clocks.snoc.sample_idx ].tasklog_idx = (unsigned int) context->request.user.other;
    #endif

    /* Prepare request for IPA */
    context->meta.ipa.req.cmd_type = ALGO_TO_IPA_ZIP_CMD[ (*request)->algo ];
    context->meta.ipa.req.src_buf  = (*request)->src.ptr;
    context->meta.ipa.req.src_len  = (*request)->src.len;
    context->meta.ipa.req.dst_buf  = (*request)->dst.ptr;
    context->meta.ipa.req.dst_len  = (*request)->dst.len;
    context->meta.ipa.req.cb_tag   = (ipa_zip_cb_tag_type) context;
    context->meta.ipa.seqno        = q6zip_ipa_module.ipa.next_seqno;

    #if defined( Q6ZIP_IPA_WORKAROUND_RO_UNZIP_EOB_IN_LAST_WORD )
    if ( (*request)->algo == Q6ZIP_ALGO_UNZIP_RO )
    {
        uint32 const * words = (uint32 * )( (*request)->src.ptr );

        /* Does last word contain full/part of EOB? */
        if ( ( words[ ( (*request)->src.len / 4 ) - 1 ] & 0xFFFFFFE0 ) == 0 )
        {
            /* Clip the last word out */
            context->meta.ipa.req.src_len -= 4;
        }
    }
    #endif

    if ( (*request)->priority == Q6ZIP_FIXED_PRIORITY_IMPL )
    {
        switch ( (*request)->algo )
        {
            case Q6ZIP_ALGO_UNZIP_RO:
            case Q6ZIP_ALGO_UNZIP_RW:
                /* Decompressions are considered high priority */
                context->meta.ipa.queue_id = Q6ZIP_IPA_QUEUE_HI;
                break;

            case Q6ZIP_ALGO_ZIP_RW:
                /* Compressions are considered low priority */
                context->meta.ipa.queue_id = Q6ZIP_IPA_QUEUE_LO;
                break;

            case Q6ZIP_ALGO_MEMCPY:
                context->meta.ipa.queue_id = ( (*request)->priority == Q6ZIP_FIXED_PRIORITY_HIGH ) ?
                    Q6ZIP_IPA_QUEUE_HI : Q6ZIP_IPA_QUEUE_LO;
                break;

            default:
                ASSERT( FALSE );
        }
    }
    else
    {
        context->meta.ipa.queue_id = ( (*request)->priority == Q6ZIP_FIXED_PRIORITY_HIGH ) ?
            Q6ZIP_IPA_QUEUE_HI : Q6ZIP_IPA_QUEUE_LO;
    }

    qstats = context->meta.ipa.queue_id == Q6ZIP_IPA_QUEUE_HI ?
        &q6zip_ipa_module.ipa.stats.hi : &q6zip_ipa_module.ipa.stats.lo;

    q_put( &q6zip_ipa_module.ipa.queue[ context->meta.ipa.queue_id ], &context->link );

    #if defined( Q6ZIP_IPA_LOGGING_ENABLED )
    context->stats.sclock.start = qurt_sysclock_get_hw_ticks();
    q6zip_log_put(
        context->stats.sclock.start,
        context->meta.ipa.seqno,
        q_cnt( &q6zip_ipa_module.ipa.queue[ Q6ZIP_IPA_QUEUE_LO ] ),
        q_cnt( &q6zip_ipa_module.ipa.queue[ Q6ZIP_IPA_QUEUE_HI ] ),
        context->meta.ipa.queue_id & 0x1,
        Q6ZIP_LOG_TYPE_REQ,
        context->request.algo,
        context->request.src.len >> 2 );
    #endif

    if ( context->meta.ipa.queue_id == Q6ZIP_IPA_QUEUE_HI )
    {
        ipa_error = ipa_zip_cmd_req_hi_prio( &context->meta.ipa.req );
    }
    else
    {
        ipa_error = ipa_zip_cmd_req_def_prio( &context->meta.ipa.req );
    }

    Q6ZIP_ASSERT( context->meta.ipa.seqno == q6zip_ipa_module.ipa.next_seqno );

    /* Increment sequence number irrespective of IPA error */
    q6zip_ipa_module.ipa.next_seqno = ( q6zip_ipa_module.ipa.next_seqno + 1 ) & 0xFF;

    if ( Q6ZIP_UNLIKELY( IS_IPA_ERROR( ipa_error ) ) )
    {
        /* IPA threw an error. Reclaim the q6zip_context_t object and signal
          error to caller */
        q_delete( &q6zip_ipa_module.ipa.queue[ context->meta.ipa.queue_id ], &context->link );

        qstats->dropped++;

        error = Q6ZIP_ERROR_GENERIC;

        /* @todo anandj *HACK* need a better way to control semaphore. Need to
           grow an interface to free request explicitly */
        qurt_sem_up( &q6zip_ipa_module.sem );
    }
    else
    {
        q6zip_latency_meas_record_inlined( TRUE );

        /* Logging ... */
        #if defined( Q6ZIP_IPA_LOGGING_ENABLED ) && defined( Q6ZIP_LOG_ASYNC_FLUSH )
        /* @warning Intentionally kept *after* IPA programming to parallelize */
        q6zip_log_flush();
        #endif

        /* Statistics ... */
        qstats->submitted++;

        error = Q6ZIP_ERROR_NONE;
    }

    return error;
}

static q6zip_error_t q6zip_ipa_submit_pair (q6zip_request_pair_t * pair)
{
    return Q6ZIP_ERROR_NOT_IMPLEMENTED;
}

static void q6zip_ipa_fini (void)
{
    if ( q6zip_ipa_module.ipa.initialized )
    {
        ipa_zip_unregister();
    }

    /* @todo anandj Unit-test code! Nothing should be pending with IPA */
    Q6ZIP_ASSERT( q_cnt( &q6zip_ipa_module.ipa.queue[ Q6ZIP_IPA_QUEUE_LO ] ) == 0 );
    Q6ZIP_ASSERT( q_cnt( &q6zip_ipa_module.ipa.queue[ Q6ZIP_IPA_QUEUE_HI ] ) == 0 );
}

q6zip_iface_t const Q6ZIP_IPA = {

    q6zip_ipa_init,
    q6zip_ipa_get_request,
    q6zip_ipa_submit,
    q6zip_ipa_submit_pair,
    q6zip_ipa_fini

};
