#include "msg_diag_service.h"
#if !defined( Q6ZIP_ENABLE_ASSERTS )
#define Q6ZIP_ENABLE_ASSERTS
#endif
#include "q6zip_assert.h"
#include "q6zip_test_scenario_error_ro.h"
#include "q6zip_test_vectors_ro.h"
#include "q6zip_waiter.h"

typedef PACK( struct )
{
    uint8 control;

    /** @brief Index (zero-based) of vector to run */
    uint8 vector_idx;

} q6zip_test_scenario_error_ro_conf_t;

/** @brief Index of vector to run */
static uint8 vector_idx;

/** @brief Destination/uncompressed page
    @warning *MUST* be aligned on cache line */
static uint8 unzipped_page[ 4096 ] __attribute__(( aligned( 32 ) ));

static q6zip_waiter_t waiter;

static void initialize (q6zip_test_scenario_t * scenario)
{
    q6zip_waiter_init( &waiter );
}

static void configure (
    q6zip_test_scenario_t * scenario,
    q6zip_test_copyd_knobs_t const * knobs,
    uint8 const * msg,
    uint16 len
)
{
    q6zip_test_scenario_error_ro_conf_t const * conf = (q6zip_test_scenario_error_ro_conf_t *)msg;
    vector_idx = conf->vector_idx;
}

static void start (q6zip_test_scenario_t * scenario)
{
    q6zip_iface_t const * q6zip = q6zip_test_get_implementation();
    q6zip_request_t * request;
    q6zip_error_t error;

    if ( vector_idx >= Q6ZIP_TEST_VECTORS_RO_ERROR_MAX )
    {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "vector_idx %u out-of-range", vector_idx, 0, 0 );
        return;
    }

    request = q6zip->get_request();
    Q6ZIP_ASSERT( request );

    request->src.ptr  = (void*)RO_ERROR_VECTORS[ vector_idx ].zipped;
    request->src.len  = RO_ERROR_VECTORS[ vector_idx ].zipped_size;
    request->dst.ptr  = unzipped_page;
    request->dst.len  = sizeof( unzipped_page );
    request->algo     = Q6ZIP_ALGO_UNZIP_RO;
    request->priority = Q6ZIP_FIXED_PRIORITY_HIGH;

    error = q6zip->submit( &request );
    Q6ZIP_ASSERT( !error );

    qurt_timer_sleep( 100000 );

    request = q6zip->get_request();
    Q6ZIP_ASSERT( request );

    request->src.ptr  = (void*)RO_VECTORS_INDEX[ vector_idx ].zipped;
    request->src.len  = RO_VECTORS_INDEX[ vector_idx ].zipped_size;
    request->dst.ptr  = unzipped_page;
    request->dst.len  = sizeof( unzipped_page );
    request->algo     = Q6ZIP_ALGO_UNZIP_RO;
    request->priority = Q6ZIP_FIXED_PRIORITY_HIGH;

    /* Should fail */
    error = q6zip->submit( &request );
    Q6ZIP_ASSERT( !error );
}

static void response (
    q6zip_test_scenario_t * scenario,
    q6zip_request_t const * request,
    q6zip_response_t const * response
)
{
    q6zip_waiter_wake( &waiter );
}

static void stop (q6zip_test_scenario_t * scenario)
{
}

q6zip_test_scenario_t q6zip_test_scenario_error_ro =
{
    .ops = 
    {
        initialize,
        configure,
        start,
        response,
        stop
    }
};
