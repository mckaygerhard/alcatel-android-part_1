#! /usr/bin/env python
import sys
import struct

def pushCompressedBits(bits,numBits,compressedWordStream):
    global numCompressedWords
    global compressedPartialWord
    global numCompressedPartialBits
    temp = bits << numCompressedPartialBits                                 
    numCompressedPartialBits += numBits                                                                                        
    compressedPartialWord = compressedPartialWord | temp   
    if numCompressedPartialBits >= 32:                                                                               
        compressedWordStream.append(compressedPartialWord & 0xFFFFFFFF)
        numCompressedWords += 1                                              
        compressedPartialWord = compressedPartialWord >> 32   
        numCompressedPartialBits -= 32              

def finalizeCompressedBits(compressedWordStream,compressedPartialWord,numCompressedPartialBits):  
    global numCompressedWords
    if numCompressedPartialBits > 0:                                                                                                                                                                                                    
        compressedWordStream.append(compressedPartialWord & 0xFFFFFFFF)  
        numCompressedWords += 1   

def checkAnchor(anchor,val,compressed):
    global anchors
    anchor_val = anchors[anchor]; 
    if anchors[anchor] == val: 
        pushCompressedBits((anchor << 2) + 1,2 + 2,compressed)
        return 1
    elif ((anchor_val & 0xFFFFFC00) == (val & 0xFFFFFC00)):
        pushCompressedBits(((val&0x3FF)<<(2+2))+((anchor<<2)+2),2+2+10,compressed)
        anchors[anchor] = val; 
        return 1
    else:
        return 0

def __GET_BITS(__hold,n):
     return __hold & ((1 << n) - 1)
def __SKIP_BITS_W_CHECK(compressed,size,n): 
    global __numAvailBits   
    global __hold
    global __in_index
    __numAvailBits -= n                                             
    __hold = __hold >> n                                                  
    if __numAvailBits <= 32: 
        if __in_index != size:
            __hold |= compressed[__in_index] << __numAvailBits              
            __in_index += 1                                                                         
            __numAvailBits += 32                                
			
def deltaCompress (uncompressed,compressed):
    global numCompressedWords
    global compressedPartialWord
    global numCompressedPartialBits
    global anchors
    anchors = [0,0,0,0]
    numCompressedWords = 0
    compressedPartialWord = 0
    numCompressedPartialBits = 0
    anchorIndex = 3
    # @warning anandj In HW implementation, compressed data starts at first word
    #compressed.append(len(uncompressed)) 
    #numCompressedWords += 1
    for i in xrange(len(uncompressed)):
        val = uncompressed[i]
        if (val == 0):
            pushCompressedBits(0,2,compressed)
        else:
            anchor = anchorIndex
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor + (4 - 1)) & (4 - 1)
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor + (4 - 1)) & (4 - 1)
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor + (4 - 1)) & (4 - 1)
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchorIndex = (anchorIndex + 1) & (4 - 1)
            anchors[anchorIndex] = val
            pushCompressedBits(3,2,compressed)
            pushCompressedBits(val,32,compressed)
    finalizeCompressedBits(compressed,compressedPartialWord,numCompressedPartialBits)
    return numCompressedWords

def deltaUncompress(compressed,uncompressed):
    global __numAvailBits   
    global __hold
    global __in_index
    __anchors = [0,0,0,0]
    anchorIndex = 3
    __numAvailBits = 0
    __hold = 0
    __in_index = 0
    out_index = 0
    #size = compressed[__in_index]
    size = 1024
    if size == 0:
        return 0
    #__in_index += 1
    compressed_size = len(compressed)
    __SKIP_BITS_W_CHECK(compressed,compressed_size,0)
    if size != 1:
        __SKIP_BITS_W_CHECK(compressed,compressed_size,0)
    while out_index < size:
        code = __GET_BITS(__hold,2)
        if code == 0:
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2)
            uncompressed.append(0)
            out_index += 1
        elif code == 1:
            anchor = __GET_BITS(__hold>>2,2)
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2+2)
            uncompressed.append(__anchors[anchor])
            out_index += 1
        elif code == 2:
            anchor = __GET_BITS(__hold>>2,2)
            delta = __GET_BITS(__hold>>(2+2),10)
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2+2+10)
            val = (__anchors[anchor] & 0xFFFFFC00) | delta
            uncompressed.append(val)
            out_index += 1
            __anchors[anchor] = val
        elif code == 3:
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2)
            val = __hold & 0xFFFFFFFF
            __SKIP_BITS_W_CHECK(compressed,compressed_size,32)
            uncompressed.append(val)
            out_index += 1
            anchorIndex = (anchorIndex + 1) & (4 - 1)
            __anchors[anchorIndex] = val 
    return out_index

BLOCK_SIZE = 1024

def rw_py_compress(page_size=BLOCK_SIZE*4, VA_start=0, input=None):
    instrList=[]
    for word in (input[i:i+4] for i in xrange(0,len(input),4)):
        if len(word) == 4:
            instrList.append( struct.unpack('I',word)[0] )

    n_blocks = len(instrList)/BLOCK_SIZE
    print "n_blocks of RW = %d"%(n_blocks)
    v_addrs = []
    va = VA_start + 2 + 2 + 4 * n_blocks  #2 bytes for n_blocks, 2 for 0, 4 per block start addr

    compressed_text = []
    for block in xrange(n_blocks):
        v_addrs.append( struct.pack('I',va) )
        compressed = []
        #print "calling deltaCompress, block = %d"%(block)
        deltaCompress(instrList[block*BLOCK_SIZE:(block+1)*BLOCK_SIZE],compressed)
        #print "compressed len = %d"%(len(compressed))
        for word in compressed:
            compressed_text.append(struct.pack('I',word))
        va += 4 * len(compressed)

    print "creating metadata for RW"
    metadata = [struct.pack("H",n_blocks), struct.pack("H",0)]
    metadata += v_addrs
    metadata += compressed_text

    return ''.join(metadata)  #joins list elements together as string with no spaces


def __main():
    if len(sys.argv) > 1:
        if sys.argv[1]=="COMPRESS":
            uncompressed = []
            with open(sys.argv[2], 'rb') as f:
                while True:
                    word = f.read(4)
                    if len(word) != 4:
                        break
                    uncompressed.append( struct.unpack('I', word)[0] )

            print "len of uncompressed = %d"%(len(uncompressed))
            #print "uncompressed[%d] = 0x%x"%(len(uncompressed)/2, uncompressed[len(uncompressed)/2]) 
            compressed = []
            compressed_size = deltaCompress(uncompressed,compressed)
            print "compressed len words = %d"%(compressed_size)
            print "compression ratio = %f" % (float(len(compressed))/len(uncompressed))

            compressed_file = open("compressed_page.bin", 'wb')
            for i in compressed:
                 compressed_file.write(struct.pack('I', i))         
            compressed_file.close()

        if sys.argv[1]=="UNCOMPRESS":
            compressed = []
            with open(sys.argv[2], 'rb') as f:
                while True:
                    word = f.read(4)
                    if len(word) != 4:
                        break
                    compressed.append( struct.unpack('I', word)[0] )

            print "compressed len words = %d"%(len(compressed))
            uncompressed = []
            uncompressed_size = deltaUncompress(compressed,uncompressed)
            print "compression ratio = %f" % (float(len(compressed))/len(uncompressed))
            print "uncompressed len words = %d"%(uncompressed_size)
            #print "uncompressed[%d] = 0x%x"%(len(uncompressed)/2, uncompressed[len(uncompressed)/2])

            #uncompressed_file = open("uncompressed_page.bin", 'wb')
            uncompressed_file = open("decompressedrw"+sys.argv[3]+".bin", 'wb')
            for i in uncompressed:
                 uncompressed_file.write(struct.pack('I', i))         
            uncompressed_file.close()
    else:
        print "python rw_pycompress.py COMPRESS/DECOMPRESS inputFileName"

if __name__ == '__main__':
    __main()






