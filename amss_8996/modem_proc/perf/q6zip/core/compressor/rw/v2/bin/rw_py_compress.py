#! /usr/bin/env python
import struct

def pushCompressedBits(bits,numBits,compressedWordStream):
    global numCompressedWords
    global compressedPartialWord
    global numCompressedPartialBits
    temp = bits << numCompressedPartialBits                                 
    numCompressedPartialBits += numBits                                                                                        
    compressedPartialWord = compressedPartialWord | temp   
    if numCompressedPartialBits >= 32:                                                                               
        compressedWordStream.append(compressedPartialWord & 0xFFFFFFFF)
        numCompressedWords += 1                                              
        compressedPartialWord = compressedPartialWord >> 32   
        numCompressedPartialBits -= 32              

def finalizeCompressedBits(compressedWordStream,compressedPartialWord,numCompressedPartialBits):  
    global numCompressedWords
    if numCompressedPartialBits > 0:                                                                                                                                                                                                    
        compressedWordStream.append(compressedPartialWord & 0xFFFFFFFF)  
        numCompressedWords += 1   

def checkAnchor(anchor,val,compressed):
    global anchors
    anchor_val = anchors[anchor]; 
    if anchors[anchor] == val: 
        pushCompressedBits((anchor << 2) + 1,2 + 2,compressed)
        #print'full match ', anchor
        return 1
    elif ((anchor_val & 0xFFFFFC00) == (val & 0xFFFFFC00)):
        pushCompressedBits(((val&0x3FF)<<(2+2))+((anchor<<2)+2),2+2+10,compressed)
        anchors[anchor] = val; 
        #print 'partial match ', anchor, " val %8x" %val
        return 1
    else:
        return 0

def deltaCompress (uncompressed,compressed):
    global numCompressedWords
    global compressedPartialWord
    global numCompressedPartialBits
    global anchors
    anchors = [0,0,0,0]
    numCompressedWords = 0
    compressedPartialWord = 0
    numCompressedPartialBits = 0
    anchorIndex = 3
    #compressed.append(len(uncompressed)) 
    #numCompressedWords += 1
    for i in xrange(len(uncompressed)-1):
        val = uncompressed[i]
        if (val == 0):
            #print 'zero'
            pushCompressedBits(0,2,compressed)
        else:
            anchor = 0
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor + 1) & (4 - 1)
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor +1 ) & (4 - 1)
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor + 1 ) & (4 - 1)
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchorIndex = (anchorIndex + 1) & (4 - 1)
            #print 'no match val = %x, putting in anchor ' %val, anchorIndex
            anchors[anchorIndex] = val
            pushCompressedBits(3,2,compressed)
            pushCompressedBits(val,32,compressed)
    val = uncompressed[len(uncompressed)-1]
    pushCompressedBits(3,2,compressed)
    pushCompressedBits(val,32,compressed)
    #print "%x  " %compressed [-2], "%x" %compressed[-1]
    finalizeCompressedBits(compressed,compressedPartialWord,numCompressedPartialBits)
    return numCompressedWords

BLOCK_SIZE = 1024

def rw_py_compress(page_size=BLOCK_SIZE*4, VA_start=0, input=None):
    instrList=[]
    for word in (input[i:i+4] for i in xrange(0,len(input),4)):
        if len(word) == 4:
            instrList.append( struct.unpack('I',word)[0] )

    n_blocks = len(instrList)/BLOCK_SIZE
    print "n_blocks of RW = %d"%(n_blocks)
    v_addrs = []
    va = VA_start + 2 + 2 + 4 * n_blocks  #2 bytes for n_blocks, 2 for 0, 4 per block start addr

    compressed_text = []
    for block in xrange(n_blocks):
        v_addrs.append( struct.pack('I',va) )
        compressed = []
        #print "calling deltaCompress, block = %d"%(block)
        deltaCompress(instrList[block*BLOCK_SIZE:(block+1)*BLOCK_SIZE],compressed)
        #print "deltaCompress Block[%d] compressed len = %d"%(block,len(compressed))
        for word in compressed:
            compressed_text.append(struct.pack('I',word))
        va += 4 * len(compressed)

    print "creating metadata for RW"
    metadata = [struct.pack("H",n_blocks), struct.pack("H",0)]
    metadata += v_addrs
    metadata += compressed_text

    return ''.join(metadata)  #joins list elements together as string with no spaces

if __name__ == '__main__':
    rw_py_compress()

