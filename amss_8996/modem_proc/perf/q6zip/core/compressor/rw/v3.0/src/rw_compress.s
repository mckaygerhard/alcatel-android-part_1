	.file	"rw_compress.c"
	.text
.Ltext0:
	.p2align 2
	.p2align 4,,15
	.globl deltaCompress
	.type	deltaCompress, @function
deltaCompress:
	.file 1 "..\\compressor\\rw\\vRic\\src\\rw_compress.c"
	// saved LR + FP regs size (bytes) = 0
	// callee saved regs size (bytes) = 40
	// local vars size (bytes) = 16
	// fixed args size (bytes) = 0
	{
		r6 = and(r0,#31)
		r2 = asl(r2,#2)
		r3 = #32
	}
	{
		r2 = add(r6,add(r2,#31))
		memd(r29+#-8) = r17:16
		memd(r29+#-16) = r19:18
		r29 = add(r29,#-56)
	}
	{
		r2 = lsr(r2,#5)
		memd(r29+#32) = r21:20
		memd(r29+#24) = r23:22
		r9 = r29
	}
	{
		r2=combine(r3.l,r2.l)
		memd(r29+#16) = r25:24
		r28 = add(r29,#12)
		r6 = sub(r0,r6)
	}
	{
		r3:2=combine(#32,r2)
	}
	//l2fetch(r6,r3:2)
	{
		//loop0(.L2,#136)
		r2 = r1
	}
/*.p2align 5
.L2:
	{
		dczeroa(r2)
		r2 = add(r2,#32)
	}:endloop0 // start=.L2*/
	{
		r4 = #1024
		r22 = r9
		r8 = r1
	}
	{
		r3:2 = #0
		r12 = #0
		r13 = #0
		r7:6 = #0
	}
	{
		memd(r9+#8) = r3:2
		memd(r29+#0) = r3:2
		/*r11:10 = r3:2
		r15:14 = r3:2*/

		p3 = cmp.eq(r0,r0)
		r2 = #0
	}
	{
		r17:16 = #1
		r21:20 = #2
	}
	{
		dcfetch(r0)
		r25:24 = #3
		loop0(.START,r4)		
	}
	 {
		 dcfetch(r0+#32)
		 R4=##4096
	 }
	 {
		 M0=R4
	 }
	 {
		 CS0=R0
	 }
	 {
		 dcfetch(r0+#64)
		 r3 = memw(r0++#4:circ(M0))
		 r5:4 = combine(#0,#0)
		 r19:18 = combine(#0,#0)
	 }

.p2align 5
.START:
	{
	    p0=cmp.eq(r3,#0)
		if (!p0.new) jump:nt .NO_ZERO_MATCH
		p2 = cmp.gtu(r2, #29); 
		if (p0.new) r2 = add(r2,#2)
		dcfetch(r0+#64)
	}
	{
		memw(r8+#0) = r6
		if (!p3) jump .L14
		if (p2) r6=r7
		if (p2) r8 = add(r8,#4)
	}
    {
		if (p2) r7=#0
		if (p2) r2 = add(r2,#-32)
		r12 = #0
		r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.L14:
	{
		if (p2) r2 = add(r2,#-32)
		r11:10 = combine(#0,r12)
		r12=#0
		p3 = cmp.eq(r0,r0)
	}
	{
		if (p2) r7=#0
		r11:10 = asl(r11:10,r2)
		r2 = add(r2,r13)
		p0 = cmp.gtu(r2,#31)
	}
	{
		r7:6 = or(r7:6,r11:10)
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r12=#32
	}
	{
		memw(r8+#0) = r6
		r7:6 = lsr(r7:6,r12)
		r13:12 = combine(#0,#0)
		r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.NO_ZERO_MATCH:
	{
		r10 = xor(r3,r4)
		r11 = xor(r3,r5)
		r14 = xor(r3,r18)
		r15 = xor(r3,r19)
	}
	{
		r10=cl0(r10)
		r11=cl0(r11)
		dcfetch(r0+#32)
	}
	{
		r14=cl0(r14)
		r15=cl0(r15)
		p0 = cmp.eq(r10,#32) 
		p1 = cmp.eq(r11,#32) 
	}
	{
		if (p0) jump .EXACT_ANCHOR_0
		if (p1) jump .EXACT_ANCHOR_1
		p0 = cmp.eq(r14,#32)
		p1 = cmp.eq(r15,#32)
	}
	{
		if (p0) jump .EXACT_ANCHOR_2
		if (p1) jump .EXACT_ANCHOR_3
		p0 = cmp.gt(r10,#21)
		p1 = cmp.gt(r11,#21)		
    }
	/* Partial Matches*/	
	{
		if (p0) jump .PARTIAL_ANCHOR_0
		if (p1) jump .PARTIAL_ANCHOR_1
		p0 = cmp.gt(r14,#21)
		p1 = cmp.gt(r15,#21)
	}
	{
		if (p0) jump .PARTIAL_ANCHOR_2
		if (p1) jump .PARTIAL_ANCHOR_3
	}

.RAW_CODE:
	{
		r11:10 = asl(r25:24,r2)
		memw(r22++#4) = r3
		r2 = add(r2,#2)
		p2 = cmp.eq(r22,r28)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
		if (p2) r22 = r9
		r19:18 = memd(r9+#8)
	}
	{
		r5:4 = memd(r9+#0)
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
	}
	{
		if (p0) r7=#0
		if (p0) r6=r7
		r11:10 = combine(#0,r12)
		if (!p3) jump .L61
	}
	.falign
.L35:
	{
		r13:12 = combine(#32,r3)
		p3 = cmp.gt(r0,r0)
		r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.L61:
	{
		r11:10 = asl(r11:10,r2)
		r2 = add(r2,r13)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (!p0) jump .L35
	}
	{
		r7:6 = lsr(r7:6,#32)
		r13:12 = combine(#32,r3)
	    r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}

.PARTIAL_ANCHOR_2:
    {
		r11:10 = asl(r21:20,r2)
		r2 = add(r2,#2)
    }
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
		r10 = #0		
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0)  r10=#32
	}
	{
		r7:6 = lsr(r7:6,r10)
		r11:10 = combine(#0,r12)
		if (!p3) jump .L48
	}
	.falign
.L30:
	{
		memw(r9+#8) = r3
		r18=r3 //Anchor 2 updated
		r3 = and(##4092,asl(r3,#2))
	}
	{
		r13 = #12
		p3 = cmp.gt(r0,r0)
		r12 = add(r3,#2)
		r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.EXACT_ANCHOR_0:
	{
		r11:10 = asl(r17:16,r2)
		r2 = add(r2,#2)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
		r10=#0
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r10=#32
	}
	{
		r7:6 = lsr(r7:6,r10)
		r11:10 = combine(#0,r12)
		if (!p3) jump .L51
	}
	.falign
.L18:
	{
		r12 = #0
		r13 = #2
		p3 = cmp.gt(r0,r0)
		r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.EXACT_ANCHOR_1:
	{
		r11:10 = asl(r17:16,r2)
		r2 = add(r2,#2)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r6=r7
	}
	{
	    if (p0) r7=#0
		r11:10 = combine(#0,r12)
		if (!p3) jump .L52
	}
	.falign
.L20:
	{
		r12 = #1
		r13 = #2
		p3 = cmp.gt(r0,r0)
		r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
	 jump .EXIT
	}

.p2align 5
.EXACT_ANCHOR_2:
	{
		r11:10 = asl(r17:16,r2)
		r2 = add(r2,#2)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
		r10=#0
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r10=#32
	}
	{
		r7:6 = lsr(r7:6,r10)
		r11:10 = combine(#0,r12)
		if (!p3) jump .L53
	}
	.falign
.L22:
	{
		r12 = #2
		r13 = #2
		p3 = cmp.gt(r0,r0)
		 r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.L53:
	{
		r11:10 = asl(r11:10,r2)
		r2 = add(r2,r13)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (!p0) jump .L22
	}
	{
		r7:6 = lsr(r7:6,#32)
		r13:12 = combine(#2,#2)
		r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.EXACT_ANCHOR_3:
	{
		r11:10 = asl(r17:16,r2)
		r2 = add(r2,#2)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
		r10=#0
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r10=#32
	}
	{
		r7:6 = lsr(r7:6,r10)
		r11:10 = combine(#0,r12)
		if (!p3) jump .L54
	}
	.falign
.L24:
	{
		r12 = #3
	    r13 = #2
	    p3 = cmp.gt(r0,r0)
		r3 = memw(r0++#4:circ(M0))
    }:endloop0 // start=.L36
    {
	 jump .EXIT
    }
.p2align 5
.L54:
	{
		r11:10 = asl(r11:10,r2)
		r2 = add(r2,r13)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (!p0) jump .L24
	}
	{
		r7:6 = lsr(r7:6,#32)
		r13:12 = combine(#2,#3)
		r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.PARTIAL_ANCHOR_0:
	{
		r11:10 = asl(r21:20,r2)
		r2 = add(r2,#2)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r6=r7
	}
	{
		if (p0) r7=#0
		r11:10 = combine(#0,r12)
		if (!p3) jump .L55
	}
	.falign
.L26:
	{
		r12 = r3
		memw(r9+#0) = r3
		r4=r3  //Anchor 0 updated
		r13 = #12
	}
	{
		p3 = cmp.gt(r0,r0)
		r12 = and(##4092,asl(r12,#2))
		r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.L55:
	{
		r11:10 = asl(r11:10,r2)
		r2 = add(r2,r13)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
		r12 = #0
		r13 = #12
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r12 = #32
	}
	{
		r7:6 = lsr(r7:6,r12)
		r12 = r3
		memw(r9+#0) = r3
		r4=r3  //Anchor 0 updated
	}
	{
		p3 = cmp.gt(r0,r0)
		r12 = and(##4092,asl(r12,#2))
		r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.PARTIAL_ANCHOR_1:
	{		
		r11:10 = asl(r21:20,r2)
		r2 = add(r2,#2)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
		r11 = #0
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r11=#32
	}
	{
		r7:6 = lsr(r7:6,r11)
		r11:10 = combine(#0,r12)
		if (!p3) jump .L57
	}
	.falign
.L28:
	{
		memw(r9+#4) = r3
		r5=r3  //Anchor 1 updated
		r3 = and(##4092,asl(r3,#2))
	}
	{
		r13 = #12
		p3 = cmp.gt(r0,r0)
		r12 = add(r3,#1)
		r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.PARTIAL_ANCHOR_3:
	{
		r11:10 = asl(r21:20,r2)
		r2 = add(r2,#2)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
		r10=#0
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r10=#32
	}
	{
		r7:6 = lsr(r7:6,r10)
		r11:10 = combine(#0,r12)
		if (!p3) jump .L60
	}
	.falign
.L32:
	{
		memw(r9+#12) = r3
		r19=r3  //Anchor  updated
		r3 = and(##4092,asl(r3,#2))
	}
	{
		r13 = #12
		p3 = cmp.gt(r0,r0)
		r12 = add(r3,#3)
		 r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.L51:
	{
		r11:10 = asl(r11:10,r2)
		r2 = add(r2,r13)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (!p0) jump .L18
	}
	{
		r7:6 = lsr(r7:6,#32)
		r13:12 = combine(#2,#0)
		 r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.L60:
	{
		r11:10 = asl(r11:10,r2)
		r2 = add(r2,r13)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r6=r7
	}
	{
		if (p0) r7=#0
		jump .L32
	}
.p2align 5
.L52:
	{
		r11:10 = asl(r11:10,r2)
		r2 = add(r2,r13)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (!p0) jump .L20
	}
	{
		r7:6 = lsr(r7:6,#32)
		r13:12 = combine(#2,#1)
		 r3 = memw(r0++#4:circ(M0))
	}:endloop0 // start=.L36
	{
		jump .EXIT
	}
.p2align 5
.L48:
	{
		r11:10 = asl(r11:10,r2)
		r2 = add(r2,r13)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r6=r7
	}
	{
		if (p0) r7=#0
		jump .L30
	}
.p2align 5
.L57:
	{
		r11:10 = asl(r11:10,r2)
		r2 = add(r2,r13)
	}
	{
		r7:6 = or(r7:6,r11:10)
		p0 = cmp.gtu(r2,#31)
	}
	{
		memw(r8+#0) = r6
		if (p0) r8 = add(r8,#4)
		if (p0) r2 = add(r2,#-32)
		if (p0) r6=r7
	}
	{
		if (p0) r7=#0
		jump .L28
	}
.p2align 5
.EXIT1:
	{
		r3:2 = asl(r3:2,r0)
		r0 = add(r0,r13)
	}
	{
		r3:2 = or(r3:2,r7:6)
		p0 = cmp.gtu(r0,#31)
		if (p0.new) r0 = add(r0,#-32)
	}
	{
		r6 = r2
		memw(r8+#0) = r2
		if (p0) r8 = add(r8,#4)
	}
	{
		if (p0) r6 = r3
		jump .EXIT0
	}
.falign
.EXIT:
	{
		memw(r8+#0) = r6
		r0 = add(r2,#2)
	}
	{
		p0 = cmp.gtu(r0, #31); if (!p0.new) jump:nt .EXIT2
	}
	{
		r8 = add(r8,#4)
		r7:6 = lsr(r7:6,#32)
		r0 = add(r2,#-30)
	}
.falign
.EXIT2:
	{
		r3:2 = combine(#0,r12)
		if (!p3) jump .EXIT1
	}
.falign
.EXIT0:
	{
		p0 = cmp.eq(r0,#0)
		memw(r8+#0) = r6
		if (!p0.new) r8 = add(r8,#4)
	}
	{
		r25:24 = memd(r29+#16)
		r23:22 = memd(r29+#24)
		r0 = sub(r8,r1)
	}
	{
		r21:20 = memd(r29+#32)
		r19:18 = memd(r29+#40)
		r0 = and(r0,#-4)
	}
	{
		r0 = add(r0,#4)
		r17:16 = memd(r29+#48)
		r29 = add(r29,#56)
		jumpr r31
	}
	.size	deltaCompress, .-deltaCompress

