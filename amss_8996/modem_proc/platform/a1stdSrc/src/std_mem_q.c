#include <string.h>
#include <stdio.h>

#include "AEEstd.h"

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

#define TEST(x) \
   do { if (!(x)) { \
      printf(__FILENAME__ ":%d: test failed: %s\n", __LINE__, #x); \
      errs++; \
   } } while (0)


static int StrEQ(const char *a, const char *b)
{
   return 0 == std_strcmp(a,b);
}

static void InitMemsetBuf(char *p, int size)
{
   int idx;

   for (idx = 0; idx < size; idx++) {
      *(p + idx) = '-';
   }
}

static int CmpMemsetBuf(char *p, int size, int r1, int r2)
{
   int idx;
   int errs = 0;

   for (idx = 0; idx < size; idx++) { 
      if ((idx < r1) || (idx >= r2)) { 
         TEST('-' == *(p+idx)); 
      } else { 
         TEST('o' == *(p+idx));
      }
   } 

   return errs;
}
   
static int std_memset_align_test(char *p, int size)
{
   char *t;
   int align;
   int size1;
   int errs = 0;

   for (align = 0; align < 32; align++) {
      InitMemsetBuf(p, size);
      t = p + align;
      for (size1 = 1; size1 <= size - align; size1++) { 
         std_memset(t, 'o', size1);
         errs += CmpMemsetBuf(p, size, align, align + size1);
         InitMemsetBuf(t, size1);
      }

      InitMemsetBuf(p, size);
      std_memset(p, 'o', size - align);
      errs += CmpMemsetBuf(p, size, 0, size - align);

      InitMemsetBuf(p, size);
      t = p + align;
      std_memset(t, 'o', size - (2 * align));
      errs += CmpMemsetBuf(p, size, align, size - align);
   }

   return errs;
}

static int std_memset_test(void)
{
   int  errs = 0;
   char pc[20];

   std_strlcpy(pc, "abcdef", sizeof(pc));

   TEST( std_memset(pc+1, 'x', 0) == pc+1 );
   TEST( StrEQ(pc, "abcdef") );

   TEST( std_memset(pc+1, 'x', MIN_INT32) == pc+1 );
   TEST( StrEQ(pc, "abcdef") );

   TEST( std_memset(pc+1, 'x', 1) == pc+1 );
   TEST( StrEQ(pc, "axcdef") );

   TEST( std_memset(pc+1, 'x', 2) == pc+1 );
   TEST( StrEQ(pc, "axxdef") );

   {
      uint8 buf[10] = {'-','-','-','-','-','-','-','-','-','-'};
      uint8 *pu8Ret;

      pu8Ret = std_memset(buf, 'f', 5);
      TEST(0 == std_memcmp(buf, "fffff-----", 10));
      TEST(pu8Ret == buf);

      pu8Ret = std_memset(&buf[5], 'o', 5);
      TEST(0 == std_memcmp(buf, "fffffooooo", 10));
      TEST(pu8Ret == &buf[5]);

      pu8Ret = std_memset(buf, 0, 0);
      TEST(0 == std_memcmp(buf, "fffffooooo", 10));
      TEST(pu8Ret == buf);
   }

   {
      char buf[256];

      errs += std_memset_align_test(buf,sizeof(buf));
   }

   return errs;
}

static void InitMemmoveBuf(char *p, int size)
{
   int idx;

   for (idx = 0; idx < size; idx++) {
      *(p + idx) = (char)idx;
   }
}

static int CmpMemmoveBuf(char *p1, char *p2, int size, int size1, int align1, int align2)
{
   int idx;
   int errs = 0;
   char *pc = p1 + align1;

   for (idx = 0; idx < size; idx++) {
      if ((idx < align2) || (idx >= (size - align1)) || (idx >= (size1 + align2))) {
         TEST(*(p1+idx) == *(p2+idx));
      } else {
         TEST(*pc++ == *(p2+idx));
      }
   }

   return errs;
}

static int std_memmove_align_test(char *p1, char *p2, char *p3, int size)
{
   char *t1, *t2;
   int align1, align2;
   int range1, range2, range3;
   int size1;
   int errs = 0;

   range1 = range2 = (size > 31) ? 31 : size;

   InitMemmoveBuf(p3, size);
   for (align1 = 0; align1 < range1; align1++) {
      InitMemmoveBuf(p1, size);
      for (align2 = 0; align2 < range2; align2++) {
         t1 = p1 + align1;
         t2 = p2 + align2;
         range3 = size - align2 - align1;
         InitMemmoveBuf(p2, size);
         for (size1 = 1; size1 <= range3; size1++) {
            InitMemmoveBuf(p2, align2 + size1);
            std_memmove(t2, t1, size1);
            errs += CmpMemmoveBuf(p3, p2, size, size1, align1, align2);
         }
      }   
   }

   return errs;
}

int std_memmove_test(void)
{
   #include "AEEbeginpack.h"
   struct {
      char guard1;
      char buf[16];
      char guard2;
   } 
   #include "AEEendpack.h"
   mm;

   uint32 i;
   int  errs = 0;

   mm.guard1 = mm.guard2 = 0;

   memset((void *)mm.buf, 0, sizeof(mm.buf));
   if (mm.buf != std_memmove((void *)mm.buf, "hi", 0)) {
      printf("std_memmove(mm.buf, \"hi\", 0) returned wrong thing\n");
      errs++;
   }

   for (i = 0; i < sizeof(mm.buf); i++) {
      if (mm.buf[i]) {
         printf("std_memmove(mm.buf, \"hi\", 0) corrupted mm.buf\n");
         errs++;
      }
   }
   if (mm.guard1 || mm.guard2) {
      printf("std_memmove(mm.buf, \"hi\", 0) corrupted guards)\n");
      errs++;
   }

   memset((void *)mm.buf, 0, sizeof(mm.buf));
   if (mm.buf != std_memmove((void *)mm.buf, "hi", -1)) {
      printf("std_memmove(mm.buf, \"hi\", -1) returned wrong thing\n");
      errs++;
   }

   for (i = 0; i < sizeof(mm.buf); i++) {
      if (mm.buf[i]) {
         printf("std_memmove(mm.buf, \"hi\", -1) corrupted mm.buf\n");
         errs++;
      }
   }
   if (mm.guard1 || mm.guard2) {
      printf("std_memmove(mm.buf, \"hi\", 0) corrupted guards)\n");
      errs++;
   }

   memset((void *)mm.buf, 0, sizeof(mm.buf));
   if (mm.buf != std_memmove((void *)mm.buf, "hi", 1)) {
      printf("std_memmove(mm.buf, \"hi\", 1) returned wrong thing\n");
      errs++;
   }

   if (mm.buf[0] != 'h') {
      printf("std_memmove(mm.buf, \"hi\", 1) didn't set mm.buf[0] to 'h'\n");
      errs++;
   }

   for (i = 1; i < sizeof(mm.buf); i++) {
      if (mm.buf[i]) {
         printf("std_memmove(mm.buf, \"hi\", 1) corrupted mm.buf\n");
         errs++;
      }
   }
   if (mm.guard1 || mm.guard2) {
      printf("std_memmove(mm.buf, \"hi\", 1) corrupted guards)\n");
      errs++;
   }

   memset((void *)mm.buf, 0, sizeof(mm.buf));
   if (mm.buf != std_memmove((void *)mm.buf, "hi", 2)) {
      printf("std_memmove(mm.buf, \"hi\", 2) returned wrong thing\n");
      errs++;
   }

   if (mm.buf[0] != 'h' || mm.buf[1] != 'i') {
      printf("std_memmove(mm.buf, \"hi\", 1) didn't set mm.buf[0] to 'h'\n");
      errs++;
   }

   for (i = 2; i < sizeof(mm.buf); i++) {
      if (mm.buf[i]) {
         printf("std_memmove(mm.buf, \"hi\", 2) corrupted mm.buf\n");
         errs++;
      }
   }
   if (mm.guard1 || mm.guard2) {
      printf("std_memmove(mm.buf, \"hi\", 1) corrupted guards (%c, %c)\n", mm.guard1, mm.guard2);
      errs++;
   }

   for (i = 0; i < sizeof(mm.buf); i++) {
      mm.buf[i] = (char) i;
   }

   if (mm.buf != std_memmove((void *)mm.buf, (void *)(mm.buf+1), sizeof(mm.buf)-1)) {
      printf("std_memmove(mm.buf, mm.buf+1, sizeof(mm.buf)-1) returned wrong thing\n");
      errs++;
   }

   for (i = 0; i < sizeof(mm.buf) - 1; i++) {
      if (mm.buf[i] != i + 1) {
         printf("std_memmove(mm.buf, mm.buf+1, sizeof(mm.buf)-1) didn't work\n");
         errs++;
      }
   }
   if (mm.buf[i] != i) {
      printf("std_memmove(mm.buf, mm.buf+1, sizeof(mm.buf)-1) didn't work\n");
      errs++;
   }

   if (mm.guard1 || mm.guard2) {
      printf("std_memmove(mm.buf, mm.buf+1, sizeof(mm.buf)-1) corrupted guards (%c, %c)\n", mm.guard1, mm.guard2);
      errs++;
   }

   for (i = 0; i < sizeof(mm.buf); i++) {
      mm.buf[i] = (char) i;
   }

   if (mm.buf + 1 != std_memmove((void *)(mm.buf+1), (void *)mm.buf, sizeof(mm.buf)-1)) {
      printf("std_memmove(mm.buf + 1, mm.buf, sizeof(mm.buf)-1) returned wrong thing\n");
      errs++;
   }

   if (mm.buf[0] != 0) {
      printf("std_memmove(mm.buf+1, mm.buf, sizeof(mm.buf)-1) didn't work\n");
      errs++;
   }

   for (i = 1; i < sizeof(mm.buf); i++) {
      if (mm.buf[i] != i - 1) {
         printf("std_memmove(mm.buf+1, mm.buf, sizeof(mm.buf)-1) didn't work\n");
         errs++;
      }
   }

   if (mm.guard1 || mm.guard2) {
      printf("std_memmove(mm.buf+1, mm.buf, sizeof(mm.buf)-1) corrupted guards (%c, %c)\n", mm.guard1, mm.guard2);
      errs++;
   }

   for (i = 0; i < sizeof(mm.buf); i++) {
      mm.buf[i] = (char) i;
   }

   if (mm.buf != std_memmove((void *)mm.buf, (void *)mm.buf, sizeof(mm.buf))) {
      printf("std_memmove(mm.buf, mm.buf, sizeof(mm.buf)) returned wrong thing\n");
      errs++;
   }

   for (i = 0; i < sizeof(mm.buf); i++) {
      if (mm.buf[i] != i) {
         printf("std_memmove(mm.buf, mm.buf, sizeof(mm.buf)) didn't work\n");
         errs++;
      }
   }

   if (mm.guard1 || mm.guard2) {
      printf("std_memmove(mm.buf, mm.buf, sizeof(mm.buf)) corrupted guards (%c, %c)\n", mm.guard1, mm.guard2);
      errs++;
   }

   {
      uint8 buf[10] = {'1','2','3','4','5','6','7','8','9','0'}; 
      uint8 buf2[10] = {'0','9','8','7','6','5','4','3','2','1'}; 
      uint8 *pu8Ret;

      pu8Ret = std_memmove(buf, buf2, 5);
      TEST(0 == std_memcmp(buf, "0987667890", 10));
      TEST(pu8Ret == buf);

      pu8Ret = std_memmove(buf2, &buf2[5], 5);
      TEST(0 == std_memcmp(buf2, "5432154321", 10));
      TEST(pu8Ret == buf2);

      pu8Ret = std_memmove(buf2, &buf2, 10);
      TEST(0 == std_memcmp(buf2, "5432154321", 10));
      TEST(pu8Ret == buf2);

      pu8Ret = std_memmove(buf2, &buf2, 0);
      TEST(0 == std_memcmp(buf2, "5432154321", 10));
      TEST(pu8Ret == buf2);
   }

   {
      char buf1[128];
      char buf2[128];
      char buf3[128];

      errs += std_memmove_align_test(buf1, buf2, buf3, sizeof(buf1));
      errs += std_memmove_align_test(buf1, buf1, buf3, sizeof(buf1));
   }

   return errs;
}

int main(void)
{
   return
   std_memset_test() +
   std_memmove_test();
}

