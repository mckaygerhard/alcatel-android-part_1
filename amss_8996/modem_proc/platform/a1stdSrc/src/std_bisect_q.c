#include "std_bisect.c"

static int cmp_int32(void* _, const void* aa, const void* bb)
{
   return *((int32*)aa) == *((int32*)bb) ? 0 : (*((int32*)aa) < *((int32*)bb) ? -1 : 1);
}

int main(void) 
{
   int nErrs = 0;
   int ii, pos;
   int elems[] = {0,1,2,3,4,4,6,7,8,9,10};
   for (ii = 0; ii < STD_ARRAY_SIZE(elems); ++ii) {
      pos = std_bisect(elems, STD_ARRAY_SIZE(elems), sizeof(elems[0]), &elems[ii], cmp_int32, 0);
      nErrs += (elems[ii] == 4) || pos == elems[ii] ? 0 : 1;
      nErrs += ( ii >= elems[pos] && (pos >= (STD_ARRAY_SIZE(elems) - 1) || ii < elems[pos + 1])) ? 0 : 1;
   }
   ii = 5;
   pos = std_bisect(elems, STD_ARRAY_SIZE(elems), sizeof(ii), &ii, cmp_int32, 0);
   nErrs += pos == 6 ? 0 : 1;
   ii = 11;
   pos = std_bisect(elems, STD_ARRAY_SIZE(elems), sizeof(ii), &ii, cmp_int32, 0);
   nErrs += pos ==  STD_ARRAY_SIZE(elems) ? 0 : 1;
   return nErrs;
}

