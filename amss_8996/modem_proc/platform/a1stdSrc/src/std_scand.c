/*
=======================================================================

FILE:         std_scand.c

SERVICES:     ASCII -> double conversion

=======================================================================
        Copyright (c) 2009 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary and Confidential
=======================================================================
*/

#include "AEEstd.h"
#include "std_dtoa.h"
#include "math.h"

#define INFINITY_STRING    "INFINITY"
#define INF_STRING         "INF"
#define NAN_STRING         "NAN"
#define DECIMAL_POINT_CHAR '.'
#define DECIMAL_DIGS       17
#define MAX_SIG_DIGITS     60

#define IsAsciiDigit(c)       STD_BETWEEN(c, '0', '9'+1)
#define IsAsciiAlpha(c)       STD_BETWEEN((c)|32, 'a', 'z'+1)
#define AsciiToLower(c)       ((c)|32)
#define IsAsciiHexDigit(c)    (STD_BETWEEN(c, '0', '9'+1) || \
                               STD_BETWEEN((c)|32, 'a', 'f'+1))
#define AsciiDecimalToInt(c)  ((c) - '0')
#define AsciiHexToInt(c)      (AsciiToLower(ch) - 'a' + 10)
#define FP_POW_10(n)          fp_pow_10(n)
#define FP_POW_2(n)           ldexp(1.0,(n))

#define EXP_OVERFLOW(b,e)     ( (b) == 16 ? (e > STD_DTOA_DP_MAX_EXPONENT) : \
                                            (e > STD_DTOA_DP_MAX_EXPONENT_DEC) )
#define EXP_UNDERFLOW(b,e)    ( (b) == 16 ? (e < STD_DTOA_DP_MIN_EXPONENT_DENORM) : \
                                            (e < STD_DTOA_DP_MIN_EXPONENT_DEC_DENORM) )
#define EXPONENT_MULT(b,e)    ( (b) == 16 ? FP_POW_2(e) : FP_POW_10(e) )

//
// Useful Macros
// 
#define IS_NULL(p)               ( (p) == NULL )
#define NOT_NULL(p)              !IS_NULL(p)

static double ParseNumber( const char** ppch, int nRadix,
                           int* pnExp, int* pnExpMult )
//
// Expects a string in the form:
//    <Integral Part>[.[<Fractional Part>][<Rest of the String>]]
// and returns the corresponding converted number without factoring in
// the decimal point.
// For example:
// If input string equals 123.456e87 and radix 10:
//    return value = 123456.0
//    *pnExp = 2   // Calculated exponent for an equivalent scientific notation.
//    *pnExpMult = -3   // the exponent multiplier to recover the actual number.
// i.e., the actual number can be recovered as:
//    return value * radix^(*pnExpMult)
//    = 123456.0 * 10^-3 = 123.456
// *pnExp contains the calculated exponent for the parsed number when
// it is represented in scientific notation. In this case, 
//    123.456 = 1.23456e2
// 
// If input string equals 0.0abcp23 and radix 16:
//    *pnExp = -2   // Calculated exponent for an equivalent scientific notation.
//    *pnExpMult = -4   // the exponent multiplier to recover the actual number.
//    return value = abc.0
// i.e., the actual number can be recovered as:
//    return value * radix^(*pnExpMult)
//    = abc.0 * 2^-4 = 0.0abc
// *pnExp contains the calculated exponent for the parsed number when
// it is represented in scientific notation. In this case,
//    0.0abc = a.bcp-2
// 
// Note that this function does not parse any explicit exponents that may have
// been specified in the 'Rest of the String' segment of the input string.
// 
{
   double dRet = 0.0;
   unsigned char ch = **ppch;
   int nIntDigits = 0;
   int nFracDigits = 0;
   char SigDigits[MAX_SIG_DIGITS] = {0};
   boolean bLeadingZeroes = TRUE;
   int nDPPos = -1;
   int nFirstNZDigitPos = -1;
   int nZeroesStartPos = -1;
   int nZeroes = 0;
   int nSigDigits = 0;
   int nI = 0;
   int nMaxSigDigits = (nRadix == 16) ? MAX_SIG_DIGITS : DECIMAL_DIGS;

#define RESET_ZERO_POS_DATA     nZeroes = 0; nZeroesStartPos = -1;

   for( nI = 0, nSigDigits = 0; ; nI++ )
   {
      ch = (*ppch)[nI];

      // Check for the decimal point character
      if( DECIMAL_POINT_CHAR == ch )
      {
         if( nDPPos >= 0 )
         {
            // Already seen a decimal point character
            break;
         }
         nDPPos = nI;

         // Any leading zeroes till this point need to be discarded.
         // However, any significant zeroes till this point need to be
         // stored. Also, leading zeroes from this point onwards are
         // significant.
         if( bLeadingZeroes )
         {
            RESET_ZERO_POS_DATA;
            bLeadingZeroes = FALSE;
         }
         else if( nZeroes > 0 )
         {
            // Make sure there's enough space
            if( ( nSigDigits + nZeroes ) >= nMaxSigDigits )
            {
               nZeroes = ( nMaxSigDigits - nSigDigits ) - 1;
            }

            // Just advance the write index since the buffer is zero initialized
            nSigDigits += nZeroes;
            nIntDigits += nZeroes;
            RESET_ZERO_POS_DATA;
         }
      }
      else if( '0' == ch )
      {
         // Keep track of the starting position of any consecutive zeroes.
         // We will ignore insignificant zeroes to save some unneccesary 
         // calculations. Otherwise, we will store them when we next encounter
         // a non-zero number, the decimal point character or reach the
         // end of the string.
         if( nZeroesStartPos < 0 )
         {
            nZeroesStartPos = nI;
         }
         nZeroes++;
      }
      else if( !IsAsciiHexDigit( ch ) )
      {
         // Encoutered an invalid character. Possibly the end of the string.
         break;
      }
      else
      {
         // Found a valid digit. Store it in the buffer
         if( bLeadingZeroes )
         {
            // This is the first significant non-zero digit of the integral
            // part of the number. Ignore all the zeroes seen till this point.
            RESET_ZERO_POS_DATA;
            bLeadingZeroes = FALSE;
            nFirstNZDigitPos = nI;
         }

         // Store all the zeroes seen so far.
         if( nZeroes > 0 )
         {
            // Make sure there's enough space
            if( ( nSigDigits + nZeroes ) >= nMaxSigDigits )
            {
               nZeroes = ( nMaxSigDigits - nSigDigits ) - 1;
            }

            // Just advance the write index since the buffer is zero initialized
            nSigDigits += nZeroes;
            ( nDPPos < 0 ) ? (nIntDigits+=nZeroes) : (nFracDigits+=nZeroes);
            RESET_ZERO_POS_DATA;
         }

         // Store the current digit
         if( nSigDigits < nMaxSigDigits )
         {
            if( IsAsciiDigit( ch ) )
            {
               // Decimal digit
               SigDigits[ nSigDigits++ ] = AsciiDecimalToInt( ch );
            }
            else if( IsAsciiAlpha( ch ) )
            {
               // Hex digit. Valid only if we are parsing a hex number.
               if( 16 == nRadix )
               {
                  SigDigits[ nSigDigits++ ] = AsciiHexToInt( ch );
               }
               else
               {
                  break;
               }
            } 

            // Record whether the digit belongs to the integral or the
            // fractional part
            ( nDPPos < 0 ) ? nIntDigits++ : nFracDigits++;
         }
      }
   }

   // Scanned all the digits. Advance the source pointer.
   (*ppch) += nI;

   // If there are any unconsumed trailing zeroes, they are significant only
   // if they belong to the integral part of the number. That can be one of
   // the two cases:
   //    Case 1: A number such as 111000
   //    Case 2: A number such as 111000. or 111000.0000
   // Compute the number of remaining significant trailing zeroes and store
   // them.
   if( nZeroes > 0 )
   {
      nZeroes = (nDPPos < 0) ? (nI - nZeroesStartPos) : (nDPPos - nZeroesStartPos);
      for( ; nZeroes > 0 && nSigDigits < nMaxSigDigits; nZeroes-- )
      {
         SigDigits[ nSigDigits++ ] = 0;
      }
   }

   // Check if there were any non-zero valid digits.
   if( nSigDigits <= 0 )
   {
      dRet = 0.0;
      goto Cleanup;
   }

   // Compute the number
   for( nI = 0; nI < nSigDigits; nI++ )
   {
      dRet = ( dRet * nRadix ) + SigDigits[ nI ];
   }

Cleanup:

   if( NOT_NULL( pnExp ) )
   {
      if( nDPPos >= 0 )
      {
         *pnExp = nDPPos - nFirstNZDigitPos; 
         if( nDPPos > nFirstNZDigitPos )
         {
            (*pnExp)--;
         }
      }
      else
      {
         *pnExp = nSigDigits - 1;
      }
   }
   if( NOT_NULL( pnExpMult ) )
   {
      *pnExpMult = -nFracDigits;
   }
   return dRet;
}

double std_scand( const char *pchBuf, const char **ppchEnd )
{
   double dRet = 0.0;
   const char *pch = (const char*)pchBuf;
   boolean bIsNegative = FALSE;
   uint64 ulInf = STD_DTOA_FP_POSITIVE_INF;
   uint64 ulNan = STD_DTOA_FP_QNAN;
   int nRadix = 10;
   int nFracDigits = 0;
   int nExpComputed = 0;
   int nExpScanned = 0;
   char ch = 0;

   // Skip whitespace
   while( ( *pch == ' ' ) || ( *pch == '\t' ) )
   {
      ++pch;
   }

   // Scan sign
   if( *pch == '-' )
   {
      bIsNegative = TRUE;
      ++pch;
   }
   else if( *pch == '+' )
   {
      ++pch;
   }

   // Check for valid words denoting infinity or nan.
   if( !IsAsciiHexDigit( *pch ) && ( *pch != '.' ) )
   {
      char* pcTemp = NULL;
      // Has to be one of the following words (case insensitive):
      //    INF, INFINITY, NAN
      if( NOT_NULL( pcTemp = std_stribegins( pch, INFINITY_STRING ) ) || 
          NOT_NULL( pcTemp = std_stribegins( pch, INF_STRING ) ) )
      {
         dRet = UINT64_TO_DOUBLE( ulInf );
         pch = pcTemp;
         goto Cleanup;
      }
      else if( NOT_NULL( pcTemp = std_stribegins( pch, NAN_STRING ) ) )
      {
         dRet = UINT64_TO_DOUBLE( ulNan );
         pch = pcTemp;
         goto Cleanup;
      }
      else
      {
         dRet = 0.0;
         goto Cleanup;
      }
   }

   // Check to see if we need to read a hex number
   if( ( '0' == pch[0] ) && ( 'x' == AsciiToLower( pch[1] ) ) && 
       IsAsciiHexDigit( pch[2] ) )
   {
      pch += 2;
      nRadix = 16;
   }

   ///////////////////////////////////
   // Parse the integer/fraction part
   ///////////////////////////////////
   dRet = ParseNumber( &pch, nRadix, &nExpComputed, &nFracDigits );

   ///////////////////////////
   // Parse the exponent part
   ///////////////////////////

   // Check for any exponent specifier.
   // Exponents are specified using the letter 'e' for decimal numbers
   // and the letter 'p' for hex numbers.
   if( ( ( ch = AsciiToLower( pch[0] ) ) == 'p' ) || ( ch == 'e' ) )
   {
      int nIndex = 1;
      int nExpSign = 1;
      int nExpDigits = 0;

      if( ch == 'p' )
      {
         // Make sure that we are decoding a hex number
         if( nRadix != 16 )
         {
            goto Cleanup;
         }
      }
      else
      {
         // Make sure that we are decoding a decimal number
         if( nRadix != 10 )
         {
            goto Cleanup;
         }
      }

      // Decode the exponent
      switch( pch[nIndex] )
      {
         case '-':
            nExpSign = -1;    // Follow through to the next case
         case '+':
            nIndex++;         // Follow through to the next case
         default:
            if( !IsAsciiDigit( pch[nIndex] ) )
            {
               // Not a valid exponent as exponents can only be specified
               // using decimal digitis (i.e., 0-9)
               goto Cleanup;
            }
            else
            {
               // The exponent sign is consumed only if it is followed by
               // a valid exponent value.
               pch += nIndex;
            }
      }

      // Parse the exponent
      nExpScanned = nExpSign * (int)ParseNumber( &pch, 10, &nExpDigits, 0 );
   }

Cleanup:

   // Add the parsed value of an explicitly specified exponent to the 
   // comptued value of the exponent so far.
   // i.e, if the string was 123.45e3, then currently:
   //    dRet = 12345
   //    nExpComputed = 2
   //    nExpScanned = 3
   // To check for overflows/underflows, we need to compute the
   // actual exponent of the input number, which is 1.2345e5
   // In other words, we need to add the parsed exponent value to the 
   // computed exponent so far.
   nExpComputed += nExpScanned;

   // Check for exponent overflow/underflow
   if( EXP_OVERFLOW( nRadix, nExpComputed ) )
   {
      // return HUGE_VAL
      // c99 does not specify what huge val is. So we just set it to infinity
      dRet = UINT64_TO_DOUBLE( ulInf );
   }
   else if( EXP_UNDERFLOW( nRadix, nExpComputed ) )
   {
      // return 0
      dRet = 0.0;
   }
   else
   {
      // Calculate the exponent multiplier
      // Going back to the above example. Currently,
      //    dRet = 12345
      // To get the final number, we need to multiply dRet by 
      // 10^(nExpScanned - nFracDigits), which is 10^1, results in:
      //    dRet = 123450 = 1234500 = 1.2345e5
      // 
      if( 16 == nRadix )
      {
         // Hex floating point number
         // If an explicit exponent was specified (nExpScanned) then it's to the base 2.
         // nFracDigits is to the base 16. Convert it to base 2 (ie, multiply by 4)
         dRet = dRet * FP_POW_2( nExpScanned + 4*nFracDigits );
      }
      else
      {
         // Decmial number
         // If an explicit exponent was specified (nExpScanned) then it's to the base 10.
         // nFracDigits is also to the base 10.
         int nExp = nExpScanned + nFracDigits;
         if( nExp < 0 )
         {
            dRet = dRet / FP_POW_10( -nExp );
         }
         else
         {
            dRet = dRet * FP_POW_10( nExp );
         }
      }
   }

   // Set the sign
   if( bIsNegative )
   {
      dRet = -dRet;
   }

   // Set the output pointer to the end of the last character scanned
   if( NOT_NULL( ppchEnd ) )
   {
      *ppchEnd = pch;
   }

   return dRet;
}
