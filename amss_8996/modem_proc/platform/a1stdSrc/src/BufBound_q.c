/*=====================================================================
        Copyright (c) 2005 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary and Confidential
=====================================================================*/

#include <stdio.h>
#include "AEEstd.h"
#include "AEEBufBound.h"

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

#define ASSERT(cond)  \
   if (! (cond)) { \
      printf(__FILENAME__ ":%d: ASSERT failed\n", __LINE__); \
      return 1; \
   } // else {printf ("OK %s\n", #cond); }


static int MemIsSet(const char* cpc, char c, int nLen)
{
   for (;nLen > 0; --nLen, ++cpc) {
      if (*cpc != c) {
         return 0;
      }
   }
   return 1;
}

#define MAXRESULT   100
#define GUARDBYTES  8
#define CHGUARD     '.'


static int CheckResult(BufBound *me, char *pcExpected, int nLenExpected, int nCountExpected)
{
   char *pc = me->pcBuf;
   int nWrote = BufBound_ReallyWrote(me);

   if (BufBound_Wrote(me) != nCountExpected) {
      printf("Count %d != %d\n", BufBound_Wrote(me), nCountExpected);
      return 0;
   }
   if (nWrote != nLenExpected) {
      printf("Length %d != %d\n", nWrote, nLenExpected);
      return 0;
   }
   if (! MemIsSet(pc-GUARDBYTES, CHGUARD, GUARDBYTES)) {
      printf("Guard below corrupted!\n");
      return 0;
   }
   if (!MemIsSet(pc+nLenExpected, CHGUARD, GUARDBYTES)) {
      printf("Guard above corrupted!\n");
      return 0;
   }
   if (std_memcmp(pc, pcExpected, nLenExpected)) {
      pc[nLenExpected] = '\0';
      printf("Result not expected: \"%s\" != \"%s\"\n", pc, pcExpected);
      return 0;
   }
   return 1;
}

int main(int argc, char **argv)
{
   BufBound b;
   char ac[GUARDBYTES * 2 + MAXRESULT];
   char *pc = ac + GUARDBYTES;
   unsigned long ul;
   int n;

   #define INIT(size) \
      { std_memset(ac, CHGUARD, sizeof(ac)); \
        BufBound_Init(&b, pc, size); }
   
   #define HOLDS(str, cnt) \
      ASSERT( CheckResult(&b, (str), sizeof(str)-1, cnt) )
   
   #define COUNT(nwrote) \
      ASSERT( (nwrote) == BufBound_Wrote(&b) )

   
   // Simple copying
   
   INIT(MAXRESULT);
   BufBound_Putc(&b, 'a');
   BufBound_Write(&b, "bc", 2);
   BufBound_Putnc(&b, 'd', 2);
   BufBound_ForceNullTerm(&b);
   HOLDS( "abcdd\0", 6 );

   // Negative sizes (treated as zero)

   BufBound_Write(&b, "abc", -3);
   HOLDS( "abcdd\0", 6);

   BufBound_Putnc(&b, 'a', -1);
   HOLDS( "abcdd\0", 6 );
   
   BufBound_Write(&b, "abc", -9);
   HOLDS( "abcdd\0", 6 );

   BufBound_Putnc(&b, 'a', -9);
   HOLDS( "abcdd\0", 6 );

   BufBound_Write(&b, "abc", MIN_INT32);
   HOLDS( "abcdd\0", 6 );

   BufBound_Putnc(&b, 'a', MIN_INT32);
   HOLDS( "abcdd\0", 6 );

   INIT(10);
   BufBound_Write(&b, "", -1);
   COUNT( 0 );

   BufBound_Putnc(&b, 'a', -1);
   COUNT( 0 );
   
   BufBound_Write(&b, "", MIN_INT32);
   COUNT( 0 );

   BufBound_Putnc(&b, 'a', MIN_INT32);
   COUNT( 0 );
   
   // Advance

   INIT(MAXRESULT);
   BufBound_Advance(&b, 6);
   COUNT( 6 );
   ASSERT( 6 == BufBound_ReallyWrote(&b) );
   
   BufBound_Advance(&b, -3);
   COUNT( 3 );
   ASSERT( 3 == BufBound_ReallyWrote(&b) );
   
   BufBound_Advance(&b, -9);
   COUNT( 0 );
   ASSERT( 0 == BufBound_ReallyWrote(&b) );

   BufBound_Advance(&b, -1);
   COUNT( 0 );
   ASSERT( 0 == BufBound_ReallyWrote(&b) );
   
   BufBound_Advance(&b, MIN_INT32);
   COUNT( 0 );
   ASSERT( 0 == BufBound_ReallyWrote(&b) );
   
   BufBound_Advance(&b, MAXRESULT*2);
   COUNT( MAXRESULT*2 );
   ASSERT( MAXRESULT == BufBound_ReallyWrote(&b) );
   
   BufBound_Advance(&b, MAX_INT32);
   COUNT( MAX_INT32 );
   ASSERT( MAXRESULT == BufBound_ReallyWrote(&b) );

   // ForceNullTerm
   
   INIT(3);
   BufBound_Write(&b, "abc", 3);
   BufBound_ForceNullTerm(&b);
   HOLDS( "ab\0", 4 );

   INIT(3);
   BufBound_Write(&b, "abcd", 4);
   BufBound_ForceNullTerm(&b);
   HOLDS( "ab\0", 5);

   INIT(3);
   BufBound_Write(&b, "abc", 3);
   BufBound_Advance(&b, MAX_INT32);
   BufBound_ForceNullTerm(&b);
   HOLDS( "ab\0", MAX_INT32 );

   INIT(4);
   BufBound_Write(&b, "ab", 2);
   BufBound_ForceNullTerm(&b);
   HOLDS( "ab\0", 3 );
   
   BufBound_ForceNullTerm(&b);
   HOLDS( "ab\0\0", 4 );
   
   // Overflow

   INIT(2);
   BufBound_Putc(&b, 'a');
   BufBound_Putc(&b, 'b');
   BufBound_Putc(&b, 'c');  // overflow
   HOLDS( "ab", 3 );

   INIT(0);
   BufBound_Putc(&b, 'a');         // overflow
   COUNT( 1 );
   
   BufBound_Write(&b, "abcd", 4);  // overflow
   COUNT( 5 );

   BufBound_Putnc(&b, 'b', 100);  // overflow
   HOLDS( "", 105 );
   
   INIT(4);
   BufBound_Putnc(&b, 'a', 5);
   COUNT( 5 );
   ASSERT( 4 == BufBound_ReallyWrote(&b) );
   ASSERT( CHGUARD == pc[4] );

   BufBound_ForceNullTerm(&b);
   ASSERT( CHGUARD == pc[4] );
   ASSERT( '\0'   == pc[3] );

   INIT(4);
   BufBound_Write(&b, "abcde", 5);
   HOLDS( "abcd", 5 );

   // Saturation

   BufBound_Advance(&b, MAX_INT32);
   COUNT( MAX_INT32 );
   BufBound_Putc(&b, 'a');
   COUNT( MAX_INT32 );
   BufBound_Write(&b, "abc", 3);
   COUNT( MAX_INT32 );
   BufBound_Write(&b, "abc", -3);
   COUNT( MAX_INT32 );

   // Pointer Wraparound

   BufBound_Init(&b, (void*)0xFFFFfffe, 0);
   BufBound_Putc(&b, 'A');

   ASSERT( 0 == BufBound_ReallyWrote(&b) );
   COUNT( 1 );
   
   BufBound_Advance(&b, 99);
   
   ASSERT( 0 == BufBound_ReallyWrote(&b) );
   COUNT( 100 );
   
   BufBound_Putnc(&b, 'a', 100200200);
   COUNT( 100200300 );

   // WriteLE, WriteBE

   ul = 0x41424344;

   INIT(4);
   BufBound_WriteLE(&b, &ul, sizeof(ul), "L");
   HOLDS( "DCBA", 4 );
   
   INIT(3);
   BufBound_WriteLE(&b, &ul, sizeof(ul), "L");
   HOLDS( "DCB", 4 );
   
   INIT(9);
   BufBound_WriteLE(&b, &ul, MIN_INT32, "L");
   HOLDS( "", 0 );
   
   INIT(4);
   BufBound_WriteBE(&b, &ul, sizeof(ul), "L");
   HOLDS( "ABCD", 4 );
   
   INIT(1);
   BufBound_WriteBE(&b, &ul, sizeof(ul), "L");
   HOLDS( "A", 4 );
   
   INIT(3);
   BufBound_WriteBE(&b, &ul, sizeof(ul), "L");
   HOLDS( "ABC", 4 );
   
   INIT(9);
   BufBound_WriteBE(&b, &ul, MIN_INT32, "L");
   HOLDS( "", 0 );


   // Puts

   INIT(3);
   BufBound_Puts(&b, "abcd");
   HOLDS( "abc", 4 );

   INIT(3);
   BufBound_Puts(&b, "abcd");
   HOLDS( "abc", 4 );

   // Init, BufSize, Left

   INIT(3);
   BufBound_Putc(&b, 'x');
   ASSERT( 2 == BufBound_Left(&b) );
   ASSERT( 3 == BufBound_BufSize(&b) );
   
   for (n=0; n<3; ++n) {
      // We won't actually be writing to these pointers...
      static void * const aPtr[] = {
         (void*)MAX_INT32,
         (void*)MIN_INT32,
         (void*) 0
      };

      BufBound_Init(&b, aPtr[n], MIN_INT32);        // MIN_INT32 size
      ASSERT( 0 == BufBound_BufSize(&b) );
      
      BufBound_Init(&b, aPtr[n], -1);               // -1 size
      ASSERT( 0 == BufBound_BufSize(&b) );
      
      BufBound_Putnc(&b, 'x', MAX_INT32);
      ASSERT( -MAX_INT32 == BufBound_Left(&b) );    // min Left() result
      
      BufBound_Init(&b, aPtr[n], MAX_INT32);
      ASSERT( MAX_INT32 == BufBound_BufSize(&b) );  // max BufSize()
      ASSERT( MAX_INT32 == BufBound_Left(&b) );     // max Left() result
   }
   
   return 0;
}
