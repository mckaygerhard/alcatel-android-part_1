/*==============================================================================

Copyright (c) 2012 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==============================================================================*/

#include "Elite_APPI.h"

#include "qurt_atomic_ops.h"

#ifdef __cplusplus
extern "C"
{
#endif //__cplusplus
#include "qurt_mutex.h"
#ifdef __cplusplus
}
#endif //__cplusplus

#include "platform_libs.h"
#include "mod_table.h"

#include "adsp_amdb_static.h"

#include <string.h>
#include <stdlib.h>

#include "dlw.h"
#include "hashtable.h"

#ifndef FARF_ERROR
#define FARF_ERROR 1
#endif
#include "HAP_farf.h"

#define STD_OFFSETOF(type,member)     (((char*)(&((type*)1)->member))-((char*)1))

#define STD_RECOVER_REC(type,member,p) ((void)((p)-&(((type*)1)->member)),\
                                        (type*)(void*)(((char*)(void*)(p))-STD_OFFSETOF(type,member)))

#define TRY(exception, func) \
   if (ADSP_EOK != (exception = func)) {\
      goto exception##bail; \
   }

#define THROW(exception, errno) \
   exception = errno; \
   goto exception##bail;

#define CATCH(exception) exception##bail: if (exception != ADSP_EOK)

// TODO global instances, who should own this and how should consumers gain access
// TODO these should be more formal objects
// TODO add support for delayed load (!preload)
// TODO Could consolidate static modules into one alloc
// TODO one way around mutex use would be to have all reg/deregistration requests
// posted to the thread that also does the consuming of the list, aka, force single threaded access
// TODO 40 bytes tacked on for wrapper?

/******************************************************************************/

typedef void (*capi_wrapper_orig_dtor_f)(ICAPIVtbl*);

// capi_wrapper hijacks the objects vtbl adds a wrapper to the end function
// this method of tracking module lifetime is not the cleaneset due to the
// assumption that we can point the object to the wrappers vtbl without any
// ill side effect.  But, it does provide for the least overhead since the rest
// of the capi functions are direct calls
typedef struct capi_wrapper_t
{
  ICAPIVtbl vtbl;
  capi_wrapper_orig_dtor_f pfnDtor;
   adsp_amdb_capi_t* capi_ptr;
  ICAPI* icapi;
} capi_wrapper_t;

static void capi_wrapper_dtor(ICAPIVtbl* icapi)
{
  capi_wrapper_t* me = (capi_wrapper_t*)((char*)icapi -
                                         align_to_8_byte(sizeof(capi_wrapper_t)));

  me->pfnDtor(icapi);
  adsp_amdb_capi_release(me->capi_ptr);
  free(me);
}

ADSPResult capi_wrapper_init(capi_wrapper_t* me, ICAPI* icapi,
                             adsp_amdb_capi_t* capi_ptr)
{
  ICAPIVtbl** ppvtbl = (ICAPIVtbl**)(void**)icapi;

  memmove(&me->vtbl, (ICAPIVtbl*)*(void**)icapi, sizeof(ICAPIVtbl));
  // dtor sits at first entry in vtbl, overwrite with wrapper
  me->pfnDtor = *(capi_wrapper_orig_dtor_f*)&(me->vtbl);
  *(capi_wrapper_orig_dtor_f*)&(me->vtbl) = capi_wrapper_dtor;
  *ppvtbl = &me->vtbl;

  me->icapi = icapi;
  me->capi_ptr = capi_ptr;
  (void)adsp_amdb_capi_addref(capi_ptr);

  return ADSP_EOK;
}

/******************************************************************************/

struct adsp_amdb_capi_t {
  hash_node hn;
  unsigned int refs;

  int id;
  char filename_str[256];
  bool_t is_static;

  void* h;
  capi_getsize_f getsize_f;
  capi_ctor_f ctor_f;
};

uint32_t adsp_amdb_capi_addref(adsp_amdb_capi_t* me)
{
  int refs;
  refs = qurt_atomic_inc_return(&me->refs);
  return refs;
}

uint32_t adsp_amdb_capi_release(adsp_amdb_capi_t* me)
{
  int refs;

  refs = qurt_atomic_dec_return(&me->refs);
  if (0 == refs) {
    if (0 != me->h) {
      dlw_Close(me->h);
    }
    free(me);
  }
  return refs;
}

ADSPResult adsp_amdb_capi_new_f(adsp_amdb_capi_t* me, uint32_t format,
                                uint32_t bps, ICAPI** capi_ptr_ptr)
{
  ICAPI* icapi = 0;
  uint32_t size;
  capi_wrapper_t* wrapper_ptr;
  ADSPResult err;

  TRY(err, me->getsize_f(format, bps, &size));

  // save size of module, wrapper will be placed before it
  size = align_to_8_byte(size);
  size += align_to_8_byte(sizeof(capi_wrapper_t));

  wrapper_ptr = (capi_wrapper_t*)malloc(size);
  if (0 == wrapper_ptr) {
    FARF(ERROR, "capi: failed to allocate wrapper and capi object");
    THROW(err, ADSP_ENOMEMORY);
  }

  icapi = (ICAPI*)((char*)wrapper_ptr +
                   align_to_8_byte(sizeof(capi_wrapper_t)));

  TRY(err, me->ctor_f(icapi, format, bps));
  TRY(err, capi_wrapper_init(wrapper_ptr, icapi, me));
  *capi_ptr_ptr = icapi;

  CATCH(err);
  return err;
}

static ADSPResult capi_init(adsp_amdb_capi_t* me, int id,
                            capi_getsize_f getsize_f,
                            capi_ctor_f ctor_f, const char* filename_str,
                            const char* getsize_str, const char* ctor_str)
{
  uint32_t err;

  memset(me, 0, sizeof(*me));

  qurt_atomic_set(&me->refs, 1);

  me->id = id;

  me->hn.key_size = sizeof(me->id);
  me->hn.key_ptr = &me->id;

  if (0 != getsize_f) {
    me->is_static = TRUE;
    me->getsize_f = getsize_f;
    me->ctor_f = ctor_f;
  } else {
    me->h = dlw_Open(filename_str, DLW_RTLD_NOW);
    if (0 == me->h) {
      FARF(ERROR, "capi: failed to open %s, %s",
           filename_str, dlw_Error());
      THROW(err, ADSP_EFAILED);
    }
    strlcpy(me->filename_str, filename_str, sizeof(me->filename_str));
    me->getsize_f = (capi_getsize_f)dlw_Sym(me->h, getsize_str);
    if (!me->getsize_f) {
      FARF(ERROR, "adsp_amdb_capi_t: dlsym failed %s, %s",
           getsize_str, dlw_Error());
      THROW(err, ADSP_EFAILED);
    }
    me->ctor_f = (capi_ctor_f)dlw_Sym(me->h, ctor_str);
    if (!me->ctor_f) {
      FARF(ERROR, "capi: dlsym failed %s , %s",
           ctor_str, dlw_Error());
      THROW(err, ADSP_EFAILED);
    }
  }
  THROW(err, ADSP_EOK);

  CATCH(err);
  return err;
}

/******************************************************************************/

typedef ADSPResult(* appi_wrapper_orig_end_f)(appi_t* appi_ptr);

// appi_wrapper hijacks the objects vtbl adds a wrapper to the end function
// this method of tracking module lifetime is not the cleaneset due to the
// assumption that we can point the object to the wrappers vtbl without any
// ill side effect.  But, it does provide for the least overhead since the rest
// of the appi functions are direct calls
typedef struct appi_wrapper_t
{
  const appi_vtbl_t* pvtbl;
  appi_t* iappi;
  adsp_amdb_appi_t* appi_ptr;
} appi_wrapper_t;

static ADSPResult appi_wrapper_reinit(appi_t* iappi,
                                      const appi_format_t* in_format_ptr,
                                      appi_format_t* out_format_ptr,
                                      appi_buf_t* info_ptr)
{
  appi_wrapper_t* me = (appi_wrapper_t*)iappi;
  return me->iappi->vtbl_ptr->reinit(me->iappi, in_format_ptr, out_format_ptr,
                                     info_ptr);
}

static ADSPResult appi_wrapper_process(appi_t* iappi,
                                       const appi_buflist_t* input,
                                       appi_buflist_t* output,
                                       appi_buf_t* info_ptr)
{
  appi_wrapper_t* me = (appi_wrapper_t*)iappi;
  return me->iappi->vtbl_ptr->process(me->iappi, input, output, info_ptr);
}

static ADSPResult appi_wrapper_end(appi_t* iappi)
{
  appi_wrapper_t* me = (appi_wrapper_t*)iappi;
  ADSPResult err;

  err = me->iappi->vtbl_ptr->end(me->iappi);
  adsp_amdb_appi_release(me->appi_ptr);
  return err;
}

static ADSPResult appi_wrapper_set_param(appi_t* iappi,
                                         uint32_t param_id,
                                         const appi_buf_t* params_ptr)
{
  appi_wrapper_t* me = (appi_wrapper_t*)iappi;
  return me->iappi->vtbl_ptr->set_param(me->iappi, param_id, params_ptr);
}

static ADSPResult appi_wrapper_get_param(appi_t* iappi,
                                         uint32_t param_id,
                                         appi_buf_t* params_ptr)
{
  appi_wrapper_t* me = (appi_wrapper_t*)iappi;
  return me->iappi->vtbl_ptr->get_param(me->iappi, param_id, params_ptr);
}

static ADSPResult appi_wrapper_get_input_req(appi_t* iappi,
                                             const uint32_t output_size,
                                             uint32_t* input_size_ptr)
{
  appi_wrapper_t* me = (appi_wrapper_t*)iappi;
  return me->iappi->vtbl_ptr->get_input_req(me->iappi, output_size,
                                            input_size_ptr);
}

static const appi_vtbl_t vtbl_wrapper =
{
  appi_wrapper_reinit,
  appi_wrapper_process,
  appi_wrapper_end,
  appi_wrapper_set_param,
  appi_wrapper_get_param,
  appi_wrapper_get_input_req
};

ADSPResult appi_wrapper_init(appi_wrapper_t* me, appi_t* iappi,
                             adsp_amdb_appi_t* appi_ptr)
{
  me->pvtbl = &vtbl_wrapper;
  me->iappi = iappi;
  me->appi_ptr = appi_ptr;

  (void)adsp_amdb_appi_addref(appi_ptr);
  return ADSP_EOK;
}

/******************************************************************************/

struct adsp_amdb_appi_t {
  hash_node hn;
  unsigned int refs;

  int id;
  char filename_str[256];
  bool_t is_static;
  uint32_t num_skip;

  void* h;
  appi_getsize_f getsize_f;
  appi_init_f init_f;
};

uint32_t adsp_amdb_appi_addref(adsp_amdb_appi_t* me)
{
  return qurt_atomic_inc_return(&me->refs);
}

uint32_t adsp_amdb_appi_release(adsp_amdb_appi_t* me)
{
  int refs;

  refs = qurt_atomic_dec_return(&me->refs);
  if (0 == refs) {
    if (0 != me->h) {
      dlw_Close(me->h);
    }
    free(me);
  }
  return refs;
}

ADSPResult adsp_amdb_appi_getsize_f(adsp_amdb_appi_t* me, const appi_buf_t* params_ptr,
                                    uint32_t* size_ptr)
{
  ADSPResult err;

  TRY(err, me->getsize_f(params_ptr, size_ptr));

  // save size of module, wrapper will be placed before it
  *size_ptr = align_to_8_byte(*size_ptr);

  // make room for appi wrapper
  *size_ptr += align_to_8_byte(sizeof(appi_wrapper_t));

  CATCH(err);
  return err;
}

ADSPResult adsp_amdb_appi_init_f(adsp_amdb_appi_t* me, appi_t* iappi,
                                 bool_t* is_inplace_ptr,
                                 const appi_format_t* in_format_ptr,
                                 appi_format_t* out_format_ptr,
                                 appi_buf_t* info_ptr)
{
  appi_wrapper_t* wrapper_ptr = (appi_wrapper_t*)iappi;
  ADSPResult err;

  iappi = (appi_t*)((char*)wrapper_ptr +
                    align_to_8_byte(sizeof(appi_wrapper_t)));

  TRY(err, me->init_f(iappi, is_inplace_ptr, in_format_ptr,
                      out_format_ptr, info_ptr));
  TRY(err, appi_wrapper_init(wrapper_ptr, iappi, me));

  CATCH(err);
  return err;
}

static ADSPResult appi_init(adsp_amdb_appi_t* me, int id,
                            appi_getsize_f getsize_f,
                            appi_init_f init_f, const char* filename_str,
                            const char* getsize_str, const char* init_str)
{
  uint32_t err;

  memset(me, 0, sizeof(*me));

  qurt_atomic_set(&me->refs, 1);

  me->id = id;

  me->hn.key_size = sizeof(me->id);
  me->hn.key_ptr = &me->id;

  if (0 != getsize_f) {
    me->is_static = TRUE;
    me->getsize_f = getsize_f;
    me->init_f = init_f;
  } else {
    me->h = dlw_Open(filename_str, DLW_RTLD_NOW);
    if (0 == me->h) {
      FARF(ERROR, "appi: failed to open %s, %s",
           filename_str, dlw_Error());
      THROW(err, ADSP_EFAILED);
    }
    strlcpy(me->filename_str, filename_str, sizeof(me->filename_str));
    me->getsize_f = (appi_getsize_f)dlw_Sym(me->h, getsize_str);
    if (!me->getsize_f) {
      FARF(ERROR, "adsp_amdb_appi_t: dlsym failed %s, %s",
           getsize_str, dlw_Error());
      THROW(err, ADSP_EFAILED);
    }
    me->init_f = (appi_init_f)dlw_Sym(me->h, init_str);
    if (!me->init_f) {
      FARF(ERROR, "appi: dlsym failed %s , %s",
           init_str, dlw_Error());
      THROW(err, ADSP_EFAILED);
    }
  }
  THROW(err, ADSP_EOK);

  CATCH(err);
  return err;
}

/******************************************************************************/

struct adsp_amdb_t {
  hashtable ht_capi;
  qurt_mutex_t mutex_capi;

  hashtable ht_appi;
  qurt_mutex_t mutex_appi;
};

static adsp_amdb_t adsp_amdb_;
static adsp_amdb_t* adsp_amdb_ptr_ = &adsp_amdb_;

ADSPResult adsp_amdb_get_appi(int id, adsp_amdb_appi_t** appi_ptrptr)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;
  hash_node* phn = 0;
  ADSPResult err;

  qurt_mutex_lock(&me->mutex_appi);
  phn = hashtable_find(&me->ht_appi, &id, sizeof(id));
  if (phn) {
    adsp_amdb_appi_t* appi_ptr = STD_RECOVER_REC(adsp_amdb_appi_t, hn, phn);
    adsp_amdb_appi_addref(appi_ptr);
    *appi_ptrptr = appi_ptr;
    qurt_mutex_unlock(&me->mutex_appi);
    THROW(err, ADSP_EOK);
  }
  qurt_mutex_unlock(&me->mutex_appi);
  THROW(err, ADSP_EFAILED);

  CATCH(err) {
    FARF(ERROR, "appi: failed to get id 0x%08lX", id);
  }
  return err;
}

ADSPResult adsp_amdb_remove_appi(int id, const char* filename_str)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;
  adsp_amdb_appi_t* appi_ptr;
  hash_node* phn = 0;
  ADSPResult err;

  if (0 != filename_str[0]) {
    // remove all contained in this file
    uint32_t n = 0;

    qurt_mutex_lock(&me->mutex_appi);
    while (0 != (phn = hashtable_getnext(&me->ht_appi, &n))) {
      appi_ptr = STD_RECOVER_REC(adsp_amdb_appi_t, hn, phn);
      if (!appi_ptr->is_static && !strncmp(appi_ptr->filename_str,
                                           filename_str,
                                           sizeof(appi_ptr->filename_str))) {
        if (ADSP_EOK == hashtable_remove(&me->ht_appi, &appi_ptr->id,
                                         sizeof(appi_ptr->id), phn)) {
          n--;
        }
      }
    }
    qurt_mutex_unlock(&me->mutex_appi);
    THROW(err, ADSP_EOK);
  } else {
    // remove a specific id
    qurt_mutex_lock(&me->mutex_appi);
    phn = hashtable_find(&me->ht_appi, &id, sizeof(id));
    appi_ptr = STD_RECOVER_REC(adsp_amdb_appi_t, hn, phn);
    if (phn && !appi_ptr->is_static) {
      err = hashtable_remove(&me->ht_appi, &appi_ptr->id, sizeof(appi_ptr->id),
                             &appi_ptr->hn);
      if (ADSP_EOK == err) {
        qurt_mutex_unlock(&me->mutex_appi);
        THROW(err, ADSP_EOK);
      }
    }
    qurt_mutex_unlock(&me->mutex_appi);
    THROW(err, ADSP_EFAILED);
  }
  CATCH(err);

  return err;
}

ADSPResult adsp_amdb_remove_all_appi(void)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;
  adsp_amdb_appi_t* appi_ptr;
  uint32_t n = 0;
  hash_node* phn = 0;

  qurt_mutex_lock(&me->mutex_appi);
  while (0 != (phn = hashtable_getnext(&me->ht_appi, &n))) {
    appi_ptr = STD_RECOVER_REC(adsp_amdb_appi_t, hn, phn);
    if (!appi_ptr->is_static) {
      if (ADSP_EOK == hashtable_remove(&me->ht_appi, &appi_ptr->id,
                                       sizeof(appi_ptr->id), phn)) {
        adsp_amdb_appi_release(appi_ptr);
        n--;
      }
    }
  }
  qurt_mutex_unlock(&me->mutex_appi);

  return ADSP_EOK;
}

ADSPResult adsp_amdb_print_all_appi(void)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;
  adsp_amdb_appi_t* appi_ptr;
  uint32_t n = 0;
  hash_node* phn = 0;

  FARF(ALWAYS, "appi: print db");
  qurt_mutex_lock(&me->mutex_appi);
  while (0 != (phn = hashtable_getnext(&me->ht_appi, &n))) {
    appi_ptr = STD_RECOVER_REC(adsp_amdb_appi_t, hn, phn);
    FARF(ALWAYS, "appi: module id 0x%08lX %s %s",
         appi_ptr->id, appi_ptr->is_static == TRUE ? "Static" : "Dynamic",
         appi_ptr->filename_str);
  }
  qurt_mutex_unlock(&me->mutex_appi);

  return ADSP_EOK;
}

static ADSPResult adsp_amdb_addInternal_appi(int id,
                                             appi_getsize_f getsize_f,
                                             appi_init_f init_f,
                                             const char* filename_str,
                                             const char* getsize_str,
                                             const char* init_str)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;
  adsp_amdb_appi_t* appi_ptr;
  ADSPResult err;

  appi_ptr = (adsp_amdb_appi_t*)malloc(sizeof(adsp_amdb_appi_t));
  if (0 == appi_ptr) {
    FARF(ERROR, "appi: failed to allocate list entry");
    THROW(err, ADSP_ENOMEMORY);
  }

  err = appi_init(appi_ptr, id, getsize_f, init_f,
                  filename_str, getsize_str, init_str);
  if (ADSP_EOK != err) {
    FARF(ERROR, "appi: adsp_amdb_appi_t Init() failed %d", err);
    THROW(err, err);
  }

  qurt_mutex_lock(&me->mutex_appi);
  err = hashtable_insert(&me->ht_appi, &appi_ptr->hn);
  qurt_mutex_unlock(&me->mutex_appi);
  if (ADSP_EOK != err) {
    FARF(ERROR, "appi: hashtable_insert() failed %d", err);
    THROW(err, err);
  }

  CATCH(err) {
    if (appi_ptr) {
      free(appi_ptr);
    }
  }
  return err;
}

ADSPResult adsp_amdb_add_static_appi(int id,
                                     appi_getsize_f getsize_f,
                                     appi_init_f init_f)
{
  return adsp_amdb_addInternal_appi(id, getsize_f, init_f, 0, 0, 0);
}

ADSPResult adsp_amdb_add_appi(int id, bool_t preload,
                              const char* filename_str,
                              const char* getsize_str,
                              const char* init_str)
{
  (void)preload;

  return adsp_amdb_addInternal_appi(id, 0, 0, filename_str, getsize_str, init_str);
}

static void adsp_amdb_appi_free(void* void_ptr, hash_node* node)
{
  adsp_amdb_appi_release(STD_RECOVER_REC(adsp_amdb_appi_t, hn, node));
}

/*************************************************************************************************/

ADSPResult adsp_amdb_get_capi(int id, adsp_amdb_capi_t** capi_ptrptr)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;
  adsp_amdb_capi_t* capi_ptr;
  hash_node* phn = 0;
  ADSPResult err;

  qurt_mutex_lock(&me->mutex_capi);
  phn = hashtable_find(&me->ht_capi, &id, sizeof(id));
  if (phn) {
    capi_ptr = STD_RECOVER_REC(adsp_amdb_capi_t, hn, phn);
    adsp_amdb_capi_addref(capi_ptr);
    *capi_ptrptr = capi_ptr;
    qurt_mutex_unlock(&me->mutex_capi);
    THROW(err, ADSP_EOK);
  }
  qurt_mutex_unlock(&me->mutex_capi);
  THROW(err, ADSP_EFAILED);

  CATCH(err) {
    FARF(ERROR, "capi: failed to get id 0x%08lX", id);
  }
  return err;
}

ADSPResult adsp_amdb_remove_capi(int id, const char* filename_str)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;
  adsp_amdb_capi_t* capi_ptr;
  hash_node* phn = 0;
  ADSPResult err;

  if (0 != filename_str[0]) {
    // remove all contained in this file
    uint32_t n = 0;

    qurt_mutex_lock(&me->mutex_capi);
    while (0 != (phn = hashtable_getnext(&me->ht_capi, &n))) {
      capi_ptr = STD_RECOVER_REC(adsp_amdb_capi_t, hn, phn);
      if (!capi_ptr->is_static && !strncmp(capi_ptr->filename_str,
                                           filename_str,
                                           sizeof(capi_ptr->filename_str))) {
        if (ADSP_EOK == hashtable_remove(&me->ht_capi, &capi_ptr->id,
                                         sizeof(capi_ptr->id), phn)) {
          n--;
        }
      }
    }
    qurt_mutex_unlock(&me->mutex_capi);
    THROW(err, ADSP_EOK);
  } else {
    // remove a specific id
    qurt_mutex_lock(&me->mutex_capi);
    phn = hashtable_find(&me->ht_capi, &id, sizeof(id));
    capi_ptr = STD_RECOVER_REC(adsp_amdb_capi_t, hn, phn);
    if (phn) {
      if (!capi_ptr->is_static) {

        err = hashtable_remove(&me->ht_capi, &capi_ptr->id, sizeof(capi_ptr->id),
                               &capi_ptr->hn);
        if (ADSP_EOK == err) {
          qurt_mutex_unlock(&me->mutex_capi);
          THROW(err, ADSP_EOK);
        }
      }
    }
    qurt_mutex_unlock(&me->mutex_capi);
    THROW(err, ADSP_EFAILED);
  }

  CATCH(err);
  return err;
}

ADSPResult adsp_amdb_remove_all_capi(void)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;
  adsp_amdb_capi_t* capi_ptr;
  uint32_t n = 0;
  hash_node* phn = 0;

  qurt_mutex_lock(&me->mutex_capi);
  while (0 != (phn = hashtable_getnext(&me->ht_capi, &n))) {
    capi_ptr = STD_RECOVER_REC(adsp_amdb_capi_t, hn, phn);
    if (!capi_ptr->is_static) {
      if (ADSP_EOK == hashtable_remove(&me->ht_capi, &capi_ptr->id,
                                       sizeof(capi_ptr->id), phn)) {
        adsp_amdb_capi_release(capi_ptr);
        n--;
      }
    }
  }
  qurt_mutex_unlock(&me->mutex_capi);

  return ADSP_EOK;
}

ADSPResult adsp_amdb_print_all_capi(void)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;
  adsp_amdb_capi_t* capi_ptr;
  uint32_t n = 0;
  hash_node* phn = 0;

  FARF(ALWAYS, "capi: print db");
  qurt_mutex_lock(&me->mutex_capi);
  while (0 != (phn = hashtable_getnext(&me->ht_capi, &n))) {
    capi_ptr = STD_RECOVER_REC(adsp_amdb_capi_t, hn, phn);
    FARF(ALWAYS, "capi: module id 0x%08lX %s %s",
         capi_ptr->id, capi_ptr->is_static == TRUE ? "Static" : "Dynamic",
         capi_ptr->filename_str);
  }
  qurt_mutex_unlock(&me->mutex_capi);

  return ADSP_EOK;
}

static ADSPResult adsp_amdb_addInternal_capi(int id,
                                             capi_getsize_f getsize_f,
                                             capi_ctor_f ctor_f,
                                             const char* filename_str,
                                             const char* getsize_str,
                                             const char* ctor_str)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;
  adsp_amdb_capi_t* capi_ptr;
  ADSPResult err;

  capi_ptr = (adsp_amdb_capi_t*)malloc(sizeof(adsp_amdb_capi_t));
  if (0 == capi_ptr) {
    FARF(ERROR, "capi: failed to allocate list entry");
    THROW(err, ADSP_ENOMEMORY);
  }

  err = capi_init(capi_ptr, id, getsize_f, ctor_f,
                  filename_str, getsize_str, ctor_str);
  if (ADSP_EOK != err) {
    FARF(ERROR, "capi: adsp_amdb_capi_t Ctor() failed %d", err);
    THROW(err, err);
  }

  qurt_mutex_lock(&me->mutex_capi);
  err = hashtable_insert(&me->ht_capi, &capi_ptr->hn);
  qurt_mutex_unlock(&me->mutex_capi);
  if (ADSP_EOK != err) {
    FARF(ERROR, "capi: hashtable_insert() failed %d", err);
    THROW(err, err);
  }

  CATCH(err) {
    if (capi_ptr) {
      free(capi_ptr);
    }
  }
  return err;
}

ADSPResult adsp_amdb_add_static_capi(int id,
                                     capi_getsize_f getsize_f,
                                     capi_ctor_f ctor_f)
{
  return adsp_amdb_addInternal_capi(id, getsize_f, ctor_f, 0, 0, 0);
}

ADSPResult adsp_amdb_add_capi(int id, bool_t preload,
                              const char* filename_str,
                              const char* getsize_str,
                              const char* ctor_str)
{
  (void)preload;

  return adsp_amdb_addInternal_capi(id, 0, 0, filename_str, getsize_str, ctor_str);
}

static void adsp_amdb_capi_free(void* void_ptr, hash_node* node)
{
  adsp_amdb_capi_release(STD_RECOVER_REC(adsp_amdb_capi_t, hn, node));
}

/*************************************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif //__cplusplus

ADSPResult adsp_amdb_init(void);
void adsp_amdb_deinit(void);
int adsp_amdb_skel_invoke(uint32 _sc, remote_arg* _pra);

#ifdef __cplusplus
}
#endif //__cplusplus

PL_DEP(mod_table)
PL_DEP(dlw)

ADSPResult adsp_amdb_init(void)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;
  ADSPResult err;

  err = hashtable_init(&me->ht_appi, 16, 2, adsp_amdb_appi_free, me);
  if (ADSP_EOK != err) {
    FARF(ERROR, "amdb: appi hashtable init failed %d", err);
    THROW(err, err);
  }
  qurt_mutex_init(&me->mutex_appi);

  err = hashtable_init(&me->ht_capi, 16, 2, adsp_amdb_capi_free, me);
  if (ADSP_EOK != err) {
    FARF(ERROR, "amdb: capi hashtable init failed %d", err);
    THROW(err, err);
  }
  qurt_mutex_init(&me->mutex_capi);

  err = PL_INIT(mod_table);
  if (ADSP_EOK != err) {
    FARF(ERROR, "amdb: mod_table init failed %d", err);
    THROW(err, err);
  }

  err = mod_table_register_static("adsp_amdb", adsp_amdb_skel_invoke);
  if (ADSP_EOK != err) {
    FARF(ERROR, "amdb: mod table register failed %d", err);
    THROW(err, err);
  }

  THROW(err, ADSP_EOK);

  CATCH(err);
  return err;
}

void adsp_amdb_deinit(void)
{
  adsp_amdb_t* me = adsp_amdb_ptr_;

  PL_DEINIT(mod_table);

  qurt_mutex_lock(&me->mutex_appi);
  (void)hashtable_remove_all(&me->ht_appi);
  hashtable_deinit(&me->ht_appi);
  qurt_mutex_unlock(&me->mutex_appi);
  qurt_mutex_destroy(&me->mutex_appi);

  qurt_mutex_lock(&me->mutex_capi);
  (void)hashtable_remove_all(&me->ht_capi);
  hashtable_deinit(&me->ht_capi);
  qurt_mutex_unlock(&me->mutex_capi);
  qurt_mutex_destroy(&me->mutex_capi);
}

PL_DEFINE(adsp_amdb, adsp_amdb_init, adsp_amdb_deinit);

