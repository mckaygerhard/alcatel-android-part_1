#ifndef _ADSP_AMDB_SKEL_H
#define _ADSP_AMDB_SKEL_H
#include "adsp_amdb.h"
#include "remote.h"
#include <string.h>

#ifndef SLIM_H
#define SLIM_H

#include "AEEStdDef.h"

//a C data structure for the idl types that can be used to implement
//static and dynamic language bindings fairly efficiently.
//
//the goal is to have a minimal ROM and RAM footprint and without
//doing too many allocations.  A good way to package these things seemed
//like the module boundary, so all the idls within  one module can share
//all the type references.


#define PARAMETER_IN       0x0
#define PARAMETER_OUT      0x1
#define PARAMETER_INOUT    0x2
#define PARAMETER_ROUT     0x3
#define PARAMETER_INROUT   0x4

//the types that we get from idl
#define TYPE_OBJECT             0x0
#define TYPE_INTERFACE          0x1
#define TYPE_PRIMITIVE          0x2
#define TYPE_ENUM               0x3
#define TYPE_STRING             0x4
#define TYPE_WSTRING            0x5
#define TYPE_STRUCTURE          0x6
#define TYPE_UNION              0x7
#define TYPE_ARRAY              0x8
#define TYPE_SEQUENCE           0x9

//these require the pack/unpack to recurse
//so it's a hint to those languages that can optimize in cases where
//recursion isn't necessary.
#define TYPE_COMPLEX_STRUCTURE  (0x10 | TYPE_STRUCTURE)
#define TYPE_COMPLEX_UNION      (0x10 | TYPE_UNION)
#define TYPE_COMPLEX_ARRAY      (0x10 | TYPE_ARRAY)
#define TYPE_COMPLEX_SEQUENCE   (0x10 | TYPE_SEQUENCE)


typedef struct Type Type;

#define INHERIT_TYPE\
   int32 nativeSize;                /*in the simple case its the same as wire size and alignment*/\
   union {\
      struct {\
         const uint32      p1;\
         const uint32      p2;\
      } _cast;\
      struct {\
         AEEIID  iid;\
         uint32  bNotNil;\
      } object;\
      struct {\
         const Type  *arrayType;\
         int32       nItems;\
      } array;\
      struct {\
         const Type *seqType;\
         int32       nMaxLen;\
      } seqSimple; \
      struct {\
         uint32 bFloating;\
         uint32 bSigned;\
      } prim; \
      const SequenceType* seqComplex;\
      const UnionType  *unionType;\
      const StructType *structType;\
      int32          stringMaxLen;\
      boolean        bInterfaceNotNil;\
   } param;\
   uint8    type;\
   uint8    nativeAlignment\

typedef struct UnionType UnionType;
typedef struct StructType StructType;
typedef struct SequenceType SequenceType;
struct Type {
   INHERIT_TYPE;
};

struct SequenceType {
   const Type *         seqType;
   uint32               nMaxLen;
   uint32               inSize;
   uint32               routSizePrimIn;
   uint32               routSizePrimROut;
};

//byte offset from the start of the case values for
//this unions case value array.  it MUST be aligned
//at the alignment requrements for the descriptor
//
//if negative it means that the unions cases are
//simple enumerators, so the value read from the descriptor
//can be used directly to find the correct case
typedef union CaseValuePtr CaseValuePtr;
union CaseValuePtr {
   const uint8*   value8s;
   const uint16*  value16s;
   const uint32*  value32s;
   const uint64*  value64s;
};

//these are only used in complex cases
//so I pulled them out of the type definition as references to make
//the type smaller
struct UnionType {
   const Type           *descriptor;
   uint32               nCases;
   const CaseValuePtr   caseValues;
   const Type * const   *cases;
   int32                inSize;
   int32                routSizePrimIn;
   int32                routSizePrimROut;
   uint8                inAlignment;
   uint8                routAlignmentPrimIn;
   uint8                routAlignmentPrimROut;
   uint8                inCaseAlignment;
   uint8                routCaseAlignmentPrimIn;
   uint8                routCaseAlignmentPrimROut;
   uint8                nativeCaseAlignment;
   boolean              bDefaultCase;
};

struct StructType {
   uint32               nMembers;
   const Type * const   *members;
   int32                inSize;
   int32                routSizePrimIn;
   int32                routSizePrimROut;
   uint8                inAlignment;
   uint8                routAlignmentPrimIn;
   uint8                routAlignmentPrimROut;
};

typedef struct Parameter Parameter;
struct Parameter {
   INHERIT_TYPE;
   uint8    mode;
   boolean  bNotNil;
};

#define SLIM_SCALARS_IS_DYNAMIC(u) (((u) & 0x00ffffff) == 0x00ffffff)

typedef struct Method Method;
struct Method {
   uint32                      uScalars;            //no method index
   int32                       primInSize;
   int32                       primROutSize;
   int                         maxArgs;
   int                         numParams;
   const Parameter * const     *params;
   uint8                       primInAlignment;
   uint8                       primROutAlignment;
};

typedef struct Interface Interface;

struct Interface {
   int                            nMethods;
   const Method  * const          *methodArray;
   int                            nIIds;
   const AEEIID                   *iids;
   const uint16*                  methodStringArray;
   const uint16*                  methodStrings;
   const char*                    strings;
};


#endif //SLIM_H


#ifndef _ADSP_AMDB_SLIM_H
#define _ADSP_AMDB_SLIM_H
#include "remote.h"

#ifndef __QAIC_SLIM
#define __QAIC_SLIM(ff) ff
#endif
#ifndef __QAIC_SLIM_EXPORT
#define __QAIC_SLIM_EXPORT
#endif

static const Parameter parameters[3] = {{0x4,{{0,1}}, 2,0x4,0,0},{0x1,{{0,0}}, 2,0x1,0,0},{0x8,{{(const uint32)0x0,0}}, 4,0x4,0,0}};
static const Parameter* const parameterArrays[7] = {(&(parameters[0])),(&(parameters[1])),(&(parameters[2])),(&(parameters[2])),(&(parameters[2])),(&(parameters[0])),(&(parameters[2]))};
static const Method methods[3] = {{REMOTE_SCALARS_MAKEX(0,0,0x4,0x0,0x0,0x0),0x14,0x0,5,5,(&(parameterArrays[0])),0x4,0x0},{REMOTE_SCALARS_MAKEX(0,0,0x2,0x0,0x0,0x0),0x8,0x0,2,2,(&(parameterArrays[5])),0x4,0x0},{REMOTE_SCALARS_MAKEX(0,0,0x0,0x0,0x0,0x0),0x0,0x0,0,0,0,0x0,0x0}};
static const Method* const methodArrays[8] = {&(methods[0]),&(methods[0]),&(methods[1]),&(methods[1]),&(methods[2]),&(methods[2]),&(methods[2]),&(methods[2])};
static const char strings[158] = "remove_all_appi\0remove_all_capi\0print_all_appi\0print_all_capi\0filename_str\0remove_appi\0remove_capi\0getsize_str\0init_str\0add_appi\0ctor_str\0add_capi\0preload\0id\0";
static const uint16 methodStrings[22] = {120,155,147,62,99,111,138,155,147,62,99,129,75,155,62,87,155,62,32,47,0,16};
static const uint16 methodStringsArrays[8] = {6,0,15,12,21,20,19,18};
__QAIC_SLIM_EXPORT const Interface __QAIC_SLIM(adsp_amdb_slim) = {8,&(methodArrays[0]),0,0,&(methodStringsArrays [0]),methodStrings,strings};
#endif //_ADSP_AMDB_SLIM_H
#ifdef __qdsp6__
#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wuninitialized"
#endif
#endif

#ifndef __QAIC_REMOTE
#define __QAIC_REMOTE(ff) ff
#endif //__QAIC_REMOTE

#ifndef __QAIC_HEADER
#define __QAIC_HEADER(ff) ff
#endif //__QAIC_HEADER

#ifndef __QAIC_HEADER_EXPORT
#define __QAIC_HEADER_EXPORT
#endif // __QAIC_HEADER_EXPORT

#ifndef __QAIC_HEADER_ATTRIBUTE
#define __QAIC_HEADER_ATTRIBUTE
#endif // __QAIC_HEADER_ATTRIBUTE

#ifndef __QAIC_IMPL
#define __QAIC_IMPL(ff) ff
#endif //__QAIC_IMPL

#ifndef __QAIC_IMPL_EXPORT
#define __QAIC_IMPL_EXPORT
#endif // __QAIC_IMPL_EXPORT

#ifndef __QAIC_IMPL_ATTRIBUTE
#define __QAIC_IMPL_ATTRIBUTE
#endif // __QAIC_IMPL_ATTRIBUTE

#ifndef __QAIC_STUB
#define __QAIC_STUB(ff) ff
#endif //__QAIC_STUB

#ifndef __QAIC_STUB_EXPORT
#define __QAIC_STUB_EXPORT
#endif // __QAIC_STUB_EXPORT

#ifndef __QAIC_STUB_ATTRIBUTE
#define __QAIC_STUB_ATTRIBUTE
#endif // __QAIC_STUB_ATTRIBUTE

#ifndef __QAIC_SKEL
#define __QAIC_SKEL(ff) ff
#endif //__QAIC_SKEL__

#ifndef __QAIC_SKEL_EXPORT
#define __QAIC_SKEL_EXPORT
#endif // __QAIC_SKEL_EXPORT

#ifndef __QAIC_SKEL_ATTRIBUTE
#define __QAIC_SKEL_ATTRIBUTE
#endif // __QAIC_SKEL_ATTRIBUTE

#ifdef __QAIC_DEBUG__
   #ifndef __QAIC_DBG_PRINTF__
   #define __QAIC_DBG_PRINTF__( ee ) do { printf ee ; } while(0)
   #endif
#else
   #define __QAIC_DBG_PRINTF__( ee ) (void)0
#endif


#define _OFFSET(src, sof)  ((void*)(((char*)(src)) + (sof)))

#define _COPY(dst, dof, src, sof, sz)  \
   do {\
         struct __copy { \
            char ar[sz]; \
         };\
         *(struct __copy*)_OFFSET(dst, dof) = *(struct __copy*)_OFFSET(src, sof);\
   } while (0)

#define _ASSIGN(dst, src, sof)  \
   do {\
      dst = OFFSET(src, sof); \
   } while (0)

#define _STD_STRLEN_IF(str) (str == 0 ? 0 : strlen(str))

#include "AEEStdErr.h"

#define _TRY(ee, func) \
   do { \
      if (AEE_SUCCESS != ((ee) = func)) {\
         __QAIC_DBG_PRINTF__((__FILE_LINE__  ": error: %d\n", (int)(ee)));\
         goto ee##bail;\
      } \
   } while (0)

#define _CATCH(exception) exception##bail: if (exception != AEE_SUCCESS)

#define _ASSERT(nErr, ff) _TRY(nErr, 0 == (ff) ? AEE_EBADPARM : AEE_SUCCESS)

#ifdef __cplusplus
extern "C" {
#endif
static __inline int _skel_method(int (*_pfn)(void), uint32 _sc, remote_arg* _pra) {
   remote_arg* _praEnd;
   int _nErr = 0;
   _praEnd = ((_pra + REMOTE_SCALARS_INBUFS(_sc)) + REMOTE_SCALARS_OUTBUFS(_sc));
   _ASSERT(_nErr, (_pra + 0) <= _praEnd);
   _TRY(_nErr, _pfn());
   _CATCH(_nErr) {}
   return _nErr;
}
static __inline int _skel_method_1(int (*_pfn)(uint32, char*), uint32 _sc, remote_arg* _pra) {
   remote_arg* _praEnd;
   uint32 _in0[1];
   char* _in1[1];
   uint32 _in1Len[1];
   uint32* _primIn;
   remote_arg* _praIn;
   int _nErr = 0;
   _praEnd = ((_pra + REMOTE_SCALARS_INBUFS(_sc)) + REMOTE_SCALARS_OUTBUFS(_sc));
   _ASSERT(_nErr, (_pra + 2) <= _praEnd);
   _ASSERT(_nErr, _pra[0].buf.nLen >= 8);
   _primIn = _pra[0].buf.pv;
   _COPY(_in0, 0, _primIn, 0, 4);
   _COPY(_in1Len, 0, _primIn, 4, 4);
   _praIn = (_pra + 1);
   _ASSERT(_nErr, (_praIn[0].buf.nLen / 1) >= (int)_in1Len[0]);
   _in1[0] = _praIn[0].buf.pv;
   if(_in1Len[0] > 0)
   {
      _in1[0][(_in1Len[0] - 1)] = 0;
   }
   _TRY(_nErr, _pfn(*_in0, *_in1));
   _CATCH(_nErr) {}
   return _nErr;
}
static __inline int _skel_method_2(int (*_pfn)(uint32, uint8, char*, char*, char*), uint32 _sc, remote_arg* _pra) {
   remote_arg* _praEnd;
   uint32 _in0[1];
   uint8 _in1[1];
   char* _in2[1];
   uint32 _in2Len[1];
   char* _in3[1];
   uint32 _in3Len[1];
   char* _in4[1];
   uint32 _in4Len[1];
   uint32* _primIn;
   remote_arg* _praIn;
   int _nErr = 0;
   _praEnd = ((_pra + REMOTE_SCALARS_INBUFS(_sc)) + REMOTE_SCALARS_OUTBUFS(_sc));
   _ASSERT(_nErr, (_pra + 4) <= _praEnd);
   _ASSERT(_nErr, _pra[0].buf.nLen >= 20);
   _primIn = _pra[0].buf.pv;
   _COPY(_in0, 0, _primIn, 0, 4);
   _COPY(_in1, 0, _primIn, 4, 1);
   _COPY(_in2Len, 0, _primIn, 8, 4);
   _praIn = (_pra + 1);
   _ASSERT(_nErr, (_praIn[0].buf.nLen / 1) >= (int)_in2Len[0]);
   _in2[0] = _praIn[0].buf.pv;
   if(_in2Len[0] > 0)
   {
      _in2[0][(_in2Len[0] - 1)] = 0;
   }
   _COPY(_in3Len, 0, _primIn, 12, 4);
   _ASSERT(_nErr, (_praIn[1].buf.nLen / 1) >= (int)_in3Len[0]);
   _in3[0] = _praIn[1].buf.pv;
   if(_in3Len[0] > 0)
      _in3[0][(_in3Len[0] - 1)] = 0;
   _COPY(_in4Len, 0, _primIn, 16, 4);
   _ASSERT(_nErr, (_praIn[2].buf.nLen / 1) >= (int)_in4Len[0]);
   _in4[0] = _praIn[2].buf.pv;
   if(_in4Len[0] > 0)
      _in4[0][(_in4Len[0] - 1)] = 0;
   _TRY(_nErr, _pfn(*_in0, *_in1, *_in2, *_in3, *_in4));
   _CATCH(_nErr) {}
   return _nErr;
}
__QAIC_SKEL_EXPORT int __QAIC_SKEL(adsp_amdb_skel_invoke)(uint32 _sc, remote_arg* _pra) __QAIC_SKEL_ATTRIBUTE {
   switch(REMOTE_SCALARS_METHOD(_sc))
   {
      case 0:
      return _skel_method_2((void*)__QAIC_IMPL(adsp_amdb_add_capi), _sc, _pra);
      case 1:
      return _skel_method_2((void*)__QAIC_IMPL(adsp_amdb_add_appi), _sc, _pra);
      case 2:
      return _skel_method_1((void*)__QAIC_IMPL(adsp_amdb_remove_capi), _sc, _pra);
      case 3:
      return _skel_method_1((void*)__QAIC_IMPL(adsp_amdb_remove_appi), _sc, _pra);
      case 4:
      return _skel_method((void*)__QAIC_IMPL(adsp_amdb_remove_all_capi), _sc, _pra);
      case 5:
      return _skel_method((void*)__QAIC_IMPL(adsp_amdb_remove_all_appi), _sc, _pra);
      case 6:
      return _skel_method((void*)__QAIC_IMPL(adsp_amdb_print_all_capi), _sc, _pra);
      case 7:
      return _skel_method((void*)__QAIC_IMPL(adsp_amdb_print_all_appi), _sc, _pra);
   }
   return AEE_EUNSUPPORTED;
}
#ifdef __cplusplus
}
#endif
#endif //_ADSP_AMDB_SKEL_H
