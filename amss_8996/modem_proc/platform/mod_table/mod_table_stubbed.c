#include "mod_table.h"
#include "mod_table_cache.h"
#include "platform_libs.h"

/**
  * register a static component for invocations
  * this can be called at any time including from a static constructor
  *
  * name, name of the interface to register
  * pfn, function pointer to the skel invoke function
  *
  * for example:
  *   __attribute__((constructor)) static void my_module_ctor(void) {
  *      mod_table_register_static("my_module", my_module_skel_invoke);
  *   }
  *
  */
int mod_table_register_static(const char* name, int(*pfn)(uint32 sc, remote_arg* pra)) {	
  return 0;
}

/**
  * get and set the modules prefered caching attributes for input and output buffers
  * this can only be called from the invoking thread.
  *
  * @param bInBuf, if TRUE the value is set for input buffers, if FALSE for output buffers
  * @param attr, one of qurts attributes
  * 
  */
 
int mod_table_set_cache_attr(boolean bInBuf, qurt_mem_cache_mode_t attr) {
   return 0;
}

int mod_table_get_cache_attr(boolean bInBuf, qurt_mem_cache_mode_t* attr) {
   return 0;
}

/**
 * Open a module and get a handle to it
 *
 * in_name, name of module to open
 * handle, Output handle
 * dlerr, Error String (if an error occurs)
 * dlerrorLen, Length of error String (if an error occurs)
 * pdlErr, Error identifier
 */
int mod_table_open(const char* in_name, remote_handle* handle, char* dlerr, int dlerrorLen, int* pdlErr) {
   return 0;
}
/**
 * invoke a handle in the mod table
 *
 * handle, handle to invoke
 * sc, scalars, see remote.h for documentation.
 * pra, args, see remote.h for documentation.
 */
int mod_table_invoke(remote_handle handle, uint32 sc, remote_arg* pra) {	
   return 0;
}

/**
 * Closes a handle in the mod table
 *
 * handle, handle to close
 * errStr, Error String (if an error occurs)
 * errStrLen, Length of error String (if an error occurs)
 * pdlErr, Error identifier
 */
int mod_table_close(remote_handle handle, char* errStr, int errStrLen, int* pdlErr) {
	return 0;
}

/**
 * internal use only
 */
int mod_table_register_const_handle(remote_handle handle, const char* in_name, int(*pfn)(uint32 sc, remote_arg* pra)) {
   return 0;
}

// Constructor and destructor
static int mod_table_ctor(void) {
	return 0;
}
static void mod_table_dtor(void) {
	return;
}

PL_DEFINE(mod_table, mod_table_ctor, mod_table_dtor);
