#include <dlfcn.h>
#include "verify.h"

int main(void) {
   int nErr = 0;
   int count = -1;
   void* handle;
   void* sym;
   char *builtin[]={"libgcc.so", "libc.so", "libstdc++.so"};

   dlinit(3, builtin);
   handle = dlopen("libctors.so", 0);
   VERIFY(0 != (sym = dlsym(handle, "get_count")));
   VERIFY(0 == ((int(*)(int*))sym)(&count));
   VERIFY(0 == dlclose(handle));
   VERIFY(0 == count);
bail:
   return nErr;
}
