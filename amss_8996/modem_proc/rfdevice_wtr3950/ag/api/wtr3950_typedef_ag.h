#ifndef WTR3950_TYPEDEF_AG_H
#define WTR3950_TYPEDEF_AG_H
/*
  WARNING: This file is auto-generated.

  Generated at:    0
  Generated using: RFDevice_AutoGenerate.pl
  Generated from:  revision 55.4 of the Boson SSBI spreadsheet
*/

/*!
  @file
  wtr3950_typedef_ag.h

  @brief
  WTR3950 Data Type Definition autogen header

  @details
  This file is auto-generated and it contains the WTR3950 Data Type Definition SSBI data type to support the 
  interaction with the QUALCOMM WTR3950 chip

  @addtogroup WTR3950 Data Type Definition
  @{
*/

/*=============================================================================

Copyright (c) 2013, 2014 by QUALCOMM Technologies, Inc.  All Rights Reserved.
Copyright (c) 2015, 2016 by QUALCOMM Technologies, Inc.  All Rights Reserved.

Qualcomm Technologies Proprietary and Confidential

  Export of this technology or software is regulated by the U.S. 
  Government. Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfdevice_wtr3950/ag/api/wtr3950_typedef_ag.h#1 $
$DateTime: 2016/03/28 23:07:39 $
$Author: mplcsds1 $

=============================================================================*/
  
/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include "comdef.h"

  
/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR3950 ltefdd_rx_port enum.
*/
typedef enum 
{
  WTR3950_LTEFDD_RX_BAND_NUM, 
  WTR3950_LTEFDD_RX_BAND_INVALID, 
} wtr3950_ltefdd_rx_port_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR3950 ltefdd_rx_port enum.
*/
typedef enum 
{
  WTR3950_LTEFDD_TX_BAND_NUM, 
  WTR3950_LTEFDD_TX_BAND_INVALID, 
} wtr3950_ltefdd_tx_port_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR3925 fbrx_attn_mode enum.
*/
typedef enum 
{
  WTR3950_FBRX_LOW_ATTN_MODE, 
  WTR3950_FBRX_HIGH_ATTN_MODE, 
  WTR3950_FBRX_ATTN_NUM, 
  WTR3950_FBRX_ATTN_INVALID, 
  WTR3950_FBRX_ATTN_DEFAULT = WTR3950_FBRX_LOW_ATTN_MODE, 
} wtr3950_fbrx_attn_mode_data_type; 

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR3950 ltetdd_rx_port enum.
*/
typedef enum 
{
  WTR3950_LTETDD_DRXLGY1_BAND252_DHB1, 
  WTR3950_LTETDD_DRXLGY1_BAND253_DHB1, 
  WTR3950_LTETDD_DRXLGY1_BAND254_DHB1, 
  WTR3950_LTETDD_DRXLGY1_BAND255_DHB1, 
  WTR3950_LTETDD_PRXLGY1_BAND252_PHB1, 
  WTR3950_LTETDD_PRXLGY1_BAND253_PHB1, 
  WTR3950_LTETDD_PRXLGY1_BAND254_PHB1, 
  WTR3950_LTETDD_PRXLGY1_BAND255_PHB1, 
  WTR3950_LTETDD_RX_BAND_NUM, 
  WTR3950_LTETDD_RX_BAND_INVALID, 
} wtr3950_ltetdd_rx_port_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR3950 ltetdd_tx_port enum.
*/
typedef enum 
{
  WTR3950_LTETDD_TX_BAND252_THB1, 
  WTR3950_LTETDD_TXWSAW_BAND252_THB1, 
  WTR3950_LTETDD_TX_BAND253_THB1, 
  WTR3950_LTETDD_TXWSAW_BAND253_THB1, 
  WTR3950_LTETDD_TX_BAND254_THB1, 
  WTR3950_LTETDD_TXWSAW_BAND254_THB1, 
  WTR3950_LTETDD_TX_BAND255_THB1, 
  WTR3950_LTETDD_TXWSAW_BAND255_THB1, 
  WTR3950_LTETDD_TX_BAND_NUM, 
  WTR3950_LTETDD_TX_BAND_INVALID, 
} wtr3950_ltetdd_tx_port_data_type;


#endif /* WTR3950_TYPEDEF_AG_H */

/*! @} */ 
