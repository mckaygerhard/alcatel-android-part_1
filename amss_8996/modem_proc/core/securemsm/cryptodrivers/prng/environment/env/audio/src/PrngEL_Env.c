/**
@file PrngEL.c 
@brief PRNG Engine source file 
*/

/*===========================================================================

                     P R N G E n g i n e D r i v e r

DESCRIPTION
  This file contains declarations and definitions for the
  environment layer of PRNG driver

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

 $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/cryptodrivers/prng/environment/env/audio/src/PrngEL_Env.c#1 $
 $DateTime: 2016/03/28 23:02:17 $
 $Author: mplcsds1 $ 

when         who     what, where, why
--------     ---     ----------------------------------------------------------
2013-12-03   nk     Increased random number placeholder
2012-09-19   amen   Added PRNG lite api's, dont use mutex or dal apis, direct register config
2011-09-08   nk     Added the implementation of de-init routine. 
2010-06-25   yk     Initial version
============================================================================*/


#include "comdef.h"
#include "PrngCL.h"
#include "PrngEL.h"
#include "PrngEL_Env.h"
#include "PrngCL_target.h"
#include <qurt.h>
#include <qurt_qdi_driver.h>

#define PRNG_DRIVER_NAME     ("/dev/prng")
#define PRNG_METHOD_GETDATA  (QDI_PRIVATE+0)

PrngEL_Result_Type PrngEL_getdata_QDI(uint8* random_ptr, uint16 random_len)
{
   static int handle = -1;
   int ret;

   /* If no QDI handle yet, try to get one */
   if (handle < 0)
      handle = qurt_qdi_open(PRNG_DRIVER_NAME);

   /* If still no QDI handle, give up */
   if (handle < 0)
      return PRNGEL_ERROR_UNSUPPORTED;

   /* Ask the QDI driver to do the work; if it returns a negative value, that's a QDI failure */
   ret = qurt_qdi_handle_invoke(handle, PRNG_METHOD_GETDATA, random_ptr, random_len);
   if (ret < 0)
      return PRNGEL_ERROR_UNSUPPORTED;

   /* Return the result of the QDI call, appropriately cast */
   return (PrngEL_Result_Type)(ret);
}

