/**
* @file modem_rollback_version_utils.c
* @brief modem rollback utility function
*
* This file implements functions to get anti-rollback hwio address table. 
*
*/
/*===========================================================================
   Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/modem_sec/src/9x45/modem_rollback_version_utils.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

when       who      what, where, why
--------   ---      ------------------------------------
05/29/2015 hw       Initial version
===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include "modem_hwio.h"
#include "qfprom.h"
#include "DALSys.h"
#include "modem_rollback_version.h"


#define MODEM_CHIPINFO_FAMILY_MDM9x45 (0x2)
#define MODEM_CHIPINFO_VERSION(major, minor)  (((major) << 8) | (minor))

/*===========================================================================

**  functions to check chipset family

** ==========================================================================
*/
uint32 modem_Chipinfo_GetFamily (void)
{
  uint32 ChipFamilyNum = 0;
  ChipFamilyNum = HWIO_INF(TCSR_SOC_HW_VERSION, FAMILY_NUMBER);
  return ChipFamilyNum;    
       
}

uint32 modem_Chipinfo_GetVersion (void)
{
  uint32 ChipVersion;
  ChipVersion = MODEM_CHIPINFO_VERSION(
      HWIO_INF(TCSR_SOC_HW_VERSION, MAJOR_VERSION),
      HWIO_INF(TCSR_SOC_HW_VERSION, MINOR_VERSION));

  return ChipVersion;
}

struct modem_version_rollback_img_set* modem_get_modem_sec_modem_img()
{

    if ((modem_Chipinfo_GetFamily() == MODEM_CHIPINFO_FAMILY_MDM9x45) && (modem_Chipinfo_GetVersion() >= MODEM_CHIPINFO_VERSION(2,0)))
    {
      return &modem_sec_modem_img_v2;
    }
    else
    {
      return &modem_sec_modem_img;
    }
}

struct modem_version_rollback_img_set* modem_get_modem_sec_mba_img()
{
    if ((modem_Chipinfo_GetFamily() == MODEM_CHIPINFO_FAMILY_MDM9x45) && (modem_Chipinfo_GetVersion() >= MODEM_CHIPINFO_VERSION(2,0)))
    {
      return &modem_sec_mba_img_v2;
    }
    else
    {
      return &modem_sec_mba_img;
    }
}