/**
* @file modem_rollback_version_utils.c
* @brief modem rollback utility function
*
* This file implements functions to get anti-rollback hwio address table. 
*
*/
/*===========================================================================
   Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/modem_sec/src/8996/modem_rollback_version_utils.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

when       who      what, where, why
--------   ---      ------------------------------------
05/29/2015 hw       Initial version
===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include "modem_hwio.h"
#include "qfprom.h"
#include "DALSys.h"
#include "modem_rollback_version.h"


struct modem_version_rollback_img_set* modem_get_modem_sec_modem_img()
{
  return &modem_sec_modem_img;
}

struct modem_version_rollback_img_set* modem_get_modem_sec_mba_img()
{
  return &modem_sec_mba_img;
}