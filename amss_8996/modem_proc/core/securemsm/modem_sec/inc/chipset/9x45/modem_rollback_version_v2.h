#ifndef MODEM_ROLLBACK_VERSION_V2_H
#define MODEM_ROLLBACK_VERSION_V2_H

/**
@file modem_rollback_version.h
@brief Modem Rollback Protection
*/

/*===========================================================================
   Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/modem_sec/inc/chipset/9x45/modem_rollback_version_v2.h#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

when       who      what, where, why
--------   ---      ------------------------------------
01/09/13   rsahay       Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "comdef.h"
#include "HALhwio.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/* Retrieved from FLat File - ARM_ADDRESS_FILE.FLAT*/
#include "msmhwiobase.h"
#include "modem_rollback.h"

/* HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ANTI_ROLLBACK_FEATURE_EN_BMSK reads all 4 rollback bits 
 *  So we define our own mask here that reads only if MSA Rollback is enabled.
 */
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_FEATURE_EN_BMSK       0x00400000

static struct modem_version_rollback_img_set modem_sec_mba_img_v2 =
{
    /* image_id */
    MODEM_SEC_MBA_ID,

    /* feature_enable_mask */
     HWIO_QFPROM_CORR_ANTI_ROLLBACK_FEATURE_EN_BMSK,
    
     /* feature_enable_base_addr */
    (uint32 *)HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_V2_ADDR,
    
     /* version_mask*/
     HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_V2_MBA_BMSK,
    
     /* version_shift */
     HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_V2_MBA_SHFT,    
    
     /* version_lsb_base_addr */
     (uint32 *)HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_V2_PHYS,
    
     /* version_lsb_base_addr */
     (uint32 *)HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_V2_ADDR,

     /* current_version_num */
     0,
};

static struct modem_version_rollback_img_set modem_sec_modem_img_v2 =
{
    /* image_id */
    MODEM_SEC_MODEM_ID,

    /* feature_enable_mask */
     HWIO_QFPROM_CORR_ANTI_ROLLBACK_FEATURE_EN_BMSK,
    
     /* feature_enable_base_addr */
     (uint32 *)HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_V2_ADDR,
    
     /* version_mask*/
     HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_V2_MSS_BMSK,
    
     /* version_shift */
     HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_V2_MSS_SHFT,    
    
     /* version_lsb_base_addr */
     (uint32 *)HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_V2_PHYS,

     /* version_lsb_base_addr */
     (uint32 *)HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_V2_ADDR,
    
     /* current_version_num */
     0,
};

#endif /* MODEM_ROLLBACK_VERSION_V2_H */
