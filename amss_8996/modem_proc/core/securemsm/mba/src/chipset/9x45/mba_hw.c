/**
* @file mba_hw.c
* @brief define chipset specific hw function
*
*/
/*===========================================================================
   Copyright (c) 2015 by QUALCOMM, Technologies Inc.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/mba/src/chipset/9x45/mba_hw.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $


when       who      what, where, why
--------   ---      ------------------------------------
06/03/2015 hw       Initial version
===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <IxErrno.h>
#include <comdef.h>
#include "HALhwio.h"
#include "mba_hwio.h"
#include "mba_rmb.h"
#include "secboot_hw.h"
#include "mba_utils.h"


uint32 mba_hw_read_mss_anti_rollback_fuse_version()
{
  uint32 msa_rollback = 0;
  if (mba_Chipinfo_GetFamily() == MBA_CHIPINFO_FAMILY_MDM9x45) 
  {
    if (mba_Chipinfo_GetVersion() >= MBA_CHIPINFO_VERSION(2,0))
    {
      msa_rollback = HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_V2_IN;  
      msa_rollback = msa_rollback & HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_V2_MSS_BMSK;
      msa_rollback = msa_rollback >> HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_V2_MSS_SHFT;
    }
    else
    {
      msa_rollback = HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_IN;  
      msa_rollback = msa_rollback & HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MSS_BMSK;
      msa_rollback = msa_rollback >> HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MSS_SHFT;
    }
  }
  return msa_rollback;
}

int mba_hw_write_override_4_fuse_value
(
  uint32 fuse_value
)
{
  /* Write to override register to make sure it doesnt get written to later*/
  if ((mba_Chipinfo_GetFamily() == MBA_CHIPINFO_FAMILY_MDM9x45) && (mba_Chipinfo_GetVersion() >= MBA_CHIPINFO_VERSION(2,0)))
  {
    HWIO_OVERRIDE_4_V2_OUT(fuse_value);
  }
  else
  {
    HWIO_OVERRIDE_4_OUT(fuse_value);
  }
  return E_SUCCESS;
}

int mba_pipe_vmid_configure()
{
#ifndef FEATURE_FLASHLESS_MBA
  /* Enable MSA signal generation for MSA owned pipes in QPIC BAM */
  //HWIO_PERIPH_SS_QPIC_QPIC_VMIDMT_MSDR0_OUT(0x38);
  // Check with AC team what value needs to be put here. The wiki
  // currently has no info on this
  HWIO_QPIC_QPIC_VMIDMT_MSDR0_OUT(0x38);
#endif

#if defined(TARGET_8916)
  HWIO_BLSP1_BLSP_BAM_VMIDMT_MSDR0_OUT(0x00FCF000);
#else
  // The 9x45 AC wiki for this VMIDMT says "All pipes are assigned to MSA=0"
  //HWIO_PERIPH_SS_BLSP1_BLSP_BAM_VMIDMT_MSDR0_OUT(0x00FFF000);
#endif

  HWIO_IPA_IPA_VMIDMT_MSDR0_OUT(0x600003C0);
  
  return E_SUCCESS;
}