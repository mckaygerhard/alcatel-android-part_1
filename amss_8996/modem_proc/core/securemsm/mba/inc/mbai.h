/**
* @file mbai.h
* @brief internal mba header file
*
* This file declare internal functions
*
*/
/*===========================================================================
   Copyright (c) 2015 by QUALCOMM Technologies, Inc.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/mba/inc/mbai.h#2 $
  $DateTime: 2016/10/13 03:07:25 $
  $Author: pwbldsvc $


when       who      what, where, why
--------   ---      ------------------------------------
02/28/2012 mm       Integrated with secure pil library
12/02/2011 mm       Initial version
===========================================================================*/
#include <comdef.h>
#include "mba_ac.h"

#define XPU_ENABLE          0x01 /**< Enables an XPU. */
#define XPU_NO_INTERRUPTS   0x02 /**< Disables XPU error interrupts. */
#define XPU_MODEM_PROT      0x04 /**< Marks modem only items. */
#define XPU_SEC             0x08 /**< Secure partition */
#define XPU_NON_SEC         0x10 /**< Non Secure partition */
#define XPU_UMR_MSACLROE    (1 << 5) /**< MSA unmapped read-access  */
#define XPU_UMR_MSACLRWE    (1 << 6) /**< MSA unmapped read/write-access  */

#define XPU_RWGE                0x0100 /**< Read/write global enable. */
#define XPU_ROGE                0x0200 /**< Read-only global enable. */
#define XPU_RWE                 0x0400 /**< Read/write VMID enable. */
#define XPU_ROE                 0x0800 /**< Read-only VMID enable. */
#define XPU_VMIDCLROE           0x1000 /**< VMID based Read-Only enable for Secure/MSA resource. */
#define XPU_VMIDCLRWE           0x2000 /**< VMID based Read-Write enable for Secure/MSA resource. */
#define XPU_MSACLROE            0x4000 /**< MSA Read-only enable for secure partitions. */
#define XPU_MSACLRWE            0x8000 /**< MSA Read-Write enable for secure partitions. */

#define XPU_VMID_NOACCESS     0
#define XPU_VMID_VMID_0       0
#define XPU_VMID_NOACCESS_BIT (1<<XPU_VMID_NOACCESS)
#define XPU_VMID_VMID_0_BIT   (1<<XPU_VMID_VMID_0)

#define HAS_RWGE(config, ii) \
      ((XPU_RWGE & (config)->rg.rpu[(ii)].flags) ? TRUE: FALSE)
#define HAS_RWE(config, ii) \
      ((XPU_RWE & (config)->rg.rpu[(ii)].flags) ? TRUE: FALSE)
#define HAS_ROGE(config, ii) \
      ((XPU_ROGE & (config)->rg.rpu[(ii)].flags) ? TRUE: FALSE)
#define HAS_ROE(config, ii) \
      ((XPU_ROE & (config)->rg.rpu[(ii)].flags) ? TRUE: FALSE)

typedef enum
{
  XPU_DEVICE_RPU            = 0,           /**< Register protection unit */
  XPU_DEVICE_APU            = 1,           /**< Area protection unit */
  XPU_DEVICE_MPU            = 2,           /**< Memory protection unit */
  XPU_DEVICE_COUNT,
  XPU_DEVICE_DNE            = 0x7FFFFFFE,  /**< device does not exist */
  XPU_DEVICE_SIZE           = 0x7FFFFFFF,  /* force to 32-bit enum */
} xpu_type_t;

/**
 * Resource group configuration for MPUs. All MPUs are multi-VMID.
 */
typedef struct xpu_mpu_rg_s
{
  uint16 index;      /* Index of the MPU resource group. */
  /** XPU status bits, currently \c TZBSP_XPU_ENABLE, \c
   * TZBSP_XPU_NO_INTERRUPTS, \c TZBSP_XPU_SEC, \c TZBSP_XPU_NON_SEC and
   * \c TZBSP_XPU_MODEM_PROT are supported.
   */
  uint16 flags;
  uint32 read_vmid;  /* VMIDs able to read this partition. */
  uint32 write_vmid; /* VMIDs able to write this partition. */
  uint32 start;  /* Start of the partition. */
  uint32 end;    /* End of the partition, not included in the range */
} xpu_mpu_rg_t;

typedef struct xpu_rpu_rg_s
{
  /** Index of the APU/RPU resource group. */
  uint16 index;
  /**
   * Valid for single VMID resource groups only. Bits \c TZBSP_RWGE and \c
   * TZBSP_ROGE allow defining read/write global enable and read-only global
   * enable. Bit \c TZBSP_RWE enables the read/write access VMID(s). Bit \c
   * TZBSP_ROE enables the read access VMID(s).
   */
  uint16 flags;
  /**
   * For multi VMID resource groups contains a bitmap of VMIDs that can read
   * the resource. For single VMID resource groups contains a VMID value that
   * can read the resource.
   */
  uint32 read_vmid;
  /**
   * For multi VMID resource groups contains a bitmap of VMIDs that can
   * read/write the resource. For single VMID resource groups contains a VMID
   * value that can read/write the resource.
   */
  uint32 write_vmid;
} xpu_rpu_rg_t;

/**
 * Root level structure for XPU configuration.
 */
typedef struct
{
  /** Type of the XPU (APU/RPU/MPU) */
  xpu_type_t type;
  /** Index of the XPU, @see \c HAL_xpu_XPUType. */
  uint16 id;
  /** XPU status bits, currently \c TZBSP_XPU_ENABLE, \c
   * TZBSP_XPU_NO_INTERRUPTS and \c TZBSP_XPU_MODEM_PROT are supported.
   */
  uint16 flags;
  /** Read access VMIDs for unmapped area */
  uint32 unmapped_rvmid;
  /** Write access VMIDs for unmapped area */
  uint32 unmapped_wvmid;
  /** Number of configured resource groups. */
  uint16 nrg;
  union
  {
    const void* any;           /* A dummy to keep compiler happy. */
    const xpu_rpu_rg_t* rpu; /* Can be NULL. */
    const xpu_mpu_rg_t* mpu; /* Can be NULL. */
  } rg;
} xpu_cfg_t;

/* Extern functions */
int mba_unlock_xpu_all(void);
void mba_scribble_xpu_memory(void);
int mba_scribble_xpu_single_partition_memory(resource_id_type resource_id);
int mba_initialize (void);
int mba_bam_initialize (void);
int mba_xpu_lock_ddr_heap(void);
int mba_deinitialize (void);
uint32 mba_configure_dehr(void);
int mba_unlock_xpu_all(void);
int mba_heap_entry_delete(void);
 /* Get thes software version from the Rollback fuse */
 uint32 mba_secctrl_get_software_version(void);
 
 int mba_xpu_unlock_region(resource_id_type resource_id);
/* mba initialize heap for crypto */
int mba_heap_init(void);
/* read MSS anti_rollback fuse version */
uint32 mba_hw_read_mss_anti_rollback_fuse_version();
/* write OVERRIDE_4 */
int mba_hw_write_override_4_fuse_value(uint32 fuse_value);
/* configure vmids */
int mba_pipe_vmid_configure();
#ifdef FEATURE_ANOC2_XPU_ENABLED
/* config anoc2 xpu */
int mba_anoc2_xpu_configure(uint32 selected_partition,uint32 partition_start,uint32 partition_end,  
	                        uint32 anoc2_mpu_partition_num, uint32 mba_owned_partition_num);
int mba_xpu_static_config();
#endif

