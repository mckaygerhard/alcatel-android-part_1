#ifndef MBA_TZ_H
#define MBA_TZ_H

#include "comdef.h"

#define TZ_MPSS_SEC_CHANNEL_FUSE_OFFSET 0x100

/* Read the L1 key from TZ controlled memory */
void mba_get_l1_key(uint8 *key, uint32 keylen);

#endif /* MBA_TZ_H*/