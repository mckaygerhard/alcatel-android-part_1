#ifndef MBA_XPU_H
#define MBA_XPU_H

/**
* @file mba_ac.h
* @brief mba access control header
*
* This file defines access control value
*
*/
/*===========================================================================
   Copyright (c) 2015 by QUALCOMM Technologies, Inc.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/mba/inc/chipset/8996/mba_xpu.h#1 $
  $DateTime: 2016/03/28 23:02:17 $ 
  $Author: mplcsds1 $

when       who          what, where, why
--------   --------     ------------------------------------------------------
06/16/15   hw         Initial version. 
============================================================================*/

#include "comdef.h"

/* Apps VMID for Modem EFS */
#define TZBSP_VMID_VMID_0_BIT   0x1
#define TZBSP_VMID_APCPU        0x10000 // (bit 16 for 8996, bit 11 for 9x45)
#define TZBSP_VMID_TZ           0x4 

#define MBA_XPU_EFS_READ_VMIDS (TZBSP_VMID_VMID_0_BIT|TZBSP_VMID_APCPU|TZBSP_VMID_TZ)
#define MBA_XPU_EFS_WRITE_VMIDS (TZBSP_VMID_VMID_0_BIT|TZBSP_VMID_APCPU|TZBSP_VMID_TZ)

/* all the other cases is for MSM for which bank size is 256MB */ 
#define MBA_HW_LARGE_BANK_SETUP (0x1)

/*BOOT ROM MPU - MODEM PARTITION */
#define MBA_BOOT_ROM_MPU_SELECTED_PART (0x1)
#define MBA_BOOT_ROM_MPU_PART_START (0x12A000)
#define MBA_BOOT_ROM_MPU_PART_END (0x13C000)

/* ANOC2 XPU configuration */
#define MBA_ANOC2_MPU_PARTITION (0x4)
#define MBA_MAX_ANOC2_MPU_PARTITIONS (0x1)
#define MBA_ANOC2_MPU_SELECTED_RG (0x1)
#define MBA_ANOC2_MPU_PART_START (0x588000)
#define MBA_ANOC2_MPU_PART_END (0x589000)

#endif /* MBA_XPU_H */