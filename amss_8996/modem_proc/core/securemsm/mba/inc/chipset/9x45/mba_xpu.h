#ifndef MBA_XPU_H
#define MBA_XPU_H

/**
* @file mba_ac.h
* @brief mba access control header
*
* This file defines access control value
*
*/
/*===========================================================================
   Copyright (c) 2015 by QUALCOMM Technologies, Inc.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/mba/inc/chipset/9x45/mba_xpu.h#1 $
  $DateTime: 2016/03/28 23:02:17 $ 
  $Author: mplcsds1 $

when       who          what, where, why
--------   --------     ------------------------------------------------------
06/16/15   hw         Initial version. 
============================================================================*/

#include "comdef.h"

/* Apps VMID for Modem EFS */
#define TZBSP_VMID_APCPU        0x800
#define TZBSP_VMID_AP           0x8
#define TZBSP_VMID_TZ           0x4

#define MBA_XPU_EFS_READ_VMIDS (TZBSP_VMID_AP|TZBSP_VMID_APCPU|TZBSP_VMID_TZ)
#define MBA_XPU_EFS_WRITE_VMIDS (TZBSP_VMID_AP|TZBSP_VMID_APCPU|TZBSP_VMID_TZ)

/* This is the case with MDM for which bank size will be 128MB */
#define MBA_HW_LARGE_BANK_SETUP (0x0)

/*BOOT ROM MPU - MODEM PARTITION */
#define MBA_BOOT_ROM_MPU_SELECTED_PART (0x0)
#define MBA_BOOT_ROM_MPU_PART_START 0x18000
#define MBA_BOOT_ROM_MPU_PART_END 0x24000

#endif /* MBA_XPU_H */