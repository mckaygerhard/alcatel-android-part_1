/**
* @file sec_pil.c
* @brief Secure PIL implementation
*
* This file implements the subsystem authentication for PIL. 
* These routines are used by Trustzone/MBA to securely authenticate the ELF image
* for the respective subsystem. At the time of Secure PIL loading, the HLOS is
* already up and running to for better user experience. When Secure PIL is
* called, TZ/MBA XPU protects the relevant memory, authenticates the ELF image, and
* then brings the respective subsystem out of reset.
*/

/*===========================================================================
   Copyright (c) 2012-2014 by QUALCOMM Technologies, Inc.   All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/secpil/chipset/mdm9x45/src/sec_pil.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $


when       who      what, where, why
--------   ---      ------------------------------------
04/30/14   hw       added qpsi review fixes
26/03/13   dm       Added overflow checks & error checks
05/02/12   vg       Added MPU functinality for segmented authentcation.
04/05/12   vg       Added segmented authentication changes.
02/28/12   vg       Fixed review commnets and Definition is added for 
                    sec_pil_get_image_length().
02/26/12   vg       Moved inclusion of sec_pil_env.h to sec_pil.h
02/20/12   vg       Ported from TZ PIL.
===========================================================================*/


/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <string.h>
#include <IxErrno.h>
#include "sec_pil.h"
#include "sec_pil_env.h"
#include "sec_pil_priv.h"
#include "sec_pil_auth.h"


/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
static sec_pil_priv_t s_pil_info[SEC_PIL_AUTH_NUM_SUPPORTED_PROCS];
static sec_pil_seg_auth_info_t s_pil_seg_info[SEC_PIL_AUTH_NUM_SUPPORTED_PROCS];
static int sec_pil_adjust_elf_addrs(sec_pil_proc_et proc);


/*----------------------------------------------------------------------------
 * External Function Defintions 
 * -------------------------------------------------------------------------*/

/**
 * @brief Initilize all static/global variables used for PIL
 *
 * @return 0 on success, negative on failure
 */
int sec_pil_init(void)
{

  memset(s_pil_info, 0, sizeof(sec_pil_priv_t) *
         SEC_PIL_AUTH_UNSUPPORTED_PROC);

  memset(s_pil_seg_info, 0, sizeof(sec_pil_seg_auth_info_t) *
         SEC_PIL_AUTH_UNSUPPORTED_PROC);

  if(sec_pil_get_secboot_ftbl() != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_GET_SEBOOT_FTBL_FAILED);
    return -E_FAILURE;
  }
  
  return E_SUCCESS;
}

/**
 * @brief Authenticates hash table segment against its signature.
 *
 * @param[in] proc        The peripheral image
 * @param[in] elf_hdr     Pointer to the ELF header 
 *
 * @return 0 on success, negative on failure.
 */

int sec_pil_init_image(sec_pil_proc_et proc,
                       const Elf32_Ehdr * elf_hdr,
                       sec_pil_verified_info_t *v_info)
{
  int ret = E_SUCCESS;  

  if (elf_hdr == NULL)
  {
    SEC_PIL_ERROR_CODE(PIL_INIT_IMAGE_NULL_ELF_HDR);
    return -E_INVALID_ARG;
  }

  if (!sec_pil_is_proc_supported(proc))
  {
    SEC_PIL_ERROR_CODE(PIL_INIT_IMAGE_UNSUPPORTED_PROC);
    return -E_INVALID_ARG;
  }

  /* ELF header must be fully in non-secure memory. */
  if(!sec_pil_is_ns_range(elf_hdr, sizeof(Elf32_Ehdr)))
  {
    SEC_PIL_ERROR_CODE(PIL_INIT_IMAGE_ELF_HDR_NOT_NONSECURE);
    return -E_BAD_ADDRESS;
  }

  do
  {
    sec_pil_clean_pil_priv(&s_pil_info[proc],&s_pil_seg_info[proc]);
    s_pil_info[proc].proc = proc;
    s_pil_seg_info[proc].proc = proc;

    if (sec_pil_is_elf(elf_hdr) == FALSE)
    {
      SEC_PIL_ERROR_CODE(PIL_INIT_IMAGE_IS_NOT_ELF);
      ret = -E_FAILURE;
      break;
    }

    /* Allocate memory and copy headers and segments necessary for reset */
    if (sec_pil_populate_elf_info(proc, elf_hdr, &s_pil_info[proc].elf) != E_SUCCESS)
    {
      SEC_PIL_ERROR_CODE(PIL_INIT_IMAGE_POPULATE_ELF_INFO_FAILED);
      ret = -E_FAILURE;
      break;
    }

    if (sec_pil_verify_sig(proc, &s_pil_info[proc].elf, v_info) != E_SUCCESS)
    {
      SEC_PIL_ERROR_CODE(PIL_INIT_IMAGE_VERIFY_SIG_FAILED);
      ret = -E_FAILURE;
      break;
    }

    /* Does some confiurations like enabling clocks...etc if requried */
    if(sec_pil_prepare_subsys(proc) != E_SUCCESS)
    {
      SEC_PIL_ERROR_CODE(PIL_INIT_IMAGE_PREPARE_SUBSYS_FAILED);
      ret = -E_FAILURE;
      break;
    }
  } while (0);

  if(ret == E_SUCCESS)
  {
    s_pil_info[proc].state = SEC_PIL_STATE_RESET;
  }
  else
  {
    sec_pil_clean_pil_priv(&s_pil_info[proc],&s_pil_seg_info[proc]);
  }
  
  return ret;
}

/**
 * @brief Validates ELF segments against hash table and reset peripheral 
 *
 * @param[in] proc  The peripheral image
 * @param[in] reset Brings up the subsytem if the reset is TRUE.
 *
 * @return 0 on success, negative on failure
 */
int sec_pil_auth_reset(sec_pil_proc_et proc,boolean reset)
{
  int ret = E_SUCCESS;  
  #ifdef FEATURE_NON_MSA_AUTH
  uint32 code_seg = 0;
  uint32 auth_enabled = FALSE;
  #endif
  if (!sec_pil_is_proc_supported(proc))
  {
    SEC_PIL_ERROR_CODE(PIL_AUTH_RESET_UNSUPPORTED_PROC);
    return -E_INVALID_ARG;
  }  
#ifdef FEATURE_NON_MSA_AUTH
  /* checking for secboot fuse */
  if(sec_pil_get_code_segment(proc,&code_seg) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_GET_CODE_SEGMENT_FAILED);
    return -E_FAILURE;
  }
  
  if(sec_pil_hw_is_auth_enabled(code_seg,&auth_enabled) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_VERIFY_SIG_HW_IS_AUTH_ENABLED_FAILED);
    return -E_FAILURE;
  }

  if(!auth_enabled)
  {
    if (sec_pil_lock_xpu(proc, s_pil_info[proc].elf.prog_hdr,
      s_pil_info[proc].elf.prog_hdr_num, s_pil_info[proc].elf.elf_hdr->e_entry) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_AUTH_SEGMENTS_LOCK_XPU_FAILED);
    return -E_FAILURE;
  }

    return E_SUCCESS;
  }
#endif  
  do
  {
    if (s_pil_info[proc].state != SEC_PIL_STATE_RESET)
    {
      SEC_PIL_ERROR_CODE(PIL_AUTH_RESET_PROC_NOT_IN_RESET);
      ret = -E_FAILURE;
      break;
    }

    if (E_SUCCESS != sec_pil_adjust_elf_addrs(proc))
    {
      SEC_PIL_ERROR_CODE(PIL_AUTH_RESET_AUTH_SEGMENTS_FAILED);
      ret = -E_FAILURE;
      break;
    }

    if (sec_pil_auth_segments(proc, &s_pil_info[proc].elf) != E_SUCCESS)
    {
      SEC_PIL_ERROR_CODE(PIL_AUTH_RESET_AUTH_SEGMENTS_FAILED);
      ret = -E_FAILURE;
      break;
    }

    s_pil_info[proc].state = SEC_PIL_STATE_DONE;

    if(reset)
    {
      /* Call the processor reset routine */
      if(E_SUCCESS != sec_pil_subsys_bring_up((sec_pil_proc_et)proc,
         s_pil_info[proc].elf.elf_hdr->e_entry))
      {
        SEC_PIL_ERROR_CODE(PIL_AUTH_RESET_SUBSYS_BRING_UP_FAILED);
        ret = -E_FAILURE;
        break;
      }
    }
  } while (0);
      
  return ret;
}

/**
 * @brief The MBA allows for segmented Primary Modem Image Authentication.
 *        This allows for parallelization of loading and authenticating images.
 *        Benefits of such loading can decrease the overall hash verification 
 *        time by allowing one master to load the image from flash into RAM, 
 *        while the crypto engine performs SHA updates of a running hash in MBA.
 *
 * @param[in] proc              The peripheral image
 * @param[in] image_start_addr  The physical start address of the ELF Program Segments
 * @param[in] length_loaded     Length of the image loaded so far.
 *
 * @return 0 on success, negative on failure.
 */
int sec_pil_verify_segments(sec_pil_proc_et proc,
                            uint32 image_start_addr,
                            uint32 length_loaded,
                            uint32 total_image_length)
{

  uint32 len_tb_read; /* length to be read */
  uint32 end_addr;
  static uint32 resource;
  
  if (!sec_pil_is_proc_supported(proc))
  {
    SEC_PIL_ERROR_CODE(PIL_VERIFY_SEGMENTS_UNSUPPORTED_PROC);
    return -E_INVALID_ARG;
  }
 
  if (s_pil_info[proc].state != SEC_PIL_STATE_RESET)
  {
    SEC_PIL_ERROR_CODE(PIL_VERIFY_SEGMENTS_PIL_STATE_NOT_IN_RESET);
    return -E_FAILURE;    
  }

   if (E_SUCCESS != sec_pil_adjust_elf_addrs(proc))
   {
   	     SEC_PIL_ERROR_CODE(PIL_AUTH_RESET_AUTH_SEGMENTS_FAILED);
   	     return -E_FAILURE;
   }
 
  /* Initialisation */
  if(!(s_pil_seg_info[proc].init))
  {
    if(sec_pil_seg_auth_init(proc,image_start_addr,&s_pil_info[proc],&s_pil_seg_info[proc]) != E_SUCCESS)
    {
      SEC_PIL_ERROR_CODE(PIL_VERIFY_SEGMENTS_AUTH_INIT_FAILED);
      return -E_FAILURE;
    }
  }

  /* Calculate the length to be read */
  if(!s_pil_seg_info[proc].len_read)
  {
    len_tb_read = length_loaded;    
  }
  else
  {
    if(length_loaded < s_pil_seg_info[proc].len_read)
    {
      /* Something went wrong.length_loaded is total length loaded so far.
         So it can not be less than the length of data already read.*/
      SEC_PIL_ERROR_CODE(PIL_VERIFY_SEGMENTS_ERROR);
      return -E_FAILURE;      
    }
    len_tb_read = length_loaded - s_pil_seg_info[proc].len_read;    
  }
  
  /* Call XPU APIs here. if len_tb_read is not zero */
  if(len_tb_read)
  { 
    /* Compute the end address for applying XPU as length loaded does not cover the
       alignment gaps between ELF prgram segments.*/
    if (sec_pil_get_end_addr(length_loaded, &s_pil_info[proc], &end_addr) != E_SUCCESS)
    {
      SEC_PIL_ERROR_CODE(PIL_VERIFY_SEGMENTS_GET_END_ADDR_FAILED);
      return -E_FAILURE;
    }
    
    if (sec_pil_mpu_lock_area(&resource,image_start_addr,end_addr) != E_SUCCESS)
    {
      SEC_PIL_ERROR_CODE(PIL_VERIFY_SEGMENTS_MPU_LOCK_FAILED);
      return -E_FAILURE;
    } 
    
    if(sec_pil_auth_partial_segments(proc,len_tb_read,&s_pil_info[proc],&s_pil_seg_info[proc])< 0)
    {
      SEC_PIL_ERROR_CODE(PIL_VERIFY_SEGMENTS_AUTH_PARTIAL_SEGMENTS_FAILED);
      sec_pil_mpu_unlock_area(resource);      
      return -E_FAILURE;
    }
  }  
  /* Change the proc state when full image verified & all hash data compared */
  if(total_image_length == length_loaded)
  {
    if(s_pil_seg_info[proc].hash_len == 0)
    {
      s_pil_info[proc].state = SEC_PIL_STATE_DONE;
    }
    else
    {
      sec_pil_mpu_unlock_area(resource);
      SEC_PIL_ERROR_CODE(PIL_VERIFY_SEGMENTS_HASH_PENDING);
      return -E_FAILURE;
    }
  }
  return E_SUCCESS;
}
   


/**
 * @brief Gets the image entry's point.
 *
 * @param[in]  proc         The peripheral image
 * @param[out] image_entry  Holds the image entry point.
 *
 * @return 0 on success, negative on failure.
 */
 
int sec_pil_get_entry (sec_pil_proc_et proc, uint32 *image_entry)
{
  if (!sec_pil_is_proc_supported(proc))
  {
    SEC_PIL_ERROR_CODE(PIL_GET_ENTRY_UNSUPPORTED_PROC);
    return -E_INVALID_ARG;
  }

  *image_entry = s_pil_info[proc].elf.elf_hdr->e_entry;
  
  return E_SUCCESS;
}

/**
 * @brief sum up the size for all segments
 *
 * @param[in]  proc          The peripheral image
 * @param[out] image_length  Holds the image length.
 *
 * @return 0 on success, negative on failure.
 */

int sec_pil_get_image_length(sec_pil_proc_et proc, uint32 *image_length)
{
  uint32 i = 0;
  uint32 length = 0;
  
  if (!sec_pil_is_proc_supported(proc))
  {
    SEC_PIL_ERROR_CODE(PIL_GET_IMAGE_LENGTH_UNSUPPORTED_PROC);
    return -E_INVALID_ARG;
  }  

  for(i=0; i < s_pil_info[proc].elf.prog_hdr_num; ++i)
  {
    if(sec_pil_is_valid_segment(&s_pil_info[proc].elf.prog_hdr[i]))
    {
      if ( (length + (s_pil_info[proc].elf.prog_hdr[i].p_memsz) ) < length )
      {
        SEC_PIL_ERROR_CODE(AUTH_ELF_HEADERS_PROG_HDR_NUM_OVERFLOW);
        return -E_INVALID_ARG;
      }
      length += s_pil_info[proc].elf.prog_hdr[i].p_memsz;
    }
  }

  /* Return length */
  *image_length = length;

  return E_SUCCESS;
}

/**
 * @brief Gets the meta data length from ELF and program headers.
 *        Meta data = size of ELF header+size of proram header+size of hash segment.
 *
 * @param[in]      proc               The peripheral image
 * @param[in]      elf_hdr            Pointer to the ELF header
 * @param[in,out]  meta_data_length   Holds the meta data length loaded for input
 *                                    and holds the actual meta data length required. 
 *
 * @return 0 on success, negative on failure.
 */
int sec_pil_get_meta_data_length(sec_pil_proc_et proc,
                                 const Elf32_Ehdr * elf_hdr,
                                 uint32 *meta_data_length)
{   
  Elf32_Phdr  *prog_hdr = NULL,*prog_hdr_hash = NULL;
  uint32 prog_tbl_sz = 0;
  uint32 elf_hdr_sz = 0;
  uint32 hash_seg_sz = 0;
  
  if (proc >= SEC_PIL_AUTH_UNSUPPORTED_PROC)
  {
    SEC_PIL_ERROR_CODE(GET_META_DATA_LENGTH_UNSUPPORTED_PROC);
    return -E_INVALID_ARG;
  }

  elf_hdr_sz = (uint32)(sizeof(Elf32_Ehdr));
  prog_tbl_sz = (uint32)(sizeof(Elf32_Phdr)) * (elf_hdr->e_phnum);

  /* Ensure that all the program headers are loaded as we traverse the program
     headers to find the program header for Hash table segment */
  if((elf_hdr_sz + prog_tbl_sz) > *meta_data_length)
  {
    SEC_PIL_ERROR_CODE(GET_META_DATA_INSUFFICIENT_MEMORY);
    return -E_FAILURE;
  }
  prog_hdr = (Elf32_Phdr*)((uint8*)elf_hdr + sizeof(Elf32_Ehdr));  

  /* Size of the hash table segment - Find the program header for
   * the segment of type MI_PBT_HASH_SEGMENT, and get the size
   * from that entry
   */
  if ((prog_hdr_hash = sec_pil_elf_find_seg(prog_hdr,
                                            (Elf32_Phdr*)((uint8*)prog_hdr + prog_tbl_sz),
                                            MI_PBT_HASH_SEGMENT)) == NULL)
  {
    SEC_PIL_ERROR_CODE(GET_META_DATA_NO_PROG_HDR);
    return -E_FAILURE;
  }

  hash_seg_sz = prog_hdr_hash->p_filesz;

  /* Return the actual meta data length */
  *meta_data_length = elf_hdr_sz + prog_tbl_sz + hash_seg_sz;

  return E_SUCCESS;
}


/**
 * @brief Gets the image length in memory.
 *
 * @param[in]  proc          The peripheral image
 * @param[out] image_length  Holds the image length.
 *
 * @return 0 on success, negative on failure.
 */
int sec_pil_get_image_length_in_memory(sec_pil_proc_et proc, uint32 *image_length)
{
  uint32 i = 0;  
  uint32 start_addr =0, end_addr=0;
  uint32 range_intialised = 0;
  Elf32_Phdr  *prog_hdr;
  
  if (proc >= SEC_PIL_AUTH_UNSUPPORTED_PROC)
  {
    SEC_PIL_ERROR_CODE(GET_IMAGE_LENGTH_IN_MEMORY_UNSUPPORTED_PROC);
    return -E_INVALID_ARG;
  }

  prog_hdr = s_pil_info[proc].elf.prog_hdr;
  
  /* All image segments must be within the image area (either EBI or internal
   * memory). */
  for(i = 0; i < s_pil_info[proc].elf.prog_hdr_num; i++, prog_hdr++)
  {
    /* If Elf Segment is a loadable segment */
    if((sec_pil_is_valid_segment(prog_hdr) ||
     (MI_PBT_ACCESS_TYPE_VALUE(prog_hdr->p_flags) == MI_PBT_ZI_SEGMENT)) && (prog_hdr->p_memsz != 0))
    {
      if((start_addr > prog_hdr->p_paddr)||(range_intialised == 0))
        start_addr = prog_hdr->p_paddr;
 
      if((end_addr < prog_hdr->p_paddr)||(range_intialised == 0))
        end_addr = prog_hdr->p_paddr + prog_hdr->p_memsz;
      
      range_intialised = 1;
    }
  }

  if(end_addr < start_addr)
  {
    SEC_PIL_ERROR_CODE(GET_IMAGE_LENGTH_IN_MEMORY_ERROR);
    return -E_FAILURE;
  }
  /* Return length */
  *image_length = end_addr - start_addr;

  return E_SUCCESS;
}


/**
 * @brief Check for loadable, non paged segments
 *
 * @param[in] Program hdr entry for the segment
 *
 * @result \c TRUE if the segment is valid, \c FALSE otherwise.
 */
boolean sec_pil_is_valid_segment(const Elf32_Phdr* entry)
{
  /* Valid hashed segments are loadable */
  if(entry->p_type == ELF_PT_LOAD)
  {
    if ((MI_PBT_PAGE_MODE_VALUE(entry->p_flags) ==
         MI_PBT_NON_PAGED_SEGMENT) &&
        (MI_PBT_SEGMENT_TYPE_VALUE(entry->p_flags) !=
          MI_PBT_HASH_SEGMENT) &&
        (MI_PBT_ACCESS_TYPE_VALUE(entry->p_flags) !=
          MI_PBT_NOTUSED_SEGMENT) &&
        (MI_PBT_ACCESS_TYPE_VALUE(entry->p_flags) !=
          MI_PBT_SHARED_SEGMENT))
    {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * @brief Find the segment of the given p_flags
 *
 * @param[in] phdr The program header entry start ptr
 * @param[in] end The program header entry end ptr
 * @param[in] seg_flags The p_flags of a given segment
 *
 * @return Valid program header pointer if found, \c NULL otherwise.
 */
Elf32_Phdr *sec_pil_elf_find_seg(const Elf32_Phdr * phdr, const Elf32_Phdr * end,
                                 uint32 seg_flags)
{
  Elf32_Phdr *cur= (Elf32_Phdr *) phdr;

  if (phdr == NULL || end == NULL)
    return NULL;

  while ((uint32*)cur < (uint32*)end)
  {
    if (MI_PBT_SEGMENT_TYPE_VALUE(cur->p_flags) == seg_flags)
    {
      return cur;
    }
    cur++;
  }
  return NULL;
}


/**
 * @brief If the image was relocated by the HLOS, the addresses of the segments
 *        need to be updated to reflect the new location of the segments
 *
 * @param[in]   proc    The peripheral image
 *
 * @return \c E_SUCCESS on success, return code of 
 *         @see tzbsp_subsys_set_memory_range otherwise
 */
int sec_pil_adjust_elf_addrs(sec_pil_proc_et proc)
{
  uint32 start_addr = 0, new_start = 0, diff;
  int err = E_SUCCESS;
  uint32 i = 0;
  uint32 mem_range_initialized = 0;

  /* find the smallest start address of segments for the start address of the modem image. */
  	for (i=0; i < s_pil_info[proc].elf.prog_hdr_num; ++i)
    {
    
      if((sec_pil_is_valid_segment(&s_pil_info[proc].elf.prog_hdr[i]) ||
       (MI_PBT_ACCESS_TYPE_VALUE(s_pil_info[proc].elf.prog_hdr[i].p_flags) == MI_PBT_ZI_SEGMENT))
	   && (s_pil_info[proc].elf.prog_hdr[i].p_memsz != 0))
	  { 
	     if((start_addr > s_pil_info[proc].elf.prog_hdr[i].p_paddr)||
		    (mem_range_initialized == 0))
		 {
            start_addr = s_pil_info[proc].elf.prog_hdr[i].p_paddr;
       
	        mem_range_initialized = 1;
	     }
	  }
    }

    err = sec_pil_get_load_addr (proc, &new_start);	

   /* If this new start addresses matches the one from the ELF, then
    * nothing to be done */
    if(new_start != start_addr)
    {      

    /* Adjustment required */
	if (new_start> start_addr)
	{
      diff = new_start - start_addr;

      /* Entry point and each loadable segment must be adjusted */
      if (((s_pil_info[proc].elf.elf_hdr->e_entry) + diff) 
                    < (s_pil_info[proc].elf.elf_hdr->e_entry))
      {
        return -E_DATA_INVALID;
      }
      s_pil_info[proc].elf.elf_hdr->e_entry += diff;

      for (i=0; i < s_pil_info[proc].elf.prog_hdr_num; i++)
      {
    
       if((sec_pil_is_valid_segment(&s_pil_info[proc].elf.prog_hdr[i]) ||
       (MI_PBT_ACCESS_TYPE_VALUE(s_pil_info[proc].elf.prog_hdr[i].p_flags) == MI_PBT_ZI_SEGMENT))
	   && (s_pil_info[proc].elf.prog_hdr[i].p_memsz != 0))
      {
        if (((s_pil_info[proc].elf.prog_hdr[i].p_paddr) + diff) 
                      < (s_pil_info[proc].elf.prog_hdr[i].p_paddr))
        {
          return -E_DATA_INVALID;
        }
         s_pil_info[proc].elf.prog_hdr[i].p_paddr += diff;
      }
    } 

	}
	else
	{
	   diff = start_addr - new_start;

       /* Entry point and each loadable segment must be adjusted */
       if (((s_pil_info[proc].elf.elf_hdr->e_entry) - diff) 
                     > (s_pil_info[proc].elf.elf_hdr->e_entry))
       {
         return -E_DATA_INVALID;
       }
       s_pil_info[proc].elf.elf_hdr->e_entry -= diff;
	   
	   for (i=0; i < s_pil_info[proc].elf.prog_hdr_num; ++i)
      {
    
         if((sec_pil_is_valid_segment(&s_pil_info[proc].elf.prog_hdr[i]) ||
       (MI_PBT_ACCESS_TYPE_VALUE(s_pil_info[proc].elf.prog_hdr[i].p_flags) == MI_PBT_ZI_SEGMENT))
	   && (s_pil_info[proc].elf.prog_hdr[i].p_memsz != 0))
        {
          if (((s_pil_info[proc].elf.prog_hdr[i].p_paddr) - diff) 
                        > (s_pil_info[proc].elf.prog_hdr[i].p_paddr))
          {
            return -E_DATA_INVALID;
          }
           s_pil_info[proc].elf.prog_hdr[i].p_paddr -= diff;
        }
 	  }
   }
  }
  return err;
}

/**
 * @brief Find the segment of the given p_flags
 *
 * @param[in] phdr The program header entry start ptr
 * @param[in] end The program header entry end ptr
 * @param[in] seg_flags The p_flags of a given segment
 *
 * @return Valid program header pointer if found, \c NULL otherwise.
 */
int sec_pil_auth_validate_hash_segment_entries
(
  const Elf32_Phdr * phdr, 
  const Elf32_Phdr * end,
  uint32 seg_flags
)
{
  uint32 count = 0;
  Elf32_Phdr *cur= (Elf32_Phdr *) phdr;
  int ret = -E_FAILURE;  

  if (phdr == NULL || end == NULL)
    return ret;

  while (cur < end)
  {
    if (MI_PBT_SEGMENT_TYPE_VALUE(cur->p_flags) == seg_flags)
    {
      count++;
    }
    cur++;
  }
  if( count == 1)
  {
    ret = E_SUCCESS;
  }
  else
  {
	ret = -E_FAILURE;
  }
  return ret;
}





