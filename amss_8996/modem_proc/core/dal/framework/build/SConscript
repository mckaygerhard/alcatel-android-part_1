#===============================================================================
#
# DAL Framework
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009 - 2011 Qualcomm Incorporated.
# All Rights Reserved.
# Qualcomm Confidential and Proprietary
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.4.c3.11/dal/framework/build/SConscript#1 $
#  $DateTime: 2016/03/28 23:02:17 $
#  $Author: mplcsds1 $
#  $Change: 10156097 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 4/16/13    sho     Move all source code unrelated to device management out
#
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${DAL_ROOT}/framework/src"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'DAL',
   'MPROC',
   'SERVICES',
   'SYSTEMDRIVERS',
   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

#-------------------------------------------------------------------------------
# GuestOS Source Code
#-------------------------------------------------------------------------------
GUESTOS_SOURCES =  [
   '${BUILDPATH}/common/dalfwkbase.c',
   '${BUILDPATH}/common/DALDevice.c',
   '${BUILDPATH}/common/DALMemDesc.c',
   '${BUILDPATH}/common/DALQueue.c',
   ]

if env.GetUsesFlag('USES_MBA') is True:
   if env.GetUsesFlag('USES_QPIC_NAND') is True:
      env.AddLibrary(['MBA_CORE_SW'], '${BUILDPATH}/DALFwk', GUESTOS_SOURCES)
elif env.GetUsesFlag('USES_MPSS_MULTI_PD') is True:
   if env.GetUsesFlag('USES_USER_PD') is True:
      env.Append(CPPDEFINES = ["FEATURE_DAL_REMOTE_CLIENT"])
   env.Append(CPPDEFINES = ["FEATURE_DAL_REMOTE"])
   GUESTOS_SOURCES.extend([
      '${BUILDPATH}/qdi/DALQdiQurtOS.c',
      '${BUILDPATH}/qdi/DALQdiRcvr.S'])

env.AddLibrary(
   ['SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE', 'MODEM_IMAGE', 'CBSP_MODEM_IMAGE',
    'APPS_IMAGE', 'CBSP_APPS_IMAGE', 'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE'],
    '${BUILDPATH}/DALFwk', GUESTOS_SOURCES)

#-------------------------------------------------------------------------------
# User Source Code
#-------------------------------------------------------------------------------
USER_SOURCES =  [
   '${BUILDPATH}/common/dalfwkbase.c',
   '${BUILDPATH}/common/DALQueue.c',
   '${BUILDPATH}/common/DALDevice.c',
   '${BUILDPATH}/qdi/DALQdiFwdr.S',
   '${BUILDPATH}/qdi/DALQdiUser.c',
   ]

env.AddLibrary(['CORE_USER_PD'], '${BUILDPATH}/DALFwk_user', USER_SOURCES)

#-------------------------------------------------------------------------------
# Load Subunits (sys)
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()
