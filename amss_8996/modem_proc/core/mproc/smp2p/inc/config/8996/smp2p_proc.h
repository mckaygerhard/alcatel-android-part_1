#ifndef SMP2P_PROC_H
#define SMP2P_PROC_H

/*---------------------------------------------------------------------------
          Shared Memory State Manager Processor-Specific Definitions
---------------------------------------------------------------------------*/
/*!
  @file
    SMP2P_proc.h

  @brief
    This file contains the SMP2P processor-specific definitions.
*/

/*--------------------------------------------------------------------------
     Copyright  2012-2013 Qualcomm Technologies Incorporated.
     All rights reserved.
---------------------------------------------------------------------------*/
/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.4.c3.11/mproc/smp2p/inc/config/8996/smp2p_proc.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/24/14   bc      Adding sensor processor support 
09/25/13   bt      Change ASSERT_BRK to ASSERT by default.
09/16/13   pa/bt   Add RESET_LOCAL state and reset handshake.
08/19/13   pa      Added SMP2P_LOG_EX.
10/25/12   pa      Created processor-specific file for B-family MPSS.
===========================================================================*/

/*===========================================================================

                        INCLUDE FILES

===========================================================================*/
#include "DDIIPCInt.h"
#include "assert.h"

#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================
                        MACRO DEFINITIONS
===========================================================================*/
#ifdef FEATURE_SMP2P_ASSERT_BRK
/*  To enable this, define FEATURE_SMP2P_ASSERT_BRK, or just change the above
 *  line to #if 1. */
/** Breakpoint assert.  Halts the processor at the assert location rather
 *  than going through the error handling code.
 *  Useful for bringup of new code.
 */
#define ASSERT_BRK(cond) if (!(cond)) { \
  __asm__ __volatile__ ( \
  "brkpt \n" \
  : \
  : \
  :"memory"); }
#else
#define ASSERT_BRK  ASSERT
#endif

/** \name SMP2P Host
 *
 * This macro defines the local processor in which SMP2P runs.
 *
 * @{
 */
#define SMP2P_THIS_HOST        SMEM_MODEM
/*@}*/

/* Feature flags (smp2p_header_version_type.flags, 24 bits) */
#define SMP2P_FFLAGS_SSR_HANDSHAKE      0x000001

#define SMP2P_FFLAGS_SUPPORTED  (SMP2P_FFLAGS_SSR_HANDSHAKE)
#define SMP2P_FFLAGS_REQUESTED  (SMP2P_FFLAGS_SSR_HANDSHAKE)

/* Max entries */
#define SMP2P_ENTRIES_TOTAL   16

/** SMP2P log macro.
 *  This macro defines the log routines used by SMP2P to track events that
 *  occur at runtime.
 *  This is defined in the processor specific header file so that each
 *  processor may define it's own policy. */
#define SMP2P_LOG(a,b,c,d)      smp2p_os_log(a,b,c,d, SMP2P_FLAGS_NONE)

/** SMP2P log macro with flags */
#define SMP2P_LOG_EX(a,b,c,d,e) smp2p_os_log(a,b,c,d,e)

/** Defines the number of entries in the SMP2P local log */
#define SMP2P_LOG_NUM_ENTRIES 64

/** This define, if present, enables local logging */
#define SMP2P_LOG_LOCAL

/** This define, if present, enables logging to SMEM log */
/* #define SMP2P_LOG_SMEM */

/*-------------------------------------------------------------------------
  Interrupt Defines
-------------------------------------------------------------------------*/
/** NULL Interrupt definitions */
#define SMP2P_NULL_IN_INTERRUPT  ( DALIPCINT_INT_32BITS )
#define SMP2P_NULL_OUT_INTERRUPT SMP2P_NULL_IN_INTERRUPT

/** \name SMP2P Target Modem
 *
 * These values represent the target specific IRQs and ISRs supported.
 *
 * @{
 */
#define SMP2P_APPS_IN          38
#define SMP2P_APPS_OUT         DALIPCINT_GP_2
#define SMP2P_APPS_IPCPROC     DALIPCINT_PROC_ACPU

/** No interrupts from/to self. */
#define SMP2P_MPSS_IN          SMP2P_NULL_IN_INTERRUPT
#define SMP2P_MPSS_OUT         SMP2P_NULL_OUT_INTERRUPT
#define SMP2P_MPSS_OUT_M       0 /* Not used with DAL. */
#define SMP2P_MPSS_IPCPROC     DALIPCINT_PROC_NULL

#define SMP2P_ADSP_IN          42
#define SMP2P_ADSP_OUT         DALIPCINT_GP_2
#define SMP2P_ADSP_OUT_M       0
#define SMP2P_ADSP_IPCPROC     DALIPCINT_PROC_ADSP

/** No SMSM interrupts from/to WCNSS implemented. */
#define SMP2P_WCNSS_IN         50
#define SMP2P_WCNSS_OUT        DALIPCINT_GP_2
#define SMP2P_WCNSS_OUT_M      0
#define SMP2P_WCNSS_IPCPROC    DALIPCINT_PROC_WCN

/** Currently from sensor to Modem interrupt is not showing in ipcatalog */
#define SMP2P_SSC_IN           58
#define SMP2P_SSC_OUT          DALIPCINT_GP_2
#define SMP2P_SSC_OUT_M        0
#define SMP2P_SSC_IPCPROC      DALIPCINT_PROC_SPS

/** No interrupts from/to Q6FW. */
#define SMP2P_Q6FW_IN          SMP2P_NULL_IN_INTERRUPT
#define SMP2P_Q6FW_OUT         SMP2P_NULL_OUT_INTERRUPT
#define SMP2P_Q6FW_OUT_M       0
#define SMP2P_Q6FW_IPCPROC     DALIPCINT_PROC_NULL

/** No interrupts from/to RPM. */
#define SMP2P_RPM_IN           SMP2P_NULL_IN_INTERRUPT
#define SMP2P_RPM_OUT          SMP2P_NULL_OUT_INTERRUPT
#define SMP2P_RPM_OUT_M        0
#define SMP2P_RPM_IPCPROC      DALIPCINT_PROC_NULL

/** Interrupts from/to TZ */
#define SMP2P_TZ_IN            37
#define SMP2P_TZ_OUT           DALIPCINT_GP_1
#define SMP2P_TZ_OUT_M         0
#define SMP2P_TZ_IPCPROC       DALIPCINT_PROC_ACPU
/*@}*/

/*===========================================================================
                        TYPE DEFINITIONS
===========================================================================*/
/** OS abstraction over DAL types */
typedef DalIPCIntProcessorType smp2p_os_proc_type;
typedef DalIPCIntInterruptType smp2p_os_intr_type;

#ifdef __cplusplus
}
#endif

#endif  /* SMP2P_PROC_H */
