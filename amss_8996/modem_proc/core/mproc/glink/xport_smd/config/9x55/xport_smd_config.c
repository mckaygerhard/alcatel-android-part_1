/*===========================================================================

            GLink SMD transport 9x55 MPSS Configuration Structures

=============================================================================

  @file
    xport_smd_config.c

    Contains structures to be used in Glink SMD trasnport configuration.

  Copyright (c) 2014-2015 Qualcomm Technologies Incorporated. 
  All rights reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.4.c3.11/mproc/glink/xport_smd/config/9x55/xport_smd_config.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/25/15   db      Modified config for 9x55 modem
08/06/14   bm      Initial version for 8996 modem
===========================================================================*/

/*===========================================================================
                        INCLUDE FILES
===========================================================================*/
#include "xport_smd_config.h"

/*===========================================================================
                        DATA DECLARATIONS
===========================================================================*/
const xport_smd_config_type xport_smd_config[] =
{
  /* modem->Apss */
  {
    "apss",                /* Remote host name        */
    SMD_APPS_MODEM         /* SMD compatible edge definition */
  },

  /* modem->rpm */
  {
    "rpm",                 /* Remote host name        */
    SMD_MODEM_RPM          /* SMD compatible edge definition */
  }   
};

const uint32 xport_smd_config_num = sizeof(xport_smd_config)/sizeof(xport_smd_config[0]);
const smem_host_type xport_smd_this_host = SMEM_MODEM;


const xport_smd_config_type* xport_smd_get_config(uint32 ind)
{
  if (ind >= xport_smd_config_num)
  {
    return NULL;
  }

  return &xport_smd_config[ind];
}
