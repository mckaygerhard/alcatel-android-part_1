#===============================================================================
#
# ERR INJECT Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2014 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.4.c3.11/debugtools/eic/build/err_inject.scons#1 $
#  $DateTime: 2016/03/28 23:02:17 $
#  $Author: mplcsds1 $
#  $Change: 10156097 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 11/04/14   dav     Created separate folder for ERR Inject.
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# SUPPORTED IMAGES
#-------------------------------------------------------------------------------



CRASH_INJECT_IMGS = [
    'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE','CORE_ROOT_PD', 
]

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/debugtools/eic/src"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Features and Definitions
#-------------------------------------------------------------------------------
env.Append(CPPDEFINES=['ERR_IMG_MPSS'])
env.Append(CPPDEFINES=['ERR_HW_QDSP6'])
env.Append(CPPDEFINES=['ERR_OS_QURT'])

#check for missing dependencies
if 'USES_ERR_INJECT_CRASH' in env:
    env.Append(CPPDEFINES=['ERR_INJECT_CRASH'])

if 'USES_DEVCFG' in env:
    env.Append(CPPDEFINES=['ERR_USES_DEVCFG'])

#-------------------------------------------------------------------------------
# External depends outside CoreBSP
#-------------------------------------------------------------------------------

env.RequireExternalApi([
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------

CBSP_API = [
   'DAL',
   'DEBUGTOOLS',
   'SERVICES',
   'SYSTEMDRIVERS',

   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

#-------------------------------------------------------------------------------
# Sources
#-------------------------------------------------------------------------------
#Some sections meant for Uerr, Should be explicitly added for island mode
ERR_INJECT_CRASH_SECTION_SOURCES = [
   '${BUILDPATH}/err_inject_crash.c'
   ]

ERR_INJECT_CRASH_SOURCES = [
   '${BUILDPATH}/err_inject_crash.c',
]


#-------------------------------------------------------------------------------
# Libraries
#-------------------------------------------------------------------------------

if 'USES_ERR_INJECT_CRASH' in env:
    env.AddLibrary(
        CRASH_INJECT_IMGS,
        '${BUILDPATH}/err_inject_crash', ERR_INJECT_CRASH_SOURCES )

if 'USES_ERR_INJECT_CRASH' in env:
    if 'USES_ISLAND' in env:
       err_inject_crash_section_objs = env.Object(ERR_INJECT_CRASH_SECTION_SOURCES)
       island_section = ['.text.uErr','.data.uErr']
       env.AddIslandObject(CRASH_INJECT_IMGS, err_inject_crash_section_objs, island_section )
       env.Append(CFLAGS = "-DUERR_ISLAND_MODE ")

# Devcfg Registrations
if 'USES_ERR_INJECT_CRASH' in env:
    if 'USES_DEVCFG' in env:
        DEVCFG_IMG = ['DAL_DEVCFG_IMG']
        env.AddDevCfgInfo(DEVCFG_IMG,
        {
         'soc_xml' : ['${BUILD_ROOT}/core/debugtools/eic/src/err_inject_crash.xml']
        })

if 'USES_ERR_INJECT_CRASH' in env:
  if 'USES_RCINIT' in env:
     env.AddRCInitFunc( 
      CRASH_INJECT_IMGS, 
      {
       'sequence_group'             : 'RCINIT_GROUP_2',                   # required
       'init_name'                  : 'err_inject_crash_init',            # required
       'init_function'              : 'err_inject_crash_init',            # required
      # 'dependencies'               : ['',]
      })

