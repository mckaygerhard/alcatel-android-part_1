# vi: tw=128 ts=3 sw=3 et :
# @file rcxh.scons
# @brief This file contains the API for the Run Control Framework, API 2.1
#===============================================================================
# NOTE: The @brief description above does not appear in the PDF.
# The tms_mainpage.dox file contains the group/module descriptions that
# are displayed in the output PDF generated using Doxygen and LaTeX. To
# edit or update any of the group/module text in the PDF, edit the
# tms_mainpage.dox file or contact Tech Pubs.
#===============================================================================
#===============================================================================
# Copyright (c) 2015 Qualcomm Technologies Incorporated.
# All Rights Reserved.
# Qualcomm Confidential and Proprietary.
#===============================================================================
#===============================================================================
# Edit History
# $Header: //components/rel/core.mpss/3.4.c3.11/debugtools/rcxh/build/rcxh.scons#1 $
# $DateTime: 2016/03/28 23:02:17 $
# $Change: 10156097 $
# $Author: mplcsds1 $
#===============================================================================

# Module Version, Python Standard PEP8, 2001, https://www.python.org/dev/peps/pep-0008/
__version__ = "$Header: //components/rel/core.mpss/3.4.c3.11/debugtools/rcxh/build/rcxh.scons#1 $"

# Shared Library Version, Semantic Versioning 2.0.0, http://semver.org/
__version_info__ = "2.1.0"

Import('env')

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------

env.VariantDir('${BUILDPATH}', '${BUILD_ROOT}/core/debugtools/rcxh/src', duplicate=0)

#-------------------------------------------------------------------------------
# External Dependency
#-------------------------------------------------------------------------------

#env.RequireExternalApi([
#   'XYZ',
#])

#-------------------------------------------------------------------------------
# Internal Dependency
#-------------------------------------------------------------------------------

env.RequirePublicApi([
   'DAL',
   'TMS',
   'DEBUGTOOLS',
   'DEBUGTRACE',
   'SERVICES',
   'SYSTEMDRIVERS',
   'KERNEL',   # needs to be last also contains wrong comdef.h
])

env.RequireRestrictedApi([
   'TMS_RESTRICTED',
   'DEBUGTOOLS',
])

#-------------------------------------------------------------------------------
# Sources
#-------------------------------------------------------------------------------

RCXH_COMMON = [
    '${BUILDPATH}/rcxh.c',
]

#-------------------------------------------------------------------------------
# Libraries
#-------------------------------------------------------------------------------

RCXH_IMG = [

   'CORE_APPS',                  # Domain(REX + TN)
   'CORE_EOS',                   # Domain(REX + EOS)
   'CORE_GSS',                   # Domain(REX + GSS)
   'CORE_RPM',                   # Domain(REX + RPM)
   'CORE_VPU',                   # Domain(NHLOS + VPU)
   'CORE_WCN',                   # Domain(REX + WCN)

   'CORE_ROOT_PD',               # Domain(Root)
   'CORE_USER_PD',               # Domain(User)

   'CORE_QDSP6_SW',              # Domain(QURTOS + Audio)
   'CORE_MODEM', 'MODEM_MODEM',  # Domain(QURTOS + Modem)

   'CORE_QDSP6_AUDIO_SW',        # Domain(User Audio)
   'CORE_QDSP6_MODEM_SW',        # Domain(User Modem)
   'CORE_QDSP6_SENSOR_SW',       # Domain(User Sensors)

]

env.AddLibrary(RCXH_IMG, '${BUILDPATH}/rcxh', RCXH_COMMON)

#-------------------------------------------------------------------------------
# Task Provisioning Information
#-------------------------------------------------------------------------------

# NONE

#-------------------------------------------------------------------------------
# Tracer Software Events
#-------------------------------------------------------------------------------

RCXH_SWE_EVENTS = [
   ['RCXH_SWE_TRY',            'RCXH Try             %x %x', 'T'],
   ['RCXH_SWE_CATCH',          'RCXH Catch           %x %x', 'T'],
   ['RCXH_SWE_CAUGHT',         'RCXH Caught          %x %x', 'T'],
   ['RCXH_SWE_FINALLY',        'RCXH Finally         %x %x', 'T'],
   ['RCXH_SWE_ENDTRY',         'RCXH EndTry          %x %x', 'T'],
   ['RCXH_SWE_THROW',          'RCXH Throw           %x %x', 'T'],
   ['RCXH_SWE_UNCAUGHT',       'RCXH Uncaught        %x %x', 'T'],
]

if 'USES_QDSS_SWE' in env and len(RCXH_SWE_EVENTS) != 0:
   env.Append(CPPDEFINES = ["RCXH_TRACER_SWEVT"])
   env.Append(CPPPATH = ['${BUILD_ROOT}/core/debugtools/rcxh/build/${BUILDPATH}'])
   env.SWEBuilder(['${BUILD_ROOT}/core/debugtools/rcxh/build/${BUILDPATH}/rcxh_tracer_swe.h'], None)
   env.AddSWEInfo(RCXH_IMG, RCXH_SWE_EVENTS)

#-------------------------------------------------------------------------------
# DALCFG Image "Static" Configuration Items
#-------------------------------------------------------------------------------

# NONE
