#ifndef SERVREG_QMI_NOTIFIER_SERVER_H
#define SERVREG_QMI_NOTIFIER_SERVER_H
/*
#============================================================================
#  Name:
#    servreg_qmi_notifier_server.h
#
#  Description:
#    Common Service Registry notifier header file for root images. This module serves as the end-point
#    of communication via QMI.
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/
#include "stdlib.h"
#include "qurt.h"
#include "servreg_common.h"

#if defined(__cplusplus)
extern "C"
{
#endif

/** =====================================================================
 * Function:
 *     servreg_get_qmi_notif_server_node
 *
 * Description:
 *     Checks if a SRM node already exists with the given name. If it does 
 *     exists it returns a pointer to that srm node.
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by just domain name.
 *
 * Returns:
 *    SERVREG_QDI_NOTIF_HANDLE : handle to the sr notif node
 * =====================================================================  */
//#if (defined(__GNUC__) && __GNUC__ >= 4) || defined(__clang__)
//__attribute__((nonnull(1)))
//#endif
//SERVREG_QDI_NOTIF_HANDLE servreg_get_qmi_notif_server_node(SERVREG_MON_HANDLE sr_mon_handle);

/** =====================================================================
 * Function:
 *     servreg_create_qmi_notif_server_node
 *
 * Description:
 *     Creates a srnotif node with the given sr_mon_handle
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by just domain name.
 *
 * Returns:
 *    SERVREG_QDI_NOTIF_HANDLE : handle to the sr notif node
 * =====================================================================  */
//#if (defined(__GNUC__) && __GNUC__ >= 4) || defined(__clang__)
//__attribute__((nonnull(1)))
//#endif
//SERVREG_QDI_NOTIF_HANDLE servreg_create_qmi_notif_server_node(SERVREG_MON_HANDLE sr_mon_handle);

/** =====================================================================
 * Function:
 *     servreg_delete_qmi_notif_server_node
 *
 * Description:
 *     Deletes a sr notif node given the sr notif handle
 *
 * Parameters:
 *     sr_qmi_notif_server_handle  : Handle to the notifier node to be deleted
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
//#if (defined(__GNUC__) && __GNUC__ >= 4) || defined(__clang__)
//__attribute__((nonnull(1)))
//#endif
//SERVREG_RESULT servreg_delete_qmi_notif_server_node(SERVREG_QDI_NOTIF_HANDLE sr_qmi_notif_server_handle);

/** =====================================================================
 * Thread :
 *     servreg_qmi_server_task
 *
 * Description:
 *     Service Registry QMI notifier server task
 *
 * Parameters:
 *    param: Task init parameter
 *
 * Returns:
 *     None
 * =====================================================================  */
//void servreg_qmi_server_task(dword param);

/** =====================================================================
 * Function:
 *     servreg_send_qmi_ind_msg_state_change
 *
 * Description:
 *     Check which notif node state has changed and return that notif node's handle
 *
 * Parameters:
 *     servreg_qmi_notif_server_list_head : Head pointer of the notifier handle list for the user PD.
 *
 * Returns:
 *    SERVREG_QMI_NOTIF_SERVER_HANDLE : handle to the sr notif node
 * =====================================================================  */
void servreg_send_qmi_ind_msg_state_change(void);

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_task_init
 *
 * Description:
 *     Initialization function for OS specific task to handle QMI request messages
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_qmi_notif_task_init(void);

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_server_init
 *
 * Description:
 *     Initialization function for the service registry QMI notifier module
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_qmi_notif_server_init(void);

#if defined(__cplusplus)
}
#endif

#endif