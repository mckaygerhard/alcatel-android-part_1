/*
#============================================================================
#  Name:
#    servreg_qmi_notifier_server.c
#
#  Description:
#    Service Registry notifier file for root image. This module serves as the end-point
#    of communication via QMI.
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/
#include "stdarg.h"
#include "stdlib.h"
#include <stringl/stringl.h>
#include "comdef.h"             /* Definitions for byte, word, etc.     */
#include "err.h"
#include "rex.h"
#include "msg.h"
#include "qmi_csi.h"
#include "qmi_csi_target_ext.h"
#include "rcinit.h"

#include "servreg_internal.h"
#include "servreg_locator.h"
#include "servreg_localdb.h"
#include "servreg_monitor.h"
#include "servreg_notifier.h"
#include "servreg_qmi_notifier_server.h"
#include "servreg_qmi_notifier_client.h"

#define SERVREG_QMI_NOTIF_SERVER_WAIT_MASK                0x20

extern qmi_csi_service_handle servreg_qmi_server_init(qmi_csi_os_params *os_params);

/** =====================================================================
 * Thread: REX
 *     servreg_qmi_server_req_task
 *
 * Description:
 *     Service Registry QMI notifier server task
 *
 * Parameters:
 *    param: Task init parameter
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_qmi_server_req_task(uint32 handle)
{
   uint32_t q_mask;
   qmi_csi_os_params os_params;
   qmi_csi_service_handle servreg_qmi_server_handle;

   os_params.tcb = rex_self();
   os_params.sig = SERVREG_QMI_NOTIF_SERVER_WAIT_MASK;

   servreg_qmi_server_handle = servreg_qmi_server_init(&os_params);
   servreg_qmi_notif_client_init();

   /* Block for start signal */
   rcinit_handshake_startup();    

   /* Task forever loop */
   for (;;)
   {
      q_mask = rex_wait(SERVREG_QMI_NOTIF_SERVER_WAIT_MASK );
      if (q_mask & SERVREG_QMI_NOTIF_SERVER_WAIT_MASK)
      {
         rex_clr_sigs(rex_self(), SERVREG_QMI_NOTIF_SERVER_WAIT_MASK);
         qmi_csi_handle_event(servreg_qmi_server_handle, &os_params);
      }
   }
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_task_init
 *
 * Description:
 *     Initialization function for OS specific task to handle QMI request messages
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_qmi_notif_task_init(void)
{
   char task_name[] = SERVREG_QMI_NOTIF_REQ_SERVER_TASK_NAME;
   static rex_tcb_type servreg_rex_tcb;
   static rex_task_attributes_type rex_attr;
   static char sr_rex_client_stack[SERVREG_QMI_NOTIF_SERVER_TASK_STACK];

   /* Rex task to handle QMI req messages */
   rex_task_attr_init(&rex_attr);
   rex_task_attr_set_stack_addr(&rex_attr, (rex_stack_pointer_type)sr_rex_client_stack);
   rex_task_attr_set_stack_size(&rex_attr, SERVREG_QMI_NOTIF_SERVER_TASK_STACK);
   rex_task_attr_set_prio(&rex_attr, 10);
   rex_task_attr_set_entry_func(&rex_attr, servreg_qmi_server_req_task, NULL);
   rex_task_attr_set_initially_suspended(&rex_attr, FALSE);
   rex_task_attr_set_task_name(&rex_attr, task_name);
   rex_common_def_task(&servreg_rex_tcb, &rex_attr);

   return;
}
