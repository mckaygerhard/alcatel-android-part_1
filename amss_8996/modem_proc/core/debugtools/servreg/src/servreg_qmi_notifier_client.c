/*
#============================================================================
#  Name:
#    servreg_qmi_notifier_client.c
#
#  Description:
#    Service Registry notifier file for root image. This module serves as the end-point
#    of communication via QMI.
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/
#include "stdarg.h"
#include "stdlib.h"
#include <stringl/stringl.h>
#include "comdef.h"             /* Definitions for byte, word, etc.     */
#include "err.h"
#include "qurt.h"
#include "msg.h"
#include "tms_utils.h"
#include "qmi_client.h"
#include "qmi_cci_target_ext.h"
#include "timer.h"
#include "rcecb.h"

#include "service_registry_notifier_v01.h"
#include "servreg_internal.h"
#include "servreg_locator.h"
#include "servreg_localdb.h"
#include "servreg_monitor.h"
#include "servreg_notifier.h"
#include "servreg_qmi_notifier_client.h"

#define SERVREG_QMI_NOTIF_CLIENT_WAIT_MASK                (0x1 << 5)
#define SERVREG_QMI_NOTIF_CLIENT_TIMER_SIG                (0x1 << 6)
#define SERVREG_QMI_NOTIF_CLIENT_TIMEOUT_MS               500
#define SERVREG_QMI_NOTIF_CLIENT_REQ_TIMEOUT_MS           1000
#define SERVREG_QMI_NOTIF_CLIENT_INT_TIMEOUT_MS           5000

/* Flag indicating if the QMI client initialization is done */
static SERVREG_SERVICE_STATE servreg_qmi_notifier_client_init_status = SERVREG_SERVICE_STATE_UNINIT;

/* QMI message related and keeping it global because it should be valid for the lifetime of the user_handle */
static qmi_idl_service_object_type servreg_notif_service_object;
static qmi_client_type servreg_qmi_client_notifier;
//qurt_anysignal_t servreg_notif_client_signal;

typedef enum
{
   SERVREG_NOTIF_REGISTER_LISTENER_REQ      = QMI_SERVREG_NOTIF_REGISTER_LISTENER_REQ_V01,
   SERVREG_NOTIF_QUERY_STATE_REQ            = QMI_SERVREG_NOTIF_QUERY_STATE_REQ_V01,
   SERVREG_NOTIF_STATE_UPDATED_IND          = QMI_SERVREG_NOTIF_STATE_UPDATED_IND_V01,
   SERVREG_NOTIF_STATE_UPDATED_IND_ACK_REQ  = QMI_SERVREG_NOTIF_STATE_UPDATED_IND_ACK_REQ_V01,
}servreg_notif_msg_req_type;

/* Service Registry Notifier QMI entry table */
struct servreg_qmi_entry_table_s
{
   SERVREG_NAME domain_name;

   uint32_t qmi_instance_id;

   qmi_service_info servreg_notif_server_info;

   qmi_client_type servreg_qmi_notifier_client_handle;

   struct servreg_qmi_entry_table_s* next;
};
typedef struct servreg_qmi_entry_table_s servreg_qmi_entry_table_t, * servreg_qmi_entry_table_p;

/* Head node for qmi table linked list */
servreg_qmi_entry_table_p servreg_qmi_table_head = SERVREG_NULL;

/* Service Registry Notifier ROOT node structure. List of SERVREG_MON_HANDLE'S*/
struct servreg_qmi_notif_client_node_s
{
   uint32_t notif_signature;

   SERVREG_MON_HANDLE sr_mon_handle;

   struct servreg_qmi_notif_client_node_s* next;
};
typedef struct servreg_qmi_notif_client_node_s servreg_qmi_notif_client_node_t, * servreg_qmi_notif_client_node_p;

/* Type casts as accessor functions */
#define sr_qmi_notif_client_node2sr_qmi_notif_client_handle(x)        ((SERVREG_QMI_NOTIF_CLIENT_HANDLE)x)
#define sr_qmi_notif_client_handle2sr_qmi_notif_client_node(x)        ((servreg_qmi_notif_client_node_p)x)

/* Pool Allocations Notif node */
struct servreg_qmi_notif_client_node_pool_s
{
   struct servreg_qmi_notif_client_node_s servreg_qmi_notif_client_node_pool[SERVREG_QMI_NOTIF_CLIENT_NODE_POOL_SIZE];
   struct servreg_qmi_notif_client_node_pool_s * next;
};
typedef struct servreg_qmi_notif_client_node_pool_s servreg_qmi_notif_client_node_pool_t, * servreg_qmi_notif_client_node_pool_p;

/* Internal structure */
struct servreg_qmi_notif_client_node_internal_s
{
   servreg_qmi_notif_client_node_pool_p servreg_qmi_notif_client_node_pool_head_p;
   servreg_qmi_notif_client_node_p servreg_qmi_notif_client_node_pool_free_p;
   servreg_mutex_t mutex;
   servreg_mutex_t mutex_create;
   SERVREG_BOOL dynamic_use;
   unsigned long init_flag;
};

struct servreg_qmi_notif_client_node_internal_s servreg_qmi_notif_client_node_internal;
servreg_qmi_notif_client_node_pool_t servreg_qmi_notif_client_node_pool_static;

/* Head node of the servreg notification list */
servreg_qmi_notif_client_node_p servreg_qmi_notif_client_list_head = SERVREG_NULL;

/* Static functions in this file */
static servreg_qmi_notif_client_node_p servreg_qmi_notif_client_node_pool_init(void);
static servreg_qmi_notif_client_node_p servreg_qmi_notif_client_node_pool_alloc(void);
static servreg_qmi_notif_client_node_p servreg_qmi_notif_client_node_pool_free(servreg_qmi_notif_client_node_p servreg_qmi_notif_client_node_p);
static void servreg_qmi_notif_client_node_internal_init(void);
static SERVREG_RESULT servreg_qmi_notifier_client_init(SERVREG_NAME domain_name);
static void servreg_ssr_event_cb(uint32_t ssr_name_addr, uint32_t sr_mon_handle_addr);
static void servreg_register_ssr_event(SERVREG_NAME service_name, SERVREG_MON_HANDLE sr_mon_handle);
static servreg_qmi_entry_table_p servreg_get_qmi_table_entry(SERVREG_NAME domain_name);

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_client_node_pool_init
 *
 * Description:
 *     Initializes the memory pool for notifier node structure
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_qmi_notif_client_node_p : Returns the first free notifier node space from the pool
 * =====================================================================  */
static servreg_qmi_notif_client_node_p servreg_qmi_notif_client_node_pool_init(void)
{
   servreg_qmi_notif_client_node_pool_p next_pool = SERVREG_NULL;

   if (SERVREG_NULL == servreg_qmi_notif_client_node_internal.servreg_qmi_notif_client_node_pool_head_p)
   {
      next_pool = &servreg_qmi_notif_client_node_pool_static;
   }
   else if (SERVREG_TRUE == servreg_qmi_notif_client_node_internal.dynamic_use)
   {
      next_pool = (servreg_qmi_notif_client_node_pool_p)servreg_malloc(sizeof(servreg_qmi_notif_client_node_pool_t));
   }

   if (SERVREG_NULL != next_pool)
   {
      int i;

      for (i = 0; i < SERVREG_QMI_NOTIF_CLIENT_NODE_POOL_SIZE; i++)
      {
         if (i != (SERVREG_QMI_NOTIF_CLIENT_NODE_POOL_SIZE - 1))
         {
            next_pool->servreg_qmi_notif_client_node_pool[i].next = &(next_pool->servreg_qmi_notif_client_node_pool[i + 1]);
         }
         else
         {
            next_pool->servreg_qmi_notif_client_node_pool[i].next = servreg_qmi_notif_client_node_internal.servreg_qmi_notif_client_node_pool_free_p;
         }

         next_pool->servreg_qmi_notif_client_node_pool[i].notif_signature = SERVREG_QMI_NOTIF_CLIENT_SIGNATURE;
         next_pool->servreg_qmi_notif_client_node_pool[i].sr_mon_handle = SERVREG_NULL;
      }

      servreg_qmi_notif_client_node_internal.servreg_qmi_notif_client_node_pool_free_p = &(next_pool->servreg_qmi_notif_client_node_pool[0]);
      next_pool->next = servreg_qmi_notif_client_node_internal.servreg_qmi_notif_client_node_pool_head_p;
      servreg_qmi_notif_client_node_internal.servreg_qmi_notif_client_node_pool_head_p = next_pool;
   }
   else
   {
      return SERVREG_NULL;
   }

   return servreg_qmi_notif_client_node_internal.servreg_qmi_notif_client_node_pool_free_p;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_client_node_pool_alloc
 *
 * Description:
 *     Gives the first available free and allocated space from the memory
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_qmi_notif_client_node_p : the first available free and allocated space from the memory
 * =====================================================================  */
static servreg_qmi_notif_client_node_p servreg_qmi_notif_client_node_pool_alloc(void)
{
   servreg_qmi_notif_client_node_p ret;
   servreg_qmi_notif_client_node_p sr_qmi_notif_client_node;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_client_node_internal.mutex));

   if (SERVREG_NULL == servreg_qmi_notif_client_node_internal.servreg_qmi_notif_client_node_pool_free_p)
   {
      sr_qmi_notif_client_node = servreg_qmi_notif_client_node_pool_init();
   }
   else
   {
      sr_qmi_notif_client_node = servreg_qmi_notif_client_node_internal.servreg_qmi_notif_client_node_pool_free_p;
   }

   if (SERVREG_NULL != sr_qmi_notif_client_node)
   {
      servreg_qmi_notif_client_node_internal.servreg_qmi_notif_client_node_pool_free_p = sr_qmi_notif_client_node->next;
      sr_qmi_notif_client_node->next = SERVREG_NULL;
      ret = sr_qmi_notif_client_node;
   }
   else
   {
      ret = SERVREG_NULL;
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_client_node_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_client_node_pool_free
 *
 * Description:
 *     Reclaims back the sr_qmi_notif_client_node to the memory pool
 *
 * Parameters:
 *     sr_qmi_notif_client_node : space to be reclaimed back
 *
 * Returns:
 *    servreg_qmi_notif_client_node_p : The next available free space in the memory pool
 * =====================================================================  */
static servreg_qmi_notif_client_node_p servreg_qmi_notif_client_node_pool_free(servreg_qmi_notif_client_node_p sr_qmi_notif_client_node)
{
   servreg_qmi_notif_client_node_p ret = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_client_node_internal.mutex));

   if(SERVREG_NULL != sr_qmi_notif_client_node)
   {
      sr_qmi_notif_client_node->sr_mon_handle = SERVREG_NULL;

      sr_qmi_notif_client_node->next = servreg_qmi_notif_client_node_internal.servreg_qmi_notif_client_node_pool_free_p;
      servreg_qmi_notif_client_node_internal.servreg_qmi_notif_client_node_pool_free_p = sr_qmi_notif_client_node;
      ret = sr_qmi_notif_client_node;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_qmi_notif_client_node_pool_free() sr_qmi_notif_client_node is NULL and cannot be freed");
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_client_node_internal.mutex));

   return ret;
}


/** =====================================================================
 * Function:
 *     servreg_qmi_notif_client_node_internal_init
 *
 * Description:
 *     Initialization of the internal memory pools and other internals
 *     for sr notifier nodes
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_qmi_notif_client_node_internal_init(void)
{
   servreg_mutex_init_dal(&(servreg_qmi_notif_client_node_internal.mutex));
   servreg_mutex_init_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));

   servreg_mutex_lock_dal(&(servreg_qmi_notif_client_node_internal.mutex));

   servreg_qmi_notif_client_node_internal.dynamic_use = TRUE;
   servreg_qmi_notif_client_node_pool_init();

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_client_node_internal.mutex));
}

/** =====================================================================
 * Function:
 *     servreg_get_qmi_notif_client_node
 *
 * Description:
 *     Checks if a node already exists with the given name. If it does 
 *     exists it returns a pointer to that node.
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by just domain name.
 *
 * Returns:
 *    SERVREG_QMI_NOTIF_CLIENT_HANDLE : handle to the sr notif node
 * =====================================================================  */
SERVREG_QMI_NOTIF_CLIENT_HANDLE servreg_get_qmi_notif_client_node(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_qmi_notif_client_node_p sr_qmi_notif_client_node = SERVREG_NULL;
   SERVREG_QMI_NOTIF_CLIENT_HANDLE sr_qmi_notif_client_handle = SERVREG_NULL;

   sr_qmi_notif_client_node = servreg_qmi_notif_client_list_head;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));

   while(SERVREG_NULL != sr_qmi_notif_client_node)
   {
      if(sr_mon_handle == sr_qmi_notif_client_node->sr_mon_handle)
      {
         sr_qmi_notif_client_handle = sr_qmi_notif_client_node2sr_qmi_notif_client_handle(sr_qmi_notif_client_node);
         break;
      }
      else
      {
         sr_qmi_notif_client_node = sr_qmi_notif_client_node->next;
      }
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));

   return sr_qmi_notif_client_handle;
}

/** =====================================================================
 * Function:
 *     servreg_create_qmi_notif_client_node
 *
 * Description:
 *     Creates a srnotif node with the given sr_mon_handle
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by just domain name.
 *     curr_state : curr_state of the service
 *
 * Returns:
 *    SERVREG_QMI_NOTIF_CLIENT_HANDLE : handle to the sr notif node
 * =====================================================================  */
SERVREG_QMI_NOTIF_CLIENT_HANDLE servreg_create_qmi_notif_client_node(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_qmi_notif_client_node_p sr_qmi_notif_client_node = SERVREG_NULL;  
   SERVREG_QMI_NOTIF_CLIENT_HANDLE sr_qmi_notif_client_handle = SERVREG_NULL;

   /* Check if the srnotif node exists */
   //sr_qmi_notif_client_handle = servreg_get_qmi_notif_client_node(sr_mon_handle);

   //if(SERVREG_NULL == sr_qmi_notif_client_handle)
   //{
      servreg_mutex_lock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));

      sr_qmi_notif_client_node = servreg_qmi_notif_client_node_pool_alloc();

      if(SERVREG_NULL != sr_qmi_notif_client_node)
      {
         /*  Insert to head of list */
         sr_qmi_notif_client_node->next = servreg_qmi_notif_client_list_head;

         /* Update head */
         servreg_qmi_notif_client_list_head = sr_qmi_notif_client_node;

         sr_qmi_notif_client_node->sr_mon_handle = sr_mon_handle;

         sr_qmi_notif_client_handle = sr_qmi_notif_client_node2sr_qmi_notif_client_handle(sr_qmi_notif_client_node);
      }
      else
      {
         ERR_FATAL( "SERVREG_QMI_NOTIF: sr_qmi_notif_client_node malloc failed", 0, 0, 0);
      }

      servreg_mutex_unlock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));
   //}
   return sr_qmi_notif_client_handle;

}

/** =====================================================================
 * Function:
 *     servreg_delete_qmi_notif_client_node
 *
 * Description:
 *     Deletes a sr notif node given the sr notif handle
 *
 * Parameters:
 *     sr_qmi_notif_client_handle  : Handle to the notifier node to be deleted
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_delete_qmi_notif_client_node(SERVREG_QMI_NOTIF_CLIENT_HANDLE sr_qmi_notif_client_handle)
{
   servreg_qmi_notif_client_node_p sr_qmi_notif_client_node = SERVREG_NULL, sr_notif_prev = SERVREG_NULL, sr_qmi_notif_client_node_del = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));

   sr_qmi_notif_client_node = servreg_qmi_notif_client_list_head;
   sr_qmi_notif_client_node_del = sr_qmi_notif_client_handle2sr_qmi_notif_client_node(sr_qmi_notif_client_handle);

   if(SERVREG_NULL != sr_qmi_notif_client_node_del)
   {
      if(SERVREG_QMI_NOTIF_CLIENT_SIGNATURE == sr_qmi_notif_client_node_del->notif_signature)
      {
         while(SERVREG_NULL != sr_qmi_notif_client_node)
         {
            if(sr_qmi_notif_client_node == sr_qmi_notif_client_node_del)
            {
               if(SERVREG_NULL == sr_notif_prev)
               {
                  servreg_qmi_notif_client_list_head = sr_qmi_notif_client_node->next;
               }
               else
               {
                  sr_notif_prev->next = sr_qmi_notif_client_node->next;
               }
 
               sr_qmi_notif_client_node->next = SERVREG_NULL;

              /* Reclaim back the notif msg to free pool */
               if(SERVREG_NULL != servreg_qmi_notif_client_node_pool_free(sr_qmi_notif_client_node))
               {
                  ret = SERVREG_SUCCESS;
               }
               else
               {
                  MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_delete_qmi_notif_client_node() pool_free returned NULL ");
               }
               break;
            }
            else
            {
               sr_notif_prev = sr_qmi_notif_client_node;
               sr_qmi_notif_client_node = sr_qmi_notif_client_node->next;  
            }
         } /* while() */
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_delete_qmi_notif_client_node() sr_qmi_notif_client_handle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_delete_qmi_notif_client_node() sr_qmi_notif_client_handle is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_ssr_event_cb
 *
 * Description:
 *     Callback function invoked when ssr event that was registered for is received
 *
 * Parameters:
 *     ssr_name_addr      : address of the name of the SSR event registered
 *     sr_mon_handle_addr : address of the monitor handle
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_ssr_event_cb(uint32_t ssr_name_addr, uint32_t sr_mon_handle_addr)
{
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;
   SERVREG_NAME ssr_name = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   sr_mon_handle = (SERVREG_MON_HANDLE)sr_mon_handle_addr;
   ssr_name = (SERVREG_NAME)ssr_name_addr;

   ret = servreg_set_state(sr_mon_handle, SERVREG_SERVICE_STATE_DOWN);

   if(SERVREG_SUCCESS == ret)
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: servreg_ssr_event_cb() servreg_set_state success");
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: servreg_ssr_event_cb() servreg_set_state failed");
   }

   if(SERVREG_NULL != ssr_name)
   {
      if(RCECB_NULL != rcecb_unregister_parm2_name(ssr_name, (RCECB_CALLBACK_FNSIG_P2)servreg_ssr_event_cb, (RCECB_PARM)ssr_name, (RCECB_PARM)sr_mon_handle))
      {
         servreg_free(ssr_name);
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_ssr_event_cb() root-pd unregistered with SSR \n");
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_ssr_event_cb() could not unregister with SSR \n");
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_ssr_event_cb() root-pd could not unregister with SSR \n");
   }
}

/** =====================================================================
 * Function:
 *     servreg_register_ssr_event
 *
 * Description:
 *     Registers for the SSR event based on the service_name
 *
 * Parameters:
 *     service_name : string which has the "soc/domain/subdomain" info
 *     sr_mon_handle : Opaque handle to existing event
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_register_ssr_event(SERVREG_NAME service_name, SERVREG_MON_HANDLE sr_mon_handle)
{
   SERVREG_NAME ssr_name = SERVREG_NULL, sub_name = SERVREG_NULL;

   sub_name = strstr(service_name , "root_pd");

   if(SERVREG_NULL != sub_name)
   {
      ssr_name = servreg_get_ssr_name(service_name);
      if(SERVREG_NULL != ssr_name)
      {
         if(RCECB_NULL != rcecb_register_parm2_name(ssr_name, (RCECB_CALLBACK_FNSIG_P2)servreg_ssr_event_cb, (RCECB_PARM)ssr_name, (RCECB_PARM)sr_mon_handle))
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_register_ssr_event() root-pd registered with SSR \n");
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_register_ssr_event() could not register with SSR \n");
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_register_ssr_event() root-pd could not register with SSR \n");
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_register_ssr_event() user-pd need not register for ssr");
   }
}

/** =====================================================================
 * Function:
 *     servreg_get_qmi_table_entry
 *
 * Description:
 *     Checks if the qmi_instance_id field is already stored for the given domain name
 *
 * Parameters:
 *     domain_name : string which has the "soc/domain/subdomain" info
 *
 * Returns:
 *     Pointer to the qmi entry
 * =====================================================================  */
static servreg_qmi_entry_table_p servreg_get_qmi_table_entry(SERVREG_NAME domain_name)
{
   servreg_qmi_entry_table_p sr_qmi_entry = SERVREG_NULL;

   sr_qmi_entry = servreg_qmi_table_head;

   if(SERVREG_NULL != domain_name)
   {
      while(SERVREG_NULL != sr_qmi_entry)
      {
         if(strcmp(domain_name, sr_qmi_entry->domain_name) == 0)
         {
            break;
         }
         else
         {
            sr_qmi_entry = sr_qmi_entry->next;
         }
      }
   }
   return sr_qmi_entry;
}

/** =====================================================================
 * Function:
 *     servreg_create_qmi_table_entry
 *
 * Description:
 *     Stores the qmi_instance_id value for that domain_name
 *
 * Parameters:
 *     domain_name : string which has the "soc/domain/subdomain" info
 *     qmi_instance_id : instance id used to establish a qmi connection for remote service updates
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_create_qmi_table_entry(SERVREG_NAME domain_name, uint32 qmi_instance_id)
{
   servreg_qmi_entry_table_p sr_qmi_entry = SERVREG_NULL;
   uint32_t len = 0;
   SERVREG_NAME local_subdomain_name = SERVREG_NULL;

   local_subdomain_name = servreg_get_local_subdomain_scope();

   if(strcmp(local_subdomain_name, "root_pd") == 0)
   {
      sr_qmi_entry = servreg_get_qmi_table_entry(domain_name);

      if(SERVREG_NULL == sr_qmi_entry)
      {
         sr_qmi_entry = (servreg_qmi_entry_table_p)servreg_malloc(sizeof(servreg_qmi_entry_table_t));

         if(SERVREG_NULL != sr_qmi_entry)
         {
            /*  Insert to head of list */
            sr_qmi_entry->next = servreg_qmi_table_head;
            /* Update head */
            servreg_qmi_table_head = sr_qmi_entry;

            len = servreg_nmelen(domain_name) + 1;
            sr_qmi_entry->domain_name = (SERVREG_NAME)servreg_malloc(len * sizeof(char));

            if(SERVREG_NULL != sr_qmi_entry->domain_name)
            {
               strlcpy(sr_qmi_entry->domain_name, domain_name, len);
            }
            else
            {
               ERR_FATAL( "SERVREG_QMI_NOTIF: in servreg_create_qmi_table_entry() malloc failed", 0, 0, 0);
            }

            sr_qmi_entry->qmi_instance_id = qmi_instance_id;
         }
         else
         {
            ERR_FATAL( "SERVREG_QMI_NOTIF: in servreg_create_qmi_table_entry() malloc failed", 0, 0, 0);
         }
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_create_qmi_table_entry() don't need to record qmi_instance_id in user-pd");
   }
}

/** =====================================================================
 * Function:
 *     servreg_register_remote_listener
 *
 * Description:
 *     Register client as a remote listener because the service is not a local service.
 *     QMI or sockets used as the communication medium for registering the listener
 *     with the remote service registry framework
 *
 *     For every service, remote registration is done only once and one proxy listener in 
 *     the root PD of the other QMI end point will be registered that represents all 
 *     the listeners in the clients root PD for that service.
 *
 * Parameters:
 *     service_name             : Service name that the client is interested in 
 *     sr_mon_handle            : Opaque handle to existing event
 *     sr_curr_remote_state     : State of the service received from the QMI message
 *     sr_remote_transaction_id : Transaction id of the service received from the QMI message
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_register_remote_listener(SERVREG_NAME service_name, SERVREG_MON_HANDLE sr_mon_handle, uint32_t * sr_curr_remote_state, uint32_t * sr_remote_transaction_id)
{
   qmi_client_error_type qmi_client_return;
   qmi_servreg_notif_register_listener_req_msg_v01 servreg_notif_req;
   qmi_servreg_notif_register_listener_resp_msg_v01 servreg_notif_resp;
   SERVREG_RESULT ret = SERVREG_FAILURE;
   SERVREG_QMI_NOTIF_CLIENT_HANDLE sr_qmi_notif_client_handle = SERVREG_NULL;
   servreg_qmi_entry_table_p sr_qmi_entry = SERVREG_NULL;

   /* Check if the srnotif node exists */
   sr_qmi_notif_client_handle = servreg_get_qmi_notif_client_node(sr_mon_handle);

   /* Create a new notif node and register a new proxy listener for that service in root-pd of the other subsystem via QMI only if the srnotif node does not exist */
   if(SERVREG_NULL == sr_qmi_notif_client_handle)
   {
      servreg_mutex_lock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));

      /* Create a Service Notifier node to list the sr_mon_handle to get remote notifications from root pd */
      sr_qmi_notif_client_handle = servreg_create_qmi_notif_client_node(sr_mon_handle);

      /* Register with SSR to get notified if that subsystem does down */
      servreg_register_ssr_event(service_name, sr_mon_handle);

      if(SERVREG_NULL != sr_qmi_notif_client_handle)
      {
         /* Establish a qmi connection with the correct handle during registration of remote service only */
         ret = servreg_qmi_notifier_client_init(service_name);

         if(SERVREG_SUCCESS == ret)
         {
            /* Enable registration */
            servreg_notif_req.enable = SERVREG_TRUE;

            strlcpy(servreg_notif_req.service_name, service_name, QMI_SERVREG_NOTIF_NAME_LENGTH_V01+1);

            sr_qmi_entry = servreg_get_qmi_table_entry(service_name);

            if(SERVREG_NULL != sr_qmi_entry)
            {
               /* QMI message to send request to register as remote listener */
               qmi_client_return = qmi_client_send_msg_sync(sr_qmi_entry->servreg_qmi_notifier_client_handle,  /* user_handle */
                                              SERVREG_NOTIF_REGISTER_LISTENER_REQ,               /* msg_id */
                                              (void*)&servreg_notif_req,                         /* *req_c_table */
                                              sizeof(servreg_notif_req),                         /* req_c_table_len*/
                                              (void*)&servreg_notif_resp,                        /* *resp_c_table*/
                                              sizeof(servreg_notif_resp),                        /* resp_c_table_len*/
                                              SERVREG_QMI_NOTIF_CLIENT_REQ_TIMEOUT_MS                   /* timeout_msecs */
                                              );
               if(QMI_NO_ERR == qmi_client_return)
               {
                  if(QMI_RESULT_SUCCESS_V01 == servreg_notif_resp.resp.result)
                  {
                     /* Get the initial state of the remote handle and set that state in the local handle also */
                     *sr_curr_remote_state = servreg_notif_resp.curr_state;

                     /* IDL file does not support to get the remote transaction id, so update the transaction id when ind msg is received */
                     //*sr_remote_transaction_id = servreg_notif_resp.transaction_id;
                     MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_register_remote_listener() proxy client registered via QMI \n");
                  }
                  else
                  {
                     MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_register_remote_listener() ERROR qmi response = %d\n", servreg_notif_resp.resp.result);
                     ret = SERVREG_FAILURE;
                  }
               }
               else
               {
                  MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_register_remote_listener() ERROR qmi_client_return= %d\n", qmi_client_return);
                  ret = SERVREG_FAILURE;
               }
            }
            else
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_register_remote_listener() sr_qmi_entry NULL \n");
               ret = SERVREG_FAILURE;
            }
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_register_remote_listener() cannot connect to the QMI server");
         }

         if(SERVREG_FAILURE == ret)
         {
            if(SERVREG_SUCCESS != servreg_delete_qmi_notif_client_node(sr_qmi_notif_client_handle))
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_register_remote_listener() servreg_delete_qmi_notif_client_node() failed\n");
            }
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_register_remote_listener() could not register as a remote listener in the notifier pool\n");
      }

      servreg_mutex_unlock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_register_remote_listener() proxy client already registered via QMI \n");
      ret = SERVREG_SUCCESS;
   }

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_deregister_remote_listener
 *
 * Description:
 *     De-register the remote proxy listener via QMI only if there are no more local listeners 
 *     for that service.
 *
 * Parameters:
 *     service_name       : Service name that the client is interested in 
 *     SERVREG_MON_HANDLE : Opaque handle to existing event
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_deregister_remote_listener(SERVREG_NAME service_name, SERVREG_MON_HANDLE sr_mon_handle)
{
   qmi_client_error_type qmi_client_return;
   qmi_servreg_notif_register_listener_req_msg_v01 servreg_notif_req;
   qmi_servreg_notif_register_listener_resp_msg_v01 servreg_notif_resp;
   SERVREG_RESULT ret = SERVREG_FAILURE;
   SERVREG_QMI_NOTIF_CLIENT_HANDLE sr_qmi_notif_client_handle = SERVREG_NULL;
   servreg_qmi_entry_table_p sr_qmi_entry = SERVREG_NULL;
   uint32_t sr_listener_count = 0;

   sr_listener_count = servreg_get_listener_ref_count(sr_mon_handle);

   /* Check if the srnotif node exists */
   sr_qmi_notif_client_handle = servreg_get_qmi_notif_client_node(sr_mon_handle);

   /* Delete the notif node and de-register the proxy listener for that service in root-pd of the other subsystem via QMI only if the srnotif node already exist and the number of local listeners are zero*/
   if(SERVREG_NULL != sr_qmi_notif_client_handle && sr_listener_count == 0)
   {
      servreg_mutex_lock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));

      sr_qmi_entry = servreg_get_qmi_table_entry(service_name);

      if(SERVREG_NULL != sr_qmi_entry)
      {
         /* Disable registration */
         servreg_notif_req.enable = SERVREG_FALSE;

         strlcpy(servreg_notif_req.service_name, service_name, QMI_SERVREG_NOTIF_NAME_LENGTH_V01+1);

         /* QMI message to send request to register as remote listener */
         qmi_client_return = qmi_client_send_msg_sync(sr_qmi_entry->servreg_qmi_notifier_client_handle,  /* user_handle */
                                        SERVREG_NOTIF_REGISTER_LISTENER_REQ,               /* msg_id */
                                        (void*)&servreg_notif_req,                         /* *req_c_table */
                                        sizeof(servreg_notif_req),                         /* req_c_table_len*/
                                        (void*)&servreg_notif_resp,                        /* *resp_c_table*/
                                        sizeof(servreg_notif_resp),                        /* resp_c_table_len*/
                                        SERVREG_QMI_NOTIF_CLIENT_REQ_TIMEOUT_MS                   /* timeout_msecs */
                                        );
         if(QMI_NO_ERR == qmi_client_return)
         {
            if(QMI_RESULT_SUCCESS_V01 == servreg_notif_resp.resp.result)
            {
               /* Delete the Service Notifier node as there is only one remote registrant for that service per root PD */
               ret = servreg_delete_qmi_notif_client_node(sr_mon_handle);
            }
            else
            {
               MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_deregister_remote_listener() ERROR qmi response = %d\n", servreg_notif_resp.resp.result);
               ret = SERVREG_FAILURE;
            }
         }
         else
         {
            MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_deregister_remote_listener() ERROR qmi_client_return= %d\n", qmi_client_return);
            ret = SERVREG_FAILURE;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_deregister_remote_listener() sr_qmi_entry NULL \n");
         ret = SERVREG_FAILURE;
      }

      servreg_mutex_unlock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));
   }

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_free_remote_handle
 *
 * Description:
 *     Free the handle created when clients register as a remote listener. 
 *
 * Parameters:
 *     service_name       : Service name that the client is interested in 
 *     SERVREG_MON_HANDLE : Opaque handle to existing event
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_free_remote_handle(SERVREG_NAME service_name, SERVREG_MON_HANDLE sr_mon_handle)
{
   /* No QMI message currently in IDL file that supports servreg_free_remote_handle() */
   /* Currently we delete the notif node if the number of registrants = 0 */
   return SERVREG_SUCCESS;
}

/** =====================================================================
 * Function:
 *     servreg_set_remote_ack
 *
 * Description:
 *     Set the remote ack count for the remote proxy listener via QMI only if 
 *     all the local acks have been received
 *
 * Parameters:
 *     service_name       : Service name that the client is interested in 
 *     SERVREG_MON_HANDLE : Opaque handle to existing event
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_set_remote_ack(SERVREG_NAME service_name, SERVREG_MON_HANDLE sr_mon_handle, SERVREG_SERVICE_STATE curr_state, uint32_t sr_transaction_id)
{
   qmi_client_error_type qmi_client_return;
   qmi_servreg_notif_set_ack_req_msg_v01 servreg_notif_req;
   qmi_servreg_notif_set_ack_resp_msg_v01 servreg_notif_resp;
   SERVREG_RESULT ret = SERVREG_FAILURE;
   SERVREG_QMI_NOTIF_CLIENT_HANDLE sr_qmi_notif_client_handle = SERVREG_NULL;
   servreg_qmi_entry_table_p sr_qmi_entry = SERVREG_NULL;

   /* Check if the srnotif node exists */
   sr_qmi_notif_client_handle = servreg_get_qmi_notif_client_node(sr_mon_handle);

   if(SERVREG_NULL != sr_qmi_notif_client_handle)
   {
      servreg_mutex_lock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));

      sr_qmi_entry = servreg_get_qmi_table_entry(service_name);

      if(SERVREG_NULL != sr_qmi_entry)
      {
         strlcpy(servreg_notif_req.service_name, service_name, QMI_SERVREG_NOTIF_NAME_LENGTH_V01+1);
         servreg_notif_req.transaction_id = sr_transaction_id;

         /* QMI message to the ACK message req */
         qmi_client_return = qmi_client_send_msg_sync(sr_qmi_entry->servreg_qmi_notifier_client_handle,  /* user_handle */
                                        SERVREG_NOTIF_STATE_UPDATED_IND_ACK_REQ,           /* msg_id */
                                        (void*)&servreg_notif_req,                         /* *req_c_table */
                                        sizeof(servreg_notif_req),                         /* req_c_table_len*/
                                        (void*)&servreg_notif_resp,                        /* *resp_c_table*/
                                        sizeof(servreg_notif_resp),                        /* resp_c_table_len*/
                                        SERVREG_QMI_NOTIF_CLIENT_REQ_TIMEOUT_MS                   /* timeout_msecs */
                                        );
         if(QMI_NO_ERR == qmi_client_return)
         {
            if(QMI_RESULT_SUCCESS_V01 == servreg_notif_resp.resp.result)
            {
               ret = SERVREG_SUCCESS;
               MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_set_remote_ack() success \n");
            }
            else
            {
               MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_set_remote_ack() ERROR qmi response = %d\n", servreg_notif_resp.resp.result);
               ret = SERVREG_FAILURE;
            }
         }
         else
         {
            MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_set_remote_ack() ERROR qmi_client_return= %d\n", qmi_client_return);
            ret = SERVREG_FAILURE;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_set_remote_ack() sr_qmi_entry NULL \n");
         ret = SERVREG_FAILURE;
      }

      servreg_mutex_unlock_dal(&(servreg_qmi_notif_client_node_internal.mutex_create));
   }

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_notif_client_ind_cb
 *
 * Description: 
 *     Called by the QCCI infrastructure when an INDICATION message
 *     is received by the client
 *
 * Parameters:
 *     client_handle : Handle used by the infrastructure to 
 *                     identify different clients.
 *     msg_id        : Message ID
 *     ind_buf       : Pointer to the raw/un-decoded indication
 *     ind_buf_len   : Length of the indication
 *     ind_cb_data   : User-data
 *
 * Returns:
 *             None
 * =====================================================================  */
static void servreg_notif_client_ind_cb(qmi_client_type client_handle, uint32_t msg_id, void *ind_buf, uint32_t ind_buf_len, void *ind_cb_data)
{
   void *ind_msg;
   qmi_client_error_type qmi_err;
   uint32_t decoded_size;
   qmi_servreg_notif_state_updated_ind_msg_v01* indication = NULL;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;
   SERVREG_QMI_NOTIF_CLIENT_HANDLE sr_qmi_notif_client_handle = SERVREG_NULL;
   servreg_qmi_notif_client_node_p sr_qmi_notif_client_node = SERVREG_NULL;
   uint32_t transaction_id = 0;

   MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_notif_client_ind_cb() request: client_handle = %d, msg_id =%d", client_handle, msg_id);

   qmi_err = qmi_idl_get_message_c_struct_len(servreg_notif_service_object, QMI_IDL_INDICATION, msg_id, &decoded_size);

   if(QMI_NO_ERR != qmi_err)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: Received error from QMI framework call %d", qmi_err);
      return;
   }

   ind_msg = servreg_malloc(decoded_size);
   if(!ind_msg) 
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_client_ind_cb() indication buff allocation failed");
      return;
   }

   qmi_err = qmi_client_message_decode(client_handle, QMI_IDL_INDICATION, msg_id, ind_buf, ind_buf_len, ind_msg, decoded_size);
   if (qmi_err != QMI_NO_ERR)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_client_ind_cb() Received error from QMI framework call %d" , qmi_err);
      servreg_free(ind_msg);
      return;
   }

   indication = (qmi_servreg_notif_state_updated_ind_msg_v01*)ind_msg;

   /* Get the existing sr_mon_handle */
   sr_mon_handle = servreg_get_sr_mon_handle(indication->service_name);

   /* Get the Service Notifier node that has already listed to get remote notifications from root pd */
   sr_qmi_notif_client_handle = servreg_get_qmi_notif_client_node(sr_mon_handle);
   sr_qmi_notif_client_node = sr_qmi_notif_client_handle2sr_qmi_notif_client_node(sr_qmi_notif_client_handle);
   
   if(SERVREG_NULL != sr_qmi_notif_client_node)
   {
      /* Decrease the transaction_id by one as servreg_set_state will increase it by one */
      transaction_id = indication->transaction_id - 1;
      servreg_set_transaction_id(sr_mon_handle, transaction_id);

      ret = servreg_set_state(sr_mon_handle, indication->curr_state);

      if(SERVREG_SUCCESS == ret)
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: servreg_notif_client_ind_cb() success");
      }
      else
      {
         ERR_FATAL( "SERVREG_QMI_NOTIF: in servreg_notif_client_ind_cb() ind msg servreg_set_state() error", 0, 0, 0);
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_client_ind_cb() could not get the notif node \n");
   }

   servreg_free(ind_msg);

   return;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notifier_client_init
 *
 * Description:
 *     Initializes the service qmi_notifier QMI client
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     SERVREG_SUCCESS, SERVREG_FAILURE
 * =====================================================================  */
SERVREG_RESULT servreg_qmi_notifier_client_init(SERVREG_NAME domain_name)
{
   qmi_client_error_type qmi_client_return;
   SERVREG_RESULT ret = SERVREG_FAILURE;
   qmi_service_info servreg_notif_server_info;
   qmi_client_type servreg_qmi_notifier_client_handle;
   servreg_qmi_entry_table_p sr_qmi_entry = SERVREG_NULL;

   sr_qmi_entry = servreg_get_qmi_table_entry(domain_name);

   if(SERVREG_NULL != sr_qmi_entry)
   {
      qmi_client_return = qmi_client_get_service_instance(servreg_notif_service_object, sr_qmi_entry->qmi_instance_id, &servreg_notif_server_info);
      if(QMI_NO_ERR == qmi_client_return)
      {
         qmi_client_return = qmi_client_init(&servreg_notif_server_info, servreg_notif_service_object, (void *)servreg_notif_client_ind_cb, NULL, NULL, &servreg_qmi_notifier_client_handle);
         if(QMI_NO_ERR == qmi_client_return)
         {
            /* Record server connection specific info */
            sr_qmi_entry->servreg_notif_server_info = servreg_notif_server_info;
            sr_qmi_entry->servreg_qmi_notifier_client_handle = servreg_qmi_notifier_client_handle;

            ret = SERVREG_SUCCESS;
            MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_qmi_notifier_client_init() qmi_client_init SUCCESS with qmi_intance_id = %d \n", sr_qmi_entry->qmi_instance_id);
         }
         else
         {
            MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_qmi_notifier_client_init() qmi_client_init returned failure qmi_client_return = %d\n", qmi_client_return);
            //qmi_client_release(servreg_qmi_client_notifier);
            ret = SERVREG_FAILURE;
         }
      }
      else
      {
         MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_qmi_notifier_client_init() qmi_client_get_service_instance returned failure qmi_client_return = %d", qmi_client_return);
         //qmi_client_release(servreg_qmi_client_notifier);
         ret = SERVREG_FAILURE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_qmi_notifier_client_init() qmi_instance_id not found \n");
      ret = SERVREG_INVALID_HANDLE;
   }

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_client_init
 *
 * Description:
 *     Initialization function for the service registry QMI notifier module
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_qmi_notif_client_init(void)
{
   qmi_client_error_type qmi_client_return;

   servreg_qmi_notif_client_node_internal_init();

   if(SERVREG_SERVICE_STATE_UP == servreg_qmi_notifier_client_init_status)
   {
      return;
   }
   else
   {
      servreg_notif_service_object = servreg_notif_get_service_object_v01();

      if (servreg_notif_service_object)
      {
         /* Initialize the qmi client notifier */
         qmi_client_return = qmi_client_notifier_init(servreg_notif_service_object, NULL, &servreg_qmi_client_notifier);

         if(QMI_NO_ERR == qmi_client_return)
         {
            servreg_qmi_notifier_client_init_status = SERVREG_SERVICE_STATE_UP;
         }
         else
         {
            MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF:in servreg_qmi_notif_client_init() qmi_client_notifier_init qmi_client_return = %d ",qmi_client_return);
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_qmi_notif_client_init() servreg_notif_service_object in NULL \n");
      }
   }

   return;
}
