#ifndef _QMI_TIME_SERVER_H_
#define _QMI_TIME_SERVER_H_

/* Private part of the server */
#include "qmi_core_server.h"
#include "qmi_csi_common.h"
#include "time_service_v01.h"
#include <stdint.h>
#include "time_service_impl_v01.h"

#include "time_genoff.h"

#if (!defined (FEATURE_NO_DB) && !defined (FEATURE_GSM_PLT))
#include "db.h"
#endif

#define QMI_TIME_SVC_WAIT_SIG 0x00020000
#define TIME_MAX_NUM_CLIENTS 50
#define TIME_MAX_CLIENT_SIG_STR_THRESHOLDS                (5)


typedef struct time_event_report_ind
{
    /* First element of the indication should be
     qmi_indication_type so that to use the services
     provided by the indication services */
    qmi_indication_type event_report_ind;

}time_change_report_ind_type;

/*This struct keeps track of the clients that connects through connect callback*/
typedef  struct time_server_client_data  {
    /* First element of structure should be connection object */
    qmi_core_conn_obj_type        conn_obj;

    /* Indications for ats_tod time change*/
    time_change_report_ind_type     time_ats_tod_change_event_report_indication;    

    /* Indications for ats_user time change*/
    time_change_report_ind_type     time_ats_user_change_event_report_indication;    
    
    /* Indications for ats_1x time change*/
    time_change_report_ind_type     time_ats_1X_change_event_report_indication;   

    /* Indications for ats_gps time change*/
    time_change_report_ind_type     time_ats_gps_change_event_report_indication;   

	/* Indications for ats_secure time change*/
    time_change_report_ind_type     time_ats_secure_change_event_report_indication;   
    
    /* Rest of the client data goes here */
    time_bases_type               time_base;

    int64                         offset;

    /*curbs for setting the remote updates*/
    uint32                        curbs;

    uint32_t                transaction_counter;/* Counts number of transaction served */
    uint8_t                 active_flag;         
}time_server_client_data_type;


/* This is the server object */
typedef struct time_server_class {
    /* Core object should be first element in this struct */
    qmi_core_server_object_type        core_object;
    /* Client data */
    time_server_client_data_type       client_data[TIME_MAX_NUM_CLIENTS];
    uint32_t                           client_index;/* Index into the client data array */
    uint32_t                           client_counter;/*Counts number of active clients */
    /* Rest of the server data goes here */
    
}time_server_class_type;



/* Private part ends here */


/* Public part begins here */

typedef time_server_class_type time_server;


typedef  enum {
    //Inherits from error in the core server
    TIME_SERVER_NO_ERROR = QMI_CORE_SERVER_NO_ERR,
    TIME_SERVER_INTERNAL_ERR = QMI_CORE_SERVER_INTERNAL_ERR + 1,
    TIME_SERVER_NOT_INITIALISED,
    TIME_SERVER_INVALID_OBJECT
}time_server_error;


/* Prototypes for class constructor */
time_server_error  
time_server_class_init(void);


/* Prototype for class destructor */
time_server_error  
time_server_class_destruct(void);


time_server*
time_server_new(char                  *name,
                uint32_t              instance_id,
                time_server_error     *err);


time_server_error
time_server_delete(time_server *me);


time_server_error
time_server_register(time_server *me );

time_server_error
time_server_unregister(time_server *me);

time_server_error
time_server_start_server(time_server *me);

/* FIXME:Send to a particular client*/
time_server_error
time_server_send_ind( time_server *me,
                     int32_t     msg_id,
                     uint16_t    base,
                     uint64_t    offset);

/* Public part ends here */

/*this is test code, this has to go*/
void time_server_call_ind(uint16 base, uint64 offset);

qmi_csi_os_params* time_server_get_os_params
(
  time_server  *me
);

void time_server_handle_event
(
  time_server  *me,
  rex_sigs_type        *event
);

/*
void time_server_call_ind1(void);
*/

#endif
