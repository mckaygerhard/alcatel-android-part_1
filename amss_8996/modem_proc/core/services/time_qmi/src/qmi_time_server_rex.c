#include "qmi_time_server.h"
#include "core_server_platform.h"

void time_server_event_loop(dword param)
{
   rex_sigs_type      wait_on,sig_received;
   qmi_csi_os_params  *os_params;
   time_server       *time_srv_obj;
 
   time_srv_obj =   (time_server *)param;
   time_server_register(time_srv_obj);
   
   os_params = time_server_get_os_params(time_srv_obj);
   wait_on = QMI_TIME_SVC_WAIT_SIG;

   while (1) {
       sig_received = rex_wait(wait_on);
       rex_clr_sigs(os_params->tcb,sig_received);
       time_server_handle_event(time_srv_obj,
                               &sig_received);
   }
}



