
#include "core_variation.h"
#include "customer.h"
#include "comdef.h"

#include "diagcmd.h"
#include "diag.h"
#include "diagnv.h"
#include "diagnv_v.h"
#include "diagdiag.h"
#include "diagtune.h"

#include "timetick.h"
#include "nv.h"
#include "msg.h"

#include "smldiag.h"
#include "fs_fcntl.h"
#include "fs_public.h"
#include "sfs_api.h"

/*===========================================================================

FUNCTION SMLDIAG_EXT_SFS_BACKUP

DESCRIPTION
  This procedure processes a request to read an SIMLOCK dir file.

DEPENDENCIES
  None.

RETURN VALUE
  Pointer to response packet.

SIDE EFFECTS
  None.

============================================================================*/
PACK(void *) smldiag_ext_sfs_backup (
  PACK(void *) req_pkt,
  uint16 pkt_len
)
{
	DIAG_SIMLOCK_EXT_SFS_BACKUP_F_req_type *req; // receive data struct
    DIAG_SIMLOCK_EXT_SFS_BACKUP_F_rsp_type *rsp; // return data struct
    sfs_file_info_list sml_file_list;
    sfs_file_info sml_file[SML_FILELIST_SIZE];
    char  file[DIAG_SIMLOCK_FILE_NAME_SIZE];      // file name which will be open
    int   status;
    int   i;

    // receive data
    req = (DIAG_SIMLOCK_EXT_SFS_BACKUP_F_req_type *) req_pkt;
    // allocate return data struct
    rsp = (DIAG_SIMLOCK_EXT_SFS_BACKUP_F_rsp_type *)
              diagpkt_subsys_alloc(DIAG_SUBSYS_SIMLOCK_EXT,
                                   DIAG_SUBSYS_SIMLOCK_EXT_SFS_BACKUP_F,
                                   sizeof(DIAG_SIMLOCK_EXT_SFS_BACKUP_F_rsp_type));

    if(NULL == rsp) {
      return NULL;/* If we can't allocate, diagpkt_err_rsp() can't either. */
    }

    memset(rsp, 0x00, sizeof(DIAG_SIMLOCK_EXT_SFS_BACKUP_F_rsp_type));
    memset(file, 0x00, sizeof(char) * DIAG_SIMLOCK_FILE_NAME_SIZE);

    /*-----------------------------------------------------------------------
      Packet command id looks ok, format a command to simlock to getlist the item.
      will wait for the response. If neither of those special cases, then
      we're allowed to getlist. The response packet is the same format as the
      request packet, so start by copying the data.
    -----------------------------------------------------------------------*/
    // item file
    if(0 != req->file[0]) {
        memcpy((file),
                (req->file),DIAG_SIMLOCK_FILE_NAME_SIZE);
    } else {
        memcpy((file),
                DEFAULT_FILE_PATH,DIAG_SIMLOCK_FILE_NAME_SIZE);
    }
    memcpy((rsp->file),
            (file),DIAG_SIMLOCK_FILE_NAME_SIZE);

    sml_file_list.file_info = sml_file;

    memset(&sml_file_list, 0x0, sizeof(sml_file_list));

    status = sfs_get_filelist(file, &sml_file_list);

	//TLOG("qmi_nv_diag_req_v01_handler sfs_get_filelist return %d\n", status);
    if(!status) {
		rsp->Files.fileNum = sml_file_list.file_num;
		//TLOG("qmi_nv_diag_req_v01_handler reading %d files\n", sml_file_list.file_num);
        for(i = 0; i < sml_file_list.file_num; i++) {
            strcpy(rsp->Files.files[i].name,
                   sml_file_list.file_info[i].file_name);
            rsp->Files.files[i].item_size = sml_file_list.file_info[i].file_size;

			// read file data
			int fd = sfs_open(sml_file_list.file_info[i].file_name,O_RDONLY);
		    if(0 == fd){
		        //TLOG("qmi_nv_diag_req_v01_handler sfs_open %s failed\n", sml_file_list.file_info[i].file_name);
				continue;
		    }

			uint32 filesize = 0;
			sfs_getSize(fd, &filesize);
		    int return_size = sfs_read(fd, rsp->Files.files[i].item_data, filesize);
			//TLOG("qmi_nv_diag_req_v01_handler sfs_read %s size is %d\n", sml_file_list.file_info[i].file_name,return_size);

			sfs_close(fd);

        }
    }
    sfs_clean_filelist(&sml_file_list);

    return rsp;
}

static const diagpkt_user_table_entry_type smldiag_subsys_ext_tbl[] =
{
    {DIAG_SUBSYS_SIMLOCK_EXT_SFS_BACKUP_F, DIAG_SUBSYS_SIMLOCK_EXT_SFS_BACKUP_F, smldiag_ext_sfs_backup},
};

void smldiag_init (void) {
    DIAGPKT_DISPATCH_TABLE_REGISTER (DIAG_SUBSYS_SIMLOCK_EXT, smldiag_subsys_ext_tbl);
}
