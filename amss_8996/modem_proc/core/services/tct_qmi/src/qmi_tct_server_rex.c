#include "qmi_tct_server.h"
#include "core_server_platform.h"

void tct_server_event_loop(dword param)
{
    rex_sigs_type wait_on, sig_received;
    qmi_csi_os_params *os_params;
    tct_server_class_type *tct_server;

    tct_server = (tct_server_class_type *)param;

    /*
     * Register our server with the QMI framework
     * before we start our event loop.
     */
    tct_server_register(tct_server);

    os_params = qmi_core_server_get_os_params(&(tct_server->core_object));

    wait_on = QMI_TCT_SVC_WAIT_SIG;

    while (1) {
        sig_received = rex_wait(wait_on);
        rex_clr_sigs(os_params->tcb, sig_received);
        qmi_core_server_handle_event(&(tct_server->core_object));
    }
}
