#ifndef _QMI_TCT_H_
#define _QMI_TCT_H_

#include "qmi_core_server.h"
#include "qmi_csi_common.h"
#include "qmi_tct_api_v01.h"
#include <stdint.h>

#define MAX_NUM_CLIENTS 10
#define QMI_TCT_SVC_WAIT_SIG 0x00080000

/*This struct keeps track of the clients that connects through connect callback*/
typedef struct tct_server_client_data  {
    /* First element of structure should be connection object */
    qmi_core_conn_obj_type conn_obj;
    uint8_t active_flag;
} tct_server_client_data_type;

/* This is the server object */
typedef struct tct_server_class {
    /* Core object should be first element in this struct */
    qmi_core_server_object_type core_object;
    /* Client data */
    tct_server_client_data_type client_data[MAX_NUM_CLIENTS];
} tct_server_class_type;

typedef struct {
    tct_server_class_type *tct_server;
} qmi_tct_server_data_type;

/* Declare interfaces */
qmi_core_server_error_type tct_server_register(tct_server_class_type *me);

/* Send indication */
qmi_indication_error_type qmi_tct_send_hello_ind(void);

#endif /* _QMI_TCT_H_ */
