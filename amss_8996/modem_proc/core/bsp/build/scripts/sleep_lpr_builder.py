#===========================================================================
#  GENERAL DESCRIPTION:
#    Contains a scons custom build for creating Sleep LPRs
#
#  Copyright (c) 2012-2015 Qualcomm Technologies, Inc. (QTI).
#  All Rights Reserved.
#  Qqualcomm Technologies Confidential and Proprietary
#===========================================================================
#  DEPENDENCIES: 
#   The CoreSleepLPRBuilder() is hardcoded to use the filename sleep_LPR_master_list.txt.
#
#  OVERVIEW:
#   The following gives an overview for using the sleep_lpr_builder.
#
#  SwTechAreaBuildScons
#   AddCoreSleepLPR|add_sleep_lpr_info() --> Add tech area xml files to the CORESLEEP_ENV_DESCRIPTOR.
#
#  LibBuildScons (or ImageBuildScons if no libs being combined)
#   SaveCoreSleepLPRListBuilder|save_core_sleep_lpr_list()--> Save all xml files added to CORESLEEP_ENV_DESCRIPTOR into a sleep_LPR_list.txt file.
#
#  ImageBuildScons
#   SleepLPRMergeBuilder|sleeplpr_merge_builder()--> Combine the designated sleep_LPR_list.txt files (individual libs being built into an image) 
#   into one SleepLPR_consolidate_list.txt file.
#
#   SleepLPRReplicateBuilder|sleeplpr_replicate_builder()--> For each of the xml list files that were designated to be combined, copy the SleepLPR_consolidate_list.txt 
#   file to a sleep_LPR_master_list.txt file for that Lib build.
#
#  SwTechAreaScons
#   CoreSleepLPRBuilder|CoreSleepLPRFinalBuilder|core_sleep_lpr_action()--> Use the sleep_LPR_master_list.txt file to generate the SW tech area's event ids header file.
#===========================================================================

import os
import subprocess
import itertools
import fnmatch
import string
import time
import shutil
from SCons.Script import *

#---------------------------------------------------------------------------
# Globals
#---------------------------------------------------------------------------

CORESLEEP_ENV_DESCRIPTOR = 'CORESLEEP_INFO_COLLECTOR'
SLEEP_LPR_XML_TAG = 'sleep_lpr_xml'

#---------------------------------------------------------------------------
# Plugins for the SCons framework
#---------------------------------------------------------------------------
def exists(env):
  return True;

def generate(env):
  ''' 
  Sets up a custom scons builder for sleep synth tool. Gets loaded when all
  scons files are scanned
  '''
  env.Append(SLEEP_LPR_MULTIPD = True) # Signal to Call MultiPD builder 

  env.PrintDebugInfo('sleepbuilder', 'Sleep builder is getting constructed') 

  # Adding a flag to environment. This flag must be present to use this builder.
  env.AddUsesFlags('USES_CORESLEEP_TOOL', from_builds_file = True)

  rootenv = env.get('IMAGE_ENV')
  rootenv[CORESLEEP_ENV_DESCRIPTOR] = []

  # Adding a method to environment for client to interact
  rootenv.AddMethod(add_sleep_lpr_info, "AddCoreSleepLPR")
  rootenv.AddMethod(core_sleep_lpr_builder, "CoreSleepLPRBuilder")

  # Creating a builder and adding it to the build environment
  sleeplpr_source_scanner = env.Scanner(core_sleep_lpr_scanner, name="CoreSleepLPRScanner")
  
  sleeplpr_final_action = env.GetBuilderAction(core_sleep_lpr_action, action_source=None)
  sleeplpr_final_bld = env.Builder( action = sleeplpr_final_action, source_scanner = sleeplpr_source_scanner) 
  rootenv.Append(BUILDERS = {'CoreSleepLPRFinalBuilder' : sleeplpr_final_bld })
  
  sleeplpr_merge_actn = env.GetBuilderAction(sleeplpr_merge_builder, action_source=None)
  sleeplpr_merge_bldr = env.Builder(action=sleeplpr_merge_actn, source_scanner = sleeplpr_source_scanner, multi=True)
  rootenv.Append(BUILDERS={'SleepLPRMergeBuilder' : sleeplpr_merge_bldr})

  sleeplpr_replicate_actn = env.GetBuilderAction(sleeplpr_replicate_builder, action_source=None)
  sleeplpr_replicate_bldr = env.Builder(action=sleeplpr_replicate_actn, source_scanner = sleeplpr_source_scanner, multi=True)
  rootenv.Append(BUILDERS={'SleepLPRReplicateBuilder' : sleeplpr_replicate_bldr})
  
  sleeplpr_list_actn = env.GetBuilderAction(save_core_sleep_lpr_list, action_source=None)
  sleeplpr_list_scanner = env.Scanner(core_sleep_lpr_list_scanner, name="CoreSleepLPRListScanner")
  sleeplpr_list_bldr = env.Builder(action=sleeplpr_list_actn, target_scanner = sleeplpr_list_scanner)
  rootenv.Append(BUILDERS={'SaveCoreSleepLPRListBuilder' : sleeplpr_list_bldr})
    
  return
  
def core_sleep_lpr_builder(env, target, source):
    return env.CoreSleepLPRFinalBuilder(target, env.RealPath('${IMAGE_ROOT}/${SHORT_BUILDPATH}/sleep_LPR_master_list.txt'))

def core_sleep_lpr_scanner(node, env, path):
  '''
  This will be called once all XML files are added to the environment. Put
  dependencies on each of them
  '''
  env.PrintDebugInfo('sleepbuilder','In scanner function')
  contents = node.get_contents().splitlines()
  nodes = []
  for f in contents:
     if os.path.exists(f):
         nodes.append(env.RealPath(f))
  nodes.extend([node])
  return nodes
  
def core_sleep_lpr_list_scanner(node, env, path):
  '''
  This will be called once all XML files are added to the environment. Put
  dependencies on each of them
  '''
  rootenv = env.get('IMAGE_ENV')
  env.PrintDebugInfo('sleepbuilder','In scanner function')
  nodes = list(rootenv.get(CORESLEEP_ENV_DESCRIPTOR, []))
  nodes.extend([node])
  return nodes


def add_sleep_lpr_info(env, targets, input_dict):
  '''
  This function gets called when client make a call of AddCoreSleepLPR.
  '''
  if not env.IsTargetEnable(targets):
    env.PrintWarning('Found disabled target call')
    return

  rootenv = env.get('IMAGE_ENV')

  # Checking validity of input dictionary
  if SLEEP_LPR_XML_TAG not in input_dict:
    sleeplpr_error(env, SLEEP_LPR_XML_TAG + ' entry not in dictionary')
  elif None == input_dict[SLEEP_LPR_XML_TAG]:
    sleeplpr_error(env, SLEEP_LPR_XML_TAG + ' entry cannot be empty')

  # Obtaining input file list. If not a list, convert it.
  input_paths = input_dict[SLEEP_LPR_XML_TAG]
  if type(input_paths) is not list:
    input_paths = [input_paths]
   
  # Adding files at the input path to global list
  for aPath in input_paths:

    # Check if the path exists. Raise an exception if not
    if not env.PathExists(aPath):
      errString = 'Path ' + str(aPath) + ' not found'
      sleeplpr_error(env, errString)
  
    # Getting full path
    xml_fullpath = env.RealPath(aPath)

    # Checking if the path is directory or file. If directory, search for
    # xml files in that and add them to global list. If file, directy add
    # it to global list
    if os.path.isdir(xml_fullpath):
      dir_xml_files = env.FindFiles( file_name_patterns = ['*.xml', '*.XML'], root_dir = xml_fullpath )
    else:
      dir_xml_files = [xml_fullpath]

    for aFile in dir_xml_files:
      # Check if same file is specified multiple times
      if aFile in rootenv[CORESLEEP_ENV_DESCRIPTOR]:
        env.PrintDebugInfo('sleepbuilder', "Duplicate LPR XML input file specified " + str(aFile))
      else:
        env.PrintDebugInfo('sleepbuilder', 'Adding LPR XML file to list %s' %str(aFile))
        rootenv[CORESLEEP_ENV_DESCRIPTOR].append(aFile)

  return


def core_sleep_lpr_action(target, source, env):
  '''
  Will be called from sleep/build/sconscript. It will launch sleep synth tool
  '''
  sleeplpr_descriptor =[]
  try:
      sleeplpr_source_file_path = str(source[0])
  except:
      env.PrintError("SleepLPR Builder: sleep LPR source file argument not passed")
      exit()

  try:
     with open(env.RealPath(sleeplpr_source_file_path), 'r') as input:
       for line in input.read().splitlines():
          sleeplpr_descriptor.append(line)
  except IOError:
     env.PrintError("SleepLPR Builder: Could not open sleep LPR source file for read")
     exit()
       
  rootenv = env.get('IMAGE_ENV')
  env.PrintDebugInfo('sleepbuilder', 'Action function')
  env.PrintDebugInfo('sleepbuilder', 'Curr Directory: %s (Build Location: %s)' %(os.getcwd(), rootenv['BUILD_ROOT']))
  env.PrintDebugInfo('sleepbuilder', 'No of (source, target) files (%d, %d)' %(len(target), len(source)))
  env.PrintDebugInfo('sleepbuilder', 'Input files: %d' %(len(sleeplpr_descriptor) ))
  env.PrintDebugInfo('sleepbuilder', 'BuildPath ${BUILDPATH}')

  for aFile in target:
    env.PrintDebugInfo('sleepbuilder', 'Target file %s' %(str(aFile)))
       
  input_xml_files = [ aFile for aFile in sleeplpr_descriptor ]
  proc_name = rootenv.get('PROC_CONFIG')

  EXE_PATH = rootenv['BUILD_ROOT']
  OP_PATH = os.path.dirname(target[0].path)
  HOP_PATH = os.path.dirname(target[3].path)
  NO_MULTICORE = ''

  if proc_name == 'wcn':
    EXE_PATH += '/core/power/sleep/tools/SleepSynth'
  else:
    EXE_PATH += '/core/power/sleep2.0/tools/SleepSynth/bin/SleepSynth'
    NO_MULTICORE = ' -nomc '

  if env['PLATFORM'] == 'windows':
    EXE_PATH += '.exe'
  else:
    os.chmod(EXE_PATH, 0777)      # Adding execution permission for linux
    
  SRC_XMLS = ''

  # Preparing command line for input LPR files
  for aFile in input_xml_files:
    env.PrintDebugInfo('sleepbuilder', 'Input xml: %s' %aFile)
    SRC_XMLS += ' -ip "' + aFile + '"'

  # Entire command for sleep synth tool
  sleep_synth_cmd = EXE_PATH + NO_MULTICORE + SRC_XMLS + ' -op "' + OP_PATH + '" -hop "' + HOP_PATH + '"'
  env.PrintDebugInfo('sleepbuilder', 'SynthTool command: %s' %sleep_synth_cmd);

  # Calling SleepSynth Tool
  sop, serr, rv = env.ExecCmds(cmds=sleep_synth_cmd, target=target, shell=True)
  env.PrintDebugInfo('sleepbuilder', 'stdout: %s' %str(sop))
  env.PrintDebugInfo('sleepbuilder', 'stderr: %s' %str(serr))
  env.PrintDebugInfo('sleepbuilder', 'return: %d' %rv)

  # If tool fails, raise an exception
  if rv != 0:
    err_msg = serr + ' (SleepSynth tool failed (error code: %d))' %rv
    sleeplpr_error(env, err_msg)

  return 


def sleeplpr_error(env, error_string):
  env.PrintError("SleepLPRBuilder error found: " + error_string)
  raise Exception(error_string)

def save_core_sleep_lpr_list(target, source, env):
   
   if not os.path.exists(env.RealPath('${IMAGE_ROOT}/${SHORT_BUILDPATH}')):
      if os.makedirs(env.RealPath('${IMAGE_ROOT}/${SHORT_BUILDPATH}')):
         raise

   #sleeplpr_list_path argument MUST be passed
   try:
      sleeplpr_list_path = str(target[0])
   except:
      env.PrintError("Sleep LPR Builder: sleeplpr_list_path argument not passed")
      exit()

   try:
      sleeplpr_list_file = open(env.RealPath(sleeplpr_list_path), 'w')
   except IOError:
      env.PrintError("Sleep LPR Builder: Could not open sleep LPR list file for write")
      exit()

   for item in env[CORESLEEP_ENV_DESCRIPTOR]:
      sleeplpr_list_file.write(item)
      sleeplpr_list_file.write('\n')  
   sleeplpr_list_file.close()
   return
   
def sleeplpr_merge_builder(target, source, env):
   # Consolidate all the source files in one target file while maintaining the sorting 

   if not os.path.exists(env.RealPath('${IMAGE_ROOT}/${SHORT_BUILDPATH}')):
      if os.makedirs(env.RealPath('${IMAGE_ROOT}/${SHORT_BUILDPATH}')):
         raise
      
   #consolidated_target_file_path argument MUST be passed
   try:
     consolidated_target_file_path = str(target[0])
   except:
     env.PrintError("Sleep LPR Builder: Merge sleep LPR file argument not passed")
     exit()

   try:
     consolidated_target_file = open(env.RealPath(consolidated_target_file_path), 'w')
   except IOError:
     env.PrintError("Sleep LPR Builder: Could not open sleep LPR consolidate file for write")
     exit()

   sleeplpr_descriptor=[]

   sleeplprlists = map(lambda x: str(x), source)
   for sleeplprlist in sleeplprlists:
     try :
        with open(env.RealPath(sleeplprlist), 'r') as input:
                for line in input:
                    sleeplpr_descriptor.append(line) # Add item to list
     except IOError:
        env.PrintError("Sleep LPR Builder: Could not open sleep LPR intermediate file for read")
        exit()
      
   for item in sleeplpr_descriptor:
      consolidated_target_file.write(str(item))
   consolidated_target_file.close()
   return None
   
def sleeplpr_replicate_builder(target, source, env):
            
   try:
      sleeplpr_source_file = str(source[0])
   except:
      env.PrintError("Sleep LPR Builder: Sleep LPR source file argument not passed")
      exit()

   sleeplprtargetlists = map(lambda x: str(x), target)
   for sleeplprtargetlist in sleeplprtargetlists:
      try:
         shutil.copyfile(env.RealPath(sleeplpr_source_file),env.RealPath(sleeplprtargetlist))
      except:
         env.PrintError("Sleep LPR Builder: Could not open Sleep LPR target file for write")
         exit()
   return 
