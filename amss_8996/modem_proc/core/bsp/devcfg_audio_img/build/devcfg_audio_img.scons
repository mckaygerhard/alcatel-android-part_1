#===============================================================================
#
# Target-Level Build Script for Modem Processor
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2009 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.4.c3.11/bsp/devcfg_audio_img/build/devcfg_audio_img.scons#1 $
#  $DateTime: 2016/03/28 23:02:17 $
#  $Change: 10156097 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================
import sys
import os
Import('env')

env = env.Clone()

#------------------------------------------------------------------------------
# Check if we need to load this script or just bail-out
#------------------------------------------------------------------------------
# alias first alias is always the target then the other possibles
aliases = [
   'devcfg_audio_img','audio_img','MPSS_MPD','adsp_mpd_images'
]

# init environment variables
output_elf_name = 'M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}_AUDIO_MERGED'
env.InitImageVars(
   alias_list=aliases,        # aliases
   proc='qdsp6',              # proc
   config='qdsp6_sw',         # config type, proc_name
   plat='qurt',
   target= output_elf_name,   # target (elf, image file name)
   build_tags = ['DAL_DEVCFG_AUDIO_PD_IMG'],
   tools = ['${BUILD_ROOT}/core/bsp/build/scripts/bin_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/hex_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/scl_builder.py',
            'buildspec_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/kernel_builders.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/mbn_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/doc_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/dnt_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/devcfg_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/cmm_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/gendevcfghdr.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/elfmergerutil.py',            
            '${BUILD_ROOT}/core/bsp/build/scripts/lcs_builder.py',            
            '${BUILD_ROOT}/core/bsp/build/scripts/devcfg_lcs_builder.py',
            ]
   )

if not env.CheckAlias(alias_list=aliases):
   Return()

#------------------------------------------------------------------------------
# Setting up export paths for access to elfparserutil.py
#------------------------------------------------------------------------------
corebsp_scripts_path = env.RealPath('${BUILD_ROOT}/core/bsp/build/scripts/')
sys.path.append(corebsp_scripts_path)

# Add defines
env.Append(CPPDEFINES = [
   "CUST_H=\\\"${CUST_H}\\\"",
   "MODEM_FW_NUM_PRIO=75"
])

#---------------------------------------------------------------------------
# Now that we know we need to build something, the first thing we need
# to do is add our image to BuildProducts.txt, so that tools can verify
# when our build is successful.  Make sure we append, so that we don't
# overwrite other images that have written to this file.
#---------------------------------------------------------------------------
prod_files = []
prod_files.append(env.BuildProductsFile ("${BUILDPATH}/BuildProducts_qdsp6sw.txt", "${BUILD_ROOT}/build/ms/bin/${SHORT_BUILDPATH}/qdsp6sw.mbn")) 

#---------------------------------------------------------------------------
# Load in CBSP uses and path variables
#---------------------------------------------------------------------------
env.LoadToolScript('build_utils', toolpath = ['${BUILD_ROOT}/build/scripts'])
env.InitTargetVariation()
env.InitBuildConfig()
env.Replace(USES_AUDIO_PD = True)
env.Replace(USES_DEVCFG = True)
env.Replace(USES_MULTI_DEVCFG = True)
env.Replace(DEVCONFIG_ASSOC_FLAG = 'DAL_DEVCFG_AUDIO_PD_IMG')

#---------------------------------------------------------------------------
# Load in the tools scripts
#---------------------------------------------------------------------------
env.Tool('hexagon', toolpath = ['${BUILD_ROOT}/tools/build/scons/scripts'])
env.Tool('qdsp6_defs', toolpath = ['${BUILD_ROOT}/tools/build/scons/scripts']) 
if env.PathExists('${BUILD_ROOT}/build/scripts/build_utils.py'):
   use_build_utils = True
   env.LoadToolScript('build_utils', toolpath = ['${BUILD_ROOT}/build/scripts'])
else:
   use_build_utils = False

#---------------------------------------------------------------------------
# Load in the compiler options for the devcfg image
#---------------------------------------------------------------------------
# This ensures that there is no .sdata section created in the devcfg image
env.Append(CFLAGS = "-G0 -fno-zero-initialized-in-bss ")

#---------------------------------------------------------------------------
# Libraries/Objects Section
#---------------------------------------------------------------------------
devcfgimg_libs = []
devcfgimg_objs = []
devcfg_units = [prod_files]
primary_elf = env.RealPath('${BUILD_MS_ROOT}/M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}_AUDIO_PD.elf')
devcfg_build_dest_name = '${BUILDPATH}/DEVCFG_${BUILD_ASIC}${BUILD_ID}${BUILD_VER}'
devcfg_elf_name = env.RealPath('${BUILD_MS_ROOT}/M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}_AUDIO_DEVCFG.elf')
if use_build_utils:
   devcfg_lcs_template_file = env.FindConfigFiles('DEVCFG_IMG.lcs.template')[0]
else:
   devcfg_lcs_template_file = env.RealPath('${BUILD_MS_ROOT}/DEVCFG_IMG.lcs.template')

devcfg_lcs_name = 'DEVCFG_IMG.lcs'
devcfg_lcs_file_path_name = env.RealPath('${BUILDPATH}/DEVCFG_IMG.lcs')
merged_elf_name = env.RealPath('${BUILDPATH}/M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}.elf')
merged_elf_name_output = env.RealPath('${BUILD_MS_ROOT}/' + output_elf_name + '.elf')
devcfg_hdr_file_name = 'devcfg_def.h'
devcfg_hdr_file = env.RealPath('${BUILDPATH}/' + devcfg_hdr_file_name)

# Template to load au_name libs/objs build rules.
#au_name_path = env.RealPath('${BUILD_ROOT}/au_name')
#if env.PathExists(au_name_path):
   #au_name_items = env.LoadAreaSoftwareUnits('au_name')
   #ourimg_libs.extend(au_name_items['LIBS'])
   #ourimg_objs.extend(au_name_items['OBJS'])

# load Core BSP libs/objs build rules.
core_path = env.RealPath('${BUILD_ROOT}/core')
if env.PathExists(core_path):
   au_items = env.LoadAreaSoftwareUnits('core')
   devcfgimg_libs.extend(au_items['LIBS'])
   devcfgimg_objs.extend(au_items['OBJS'])

#Load audio_avs libs/objs build rules,outside core
audio_path = env.RealPath('${BUILD_ROOT}/audio_avs')
if env.PathExists(audio_path):
   env.LoadToolScript('platform_builders', toolpath = ['${BUILD_ROOT}/platform/build']) 
   au_items = env.LoadAreaSoftwareUnits('audio_avs')
   devcfgimg_libs.extend(au_items['LIBS'])
   devcfgimg_objs.extend(au_items['OBJS'])
   
devcfg_units = [devcfgimg_libs, devcfgimg_objs]

#------------------------------------------------------------------------------
# Rule for compiling devcfg_main.c 
#------------------------------------------------------------------------------
# filter is not define or filter is current target files only, ok to build misc files
filter_opt = env.get('FILTER_OPT')
if (filter_opt is None) or (env.FilterMatch(os.getcwd())):
   env.VariantDir("${BUILDPATH}", "${BUILD_ROOT}/core/bsp/devcfg_img/src", duplicate=0)
   env.RequirePublicApi(['DAL'])
   env.RequireRestrictedApi(['DAL'])
   #Add devcfg_main as an object in the devcfg image 
   devcfg_main_obj = env.AddObject('DAL_DEVCFG_AUDIO_IMG' , '${BUILDPATH}/devcfg_main.c')
   env.Depends(devcfg_main_obj, primary_elf)
   devcfgimg_objs.extend(devcfg_main_obj)

   #=========================================================================
   # Beging building DEVCFG Image
   #-------------------------------------------------------------------------
   # # Create the devcfg linker script
   devcfg_lcs_file = env.DevcfgLCSBuilder(devcfg_lcs_file_path_name, devcfg_lcs_template_file, DEVCFG_DEF_HDR_FILE = devcfg_hdr_file_name)
   env.Depends(devcfg_lcs_file, devcfg_lcs_template_file)
   env.Depends(devcfg_lcs_file, (env.Value(env.get('DEVCFG_ENV_USED_XML_TAGS'))))
   #install_devcfg_lcs_file = env.InstallAs(devcfg_lcs_file_path_name, devcfg_lcs_file)
   devcfg_units.extend(devcfg_lcs_file)
   env.AddArtifact('DAL_DEVCFG_AUDIO_IMG', devcfg_lcs_file)

   # Create the devcfg_def hdr file that contains the segment addrs from the primary elf. This 
   # will be the address at which the devcfg elf data segments will be built at
   # Collect the segment name and the addr variable that was constructed when creating 
   # the devcfg lcs file: build\ms\DEVCFG_IMG.lcs.
   devcfg_scl_hdr_file = env.GenerateDevCfgHdrs(devcfg_hdr_file, [primary_elf])
   env.Depends(devcfg_scl_hdr_file, primary_elf)
   env.Depends(devcfg_scl_hdr_file, devcfg_lcs_file_path_name)
   #install_devcfg_hdr_file = env.InstallAs(devcfg_hdr_file, devcfg_scl_hdr_file)
   devcfg_units.extend(devcfg_scl_hdr_file)

# the following steps can only be performed if there is no filters in effect
if filter_opt is None:
   libs_path = env['INSTALL_LIBPATH']
   # create list of files used during link.

   #Preprocess the LCS (linker script) file
   pp_lcs = env.PreProcess(devcfg_lcs_file_path_name + '.pp', devcfg_lcs_file_path_name)
   #Cleanup the Preprocessed LCS file
   pp_lcs_clean = env.PreProcessClean(devcfg_lcs_file_path_name + '.i', pp_lcs)
   
   #Setup the bare minimum linker commands
   env.Replace(LINKFLAGS = "-m${Q6VERSION} -nostdlib --section-start .start=0x0 --entry=0x0 ${LFLAGS}")
   env.Append(LINKFLAGS = '-T' + env.RealPath(str(pp_lcs_clean[0])))
   
   env.Replace(LINKOBJGRP = "--start-group $_LIBFLAGS_POSIX  $SOURCES.posix --end-group ")
   #Invoke the build command
   devcfg_elf = env.Program(devcfg_build_dest_name, 
                              source=[devcfgimg_objs], 
                              LIBS=[devcfgimg_libs], 
                              LIBPATH=libs_path)

   install_devcfg_elf = env.InstallAs(devcfg_elf_name, devcfg_elf)

   devcfg_map = env.SideEffect(env.subst(devcfg_build_dest_name + '.map'), devcfg_elf)
   devcfg_sym = env.SideEffect(env.subst(devcfg_build_dest_name + '.sym'), devcfg_elf)
   env.Clean(devcfg_elf, devcfg_map)
   env.Clean(devcfg_elf, devcfg_sym)
   
   env.Depends(pp_lcs_clean, devcfg_lcs_file_path_name)
   env.Depends(devcfg_elf, pp_lcs_clean)
   env.Depends(devcfg_elf, primary_elf)

   #-------------------------------------------------------------------------
   # Install ELF, reloc files
   #-------------------------------------------------------------------------
   # Merge elfs here
   #print 'Section Names: ', env.GetDevcfgSectionInfoList(['DAL_DEVCFG_IMG'], {})
   # elfmergeout1 = env.ElfMergerUtilBuilder(merged_elf_name_v0, [primary_elf, devcfg_elf_name], 
                     # PRIMARY_SECTION_NAME=".8974_DEVCFG_DATA", SECONDARY_SECTION_NAME=".8974_DEVCFG_DATA")
   # elfmergeout = env.ElfMergerUtilBuilder(merged_elf_name, [merged_elf_name_v0, devcfg_elf_name], 
                     # PRIMARY_SECTION_NAME=".8626_DEVCFG_DATA", SECONDARY_SECTION_NAME=".8626_DEVCFG_DATA")
   elfmergeout = env.ElfMergerUtilBuilder(merged_elf_name, [primary_elf, devcfg_elf_name], 
                     PRIMARY_SECTION_NAME=[], SECONDARY_SECTION_NAME=[])
   env.Depends(elfmergeout, (env.Value(env.get('SECTION_NAME_HDR_LIST'))))
   env.Depends(elfmergeout, devcfg_lcs_file)
   env.Depends(elfmergeout, install_devcfg_elf)   

   install_merged_elf = env.InstallAs(merged_elf_name_output, elfmergeout)
         
   #=========================================================================
   # Define targets needed DEVCFGIMG
   #=========================================================================
   devcfg_units += [
      install_devcfg_elf,
      elfmergeout,
      install_merged_elf,
   ]

# add aliases
for alias in aliases:
   env.Alias(alias, devcfg_units)
