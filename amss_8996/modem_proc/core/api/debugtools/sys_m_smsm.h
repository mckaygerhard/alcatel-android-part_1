#ifndef SYS_M_SMSM_H
#define SYS_M_SMSM_H
/*===========================================================================

           S Y S _ M _ S M S M. H

DESCRIPTION

Copyright (c) 2013-15 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

===========================================================================
DESCRIPTION
High level system monitor
===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.4.c3.11/api/debugtools/sys_m_smsm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/26/13   rks     CR542178:Modify mechanism for sending smp2p notification of 
                   modem failure to HLOS

===========================================================================*/
#include "comdef.h"

#define SYS_M_SMP2P_SUCCESS         0
#define SYS_M_SMP2P_NOT_READY       1
#define SYS_M_SMP2P_INVALID_PARAM   2

#define SYS_M_AP2SUBSYS_SMSM_ERRFATAL       0x1
#define SYS_M_AP2SUBSYS_SMSM_SHUT_DWN       0x2

// Force 32 bits enum
typedef enum
{
   SYS_M_SUBSYS2AP_SMSM_ERRFATAL = 0x00000001,
   SYS_M_SUBSYS2AP_SMSM_ERR_HDL_RDY = 0x00000002,
   SYS_M_SUBSYS2AP_SMSM_PWR_CLK_RDY = 0x00000004,
   SYS_M_SUBSYS2AP_SMSM_SHUT_DWN_ACK = 0x00000008,
   SYS_M_SUBSYS2AP_SMSM_SHUT_DWN = 0x00000010,
   SYS_M_SUBSYS2AP_SMSM_PWR_OFF = 0x00000020,
   SYS_M_SUBSYS2AP_QMI_REQ_ACK = 0x00000080,
   SYS_M_SUBSYS2AP_FORCE_32BIT_ENUM = 0xFFFFFFFF    // Do not use, this is here only to force the enum to 32 bits
} SYS_M_SUBSYS2AP_SMSM_BITS;

/**
  Sets bit in sys_m point to point array to apps

  @param[in] bit       Bit to set
  
  @return
  None.
  
  @dependencies
  None.
*/
int32 sys_m_smsm_apps_set(uint32 bit);

/** 
  Single threaded mode version of above (no mutex operations) 
  Sets bit in sys_m point to point array to apps 
                        
  @return
  Error codes
  
  @dependencies
  None.
*/
int32 sys_m_smsm_apps_set_stm(uint32 bits);


#endif  /* SYS_M_SMSM_H */
