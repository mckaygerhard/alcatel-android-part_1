#ifndef TMC_H
#define TMC_H

/*===========================================================================

TASK MAIN CONTROLLER

DESCRIPTION
  UNSUPPORTED & REMOVED TMC/REX API. REMOVE ALL FALSE DEPENDENCY TO
  THIS HEADER FILE.

Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/api/debugtools/tmc.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/

// REMEDIATION: REMOVE REFERENCES TO THIS HEADER FILE

#if defined(__GNUC__)
#pragma message "UNSUPPORTED & REMOVED API, remove the false dependency to TMC.H"
#elif defined(__ARMCC_VERSION)
#warning "UNSUPPORTED & REMOVED API, remove the false dependency to TMC.H"
#else
// ALL OTHERS : NO MESSAGE
#endif

#if !defined(ERR_H)
//#warning "UNSUPPORTED & REMOVED API, potential access to ERR.H through TMC.H"
#include "err.h"
#endif

#define tmc_task_offline()    ERR_FATAL("REMOVE FALSE DEPENDENCY TO TMC", 0, 0, 0)
#define tmc_task_stop()       ERR_FATAL("REMOVE FALSE DEPENDENCY TO TMC", 0, 0, 0)

#endif
