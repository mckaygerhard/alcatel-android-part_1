/***********************************************************************
 * rmts_config.h
 *
 * External configuration items for RMTS
 * Copyright (C) 2014 QUALCOMM Technologies, Inc.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/api/storage/rmts_config.h#1 $ $DateTime: 2016/03/28 23:02:17 $ $Author: mplcsds1 $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2014-07-22   dks   Create

===========================================================================*/

#ifndef __RMTS_CONFIG_H__
#define __RMTS_CONFIG_H__

#include "customer.h"

#endif /* not __RMTS_CONFIG_H__ */
