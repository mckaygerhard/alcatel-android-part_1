#ifndef PM_RTC_H
#define PM_RTC_H

/** @file pm_rtc.h
*/

/*===========================================================================

           R T C   S E R V I C E S   H E A D E R   F I L E

DESCRIPTION
  This file contains functions prototypes and variable/type/constant 
  declarations for the RTC services developed for the Qualcomm Power
  Management IC.
  
  Copyright (c) 2003-2009, 2013 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/api/systemdrivers/pmic/pm_rtc.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/20/09   jtn     Move init function prototype to pm_device_init_i.h
10/23/08   jtn     Added API pm_hal_rtc_prescaler_reset()
06/18/07   cng     Added meta comments to miscellaneous RTC APIs
05/31/06   Vish    Fixed LINT warnings.
05/03/05   Vish    Modified pm_hal_rtc_get_time() to read RTC time even in
                   the case when RTC was not running.
01/19/05   Vish    Updated function header for pm_hal_rtc_get_time().
01/28/04   rmd     Added initial support for multiple PMIC models/tiers.
11/07/03   Vish    Added the task of clearing the alarm trigger condition
                   within pm_hal_rtc_disable_alarm() so that the master
                   RTC alarm interrupt could be cleared afterward.
10/02/03   Vish    Added pm_hal_rtc_get_alarm_time() and 
                   pm_hal_rtc_get_alarm_status().
09/23/03   Vish    Changed all pm_rtc_xxx variables/fns to pm_hal_rtc_xxx.
09/13/03   Vish    Created.
===========================================================================*/

#include "comdef.h"
#include "pm_err_flags.h"

/* =========================================================================
                         TYPE DEFINITIONS
========================================================================= */

/** @addtogroup pm_rtc
@{ */

/** RTC time structure used for setting and retrieving current time and for
    setting the alarm time. */
typedef struct
{
   uint32  sec;  /**< RTC time in seconds. */
} pm_hal_rtc_time_type;

/** Type for selecting the physical alarms available in the RTC. */
typedef enum
{
   PM_HAL_RTC_ALARM_1,  /**< RTC alarm 1. */
   PM_HAL_RTC_ALARM_INVALID
} pm_hal_rtc_alarm;

/** Status of the various physical alarms available in the RTC. */
typedef struct
{
   boolean  alarm_1_triggered;  /**< Alarm 1 is triggered. */
} pm_hal_rtc_alarm_status_type;


/* =========================================================================
                         FUNCTION PROTOTYPES
========================================================================= */

/*===========================================================================
FUNCTION  pm_rtc_start                                 EXTERNAL FUNCTION
===========================================================================*/

/**
   Starts the RTC ticking with the indicated time as its current
   (start-up) time.

@param[in] pmic_chip Select the device in which the coin cell charger
                             being controlled is located. Device index starts
                             with zero.
@param[in] start_time_ptr Pointer to the start-up time for the real-time clock.
                          See #pm_hal_rtc_time_type. Valid input is a valid
                          non-NULL RTC time structure containing any 32-bit
                          number indicating the number of seconds elapsed from
                          a known point in time in history.
   
@return 
  SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
  pm_init() must have been called.

@sideeffects
   Interrupts are disabled during this function.
*/
extern pm_err_flag_type pm_rtc_start(
   uint8 pmic_chip, 
   const pm_hal_rtc_time_type *start_time_ptr
);



/*===========================================================================
FUNCTION  pm_rtc_stop                                  EXTERNAL FUNCTION

DESCRIPTION
   This function stops the RTC from ticking.

@param[in] pmic_chip Selects the device in which the coin cell charger being controlled is located. 
Device index starts with zero.

@return
   SUCCESS or Error -- See #pm_err_flag_type.          

@dependencies
pm_init() must have been called.

@sideeffects
   Interrupts are disabled during this function.
*/

extern pm_err_flag_type pm_rtc_stop(uint8 pmic_chip);


/*===========================================================================
FUNCTION  pm_hal_rtc_get_time                              EXTERNAL FUNCTION
===========================================================================*/

 /**  This function returns the current time of the RTC. This will be the actual
   present time if the RTC has been ticking or the time at which the RTC
   was last stopped.

@param[in] pmic_chip Select the device in which the coin cell charger
                             being controlled is located. Device index starts with zero.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
  pm_init() and pm_rtc_start() must have been called.

@sideeffects
   Interrupts are disabled during this function.
*/
extern pm_err_flag_type pm_dev_hal_rtc_get_time(
    uint8 pmic_chip, 
    pm_hal_rtc_time_type *time_ptr
);
//#pragma message("Do not use pm_hal_rtc_get_time(), which is deprecated. Use pm_dev_hal_rtc_get_time() instead.")
extern pm_err_flag_type pm_hal_rtc_get_time(pm_hal_rtc_time_type *time_ptr);



/*===========================================================================
FUNCTION  pm_rtc_enable_alarm                          EXTERNAL FUNCTION
===========================================================================*/

/**
   Enables the selected alarm to go off at the specified time.

@param[in] pmic_chip Selects the device in which the coin cell charger
                             being controlled is located. Device index starts
                             with zero.
@param[in] what_alarm Indicates which alarm is to be turned on. See
                      #pm_hal_rtc_alarm.
@param[in] trigger_time_ptr Pointer to the time at which the alarm is to go
                            off. See #pm_hal_rtc_time_type. Valid input is to
                            a valid non-NULL RTC time structure containing any
                            32-bit value indicating the number of seconds that
                            have elapsed (in relation to a known point in time
                            in history, as specified in the previous
                            pm_rtc_start() call) exactly at the moment the
                            alarm is expected to go off.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
  pm_init() and pm_rtc_start() must have been called.

@sideeffects
   Interrupts are disabled during this function.
*/

extern pm_err_flag_type pm_rtc_enable_alarm(    
    uint8 pmic_chip, 
    pm_hal_rtc_alarm             what_alarm,
    const pm_hal_rtc_time_type  *trigger_time_ptr
);


/*===========================================================================
FUNCTION  pm_rtc_disable_alarm                         EXTERNAL FUNCTION */

/**
   Disables the specified alarm so that it does not go off in the future.
   If the alarm has already been triggered by the time this function
   was called, it also clears the alarm trigger condition so that the
   master RTC alarm interrupt can be cleared afterward.

@param[in] pmic_chip Select the device in which the coin cell charger
                             being controlled is located. Device index starts
                             with zero.
@param[in] what_alarm Indicates which alarm is to be turned off. See
                      #pm_hal_rtc_alarm.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
  pm_init() must have been called.

@sideeffects
   Interrupts are disabled during this function.
*/
extern pm_err_flag_type pm_rtc_disable_alarm(
    uint8 pmic_chip, 
    pm_hal_rtc_alarm  what_alarm
);

/**===========================================================================
FUNCTION  pm_rtc_get_alarm_time                        EXTERNAL FUNCTION
===========================================================================*/

/**
   Returns the time at which the specified alarm has been set to go off.

@param[in] pmic_chip Select the device in which the coin cell charger
                             being controlled is located. Device index starts
                             with zero.
@param[in] what_alarm Indicates the alarm to be read. See # pm_hal_rtc_alarm.
@param[out] alarm_time_ptr Pointer to the time structure that is used by this
                           function to return the time at which the specified
                           alarm has been programmed to trigger. See
                           #pm_hal_rtc_time_type. Valid input is any valid
                           non-NULL RTC time structure.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
  pm_init() must have been called.

  @sideeffects
   Interrupts are disabled during this function.
*/
extern pm_err_flag_type pm_rtc_get_alarm_time(
   uint8 pmic_chip, 
   pm_hal_rtc_alarm       what_alarm,
   pm_hal_rtc_time_type  *alarm_time_ptr
);

/*===========================================================================
FUNCTION  pm_rtc_get_alarm_status                      EXTERNAL FUNCTION
===========================================================================*/

/**
   Returns the status of the various alarms supported in the hardware.
   A value of 1 means that the alarm has been triggered, and 0 means
   it has not.

@param[in] pmic_chip Select the device in which the coin cell charger
                             being controlled is located. Device index starts
                             with zero.
@param[out] status_ptr  Pointer to a valid uint8 value used for returning the
                        status information for the various alarms (currently,
                        only one alarm is valid -- PM_HAL_RTC_ALARM_1).
                        Valid inputs: A valid uint8 variable, e.g.,
@verbatim
              Bit7   Bit6   Bit5   Bit4   Bit3   Bit2   Bit1   Bit0
                                                                ^ 
                                                                |
                                                Alarm1's status @endverbatim

@return 
  SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
 pm_init() must have been called.

@sideeffects
   Interrupts are disabled during this function.
*/
extern pm_err_flag_type pm_rtc_get_alarm_status(uint8 pmic_chip, uint8 *status_ptr);


/*===========================================================================
FUNCTION  pm_rtc_set_time_adjust                       EXTERNAL FUNCTION
===========================================================================*/

/**
   Sets the time adjustment correction factor for a RTC crystal
   oscillator that is slightly off the 32768 Hz frequency. Every 10th second,
   the frequency divider is switched from the nominal 32768 to
   (32768 - 64 + time_adjust).

@param[in] pmic_chip Select the device in which the coin cell charger
                             being controlled is located. Device index starts
                             with zero.
@param[in] time_adjust Adjustment factor (uint8). Valid inputs: \n
           0 to 63    -->  Compensates for a slower crystal oscillator. \n
           64         -->  Provides no compensation. \n
           65 to 127  -->  Compensates for a faster crystal oscillator.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
  pm_init() must have been called.

@sideeffects
   Interrupts are disabled during this function.
*/
extern pm_err_flag_type pm_rtc_set_time_adjust(
    uint8 pmic_chip,
    uint8 time_adjust
);


/*===========================================================================
FUNCTION  pm_rtc_get_time_adjust                       EXTERNAL FUNCTION
===========================================================================*/

/**
   Returns the current time adjustment correction factor in use, as
   set by the previous pm_rtc_set_time_adjust().

@param[in] pmic_chip Select the device in which the coin cell charger
                             being controlled is located. Device index starts
                             with zero.
@param[out] time_adjust_ptr Pointer to the adjustment factor (uint8).
                           Valid inputs: \n
           Non-NULL pointer to a valid uint8 containing: \n
           0 to 63    -->  Compensation for a slower crystal oscillator. \n
           64         -->  No compensation. \n
           65 to 127  -->  Compensation for a faster crystal oscillator.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
  pm_init() and pm_rtc_set_time_adjust() must have been called.

@sideeffects
   Interrupts are disabled during this function.
*/
extern pm_err_flag_type pm_rtc_get_time_adjust(
    uint8 pmic_chip, 
    uint8 *time_adjust_ptr
);


/** @} */ /* end_addtogroup pm_rtc */

#endif // PM_RTC_H


