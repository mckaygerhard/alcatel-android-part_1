#ifndef __PM_XO_CORE_H__
#define __PM_XO_CORE_H__

/** @file pm_xo_core.h
*
*   PMIC-MEGA XO CORE RELATED DECLARATION 
*   This header file contains functions and variable declarations 
*  to support Qualcomm PMIC MEGA XO module. 
*/
/*
 *  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
 *  All Rights Reserved.
 *  Confidential and Proprietary - Qualcomm Technologies, Inc.
*/

/* =======================================================================
                                Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/api/systemdrivers/pmic/pm_xo_core.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/14/12   hs      Initial version. 
========================================================================== */
#include "comdef.h"
#include "pm_err_flags.h"

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/
/** @addtogroup pm_xo_core
@{ */

/** Crystal oscillator core power modes. */
typedef enum
{
    /**
     * Very low power mode.
     */
    PM_XO_CORE_PWR_MODE__VLPM,
    /**
     * Low power mode
     */
    PM_XO_CORE_PWR_MODE__LPM,
    /**
     * Normal power mode
     */
    PM_XO_CORE_PWR_MODE__NPM, 
    /**
     * High power mode
     */
    PM_XO_CORE_PWR_MODE__HPM
} pm_xo_core_power_mode_type;

/*===========================================================================

                 SMBC DRIVER FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

FUNCTION FUNCTION pm_mega_xo_set_power_mode                EXTERNAL FUNCTION
===========================================================================*/

/**
Sets the XO core power mode.

@param[in] pmic_chip Select the device in which the coin cell charger
                             being controlled is located. Device index starts
                             with zero.
@param[in] externalResourceIndex External resource index.
@param[in] mode XO core power mode. See #pm_xo_core_power_mode_type.

@return 
 SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
 None.

@sideeffects
Interrupts are disabled while communicating with the PMIC.
*/
pm_err_flag_type pm_xo_core_set_power_mode(uint8 pmic_chip, 
                                           int externalResourceIndex, 
                                           pm_xo_core_power_mode_type mode);


/*===========================================================================

FUNCTION FUNCTION pm_xo_set_xo_trim                     EXTERNAL FUNCTION
===========================================================================*/

/**
Trims the 19.2M Hz XO load capacitances.

@param[in] trim_value Raw trim value to be written, in the range of 0 to 63
                      (uint8).

@return 
 SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
pm_init() must have been called.

@sideeffects
Interrupts are disabled while communicating with the PMIC.
*/
pm_err_flag_type pm_dev_mega_xo_set_xo_trim(uint8 pmic_chip, int externalResourceIndex, uint8 trim_value);
//#pragma message("Do not use pm_mega_xo_set_xo_trim(), which is deprecated. Use pm_dev_mega_xo_set_xo_trim() instead.")
pm_err_flag_type pm_mega_xo_set_xo_trim(uint8 pmic_chip, int externalResourceIndex, uint8 trim_value);
/*===========================================================================

FUNCTION FUNCTION pm_xo_get_xo_trim                     EXTERNAL FUNCTION
===========================================================================*/

/**
Gets the raw trim value of the load capacitances for the 19.2 MHz XO.

@param[out] trim_value Pointer to the raw trim value.

@return
 Raw trim value in the range of 0 to 63. \n
 A value of 0xFF is an error code that indicates a read failure.

@dependencies
pm_init() must have been called.

@sideeffects
Interrupts are disabled while communicating with the PMIC.
*/
pm_err_flag_type pm_dev_mega_xo_get_xo_trim(uint8 pmic_chip, int externalResourceIndex, uint8* trim_value);
//#pragma message("Do not use pm_xo_core_get_xo_trim(), which is deprecated. Use pm_dev_xo_core_get_xo_trim() instead.")
pm_err_flag_type pm_xo_core_get_xo_trim(uint8 pmic_chip, int externalResourceIndex, uint8* trim_value);
/**
 * Forces the XO core on if pm_xo_enable(TRUE) has been called.
 * 
 *  * @param[in] xo_core_enable TRUE -- Force the XO core on. \n
 *                           FALSE -- Leave the XO core off.
 * 
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type  pm_xo_core_set_xo_core_force_on(uint8 pmic_chip, int externalResourceIndex, boolean xo_core_enable);


/** @} */ /* end_addtogroup pm_xo_core */

#endif /* __PM_XO_CORE_H__ */
