#ifndef PM_MPPS_H
#define PM_MPPS_H

/** @file pm_mpps.h
*/

/*===========================================================================

            P M _ M P P S   H E A D E R    F I L E

DESCRIPTION
    This file contains functions prototypes and variable/type/constant 
  declarations to support the different voltage regulators inside the 
  Qualcomm Power Manager ICs.
===========================================================================*/
/*
 *  Copyright (c) 2003-2013 Qualcomm Technologies, Inc.
 *  All Rights Reserved.
 *  Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
/*===========================================================================

                      EDIT HISTORY FOR FILE
                                                                         
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/api/systemdrivers/pmic/pm_mpps.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/16/14   vk      Techpubs tagging for doc output
                   Expose pm_mpp_enable API
07/05/12   hs      Updated the interface.
03/02/12   hs	   Removed deprecated APIs.
05/03/11   dy      Rename digital logic level enumerations
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
07/26/10   wra     Changed documentation from @example to Examples so Deoxygen 
                    can parse the file
07/11/10   jtn     Aded APIs to get MPP status, corrected pm_mpp_dlogic_inout_pup_type 
                   to reflect PMIC HW capabilities 
07/02/10   wra     Changed pm_mpp_which_type to int
03/15/10   fpe     CMI Merge - Made the interface common between 7x30 and SCMM
03/05/10   vk      Added API pm_mpp_config_dtest_output()
10/20/09   jtn     Move init function prototype to pm_device_init_i.h
10/19/09   vk      Removed init API
07/06/09   jtn     Added enums for PM8028 digital logic levels
01/26/09   Vk      Introduced following APIs
                   pm_config_secure_mpp_config_digital_input()
                   pm_secure_mpp_config_digital_input()
12/17/08   APU     Introduced the APIs:
                   1. pm_secure_mpp_config_i_sink ()
                   2. pm_make_secure__mpp_config_i_sink ()
08/01/08   jtn     Move all proc comm functions to pm_pcil.c
06/27/08   jtn     Merge changes from QSC1100 branch, consolidated 
                   definition for max. number of MPPs
06/24/08   vk      Provided API that maintains the list of MPPs that have shunt
                   capacitors to ground. In the MPP configuration API for 
                   anaolog output this list is used.
03/01/08   jtn     Corrected number of MPPs for Han
02/27/08   jtn     Added support for Kip PMIC
01/07/08   jtn     Merged from QSC6270 branch
(begin QSC6270 changes) 
10/11/07   jnoblet Added MPPs for Han
(end QSC6270 changes) 
11/13/07   jtn     Added pm_config_secure_mpp_config_digital_output and 
                   pm_secure_mpp_config_digital_output to support RPC access
                   to MPP digital outputs
06/18/07   cng     Added an input parameter to pm_mpp_config_analog_output
                   to allow configurable analog output voltage 
05/18/07   hs      Restored the third parameter in pm_mpp_config_digital_input()
09/08/06   cng     Removed API pm_mpp_config_d_test and enum pm_mpp_dtest_in_dbus_type
06/22/06   hs      Removed the third parameter in pm_mpp_config_digital_input()
06/08/06   hs      Added pm_mpp_config_atest API support
11/09/05   cng     Added PM6620 support
10/24/05   cng     Added Panoramix support; Removed MPP config type enum;
                   Corrected number of MPPs for PM6650 
07/25/05   cng     PM7500 support
07/01/05   cng     Added MPP config type enum
03/15/05   cng     For the analog input function, added the functionality to
                   route the MPP input to one of the 3 analog buses 
01/28/04   rmd     Added initial support for multiple PMIC models/tiers.
11/03/03   rmd     Updated the settings for the BIDIRECTIONAL the pull up 
                   resistor.
09/08/03   rmd     enum "pm_mpp_aout_switch_type" was in the wrong order.
08/11/03   rmd     Created.
===========================================================================*/

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

#include "comdef.h"
#include "pm_err_flags.h"

/** @addtogroup pm_mpps
@{ */

/** Which MPP to be configured. */
typedef enum
{
  PM_MPP_1,   /**< MPP 1. */
  PM_MPP_2,   /**< MPP 2. */
  PM_MPP_3,   /**< MPP 3. */
  PM_MPP_4,   /**< MPP 4. */
  PM_MPP_5,   /**< MPP 5. */
  PM_MPP_6,   /**< MPP 6. */
  PM_MPP_7,   /**< MPP 7. */
  PM_MPP_8,   /**< MPP 8. */
  PM_MPP_9,   /**< MPP 9. */
  PM_MPP_10,  /**< MPP 10. */
  PM_MPP_11,  /**< MPP 11. */
  PM_MPP_12,  /**< MPP 12. */
  PM_MPP_13,  /**< MPP 13. */
  PM_MPP_14,  /**< MPP 14. */
  PM_MPP_15,  /**< MPP 15. */
  PM_MPP_16,  /**< MPP 16. */
  PM_MPP_INVALID,
}pm_mpp_which_type;

/*************************************************************************** 
   The types defined below are used to configure the following MPPs modes:
   DIGITAL INPUT, DIGITAL OUTPUT and BIDIRECTIONAL.
 ***************************************************************************/

/** Digital logic levels. */
typedef enum
{
  PM_MPP__DLOGIC__LVL_VIO_0 = 0,  /**< Voltage I/O level 0. */
  PM_MPP__DLOGIC__LVL_VIO_1,      /**< Voltage I/O level 1. */
  PM_MPP__DLOGIC__LVL_VIO_2,      /**< Voltage I/O level 2. */
  PM_MPP__DLOGIC__LVL_VIO_3,      /**< Voltage I/O level 3. */
  PM_MPP__DLOGIC__LVL_VIO_4,      /**< Voltage I/O level 4. */
  PM_MPP__DLOGIC__LVL_VIO_5,      /**< Voltage I/O level 5. */
  PM_MPP__DLOGIC__LVL_VIO_6,      /**< Voltage I/O level 6. */
  PM_MPP__DLOGIC__LVL_VIO_7,      /**< Voltage I/O level 7. */
  PM_MPP__DLOGIC__LVL_INVALID = 8,
  
} pm_mpp_dlogic_lvl_type;

/* MODE = DIGITAL INPUT. Configures the output logic. */
/** Data bus configuration for the digital input. */
typedef enum
{
  PM_MPP__DLOGIC_IN__DBUS1,  /**< DBUS 1. */
  PM_MPP__DLOGIC_IN__DBUS2,  /**< DBUS 2. */
  PM_MPP__DLOGIC_IN__DBUS3,  /**< DBUS 3. */
  PM_MPP__DLOGIC_IN__DBUS4,  /**< DBUS 4. */
  PM_MPP__DLOGIC_IN__DBUS_INVALID
}pm_mpp_dlogic_in_dbus_type;

/* MODE = DIGITAL OUT. Configures the output logic. */
//TODO: hshen: this enum seems obsolete for BADGER
typedef enum
{
  PM_MPP__DLOGIC_OUT__CTRL_LOW,    /* MPP OUTPUT= LOGIC LOW         */
  PM_MPP__DLOGIC_OUT__CTRL_HIGH,   /* MPP OUTPUT= LOGIC HIGH        */
  PM_MPP__DLOGIC_OUT__CTRL_MPP,    /* MPP OUTPUT= CORRESPONDING 
                                                  MPP INPUT         */
  PM_MPP__DLOGIC_OUT__CTRL_NOT_MPP,/* MPP OUTPUT= CORRESPONDING
                                                  INVERTED MPP INPUT*/
  PM_MPP__DLOGIC_OUT__CTRL_INVALID
}pm_mpp_dlogic_out_ctrl_type;

/* MODE = BIDIRECTIONAL. Configures the pull up resistor. */
/** Pull-up resistor configuration. */
typedef enum
{
  PM_MPP__DLOGIC_INOUT__PUP_1K,    /**< Pull-up = 1 K Ohm. */
  PM_MPP__DLOGIC_INOUT__PUP_10K,   /**< Pull-up = 10 K Ohms. */
  PM_MPP__DLOGIC_INOUT__PUP_30K,   /**< Pull-up = 30 K Ohms. */   
  PM_MPP__DLOGIC_INOUT__PUP_OPEN,  /**< Pull-up = OPEN. */
  PM_MPP__DLOGIC_INOUT__PUP_INVALID
}pm_mpp_dlogic_inout_pup_type;

/* MODE = DIGITAL TEST OUTPUT. */
/** Data bus configuration for the digital test output. */
typedef enum
{
  PM_MPP__DLOGIC_OUT__DBUS1,  /**< DBUS 1. */
  PM_MPP__DLOGIC_OUT__DBUS2,  /**< DBUS 2. */
  PM_MPP__DLOGIC_OUT__DBUS3,  /**< DBUS 3. */
  PM_MPP__DLOGIC_OUT__DBUS4,  /**< DBUS 4. */
  PM_MPP__DLOGIC_OUT__DBUS_INVALID
}pm_mpp_dlogic_out_dbus_type;

/*************************************************************************** 
   The types defined below are used to configure the following MPPs modes:
   ANALOG INPUT.
 ***************************************************************************/

/** Analog input channel types. */
typedef enum
{
  PM_MPP__AIN__CH_AMUX5,  /**< Analog input channel AMUX5. */
  PM_MPP__AIN__CH_AMUX6,  /**< Analog input channel AMUX6. */
  PM_MPP__AIN__CH_AMUX7,  /**< Analog input channel AMUX7. */
  PM_MPP__AIN__CH_AMUX8,  /**< Analog input channel AMUX8. */
  PM_MPP__AIN__CH_ABUS1,  /**< Analog input channel ABUS1. */
  PM_MPP__AIN__CH_ABUS2,  /**< Analog input channel ABUS2. */
  PM_MPP__AIN__CH_ABUS3,  /**< Analog input channel ABUS3. */
  PM_MPP__AIN__CH_ABUS4,  /**< Analog input channel ABUS4. */
  PM_MPP__AIN__CH_INVALID
}pm_mpp_ain_ch_type;

/*************************************************************************** 
   The types defined below are used to configure the following MPPs modes:
   ANALOG OUTPUT.
 ***************************************************************************/

/** Analog output voltage reference levels. */
typedef enum
{     
  PM_MPP__AOUT__LEVEL_VREF_1p25_Volts,    
    /**< Voltage reference level is 1.25 V. */ 
  PM_MPP__AOUT__LEVEL_VREF_0p625_Volts,
    /**< Voltage reference level is 0.625 V. */
  PM_MPP__AOUT__LEVEL_VREF_0p3125_Volts,
    /**< Voltage reference level is 0.3125 V. */
  PM_MPP__AOUT__LEVEL_INVALID
}pm_mpp_aout_level_type;

/** Analog output switch configuration. */
typedef enum
{  
  PM_MPP__AOUT__SWITCH_OFF,             /**< Switch is off. */
  PM_MPP__AOUT__SWITCH_ON,              /**< Switch is on. */
  PM_MPP__AOUT__SWITCH_ON_IF_MPP_HIGH,  /**< Switch is on if MPP is high. */
  PM_MPP__AOUT__SWITCH_ON_IF_MPP_LOW,   /**< Switch is on if MPP is low. */
  PM_MPP__AOUT__SWITCH_INVALID
}pm_mpp_aout_switch_type;

/*************************************************************************** 
   The types defined below are used to configure the following MPPs modes:
   CURRENT SINK.
 ***************************************************************************/

/** Current sink levels. */
typedef enum
{
  PM_MPP__I_SINK__LEVEL_5mA,   /**< Current sink level 5 mA. */
  PM_MPP__I_SINK__LEVEL_10mA,  /**< Current sink level 10 mA. */
  PM_MPP__I_SINK__LEVEL_15mA,  /**< Current sink level 15 mA. */
  PM_MPP__I_SINK__LEVEL_20mA,  /**< Current sink level 20 mA. */
  PM_MPP__I_SINK__LEVEL_25mA,  /**< Current sink level 25 mA. */
  PM_MPP__I_SINK__LEVEL_30mA,  /**< Current sink level 30 mA. */
  PM_MPP__I_SINK__LEVEL_35mA,  /**< Current sink level 35 mA. */
  PM_MPP__I_SINK__LEVEL_40mA,  /**< Current sink level 40 mA. */
  PM_MPP__I_SINK__LEVEL_INVALID
}pm_mpp_i_sink_level_type;

/** Current sink switch configuration. */
typedef enum
{
  PM_MPP__I_SINK__SWITCH_DIS,              /**< Current sink switch disabled. */
  PM_MPP__I_SINK__SWITCH_ENA,              /**< Current sink switch enabled. */
  PM_MPP__I_SINK__SWITCH_ENA_IF_MPP_HIGH,  /**< Current sink switch enabled if
                                                the MPP is high. */
  PM_MPP__I_SINK__SWITCH_ENA_IF_MPP_LOW,   /**< Current sink switch enabled if
                                                the MPP is low. */
  PM_MPP__I_SINK__SWITCH_INVALID
}pm_mpp_i_sink_switch_type;
/***************************************************************************/

/* ATEST*/
/** A-test configuration. */
typedef enum
{
  PM_MPP__CH_ATEST1,  /**< A-test 1. */
  PM_MPP__CH_ATEST2,  /**< A-test 2. */
  PM_MPP__CH_ATEST3,  /**< A-test 3. */
  PM_MPP__CH_ATEST4,  /**< A-test 4. */
  PM_MPP__CH_ATEST_INVALID
}pm_mpp_ch_atest_type;

/**
 * Structure used for returning the digital input configuration.
 */
typedef struct 
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;
      /**< Digital logic level. */
    pm_mpp_dlogic_in_dbus_type mpp_dlogic_in_dbus;
      /**< Digital logic in the D-bus. */
} pm_mpp_digital_input_status_type;

/**
 Structure used for returning the digital output configuration.
 */
typedef struct 
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;
      /**< Digital logic level. */
    pm_mpp_dlogic_out_ctrl_type mpp_dlogic_out_ctrl;
      /**< Digital logic output control. */
} pm_mpp_digital_output_status_type;

/**
 Structure used for returning the bidirectional configuration.
 */
typedef struct
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;
      /**< Digital logic level. */
    pm_mpp_dlogic_inout_pup_type mpp_dlogic_inout_pup;
      /**< Digital logic input/output pull-up. */
}  pm_mpp_bidirectional_status_type;

/**
 * Structure used for returning analog input configuration.
 */
typedef struct
{
    pm_mpp_ain_ch_type mpp_ain_ch;
      /**< Analog input channel. */
}  pm_mpp_analog_input_status_type;

/**
 Structure used for returning the analog output configuration.
 */
typedef struct
{
    pm_mpp_aout_level_type mpp_aout_level;
      /**< Analog output level. */
    pm_mpp_aout_switch_type mpp_aout_switch;
      /**< Analog output switch. */
}  pm_mpp_analog_output_status_type;

/**
 Structure used for returning current sink configuration.
 */
typedef struct
{
    pm_mpp_i_sink_level_type  mpp_i_sink_level;
      /**< Current sink level. */
    pm_mpp_i_sink_switch_type mpp_i_sink_switch;
      /**< Current sink switch. */
}  pm_mpp_current_sink_status_type;

/**
  Structure used for returning the DTEST output configuration.
 */
typedef struct
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;
      /**< Digital logic level. */
    pm_mpp_dlogic_out_dbus_type  mpp_dlogic_out_dbus;
      /**< Digital logic output Dbus. */
} pm_mpp_dtest_output_status_type;

/**
  Enumeration of available MPP modes.
 */
typedef enum pm_drv_mpp_config_type
{
    PM_MPP_DIG_IN, // - 0: Digital Input
      /**< Digital input. */
    PM_MPP_DIG_OUT, //- 1: Digital Output
      /**< Digital output. */
    PM_MPP_DIG_IN_DIG_OUT, //- 2: Digital Input and Digital Output
      /**< Digital input and digital output. */
    PM_MPP_BI_DIR, //- 3: Bidirectional Logic
      /**< Bidirectional logic. */
    PM_MPP_ANALOG_IN, //- 4: Analog Input
      /**< Analog input. */
    PM_MPP_ANALOG_OUT, //- 5: Analog Output
      /**< Analog output. */
    PM_MPP_I_SINK, //- 6: Current Sink
      /**< Current sink. */
    PM_MPP_TYPE_INVALID
} pm_drv_mpp_config_type;

/**
 Structure type used for returning MPP status.
 */
typedef struct s_pm_mpp_status_type
{
    pm_drv_mpp_config_type mpp_config; 
      /**< Returns the MPP's mode (input, output, etc.) */

    /**
     Union of structures used to return MPP status.  The structure
     that has valid data will depend on the MPP mode (input,
     output, etc.) returned in mpp_config.
     */
    union 
    {
        pm_mpp_digital_input_status_type mpp_digital_input_status;   /*~ IF ( _DISC_ == 0 ) s_pm_mpp_status_type.mpp_config_settings.mpp_digital_input_status */
		/**< Digital input status. */
        pm_mpp_digital_output_status_type mpp_digital_output_status; /*~ IF ( _DISC_ == 1 ) s_pm_mpp_status_type.mpp_config_settings.mpp_digital_output_status */
        /**< Digigal output status. */
        pm_mpp_bidirectional_status_type mpp_bidirectional_status;   /*~ IF ( _DISC_ == 2 ) s_pm_mpp_status_type.mpp_config_settings.mpp_bidirectional_status */
        /**< Bidirectional status. */
        pm_mpp_analog_input_status_type mpp_analog_input_status;     /*~ IF ( _DISC_ == 3 ) s_pm_mpp_status_type.mpp_config_settings.mpp_analog_input_status */
        /**< Analog input status. */
        pm_mpp_analog_output_status_type mpp_analog_output_status;   /*~ IF ( _DISC_ == 4 ) s_pm_mpp_status_type.mpp_config_settings.mpp_analog_output_status */
        /**< Analog output status. */
        pm_mpp_current_sink_status_type mpp_current_sink_status;     /*~ IF ( _DISC_ == 5 ) s_pm_mpp_status_type.mpp_config_settings.mpp_current_sink_status */
        /**< Current sink status. */
        pm_mpp_dtest_output_status_type mpp_dtest_output_status;     /*~ IF ( _DISC_ == 6 ) s_pm_mpp_status_type.mpp_config_settings.mpp_dtest_output_status */
	/**< D-test output status. */
    } mpp_config_settings;                                           /*~ FIELD s_pm_mpp_status_type.mpp_config_settings DISC s_pm_mpp_status_type.mpp_config */
} pm_mpp_status_type;

/**
  Returns the status of one of the PMIC MPPs.
  
  @param[in] pmic_chip Select the device index.
  @param[in] perph_index MPP to configure. See #pm_mpp_which_type.
  @param[out] mpp_status Pointer to the MPP status structure #s_pm_mpp_status_type
  
  @return 
   SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_mpp_status_get(uint8 pmic_chip, pm_mpp_which_type perph_index, pm_mpp_status_type *mpp_status);

/*~ FUNCTION  pm_mpp_status_get */
/*~ PARAM OUT mpp_status POINTER */
/*===========================================================================
                          FUNCTION DEFINITIONS
===========================================================================*/

/*===========================================================================
FUNCTION pm_mpp_config_digital_input                    EXTERNAL FUNCTION
===========================================================================*/

/**
  Configures the selected Multi Purpose Pin (MPP) to be a digital input pin.

@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.
@param[in] level Logic level reference to use with this MPP. See
                 #pm_mpp_dlogic_lvl_type.
@param[in] dbus Data line the MPP is to drive. Only one MPP 
                can be assigned per DBUS.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
  The following functions must have been called: \n
  pm_init()

@sideeffects
  Interrupts are disabled while communicating with the PMIC.
               
 <b>Example </b> \n
    Configure MPP5 to be a digital input; set the voltage reference
    to MSMP VREG; set the drive Dbus to 2:
@code        
  errFlag = pm_mpp_config_digital_input(PM_MPP_5, 
                                         PM_MPP__DLOGIC__LVL_MSMP,
                                          PM_MPP__DLOGIC_IN__DBUS2); @endcode
        
@note1hang The MPPs can be paired as MPP(input) -> MPP(output) or  
        MPP(Bidirec) <-> MPP(Bidirec) according to following: \n

        MPP level translator pairs: \n
          -  MPP1   <->  MPP2 \n
          -  MPP3   <->  MPP4 \n
          -  MPP5   <->  MPP6 \n
          -  MPP7   <->  MPP8 \n
          -  MPP9   <->  MPP10 \n
          -  MPP11  <->  MPP12 
*/
extern pm_err_flag_type pm_mpp_config_digital_input(
                                          uint8                    pmic_chip, 
                                          pm_mpp_which_type           perph_index,
                                          pm_mpp_dlogic_lvl_type      level,
                                          pm_mpp_dlogic_in_dbus_type  dbus);

/*===========================================================================
FUNCTION pm_mpp_config_digital_output                      EXTERNAL FUNCTION
===========================================================================*/

/**
  Configures the selected MPP to be a digital output pin.

@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.
@param[in] level Logic level reference to use with this MPP. See
                 #pm_mpp_dlogic_lvl_type.
@param[in] output_ctrl Output setting of the selected MPP.      

@return 
  SUCCESS or Error -- See #pm_err_flag_type.

@dependencies
  The following functions must have been called: \n
  pm_init()

@sideeffects
  Interrupts are disabled while communicating with the PMIC.
     
 <b>Example </b> \n
    Configure MPP10 to be a digital output. Set the MPP voltage reference to 
	RUIM VREG. The Digital Ouput is equal to the input of MPP9 (See #pm_mpp_dlogic_lvl_type):
@code      
  err |= pm_mpp_config_digital_output(PM_MPP_10, 
                                       PM_MPP__DLOGIC__LVL_RUIM, 
                                        PM_MPP__DLOGIC_OUT__CTRL_MPP); @endcode
        
@note1hang The MPPs can be paired as MPP(input) -> MPP(output) or  
        MPP(Bidirec) <-> MPP(Bidirec) according to the following: \n

        MPP level translator pairs: \n
          -  MPP1   <->  MPP2 \n
          -  MPP3   <->  MPP4 \n
          -  MPP5   <->  MPP6 \n
          -  MPP7   <->  MPP8 \n
          -  MPP9   <->  MPP10 \n
          -  MPP11  <->  MPP12 
*/
extern pm_err_flag_type pm_mpp_config_digital_output(
                                    uint8 pmic_chip, 
                                    pm_mpp_which_type           perph_index,
                                    pm_mpp_dlogic_lvl_type      level,
                                    pm_mpp_dlogic_out_ctrl_type output_ctrl);


/*===========================================================================
FUNCTION pm_mpp_config_digital_inout                    EXTERNAL FUNCTION
===========================================================================*/

/**
  Configures the selected MPP to be a digital 
  bidirectional pin.
                                           
@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.
@param[in] level Logic level reference to use with this MPP. See
                 #pm_mpp_dlogic_lvl_type.
@param[in] pull_up Pull-up resistor setting of the selected MPP. See
                   #pm_mpp_dlogic_inout_pup_type.
  
@return 
  SUCCESS or Error -- See #pm_err_flag_type.
        
@dependencies
  The following functions must have been called: \n
  pm_init()

@sideeffects
  Interrupts are disabled while communicating with the PMIC.

 <b>Example </b> \n
      Configure MPP11 and MPP12 to be bidirectionial (see Note).
      Set their logic voltage reference to RUIM VREG.
    Set up the pull-up resistor setting to 30K Ohms: \n
@code      
      err =  pm_mpp_config_digital_inout( PM_MPP_11, 
                                          PM_MPP__DLOGIC__LVL_RUIM,
                                          PM_MPP__DLOGIC_INOUT__PUP_30K);

      err |= pm_mpp_config_digital_inout( PM_MPP_12, 
                                          PM_MPP__DLOGIC__LVL_RUIM,
                                      PM_MPP__DLOGIC_INOUT__PUP_30K); @endcode

@note1hang The MPPs can be paired as MPP(input) -> MPP(output) or  
        MPP(Bidirec) <-> MPP(Bidirec) according to the following: \n
                                           
        MPP level translator pairs: \n
           - MPP1   <->  MPP2 \n
           - MPP3   <->  MPP4 \n
           - MPP5   <->  MPP6 \n
           - MPP7   <->  MPP8 \n
           - MPP9   <->  MPP10 \n
           - MPP11  <->  MPP12
*/
extern pm_err_flag_type pm_mpp_config_digital_inout(
                                    uint8                     pmic_chip, 
                                    pm_mpp_which_type           perph_index,
                                    pm_mpp_dlogic_lvl_type       level,
                                    pm_mpp_dlogic_inout_pup_type pull_up);



/*===========================================================================
FUNCTION pm_mpp_config_dtest_output                      EXTERNAL FUNCTION
===========================================================================*/

/**
  Configures the selected MPP to be a digital test output pin.

@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.
@param[in] level Logic level reference to use with this MPP. See
                 #pm_mpp_dlogic_lvl_type.
@param[in] dbus Data line the MPP is to drive. See
                #pm_mpp_dlogic_out_dbus_type.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.
                                                
@dependencies
  The following functions must have been called: \n
  pm_init()
        
@sideeffects
  Interrupts are disabled while communicating with the PMIC.

 <b>Example </b> \n
    Configure MPP10 to be a digital test output.
     Set the MPP voltage reference to RUIM VREG and connect it to DBUS1.
@code            
  err |= pm_mpp_config_dtest_output(PM_MPP_10, 
                                       PM_MPP__DLOGIC__LVL_RUIM, 
                                        PM_MPP__DLOGIC_IN__DBUS1); @endcode
*/
pm_err_flag_type pm_mpp_config_dtest_output(
                                             uint8                    pmic_chip, 
                                             pm_mpp_which_type          perph_index,
                                             pm_mpp_dlogic_lvl_type      level,
                                             pm_mpp_dlogic_out_dbus_type dbus);

/*===========================================================================
FUNCTION pm_mpp_config_analog_input                    EXTERNAL FUNCTION
===========================================================================*/

/**
    Configures the selected MPP as an analog input.

@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.
@param[in] ch_select Analog MUX or analog bus to which the selected MPP is
                     to be routed. See #pm_mpp_ain_ch_type.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.
     
@dependencies
  The following functions must have been called: \n
  pm_init()
        
@sideeffects
  Interrupts are disabled while communicating with the PMIC.

 <b>Example </b> \n
  Configure MPP4 to be an analog input and connect it to analog MUX6:
@code        
   err = pm_mpp_config_analog_input(PM_MPP_6,PM_MPP__AIN__CH_AMUX9); @endcode
*/
extern pm_err_flag_type pm_dev_mpp_config_analog_input(
                                    uint8             pmic_chip, 
                                    pm_mpp_which_type   perph_index,
                                    pm_mpp_ain_ch_type   ch_select);
//#pragma message("Do not use pm_mpp_config_analog_input(), which is deprecated. Use pm_dev_mpp_config_analog_input() instead.")
extern pm_err_flag_type pm_mpp_config_analog_input(
    pm_mpp_which_type   perph_index,
    pm_mpp_ain_ch_type   ch_select);


/*===========================================================================
FUNCTION pm_mpp_config_analog_output                    EXTERNAL FUNCTION
===========================================================================*/

/**
    Configures the selected MPP as an analog output.

@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.
@param[in] level Specifies the analog output reference voltage level. See
                 #pm_mpp_aout_level_type.
@param[in] OnOff Enable/disable the MPP output. See #pm_mpp_aout_switch_type.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.
     
@dependencies
  The following functions must have been called: \n
  pm_init()
        
@sideeffects
  Interrupts are disabled while communicating with the PMIC.

 <b>Example </b> \n
  Configure MPP14 to be an analog out and turn it on:
@code        
   err = pm_mpp_config_analog_output(PM_MPP_14,PM_MPP__AOUT__SWITCH_ON); @endcode
*/
extern pm_err_flag_type pm_dev_mpp_config_analog_output(
                                    uint8                 pmic_chip, 
                                    pm_mpp_which_type       perph_index,
                                    pm_mpp_aout_level_type   level,
                                    pm_mpp_aout_switch_type  OnOff);
//#pragma message("Do not use pm_mpp_config_analog_output(), which is deprecated. Use pm_dev_mpp_config_analog_output() instead.")
extern pm_err_flag_type pm_mpp_config_analog_output(
    pm_mpp_which_type       perph_index,
    pm_mpp_aout_level_type   level,
    pm_mpp_aout_switch_type  OnOff);

/*===========================================================================
FUNCTION pm_mpp_config_i_sink                           EXTERNAL FUNCTION
===========================================================================*/

/**
  Configures the selected MPP as a current sink.

@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.
@param[in] level Amount of current to allow the MPP to sink. See
                 #pm_mpp_i_sink_level_type.
@param[in] OnOff Enable/disable the current sink. See 
                 #pm_mpp_i_sink_switch_type.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.
                 
@dependencies
  The following functions must have been called: \n
  pm_init()
        
@sideeffects
  Interrupts are disabled while communicating with the PMIC.

 <b>Example </b> \n
  Configure MPP7 to be a current sink. Set the current sink to 25 mA and
  enable it:
@code    
   err = pm_mpp_config_i_sink(PM_MPP_15, 
                                  PM_MPP__I_SINK__LEVEL_25mA,
                                      PM_MPP__I_SINK__SWITCH_ENA); @endcode
*/
extern pm_err_flag_type pm_dev_mpp_config_i_sink(
                                    uint8                  pmic_chip, 
                                    pm_mpp_which_type        perph_index,
                                    pm_mpp_i_sink_level_type  level,
                                    pm_mpp_i_sink_switch_type OnOff);
//#pragma message("Do not use pm_mpp_config_i_sink(), which is deprecated. Use pm_dev_mpp_config_i_sink() instead.")
extern pm_err_flag_type pm_mpp_config_i_sink(
    pm_mpp_which_type        perph_index,
    pm_mpp_i_sink_level_type  level,
    pm_mpp_i_sink_switch_type OnOff);


/*===========================================================================
FUNCTION pm_mpp_config_atest                           EXTERNAL FUNCTION
===========================================================================*/

/**
    Configures the selected MPP as A-test.

@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.
@param[in] atest_select A-test to which the selected MPP is to be routed.
                        See #pm_mpp_ch_atest_type.
     
@return 
  SUCCESS or Error -- See #pm_err_flag_type.
        
@dependencies
  The following functions must have been called: \n
  pm_init()

@sideeffects
  Interrupts are disabled while communicating with the PMIC.
*/
extern pm_err_flag_type pm_mpp_config_atest(
                                    uint8             pmic_chip, 
                                    pm_mpp_which_type   perph_index,
                                    pm_mpp_ch_atest_type atest_select);

/*===========================================================================
FUNCTION pm_mpp_set_list_mpp_with_shunt_cap                 EXTERNAL FUNCTION
===========================================================================*/

/**
  Creates an array of MPPs that have shunt capacitors to ground.
     
@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.

@return 
  SUCCESS or Error -- See #pm_err_flag_type.
        
@dependencies
  The following functions must have been called: \n
  pm_init()

@sideeffects
  Interrupts are disabled while communicating with the PMIC.

 <b>Example </b> \n
  Configure MPP8 with a shunt capacitor to ground:
@code    
   err = pm_mpp_set_list_mpp_with_shunt_cap(PM_MPP_8); @endcode
*/

pm_err_flag_type pm_mpp_set_list_mpp_with_shunt_cap(uint8 pmic_chip, pm_mpp_which_type perph_index);



/*===========================================================================

FUNCTION pm_get_mpp_with_shunt_cap_list_status_for_device             EXTERNAL FUNCTION
===========================================================================*/

/**
    Returns the 32-bit status as to whether the corresponding
    MPP has a shunt capacitor to ground;
    e.g., if MPP8 and MPP1 have shunt capacitors to ground: \n
        status = 00000000 00000000 00000000 10000001

@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.
@param[in] shuntList List of shunt capacitors.

@return
  A 32-bit uint32 value representing whether the corresponding MPP has a shunt 
    capacitor to ground.

@dependencies
  None.
*/
pm_err_flag_type pm_get_mpp_with_shunt_cap_list_status_for_device(uint8 pmic_chip, pm_mpp_which_type perph_index, uint32* shuntList);


/*===========================================================================
FUNCTION pm_mpp_enable                           EXTERNAL FUNCTION
===========================================================================*/

/**
    Enable/disable MPP.

@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.
@param[in] enable TRUE: turn on MPP, FALSE: turn off MPP.
     
@return 
  SUCCESS or Error -- See #pm_err_flag_type.
        
@dependencies
  The following functions must have been called: \n
  pm_init()

@sideeffects
  Interrupts are disabled while communicating with the PMIC.
*/
extern pm_err_flag_type pm_mpp_enable(uint8 pmic_chip, pm_mpp_which_type  perph_index, boolean enable);

/** @} */ /* end_addtogroup pm_mpps */

#endif /* PM66XXMPPS_H */
