#ifndef PMAPP_NPA_H
#define PMAPP_NPA_H

/*! \file  pmapp_npa.h
*  
 *  \brief  File Contains the PMIC NPA CMI Code
 *  \details  This file contains the needed definition and enum for PMIC NPA layer.
*
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Locked Version: pmd9x45_12_02_2014_v01 - LOCKED

 *    This file contains code for Target specific settings and modes.
*
 *  &copy; Copyright 2014 Qualcomm Technologies, All Rights Reserved
*/

/*===========================================================================

                EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module over time.

$Header: //components/rel/core.mpss/3.4.c3.11/api/systemdrivers/pmic/pmapp_npa.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/

/*===========================================================================

                        DEFINITION

===========================================================================*/


/*===========================================================================

                        DEFINITIONS

===========================================================================*/
/** @addtogroup pmapp_npa
@{ */

/*
Generic GROUP IDs/Mode IDs that can be used by clients to configure the resources in
ACTIVE/STANDBY states.
 */

#define PMIC_NPA_GROUP_ID_ANT1 "/pmic/client/ant1"
#define PMIC_NPA_GROUP_ID_HFPLL1 "/pmic/client/hfpll1"
#define PMIC_NPA_GROUP_ID_HKADC "/pmic/client/hkadc"
#define PMIC_NPA_GROUP_ID_MCPM_DIG "/pmic/client/mcpm_dig"
#define PMIC_NPA_GROUP_ID_MCPM_MEM "/pmic/client/mcpm_mem"
#define PMIC_NPA_GROUP_ID_PA1 "/pmic/client/pa1"
#define PMIC_NPA_GROUP_ID_PLL_HF "/pmic/client/pll_hf"
#define PMIC_NPA_GROUP_ID_PLL_SR2 "/pmic/client/pll_sr2"
#define PMIC_NPA_GROUP_ID_PLL_TS "/pmic/client/pll_ts"
#define PMIC_NPA_GROUP_ID_QFPROM "/pmic/client/qfprom"
#define PMIC_NPA_GROUP_ID_RAIL_CX "/pmic/client/rail_cx"
#define PMIC_NPA_GROUP_ID_RAIL_MSS "/pmic/client/rail_mss"
#define PMIC_NPA_GROUP_ID_RAIL_MX "/pmic/client/rail_mx"
#define PMIC_NPA_GROUP_ID_RF "/pmic/client/rf"
#define PMIC_NPA_GROUP_ID_RF1 "/pmic/client/rf1"
#define PMIC_NPA_GROUP_ID_RF1_MBB "/pmic/client/rf1_mbb"
#define PMIC_NPA_GROUP_ID_RF1_TECH_1X "/pmic/client/rf1_tech_1x"
#define PMIC_NPA_GROUP_ID_RF1_TECH_1X_MBB "/pmic/client/rf1_tech_1x_mbb"
#define PMIC_NPA_GROUP_ID_RF1_TECH_DO "/pmic/client/rf1_tech_do"
#define PMIC_NPA_GROUP_ID_RF1_TECH_DO_MBB "/pmic/client/rf1_tech_do_mbb"
#define PMIC_NPA_GROUP_ID_RF1_TECH_GPS "/pmic/client/rf1_tech_gps"
#define PMIC_NPA_GROUP_ID_RF1_TECH_GPS_ELNA "/pmic/client/rf1_tech_gps_elna"
#define PMIC_NPA_GROUP_ID_RF1_TECH_GPS_ELNA2 "/pmic/client/rf1_tech_gps_elna2"
#define PMIC_NPA_GROUP_ID_RF1_TECH_GPS_ELNA3 "/pmic/client/rf1_tech_gps_elna3"
#define PMIC_NPA_GROUP_ID_RF1_TECH_GSM "/pmic/client/rf1_tech_gsm"
#define PMIC_NPA_GROUP_ID_RF1_TECH_GSM_MBB "/pmic/client/rf1_tech_gsm_mbb"
#define PMIC_NPA_GROUP_ID_RF1_TECH_GSM2 "/pmic/client/rf1_tech_gsm2"
#define PMIC_NPA_GROUP_ID_RF1_TECH_GSM3 "/pmic/client/rf1_tech_gsm3"
#define PMIC_NPA_GROUP_ID_RF1_TECH_INIT "/pmic/client/rf1_tech_init"
#define PMIC_NPA_GROUP_ID_RF1_TECH_LTE "/pmic/client/rf1_tech_lte"
#define PMIC_NPA_GROUP_ID_RF1_TECH_LTE_MBB "/pmic/client/rf1_tech_lte_mbb"
#define PMIC_NPA_GROUP_ID_RF1_TECH_LTE2 "/pmic/client/rf1_tech_lte2"
#define PMIC_NPA_GROUP_ID_RF1_TECH_LTE2_MBB "/pmic/client/rf1_tech_lte2_mbb"
#define PMIC_NPA_GROUP_ID_RF1_TECH_LTE_U "/pmic/client/rf1_tech_lte_u"
#define PMIC_NPA_GROUP_ID_RF1_TECH_TDSCDMA "/pmic/client/rf1_tech_tdscdma"
#define PMIC_NPA_GROUP_ID_RF1_TECH_TDSCDMA_MBB "/pmic/client/rf1_tech_tdscdma_mbb"
#define PMIC_NPA_GROUP_ID_RF1_TECH_WCDMA "/pmic/client/rf1_tech_wcdma"
#define PMIC_NPA_GROUP_ID_RF1_TECH_WCDMA_MBB "/pmic/client/rf1_tech_wcdma_mbb"
#define PMIC_NPA_GROUP_ID_RF2_TECH_1X "/pmic/client/rf2_tech_1x"
#define PMIC_NPA_GROUP_ID_RF2_TECH_DO "/pmic/client/rf2_tech_do"
#define PMIC_NPA_GROUP_ID_RF2_TECH_GPS "/pmic/client/rf2_tech_gps"
#define PMIC_NPA_GROUP_ID_RF2_TECH_GSM "/pmic/client/rf2_tech_gsm"
#define PMIC_NPA_GROUP_ID_RF2_TECH_GSM2 "/pmic/client/rf2_tech_gsm2"
#define PMIC_NPA_GROUP_ID_RF2_TECH_GSM3 "/pmic/client/rf2_tech_gsm3"
#define PMIC_NPA_GROUP_ID_RF2_TECH_LTE "/pmic/client/rf2_tech_lte"
#define PMIC_NPA_GROUP_ID_RF2_TECH_LTE2 "/pmic/client/rf2_tech_lte2"
#define PMIC_NPA_GROUP_ID_RF2_TECH_TDSCDMA "/pmic/client/rf2_tech_tdscdma"
#define PMIC_NPA_GROUP_ID_RF2_TECH_WCDMA "/pmic/client/rf2_tech_wcdma"
#define PMIC_NPA_GROUP_ID_UIM1_ACTIVITY "/pmic/client/uim1_activity"
#define PMIC_NPA_GROUP_ID_UIM1_ENABLE "/pmic/client/uim1_enable"
#define PMIC_NPA_GROUP_ID_UIM2_ACTIVITY "/pmic/client/uim2_activity"
#define PMIC_NPA_GROUP_ID_UIM2_ENABLE "/pmic/client/uim2_enable"
#define PMIC_NPA_GROUP_ID_UIM3_ACTIVITY "/pmic/client/uim3_activity"
#define PMIC_NPA_GROUP_ID_UIM3_ENABLE "/pmic/client/uim3_enable"
#define PMIC_NPA_GROUP_ID_MEM_UVOL "/pmic/client/vdd_mem_uvol"

/*===========================================================================

                        ENUMERATION

===========================================================================*/

/**
PMIC_NPA_MODE_ID -- Generic MODEs used by clients for resource settings. 
Resource settings are part of the PAM table. \n
\n
Modes used by PMIC_NPA_GROUP_ID_ANT1. */

enum
{
   PMIC_NPA_MODE_ID_ANT_MODE_0 = 0, /**< PMIC_NPA_MODE_ID_RF_ANT_SLEEP */
   PMIC_NPA_MODE_ID_ANT_MODE_1 = 1, /**< PMIC_NPA_MODE_ID_RF_ANT_ACTIVE */
   PMIC_NPA_MODE_ID_ANT_MODE_2 = 2, /**< Not used */
   PMIC_NPA_MODE_ID_ANT_MAX = 3, /**< Not used */
};

/** Generic mode ids used by QFPROM/HFPLL1/PLL_SR2 clients. */
enum
{
   PMIC_NPA_MODE_ID_GENERIC_STANDBY = 0, /**< STANDBY */
   PMIC_NPA_MODE_ID_GENERIC_ACTIVE = 1, /**< ACTIVE */
   PMIC_NPA_MODE_ID_GENERIC_MAX = 2, /**< MAX */
};

enum
{
   PMIC_NPA_MODE_ID_CORNER_LEVEL_NO_VOTE = 0,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_1 = 1,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_2 = 2,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_3 = 3,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_4 = 4,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_5 = 5,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_6 = 6,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_7 = 7,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_8 = 8,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_MAX = 9,
};

enum
{
   PMIC_NPA_MODE_ID_HKADC_OFF = 0,
   PMIC_NPA_MODE_ID_HKADC_ACTIVE = 1,
   PMIC_NPA_MODE_ID_HKADC_MAX = 2,
};

enum
{
   PMIC_NPA_MODE_ID_MCPM_MODE_0 = 0,
   PMIC_NPA_MODE_ID_MCPM_MODE_1 = 1,
   PMIC_NPA_MODE_ID_MCPM_MODE_2 = 2,
   PMIC_NPA_MODE_ID_MCPM_MODE_3 = 3,
   PMIC_NPA_MODE_ID_MCPM_MODE_4 = 4,
   PMIC_NPA_MODE_ID_MCPM_MODE_5 = 5,
   PMIC_NPA_MODE_ID_MCPM_MODE_6 = 6,
   PMIC_NPA_MODE_ID_MCPM_MODE_7 = 7,
   PMIC_NPA_MODE_ID_MCPM_MODE_8 = 8,
   PMIC_NPA_MODE_ID_MCPM_MODE_MAX = 9,
};

/** Mode ids used for PA control. */
enum
{
   PMIC_NPA_MODE_ID_PA_MODE_0 = 0, /**< SLEEP */
   PMIC_NPA_MODE_ID_PA_MODE_1 = 1, /**< ACTIVE */
   PMIC_NPA_MODE_ID_PA_MODE_2 = 2, /**< Not used */
   PMIC_NPA_MODE_ID_PA_MODE_3 = 3, /**< Not used */
};

/** Modes used by RAIL_MX,RAIL_CX GROUP IDs. */
enum
{
   PMIC_NPA_MODE_ID_CORE_RAIL_OFF = 0, /**< RAIL OFF */
   PMIC_NPA_MODE_ID_CORE_RAIL_RETENTION = 1, /**< RETENTION */
   PMIC_NPA_MODE_ID_CORE_RAIL_LOW_MINUS = 2, /**< LOW_MINUS */
   PMIC_NPA_MODE_ID_CORE_RAIL_LOW = 3, /**< SVS */
   PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL = 4, /**< NOMINAL */
   PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL_PLUS = 5, /**< NOMINAL_PLUS */
   PMIC_NPA_MODE_ID_CORE_RAIL_TURBO = 6, /**< TURBO */
   PMIC_NPA_MODE_ID_CORE_RAIL_MAX = 7, /**< Max */
};

/** Generic modes used by all RF1_TECH GROUP IDs. */
enum
{
   PMIC_NPA_MODE_ID_RF_MODE_0 = 0, /**< SLEEP */
   PMIC_NPA_MODE_ID_RF_MODE_1 = 1, /**< DVS_LOW */
   PMIC_NPA_MODE_ID_RF_MODE_2 = 2, /**< DVS_HIGH */
   PMIC_NPA_MODE_ID_RF_MODE_3 = 3, /**< optional DVS_LOW for 2nd RF card*/
   PMIC_NPA_MODE_ID_RF_MODE_4 = 4, /**< optional DVS_HIGH for 2nd RF card*/
   PMIC_NPA_MODE_ID_RF_MODE_5 = 5, /**< Not used */
   PMIC_NPA_MODE_ID_RF_MODE_6 = 6, /**< Not used */
   PMIC_NPA_MODE_ID_RF_MODE_MAX = 7, /**< Max */
};

enum
{
   PMIC_NPA_MODE_ID_RF_SLEEP = 0,
   PMIC_NPA_MODE_ID_RF_UMTS_NOMINAL = 1,
   PMIC_NPA_MODE_ID_RF_AUTOCAL = 1,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_C2K_RX = 2,
   PMIC_NPA_MODE_ID_RF_1X_NOMINAL = 2,
   PMIC_NPA_MODE_ID_RF_UMTS_SS = 3,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_C2K_RX = 3,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_C2K_RXTX = 4,
   PMIC_NPA_MODE_ID_RF_1X_SS = 4,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_C2K_RXTX = 5,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_UMTS_RX = 6,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_UMTS_RX = 7,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_UMTS_RXTX = 8,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_UMTS_RXTX = 9,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_LTE_EVDO_RX = 10,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_LTE_EVDO_RX = 11,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_LTE_EVDO_RXTX = 12,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_LTE_EVDO_RXTX = 13,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_SV_LTE_EVDO_RX = 14,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_SV_LTE_EVDO_RX = 15,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_SV_LTE_EVDO_RXTX = 16,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_SV_LTE_EVDO_RXTX = 17,
   PMIC_NPA_MODE_ID_RF_MAX = 18,
};


/** Modes used by PMIC_NPA_GROUP_ID_RF1_TECH_GPS. */
enum
{
   PMIC_NPA_MODE_ID_GPS_MODE_0 = 0, /**< STANDBY */
   PMIC_NPA_MODE_ID_GPS_MODE_1 = 1, /**< PMIC_NPA_MODE_ID_GPS_DVS_LOW_RX */
   PMIC_NPA_MODE_ID_GPS_MODE_2 = 2, /**< PMIC_NPA_MODE_ID_GPS_DVS_HIGH_RX */
   PMIC_NPA_MODE_ID_GPS_MODE_3 = 3, /**< PMIC_NPA_MODE_ID_GPS_DVS_LOW_RX_CRIT_DEMOD, PMIC_NPA_MODE_ID_GPS_DVS_HIGH_RX_CRIT_DEMOD */
   PMIC_NPA_MODE_ID_GPS_MODE_4 = 4, /**< Not used */
   PMIC_NPA_MODE_ID_GPS_MODE_5 = 5, /**< Not used */
   PMIC_NPA_MODE_ID_GPS_MODE_6 = 6, /**< Not used */
   PMIC_NPA_MODE_ID_GPS_MODE_MAX = 7, /**< MAX */
};

/** Generic modes used by UIM GROUP IDs. */
enum
{
   PMIC_NPA_MODE_ID_UIM_MODE_1 = 0, /**< STANDBY/NO_VOTE */
   PMIC_NPA_MODE_ID_UIM_MODE_2 = 1, /**< ACTIVE/UIM_CLASS_C_LOW */
   PMIC_NPA_MODE_ID_UIM_MODE_3 = 2, /**< UIM_CLASS_C */
   PMIC_NPA_MODE_ID_UIM_MODE_4 = 3, /**< ACTIVE/UIM_CLASS_C_HIGH */
   PMIC_NPA_MODE_ID_UIM_MODE_5 = 4, /**< UIM_CLASS_B_LOW */
   PMIC_NPA_MODE_ID_UIM_MODE_6 = 5, /**< UIM_CLASS_B */
   PMIC_NPA_MODE_ID_UIM_MODE_7 = 6, /**< UIM_CLASS_B_HIGH */
   PMIC_NPA_MODE_ID_UIM_MODE_8 = 7, /**< Not used */
   PMIC_NPA_MODE_ID_UIM_MODE_9 = 8, /**< Not used */
   PMIC_NPA_MODE_ID_UIM_MODE_10 = 9, /**< Not used */
   PMIC_NPA_MODE_ID_UIM_MODE_11 = 10, /**< Not used */
   PMIC_NPA_MODE_ID_UIM_MODE_12 = 11, /**< Not used */
   PMIC_NPA_MODE_ID_UIM_MODE_13 = 12, /**< Not used */
   PMIC_NPA_MODE_ID_UIM_MODE_MAX = 13, /**< Max */
};

enum
{
   PMIC_NPA_MODE_ID_UIM_STANDBY = 0,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_C_LOW = 1,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_C = 2,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_C_HIGH = 3,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_B_LOW = 4,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_B = 5,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_B_HIGH = 6,
   PMIC_NPA_MODE_ID_UIM_MAX = 7,
};

/** @} */ /* end_addtogroup pmapp_npa */
#endif // PMAPP_NPA_H

