#ifndef __PM_UICC_H__
#define __PM_UICC_H__

/*! \file pm_uicc.h
 *  \n
 *  \brief This file contains functions prototypes and variable/type/constant
 *  declarations to support the HOTSWAP (UICC) feature inside the Qualcomm
 *  PMIC chips
 *  \n
 *  \n &copy; Copyright 2011-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/api/systemdrivers/pmic/pm_uicc.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
02/24/11   dy      Add API to set hotswap polarity
09/14/11   dy      Created.
===========================================================================*/
/*===========================================================================

                        HEADER FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "com_dtypes.h"
#include "pm_bua.h"

/*===========================================================================

                        API PROTOTYPE

===========================================================================*/
/** @addtogroup pm_uicc
@{ */
/** 
 * @name pm_uicc_cntrl_enable 
 *  
 * @brief This function is used to enable/disable the UICC Hotswap module.
 * 
 * @param alarm: PM_BUA_BATT_ALARM for BATTERY and PM_BUA_UICCx_ALARM for
 *               the corresponding UIM slots.Refer to enum pm_bua_alarm_type
 *               in pm_bua.h file.
 * @param enable: TRUE to enable, FALSE to disable
 *
 * @return pm_err_flag_type 
 *         PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Failure in enabling/disabling
 *         the UICC feature or feature not supported.
 *         PM_ERR_FLAG__SUCCESS = Success.
 */
pm_err_flag_type pm_uicc_cntrl_enable(pm_bua_alarm_type alarm, boolean enable);

/** @} */ /* end_addtogroup pm_uicc */

#endif /* __PM_UICC_H__ */
