#ifndef __MSMHWIOBASE_H__
#define __MSMHWIOBASE_H__
/*
===========================================================================
*/
/**
  @file msmhwiobase.h
  @brief Auto-generated HWIO base include file.
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.4.c3.11/api/systemdrivers/hwio/msm8996/msmhwiobase.h#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * BASE: XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB
 *--------------------------------------------------------------------------*/

#define XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB_BASE                 0xe0030000
#define XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB_BASE_SIZE            0x00000400
#define XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB_BASE_PHYS            0x00030000

/*----------------------------------------------------------------------------
 * BASE: SPDM_WRAPPER_TOP
 *--------------------------------------------------------------------------*/

#define SPDM_WRAPPER_TOP_BASE                                       0xe0148000
#define SPDM_WRAPPER_TOP_BASE_SIZE                                  0x00008000
#define SPDM_WRAPPER_TOP_BASE_PHYS                                  0x00048000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_START_ADDRESS_BASE                           0xe0268000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_SIZE                      0x00006000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_PHYS                      0x00068000

/*----------------------------------------------------------------------------
 * BASE: SECURITY_CONTROL
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_BASE                                       0xe0370000
#define SECURITY_CONTROL_BASE_SIZE                                  0x00010000
#define SECURITY_CONTROL_BASE_PHYS                                  0x00070000

/*----------------------------------------------------------------------------
 * BASE: PRNG_CFG_PRNG_TOP
 *--------------------------------------------------------------------------*/

#define PRNG_CFG_PRNG_TOP_BASE                                      0xe0480000
#define PRNG_CFG_PRNG_TOP_BASE_SIZE                                 0x00010000
#define PRNG_CFG_PRNG_TOP_BASE_PHYS                                 0x00080000

/*----------------------------------------------------------------------------
 * BASE: BOOT_ROM
 *--------------------------------------------------------------------------*/

#define BOOT_ROM_BASE                                               0xe0500000
#define BOOT_ROM_BASE_SIZE                                          0x00100000
#define BOOT_ROM_BASE_PHYS                                          0x00100000

/*----------------------------------------------------------------------------
 * BASE: RPM
 *--------------------------------------------------------------------------*/

#define RPM_BASE                                                    0xe0600000
#define RPM_BASE_SIZE                                               0x00100000
#define RPM_BASE_PHYS                                               0x00200000

/*----------------------------------------------------------------------------
 * BASE: CLK_CTL
 *--------------------------------------------------------------------------*/

#define CLK_CTL_BASE                                                0xe0700000
#define CLK_CTL_BASE_SIZE                                           0x000a0000
#define CLK_CTL_BASE_PHYS                                           0x00300000

/*----------------------------------------------------------------------------
 * BASE: BIMC
 *--------------------------------------------------------------------------*/

#define BIMC_BASE                                                   0xe0800000
#define BIMC_BASE_SIZE                                              0x00080000
#define BIMC_BASE_PHYS                                              0x00400000

/*----------------------------------------------------------------------------
 * BASE: MPM2_MPM
 *--------------------------------------------------------------------------*/

#define MPM2_MPM_BASE                                               0xe09a0000
#define MPM2_MPM_BASE_SIZE                                          0x00010000
#define MPM2_MPM_BASE_PHYS                                          0x004a0000

/*----------------------------------------------------------------------------
 * BASE: CONFIG_NOC
 *--------------------------------------------------------------------------*/

#define CONFIG_NOC_BASE                                             0xe0a00000
#define CONFIG_NOC_BASE_SIZE                                        0x00000080
#define CONFIG_NOC_BASE_PHYS                                        0x00500000

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_NOC
 *--------------------------------------------------------------------------*/

#define SYSTEM_NOC_BASE                                             0xe0b20000
#define SYSTEM_NOC_BASE_SIZE                                        0x0000a100
#define SYSTEM_NOC_BASE_PHYS                                        0x00520000

/*----------------------------------------------------------------------------
 * BASE: A2_NOC_AGGRE2_NOC
 *--------------------------------------------------------------------------*/

#define A2_NOC_AGGRE2_NOC_BASE                                      0xe0c80000
#define A2_NOC_AGGRE2_NOC_BASE_SIZE                                 0x00008100
#define A2_NOC_AGGRE2_NOC_BASE_PHYS                                 0x00580000

/*----------------------------------------------------------------------------
 * BASE: CRYPTO0_CRYPTO_TOP
 *--------------------------------------------------------------------------*/

#define CRYPTO0_CRYPTO_TOP_BASE                                     0xe0d40000
#define CRYPTO0_CRYPTO_TOP_BASE_SIZE                                0x00040000
#define CRYPTO0_CRYPTO_TOP_BASE_PHYS                                0x00640000

/*----------------------------------------------------------------------------
 * BASE: IPA_0_IPA_WRAPPER
 *--------------------------------------------------------------------------*/

#define IPA_0_IPA_WRAPPER_BASE                                      0xe0e80000
#define IPA_0_IPA_WRAPPER_BASE_SIZE                                 0x00080000
#define IPA_0_IPA_WRAPPER_BASE_PHYS                                 0x00680000

/*----------------------------------------------------------------------------
 * BASE: CORE_TOP_CSR
 *--------------------------------------------------------------------------*/

#define CORE_TOP_CSR_BASE                                           0xe0f00000
#define CORE_TOP_CSR_BASE_SIZE                                      0x00100000
#define CORE_TOP_CSR_BASE_PHYS                                      0x00700000

/*----------------------------------------------------------------------------
 * BASE: TLMM
 *--------------------------------------------------------------------------*/

#define TLMM_BASE                                                   0xe1000000
#define TLMM_BASE_SIZE                                              0x00310000
#define TLMM_BASE_PHYS                                              0x01000000

/*----------------------------------------------------------------------------
 * BASE: MODEM_TOP
 *--------------------------------------------------------------------------*/

#define MODEM_TOP_BASE                                              0xec000000
#define MODEM_TOP_BASE_SIZE                                         0x01000000
#define MODEM_TOP_BASE_PHYS                                         0x02000000

/*----------------------------------------------------------------------------
 * BASE: QDSS_QDSS_ISTARI
 *--------------------------------------------------------------------------*/

#define QDSS_QDSS_ISTARI_BASE                                       0xe2000000
#define QDSS_QDSS_ISTARI_BASE_SIZE                                  0x00800000
#define QDSS_QDSS_ISTARI_BASE_PHYS                                  0x03000000

/*----------------------------------------------------------------------------
 * BASE: PMIC_ARB
 *--------------------------------------------------------------------------*/

#define PMIC_ARB_BASE                                               0xe3000000
#define PMIC_ARB_BASE_SIZE                                          0x01c00000
#define PMIC_ARB_BASE_PHYS                                          0x04000000

/*----------------------------------------------------------------------------
 * BASE: PERIPH_SS
 *--------------------------------------------------------------------------*/

#define PERIPH_SS_BASE                                              0xe5000000
#define PERIPH_SS_BASE_SIZE                                         0x00400000
#define PERIPH_SS_BASE_PHYS                                         0x07400000

/*----------------------------------------------------------------------------
 * BASE: MODEM
 *--------------------------------------------------------------------------*/

#define MODEM_BASE                                                  0xed000000
#define MODEM_BASE_SIZE                                             0x02000000
#define MODEM_BASE_PHYS                                             0x10000000


#endif /* __MSMHWIOBASE_H__ */
