#ifndef VSENSE_H
#define VSENSE_H


/*=======================================================================
 *! \file vsense.h
 *  \n
 *  \brief This file contains vsense related function prototypes,
 *         enums and driver data structure type.   
 *  \n  
 *  \n &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
 */
/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/25/15   aks     created

========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

/*
 * VCS NPA Rail Event Data.
 *
 * All fields are either:
 *   Non-zero when the state change is known prior to dispatching the event
 *   against the resource being monitored.
 *   For example: eCorner pre vs. post is always known.
 *
 *   Zero when the state change is unknown prior to dispatching the event
 *   against the resource being monitored.
 *   For example: nVoltageUV pre vs. post is not always known because the
 *   final voltage comes from CPR which is only known after the pre event has
 *   been dispatched. Another example is the voltage on CX/MX when changing
 *   corners as only RPM knows the actual voltage on those rails.
 */
/*typedef struct
{
  boolean bIsNAS ;            // Boolean indicates whether this change is NAS vs. CAS (immediate).

  struct
  {
    VCSCornerType eCorner;    // Voltage corner before the change event completes.
                              // Empty for limit max events.
    uint32        nVoltageUV; // Voltage in uV before the change event completes.
  } PreChange;
  struct
  {
    VCSCornerType eCorner;    // Voltage corner after the change event completes.
                              // New max corner for limit max events.
    uint32        nVoltageUV; // Voltage in uV after the change event completes.
  } PostChange;
} VCSNPARailEventDataType; */



/*----------------------------------------------------------------------------
 * Function : vsense_init
 * -------------------------------------------------------------------------*/
/*!
    Description: Initialize the vsense driver 
    @param
      void
    @return
    vsense_error_type 
    
-------------------------------------------------------------------------*/
vsense_error_type vsense_init(void);


#endif 
