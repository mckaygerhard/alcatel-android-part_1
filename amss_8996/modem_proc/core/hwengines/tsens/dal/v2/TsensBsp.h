#ifndef __TSENS_BSP_H__
#define __TSENS_BSP_H__
/*============================================================================
  @file TsensBsp.h

  Tsens BSP file

                Copyright (c) 2012, 2014 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/* $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/tsens/dal/v2/TsensBsp.h#1 $ */

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "DALStdDef.h"

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/
typedef struct
{
   const char *pszSROTModule;  /* Name of SROT module */
   const char *pszTMModule;    /* Name of TM module */
} TsensControllerCfgType;

typedef struct
{
   uint32 uController;  /* Controller index */
   uint32 uChannel;     /* Channel index */
} TsensSensorCfgType;

typedef struct
{
   const TsensControllerCfgType *paControllerCfgs;  /* Array of controller configs */
   uint32 uNumControllers;                          /* Number of controllers */
   const TsensSensorCfgType *paSensorCfgs;          /* Array of sensor configs */
   uint32 uNumSensors;                              /* Number of sensors */
} TsensBspType;

/*-------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ----------------------------------------------------------------------*/

#endif

