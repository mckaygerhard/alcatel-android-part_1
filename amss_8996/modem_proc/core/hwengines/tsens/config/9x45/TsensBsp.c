/*============================================================================
  FILE:         TsensBsp.c

  OVERVIEW:     Tsens bsp file

  DEPENDENCIES: None

                Copyright (c) 2012-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/tsens/config/9x45/TsensBsp.c#1 $$DateTime: 2016/03/28 23:02:17 $$Author: mplcsds1 $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-01-29  jjo  Use designated initializers.
  2014-06-10  jjo  Remove base addresses.
  2014-03-31  jjo  Ported to 9x45.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "DALFramework.h"
#include "TsensBsp.h"
#include "TsensiConversion.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define TSENS_NUM_SENSORS                    5
#define TSENS_SENSOR_ENABLE_MASK          0x1F
#define TSENS_INTERRUPT_ID                 216
#define TSENS_SENSOR_CONV_TIME_US          150

#define TSENS_Y1                            30
#define TSENS_Y2                           120

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
/* Default TSENS ADC codes at 30 deg C */
static const int32 anX1_default[TSENS_NUM_SENSORS] =
{
   532,
   532,
   532,
   532,
   532
};

/* Default slope: factor * median slope [C/code] */
static const int32 anM_default[TSENS_NUM_SENSORS] =
{
   (int32)(TSENS_FACTOR * (1 / 3.3965)),
   (int32)(TSENS_FACTOR * (1 / 3.3965)),
   (int32)(TSENS_FACTOR * (1 / 3.3965)),
   (int32)(TSENS_FACTOR * (1 / 3.3965)),
   (int32)(TSENS_FACTOR * (1 / 3.3965))
};

const TsensBspType TsensBsp[] =
{
   {
      .uNumSensors         = TSENS_NUM_SENSORS,
      .uSensorEnableMask   = TSENS_SENSOR_ENABLE_MASK,
      .uInterruptId        = TSENS_INTERRUPT_ID,
      .uSensorConvTime_us  = TSENS_SENSOR_CONV_TIME_US,
      .panX1_default       = anX1_default,
      .panM_default        = anM_default,
      .nY1                 = TSENS_Y1,
      .nY2                 = TSENS_Y2
   }
};

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/

