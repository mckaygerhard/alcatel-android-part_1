#===============================================================================
#
# Tsens Libs
#
# GENERAL DESCRIPTION
#    Tsens Libs build script
#
# Copyright (c) 2010, 2013-2015 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Qualcomm Technologies Confidential and Proprietary.
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/tsens/build/tsens.scons#1 $
#  $DateTime: 2016/03/28 23:02:17 $
#  $Author: mplcsds1 $
#  $Change: 10156097 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 15-02-05   jjo     Pack update.
# 14-12-11   jjo     Added 8996.
# 14-11-03   jjo     Added TSENS diag.
# 14-03-31   jjo     Added 9x45.
# 14-02-20   jjo     Ported to 8916.
#
#===============================================================================
Import('env')
env = env.Clone()

if env['MSM_ID'] not in ['9x45', '8996']:
   Return();

#-------------------------------------------------------------------------------
# Defines
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Publish Private APIs
#-------------------------------------------------------------------------------
if env['MSM_ID'] in ['9x45']:
   env.PublishPrivateApi('HWENGINES_TSENS', [
      "${INC_ROOT}/core/hwengines/tsens/dal/common",
      "${INC_ROOT}/core/hwengines/tsens/dal/v1",
      "${INC_ROOT}/core/hwengines/tsens/diag/protected",
   ])

if env['MSM_ID'] in ['8996']:
   env.PublishPrivateApi('HWENGINES_TSENS', [
      "${INC_ROOT}/core/hwengines/tsens/dal/common",
      "${INC_ROOT}/core/hwengines/tsens/dal/v2",
      "${INC_ROOT}/core/hwengines/tsens/diag/protected",
   ])

#-------------------------------------------------------------------------------
# SRC PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/hwengines/tsens"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
    'DAL',
    'BUSES',
    'HWENGINES',
    'SERVICES',
    'SYSTEMDRIVERS',
    'POWER'
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

#-------------------------------------------------------------------------------
# Configuration
#-------------------------------------------------------------------------------

# Default settings
TSENS_DIAG_SUPPORT = 'YES'
TSENS_TEST_SUPPORT = 'NO'

#-------------------------------------------------------------------------------
# Sources
#-------------------------------------------------------------------------------

# Common sources

TSENS_DAL_COMMON_SOURCES = [
   '${BUILDPATH}/dal/common/DALTsensFwk.c',
   '${BUILDPATH}/dal/common/DALTsensInfo.c',
]

# Target sources

TSENS_DAL_V1_SOURCES = [
   '${BUILDPATH}/dal/v1/DALTsens.c',
   '${BUILDPATH}/dal/v1/TsensiConversion.c'
]

TSENS_DAL_V2_SOURCES = [
   '${BUILDPATH}/dal/v2/DALTsens.c',
]

if env['MSM_ID'] in ['9x45']:
   TSENS_DAL_HW_SOURCES = TSENS_DAL_V1_SOURCES

if env['MSM_ID'] in ['8996']:
   TSENS_DAL_HW_SOURCES = TSENS_DAL_V2_SOURCES

TSENS_HAL_SOURCES = [
   '${BUILDPATH}/hal/${MSM_ID}/HALtsens.c',
]

# Test sources

TSENS_TEST_SOURCES = [
   '${BUILDPATH}/test/src/TsensTest.c',
]

# Diag sources

TSENS_DIAG_SOURCES = [
   '${BUILDPATH}/diag/src/TsensDiag.c'
]

#-------------------------------------------------------------------------------
# XML files
#-------------------------------------------------------------------------------

TSENS_CONFIG_FILE_XML = '${BUILD_ROOT}/core/hwengines/tsens/config/${MSM_ID}/tsens_props.xml'

if TSENS_CONFIG_FILE_XML:
   if 'USES_DEVCFG' in env:
      env.AddDevCfgInfo(['DAL_DEVCFG_IMG'],
      {
            '9645_xml' : ['${BUILD_ROOT}/core/hwengines/tsens/config/9x45/tsens_props.xml',
                          '${BUILD_ROOT}/core/hwengines/tsens/config/9x45/TsensBsp.c'],
            '8996_xml' : ['${BUILD_ROOT}/core/hwengines/tsens/config/8996/tsens_props.xml',
                          '${BUILD_ROOT}/core/hwengines/tsens/config/8996/TsensBsp.c'],
      })

#-------------------------------------------------------------------------------
# Libraries
#-------------------------------------------------------------------------------

env.AddLibrary(
   ['SINGLE_IMAGE',
    'CBSP_SINGLE_IMAGE',
    'MODEM_IMAGE',
    'CBSP_MODEM_IMAGE',
    'QDSP6_SW_IMAGE',
    'CBSP_QDSP6_SW_IMAGE',
    'QDSP6_SW_IMAGE',
    'APPS_IMAGE',
    'CBSP_APPS_IMAGE'],
    '${BUILDPATH}/tsens', [TSENS_DAL_COMMON_SOURCES, TSENS_DAL_HW_SOURCES, TSENS_HAL_SOURCES])

if TSENS_DIAG_SUPPORT in ['YES']:
   env.AddLibrary (
      ['SINGLE_IMAGE',
       'CBSP_SINGLE_IMAGE',
       'MODEM_IMAGE',
       'CBSP_MODEM_IMAGE',
       'QDSP6_SW_IMAGE',
       'CBSP_QDSP6_SW_IMAGE',
       'QDSP6_SW_IMAGE',
       'APPS_IMAGE',
       'CBSP_APPS_IMAGE'],
       '${BUILDPATH}/tsens_diag', [TSENS_DIAG_SOURCES])

if TSENS_TEST_SUPPORT in ['YES']:
   env.AddLibrary(
      ['SINGLE_IMAGE',
       'CBSP_SINGLE_IMAGE',
       'MODEM_IMAGE',
       'CBSP_MODEM_IMAGE',
       'QDSP6_SW_IMAGE',
       'CBSP_QDSP6_SW_IMAGE',
       'QDSP6_SW_IMAGE',
       'APPS_IMAGE',
       'CBSP_APPS_IMAGE'],
       '${BUILDPATH}/tsens_test', [TSENS_TEST_SOURCES])

#-------------------------------------------------------------------------------
# Load sub scripts
#-------------------------------------------------------------------------------
#env.LoadSoftwareUnits(level=1)

#-------------------------------------------------------------------------------
# RCINIT
#-------------------------------------------------------------------------------
# Put TSENS test in RCINIT if test mode on

RCINIT_IMAGES = ['APPS_IMAGE', 'CBSP_APPS_IMAGE', 'CORE_MODEM', 'CORE_QDSP6_SW']

RCINIT_INIT_FUNC_1 = {
   'sequence_group'             : 'RCINIT_GROUP_2',             # required
   'init_name'                  : 'tsens_diag',                 # required
   'init_function'              : 'TsensDiagInit',
   'dependencies'               : ['npa'],
}

RCINIT_INIT_FUNC_2 = {
   'sequence_group'             : 'RCINIT_GROUP_2',             # required
   'init_name'                  : 'tsens_test',                 # required
   'init_function'              : 'tsens_test',
   'dependencies'               : ['npa'],
}

if 'USES_RCINIT' in env:
   if TSENS_DIAG_SUPPORT in ['YES']:
      env.AddRCInitFunc(RCINIT_IMAGES, RCINIT_INIT_FUNC_1)
   if TSENS_TEST_SUPPORT in ['YES']:
      env.AddRCInitFunc(RCINIT_IMAGES, RCINIT_INIT_FUNC_2)

#-------------------------------------------------------------------------------
# Pack files (files to remove)
#-------------------------------------------------------------------------------

# Remove HW files

if env['MSM_ID'] in ['9x45']:
   TSENS_DAL_HW_REMOVE_SOURCES = env.FindFiles(['*'], '${BUILD_ROOT}/core/hwengines/tsens/dal/v2/')

if env['MSM_ID'] in ['8996']:
   TSENS_DAL_HW_REMOVE_SOURCES = env.FindFiles(['*'], '${BUILD_ROOT}/core/hwengines/tsens/dal/v1/')

env.CleanPack(
   ['SINGLE_IMAGE',
    'CBSP_SINGLE_IMAGE',
    'MODEM_IMAGE',
    'CBSP_MODEM_IMAGE',
    'QDSP6_SW_IMAGE',
    'CBSP_QDSP6_SW_IMAGE',
    'QDSP6_SW_IMAGE',
    'APPS_IMAGE',
    'CBSP_APPS_IMAGE'],
    TSENS_DAL_HW_REMOVE_SOURCES)

# Remove test files
env.CleanPack(
   ['SINGLE_IMAGE',
    'CBSP_SINGLE_IMAGE',
    'MODEM_IMAGE',
    'CBSP_MODEM_IMAGE',
    'QDSP6_SW_IMAGE',
    'CBSP_QDSP6_SW_IMAGE',
    'QDSP6_SW_IMAGE',
    'APPS_IMAGE',
    'CBSP_APPS_IMAGE'],
    TSENS_TEST_SOURCES)

# Remove BSP files
BSP_FILES = env.FindFiles(['*'], '${BUILD_ROOT}/core/hwengines/tsens/config/')

BSP_FILES = filter(lambda BSP_FILES: env['MSM_ID'] not in BSP_FILES[:], BSP_FILES)

env.CleanPack(
   ['SINGLE_IMAGE',
    'CBSP_SINGLE_IMAGE',
    'MODEM_IMAGE',
    'CBSP_MODEM_IMAGE',
    'QDSP6_SW_IMAGE',
    'CBSP_QDSP6_SW_IMAGE',
    'QDSP6_SW_IMAGE',
    'APPS_IMAGE',
    'CBSP_APPS_IMAGE'],
    BSP_FILES)

# Remove HAL files
HAL_FILES = env.FindFiles(['*'], '${BUILD_ROOT}/core/hwengines/tsens/hal/')

HAL_FILES = filter(lambda HAL_FILES: env['MSM_ID'] not in HAL_FILES[:], HAL_FILES)

env.CleanPack(
   ['SINGLE_IMAGE',
    'CBSP_SINGLE_IMAGE',
    'MODEM_IMAGE',
    'CBSP_MODEM_IMAGE',
    'QDSP6_SW_IMAGE',
    'CBSP_QDSP6_SW_IMAGE',
    'QDSP6_SW_IMAGE',
    'APPS_IMAGE',
    'CBSP_APPS_IMAGE'],
    HAL_FILES)

