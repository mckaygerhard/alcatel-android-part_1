#ifndef __VADCTM_DEVICE_H__
#define __VADCTM_DEVICE_H__
/*============================================================================
  @file VAdcTMDevice.h

  Function and data structure declarations for VADC TM DAL


               Copyright (c) 2013, 2015 Qualcomm Technologies, Inc.
               All Rights Reserved.
               Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/* $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/adc/devices/vadc/src/VAdcTMDevice.h#1 $ */

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "DALFramework.h"
#include "DDIAdcDevice.h"
#include "VAdcTMHal.h"
#include "DDISpmi.h"
#include "pmic.h"
#include "npa.h"

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/
#define VADCTM_MAX_NUM_MEAS      8
#define VADCTM_MAX_NUM_CLIENTS   3

enum
{
   VADCTM_EVENT_DEFAULT = 0,
   VADCTM_EVENT_TIMEOUT,
   _VADCTM_NUM_EVENTS
};

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/
/*
 * VAdcTMMeasConfigType
 */
typedef struct
{
   const char *pName;
   VAdcTMMeasIntervalTimeSelectType eMeasIntervalTimeSelect;
} VAdcTMMeasConfigType;

/*
 * VAdcTMBspType
 */
typedef struct
{
   const VAdcTMMeasConfigType *paMeasConfig;
   uint32 uNumMeas;
   SpmiBus_AccessPriorityType eAccessPriority;
   uint32 uSlaveId;
   uint32 uPmicDevice;
   uint32 uPeripheralID;
   uint32 uMasterID;
   uint32 uMinDigMinor;
   uint32 uMinDigMajor;
   uint32 uMinAnaMinor;
   uint32 uMinAnaMajor;
   uint32 uPerphType;
   uint32 uPerphSubType;
   VAdcTMDecimationRatioType eDecimationRatio;
   VAdcTMClockSelectType eClockSelect;
   uint32 uConversionTime_us;
   uint32 uVAdcConfigIdx;
   VAdcTMSettlingDelay eSettlingDelay;
   VAdcTMFastAverageModeType eFastAverageMode;
   VAdcTMMeasIntervalTime1Type eMeasIntervalTime1;
   VAdcTMMeasIntervalTime2Type eMeasIntervalTime2;
   VAdcTMMeasIntervalTime3Type eMeasIntervalTime3;
   int32 nThresholdMin_mV;
   int32 nThresholdMax_mV;
} VAdcTMBspType;

typedef struct VAdcTMDevCtxt VAdcTMDevCtxt;

/*-------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ----------------------------------------------------------------------*/
DALResult
VAdcTM_Init(
   VAdcTMDevCtxt **ppDevCtxt,
   DalDeviceHandle *phVAdcDev
   );

DALResult
VAdcTM_DeInit(
   VAdcTMDevCtxt *pDevCtxt
   );

DALResult
VAdcTM_GetInputProperties(
   VAdcTMDevCtxt *pDevCtxt,
   const char * pInputName,
   uint32 *puMeasIdx
   );

DALResult
VAdcTM_GetRange(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uMeasIdx,
   AdcDeviceTMRangeType *pAdcDeviceTMRange
   );

DALResult
VAdcTM_SetThreshold(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uClientId,
   uint32 uMeasIdx,
   const DALSYSEventHandle hEvent,
   AdcDeviceTMThresholdType eThreshold,
   const int32 *pnThresholdDesired,
   int32 *pnThresholdSet
   );

DALResult
VAdcTM_SetEnableThresholds(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uClientId,
   DALBOOL bEnable
   );

DALResult
VAdcTM_UpdateCalibration(
   VAdcTMDevCtxt *pDevCtxt
   );

DALResult
VAdcTM_UnregisterClient(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uClientId
   );

DALResult
VAdcTM_SetTolerance(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uClientId,
   uint32 uMeasIdx,
   const DALSYSEventHandle hEvent,
   const int32 *pnLowerTolerance,
   const int32 *pnHigherTolerance
   );

#endif /* #ifndef __VADCTM_DEVICE_H__ */

