/*============================================================================
  FILE:         VAdcTMHal.c

  OVERVIEW:     Implementation of device HAL for VADC TM using VBAT_MIN

  DEPENDENCIES: None

                Copyright (c) 2012, 2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/adc/devices/vadc/src/VAdcTMHal.c#1 $$DateTime: 2016/03/28 23:02:17 $$Author: mplcsds1 $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-02-18  jjo  Switch to VBAT_MIN peripheral
  2015-01-12  jjo  Reduce number of SPMI transactions
  2012-09-28  jjo  Initial revision

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "VAdcTMHal.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) \
   (sizeof(a) / sizeof((a)[0]))

#define READ_FIELD(reg_val, reg, field) \
   ((reg_val & reg##_##field##_BMSK) >> reg##_##field##_SHFT)

#define READ_REG(reg_val, reg) \
   (reg_val & reg##_RMSK)

#define WRITE_FIELD(reg_val, reg, field, val) \
   ((reg_val & ~reg##_##field##_BMSK) | ((val << reg##_##field##_SHFT) & reg##_##field##_BMSK))

#define WRITE_REG(reg_val, reg, val) \
   ((reg_val & ~reg##_RMSK) | (val & reg##_RMSK))

#define VADCTM_REVISION1_ADDR                                                0
#define VADCTM_REVISION1_RMSK                                             0xFF
#define VADCTM_REVISION1_DIG_MINOR_BMSK                                   0xFF
#define VADCTM_REVISION1_DIG_MINOR_SHFT                                      0

#define VADCTM_REVISION2_ADDR                                                1
#define VADCTM_REVISION2_RMSK                                             0xFF
#define VADCTM_REVISION1_DIG_MAJOR_BMSK                                   0xFF
#define VADCTM_REVISION1_DIG_MAJOR_SHFT                                      0

#define VADCTM_REVISION3_ADDR                                                2
#define VADCTM_REVISION3_RMSK                                             0xFF
#define VADCTM_REVISION1_ANA_MINOR_BMSK                                   0xFF
#define VADCTM_REVISION1_ANA_MINOR_SHFT                                      0

#define VADCTM_REVISION4_ADDR                                                3
#define VADCTM_REVISION4_RMSK                                             0xFF
#define VADCTM_REVISION1_ANA_MAJOR_BMSK                                   0xFF
#define VADCTM_REVISION1_ANA_MAJOR_SHFT                                      0

#define VADCTM_PERPH_TYPE_ADDR                                               4
#define VADCTM_PERPH_TYPE_RMSK                                            0xFF
#define VADCTM_PERPH_TYPE_TYPE_BMSK                                       0xFF
#define VADCTM_PERPH_TYPE_TYPE_SHFT                                          0

#define VADCTM_PERPH_SUBTYPE_ADDR                                            5
#define VADCTM_PERPH_SUBTYPE_RMSK                                         0xFF
#define VADCTM_PERPH_SUBTYPE_SUBTYPE_BMSK                                 0xFF
#define VADCTM_PERPH_SUBTYPE_SUBTYPE_SHFT                                    0

#define VADCTM_STATUS1_ADDR                                                  8
#define VADCTM_STATUS1_RMSK                                               0x1F
#define VADCTM_STATUS1_OP_MODE_BMSK                                       0x18
#define VADCTM_STATUS1_OP_MODE_SHFT                                          3
#define VADCTM_STATUS1_MEAS_INTERVAL_EN_STS_BMSK                          0x04
#define VADCTM_STATUS1_MEAS_INTERVAL_EN_STS_SHFT                             2
#define VADCTM_STATUS1_REQ_STS_AND_EOC_BMSK                               0x03
#define VADCTM_STATUS1_REQ_STS_AND_EOC_SHFT                                  0

#define VADCTM_STATUS2_ADDR                                                  9
#define VADCTM_STATUS2_RMSK                                               0xFB
#define VADCTM_STATUS2_CONV_SEQ_STATE_BMSK                                0xF8
#define VADCTM_STATUS2_CONV_SEQ_STATE_SHFT                                   3
#define VADCTM_STATUS2_FIFO_NOT_EMPTY_FLAG_BMSK                           0x02
#define VADCTM_STATUS2_FIFO_NOT_EMPTY_FLAG_SHFT                              1
#define VADCTM_STATUS2_CONV_SEQ_TIMEOUT_STS_BMSK                          0x01
#define VADCTM_STATUS2_CONV_SEQ_TIMEOUT_STS_SHFT                             0

#define VADCTM_STATUS_LOW_ADDR                                            0x0A
#define VADCTM_STATUS_LOW_RMSK                                            0xFF

#define VADCTM_STATUS_HIGH_ADDR                                           0x0B
#define VADCTM_STATUS_HIGH_RMSK                                           0xFF

#define VADCTM_INT_RT_STS_ADDR                                            0x10

#define VADCTM_INT_SET_TYPE_ADDR                                          0x11
#define VADCTM_INT_SET_TYPE_RMSK                                          0x1F
#define VADCTM_INT_SET_TYPE_LOW_THR_BMSK                                  0x10
#define VADCTM_INT_SET_TYPE_LOW_THR_SHFT                                     4
#define VADCTM_INT_SET_TYPE_HIGH_THR_BMSK                                 0x08
#define VADCTM_INT_SET_TYPE_HIGH_THR_SHFT                                    3
#define VADCTM_INT_SET_TYPE_EOC_BMSK                                      0x01
#define VADCTM_INT_SET_TYPE_EOC_SHFT                                         0

#define VADCTM_INT_POLARITY_HIGH_ADDR                                     0x12
#define VADCTM_INT_POLARITY_HIGH_RMSK                                     0x1F
#define VADCTM_INT_POLARITY_HIGH_LOW_THR_BMSK                             0x10
#define VADCTM_INT_POLARITY_HIGH_LOW_THR_SHFT                                4
#define VADCTM_INT_POLARITY_HIGH_HIGH_THR_BMSK                            0x08
#define VADCTM_INT_POLARITY_HIGH_HIGH_THR_SHFT                               3
#define VADCTM_INT_POLARITY_HIGH_EOC_BMSK                                 0x01
#define VADCTM_INT_POLARITY_HIGH_EOC_SHFT                                    0

#define VADCTM_INT_POLARITY_LOW_ADDR                                      0x13
#define VADCTM_INT_POLARITY_LOW_RMSK                                      0x1F
#define VADCTM_INT_POLARITY_LOW_LOW_THR_BMSK                              0x10
#define VADCTM_INT_POLARITY_LOW_LOW_THR_SHFT                                 4
#define VADCTM_INT_POLARITY_LOW_HIGH_THR_BMSK                             0x08
#define VADCTM_INT_POLARITY_LOW_HIGH_THR_SHFT                                3
#define VADCTM_INT_POLARITY_LOW_EOC_BMSK                                  0x01
#define VADCTM_INT_POLARITY_LOW_EOC_SHFT                                     0

#define VADCTM_INT_LATCHED_CLR_ADDR                                       0x14
#define VADCTM_INT_LATCHED_CLR_RMSK                                       0x1F

#define VADCTM_INT_EN_SET_ADDR                                            0x15
#define VADCTM_INT_EN_SET_RMSK                                            0x1F
#define VADCTM_INT_EN_SET_LOW_THR_BMSK                                    0x10
#define VADCTM_INT_EN_SET_LOW_THR_SHFT                                       4
#define VADCTM_INT_EN_SET_HIGH_THR_BMSK                                   0x08
#define VADCTM_INT_EN_SET_HIGH_THR_SHFT                                      3
#define VADCTM_INT_EN_SET_EOC_BMSK                                        0x01
#define VADCTM_INT_EN_SET_EOC_SHFT                                           0

#define VADCTM_INT_EN_CLR_ADDR                                            0x16

#define VADCTM_INT_LATCHED_STS_ADDR                                       0x18

#define VADCTM_INT_PENDING_STS_ADDR                                       0x19
#define VADCTM_INT_PENDING_STS_RMSK                                       0x1F

#define VADCTM_INT_MID_SEL_ADDR                                           0x1A
#define VADCTM_INT_MID_SEL_RMSK                                           0x03
#define VADCTM_INT_MID_SEL_INT_MID_SEL_BMSK                               0x03
#define VADCTM_INT_MID_SEL_INT_MID_SEL_SHFT                                  0

#define VADCTM_INT_PRIORITY_ADDR                                          0x1B

#define VADCTM_MODE_CTL_ADDR                                              0x40
#define VADCTM_MODE_CTL_RMSK                                              0x1F
#define VADCTM_MODE_CTL_OP_MODE_BMSK                                      0x18
#define VADCTM_MODE_CTL_OP_MODE_SHFT                                         3

#define VADCTM_MULTI_MEAS_EN_ADDR                                         0x41
#define VADCTM_MULTI_MEAS_EN_RMSK                                         0xFF

#define VADCTM_LOW_THR_INT_EN_ADDR                                        0x42
#define VADCTM_LOW_THR_INT_EN_RMSK                                        0xFF

#define VADCTM_HIGH_THR_INT_EN_ADDR                                       0x43
#define VADCTM_HIGH_THR_INT_EN_RMSK                                       0xFF

#define VADCTM_EN_CTL1_ADDR                                               0x46
#define VADCTM_EN_CTL1_RMSK                                               0x80
#define VADCTM_EN_CTL1_ADC_EN_BMSK                                        0x80
#define VADCTM_EN_CTL1_ADC_EN_SHFT                                           7

#define VADCTM_M0_ADC_CH_SEL_CTL_ADDR                                     0x48
#define VADCTM_M0_ADC_CH_SEL_CTL_RMSK                                     0xFF
#define VADCTM_M0_ADC_CH_SEL_CTL_ADC_CH_SEL_BMSK                          0xFF
#define VADCTM_M0_ADC_CH_SEL_CTL_ADC_CH_SEL_SHFT                             0

#define VADCTM_ADC_DIG_PARAM_ADDR                                         0x50
#define VADCTM_ADC_DIG_PARAM_RMSK                                         0x0F
#define VADCTM_ADC_DIG_PARAM_DEC_RATIO_SEL_BMSK                           0x0C
#define VADCTM_ADC_DIG_PARAM_DEC_RATIO_SEL_SHFT                              2
#define VADCTM_ADC_DIG_PARAM_CLK_SEL_BMSK                                 0x03
#define VADCTM_ADC_DIG_PARAM_CLK_SEL_SHFT                                    0

#define VADCTM_HW_SETTLE_DELAY_ADDR                                       0x51
#define VADCTM_HW_SETTLE_DELAY_RMSK                                       0x0F
#define VADCTM_HW_SETTLE_DELAY_HW_SETTLE_DELAY_BMSK                       0x0F
#define VADCTM_HW_SETTLE_DELAY_HW_SETTLE_DELAY_SHFT                          0

#define VADCTM_CONV_REQ_ADDR                                              0x52
#define VADCTM_CONV_REQ_REQ_BMSK                                          0x80
#define VADCTM_CONV_REQ_REQ_SHFT                                             7

#define VADCTM_CONV_SEQ_CTL_ADDR                                          0x54
#define VADCTM_CONV_SEQ_CTL_RMSK                                          0xFF
#define VADCTM_CONV_SEQ_CTL_CONV_SEQ_HOLDOFF_BMSK                         0xF0
#define VADCTM_CONV_SEQ_CTL_CONV_SEQ_HOLDOFF_SHFT                            4
#define VADCTM_CONV_SEQ_CTL_CONV_SEQ_TIMEOUT_BMSK                         0x0F
#define VADCTM_CONV_SEQ_CTL_CONV_SEQ_TIMEOUT_SHFT                            0

#define VADCTM_CONV_SEQ_TRIG_CTL_ADDR                                     0x55
#define VADCTM_CONV_SEQ_TRIG_CTL_RMSK                                     0x83
#define VADCTM_CONV_SEQ_TRIG_CTL_CONV_SEQ_TRIG_COND_BMSK                  0x80
#define VADCTM_CONV_SEQ_TRIG_CTL_CONV_SEQ_TRIG_COND_SHFT                     7
#define VADCTM_CONV_SEQ_TRIG_CTL_CONV_SEQ_TRIG_SEL_BMSK                   0x03
#define VADCTM_CONV_SEQ_TRIG_CTL_CONV_SEQ_TRIG_SEL_SHFT                      0

#define VADCTM_MEAS_INTERVAL_CTL_ADDR                                     0x57
#define VADCTM_MEAS_INTERVAL_CTL_RMSK                                     0x0F
#define VADCTM_MEAS_INTERVAL_CTL_MEAS_INTERVAL_TIME1_BMSK                 0x0F
#define VADCTM_MEAS_INTERVAL_CTL_MEAS_INTERVAL_TIME1_SHFT                    0

#define VADCTM_MEAS_INTERVAL_CTL2_ADDR                                    0x58
#define VADCTM_MEAS_INTERVAL_CTL2_RMSK                                    0xFF
#define VADCTM_MEAS_INTERVAL_CTL2_MEAS_INTERVAL_TIME2_BMSK                0xF0
#define VADCTM_MEAS_INTERVAL_CTL2_MEAS_INTERVAL_TIME2_SHFT                   4
#define VADCTM_MEAS_INTERVAL_CTL2_MEAS_INTERVAL_TIME3_BMSK                0x0F
#define VADCTM_MEAS_INTERVAL_CTL2_MEAS_INTERVAL_TIME3_SHFT                   0

#define VADCTM_MEAS_INTERVAL_OP_CTL_ADDR                                  0x59
#define VADCTM_MEAS_INTERVAL_OP_CTL_RMSK                                  0x83
#define VADCTM_MEAS_INTERVAL_OP_CTL_MEAS_INTERVAL_OP_BMSK                 0x80
#define VADCTM_MEAS_INTERVAL_OP_CTL_MEAS_INTERVAL_OP_SHFT                    7
#define VADCTM_MEAS_INTERVAL_OP_CTL_M0_MEAS_INTERVAL_TIME_BMSK            0x03
#define VADCTM_MEAS_INTERVAL_OP_CTL_M0_MEAS_INTERVAL_TIME_SHFT               0

#define VADCTM_FAST_AVG_CTL_ADDR                                          0x5A
#define VADCTM_FAST_AVG_CTL_RMSK                                          0x0F
#define VADCTM_FAST_AVG_CTL_FAST_AVG_SAMPLES_BMSK                         0x0F
#define VADCTM_FAST_AVG_CTL_FAST_AVG_SAMPLES_SHFT                            0

#define VADCTM_FAST_AVG_EN_ADDR                                           0x5B
#define VADCTM_FAST_AVG_EN_RMSK                                           0x80
#define VADCTM_FAST_AVG_EN_FAST_AVG_EN_BMSK                               0x80
#define VADCTM_FAST_AVG_EN_FAST_AVG_EN_SHFT                                  7

#define VADCTM_M0_LOW_THR0_ADDR                                           0x5C
#define VADCTM_M0_LOW_THR0_RMSK                                           0xFF
#define VADCTM_M0_LOW_THR0_LOW_THR_7_0_BMSK                               0xFF
#define VADCTM_M0_LOW_THR0_LOW_THR_7_0_SHFT                                  0

#define VADCTM_M0_LOW_THR1_ADDR                                           0x5D
#define VADCTM_M0_LOW_THR1_RMSK                                           0xFF
#define VADCTM_M0_LOW_THR1_LOW_THR_15_8_BMSK                              0xFF
#define VADCTM_M0_LOW_THR1_LOW_THR_15_8_SHFT                                 0

#define VADCTM_M0_HIGH_THR0_ADDR                                          0x5E
#define VADCTM_M0_HIGH_THR0_RMSK                                          0xFF
#define VADCTM_M0_HIGH_THR0_HIGH_THR_7_0_BMSK                             0xFF
#define VADCTM_M0_HIGH_THR0_HIGH_THR_7_0_SHFT                                0

#define VADCTM_M0_HIGH_THR1_ADDR                                          0x5F
#define VADCTM_M0_HIGH_THR1_RMSK                                          0xFF
#define VADCTM_M0_HIGH_THR1_HIGH_THR_15_8_BMSK                            0xFF
#define VADCTM_M0_HIGH_THR1_HIGH_THR_15_8_SHFT                               0

#define VADCTM_M0_DATA0_ADDR                                              0x60
#define VADCTM_M0_DATA0_RMSK                                              0xFF
#define VADCTM_M0_DATA0_DATA_7_0_BMSK                                     0xFF
#define VADCTM_M0_DATA0_DATA_7_0_SHFT                                        0

#define VADCTM_M0_DATA1_ADDR                                              0x61
#define VADCTM_M0_DATA1_RMSK                                              0xFF
#define VADCTM_M0_DATA1_DATA_7_0_BMSK                                     0xFF
#define VADCTM_M0_DATA1_DATA_15_8_SHFT                                       0

/* For the following registers, N = 1 to 7; M0 defined above */
#define VADCTM_Mn_ADC_CH_SEL_CTL_ADDR(n)                    (0x60 + 0x8 * (n))
#define VADCTM_Mn_ADC_CH_SEL_CTL_RMSK                                     0xFF
#define VADCTM_Mn_ADC_CH_SEL_CTL_ADC_CH_SEL_BMSK                          0xFF
#define VADCTM_Mn_ADC_CH_SEL_CTL_ADC_CH_SEL_SHFT                             0

#define VADCTM_Mn_LOW_THR0_ADDR(n)                          (0x61 + 0x8 * (n))
#define VADCTM_Mn_LOW_THR0_RMSK                                           0xFF
#define VADCTM_Mn_LOW_THR0_LOW_THR_7_0_BMSK                               0xFF
#define VADCTM_Mn_LOW_THR0_LOW_THR_7_0_SHFT                                  0

#define VADCTM_Mn_LOW_THR1_ADDR(n)                          (0x62 + 0x8 * (n))
#define VADCTM_Mn_LOW_THR1_RMSK                                           0xFF
#define VADCTM_Mn_LOW_THR1_LOW_THR_15_8_BMSK                              0xFF
#define VADCTM_Mn_LOW_THR1_LOW_THR_15_8_SHFT                                 0

#define VADCTM_Mn_HIGH_THR0_ADDR(n)                         (0x63 + 0x8 * (n))
#define VADCTM_Mn_HIGH_THR0_RMSK                                          0xFF
#define VADCTM_Mn_HIGH_THR0_HIGH_THR_7_0_BMSK                             0xFF
#define VADCTM_Mn_HIGH_THR0_HIGH_THR_7_0_SHFT                                0

#define VADCTM_Mn_HIGH_THR1_ADDR(n)                         (0x64 + 0x8 * (n))
#define VADCTM_Mn_HIGH_THR1_RMSK                                          0xFF
#define VADCTM_Mn_HIGH_THR1_HIGH_THR_15_8_BMSK                            0xFF
#define VADCTM_Mn_HIGH_THR1_HIGH_THR_15_8_SHFT                               0

#define VADCTM_Mn_MEAS_INTERVAL_CTL_ADDR(n)                 (0x65 + 0x8 * (n))
#define VADCTM_Mn_MEAS_INTERVAL_CTL_RMSK                                  0x03
#define VADCTM_Mn_MEAS_INTERVAL_CTL_Mn_MEAS_INTERVAL_TIME_BMSK            0x03
#define VADCTM_Mn_MEAS_INTERVAL_CTL_Mn_MEAS_INTERVAL_TIME_SHFT               0

#define VADCTM_Mn_DATA0_ADDR(n)                             (0x9E + 0x2 * (n))
#define VADCTM_Mn_DATA0_RMSK                                              0xFF
#define VADCTM_Mn_DATA0_DATA_7_0_BMSK                                     0xFF
#define VADCTM_Mn_DATA0_DATA_7_0_SHFT                                        0

#define VADCTM_Mn_DATA1_ADDR(n)                             (0x9F + 0x2 * (n))
#define VADCTM_Mn_DATA1_RMSK                                              0xFF
#define VADCTM_Mn_DATA1_DATA_7_0_BMSK                                     0xFF
#define VADCTM_Mn_DATA1_DATA_15_8_SHFT                                       0

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
static const uint8 vAdcHalRegDumpAddresses[VADCTM_HAL_NUM_REGISTERS_TO_DUMP] =
{
   VADCTM_REVISION1_ADDR,
   VADCTM_REVISION2_ADDR,
   VADCTM_REVISION3_ADDR,
   VADCTM_REVISION4_ADDR,
   VADCTM_PERPH_TYPE_ADDR,
   VADCTM_PERPH_SUBTYPE_ADDR,
   VADCTM_STATUS1_ADDR,
   VADCTM_STATUS2_ADDR,
   VADCTM_STATUS_LOW_ADDR,
   VADCTM_STATUS_HIGH_ADDR,
   VADCTM_INT_RT_STS_ADDR,
   VADCTM_INT_SET_TYPE_ADDR,
   VADCTM_INT_POLARITY_HIGH_ADDR,
   VADCTM_INT_POLARITY_LOW_ADDR,
   VADCTM_INT_EN_SET_ADDR,
   VADCTM_INT_EN_CLR_ADDR,
   VADCTM_INT_LATCHED_STS_ADDR,
   VADCTM_INT_PENDING_STS_ADDR,
   VADCTM_INT_MID_SEL_ADDR,
   VADCTM_INT_PRIORITY_ADDR,
   VADCTM_MODE_CTL_ADDR,
   VADCTM_MULTI_MEAS_EN_ADDR,
   VADCTM_LOW_THR_INT_EN_ADDR,
   VADCTM_HIGH_THR_INT_EN_ADDR,
   VADCTM_EN_CTL1_ADDR,
   VADCTM_M0_ADC_CH_SEL_CTL_ADDR,
   VADCTM_ADC_DIG_PARAM_ADDR,
   VADCTM_HW_SETTLE_DELAY_ADDR,
   VADCTM_CONV_SEQ_CTL_ADDR,
   VADCTM_CONV_SEQ_TRIG_CTL_ADDR,
   VADCTM_MEAS_INTERVAL_CTL_ADDR,
   VADCTM_MEAS_INTERVAL_CTL2_ADDR,
   VADCTM_MEAS_INTERVAL_OP_CTL_ADDR,
   VADCTM_FAST_AVG_CTL_ADDR,
   VADCTM_FAST_AVG_EN_ADDR,
   VADCTM_M0_LOW_THR0_ADDR,
   VADCTM_M0_LOW_THR1_ADDR,
   VADCTM_M0_HIGH_THR0_ADDR,
   VADCTM_M0_HIGH_THR1_ADDR,
   VADCTM_M0_DATA0_ADDR,
   VADCTM_M0_DATA1_ADDR,
   VADCTM_Mn_ADC_CH_SEL_CTL_ADDR(1),
   VADCTM_Mn_LOW_THR0_ADDR(1),
   VADCTM_Mn_LOW_THR1_ADDR(1),
   VADCTM_Mn_HIGH_THR0_ADDR(1),
   VADCTM_Mn_HIGH_THR1_ADDR(1),
   VADCTM_Mn_MEAS_INTERVAL_CTL_ADDR(1),
   VADCTM_Mn_DATA0_ADDR(1),
   VADCTM_Mn_DATA1_ADDR(1),
   VADCTM_Mn_ADC_CH_SEL_CTL_ADDR(2),
   VADCTM_Mn_LOW_THR0_ADDR(2),
   VADCTM_Mn_LOW_THR1_ADDR(2),
   VADCTM_Mn_HIGH_THR0_ADDR(2),
   VADCTM_Mn_HIGH_THR1_ADDR(2),
   VADCTM_Mn_MEAS_INTERVAL_CTL_ADDR(2),
   VADCTM_Mn_DATA0_ADDR(2),
   VADCTM_Mn_DATA1_ADDR(2),
   VADCTM_Mn_ADC_CH_SEL_CTL_ADDR(3),
   VADCTM_Mn_LOW_THR0_ADDR(3),
   VADCTM_Mn_LOW_THR1_ADDR(3),
   VADCTM_Mn_HIGH_THR0_ADDR(3),
   VADCTM_Mn_HIGH_THR1_ADDR(3),
   VADCTM_Mn_MEAS_INTERVAL_CTL_ADDR(3),
   VADCTM_Mn_DATA0_ADDR(3),
   VADCTM_Mn_DATA1_ADDR(3),
   VADCTM_Mn_ADC_CH_SEL_CTL_ADDR(4),
   VADCTM_Mn_LOW_THR0_ADDR(4),
   VADCTM_Mn_LOW_THR1_ADDR(4),
   VADCTM_Mn_HIGH_THR0_ADDR(4),
   VADCTM_Mn_HIGH_THR1_ADDR(4),
   VADCTM_Mn_MEAS_INTERVAL_CTL_ADDR(4),
   VADCTM_Mn_DATA0_ADDR(4),
   VADCTM_Mn_DATA1_ADDR(4)
};

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/
VAdcTMHalResultType VAdcTMHalGetRevisionInfo(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMRevisionInfoType *pRevisionInfo)
{
   VAdcTMHalResultType result;
   uint8 aucData[6];
   uint32 uTotalBytesRead;
   uint32 uOffset = VADCTM_REVISION1_ADDR;

   result = piVAdcTMHal->pfnReadBytes(piVAdcTMHal->pCtxt,
                                      uOffset,
                                      aucData,
                                      ARRAY_LENGTH(aucData),
                                      &uTotalBytesRead);
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   if (uTotalBytesRead != ARRAY_LENGTH(aucData))
   {
      return VADCTM_HAL_ERROR;
   }

   pRevisionInfo->uDigitalMinor = aucData[0];
   pRevisionInfo->uDigitalMajor = aucData[1];
   pRevisionInfo->uAnalogMinor = aucData[2];
   pRevisionInfo->uAnalogMajor = aucData[3];
   pRevisionInfo->uType = aucData[4];
   pRevisionInfo->uSubType = aucData[5];

   return result;
}

VAdcTMHalResultType VAdcTMHalGetStatus(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMStatusType *pStatus)
{
   VAdcTMHalResultType result;
   uint8 aucData[2];
   uint32 uTotalBytesRead;
   uint32 uOffset = VADCTM_STATUS1_ADDR;

   result = piVAdcTMHal->pfnReadBytes(piVAdcTMHal->pCtxt,
                                      uOffset,
                                      aucData,
                                      ARRAY_LENGTH(aucData),
                                      &uTotalBytesRead);
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   if (uTotalBytesRead != ARRAY_LENGTH(aucData))
   {
      return VADCTM_HAL_ERROR;
   }

   pStatus->eOpMode = (VAdcTMOpModeType)READ_FIELD(aucData[0], VADCTM_STATUS1, OP_MODE);
   pStatus->eIntervalMode = (VAdcTMIntervalModeType)READ_FIELD(aucData[0], VADCTM_STATUS1, MEAS_INTERVAL_EN_STS);
   pStatus->eConversionStatus = (VAdcTMConversionStatusType)READ_FIELD(aucData[0], VADCTM_STATUS1, REQ_STS_AND_EOC);

   pStatus->eSequencerStatus = (VAdcTMSequencerStatusType)READ_FIELD(aucData[1], VADCTM_STATUS2, CONV_SEQ_STATE);

   if (READ_FIELD(aucData[1], VADCTM_STATUS2, FIFO_NOT_EMPTY_FLAG) == 0)
   {
      pStatus->bSequencerFifoNotEmptyErrorOccurred = FALSE;
   }
   else
   {
      pStatus->bSequencerFifoNotEmptyErrorOccurred = TRUE;
   }

   if (READ_FIELD(aucData[1], VADCTM_STATUS2, CONV_SEQ_TIMEOUT_STS) == 0)
   {
      pStatus->bSequencerTimeoutErrorOccurred = FALSE;
   }
   else
   {
      pStatus->bSequencerTimeoutErrorOccurred = TRUE;
   }

   pStatus->lowThresholdStatus = 1;

   pStatus->highThresholdStatus = 1;

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalConfigInterrupts(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMInterruptsConfigType *pConfigs)
{
   VAdcTMHalResultType result;
   uint8 aucData[3];
   uint32 uTotalBytesRead;
   uint32 uOffset = VADCTM_INT_SET_TYPE_ADDR;

   result = piVAdcTMHal->pfnReadBytes(piVAdcTMHal->pCtxt,
                                      uOffset,
                                      aucData,
                                      ARRAY_LENGTH(aucData),
                                      &uTotalBytesRead);
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   if (uTotalBytesRead != ARRAY_LENGTH(aucData))
   {
      return VADCTM_HAL_ERROR;
   }

   /* Only supporting the threshold interrupts for TM */
   switch (pConfigs->eLowThresholdInterruptConfig)
   {
      case VADCTM_INTERRUPT_CONFIG_LEVEL_HIGH:
         aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_SET_TYPE, LOW_THR, 0);
         aucData[1] = WRITE_FIELD(aucData[1], VADCTM_INT_POLARITY_HIGH, LOW_THR, 1);
         aucData[2] = WRITE_FIELD(aucData[2], VADCTM_INT_POLARITY_LOW, LOW_THR, 0);
         break;
      case VADCTM_INTERRUPT_CONFIG_LEVEL_LOW:
         aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_SET_TYPE, LOW_THR, 0);
         aucData[1] = WRITE_FIELD(aucData[1], VADCTM_INT_POLARITY_HIGH, LOW_THR, 0);
         aucData[2] = WRITE_FIELD(aucData[2], VADCTM_INT_POLARITY_LOW, LOW_THR, 1);
         break;
      case VADCTM_INTERRUPT_CONFIG_RISING_EDGE:
         aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_SET_TYPE, LOW_THR, 1);
         aucData[1] = WRITE_FIELD(aucData[1], VADCTM_INT_POLARITY_HIGH, LOW_THR, 1);
         aucData[2] = WRITE_FIELD(aucData[2], VADCTM_INT_POLARITY_LOW, LOW_THR, 0);
         break;
      case VADCTM_INTERRUPT_CONFIG_FALLING_EDGE:
         aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_SET_TYPE, LOW_THR, 1);
         aucData[1] = WRITE_FIELD(aucData[1], VADCTM_INT_POLARITY_HIGH, LOW_THR, 0);
         aucData[2] = WRITE_FIELD(aucData[2], VADCTM_INT_POLARITY_LOW, LOW_THR, 1);
         break;
      case VADCTM_INTERRUPT_CONFIG_RISING_OR_FALLING_EDGE:
         aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_SET_TYPE, LOW_THR, 1);
         aucData[1] = WRITE_FIELD(aucData[1], VADCTM_INT_POLARITY_HIGH, LOW_THR, 1);
         aucData[2] = WRITE_FIELD(aucData[2], VADCTM_INT_POLARITY_LOW, LOW_THR, 1);
         break;
      default:
         break;
   }

   switch (pConfigs->eHighThresholdInterruptConfig)
   {
      case VADCTM_INTERRUPT_CONFIG_LEVEL_HIGH:
         aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_SET_TYPE, HIGH_THR, 0);
         aucData[1] = WRITE_FIELD(aucData[1], VADCTM_INT_POLARITY_HIGH, HIGH_THR, 1);
         aucData[2] = WRITE_FIELD(aucData[2], VADCTM_INT_POLARITY_LOW, HIGH_THR, 0);
         break;
      case VADCTM_INTERRUPT_CONFIG_LEVEL_LOW:
         aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_SET_TYPE, HIGH_THR, 0);
         aucData[1] = WRITE_FIELD(aucData[1], VADCTM_INT_POLARITY_HIGH, HIGH_THR, 0);
         aucData[2] = WRITE_FIELD(aucData[2], VADCTM_INT_POLARITY_LOW, HIGH_THR, 1);
         break;
      case VADCTM_INTERRUPT_CONFIG_RISING_EDGE:
         aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_SET_TYPE, HIGH_THR, 1);
         aucData[1] = WRITE_FIELD(aucData[1], VADCTM_INT_POLARITY_HIGH, HIGH_THR, 1);
         aucData[2] = WRITE_FIELD(aucData[2], VADCTM_INT_POLARITY_LOW, HIGH_THR, 0);
         break;
      case VADCTM_INTERRUPT_CONFIG_FALLING_EDGE:
         aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_SET_TYPE, HIGH_THR, 1);
         aucData[1] = WRITE_FIELD(aucData[1], VADCTM_INT_POLARITY_HIGH, HIGH_THR, 0);
         aucData[2] = WRITE_FIELD(aucData[2], VADCTM_INT_POLARITY_LOW, HIGH_THR, 1);
         break;
      case VADCTM_INTERRUPT_CONFIG_RISING_OR_FALLING_EDGE:
         aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_SET_TYPE, HIGH_THR, 1);
         aucData[1] = WRITE_FIELD(aucData[1], VADCTM_INT_POLARITY_HIGH, HIGH_THR, 1);
         aucData[2] = WRITE_FIELD(aucData[2], VADCTM_INT_POLARITY_LOW, HIGH_THR, 1);
         break;
      default:
         break;
   }

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalGetPendingInterrupts(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMInterruptMaskType *puIntrMask)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uTotalBytesRead;
   uint32 uOffset = VADCTM_INT_PENDING_STS_ADDR;

   result = piVAdcTMHal->pfnReadBytes(piVAdcTMHal->pCtxt,
                                      uOffset,
                                      aucData,
                                      ARRAY_LENGTH(aucData),
                                      &uTotalBytesRead);
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   if (uTotalBytesRead != ARRAY_LENGTH(aucData))
   {
      return VADCTM_HAL_ERROR;
   }

   *puIntrMask = READ_REG(aucData[0], VADCTM_INT_PENDING_STS);

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalClearInterrupts(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMInterruptMaskType uIntrMask)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uOffset = VADCTM_INT_LATCHED_CLR_ADDR;

   aucData[0] = WRITE_REG(0, VADCTM_INT_LATCHED_CLR, uIntrMask);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalEnableInterrupts(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMInterruptMaskType uIntrMask)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uOffset = VADCTM_INT_EN_SET_ADDR;

   aucData[0] = 0;  /* Other bits are not populated */
   aucData[0] = WRITE_REG(aucData[0], VADCTM_INT_EN_SET, uIntrMask);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetInterruptMid(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMInterruptMid uIntrMid)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uTotalBytesRead;
   uint32 uOffset = VADCTM_INT_MID_SEL_ADDR;

   result = piVAdcTMHal->pfnReadBytes(piVAdcTMHal->pCtxt,
                                      uOffset,
                                      aucData,
                                      ARRAY_LENGTH(aucData),
                                      &uTotalBytesRead);
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   if (uTotalBytesRead != ARRAY_LENGTH(aucData))
   {
      return VADCTM_HAL_ERROR;
   }

   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_MID_SEL, INT_MID_SEL, uIntrMid);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetOpMode(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMOpModeType eOpMode)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uOffset = VADCTM_MODE_CTL_ADDR;

   aucData[0] = 0x3;  /* AMUX_TRIM_EN = 1; ADC_TRIM_EN = 1 */
   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_MODE_CTL, OP_MODE, (uint8)eOpMode);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetEnable(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMEnableType eEnable)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uOffset = VADCTM_EN_CTL1_ADDR;

   aucData[0] = 0;  /* Only bit 7 exists, no need to read first */
   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_EN_CTL1, ADC_EN, (uint8)eEnable);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetConversionParameters(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMConversionParametersType *pParams)
{
   VAdcTMHalResultType result;
   uint8 aucData[2];
   uint32 uTotalBytesRead;
   uint32 uOffset;

   /* Set the dig params & HW settle delay */
   uOffset = VADCTM_ADC_DIG_PARAM_ADDR;

   aucData[0] = 0;  /* Other bits are not populated */
   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_ADC_DIG_PARAM, DEC_RATIO_SEL, (uint8)pParams->eDecimationRatio);
   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_ADC_DIG_PARAM, CLK_SEL, (uint8)pParams->eClockSelect);

   aucData[1] = 0;  /* Other bits are not populated */
   aucData[1] = WRITE_FIELD(aucData[1], VADCTM_HW_SETTLE_DELAY, HW_SETTLE_DELAY, (uint8)pParams->eSettlingDelay);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   /* Set the measurement interval operating mode */
   uOffset = VADCTM_MEAS_INTERVAL_OP_CTL_ADDR;
   result = piVAdcTMHal->pfnReadBytes(piVAdcTMHal->pCtxt,
                                      uOffset,
                                      aucData,
                                      sizeof(aucData[0]),
                                      &uTotalBytesRead);
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   if (uTotalBytesRead != sizeof(aucData[0]))
   {
      return VADCTM_HAL_ERROR;
   }

   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_MEAS_INTERVAL_OP_CTL, MEAS_INTERVAL_OP, (uint8)pParams->eIntervalMode);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       sizeof(aucData[0]));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   /* Set fast average mode */
   uOffset = VADCTM_FAST_AVG_CTL_ADDR;

   aucData[0] = 0;  /* Other bits are not populated */
   aucData[1] = 0;  /* Other bits are not populated */

   if (pParams->eFastAverageMode == VADCTM_FAST_AVERAGE_NONE)
   {
      aucData[0] = WRITE_FIELD(aucData[0], VADCTM_FAST_AVG_CTL, FAST_AVG_SAMPLES, 0);
      aucData[1] = WRITE_FIELD(aucData[1], VADCTM_FAST_AVG_EN, FAST_AVG_EN, 0);
   }
   else
   {
      aucData[0] = WRITE_FIELD(aucData[0], VADCTM_FAST_AVG_CTL, FAST_AVG_SAMPLES, (uint8)pParams->eFastAverageMode);
      aucData[1] = WRITE_FIELD(aucData[1], VADCTM_FAST_AVG_EN, FAST_AVG_EN, 1);
   }

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetSequencerParameters(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMSequencerParametersType *pParams)
{
   VAdcTMHalResultType result;
   uint8 aucData[2];
   uint32 uOffset = VADCTM_CONV_SEQ_CTL_ADDR;

   aucData[0] = 0;  /* Other bits are not populated */
   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_CONV_SEQ_CTL, CONV_SEQ_HOLDOFF, (uint8)pParams->eHoldoff);
   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_CONV_SEQ_CTL, CONV_SEQ_TIMEOUT, (uint8)pParams->eTimeoutTime);

   aucData[1] = 0;  /* Other bits are not populated */
   aucData[1] = WRITE_FIELD(aucData[1], VADCTM_CONV_SEQ_TRIG_CTL, CONV_SEQ_TRIG_COND, (uint8)pParams->eTriggerCondition);
   aucData[1] = WRITE_FIELD(aucData[1], VADCTM_CONV_SEQ_TRIG_CTL, CONV_SEQ_TRIG_SEL, (uint8)pParams->eTriggerInput);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalRequestConversion(VAdcTMHalInterfaceType *piVAdcTMHal)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uOffset = VADCTM_CONV_REQ_ADDR;

   aucData[0] = 0; /* register is write-only so cannot read the other bits - assume they are 0 */
   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_CONV_REQ, REQ, 1);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetMeasEnable(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMThresholdMaskType uSensorMask)
{
   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetLowThrIntEnable(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMThresholdMaskType uSensorMask)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uOffset = VADCTM_INT_EN_SET_ADDR;
   uint32 val = uSensorMask & 0x1;
   uint32 uTotalBytesRead;

   result = piVAdcTMHal->pfnReadBytes(piVAdcTMHal->pCtxt,
                                      uOffset,
                                      aucData,
                                      ARRAY_LENGTH(aucData),
                                      &uTotalBytesRead);
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   if (uTotalBytesRead != ARRAY_LENGTH(aucData))
   {
      return VADCTM_HAL_ERROR;
   }

   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_EN_SET, LOW_THR, val);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetHighThrIntEnable(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMThresholdMaskType uSensorMask)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uOffset = VADCTM_INT_EN_SET_ADDR;
   uint32 val = uSensorMask & 0x1;
   uint32 uTotalBytesRead;

   result = piVAdcTMHal->pfnReadBytes(piVAdcTMHal->pCtxt,
                                      uOffset,
                                      aucData,
                                      ARRAY_LENGTH(aucData),
                                      &uTotalBytesRead);
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   if (uTotalBytesRead != ARRAY_LENGTH(aucData))
   {
      return VADCTM_HAL_ERROR;
   }

   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_INT_EN_SET, HIGH_THR, val);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetMeasIntervalCtl(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIntervalCtlType *pMeasIntervalCtl)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uOffset = VADCTM_MEAS_INTERVAL_CTL_ADDR;

   aucData[0] = 0;  /* Other bits are not populated */
   aucData[0] = WRITE_FIELD(aucData[0], VADCTM_MEAS_INTERVAL_CTL, MEAS_INTERVAL_TIME1, (uint8)pMeasIntervalCtl->eMeasIntervalTime1);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetChannel(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIndexType uMeasIdx, VAdcTMAmuxChannelSelectType uChannel)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uOffset;

   if (uMeasIdx == 0)
   {
      uOffset = VADCTM_M0_ADC_CH_SEL_CTL_ADDR;
   }
   else
   {
      uOffset = VADCTM_Mn_ADC_CH_SEL_CTL_ADDR(uMeasIdx);
   }

   aucData[0] = (uint8)(uChannel & 0xFF);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetMeasInterval(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIndexType uMeasIdx, VAdcTMMeasIntervalTimeSelectType eMeasIntervalTimeSelect)
{
   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetLowThresholdCode(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIndexType uMeasIdx, VAdcTMConversionCodeType uCode)
{
   VAdcTMHalResultType result;
   uint8 aucData[2];
   uint32 uOffset;

   if (uMeasIdx == 0)
   {
      uOffset = VADCTM_M0_LOW_THR0_ADDR;
   }
   else
   {
      uOffset = VADCTM_Mn_LOW_THR0_ADDR(uMeasIdx);
   }

   aucData[0] = (uint8)(uCode & 0xFF);
   aucData[1] = (uint8)((uCode >> 8) & 0xFF);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalSetHighThresholdCode(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIndexType uMeasIdx, VAdcTMConversionCodeType uCode)
{
   VAdcTMHalResultType result;
   uint8 aucData[2];
   uint32 uOffset;

   if (uMeasIdx == 0)
   {
      uOffset = VADCTM_M0_HIGH_THR0_ADDR;
   }
   else
   {
      uOffset = VADCTM_Mn_HIGH_THR0_ADDR(uMeasIdx);
   }

   aucData[0] = (uint8)(uCode & 0xFF);
   aucData[1] = (uint8)((uCode >> 8) & 0xFF);

   result = piVAdcTMHal->pfnWriteBytes(piVAdcTMHal->pCtxt,
                                       uOffset,
                                       aucData,
                                       ARRAY_LENGTH(aucData));
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalGetConversionCode(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIndexType uMeasIdx, VAdcTMConversionCodeType *puCode)
{
   VAdcTMHalResultType result;
   uint8 aucData[2];
   uint32 uTotalBytesRead;
   uint32 uOffset;

   if (uMeasIdx == 0)
   {
      uOffset = VADCTM_M0_DATA0_ADDR;
   }
   else
   {
      uOffset = VADCTM_Mn_DATA0_ADDR(uMeasIdx);
   }

   result = piVAdcTMHal->pfnReadBytes(piVAdcTMHal->pCtxt,
                                      uOffset,
                                      aucData,
                                      ARRAY_LENGTH(aucData),
                                      &uTotalBytesRead);
   if (result != VADCTM_HAL_SUCCESS)
   {
      return result;
   }

   if (uTotalBytesRead != ARRAY_LENGTH(aucData))
   {
      return VADCTM_HAL_ERROR;
   }

   *puCode = (VAdcTMConversionCodeType)(((aucData[1] & 0xFF) << 8) | (aucData[0] & 0xFF));

   return VADCTM_HAL_SUCCESS;
}

VAdcTMHalResultType VAdcTMHalDumpRegisters(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMHalRegDumpType *pVAdcTMRegDump)
{
   VAdcTMHalResultType result;
   uint8 aucData[1];
   uint32 uTotalBytesRead;
   uint32 uOffset;
   uint32 uReg;

   for (uReg = 0; uReg < VADCTM_HAL_NUM_REGISTERS_TO_DUMP; uReg++)
   {
      uOffset = vAdcHalRegDumpAddresses[uReg];

      result = piVAdcTMHal->pfnReadBytes(piVAdcTMHal->pCtxt,
                                         uOffset,
                                         aucData,
                                         ARRAY_LENGTH(aucData),
                                         &uTotalBytesRead);
      if (result != VADCTM_HAL_SUCCESS)
      {
         return result;
      }

      if (uTotalBytesRead != ARRAY_LENGTH(aucData))
      {
         return VADCTM_HAL_ERROR;
      }

      pVAdcTMRegDump->aVAdcTMReg[uReg].u8Offset = (uint8)uOffset;
      pVAdcTMRegDump->aVAdcTMReg[uReg].u8Val = aucData[0];
   }

   return VADCTM_HAL_SUCCESS;
}

