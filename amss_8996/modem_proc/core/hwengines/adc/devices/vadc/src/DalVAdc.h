#ifndef __DALVADC_H__
#define __DALVADC_H__
/*============================================================================
  @file DalVAdc.h

  Function and data structure declarations for VADC DAL


                Copyright (c) 2008-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/* $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/adc/devices/vadc/src/DalVAdc.h#1 $ */

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "DALFramework.h"
#include "DDIAdcDevice.h"
#include "DDISpmi.h"
#include "npa.h"
#include "AdcScalingUtil.h"
#include "VAdcHal.h"
#include "pmic.h"
#include "VAdcLog.h"
#include "VAdcTMDevice.h"

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/
#define VADC_MAX_NUM_DEVICES 2

enum
{
   VADC_EVENT_DEFAULT = 0,
   VADC_EVENT_TIMEOUT,
   _VADC_NUM_EVENTS
};

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/
typedef enum
{
   VADC_SCALE_TO_MILLIVOLTS = 0,
   VADC_SCALE_PMIC_SENSOR_TO_MILLIDEGREES,
   VADC_SCALE_INTERPOLATE_FROM_RT_R25,
   VADC_SCALE_INTERPOLATE_FROM_MILLIVOLTS,
   VADC_SCALE_THERMISTOR
} VAdcScalingMethodType;

/*
 * VAdcConfigType:
 *
 * eDecimationRatio   -- Decimation Ratio for the filter.
 * eClockSelect       -- VADC clock rate.
 * uConversionTime_us -- conversion time depends on the above configuration.
 */
typedef struct
{
   VAdcDecimationRatioType eDecimationRatio;
   VAdcClockSelectType eClockSelect;
   uint32 uConversionTime_us;
} VAdcConfigType;

typedef struct
{
   uint32 uNumerator;
   uint32 uDenominator;
} VAdcChannelScalingFactor;

typedef enum
{
   VADC_CHANNEL_MPP_CONFIG_NONE,
   VADC_CHANNEL_MPP_CONFIG_STATIC
} VAdcChannelMppConfigType;

/*
 * VAdcChannelConfigType:
 *
 * pName                      -- Channel name as defined in core/api/AdcInputs
 * uAdcHardwareChannel        -- PMIC hardware channel number
 * uConfigIdx                 -- VAdc Filter Configuration used for this
 *                               channel. Refer to VAdcConfigType for more
 *                               details.
 * eSettlingDelay             -- Amount of time to wait for the signal to settle
 *                               after requesting the conversion
 * eFastAverageMode           -- If enabled, will automatically take multiple
 *                               readings and average them
 * bUseSequencer              -- Whether or not to use the conversion sequencer
 *                               for readings on this channel
 * uSequencerIdx              -- Sequencer index to use if we�re using the
 *                               sequencer
 * scalingFactor              -- Since the inputs are prescaled, we must know
 *                               the hardware�s scaling factor so that we can
 *                               multiply by the appropriate amount to get the
 *                               correct (unscaled) reading value
 * eScalingMethod             -- Scaling method for converting the reading to
 *                               the appropriate units. Ratiometric is for
                                 resistor divider-style inputs like BATT_THERM.
                                 Absolute calibration should be used for signals
                                 with an absolute value, e.g. VBATT
 * pInterpolationTable        -- Scaling function (if required by the scaling
 *                               method) to convert a reading to units other
 *                               than millivolts.
 * uInterpolationTableLength  -- the length of the interpolation table
 * eCalMethod                 -- The calibration type we'll use for a
 *                               particular channel.
 *
 * eMppConfig                 -- How to map the mpp � statically or not at all
 * eMpp                       -- The source MPP to use.
 * eChSelect                  -- The AMUX channel to route the MPP to.
 *
 * MPP routing:
 *    mpp -> amux
 *     1  ->  5
 *     2  ->  6
 *     3  ->  7
 *     4  ->  8
 *     5  ->  5
 *     6  ->  6
 *     7  ->  7
 *     8  ->  8
 */
typedef struct
{
   const char *pName;
   VAdcAmuxChannelSelectType uAdcHardwareChannel;
   uint32 uConfigIdx;
   VAdcSettlingDelay eSettlingDelay;
   VAdcFastAverageModeType eFastAverageMode;
   DALBOOL bUseSequencer;
   uint32 uSequencerIdx;
   VAdcChannelScalingFactor scalingFactor;
   VAdcScalingMethodType eScalingMethod;
   const AdcMapPtInt32toInt32Type *pInterpolationTable;
   uint32 uInterpolationTableLength;
   uint32 uPullUp;
   enum
   {
      VADC_CAL_METHOD_RATIOMETRIC,
      VADC_CAL_METHOD_ABSOLUTE
   }  eCalMethod;
   VAdcChannelMppConfigType eMppConfig;
   pm_mpp_which_type eMpp;
   pm_mpp_ain_ch_type eChSelect;
} VAdcChannelConfigType;

/*
 * VAdcBspType
 */
typedef struct
{
   SpmiBus_AccessPriorityType eAccessPriority;
   uint32 uSlaveId;
   uint32 uPmicDevice;
   uint32 uPeripheralID;
   uint32 uMasterID;
   DALBOOL bUsesInterrupts;
   DALBOOL bHasTM;
   uint32 uMinDigMinor;
   uint32 uMinDigMajor;
   uint32 uMinAnaMinor;
   uint32 uMinAnaMajor;
   uint32 uPerphType;
   uint32 uPerphSubType;
   uint32 uVrefP_mv;
   uint32 uVrefN_mv;
   uint32 uVref1_mv;
   uint32 uVref2_mv;
   uint32 uReadTimeout_us;
   uint32 uLDOSettlingTime_us;
   uint32 uNumSequencerConfigs;
   const VAdcSequencerParametersType *paSequencerParams;
   uint32 uNumConfigs;
   const VAdcConfigType *paConfigs;
   uint32 uNumChannels;
   const VAdcChannelConfigType *paChannels;
   uint32 uNumCalChannels;
   const VAdcChannelConfigType *paCalChannels;
} VAdcBspType;

/*
 * VAdcCalibDataType:
 *
 * Here, we store the calibration data for the VAdc. Calibration Data is
 * used to determine the error present the VADC circuit. Ideally, ground
 * should give us a raw value of 0x0 but this may not be the case because
 * of error (both internal and external) introduced in the circuit.
 *
 * Here, we measure set of standard reference volatge to determine their
 * raw value. We than use the raw value to make correction on the scaled
 * data.
 *
 * uVrefP            -- Raw ADC Data for VRef voltage.
 * uVrefN            -- Raw ADC Data for Ground.
 * uVref1            -- Raw ADC Data for mvRef1. mvRef1 is defined in bsp.
 * uVref2            -- Raw ADC Data for mvRef2. mvRef2 is defined in bsp.
 */
typedef struct
{
   uint32 uVrefP;
   uint32 uVrefN;
   uint32 uVref1;
   uint32 uVref2;
} VAdcCalibDataType;

typedef struct
{
   uint32 uPercentVrefP;
   uint32 uPercentVrefN;
   uint32 uPercentVref1;
   uint32 uPercentVref2;
} VAdcScalingDataType;

/*
 * Declaring a "VAdc" Driver, device and client context
 */
typedef struct VAdcDrvCtxt VAdcDrvCtxt;
typedef struct VAdcDevCtxt VAdcDevCtxt;
typedef struct VAdcClientCtxt VAdcClientCtxt;

/*
 * Declaring a private "VAdc" Vtable
 */
typedef struct VAdcDALVtbl VAdcDALVtbl;
struct VAdcDALVtbl
{
  int (*VAdc_DriverInit)(VAdcDrvCtxt *);
  int (*VAdc_DriverDeInit)(VAdcDrvCtxt *);
};

typedef struct
{
   VAdcDevCtxt *pDevCtxt;
} VAdcHalInterfaceCtxtType;

struct VAdcDevCtxt
{
   // Base Members
   uint32 dwRefs;
   DALDEVICEID DevId;
   uint32 dwDevCtxtRefIdx;
   VAdcDrvCtxt *pVAdcDrvCtxt;
   DALSYS_PROPERTY_HANDLE_DECLARE(hProp);
   uint32 Reserved[16];
   // VAdc Dev state can be added by developers here
   VAdcDebugInfoType vAdcDebug;                     /* debug structure */
   VAdcScalingDataType vAdcScalingData;             /* scaling data */
   VAdcHalInterfaceCtxtType vAdcHalInterfaceCtxt;   /* VADC HAL interface context */
   VAdcHalInterfaceType iVAdcHalInterface;          /* VADC HAL interface */
   VAdcRevisionInfoType revisionInfo;               /* VADC revision info */
   pm_device_info_type pmicDeviceInfo;              /* PMIC device info */
   const VAdcBspType *pBsp;                         /* pointer to the BSP */
   VAdcCalibDataType *paCalibData;                  /* pointer to measured VADC calibration data */
   VAdcTMDevCtxt *pVAdcTMDevCtxt;                   /* VADC TM device context */
   DalDeviceHandle *phSpmiDev;                      /* handle to the SPMI DAL device */
   DALSYSEventHandle ahEocEvent[_VADC_NUM_EVENTS];  /* end-of-conversion event - EOC signal & a timeout */
   npa_client_handle hNPACpuLatency;                /* npa handle for min latency vote */
   npa_client_handle hNPAPmicHkadc;                 /* npa handle for VREG */
   DALBOOL bHardwareSupported;                      /* flag to indicate if the hardware is supported */
   DALBOOL bCalibrated;                             /* indicates if the VADC has been calibrated */
};

struct VAdcDrvCtxt
{
   // Base Members
   VAdcDALVtbl VAdcDALVtbl;
   uint32 dwNumDev;
   uint32 dwSizeDevCtxt;
   uint32 bInit;
   uint32 dwRefs;
   VAdcDevCtxt VAdcDevCtxt[VADC_MAX_NUM_DEVICES];
   // VAdc Drv state can be added by developers here
};

/*
 * Declaring a "VAdc" Client Context
 */
struct VAdcClientCtxt
{
   // Base Members
   uint32 dwRefs;
   uint32 dwAccessMode;
   void *pPortCtxt;
   VAdcDevCtxt *pVAdcDevCtxt;
   DalAdcDeviceHandle DalAdcDeviceHandle;
   // VAdc Client state can be added by developers here
};

/*-------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ----------------------------------------------------------------------*/
/* Non-DAL functions */
uint32
VAdc_GetHardwareChannel(
   VAdcDevCtxt *pDevCtxt,
   uint32 uChannelIdx
   );

void
VAdc_GetCalibration(
   VAdcDevCtxt *pDevCtxt,
   uint32 uConfigIdx,
   VAdcCalibDataType *pCalibData
   );

void
VAdc_ProcessConversionResult(
   VAdcDevCtxt *pDevCtxt,
   uint32 uChannel,
   const VAdcCalibDataType *pCalibData,
   uint32 uCode,
   AdcDeviceResultType *pResult
   );

void
VAdc_ProcessConversionResultInverse(
   VAdcDevCtxt *pDevCtxt,
   uint32 uChannel,
   const VAdcCalibDataType *pCalibData,
   int32 nPhysical,
   AdcDeviceResultType *pResult
   );

void
VAdc_ProcessConversionResultFromMilliVolts(
   VAdcDevCtxt *pDevCtxt,
   uint32 uChannel,
   const VAdcCalibDataType *pCalibData,
   int32 nMilliVolts_noPrescalar,
   AdcDeviceResultType *pResult
   );

/* Functions specific to DAL */
DALResult VAdc_DalAdcDevice_Attach(const char *, DALDEVICEID, DalDeviceHandle **);
DALResult VAdc_DriverInit(VAdcDrvCtxt *);
DALResult VAdc_DriverDeInit(VAdcDrvCtxt *);
DALResult VAdc_DeviceInit(VAdcClientCtxt *);
DALResult VAdc_DeviceDeInit(VAdcClientCtxt *);
DALResult VAdc_Open(VAdcClientCtxt *, uint32);
DALResult VAdc_Close(VAdcClientCtxt *);

/* Functions specific to DalAdcDevice interface */
DALResult VAdc_GetDeviceProperties(VAdcClientCtxt *, AdcDeviceDevicePropertiesType *);
DALResult VAdc_GetChannel(VAdcClientCtxt *, const char *, uint32 *);
DALResult VAdc_ReadChannel(VAdcClientCtxt *, uint32, AdcDeviceResultType *);
DALResult VAdc_RecalibrateChannel(VAdcClientCtxt *, uint32, AdcDeviceRecalibrateResultType *);

#endif /* #ifndef __DALVADC_H__ */

