#ifndef __VADCTMHAL_H__
#define __VADCTMHAL_H__
/*============================================================================
  @file VAdcTMHal.h

  Function and data structure declarations for VADC TM HAL


               Copyright (c) 2012, 2015 Qualcomm Technologies, Inc.
               All Rights Reserved.
               Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/* $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/adc/devices/vadc/src/VAdcTMHal.h#1 $ */

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "com_dtypes.h"

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/
#define VADCTM_HAL_NUM_REGISTERS_TO_DUMP 73

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/
typedef struct
{
   uint8 uDigitalMajor;
   uint8 uDigitalMinor;
   uint8 uAnalogMajor;
   uint8 uAnalogMinor;
   uint8 uType;
   uint8 uSubType;
} VAdcTMRevisionInfoType;

typedef enum
{
   VADCTM_OP_MODE_NORMAL = 0,
   VADCTM_OP_MODE_SEQUENCER,
   VADCTM_OP_MODE_INTERVAL
} VAdcTMOpModeType;

typedef enum
{
   VADCTM_INTERVAL_MODE_SINGLE = 0,
   VADCTM_INTERVAL_MODE_CONTINUOUS
} VAdcTMIntervalModeType;

typedef enum
{
   VADCTM_CONVERSION_STATUS_INVALID = 0,
   VADCTM_CONVERSION_STATUS_COMPLETE,
   VADCTM_CONVERSION_STATUS_PENDING,
   VADCTM_CONVERSION_STATUS_WAITING
} VAdcTMConversionStatusType;

typedef enum
{
   VADCTM_SEQUENCER_STATUS_IDLE = 0,
   VADCTM_SEQUENCER_STATUS_WAITING_FOR_TRIGGER,
   VADCTM_SEQUENCER_STATUS_WAITING_FOR_HOLDOFF,
   VADCTM_SEQUENCER_STATUS_WAITING_FOR_EOC
} VAdcTMSequencerStatusType;

typedef uint32 VAdcTMThresholdMaskType;

typedef struct
{
   VAdcTMOpModeType eOpMode;
   VAdcTMIntervalModeType eIntervalMode;
   VAdcTMConversionStatusType eConversionStatus;
   VAdcTMSequencerStatusType eSequencerStatus;
   boolean bSequencerFifoNotEmptyErrorOccurred;
   boolean bSequencerTimeoutErrorOccurred;
   VAdcTMThresholdMaskType lowThresholdStatus;
   VAdcTMThresholdMaskType highThresholdStatus;
} VAdcTMStatusType;

typedef enum
{
   VADCTM_DISABLE = 0,
   VADCTM_ENABLE
} VAdcTMEnableType;

typedef enum
{
   VADCTM_DECIMATION_RATIO_512 = 0,
   VADCTM_DECIMATION_RATIO_1024,
   VADCTM_DECIMATION_RATIO_2048,
   VADCTM_DECIMATION_RATIO_4096
} VAdcTMDecimationRatioType;

typedef enum
{
   VADCTM_CLOCK_SELECT_2P4_MHZ = 0,
   VADCTM_CLOCK_SELECT_4P8_MHZ
   /*
    * Note: Although register values exist for the 9.6 MHZ and 19.2 MHZ clock configurations,
    * these clock modes are not supported by the VADCTM HW.
    */
} VAdcTMClockSelectType;

typedef uint32 VAdcTMAmuxChannelSelectType;

typedef uint32 VAdcTMMeasIndexType;

typedef enum
{
   VADCTM_SETTLING_DELAY_0_US = 0,
   VADCTM_SETTLING_DELAY_100_US,
   VADCTM_SETTLING_DELAY_200_US,
   VADCTM_SETTLING_DELAY_300_US,
   VADCTM_SETTLING_DELAY_400_US,
   VADCTM_SETTLING_DELAY_500_US,
   VADCTM_SETTLING_DELAY_600_US,
   VADCTM_SETTLING_DELAY_700_US,
   VADCTM_SETTLING_DELAY_800_US,
   VADCTM_SETTLING_DELAY_900_US,
   VADCTM_SETTLING_DELAY_1_MS,
   VADCTM_SETTLING_DELAY_2_MS,
   VADCTM_SETTLING_DELAY_4_MS,
   VADCTM_SETTLING_DELAY_6_MS,
   VADCTM_SETTLING_DELAY_8_MS,
   VADCTM_SETTLING_DELAY_10_MS
} VAdcTMSettlingDelay;

typedef uint32 VAdcTMConversionCodeType;

typedef enum
{
   VADCTM_SEQUENCER_HOLDOFF_25_US = 0,
   VADCTM_SEQUENCER_HOLDOFF_50_US,
   VADCTM_SEQUENCER_HOLDOFF_75_US,
   VADCTM_SEQUENCER_HOLDOFF_100_US,
   VADCTM_SEQUENCER_HOLDOFF_125_US,
   VADCTM_SEQUENCER_HOLDOFF_150_US,
   VADCTM_SEQUENCER_HOLDOFF_175_US,
   VADCTM_SEQUENCER_HOLDOFF_200_US,
   VADCTM_SEQUENCER_HOLDOFF_225_US,
   VADCTM_SEQUENCER_HOLDOFF_250_US,
   VADCTM_SEQUENCER_HOLDOFF_275_US,
   VADCTM_SEQUENCER_HOLDOFF_300_US,
   VADCTM_SEQUENCER_HOLDOFF_325_US,
   VADCTM_SEQUENCER_HOLDOFF_350_US,
   VADCTM_SEQUENCER_HOLDOFF_375_US,
   VADCTM_SEQUENCER_HOLDOFF_400_US,
} VAdcTMSequencerHoldoffType;

typedef enum
{
   VADCTM_SEQUENCER_TIMEOUT_TIME_0_MS = 0,
   VADCTM_SEQUENCER_TIMEOUT_TIME_1_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_2_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_3_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_4_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_5_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_6_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_7_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_8_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_9_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_10_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_11_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_12_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_13_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_14_MS,
   VADCTM_SEQUENCER_TIMEOUT_TIME_15_MS
} VAdcTMSequencerTimeoutTimeType;

typedef enum
{
   VADCTM_TRIGGER_CONDITION_FALLING_EDGE = 0,
   VADCTM_TRIGGER_CONDITION_RISING_EDGE
} VAdcTMTriggerConditionType;

typedef enum
{
   VADCTM_TRIGGER_INPUT_TRIG_1 = 0,
   VADCTM_TRIGGER_INPUT_TRIG_2,
   VADCTM_TRIGGER_INPUT_TRIG_3,
   VADCTM_TRIGGER_INPUT_TRIG_4
} VAdcTMTriggerInputType;

typedef enum
{
   VADCTM_FAST_AVERAGE_1_SAMPLE = 0,
   VADCTM_FAST_AVERAGE_2_SAMPLES,
   VADCTM_FAST_AVERAGE_4_SAMPLES,
   VADCTM_FAST_AVERAGE_8_SAMPLES,
   VADCTM_FAST_AVERAGE_16_SAMPLES,
   VADCTM_FAST_AVERAGE_32_SAMPLES,
   VADCTM_FAST_AVERAGE_64_SAMPLES,
   VADCTM_FAST_AVERAGE_128_SAMPLES,
   VADCTM_FAST_AVERAGE_256_SAMPLES,
   VADCTM_FAST_AVERAGE_512_SAMPLES,
   VADCTM_FAST_AVERAGE_NONE = 0xFFFF
} VAdcTMFastAverageModeType;

typedef struct
{
   VAdcTMIntervalModeType eIntervalMode;
   VAdcTMDecimationRatioType eDecimationRatio;
   VAdcTMClockSelectType eClockSelect;
   VAdcTMSettlingDelay eSettlingDelay;
   VAdcTMFastAverageModeType eFastAverageMode;
} VAdcTMConversionParametersType;

typedef struct
{
   VAdcTMSequencerHoldoffType eHoldoff;
   VAdcTMSequencerTimeoutTimeType eTimeoutTime;
   VAdcTMTriggerConditionType eTriggerCondition;
   VAdcTMTriggerInputType eTriggerInput;
} VAdcTMSequencerParametersType;

typedef enum
{
   VADCTM_MEAS_INTERVAL_TIME1_0_MS = 0,
   VADCTM_MEAS_INTERVAL_TIME1_1_MS,
   VADCTM_MEAS_INTERVAL_TIME1_2_MS,
   VADCTM_MEAS_INTERVAL_TIME1_3P9_MS,
   VADCTM_MEAS_INTERVAL_TIME1_7P8_MS,
   VADCTM_MEAS_INTERVAL_TIME1_15P6_MS,
   VADCTM_MEAS_INTERVAL_TIME1_31P1_MS,
   VADCTM_MEAS_INTERVAL_TIME1_62P5_MS,
   VADCTM_MEAS_INTERVAL_TIME1_125_MS,
   VADCTM_MEAS_INTERVAL_TIME1_250_MS,
   VADCTM_MEAS_INTERVAL_TIME1_500_MS,
   VADCTM_MEAS_INTERVAL_TIME1_1_S,
   VADCTM_MEAS_INTERVAL_TIME1_2_S,
   VADCTM_MEAS_INTERVAL_TIME1_4_S,
   VADCTM_MEAS_INTERVAL_TIME1_8_S,
   VADCTM_MEAS_INTERVAL_TIME1_16_S
} VAdcTMMeasIntervalTime1Type;

typedef enum
{
   VADCTM_MEAS_INTERVAL_TIME2_0_MS = 0,
   VADCTM_MEAS_INTERVAL_TIME2_100_MS,
   VADCTM_MEAS_INTERVAL_TIME2_200_MS,
   VADCTM_MEAS_INTERVAL_TIME2_300_MS,
   VADCTM_MEAS_INTERVAL_TIME2_400_MS,
   VADCTM_MEAS_INTERVAL_TIME2_500_MS,
   VADCTM_MEAS_INTERVAL_TIME2_600_MS,
   VADCTM_MEAS_INTERVAL_TIME2_700_MS,
   VADCTM_MEAS_INTERVAL_TIME2_800_MS,
   VADCTM_MEAS_INTERVAL_TIME2_900_MS,
   VADCTM_MEAS_INTERVAL_TIME2_1_S,
   VADCTM_MEAS_INTERVAL_TIME2_1P1_S,
   VADCTM_MEAS_INTERVAL_TIME2_1P2_S,
   VADCTM_MEAS_INTERVAL_TIME2_1P3_S,
   VADCTM_MEAS_INTERVAL_TIME2_1P4_S,
   VADCTM_MEAS_INTERVAL_TIME2_1P5_S
} VAdcTMMeasIntervalTime2Type;

typedef enum
{
   VADCTM_MEAS_INTERVAL_TIME3_0_S = 0,
   VADCTM_MEAS_INTERVAL_TIME3_1_S,
   VADCTM_MEAS_INTERVAL_TIME3_2_S,
   VADCTM_MEAS_INTERVAL_TIME3_3_S,
   VADCTM_MEAS_INTERVAL_TIME3_4_S,
   VADCTM_MEAS_INTERVAL_TIME3_5_S,
   VADCTM_MEAS_INTERVAL_TIME3_6_S,
   VADCTM_MEAS_INTERVAL_TIME3_7_S,
   VADCTM_MEAS_INTERVAL_TIME3_8_S,
   VADCTM_MEAS_INTERVAL_TIME3_9_S,
   VADCTM_MEAS_INTERVAL_TIME3_10_S,
   VADCTM_MEAS_INTERVAL_TIME3_11_S,
   VADCTM_MEAS_INTERVAL_TIME3_12_S,
   VADCTM_MEAS_INTERVAL_TIME3_13_S,
   VADCTM_MEAS_INTERVAL_TIME3_14_S,
   VADCTM_MEAS_INTERVAL_TIME3_15_S
} VAdcTMMeasIntervalTime3Type;

typedef struct
{
   VAdcTMMeasIntervalTime1Type eMeasIntervalTime1;
   VAdcTMMeasIntervalTime2Type eMeasIntervalTime2;
   VAdcTMMeasIntervalTime3Type eMeasIntervalTime3;
} VAdcTMMeasIntervalCtlType;

typedef enum
{
   VADCTM_MEAS_INTERVAL_TIME1 = 0,
   VADCTM_MEAS_INTERVAL_TIME2,
   VADCTM_MEAS_INTERVAL_TIME3
} VAdcTMMeasIntervalTimeSelectType;

typedef enum
{
   VADCTM_INTERRUPT_CONFIG_NONE = 0,
   VADCTM_INTERRUPT_CONFIG_LEVEL_HIGH,
   VADCTM_INTERRUPT_CONFIG_LEVEL_LOW,
   VADCTM_INTERRUPT_CONFIG_RISING_EDGE,
   VADCTM_INTERRUPT_CONFIG_FALLING_EDGE,
   VADCTM_INTERRUPT_CONFIG_RISING_OR_FALLING_EDGE,
} VAdcTMInterruptConfigType;

typedef struct
{
   VAdcTMInterruptConfigType eEocInterruptConfig;
   VAdcTMInterruptConfigType eFifoNotEmptyInterruptConfig;
   VAdcTMInterruptConfigType eSequencerTimeoutInterruptConfig;
   VAdcTMInterruptConfigType eHighThresholdInterruptConfig;
   VAdcTMInterruptConfigType eLowThresholdInterruptConfig;
} VAdcTMInterruptsConfigType;

typedef enum
{
   VADCTM_INTERRUPT_MASK_BIT_EOC = 0x1,
   VADCTM_INTERRUPT_MASK_BIT_FIFO_NOT_EMPTY = 0x2,
   VADCTM_INTERRUPT_MASK_BIT_SEQUENCER_TIMEOUT = 0x4,
   VADCTM_INTERRUPT_MASK_BIT_HIGH_THRESHOLD = 0x8,
   VADCTM_INTERRUPT_MASK_BIT_LOW_THRESHOLD = 0x10,
} VAdcTMInterruptMaskBitsType;

typedef uint32 VAdcTMInterruptMaskType;

typedef uint32 VAdcTMInterruptMid;

typedef struct
{
   uint8 u8Offset;
   uint8 u8Val;
} VAdcTMHalRegType;

typedef struct
{
   VAdcTMHalRegType aVAdcTMReg[VADCTM_HAL_NUM_REGISTERS_TO_DUMP];
} VAdcTMHalRegDumpType;

typedef enum
{
   VADCTM_HAL_SUCCESS = 0,
   VADCTM_HAL_ERROR = 1
} VAdcTMHalResultType;

typedef struct
{
   void *pCtxt;
   VAdcTMHalResultType(* pfnWriteBytes)(void *pCtxt, uint32 uRegisterAddress, unsigned char* pucData, uint32 uDataLen);
   VAdcTMHalResultType(* pfnReadBytes)(void *pCtxt, uint32 uRegisterAddress, unsigned char* pucData, uint32 uDataLen, uint32* puTotalBytesRead);
} VAdcTMHalInterfaceType;

/*-------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ----------------------------------------------------------------------*/
VAdcTMHalResultType VAdcTMHalGetRevisionInfo(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMRevisionInfoType *pRevisionInfo);
VAdcTMHalResultType VAdcTMHalGetStatus(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMStatusType *pStatus);
VAdcTMHalResultType VAdcTMHalConfigInterrupts(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMInterruptsConfigType *pConfigs);
VAdcTMHalResultType VAdcTMHalGetPendingInterrupts(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMInterruptMaskType *puIntrMask);
VAdcTMHalResultType VAdcTMHalClearInterrupts(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMInterruptMaskType uIntrMask);
VAdcTMHalResultType VAdcTMHalEnableInterrupts(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMInterruptMaskType uIntrMask);
VAdcTMHalResultType VAdcTMHalSetInterruptMid(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMInterruptMid uIntrMid);
VAdcTMHalResultType VAdcTMHalSetOpMode(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMOpModeType eOpMode);
VAdcTMHalResultType VAdcTMHalSetEnable(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMEnableType eEnable);
VAdcTMHalResultType VAdcTMHalSetConversionParameters(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMConversionParametersType *pParams);
VAdcTMHalResultType VAdcTMHalSetSequencerParameters(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMSequencerParametersType *pParams);
VAdcTMHalResultType VAdcTMHalRequestConversion(VAdcTMHalInterfaceType *piVAdcTMHal);
VAdcTMHalResultType VAdcTMHalSetMeasEnable(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMThresholdMaskType uSensorMask);
VAdcTMHalResultType VAdcTMHalSetLowThrIntEnable(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMThresholdMaskType uSensorMask);
VAdcTMHalResultType VAdcTMHalSetHighThrIntEnable(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMThresholdMaskType uSensorMask);
VAdcTMHalResultType VAdcTMHalSetMeasIntervalCtl(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIntervalCtlType *pMeasIntervalCtl);
VAdcTMHalResultType VAdcTMHalSetChannel(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIndexType uMeasIdx, VAdcTMAmuxChannelSelectType uChannel);
VAdcTMHalResultType VAdcTMHalSetMeasInterval(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIndexType uMeasIdx, VAdcTMMeasIntervalTimeSelectType eMeasIntervalTimeSelect);
VAdcTMHalResultType VAdcTMHalSetLowThresholdCode(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIndexType uMeasIdx, VAdcTMConversionCodeType uCode);
VAdcTMHalResultType VAdcTMHalSetHighThresholdCode(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIndexType uMeasIdx, VAdcTMConversionCodeType uCode);
VAdcTMHalResultType VAdcTMHalGetConversionCode(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMMeasIndexType uMeasIdx, VAdcTMConversionCodeType *puCode);
VAdcTMHalResultType VAdcTMHalDumpRegisters(VAdcTMHalInterfaceType *piVAdcTMHal, VAdcTMHalRegDumpType *pVAdcTMRegDump);

#endif /* #ifndef __VADCTMHAL_H__ */
