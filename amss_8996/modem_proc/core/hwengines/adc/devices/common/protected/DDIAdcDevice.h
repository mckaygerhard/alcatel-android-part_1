#ifndef __DDIADCDEVICE_H__
#define __DDIADCDEVICE_H__
/*============================================================================
  @file DDIAdcDevice.h

  ADC Physical Device Driver Interface header

  This header is to be included solely by the ADC DAL and implementations of
  this DDI. It provides a common interface for the ADC DAL to interface with
  different ADC hardware.

  External clients may not access these interfaces.

                Copyright (c) 2008-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/* $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/adc/devices/common/protected/DDIAdcDevice.h#1 $ */

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "DalDevice.h"

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/
#define DALADCDEVICE_INTERFACE_VERSION DALINTERFACE_VERSION(4,1)

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/
enum
{
   ADC_DEVICE_ERROR_OUT_OF_TM_CLIENTS = 1,
   ADC_DEVICE_ERROR_TM_NOT_SUPPORTED,
   ADC_DEVICE_ERROR_TM_THRESHOLD_OUT_OF_RANGE,
   ADC_DEVICE_ERROR_TM_INVALID_MEAS_IDX,
   ADC_DEVICE_ERROR_TM_BUSY
};

typedef enum
{
   ADC_DEVICE_CHANNEL_READY,
   ADC_DEVICE_CHANNEL_BUSY,
   ADC_DEVICE_CHANNEL_CALIBRATING,
   ADC_DEVICE_CHANNEL_ERROR,
   _ADC_MAX_DEVICE_CHANNEL_STATUS = 0x7FFFFFFF
} AdcDeviceChannelStatusType;

typedef enum
{
   ADC_DEVICE_RESULT_VALID,
   ADC_DEVICE_RESULT_INVALID,
   ADC_DEVICE_RESULT_INVALID_CHANNEL,
   ADC_DEVICE_RESULT_TIMEOUT,
   ADC_DEVICE_RESULT_FIFO_NOT_EMPTY,
   _ADC_MAX_DEVICE_RESULT_STATUS = 0x7FFFFFFF
} AdcDeviceResultStatusType;

typedef struct
{
   uint32 uNumChannels;      /* number of ADC channels */
} AdcDeviceDevicePropertiesType;

typedef struct
{
   AdcDeviceResultStatusType eStatus;
   int32 nPhysical1_uV;    /* ref 1 in microvolts */
   int32 nPhysical2_uV;    /* ref 2 in microvolts */
   uint32 uCode1;          /* raw ADC code for ref 1 */
   uint32 uCode2;          /* raw ADC code for ref 2 */
} AdcDeviceRecalibrateResultType;

typedef struct
{
   AdcDeviceResultStatusType eStatus;
   int32 nPhysical;     /* result in physical units. Units depend on BSP */
   uint32 uPercent;     /* result as percentage of reference voltage used
                         * for conversion. 0 = 0%, 65535 = 100% */
   uint32 uMicrovolts;  /* result in microvolts */
   uint32 uCode;        /* raw ADC code */
} AdcDeviceResultType;

typedef struct
{
   int32 nPhysicalMin;   /* Minimum threshold in physical units */
   int32 nPhysicalMax;   /* Maximum threshold in physical units */
} AdcDeviceTMRangeType;

typedef enum
{
   ADC_DEVICE_TM_THRESHOLD_LOWER,    /* Lower threshold */
   ADC_DEVICE_TM_THRESHOLD_HIGHER,   /* Higher threshold */
   ADC_DEVICE_TM_NUM_THRESHOLDS
} AdcDeviceTMThresholdType;

/*-------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ----------------------------------------------------------------------*/
DALResult
GetInputProperties(DalDeviceHandle *h,
   /*in*/ const char *pChannelName,
   /*out*/ uint32 *puChannelIdx);

DALResult
GetDeviceProperties(DalDeviceHandle *h,
   /*out*/ AdcDeviceDevicePropertiesType *pAdcDeviceProp);

DALResult
RecalibrateChannel(DalDeviceHandle *h,
   /*in*/ uint32 uChannelIdx,
   /*out*/ AdcDeviceRecalibrateResultType *pAdcDeviceRecalibrateResult);

DALResult
ReadChannel(DalDeviceHandle *h,
   /*in*/ uint32 uChannelIdx,
   /*out*/ AdcDeviceResultType *pAdcDeviceReadResult);

DALResult
TMGetRange(DalDeviceHandle *h,
   /*in*/ uint32 uMeasIdx,
   /*out*/ AdcDeviceTMRangeType *pAdcTMDeviceRange);

DALResult
TMSetThreshold(DalDeviceHandle * h,
   /*in*/ const char * pInputName,
   /*out*/ uint32 *puChannelIdx);

DALResult
TMGetInputProperties(DalDeviceHandle *h,
   /*in*/ uint32 uChannelIdx,
   /*in*/ const DALSYSEventHandle hEvent,
   /*in*/ AdcDeviceTMThresholdType eThreshold,
   /*inout*/ int32 *pnThreshold_physical);

DALResult
TMSetEnableThresholds(DalDeviceHandle * h,
   /*in*/ DALBOOL bEnable);

DALResult
TMUnregisterClient(DalDeviceHandle * _h,
   /*in*/ uint32 uClientId);

typedef struct DalAdcDevice DalAdcDevice;
struct DalAdcDevice
{
   DalDevice DalDevice;
   DALResult (*GetInputProperties)(DalDeviceHandle * _h, const char * pChannelName, uint32 *);
   DALResult (*GetDeviceProperties)(DalDeviceHandle * _h,  AdcDeviceDevicePropertiesType *pAdcDeviceProp);
   DALResult (*RecalibrateChannel)(DalDeviceHandle * _h,  uint32  uChannelIdx, AdcDeviceRecalibrateResultType *);
   DALResult (*ReadChannel)(DalDeviceHandle * _h, uint32  uChannelIdx, AdcDeviceResultType *);
   DALResult (*TMGetInputProperties)(DalDeviceHandle * _h, const char * pInputName, uint32 *puChannelIdx);
   DALResult (*TMGetRange)(DalDeviceHandle *h, uint32 uMeasIdx, AdcDeviceTMRangeType *pAdcTMDeviceRange);
   DALResult (*TMSetThreshold)(DalDeviceHandle *_h, uint32 uClientIdx, uint32 uChannelId, const DALSYSEventHandle hEvent, AdcDeviceTMThresholdType eThreshold, const int32 *pnThresholdDesired, int32 *pnThresholdSet);
   DALResult (*TMSetEnableThresholds)(DalDeviceHandle * _h, uint32 uClientId, DALBOOL bEnable);
   DALResult (*TMUnregisterClient)(DalDeviceHandle * _h, uint32 uClientId);
   DALResult (*TMSetTolerance)(DalDeviceHandle *_h, uint32 uClientId, uint32 uMeasIdx, const DALSYSEventHandle hEvent, const int32 *pnLowerTolerance, const int32 *pnHigherTolerance);
};

typedef struct DalAdcDeviceHandle DalAdcDeviceHandle;
struct DalAdcDeviceHandle
{
   uint32 dwDalHandleId;
   const DalAdcDevice *pVtbl;
   void *pClientCtxt;
};

/**
   @brief Attaches to the ADC physical device driver

   This function attaches the client to the physical ADC device driver.
   This function is used to obtain the device handle which is required to
   use the driver APIs.

   @param  pszDevName [in] The device ID string to attach to
   @param  hDalDevice [out] Pointer to DAL device handle pointer which will receive a pointer to the ADC device handle

   @return DAL_SUCCESS if the attach was successful. Other return values
           indicate that an error occurred.

*/
#define DAL_AdcDeviceDeviceAttach(pszDevName,hDalDevice)\
        DAL_StringDeviceAttachEx(NULL, pszDevName, DALADCDEVICE_INTERFACE_VERSION, hDalDevice)

/**
   @brief Gets the number of channels defined for this device

   @param  _h [in] Device handle obtained from DAL_AdcDeviceDeviceAttach
   @param  pAdcDeviceProp [out] Pointer to result data

   @see    DAL_AdcDeviceDeviceAttach

   @return DAL_SUCCESS if successful.

*/
static __inline DALResult
DalAdcDevice_GetDeviceProperties(DalDeviceHandle * _h,  AdcDeviceDevicePropertiesType *pAdcDeviceProp)
{
   return ((DalAdcDeviceHandle *)_h)->pVtbl->GetDeviceProperties( _h, pAdcDeviceProp);
}

/**
   Determine whether the channel is supported by the device.

   @param  _h [in] Device handle obtained from DAL_AdcDeviceDeviceAttach
   @param  pChannelName [in] NULL-terminated channel name
   @param  puChannelIdx [out] Channel index

   @see    DAL_AdcDeviceDeviceAttach

   @return DAL_SUCCESS if successful.

*/
static __inline DALResult
DalAdcDevice_GetInputProperties(DalDeviceHandle * _h, const char *pChannelName, uint32 *puChannelIdx)
{
   return ((DalAdcDeviceHandle *)_h)->pVtbl->GetInputProperties( _h, pChannelName, puChannelIdx);
}

/**
   @brief Recalibrates a specific channel

   This function is used to calibrate a specific channel. If multiple channels
   use the same configuration, then those channels will also be recalibrated.

   @param  _h [in] Device handle obtained from DAL_AdcDeviceDeviceAttach
   @param  uChannelIdx [in] The index of the channel to recalibrate
   @param  pAdcDeviceRecalibrateResult [out] Recalibration result

   @see    DAL_AdcDeviceDeviceAttach

   @return DAL_SUCCESS if successful.

*/
static __inline DALResult
DalAdcDevice_RecalibrateChannel(DalDeviceHandle *_h, uint32 uChannelIdx, AdcDeviceRecalibrateResultType *pAdcDeviceRecalibrateResult)
{
   return ((DalAdcDeviceHandle *)_h)->pVtbl->RecalibrateChannel( _h, uChannelIdx, pAdcDeviceRecalibrateResult);
}

/**
   @brief Reads an ADC channel

   This function is used to read an ADC channel.

   @param  _h [in] Device handle obtained from DAL_AdcDeviceDeviceAttach
   @param  uChannelIdx [in] The index of the channel to recalibrate
   @param  pAdcDeviceReadResult [out] Read result

   @see    DAL_AdcDeviceDeviceAttach

   @return DAL_SUCCESS if successful.

*/
static __inline DALResult
DalAdcDevice_ReadChannel(DalDeviceHandle *_h, uint32 uChannelIdx, AdcDeviceResultType *pAdcDeviceReadResult)
{
   return ((DalAdcDeviceHandle *)_h)->pVtbl->ReadChannel( _h, uChannelIdx, pAdcDeviceReadResult);
}

/**
   @brief Gets the properties for the TM ADC

   This function looks up the properties for an analog input.

   @param  _h [in] Handle to the ADC driver instance instantiated by DAL_AdcDeviceDeviceAttach
   @param  pInputName [in] The name of the analog input to look up
   @param  puMeasIdx [out] The measurement index

   @return DAL_SUCCESS if successful.

*/
static __inline DALResult
DalAdcDevice_TMGetInputProperties(DalDeviceHandle * _h, const char *pInputName, uint32 *puMeasIdx)
{
   return ((DalAdcDeviceHandle *)_h)->pVtbl->TMGetInputProperties( _h, pInputName, puMeasIdx);
}

/**
   @brief Gets the range of physical values that can be set for a given TM channel.

   This function gets the minimum and maximum physical value that can be set as a
   threshold for a given VADC TM channel.

   @param  _h [in] Handle to the ADC driver instance instantiated by DAL_AdcDeviceDeviceAttach
   @param  uMeasIdx [in] The VADC TM channel obtained by calling DalAdcDevice_TMGetInputProperties
   @param  pAdcTMDeviceRange [out] Structure with minimum and maximum physical values

   @return DAL_SUCCESS if successful.

*/
static __inline DALResult
DalAdcDevice_TMGetRange(DalDeviceHandle * _h, uint32 uMeasIdx, AdcDeviceTMRangeType *pAdcTMDeviceRange)
{
   return ((DalAdcDeviceHandle *)_h)->pVtbl->TMGetRange( _h, uMeasIdx, pAdcTMDeviceRange);
}

/**
   @brief Sets a threshold with the ADC TM

   The threshold event will be triggered once when the threshold is crossed.
   After the event is triggered, the threshold will not trigger the event again
   and will be in a triggered state until the client calls DalAdcDevice_TMSetThreshold
   to set a new threshold.

   Note that thresholds can be disabled/re-enabled on a per client basis by calling
   DalAdcDevice_TMSetEnableThresholds. Thresholds are enabled by default, but calling
   DalAdcDevice_TMSetThreshold does not automatically re-enable them if they were previously
   disabled by a call to DalAdcDevice_TMSetEnableThresholds.

   @param  _h [in] Handle to the ADC driver instance instantiated by DAL_AdcDeviceDeviceAttach
   @param  uClientId [in] Client ID
   @param  uMeasIdx [in] measurement index from DalAdc_TMGetInputProperties
   @param  hEvent [in] Threshold event that gets set
   @param  eThreshold [in] Type of threshold to set
   @param  pnThresholdDesired [in] Threshold value to set in physical units; If NULL the
                                current threshold is cleared.
   @param  pnThresholdSet [out] The actual threshold value that was set in physical units.


   @see    DAL_AdcDeviceDeviceAttach

   @return DAL_SUCCESS if successful.

*/
static __inline DALResult
DalAdcDevice_TMSetThreshold(DalDeviceHandle *_h, uint32 uClientId, uint32 uMeasIdx, const DALSYSEventHandle hEvent, AdcDeviceTMThresholdType eThreshold, const int32 *pnThresholdDesired, int32 *pnThresholdSet)
{
   return ((DalAdcDeviceHandle *)_h)->pVtbl->TMSetThreshold( _h, uClientId, uMeasIdx, hEvent, eThreshold, pnThresholdDesired, pnThresholdSet);
}

/**
   @brief Enables / disables thresholds for the ADC TM

   @param  _h [in] Handle to the ADC driver instance instantiated by DAL_AdcDeviceDeviceAttach
   @param  uClientId [in] Client ID
   @param  bEnable [in] TRUE: enable, FALSE: disable

   @see    DAL_AdcDeviceDeviceAttach

   @return DAL_SUCCESS if successful.

*/
static __inline DALResult
DalAdcDevice_TMSetEnableThresholds(DalDeviceHandle * _h, uint32 uClientId, DALBOOL bEnable)
{
   return ((DalAdcDeviceHandle *)_h)->pVtbl->TMSetEnableThresholds( _h, uClientId, bEnable);
}

/**
   @brief Unregisters the client from TM

   @param  _h [in] Handle to the ADC driver instance instantiated by DAL_AdcDeviceDeviceAttach
   @param  uClientId [in] Client ID

   @see    DAL_AdcDeviceDeviceAttach

   @return DAL_SUCCESS if successful.

*/
static __inline DALResult
DalAdcDevice_TMUnregisterClient(DalDeviceHandle * _h, uint32 uClientId)
{
   return ((DalAdcDeviceHandle *)_h)->pVtbl->TMUnregisterClient( _h, uClientId);
}

/**
   @brief Sets a tolerance with the ADC TM

   This API allows clients to specify a tolerance for how much the measurement can
   change by before being notified, e.g. tell me when XO_THERM changes by 0.02
   degrees C. Thresholds will be set based on the current measurement value +/- the
   allowable delta.

   Once the tolerance has been reached or exceeded, ADC will notify the client and
   automatically set new thresholds for the tolerance. Clients must clear the
   tolerances for ADC to stop monitoring. Tolerances can be cleared by setting
   a NULL value.

   Clients can set or clear either a low tolerance, high tolerance or both during
   the same function call. If the client is already monitoring a tolerance then
   setting a new tolerance will result in an update to the previously set
   tolerance, i.e. the new tolerance will replace the old tolerance.

   A client can set either a threshold or a tolerance on any one measurement but not
   both at the same time. To allow a threshold to be set after registering a
   tolerance, the tolerance should be cleared by passing in NULL parameters for the
   tolerances.

   The client event will be triggered when the tolerance is met or exceeded:
    - Lower: event will trigger when current_value <= original_value - tolerance
    - Upper: event will trigger when current_value >= original_value + tolerance

   @param[in]  _h handle to the ADC driver instance instantiated by DAL_AdcDeviceDeviceAttach
   @param[in]  uClientId client ID
   @param[in]  uMeasIdx measurement index from DalAdc_TMGetInputProperties
   @param[in]  hEvent threshold event that gets set
   @param[in]  pnLowerTolerance lower tolerance value; NULL to clear / not set
   @param[in]  pnHigherTolerance higher tolerance value; NULL to clear / not set

   @see    DAL_AdcDeviceDeviceAttach

   @return DAL_SUCCESS if successful.

*/
static __inline DALResult
DalAdcDevice_TMSetTolerance(DalDeviceHandle *_h, uint32 uClientId, uint32 uMeasIdx, const DALSYSEventHandle hEvent, const int32 *pnLowerTolerance, const int32 *pnHigherTolerance)
{
   return ((DalAdcDeviceHandle *)_h)->pVtbl->TMSetTolerance( _h, uClientId, uMeasIdx, hEvent, pnLowerTolerance, pnHigherTolerance);
}

#endif

