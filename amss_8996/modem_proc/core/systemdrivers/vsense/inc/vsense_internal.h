#ifndef VSENSE_INTERNAL_H
#define VSENSE_INTERNAL_H
/*
===========================================================================


  @file vsense_internal.h

=======================================================================
 *! \file vsense.h
 *  \n
 *  \brief This file contains vsense related function prototypes,
 *         enums and driver data structure type.   
 *  \n  
 *  \n &copy; Copyright 2014 QUALCOMM Technologies Incorporated, All Rights Reserved
 *
* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/25/15   aks     created

========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pmapp_npa.h"
#include "com_dtypes.h"
#include "ClockDefs.h"
#include "VCSDefs.h" 
#include "ULogFront.h"

 /*===========================================================================
 
                      STRUCTURE TYPE AND ENUM
 
 ===========================================================================*/
#define VSENSE_FIFO_ARRAY_SIZE  64
#define VSENSE_MAX_INSTANCES_PER_RAIL 5  //num of sensors per rail
#define VSENSE_MAX_SENSORS  12   //total num of voltage sensors on the chip

typedef enum
{
  VSENSE_TYPE_RAIL_MX      ,
  VSENSE_TYPE_RAIL_CX      ,
  VSENSE_TYPE_RAIL_EBI     , 
  VSENSE_TYPE_RAIL_MSS    , 
  VSENSE_TYPE_RAIL_GFX     ,
  VSENSE_TYPE_RAIL_MAX     ,
  VSENSE_TYPE_RAIL_INVALID = 0x7FFFFFFF,
}vsense_type_rail;


typedef enum
{
  VSENSE_TYPE_GCC_VDDCX_VS_CLK,
  VSENSE_TYPE_GCC_VDDMX_VS_CLK,
  VSENSE_TYPE_GCC_VDDA_VS_CLK,
  VSENSE_TYPE_GCC_VS_CTL_CLK,
  VSENSE_TYPE_CLK_MAX,
}vsense_type_clk;

typedef enum
{
  VSENSE_SUCCESS,
  VSENSE_ERROR_INIT_FAILURE,

}vsense_error_type;

/*typedef struct
{
  vsense_type_rail     rail;
  ClockIdType          clk_id; 
  ClockCallbackHandle  clk_handle;
  vsense_type_clk      vsense_clk_id; 
}vsense_type_clk_data; */

typedef struct
{
  vsense_type_rail rail;
  VCSCornerType    previous_corner;
  uint32           previous_uv;
  int32            railway_id;
  boolean          is_supported;
  boolean          is_vsense_pwr_en;
  boolean          is_vsense_en;
  uint32           config_0_address;
  uint32           config_1_address;
  uint32           status_address;
  uint32           status;
  uint8            num_sensor_instances;
  uint8            sensor_id_arr[VSENSE_MAX_INSTANCES_PER_RAIL];
  uint8            fifo_data[VSENSE_FIFO_ARRAY_SIZE];
}vsense_type_rail_info;
 
 typedef struct
 {
  uint32                    fuse1_voltage_code;
  uint32                    fuse2_voltage_code;
}vsense_type_voltage_code;

typedef struct
{
   vsense_type_rail          rail;
   uint8                     vsense_instances_on_rail;
   uint32                    not_calibrated; 
   uint32                    fuse1_voltage_uv;
   uint32                    fuse2_voltage_uv;
   vsense_type_voltage_code  voltage_code_arr[VSENSE_MAX_INSTANCES_PER_RAIL];   
 }vsense_type_fuse_info;
 
 typedef struct
 {
   uint32                    version;
   uint32                    num_of_vsense_rails; //no. of rails vsense is monitoring
   vsense_type_fuse_info     rails_fuse_info[VSENSE_MAX_SENSORS]; //array size is num_of_rails_supported value 
 }vsense_type_smem_info;
 
typedef enum
{
  VSENSE_TYPE_INTERRUPT_MODE,
  VSENSE_TYPE_WATERMARK_MODE,
}vsense_type_mode;

typedef struct 
{ 
  vsense_type_smem_info      smem_info;
  vsense_type_rail_info      rail_info[VSENSE_TYPE_RAIL_MAX];
}vsense_type_rail_and_fuse_info;

typedef struct 
{
  uint8 min_threshold_offset; //offset in vsense code - 1 code =>5mv roughly
  uint8 max_threshold_offset; //offset in vsense code 
  uint8 mode ; //watermark or interrupt
  uint32 duration_us ; //duration in us for min max capture 
}vsense_type_dal_cfg;

typedef enum
{
  VSENSE_TRIG_PRE,
  VSENSE_TRIG_MID,
  VSENSE_TRIG_POST,  
}vsense_type_trig_pos;

typedef enum
{
  VSENSE_TRIG_SEL_0,
  VSENSE_TRIG_SEL_1,
  VSENSE_TRIG_SEL_2,
  VSENSE_TRIG_SEL_3,
}vsense_type_trig_sel;

struct vsense_ulog_vars
{
	ULogHandle vsense_log;  
	uint32 log_size;
}vsense_ulog_vars;

typedef struct 
{
   DalDeviceHandle *pHandle; 
   ClockIdType  mss_cfg_clk_id;
}vsense_mss_clk_type;

void vsense_ulog_init(void);


/*----------------------------------------------------------------------------
 * Function : vsense_set_mode_alarm
 * -------------------------------------------------------------------------*/
/*!
    Description: set the vsense into alaram mode for the requested rail
    @param
      rail - MX, CX, GFX, EBI 
      voltage_uv - voltage of the rail to determine thresholds
    @return
    void
    
-------------------------------------------------------------------------*/

void vsense_set_mode_alarm(vsense_type_rail rail, uint32 voltage_uv );

/*----------------------------------------------------------------------------
 * Function : vsense_set_mode_slope_detection
 * -------------------------------------------------------------------------*/
/*!
    Description: set the vsense into slope detection mode for the requested rail
    @param
      rail - MX, CX, GFX, EBI 
      
    @return
    void
    
-------------------------------------------------------------------------*/

void vsense_set_mode_slope_detection(vsense_type_rail rail);

/*----------------------------------------------------------------------------
 * Function : vsense_set_mode_min_max
 * -------------------------------------------------------------------------*/
/*!
    Description: set the vsense into min max mode for the requested rail
    @param
      rail - MX, CX, GFX, EBI 
      
    @return
    void
    
-------------------------------------------------------------------------*/

void vsense_set_mode_min_max(vsense_type_rail rail);

/*----------------------------------------------------------------------------
 * Function : vsense_get_config_info
 * -------------------------------------------------------------------------*/
/*!
    Description: Get a reference to the config info for the vsense driver 
    @param
      void
    @return
    void*
    
-------------------------------------------------------------------------*/

void* vsense_get_config_info(uint32 prop_id);


#endif 
