#ifndef __HWIO_QDSS_VSENSE_CONTROLLER_H__
#define __HWIO_QDSS_VSENSE_CONTROLLER_H__
/*
===========================================================================
*/
/**
  @file hwio_qdss_vsense_controller.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8996 (Istari) v2 [istari_v2.1_p3q2r16.7]
 
  This file contains HWIO register definitions for the following modules:
    QDSS_VSENSE_CONTROLLER

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================


  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: QDSS_VSENSE_CONTROLLER
 *--------------------------------------------------------------------------*/

#define QDSS_VSENSE_CONTROLLER_REG_BASE                                                           (QDSS_QDSS_ISTARI_BASE      + 0x00038000)

#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_ADDR(n)                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000000 + 0x4 * (n))
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_RMSK                                                    0xffffffff
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_MAXn                                                             9
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_INI(n)        \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_ADDR(n), HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_RMSK)
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_INMI(n,mask)    \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_ADDR(n), mask)
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_OUTI(n,val)    \
        out_dword(HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_ADDR(n),val)
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_ADDR(n),mask,val,HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_INI(n))
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_THRESHOLD_SLOPE_BMSK                                    0xff000000
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_THRESHOLD_SLOPE_SHFT                                          0x18
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_THRESHOLD_MAX_BMSK                                        0xff0000
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_THRESHOLD_MAX_SHFT                                            0x10
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_THRESHOLD_MIN_BMSK                                          0xff00
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_THRESHOLD_MIN_SHFT                                             0x8
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_CAPTURE_DELAY_BMSK                                            0xf0
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_CAPTURE_DELAY_SHFT                                             0x4
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_TRIG_POS_BMSK                                                  0xc
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_TRIG_POS_SHFT                                                  0x2
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_RSVD_BITS1_0_BMSK                                              0x3
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_0_RSVD_BITS1_0_SHFT                                              0x0

#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ADDR(n)                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000028 + 0x4 * (n))
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_RMSK                                                    0xffffffff
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_MAXn                                                             9
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_INI(n)        \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ADDR(n), HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_RMSK)
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_INMI(n,mask)    \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ADDR(n), mask)
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_OUTI(n,val)    \
        out_dword(HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ADDR(n),val)
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ADDR(n),mask,val,HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_INI(n))
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_RSVD_BITS31_18_BMSK                                     0xfffc0000
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_RSVD_BITS31_18_SHFT                                           0x12
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ALARM_SLOPE_NEG_EN_BMSK                                    0x20000
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ALARM_SLOPE_NEG_EN_SHFT                                       0x11
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ALARM_SLOPE_POS_EN_BMSK                                    0x10000
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ALARM_SLOPE_POS_EN_SHFT                                       0x10
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ALARM_MIN_EN_BMSK                                           0x8000
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ALARM_MIN_EN_SHFT                                              0xf
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ALARM_MAX_EN_BMSK                                           0x4000
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_ALARM_MAX_EN_SHFT                                              0xe
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_POWER_EN_BMSK                                               0x2000
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_POWER_EN_SHFT                                                  0xd
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_FUNC_EN_BMSK                                                0x1000
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_FUNC_EN_SHFT                                                   0xc
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_CLEAR_BMSK                                                   0x800
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_CLEAR_SHFT                                                     0xb
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_RSVD_BIT10_BMSK                                              0x400
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_RSVD_BIT10_SHFT                                                0xa
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_HW_CAPTURE_EN_BMSK                                           0x200
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_HW_CAPTURE_EN_SHFT                                             0x9
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_SW_CAPTURE_BMSK                                              0x100
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_SW_CAPTURE_SHFT                                                0x8
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_TRIG_SEL_BMSK                                                 0xe0
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_TRIG_SEL_SHFT                                                  0x5
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_MODE_SEL_BMSK                                                 0x10
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_MODE_SEL_SHFT                                                  0x4
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_RSVD_BIT3_BMSK                                                 0x8
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_RSVD_BIT3_SHFT                                                 0x3
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_SLOPE_DELTA_CYC_BMSK                                           0x7
#define HWIO_QDSS_VSENSE_SENSORn_CONFIG_1_SLOPE_DELTA_CYC_SHFT                                           0x0

#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_ADDR                                               (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000050)
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_RMSK                                               0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_ADDR, HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_ADDR,v)
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_ADDR,m,v,HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_IN)
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_RSVD_BITS31_10_BMSK                                0xfffffc00
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_RSVD_BITS31_10_SHFT                                       0xa
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR9_BYPASS_EN_BMSK                           0x200
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR9_BYPASS_EN_SHFT                             0x9
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR8_BYPASS_EN_BMSK                           0x100
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR8_BYPASS_EN_SHFT                             0x8
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR7_BYPASS_EN_BMSK                            0x80
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR7_BYPASS_EN_SHFT                             0x7
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR6_BYPASS_EN_BMSK                            0x40
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR6_BYPASS_EN_SHFT                             0x6
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR5_BYPASS_EN_BMSK                            0x20
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR5_BYPASS_EN_SHFT                             0x5
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR4_BYPASS_EN_BMSK                            0x10
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR4_BYPASS_EN_SHFT                             0x4
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR3_BYPASS_EN_BMSK                             0x8
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR3_BYPASS_EN_SHFT                             0x3
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR2_BYPASS_EN_BMSK                             0x4
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR2_BYPASS_EN_SHFT                             0x2
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR1_BYPASS_EN_BMSK                             0x2
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR1_BYPASS_EN_SHFT                             0x1
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR0_BYPASS_EN_BMSK                             0x1
#define HWIO_QDSS_VSENSE_SENSOR_APPS_BYPASS_EN_VSENSE_SENSOR0_BYPASS_EN_SHFT                             0x0

#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_ADDR                                                (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000054)
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_RMSK                                                0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_ADDR, HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_ADDR,v)
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_ADDR,m,v,HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_IN)
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_RSVD_BITS31_10_BMSK                                 0xfffffc00
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_RSVD_BITS31_10_SHFT                                        0xa
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR9_BYPASS_EN_BMSK                            0x200
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR9_BYPASS_EN_SHFT                              0x9
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR8_BYPASS_EN_BMSK                            0x100
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR8_BYPASS_EN_SHFT                              0x8
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR7_BYPASS_EN_BMSK                             0x80
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR7_BYPASS_EN_SHFT                              0x7
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR6_BYPASS_EN_BMSK                             0x40
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR6_BYPASS_EN_SHFT                              0x6
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR5_BYPASS_EN_BMSK                             0x20
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR5_BYPASS_EN_SHFT                              0x5
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR4_BYPASS_EN_BMSK                             0x10
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR4_BYPASS_EN_SHFT                              0x4
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR3_BYPASS_EN_BMSK                              0x8
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR3_BYPASS_EN_SHFT                              0x3
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR2_BYPASS_EN_BMSK                              0x4
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR2_BYPASS_EN_SHFT                              0x2
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR1_BYPASS_EN_BMSK                              0x2
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR1_BYPASS_EN_SHFT                              0x1
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR0_BYPASS_EN_BMSK                              0x1
#define HWIO_QDSS_VSENSE_SENSOR_RPM_BYPASS_EN_VSENSE_SENSOR0_BYPASS_EN_SHFT                              0x0

#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_ADDR                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000058)
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_RMSK                                                 0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_ADDR, HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_ADDR,v)
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_ADDR,m,v,HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_IN)
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_RSVD_BITS31_10_BMSK                                  0xfffffc00
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_RSVD_BITS31_10_SHFT                                         0xa
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR9_BYPASS_EN_BMSK                             0x200
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR9_BYPASS_EN_SHFT                               0x9
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR8_BYPASS_EN_BMSK                             0x100
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR8_BYPASS_EN_SHFT                               0x8
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR7_BYPASS_EN_BMSK                              0x80
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR7_BYPASS_EN_SHFT                               0x7
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR6_BYPASS_EN_BMSK                              0x40
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR6_BYPASS_EN_SHFT                               0x6
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR5_BYPASS_EN_BMSK                              0x20
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR5_BYPASS_EN_SHFT                               0x5
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR4_BYPASS_EN_BMSK                              0x10
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR4_BYPASS_EN_SHFT                               0x4
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR3_BYPASS_EN_BMSK                               0x8
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR3_BYPASS_EN_SHFT                               0x3
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR2_BYPASS_EN_BMSK                               0x4
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR2_BYPASS_EN_SHFT                               0x2
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR1_BYPASS_EN_BMSK                               0x2
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR1_BYPASS_EN_SHFT                               0x1
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR0_BYPASS_EN_BMSK                               0x1
#define HWIO_QDSS_VSENSE_SENSOR_Q6_BYPASS_EN_VSENSE_SENSOR0_BYPASS_EN_SHFT                               0x0

#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_ADDR                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x0000005c)
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_RMSK                                                 0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_ADDR, HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_ADDR,v)
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_ADDR,m,v,HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_IN)
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_RSVD_BITS31_10_BMSK                                  0xfffffc00
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_RSVD_BITS31_10_SHFT                                         0xa
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR9_CONFIG_WR_EN_BMSK                          0x200
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR9_CONFIG_WR_EN_SHFT                            0x9
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR8_CONFIG_WR_EN_BMSK                          0x100
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR8_CONFIG_WR_EN_SHFT                            0x8
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR7_CONFIG_WR_EN_BMSK                           0x80
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR7_CONFIG_WR_EN_SHFT                            0x7
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR6_CONFIG_WR_EN_BMSK                           0x40
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR6_CONFIG_WR_EN_SHFT                            0x6
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR5_CONFIG_WR_EN_BMSK                           0x20
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR5_CONFIG_WR_EN_SHFT                            0x5
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR4_CONFIG_WR_EN_BMSK                           0x10
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR4_CONFIG_WR_EN_SHFT                            0x4
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR3_CONFIG_WR_EN_BMSK                            0x8
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR3_CONFIG_WR_EN_SHFT                            0x3
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR2_CONFIG_WR_EN_BMSK                            0x4
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR2_CONFIG_WR_EN_SHFT                            0x2
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR1_CONFIG_WR_EN_BMSK                            0x2
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR1_CONFIG_WR_EN_SHFT                            0x1
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR0_CONFIG_WR_EN_BMSK                            0x1
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_EN_VSENSE_SENSOR0_CONFIG_WR_EN_SHFT                            0x0

#define HWIO_QDSS_VSENSE_SENSOR_BROADCAST_ADDR                                                    (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000060)
#define HWIO_QDSS_VSENSE_SENSOR_BROADCAST_RMSK                                                    0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_BROADCAST_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_BROADCAST_ADDR, HWIO_QDSS_VSENSE_SENSOR_BROADCAST_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_BROADCAST_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_BROADCAST_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_BROADCAST_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_SENSOR_BROADCAST_ADDR,v)
#define HWIO_QDSS_VSENSE_SENSOR_BROADCAST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSOR_BROADCAST_ADDR,m,v,HWIO_QDSS_VSENSE_SENSOR_BROADCAST_IN)
#define HWIO_QDSS_VSENSE_SENSOR_BROADCAST_RSVD_BITS31_1_BMSK                                      0xfffffffe
#define HWIO_QDSS_VSENSE_SENSOR_BROADCAST_RSVD_BITS31_1_SHFT                                             0x1
#define HWIO_QDSS_VSENSE_SENSOR_BROADCAST_VSENSE_SENSOR_BROADCAST_EN_BMSK                                0x1
#define HWIO_QDSS_VSENSE_SENSOR_BROADCAST_VSENSE_SENSOR_BROADCAST_EN_SHFT                                0x0

#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_ADDR                                               (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000064)
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_RMSK                                               0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_ADDR, HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_ADDR,v)
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_ADDR,m,v,HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_IN)
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_RSVD_BITS31_8_BMSK                                 0xffffff00
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_RSVD_BITS31_8_SHFT                                        0x8
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DELAY_BMSK                       0xc0
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DELAY_SHFT                        0x6
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DRIVE_SDATA1_BMSK                0x20
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DRIVE_SDATA1_SHFT                 0x5
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DRIVE_SDATA0_BMSK                0x10
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DRIVE_SDATA0_SHFT                 0x4
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DRIVE_SENA_BMSK                   0x8
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DRIVE_SENA_SHFT                   0x3
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DRIVE_SMODE1_BMSK                 0x4
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DRIVE_SMODE1_SHFT                 0x2
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DRIVE_SMODE0_BMSK                 0x2
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_DRIVE_SMODE0_SHFT                 0x1
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_MODE_EN_BMSK                      0x1
#define HWIO_QDSS_VSENSE_SENSOR_SELF_TEST_MODE_VSENSE_SENSOR_SELF_TEST_MODE_EN_SHFT                      0x0

#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_ADDR                                                   (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000068)
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_RMSK                                                   0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_ADDR, HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_ADDR,v)
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_ADDR,m,v,HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_IN)
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_RSVD_BITS31_10_BMSK                                    0xfffffc00
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_RSVD_BITS31_10_SHFT                                           0xa
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR9_FC_MASK_EN_BMSK                              0x200
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR9_FC_MASK_EN_SHFT                                0x9
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR8_FC_MASK_EN_BMSK                              0x100
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR8_FC_MASK_EN_SHFT                                0x8
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR7_FC_MASK_EN_BMSK                               0x80
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR7_FC_MASK_EN_SHFT                                0x7
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR6_FC_MASK_EN_BMSK                               0x40
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR6_FC_MASK_EN_SHFT                                0x6
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR5_FC_MASK_EN_BMSK                               0x20
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR5_FC_MASK_EN_SHFT                                0x5
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR4_FC_MASK_EN_BMSK                               0x10
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR4_FC_MASK_EN_SHFT                                0x4
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR3_FC_MASK_EN_BMSK                                0x8
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR3_FC_MASK_EN_SHFT                                0x3
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR2_FC_MASK_EN_BMSK                                0x4
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR2_FC_MASK_EN_SHFT                                0x2
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR1_FC_MASK_EN_BMSK                                0x2
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR1_FC_MASK_EN_SHFT                                0x1
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR0_FC_MASK_EN_BMSK                                0x1
#define HWIO_QDSS_VSENSE_SENSOR_FC_MASK_EN_VSENSE_SENSOR0_FC_MASK_EN_SHFT                                0x0

#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_ADDR                                           (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x0000006c)
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_RMSK                                           0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_ADDR, HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_ADDR,v)
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_ADDR,m,v,HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_IN)
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_RSVD_BITS31_10_BMSK                            0xfffffc00
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_RSVD_BITS31_10_SHFT                                   0xa
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR9_ALARM_MASK_EN_BMSK                   0x200
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR9_ALARM_MASK_EN_SHFT                     0x9
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR8_ALARM_MASK_EN_BMSK                   0x100
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR8_ALARM_MASK_EN_SHFT                     0x8
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR7_ALARM_MASK_EN_BMSK                    0x80
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR7_ALARM_MASK_EN_SHFT                     0x7
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR6_ALARM_MASK_EN_BMSK                    0x40
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR6_ALARM_MASK_EN_SHFT                     0x6
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR5_ALARM_MASK_EN_BMSK                    0x20
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR5_ALARM_MASK_EN_SHFT                     0x5
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR4_ALARM_MASK_EN_BMSK                    0x10
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR4_ALARM_MASK_EN_SHFT                     0x4
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR3_ALARM_MASK_EN_BMSK                     0x8
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR3_ALARM_MASK_EN_SHFT                     0x3
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR2_ALARM_MASK_EN_BMSK                     0x4
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR2_ALARM_MASK_EN_SHFT                     0x2
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR1_ALARM_MASK_EN_BMSK                     0x2
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR1_ALARM_MASK_EN_SHFT                     0x1
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR0_ALARM_MASK_EN_BMSK                     0x1
#define HWIO_QDSS_VSENSE_SENSOR_APPS_ALARM_MASK_EN_VSENSE_SENSOR0_ALARM_MASK_EN_SHFT                     0x0

#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_ADDR                                            (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000070)
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_RMSK                                            0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_ADDR, HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_ADDR,v)
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_ADDR,m,v,HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_IN)
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_RSVD_BITS31_10_BMSK                             0xfffffc00
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_RSVD_BITS31_10_SHFT                                    0xa
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR9_ALARM_MASK_EN_BMSK                    0x200
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR9_ALARM_MASK_EN_SHFT                      0x9
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR8_ALARM_MASK_EN_BMSK                    0x100
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR8_ALARM_MASK_EN_SHFT                      0x8
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR7_ALARM_MASK_EN_BMSK                     0x80
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR7_ALARM_MASK_EN_SHFT                      0x7
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR6_ALARM_MASK_EN_BMSK                     0x40
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR6_ALARM_MASK_EN_SHFT                      0x6
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR5_ALARM_MASK_EN_BMSK                     0x20
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR5_ALARM_MASK_EN_SHFT                      0x5
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR4_ALARM_MASK_EN_BMSK                     0x10
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR4_ALARM_MASK_EN_SHFT                      0x4
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR3_ALARM_MASK_EN_BMSK                      0x8
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR3_ALARM_MASK_EN_SHFT                      0x3
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR2_ALARM_MASK_EN_BMSK                      0x4
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR2_ALARM_MASK_EN_SHFT                      0x2
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR1_ALARM_MASK_EN_BMSK                      0x2
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR1_ALARM_MASK_EN_SHFT                      0x1
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR0_ALARM_MASK_EN_BMSK                      0x1
#define HWIO_QDSS_VSENSE_SENSOR_RPM_ALARM_MASK_EN_VSENSE_SENSOR0_ALARM_MASK_EN_SHFT                      0x0

#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_ADDR                                             (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000074)
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_RMSK                                             0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_ADDR, HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_ADDR,v)
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_ADDR,m,v,HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_IN)
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_RSVD_BITS31_10_BMSK                              0xfffffc00
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_RSVD_BITS31_10_SHFT                                     0xa
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR9_ALARM_MASK_EN_BMSK                     0x200
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR9_ALARM_MASK_EN_SHFT                       0x9
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR8_ALARM_MASK_EN_BMSK                     0x100
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR8_ALARM_MASK_EN_SHFT                       0x8
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR7_ALARM_MASK_EN_BMSK                      0x80
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR7_ALARM_MASK_EN_SHFT                       0x7
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR6_ALARM_MASK_EN_BMSK                      0x40
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR6_ALARM_MASK_EN_SHFT                       0x6
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR5_ALARM_MASK_EN_BMSK                      0x20
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR5_ALARM_MASK_EN_SHFT                       0x5
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR4_ALARM_MASK_EN_BMSK                      0x10
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR4_ALARM_MASK_EN_SHFT                       0x4
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR3_ALARM_MASK_EN_BMSK                       0x8
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR3_ALARM_MASK_EN_SHFT                       0x3
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR2_ALARM_MASK_EN_BMSK                       0x4
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR2_ALARM_MASK_EN_SHFT                       0x2
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR1_ALARM_MASK_EN_BMSK                       0x2
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR1_ALARM_MASK_EN_SHFT                       0x1
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR0_ALARM_MASK_EN_BMSK                       0x1
#define HWIO_QDSS_VSENSE_SENSOR_Q6_ALARM_MASK_EN_VSENSE_SENSOR0_ALARM_MASK_EN_SHFT                       0x0

#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_ADDR                                                      (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000078)
#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_RMSK                                                      0xffffffff
#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_ADDR, HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_RMSK)
#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_ADDR, m)
#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_ADDR,v)
#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_ADDR,m,v,HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_IN)
#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_DEBUG_BUS_EN_BMSK                                         0x80000000
#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_DEBUG_BUS_EN_SHFT                                               0x1f
#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_RSVD_BITS30_2_BMSK                                        0x7ffffffc
#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_RSVD_BITS30_2_SHFT                                               0x2
#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_DEBUG_BUS_SEL_BMSK                                               0x3
#define HWIO_QDSS_VSENSE_CNTRL_DEBUG_EN_DEBUG_BUS_SEL_SHFT                                               0x0

#define HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_ADDR                                             (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x0000007c)
#define HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_RMSK                                             0xffffffff
#define HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_ADDR, HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_RMSK)
#define HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_ADDR, m)
#define HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_OUT(v)      \
        out_dword(HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_ADDR,v)
#define HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_ADDR,m,v,HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_IN)
#define HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_RSVD_BITS31_3_BMSK                               0xfffffff8
#define HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_RSVD_BITS31_3_SHFT                                      0x3
#define HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_MUX_SEL_BMSK                                            0x7
#define HWIO_QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL_MUX_SEL_SHFT                                            0x0

#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_ADDR(n)                                                  (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000080 + 0x4 * (n))
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_RMSK                                                     0xffff03ff
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_MAXn                                                              7
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_INI(n)        \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSORn_SUMMARY_ADDR(n), HWIO_QDSS_VSENSE_SENSORn_SUMMARY_RMSK)
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_INMI(n,mask)    \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSORn_SUMMARY_ADDR(n), mask)
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_VDD_MAX_BMSK                                             0xff000000
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_VDD_MAX_SHFT                                                   0x18
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_VDD_MIN_BMSK                                               0xff0000
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_VDD_MIN_SHFT                                                   0x10
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_FSM_STATE_BMSK                                                0x3c0
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_FSM_STATE_SHFT                                                  0x6
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_FIFO_COMPLETE_BMSK                                             0x20
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_FIFO_COMPLETE_SHFT                                              0x5
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_FIFO_ACTIVE_BMSK                                               0x10
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_FIFO_ACTIVE_SHFT                                                0x4
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_ALARM_MAX_BMSK                                                  0x8
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_ALARM_MAX_SHFT                                                  0x3
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_ALARM_MIN_BMSK                                                  0x4
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_ALARM_MIN_SHFT                                                  0x2
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_ALARM_SLOPE_NEG_BMSK                                            0x2
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_ALARM_SLOPE_NEG_SHFT                                            0x1
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_ALARM_SLOPE_POS_BMSK                                            0x1
#define HWIO_QDSS_VSENSE_SENSORn_SUMMARY_ALARM_SLOPE_POS_SHFT                                            0x0

#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA0_ADDR                                                   (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x000000a0)
#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA0_RMSK                                                   0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA0_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA0_ADDR, HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA0_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA0_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA0_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA0_FIFO_DATA_BMSK                                         0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA0_FIFO_DATA_SHFT                                                0x0

#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA1_ADDR                                                   (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x000000a4)
#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA1_RMSK                                                   0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA1_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA1_ADDR, HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA1_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA1_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA1_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA1_FIFO_DATA_BMSK                                         0xffffffff
#define HWIO_QDSS_VSENSE_SENSOR_FIFO_DATA1_FIFO_DATA_SHFT                                                0x0

#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_ADDR                                                   (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x000000a8)
#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_RMSK                                                        0xfff
#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_ADDR, HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_RMSK)
#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_ADDR, m)
#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_CMB_DATA_SENT_BMSK                                          0x800
#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_CMB_DATA_SENT_SHFT                                            0xb
#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_RX_FSM_ERR_CNT_BMSK                                         0x700
#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_RX_FSM_ERR_CNT_SHFT                                           0x8
#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_RX_FSM_STATE_BMSK                                            0xe0
#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_RX_FSM_STATE_SHFT                                             0x5
#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_TX_FSM_STATE_BMSK                                            0x1f
#define HWIO_QDSS_VSENSE_CNTRL_MISC_STATUS_TX_FSM_STATE_SHFT                                             0x0

#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_ADDR                                              (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x000000ac)
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_RMSK                                                    0x1f
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_ADDR, HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_RMSK)
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_ADDR, m)
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_SELF_TEST_SDATA1_STATUS_BMSK                            0x10
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_SELF_TEST_SDATA1_STATUS_SHFT                             0x4
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_SELF_TEST_SDATA0_STATUS_BMSK                             0x8
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_SELF_TEST_SDATA0_STATUS_SHFT                             0x3
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_SELF_TEST_SENA_STATUS_BMSK                               0x4
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_SELF_TEST_SENA_STATUS_SHFT                               0x2
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_SELF_TEST_SMODE1_STATUS_BMSK                             0x2
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_SELF_TEST_SMODE1_STATUS_SHFT                             0x1
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_SELF_TEST_SMODE0_STATUS_BMSK                             0x1
#define HWIO_QDSS_VSENSE_CNTRL_SELF_TEST_STATUS_SELF_TEST_SMODE0_STATUS_SHFT                             0x0

#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_ADDR                                              (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x000000b0)
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_RMSK                                               0x3ff03ff
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_ADDR, HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR9_FC_STATUS_BMSK                      0x2000000
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR9_FC_STATUS_SHFT                           0x19
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR8_FC_STATUS_BMSK                      0x1000000
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR8_FC_STATUS_SHFT                           0x18
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR7_FC_STATUS_BMSK                       0x800000
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR7_FC_STATUS_SHFT                           0x17
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR6_FC_STATUS_BMSK                       0x400000
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR6_FC_STATUS_SHFT                           0x16
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR5_FC_STATUS_BMSK                       0x200000
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR5_FC_STATUS_SHFT                           0x15
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR4_FC_STATUS_BMSK                       0x100000
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR4_FC_STATUS_SHFT                           0x14
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR3_FC_STATUS_BMSK                        0x80000
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR3_FC_STATUS_SHFT                           0x13
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR2_FC_STATUS_BMSK                        0x40000
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR2_FC_STATUS_SHFT                           0x12
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR1_FC_STATUS_BMSK                        0x20000
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR1_FC_STATUS_SHFT                           0x11
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR0_FC_STATUS_BMSK                        0x10000
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR0_FC_STATUS_SHFT                           0x10
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR9_ALARM_STATUS_BMSK                       0x200
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR9_ALARM_STATUS_SHFT                         0x9
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR8_ALARM_STATUS_BMSK                       0x100
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR8_ALARM_STATUS_SHFT                         0x8
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR7_ALARM_STATUS_BMSK                        0x80
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR7_ALARM_STATUS_SHFT                         0x7
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR6_ALARM_STATUS_BMSK                        0x40
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR6_ALARM_STATUS_SHFT                         0x6
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR5_ALARM_STATUS_BMSK                        0x20
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR5_ALARM_STATUS_SHFT                         0x5
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR4_ALARM_STATUS_BMSK                        0x10
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR4_ALARM_STATUS_SHFT                         0x4
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR3_ALARM_STATUS_BMSK                         0x8
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR3_ALARM_STATUS_SHFT                         0x3
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR2_ALARM_STATUS_BMSK                         0x4
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR2_ALARM_STATUS_SHFT                         0x2
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR1_ALARM_STATUS_BMSK                         0x2
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR1_ALARM_STATUS_SHFT                         0x1
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR0_ALARM_STATUS_BMSK                         0x1
#define HWIO_QDSS_VSENSE_SENSOR_ALARM_FC_STATUS_VSENSE_SENSOR0_ALARM_STATUS_SHFT                         0x0

#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_ADDR                                             (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x000000b4)
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_RMSK                                                  0x3ff
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_ADDR, HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_RMSK)
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_ADDR, m)
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR9_CONFIG_WR_STATUS_BMSK                  0x200
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR9_CONFIG_WR_STATUS_SHFT                    0x9
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR8_CONFIG_WR_STATUS_BMSK                  0x100
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR8_CONFIG_WR_STATUS_SHFT                    0x8
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR7_CONFIG_WR_STATUS_BMSK                   0x80
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR7_CONFIG_WR_STATUS_SHFT                    0x7
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR6_CONFIG_WR_STATUS_BMSK                   0x40
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR6_CONFIG_WR_STATUS_SHFT                    0x6
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR5_CONFIG_WR_STATUS_BMSK                   0x20
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR5_CONFIG_WR_STATUS_SHFT                    0x5
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR4_CONFIG_WR_STATUS_BMSK                   0x10
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR4_CONFIG_WR_STATUS_SHFT                    0x4
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR3_CONFIG_WR_STATUS_BMSK                    0x8
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR3_CONFIG_WR_STATUS_SHFT                    0x3
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR2_CONFIG_WR_STATUS_BMSK                    0x4
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR2_CONFIG_WR_STATUS_SHFT                    0x2
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR1_CONFIG_WR_STATUS_BMSK                    0x2
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR1_CONFIG_WR_STATUS_SHFT                    0x1
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR0_CONFIG_WR_STATUS_BMSK                    0x1
#define HWIO_QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS_VSENSE_SENSOR0_CONFIG_WR_STATUS_SHFT                    0x0

#define HWIO_QDSS_VSENSE_CNTRL_HW_ID_ADDR                                                         (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x000000b8)
#define HWIO_QDSS_VSENSE_CNTRL_HW_ID_RMSK                                                         0xffffffff
#define HWIO_QDSS_VSENSE_CNTRL_HW_ID_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_HW_ID_ADDR, HWIO_QDSS_VSENSE_CNTRL_HW_ID_RMSK)
#define HWIO_QDSS_VSENSE_CNTRL_HW_ID_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_HW_ID_ADDR, m)
#define HWIO_QDSS_VSENSE_CNTRL_HW_ID_MAJOR_BMSK                                                   0xf0000000
#define HWIO_QDSS_VSENSE_CNTRL_HW_ID_MAJOR_SHFT                                                         0x1c
#define HWIO_QDSS_VSENSE_CNTRL_HW_ID_MINOR_BMSK                                                    0xfff0000
#define HWIO_QDSS_VSENSE_CNTRL_HW_ID_MINOR_SHFT                                                         0x10
#define HWIO_QDSS_VSENSE_CNTRL_HW_ID_STEP_BMSK                                                        0xffff
#define HWIO_QDSS_VSENSE_CNTRL_HW_ID_STEP_SHFT                                                           0x0

#define HWIO_QDSS_VSENSE_CNTRL_SPARE_STATUS_ADDR                                                  (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x000000bc)
#define HWIO_QDSS_VSENSE_CNTRL_SPARE_STATUS_RMSK                                                  0xffffffff
#define HWIO_QDSS_VSENSE_CNTRL_SPARE_STATUS_IN          \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_SPARE_STATUS_ADDR, HWIO_QDSS_VSENSE_CNTRL_SPARE_STATUS_RMSK)
#define HWIO_QDSS_VSENSE_CNTRL_SPARE_STATUS_INM(m)      \
        in_dword_masked(HWIO_QDSS_VSENSE_CNTRL_SPARE_STATUS_ADDR, m)
#define HWIO_QDSS_VSENSE_CNTRL_SPARE_STATUS_RSVD_BITS31_0_BMSK                                    0xffffffff
#define HWIO_QDSS_VSENSE_CNTRL_SPARE_STATUS_RSVD_BITS31_0_SHFT                                           0x0

#define HWIO_QDSS_TPDM_CMB_CR_ADDR                                                                (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000a00)
#define HWIO_QDSS_TPDM_CMB_CR_RMSK                                                                      0xc7
#define HWIO_QDSS_TPDM_CMB_CR_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_CR_ADDR, HWIO_QDSS_TPDM_CMB_CR_RMSK)
#define HWIO_QDSS_TPDM_CMB_CR_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_CR_ADDR, m)
#define HWIO_QDSS_TPDM_CMB_CR_OUT(v)      \
        out_dword(HWIO_QDSS_TPDM_CMB_CR_ADDR,v)
#define HWIO_QDSS_TPDM_CMB_CR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_TPDM_CMB_CR_ADDR,m,v,HWIO_QDSS_TPDM_CMB_CR_IN)
#define HWIO_QDSS_TPDM_CMB_CR_ATBFLOWERR_BMSK                                                           0x80
#define HWIO_QDSS_TPDM_CMB_CR_ATBFLOWERR_SHFT                                                            0x7
#define HWIO_QDSS_TPDM_CMB_CR_EBITSET_BMSK                                                              0x40
#define HWIO_QDSS_TPDM_CMB_CR_EBITSET_SHFT                                                               0x6
#define HWIO_QDSS_TPDM_CMB_CR_FLOWCTRL_BMSK                                                              0x4
#define HWIO_QDSS_TPDM_CMB_CR_FLOWCTRL_SHFT                                                              0x2
#define HWIO_QDSS_TPDM_CMB_CR_MODE_BMSK                                                                  0x2
#define HWIO_QDSS_TPDM_CMB_CR_MODE_SHFT                                                                  0x1
#define HWIO_QDSS_TPDM_CMB_CR_E_BMSK                                                                     0x1
#define HWIO_QDSS_TPDM_CMB_CR_E_SHFT                                                                     0x0

#define HWIO_QDSS_TPDM_CMB_TIER_ADDR                                                              (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000a04)
#define HWIO_QDSS_TPDM_CMB_TIER_RMSK                                                                     0x3
#define HWIO_QDSS_TPDM_CMB_TIER_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_TIER_ADDR, HWIO_QDSS_TPDM_CMB_TIER_RMSK)
#define HWIO_QDSS_TPDM_CMB_TIER_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_TIER_ADDR, m)
#define HWIO_QDSS_TPDM_CMB_TIER_OUT(v)      \
        out_dword(HWIO_QDSS_TPDM_CMB_TIER_ADDR,v)
#define HWIO_QDSS_TPDM_CMB_TIER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_TPDM_CMB_TIER_ADDR,m,v,HWIO_QDSS_TPDM_CMB_TIER_IN)
#define HWIO_QDSS_TPDM_CMB_TIER_XTRIG_TSENAB_BMSK                                                        0x2
#define HWIO_QDSS_TPDM_CMB_TIER_XTRIG_TSENAB_SHFT                                                        0x1
#define HWIO_QDSS_TPDM_CMB_TIER_PATT_TSENAB_BMSK                                                         0x1
#define HWIO_QDSS_TPDM_CMB_TIER_PATT_TSENAB_SHFT                                                         0x0

#define HWIO_QDSS_TPDM_CMB_TPRm_ADDR(m)                                                           (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000a08 + 0x4 * (m))
#define HWIO_QDSS_TPDM_CMB_TPRm_RMSK                                                              0xffffffff
#define HWIO_QDSS_TPDM_CMB_TPRm_MAXm                                                                       0
#define HWIO_QDSS_TPDM_CMB_TPRm_INI(m)        \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_TPRm_ADDR(m), HWIO_QDSS_TPDM_CMB_TPRm_RMSK)
#define HWIO_QDSS_TPDM_CMB_TPRm_INMI(m,mask)    \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_TPRm_ADDR(m), mask)
#define HWIO_QDSS_TPDM_CMB_TPRm_OUTI(m,val)    \
        out_dword(HWIO_QDSS_TPDM_CMB_TPRm_ADDR(m),val)
#define HWIO_QDSS_TPDM_CMB_TPRm_OUTMI(m,mask,val) \
        out_dword_masked_ns(HWIO_QDSS_TPDM_CMB_TPRm_ADDR(m),mask,val,HWIO_QDSS_TPDM_CMB_TPRm_INI(m))
#define HWIO_QDSS_TPDM_CMB_TPRm_VAL_BMSK                                                          0xffffffff
#define HWIO_QDSS_TPDM_CMB_TPRm_VAL_SHFT                                                                 0x0

#define HWIO_QDSS_TPDM_CMB_TPMRm_ADDR(m)                                                          (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000a10 + 0x4 * (m))
#define HWIO_QDSS_TPDM_CMB_TPMRm_RMSK                                                             0xffffffff
#define HWIO_QDSS_TPDM_CMB_TPMRm_MAXm                                                                      0
#define HWIO_QDSS_TPDM_CMB_TPMRm_INI(m)        \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_TPMRm_ADDR(m), HWIO_QDSS_TPDM_CMB_TPMRm_RMSK)
#define HWIO_QDSS_TPDM_CMB_TPMRm_INMI(m,mask)    \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_TPMRm_ADDR(m), mask)
#define HWIO_QDSS_TPDM_CMB_TPMRm_OUTI(m,val)    \
        out_dword(HWIO_QDSS_TPDM_CMB_TPMRm_ADDR(m),val)
#define HWIO_QDSS_TPDM_CMB_TPMRm_OUTMI(m,mask,val) \
        out_dword_masked_ns(HWIO_QDSS_TPDM_CMB_TPMRm_ADDR(m),mask,val,HWIO_QDSS_TPDM_CMB_TPMRm_INI(m))
#define HWIO_QDSS_TPDM_CMB_TPMRm_VAL_BMSK                                                         0xffffffff
#define HWIO_QDSS_TPDM_CMB_TPMRm_VAL_SHFT                                                                0x0

#define HWIO_QDSS_TPDM_CMB_XPRm_ADDR(m)                                                           (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000a18 + 0x4 * (m))
#define HWIO_QDSS_TPDM_CMB_XPRm_RMSK                                                              0xffffffff
#define HWIO_QDSS_TPDM_CMB_XPRm_MAXm                                                                       0
#define HWIO_QDSS_TPDM_CMB_XPRm_INI(m)        \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_XPRm_ADDR(m), HWIO_QDSS_TPDM_CMB_XPRm_RMSK)
#define HWIO_QDSS_TPDM_CMB_XPRm_INMI(m,mask)    \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_XPRm_ADDR(m), mask)
#define HWIO_QDSS_TPDM_CMB_XPRm_OUTI(m,val)    \
        out_dword(HWIO_QDSS_TPDM_CMB_XPRm_ADDR(m),val)
#define HWIO_QDSS_TPDM_CMB_XPRm_OUTMI(m,mask,val) \
        out_dword_masked_ns(HWIO_QDSS_TPDM_CMB_XPRm_ADDR(m),mask,val,HWIO_QDSS_TPDM_CMB_XPRm_INI(m))
#define HWIO_QDSS_TPDM_CMB_XPRm_VAL_BMSK                                                          0xffffffff
#define HWIO_QDSS_TPDM_CMB_XPRm_VAL_SHFT                                                                 0x0

#define HWIO_QDSS_TPDM_CMB_XPMRm_ADDR(m)                                                          (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000a20 + 0x4 * (m))
#define HWIO_QDSS_TPDM_CMB_XPMRm_RMSK                                                             0xffffffff
#define HWIO_QDSS_TPDM_CMB_XPMRm_MAXm                                                                      0
#define HWIO_QDSS_TPDM_CMB_XPMRm_INI(m)        \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_XPMRm_ADDR(m), HWIO_QDSS_TPDM_CMB_XPMRm_RMSK)
#define HWIO_QDSS_TPDM_CMB_XPMRm_INMI(m,mask)    \
        in_dword_masked(HWIO_QDSS_TPDM_CMB_XPMRm_ADDR(m), mask)
#define HWIO_QDSS_TPDM_CMB_XPMRm_OUTI(m,val)    \
        out_dword(HWIO_QDSS_TPDM_CMB_XPMRm_ADDR(m),val)
#define HWIO_QDSS_TPDM_CMB_XPMRm_OUTMI(m,mask,val) \
        out_dword_masked_ns(HWIO_QDSS_TPDM_CMB_XPMRm_ADDR(m),mask,val,HWIO_QDSS_TPDM_CMB_XPMRm_INI(m))
#define HWIO_QDSS_TPDM_CMB_XPMRm_VAL_BMSK                                                         0xffffffff
#define HWIO_QDSS_TPDM_CMB_XPMRm_VAL_SHFT                                                                0x0

#define HWIO_QDSS_TPDM_ITATBCNTRL_ADDR                                                            (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000ef0)
#define HWIO_QDSS_TPDM_ITATBCNTRL_RMSK                                                            0xc07fffff
#define HWIO_QDSS_TPDM_ITATBCNTRL_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_ITATBCNTRL_ADDR, HWIO_QDSS_TPDM_ITATBCNTRL_RMSK)
#define HWIO_QDSS_TPDM_ITATBCNTRL_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_ITATBCNTRL_ADDR, m)
#define HWIO_QDSS_TPDM_ITATBCNTRL_OUT(v)      \
        out_dword(HWIO_QDSS_TPDM_ITATBCNTRL_ADDR,v)
#define HWIO_QDSS_TPDM_ITATBCNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_TPDM_ITATBCNTRL_ADDR,m,v,HWIO_QDSS_TPDM_ITATBCNTRL_IN)
#define HWIO_QDSS_TPDM_ITATBCNTRL_TSREQ_BMSK                                                      0x80000000
#define HWIO_QDSS_TPDM_ITATBCNTRL_TSREQ_SHFT                                                            0x1f
#define HWIO_QDSS_TPDM_ITATBCNTRL_ATVALID_BMSK                                                    0x40000000
#define HWIO_QDSS_TPDM_ITATBCNTRL_ATVALID_SHFT                                                          0x1e
#define HWIO_QDSS_TPDM_ITATBCNTRL_ATDATAMODE_BMSK                                                   0x400000
#define HWIO_QDSS_TPDM_ITATBCNTRL_ATDATAMODE_SHFT                                                       0x16
#define HWIO_QDSS_TPDM_ITATBCNTRL_ATBYTES_BMSK                                                      0x3c0000
#define HWIO_QDSS_TPDM_ITATBCNTRL_ATBYTES_SHFT                                                          0x12
#define HWIO_QDSS_TPDM_ITATBCNTRL_ATDATA_BMSK                                                        0x3fc00
#define HWIO_QDSS_TPDM_ITATBCNTRL_ATDATA_SHFT                                                            0xa
#define HWIO_QDSS_TPDM_ITATBCNTRL_ATID_BMSK                                                            0x3f8
#define HWIO_QDSS_TPDM_ITATBCNTRL_ATID_SHFT                                                              0x3
#define HWIO_QDSS_TPDM_ITATBCNTRL_TSVAL_BMSK                                                             0x7
#define HWIO_QDSS_TPDM_ITATBCNTRL_TSVAL_SHFT                                                             0x0

#define HWIO_QDSS_TPDM_ITCNTRL_ADDR                                                               (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000f00)
#define HWIO_QDSS_TPDM_ITCNTRL_RMSK                                                                      0x1
#define HWIO_QDSS_TPDM_ITCNTRL_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_ITCNTRL_ADDR, HWIO_QDSS_TPDM_ITCNTRL_RMSK)
#define HWIO_QDSS_TPDM_ITCNTRL_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_ITCNTRL_ADDR, m)
#define HWIO_QDSS_TPDM_ITCNTRL_OUT(v)      \
        out_dword(HWIO_QDSS_TPDM_ITCNTRL_ADDR,v)
#define HWIO_QDSS_TPDM_ITCNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_TPDM_ITCNTRL_ADDR,m,v,HWIO_QDSS_TPDM_ITCNTRL_IN)
#define HWIO_QDSS_TPDM_ITCNTRL_IME_BMSK                                                                  0x1
#define HWIO_QDSS_TPDM_ITCNTRL_IME_SHFT                                                                  0x0

#define HWIO_QDSS_TPDM_CLAIMSET_ADDR                                                              (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fa0)
#define HWIO_QDSS_TPDM_CLAIMSET_RMSK                                                              0xffffffff
#define HWIO_QDSS_TPDM_CLAIMSET_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_CLAIMSET_ADDR, HWIO_QDSS_TPDM_CLAIMSET_RMSK)
#define HWIO_QDSS_TPDM_CLAIMSET_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_CLAIMSET_ADDR, m)
#define HWIO_QDSS_TPDM_CLAIMSET_VAL_SET_BMSK                                                      0xffffffff
#define HWIO_QDSS_TPDM_CLAIMSET_VAL_SET_SHFT                                                             0x0

#define HWIO_QDSS_TPDM_CLAIMCLR_ADDR                                                              (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fa4)
#define HWIO_QDSS_TPDM_CLAIMCLR_RMSK                                                              0xffffffff
#define HWIO_QDSS_TPDM_CLAIMCLR_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_CLAIMCLR_ADDR, HWIO_QDSS_TPDM_CLAIMCLR_RMSK)
#define HWIO_QDSS_TPDM_CLAIMCLR_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_CLAIMCLR_ADDR, m)
#define HWIO_QDSS_TPDM_CLAIMCLR_VAL_CLR_BMSK                                                      0xffffffff
#define HWIO_QDSS_TPDM_CLAIMCLR_VAL_CLR_SHFT                                                             0x0

#define HWIO_QDSS_TPDM_DEVAFF0_ADDR                                                               (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fa8)
#define HWIO_QDSS_TPDM_DEVAFF0_RMSK                                                               0xffffffff
#define HWIO_QDSS_TPDM_DEVAFF0_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_DEVAFF0_ADDR, HWIO_QDSS_TPDM_DEVAFF0_RMSK)
#define HWIO_QDSS_TPDM_DEVAFF0_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_DEVAFF0_ADDR, m)
#define HWIO_QDSS_TPDM_DEVAFF0_VAL_BMSK                                                           0xffffffff
#define HWIO_QDSS_TPDM_DEVAFF0_VAL_SHFT                                                                  0x0

#define HWIO_QDSS_TPDM_DEVAFF1_ADDR                                                               (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fac)
#define HWIO_QDSS_TPDM_DEVAFF1_RMSK                                                               0xffffffff
#define HWIO_QDSS_TPDM_DEVAFF1_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_DEVAFF1_ADDR, HWIO_QDSS_TPDM_DEVAFF1_RMSK)
#define HWIO_QDSS_TPDM_DEVAFF1_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_DEVAFF1_ADDR, m)
#define HWIO_QDSS_TPDM_DEVAFF1_VAL_BMSK                                                           0xffffffff
#define HWIO_QDSS_TPDM_DEVAFF1_VAL_SHFT                                                                  0x0

#define HWIO_QDSS_TPDM_LAR_ADDR                                                                   (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fb0)
#define HWIO_QDSS_TPDM_LAR_RMSK                                                                   0xffffffff
#define HWIO_QDSS_TPDM_LAR_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_LAR_ADDR, HWIO_QDSS_TPDM_LAR_RMSK)
#define HWIO_QDSS_TPDM_LAR_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_LAR_ADDR, m)
#define HWIO_QDSS_TPDM_LAR_OUT(v)      \
        out_dword(HWIO_QDSS_TPDM_LAR_ADDR,v)
#define HWIO_QDSS_TPDM_LAR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_TPDM_LAR_ADDR,m,v,HWIO_QDSS_TPDM_LAR_IN)
#define HWIO_QDSS_TPDM_LAR_KEY_BMSK                                                               0xffffffff
#define HWIO_QDSS_TPDM_LAR_KEY_SHFT                                                                      0x0

#define HWIO_QDSS_TPDM_LSR_ADDR                                                                   (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fb4)
#define HWIO_QDSS_TPDM_LSR_RMSK                                                                          0x7
#define HWIO_QDSS_TPDM_LSR_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_LSR_ADDR, HWIO_QDSS_TPDM_LSR_RMSK)
#define HWIO_QDSS_TPDM_LSR_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_LSR_ADDR, m)
#define HWIO_QDSS_TPDM_LSR_NTT_BMSK                                                                      0x4
#define HWIO_QDSS_TPDM_LSR_NTT_SHFT                                                                      0x2
#define HWIO_QDSS_TPDM_LSR_SLK_BMSK                                                                      0x2
#define HWIO_QDSS_TPDM_LSR_SLK_SHFT                                                                      0x1
#define HWIO_QDSS_TPDM_LSR_SLI_BMSK                                                                      0x1
#define HWIO_QDSS_TPDM_LSR_SLI_SHFT                                                                      0x0

#define HWIO_QDSS_TPDM_AUTHSTATUS_ADDR                                                            (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fb8)
#define HWIO_QDSS_TPDM_AUTHSTATUS_RMSK                                                                  0xff
#define HWIO_QDSS_TPDM_AUTHSTATUS_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_AUTHSTATUS_ADDR, HWIO_QDSS_TPDM_AUTHSTATUS_RMSK)
#define HWIO_QDSS_TPDM_AUTHSTATUS_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_AUTHSTATUS_ADDR, m)
#define HWIO_QDSS_TPDM_AUTHSTATUS_SNID_BMSK                                                             0xc0
#define HWIO_QDSS_TPDM_AUTHSTATUS_SNID_SHFT                                                              0x6
#define HWIO_QDSS_TPDM_AUTHSTATUS_SID_BMSK                                                              0x30
#define HWIO_QDSS_TPDM_AUTHSTATUS_SID_SHFT                                                               0x4
#define HWIO_QDSS_TPDM_AUTHSTATUS_NSNID_BMSK                                                             0xc
#define HWIO_QDSS_TPDM_AUTHSTATUS_NSNID_SHFT                                                             0x2
#define HWIO_QDSS_TPDM_AUTHSTATUS_NSID_BMSK                                                              0x3
#define HWIO_QDSS_TPDM_AUTHSTATUS_NSID_SHFT                                                              0x0

#define HWIO_QDSS_TPDM_DEVARCH_ADDR                                                               (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fbc)
#define HWIO_QDSS_TPDM_DEVARCH_RMSK                                                               0xffffffff
#define HWIO_QDSS_TPDM_DEVARCH_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_DEVARCH_ADDR, HWIO_QDSS_TPDM_DEVARCH_RMSK)
#define HWIO_QDSS_TPDM_DEVARCH_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_DEVARCH_ADDR, m)
#define HWIO_QDSS_TPDM_DEVARCH_ARCHITECT_BMSK                                                     0xffe00000
#define HWIO_QDSS_TPDM_DEVARCH_ARCHITECT_SHFT                                                           0x15
#define HWIO_QDSS_TPDM_DEVARCH_PRESENT_BMSK                                                         0x100000
#define HWIO_QDSS_TPDM_DEVARCH_PRESENT_SHFT                                                             0x14
#define HWIO_QDSS_TPDM_DEVARCH_REVISION_BMSK                                                         0xf0000
#define HWIO_QDSS_TPDM_DEVARCH_REVISION_SHFT                                                            0x10
#define HWIO_QDSS_TPDM_DEVARCH_ARCHID_BMSK                                                            0xffff
#define HWIO_QDSS_TPDM_DEVARCH_ARCHID_SHFT                                                               0x0

#define HWIO_QDSS_TPDM_DEVID_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fc8)
#define HWIO_QDSS_TPDM_DEVID_RMSK                                                                       0x30
#define HWIO_QDSS_TPDM_DEVID_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_DEVID_ADDR, HWIO_QDSS_TPDM_DEVID_RMSK)
#define HWIO_QDSS_TPDM_DEVID_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_DEVID_ADDR, m)
#define HWIO_QDSS_TPDM_DEVID_CMB_IN_BMSK                                                                0x30
#define HWIO_QDSS_TPDM_DEVID_CMB_IN_SHFT                                                                 0x4

#define HWIO_QDSS_TPDM_PIDR4_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fd0)
#define HWIO_QDSS_TPDM_PIDR4_RMSK                                                                       0xff
#define HWIO_QDSS_TPDM_PIDR4_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR4_ADDR, HWIO_QDSS_TPDM_PIDR4_RMSK)
#define HWIO_QDSS_TPDM_PIDR4_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR4_ADDR, m)
#define HWIO_QDSS_TPDM_PIDR4_SIZE_BMSK                                                                  0xf0
#define HWIO_QDSS_TPDM_PIDR4_SIZE_SHFT                                                                   0x4
#define HWIO_QDSS_TPDM_PIDR4_DES_2_BMSK                                                                  0xf
#define HWIO_QDSS_TPDM_PIDR4_DES_2_SHFT                                                                  0x0

#define HWIO_QDSS_TPDM_PIDR5_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fd4)
#define HWIO_QDSS_TPDM_PIDR5_RMSK                                                                 0xffffffff
#define HWIO_QDSS_TPDM_PIDR5_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR5_ADDR, HWIO_QDSS_TPDM_PIDR5_RMSK)
#define HWIO_QDSS_TPDM_PIDR5_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR5_ADDR, m)
#define HWIO_QDSS_TPDM_PIDR5_EMPTY_BMSK                                                           0xffffffff
#define HWIO_QDSS_TPDM_PIDR5_EMPTY_SHFT                                                                  0x0

#define HWIO_QDSS_TPDM_PIDR6_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fd8)
#define HWIO_QDSS_TPDM_PIDR6_RMSK                                                                 0xffffffff
#define HWIO_QDSS_TPDM_PIDR6_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR6_ADDR, HWIO_QDSS_TPDM_PIDR6_RMSK)
#define HWIO_QDSS_TPDM_PIDR6_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR6_ADDR, m)
#define HWIO_QDSS_TPDM_PIDR6_EMPTY_BMSK                                                           0xffffffff
#define HWIO_QDSS_TPDM_PIDR6_EMPTY_SHFT                                                                  0x0

#define HWIO_QDSS_TPDM_PIDR7_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fdc)
#define HWIO_QDSS_TPDM_PIDR7_RMSK                                                                 0xffffffff
#define HWIO_QDSS_TPDM_PIDR7_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR7_ADDR, HWIO_QDSS_TPDM_PIDR7_RMSK)
#define HWIO_QDSS_TPDM_PIDR7_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR7_ADDR, m)
#define HWIO_QDSS_TPDM_PIDR7_EMPTY_BMSK                                                           0xffffffff
#define HWIO_QDSS_TPDM_PIDR7_EMPTY_SHFT                                                                  0x0

#define HWIO_QDSS_TPDM_PIDR0_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fe0)
#define HWIO_QDSS_TPDM_PIDR0_RMSK                                                                       0xff
#define HWIO_QDSS_TPDM_PIDR0_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR0_ADDR, HWIO_QDSS_TPDM_PIDR0_RMSK)
#define HWIO_QDSS_TPDM_PIDR0_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR0_ADDR, m)
#define HWIO_QDSS_TPDM_PIDR0_PART_7_6_BMSK                                                              0xc0
#define HWIO_QDSS_TPDM_PIDR0_PART_7_6_SHFT                                                               0x6
#define HWIO_QDSS_TPDM_PIDR0_PART_5_BMSK                                                                0x20
#define HWIO_QDSS_TPDM_PIDR0_PART_5_SHFT                                                                 0x5
#define HWIO_QDSS_TPDM_PIDR0_PART_4_BMSK                                                                0x10
#define HWIO_QDSS_TPDM_PIDR0_PART_4_SHFT                                                                 0x4
#define HWIO_QDSS_TPDM_PIDR0_PART_3_BMSK                                                                 0x8
#define HWIO_QDSS_TPDM_PIDR0_PART_3_SHFT                                                                 0x3
#define HWIO_QDSS_TPDM_PIDR0_PART_2_BMSK                                                                 0x4
#define HWIO_QDSS_TPDM_PIDR0_PART_2_SHFT                                                                 0x2
#define HWIO_QDSS_TPDM_PIDR0_PART_1_BMSK                                                                 0x2
#define HWIO_QDSS_TPDM_PIDR0_PART_1_SHFT                                                                 0x1
#define HWIO_QDSS_TPDM_PIDR0_PART_0_BMSK                                                                 0x1
#define HWIO_QDSS_TPDM_PIDR0_PART_0_SHFT                                                                 0x0

#define HWIO_QDSS_TPDM_PIDR1_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fe4)
#define HWIO_QDSS_TPDM_PIDR1_RMSK                                                                       0xff
#define HWIO_QDSS_TPDM_PIDR1_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR1_ADDR, HWIO_QDSS_TPDM_PIDR1_RMSK)
#define HWIO_QDSS_TPDM_PIDR1_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR1_ADDR, m)
#define HWIO_QDSS_TPDM_PIDR1_DES_0_BMSK                                                                 0xf0
#define HWIO_QDSS_TPDM_PIDR1_DES_0_SHFT                                                                  0x4
#define HWIO_QDSS_TPDM_PIDR1_PART_1_BMSK                                                                 0xf
#define HWIO_QDSS_TPDM_PIDR1_PART_1_SHFT                                                                 0x0

#define HWIO_QDSS_TPDM_PIDR2_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fe8)
#define HWIO_QDSS_TPDM_PIDR2_RMSK                                                                       0xff
#define HWIO_QDSS_TPDM_PIDR2_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR2_ADDR, HWIO_QDSS_TPDM_PIDR2_RMSK)
#define HWIO_QDSS_TPDM_PIDR2_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR2_ADDR, m)
#define HWIO_QDSS_TPDM_PIDR2_REVISION_BMSK                                                              0xf0
#define HWIO_QDSS_TPDM_PIDR2_REVISION_SHFT                                                               0x4
#define HWIO_QDSS_TPDM_PIDR2_JEDEC_BMSK                                                                  0x8
#define HWIO_QDSS_TPDM_PIDR2_JEDEC_SHFT                                                                  0x3
#define HWIO_QDSS_TPDM_PIDR2_DES_1_BMSK                                                                  0x7
#define HWIO_QDSS_TPDM_PIDR2_DES_1_SHFT                                                                  0x0

#define HWIO_QDSS_TPDM_PIDR3_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000fec)
#define HWIO_QDSS_TPDM_PIDR3_RMSK                                                                       0xff
#define HWIO_QDSS_TPDM_PIDR3_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR3_ADDR, HWIO_QDSS_TPDM_PIDR3_RMSK)
#define HWIO_QDSS_TPDM_PIDR3_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_PIDR3_ADDR, m)
#define HWIO_QDSS_TPDM_PIDR3_REVAND_BMSK                                                                0xf0
#define HWIO_QDSS_TPDM_PIDR3_REVAND_SHFT                                                                 0x4
#define HWIO_QDSS_TPDM_PIDR3_CMOD_BMSK                                                                   0xf
#define HWIO_QDSS_TPDM_PIDR3_CMOD_SHFT                                                                   0x0

#define HWIO_QDSS_TPDM_CIDR0_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000ff0)
#define HWIO_QDSS_TPDM_CIDR0_RMSK                                                                       0xff
#define HWIO_QDSS_TPDM_CIDR0_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_CIDR0_ADDR, HWIO_QDSS_TPDM_CIDR0_RMSK)
#define HWIO_QDSS_TPDM_CIDR0_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_CIDR0_ADDR, m)
#define HWIO_QDSS_TPDM_CIDR0_PRMBL_0_BMSK                                                               0xff
#define HWIO_QDSS_TPDM_CIDR0_PRMBL_0_SHFT                                                                0x0

#define HWIO_QDSS_TPDM_CIDR1_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000ff4)
#define HWIO_QDSS_TPDM_CIDR1_RMSK                                                                       0xff
#define HWIO_QDSS_TPDM_CIDR1_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_CIDR1_ADDR, HWIO_QDSS_TPDM_CIDR1_RMSK)
#define HWIO_QDSS_TPDM_CIDR1_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_CIDR1_ADDR, m)
#define HWIO_QDSS_TPDM_CIDR1_CLASS_BMSK                                                                 0xf0
#define HWIO_QDSS_TPDM_CIDR1_CLASS_SHFT                                                                  0x4
#define HWIO_QDSS_TPDM_CIDR1_PRMBL_1_BMSK                                                                0xf
#define HWIO_QDSS_TPDM_CIDR1_PRMBL_1_SHFT                                                                0x0

#define HWIO_QDSS_TPDM_CIDR2_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000ff8)
#define HWIO_QDSS_TPDM_CIDR2_RMSK                                                                       0xff
#define HWIO_QDSS_TPDM_CIDR2_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_CIDR2_ADDR, HWIO_QDSS_TPDM_CIDR2_RMSK)
#define HWIO_QDSS_TPDM_CIDR2_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_CIDR2_ADDR, m)
#define HWIO_QDSS_TPDM_CIDR2_PRMBL_2_BMSK                                                               0xff
#define HWIO_QDSS_TPDM_CIDR2_PRMBL_2_SHFT                                                                0x0

#define HWIO_QDSS_TPDM_CIDR3_ADDR                                                                 (QDSS_VSENSE_CONTROLLER_REG_BASE      + 0x00000ffc)
#define HWIO_QDSS_TPDM_CIDR3_RMSK                                                                       0xff
#define HWIO_QDSS_TPDM_CIDR3_IN          \
        in_dword_masked(HWIO_QDSS_TPDM_CIDR3_ADDR, HWIO_QDSS_TPDM_CIDR3_RMSK)
#define HWIO_QDSS_TPDM_CIDR3_INM(m)      \
        in_dword_masked(HWIO_QDSS_TPDM_CIDR3_ADDR, m)
#define HWIO_QDSS_TPDM_CIDR3_PRMBL_3_BMSK                                                               0xff
#define HWIO_QDSS_TPDM_CIDR3_PRMBL_3_SHFT                                                                0x0


#endif /* __HWIO_QDSS_VSENSE_CONTROLLER_H__ */
