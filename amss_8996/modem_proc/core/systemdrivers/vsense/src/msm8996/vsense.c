#include "vsense_internal.h"
#include "vsense.h"
#include "DALSys.h"
#include "vsense_dal_prop_ids.h"
#include "HALhwio.h"
#include "tcsr_hwio.h"
#include "smem.h"
#include "smem_type.h"
#include "CoreVerify.h"
#include "mpss_perph_hwio.h"
#include "vsense_utils.h"
#include "npa_resource.h"
#include "npa.h"
#include "hwio_qdss_vsense_controller.h"
#include "rpm_core_hwio.h"
#include "msmhwiobase.h"
#include "pm_smps.h"
#include "gcc_mss_hwio.h" 
#include "ClockHWIO.h"
#include "DDIClock.h"
#include "DALDeviceId.h"

static DALSYS_PROPERTY_HANDLE_DECLARE(hProp_vsense);
static DALSYSPropertyVar prop_vsense;               
//static uint32 vsense_fuse1 = 74; //need to read fuses but just define for now (0.85V)
//static uint32 vsense_fuse2 = 36; //delta bw 0.85V and 1.05V

//static uint32 vsense_voltage_fuse_1 = 850; //voltage calibrated for fuse 1
//static uint32 vsense_voltage_fuse_2 = 1050; //voltage calibrated for fuse 2
#define VCS_VDD_MSS_RAIL_PRE_EVENT  "vcx_mss_pre_change"
#define VCS_VDD_MSS_RAIL_POST_EVENT  "vcx_mss_post_change"


#define VSENSE_FIFO_READ_DELAY  1 // 10 XO cycles
#define VSENSE_STATUS_ALARM_MASK 0xF //mask for alarm bits

static vsense_type_dal_cfg*  vsense_dal_data;
static npa_event_handle pre_volt_cb_handle = NULL;
static npa_event_handle post_volt_cb_handle = NULL;
static boolean vsense_is_nas_turning_on = FALSE; 
static vsense_mss_clk_type  vsense_mss_clk;
//static uint64 vsense_watermark_mode_start_timestamp = 0;
#define VSENSE_IRQ     34
#define UNLOCK_REG_VAL 0xC5ACCE55
#define VSENSE_WAIT_FOR_WR_STATUS(sensor_id)  while((inpdw(HWIO_ADDR( QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS)) & (1 << sensor_id)) != (1 << sensor_id))  //DALSYS_BusyWait(20)
//while(inpdw(HWIO_ADDR( QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS)) != (1 << sensor_id))

static vsense_type_rail_and_fuse_info vsense_rail_and_fuse_info =
{
  .rail_info = 
  {
    {
    .rail  = VSENSE_TYPE_RAIL_MX,
    .is_supported = TRUE,
    .num_sensor_instances = 3,
    .sensor_id_arr =
    	{2,5,7}
   },                
   {               
    .rail = VSENSE_TYPE_RAIL_CX,
    .is_supported = TRUE, 
    .num_sensor_instances = 1,
    .sensor_id_arr =
    	{0}
   },
   
   {               
    .rail = VSENSE_TYPE_RAIL_EBI,
    .is_supported = TRUE, 
    .num_sensor_instances = 2,
    .sensor_id_arr =
    	{3,6}
   },
   {               
    .rail = VSENSE_TYPE_RAIL_MSS,
    .is_supported = TRUE, 
    .num_sensor_instances = 1,
    .sensor_id_arr =
    	{4}
   },
   {               
    .rail = VSENSE_TYPE_RAIL_GFX,
    .is_supported = TRUE, 
   
   },
  },
  .smem_info = 
  {  
    .version = 1,
    .num_of_vsense_rails = VSENSE_TYPE_RAIL_MAX,
    .rails_fuse_info = 
    {
      {
        .rail  = VSENSE_TYPE_RAIL_MX,
      },                
      {               
        .rail = VSENSE_TYPE_RAIL_CX,
      },
      {               
        .rail = VSENSE_TYPE_RAIL_EBI,
     },
     {               
        .rail = VSENSE_TYPE_RAIL_MSS,
     },
     {               
        .rail = VSENSE_TYPE_RAIL_GFX,
      },
    
   }
  }
};



static void vsense_pre_vdd_switch_cb(
   void         *context,
   unsigned int  event_type,
   void         *data,
   unsigned int  data_size);
static void  vsense_post_vdd_switch_cb(
   void         *context,
   unsigned int  event_type,
   void         *data,
   unsigned int  data_size);
uint32 vsense_cal_voltage_code(vsense_type_rail rail,uint32 input_uv, uint8 sensor_count);
void vsense_config_common(vsense_type_rail rail, uint8 sensor_id);
void vsense_power_en(vsense_type_rail rail, boolean en_vsense);
//void vsense_log_rail_status(void);
void vsense_read_fifo(vsense_type_rail rail, uint8* fifo_data);
void vsense_disable_and_reset(vsense_type_rail rail, uint8 sensor_id);
vsense_error_type vsense_clock_init(void);
void vsense_controller_en(boolean en_vsense);
//void vsense_clear_bit_enable(vsense_type_rail rail, boolean enable);
void vsense_unreset_and_cgc_clk_enable(vsense_type_rail rail);
//void vsense_reset_and_cgc_clk_disable(vsense_type_rail rail);
void vsense_isr_config(void);
void vsense_config_watermark_mode(vsense_type_rail rail);
void vsense_check_watermark_duration(void);
void vsense_disable_alarms(vsense_type_rail rail);
void vsense_start_capture(vsense_type_rail rail, boolean en_capture);
uint8 vsense_calibrate_rail(vsense_type_rail rail);
void vsense_func_en(vsense_type_rail rail, boolean en_vsense, uint8 sensor_id);
extern unsigned int qdss_fuse_trace_access(void);
//static void vsense_log_status_and_fifo_isr(void) __irq ;

void vsense_ulog_init(void){
	
	vsense_ulog_vars.log_size=4096;
	ULogFront_RealTimeInit(
      &vsense_ulog_vars.vsense_log,
      "VSENSE Log",
      vsense_ulog_vars.log_size,
      ULOG_MEMORY_LOCAL,
      ULOG_LOCK_OS);
	  
	ULogCore_HeaderSet(vsense_ulog_vars.vsense_log, "Content-Type: text/tagged-log-1.0;");
}


//Configure the initial state of the vsense
vsense_error_type vsense_init()
{
  int32 rail_count;
  uint32 smem_size ; //, clk_status; 
  //pm_volt_level_type cur_mss_uv;
  //pm_on_off_type  sw_en_status ;
  DALSYS_GetDALPropertyHandleStr("/sysdrivers/vsense",hProp_vsense);


  vsense_dal_data       = (vsense_type_dal_cfg*)
                         vsense_get_config_info(VSENSE_PROP_CONFIG);

  vsense_type_smem_info* vsense_data = 
       (vsense_type_smem_info*) smem_get_addr(SMEM_VSENSE_DATA, &smem_size);

  if(FALSE != qdss_fuse_trace_access())
  {
     vsense_controller_en(TRUE);
  }
  vsense_ulog_init();
  //if (vsense_dal_data == NULL)
  if (vsense_data == NULL || vsense_dal_data == NULL)
  {
    //SWEVENT(VSENSE_EVENT_INIT); 
       ULOG_RT_PRINTF_0(
        vsense_ulog_vars.vsense_log, " VSENSE_EVENT_INIT ");
    return  VSENSE_ERROR_INIT_FAILURE;
  }
  else
  {
     memset((void*)&vsense_rail_and_fuse_info.smem_info, 0 , smem_size);
    // HWIO_OUT(RPM_PAGE_SELECT,0x2);                                         
     memcpy((void*)&vsense_rail_and_fuse_info.smem_info,vsense_data , 
                   sizeof(vsense_rail_and_fuse_info.smem_info));
    // HWIO_OUT(RPM_PAGE_SELECT,0x0); 
  }

  vsense_clock_init(); 

  DALSYS_BusyWait(50);


  HWIO_OUT(QDSS_TPDM_LAR, UNLOCK_REG_VAL);
  //HWIO_OUT(GCC_MSS_VS_RESET, 0x1); 
  
  //Register Callbacks 
  /*
     VCSNPARailEventDataType
  */
  pre_volt_cb_handle = npa_create_custom_event( VCS_NPA_RESOURCE_VDD_MSS_UV,
                                              VCS_VDD_MSS_RAIL_PRE_EVENT,
                                              VCS_NPA_RAIL_EVENT_PRE_CHANGE,
                                              NULL,
                                              vsense_pre_vdd_switch_cb,
                                              NULL);

  post_volt_cb_handle = npa_create_custom_event( VCS_NPA_RESOURCE_VDD_MSS_UV,
                                              VCS_VDD_MSS_RAIL_POST_EVENT,
                                              VCS_NPA_RAIL_EVENT_POST_CHANGE,
                                              NULL,
                                              vsense_post_vdd_switch_cb,
                                              NULL);
  /* ====== Test Code =======*/
  /*pm_smps_sw_enable_status(0, 6,&sw_en_status);
  if(sw_en_status == PM_OFF)
  {
    pm_smps_sw_enable(0, 6,PM_ON);
  }  
	
	clk_status = inpdw(HWIO_ADDR( GCC_MSS_CFG_AHB_CBCR));
	clk_status |= 0x1;
	HWIO_OUT(GCC_MSS_CFG_AHB_CBCR, clk_status);
	while((inpdw(HWIO_ADDR( GCC_MSS_CFG_AHB_CBCR)) & 0x80000000) != 0x0);
  //get the current voltage on MSS rail 
    pm_smps_volt_level(0, 6, 1000000);
  pm_smps_volt_level_status(0, 6, &cur_mss_uv);
  HWIO_OUT(MSS_CLAMP_IO, 0x8); //unclamp MSS 
    //HWIO_OUT(GCC_MSS_VS_RESET, 0x0); 
  vsense_set_mode_alarm(VSENSE_TYPE_RAIL_MSS,cur_mss_uv );
  vsense_set_mode_slope_detection(VSENSE_TYPE_RAIL_MSS);
  */
//register isr
   //vsense_isr_config();
   return VSENSE_SUCCESS;
}

/*
void vsense_isr_config(void)
{
  interrupt_set_isr(VSENSE_IRQ, vsense_log_status_and_fifo_isr);
  interrupt_configure(VSENSE_IRQ, RISING_EDGE);
  interrupt_enable(VSENSE_IRQ);
}


static void vsense_log_status_and_fifo_isr(void) __irq 
{
  lock_ints();
  //SWEVENT(VSENSE_EVENT_ISR );
  //QDSS_VSENSE_SENSOR_ALARM_FC_STATUS : 0x03038000 + 0x00000098 (0x03038098) 
  //vsense_log_rail_status();

  unlock_ints();
}
*/

static npa_client_handle vsense_qdss_client;


vsense_error_type vsense_clock_init()
{
  DALResult dal_result;
  int32     clk_count;
    
   /*
  gcc_vddcx_vs_clk
  gcc_vddmx_vs_clk
  gcc_vdda_vs_clk
  gcc_vs_ctrl_clk          
  */
  
   
  vsense_qdss_client = npa_create_sync_client("/clk/qdss", "debugger", NPA_CLIENT_REQUIRED);
  CORE_VERIFY(vsense_qdss_client);
  npa_issue_required_request(vsense_qdss_client,CLOCK_QDSS_LEVEL_DEBUG);
   
  if(DAL_SUCCESS != DAL_DeviceAttach(DALDEVICEID_CLOCK, &vsense_mss_clk.pHandle))
  {
      ULOG_RT_PRINTF_0( vsense_ulog_vars.vsense_log, 
	          " VSense Clock Init Failed ");
      return VSENSE_ERROR_INIT_FAILURE;
  }
  
  if(DAL_SUCCESS != 
            DalClock_GetClockId(vsense_mss_clk.pHandle, "gcc_mss_cfg_ahb_clk", 
                                 &vsense_mss_clk.mss_cfg_clk_id ))
  {
         ULOG_RT_PRINTF_0( vsense_ulog_vars.vsense_log, 
		             " VSense Could not get ClockId ");
        return VSENSE_ERROR_INIT_FAILURE;
  }								 
 
  return VSENSE_SUCCESS;
}

//power on the vsense if it was previously off 
void vsense_power_en(vsense_type_rail rail, boolean pwr_en_vsense)
{
  
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
   
  if((vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en == TRUE) 
      && (pwr_en_vsense == FALSE))
  {
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, rail, POWER_EN, 0);
  }
  else if((vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en == FALSE) && (pwr_en_vsense == TRUE))
  {
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, rail, POWER_EN, 1);
  }
  
}



void vsense_func_en(vsense_type_rail rail, boolean en_vsense, uint8 sensor_id)
{
  
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
   
  if(en_vsense == FALSE)
  {
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, FUNC_EN , 0);
  }
  else
  {
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, FUNC_EN, 1);
  }
  
}


void vsense_alarm_en(vsense_type_rail rail, boolean en_alarm, uint8 sensor_id)
{
  //uint32 reg_addr; 
 
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
  

  if(en_alarm)
  {
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, ALARM_MIN_EN, 1 );
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, ALARM_MAX_EN, 1 );
  }
  else
  {
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, ALARM_MIN_EN, 0 );
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, ALARM_MAX_EN, 0 );
  }

  //set the write enable to push the data through the daisy chain
  //reg_addr = HWIO_ADDR(QDSS_VSENSE_SENSOR_CONFIG_WR_EN);
  //outpdw(reg_addr, 0);
  //outpdw(reg_addr, (1 << sensor_id));
  //VSENSE_WAIT_FOR_WR_STATUS(sensor_id);

}

//enable/disable vsense 
void vsense_controller_en( boolean en_vsense)
{

  //HWIO_OUT(QDSS_TPDM_LAR, UNLOCK_REG_VAL);

  if(en_vsense == FALSE)
  {
    HWIO_OUT(TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER, 0); //disable controller
  }
  else
  {
    HWIO_OUT(TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER, 1); //enable controller
  }

   
}


//change the rail min or max thresholds based on the mode 
void vsense_pre_vdd_switch_cb(
   void         *context,
   unsigned int  event_type,
   void         *data,
   unsigned int  data_size
   )
{
  uint32 voltage_code = 0;
  uint32 addr_config_0 = 0;
  uint32 addr_wr_en = 0;
  uint32 val = 0;
  uint32 status_addr ;
  uint8  sensor_count = 0; 
  uint8  sensor_id = 0;
  VCSNPARailEventDataType *proposal;
  uint8  num_of_sensors =
  	       vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MSS].num_sensor_instances;

  //SWEVENT(VSENSE_EVENT_PRE_VDD_CB, proposal->PreChange.eCorner, proposal->PreChange.nVoltageUV); 
         ULOG_RT_PRINTF_2(
        vsense_ulog_vars.vsense_log, " VSENSE_EVENT_PRE_VDD_CB proposal->PreChange.eCorner:%d , proposal->PreChange.nVoltageUV: %d ",
                          proposal->PreChange.eCorner, proposal->PreChange.nVoltageUV);
  
  if(vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MSS].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
  if (data == NULL)
  {
    //log something
    return;
  }
  if (event_type != VCS_NPA_RAIL_EVENT_PRE_CHANGE)
  {
    //log something
    return;
  }
  proposal = (VCSNPARailEventDataType *)data;
 /*
  if(vsense_dal_data->mode == VSENSE_TYPE_WATERMARK_MODE)
  {
    vsense_check_watermark_duration();
    return;
  }
 */
 addr_wr_en = HWIO_ADDR(QDSS_VSENSE_SENSOR_CONFIG_WR_EN);
 
 for(sensor_count= 0; 
         sensor_count < num_of_sensors; 
         sensor_count++)
  {
 
      sensor_id =
      	 vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MSS].sensor_id_arr[sensor_count];
      addr_config_0 = HWIO_ADDRI(QDSS_VSENSE_SENSORn_CONFIG_0, sensor_id);
      status_addr   = HWIO_ADDRI(QDSS_VSENSE_SENSORn_SUMMARY, sensor_id);
  
      vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MSS].status =
                                inpdw(status_addr);

      //log the rail status in case the interrupt did not woke us up
      //vsense_log_rail_status();
      if(proposal->bIsNAS)
      {
        vsense_is_nas_turning_on = TRUE; 
        vsense_disable_and_reset(VSENSE_TYPE_RAIL_MSS, sensor_id);
        //push it through the controller now
        outpdw(addr_wr_en, 0);
        outpdw(addr_wr_en, (1 << sensor_id));
        VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
        HWIO_OUT(MSS_CLAMP_IO, 0x0); //clamp MSS 
        HWIO_OUT(GCC_MSS_VS_RESET, 0x1);		
        DalClock_DisableClock(vsense_mss_clk.pHandle, vsense_mss_clk.mss_cfg_clk_id);	
        continue;
      }
      
      switch(proposal->PostChange.eCorner)
      {
        case VCS_CORNER_OFF:
        case VCS_CORNER_RETENTION:	
         // vsense_power_en( rail_info->rail, FALSE);
          vsense_disable_and_reset(VSENSE_TYPE_RAIL_MSS, sensor_id);
          
          DalClock_DisableClock(vsense_mss_clk.pHandle, vsense_mss_clk.mss_cfg_clk_id);
          //push it through the controller now
          outpdw(addr_wr_en, 0);
          outpdw(addr_wr_en, (1 << sensor_id));
          VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
          HWIO_OUT(MSS_CLAMP_IO, 0x0); //clamp MSS 
		      HWIO_OUT(GCC_MSS_VS_RESET, 0x1);
        	 break;
      
         
        case VCS_CORNER_LOW_MINUS :      
        case VCS_CORNER_LOW   :   
        case VCS_CORNER_LOW_PLUS  :
        case VCS_CORNER_NOMINAL   :
        case VCS_CORNER_NOMINAL_PLUS     :
        case VCS_CORNER_TURBO :
       
        //if we are coming out of retention , do not do anything in pre
        if(proposal->PreChange.eCorner > VCS_CORNER_RETENTION)
        {
              
          voltage_code = vsense_cal_voltage_code(VSENSE_TYPE_RAIL_MSS ,
          	             proposal->PostChange.nVoltageUV, sensor_count);
          vsense_func_en(VSENSE_TYPE_RAIL_MSS, FALSE, sensor_id);
          vsense_alarm_en(VSENSE_TYPE_RAIL_MSS, FALSE, sensor_id);
          //push it through the controller now
          outpdw(addr_wr_en, 0);
          outpdw(addr_wr_en, (1 << sensor_id));
          VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
        
          val = inpdw(addr_config_0);
          if(proposal->PreChange.nVoltageUV < proposal->PostChange.nVoltageUV) //going high
          {
            val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MAX);
            val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MAX, 
                        voltage_code + vsense_dal_data->max_threshold_offset);
          }
          else if(proposal->PreChange.nVoltageUV > proposal->PostChange.nVoltageUV)//going low
          {
            val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MIN);
            val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MIN, 
                        voltage_code - vsense_dal_data->min_threshold_offset);
          }
          outpdw(addr_config_0, val);
          //push it through the controller now
          outpdw(addr_wr_en, 0);
          outpdw(addr_wr_en, (1 << sensor_id));
          VSENSE_WAIT_FOR_WR_STATUS(sensor_id);

          vsense_func_en(VSENSE_TYPE_RAIL_MSS, TRUE, sensor_id);
          vsense_alarm_en(VSENSE_TYPE_RAIL_MSS, TRUE, sensor_id);
          //push it through the controller now
          outpdw(addr_wr_en, 0);
          outpdw(addr_wr_en, (1 << sensor_id));
          VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
          
        }
        break;
		default :
		break;
      }
  }

}

//change the rail min or max thresholds based on the mode 
void vsense_post_vdd_switch_cb(
   void         *context,
   unsigned int  event_type,
   void         *data,
   unsigned int  data_size)
{
  
  uint32 voltage_code = 0;
  uint32 addr_config_0 ;
  uint32 addr_wr_en = 0;
  uint32 val = 0;
  uint8  sensor_count = 0; 
  uint8  sensor_id = 0;

  VCSNPARailEventDataType *proposal;
  uint8  num_of_sensors =
  	       vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MSS].num_sensor_instances;
  
  if(vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MSS].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
  if (data == NULL)
  {
    //log something
    return;
  }
  if (event_type != VCS_NPA_RAIL_EVENT_POST_CHANGE)
  {
    //log something
    return;
  }
  proposal = (VCSNPARailEventDataType *)data;

  addr_wr_en = HWIO_ADDR(QDSS_VSENSE_SENSOR_CONFIG_WR_EN);

   for(sensor_count= 0; 
         sensor_count < num_of_sensors; 
         sensor_count++)
  {
 
      sensor_id =
      	 vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MSS].sensor_id_arr[sensor_count];
      addr_config_0 = HWIO_ADDRI(QDSS_VSENSE_SENSORn_CONFIG_0, sensor_id);

      if(vsense_is_nas_turning_on)
      {
         vsense_is_nas_turning_on = FALSE;
         if(DAL_SUCCESS != DalClock_EnableClock(vsense_mss_clk.pHandle, vsense_mss_clk.mss_cfg_clk_id))
        {
          ULOG_RT_PRINTF_0(
             vsense_ulog_vars.vsense_log,"Vsense: Could not enable clock");
          return ;
        }
    		 
         HWIO_OUT(MSS_CLAMP_IO, 0x8); //unclamp MSS 
		 HWIO_OUT(GCC_MSS_VS_RESET, 0x0);
         vsense_set_mode_alarm(VSENSE_TYPE_RAIL_MSS, proposal->PostChange.nVoltageUV);
         vsense_set_mode_slope_detection(VSENSE_TYPE_RAIL_MSS);
         continue;
      }
  
      switch(proposal->PostChange.eCorner)
      {
         case VCS_CORNER_LOW_MINUS :      
         case VCS_CORNER_LOW   :   
         case VCS_CORNER_LOW_PLUS  :
         case VCS_CORNER_NOMINAL   :
         case VCS_CORNER_NOMINAL_PLUS     :
         case VCS_CORNER_TURBO : 
          
        	if(proposal->PreChange.eCorner < VCS_CORNER_LOW_MINUS)
        	{
               if(DAL_SUCCESS != DalClock_EnableClock(vsense_mss_clk.pHandle, vsense_mss_clk.mss_cfg_clk_id))
               {
                  ULOG_RT_PRINTF_0(
                   vsense_ulog_vars.vsense_log,"Vsense: Could not enable clock");
                   return ;
               } 
            HWIO_OUT(MSS_CLAMP_IO, 0x8); //unclamp MSS 
			  HWIO_OUT(GCC_MSS_VS_RESET, 0x0);
            vsense_set_mode_alarm(VSENSE_TYPE_RAIL_MSS, proposal->PostChange.nVoltageUV);
            vsense_set_mode_slope_detection(VSENSE_TYPE_RAIL_MSS);
        	}
         else
         {
          voltage_code = vsense_cal_voltage_code(VSENSE_TYPE_RAIL_MSS, proposal->PostChange.nVoltageUV, sensor_count); 
          val = inpdw(addr_config_0);
          vsense_func_en(VSENSE_TYPE_RAIL_MSS, FALSE, sensor_id);
          vsense_alarm_en(VSENSE_TYPE_RAIL_MSS, FALSE, sensor_id);
          //push it through the controller now
          outpdw(addr_wr_en, 0);
          outpdw(addr_wr_en, (1 << sensor_id));
          VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
        
          //we can set both min and max here since in the pre we would have 
          //already set the required min or max , so one of the thresholds(low/high) would 
          //have already been set by the time we reach here.
        
          val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MIN);
          val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MIN, 
                        voltage_code - vsense_dal_data->min_threshold_offset);

          val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MAX);
          val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MAX, 
                      voltage_code + vsense_dal_data->max_threshold_offset);
      
          outpdw(addr_config_0, val);
          //push it through the controller now
          outpdw(addr_wr_en, 0);
          outpdw(addr_wr_en, (1 << sensor_id));
          VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
          
          //vsense_set_mode_alarm(rail_info->rail, proposal->microvolts );
          //vsense_set_mode_slope_detection(rail_info->rail);
          vsense_func_en(VSENSE_TYPE_RAIL_MSS, TRUE, sensor_id);
          vsense_alarm_en(VSENSE_TYPE_RAIL_MSS, TRUE, sensor_id);
          //push it through the controller now
          outpdw(addr_wr_en, 0);
          outpdw(addr_wr_en, (1 << sensor_id));
          VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
          
          
         	}
          break;
		  default:
		  break;
      	}
  }
  //rail_info->previous_corner = proposal->mode;
  //rail_info->previous_uv     = proposal->microvolts;
  
}

//common settings for all the voltage sensors 
void vsense_config_common(vsense_type_rail rail, uint8 sensor_id)
{
  uint32 val_config_1 ;
  uint32 addr_config_1 ;
 

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
  
  addr_config_1 = HWIO_ADDRI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id);
  val_config_1 = inpdw(addr_config_1);
  //val_config_1 |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_1, RSVD_BITS31_18, 0x2000);
  val_config_1 |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_1, POWER_EN, 1);
  outpdw(addr_config_1, val_config_1);

  //set the write enable to push the data through the daisy chain
  addr_config_1 = HWIO_ADDR(QDSS_VSENSE_SENSOR_CONFIG_WR_EN);
  outpdw(addr_config_1, 0);
  outpdw(addr_config_1, (1 << sensor_id));
  VSENSE_WAIT_FOR_WR_STATUS(sensor_id);

}

//configure the vsense to set alarm
void vsense_set_mode_alarm(vsense_type_rail rail, uint32 voltage_uv )
{
  uint8 voltage_code = 0;
  uint32 val ;
  uint32 addr_config_1 = 0;
  uint32 addr_config_0 = 0;
  uint8  sensor_count = 0; 
  uint8  sensor_id = 0;
  uint8  num_of_sensors = 
  	          vsense_rail_and_fuse_info.rail_info[rail].num_sensor_instances;
  
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }

  for(sensor_count= 0; 
         sensor_count < num_of_sensors; 
         sensor_count++)
  {
      sensor_id = vsense_rail_and_fuse_info.rail_info[rail].sensor_id_arr[sensor_count];
      addr_config_1 = HWIO_ADDRI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id);
      addr_config_0 = HWIO_ADDRI(QDSS_VSENSE_SENSORn_CONFIG_0, sensor_id);

      voltage_code = vsense_cal_voltage_code(rail, voltage_uv, sensor_count);

      //call the common routine to set the initialization sequence 
      vsense_config_common(rail, sensor_id);
  
      val = inpdw(addr_config_0);
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, TRIG_POS);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_0, TRIG_POS, VSENSE_TRIG_POST);
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, CAPTURE_DELAY);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_0, CAPTURE_DELAY, 0);
  
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MIN);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MIN, 
                        voltage_code - vsense_dal_data->min_threshold_offset);
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MAX);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MAX, 
                        voltage_code + vsense_dal_data->max_threshold_offset);
      outpdw(addr_config_0, val);

    
      val = inpdw(addr_config_1);
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_1, MODE_SEL);
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_1, SW_CAPTURE);
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_1, FUNC_EN);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_1, FUNC_EN, 1);
      outpdw(addr_config_1, val);
  
      vsense_alarm_en( rail, TRUE, sensor_id);


      //set the write enable to push the data through the daisy chain
      addr_config_1 = HWIO_ADDR(QDSS_VSENSE_SENSOR_CONFIG_WR_EN);
      outpdw(addr_config_1, 0);
      outpdw(addr_config_1, (1 << sensor_id));
      VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
  }
  //SWEVENT(VSENSE_EVENT_CONFIG_ALARM, rail, voltage_uv, voltage_code,
      //         voltage_code - vsense_dal_data->min_threshold_offset
              //voltage_code + vsense_dal_data->max_threshold_offset*/ );

  vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en = TRUE;
 
}
//configure the vsense to detect droops (slope) 
void vsense_set_mode_slope_detection(vsense_type_rail rail)
{
  uint32 val ;
  uint32 addr_config_1 ;
  uint32 addr_config_0 ;
  uint8  slope_threshold;
  uint8  sensor_count = 0; 
  uint8  sensor_id = 0;


  vsense_type_fuse_info  *fuse_info = 
  	                      vsense_rail_and_fuse_info.smem_info.rails_fuse_info;

  //uint8  delta_cycle = 1; //diff between 1st and 3rd sample;
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
 

 

        
  
 
  
  for(sensor_count= 0; 
          sensor_count < vsense_rail_and_fuse_info.rail_info[rail].num_sensor_instances; 
          sensor_count++)
  {
      uint32 fuse1_code = 
           fuse_info[rail].voltage_code_arr[sensor_count].fuse1_voltage_code;
      uint32 fuse2_code = 
           fuse_info[rail].voltage_code_arr[sensor_count].fuse2_voltage_code;
      uint32 fuse1_voltage = 
           fuse_info[rail].fuse1_voltage_uv /1000; //convert to mV 
      uint32 fuse2_voltage = 
           fuse_info[rail].fuse2_voltage_uv / 1000; //convert to mV

      slope_threshold = 3 * ((fuse2_voltage - fuse1_voltage) / (fuse2_code - fuse1_code));
       
	  //SWEVENT(VSENSE_EVENT_SLOPE_THRESHOLD, rail, slope_threshold, 0);
	  
      sensor_id = vsense_rail_and_fuse_info.rail_info[rail].sensor_id_arr[sensor_count];
      addr_config_1 = HWIO_ADDRI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id);
      addr_config_0 = HWIO_ADDRI(QDSS_VSENSE_SENSORn_CONFIG_0, sensor_id);
   
      val = inpdw(addr_config_0);
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, TRIG_POS);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_0, TRIG_POS, VSENSE_TRIG_POST);
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, CAPTURE_DELAY);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_0, CAPTURE_DELAY, 0);
      outpdw(addr_config_0, val);

      val  = inpdw(addr_config_1);
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_SLOPE);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_SLOPE, slope_threshold); //may not cross min-max but still a transient to catch 
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_1, SLOPE_DELTA_CYC);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_1, SLOPE_DELTA_CYC, 0); //diff between 1st and 3rd sample
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_1, FUNC_EN);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_1, FUNC_EN, 1);
      outpdw(addr_config_1, val);

      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_1, ALARM_SLOPE_NEG_EN);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_1, ALARM_SLOPE_NEG_EN, 1);
      val &= ~HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_1, ALARM_SLOPE_POS_EN);
      val |= HWIO_FVAL(QDSS_VSENSE_SENSORn_CONFIG_1, ALARM_SLOPE_POS_EN, 1);
      outpdw(addr_config_1, val);

      //set the write enable to push the data through the daisy chain
      addr_config_1 = HWIO_ADDR(QDSS_VSENSE_SENSOR_CONFIG_WR_EN);
      outpdw(addr_config_1, 0);
      outpdw(addr_config_1, (1 << sensor_id));
      VSENSE_WAIT_FOR_WR_STATUS(sensor_id);  

      vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en = TRUE;
  }
   
}
//configure the vsense to capture min and max for a given duration

/*
void vsense_set_mode_min_max(vsense_type_rail rail)
{
  uint32 val_config_0;
  uint32 val_config_1;
  uint32 addr_config_1 = 0;
  uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  vsense_config_common(rail);
  
  val_config_0  = inpdw(addr_config_0);
  val_config_0 |= HWIO_FVAL(TCSR_CX_VSENS_CONFIG_0, MODE_SEL, 1);
  outpdw(addr_config_0, val_config_0);

  val_config_1  = inpdw(addr_config_1);
  val_config_1 |= HWIO_FVAL(TCSR_CX_VSENS_CONFIG_1, FUNC_EN, 1);
  outpdw(addr_config_1, val_config_1);


  val_config_0 |= HWIO_FVAL(TCSR_CX_VSENS_CONFIG_0, START_CAPTURE, 1);
  outpdw(addr_config_0, val_config_0);

  DALSYS_BusyWait(1000000); 

  val_config_0 &= HWIO_FVAL(TCSR_CX_VSENS_CONFIG_0, START_CAPTURE, 0);
  outpdw(addr_config_0, val_config_0);
  
  vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en = TRUE;

}
*/
//get vsense config data 
void* vsense_get_config_info(uint32 prop_id)
{
  void* cfg_ptr = NULL;

  if(DAL_SUCCESS == DALSYS_GetPropertyValue(hProp_vsense, NULL, prop_id, &prop_vsense))
  {
      cfg_ptr = (void*) prop_vsense.Val.pStruct;
  }

  return cfg_ptr;
}

//calculate the code point based on input voltage
uint32 vsense_cal_voltage_code(vsense_type_rail rail, uint32 input_uv, uint8 sensor_count)
{
  int32 voltage_code = 0;
  vsense_type_fuse_info  *fuse_info = 
  	                      vsense_rail_and_fuse_info.smem_info.rails_fuse_info;
  int32 input_volt = input_uv;
  uint32 fuse1_code = 
           fuse_info[rail].voltage_code_arr[sensor_count].fuse1_voltage_code;
  uint32 fuse2_code = 
           fuse_info[rail].voltage_code_arr[sensor_count].fuse2_voltage_code;
  uint32 fuse1_voltage = 
           fuse_info[rail].fuse1_voltage_uv /1000; //convert to mV 
  uint32 fuse2_voltage = 
           fuse_info[rail].fuse2_voltage_uv / 1000; //convert to mV

  if(fuse1_code == 0 || fuse2_code == 0 || fuse1_voltage == 0 || fuse2_voltage == 0)
  {
     //SWEVENT(VSENSE_EVENT_CODE_CALC_ERROR, fuse1_code, fuse2_code, 
     	   //                                fuse1_voltage, fuse2_voltage);
     	   return 0;
  }

  //voltage_code = (fuse1_code)+ (fuse2_code - fuse1_code )*((input_uv/1000) - 
  //               fuse1_voltage)/(fuse2_voltage - fuse1_voltage) ;
  //SWEVENT(VSENSE_EVENT_CAL_VOLT_CODE, fuse1_code, fuse1_voltage, fuse2_code, fuse2_voltage );
  voltage_code =  (fuse1_code)+ ((input_volt/1000) - fuse1_voltage)/
                   ((fuse2_voltage - fuse1_voltage) / (fuse2_code - fuse1_code));

  return voltage_code;
}

//disable and reset the vsense
void vsense_disable_and_reset(vsense_type_rail rail, uint8 sensor_id)
{
  uint32 val_config_0 ;
  uint32 val_config_1 ;
  uint32 addr_config_1 = 0;
  uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  

  addr_config_1 = HWIO_ADDRI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id);
  addr_config_0 = HWIO_ADDRI(QDSS_VSENSE_SENSORn_CONFIG_0, sensor_id);

  val_config_0 = 0xFFFF0000; //POR value from the HPG
  val_config_1 = 0x0; //POR value from the HPG

   outpdw(addr_config_0, val_config_0);
   outpdw(addr_config_1, val_config_1); 


}
/*
void vsense_clear_bit_enable(vsense_type_rail rail, boolean enable)
{
  uint32 val_config_1 =0;
  uint32 addr_config_1 ;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;

  if(enable == TRUE)
  {
    val_config_1 &= ~HWIO_FMSK(TCSR_CX_VSENS_CONFIG_1, STATUS_CLEAR);
  }
  else
  {
    val_config_1 |= HWIO_FVAL(TCSR_CX_VSENS_CONFIG_1, STATUS_CLEAR, 1);
  }

   outpdw(addr_config_1, val_config_1); 

}
*/
/*
void vsense_log_rail_status()
{
  int32 rail_count;
  uint32 addr_summary_reg;
  uint32 val_summary_reg;
  static uint32 val_config_0;
  static uint32 val_config_1;
  uint32 addr_config_1;
  uint32 addr_config_0;
  uint8  sensor_id = 0;
  uint8 sensor_count ;
  uint8  num_of_sensors = 0;
  uint32 addr_alarm_fc_status = 0;
  //static uint8* fifo_array = NULL;
  //uint32 fifo_log_loop;
  //uint32* fifo_ptr = NULL;

  addr_alarm_fc_status = HWIO_ADDR(QDSS_VSENSE_SENSOR_ALARM_FC_STATUS);
  
  for( rail_count = 0; rail_count < VSENSE_TYPE_RAIL_APC0; rail_count++ )
  {
    if(vsense_rail_and_fuse_info.rail_info[rail_count].is_supported)
    {
        
      num_of_sensors =
  	       vsense_rail_and_fuse_info.rail_info[rail_count].num_sensor_instances;
      for(sensor_count= 0; sensor_count < num_of_sensors; sensor_count++)
      {
        sensor_id = 
        	 vsense_rail_and_fuse_info.rail_info[rail_count].sensor_id_arr[sensor_count];
        
      
        if(addr_alarm_fc_status &  1 << sensor_id)
        {
          addr_config_1 = HWIO_ADDRI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id);
          addr_config_0 = HWIO_ADDRI(QDSS_VSENSE_SENSORn_CONFIG_0, sensor_id);
          addr_summary_reg =       HWIO_ADDRI(QDSS_VSENSE_SENSORn_SUMMARY, sensor_id);
          val_summary_reg = inpdw(addr_summary_reg);

          val_config_0 = inpdw(addr_config_0);
          val_config_1 = inpdw(addr_config_1);
          SWEVENT(VSENSE_EVENT_SUMMARY, val_summary_reg);
          SWEVENT(VSENSE_EVENT_STATUS, sensor_id, 
                  (val_config_0 & HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MIN)) >> HWIO_SHFT(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MIN) ,
                  (val_config_0 & HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MAX)) >> HWIO_SHFT(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_MAX) ,
                  (val_config_1 & HWIO_FMSK(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_SLOPE)) >> HWIO_SHFT(QDSS_VSENSE_SENSORn_CONFIG_0, THRESHOLD_SLOPE));
        
        SWEVENT(VSENSE_EVENT_ERROR_STATUS,
                  (val_status_reg & HWIO_FMSK(TCSR_CX_VSENS_STATUS, ALARM_MIN)) >> HWIO_SHFT(TCSR_CX_VSENS_STATUS, ALARM_MIN), 
                  (val_status_reg & HWIO_FMSK(TCSR_CX_VSENS_STATUS, ALARM_MAX)) >> HWIO_SHFT(TCSR_CX_VSENS_STATUS, ALARM_MAX),
                  (val_status_reg & HWIO_FMSK(TCSR_CX_VSENS_STATUS, SLOPE_POS)) >> HWIO_SHFT(TCSR_CX_VSENS_STATUS, SLOPE_POS),
                  (val_status_reg & HWIO_FMSK(TCSR_CX_VSENS_STATUS, SLOPE_NEG)) >> HWIO_SHFT(TCSR_CX_VSENS_STATUS, SLOPE_NEG)); 
        
        
        fifo_array = vsense_rail_and_fuse_info.rail_info[rail_count].fifo_data;  
        vsense_read_fifo(vsense_rail_and_fuse_info.rail_info[rail_count].rail,
                         fifo_array );
        
        fifo_ptr = (uint32*)fifo_array;       
        fifo_ptr+= 16; 
        fifo_ptr--;
        for(fifo_log_loop =0; fifo_log_loop < 4 ; fifo_log_loop++)
        {
          SWEVENT(VSENSE_EVENT_FIFO, *(fifo_ptr - 0), *(fifo_ptr - 1), *(fifo_ptr - 2), *(fifo_ptr - 3));
          fifo_ptr = fifo_ptr - 4;
        }
       
         
           //vsense_calibrate_rail(vsense_rail_and_fuse_info.rail_info[rail_count].rail);
          //we detected alarm, reset the sensor and re-arm
          //vsense_disable_and_reset(vsense_rail_and_fuse_info.rail_info[rail_count].rail);
          //previous voltage should be valid since alarm would not be set if we are coming out of SVS 
          //going into SVS should have a valid voltage
          //vsense_set_mode_alarm(vsense_rail_and_fuse_info.rail_info[rail_count].rail,
          //                   vsense_rail_and_fuse_info.rail_info[rail_count].previous_uv);
          //vsense_set_mode_slope_detection(vsense_rail_and_fuse_info.rail_info[rail_count].rail);
          // abort();
        }
      	}
    }
  } 
  
}
*/

/*
//read the 64 bytes fifo from the status register
void vsense_read_fifo(vsense_type_rail rail, uint8* fifo_data)
{
  uint32 addr_config_1 ;
  uint32 addr_status ;
  uint32 val_config_1 = 0;
  uint32 val_status_reg = 0; 
  int8  fifo_reg_offset  = 0x3F;
  uint8  read_byte_count = 0;
  uint32 wait_time = 10000; //10us
  boolean do_until = TRUE;
     
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   return;
  }
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_status = vsense_rail_and_fuse_info.rail_info[rail].status_address;
  val_status_reg = inpdw(addr_status);
  while(do_until)
  {
  if(val_status_reg & HWIO_FVAL(TCSR_CX_VSENS_STATUS, FIFO_COMPLETE_STS, 1))
  {
      do_until = FALSE; 
    do
    {
      val_config_1 = inpdw(addr_config_1);
      val_config_1 &= ~HWIO_FMSK(TCSR_CX_VSENS_CONFIG_1, FIFO_ADDR);
      val_config_1 |= HWIO_FVAL(TCSR_CX_VSENS_CONFIG_1, FIFO_ADDR, fifo_reg_offset);
      outpdw(addr_config_1, val_config_1);
      DALSYS_BusyWait(VSENSE_FIFO_READ_DELAY); //10 XO clk  cycles
      val_status_reg = inpdw(addr_status);
      fifo_data[read_byte_count++] = 
      (val_status_reg & HWIO_FMSK(TCSR_CX_VSENS_STATUS, FIFO_DATA)) >>
      HWIO_SHFT(TCSR_CX_VSENS_STATUS, FIFO_DATA);
      fifo_reg_offset--;
    }while (fifo_reg_offset >= 0);
  }
    else
    {
      if(wait_time)
      {
            DALSYS_BusyWait(1000); //1us
          wait_time = wait_time - 1000;
      }
      else
      {
        //do_untill = FALSE; 
        break;
      }
    }
  }
  
}
*/
/*
//start capture enable disable
void vsense_start_capture(vsense_type_rail rail, boolean en_capture)
{
  uint32 val_config_0 ;
  uint32 addr_config_0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;
  val_config_0 = inpdw(addr_config_0);

  
  if(en_capture == TRUE)
  {
  val_config_0 |= HWIO_FVAL(TCSR_CX_VSENS_CONFIG_0, START_CAPTURE, 1);
  }
  else
  {
    val_config_0 &= ~HWIO_FMSK(TCSR_CX_VSENS_CONFIG_0, START_CAPTURE);
  }

  outpdw(addr_config_0, val_config_0);
  
}
*/
/*
void vsense_config_watermark_mode(vsense_type_rail rail)
{
  uint32 addr_config_0 ;
  uint32 val_config_0 = 0;
     
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   return;
  }
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  vsense_disable_and_reset(rail);
  vsense_config_common(rail);
  
  val_config_0 = inpdw(addr_config_0);
  val_config_0 &= ~HWIO_FMSK(TCSR_CX_VSENS_CONFIG_0, MODE_SEL);
  val_config_0 |= HWIO_FVAL(TCSR_CX_VSENS_CONFIG_0, MODE_SEL, VSENSE_TYPE_WATERMARK_MODE);

  outpdw(addr_config_0, val_config_0);
  
  vsense_en(rail, TRUE);
  vsense_start_capture(rail, TRUE); 
  vsense_watermark_mode_start_timestamp = time_service_now();
   
}
*/
/*
void vsense_check_watermark_duration(void)
{
  uint8 rail_count;
  static uint8* fifo_array = NULL;
  uint64 current_time = time_service_now();

  //we have expired the duration
  if ( vsense_dal_data->duration_us < vsense_convert_timetick_to_time(current_time - 
                                     vsense_watermark_mode_start_timestamp))
  {
    //stop the capture and log the data
    for( rail_count = 0; rail_count < VSENSE_TYPE_RAIL_APC0; rail_count++ )
    {
      if(vsense_rail_and_fuse_info.rail_info[rail_count].is_supported)
      {
        vsense_start_capture(vsense_rail_and_fuse_info.rail_info[rail_count].rail, FALSE);
        fifo_array = vsense_rail_and_fuse_info.rail_info[rail_count].fifo_data;  
        vsense_read_fifo(vsense_rail_and_fuse_info.rail_info[rail_count].rail,
                         fifo_array );
        
        SWEVENT(VSENSE_EVENT_FIFO, fifo_array[63], fifo_array[62], fifo_array[61], fifo_array[60]);
        vsense_config_watermark_mode(vsense_rail_and_fuse_info.rail_info[rail_count].rail);
      }
    }

  }
  
}
*/
/*
void vsense_disable_alarms(vsense_type_rail rail)
{
  uint32 val_config_1 ;
  uint32 addr_config_1 = 0;
 

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
 
  val_config_1 = inpdw(addr_config_1);

  val_config_1 &= ~HWIO_FMSK(TCSR_CX_VSENS_CONFIG_1, EN_ALARM);
  val_config_1 &= ~HWIO_FMSK(TCSR_CX_VSENS_CONFIG_1, ALARM_SLOPE_ENABLE);

  outpdw(addr_config_1, val_config_1);
  
}
*/

/*
void vsense_FUNC_EN(vsense_type_rail rail,boolean FUNC_EN)
{
  uint32 val_config_1;
  uint32 addr_config_1; 

  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  val_config_1 = inpdw(addr_config_1);
   
  if(FUNC_EN == TRUE)
  {
    val_config_1 |= HWIO_FVAL(TCSR_CX_VSENS_CONFIG_1, FUNC_EN, 1);
  }
  else
  {
    val_config_1 &= ~HWIO_FMSK(TCSR_CX_VSENS_CONFIG_1, FUNC_EN);
  }
  outpdw(addr_config_1, val_config_1);

}


uint8 vsense_calibrate_rail(vsense_type_rail rail)
{

  uint8 read_fifo_count ;
  uint8 fifo_array[64];
  uint32 fifo_log_loop;
  uint32* fifo_ptr = NULL;
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   return 0;
  }
  
  
  //configure the common settings
  vsense_config_common(rail);
  vsense_disable_alarms(rail);
  for(read_fifo_count = 0; read_fifo_count < 200; read_fifo_count++)
  {
    vsense_FUNC_EN(rail, TRUE); 
    //set the en and start capture 
    vsense_start_capture(rail, TRUE);
   //read the fifo buffer
    vsense_read_fifo(rail,fifo_array);
    fifo_ptr = (uint32*)fifo_array;       
        fifo_ptr+= 16; 
        fifo_ptr--;
        for(fifo_log_loop =0; fifo_log_loop < 4 ; fifo_log_loop++)
        {
          SWEVENT(VSENSE_EVENT_FIFO, *(fifo_ptr - 0), *(fifo_ptr - 1), *(fifo_ptr - 2), *(fifo_ptr - 3));
          fifo_ptr = fifo_ptr - 4;
        }
    //dog_kick(); 
    vsense_FUNC_EN(rail,FALSE);
    vsense_start_capture(rail, FALSE);
    vsense_reset_and_cgc_clk_disable(rail);
    vsense_unreset_and_cgc_clk_enable(rail);
  }
 
  return 0; 
  
}
*/


