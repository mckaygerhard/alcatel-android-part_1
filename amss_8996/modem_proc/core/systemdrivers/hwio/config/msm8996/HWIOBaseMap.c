
/*
===========================================================================
*/
/**
  @file HWIOBaseMap.c
  @brief Auto-generated HWIO Device Configuration base file.

  DESCRIPTION:
    This file contains Device Configuration data structures for mapping
    physical and virtual memory for HWIO blocks.
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hwio/config/msm8996/HWIOBaseMap.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

  ===========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "DalHWIO.h"


/*=========================================================================
      Data Definitions
==========================================================================*/

static HWIOModuleType HWIOModules_XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB[] =
{
  { "XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB",      0x00000000, 0x00000400 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SPDM_WRAPPER_TOP[] =
{
  { "SPDM_SPDM_CREG",                              0x00000000, 0x00000120 },
  { "SPDM_SPDM_OLEM",                              0x00001000, 0x0000015c },
  { "SPDM_SPDM_RTEM",                              0x00002000, 0x00000318 },
  { "SPDM_APU0132_1",                              0x00003000, 0x00000280 },
  { "SPDM_SPDM_SREG",                              0x00004000, 0x00000120 },
  { "SPDM_VMIDMT_IDX_1_SSD0",                      0x00005000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_RPM_SS_MSG_RAM_START_ADDRESS[] =
{
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SECURITY_CONTROL[] =
{
  { "SECURITY_CONTROL_CORE",                       0x00000000, 0x00007000 },
  { "SECURE_CHANNEL",                              0x00008000, 0x00004200 },
  { "KEY_CTRL",                                    0x00008000, 0x00004000 },
  { "CRI_CM",                                      0x0000c000, 0x00000100 },
  { "CRI_CM_EXT",                                  0x0000c100, 0x00000100 },
  { "SEC_CTRL_APU_APU1132_37",                     0x0000e000, 0x00001480 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PRNG_CFG_PRNG_TOP[] =
{
  { "PRNG_CFG_CM_CM_PRNG_CM",                      0x00000000, 0x00001000 },
  { "PRNG_CFG_TZ_TZ_PRNG_TZ",                      0x00001000, 0x00001000 },
  { "PRNG_CFG_MSA_MSA_PRNG_SUB",                   0x00002000, 0x00001000 },
  { "PRNG_CFG_EE2_EE2_PRNG_SUB",                   0x00003000, 0x00001000 },
  { "PRNG_CFG_EE3_EE3_PRNG_SUB",                   0x00004000, 0x00001000 },
  { "PRNG_CFG_EE4_EE4_PRNG_SUB",                   0x00005000, 0x00001000 },
  { "PRNG_CFG_EE5_EE5_PRNG_SUB",                   0x00006000, 0x00001000 },
  { "PRNG_CFG_EE6_EE6_PRNG_SUB",                   0x00007000, 0x00001000 },
  { "PRNG_CFG_EE7_EE7_PRNG_SUB",                   0x00008000, 0x00001000 },
  { "PRNG_CFG_EE8_EE8_PRNG_SUB",                   0x00009000, 0x00001000 },
  { "PRNG_CFG_EE9_EE9_PRNG_SUB",                   0x0000a000, 0x00001000 },
  { "PRNG_CFG_EE10_EE10_PRNG_SUB",                 0x0000b000, 0x00001000 },
  { "PRNG_CFG_EE11_EE11_PRNG_SUB",                 0x0000c000, 0x00001000 },
  { "PRNG_CFG_EE12_EE12_PRNG_SUB",                 0x0000d000, 0x00001000 },
  { "PRNG_CFG_EE13_EE13_PRNG_SUB",                 0x0000e000, 0x00001000 },
  { "PRNG_CFG_EE14_EE14_PRNG_SUB",                 0x0000f000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_BOOT_ROM[] =
{
  { "BOOT_ROM_MPU1032_4_M17L10_AHB",               0x000ff000, 0x00000400 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_RPM[] =
{
  { "RPM_DEC",                                     0x00080000, 0x00002000 },
  { "RPM_QTMR_AC",                                 0x00082000, 0x00001000 },
  { "RPM_F0_QTMR_V1_F0",                           0x00083000, 0x00001000 },
  { "RPM_F1_QTMR_V1_F1",                           0x00084000, 0x00001000 },
  { "RPM_MSTR_MPU",                                0x00089000, 0x00000c00 },
  { "RPM_VMIDMT",                                  0x00088000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CLK_CTL[] =
{
  { "GCC_CLK_CTL_REG",                             0x00000000, 0x00090000 },
  { "GCC_RPU_RPU0032_144_L12",                     0x00090000, 0x00004a00 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_BIMC[] =
{
  { "BIMC_GLOBAL0",                                0x00000000, 0x00001000 },
  { "BIMC_GLOBAL1",                                0x00001000, 0x00001000 },
  { "BIMC_GLOBAL2",                                0x00002000, 0x00001000 },
  { "BIMC_CONFIG_APU",                             0x00004000, 0x00000a00 },
  { "BIMC_PERFMON",                                0x00003000, 0x00001000 },
  { "BIMC_DTE",                                    0x00006000, 0x0000005c },
  { "BIMC_M_APP_MPORT",                            0x00008000, 0x00001000 },
  { "BIMC_M_GPU_MPORT",                            0x0000c000, 0x00001000 },
  { "BIMC_M_MMSS_MPORT",                           0x00010000, 0x00001000 },
  { "BIMC_M_SYS_MPORT",                            0x00014000, 0x00001000 },
  { "BIMC_M_PIMEM_MPORT",                          0x00018000, 0x00001000 },
  { "BIMC_M_MDSP_MPORT",                           0x0001c000, 0x00001000 },
  { "BIMC_M_APP_PROF",                             0x00009000, 0x00001000 },
  { "BIMC_M_GPU_PROF",                             0x0000d000, 0x00001000 },
  { "BIMC_M_MMSS_PROF",                            0x00011000, 0x00001000 },
  { "BIMC_M_SYS_PROF",                             0x00015000, 0x00001000 },
  { "BIMC_M_PIMEM_PROF",                           0x00019000, 0x00001000 },
  { "BIMC_M_MDSP_PROF",                            0x0001d000, 0x00001000 },
  { "BIMC_S_DDR0_SCMO",                            0x00030000, 0x00001000 },
  { "BIMC_S_DDR1_SCMO",                            0x0003c000, 0x00001000 },
  { "BIMC_S_APP_SWAY",                             0x00048000, 0x00001000 },
  { "BIMC_S_SYS0_SWAY",                            0x00050000, 0x00001000 },
  { "BIMC_S_SYS1_SWAY",                            0x00058000, 0x00001000 },
  { "BIMC_S_DEFAULT_SWAY",                         0x00060000, 0x00001000 },
  { "BIMC_S_DDR0_ARB",                             0x00031000, 0x00001000 },
  { "BIMC_S_DDR1_ARB",                             0x0003d000, 0x00001000 },
  { "BIMC_S_APP_ARB",                              0x00049000, 0x00001000 },
  { "BIMC_S_SYS0_ARB",                             0x00051000, 0x00001000 },
  { "BIMC_S_SYS1_ARB",                             0x00059000, 0x00001000 },
  { "BIMC_S_DEFAULT_ARB",                          0x00061000, 0x00001000 },
  { "BIMC_S_DDR0",                                 0x00032000, 0x00000e80 },
  { "BIMC_S_DDR0_DPE",                             0x00034000, 0x00001000 },
  { "BIMC_S_DDR0_SHKE",                            0x00035000, 0x00001000 },
  { "BIMC_S_DDR0_DTTS_CFG",                        0x00036000, 0x00001000 },
  { "BIMC_S_DDR0_DTTS_SRAM",                       0x00037000, 0x00004000 },
  { "BIMC_S_DDR1",                                 0x0003e000, 0x00000e80 },
  { "BIMC_S_DDR1_DPE",                             0x00040000, 0x00001000 },
  { "BIMC_S_DDR1_SHKE",                            0x00041000, 0x00001000 },
  { "BIMC_S_DDR1_DTTS_CFG",                        0x00042000, 0x00001000 },
  { "BIMC_S_DDR1_DTTS_SRAM",                       0x00043000, 0x00004000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MPM2_MPM[] =
{
  { "MPM2_MPM",                                    0x00000000, 0x00001000 },
  { "MPM2_G_CTRL_CNTR",                            0x00001000, 0x00001000 },
  { "MPM2_G_RD_CNTR",                              0x00002000, 0x00001000 },
  { "MPM2_SLP_CNTR",                               0x00003000, 0x00001000 },
  { "MPM2_QTIMR_AC",                               0x00004000, 0x00001000 },
  { "MPM2_QTIMR_V1",                               0x00005000, 0x00001000 },
  { "MPM2_TSYNC",                                  0x00006000, 0x00001000 },
  { "MPM2_APU",                                    0x00007000, 0x00000880 },
  { "MPM2_TSENS0",                                 0x00008000, 0x00001000 },
  { "MPM2_TSENS0_TSENS0_TM",                       0x00009000, 0x00001000 },
  { "MPM2_WDOG",                                   0x0000a000, 0x00000020 },
  { "MPM2_PSHOLD",                                 0x0000b000, 0x00001000 },
  { "MPM2_TSENS1",                                 0x0000c000, 0x00001000 },
  { "MPM2_TSENS1_TSENS1_TM",                       0x0000d000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CONFIG_NOC[] =
{
  { "CONFIG_NOC",                                  0x00000000, 0x00000080 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SYSTEM_NOC[] =
{
  { "SYSTEM_NOC",                                  0x00000000, 0x0000a100 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_A2_NOC_AGGRE2_NOC[] =
{
  { "A2_NOC_AGGRE2_NOC",                           0x00000000, 0x00008100 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CRYPTO0_CRYPTO_TOP[] =
{
  { "CRYPTO0_CRYPTO",                              0x0003a000, 0x00006000 },
  { "CRYPTO0_CRYPTO_BAM",                          0x00004000, 0x00024000 },
  { "CRYPTO0_CRYPTO_BAM_XPU2_BAM",                 0x00002000, 0x00002000 },
  { "CRYPTO0_CRYPTO_BAM_VMIDMT_BAM",               0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_IPA_0_IPA_WRAPPER[] =
{
  { "IPA_0_BAM_NDP",                               0x00000000, 0x0002b000 },
  { "IPA_0_BAM",                                   0x00004000, 0x00027000 },
  { "IPA_0_XPU2",                                  0x00002000, 0x00002000 },
  { "IPA_0_IPA_VMIDMT",                            0x00030000, 0x00001000 },
  { "IPA_0_IPA",                                   0x00040000, 0x00010000 },
  { "IPA_0_IPA_UC",                                0x00050000, 0x00014000 },
  { "IPA_0_IPA_UC_IPA_UC_RAM",                     0x00050000, 0x00008000 },
  { "IPA_0_IPA_UC_IPA_UC_PER",                     0x00060000, 0x00002000 },
  { "IPA_0_IPA_UC_IPA_UC_MBOX",                    0x00062000, 0x00002000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CORE_TOP_CSR[] =
{
  { "TCSR_MUTEX_RPU1132_64_L12",                   0x00000000, 0x00002200 },
  { "TCSR_TCSR_MUTEX",                             0x00040000, 0x00040000 },
  { "TCSR_REGS_RPU1132_32_L12",                    0x00080000, 0x00001200 },
  { "TCSR_TCSR_REGS",                              0x000a0000, 0x00020000 },
  { "TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW",     0x000b9000, 0x00000080 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_TLMM[] =
{
  { "TLMM_APU1032_175",                            0x00000000, 0x00005980 },
  { "TLMM_CSR",                                    0x00010000, 0x00300000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MODEM_TOP[] =
{
  { "MPSS_TOP",                                    0x00180000, 0x00080000 },
  { "MPSS_RMB",                                    0x00180000, 0x00010000 },
  { "MSS_UIM0_UART_DM",                            0x00190000, 0x00000200 },
  { "MSS_UIM1_UART_DM",                            0x00194000, 0x00000200 },
  { "MSS_UIM2_UART_DM",                            0x00198000, 0x00000200 },
  { "MSS_UIM3_UART_DM",                            0x0019c000, 0x00000200 },
  { "MSS_CXM_UART_DM",                             0x001a0000, 0x00000204 },
  { "MSS_CPR3",                                    0x001a4000, 0x00004000 },
  { "MPSS_PERPH",                                  0x001a8000, 0x00002120 },
  { "MSS_CONF_BUS_TIMEOUT",                        0x001b0000, 0x00001000 },
  { "MSS_MGPI",                                    0x001b2000, 0x00000128 },
  { "MSS_MVC",                                     0x001b3000, 0x00000048 },
  { "MSS_STMR",                                    0x001b4000, 0x000000fd },
  { "MSS_RFC",                                     0x001b8000, 0x00008000 },
  { "MSS_PDMEM",                                   0x001b8000, 0x00006000 },
  { "MSS_RFC_SWI",                                 0x001bfe00, 0x00000200 },
  { "MSS_CRYPTO_TOP",                              0x001c0000, 0x00040000 },
  { "MSS_CRYPTO",                                  0x001fa000, 0x00006000 },
  { "MSS_CRYPTO_BAM",                              0x001c4000, 0x00024000 },
  { "MSS_NAV",                                     0x00200000, 0x000f888d },
  { "MSS_DANGER",                                  0x00300000, 0x00001000 },
  { "MSS_DGR_CRP_CSR",                             0x00300000, 0x00000800 },
  { "MSS_DGR_CSR",                                 0x00300800, 0x00000800 },
  { "MSS_QDSP6SS_PUB",                             0x00080000, 0x00004040 },
  { "MSS_QDSP6SS_PRIVATE",                         0x00100000, 0x00080000 },
  { "MSS_QDSP6SS_CSR",                             0x00100000, 0x00008028 },
  { "MSS_QDSP6SS_L2VIC",                           0x00110000, 0x00001000 },
  { "MSS_QDSP6SS_QDSP6SS_QTMR_AC",                 0x00120000, 0x00001000 },
  { "MSS_QDSP6SS_QTMR_F0_0",                       0x00121000, 0x00001000 },
  { "MSS_QDSP6SS_QTMR_F1_1",                       0x00122000, 0x00001000 },
  { "MSS_QDSP6SS_QTMR_F2_2",                       0x00123000, 0x00001000 },
  { "MSS_QDSP6SS_QDSP6SS_SAW3_QDSP6V55SS_WRAPPER", 0x00130000, 0x00001000 },
  { "MSS_QDSP6SS_SAW3",                            0x00130000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_QDSS_QDSS_ISTARI[] =
{
  { "QDSS_DAPROM",                                 0x00000000, 0x00001000 },
  { "QDSS_QDSSCSR",                                0x00001000, 0x00001000 },
  { "QDSS_CXSTM_8_32_32_TRUE",                     0x00002000, 0x00001000 },
  { "QDSS_TPDA_TPDA_TPDA_S8_W64_D8_M64_CSF9D1CB23", 0x00003000, 0x00001000 },
  { "QDSS_TPDM_TPDM_TPDM_ATB64_ATCLK_DSB256_CS319DD72A", 0x00004000, 0x00001000 },
  { "QDSS_CTI0_CTI0_CSCTI",                        0x00010000, 0x00001000 },
  { "QDSS_CTI1_CTI1_CSCTI",                        0x00011000, 0x00001000 },
  { "QDSS_CTI2_CTI2_CSCTI",                        0x00012000, 0x00001000 },
  { "QDSS_CTI3_CTI3_CSCTI",                        0x00013000, 0x00001000 },
  { "QDSS_CTI4_CTI4_CSCTI",                        0x00014000, 0x00001000 },
  { "QDSS_CTI5_CTI5_CSCTI",                        0x00015000, 0x00001000 },
  { "QDSS_CTI6_CTI6_CSCTI",                        0x00016000, 0x00001000 },
  { "QDSS_CTI7_CTI7_CSCTI",                        0x00017000, 0x00001000 },
  { "QDSS_CTI8_CTI8_CSCTI",                        0x00018000, 0x00001000 },
  { "QDSS_CTI9_CTI9_CSCTI",                        0x00019000, 0x00001000 },
  { "QDSS_CTI10_CTI10_CSCTI",                      0x0001a000, 0x00001000 },
  { "QDSS_CTI11_CTI11_CSCTI",                      0x0001b000, 0x00001000 },
  { "QDSS_CTI12_CTI12_CSCTI",                      0x0001c000, 0x00001000 },
  { "QDSS_CTI13_CTI13_CSCTI",                      0x0001d000, 0x00001000 },
  { "QDSS_CTI14_CTI14_CSCTI",                      0x0001e000, 0x00001000 },
  { "QDSS_CSTPIU_CSTPIU_CSTPIU",                   0x00020000, 0x00001000 },
  { "QDSS_IN_FUN0_IN_FUN0_CXATBFUNNEL_128W8SP",    0x00021000, 0x00001000 },
  { "QDSS_IN_FUN1_IN_FUN1_CXATBFUNNEL_128W8SP",    0x00022000, 0x00001000 },
  { "QDSS_IN_FUN2_IN_FUN2_CXATBFUNNEL_128W8SP",    0x00023000, 0x00001000 },
  { "QDSS_IN_FUN3_IN_FUN3_CXATBFUNNEL_128W8SP",    0x00024000, 0x00001000 },
  { "QDSS_MERG_FUN_MERG_FUN_CXATBFUNNEL_128W4SP",  0x00025000, 0x00001000 },
  { "QDSS_REPL64_REPL64_CXATBREPLICATOR_64WP",     0x00026000, 0x00001000 },
  { "QDSS_ETFETB_ETFETB_CXTMC_F128W64K",           0x00027000, 0x00001000 },
  { "QDSS_ETR_ETR_CXTMC_R64W32D",                  0x00028000, 0x00001000 },
  { "QDSS_SSC_CTI_0_SSC_CTI_0_CSCTI",              0x00030000, 0x00001000 },
  { "QDSS_SSC_CTI_1_SSC_CTI_1_CSCTI",              0x00031000, 0x00001000 },
  { "QDSS_SSC_STM_SSC_STM_CXSTM_2_4_4_TRUE",       0x00033000, 0x00001000 },
  { "QDSS_SSC_FUN0_SSC_FUN0_CXATBFUNNEL_32W2SP",   0x00034000, 0x00001000 },
  { "QDSS_SSC_ETFETB_SSC_ETFETB_CXTMC_F32W8K",     0x00035000, 0x00001000 },
  { "QDSS_VSENSE_CONTROLLER",                      0x00038000, 0x00001000 },
  { "QDSS_MSS_QDSP6_CTI_MSS_QDSP6_CTI_CSCTI",      0x00040000, 0x00001000 },
  { "QDSS_QDSP6_CTI_QDSP6_CTI_CSCTI",              0x00044000, 0x00001000 },
  { "QDSS_RPM_M3_CTI_RPM_M3_CTI_CSCTI",            0x00048000, 0x00001000 },
  { "QDSS_PRNG_TPDM_PRNG_TPDM_TPDM_ATB32_APCLK_CMB32_CS5CFE4153", 0x0004c000, 0x00001000 },
  { "QDSS_PIMEM_TPDM_PIMEM_TPDM_TPDM_ATB64_APCLK_GPRCLK_BC8_TC2_CMB64_DSB64_CS4528A7E6", 0x00050000, 0x00001000 },
  { "QDSS_DCC_TPDM_DCC_TPDM_TPDM_ATB8_ATCLK_CMB32_CSDED7E5A1", 0x00054000, 0x00001000 },
  { "QDSS_ISTARI_APB2JTAG",                        0x00058000, 0x00004000 },
  { "QDSS_VMIDDAP_VMIDDAP_VMIDMT_IDX_2_SSD1",      0x00076000, 0x00001000 },
  { "QDSS_VMIDETR_VMIDETR_VMIDMT_IDX_2_SSD1",      0x00077000, 0x00001000 },
  { "QDSS_NDPBAM_NDPBAM_BAM_NDP_TOP_AUTO_SCALE_V2_0", 0x00080000, 0x00019000 },
  { "QDSS_NDPBAM_BAM",                             0x00084000, 0x00015000 },
  { "QDSS_ISDB_CTI_ISDB_CTI_CSCTI",                0x00141000, 0x00001000 },
  { "QDSS_GPMU_CTI_GPMU_CTI_CSCTI",                0x00142000, 0x00001000 },
  { "QDSS_ARM9_CTI_ARM9_CTI_CSCTI",                0x00180000, 0x00001000 },
  { "QDSS_VENUS_ARM9_ETM_VENUS_ARM9_ETM_A9ETM_V2", 0x00181000, 0x00001000 },
  { "QDSS_MMSS_FUNNEL_MMSS_FUNNEL_CXATBFUNNEL_64W8SP", 0x00184000, 0x00001000 },
  { "QDSS_MMSS_DSAT_TPDM_MMSS_DSAT_TPDM_TPDM_ATB32_APCLK_GPRCLK_BC32_TC3_DSB128_CS7D2CD278", 0x00185000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PMIC_ARB[] =
{
  { "SPMI_CFG_TOP",                                0x00000000, 0x0000d000 },
  { "SPMI_GENI_CFG",                               0x0000a000, 0x00000700 },
  { "SPMI_CFG",                                    0x0000a700, 0x00001a00 },
  { "SPMI_PIC",                                    0x01800000, 0x00200000 },
  { "PMIC_ARB_MPU1132_18_M25L12_AHB",              0x0000e000, 0x00000b00 },
  { "PMIC_ARB_CORE",                               0x0000f000, 0x00001000 },
  { "PMIC_ARB_CORE_REGISTERS",                     0x00400000, 0x00800000 },
  { "PMIC_ARB_CORE_REGISTERS_OBS",                 0x00c00000, 0x00800000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PERIPH_SS[] =
{
  { "PERIPH_SS_PDM_PERPH_WEB",                     0x00000000, 0x00004000 },
  { "PERIPH_SS_PDM_WEB_TCXO4",                     0x00000000, 0x00004000 },
  { "PERIPH_SS_BUS_TIMEOUT_0_BUS_TIMEOUT",         0x00004000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_1_BUS_TIMEOUT",         0x00005000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_2_BUS_TIMEOUT",         0x00006000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_3_BUS_TIMEOUT",         0x00007000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_4_BUS_TIMEOUT",         0x00008000, 0x00001000 },
  { "PERIPH_SS_MPU_MPU1132A_4_M15L7_AHB",          0x0000f000, 0x00000400 },
  { "PERIPH_SS_USB_PHYS_AHB2PHY",                  0x00010000, 0x00008000 },
  { "PERIPH_SS_AHB2PHY_BROADCAST_SWMAN",           0x00017000, 0x00000400 },
  { "PERIPH_SS_AHB2PHY_SWMAN",                     0x00016000, 0x00000400 },
  { "PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_SEC_CM_QUSB2_LQ_1EX", 0x00012000, 0x00000180 },
  { "PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PRIM_CM_QUSB2_LQ_1EX", 0x00011000, 0x00000180 },
  { "PERIPH_SS_USB3PHY_USB3PHY_CM_USB3_SW",        0x00010000, 0x00000800 },
  { "PERIPH_SS_USB3PHY_QSERDES_COM_QSERDES_COM_USB3_QSRV_COM", 0x00010000, 0x000001c4 },
  { "PERIPH_SS_USB3PHY_QSERDES_TX_QSERDES_TX_USB3_TX_MPHY", 0x00010200, 0x00000130 },
  { "PERIPH_SS_USB3PHY_QSERDES_RX_QSERDES_RX_USB3_RX_MPHY", 0x00010400, 0x00000200 },
  { "PERIPH_SS_USB3PHY_USB3_PHY_USB3_PHY_USB3_PCS", 0x00010600, 0x000001a8 },
  { "PERIPH_SS_QSPI_QSPI",                         0x00018000, 0x00000100 },
  { "PERIPH_SS_SDC1_SDCC5_TOP",                    0x00040000, 0x00026800 },
  { "PERIPH_SS_SDC1_SDCC_ICE",                     0x00043000, 0x00008000 },
  { "PERIPH_SS_SDC1_SDCC_ICE_REGS",                0x00043000, 0x00002000 },
  { "PERIPH_SS_SDC1_SDCC_ICE_LUT_KEYS",            0x00045000, 0x00001000 },
  { "PERIPH_SS_SDC1_SDCC_ICE_XPU2",                0x00046000, 0x00000380 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5",                   0x00064000, 0x00000800 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5_HC",                0x00064900, 0x00000500 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5_HC_CMDQ",           0x00064e00, 0x00000200 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5_HC_SCM",            0x00065000, 0x00000100 },
  { "PERIPH_SS_SDC2_SDCC5_TOP",                    0x00080000, 0x00026800 },
  { "PERIPH_SS_SDC2_SDCC_SDCC5",                   0x000a4000, 0x00000800 },
  { "PERIPH_SS_SDC2_SDCC_SDCC5_HC",                0x000a4900, 0x00000500 },
  { "PERIPH_SS_SDC2_SDCC_SDCC5_HC_SCM",            0x000a5000, 0x00000100 },
  { "PERIPH_SS_SDC4_SDCC5_TOP",                    0x00100000, 0x00026800 },
  { "PERIPH_SS_SDC4_SDCC_SDCC5",                   0x00124000, 0x00000800 },
  { "PERIPH_SS_SDC4_SDCC_SDCC5_HC",                0x00124900, 0x00000500 },
  { "PERIPH_SS_SDC4_SDCC_SDCC5_HC_SCM",            0x00125000, 0x00000100 },
  { "PERIPH_SS_BLSP1_BLSP",                        0x00140000, 0x00040000 },
  { "PERIPH_SS_BLSP1_BLSP_BAM",                    0x00144000, 0x0002b000 },
  { "PERIPH_SS_BLSP1_BLSP_BAM_XPU2",               0x00142000, 0x00002000 },
  { "PERIPH_SS_BLSP1_BLSP_BAM_VMIDMT",             0x00140000, 0x00001000 },
  { "PERIPH_SS_BLSP1_BLSP_UART0_UART0_DM",         0x0016f000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART1_UART1_DM",         0x00170000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART2_UART2_DM",         0x00171000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART3_UART3_DM",         0x00172000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART4_UART4_DM",         0x00173000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART5_UART5_DM",         0x00174000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_QUP0",                   0x00175000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP1",                   0x00176000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP2",                   0x00177000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP3",                   0x00178000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP4",                   0x00179000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP5",                   0x0017a000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP",                        0x00180000, 0x00040000 },
  { "PERIPH_SS_BLSP2_BLSP_BAM",                    0x00184000, 0x0002b000 },
  { "PERIPH_SS_BLSP2_BLSP_BAM_XPU2",               0x00182000, 0x00002000 },
  { "PERIPH_SS_BLSP2_BLSP_BAM_VMIDMT",             0x00180000, 0x00001000 },
  { "PERIPH_SS_BLSP2_BLSP_UART0_UART0_DM",         0x001af000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART1_UART1_DM",         0x001b0000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART2_UART2_DM",         0x001b1000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART3_UART3_DM",         0x001b2000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART4_UART4_DM",         0x001b3000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART5_UART5_DM",         0x001b4000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_QUP0",                   0x001b5000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP1",                   0x001b6000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP2",                   0x001b7000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP3",                   0x001b8000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP4",                   0x001b9000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP5",                   0x001ba000, 0x00000600 },
  { "PERIPH_SS_TSIF_TSIF_12SEG_WRAPPER",           0x001c0000, 0x00040000 },
  { "PERIPH_SS_TSIF_TSIF_TSIF_BAM_LITE_TOP_AUTO_SCALE_V2_0", 0x001c0000, 0x00027000 },
  { "PERIPH_SS_TSIF_TSIF_BAM",                     0x001c4000, 0x00023000 },
  { "PERIPH_SS_TSIF_TSIF_0_TSIF_0_TSIF",           0x001e7000, 0x00000200 },
  { "PERIPH_SS_TSIF_TSIF_1_TSIF_1_TSIF",           0x001e8000, 0x00000200 },
  { "PERIPH_SS_TSIF_TSIF_TSIF_TSPP",               0x001e9000, 0x00001000 },
  { "PERIPH_SS_USB1_HS_USB20S",                    0x00200000, 0x00200000 },
  { "PERIPH_SS_USB1_HS_DWC_USB3",                  0x00200000, 0x0000cd00 },
  { "PERIPH_SS_USB1_HS_USB30_QSCRATCH",            0x002f8800, 0x00000400 },
  { "PERIPH_SS_USB1_HS_USB30_QSRAM_REGS",          0x002fc000, 0x00000100 },
  { "PERIPH_SS_USB1_HS_USB30_DBM_REGFILE",         0x002f8000, 0x00000400 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MODEM[] =
{
  { "MODEM_VUIC",                                  0x00000000, 0x00200000 },
  { "VUSS1_VUSS1_TOP",                             0x00020000, 0x00020000 },
  { "VUSS1_VUSS_CSR",                              0x00020000, 0x00000800 },
  { "VUSS1_PMEMSS_CSR",                            0x00020800, 0x00000800 },
  { "VUSS1_VUSS_DMEM_X4X8",                        0x00021000, 0x00004000 },
  { "VUSS2_VUSS2_TOP",                             0x00040000, 0x00020000 },
  { "VUSS2_VUSS_CSR",                              0x00040000, 0x00000800 },
  { "VUSS2_VUSS_DMEM_X4X8",                        0x00041000, 0x00004000 },
  { "VUSS3_VUSS3_TOP",                             0x00060000, 0x00020000 },
  { "VUSS3_VUSS_CSR",                              0x00060000, 0x00000800 },
  { "VUSS3_VUSS_DMEM_X4X8",                        0x00061000, 0x00004000 },
  { "VUSS4_VUSS4_TOP",                             0x00080000, 0x00020000 },
  { "VUSS4_VUSS_CSR",                              0x00080000, 0x00000800 },
  { "VUSS4_PMEMSS_CSR",                            0x00080800, 0x00000800 },
  { "VUSS4_VUSS_DMEM_X4X8",                        0x00081000, 0x00004000 },
  { "VUSS5_VUSS5_TOP",                             0x000a0000, 0x00020000 },
  { "VUSS5_VUSS_CSR",                              0x000a0000, 0x00000800 },
  { "VUSS5_VUSS_DMEM_X4X8",                        0x000a1000, 0x00004000 },
  { "VUSS6_VUSS6_TOP",                             0x000c0000, 0x00020000 },
  { "VUSS6_VUSS_CSR",                              0x000c0000, 0x00000800 },
  { "VUSS6_PMEMSS_CSR",                            0x000c0800, 0x00000800 },
  { "VUSS6_VUSS_DMEM_X4X8",                        0x000c1000, 0x00004000 },
  { "DEMAP_DEMAP_TOP",                             0x000e0000, 0x00020000 },
  { "DEMAP_VUSS_CSR",                              0x000e0000, 0x00000800 },
  { "DEMAP_VUSS_DMEM_X4X8",                        0x000e1000, 0x00004000 },
  { "CCHP_CCHP_TOP",                               0x00100000, 0x00020000 },
  { "CCHP_VUSS_CSR",                               0x00100000, 0x00000800 },
  { "CCHP_PMEMSS_CSR",                             0x00100800, 0x00000800 },
  { "CCHP_VUSS_DMEM_CCHP",                         0x00101000, 0x00008000 },
  { "ENC_ENC_TOP",                                 0x00120000, 0x00020000 },
  { "ENC_VUSS_CSR",                                0x00120000, 0x00000800 },
  { "ENC_VUSS_DMEM_ENC",                           0x00121000, 0x0000d800 },
  { "MPMU_TOP",                                    0x00300000, 0x00100000 },
  { "MODEM_MPMU_CSR",                              0x00300400, 0x00000400 },
  { "MNOC_AXI_BR_CSR",                             0x00400000, 0x00100000 },
  { "DBSS_TOP",                                    0x00600000, 0x00100000 },
  { "CCS",                                         0x00600000, 0x00020000 },
  { "CIPHER_TOP",                                  0x0061fe00, 0x00000200 },
  { "CIPHER",                                      0x0061fe00, 0x00000100 },
  { "ENCRYPT",                                     0x0061ff00, 0x000000fd },
  { "CONTROL",                                     0x0061fc00, 0x00000200 },
  { "CPMEM",                                       0x0061f800, 0x00000400 },
  { "CCS_PDMEM",                                   0x00600000, 0x00010000 },
  { "MTC_TOP",                                     0x00620000, 0x00010000 },
  { "MCDMA",                                       0x00620400, 0x00000400 },
  { "A2",                                          0x00620800, 0x00000400 },
  { "DBG",                                         0x00620c00, 0x00000400 },
  { "MTC_BRDG",                                    0x00621000, 0x00000400 },
  { "A2_MEM",                                      0x00622000, 0x00002000 },
  { "MCDMA_TS_TRIF",                               0x00624000, 0x00000c00 },
  { "TDEC",                                        0x00640000, 0x000000a8 },
  { "TD_CFG_TRIF",                                 0x00641000, 0x00000a00 },
  { "TD_TRIF",                                     0x00642000, 0x00000900 },
  { "TDECIB_MEM",                                  0x00643000, 0x00001000 },
  { "DEMBACK_COMMON",                              0x00660000, 0x00000010 },
  { "DEMBACK_BRDG",                                0x00661000, 0x00000100 },
  { "LTE_DEMBACK",                                 0x00662000, 0x00000050 },
  { "LTE_REENC",                                   0x00663000, 0x00000008 },
  { "UMTS_DEMBACK",                                0x00664000, 0x00000300 },
  { "TDS_DEMBACK",                                 0x00665000, 0x00000100 },
  { "CDMA_DEINT",                                  0x00666000, 0x00000100 },
  { "CDMA_WIDGET",                                 0x00666100, 0x00000100 },
  { "HDR_DEINT",                                   0x00667000, 0x00000100 },
  { "SVD_TBVD",                                    0x00668000, 0x00000300 },
  { "LTE_DEMBACK_SCH_TRIF",                        0x00669000, 0x00000300 },
  { "LTE_DEMBACK_CCH_TRIF",                        0x0066a000, 0x00000c00 },
  { "LTE_REENC_TS_TRIF",                           0x0066b000, 0x00000400 },
  { "W_DBACK_HS_TRIF",                             0x0066c000, 0x00000400 },
  { "W_DBACK_NONHS_TRIF",                          0x0066d000, 0x00000400 },
  { "T_DBACK_TRIF",                                0x0066e000, 0x00000500 },
  { "HDR_DEINT_TS_TRIF",                           0x0066f000, 0x00000300 },
  { "TBVD_CCH_TRIF",                               0x00670000, 0x00000400 },
  { "DB_BUF",                                      0x00674000, 0x00004000 },
  { "TDEC_DB1",                                    0x00680000, 0x000000a8 },
  { "TD_CFG_TRIF_DB1",                             0x00681000, 0x00000a00 },
  { "TD_TRIF_DB1",                                 0x00682000, 0x00000900 },
  { "TDECIB_MEM_DB1",                              0x00683000, 0x00001000 },
  { "DEMBACK_COMMON_DB1",                          0x006a0000, 0x00000010 },
  { "DEMBACK_BRDG_DB1",                            0x006a1000, 0x00000100 },
  { "LTE_DEMBACK_DB1",                             0x006a2000, 0x00000050 },
  { "LTE_REENC_DB1",                               0x006a3000, 0x00000008 },
  { "UMTS_DEMBACK_DB1",                            0x006a4000, 0x00000300 },
  { "TDS_DEMBACK_DB1",                             0x006a5000, 0x00000100 },
  { "CDMA_DEINT_DB1",                              0x006a6000, 0x00000100 },
  { "CDMA_WIDGET_DB1",                             0x006a6100, 0x00000100 },
  { "HDR_DEINT_DB1",                               0x006a7000, 0x00000100 },
  { "SVD_TBVD_DB1",                                0x006a8000, 0x00000300 },
  { "LTE_DEMBACK_SCH_TRIF_DB1",                    0x006a9000, 0x00000300 },
  { "LTE_DEMBACK_CCH_TRIF_DB1",                    0x006aa000, 0x00000c00 },
  { "LTE_REENC_TS_TRIF_DB1",                       0x006ab000, 0x00000400 },
  { "W_DBACK_HS_TRIF_DB1",                         0x006ac000, 0x00000400 },
  { "W_DBACK_NONHS_TRIF_DB1",                      0x006ad000, 0x00000400 },
  { "T_DBACK_TRIF_DB1",                            0x006ae000, 0x00000500 },
  { "HDR_DEINT_TS_TRIF_DB1",                       0x006af000, 0x00000300 },
  { "TBVD_CCH_TRIF_DB1",                           0x006b0000, 0x00000400 },
  { "DB_BUF_DB1",                                  0x006b4000, 0x00004000 },
  { "DTR",                                         0x00700000, 0x00100000 },
  { "MODEM_DTR_ANALOG",                            0x00700000, 0x00008000 },
  { "RXFE_TOP_CFG",                                0x00708000, 0x00008000 },
  { "RXFE_WB_WB0",                                 0x00710000, 0x00008000 },
  { "RXFE_WB_WB1",                                 0x00718000, 0x00008000 },
  { "RXFE_WB_WB2",                                 0x00720000, 0x00008000 },
  { "RXFE_WB_WB3",                                 0x00728000, 0x00008000 },
  { "RXFE_WB_WB4",                                 0x00730000, 0x00008000 },
  { "RXFE_WB_WB5",                                 0x00738000, 0x00008000 },
  { "RXFE_WB_WB6",                                 0x00740000, 0x00008000 },
  { "RXFE_NB_NB0",                                 0x00748000, 0x00008000 },
  { "RXFE_NB_NB1",                                 0x00750000, 0x00008000 },
  { "RXFE_NB_NB2",                                 0x00758000, 0x00008000 },
  { "RXFE_NB_NB3",                                 0x00760000, 0x00008000 },
  { "RXFE_NB_NB4",                                 0x00768000, 0x00008000 },
  { "RXFE_NB_NB5",                                 0x00770000, 0x00008000 },
  { "RXFE_NB_NB6",                                 0x00778000, 0x00008000 },
  { "RXFE_NB_NB7",                                 0x00780000, 0x00008000 },
  { "RXFE_NB_NB8",                                 0x00788000, 0x00008000 },
  { "RXFE_NB_NB9",                                 0x00790000, 0x00008000 },
  { "RXFE_NB_NB10",                                0x00798000, 0x00008000 },
  { "DTR_EVT",                                     0x007a0000, 0x00008000 },
  { "TXFE_SRIF_CHAIN0",                            0x007b8000, 0x00008000 },
  { "TXFE_SRIF_CHAIN1",                            0x007c0000, 0x00008000 },
  { "DTR_BRDG",                                    0x007d0000, 0x00008000 },
  { "MODEM_DTR_DAC_CALIB_0",                       0x007d8000, 0x00001000 },
  { "DAC_REGARRAY_0",                              0x007d9000, 0x00001000 },
  { "MODEM_DTR_DAC_CALIB_1",                       0x007da000, 0x00001000 },
  { "DAC_REGARRAY_1",                              0x007db000, 0x00001000 },
  { "MNOC_AXI_BR_MEM",                             0x00800000, 0x00800000 },
  { NULL, 0, 0 }
};

HWIOPhysRegionType HWIOBaseMap[] =
{
  {
    "XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB",
    (DALSYSMemAddr)0x00030000,
    0x00000400,
    (DALSYSMemAddr)0xe0030000,
    HWIOModules_XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB
  },
  {
    "SPDM_WRAPPER_TOP",
    (DALSYSMemAddr)0x00048000,
    0x00008000,
    (DALSYSMemAddr)0xe0148000,
    HWIOModules_SPDM_WRAPPER_TOP
  },
  {
    "RPM_SS_MSG_RAM_START_ADDRESS",
    (DALSYSMemAddr)0x00068000,
    0x00006000,
    (DALSYSMemAddr)0xe0268000,
    HWIOModules_RPM_SS_MSG_RAM_START_ADDRESS
  },
  {
    "SECURITY_CONTROL",
    (DALSYSMemAddr)0x00070000,
    0x00010000,
    (DALSYSMemAddr)0xe0370000,
    HWIOModules_SECURITY_CONTROL
  },
  {
    "PRNG_CFG_PRNG_TOP",
    (DALSYSMemAddr)0x00080000,
    0x00010000,
    (DALSYSMemAddr)0xe0480000,
    HWIOModules_PRNG_CFG_PRNG_TOP
  },
  {
    "BOOT_ROM",
    (DALSYSMemAddr)0x00100000,
    0x00100000,
    (DALSYSMemAddr)0xe0500000,
    HWIOModules_BOOT_ROM
  },
  {
    "RPM",
    (DALSYSMemAddr)0x00200000,
    0x00100000,
    (DALSYSMemAddr)0xe0600000,
    HWIOModules_RPM
  },
  {
    "CLK_CTL",
    (DALSYSMemAddr)0x00300000,
    0x000a0000,
    (DALSYSMemAddr)0xe0700000,
    HWIOModules_CLK_CTL
  },
  {
    "BIMC",
    (DALSYSMemAddr)0x00400000,
    0x00080000,
    (DALSYSMemAddr)0xe0800000,
    HWIOModules_BIMC
  },
  {
    "MPM2_MPM",
    (DALSYSMemAddr)0x004a0000,
    0x00010000,
    (DALSYSMemAddr)0xe09a0000,
    HWIOModules_MPM2_MPM
  },
  {
    "CONFIG_NOC",
    (DALSYSMemAddr)0x00500000,
    0x00000080,
    (DALSYSMemAddr)0xe0a00000,
    HWIOModules_CONFIG_NOC
  },
  {
    "SYSTEM_NOC",
    (DALSYSMemAddr)0x00520000,
    0x0000a100,
    (DALSYSMemAddr)0xe0b20000,
    HWIOModules_SYSTEM_NOC
  },
  {
    "A2_NOC_AGGRE2_NOC",
    (DALSYSMemAddr)0x00580000,
    0x00008100,
    (DALSYSMemAddr)0xe0c80000,
    HWIOModules_A2_NOC_AGGRE2_NOC
  },
  {
    "CRYPTO0_CRYPTO_TOP",
    (DALSYSMemAddr)0x00640000,
    0x00040000,
    (DALSYSMemAddr)0xe0d40000,
    HWIOModules_CRYPTO0_CRYPTO_TOP
  },
  {
    "IPA_0_IPA_WRAPPER",
    (DALSYSMemAddr)0x00680000,
    0x00080000,
    (DALSYSMemAddr)0xe0e80000,
    HWIOModules_IPA_0_IPA_WRAPPER
  },
  {
    "CORE_TOP_CSR",
    (DALSYSMemAddr)0x00700000,
    0x00100000,
    (DALSYSMemAddr)0xe0f00000,
    HWIOModules_CORE_TOP_CSR
  },
  {
    "TLMM",
    (DALSYSMemAddr)0x01000000,
    0x00310000,
    (DALSYSMemAddr)0xe1000000,
    HWIOModules_TLMM
  },
  {
    "MODEM_TOP",
    (DALSYSMemAddr)0x02000000,
    0x01000000,
    (DALSYSMemAddr)0xec000000,
    HWIOModules_MODEM_TOP
  },
  {
    "QDSS_QDSS_ISTARI",
    (DALSYSMemAddr)0x03000000,
    0x00800000,
    (DALSYSMemAddr)0xe2000000,
    HWIOModules_QDSS_QDSS_ISTARI
  },
  {
    "PMIC_ARB",
    (DALSYSMemAddr)0x04000000,
    0x01c00000,
    (DALSYSMemAddr)0xe3000000,
    HWIOModules_PMIC_ARB
  },
  {
    "PERIPH_SS",
    (DALSYSMemAddr)0x07400000,
    0x00400000,
    (DALSYSMemAddr)0xe5000000,
    HWIOModules_PERIPH_SS
  },
  {
    "MODEM",
    (DALSYSMemAddr)0x10000000,
    0x02000000,
    (DALSYSMemAddr)0xed000000,
    HWIOModules_MODEM
  },
  { NULL, 0, 0, 0, NULL }
};

