/*
==============================================================================

FILE:         HALclkCRYPTO.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   CRYPTO clocks.

   List of clock domains:
     - HAL_clk_mMSSCLKCRYPTOClkDomain


   List of power domains:



==============================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/mss/HALclkCRYPTO.c#1 $

when         who     what, where, why
----------   ---     ----------------------------------------------------------- 
07/09/2014           Auto-generated.


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControl;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mCLKCRYPTOClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mCLKCRYPTOClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_axi_crypto",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_AXI_CRYPTO_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ 0
  },
};


/*
 * HAL_clk_mMSSCLKCRYPTOClkDomain
 *
 * CLKCRYPTO clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSCLKCRYPTOClkDomain =
{
  /* .nCGRAddr             = */ 0, /* this domain does not have a cmd rcgr */
  /* .pmClocks             = */ HAL_clk_mCLKCRYPTOClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mCLKCRYPTOClkDomainClks)/sizeof(HAL_clk_mCLKCRYPTOClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

