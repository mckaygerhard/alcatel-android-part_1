/*
==============================================================================

FILE:         HALclkMSSMain.c

DESCRIPTION:
   The main auto-generated file for MSS.


==============================================================================

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/mss/HALclkMSSMain.c#1 $

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/



/*
 * Clock domains
 */
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSBITCOXMMNDClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSBUSMSSCONFIGClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSQ6ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSCLKXOClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSRBCPRREFClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSUIMClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSUIM0MNDClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSUIM1MNDClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSUIM2MNDClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSUIM3MNDClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSCLKCRYPTOClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSCLKNAVClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMSSCLKSNOCClkDomain;

/*
 * Power domains
 */


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * aMSSSourceMap
 *
 * MSS HW source mapping
 * 
 * NOTES:
 * - HAL_clk_SourceMapType is an array of mapped sources
 *   - see HALclkInternal.h.
 *
 * - If source index is reserved/not used in a clock diagram, please tie that
 *   to HAL_CLK_SOURCE_GROUND.
 *
 * - {HAL_CLK_SOURCE_NULL, HAL_CLK_SOURCE_INDEX_INVALID} is used to indicate
 *   the end of the mapping array. If we reach this element during our lookup,
 *   we'll know we could not find the matching source enum for the register
 *   value, or vice versa.
 * 
 */
static HAL_clk_SourceMapType aMSSSourceMap[] =
{
  { HAL_CLK_SOURCE_XO,                 0 },
  { HAL_CLK_SOURCE_GPLL0_DIV2,         1 },
  { HAL_CLK_SOURCE_GROUND,             2 },
  { HAL_CLK_SOURCE_GROUND,             3 },
  { HAL_CLK_SOURCE_GROUND,             4 },
  { HAL_CLK_SOURCE_GROUND,             5 },
  { HAL_CLK_SOURCE_GROUND,             6 },
  { HAL_CLK_SOURCE_PLLTEST,            7 },
  { HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID }
};


/*
 * HAL_clk_mMSSClockDomainControl
 *
 * Functions for controlling MSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMSSClockDomainControl =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMSSSourceMap
};


/*
 * HAL_clk_mMSSClockDomainControlRO
 *
 * Read-only functions for MSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMSSClockDomainControlRO =
{
   /* .ConfigMux          = */ NULL,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMSSSourceMap
};


/*
 * HAL_clk_aMSSClockDomainDesc
 *
 * List of MSS clock domains
*/
static HAL_clk_ClockDomainDescType * HAL_clk_aMSSClockDomainDesc [] =
{
  &HAL_clk_mMSSBITCOXMMNDClkDomain,
  &HAL_clk_mMSSBUSMSSCONFIGClkDomain,
  &HAL_clk_mMSSQ6ClkDomain,
  &HAL_clk_mMSSCLKXOClkDomain,
  &HAL_clk_mMSSRBCPRREFClkDomain,
  &HAL_clk_mMSSUIMClkDomain,
  &HAL_clk_mMSSUIM0MNDClkDomain,
  &HAL_clk_mMSSUIM1MNDClkDomain,
  &HAL_clk_mMSSUIM2MNDClkDomain,
  &HAL_clk_mMSSUIM3MNDClkDomain,
  &HAL_clk_mMSSCLKCRYPTOClkDomain,
  &HAL_clk_mMSSCLKNAVClkDomain,
  &HAL_clk_mMSSCLKSNOCClkDomain,
  NULL
};


/*
 * HAL_clk_aMSSPowerDomainDesc
 *
 * List of MSS power domains
 */
static HAL_clk_PowerDomainDescType * HAL_clk_aMSSPowerDomainDesc [] =
{
  NULL
};



/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/


/* ===========================================================================
**  HAL_clk_PlatformInitMSSMain
**
** ======================================================================== */

void HAL_clk_PlatformInitMSSMain (void)
{

  /*
   * Install all clock domains
   */
  HAL_clk_InstallClockDomains(HAL_clk_aMSSClockDomainDesc, MODEM_TOP_BASE);

  /*
   * Install all power domains
   */
  HAL_clk_InstallPowerDomains(HAL_clk_aMSSPowerDomainDesc, MODEM_TOP_BASE);

} /* END HAL_clk_PlatformInitMSSMain */

