/*
==============================================================================

FILE:         HALclkBUS.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   BUS clocks.

   List of clock domains:
     - HAL_clk_mMSSBUSMSSCONFIGClkDomain


   List of power domains:



==============================================================================

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/mss/HALclkBUS.c#1 $

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * aConfigBusSourceMap
 *
 * Config Bus HW source mapping
 * 
 * NOTES:
 * - HAL_clk_SourceMapType is an array of mapped sources
 *   - see HALclkInternal.h.
 *
 * - If source index is reserved/not used in a clock diagram, please tie that
 *   to HAL_CLK_SOURCE_GROUND.
 *
 * - {HAL_CLK_SOURCE_NULL, HAL_CLK_SOURCE_INDEX_INVALID} is used to indicate
 *   the end of the mapping array. If we reach this element during our lookup,
 *   we'll know we could not find the matching source enum for the register
 *   value, or vice versa.
 * 
 */
static HAL_clk_SourceMapType aConfigBusSourceMap[] =
{
  {HAL_CLK_SOURCE_XO,          0},
  {HAL_CLK_SOURCE_GPLL0,       1},
  {HAL_CLK_SOURCE_PLLTEST,     7},
  {HAL_CLK_SOURCE_NULL,        HAL_CLK_SOURCE_INDEX_INVALID}
};


/*
 * HAL_clk_mBUSMSSCONFIGClockDomainControl
 *
 * Functions for controlling BUSMSSCONFIG clock domains
 */
static HAL_clk_ClockDomainControlType HAL_clk_mBUSMSSCONFIGClockDomainControl =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aConfigBusSourceMap
}; 


/*                           
 *  HAL_clk_mBUSMSSCONFIGClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mBUSMSSCONFIGClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_src_bus_mss_config",
    /* .mRegisters       = */ { 0, 0, {0, 0} },
    /* .pmControl        = */ NULL,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_SRC_BUS_MSS_CONFIG
  },
  {
    /* .szClockName      = */ "clk_bus_coxm",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_COXM_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_COXM
  },
  {
    /* .szClockName      = */ "clk_bus_crypto",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_CRYPTO_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_CRYPTO
  },
  {
    /* .szClockName      = */ "clk_bus_mgpi",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_MGPI_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_MGPI
  },
  {
    /* .szClockName      = */ "clk_bus_mvc",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_MVC_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_MVC
  },
  {
    /* .szClockName      = */ "clk_bus_nav",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_NAV_CBCR), HWIO_OFFS(MSS_NAV_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_NAV
  },
  {
    /* .szClockName      = */ "clk_bus_rbcpr",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_RBCPR_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_RBCPR
  },
  {
    /* .szClockName      = */ "clk_bus_rfc",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_RFC_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_RFC
  },
  {
    /* .szClockName      = */ "clk_bus_slave_timeout",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_SLAVE_TIMEOUT_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_SLAVE_TIMEOUT
  },
  {
    /* .szClockName      = */ "clk_bus_stmr",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_STMR_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_STMR
  },
  {
    /* .szClockName      = */ "clk_bus_uim0",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_UIM0_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_UIM0
  },
  {
    /* .szClockName      = */ "clk_bus_uim1",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_UIM1_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_UIM1
  },
  {
    /* .szClockName      = */ "clk_bus_uim2",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_UIM2_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_UIM2
  },
  {
    /* .szClockName      = */ "clk_bus_uim3",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_UIM3_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_UIM3
  },
  {
    /* .szClockName      = */ "mss_dgr_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_DGR_AHB_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_MSS_DGR_AHB_CLK
  },
};


/*
 * HAL_clk_mMSSBUSMSSCONFIGClkDomain
 *
 * BUSMSSCONFIG clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSBUSMSSCONFIGClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MSS_BUS_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mBUSMSSCONFIGClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mBUSMSSCONFIGClkDomainClks)/sizeof(HAL_clk_mBUSMSSCONFIGClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mBUSMSSCONFIGClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

