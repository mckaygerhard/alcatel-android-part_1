/*
==============================================================================

FILE:         HALclkUIM.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   UIM clocks.

   List of clock domains:
     - HAL_clk_mMSSUIMClkDomain
     - HAL_clk_mMSSUIM0MNDClkDomain
     - HAL_clk_mMSSUIM1MNDClkDomain
     - HAL_clk_mMSSUIM2MNDClkDomain
     - HAL_clk_mMSSUIM3MNDClkDomain


   List of power domains:



==============================================================================

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/mss/HALclkUIM.c#1 $

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mUIMClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mUIMClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_card_src_uim0",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_CARD_SRC_UIM0_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_CARD_SRC_UIM0
  },
  {
    /* .szClockName      = */ "clk_card_src_uim1",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_CARD_SRC_UIM1_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_CARD_SRC_UIM1
  },
  {
    /* .szClockName      = */ "clk_card_src_uim2",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_CARD_SRC_UIM2_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_CARD_SRC_UIM2
  },
  {
    /* .szClockName      = */ "clk_card_src_uim3",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_CARD_SRC_UIM3_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_CARD_SRC_UIM3
  },
};


/*
 * HAL_clk_mMSSUIMClkDomain
 *
 * UIM clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSUIMClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MSS_UIM_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mUIMClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mUIMClkDomainClks)/sizeof(HAL_clk_mUIMClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mUIM0MNDClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mUIM0MNDClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_uart_bit_uim0",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_UART_BIT_UIM0_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_UART_BIT_UIM0
  },
};


/*
 * HAL_clk_mMSSUIM0MNDClkDomain
 *
 * UIM0MND clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSUIM0MNDClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MSS_UIM0_MND_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mUIM0MNDClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mUIM0MNDClkDomainClks)/sizeof(HAL_clk_mUIM0MNDClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mUIM1MNDClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mUIM1MNDClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_uart_bit_uim1",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_UART_BIT_UIM1_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_UART_BIT_UIM1
  },
};


/*
 * HAL_clk_mMSSUIM1MNDClkDomain
 *
 * UIM1MND clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSUIM1MNDClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MSS_UIM1_MND_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mUIM1MNDClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mUIM1MNDClkDomainClks)/sizeof(HAL_clk_mUIM1MNDClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mUIM2MNDClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mUIM2MNDClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_uart_bit_uim2",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_UART_BIT_UIM2_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_UART_BIT_UIM2
  },
};


/*
 * HAL_clk_mMSSUIM2MNDClkDomain
 *
 * UIM2MND clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSUIM2MNDClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MSS_UIM2_MND_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mUIM2MNDClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mUIM2MNDClkDomainClks)/sizeof(HAL_clk_mUIM2MNDClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mUIM3MNDClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mUIM3MNDClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_uart_bit_uim3",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_UART_BIT_UIM3_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_UART_BIT_UIM3
  },
};


/*
 * HAL_clk_mMSSUIM3MNDClkDomain
 *
 * UIM3MND clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSUIM3MNDClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MSS_UIM3_MND_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mUIM3MNDClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mUIM3MNDClkDomainClks)/sizeof(HAL_clk_mUIM3MNDClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

