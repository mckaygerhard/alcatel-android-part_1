/*
==============================================================================

FILE:         HALclkXO.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   XO clocks.

   List of clock domains:
     - HAL_clk_mMSSCLKXOClkDomain


   List of power domains:



==============================================================================

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/mss/HALclkXO.c#1 $

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mCLKXOClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mCLKXOClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_xo_mdm",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_XO_MDM_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_XO_MDM
  },
  {
    /* .szClockName      = */ "clk_xo_nav",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_XO_NAV_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_XO_NAV
  },
};


/*
 * HAL_clk_mMSSCLKXOClkDomain
 *
 * CLKXO clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSCLKXOClkDomain =
{
  /* .nCGRAddr             = */ 0, /* this domain does not have a cmd rcgr */
  /* .pmClocks             = */ HAL_clk_mCLKXOClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mCLKXOClkDomainClks)/sizeof(HAL_clk_mCLKXOClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

