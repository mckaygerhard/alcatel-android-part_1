/*
==============================================================================

FILE:         HALclkRBCPR.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   RBCPR clocks.

   List of clock domains:
     - HAL_clk_mMSSRBCPRREFClkDomain


   List of power domains:



==============================================================================

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/mss/HALclkRBCPR.c#1 $

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mRBCPRREFClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mRBCPRREFClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_rbcpr_ref",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_RBCPR_REF_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_RBCPR_REF
  },
};


/*
 * HAL_clk_mMSSRBCPRREFClkDomain
 *
 * RBCPRREF clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSRBCPRREFClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MSS_RBCPR_REF_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mRBCPRREFClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mRBCPRREFClkDomainClks)/sizeof(HAL_clk_mRBCPRREFClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

