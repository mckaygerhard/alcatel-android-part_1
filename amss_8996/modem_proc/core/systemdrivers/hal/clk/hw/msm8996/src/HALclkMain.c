/*
==============================================================================

FILE:         HALclkMain.c

DESCRIPTION:
  This file contains the main platform initialization code for the clock
  HAL on the mss processor.


==============================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/HALclkMain.c#1 $

when         who     what, where, why
----------   ---     ----------------------------------------------------------- 
07/09/2014           Auto-generated.


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include "HALclkInternal.h"
#include "HALclkGeneric.h"
#include "HALclkGenericPLL.h"
#include "HALhwio.h"
#include "HALclkHWIO.h"


/* ============================================================================
**    Prototypes
** ==========================================================================*/

void HAL_clk_PlatformInitSources(void);


/* ============================================================================
**    Externs
** ==========================================================================*/


extern void HAL_clk_PlatformInitGCCMain(void);
extern void HAL_clk_PlatformInitMSSMain(void);


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_clk_aInitFuncs
 *
 * Declare array of module initialization functions.
 */
static HAL_clk_InitFuncType HAL_clk_afInitFuncs[] =
{
  /*
   * Sources
   */
  HAL_clk_PlatformInitSources,

  /*
   * GCC
   */
  HAL_clk_PlatformInitGCCMain,

  /*
   * MSS
   */
  HAL_clk_PlatformInitMSSMain,

  NULL
};



/*
 * Declare the base pointers for HWIO access.
 */
uint32 HAL_clk_nHWIOBaseGCC;
uint32 HAL_clk_nHWIOBaseMSS;
uint32 HAL_clk_nHWIOBaseSecurity;


/*
 * HAL_clk_aHWIOBases
 *
 * Declare array of HWIO bases in use on this platform.
 */
static HAL_clk_HWIOBaseType HAL_clk_aHWIOBases[] =
{

  { CLK_CTL_BASE_PHYS,          CLK_CTL_BASE_SIZE,          &HAL_clk_nHWIOBaseGCC      },
  { MODEM_TOP_BASE_PHYS,        MODEM_TOP_BASE_SIZE,        &HAL_clk_nHWIOBaseMSS      },
  { SECURITY_CONTROL_BASE_PHYS, SECURITY_CONTROL_BASE_SIZE, &HAL_clk_nHWIOBaseSecurity },

  { 0, 0, NULL }
};


/*
 * HAL_clk_Platform;
 * Platform data.
 */
HAL_clk_PlatformType HAL_clk_Platform =
{
  HAL_clk_afInitFuncs,
  HAL_clk_aHWIOBases
};


/*
 * GCC PLL contexts
 */
static HAL_clk_PLLContextType HAL_clk_aPLLContextGCCPLL[] =
{
  {
    HWIO_OFFS(GCC_GPLL0_MODE),
    HAL_CLK_FMSK(GCC_MSS_Q6_GPLL_ENA_VOTE, GPLL0),
    HAL_CLK_PLL_SPARK
  },
};


/*
 * MSS PLL contexts
 */
static HAL_clk_PLLContextType HAL_clk_aPLLContextMSSPLL[] =
{

  {
    HWIO_OFFS(MSS_OFFLINE_PLL_MODE),
    {0},
    HAL_CLK_PLL_SPARK
  },
/*
 * We may need to add these back if DTR can't properly manage these resources.
 */
#if 0
  {
    HWIO_OFFS(MSS_MODEM_DTR_PLL1_MODE),
    {0},
    HAL_CLK_PLL_BRAMMO
  },
  {
    HWIO_OFFS(MSS_MODEM_DTR_PLL2_MODE),
    {0},
    HAL_CLK_PLL_BRAMMO
  },
#endif
  {
    HWIO_OFFS(MSS_QDSP6SS_PLL_MODE),
    {0},
    HAL_CLK_PLL_SPARK
  },
  {
    HWIO_OFFS(MSS_NAV_PLL4_PLLMODE),
    {0},
    HAL_CLK_PLL_BRAMMO
  },

};


/* ============================================================================
**    Functions
** ==========================================================================*/


/* ===========================================================================
**  HAL_clk_PlatformInitSources
**
** ======================================================================== */

void HAL_clk_PlatformInitSources (void)
{
  /*
   * Install PLL handlers.
   */
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL0, &HAL_clk_aPLLContextGCCPLL[0], CLK_CTL_BASE);
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MPLL0, &HAL_clk_aPLLContextMSSPLL[0], MODEM_TOP_BASE);
/*
 * We may need to add these back if DTR can't properly manage these resources.
 */
#if 0
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MPLL1, &HAL_clk_aPLLContextMSSPLL[1], MODEM_TOP_BASE);
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MPLL2, &HAL_clk_aPLLContextMSSPLL[2], MODEM_TOP_BASE);
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MPLL3, &HAL_clk_aPLLContextMSSPLL[3], MODEM_TOP_BASE);
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MPLL4, &HAL_clk_aPLLContextMSSPLL[4], MODEM_TOP_BASE);
#else
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MPLL3, &HAL_clk_aPLLContextMSSPLL[1], MODEM_TOP_BASE);
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MPLL4, &HAL_clk_aPLLContextMSSPLL[2], MODEM_TOP_BASE);
#endif

} /* END HAL_clk_PlatformInitSources */



/* ===========================================================================
**  HAL_clk_Save
**
** ======================================================================== */

void HAL_clk_Save (void)
{
  /*
   * Nothing to save.
   */

} /* END HAL_clk_Save */


/* ===========================================================================
**  HAL_clk_Restore
**
** ======================================================================== */

void HAL_clk_Restore (void)
{
  /*
   * Nothing to restore.
   */
  
} /* END HAL_clk_Restore */


