/*
==============================================================================

FILE:         HALclkGenericPLL.c

DESCRIPTION:
  This file contains the generic clock HAL code for the PLL control.
  These PLLs are of the Prius and Tesla PLL variety.

==============================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/v1/HALclkGenericPLL.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------- 
05/30/13   frv     Created

==============================================================================
            Copyright (c) 2013 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/

#include "HALhwio.h"
#include "HALclkGenericPLL.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Definitions
** ==========================================================================*/


/* ============================================================================
**    Data Declarations
** ==========================================================================*/


/* ============================================================================
**    External Definitions
** ==========================================================================*/


extern HAL_clk_SourceControlType HAL_clk_PriusPLLControl;
extern HAL_clk_SourceControlType HAL_clk_SparkPLLControl;


/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/


/* ===========================================================================
**  HAL_clk_InstallPLL
**
** ======================================================================== */

void HAL_clk_InstallPLL
(
  HAL_clk_SourceType      eSource,
  HAL_clk_PLLContextType *pCtxt,
  uint32                  nBaseAddress
)
{
  /*
   * Add base address
   */
  pCtxt->nAddr += nBaseAddress;

  if (pCtxt->VoterRegister.nAddr != 0)
  {
    pCtxt->VoterRegister.nAddr += nBaseAddress;
  }

  /*
   * Install PLL handler
   */
  if((pCtxt->ePLLType == HAL_CLK_PLL_PRIUS) ||
     (pCtxt->ePLLType == HAL_CLK_PLL_TESLA))
  {
    HAL_clk_InstallSource(eSource, &HAL_clk_PriusPLLControl, pCtxt);
  }
  else if ((pCtxt->ePLLType == HAL_CLK_PLL_SPARK) ||
           (pCtxt->ePLLType == HAL_CLK_PLL_BRAMMO))
  {
    HAL_clk_InstallSource(eSource, &HAL_clk_SparkPLLControl, pCtxt);
  }
  else
  {
    // error but no return parameter to provide!
  }

} /* END HAL_clk_InstallPLL */
