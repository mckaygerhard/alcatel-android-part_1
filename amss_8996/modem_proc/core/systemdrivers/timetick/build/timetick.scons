#===============================================================================
#
# DAL Timetick Lib
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2010-2014 Qualcomm Technologies Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/timetick/build/timetick.scons#1 $
#  $DateTime: 2016/03/28 23:02:17 $
#  $Author: mplcsds1 $
#  $Change: 10156097 $
#
#===============================================================================

Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/systemdrivers/timetick/src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'DAL',
   'HAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   
   # needs to be last also contains wrong comdef.h      
   'KERNEL',   
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

env.PublishPrivateApi("SYSTEMDRIVERS_TIMETICK", [
   "${BUILD_ROOT}/core/systemdrivers/timetick/src"
])

if env.has_key('HWIO_IMAGE'):

  env.AddHWIOFile('HWIO', [
    {
      'filename': '${BUILDPATH}/TimetickHWIO.h',

      # Filter to include only the QTMR symbol.
      'filter-include': ['QTMR'],
    }
  ]
)
#-------------------------------------------------------------------------------
# Source Code
#-------------------------------------------------------------------------------

DAL_TIMETICK_SOURCES =  [
   '${BUILDPATH}/DalTimetickInfo.c',
   '${BUILDPATH}/DalTimetick.c',
   '${BUILDPATH}/DalTimetickFwk.c',
   '${BUILDPATH}/timetickLegacy.c',
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------

env.AddLibrary(
   ['CORE_MODEM'],
   '${BUILDPATH}/DALTimetick_modem',
   DAL_TIMETICK_SOURCES)

if 'USES_DEVCFG' in env:
   DEVCFG_IMG = ['DAL_DEVCFG_IMG']
   env.AddDevCfgInfo(DEVCFG_IMG, 
   {
      'soc_xml' : ['${BUILD_ROOT}/core/systemdrivers/timetick/config/Timetick.xml']
   })
   