/*==============================================================================

  D A L   G P I O   I N T E R R U P T   C O N T R O L L E R    

DESCRIPTION
  This file has the direct connect GPIO interrupt support for the 8960 target.

REFERENCES

        Copyright � 2011 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/GPIOInt/config/user/mdm9x45/GPIOInt_ConfigData.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
1/1/2010   aratin  First draft created. 
===========================================================================*/

#include "DALReg.h"
#include "DALDeviceId.h"
#include "DDIGPIOInt.h"
#include "HALgpioint.h"
#include "GPIOIntUser.h"

/*
 * Modem Interrupt Config Map
 */

GPIOIntConfigMapType interrupt_config_map[] =
{
  {GPIOINT_NONE, 73,GPIOINTF_EXTERNAL_CONFIG}, /* Direct connect  0 owned by audio Codec*/
  {GPIOINT_NONE, 74,GPIOINTF_EXTERNAL_CONFIG}, /* Direct connect  1 owned by audio Codec*/
  {GPIOINT_NONE, 75,0}, /* Direct connect  2 owned by audio PD*/ 
};









  
