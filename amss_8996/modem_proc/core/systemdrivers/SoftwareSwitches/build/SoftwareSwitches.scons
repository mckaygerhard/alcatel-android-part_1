#===============================================================================
#
# SOFTWARE SWITCHES
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2014 Qualcomm Technologies Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/SoftwareSwitches/build/SoftwareSwitches.scons#1 $
#
#===============================================================================

Import('env')
env = env.Clone()

#-----------------------------------------------------------------------------
# Define paths
#-----------------------------------------------------------------------------

SRCPATH = "${BUILD_ROOT}/core/systemdrivers/SoftwareSwitches/src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

#-------------------------------------------------------------------------------
# External depends outside CoreBSP
#-------------------------------------------------------------------------------

env.RequireExternalApi([
   'ASIC',
   'DSM',
   'BREW',
   'MODEM_PMIC',
   'MODEM_DATA',
   'MODEM_1X',
   'MODEM_RF',
   'MODEM_SERVICES',
   'MODEM_NAS',
   'MODEM_GPS',
   'MODEM_UIM',
   'MODEM_WCDMA',
   'MODEM_FTM',
   'MODEM_HDR',
   'MODEM_ADIE',
   'MODEM_MDSP',
   'MODEM_GERAN',
   'MODEM_MCS',
   'MULTIMEDIA_AUDIO',
   'MULTIMEDIA_DISPLAY',
   'MULTIMEDIA_CORE',
   'MULTIMEDIA',
   'WCONNECT',
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------

CBSP_API = [
   'DAL',
   'DEBUGTOOLS',
   'SERVICES',
   'SYSTEMDRIVERS',

   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

env.PublishPrivateApi("SYSTEMDRIVERS_SOFTWARESWITCHES", [
   "${INC_ROOT}/core/systemdrivers/SoftwareSwitches/src",
   "${INC_ROOT}/core/systemdrivers/SoftwareSwitches/src/wrapper",
])

#-----------------------------------------------------------------------------
# Define sources
#-----------------------------------------------------------------------------

SWITCH_SOURCES = [
   '${BUILDPATH}/DalSoftwareSwitches.c',
   '${BUILDPATH}/DalSoftwareSwitchesFwk.c',
   '${BUILDPATH}/DalSoftwareSwitchesInfo.c',
   '${BUILDPATH}/wrapper/SoftwareSwitches.c',
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------

env.AddLibrary(
   ['CORE_MODEM'],
   '${BUILDPATH}/DALsoftwareswitches',
   SWITCH_SOURCES)
               
#-------------------------------------------------------------------------------
# Register initialization function and dependencies.
#-------------------------------------------------------------------------------

if 'USES_RCINIT' in env:
  RCINIT_IMG = ['CORE_MODEM']
  env.AddRCInitFunc(              # Code Fragment in TMC: NO
    RCINIT_IMG,                   # define TMC_RCINIT_INIT_SOFTWARESWITCHES_INIT
    {
      'sequence_group' : 'RCINIT_GROUP_0',          # required
      'init_name'      : 'SoftwareSwitches',        # required
      'init_function'  : 'SoftwareSwitches_Init',   # required
      'dependencies'   : ['dalsys'],                # opt [python list]
    })

#-------------------------------------------------------------------------------
# DEVCFG
#-------------------------------------------------------------------------------

if 'USES_DEVCFG' in env:
   DEVCFG_IMG = ['DAL_DEVCFG_IMG']
   env.AddDevCfgInfo(DEVCFG_IMG, 
   {
      'soc_xml' : ['${BUILD_ROOT}/core/systemdrivers/SoftwareSwitches/config/SoftwareSwitches.xml']
   })

