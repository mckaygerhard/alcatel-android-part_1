/*! \file pm_target_information.c
*  
*  \brief This file contains target specific PMIC settings common across processors.
*  \n   
*  &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/pmic/framework/src/pm_target_information.c#1 $ 

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_target_information.h"
#include "DALSys.h"
#include "pm_version.h"
#include "pm_utils.h"
#include "pm_comm.h"
#include "device_info.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

static DALSYS_PROPERTY_HANDLE_DECLARE(hProp_common);
static DALSYS_PROPERTY_HANDLE_DECLARE(hProp_target);
static DALSYSPropertyVar prop_common;                /* Union to retreive property values */
static DALSYSPropertyVar prop_target;                /* Union to retreive property values */

static struct {
	boolean is_pmk_present;
	uint8	pmk_chip_index;
}pmk_info; //store flag and chip index if PMK is detected, get pmic chip index for xo and rtc drivers

extern boolean                      bPMICStub;

void pm_target_information_init(void)
{
	/*
	* Runtime detection to support multiple PMIC configuration on any target.
	* We're only concerned with the primary PMIC here so we use index 0.
	*/
    char* pmic_props = NULL;
    const char* pmic_props_common = "/modem/pmic/common";
    uint8 pm_model = 0;
    uint8 pmic_opt_pin_data = 0;
    uint8 pmk_chip;
	
    pm_err_flag_type err_flag=PM_ERR_FLAG__SUCCESS;

    if (bPMICStub == TRUE)
    {
        pmic_props = "/modem/pm8994_pmi8994";
    }
    else
    {	
        pm_model = pm_get_pmic_model((uint8)0);
        switch(pm_model)
        {
            case PMIC_IS_PMD9645:
                pmic_props = "/modem/pmd9645";
                break;
            case PMIC_IS_PM8994:
                //check option pin
                pm_get_pmic_option_pin_info(0, &pmic_opt_pin_data);

                if((pmic_opt_pin_data & 0x3)  == 0x03)
                {
                     //option pin1 high
                      pmic_props = "/modem/pm8994_pm8004";
                }
                else
                {
                     //option pin1 low
                      pmic_props = "/modem/pm8994_pmi8994";
                }
                break;
            case PMIC_IS_PMD9655:
                pmic_props = "/modem/pmd9645";
                break;

            default:
                break;
        }
    }
	
	if(PM_ERR_FLAG__SUCCESS==(err_flag=pm_is_pmic_supported(PMIC_IS_PMK8001, &pmk_chip))){
		pmk_info.is_pmk_present=TRUE;
		pmk_info.pmk_chip_index=pmk_chip;
	}
	//can add else if coditions in future if pmk targets other than 8001 are available. 
	else
	{
		pmk_info.is_pmk_present=FALSE;
		pmk_info.pmk_chip_index=(PM_MAX_NUM_PMICS+1);// set greater than max num of pmic's 
	}
	
	if(NULL == pmic_props)
	{
		/* 
		* ERR Fatal here as we were unable to discover the PMIC.  No device ID for PMIC -
		* just use 0.
		*/
		DALSYS_LogEvent (0, DALSYS_LOGEVENT_FATAL_ERROR,
		"PMIC:  Initialization failed to identify PMIC device!  PMIC Model listed as: %d.", pm_model);
		return;
	}
	
	if(DAL_SUCCESS != DALSYS_GetDALPropertyHandleStr((const char*)pmic_props, hProp_target))
	{
		DALSYS_LogEvent (0, DALSYS_LOGEVENT_FATAL_ERROR,
		"PMIC:  Initialization failed hProp_target handle retrieval!");
	}
    if(DAL_SUCCESS != DALSYS_GetDALPropertyHandleStr(pmic_props_common, hProp_common))
	{
		DALSYS_LogEvent (0, DALSYS_LOGEVENT_FATAL_ERROR,
        "PMIC:  Initialization failed hProp_common handle retrieval!");
	}
}

boolean pm_is_pmk_present(uint8 *pmic_index){
	if(pmk_info.is_pmk_present==TRUE)
		*pmic_index=pmk_info.pmk_chip_index;

	return pmk_info.is_pmk_present;
}


pm_err_flag_type pm_target_information_read_peripheral_rev(pm_comm_info_type *comm_ptr, peripheral_info_type *peripheral_info)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type base_address = (pm_register_address_type)peripheral_info->base_address;
    uint32 digital_minor_index = 0;
    uint32 digital_major_index = 1;
    uint32 analog_minor_index = 2;
    uint32 analog_major_index = 3;
    uint32 peripheral_type_index = 4;
    uint32 peripheral_subtype_index = 5;
    uint32 peripheral_dummy_index = 6;
    const uint32 num_of_bytes = 7;
    pm_register_data_type temp_peripheral_info[7] = {0};

    if(!comm_ptr)
    {
        return PM_ERR_FLAG__COMM_TYPE_NOT_RECOGNIZED;
    }

    // Reset the peripheral info
    peripheral_info->peripheral_type = 0;
    peripheral_info->peripheral_subtype = 0;
    peripheral_info->analog_major_version = 0;
    peripheral_info->analog_minor_version = 0;
    peripheral_info->digital_major_version = 0;
    peripheral_info->digital_minor_version = 0;

    /* Burst read the peripheral info */
    pm_comm_read_byte_array(comm_ptr->slave_id, base_address, num_of_bytes, temp_peripheral_info, 0);

    /* When we burst read multiple bytes from a non-existent peripheral, the data returned
       should be same on all the bytes so we need to do the below check and return error */
    if(temp_peripheral_info[peripheral_subtype_index] == temp_peripheral_info[peripheral_dummy_index]) 
    {
        return PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }

    /* Valid peripheral type can never be 0 */   
    if(temp_peripheral_info[peripheral_type_index] == 0) 
    {
        return PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    
    peripheral_info->peripheral_type = temp_peripheral_info[peripheral_type_index];    
    peripheral_info->peripheral_subtype = temp_peripheral_info[peripheral_subtype_index];
    peripheral_info->digital_major_version = temp_peripheral_info[digital_major_index];
    peripheral_info->digital_minor_version = temp_peripheral_info[digital_minor_index];
    peripheral_info->analog_major_version = temp_peripheral_info[analog_major_index];
    peripheral_info->analog_minor_version = temp_peripheral_info[analog_minor_index];
    
    return err_flag;
}

void* pm_target_information_get_common_info(const char* prop_id)
{
  void *info = NULL;

  if(DAL_SUCCESS == DALSYS_GetPropertyValue(hProp_common, prop_id, NULL, &prop_common))
  {
      info = (void*)prop_common.Val.pStruct;
  }

  return info;
}

void* pm_target_information_get_specific_info(const char* prop_id)
{
  void *info = NULL;

  if(DAL_SUCCESS == DALSYS_GetPropertyValue(hProp_target, prop_id, NULL, &prop_target))
  {
      info = (void*)prop_target.Val.pStruct;
  }

  return info;
}

uint32 pm_target_information_get_periph_count_info(const char* prop_id , uint8 pmic_index)
{
  uint32 *peripheral_count = NULL;

  if(DAL_SUCCESS == DALSYS_GetPropertyValue(hProp_target, prop_id, NULL, &prop_target)&& 
  	 (pmic_index < PM_MAX_NUM_PMICS) && (pm_get_pmic_model(pmic_index) != PMIC_IS_UNKNOWN))
  {
      peripheral_count = (uint32*)prop_target.Val.pStruct;
      return peripheral_count[pmic_index];
  }

  return 0;
}
