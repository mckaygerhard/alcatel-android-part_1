
/*
===========================================================================
*/
/**
  @file ClockMSS.c 
  
  Main entry point for the MSS clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2011-12 Qualcomm Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/clock/hw/msm8996/mss/src/ClockMSS.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  08/30/13   frv     Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/


#include "DALDeviceId.h"
#include "ClockDriver.h"
#include "ClockMSS.h"
#include "ClockSWEVT.h"
#include <CoreIni.h>
#include "DDIVCS.h"
#include "npa_resource.h"


/*=========================================================================
      Macros
==========================================================================*/


/*
 * Clock ID's
 */
#define CLOCK_ID_CPU                            "clk_q6"
#define CLOCK_ID_MSS_CONFIG_BUS                 "clk_src_bus_mss_config"

#define CLOCK_CPR_REF_CLOCK_RATE_KHZ            19200


/*=========================================================================
      Type Definitions
==========================================================================*/


/*=========================================================================
      Extern Definitions
==========================================================================*/


/*=========================================================================
      Data
==========================================================================*/


static const char *ClockNPANodeName[] =
{
  CLOCK_NPA_NODE_NAME_CONFIG_BUS,
  CLOCK_NPA_NODE_NAME_CPU,
  CLOCK_NPA_NODE_NAME_CPU_TEST,
  CLOCK_NPA_NODE_NAME_PCNOC,
  CLOCK_NPA_NODE_NAME_CXO,
  CLOCK_NPA_NODE_NAME_QDSS,
  CLOCK_NPA_NODE_NAME_SNOC,
  CLOCK_NPA_NODE_NAME_BIMC,
  CLOCK_NPA_NODE_NAME_IPA
};


static ClockImageCtxtType Clock_ImageCtxt;


/* =========================================================================
      Functions
==========================================================================*/


/* =========================================================================
**  Function : Clock_DetectPVSData
** =========================================================================*/
/**
  Detects efuse settings and keeps a handle to the appropriate PVS data.
 
  This function is invoked at driver initialization to detect efuse
  settings to distinguish b/w slow, nominal and fast part.
 
  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if detection fails, other DAL_SUCCESS.

  @dependencies
  None.
*/ 

static DALResult Clock_DetectPVSData
(
  ClockDrvCtxt *pDrvCtxt
)
{

  /*-----------------------------------------------------------------------*/
  /* Nothing needed here - VRegCornerData is part of BSP Config.           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;
}


/* =========================================================================
**  Function : Clock_DetectBSPVersion
** =========================================================================*/
/**
  Detects which BSP configuration to use for the current HW.

  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if a valid configuration was not found, other DAL_SUCCESS.

  @dependencies
  None.
*/

static DALResult Clock_DetectBSPVersion
(
  ClockDrvCtxt *pDrvCtxt
)
{
  ClockImageCtxtType     *pImageCtxt;
  ClockCPUPerfConfigType *pCPUPerfConfig;
  uint32                  i;

  pImageCtxt = (ClockImageCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Detect which CPU BSP data to use for this HW version.                 */
  /*-----------------------------------------------------------------------*/

  pCPUPerfConfig = pImageCtxt->pBSPConfig->pCPUPerfConfig;
  for (i = 0; i < pImageCtxt->pBSPConfig->nNumCPUPerfLevelConfigs; i++)
  {
    if (Clock_IsBSPSupported(&pCPUPerfConfig[i].HWVersion) == TRUE)
    {
      pImageCtxt->CPUCtxt.PerfConfig.HWVersion = pCPUPerfConfig[i].HWVersion;
      pImageCtxt->CPUCtxt.PerfConfig.anPerfLevel = pCPUPerfConfig[i].anPerfLevel;
      pImageCtxt->CPUCtxt.PerfConfig.nMaxPerfLevel = pCPUPerfConfig[i].nMaxPerfLevel;
      pImageCtxt->CPUCtxt.PerfConfig.nMinPerfLevel = pCPUPerfConfig[i].nMinPerfLevel;
      pImageCtxt->CPUCtxt.PerfConfig.nNumPerfLevels = pCPUPerfConfig[i].nNumPerfLevels;

      break;
    }
  }

  if (i == pImageCtxt->pBSPConfig->nNumCPUPerfLevelConfigs)
  {
    return DAL_ERROR;
  }

  return DAL_SUCCESS;

} /* END of Clock_DetectBSPVersion */


/* =========================================================================
**  Function : Clock_InitCPUConfig
** =========================================================================*/
/**
  Initializes current configuration of CPU clock
 
  This function is invoked at driver initialization to initialize the current
  CPU configuration.
 
  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if configuration was not valid, other DAL_SUCCESS.

  @dependencies
  None.
*/ 

static DALResult Clock_InitCPUConfig
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult            eResult;
  boolean              bResult;
  ClockNodeType       *pClock;
  ClockSourceNodeType *pSource;
  ClockImageCtxtType  *pImageCtxt;
  ClockCPUConfigType  *pConfig;
  uint32               nSourceIndex;

  pImageCtxt = (ClockImageCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Get CPU clock ID.                                                     */
  /*-----------------------------------------------------------------------*/

  eResult =
    Clock_GetClockId(
      pDrvCtxt, CLOCK_ID_CPU,
      (ClockIdType *)&pImageCtxt->CPUCtxt.pClock);
  if (eResult != DAL_SUCCESS)
  {
    return DAL_ERROR_INTERNAL;
  }

  pClock = pImageCtxt->CPUCtxt.pClock;

  /*-----------------------------------------------------------------------*/
  /* Only NOMINAL is guaranteed on all voltage rails until VCS/PMIC/RPM    */
  /* SW components are up and functional.  Scale to max at NOMINAL now.    */
  /*                                                                       */
  /* NOTE: The below logic only works if the min supported performance     */
  /* level is <= NOMINAL.  We would need a more clever scheme if the min   */
  /* and max performance levels were above NOMINAL.                        */
  /*-----------------------------------------------------------------------*/

  eResult =
    Clock_FindCPUMaxConfigAtVoltage(
      pDrvCtxt,
      &pConfig,
      VCS_CORNER_NOMINAL);
  if (eResult != DAL_SUCCESS)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Get source node.                                                      */
  /*-----------------------------------------------------------------------*/

  nSourceIndex = pDrvCtxt->anSourceIndex[pConfig->Mux.HALConfig.eSource];
  if (nSourceIndex == 0xFF)
  {
    return DAL_ERROR_INTERNAL;
  }

  pSource = &pDrvCtxt->aSources[nSourceIndex];
  if (pSource == NULL)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Slew MPLL3 (Q6PLL) to the max freq config at the current voltage.     */
  /*-----------------------------------------------------------------------*/

  bResult =
    HAL_clk_ConfigPLL(
      pSource->eSource,
      &pConfig->Mux.pSourceFreqConfig->HALConfig,
      HAL_CLK_SOURCE_CONFIG_MODE_SLEW);
  if (bResult != TRUE)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Update the Q6 PLL state.                                              */
  /* Mark the Q6 PLL as having already been calibrated - done in MBA.      */
  /*-----------------------------------------------------------------------*/

  pSource->nFlags |= CLOCK_FLAG_SOURCE_CALIBRATED;
  pSource->pActiveFreqConfig = pConfig->Mux.pSourceFreqConfig;

  /*-----------------------------------------------------------------------*/
  /* Update Q6 clock state.                                                */
  /*-----------------------------------------------------------------------*/

  pClock->pDomain->pSource = pSource;
  pClock->pDomain->pActiveMuxConfig = &pConfig->Mux;
  pImageCtxt->CPUCtxt.pConfig = pConfig;

  /*-----------------------------------------------------------------------*/
  /* Init NAS context.                                                     */
  /*-----------------------------------------------------------------------*/

  pImageCtxt->CPUCtxt.nNASRequestCount = 0;
  pImageCtxt->CPUCtxt.pNASConfig = NULL;

  /*-----------------------------------------------------------------------*/
  /* Ensure that the CPU core clock/domain/source reference counts are 1.  */
  /*-----------------------------------------------------------------------*/

  eResult =
    Clock_EnableClock(
      pDrvCtxt,
      (ClockIdType)pImageCtxt->CPUCtxt.pClock);
  if (eResult != DAL_SUCCESS)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the DCVS module.                                           */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_InitDCVS(pDrvCtxt);
  if (eResult != DAL_SUCCESS)
  {
    return DAL_ERROR_INTERNAL;
  }

  return DAL_SUCCESS;

} /* END Clock_InitCPUConfig */


/* =========================================================================
**  Function : Clock_InitMSSConfigBusConfig
** =========================================================================*/
/**
  Initializes current configuration of the MSS config bus clock

  This function is invoked at driver initialization to init the current
  MSS config bus configuration.

  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if configuration was not valid, other DAL_SUCCESS.

  @dependencies
  None.
*/

static DALResult Clock_InitMSSConfigBusConfig
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult                 eRes;
  ClockNodeType            *pClock;
  ClockSourceNodeType      *pSource;
  ClockImageCtxtType       *pImageCtxt;
  ClockConfigBusConfigType *pConfig;
  uint32                    nSourceIndex, nPL, nCfg;

  pImageCtxt = (ClockImageCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Get MSS Config Bus clock ID.                                          */
  /*-----------------------------------------------------------------------*/

  eRes =
    Clock_GetClockId(
      pDrvCtxt, CLOCK_ID_MSS_CONFIG_BUS,
      (ClockIdType *)&pImageCtxt->ConfigBusCtxt.pClock);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to get MSS config bus clock ID.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Get proper clock and configuration data                               */
  /*-----------------------------------------------------------------------*/

  pClock = pImageCtxt->ConfigBusCtxt.pClock;

  /*-----------------------------------------------------------------------*/
  /* Config the bus clock to the single 75 MHz perf level.                 */
  /*-----------------------------------------------------------------------*/

  nPL = pImageCtxt->pBSPConfig->pConfigBusPerfConfig->nMinPerfLevel;
  nCfg = pImageCtxt->pBSPConfig->pConfigBusPerfConfig->anPerfLevel[nPL];
  pConfig = &pImageCtxt->pBSPConfig->pConfigBusConfig[nCfg];
  HAL_clk_ConfigClockMux(pClock->pDomain->HALHandle, &pConfig->Mux.HALConfig);

  /*-----------------------------------------------------------------------*/
  /* Update state.                                                         */
  /*-----------------------------------------------------------------------*/

  nSourceIndex = pDrvCtxt->anSourceIndex[pConfig->Mux.HALConfig.eSource];
  if (nSourceIndex == 0xFF)
  {
    return DAL_ERROR;
  }

  pSource = &pDrvCtxt->aSources[nSourceIndex];
  if (pSource == NULL)
  {
    return DAL_ERROR;
  }

  pClock->pDomain->pSource = pSource;
  pImageCtxt->ConfigBusCtxt.pConfig = pConfig;
  pClock->pDomain->pActiveMuxConfig = &pConfig->Mux;

  /*-----------------------------------------------------------------------*/
  /* Ensure that MSS config bus clock/domain/source reference counts are 1.*/
  /*-----------------------------------------------------------------------*/

  eRes =
    Clock_EnableClock(
      pDrvCtxt, (ClockIdType)pImageCtxt->ConfigBusCtxt.pClock);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to enable the MSS config bus clock.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize MSS Config Bus module.                                     */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_InitConfigBus(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to init the MSS config bus clock NPA node.");
    return eRes;
  }

  return DAL_SUCCESS;

} /* END Clock_InitMSSConfigBusConfig */


/* =========================================================================
**  Function : Clock_InitImage
** =========================================================================*/
/*
  See ClockDriver.h
*/ 

DALResult Clock_InitImage
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult              eRes;
  ClockPropertyValueType PropVal;

  /*-----------------------------------------------------------------------*/
  /* Assign the image context.                                             */
  /*-----------------------------------------------------------------------*/

  pDrvCtxt->pImageCtxt = &Clock_ImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Initialize the SW Events for Clocks.                                  */
  /*-----------------------------------------------------------------------*/

  Clock_SWEvent(CLOCK_EVENT_INIT, 0);

  /*-----------------------------------------------------------------------*/
  /* Get the CPU BSP Configuration.                                        */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_GetPropertyValue("ClockImageBSP", &PropVal);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_WARNING,
      "Unable to get Clock BSP Property: ClockImageBSP.");
    return eRes;
  }

  Clock_ImageCtxt.pBSPConfig = (ClockImageBSPConfigType *)PropVal;

  /*-----------------------------------------------------------------------*/
  /* Detect the BSP version to use.                                        */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_DetectBSPVersion(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Failed to detect the BSP version.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Detect CPU efuse settings and update voltage data depending on slow,  */
  /* nominal or fast part.                                                 */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_DetectPVSData(pDrvCtxt);
  if (eRes != DAL_SUCCESS) 
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_WARNING,
      "Unable to detect CPU efuse settings - assuming SLOW part.");
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the XO module.                                             */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_InitXO(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to init XO.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the modem PLL resources.                                   */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_InitPLL(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to init the PLL.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize CPU core clock frequency configuration.                    */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_InitCPUConfig(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to detect CPU core clock configuration.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize MSS config bus clock frequency configuration.              */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_InitMSSConfigBusConfig(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to detect MSS config bus clock configuration.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitImage */


/* =========================================================================
**  Function : ClockStub_InitImage
** =========================================================================*/
/*
  See ClockDriver.h
*/

DALResult ClockStub_InitImage
(
  ClockDrvCtxt *pDrvCtxt
)
{
  uint32 i;

  /*-----------------------------------------------------------------------*/
  /* Initialize the SW Events for Clocks.                                  */
  /*-----------------------------------------------------------------------*/

  Clock_SWEvent(CLOCK_EVENT_INIT, 0);

  /*-----------------------------------------------------------------------*/
  /* Create stubs for each NPA node.                                       */
  /*-----------------------------------------------------------------------*/

  for (i = 0; i < ARR_SIZE(ClockNPANodeName); i++)
  {
    npa_stub_resource(ClockNPANodeName[i]);
  }

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitImage */


/* =========================================================================
**  Function : Clock_ProcessorSleep
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_ProcessorSleep
(
  ClockDrvCtxt       *pDrvCtxt,
  ClockSleepModeType  eMode,
  uint32              nFlags
)
{
  /*-----------------------------------------------------------------------*/
  /* There are currently no use cases for calling this API.  We'll trigger */
  /* an error to ensure it isn't called by mistake.                        */
  /*-----------------------------------------------------------------------*/

  DALSYS_LogEvent(
    DALDEVICEID_CLOCK,
    DALSYS_LOGEVENT_FATAL_ERROR,
    "Clock_ProcessorSleep API is currently unsupported.");

  return DAL_ERROR_NOT_SUPPORTED;

} /* END Clock_ProcessorSleep */


/* =========================================================================
**  Function : Clock_ProcessorRestore
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_ProcessorRestore
(
  ClockDrvCtxt       *pDrvCtxt,
  ClockSleepModeType  eMode,
  uint32              nFlags
) 
{
  /*-----------------------------------------------------------------------*/
  /* There are currently no use cases for calling this API.  We'll trigger */
  /* an error to ensure it isn't called by mistake.                        */
  /*-----------------------------------------------------------------------*/

  DALSYS_LogEvent(
    DALDEVICEID_CLOCK,
    DALSYS_LOGEVENT_FATAL_ERROR,
    "Clock_ProcessorRestore API is currently unsupported.");

  return DAL_ERROR_NOT_SUPPORTED;
  
} /* END Clock_ProcessorRestore */


/* =========================================================================
**  Function : Clock_LoadNV
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_LoadNV
(
  ClockDrvCtxt *pDrvCtxt
)
{
  CoreConfigHandle    hConfig;
  uint32              nReadResult, nData;
  DALResult           eResult;

  /*-----------------------------------------------------------------------*/
  /* Read clock configuration file.                                        */
  /*-----------------------------------------------------------------------*/

  hConfig = CoreIni_ConfigCreate(CLOCK_EFS_INI_FILENAME);
  if (hConfig == NULL)
  {
    DALSYS_LogEvent(
      DALDEVICEID_CLOCK,
      DALSYS_LOGEVENT_INFO,
      "Unable to read EFS file: %s",
      CLOCK_EFS_INI_FILENAME);
  }

  /*-----------------------------------------------------------------------*/
  /* Update global flags.                                                  */
  /*-----------------------------------------------------------------------*/

  if (hConfig)
  {
    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        CLOCK_EFS_MPSS_CONFIG_SECTION,
        CLOCK_EFS_MPSS_CLOCK_FLAGS_FLAG,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pDrvCtxt->nGlobalFlags = nData;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Update DCVS data.                                                     */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_LoadNV_DCVS(pDrvCtxt, hConfig);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      DALDEVICEID_CLOCK,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to load DCVS EFS");
  }

  /*-----------------------------------------------------------------------*/
  /* Destroy the handle.                                                   */
  /*-----------------------------------------------------------------------*/

  if (hConfig)
  {
    CoreIni_ConfigDestroy(hConfig);
  }

  return eResult;

} /* END Clock_LoadNV */


/* =========================================================================
**  Function : Clock_ImageBIST
** =========================================================================*/
/*
  See ClockDriver.h
*/

DALResult Clock_ImageBIST
(
  ClockDrvCtxt  *pDrvCtxt,
  boolean       *bBISTPassed,
  uint32        *nFailedTests
)
{

  /*
   * Nothing to do yet.
   */
  return DAL_SUCCESS;

} /* END Clock_ImageBIST */
