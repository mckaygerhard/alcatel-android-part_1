/*
===========================================================================
*/
/**
  @file ClockMSSPLL.c 
  
  Modem PLL NPA node definitions for MSS clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/clock/hw/msm8996/mss/src/ClockMSSPLL.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  02/17/12   vs      Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockDriver.h"
#include "ClockMSS.h"
#include "DALDeviceId.h"
#include "DDIVCS.h"

#include <DALSys.h>
#include <npa.h>
#include <npa_resource.h>
#include <npa_remote.h>
#include <npa_remote_resource.h>
#include "pmapp_npa.h"


/*=========================================================================
      Macros
==========================================================================*/


/*
 * Generic active value for appropriate PLL node.
 */
#define CLOCK_PMIC_NPA_MODE_ID_GENERIC_ACTIVE      1


/*=========================================================================
      Type Definitions
==========================================================================*/


/*=========================================================================
      Prototypes
==========================================================================*/


/*=========================================================================
      Data
==========================================================================*/


/*=========================================================================
      Functions
==========================================================================*/


/* =========================================================================
**  Function : Clock_PLLRegulatorReadyCallback
** =========================================================================*/
/**
  Callback after /pmic/client/pll_ts node is created.

  This function is called by the NPA framework after the /pmic/client/pll_ts
  node is created.  The creation is delayed until all dependencies are also
  created.

  @param *pContext [in] -- Context pointer, the driver context in this case.

  @return
  None.

  @dependencies
  None.
*/

static void Clock_PLLRegulatorReadyCallback
(
  void *pContext
)
{
  npa_client_handle hNPAPLL;

  /*-----------------------------------------------------------------------*/
  /* Get a handle to appropriate PLL node.                                 */
  /*-----------------------------------------------------------------------*/

  hNPAPLL =
    npa_create_sync_client(
      PMIC_NPA_GROUP_ID_PLL_TS,
      "/clock" PMIC_NPA_GROUP_ID_PLL_TS,
      NPA_CLIENT_SUPPRESSIBLE);

  if (hNPAPLL == NULL)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to create NPA sync client %s.",
      PMIC_NPA_GROUP_ID_PLL_TS);

    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Vote for TS PLL active state.                                         */
  /*-----------------------------------------------------------------------*/

  npa_issue_scalar_request(hNPAPLL, CLOCK_PMIC_NPA_MODE_ID_GENERIC_ACTIVE);

} /* END Clock_PLLRegulatorReadyCallback */


/* =========================================================================
**  Function : Clock_InitPLL
** =========================================================================*/
/*
  See ClockMSS.h
*/ 

DALResult Clock_InitPLL
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult eResult;

  /*-----------------------------------------------------------------------*/
  /* Register a callback for the appropriate PLL node.                     */
  /*-----------------------------------------------------------------------*/

  eResult =
    Clock_RegisterResourceCallback(
      pDrvCtxt,
      PMIC_NPA_GROUP_ID_PLL_TS,
      Clock_PLLRegulatorReadyCallback,
      pDrvCtxt);
  if (eResult != DAL_SUCCESS)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Q6 PLL configured along with the Q6 clock.                            */
  /*-----------------------------------------------------------------------*/

  /*-----------------------------------------------------------------------*/
  /* Configure MPLL0 to the default performance level.                     */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MPLL0, NULL);
  if (eResult != DAL_SUCCESS)
  {
    return eResult;
  }

/*
 * We may need to add these back if DTR can't properly manage these resources.
 */
#if 0
  /*-----------------------------------------------------------------------*/
  /* Configure MPLL1 to the default performance level.                     */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MPLL1, NULL);
  if (eResult != DAL_SUCCESS)
  {
    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Configure MPLL2 to the default performance level.                     */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MPLL2, NULL);
  if (eResult != DAL_SUCCESS)
  {
    return eResult;
  }
#endif

  /*-----------------------------------------------------------------------*/
  /* Configure MPLL4 to the default performance level.                     */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MPLL4, NULL);
  if (eResult != DAL_SUCCESS)
  {
    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitPLL */
