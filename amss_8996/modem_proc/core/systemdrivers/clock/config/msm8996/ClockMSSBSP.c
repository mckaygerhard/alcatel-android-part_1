/*
==============================================================================

FILE:         ClockMSSBSP.c 

DESCRIPTION:
  This file contains clock bsp data for the DAL based driver.


==============================================================================

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/clock/config/msm8996/ClockMSSBSP.c#1 $

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockMSSBSP.h"
#include "comdef.h"
#include "VCSDefs.h"


/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 * Enumeration of BUSMSSCONFIG performance levels.
 */
enum
{
  CLOCK_BUSMSSCONFIG_PERF_LEVEL_0,
  CLOCK_BUSMSSCONFIG_PERF_LEVEL_1,
  CLOCK_BUSMSSCONFIG_PERF_LEVEL_TOTAL
};


/*
 * Enumeration of BUSMSSCONFIG configurations.
 */
enum
{
  CLOCK_BUSMSSCONFIG_CONFIG_19P2MHz,
  CLOCK_BUSMSSCONFIG_CONFIG_75P0MHz,
  CLOCK_BUSMSSCONFIG_CONFIG_TOTAL
};


/*
 * BUSMSSCONFIG performance levels mappings
 */
static uint32 Clock_BUSMSSCONFIGPerfLevels [] =
{
  CLOCK_BUSMSSCONFIG_CONFIG_19P2MHz,
  CLOCK_BUSMSSCONFIG_CONFIG_75P0MHz,
};


/*
 * Enumeration of Q6 performance levels.
 */
enum
{
  CLOCK_Q6_PERF_LEVEL_0,
  CLOCK_Q6_PERF_LEVEL_1,
  CLOCK_Q6_PERF_LEVEL_2,
  CLOCK_Q6_PERF_LEVEL_3,
  CLOCK_Q6_PERF_LEVEL_4,
  CLOCK_Q6_PERF_LEVEL_5,
  CLOCK_Q6_PERF_LEVEL_6,
  CLOCK_Q6_PERF_LEVEL_7,
  CLOCK_Q6_PERF_LEVEL_8,
  CLOCK_Q6_PERF_LEVEL_9,
  CLOCK_Q6_PERF_LEVEL_10,
  CLOCK_Q6_PERF_LEVEL_11,
  CLOCK_Q6_PERF_LEVEL_12,
  CLOCK_Q6_PERF_LEVEL_13,
  CLOCK_Q6_PERF_LEVEL_14,
  CLOCK_Q6_PERF_LEVEL_15,
  CLOCK_Q6_PERF_LEVEL_TOTAL
};


/*
 * Enumeration of Q6 configurations.
 */
enum
{
  CLOCK_Q6_CONFIG_19P2MHz,
  CLOCK_Q6_CONFIG_115P2MHz,
  CLOCK_Q6_CONFIG_144P0MHz,
  CLOCK_Q6_CONFIG_230P4MHz,
  CLOCK_Q6_CONFIG_288P0MHz,
  CLOCK_Q6_CONFIG_345P6MHz,
  CLOCK_Q6_CONFIG_364P8MHz,
  CLOCK_Q6_CONFIG_384P0MHz,
  CLOCK_Q6_CONFIG_499P2MHz,
  CLOCK_Q6_CONFIG_518P4MHz,
  CLOCK_Q6_CONFIG_576P0MHz,
  CLOCK_Q6_CONFIG_652P8MHz,
  CLOCK_Q6_CONFIG_729P6MHz_NOMINAL,
  CLOCK_Q6_CONFIG_729P6MHz_TURBO,
  CLOCK_Q6_CONFIG_806P4MHz,
  CLOCK_Q6_CONFIG_844P8MHz,
  CLOCK_Q6_CONFIG_883P2MHz,
  CLOCK_Q6_CONFIG_TOTAL
};


/*
 * Q6 performance levels mappings for v2.x
 */
static uint32 Clock_Q6PerfLevels_v2x [] =
{
  CLOCK_Q6_CONFIG_19P2MHz,
  CLOCK_Q6_CONFIG_115P2MHz,
  CLOCK_Q6_CONFIG_144P0MHz,
  CLOCK_Q6_CONFIG_230P4MHz,
  CLOCK_Q6_CONFIG_288P0MHz,
  CLOCK_Q6_CONFIG_345P6MHz,
  CLOCK_Q6_CONFIG_364P8MHz,
  CLOCK_Q6_CONFIG_384P0MHz,
  CLOCK_Q6_CONFIG_499P2MHz,
  CLOCK_Q6_CONFIG_518P4MHz,
  CLOCK_Q6_CONFIG_576P0MHz,
  CLOCK_Q6_CONFIG_652P8MHz,
  CLOCK_Q6_CONFIG_729P6MHz_TURBO,
  CLOCK_Q6_CONFIG_806P4MHz,
  CLOCK_Q6_CONFIG_844P8MHz,
};

/*
 * Q6 performance levels mappings for v3.x
 */
static uint32 Clock_Q6PerfLevels_v3x [] =
{
  CLOCK_Q6_CONFIG_19P2MHz,
  CLOCK_Q6_CONFIG_115P2MHz,
  CLOCK_Q6_CONFIG_144P0MHz,
  CLOCK_Q6_CONFIG_230P4MHz,
  CLOCK_Q6_CONFIG_288P0MHz,
  CLOCK_Q6_CONFIG_345P6MHz,
  CLOCK_Q6_CONFIG_364P8MHz,
  CLOCK_Q6_CONFIG_384P0MHz,
  CLOCK_Q6_CONFIG_499P2MHz,
  CLOCK_Q6_CONFIG_518P4MHz,
  CLOCK_Q6_CONFIG_576P0MHz,
  CLOCK_Q6_CONFIG_652P8MHz,
  CLOCK_Q6_CONFIG_729P6MHz_NOMINAL,
  CLOCK_Q6_CONFIG_806P4MHz,
  CLOCK_Q6_CONFIG_844P8MHz,
  CLOCK_Q6_CONFIG_883P2MHz,
};


/*=========================================================================
      Data
==========================================================================*/


/*
 * The MPLL3 source frequency configurations are defined in ClockBSP.c
 */
extern ClockSourceFreqConfigType SourceFreqConfig_MPLL3[11];


/*
 * BUSMSSCONFIG clock configurations
 */
static ClockConfigBusConfigType Clock_BUSMSSCONFIGConfig[] =
{
  /* NOTE: Divider value is (2*Div) due to potential use of fractional values */
  {
    /* .Mux             = */
    {
      /* .nFreq             = */  19200000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_LOW_MINUS,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  NULL
    },
  },
  {
    /* .Mux             = */
    {
      /* .nFreq             = */  75000000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_GPLL0, 8, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_LOW_MINUS,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  NULL
    },
  },
  { { 0 } }
};


/*
 * CPU clock configurations
 */
static ClockCPUConfigType Clock_Q6Config[] =
{
  /* NOTE: Divider value is (2*Div) due to potential use of fractional values */
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  19200000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  NULL
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_LOW_MINUS,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  115200000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 10, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[1] // 576.0 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_LOW_MINUS,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  144000000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 8, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[1] // 576.0 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_LOW_MINUS,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  230400000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 6, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[3] // 691.2 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_LOW_MINUS,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  288000000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 4, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[1] // 576.0 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_LOW_MINUS,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  345600000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 4, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[3] // 691.2 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_LOW,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  364800000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 4, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[4] // 729.6 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_LOW,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  384000000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 4, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[6] // 768.0 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_LOW,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  499200000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 4, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[10] // 998.4 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_LOW,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  518400000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[0] // 518.4 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_NOMINAL,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  576000000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[1] // 576.0 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_NOMINAL,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  652800000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[2] // 652.8 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_NOMINAL,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  729600000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[4] // 729.6 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_NOMINAL,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  729600000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[4] // 729.6 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_TURBO,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  806400000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[7] // 806.4 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_TURBO,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  844800000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[8] // 844.8 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_TURBO,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX, /* TODO */
    /* .Mux             = */
    {
      /* .nFreq             = */  883200000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_MPLL3, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_OFF,
      /* .HWVersion         = */  { {0x00, 0x00}, {0x00, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_MPLL3[9] // 883.2 MHz
    },
    /* .eVRegMSSCorner = */  VCS_CORNER_TURBO,
    .nStrapACCVal   = 0x20,
    .nSleepDiv2x    = 2
  },
  { 0 }
};


/*
 * Performance level configuration data for the BUSMSSCONFIG clock.
 */
static ClockConfigBusPerfConfigType Clock_BUSMSSCONFIGPerfConfig =
{
  .nMinPerfLevel = CLOCK_BUSMSSCONFIG_PERF_LEVEL_1,
  .nMaxPerfLevel = CLOCK_BUSMSSCONFIG_PERF_LEVEL_1,
  .anPerfLevel   = Clock_BUSMSSCONFIGPerfLevels
};


/*
 * Performance level configuration data for the Q6 clock.
 */
static ClockCPUPerfConfigType Clock_Q6PerfConfig[] =
{
  {
    .HWVersion      = {{0x00, 0x00}, {0x03, 0x00}, DALCHIPINFO_FAMILY_MSM8996},
    .nMinPerfLevel  = CLOCK_Q6_PERF_LEVEL_1,
    .nMaxPerfLevel  = CLOCK_Q6_PERF_LEVEL_14,
    .anPerfLevel    = Clock_Q6PerfLevels_v2x,
    .nNumPerfLevels = ARR_SIZE(Clock_Q6PerfLevels_v2x)
  },
  {
    .HWVersion      = {{0x00, 0x00}, {0xFF, 0xFF}},
    .nMinPerfLevel  = CLOCK_Q6_PERF_LEVEL_1,
    .nMaxPerfLevel  = CLOCK_Q6_PERF_LEVEL_15,
    .anPerfLevel    = Clock_Q6PerfLevels_v3x,
    .nNumPerfLevels = ARR_SIZE(Clock_Q6PerfLevels_v3x)
  },
};


/*
 * Image BSP data
 */
const ClockImageBSPConfigType ClockImageBSPConfig =
{
  .bEnableDCS              = TRUE,
  .pCPUConfig              = Clock_Q6Config,
  .pCPUPerfConfig          = Clock_Q6PerfConfig,
  .nNumCPUPerfLevelConfigs = ARR_SIZE(Clock_Q6PerfConfig),
  .pConfigBusConfig        = Clock_BUSMSSCONFIGConfig,
  .pConfigBusPerfConfig    = &Clock_BUSMSSCONFIGPerfConfig,
  .eCPU                    = CLOCK_CPU_MSS_Q6,
  .eRail                   = VCS_RAIL_CX,
  .szRailName              = VCS_NPA_RESOURCE_VDD_CX
};
