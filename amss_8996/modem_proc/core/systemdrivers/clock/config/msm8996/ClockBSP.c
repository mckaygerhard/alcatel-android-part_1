/*
==============================================================================

FILE:         ClockBSP.c 

DESCRIPTION:
  This file contains clock bsp data for the DAL based driver.


==============================================================================

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/clock/config/msm8996/ClockBSP.c#1 $

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBSP.h"


/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 *  SourceFreqConfig_Sleep
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_Sleep[] =
{
  {
    /* .nFreqHz    = */ 32768,
    /* .HALConfig  = */ { HAL_CLK_SOURCE_NULL },
    /* .eVRegLevel = */ VCS_CORNER_OFF,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_XO
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_XO[] =
{
  {
    /* .nFreqHz    = */ 19200 * 1000,
    /* .HALConfig  = */ { HAL_CLK_SOURCE_NULL },
    /* .eVRegLevel = */ VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  /* last entry */
  { 0 }
};

/*
 *  SourceFreqConfig_GPLL0_DIV2
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_GPLL0_DIV2[] =
{
  {
    /* .nFreqHz    = */  300000 * 1000,
    /* .HALConfig  = */  { HAL_CLK_SOURCE_GPLL0 },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  /* last entry */
  { 0 }
};

/*
 *  SourceFreqConfig_GPLL0
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_GPLL0[] =
{
  {
    /* .nFreqHz    = */  600000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  31,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x40,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_MPLL0
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MPLL0[] =
{
  {
    /* .nFreqHz    = */  1536000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  80,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  1766400 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  92,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  1900800 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  99,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  1996800 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  104,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqCalibrationConfig_MPLL0
 *
 *  Set of source frequency calibration configurations.
 */
static ClockSourceFreqConfigType SourceFreqCalibrationConfig_MPLL0[] =
{
  {
    /* .nFreqHz    = */  1766400 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  92,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW,
    /* .HWVersion  = */  { {0x00, 0x00}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 * We may need to add these back if DTR can't properly manage these resources.
 * When adding back these entries, be sure to check the VCO used is in the
 * appropriate range for the frequency selected
 */
#if 0
/*
 *  SourceFreqConfig_MPLL1
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MPLL1[] =
{
  {
    /* .nFreqHz    = */  412800 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  2,
      /* .nL             = */  43,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_MPLL2
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MPLL2[] =
{
  {
    /* .nFreqHz    = */  460800 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  2,
      /* .nL             = */  48,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  /* last entry */
  { 0 }
};
#endif


/*
 *  SourceFreqConfig_MPLL3
 *
 *  Set of source frequency configurations.
 */
ClockSourceFreqConfigType SourceFreqConfig_MPLL3[] =
{
  {
    /* .nFreqHz    = */  518400 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  27,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  576000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  30,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  652800 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  34,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  691200 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  36,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  729600 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  38,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  748800 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  39,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  768000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  40,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  806400 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  42,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  844800 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  44,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  883200 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  46,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  {
    /* .nFreqHz    = */  998400 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  52,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqCalibrationConfig_MPLL3
 *
 *  Set of source frequency calibration configurations.
 */
static ClockSourceFreqConfigType SourceFreqCalibrationConfig_MPLL3[] =
{
  {
    /* .nFreqHz    = */  748800 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  39,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0, 0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_MPLL4
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MPLL4[] =
{
  {
    /* .nFreqHz    = */  576000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  2,
      /* .nL             = */  60,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  /* last entry */
  { 0 }
};


/*
 * SourceConfig
 *
 * Clock source configuration data.
 */
const ClockSourceConfigType SourceConfig[] =
{
  {
    SOURCE_NAME(SLEEPCLK),

    /* .nConfigMask            = */ 0,
    /* .pSourceFreqConfig      = */ SourceFreqConfig_Sleep,
  },
  {
    SOURCE_NAME(XO),

    /* .nConfigMask            = */ 0,
    /* .pSourceFreqConfig      = */ SourceFreqConfig_XO,
  },
  {
    SOURCE_NAME(GPLL0_DIV2),

    /* .nConfigMask            = */ 0,
    /* .pSourceFreqConfig      = */ SourceFreqConfig_GPLL0_DIV2,
  },
  {
    SOURCE_NAME(GPLL0),

    /* .nConfigMask            = */ CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    /* .pSourceFreqConfig      = */ SourceFreqConfig_GPLL0,
  },
  {
    SOURCE_NAME(MPLL0),

    /* .nConfigMask            = */ CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
    /* .pSourceFreqConfig      = */ SourceFreqConfig_MPLL0,
    /* .pCalibrationFreqConfig = */ SourceFreqCalibrationConfig_MPLL0,
    /* .eDisableMode           = */ HAL_CLK_SOURCE_DISABLE_MODE_SAVE
  },
/*
 * We may need to add these back if DTR can't properly manage these resources.
 */
#if 0
  {
    SOURCE_NAME(MPLL1),

    /* .nConfigMask            = */ CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
    /* .pSourceFreqConfig      = */ SourceFreqConfig_MPLL1,
  },
  {
    SOURCE_NAME(MPLL2),

    /* .nConfigMask            = */ CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
    /* .pSourceFreqConfig      = */ SourceFreqConfig_MPLL2,
  },
#endif
  {
    SOURCE_NAME(MPLL3),

    /* .nConfigMask            = */ CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE |                                CLOCK_CONFIG_PLL_AUX2_OUTPUT_ENABLE,
    /* .pSourceFreqConfig      = */ SourceFreqConfig_MPLL3,
    /* .pCalibrationFreqConfig = */ SourceFreqCalibrationConfig_MPLL3,
  },
  {
    SOURCE_NAME(MPLL4),

    /* .nConfigMask            = */ CLOCK_CONFIG_PLL_AUX_OUTPUT_ENABLE,
    /* .pSourceFreqConfig      = */ SourceFreqConfig_MPLL4,
  },
  { 0 }
};


/*----------------------------------------------------------------------*/
/* GCC   Clock Configurations                                           */
/*----------------------------------------------------------------------*/


/*
 * BLSP1QUP1I2CAPPS clock configurations
 */
const ClockMuxConfigType BLSP1QUP1I2CAPPSClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,         },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, VCS_CORNER_LOW,               },
  { 0 }
};


/*
 * BLSP1QUP1SPIAPPS clock configurations
 */
const ClockMuxConfigType BLSP1QUP1SPIAPPSClockConfig[] =
{
  {     960000, { HAL_CLK_SOURCE_XO,             20,     1,      2,      2       }, VCS_CORNER_LOW_MINUS,         },
  {    4800000, { HAL_CLK_SOURCE_XO,             8,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,         },
  {    9600000, { HAL_CLK_SOURCE_XO,             4,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,         },
  {   15000000, { HAL_CLK_SOURCE_GPLL0,          20,     1,      4,      4       }, VCS_CORNER_LOW,               },
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,         },
  {   25000000, { HAL_CLK_SOURCE_GPLL0,          24,     1,      2,      2       }, VCS_CORNER_LOW,               },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, VCS_CORNER_NOMINAL,           },
  { 0 }
};


/*
 * BLSP1UART1APPS clock configurations
 */
const ClockMuxConfigType BLSP1UART1APPSClockConfig[] =
{
  {    3686400, { HAL_CLK_SOURCE_GPLL0,          2,      96,     15625,  15625   }, VCS_CORNER_LOW,               },
  {    7372800, { HAL_CLK_SOURCE_GPLL0,          2,      192,    15625,  15625   }, VCS_CORNER_LOW,               },
  {   14745600, { HAL_CLK_SOURCE_GPLL0,          2,      384,    15625,  15625   }, VCS_CORNER_LOW,               },
  {   16000000, { HAL_CLK_SOURCE_GPLL0,          10,     2,      15,     15      }, VCS_CORNER_LOW,               },
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,         },
  {   24000000, { HAL_CLK_SOURCE_GPLL0,          10,     1,      5,      5       }, VCS_CORNER_LOW,               },
  {   32000000, { HAL_CLK_SOURCE_GPLL0,          2,      4,      75,     75      }, VCS_CORNER_NOMINAL,           },
  {   40000000, { HAL_CLK_SOURCE_GPLL0,          30,     0,      0,      0       }, VCS_CORNER_NOMINAL,           },
  {   46400000, { HAL_CLK_SOURCE_GPLL0,          2,      29,     375,    375     }, VCS_CORNER_NOMINAL,           },
  {   48000000, { HAL_CLK_SOURCE_GPLL0,          25,     0,      0,      0       }, VCS_CORNER_NOMINAL,           },
  {   51200000, { HAL_CLK_SOURCE_GPLL0,          2,      32,     375,    375     }, VCS_CORNER_NOMINAL,           },
  {   56000000, { HAL_CLK_SOURCE_GPLL0,          2,      7,      75,     75      }, VCS_CORNER_NOMINAL,           },
  {   58982400, { HAL_CLK_SOURCE_GPLL0,          2,      1536,   15625,  15625   }, VCS_CORNER_NOMINAL,           },
  {   60000000, { HAL_CLK_SOURCE_GPLL0,          20,     0,      0,      0       }, VCS_CORNER_NOMINAL,           },
  {   63157895, { HAL_CLK_SOURCE_GPLL0,          19,     0,      0,      0       }, VCS_CORNER_NOMINAL,           },
  { 0 }
};


/*
 * BLSPUARTSIM clock configurations
 */
const ClockMuxConfigType BLSPUARTSIMClockConfig[] =
{
  {    3840000, { HAL_CLK_SOURCE_XO,             10,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,         },
  { 0 }
};


/*
 * SECCTRL clock configurations
 */
const ClockMuxConfigType SECCTRLClockConfig[] =
{
  {    4800000, { HAL_CLK_SOURCE_XO,             8,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,          },
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,          },
  { 0 }
};


/*
 * MSSMCDMABIMC clock configurations
 */
const ClockMuxConfigType MSSMCDMABIMCClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW,               },
  {  150000000, { HAL_CLK_SOURCE_GPLL0,          8,      0,      0,      0       }, VCS_CORNER_LOW,               },
  {  300000000, { HAL_CLK_SOURCE_GPLL0,          4,      0,      0,      0       }, VCS_CORNER_NOMINAL,           },
  { 0 }
};



/*----------------------------------------------------------------------*/
/* MSS   Clock Configurations                                           */
/*----------------------------------------------------------------------*/


/*
 * BITCOXMMND clock configurations
 */
const ClockMuxConfigType BITCOXMMNDClockConfig[] =
{
  {    1843200, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      96,     15625,  15625   }, VCS_CORNER_LOW_MINUS,         },
  {   16000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      4,      75,     75      }, VCS_CORNER_LOW_MINUS,         },
  {   32000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      8,      75,     75      }, VCS_CORNER_LOW_MINUS,         },
  {   48000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      4,      25,     25      }, VCS_CORNER_LOW_MINUS,         },
  {   51200000, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      64,     375,    375     }, VCS_CORNER_LOW_MINUS,         },
  { 0 }
};


/*
 * RBCPRREF clock configurations
 */
const ClockMuxConfigType RBCPRREFClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,         },
  {   50000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     12,     0,      0,      0       }, VCS_CORNER_NOMINAL,           },
  { 0 }
};


/*
 * UIM SIM clock configurations.
 */
const ClockMuxConfigType  UIMSIMClockConfig[] =
{
  {    3840000, { HAL_CLK_SOURCE_XO,             10,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,         },
  {    4800000, { HAL_CLK_SOURCE_XO,             8,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,         },
  { 0 }
};


/*
 * UIM0MND clock configurations
 */
const ClockMuxConfigType UIM0MNDClockConfig[] =
{
  {    4807680, { HAL_CLK_SOURCE_XO,             2,      313,    1250,   1250    }, VCS_CORNER_LOW_MINUS,         },
  {    4953600, { HAL_CLK_SOURCE_XO,             2,      129,    500,    500     }, VCS_CORNER_LOW_MINUS,         },
  {    4961280, { HAL_CLK_SOURCE_XO,             2,      323,    1250,   1250    }, VCS_CORNER_LOW_MINUS,         },
  {    5120000, { HAL_CLK_SOURCE_XO,             2,      4,      15,     15      }, VCS_CORNER_LOW_MINUS,         },
  {    5283840, { HAL_CLK_SOURCE_XO,             2,      172,    625,    625     }, VCS_CORNER_LOW_MINUS,         },
  {    5285120, { HAL_CLK_SOURCE_XO,             2,      4129,   15000,  15000   }, VCS_CORNER_LOW_MINUS,         },
  {    5760000, { HAL_CLK_SOURCE_XO,             2,      3,      10,     10      }, VCS_CORNER_LOW_MINUS,         },
  {    5775360, { HAL_CLK_SOURCE_XO,             2,      188,    625,    625     }, VCS_CORNER_LOW_MINUS,         },
  {    6328320, { HAL_CLK_SOURCE_XO,             2,      206,    625,    625     }, VCS_CORNER_LOW_MINUS,         },
  {    6343680, { HAL_CLK_SOURCE_XO,             2,      413,    1250,   1250    }, VCS_CORNER_LOW_MINUS,         },
  {    6400000, { HAL_CLK_SOURCE_XO,             2,      1,      3,      3       }, VCS_CORNER_LOW_MINUS,         },
  {    6604800, { HAL_CLK_SOURCE_XO,             2,      43,     125,    125     }, VCS_CORNER_LOW_MINUS,         },
  {    6606080, { HAL_CLK_SOURCE_XO,             2,      5161,   15000,  15000   }, VCS_CORNER_LOW_MINUS,         },
  {    6606400, { HAL_CLK_SOURCE_XO,             2,      4129,   12000,  12000   }, VCS_CORNER_LOW_MINUS,         },
  {    6606720, { HAL_CLK_SOURCE_XO,             2,      3441,   10000,  10000   }, VCS_CORNER_LOW_MINUS,         },
  {    6607360, { HAL_CLK_SOURCE_XO,             2,      2581,   7500,   7500    }, VCS_CORNER_LOW_MINUS,         },
  {    7045120, { HAL_CLK_SOURCE_XO,             2,      688,    1875,   1875    }, VCS_CORNER_LOW_MINUS,         },
  {    7046400, { HAL_CLK_SOURCE_XO,             2,      367,    1000,   1000    }, VCS_CORNER_LOW_MINUS,         },
  {    7046880, { HAL_CLK_SOURCE_XO,             2,      14681,  40000,  40000   }, VCS_CORNER_LOW_MINUS,         },
  {    7047040, { HAL_CLK_SOURCE_XO,             2,      11011,  30000,  30000   }, VCS_CORNER_LOW_MINUS,         },
  {    7047680, { HAL_CLK_SOURCE_XO,             2,      2753,   7500,   7500    }, VCS_CORNER_LOW_MINUS,         },
  {    7188480, { HAL_CLK_SOURCE_XO,             2,      234,    625,    625     }, VCS_CORNER_LOW_MINUS,         },
  {    7200000, { HAL_CLK_SOURCE_XO,             2,      3,      8,      8       }, VCS_CORNER_LOW_MINUS,         },
  {    7201280, { HAL_CLK_SOURCE_XO,             2,      2813,   7500,   7500    }, VCS_CORNER_LOW_MINUS,         },
  {    7203840, { HAL_CLK_SOURCE_XO,             2,      469,    1250,   1250    }, VCS_CORNER_LOW_MINUS,         },
  {    7432320, { HAL_CLK_SOURCE_XO,             2,      3871,   10000,  10000   }, VCS_CORNER_LOW_MINUS,         },
  {    7680000, { HAL_CLK_SOURCE_XO,             2,      2,      5,      5       }, VCS_CORNER_LOW_MINUS,         },
  {    7925760, { HAL_CLK_SOURCE_XO,             2,      258,    625,    625     }, VCS_CORNER_LOW_MINUS,         },
  {    7927680, { HAL_CLK_SOURCE_XO,             2,      4129,   10000,  10000   }, VCS_CORNER_LOW_MINUS,         },
  {    7928320, { HAL_CLK_SOURCE_XO,             2,      3097,   7500,   7500    }, VCS_CORNER_LOW_MINUS,         },
  {    7929600, { HAL_CLK_SOURCE_XO,             2,      413,    1000,   1000    }, VCS_CORNER_LOW_MINUS,         },
  {    8000000, { HAL_CLK_SOURCE_XO,             2,      5,      12,     12      }, VCS_CORNER_LOW_MINUS,         },
  {    8257920, { HAL_CLK_SOURCE_XO,             2,      4301,   10000,  10000   }, VCS_CORNER_LOW_MINUS,         },
  {    8258560, { HAL_CLK_SOURCE_XO,             2,      1613,   3750,   3750    }, VCS_CORNER_LOW_MINUS,         },
  {    8455680, { HAL_CLK_SOURCE_XO,             2,      1101,   2500,   2500    }, VCS_CORNER_LOW_MINUS,         },
  {    8456320, { HAL_CLK_SOURCE_XO,             2,      13213,  30000,  30000   }, VCS_CORNER_LOW_MINUS,         },
  {    8458240, { HAL_CLK_SOURCE_XO,             2,      826,    1875,   1875    }, VCS_CORNER_LOW_MINUS,         },
  {    8640000, { HAL_CLK_SOURCE_XO,             2,      9,      20,     20      }, VCS_CORNER_LOW_MINUS,         },
  {    8806400, { HAL_CLK_SOURCE_XO,             2,      172,    375,    375     }, VCS_CORNER_LOW_MINUS,         },
  {    8808320, { HAL_CLK_SOURCE_XO,             2,      13763,  30000,  30000   }, VCS_CORNER_LOW_MINUS,         },
  {    8808640, { HAL_CLK_SOURCE_XO,             2,      27527,  60000,  60000   }, VCS_CORNER_LOW_MINUS,         },
  {    8808960, { HAL_CLK_SOURCE_XO,             2,      1147,   2500,   2500    }, VCS_CORNER_LOW_MINUS,         },
  {    9000000, { HAL_CLK_SOURCE_XO,             2,      15,     32,     32      }, VCS_CORNER_LOW_MINUS,         },
  {    9000960, { HAL_CLK_SOURCE_XO,             2,      293,    625,    625     }, VCS_CORNER_LOW_MINUS,         },
  {    9511680, { HAL_CLK_SOURCE_XO,             2,      2477,   5000,   5000    }, VCS_CORNER_LOW_MINUS,         },
  {    9600000, { HAL_CLK_SOURCE_XO,             2,      1,      2,      2       }, VCS_CORNER_LOW_MINUS,         },
  { 0 }
};



/*
 * Clock Log Default Configuration.
 *
 * NOTE: An .nGlobalLogFlags value of 0x12 will log only clock frequency
 *       changes and source state changes by default.
 */
const ClockLogType ClockLogDefaultConfig[] =
{
  {
    /* .nLogSize        = */ 4096,
    /* .nGlobalLogFlags = */ 0x12 | CLOCK_GLOBAL_FLAG_LOG_CLOCK_STATE_CHANGE
  }
};


/*
 * Clock Flag Init Config.
 */
const ClockFlagInitType ClockFlagInitConfig[] =
{
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN,
    (void *)"clk_q6",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void *)"clk_src_bus_mss_config",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN,
    (void *)"clk_src_bus_mss_config",
    CLOCK_FLAG_INTERNAL_CONTROL
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void *)"clk_bus_uim0",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void *)"clk_bus_uim1",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void *)"clk_bus_uim2",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void *)"clk_bus_uim3",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void *)"clk_rbcpr_ref",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void *)"clk_bus_rbcpr",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void *)"clk_bus_slave_timeout",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void *)"GPLL0",
    CLOCK_FLAG_STUB_HW_RUMI
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void *)"MPLL0",
    CLOCK_FLAG_SUPPORTS_SLEWING |
    CLOCK_FLAG_BYPASS_DEPENDENCIES
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void *)"MPLL3",
    CLOCK_FLAG_SUPPORTS_SLEWING
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void *)"gcc_spmi_ahb_clk",
    CLOCK_FLAG_INTERNAL_CONTROL
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void *)"gcc_spmi_ser_clk",
    CLOCK_FLAG_INTERNAL_CONTROL
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void *)"SLEEPCLK",
    CLOCK_FLAG_SOURCE_NOT_CONFIGURABLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void *)"XO",
    CLOCK_FLAG_SOURCE_NOT_CONFIGURABLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void *)"GPLL0_DIV2",
    CLOCK_FLAG_SOURCE_NOT_CONFIGURABLE
  },

  {
    CLOCK_FLAG_NODE_TYPE_NONE,
    (void *)0,
    0
  }
};


/*
 * Stub flags.
 */
const ClockStubType ClockStubConfig =
{
  .bRUMI   = FALSE,
  .bVirtio = FALSE,
};

