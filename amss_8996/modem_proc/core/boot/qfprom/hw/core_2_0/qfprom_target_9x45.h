#ifndef QFPROM_TARGET_9X45_H
#define QFPROM_TARGET_9X45_H

/*===========================================================================

                        QFPROM  Driver Header  File

DESCRIPTION
 Contains target specific definitions used by 9x45.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright  2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/boot/qfprom/hw/core_2_0/qfprom_target_9x45.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/15/15   jz      Created (with 9x45 target specific defines here)
============================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/

/* Blow timer clock frequency in Mhz for 20nm SOC technology*/
#define QFPROM_BLOW_TIMER_CLK_FREQ_MHZ 4.8

/* Amount of time required to hold charge to blow fuse in micro-seconds */
#define QFPROM_FUSE_BLOW_TIME_IN_US            5

/*
* Configure QFPROM_ACCEL[QFPROM_TRIPPT_SEL] SW field to a value of 0x4 (011) to blow at a high resistance setting of 7 Kohms.
* Configure QFPROM_ACCEL[QFPROM_ACCEL] SW field to a value of 0x0.
* Leave the value of QFPROM_ACCEL[QFPROM_GATELAST] to a default value of 0x1 for 20SOC technology.
*/
#define QFPROM_ACCEL_VALUE             0xB00

/* QFPROM_ACCEL register default value */
#define QFPROM_ACCEL_RESET_VALUE       0xB00

/*
** Raw Address Start and End configuration to use for read/write operations.
** The start address is designed to be at the first supported region LSB address in the 
** memory map. The end address is designed to be the last supported region MSB address
** in the memory map.
*/
#define QFPROM_RAW_LOWEST_ADDRESS_OFFSET           (0x00000000 )
#define QFPROM_RAW_HIGHEST_ADDRESS_OFFSET          (0x000002E4 )

/* Macro for Read/write permission of corrected address */
#define QFPROM_READ_PERM_CORRECTED_ADDR		HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ADDR
#define QFPROM_WRITE_PERM_CORRECTED_ADDR	HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ADDR

// Added for 9x45 v2 support
#define QFPROM_RAW_LOWEST_ADDRESS_V2_OFFSET        (0x00000120 )
#define QFPROM_RAW_HIGHEST_ADDRESS_V2_OFFSET       (0x0000044C )

#define QFPROM_READ_PERM_CORRECTED_V2_ADDR         HWIO_QFPROM_CORR_RD_WR_PERM_LSB_V2_ADDR
#define QFPROM_WRITE_PERM_CORRECTED_V2_ADDR        HWIO_QFPROM_CORR_RD_WR_PERM_MSB_V2_ADDR

#endif /* QFPROM_TARGET_H */

