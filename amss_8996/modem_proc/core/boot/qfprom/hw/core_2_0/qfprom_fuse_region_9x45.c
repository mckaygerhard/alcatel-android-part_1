/*===========================================================================

                       QFPROM  Driver Source  Code

DESCRIPTION
 Contains target specific defintions and APIs to be used to read and write
 qfprom values.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright  2012 - 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/boot/qfprom/hw/core_2_0/qfprom_fuse_region_9x45.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/15/15   jz      Added runtime support for 9x45 V2 (new SECURITY_CONTROL_CORE block)
10/31/14   jz      Updated for 9x45
12/05/12   kedara  Initial revision for Qfprom driver in modem

============================================================================*/

/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include QFPROM_HWIO_REG_INCLUDE_H
#include QFPROM_TARGET_INCLUDE_H
#include "qfprom_target_common.h"
#include "qfprom_hwioreg_9x45_sec_ctrl_v2.h"

/*=============================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

=============================================================================*/
/*
**  Array containing QFPROM data items that can be read and associated
**  registers, mask and shift values for the same.
**
**  Note: The table is image specific based on requirements to support only
**  regions which are required by that image.
**
*/
/*---------------------------------------------------------------------------
  QFPROM REGIONS 
---------------------------------------------------------------------------*/
typedef enum
{
  QFPROM_JTAG_ID_REGION = 0,
  QFPROM_READ_WRITE_PERMISSION_REGION,
  QFPROM_ANTI_ROLLBACK_1_REGION,
  QFPROM_ANTI_ROLLBACK_2_REGION,
  QFPROM_ANTI_ROLLBACK_3_REGION,
  QFPROM_OEM_CONFIG_REGION,
  QFPROM_FEATURE_CONFIG_REGION,
  QFPROM_PRI_KEY_DERIVATION_KEY_REGION,
  QFPROM_SEC_KEY_DERIVATION_KEY_REGION,
  QFPROM_OEM_SEC_BOOT_REGION,
  QFPROM_QC_SEC_BOOT_REGION, 
  QFPROM_PK_HASH_REGION,
  QFPROM_CALIB_REGION,
  QFPROM_MEM_CONFIG_REGION,
  QFPROM_BOOT_ROM_PATCH,
  QFPROM_FEC_ENABLE_REGION,
  QFPROM_SPARE_REG16_REGION,
  QFPROM_SPARE_REG17_REGION,
  QFPROM_SPARE_REG18_REGION,
  QFPROM_SPARE_REG19_REGION,
  QFPROM_SPARE_REG20_REGION,
  QFPROM_SPARE_REG21_REGION,

  /* Add above this */
  QFPROM_LAST_REGION_DUMMY,
  QFPROM_MAX_REGION_ENUM                = 0x7FFF /* To ensure it's 16 bits wide */
} QFPROM_REGION_NAME;

const QFPROM_REGION_INFO qfprom_region_data[] =
{
    {
        QFPROM_ANTI_ROLLBACK_3_REGION,
        1,
        QFPROM_FEC_NONE,
        HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR,
        HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR,
        HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_BMSK,
        HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_BMSK,
        QFPROM_ROW_LSB,
        TRUE,
        4
    },

    {
        QFPROM_SPARE_REG20_REGION,
        1,
        QFPROM_FEC_63_56,
        HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR,
        HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR,
        HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE20_BMSK,
        HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE20_BMSK,
        QFPROM_ROW_LSB,
        TRUE,
        20
    },
};

uint32 qfprom_region_data_size = sizeof(qfprom_region_data)/sizeof(QFPROM_REGION_INFO);

// Added for 9x45 v2 support
typedef enum
  {
  QFPROM_JTAG_ID_REGION_V2 = 0,
  QFPROM_READ_WRITE_PERMISSION_REGION_V2,
  QFPROM_ANTI_ROLLBACK_1_REGION_V2,
  QFPROM_ANTI_ROLLBACK_2_REGION_V2,
  QFPROM_ANTI_ROLLBACK_3_REGION_V2,
  QFPROM_OEM_CONFIG_REGION_V2,
  QFPROM_FEATURE_CONFIG_REGION_V2,
  QFPROM_CM_FEATURE_CONFIG_REGION_V2,
  QFPROM_PRI_KEY_DERIVATION_KEY_REGION_V2,
  QFPROM_SEC_KEY_DERIVATION_KEY_REGION_V2,
  QFPROM_OEM_SEC_BOOT_REGION_V2,
  QFPROM_CALIB2_REGION_V2,
  QFPROM_PK_HASH_REGION_V2,
  QFPROM_CALIB_REGION_V2,
  QFPROM_MEM_CONFIG_REGION_V2,
  QFPROM_BOOT_ROM_PATCH_V2,
  QFPROM_FEC_ENABLE_REGION_V2,
  QFPROM_SPARE_REG18_REGION_V2,
  QFPROM_SPARE_REG19_REGION_V2,
  QFPROM_SPARE_REG20_REGION_V2,
  QFPROM_SPARE_REG21_REGION_V2,
  QFPROM_SPARE_REG22_REGION_V2,
  QFPROM_SPARE_REG23_REGION_V2,
  QFPROM_SPARE_REG24_REGION_V2,
  QFPROM_SPARE_REG25_REGION_V2,
  QFPROM_SPARE_REG26_REGION_V2,

  /* Add above this */
  QFPROM_LAST_REGION_DUMMY_V2,
  QFPROM_MAX_REGION_ENUM_V2                = 0x7FFF /* To ensure it's 16 bits wide */
} QFPROM_REGION_NAME_V2;

const QFPROM_REGION_INFO qfprom_region_data_v2[] =
{
    {
        QFPROM_ANTI_ROLLBACK_3_REGION_V2,
        1,
        QFPROM_FEC_NONE,
        HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_V2_ADDR,
        HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_V2_ADDR,
        HWIO_QFPROM_CORR_RD_WR_PERM_LSB_V2_ANTI_ROLLBACK_3_BMSK,
        HWIO_QFPROM_CORR_RD_WR_PERM_MSB_V2_ANTI_ROLLBACK_3_BMSK,
        QFPROM_ROW_LSB,
        TRUE,
        4
    },

    {
        QFPROM_SPARE_REG20_REGION_V2,
        1,
        QFPROM_FEC_63_56,
        HWIO_QFPROM_RAW_SPARE_REG20_LSB_V2_ADDR,
        HWIO_QFPROM_CORR_SPARE_REG20_LSB_V2_ADDR,
        HWIO_QFPROM_CORR_RD_WR_PERM_LSB_V2_SPARE20_BMSK,
        HWIO_QFPROM_CORR_RD_WR_PERM_MSB_V2_SPARE20_BMSK,
        QFPROM_ROW_LSB,
        TRUE,
        20
    },
};

uint32 qfprom_region_data_size_v2 = sizeof(qfprom_region_data_v2)/sizeof(QFPROM_REGION_INFO);

#define FAMILY_NUMBER_9x45      0x2002

/*===========================================================================
**  Function :    : qfprom_update_fusemap

** ==========================================================================
*/
/*!
*
* @brief :  This function updates the qfprom_region_data table pointer and other variables to support 9x45 V2 hw.
*
* @param  :
*   qfprom_fusemap_data *fusemap_data_ptr  - pointer to access the fusemap related variables

* @par Dependencies:
*
* @retval: None
*
*/
void qfprom_update_fusemap
( 
  qfprom_fusemap_data *fusemap_data_ptr
)
{
  uint32 hw_version = HWIO_TCSR_SOC_HW_VERSION_IN;

  /* Check HW_VERSION for V2 support */
  if (fusemap_data_ptr &&
      (((hw_version >> 16) & 0xFFFF) == FAMILY_NUMBER_9x45) && ((hw_version & 0xFFFF) >= 0x0200))
  {
    /* Update with target specific defines  */
    qfprom_region_ptr = (QFPROM_REGION_INFO *)qfprom_region_data_v2;
    qfprom_region_size = qfprom_region_data_size_v2;
	
    fusemap_data_ptr->sec_ctrl_base = SECURITY_CONTROL_V2_BASE;
    fusemap_data_ptr->sec_ctrl_base_phy = SECURITY_CONTROL_V2_BASE_PHYS;
    fusemap_data_ptr->raw_lowest_offset = QFPROM_RAW_LOWEST_ADDRESS_V2_OFFSET;
    fusemap_data_ptr->raw_highest_offset = QFPROM_RAW_HIGHEST_ADDRESS_V2_OFFSET;
    fusemap_data_ptr->read_perm_addr = QFPROM_READ_PERM_CORRECTED_V2_ADDR;
    fusemap_data_ptr->write_perm_addr = QFPROM_WRITE_PERM_CORRECTED_V2_ADDR;
    fusemap_data_ptr->fec_en_lsb_addr = HWIO_QFPROM_CORR_FEC_EN_LSB_V2_ADDR;
    fusemap_data_ptr->fec_en_msb_addr = HWIO_QFPROM_CORR_FEC_EN_MSB_V2_ADDR;
  }
}