/*
===========================================================================

FILE:         slimbus_bsp_data.c

DESCRIPTION:  This file implements the SLIMbus board support data.

===========================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.4.c3.11/buses/slimbus/config/slimbus_adsp_8936.c#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------- 
12/12/13   MJS     Initial revision for 8974 ADSP.

===========================================================================
             Copyright (c) 2013 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

#include "DALSys.h"
#include "HALhwio.h"
#include "SlimBus.h"
#include "DDITlmm.h"


/* Slimbus BSP data */
SlimBusBSPType SlimBusBSP[] =
{
  {
    2,
    "SLIMBUS",
    { 0x00, 0x00, 0x40, 0x01, 0x17, 0x02 },
    "ULTAUDIO_CORE",
    0x000c0000,
    0x077c0000,
    0x07784000,
    236,
    246,
    0,
    { DAL_GPIO_CFG(64, 2, DAL_GPIO_INPUT, DAL_GPIO_KEEPER, DAL_GPIO_8MA),
      DAL_GPIO_CFG(65, 2, DAL_GPIO_INPUT, DAL_GPIO_KEEPER, DAL_GPIO_8MA) },
    65,
    { 1, 1, 1 }
  }
};


