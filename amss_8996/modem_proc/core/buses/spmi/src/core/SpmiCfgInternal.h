/**
 * @file:  SpmiCfg.h
 * @brief: This module provides configuration options for the SPMI controller
 * 
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2016/03/28 23:02:17 $
 * $Header: //components/rel/core.mpss/3.4.c3.11/buses/spmi/src/core/SpmiCfgInternal.h#1 $
 * $Change: 10156097 $ 
 * 
 *                              Edit History
 * Date      Description
 * --------  -------------------------------------------------------------------
 * 1/20/15  Multiple bus support
 * 11/14/14  Initial Version
 */
#ifndef SPMICFGINTERNAL_H
#define	SPMICFGINTERNAL_H

#include "SpmiBusCfg.h"

//******************************************************************************
// Public API Functions
//******************************************************************************

boolean SpmiBusCfg_InDynamicChannelMode(uint8 busId);
Spmi_Result SpmiBusCfg_ConfigureChannel(uint8 busId, SpmiBusCfg_ChannelCfg* chanCfg, boolean autoAssignChan);

#endif
