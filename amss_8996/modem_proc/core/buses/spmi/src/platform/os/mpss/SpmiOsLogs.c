/**
 * @file:  SpmiOsLogs.c
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2016/03/28 23:02:17 $
 * $Header: //components/rel/core.mpss/3.4.c3.11/buses/spmi/src/platform/os/mpss/SpmiOsLogs.c#1 $
 * $Change: 10156097 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */

#include "SpmiOsLogs.h"

//******************************************************************************
// Global Data
//******************************************************************************

ULogHandle spmiLogHandle;
