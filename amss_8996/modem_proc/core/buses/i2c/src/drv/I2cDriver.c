/*=============================================================================

  FILE:   I2cDriver.c

  OVERVIEW:     This file contains the implementation of QUPI2CDriver API
  DEPENDENCIES: I2CDRV_AddDevice must be called first then other api's.
 
                Copyright c 2011 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
  ===========================================================================*/

/*=========================================================================
  EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.mpss/3.4.c3.11/buses/i2c/src/drv/I2cDriver.c#1 $
  $DateTime: 2016/03/28 23:02:17 $$Author: mplcsds1 $

  When     Who    What, where, why
  -------- ---    -----------------------------------------------------------
  09/26/11 LK     Created

  ===========================================================================*/

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/

#include "I2cDriver.h"
#include "I2cError.h"
#include "I2cDevice.h"
#include "DALSys.h"
#include "I2cLog.h"

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/

#define QUI2CDRV_REQUEST_QUEUE_SIZE                16


#define I2CDRV_VALIDATE_SYNC_TRAN(hDev,pTransfer,pClntCfg,puNumCompleted)\
     ( (NULL == hDev)?           I2CDRV_ERR_INVALID_DEV_HANDLE:           \
       (NULL == pTransfer)?      I2CDRV_ERR_INVALID_TRANSFER_POINTER:     \
       (NULL == puNumCompleted)? I2CDRV_ERR_INVALID_NUMCOMPLETED_POINTER: \
       I2C_RES_SUCCESS                                                    \
     )

#define I2CDRV_VALIDATE_SYNC_SEQ(hDev,pSequence,pClntCfg,puNumCompleted) \
     ( (NULL == hDev)?           I2CDRV_ERR_INVALID_DEV_HANDLE:           \
       (NULL == pSequence)?      I2CDRV_ERR_INVALID_SEQUENCE_POINTER:     \
       (NULL == puNumCompleted)? I2CDRV_ERR_INVALID_NUMCOMPLETED_POINTER: \
       I2C_RES_SUCCESS                                                    \
     )

#define I2CDRV_VALIDATE_ASYNC_TRAN(hDev,pTransfer,pClntCfg,pIoRes,pCallbackFn,pArg)\
     ( (NULL == hDev)?           I2CDRV_ERR_INVALID_DEV_HANDLE:           \
       (NULL == pTransfer)?      I2CDRV_ERR_INVALID_TRANSFER_POINTER:     \
       (NULL == pIoRes)?       I2CDRV_ERR_INVALID_TRAN_RESULT_POINTER:  \
       (NULL == pCallbackFn)?    I2CDRV_ERR_INVALID_CALLBACK_FN_POINTER:  \
       (NULL == pArg)?           I2CDRV_ERR_INVALID_CALLBACK_ARG_POINTER:  \
       I2C_RES_SUCCESS                                                    \
     )
#define I2CDRV_VALIDATE_ASYNC_SEQ(hDev,pSequence,pClntCfg,pIoRes,pCallbackFn,pArg)\
     ( (NULL == hDev)?           I2CDRV_ERR_INVALID_DEV_HANDLE:           \
       (NULL == pSequence)?      I2CDRV_ERR_INVALID_SEQUENCE_POINTER:     \
       (NULL == pIoRes)?       I2CDRV_ERR_INVALID_TRAN_RESULT_POINTER:  \
       (NULL == pCallbackFn)?    I2CDRV_ERR_INVALID_CALLBACK_FN_POINTER:  \
       (NULL == pArg)?           I2CDRV_ERR_INVALID_CALLBACK_ARG_POINTER:  \
       I2C_RES_SUCCESS                                                    \
     )

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/

enum  I2CDRV_Error
{
   I2CDRV_ERR_BASE = I2C_RES_ERROR_CLS_I2C_DRV,

   I2CDRV_ERR_FAILED_TO_ALLOCATE_DEVICE,
   I2CDRV_ERR_INVALID_DEVICE_ID,
   I2CDRV_ERR_FAILED_TO_REGISTER_WORKLOOP,

   I2CDRV_ERR_FAILED_TO_CREATE_DRV_SYNC_HANDLE,
   I2CDRV_ERR_FAILED_TO_CREATE_WORKLOOP_HANDLE,
   I2CDRV_ERR_FAILED_TO_CREATE_NOT_FULL_EVENT,
   I2CDRV_ERR_FAILED_TO_CREATE_QUE_SYNC_HANDLE,
   I2CDRV_ERR_FAILED_TO_CREATE_FREE_REQ_LIST_SYNC_HANDLE,
   I2CDRV_ERR_FAILED_TO_CREATE_ENQ_REQ_LIST_SYNC_HANDLE,
   I2CDRV_ERR_FAILED_TO_CREATE_ENQ_REQ_LIST_EVENT_HANDLE,
   I2CDRV_ERR_FAILED_TO_CREATE_ASYNC_WAIT_EVENT_HANDLE,
   I2CDRV_ERR_FAILED_TO_CREATE_REQ_EVENT,
   I2CDRV_ERR_FAILED_TO_CLEAR_REQ_EVENT,
   I2CDRV_ERR_FAILED_TO_DESTROY_REQ_EVENT,
   I2CDRV_ERR_FAILED_TO_WAIT_ON_REQ_EVENT,
   I2CDRV_ERR_FAILED_TO_CLEAR_DAL_EVENT,
   I2CDRV_ERR_FAILED_DAL_EVENT_WAIT,


   I2CDRV_ERR_FAILED_TO_ASSOCIATE_WORKLOOP_EVENT,
   
   I2CDRV_ERR_FAILED_TO_INIT_QUE_INVALID_STATE,
   I2CDRV_ERR_FAILED_TO_CREATE_DRVDEV_SYNC_HANDLE,
   I2CDRV_ERR_FAILED_TO_CLEAR_WAIT_ASYNC_DONE_EVENT,

   I2CDRV_ERR_FREE_REQ_LIST_EMPTY,

   I2CDRV_ERR_INVALID_DEV_HANDLE,
   I2CDRV_ERR_INVALID_TRANSFER_POINTER,
   I2CDRV_ERR_INVALID_SEQUENCE_POINTER,
   I2CDRV_ERR_INVALID_NUMCOMPLETED_POINTER,
   I2CDRV_ERR_INVALID_TRAN_RESULT_POINTER,
   I2CDRV_ERR_INVALID_CALLBACK_FN_POINTER,
   I2CDRV_ERR_INVALID_CALLBACK_ARG_POINTER,

};


typedef struct I2CDRV_DalEvt
{
   DALSYSEventHandle            hEvt;
   DALSYSEventObj               objEvt;
} I2CDRV_DalEvt;

typedef struct I2CDRV_DalWorkloop
{
   DALSYSWorkLoopHandle         hWL;
   DALSYSWorkLoopObj            objWL;
} I2CDRV_DalWorkloop;

typedef struct I2CDRV_DalSync
{
   DALSYSSyncHandle             hSync;
   DALSYS_SYNC_OBJECT(objSync);
} I2CDRV_DalSync;


typedef struct I2CDRV_Request I2CDRV_Request;
struct I2CDRV_Request
{
   enum I2CDRV_ReqApiEnumType
   {
      I2CDRV_ReqApiEnumRead,
      I2CDRV_ReqApiEnumWrite,
      I2CDRV_ReqApiEnumBatch
   } eReqApiEnum;

   enum I2CDRV_ReqSyncEnumType
   {
      I2CDRV_ReqSyncEnumSynchronous,
      I2CDRV_ReqSyncEnumAsynchronous
   } eReqSyncEnum;

   union I2CDRV_ReqUnion
   {
      struct _SyncRw
      {
         I2cTransfer                    *pTransfer;
         I2cClientConfig                *pClntCfg;
         uint32                         *puNumCompleted;
         int32                           retVal;
      } syncRw;
      struct _SyncBatch
      {
         I2cSequence                    *pSequence;
         I2cClientConfig                *pClntCfg;
         I2cIoResult                    *pIoRes;
         int32                           retVal;
      } syncBatch;
      struct _AsyncRw
      {
         I2cTransfer                    *pTransfer;
         I2cClientConfig                *pClntCfg;
         I2cIoResult                    *pIoRes;
         DRV_ASYNC_CB                    pCallbackFn;
         void                           *pArg;            

      } asyncRw;
      struct _AsyncBatch
      {
         I2cSequence                    *pSequence;
         I2cClientConfig                *pClntCfg;
         I2cIoResult                    *pIoRes;
         DRV_ASYNC_CB                    pCallbackFn;
         void                           *pArg;
      } asyncBatch;
   } reqVariant; 
   I2CDRV_DalEvt                    *pEvtReqComplete;
   I2CDRV_Request                   *pNext;
};

typedef enum I2CDRV_ReqQueueInitState
{
      I2CDRV_EnqReqListState_WorkLoopNotCreated = 0,
      I2CDRV_EnqReqListState_WorkLoopCreated,
      I2CDRV_EnqReqListState_InitDone,

}I2CDRV_ReqQueueInitState;



typedef struct I2CDRV_FreeRequestList I2CDRV_FreeRequestList;
struct I2CDRV_FreeRequestList
{
   I2CDRV_Request           *pHead;
   I2CDRV_DalSync            syncFreeReqList;
   uint32                    uNumFreeReqs;
};

typedef struct I2CDRV_EnqueuedRequestList I2CDRV_EnqueuedRequestList;
struct I2CDRV_EnqueuedRequestList
{
   I2CDRV_Request           *pHead; /* remove from head. */
   I2CDRV_Request           *pTail; /* add to tail. */
   I2CDRV_DalEvt             evtEnqReqListNotEmpty;
   I2CDRV_DalWorkloop        wlReqProcessing;
   I2CDRV_DalSync            syncEnqReqList;
   uint32                    uNumEnqReqs;
   I2CDRV_ReqQueueInitState eInitState;
};

typedef struct I2CDRV_RequestQueue
{
   /* TODO: request quesize to be definet in properties, req que allocated dynamically. */
   I2CDRV_Request                aRequests[QUI2CDRV_REQUEST_QUEUE_SIZE];
   I2CDRV_FreeRequestList        freeReqList;
   I2CDRV_EnqueuedRequestList    enqReqList;

   /* timed event wait implemented in os layer. */
   I2CDRV_DalEvt                 waitForAsyncDoneEvt;
}I2CDRV_RequestQueue;


typedef struct I2CDRV_DevType I2CDRV_DevType;
typedef struct I2CDRV_DevType* I2CDRV_PDevType;

struct I2CDRV_DevType
{
   I2CDRV_RequestQueue  devReqQueue;

   I2CDEV_HANDLE     hDev;
   uint32            uDevId;
   uint32            uOpenedClientCount;

   I2CLOG_DevType   *pDevLog;

   I2CDRV_DalSync    syncDrvDev;
   I2CDRV_DevType*   pNext;
};

/*-------------------------------------------------------------------------
* Static Variable Definitions
* ----------------------------------------------------------------------*/

static I2CDRV_DevType* pDrvDevHead;


/*-------------------------------------------------------------------------
* Static Function Declarations and Definitions
* ----------------------------------------------------------------------*/

static DALResult I2CDRV_ProcessNextTransaction(DALSYSEventHandle hEvent, void * pInDevCtxt);

/** @brief Searches for a device driver struct in the list.
  
    @param[in] uDevId    Platform id for the device.
    
    @return             Pointer to dev if found, otherwise NULL.
  */
static I2CDRV_DevType*
I2CDRV_SearchDevice
(
   uint32          uDevId 
)
{
   I2CDRV_DevType * pDevDrv = pDrvDevHead;
   while ( pDevDrv ) {
      if ( pDevDrv->uDevId == uDevId ) {
         return pDevDrv;
      }
      pDevDrv = pDevDrv->pNext;
   }

   return NULL;
}

/** @brief Links a device driver structure in the list.
  
    @param[in] pDevDrv  Pointer to device structure.
    
    @return             Nothing.
  */
static void
I2CDRV_LinkDevice
(
   I2CDRV_DevType* pDevDrv
)
{
   pDevDrv->pNext = pDrvDevHead;
   pDrvDevHead = pDevDrv;
}

/** @brief Initializes the request array.
  
    @param[in] pReqArr  Pointer to request array.
    @param[in] size     Array size.
    
    @return             Nothing.
  */
static void 
I2CDRV_InitRequestArray
(
   I2CDRV_Request *pReqArr,
   int32           size
)
{
   int32     arrIndex;
   
   if ( 0 == size ) {
      return;
   }

   pReqArr[size-1].pNext = NULL;
   for (arrIndex = size-2; arrIndex >= 0; arrIndex--) {
      pReqArr[arrIndex].pNext = &pReqArr[arrIndex+1];/* link the free request arrays. */
   }
}

/** @brief Initializes the free request array.
  
    @param[in] pDevDrv  Pointer to device structure.
    
    @return             I2C_RES_SUCCESS if successful, error
                        otherwise.
  */
int32
I2CDRV_InitFreeRequestList
(
   I2CDRV_PDevType pDevDrv
)
{
   I2CDRV_RequestQueue     *pReqQueue;
   I2CDRV_FreeRequestList  *pFreeReqList;
   DALResult                dalRes;
   
   pReqQueue = &pDevDrv->devReqQueue;
   pFreeReqList = &pReqQueue->freeReqList;
   dalRes = DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
                              &(pFreeReqList->syncFreeReqList.hSync),
                              &(pFreeReqList->syncFreeReqList.objSync));
   if ( DAL_SUCCESS != dalRes )   {
      return I2CDRV_ERR_FAILED_TO_CREATE_FREE_REQ_LIST_SYNC_HANDLE;
   }

   I2CDRV_InitRequestArray(pReqQueue->aRequests, QUI2CDRV_REQUEST_QUEUE_SIZE);
   
   pFreeReqList->pHead = pReqQueue->aRequests;
   pFreeReqList->uNumFreeReqs = QUI2CDRV_REQUEST_QUEUE_SIZE;

   return I2C_RES_SUCCESS;
}

/** @brief Get a request from the free request array.
  
    @param[in]  pDevDrv Pointer to device structure.
    @param[out] ppReq   Pointer to pointer to request structure.
    
    @return             I2C_RES_SUCCESS if successful, error
                        otherwise.
  */
int32
I2CDRV_GetFreeRequest
(
   I2CDRV_PDevType  pDevDrv,
   I2CDRV_Request **ppReq
)
{
   I2CDRV_FreeRequestList  *pFreeReqList;
   int32                    res;
   
   pFreeReqList = &pDevDrv->devReqQueue.freeReqList;

   DALSYS_SyncEnter(pFreeReqList->syncFreeReqList.hSync);

   if ( pFreeReqList->uNumFreeReqs > 0 ) { /* remove from list. */
      *ppReq = pFreeReqList->pHead;
      pFreeReqList->pHead = pFreeReqList->pHead->pNext;
      pFreeReqList->uNumFreeReqs--;
      res = I2C_RES_SUCCESS;
   }
   else {
      res = I2CDRV_ERR_FREE_REQ_LIST_EMPTY;
   }
   DALSYS_SyncLeave(pFreeReqList->syncFreeReqList.hSync);

   return res;
}

/** @brief Returns a request to the free request array.
  
    @param[in]  pDevDrv  Pointer to device structure.
    @param[in]  pReq     Pointer to request structure.
    
    @return              Nothing.
  */
void
I2CDRV_ReleaseFreeRequest
(
   I2CDRV_PDevType pDevDrv,
   I2CDRV_Request *pReq
)
{
   I2CDRV_FreeRequestList  *pFreeReqList;

   pFreeReqList = &pDevDrv->devReqQueue.freeReqList;
   DALSYS_SyncEnter(pFreeReqList->syncFreeReqList.hSync);
   pReq->pNext = pFreeReqList->pHead;
   pFreeReqList->pHead = pReq;
   pFreeReqList->uNumFreeReqs++;
   DALSYS_SyncLeave(pFreeReqList->syncFreeReqList.hSync);
}

/** @brief Initializes the enqueued request list.
  
    @param[in]  pDevDrv  Pointer to device structure.
    
    @return             I2C_RES_SUCCESS if successful, error
                        otherwise.
  */
int32
I2CDRV_InitEnqueuedRequestList
(
   I2CDRV_PDevType pDevDrv
)
{
   DALResult dalRes;
   I2CDRV_RequestQueue *pReqQueue = &pDevDrv->devReqQueue;
   I2CDRV_EnqueuedRequestList     *pEnqReqList = &pReqQueue->enqReqList;

   dalRes = DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
                              &(pEnqReqList->syncEnqReqList.hSync),
                              &(pEnqReqList->syncEnqReqList.objSync));
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_CREATE_ENQ_REQ_LIST_SYNC_HANDLE;
   }
   
   dalRes = DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT,
                               &pEnqReqList->evtEnqReqListNotEmpty.hEvt,
                               &pEnqReqList->evtEnqReqListNotEmpty.objEvt); 
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_CREATE_ENQ_REQ_LIST_EVENT_HANDLE;
   }

   dalRes = DALSYS_RegisterWorkLoop
                      (0, 10, &pEnqReqList->wlReqProcessing.hWL,
                       &pEnqReqList->wlReqProcessing.objWL);
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_REGISTER_WORKLOOP;
   }

   dalRes = DALSYS_AddEventToWorkLoop
          (pEnqReqList->wlReqProcessing.hWL,I2CDRV_ProcessNextTransaction,
           (void *) pDevDrv, pEnqReqList->evtEnqReqListNotEmpty.hEvt, NULL); 
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_ASSOCIATE_WORKLOOP_EVENT;
   }

   pEnqReqList->uNumEnqReqs  = 0;

   return I2C_RES_SUCCESS;
}

/** @brief Initializes the request queue.
  
    @param[in]  pDevDrv  Pointer to device structure.
    
    @return             I2C_RES_SUCCESS if successful, error
                        otherwise.
  */
int32
I2CDRV_InitRequestQueue
(
   I2CDRV_PDevType pDevDrv
)
{
   int32 res;
   DALResult dalRes;
   I2CDRV_RequestQueue *pReqQueue = &pDevDrv->devReqQueue;

   dalRes = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                               &pReqQueue->waitForAsyncDoneEvt.hEvt,
                               &pReqQueue->waitForAsyncDoneEvt.objEvt); 
   if (DAL_SUCCESS != dalRes) {
      return I2CDRV_ERR_FAILED_TO_CREATE_ASYNC_WAIT_EVENT_HANDLE;
   }

   res = I2CDRV_InitFreeRequestList(pDevDrv);
   if (I2C_RES_SUCCESS != res) {
     return res;
   }
   
   res = I2CDRV_InitEnqueuedRequestList(pDevDrv);
   return res;
}

/** @brief Enqueues the request.
  
    @param[in]  pDevDrv  Pointer to device structure.
    @param[in]  pReq     Pointer to request structure.
    
    @return             Nothing.
  */
void
I2CDRV_EnqueueRequest
(
   I2CDRV_PDevType pDevDrv,
   I2CDRV_Request *pReq
)
{
   I2CDRV_EnqueuedRequestList  *pEnqReqList;

   pEnqReqList = &pDevDrv->devReqQueue.enqReqList;
   pReq->pNext = NULL;
   DALSYS_SyncEnter(pEnqReqList->syncEnqReqList.hSync);
   pEnqReqList->uNumEnqReqs++;
   if ( 1 == pEnqReqList->uNumEnqReqs ) { /* first request, queue not empty. */
      pEnqReqList->pHead = pEnqReqList->pTail = pReq;
      /* signal processing thread that queue is not empty. */
      DALSYS_EventCtrl(pEnqReqList->evtEnqReqListNotEmpty.hEvt,
                       DALSYS_EVENT_CTRL_TRIGGER);
   }
   else {
      pEnqReqList->pTail->pNext = pReq;
      pEnqReqList->pTail = pReq;
   }
   DALSYS_SyncLeave(pEnqReqList->syncEnqReqList.hSync);
}

/** @brief Denqueues the request and sets the pointer to it.
  
    @param[in]  pDevDrv Pointer to device structure.
    @param[out] ppReq   Pointer to pointer to request structure.
    
    @return             Nothing.
  */
void
I2CDRV_DequeueRequest
(
   I2CDRV_PDevType pDevDrv,
   I2CDRV_Request **ppReq
)
{
   I2CDRV_EnqueuedRequestList  *pEnqReqList;
   I2CDRV_Request              *pReq;

   pEnqReqList = &pDevDrv->devReqQueue.enqReqList;

   DALSYS_SyncEnter(pEnqReqList->syncEnqReqList.hSync);
   if ( pEnqReqList->uNumEnqReqs == 0 ) {
      pReq = NULL;
   }
   else {
      pReq = pEnqReqList->pHead;
      pEnqReqList->pHead = pReq->pNext;
      pReq->pNext = NULL;
      pEnqReqList->uNumEnqReqs--;
      if ( NULL == pEnqReqList->pHead ) {
         pEnqReqList->pTail =pEnqReqList->pHead;// empty queue
      } 
   }
   DALSYS_SyncLeave(pEnqReqList->syncEnqReqList.hSync);
   *ppReq = pReq;
}

/** @brief Processes a synchronous transaction.
  
    @param[in]  pDevDrv  Pointer to device structure.
    @param[in]  pReq     Pointer to request structure.
    
    @return             Nothing.
  */
static void I2CDRV_ProcessSynchronousTransaction
(
   I2CDRV_PDevType    pDevDrv,
   I2CDRV_Request     *pReq
)
{
   DALResult              dalRes;

   I2C_CALLTRACE_LEVEL2(pDevDrv->pDevLog, 1,
                        "I2CDRV: I2CDRV_ProcessSynchronousTransaction pDev=0x%x",
                        pDevDrv);

   if ( I2CDRV_ReqApiEnumRead == pReq->eReqApiEnum ) {
      pReq->reqVariant.syncRw.retVal =
         I2CDEV_Read(pDevDrv->hDev,
                     pReq->reqVariant.syncRw.pTransfer,
                     pReq->reqVariant.syncRw.pClntCfg, 
                     pReq->reqVariant.syncRw.puNumCompleted);
   }
   else if ( I2CDRV_ReqApiEnumWrite == pReq->eReqApiEnum ) {
      pReq->reqVariant.syncRw.retVal =
         I2CDEV_Write(pDevDrv->hDev,
                      pReq->reqVariant.syncRw.pTransfer,
                      pReq->reqVariant.syncRw.pClntCfg, 
                      pReq->reqVariant.syncRw.puNumCompleted);
   }
   else if ( I2CDRV_ReqApiEnumBatch == pReq->eReqApiEnum ) {
      pReq->reqVariant.syncRw.retVal =
         I2CDEV_BatchTransfer(pDevDrv->hDev,
                              pReq->reqVariant.syncBatch.pSequence,
                              pReq->reqVariant.syncBatch.pClntCfg, 
                              pReq->reqVariant.syncBatch.pIoRes);
   }

   I2C_CALLTRACE_LEVEL2(
            pDevDrv->pDevLog, 1,
            "I2CDRV: I2CDRV_ProcessSynchronousTransaction EVTCTRL pDev=0x%x",
            pDevDrv);
   dalRes = DALSYS_EventCtrl(pReq->pEvtReqComplete->hEvt,
                             DALSYS_EVENT_CTRL_TRIGGER);
   I2C_CALLTRACE_LEVEL2(
         pDevDrv->pDevLog, 1,
         "I2CDRV: I2CDRV_ProcessSynchronousTransaction EXIT pDev=0x%x",
         pDevDrv);
   if ( DAL_SUCCESS != dalRes ) {
      /* TODO: LOG THIS EVENT */
   }
}

/** @brief Async completion callback.
  
    @param[in]  pArg  Pointer to argument.

    @return           Nothing.
  */
void
I2CDRV_ASYNC_CB
(
   void * pArg
)
{
   I2CDRV_RequestQueue *pReqQueue = (I2CDRV_RequestQueue *)pArg;
   DALSYS_EventCtrl(pReqQueue->waitForAsyncDoneEvt.hEvt,DALSYS_EVENT_CTRL_TRIGGER);
}

/** @brief Processes an asynchronous request.
  
    @param[in]  pDevDrv  Pointer to device structure.
    @param[in]  pReq     Pointer to request structure.
    
    @return              Nothing.
  */
static void
I2CDRV_ProcessAsynchronousTransaction
(
   I2CDRV_PDevType     pDevDrv,
   I2CDRV_Request     *pReq
)
{
   int32              res = I2C_RES_SUCCESS;
   DALResult          dalRes;
   I2CDRV_RequestQueue *pReqQueue = &pDevDrv->devReqQueue;
   
   do {
      if ( DAL_SUCCESS !=
            DALSYS_EventCtrl(pReqQueue->waitForAsyncDoneEvt.hEvt,
                             DALSYS_EVENT_CTRL_RESET) ) {
         res = I2CDRV_ERR_FAILED_TO_CLEAR_DAL_EVENT;
         break;
      }

      if ( I2CDRV_ReqApiEnumRead == pReq->eReqApiEnum ) {
         res = I2CDEV_AsyncRead(pDevDrv->hDev,
                                pReq->reqVariant.asyncRw.pTransfer,
                                pReq->reqVariant.asyncRw.pClntCfg, 
                                pReq->reqVariant.asyncRw.pIoRes,
                                I2CDRV_ASYNC_CB, pReqQueue);
      }
      else if ( I2CDRV_ReqApiEnumWrite == pReq->eReqApiEnum ) {
         res = I2CDEV_AsyncWrite(pDevDrv->hDev,
                                 pReq->reqVariant.asyncRw.pTransfer,
                                 pReq->reqVariant.asyncRw.pClntCfg, 
                                 pReq->reqVariant.asyncRw.pIoRes,
                                 I2CDRV_ASYNC_CB, pReqQueue);
      }
      else if ( I2CDRV_ReqApiEnumBatch == pReq->eReqApiEnum ) {
         res = I2CDEV_AsyncBatchTransfer(pDevDrv->hDev,
                                         pReq->reqVariant.asyncBatch.pSequence,
                                         pReq->reqVariant.asyncBatch.pClntCfg, 
                                         pReq->reqVariant.asyncBatch.pIoRes,
                                         I2CDRV_ASYNC_CB, pReqQueue);
      }
      if ( I2C_RES_SUCCESS != res ) {
         break;
      }
      dalRes = DALSYS_EventWait(pReqQueue->waitForAsyncDoneEvt.hEvt);
      if ( DAL_SUCCESS != dalRes ) {
         res = I2CDRV_ERR_FAILED_DAL_EVENT_WAIT;
         I2CDEV_CancelTransfer(pDevDrv->hDev);
	     break;
      }
   } while( 0 );
   
   if ( (I2CDRV_ReqApiEnumRead == pReq->eReqApiEnum) || 
        (I2CDRV_ReqApiEnumWrite == pReq->eReqApiEnum) ) {
       if ( I2C_RES_SUCCESS != res ) {
          pReq->reqVariant.asyncRw.pIoRes->nOperationResult = res;
       }
      pReq->reqVariant.asyncRw.pCallbackFn(pReq->reqVariant.asyncRw.pArg);
   }
   else {
      if ( I2C_RES_SUCCESS != res ) {
          pReq->reqVariant.asyncBatch.pIoRes->nOperationResult = res;
      }
      pReq->reqVariant.asyncBatch.pCallbackFn(pReq->reqVariant.asyncRw.pArg);
   }
   
   I2CDRV_ReleaseFreeRequest(pDevDrv, pReq);
}

/** @brief Dequeues and processes the transfer.
    
    This function is called from a workloop.
    
    @param[in]  hEvent     Event handle.
    @param[in]  pInDevCtxt Pointer to context.
    
    @return              DALResult.
  */
static DALResult
I2CDRV_ProcessNextTransaction
(
   DALSYSEventHandle hEvent,
   void * pInDevCtxt
)
{
   I2CDRV_Request     *pReq;
   I2CDRV_PDevType     pDevDrv = (I2CDRV_PDevType)pInDevCtxt;
   
   I2C_CALLTRACE_LEVEL2(pDevDrv->pDevLog, 1,
                        "I2CDRV_ProcessNextTransaction pDev=0x%x",
                        pDevDrv);

   I2CDRV_DequeueRequest(pDevDrv, &pReq);
   while ( NULL != pReq ) {
      if ( I2CDRV_ReqSyncEnumSynchronous == pReq->eReqSyncEnum ) {
         I2CDRV_ProcessSynchronousTransaction(pDevDrv, pReq);
      }
      else {
         I2CDRV_ProcessAsynchronousTransaction(pDevDrv, pReq);
      }
      I2CDRV_DequeueRequest(pDevDrv, &pReq);
   } 
   
   return DAL_SUCCESS;
}



/*-------------------------------------------------------------------------
 * Externalized Function Definitions
 * ----------------------------------------------------------------------*/

/** @brief Adds a device and sets the handle.
  
    @param[in]  uDevId  Pointer to device structure.
    @param[in]  phDev   Pointer to handle.
    
    @return             I2C_RES_SUCCESS if successful, error
                        otherwise.
  */
int32
I2CDRV_AddDevice
(
   uint32          uDevId,
   I2CDRV_HANDLE   *phDev
)
{
   DALResult     dalRes;
   int32 res = I2C_RES_SUCCESS;
   I2CDRV_PDevType pDevDrv;
   enum I2CDRV_AddDevState
   {
      I2CDRV_AddDevState_Malloc,
      I2CDRV_AddDevState_Inited,
   } eAddDevState;


   pDevDrv = I2CDRV_SearchDevice(uDevId);
   if ( pDevDrv ) {
      *phDev = (I2CDRV_HANDLE *)pDevDrv;
      return I2C_RES_SUCCESS;
   }
   
   dalRes = DALSYS_Malloc(sizeof(I2CDRV_DevType), (void **)&pDevDrv);
   if ( (DAL_SUCCESS != dalRes) || (NULL == pDevDrv) ) {
      return I2CDRV_ERR_FAILED_TO_ALLOCATE_DEVICE;
   }
   DALSYS_memset(pDevDrv, 0x0, sizeof(I2CDRV_DevType));

   pDevDrv->uDevId = uDevId;
   eAddDevState = I2CDRV_AddDevState_Malloc;
   do
   {
      dalRes = DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
                                 &(pDevDrv->syncDrvDev.hSync),
                                 &(pDevDrv->syncDrvDev.objSync));
      if ( DAL_SUCCESS != dalRes ) {
         res = I2CDRV_ERR_FAILED_TO_CREATE_DRVDEV_SYNC_HANDLE;
         break;
      }

      res = I2CDEV_Init((void *)pDevDrv->uDevId, &pDevDrv->hDev);
      if ( I2C_RES_SUCCESS != res ) {
        return res;
      }
      eAddDevState = I2CDRV_AddDevState_Inited;

      res = I2CDRV_InitRequestQueue(pDevDrv);
      if ( I2C_RES_SUCCESS != res ) {
         return res;
      }
      /* Init device logging. */
      I2cLog_Init((uint32)pDevDrv->uDevId, &pDevDrv->pDevLog);
      
      I2CDRV_LinkDevice(pDevDrv);
      *phDev = pDevDrv;
   } while ( 0 );

   if ( I2C_RES_SUCCESS != res ) {
      switch ( eAddDevState ) {
         case I2CDRV_AddDevState_Inited:
            I2CDEV_DeInit(pDevDrv->hDev);
         case I2CDRV_AddDevState_Malloc:
            DALSYS_Free(pDevDrv);
      }
   }
      
   return res;
}


/** @brief Initializes the driver.

    This Function Initializes the driver and creates the
    necessary data structures to support other calls into the
    driver.


    @return             I2C_RES_SUCCESS if successful, error
                        otherwise.
  */
int32 I2CDRV_Init(void)
{
   /*
   if (DAL_SUCCESS != 
         DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
                           &I2CDRV_DevList.syncDrv.hSync,
                           &I2CDRV_DevList.syncDrv.objSync )) {
      
      return I2CDRV_ERR_FAILED_TO_CREATE_DRV_SYNC_HANDLE;
   }
   */
   return I2C_RES_SUCCESS;
}

/** @brief Deinitializes the device.

    This Function Deinitializes the device and releases
    resources acquired during init.


    @return             I2C_RES_SUCCESS if successful, error
                        otherwise.
  */
int32
I2CDRV_DeInit(void)
{
   return I2C_RES_SUCCESS;
}

/**
    This function opens the device handle.

    @param[in]  hDev         Device handle.
    @param[in]  dwaccessMode Access mode.

    @return             I2C_RES_SUCCESS if successful, error
                        otherwise.
  */
int32
I2CDRV_Open
(
   I2CDRV_HANDLE hDev,
   uint32        dwaccessMode
)
{
   int32 res = I2C_RES_SUCCESS;
   I2CDRV_PDevType    pDevDrv = (I2CDRV_PDevType)hDev;

   /* add accounting. */
   DALSYS_SyncEnter(pDevDrv->syncDrvDev.hSync);

   if( 0 == pDevDrv->uOpenedClientCount ) {
      res = I2CDEV_SetPowerState(pDevDrv->hDev, I2CDEV_POWER_STATE_2);
   }
   if( I2C_RES_SUCCESS == res ) {
      pDevDrv->uOpenedClientCount++;
   }
 
   DALSYS_SyncLeave(pDevDrv->syncDrvDev.hSync);
 
   return res;
}/* I2CDRV_Open */

/**
    This function closes the device handle.

    @param[in]  hDev    Device handle.

    @return             I2C_RES_SUCCESS if successful, error
                        otherwise.
  */
int32
I2CDRV_Close
(
   I2CDRV_HANDLE hDev
)
{
   int32 res = I2C_RES_SUCCESS;
   I2CDRV_PDevType    pDevDrv = (I2CDRV_PDevType)hDev;

   // add accounting
   DALSYS_SyncEnter(pDevDrv->syncDrvDev.hSync);

   if( 1 == pDevDrv->uOpenedClientCount ) {
      res = I2CDEV_SetPowerState(pDevDrv->hDev, I2CDEV_POWER_STATE_0);
   }
   if( I2C_RES_SUCCESS == res ) {
      pDevDrv->uOpenedClientCount--;
   }
 
   DALSYS_SyncLeave(pDevDrv->syncDrvDev.hSync);
 
   return res;
}/* I2CDRV_Close */

/** @brief Read a buffer from i2c device.

    Read a buffer from i2c device.

    @param[in] hDev           Device handle.
    @param[in] pTransfer      Pointer to transfer.
    @param[in] pClntCfg       Pointer to Client configuration.
    @param[in] puNumCompleted Pointer to return completed reads.

    @return             I2C_RES_SUCCESS if successful, error
                        otherwise.
  */
int32
I2CDRV_Read
(
   I2CDRV_HANDLE                         hDev     ,
   I2cTransfer                          *pTransfer,
   I2cClientConfig                      *pClntCfg , /* if null keep previous. */
   uint32                               *puNumCompleted
)
{
   DALResult              dalRes;
   I2CDRV_Request     req;
   int32                  res;
   I2CDRV_DalEvt      reqEvt;
   I2CDRV_PDevType    pDevDrv = (I2CDRV_PDevType)hDev;

   res = I2CDRV_VALIDATE_SYNC_TRAN(hDev, pTransfer, pClntCfg, puNumCompleted);
   if ( I2C_RES_SUCCESS != res ) {
      return res;
   }

   dalRes = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                               &reqEvt.hEvt,
                               &reqEvt.objEvt); 
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_CREATE_REQ_EVENT;
   }
   dalRes = DALSYS_EventCtrl(reqEvt.hEvt, DALSYS_EVENT_CTRL_RESET);
   if ( dalRes != DAL_SUCCESS ) {
      return I2CDRV_ERR_FAILED_TO_CLEAR_REQ_EVENT;
   }
   req.eReqApiEnum                      = I2CDRV_ReqApiEnumRead;
   req.eReqSyncEnum                     = I2CDRV_ReqSyncEnumSynchronous;
   req.reqVariant.syncRw.pTransfer      = pTransfer;
   req.reqVariant.syncRw.pClntCfg       = pClntCfg;
   req.reqVariant.syncRw.puNumCompleted = puNumCompleted;
   req.pEvtReqComplete = &reqEvt;

   I2CDRV_EnqueueRequest(pDevDrv, &req);

   dalRes = DALSYS_EventWait(reqEvt.hEvt); /* wait for completion. */
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_WAIT_ON_REQ_EVENT;
   }
   dalRes = DALSYS_DestroyObject(reqEvt.hEvt); 
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_DESTROY_REQ_EVENT;
   }
   return req.reqVariant.syncRw.retVal;
} 

/** @brief Write a buffer to i2c device.

    Write a buffer to i2c device.

    @param[in] hDev            Device handle.
    @param[in] pTransfer       Pointer to transfer data.
    @param[in] pClntCfg        Pointer to Client configuration.
    @param[in] puNumCompleted  Pointer to return completed
          bytes.

    @return          I2C_RES_SUCCESS if successful, error
                     otherwise.
  */
int32
I2CDRV_Write
(
   I2CDRV_HANDLE                         hDev     ,
   I2cTransfer                          *pTransfer,
   I2cClientConfig                      *pClntCfg , /* if null keep previous. */
   uint32                               *puNumCompleted
)
{
   DALResult          dalRes;
   I2CDRV_Request     req;
   int32              res;
   I2CDRV_DalEvt      reqEvt;
   I2CDRV_PDevType    pDevDrv = (I2CDRV_PDevType)hDev;

   res = I2CDRV_VALIDATE_SYNC_TRAN(hDev, pTransfer, pClntCfg, puNumCompleted);
   if ( I2C_RES_SUCCESS != res ) {
      return res;
   }
   I2C_CALLTRACE_LEVEL2(pDevDrv->pDevLog, 1,
                        "I2CDRV: I2CDRV_BatchTransfer ENTRY pDev=0x%x",
                        pDevDrv);

   dalRes = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                               &reqEvt.hEvt,
                               &reqEvt.objEvt); 
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_CREATE_REQ_EVENT;
   }
   dalRes = DALSYS_EventCtrl(reqEvt.hEvt, DALSYS_EVENT_CTRL_RESET);
   if ( dalRes != DAL_SUCCESS ) {
      return I2CDRV_ERR_FAILED_TO_CLEAR_REQ_EVENT;
   }

   req.eReqApiEnum                      = I2CDRV_ReqApiEnumWrite;
   req.eReqSyncEnum                     = I2CDRV_ReqSyncEnumSynchronous;
   req.reqVariant.syncRw.pTransfer      = pTransfer;
   req.reqVariant.syncRw.pClntCfg       = pClntCfg;
   req.reqVariant.syncRw.puNumCompleted = puNumCompleted;
   req.pEvtReqComplete = &reqEvt;

   I2CDRV_EnqueueRequest(pDevDrv, &req);

   dalRes = DALSYS_EventWait(reqEvt.hEvt); /* wait for completion. */
   I2C_CALLTRACE_LEVEL2(pDevDrv->pDevLog, 1,
                        "I2CDRV: I2CDRV_BatchTransfer()=0x%x",
                        pDevDrv);
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_WAIT_ON_REQ_EVENT;
   }
   dalRes = DALSYS_DestroyObject(reqEvt.hEvt); 
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_DESTROY_REQ_EVENT;
   }
   return req.reqVariant.syncRw.retVal;
}

/** @brief Does a batch of transfers in a sequence.

    Does a batch of transfers in a sequence.

    @param[in] hDev           Device handle.
    @param[in] pSequence      Pointer to a sequence of transfer
          data.
    @param[in] pClntCfg       Pointer to Client configuration.
    @param[in] pIoRes         Pointer to io result.

    @return          I2C_RES_SUCCESS if successful, error
                     otherwise.
  */
int32
I2CDRV_BatchTransfer
(
   I2CDRV_HANDLE                         hDev     ,
   I2cSequence                          *pSequence,
   I2cClientConfig                      *pClntCfg , /* if null keep previous. */
   I2cIoResult                          *pIoRes
)
{
   DALResult          dalRes;
   I2CDRV_Request     req;
   int32              res;
   I2CDRV_DalEvt      reqEvt;
   I2CDRV_PDevType    pDevDrv = (I2CDRV_PDevType)hDev;

   res = I2CDRV_VALIDATE_SYNC_SEQ(hDev,pSequence,pClntCfg,pIoRes);
   if ( I2C_RES_SUCCESS != res ) {
      return res;
   }

   I2C_CALLTRACE_LEVEL2(pDevDrv->pDevLog, 1,
                        "I2CDRV: I2CDRV_BatchTransfer ENTRY pDev=0x%x",
                        pDevDrv);

   dalRes = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                               &reqEvt.hEvt,
                               &reqEvt.objEvt); 
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_CREATE_REQ_EVENT;
   }
   dalRes = DALSYS_EventCtrl(reqEvt.hEvt, DALSYS_EVENT_CTRL_RESET);
   if ( dalRes != DAL_SUCCESS ) {
      return I2CDRV_ERR_FAILED_TO_CLEAR_REQ_EVENT;
   }
   req.eReqApiEnum                         = I2CDRV_ReqApiEnumBatch;
   req.eReqSyncEnum                        = I2CDRV_ReqSyncEnumSynchronous;
   req.reqVariant.syncBatch.pSequence      = pSequence;
   req.reqVariant.syncBatch.pClntCfg       = pClntCfg;
   req.reqVariant.syncBatch.pIoRes         = pIoRes;
   req.pEvtReqComplete = &reqEvt;

   I2CDRV_EnqueueRequest(pDevDrv, &req);

   dalRes = DALSYS_EventWait(reqEvt.hEvt); /* wait for completion. */

   I2C_CALLTRACE_LEVEL2(pDevDrv->pDevLog, 1,
                        "I2CDRV: I2CDRV_BatchTransfer EXIT pDev=0x%x",
                        pDevDrv);

   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_WAIT_ON_REQ_EVENT;
   }
   dalRes = DALSYS_DestroyObject(reqEvt.hEvt); 
   if ( DAL_SUCCESS != dalRes ) {
      return I2CDRV_ERR_FAILED_TO_DESTROY_REQ_EVENT;
   }
   return req.reqVariant.syncBatch.retVal;
}



/** @brief Schedules a buffer read from i2c device.

    Schedules a buffer read from i2c device.
    Once the read is complete or an error occurs
    the callback will be called.

    @param[in] hDev           Device handle.
    @param[in] pTransfer      Pointer to transfer.
    @param[in] pClntCfg       Pointer to Client configuration.
    @param[in] pIoRes         Pointer to returned result of the
                               transfer.
    @param[in] pCallbackFn    Pointer to a callback function to
                               be called when transfer finishes
                               or aboarded.
    @param[in] pArg           Pointer to be passed to the
                               callback function.

    @return          I2C_RES_SUCCESS if queuing successful,
                     error otherwise.
  */
int32
I2CDRV_AsyncRead
(
   I2CDRV_HANDLE                         hDev               ,
   I2cTransfer                          *pTransfer          ,
   I2cClientConfig                      *pClntCfg, /* if null keep previous. */
   I2cIoResult                          *pIoRes             ,
   DRV_ASYNC_CB                          pCallbackFn        ,
   void                                 *pArg            
)
{
   I2CDRV_Request     *pReq;
   int32               res;
   I2CDRV_PDevType     pDevDrv = (I2CDRV_PDevType)hDev;

   res = I2CDRV_VALIDATE_ASYNC_TRAN(hDev, pTransfer, pClntCfg, pIoRes, pCallbackFn, pArg);
   if ( I2C_RES_SUCCESS != res ) {
      return res;
   }
   res = I2CDRV_GetFreeRequest(pDevDrv, &pReq);
   if ( I2C_RES_SUCCESS != res ) {
      return res;
   }

   pReq->eReqApiEnum                    = I2CDRV_ReqApiEnumRead;
   pReq->eReqSyncEnum                   = I2CDRV_ReqSyncEnumAsynchronous;
   pReq->reqVariant.asyncRw.pTransfer   = pTransfer;
   pReq->reqVariant.asyncRw.pClntCfg    = pClntCfg;
   pReq->reqVariant.asyncRw.pIoRes      = pIoRes;
   pReq->reqVariant.asyncRw.pCallbackFn = pCallbackFn;
   pReq->reqVariant.asyncRw.pArg        = pArg;

   I2CDRV_EnqueueRequest(pDevDrv, pReq);
   return I2C_RES_SUCCESS;
} 

/** @brief Schedules a buffer write to i2c device.

    Schedules a buffer write to i2c device. Once the write is
    complete or an error occurs the callback will be called.

    @param[in] hDev           Device handle.
    @param[in] pTransfer      Pointer to transfer.
    @param[in] pClntCfg       Pointer to Client configuration.
    @param[in] pIoRes       Pointer to returned result of the
                               transfer.
    @param[in] pCallbackFn    Pointer to a callback function to
                               be called when transfer finishes
                               or aboarded.
    @param[in] pArg           Pointer to be passed to the
                               callback function.

    @return          I2C_RES_SUCCESS if successful, error
                     otherwise.
  */
int32
I2CDRV_AsyncWrite
(
   I2CDRV_HANDLE                         hDev               ,
   I2cTransfer                          *pTransfer          ,
   I2cClientConfig                      *pClntCfg,/* if null keep previous. */
   I2cIoResult                          *pIoRes           ,
   DRV_ASYNC_CB                          pCallbackFn        ,
   void                                 *pArg            
)
{
   I2CDRV_Request     *pReq;
   int32               res;
   I2CDRV_PDevType    pDevDrv = (I2CDRV_PDevType)hDev;

   res = I2CDRV_VALIDATE_ASYNC_TRAN(hDev,pTransfer,pClntCfg,pIoRes,pCallbackFn,pArg);
   if ( I2C_RES_SUCCESS != res ) {
      return res;
   }
   res = I2CDRV_GetFreeRequest(pDevDrv, &pReq);
   if ( I2C_RES_SUCCESS != res ) {
      return res;
   }

   pReq->eReqApiEnum                    = I2CDRV_ReqApiEnumWrite;
   pReq->eReqSyncEnum                   = I2CDRV_ReqSyncEnumAsynchronous;
   pReq->reqVariant.asyncRw.pTransfer   = pTransfer;
   pReq->reqVariant.asyncRw.pClntCfg    = pClntCfg;
   pReq->reqVariant.asyncRw.pIoRes      = pIoRes;
   pReq->reqVariant.asyncRw.pCallbackFn = pCallbackFn;
   pReq->reqVariant.asyncRw.pArg        = pArg;

   I2CDRV_EnqueueRequest(pDevDrv, pReq);
   return I2C_RES_SUCCESS;
}

/** @brief Schedules a batch of transfers in a sequence.

    Schedulest a batch of transfers in a sequence and returns.
    The callback will be called to notify transfer is done or
    has failed.

    @param[in] hDev           Device handle.
    @param[in] pSequence      Pointer to the sequence of
          transfers.
    @param[in] pClntCfg       Pointer to Client configuration.
    @param[in] pIoRes         Pointer to returned result of the
                               transfer.
    @param[in] pCallbackFn    Pointer to a callback function to
                               be called when transfer finishes
                               or aboarded.
    @param[in] pArg           Pointer to be passed to the
                               callback function.

    @return          I2C_RES_SUCCESS if queuing successful,
                     error otherwise.
  */
int32
I2CDRV_AsyncBatchTransfer
(
   I2CDRV_HANDLE                         hDev               ,
   I2cSequence                          *pSequence          ,
   I2cClientConfig                      *pClntCfg,// if null keep previous
   I2cIoResult                          *pIoRes           ,
   DRV_ASYNC_CB                          pCallbackFn        ,
   void                                 *pArg            
)
{
   I2CDRV_Request     *pReq;
   int32               res;
   I2CDRV_PDevType     pDevDrv = (I2CDRV_PDevType)hDev;

   res = I2CDRV_VALIDATE_ASYNC_SEQ(hDev,pSequence,pClntCfg,pIoRes,pCallbackFn,pArg);
   if ( I2C_RES_SUCCESS != res ) {
      return res;
   }
   res = I2CDRV_GetFreeRequest(pDevDrv, &pReq);
   if ( I2C_RES_SUCCESS != res ) {
      return res;
   }

   pReq->eReqApiEnum                       = I2CDRV_ReqApiEnumBatch;
   pReq->eReqSyncEnum                      = I2CDRV_ReqSyncEnumAsynchronous;
   pReq->reqVariant.asyncBatch.pSequence   = pSequence;
   pReq->reqVariant.asyncBatch.pClntCfg    = pClntCfg;
   pReq->reqVariant.asyncBatch.pIoRes      = pIoRes;
   pReq->reqVariant.asyncBatch.pCallbackFn = pCallbackFn;
   pReq->reqVariant.asyncBatch.pArg        = pArg;

   I2CDRV_EnqueueRequest(pDevDrv, pReq);
   return I2C_RES_SUCCESS;
}





