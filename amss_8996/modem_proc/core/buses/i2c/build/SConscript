#===============================================================================
#
# I2C Libs
#
# GENERAL DESCRIPTION
#    Public build script for I2C BUS driver.
#
# Copyright (c) 2009-2012 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.4.c3.11/buses/i2c/build/SConscript#1 $
#  $DateTime: 2016/03/28 23:02:17 $
#  $Author: mplcsds1 $
#  $Change: 10156097 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 11/11/14   np      Added 8996 support
# 02/09/12   lk      Added logging level macro.
# 02/09/12   lk      Added device inc path.
# 02/09/12   lk      Added configurable properties file.
# 02/09/12   ag      Fixed the location where the object files are built.
# 01/21/12   ag		 Initial release
#
#===============================================================================
Import('env')
#-------------------------------------------------------------------------------
# Load sub scripts
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()


#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
env = env.Clone()
#print env
# Additional defines
env.Append(CPPDEFINES = ["FEATURE_LIBRARY_ONLY"])   

SRCPATH = "../src"

IMAGES = []
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 
CBSP_APIS = []
I2C_CONFIG_XML = {}

#-------------------------------------------------------------------------------
# Publish Private APIs
#-------------------------------------------------------------------------------
env.PublishPrivateApi('BUSES_I2C_DEVICE', [
   '${INC_ROOT}/core/buses/i2c/src/device/inc',
   '${INC_ROOT}/core/buses/i2c/src/logs/inc',
   '${INC_ROOT}/core/buses/qup/src/logs/inc',
   '${INC_ROOT}/core/buses/qup/inc',
   '${INC_ROOT}/core/buses/qup/hw',
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_APIS += [
   'BUSES',
   'DAL',
   'HAL',
   'SYSTEMDRIVERS',
   'HWENGINES',
   'KERNEL',   
   'SERVICES',
   'POWER'
]

env.RequirePublicApi(CBSP_APIS)
env.RequireRestrictedApi(CBSP_APIS)

if env['MSM_ID'] in ['8974']:
   if env.has_key('ADSP_PROC'):
      IMAGES = ['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE']
elif env['MSM_ID'] in ['9x25']:
   if env.has_key('ADSP_PROC'):
      IMAGES = ['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE']
   elif env.has_key('APPS_PROC'):
      IMAGES = ['APPS_IMAGE',      'CBSP_APPS_IMAGE']
   elif env.has_key('MODEM_PROC') and env.has_key('USES_I2C_ON_MODEM'):
      IMAGES = ['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE']
   else:
      Return();
elif env['MSM_ID'] in ['8x26']:
   if env.has_key('ADSP_PROC'):
      IMAGES = ['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE']
elif env['MSM_ID'] in ['9x45']:
   if env.has_key('MODEM_PROC'):
      IMAGES = ['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE']
elif env['MSM_ID'] in ['8996']:
   if env.has_key('MODEM_PROC'):
      IMAGES = ['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE']
else:
   Return();      

if env.has_key('ADSP_PROC'):
   I2C_CONFIG_XML = {
                      '8974_xml' : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8974.xml'],
                      '9625_xml' : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_9625.xml'],
                      '8626_xml' : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8x26.xml']
                    }
elif env.has_key('APPS_PROC'):
   I2C_CONFIG_XML = {'9625_xml' : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_9625.xml']}
elif env.has_key('MODEM_PROC'):
   I2C_CONFIG_XML = {
                     '9625_xml' : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_mpss_mdm9625.xml'],
                     '9645_xml' : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_mpss_mdm9645.xml'],
                     '8996_xml' : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_mpss_mdm8996.xml']
                    }


env.Append(CPPDEFINES = ["I2CLOG_LEVEL_COMPILE=I2CLOG_LEVEL_2"])

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
I2C_DEVICE_GLOB_FILES = env.GlobFiles('../src/*/*.c', posix=True)
 
#GLOB returns the relative path name, it needs to replaced with correct build location
I2C_DEVICE_SOURCES = [path.replace(SRCPATH, '${BUILDPATH}') for path in I2C_DEVICE_GLOB_FILES]

#-------------------------------------------------------------------------------
# Add Libraries to image
# env.AddLibrary is a new API, only if the IMAGES is valid in the build env
# the objects will built and added to the image.
#-------------------------------------------------------------------------------
env.AddLibrary(IMAGES, '${BUILDPATH}/I2cDriver', I2C_DEVICE_SOURCES)

#---------------------------------------------------------------------------
# Device Config
#---------------------------------------------------------------------------
if 'USES_DEVCFG' in env:
#   DEVCFG_IMG = ['DAL_DEVCFG_IMG']
#   env.AddDevCfgInfo(DEVCFG_IMG, 
   env.AddDevCfgInfo(['DAL_DEVCFG_IMG'], I2C_CONFIG_XML )

#   {
#      'devcfg_xml'    : '${BUILD_ROOT}/core/buses/i2c/config/${I2C_CONFIG_XML}'
#   })


