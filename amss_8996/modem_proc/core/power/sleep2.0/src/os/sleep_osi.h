#ifndef SLEEP_OSI_H
#define SLEEP_OSI_H
/*==============================================================================
  FILE:           sleep_osi.h

  OVERVIEW:       This file contains sleep internal declarations of functions 
                  that are used in the common main sleep subsystem

  DEPENDENCIES: None

                Copyright (c) 2013-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.mpss/3.4.c3.11/power/sleep2.0/src/os/sleep_osi.h#1 $
$DateTime: 2016/03/28 23:02:17 $
==============================================================================*/
#include "DALStdDef.h"

/*==============================================================================
                            GLOBAL DEFINITIONS
 =============================================================================*/
/**
 * sleepOS_IdleModeType
 *
 * @brief Enumeration to indicate the Idle Mode Type
 */
typedef enum
{
  SLEEP_OS_IDLE_MODE_DEFAULT   = 0,   /* Configure for single threaded sleep */
  SLEEP_OS_IDLE_MODE_OVERRIDE  = 1,   /* Configure for low overhead sleep    */
  SLEEP_OS_IDLE_MODE_SWFI_ONLY = 
    SLEEP_OS_IDLE_MODE_OVERRIDE   ,   /* Where override is not available     */ 
  SLEEP_OS_IDLE_MODE_USLEEP    = 2,   /* Configure for uImage based sleep    */
  SLEEP_OS_IDLE_MODE_HOLD_OFF  = 3,   /* Configure for busy wait during idle */
  SLEEP_OS_IDLE_MODE_NUM_STATES
} sleepOS_IdleModeType;

/** 
 * sleepOS_threadType
 *  
 *  @brief Enum to identify thread used by sleep
 */
typedef enum
{
  SLEEP_THREAD = 0,
  SLEEP_SOLVER_THREAD,
  PMI_HANDLER_THREAD,
  SLEEP_THREAD_MAX,
}sleepOS_threadType;

/** 
 * sleepThreadInfoType
 *  
 *  @brief Structure for capturing info of threads used by sleep
 */
typedef struct sleep_thread_info_s
{
  uint32 thread_id;
  uint32 thread_priority;
}sleepOS_threadInfoType;

/*==============================================================================
                           GLOBAL FUNCTION DECLARATIONS
 =============================================================================*/
/**
 * sleepOS_configIdleMode
 * 
 * @brief This function configures how processor idle is handled within sleep.
 * 
 * @param idleMode: Idle time behavior/configuration for next cycle.
 */
void sleepOS_configIdleMode(sleepOS_IdleModeType idleMode);

/**
 * sleepOS_getFrequency
 *
 * @brief A query function to return cpu clock frequency in KHz.
 *
 * @return CPU frequency in KHz.
 */
uint32 sleepOS_getFrequency(void);

/** 
 * sleepOS_updateThreadInfo() 
 *  
 * @brief This function is used to update thread info for 
 *        threads owned by sleep
 *  
 * @param Enum ID corresponding to thread owned by sleep 
 */
void sleepOS_updateThreadInfo(sleepOS_threadType threadID);

/** 
 * sleepOS_getThreadPriority() 
 *  
 * @brief This function is used to update task info for tasks 
 *        owned by sleep team
 *  
 * @param Enum ID corresponding to thread owned by sleep 
 *  
 * @return Returns the stored priority value 
 */
uint32 sleepOS_getThreadPriority(sleepOS_threadType threadID);

/** 
 * sleepOS_getThreadID() 
 *  
 * @brief This function is used to update task info for tasks owned by sleep team 
 *  
 * @param Enum ID corresponding to thread owned by sleep 
 *  
 * @return Returns the stored thread ID value 
 */
uint32 sleepOS_getThreadID(sleepOS_threadType threadID);

/**
 * sleepOS_setSolverTaskPriority
 * 
 * @brief Set the sleepsolver task priority value equal to the 
 *        sleep task priority value
 */                       
void sleepOS_setSolverTaskPriority( void );

/**
 * sleepOS_restoreSolverTaskPriority
 * 
 * @brief Restore the sleepsolver task priority value equal to 
 *        the initial sleepsolver task priority value
 */                       
void sleepOS_restoreSolverTaskPriority( void );
#endif /* SLEEP_OSI_H */
