#ifndef SLEEP_LPRI_H
#define SLEEP_LPRI_H
/*==============================================================================
  FILE:         sleep_lpri.h
  
  OVERVIEW:     This file provides the internal functions and data used for
                sleep_lpr functionality.

  DEPENDENCIES: None

                Copyright (c) 2010-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.mpss/3.4.c3.11/power/sleep2.0/src/sleep_lpri.h#1 $
$DateTime: 2016/03/28 23:02:17 $
==============================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include "npa_resource.h"

/*==============================================================================
                             MACRO DEFINITIONS
 =============================================================================*/
/**
 * @brief SLEEP_LPR_QUERY_RESOURCES
 *
 * ID for querying the list of registered LPRs.  List will persist in a valid
 * state until returned using SLEEP_LPR_RELEASE_RESOURCE_LIST.
 */
#define SLEEP_LPR_QUERY_RESOURCES (NPA_QUERY_RESERVED_END+1)

/**
 * @brief SLEEP_LPR_REGISTER
 *
 * Internal query.
 */
#define SLEEP_LPR_REGISTER        (NPA_QUERY_RESERVED_END+2)

/*==============================================================================
                             DATA DECLARATIONS
 =============================================================================*/
/**
 * gSleepNumOfFreqs
 *
 * @brief Number of static frequencies associated with the number of mLUTs
 */
extern volatile uint32 g_SleepNumOfFreqs;

#ifdef __cplusplus
}
#endif

#endif /* SLEEP_LPRI_H */

