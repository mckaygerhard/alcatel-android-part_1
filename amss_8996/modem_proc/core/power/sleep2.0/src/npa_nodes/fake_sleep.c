/*=============================================================================
  FILE:         fake_sleep.c

  OVERVIEW:     This file provides npa node implementation for fake sleep
                feature and some functionalities associated with that feature.

  DEPENDENCIES: Requires support from RPM driver to handshake with RPM outside
                of SPM sequence to trigger necessary set transitions. This is
                not expected to be present on all targets.

                Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
==============================================================================*/

#ifdef ENABLE_FEATURE_FAKE_SLEEP

#include <stdlib.h>
#include "npa.h"
#include "npa_resource.h"
#include "rpmclient.h"
#include "rpm_fake_sleep.h"
#include "vmpm_utils.h"
#include "CoreVerify.h"
#include "CoreTime.h"
#include "sleep_log.h"
#include "sleep_npa_scheduler.h"

/*==============================================================================
                             INTERNAL MACROS
 =============================================================================*/

/**
 * @brief Fake sleep resource name.
 */
#define FAKE_SLEEP_RESOURCE_NAME "/core/cpu/fake_sleep"

/**
 * @brief Macro to map npa custom client type to more meaningful client type.
 */
#define NPA_CLIENT_FAKE_SLEEP_EXIT NPA_CLIENT_CUSTOM1

/**
 * @brief Time in ticks to perform fake sleep entry. This will be used along
 *        with exit time to determine if we have sufficient time to perform
 *        fake sleep.
 *
 * @note This is just an estimated value based on set transition time. It
 *       needs to be profiled on actual target.
 */
#define FAKE_SLEEP_ENTER_TICKS 19200

/**
 * @brief Time in ticks to exit from fake sleep state. This is to be used
 *        with entry time to determine if we have sufficient time for
 *        fake sleep.
 *
 * @see FAKE_SLEEP_ENTER_TICKS
 */
#define FAKE_SLEEP_EXIT_TICKS 19200

/**
 * @brief Minimum time in ticks required to perform fake sleep entry and exit. 
 *
 * @see FAKE_SLEEP_ENTER_TICKS, FAKE_SLEEP_EXIT_TICKS
 */
#define FAKE_SLEEP_THRESHOLD_TICKS \
        (FAKE_SLEEP_ENTER_TICKS + FAKE_SLEEP_EXIT_TICKS)

/**
 * @brief Macro to indicate time in ticks to re-enter or re-trigger 
 *        (exit/enter/exit) fake sleep. This is used to determine if we
 *        should re-trigger fake sleep when wakeup deadline changes and 
 *        system is already in fake sleep (semi-active) state.
 *
 * @see FAKE_SLEEP_THRESHOLD_TICKS
 */
#define FAKE_SLEEP_REENTRY_THRESHOLD_TICKS \
        (FAKE_SLEEP_EXIT_TICKS + FAKE_SLEEP_THRESHOLD_TICKS)

/*==============================================================================
                                INTERNAL TYPES
 =============================================================================*/

/**
 * @brief Enumeration to indicate various states of fake sleep resource.
 *
 * FAKE_SLEEP_STATE_ACTIVE
 *   Fake sleep resource state is active i.e. from RPM perspective,
 *   subsystem is active.
 *
 * FAKE_SLEEP_STATE_SEMIACTIVE 
 *   Resource state is semi active or fake sleep i.e. from RPM perspective,
 *   subsystem is asleep.
 *
 * FAKE_SLEEP_STATE_DEADLINE_CHANGED
 *   Intermediate state of the fake sleep resource to indicate that wakeup
 *   deadline is changed and RPM needs to be updated. It is just an internal 
 *   state to handle proper state transition and is not visible outside of 
 *   resource. During this state, RPM sees subsystem as asleep.
 */
enum
{
  FAKE_SLEEP_STATE_ACTIVE = 0,
  FAKE_SLEEP_STATE_SEMIACTIVE,
  FAKE_SLEEP_STATE_DEADLINE_CHANGED,
  FAKE_SLEEP_STATE_MAX = FAKE_SLEEP_STATE_DEADLINE_CHANGED
};

/*==============================================================================
                              INTERNAL VARIABLES
 =============================================================================*/

/**
 * @brief Internal NPA client that will be used to trigger state update on
 *        resource when subsystem exits fake sleep outside of that resource. 
 */
static npa_client_handle g_fakeSleepExitClient = NULL;

/**
 * @brief Absolute wakeup time in ticks sent to RPM while entering fake sleep.
 *        This variable must maintain valid wakeup tick if resource state
 *        is SEMIACTIVE else it must be 0.
 */
static uint64 g_fakeSleepWakeupTick = 0;

/*==============================================================================
                       INTERNAL FUNCTION DEFINITIONS
 =============================================================================*/

/**
 * @brief Callback function that gets called when subsystem has exited from
 *        fake sleep. It is only when exit is not initiated from the fake
 *        sleep resource. In that case, we still need to notify the resource
 *        so that it can update its state accordingly.
 *
 * @param context: Unused (for prototype compliance)
 */
static void fakeSleep_exitCB( void *context )
{
  /* Issuing unconditional request to update fake sleep resource state. */
  npa_issue_required_request_unconditional( g_fakeSleepExitClient, 0 );
}

/**
 * @brief Callback function to be invoked when fake sleep resource becomes
 *        available for use. This function will create an internal client
 *        to that resource.
 *
 * @see g_fakeSleepExitClient
 *
 * @param context: Unused (for prototype compliance)
 * @param eventType: Unused (for prototype compliance)
 * @param date: Unused (for prototype compliance)
 * @param dataSize: Unused (for prototype compliance)
 */
static void fakeSleep_resourceAvailCB
(
  void *context,
  unsigned int eventType,
  void *data,
  unsigned int dataSize
)
{
  /* Creating an internal client to fake sleep node to trigger state
   * change when fake sleep exit happens outside of this node. */
  CORE_VERIFY_PTR( g_fakeSleepExitClient = 
                   npa_create_sync_client( FAKE_SLEEP_RESOURCE_NAME,
                                           "fakeSleepExitClient",
                                           NPA_CLIENT_FAKE_SLEEP_EXIT ) );
}

/**
 * @brief Function that gets invoked upon creation of a new client to fake 
 *        sleep resource.
 * 
 * @param client: Pointer to newly created client of the resource.
 */
static void fakeSleep_createClient( npa_client *client )
{
  /* Allocating memory to store the vector client request - 64 bit deadline. */
  if( NPA_CLIENT_VECTOR == client->type )
  {
    client->resource_data = (uint64 *)malloc(sizeof(uint64));
    CORE_VERIFY_PTR( client->resource_data );
    memset(client->resource_data, 0, sizeof(uint64));
  }
}

/**
 * @brief Returns the earliest deadline out of all vector clients of 
 *        fake sleep resource.
 *
 * @param resource: NPA resource pointer for fake sleep.
 *
 * @return The earliest deadline.
 */
static uint64 fakeSleep_getDeadline(npa_resource *resource)
{
  npa_client_handle client = resource->clients;
  uint64 deadline = UINT64_MAX;

  while(NULL != client)
  {
    /* We need to consider vector clients only. */
    if(NPA_CLIENT_VECTOR == client->type)
    {
      if(*((uint64 *)client->resource_data) < deadline)
      {
        deadline = *((uint64 *)client->resource_data);
      }
    }
    client = client->next;
  }

  return deadline;
}

/**
 * @brief This is a helper function that actually determines the state
 *        of fake sleep resource based on client requests. It is intended
 *        to be called from resource update function.
 *
 * @param resource: NPA resource on which a client issued a request.
 * @param client: Handle to the client that issued this request.
 * @param wakeupTickReq: Minimum wakeup tick request from all clients.
 *
 * @return Aggregated state of this NPA resource (fake sleep).
 */
static npa_resource_state fakeSleep_aggregateState
(
  npa_resource *resource,
  npa_client_handle client,
  uint64 wakeupTickReq
)
{
  uint64 npaSchedulerDeadline, currTick;
  npa_resource_state resourceState = resource->active_state;

  if( (NPA_CLIENT_VECTOR == client->type) && (0 != wakeupTickReq) )
  {
    /* There is a non-zero request from a vector client. See if we have 
     * enough time to (re)enter fake sleep. If so, update the resource state 
     * accordingly. */
    npaSchedulerDeadline = sleepNPAScheduler_getDeadline();
    wakeupTickReq = (wakeupTickReq <= npaSchedulerDeadline) ?
                    wakeupTickReq : npaSchedulerDeadline;
    currTick = CoreTimetick_Get64();

    if( FAKE_SLEEP_STATE_ACTIVE == resourceState )
    {
      /* We are trying to enter fake sleep state - perform necessary checks. */
      if( (currTick < wakeupTickReq) &&
          ((wakeupTickReq - currTick) > FAKE_SLEEP_THRESHOLD_TICKS) )
      {
        g_fakeSleepWakeupTick = wakeupTickReq;
        resourceState = FAKE_SLEEP_STATE_SEMIACTIVE;
      }
      else
      {
        /* Not enough time - keep state ACTIVE. */
        g_fakeSleepWakeupTick = 0;
        resourceState = FAKE_SLEEP_STATE_ACTIVE;
      }
    }
    else if( FAKE_SLEEP_STATE_SEMIACTIVE == resourceState )
    {
      /* We are already in fake sleep state - Wakeup deadline might have 
       * changed. */
      if( wakeupTickReq == g_fakeSleepWakeupTick )
      {
        /* Same deadline - do nothing. */
      }
      else if( wakeupTickReq > g_fakeSleepWakeupTick )
      {
        /* Deadline got extended. */
        if( (currTick < g_fakeSleepWakeupTick) && 
            ((wakeupTickReq - g_fakeSleepWakeupTick) >
             FAKE_SLEEP_REENTRY_THRESHOLD_TICKS) )
        {
          /* Deadline was extended by at least threshold - update it at RPM. */
          g_fakeSleepWakeupTick = wakeupTickReq;
          resourceState = FAKE_SLEEP_STATE_DEADLINE_CHANGED;
        }
        else
        {
          /* Request came after effective deadline or new deadline is very 
           * close to effective deadline - keep the state as it is. */
        }
      }
      else
      {
        /* Deadline got pulled in. */
        if( (currTick < wakeupTickReq) && 
            ((wakeupTickReq - currTick) > FAKE_SLEEP_REENTRY_THRESHOLD_TICKS) )
        {
          /* Wakeup is in sufficient future to update deadline at RPM. */
          g_fakeSleepWakeupTick = wakeupTickReq;
          resourceState = FAKE_SLEEP_STATE_DEADLINE_CHANGED;
        }
        else
        {
          /* Not enough time - change state to ACTIVE. */
          g_fakeSleepWakeupTick = 0;
          resourceState = FAKE_SLEEP_STATE_ACTIVE;
        }
      }
    }
  }
  else
  {
    /* Either fake sleep exit happened outside of this resource or someone 
     * is requesting it explicitly. Set the state as such. */
    g_fakeSleepWakeupTick = 0;
    resourceState = FAKE_SLEEP_STATE_ACTIVE;
  }
  
  return resourceState; 
}

/**
 * @brief Update function for the fake sleep resource which will aggregate
 *        the correct state based on client requests.
 *
 * @param resource: NPA resource on which client issued a request.
 * @param client: Handle to the client that issued this request.
 *
 * @return Aggregated state of the resource.
 */
static npa_resource_state fakeSleep_resourceUpdate
(
  npa_resource *resource,
  npa_client_handle client
)
{
  /* For vector NPA request, state denotes length of request vector. */
  npa_resource_state reqState = NPA_PENDING_REQUEST(client).state;
  npa_resource_state resourceState = resource->active_state;
  uint64 wakeupTickReq = 0, clientReq = 0;

  uint32 *reqPtr = NULL;
  const uint32 reqVectorLength = 2;

  if( NPA_CLIENT_VECTOR == client->type )
  {
    /* Request from actual vector client - check validity */
    CORE_VERIFY( (0 == reqState) || (reqVectorLength == reqState) );

    if( reqState != 0 )
    {
      /* New request with a deadline - can be 0 (indirect cancel) as well. */
      reqPtr = (uint32 *)(NPA_PENDING_REQUEST(client).pointer.vector);
      clientReq = ((uint64)reqPtr[0] << 32) | reqPtr[1];
      *((uint64 *)client->resource_data) = clientReq;
    }
    else
    {
      /* Client completed/canceled request and is not longer ok with fake
       * sleep. Set its deadline as 0 and re-aggregate. */
      *((uint64 *)client->resource_data) = 0;
    }

    /* Obtain wakeup deadline from all clients */
    wakeupTickReq = fakeSleep_getDeadline(resource);    
  }

  /* Determine the resource state with this client request. */
  resourceState = fakeSleep_aggregateState( resource, client, wakeupTickReq );

  return resourceState;
}

/**
 * @brief Driver function of the fake sleep node to perform necessary 
 *        actions based on aggregated state of the resource.
 *
 * @param resource: NPA resource which has its state changed (fake sleep
 *                  resource here).
 * @param client: Client whose request resulted in resource state change.
 * @param state: New aggregated state after client's request.
 *
 * @return Effective resource state after client's request has completed.
 */
static npa_resource_state fakeSleep_nodeDriver
(
  npa_resource *resource,
  npa_client *client,
  npa_resource_state state
)
{
  static npa_resource_state prevState = FAKE_SLEEP_STATE_ACTIVE;

  if( (NPA_CLIENT_INITIALIZE == client->type) || (prevState ==  state) )
  {
    /* Called at node initialization from NPA framework or due to short 
     * circuit bug in NPA framework with vector clients - simply return. */
    return state;
  }

  if( FAKE_SLEEP_STATE_SEMIACTIVE == state )
  {
    /* Valid client request to enter fake sleep */
    vmpm_SetWakeupTimetick( g_fakeSleepWakeupTick );
    CORE_VERIFY( TRUE == rpm_enter_fake_sleep() );
    sleepLog_printf( SLEEP_LOG_LEVEL_REQUESTS, 2, 
                     "Entered in fake sleep (deadline: 0x%llx)",
                     ULOG64_DATA(g_fakeSleepWakeupTick) );
  }
  else if( FAKE_SLEEP_STATE_DEADLINE_CHANGED == state )
  {
    /* Wakeup deadline changed and RPM needs to be updated about it. */
    CORE_VERIFY( TRUE == rpm_exit_fake_sleep() );
    vmpm_SetWakeupTimetick( g_fakeSleepWakeupTick );
    CORE_VERIFY( TRUE == rpm_enter_fake_sleep() );
    sleepLog_printf( SLEEP_LOG_LEVEL_REQUESTS, 2, 
                     "Re-entered in fake sleep (New deadline: 0x%llx)",
                     ULOG64_DATA(g_fakeSleepWakeupTick) );

    state = FAKE_SLEEP_STATE_SEMIACTIVE;
  }
  else
  {
    if( NPA_CLIENT_VECTOR == client->type )
    {
      /* Explicit client request to exit fake sleep. */
      CORE_VERIFY( TRUE == rpm_exit_fake_sleep() );
    }
    else
    {
      /* Fake sleep resource was bypassed for exit - just keep the updated 
       * state. No action needed from the resource. */
    }
    sleepLog_printf( SLEEP_LOG_LEVEL_REQUESTS, 0, "Exit from fake sleep" );
  }

  /* Update the last state. */
  prevState = state;

  return state;
}

/**
 * @brief Query function for fake sleep resource.
 *
 * @param resource: Reference to NPA resource being queried.
 * @param queryId: What type of query being requested.
 * @param queryResult [out]: Stores result of the query - valid only with
 *                           supported queryId. Wakeup deadline if state
 *                           is semi-active else 0.
 *
 * @return Status for the query.
 */
static npa_query_status fakeSleep_queryResource
(
  npa_resource *resource,
  unsigned int queryId,
  npa_query_type *queryResult
)
{
  npa_query_status status = NPA_QUERY_UNSUPPORTED_QUERY_ID;

  if( NPA_QUERY_CURRENT_STATE == queryId )
  {
    queryResult->data.value64 = g_fakeSleepWakeupTick;
    queryResult->type = NPA_QUERY_TYPE_VALUE64;
    status = NPA_QUERY_SUCCESS;
  }

  return status;
}

/*==============================================================================
                              NPA NODE DEFINITION
 =============================================================================*/

/**
 * @brief Plug in for fake sleep resource.
 */
const npa_resource_plugin g_fakeSleepPlugin = 
{
  fakeSleep_resourceUpdate,                       /* Request update function */
  NPA_CLIENT_VECTOR | NPA_CLIENT_FAKE_SLEEP_EXIT, /* Supported client types */
  fakeSleep_createClient                          /* Client creation function */
};

/**
 * @brief Fake sleep resource definition.
 */
static npa_resource_definition g_fakeSleepResource[] = 
{
  {
    FAKE_SLEEP_RESOURCE_NAME,                    /* Resource name */
    "Active/SemiActive",                         /* Units */
    FAKE_SLEEP_STATE_MAX,                        /* Max state */
    &g_fakeSleepPlugin,                          /* Resource plug in */
    NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED,    /* Resource attribute */
    NULL,                                        /* User data */
    fakeSleep_queryResource                      /* Query function */
  }
};

/**
 * @brief Fake sleep node definition.
 */
static npa_node_definition g_fakeSleepNode = 
{
  "/node/core/cpu/fake_sleep",                   /* Node name */
  fakeSleep_nodeDriver,                          /* Driver function */
  NPA_NODE_DEFAULT,                              /* Node attributes */
  NULL,                                          /* User data */
  NPA_EMPTY_ARRAY,                               /* Dependencies */
  NPA_ARRAY( g_fakeSleepResource )               /* Node resources */
};

/*==============================================================================
                         GLOBAL FUNCTION DEFINITIONS
 =============================================================================*/

/**
 * @brief Routine to perform fake sleep feature initialization.
 */
void fakeSleep_init(void)
{
  npa_resource_state initialState[] = { 0 };
  
  npa_define_node( &g_fakeSleepNode, initialState, NULL );
  npa_resource_available_cb( FAKE_SLEEP_RESOURCE_NAME, 
                             fakeSleep_resourceAvailCB, NULL );

  /* register fake sleep exit call back with RPM */
  rpm_register_fake_sleep_cb( fakeSleep_exitCB, NULL );
}

#else /* ENABLE_FEATURE_FAKE_SLEEP */

void fakeSleep_init(void)
{
}
#endif /* ENABLE_FEATURE_FAKE_SLEEP */
