#ifndef SLEEPACTIVE_H
#define SLEEPACTIVE_H
/*==============================================================================
  FILE:         sleepActive.h

  OVERVIEW:     This file contains public APIs exposed by the Sleep Active-time 
                Solver Thread.

  DEPENDENCIES: None

  Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
  All Rights Reserved.
  Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.mpss/3.4.c3.11/power/sleep2.0/inc/sleepActive.h#1 $
$DateTime: 2016/03/28 23:02:17 $
==============================================================================*/
#include "sleep_solver.h"

/*==============================================================================
                                GLOBAL TYPE DECLARATIONS
  =============================================================================*/ 
/* List of Signals that cause the Active Time solver 
 * to re-evaluate its selection of low-power modes */
typedef enum
{
  SLEEP_SIGNAL_CONTROL         = ( 1 << 0 ),    /* Enable/Disable Active Time Solver  */
  SLEEP_SIGNAL_LATENCY         = ( 1 << 1 ),    /* Update in Latency Constraint       */
  SLEEP_SIGNAL_SOFT_DURATION   = ( 1 << 2 ),    /* Update Idle Soft Duration          */
  SLEEP_SIGNAL_REGISTRY        = ( 1 << 3 ),    /* Update List of Enabled Synth Modes */
  SLEEP_SIGNAL_CPU_FREQUENCY   = ( 1 << 4 ),    /* Cpu Frequency Change               */
  SLEEP_SIGNAL_HARD_DURATION   = ( 1 << 5 ),    /* Update idle hard duration          */
  SLEEP_SIGNAL_MASK_ALL        = ( 1 << 6 ) - 1 /* Mask for all signals               */
} sleepActive_SignalType;

/*==============================================================================
                              GLOBAL FUNCTION DECLARATIONS
  =============================================================================*/
/**
 * @brief Trigger a Signal of type sig on Active Time solver thread
 *
 *        Signals set on the Active Time solver are used to determine 
 *        which execution conditions that have changed. 
 *        The Active Time solver thread executes the sleep solver to determine
 *        whether these modified inputs cause a change in the selection of 
 *        low-power mode.
 * 
 * @param sig: Signal to be set on Active Time solver
 */
void sleepActive_SetSignal( uint32 sig );

/**
 * @brief Returns the input parameters used by Sleep Active for 
 *        input to Active time solver
 *  
 * @return A pointer to the solver input struct
 */
sleep_solver_input *sleepActive_GetSolverInput();

/**
 * @brief Returns the low-power mode selected by Active time 
 *        solver
 *  
 * @return A pointer to the last chosen solver output struct
 */
sleep_solver_output *sleepActive_GetSolverOutput();
#endif /* SLEEPACTIVE_H */
