/*============================================================================
@file dcvs_target.c
 
This file provides target specific code for DCVS.
 
Copyright (c) 2012-2014 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
============================================================================*/

#include "dcvs_target.h"
#include "npa_resource.h"
#include "dcvs_nv.h"
#include "dcvs_algorithm.h"
#include "dcvs_npa.h"
#include "CoreVerify.h"
#include "icbarb.h"
#include "icbid.h"

/* Number of Master slave pairs for target */
#define DCVS_MS_COUNT 1 

/* DCVS data context for target */
DCVS_CONTEXT dcvsContext;

/*----------------------------------------------------------------------------
 * This section defines all target specific DCVS NV items.
 * -------------------------------------------------------------------------*/

static const DCVS_NV_PARAM_ENTRY dcvsTargetAlgParamsStartup[] =
{
  {"Algorithm", DCVS_NV_PARAM_STRING, {"qdsp_classic"}},
  {"LogSize", DCVS_NV_PARAM_UINT32, {(void*)16384}},
  {NULL, DCVS_NV_PARAM_NULL, {(void*)0}},
};

const DCVS_NV_PARAM_LIST dcvsTargetNvDefaults[] =
{
  {"Startup", DCVS_NV_CORE_ID_ALL, dcvsTargetAlgParamsStartup},  
  {NULL, 0, NULL}
};

/*----------------------------------------------------------------------------
 * List of available algorithms for this target.
 * -------------------------------------------------------------------------*/

extern DCVS_CORE_ALGO disabledAlgorithm;
extern DCVS_CORE_ALGO fixedAlgorithm;
extern DCVS_CORE_ALGO requestsAlgorithm;
extern DCVS_CORE_ALGO qdspClassicAlgorithm;

const DCVS_CORE_ALGO* dcvsTargetAlgList[] = 
{ 
  &disabledAlgorithm, 
  &fixedAlgorithm, 
  &requestsAlgorithm,
  &qdspClassicAlgorithm,
  NULL
};

/* Target specific data */
typedef struct 
{
  npa_client_handle icbClient;
  
  /* Vector of request states for the master/slave pairs */
  ICBArb_RequestType vecReq[DCVS_MS_COUNT];
  
} DCVS_TARGET_STRUCT;


static npa_resource_state corecpu_bus_driver( npa_resource *resource,
                                              npa_client   *client,
                                              npa_resource_state state )
{
  uint32 i;
  DCVS_CONTEXT* dcvsContext = (DCVS_CONTEXT*)resource->definition->data;
  DCVS_TARGET_STRUCT* dcvsTargetData = 
    (DCVS_TARGET_STRUCT*)dcvsContext->targetDataContext;
  
  if ( client->type == NPA_CLIENT_INITIALIZE )
  {
    /* There are 2 master/slave pairs which are affected by the DCVS */  
    ICBArb_MasterSlaveType msArray[] = 
    {
      {ICBID_MASTER_MSS_PROC, ICBID_SLAVE_EBI1}
    };

    /* Create a client to the bus arbiter */
    dcvsTargetData->icbClient = 
      icbarb_create_suppressible_client_ex( "/node/core/cpu/bus", 
                                            msArray, DCVS_MS_COUNT, NULL );
    CORE_VERIFY_PTR( dcvsTargetData->icbClient );

    /* Initialize the request type */
    for ( i=0; i<DCVS_MS_COUNT; i++ ) 
    {
      /* Indicate we are using CPU-style master */
      dcvsTargetData->vecReq[i].arbType = ICBARB_REQUEST_TYPE_2;
      dcvsTargetData->vecReq[i].arbData.type2.uUsagePercentage = 0;
    }
  }

  /* Set up the vector of request states for the bus arbiter. 
     uThroughPut is in bytes/s */
  for ( i=0; i<DCVS_MS_COUNT; i++ ) 
  {
    dcvsTargetData->vecReq[i].arbData.type2.uThroughPut = 
                                            (uint64)state * 1000000;
  }

  /* Issue the request */
  icbarb_issue_request( dcvsTargetData->icbClient, 
                        dcvsTargetData->vecReq, DCVS_MS_COUNT );
  
  return state;
}

static npa_node_dependency corecpu_bus_deps[] =   
{
  {"/icb/arbiter", NPA_NO_CLIENT}, 
};

static npa_resource_definition corecpu_bus_res[] =
{
  {"/core/cpu/bus", 
    "MB/s",
    NPA_MAX_STATE, // No max specified in definition
    &npa_identity_plugin,  // The resource plugin.
    NPA_RESOURCE_DEFAULT,  // The resource attributes.
    &dcvsContext}          // The user data field.
};

static npa_node_definition corecpu_bus_node = 
{ 
  "node/core/cpu/bus",  /* name */
  corecpu_bus_driver,   /* driver_fcn */
  NPA_NODE_DEFAULT, /* attributes */
  NULL,             /* data */
  NPA_ARRAY(corecpu_bus_deps),
  NPA_ARRAY(corecpu_bus_res)
};


npa_node_dependency targetNodeDeps[] = 
{
  /* Clock Resource */
  {
    "/clk/cpu", /* Name of resource */
    NPA_CLIENT_REQUIRED, /* Type of client to create */
    NULL /* Client Handle - created by NPA */
  },
  /* Bus resource */
  {
    "/core/cpu/bus", /* Name of resource */
    NPA_CLIENT_REQUIRED, /* Type of client to create */
    NULL /* Client Handle - created by NPA */
  }                                 
};

/* System dependencies  */
const char* targetSysDeps[] = {"/init/dcvs/startup"};

void DCVSTargetSpecificInit( void )
{
  npa_resource_state initialState[] = { 0 };
  
  DALSYS_Malloc( sizeof(DCVS_TARGET_STRUCT),  
                 (void**)&dcvsContext.targetDataContext );
  CORE_VERIFY_PTR( dcvsContext.targetDataContext );
  
  memset( dcvsContext.targetDataContext, 0, sizeof(DCVS_TARGET_STRUCT) ); 

  npa_define_node( &corecpu_bus_node, initialState, NULL );
  
  NodeCoreCpuCreate( targetNodeDeps, sizeof(targetNodeDeps)/sizeof(npa_node_dependency), 
                     (const char**)&targetSysDeps, sizeof(targetSysDeps)/sizeof(char*), 
                     0, &dcvsContext );
}

void MCATargetSpecificInit( void )
{
  NodeCoreMcaCreate();
}

