/**********************************************************************
 * dcvs_clkstub.c
 *
 * Provides the interface for querying clock information on the 8960 Target.
 * Every target has an implementation of this file.
 * The targets can have one of the following clock plan data types::
 * DCVS_PRIMARY_CLOCK_PLAN_DATA_TYPE
 * DCVS_LOCAL_CLOCK_PLAN_DATA_TYPE
 * 
 * The priority of the clock plan retrieval is ::
 * DCVS_PRIMARY_CLOCK_PLAN_DATA_TYPE
 * Query from the resource
 * DCVS_LOCAL_CLOCK_PLAN_DATA_TYPE
 * 
 *
 * EXTERNALIZED FUNCTIONS
 *
 * Copyright (C) 2011-2013 by QUALCOMM Technologies, Incorporated
 * All Rights Reserved.
 *
 **********************************************************************/
#include "comdef.h"
#include "dcvs_target.h"
#include "dcvs_core.h"
#include "DDIChipInfo.h"
#include "CoreVerify.h"

/* This local clock plan table is converted into the format of the
   primary clock plan table and is used if the primary plan is not available.*/

DCVS_LOCAL_CLOCK_PLAN_DATA_TYPE localClockPlanData[]=
{
  {787200}
};

/* The lookup table of requested mips and the corresponding 
   CPU frequency(In KHz)  and Bus AXI Frequency.(In MBps) 
*/
DCVS_PRIMARY_CLOCK_PLAN_DATA_TYPE primaryClockPlanDataV1[] =
{
  { 115, 115200, 0 },
  { 144, 144000, 0 },
  { 230, 230400, 400 },
  { 288, 288000, 400 },
  { 345, 345600, 685 },
  { 364, 364800, 685 },
  { 384, 384000, 685 },
  { 499, 499200, 685 },
  { 518, 518400, 685 },
  { 576, 576000, 1371 },
  { 652, 652800, 1371 },
  { 729, 729600, 2664 },
  { 806, 806400, 2664 },
  { 844, 844800, 4147 },
}; 

DCVS_PRIMARY_CLOCK_PLAN_DATA_TYPE primaryClockPlanDataV2[] =
{
  { 115, 115200, 0 },
  { 144, 144000, 0 },
  { 230, 230400, 400 },
  { 288, 288000, 400 },
  { 345, 345600, 685 },
  { 364, 364800, 685 },
  { 384, 384000, 685 }, 
  { 499, 499200, 685 },
  { 518, 518400, 685 },
  { 576, 576000, 1371 },
  { 652, 652800, 1371 },
  { 729, 729600, 2664 },
  { 806, 806400, 2664 },
  { 844, 844800, 4147 }, 
};

DCVS_LOCAL_CLOCK_PLAN_DATA_TYPE* DCVSTargetClockPlanLocalGet(void)
{
  /* Return the local clock plan table */
  return(localClockPlanData);
}

uint32 DCVSTargetClockPlanLocalNumLevelsGet(void)
{
  /* Return the number of entries in the local clock plan table. */
  return(sizeof(localClockPlanData)/sizeof(DCVS_LOCAL_CLOCK_PLAN_DATA_TYPE));
}

DCVS_TARGET_CLOCK_PLAN_INFO_TYPE dcvsTargetClockPlan = 
{
  /* The clock plan table having mappings of mips and processor frequency.*/
  primaryClockPlanDataV2,
  /* The number of performance levels in the clock plan for this target. */
  sizeof(primaryClockPlanDataV2)/sizeof(DCVS_PRIMARY_CLOCK_PLAN_DATA_TYPE),
  /* Target specific frequency conversion functions. */
  NULL,
  NULL,
  NULL,
  19200000    /* This is the freq. of the QTimer source (timer clock frequency) */
};


DCVS_TARGET_CLOCK_PLAN_INFO_TYPE* DCVSTargetClockPlanInfoGet(void)
{
  // Determine chip version

  DalChipInfoVersionType chipVersion;
  CORE_VERIFY(chipVersion = DalChipInfo_ChipVersion());

  if (chipVersion < 0x00020000)
  {
      dcvsTargetClockPlan.clockPlanTable = primaryClockPlanDataV1;  
      dcvsTargetClockPlan.numLevels = sizeof(primaryClockPlanDataV1)/sizeof(DCVS_PRIMARY_CLOCK_PLAN_DATA_TYPE);
  }
  /* Return the local clock plan info of the target. */
  return(&dcvsTargetClockPlan);
}
