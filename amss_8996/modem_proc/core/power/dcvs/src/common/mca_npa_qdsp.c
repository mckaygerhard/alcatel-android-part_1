/**********************************************************************
 * mca_npa_qdsp.c
 *
 * This module implements the NPA Resource for MCA
 *
 *
 * Copyright (C) 2009-2012 by Qualcomm Technologies, Inc.
 * All Rights Reserved.
 *
 **********************************************************************/
/*========================================================================
  EDIT HISTORY FOR MODULE
  
  $Header: //components/rel/core.mpss/3.4.c3.11/power/dcvs/src/common/mca_npa_qdsp.c#1 $
  $DateTime: 2016/03/28 23:02:17 $

  when       who     what, where, why
  --------   ----    ---------------------------------------------------
========================================================================*/
#include "BuildFeatures.h"
#ifndef DEFEATURE_DCVS_NPA_QDSP

#include "mca_internal.h"
#include "CoreString.h"
#include "CoreVerify.h"
#include "dcvs_npa.h"
#include "coremca.h"
#include "ULogFront.h"
#include "ClockDefs.h"
#include "icbarb.h"
#include "icbid.h"
#include "CoreHeap.h"
#include "VCSDefs.h"
#include "DDIPlatformInfo.h"

#define FUDGE_MULTIPLICATION_FACTOR 1000
#define BW_MULTIPLICATION_FACTOR 1048576
//(mpps*cpp limit)*1000 and corresponding fudge factor levels
//TBD - can be read from nv items rather than hardcoding here
#define MPPSCPP_LIMIT 288000

//Fudge factor level1 (1/0.6)*1000
#define FUDGE_FACTOR_LEVEL1 1666

//fudge factor level2 (1/.85) * 1000
#define FUDGE_FACTOR_LEVEL2 1176

#ifdef MCA_ENABLE_DIAG_HOOKS
npa_client_handle mcaDiagClient;
unsigned mcaHookMode = 0;
unsigned mcaNextParamIsQ6 = 0;
unsigned mcaNextParamIsIBW = 0;
unsigned mcaNextParamIsABW = 0;
unsigned mcaNextParamIsLatency = 0;
unsigned mcaNextParamIsValue = 0;
unsigned mcaFixedQ6 = 0;
unsigned mcaFixedIBW = 0;
unsigned mcaFixedABW = 0;
unsigned mcaFixedLatency = 0;
unsigned mcaDiagLogInitialized = 0;
#endif

static mcaInternalDataType mcaData;

/*=======================================================================

                      STATIC FUNCTION DEFINITIONS 

========================================================================*/
static npa_resource_state mca_bus_driver_fcn( npa_resource *resource,
                                              npa_client *client,
                                              npa_resource_state state )
{
  switch ( client->type )
  {
    case NPA_CLIENT_INITIALIZE:
    {
      /* We create the initial states for the bus arbiter */
      /* There are 2 master/slave pairs which are affected by MCA */
      ICBArb_CreateClientVectorType vector;

      ICBArb_MasterSlaveType msArray[] =
      {
        { ICBID_MASTER_MSS_PROC, ICBID_SLAVE_EBI1 }
      };

      /* Create a client for the bus arbiter */
      mcaData.icbSupClient = npa_create_sync_client_ex( "/icb/arbiter",
                                                        "/node/core/mca/bus_sup",
                                                        NPA_CLIENT_SUPPRESSIBLE_VECTOR,
                                                        ICBARB_CREATE_CLIENT_VECTOR( &vector,
                                                                                     msArray,
                                                                                     1, NULL ) );

      CORE_VERIFY_PTR( mcaData.icbSupClient );

      mcaData.icbSup2Client = npa_create_sync_client_ex( "/icb/arbiter",
                                                         "/node/core/mca/bus_sup2",
                                                         NPA_CLIENT_SUPPRESSIBLE2_VECTOR,
                                                         ICBARB_CREATE_CLIENT_VECTOR( &vector,
                                                                                      msArray,
                                                                                      1, NULL ) );

      CORE_VERIFY_PTR( mcaData.icbSup2Client );

      /* Indicate we are using request type 3 */
      mcaData.supIcbVec.arbType = ICBARB_REQUEST_TYPE_3_LAT;
      mcaData.sup2IcbVec.arbType = ICBARB_REQUEST_TYPE_3_LAT;
      break;
    }

    case NPA_CLIENT_SUPPRESSIBLE:
      mcaData.supIcbVec.arbData.type3.uAb = (uint64)mcaData.mcaSupState.ab * BW_MULTIPLICATION_FACTOR;
      mcaData.supIcbVec.arbData.type3.uIb = (uint64)mcaData.mcaSupState.ib * BW_MULTIPLICATION_FACTOR; 
      mcaData.supIcbVec.arbData.type3.uLatencyNs = mcaData.mcaSupState.latency;

#ifdef MCA_ENABLE_DIAG_HOOKS
      if ( mcaHookMode )
      {
        mcaData.supIcbVec.arbData.type3.uAb = (uint64)mcaFixedABW * BW_MULTIPLICATION_FACTOR; 
        mcaData.supIcbVec.arbData.type3.uIb = (uint64)mcaFixedIBW * BW_MULTIPLICATION_FACTOR; 
        mcaData.supIcbVec.arbData.type3.uLatencyNs = mcaFixedLatency;
      }
#endif
      npa_pass_request_attributes( client, mcaData.icbSupClient );

      icbarb_issue_request( mcaData.icbSupClient, &mcaData.supIcbVec, 1 );
      break;

    case NPA_CLIENT_SUPPRESSIBLE2:
      mcaData.sup2IcbVec.arbData.type3.uAb = (uint64)mcaData.mcaSup2State.ab * BW_MULTIPLICATION_FACTOR; 
      mcaData.sup2IcbVec.arbData.type3.uIb = (uint64)mcaData.mcaSup2State.ib * BW_MULTIPLICATION_FACTOR; 
      mcaData.sup2IcbVec.arbData.type3.uLatencyNs = mcaData.mcaSup2State.latency;

      npa_pass_request_attributes( client, mcaData.icbSup2Client );

      icbarb_issue_request( mcaData.icbSup2Client, &mcaData.sup2IcbVec, 1 );
      break;

    default:
      break;
  }
  return state;
}

/* Variable list of dependencies of "node/core/mca/bus" node to other nodes */
static npa_node_dependency mca_bus_deps[] =
{
  { "/icb/arbiter", NPA_NO_CLIENT },
};

/*Definition of /core/mca/bus resource*/
static npa_resource_definition mca_bus_res[] =
{
  { "/core/mca/bus",
    "MB/s",
    NPA_MAX_STATE,
    &npa_identity_plugin,        // The resource plugin.
    NPA_RESOURCE_SINGLE_CLIENT |
    NPA_RESOURCE_DRIVER_UNCONDITIONAL,  // The resource attributes.
    NULL }                        // The user data field.
};

/*Definition of /core/mca/bus node*/
static npa_node_definition node_mca_bus =
{
  "node/core/mca/bus",                        /* name */
  mca_bus_driver_fcn,                    /* driver_fcn */
  NPA_NODE_DEFAULT,                       /* attributes */
  NULL,                                       /* data */
  NPA_ARRAY( mca_bus_deps ),
  NPA_ARRAY( mca_bus_res )
};



/* Array of /core/mca dependencies */
static npa_node_dependency targetNodeDepsMCA[] =
{
  /* Clock Resource */
  {
    "/clk/cpu", /* Name of resource */
    NPA_CLIENT_REQUIRED, /* Type of client to create */
    NULL /* Client Handle - created by NPA */
  },
  {
    "/clk/cpu", /* Name of resource */
    NPA_CLIENT_SUPPRESSIBLE2, /* Type of client to create */
    NULL /* Client Handle - created by NPA */
  },
  /* Bus resource */
  {
    "/core/mca/bus", /* Name of resource */
    NPA_CLIENT_SUPPRESSIBLE, /* Type of client to create */
    NULL /* Client Handle - created by NPA */
  },
  {
    "/core/mca/bus", /* Name of resource */
    NPA_CLIENT_SUPPRESSIBLE2, /* Type of client to create */
    NULL /* Client Handle - created by NPA */
  }
};

/*=======================================================================

    LOCAL FUNCTION DEFINITIONS -- CORE CPU RESOURCE RELATED FUNCTIONS

========================================================================*/
/**
  @brief mca_calculateFudgeFactor
  
  This function calculates the fudge factor depends on mpps and cpp multiplicatin value
  passed as input.

  @param mppscpp: The product of the active client's MPPS and CPP result.
  @return : returns fudge factor value.
*/
static unsigned int mca_calculateFudgeFactor( uint32 value )
{
  uint32 fudgeFactor = 1, i = 0;

  //check
  if ( 0 != value && NULL != mcaData.fudgeFactors.pData )
  {
    for ( i = 0; i < mcaData.fudgeFactors.nCnt; i++ )
    {
      if ( value <= mcaData.fudgeFactors.pData[i].value )
      {
        fudgeFactor = mcaData.fudgeFactors.pData[i].fudgeFactor;
        break;
      }
    }
  }
  return fudgeFactor;
}

/**
  @brief mca_getNumberOfActiveClients
  
  This function finds the number of clients with a non-zero 
  mpps/Q6 min clock vote 

  @param resource : the resource that holds the client list we 
                  are interested in
   
  @return : returns the number of clients that are contributing 
          non-zero votes to the final aggregation 
*/
static unsigned int mca_getNumberOfActiveClients( npa_resource *resource )
{
  CORE_VERIFY_PTR( resource );
  uint32 numActiveClients = 0;
  npa_client *client = resource->clients;
  mcaInternalStateType *clientActiveRequest;

  while ( client )
  {
    if ( client->type == NPA_CLIENT_VECTOR
         || client->type == NPA_CLIENT_SUPPRESSIBLE_VECTOR
         || client->type == NPA_CLIENT_SUPPRESSIBLE2_VECTOR )
    {
      clientActiveRequest = (mcaInternalStateType *)client->resource_data;
      if ( clientActiveRequest->mcaRequest || clientActiveRequest->q6Clk )
      {
        numActiveClients++;
      }
    }
    client = client->next;
  }
  return numActiveClients;
}


/**
  @brief mca_adjustQ6Clk
  
  For given freqeuncy, it Returns maximun frequency supported without changing corner value

  @param  value: Frequency value
  @return      : Maximun corner frequency supported without change in corner level
*/
static unsigned int mca_adjustQ6Clk( uint32 value )
{
  uint32 q6Clk = value, i = 0;

  if ( NULL != mcaData.pCornerFreq )
  {
    for ( i = 0; i < mcaData.nCorners; i++ )
    {
      if ( value <= mcaData.pCornerFreq[i] )
      {
        q6Clk = mcaData.pCornerFreq[i];
        break;
      }
    }
  }
  return q6Clk;

}

/**
  @brief mcaTargetPopulateCornerMaxFreqValues
  
  This function populates max frequency values supported for each corner value

  @param  : None
  @return : None
*/
static void mcaTargetPopulateCornerMaxFreqValues( void )
{

  npa_query_handle clkCpuQueryNpaHandle;
  npa_query_type queryResult;
  npa_query_status status;
  uint32 i = 0;

  clkCpuQueryNpaHandle = npa_create_query_handle( "/clk/cpu" );
  CORE_VERIFY_PTR( clkCpuQueryNpaHandle );

  mcaData.nCorners = VCS_CORNER_TURBO;

  //allocate memory to store MAX corner freqeuncy values
  if ( 0 != mcaData.nCorners )
  {
    mcaData.pCornerFreq = Core_Malloc( mcaData.nCorners * sizeof(uint32) );
    CORE_VERIFY_PTR( mcaData.pCornerFreq );
    memset( mcaData.pCornerFreq, 0, mcaData.nCorners * sizeof(uint32) );
  }

  for ( i = 0; i < mcaData.nCorners; i++ )
  {
    status = npa_query( clkCpuQueryNpaHandle,
                        CLOCK_NPA_QUERY_CPU_MAX_KHZ_AT_CORNER + i,
                        &queryResult );

    //currenlty clk regime returns NPA_QUERY_UNSUPPORTED_QUERY_ID for CLOCK_VREG_MSS_CORNER_LOW_MINUS
    //corner level if it's not supported
    CORE_VERIFY( (NPA_QUERY_SUCCESS == status || NPA_QUERY_UNSUPPORTED_QUERY_ID == status) );
    if ( NPA_QUERY_SUCCESS == status )
    {
      mcaData.pCornerFreq[i] = queryResult.data.value;
    }

  }
  return;
}



#define MCA_INTERNAL_COMPUTE_MAX( request, field ) c = resource->clients; \
while ( c ) \
{ \
  if ( c->type & cTypes ) \
  { \
    request = (mcaInternalStateType *)c->resource_data; \
    mcaState->field = MAX( mcaState->field, request->field); \
  } \
  c = c->next; \
}

/**
  @brief mca_update_fcn
  
  This function is invoked by the /core/mca resource when a 
  client request is made. It gets the aggregates across all 
  clients and updates the clk and bus values accordingly.
 
  @param resource: The /core/mca resource
  @param client: Handle to the current client

  @return : None.
*/
static npa_resource_state mca_update_fcn( npa_resource *resource,
                                          npa_client_handle client )
{
  uint32 pendingMCARequest = 0;
  uint32 q6ClkTmp = 0;

  /*variables required to extract/aggregate data from the vector request*/
  unsigned *requestDataPtr, *valuePtr;
  unsigned numKeys, key, length, i;
  unsigned requestPriority = REQ_PRIORITY_DEFAULT;

  mcaInternalStateType * activeRequest,*mcaState;
  unsigned int cTypes;
  npa_client_handle c;

  unsigned fudgeFactorSet = 0;

  CORE_VERIFY_PTR( client );

  //store the number of keys in request
  numKeys = NPA_PENDING_REQUEST( client ).state;

  if ( client->type == NPA_CLIENT_SUPPRESSIBLE2_VECTOR )
  {
    mcaState = &mcaData.mcaSup2State;
    cTypes = (unsigned int)NPA_CLIENT_SUPPRESSIBLE2_VECTOR;
  }
  else
  {
    mcaState = &mcaData.mcaSupState;
    cTypes = (unsigned int)NPA_CLIENT_SUPPRESSIBLE_VECTOR | NPA_CLIENT_VECTOR;
  }

  switch ( client->type )
  {
    case NPA_CLIENT_INTERNAL:
#ifdef MCA_ENABLE_DIAG_HOOKS
      if ( mcaHookMode == 1 )
      {
        /* Request the state into the clock resource */
        npa_issue_required_request( NPA_DEPENDENCY( resource, 0 ), mcaFixedQ6 );
        npa_issue_internal_request( NPA_DEPENDENCY( resource, 2 ) );
      }
#endif
      break;
    case NPA_CLIENT_VECTOR:
    case NPA_CLIENT_SUPPRESSIBLE_VECTOR:
    case NPA_CLIENT_SUPPRESSIBLE2_VECTOR:
      //start multipart message to log mca request
      ULogFront_RealTimeMultipartMsgBegin( mcaData.log );
      ULogFront_RealTimePrintf( mcaData.log, 0, "MCA REQUEST RECEIVED: %m %m %m %m %m %m %m %m %m %m" ); //client, [mpps, cpp, q6, ib, ab, lat, fudge, prio], result

      activeRequest = (mcaInternalStateType *)client->resource_data;

      if ( numKeys == 0 ) //drop all votes from client if no keys are specified
      {
        //subtract client active request,
        mcaState->mcaRequest -= activeRequest->mcaRequest;
        mcaState->ab -= activeRequest->ab;

        //reaggregate for max/min keys
        if ( mcaState->q6Clk == activeRequest->q6Clk )
        {
          mcaInternalStateType *request;
          activeRequest->q6Clk = 0; // drop this client's vote before reaggregating
          mcaState->q6Clk = 0; // clear current q6 state of MCA
          MCA_INTERNAL_COMPUTE_MAX( request, q6Clk );
        }

        if ( mcaState->ib == activeRequest->ib )
        {
          mcaInternalStateType *request;
          activeRequest->ib = 0; // drop this client's vote before reaggregating
          mcaState->ib = 0; // clear current q6 state of MCA
          MCA_INTERNAL_COMPUTE_MAX( request, ib );
        }

        c = resource->clients;
        if ( mcaState->latency == activeRequest->latency )
        {
          mcaInternalStateType *request;
          activeRequest->latency = 0;
          mcaState->latency = 0;
          while ( c )
          {
            if ( c != client && c->type & cTypes )
            {
              request = (mcaInternalStateType *)c->resource_data;
              if ( request->latency != 0 &&
                   ( mcaState->latency == 0 || request->latency < mcaState->latency ) )
              {
                mcaState->latency = request->latency;
              }
            }
            c = c->next;
          }
        }
        //clear out old request data
        DALSYS_memset( client->resource_data, 0, sizeof(mcaInternalStateType) );
        ULOG_RT_PRINTF_1( mcaData.log, "(Client: 0x%08x DROPPED VOTE)", client );
      }
      else
      {
        ULOG_RT_PRINTF_2( mcaData.log, "(Client: 0x%08x) (Type: %u)", client, client->type );
      }

      //set pointer to begining of client's request struct/first key
      requestDataPtr = (unsigned *)NPA_PENDING_REQUEST( client ).pointer.vector;
      for ( i = 0; i < numKeys; i++ ) //extract/aggregate request data for each key in struct
      {
        key = *requestDataPtr;
        requestDataPtr++; //move pointer to length section of current key
        length = *requestDataPtr;
        requestDataPtr++; //move pointer to the data section of current key
        valuePtr = requestDataPtr;

        switch ( key )
        {
          case MPPS_REQ_KEY:
            CORE_VERIFY( length == sizeof(unsigned) );

            //save client's vote
            activeRequest->mpps = *valuePtr;
            requestDataPtr++; //move request pointer to next key

            ULOG_RT_PRINTF_1( mcaData.log, "(MPPS : %d)", activeRequest->mpps );
            break;
          case CPP_REQ_KEY:
            CORE_VERIFY( length == sizeof(unsigned) );

            //save client's vote
            activeRequest->cpp = *valuePtr;
            requestDataPtr++; //move request pointer to next key

            ULOG_RT_PRINTF_1( mcaData.log, "(CPP : %d)", *valuePtr );
            break;

          case AB_REQ_KEY:
            CORE_VERIFY( length == sizeof(unsigned) );

            //handle new vote
            mcaState->ab -= activeRequest->ab;
            mcaState->ab += *valuePtr;
            activeRequest->ab = *valuePtr;

            requestDataPtr++; //move request pointer to next key

            ULOG_RT_PRINTF_1( mcaData.log, "(AB : %d)", *valuePtr );
            break;
          case IB_REQ_KEY:
            CORE_VERIFY( length == sizeof(unsigned) );
            //max agg for IB requests, if the client was voting for max val,
            //we need to reaggregate across all clients
            if ( *valuePtr > mcaState->ib )
            {
              mcaState->ib = *valuePtr;
            }
            else if ( activeRequest->ib == mcaState->ib ) //old vote responsible for max?
            {
              mcaInternalStateType *request;
              //replace old vote with new vote, then reaggregate from scratch
              activeRequest->ib = *valuePtr;
              mcaState->ib = 0;
              MCA_INTERNAL_COMPUTE_MAX( request, ib );
            }

            //save client's vote
            activeRequest->ib = *valuePtr;
            requestDataPtr++; //move request data pointer to next key

            ULOG_RT_PRINTF_1( mcaData.log, "(IB : %d)", activeRequest->ib );
            break;
          case LAT_REQ_KEY:
            CORE_VERIFY( length == sizeof(unsigned) );
            //min aggregation for latency key
            if ( *valuePtr != 0 &&
                 ( mcaState->latency == 0 || *valuePtr < mcaState->latency ) )
            {
              mcaState->latency = *valuePtr;
            }
            else if ( activeRequest->latency == mcaState->latency ) //prev vote responsible for current min?
            {
              mcaInternalStateType *request;
              c = resource->clients;
              mcaState->latency = *valuePtr;
              /* Scan the pending list for a new minimum */
              while ( c )
              {
                if ( c != client && c->type & cTypes )
                {
                  request = (mcaInternalStateType *)c->resource_data;
                  if ( request->latency != 0 &&
                       ( mcaState->latency == 0 || request->latency < mcaState->latency ) )
                  {
                    mcaState->latency = request->latency;
                  }
                }
                c = c->next;
              }
            }
            
            // save client's vote
            activeRequest->latency = *valuePtr;
            requestDataPtr++; //move request data pointer to next key
            
            ULOG_RT_PRINTF_1( mcaData.log, "(Latency : %d)", activeRequest->latency );
            break;
          case Q6_CLK_REQ_KEY:
            CORE_VERIFY( length == sizeof(unsigned) );
            //max agg for q6 requests
            //if the client was voting for max val, we need to reaggregate across all clients
            if ( *valuePtr > mcaState->q6Clk ) //incoming value equal to current state?
            {
              mcaState->q6Clk = *valuePtr;
            }
            else if ( activeRequest->q6Clk == mcaState->q6Clk ) //old vote responsible for max?
            {
              mcaInternalStateType *request;
              //replace old vote with new vote, then reaggregate from scratch
              activeRequest->q6Clk = *valuePtr;
              mcaState->q6Clk = 0;
              c = resource->clients;
              MCA_INTERNAL_COMPUTE_MAX( request, q6Clk );
            }

            //save client's vote
            activeRequest->q6Clk = *valuePtr;
            requestDataPtr++; //move request data pointer to next key

            ULOG_RT_PRINTF_1( mcaData.log, "(Q6 : %d)", activeRequest->q6Clk );
            break;
          case FUDGE_FACTOR_REQ_KEY:
            CORE_VERIFY( length == sizeof(unsigned) );
            //no aggregation, just copy value and set flag
            activeRequest->fudgeFactor = *valuePtr;
            fudgeFactorSet = 1;
            requestDataPtr++;

            ULOG_RT_PRINTF_1( mcaData.log, "(Fudge Factor : %d)", activeRequest->fudgeFactor );
            break;
          case REQUEST_PRIORITY_KEY:
            CORE_VERIFY( length == sizeof(unsigned) );
            //no aggregation, just copy value to local variable, no planned implementation with this yet
            requestPriority = *valuePtr;
            requestDataPtr++;

            ULOG_RT_PRINTF_1( mcaData.log, "(Request Priority : %d)", requestPriority );
            break;

          default:
            requestDataPtr++;
            ULOG_RT_PRINTF_1( mcaData.log, "(INVALID KEY : 0x%x)", key );
            break;

        } // switch (key)
      } //for(i=0;i<numKeys;i++)

      //set client's fudge factor based on mpps*cpp value
      if ( !fudgeFactorSet )
      {
        activeRequest->fudgeFactor = mca_calculateFudgeFactor( activeRequest->mpps * activeRequest->cpp );
        ULOG_RT_PRINTF_1( mcaData.log, "(Fudge Factor : %d)", activeRequest->fudgeFactor );
        numKeys++;
      }
      pendingMCARequest = ((activeRequest->mpps * activeRequest->cpp * activeRequest->fudgeFactor) / FUDGE_MULTIPLICATION_FACTOR);

      //final aggregation
      //Adjust mcaRequest according to client current request
      //all other values have been updated to current request, but mcaRequest still reflects prev request
      mcaState->mcaRequest -= (activeRequest->mcaRequest - pendingMCARequest);
      activeRequest->mcaRequest = pendingMCARequest; //save result

      ULOG_RT_PRINTF_1( mcaData.log, "(Result : %d)", mcaState->mcaRequest );

      for ( i = 0; i < (MAX_KEYS - numKeys); i++ )
      {
        ULOG_RT_PRINTF_0( mcaData.log, " " );
      }
      ULogFront_RealTimeMultipartMsgEnd( mcaData.log );
      break;
    default:

      break;
  }

  q6ClkTmp = MAX( mcaState->mcaRequest, mcaState->q6Clk );
  if ( mca_getNumberOfActiveClients( resource ) > 1 )
  {
    q6ClkTmp = mca_adjustQ6Clk( q6ClkTmp );
  }

  ULOG_RT_PRINTF_6( mcaData.log, "MCA Request Output Params "
                    "(mcaRequest : %d) (q6minclk : %d) "
                    "(ib : %d) (ab : %d) (latency : %d) "
                    "(adjustedQ6Clk : %d)",
                    mcaState->mcaRequest, mcaState->q6Clk, mcaState->ib,
                    mcaState->ab, mcaState->latency, q6ClkTmp );

  return q6ClkTmp;
}


/**
  @brief mca_driver_fcn

  This is the driver function for MCA node. This function issues
  state requests to the clocks and bus. 

  @param resource: The /core/mca resource
  @param client: The handle to the current client
  @param state: Work request type.

  @return : None.
*/
static npa_resource_state mca_driver_fcn( npa_resource *resource,
                                          npa_client *client,
                                          npa_resource_state state )
{
  static mcaInternalStateType preSupMcaState, preSup2McaState;
  boolean bIsIbAbVoteChanged = FALSE;
  mcaInternalStateType * activeMcaState,*newMcaState;
  unsigned clkDepIdx, busDepIdx;

  switch ( client->type )
  {
    case NPA_CLIENT_INTERNAL:
#ifdef MCA_ENABLE_DIAG_HOOKS
      if ( mcaHookMode == 1 )
      {
        state = mcaFixedQ6;
        bIsIbAbVoteChanged = TRUE;
      }
#endif
    case NPA_CLIENT_VECTOR:
    case NPA_CLIENT_SUPPRESSIBLE_VECTOR:
    case NPA_CLIENT_SUPPRESSIBLE2_VECTOR:
      if ( client->type == NPA_CLIENT_SUPPRESSIBLE2_VECTOR )
      {
        activeMcaState = &preSup2McaState;
        newMcaState = &mcaData.mcaSup2State;
        clkDepIdx = 1;
        busDepIdx = 3;
      }
      else
      {
        activeMcaState = &preSupMcaState;
        newMcaState = &mcaData.mcaSupState;
        clkDepIdx = 0;
        busDepIdx = 2;
      }
      //pass request attributes if any
      npa_pass_request_attributes( client, NPA_DEPENDENCY( resource, clkDepIdx ) );

      if ( state <= activeMcaState->q6Clk )
      {
        npa_set_request_attribute( NPA_DEPENDENCY( resource, clkDepIdx ), NPA_REQUEST_FIRE_AND_FORGET );
      }
      /* Request the state into the clock resource */
      npa_issue_required_request( NPA_DEPENDENCY( resource, clkDepIdx ), state );
      activeMcaState->q6Clk = state;

      //Issue request core/mca/bus only if Ib, Ab, or latency vote changes
      if ( newMcaState->ib != activeMcaState->ib ||
           newMcaState->ab != activeMcaState->ab ||
           newMcaState->latency != activeMcaState->latency )
      {
        bIsIbAbVoteChanged = TRUE;
      }

      if ( TRUE == bIsIbAbVoteChanged )
      {
        //pass request attributes if any
        npa_pass_request_attributes( client, NPA_DEPENDENCY( resource, busDepIdx ) );

        //Set FIRE_AND_FOREGET if requests are less than the current active ones
        if ( newMcaState->ib <= activeMcaState->ib &&
             newMcaState->ab <= activeMcaState->ab )
        {
          npa_set_request_attribute( NPA_DEPENDENCY( resource, busDepIdx ), NPA_REQUEST_FIRE_AND_FORGET );
        }

        /* Request the state into the bus resource */
        npa_issue_internal_request( NPA_DEPENDENCY( resource, busDepIdx ) );
        activeMcaState->ib = newMcaState->ib;
        activeMcaState->ab = newMcaState->ab;
        activeMcaState->latency = newMcaState->latency;
      }

    default:
      break;
  }
  return state;
}

/**
  @brief NodeCoreMcaCreation_callback 

  This function is the callback from the single resource node 
  created while initializing MCA.  
  The callback will be invoked after the node is fully
  constructed.  The callback can be NULL if creation notification is
  unneeded. If invoked, the callback arguments are (context, 0,
  node->name, sizeof(const char *)).
  
  @param  context: NULL
  @param  event_type: The type of the event.
  @param  data: The name of the node being created.
  @param  data_size: The length of the node name.

  @return : None.

*/
static void NodeCoreMcaCreation_callback( void *context,
                                          unsigned int event_type,
                                          void *data,
                                          unsigned int data_size )
{
  /* MCA Test client for config of algo output over diag */
#ifdef MCA_ENABLE_DIAG_HOOKS
  mcaDiagClient = npa_create_sync_client( "/core/mca",
                                          "MCA_DIAG_Client",
                                          NPA_CLIENT_INTERNAL );
  CORE_VERIFY_PTR( mcaDiagClient );
#endif

  /*create ulog*/
  CORE_DAL_VERIFY( ULogFront_RealTimeInit( &mcaData.log,
                                           "MCA Log",
                                           16384,  // Initial Buffer Size
                                           ULOG_MEMORY_LOCAL,
                                           ULOG_LOCK_OS ) );

  /* Write the header to allow the visualization tool to know which parser to apply  */
  ULogCore_HeaderSet( mcaData.log, "Content-Type: text/tagged-log-1.0; title=MCA running history" );
  mcaTargetInitFudgeFactors( &mcaData.fudgeFactors );

  //initialize Corner levels to zero
  mcaData.nCorners = 0;
  if ( DALPLATFORMINFO_TYPE_RUMI != DalPlatformInfo_Platform() )
  {
    mcaTargetPopulateCornerMaxFreqValues();
  }

}


/**
  @brief mca_destroy_client_fcn
*/
void mca_destroy_client_fcn( npa_client *client )
{
  if ( client->resource_data )
  {
    DALSYS_Free( client->resource_data );
    client->resource_data = NULL;
  }
}


/**
  @brief mca_create_client_fcn
*/
void mca_create_client_fcn( npa_client *client ) //sga spaces
{
  //only need to allocate memory here if the client is a vector client
  if ( client->type == NPA_CLIENT_VECTOR
       || client->type == NPA_CLIENT_SUPPRESSIBLE_VECTOR
       || client->type == NPA_CLIENT_SUPPRESSIBLE2_VECTOR )
  {
    DALSYS_Malloc( sizeof(mcaInternalStateType), (void **)&client->resource_data );
    CORE_VERIFY_PTR( client->resource_data );
    
    DALSYS_memset( client->resource_data, 0, sizeof(mcaInternalStateType) );
  }
}

/* The plugin definition for /core/mca resource */
const npa_resource_plugin mca_plugin =
{
  mca_update_fcn,
  NPA_CLIENT_VECTOR |
  NPA_CLIENT_SUPPRESSIBLE_VECTOR |
  NPA_CLIENT_SUPPRESSIBLE2_VECTOR |
  NPA_CLIENT_RESERVED2 |
  NPA_CLIENT_LIMIT_MAX,               /* Supported client types */
  mca_create_client_fcn,            /* Create client function */
  mca_destroy_client_fcn,           /* Destroy client function */
  NULL                                /* Extended client create function */
};


/* /core/mca resource definition */
static npa_resource_definition mca_res[] =
{
  { "/core/mca",
    "KVPS",
    1,                          // Will be updated upon client initialization
    &mca_plugin,                // The resource plugin.
    NPA_RESOURCE_VECTOR_STATE,  // The resource attributes.
    NULL,                  // The user data field.
    NULL }
};


/* /core/mca node definition */
static npa_node_definition mca_node =
{
  "/node/core/mca",                       /* name */
  mca_driver_fcn,                     /* driver_fcn */
  NPA_NODE_DEFAULT,                   /* attributes */
  NULL,                                   /* data */
  NPA_ARRAY( targetNodeDepsMCA ),
  NPA_ARRAY( mca_res )
};


/*=======================================================================

   PUBLIC FUNCTION DEFINITIONS -- CORE MCA RESOURCE RELATED FUNCTIONS

========================================================================*/
void NodeCoreMcaCreate( void )
{
  /* Initial state of the resource */
  npa_resource_state initialState[] =
  {
    0
  };

  npa_define_node( &node_mca_bus, initialState, NULL );

  npa_define_node_cb( &mca_node, initialState, NodeCoreMcaCreation_callback, NULL );
}

#endif

