/*===========================================================================

  Copyright (c) 2014,2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_target_quotients.h"
#include "cpr_device_hw_version.h"


//MSS config.  
// 8996 MSS Version 1,2
static const cpr_target_quotient_versioned_config_t mss_quotients_v1_2 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {    //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0x3, 0x0),   0,   0xff},
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0x3, 0x0),   0,   0xff},
        },
        .foundry_range_count = 2,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7],  RO[8],  RO[9],  RO[10],  RO[11],  RO[12],  RO[13],  RO[14],  RO[15]
        {CPR_VOLTAGE_MODE_TURBO,       { 0,      0,      0,      0,      899,    805,    0,      1083,   0,      1149,   0,       0,       0,       0,       0,       0}},
        {CPR_VOLTAGE_MODE_NOMINAL,     { 0,      0,      0,      0,      669,    600,    0,      851,    0,      905,    0,       0,       0,       0,       0,       0}},
        {CPR_VOLTAGE_MODE_SVS,         { 0,      0,      0,      0,      399,    362,    0,      564,    0,      602,    0,       0,       0,       0,       0,       0}},
        {CPR_VOLTAGE_MODE_SVS2,        { 0,      0,      0,      0,      249,    231,    0,      394,    0,      421,    0,       0,       0,       0,       0,       0}},  
        {CPR_VOLTAGE_MODE_SVS_MIN,     { 0,      0,      0,      0,      132,    130,    0,      252,    0,      270,    0,       0,       0,       0,       0,       0}},  
    },
    .ro_kv_x_100 = { 0, 0, 0, 0, 227, 200, 0, 241, 0, 254, 0, 0, 0, 0, 0, 0 },
};

// 8996 MSS Version 3 and beyond
static const cpr_target_quotient_versioned_config_t mss_quotients_v3 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {     //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev 
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0x3, 0x0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),   0,   0xff },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0x3, 0x0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),   0,   0xff },
        },
        .foundry_range_count = 2,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
            {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7],  RO[8],  RO[9],  RO[10],  RO[11],  RO[12],  RO[13],  RO[14],  RO[15]
        {CPR_VOLTAGE_MODE_TURBO,       { 0,      0,      0,      0,        0,      0,    771,     725,   984,    970,     1036,    1024,       0,       0,       0,       0}},
        {CPR_VOLTAGE_MODE_NOMINAL,     { 0,      0,      0,      0,        0,      0,    577,     543,   798,    768,      823,     810,       0,       0,       0,       0}},
        {CPR_VOLTAGE_MODE_SVS,         { 0,      0,      0,      0,        0,      0,    414,     392,   584,    576,      608,     612,       0,       0,       0,       0}},
        {CPR_VOLTAGE_MODE_SVS2,        { 0,      0,      0,      0,        0,      0,    287,     273,   425,    426,      443,     453,       0,       0,       0,       0}},  
        {CPR_VOLTAGE_MODE_SVS_MIN,     { 0,      0,      0,      0,        0,      0,      0,       0,     0,      0,        0,       0,       0,       0,       0,       0}},  
    },
    //               0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,   14,   15
    .ro_kv_x_100 = { 0,   0,   0,   0,   0,   0, 204, 192, 196, 213, 225, 225,   0,   0,    0,   0 },
};


static const cpr_target_quotient_rail_config_t mss_quotient_config =
{
    .rail_id = CPR_RAIL_MSS,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &mss_quotients_v1_2,
        &mss_quotients_v3,
    },
    .versioned_target_quotient_config_count = 2,
};




const cpr_target_quotient_global_config_t cpr_bsp_target_quotient_config = 
{
    .rail_config = (const cpr_target_quotient_rail_config_t*[])
    {
        &mss_quotient_config,
    },
    .rail_config_count = 1,
};
