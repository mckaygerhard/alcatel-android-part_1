/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_closed_loop.h"
#include "cpr_defs.h"
#ifdef WINSIM
#define MODEM_TOP_BASE 0
#else
#include "msmhwiobase.h"
#endif


#define MSS_CPR_WRAPPER_BASE   (MODEM_TOP_BASE + 0x001a4000)


static cpr_closed_loop_controller_config mss_cpr_controller_config = {
    .thread_count = 1,
    .sensor_count = 35,
};




static const cpr_closed_loop_rail_config_t mss_config =
{
    .rail_id = CPR_RAIL_MSS,
    .controller_config = &mss_cpr_controller_config,
    .ref_clk_resource = "clk_rbcpr_ref",
    .ahb_clk_resource = "clk_bus_rbcpr",
    .hal_handle = { MSS_CPR_WRAPPER_BASE, 0 },
    .aging_sensor_ids =     (const uint8[]){11},
    .aging_sensor_count =       1,  // 0 = Don't use aging	
    .aging_ro_kv_x100 =         295,
    .aging_margin_limit =       20,  //12.5
    .aging_voltage_mode =       CPR_VOLTAGE_MODE_TURBO,	
    .step_quot = 40,
    .step_quot_min = 25, 
    .step_quot_max = 30,
    .interrupt_id = 144,
    .up_threshold = 0,
    .dn_threshold = 2,
    .consecutive_up = 0,
    .consecutive_dn = 2,
    .thermal_margin = 0,  //0 = no thermal adjustments
};


const cpr_closed_loop_config_t cpr_bsp_closed_loop_config =
{
    .closed_loop_config = (const cpr_closed_loop_rail_config_t*[])
    {
        &mss_config,
    },
    .closed_loop_config_count = 1,
};
