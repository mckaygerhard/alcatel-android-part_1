/*===========================================================================

  Copyright (c) 2014, 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.
  
  Fuses spread across multiple locations are always defined from MSB to LSBs. As the fuse reader
  code reads partial values, it shifts previous results left to make room for the new bits
  Example config showing the required order from MSBs to LSBs:
             CPR_FUSE_MAPPING(QFPROM__ROW1_LSB, CPRn_TARG_VOLT_OFFSET_SVS2),       //bit 7
             CPR_FUSE_MAPPING(QFPROM__ROW3_MSB, CPRn_TARG_VOLT_OFFSET_SVS2_BIT_1), //bit 6
             CPR_FUSE_MAPPING(QFPROM__ROW2_MSB, CPRn_TARG_VOLT_OFFSET_SVS2),       //bits 4-5
             CPR_FUSE_MAPPING(QFPROM__ROW2_MSB, CPRn_TARG_VOLT_OFFSET_SVS),      //bits 0-3

  ===========================================================================*/

#include "cpr_fuses.h"
#include "cpr_device_hw_version.h"
#include "cpr_qfprom.h"
#include "HALhwio.h"
#ifdef WINSIM
#define SECURITY_CONTROL_BASE 0
#endif

// 8996 V1,V2
const cpr_bsp_fuse_versioned_config_t cpr_bsp_fuse_config_v0_2 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {    //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0x3, 0),   0,   0xff },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0x3, 0),   0,   0xff },
        },
        .foundry_range_count = 2,
    },
    .rail_fuse_config = (cpr_bsp_fuse_rail_config_t[])
    {
        {   //Vdd_Cx
            .rail_id = CPR_RAIL_CX,
            .number_of_fuses = 4,
            .individual_fuse_configs = (cpr_bsp_individual_fuse_config_t[])
            {
                {
                    .fuse_type = CPR_FUSE_SVS2,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
                            CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR0_TARG_VOLT_SVS2)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_SVS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
                            CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR0_TARG_VOLT_SVS)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
                            CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR0_TARG_VOLT_NOM)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
                            CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR0_TARG_VOLT_TUR)
                        },
                },
            },
        },
        {   //Vdd_Mss
            .rail_id = CPR_RAIL_MSS,
            .number_of_fuses = 5,
            .individual_fuse_configs = (cpr_bsp_individual_fuse_config_t[])
            {
                {
                    .fuse_type = CPR_FUSE_SVS2,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
                            CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_MSB, CPR1_TARG_VOLT_SVS2)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_SVS,
                    .number_of_partial_fuse_configs = 2,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
                            CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_MSB, CPR1_TARG_VOLT_SVS_4_2),
                            CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR1_TARG_VOLT_SVS_1_0)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
                            CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR1_TARG_VOLT_NOM)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
                            CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR1_TARG_VOLT_TUR)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_AGING,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                    {
                        //{ 0, 0, 1 }  //test value.  Fuses with an address of 0 are flagged as test values and return the mask value
                        { HWIO_QFPROM_CORR_PTE_ROW3_MSB_ADDR, 23, 0x1F800000 }    //CPR_MSS_AGING
                    },
                },
            },
        },
    },
    .number_of_fused_rails = 2,
};


// 8996 V3 and beyond
const cpr_bsp_fuse_versioned_config_t cpr_bsp_fuse_config_v3 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {      //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(3, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),   0,   0xff },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(3, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),   0,   0xff },
        },
        .foundry_range_count = 2,
    },
    .rail_fuse_config = (cpr_bsp_fuse_rail_config_t[])
            {
                {   //Vdd_Cx
                    .rail_id = CPR_RAIL_CX,
                    .number_of_fuses = 4,
                    .individual_fuse_configs = (cpr_bsp_individual_fuse_config_t[])
                    {
                        {
                            .fuse_type = CPR_FUSE_SVS2,
                            .number_of_partial_fuse_configs = 1,
                            .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                            {
                                CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR0_TARG_VOLT_SVS2)
                            },
                        },
                        {
                            .fuse_type = CPR_FUSE_SVS,
                            .number_of_partial_fuse_configs = 1,
                            .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                            {
                                CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR0_TARG_VOLT_SVS)
                            },
                        },
                        {
                            .fuse_type = CPR_FUSE_NOMINAL,
                            .number_of_partial_fuse_configs = 1,
                            .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                            {
                                CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR0_TARG_VOLT_NOM)
                            },
                        },
                        {
                            .fuse_type = CPR_FUSE_TURBO,
                            .number_of_partial_fuse_configs = 1,
                            .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                            {
                                CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR0_TARG_VOLT_TUR)
                            },
                        },
                    },
                },
                {   //Vdd_Mss
                    .rail_id = CPR_RAIL_MSS,
                    .number_of_fuses = 7,
                    .individual_fuse_configs = (cpr_bsp_individual_fuse_config_t[])
                    {
                        {
                            .fuse_type = CPR_FUSE_SVS2,
                            .number_of_partial_fuse_configs = 1,
                            .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                            {
                                CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_MSB, CPR1_TARG_VOLT_SVS2)
                            },
                        },
                        {
                            .fuse_type = CPR_FUSE_SVS,
                            .number_of_partial_fuse_configs = 2,
                            .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                            {
                                CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_MSB, CPR1_TARG_VOLT_SVS_4_2),
                                CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR1_TARG_VOLT_SVS_1_0)
                            },
                        },
                        {
                            .fuse_type = CPR_FUSE_NOMINAL,
                            .number_of_partial_fuse_configs = 1,
                            .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                            {
                                CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR1_TARG_VOLT_NOM)
                            },
                        },
                        {
                            .fuse_type = CPR_FUSE_TURBO,
                            .number_of_partial_fuse_configs = 1,
                            .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                            {
                                CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR1_TARG_VOLT_TUR)
                            },
                        },
                        {   /* V3 introduces fused floor to ceiling ranges*/
                            .fuse_type = CPR_FUSE_SVS_OFFSET,
                            .number_of_partial_fuse_configs = 1,
                            .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                            {
                                //It looks funny, but CPR1_OFFSET_NOM is correct for this SVS value.  This address will be used for SVS as of 9/12/15
                                CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW12_MSB, CPR1_OFFSET_NOM)  
                                //{ 0, 0, 1 }  //test value.  Fuses with an address of 0 are flagged as test values and return the mask value
                            },
                        },
                        {
                            .fuse_type = CPR_FUSE_TURBO_OFFSET,
                            .number_of_partial_fuse_configs = 1,
                            .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                            {
                               CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW12_MSB, CPR1_OFFSET_TUR)
	                           //{ 0, 0, 1 }  //test value.  Fuses with an address of 0 are flagged as test values and return the mask value
                            },
                        },
                        {
                            .fuse_type = CPR_FUSE_AGING,
                            .number_of_partial_fuse_configs = 1,
                            .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                            {
                                //{ 0, 0, 1 }  //test value.  Fuses with an address of 0 are flagged as test values and return the mask value
                                { HWIO_QFPROM_CORR_PTE_ROW3_MSB_ADDR, 23, 0x1F800000 }    //CPR_MSS_AGING
                            },
                        },
                    },
                },
    },
    .number_of_fused_rails = 2,
};

//what version of CPR fuse revs are being used.
cpr_bsp_individual_fuse_config_t cpr_bsp_fusing_rev_info =
{
    .fuse_type = CPR_FUSE_FUSING_REV,
    .number_of_partial_fuse_configs = 1,
    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
    {
        { HWIO_QFPROM_CORR_PTE_ROW1_MSB_ADDR, 16, (1 << 18) | (1 << 17) | (1 << 16), }
		//{ 0, 0, 1 }  //test value.  Fuses with an address of 0 are flagged as test values and return the mask value
    },
};
 
const cpr_config_bsp_fuse_config_t cpr_bsp_fuse_config =
{
    .versioned_fuses = (const cpr_bsp_fuse_versioned_config_t*[])
    {
        &cpr_bsp_fuse_config_v0_2,
        &cpr_bsp_fuse_config_v3,
    },
    .versioned_fuses_count = 2,
};

