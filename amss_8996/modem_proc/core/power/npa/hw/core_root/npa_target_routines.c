/*============================================================================
@file npa_target_routines.c

Copyright (c) 2010 - 2014 Qualcomm Technologies Incorporated.
All Rights Reserved.
QUALCOMM Proprietary/GTDR

$Header: //components/rel/core.mpss/3.4.c3.11/power/npa/hw/core_root/npa_target_routines.c#1 $
============================================================================*/
#include "npa_internal.h"
#include "npa_remote_resource.h"

/* RPM available callback, defined in npa_remote_rpm_protocol.c */
extern void npa_rpm_available_cb( void*, unsigned int, void*, unsigned int );

#ifdef NPA_USES_QDI
/* NPA/QDI init routine, defined in npa_remote_qdi_drv.c */
extern void npa_qdi_init(void);

/* TODO: Review at some point to see if all of the below are needed for PD1
   In any case, resources that are never defined will not be published */
static const char *resources_for_pd1[] = 
{ 
  "/core/cpu/latency",
  "/core/cpu/latency/usec",
  "/core/cpu/wakeup",
  "/core/cpu/vdd",
  "/rail/vdd_dig",
  "/core/cpu/l2cache",
  "/core/uSleep",
  "/sleep/max_duration/usec",
  "/clk/cpu"
};
#endif /* NPA_USES_QDI */

void npa_target_init( void )
{
  npa_resource_available_cb( "/init/rpm", npa_rpm_available_cb,  NULL ); 

#ifdef NPA_USES_QDI
  npa_qdi_init();
  
  /* Publish some resources to PD1 */
  npa_remote_publish_resources( NPA_PD_1, 9, resources_for_pd1 );
  
  /* Publish this resource to all user PDs */
  npa_remote_publish_resource( NPA_ALL_PDS, "/clk/qdss" );
#endif
}

#ifdef NPA_SCHEDULED_REQUESTS_SUPPORTED
#include "rcinit.h"

/* Create the NPA Scheduler Thread */
void npa_scheduler_init_rctask( void )
{
  RCINIT_INFO handle = rcinit_lookup("npaScheduler");

  if (RCINIT_NULL != handle)
  {
    rcinit_initfn_spawn_task(handle, npaScheduler_task);
  }
}
#endif /* NPA_SCHEDULED_REQUESTS_SUPPORTED */
