/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

/**
 * @file qdi_client.c - implementation of the DAPSPM client library for QDI remoting
 *
 *  Created on: Jun 11, 2012
 *      Author: yrusakov
 */

#include "stdlib.h"
#include "qurt_qdi_constants.h"
#include "qurt_qdi_driver.h"
#include "qurt_qdi.h"
#include "qurt_thread.h"
#include "adsppm_qdi.h"
#include "mmpm.h"
#include "ULog.h"
#include "ULogFront.h"
#include "adsppm_utils.h"
#include "qdi_client_log.h"

#define STKSIZE  4096

typedef struct{
    int handle;
    int initialized;
} QDI_Adsppm_Client_Ctx_Type;


static QDI_Adsppm_Client_Ctx_Type Adsppm_Client_Ctx;

static void Adsppmcb_init_Client(void);

static void mmpm_print_reqdata(uint32 clientId,  MmpmRscExtParamType *pRscExtParam);

static void mmpm_print_reqdata(uint32 clientId,  MmpmRscExtParamType *pRscExtParam)
{
    uint32 index = 0, clkNum = 0, bwNum = 0;
    if(NULL != pRscExtParam)
	{
	    if(NULL != pRscExtParam->pReqArray)
		{
			for( index =0; index < pRscExtParam->numOfReq; index++)
			{
				{
					 switch (pRscExtParam->pReqArray[index].rscId)
					 {
					 case MMPM_RSC_ID_POWER:
						ADSPPM_QDI_DBG_MSG_1(MSG_LEGACY_HIGH, "Client %d: Power request", clientId);
						break;
					case MMPM_RSC_ID_REG_PROG:
					     ADSPPM_QDI_DBG_MSG_1(MSG_LEGACY_HIGH, "Client %d:Register programming request ", clientId);
						 break;
					case MMPM_RSC_ID_CORE_CLK:
						 ADSPPM_QDI_DBG_MSG_1(MSG_LEGACY_HIGH, "Client %d:Clock request:", clientId);
						 if((NULL != pRscExtParam->pReqArray[index].rscParam.pCoreClk) && (NULL != pRscExtParam->pReqArray[index].rscParam.pCoreClk->pClkArray))
						 {
						     for(clkNum = 0; clkNum < pRscExtParam->pReqArray[index].rscParam.pCoreClk->numOfClk; clkNum++)
							 {
								 ADSPPM_QDI_DBG_MSG_3(MSG_LEGACY_HIGH, "  clkId=%d, freqHz = %d, match = %d ",
                                            pRscExtParam->pReqArray[index].rscParam.pCoreClk->pClkArray[clkNum].clkId.clkIdLpass,
                                            pRscExtParam->pReqArray[index].rscParam.pCoreClk->pClkArray[clkNum].clkFreqHz,
                                            pRscExtParam->pReqArray[index].rscParam.pCoreClk->pClkArray[clkNum].freqMatch);
							 }
						 }
						 break;
					case MMPM_RSC_ID_SLEEP_LATENCY:
					    ADSPPM_QDI_DBG_MSG_2(MSG_LEGACY_HIGH, "Client %d:Latency request: latency =%d ", clientId, pRscExtParam->pReqArray[index].rscParam.sleepMicroSec);
						break;
					case MMPM_RSC_ID_MIPS_EXT:
					    ADSPPM_QDI_DBG_MSG_1(MSG_LEGACY_HIGH, "Client %d requesting MIPS:", clientId);
						if(NULL != pRscExtParam->pReqArray[index].rscParam.pMipsExt)
						{
							ADSPPM_QDI_DBG_MSG_4(MSG_LEGACY_HIGH, "  MIPS request: total=%d, MIPS Floor = %d, location = %d, operation =%d ",
															pRscExtParam->pReqArray[index].rscParam.pMipsExt->mipsTotal,
															pRscExtParam->pReqArray[index].rscParam.pMipsExt->mipsPerThread,
															pRscExtParam->pReqArray[index].rscParam.pMipsExt->codeLocation,
															pRscExtParam->pReqArray[index].rscParam.pMipsExt->reqOperation);
						}
						break;
					case MMPM_RSC_ID_GENERIC_BW:
					    ADSPPM_QDI_DBG_MSG_1(MSG_LEGACY_HIGH, "Client %d: BW request ", clientId);
						if((NULL != pRscExtParam->pReqArray[index].rscParam.pGenBwReq) && (NULL != pRscExtParam->pReqArray[index].rscParam.pGenBwReq->pBandWidthArray))
						{
							ADSPPM_QDI_DBG_MSG_5(MSG_LEGACY_HIGH,
							"BW request: master %d, slave %d usageType = %d, bw=%ld, percentage=%d ",
							pRscExtParam->pReqArray[index].rscParam.pGenBwReq->pBandWidthArray[bwNum].busRoute.masterPort,
							pRscExtParam->pReqArray[index].rscParam.pGenBwReq->pBandWidthArray[bwNum].busRoute.slavePort,
							pRscExtParam->pReqArray[index].rscParam.pGenBwReq->pBandWidthArray[bwNum].bwValue.busBwValue.usageType,
							pRscExtParam->pReqArray[index].rscParam.pGenBwReq->pBandWidthArray[bwNum].bwValue.busBwValue.bwBytePerSec,
							pRscExtParam->pReqArray[index].rscParam.pGenBwReq->pBandWidthArray[bwNum].bwValue.busBwValue.usagePercentage
							);							
						}
						break;
					case MMPM_RSC_ID_THERMAL:
					  ADSPPM_QDI_DBG_MSG_2(MSG_LEGACY_HIGH, "Client %d:Thermal request: level = %d ", clientId, pRscExtParam->pReqArray[index].rscParam.thermalMitigation);
				 	  break;
					case MMPM_RSC_ID_MEM_POWER:
					   ADSPPM_QDI_DBG_MSG_1(MSG_LEGACY_HIGH, "Client %d: Memory power request ", clientId);
					  if(NULL != pRscExtParam->pReqArray[index].rscParam.pMemPowerState)
					  {
						ADSPPM_QDI_DBG_MSG_2(MSG_LEGACY_HIGH, "memoryid = %d powerstate = %d ",
							pRscExtParam->pReqArray[index].rscParam.pMemPowerState->memory, 
							pRscExtParam->pReqArray[index].rscParam.pMemPowerState->powerState);
					   }
					   break;
					case MMPM_RSC_ID_CORE_CLK_DOMAIN:
					 ADSPPM_QDI_DBG_MSG_1(MSG_LEGACY_HIGH, "Client %d:Clock Domain request:", clientId);
					 if((NULL != pRscExtParam->pReqArray[index].rscParam.pCoreClkDomain) && (NULL != pRscExtParam->pReqArray[index].rscParam.pCoreClkDomain->pClkDomainArray))
					 {
					    for(clkNum = 0; clkNum < pRscExtParam->pReqArray[index].rscParam.pCoreClkDomain->numOfClk; clkNum++)
						{
						    	ADSPPM_QDI_DBG_MSG_2(MSG_LEGACY_HIGH, "lpassclkid =%d clkdomainsource =%d ",
								pRscExtParam->pReqArray[index].rscParam.pCoreClkDomain->pClkDomainArray[clkNum].clkId.clkIdLpass, 
								pRscExtParam->pReqArray[index].rscParam.pCoreClkDomain->pClkDomainArray[clkNum].clkDomainSrc.clkDomainSrcIdLpass);
						}
					 }
					 break;
                     default:
                     break;
					 }
				}
			}
		}
	}
}
/*======================================================================*/
/**
MMPM_Register_Ext

@brief MMPM_Register_Ext API is used to register access to a device.

    This API has to be called prior to calls of MMPM_Request(), MMPM_Release(), MMPM_Resume(),
    and MMPM_Pause(), as the returned client ID from MMPM_Register_Ext() will be needed as input to these
    API calls.

    The MMPM_GetInfo call does not need registration to MMPM.

@param pRegParam - A pointer to the MmpmRegParamType structure.

@return   On success, a unique client ID. On failure, 0.

@callgraph
@callergraph
 */
/*============================================================================*/
uint32          MMPM_Register_Ext(MmpmRegParamType  *pRegParam)
{
    int result = 0;
    uint32 clientId = 0;
    if((Adsppm_Client_Ctx.initialized) && (NULL !=pRegParam))
    {
	    ADSPPM_QDI_DBG_MSG_3(MSG_LEGACY_HIGH, "client=%s register for coreid=%d, instanceid=%d", 
		                     pRegParam->pClientName, pRegParam->coreId, pRegParam->instanceId);
        result = qurt_qdi_handle_invoke(Adsppm_Client_Ctx.handle, ADSPPM_REGISTER, pRegParam, &clientId);
		ADSPPM_QDI_DBG_MSG_1(MSG_LEGACY_HIGH, "return clientid=%d", clientId);
        if(0 > result)
        {
			ADSPPM_QDI_DBG_MSG_5(MSG_LEGACY_ERROR, "client=%s register for coreid=%d, instanceid=%d qdi failed, result=%d clientid=%d,set to 0", 
		                     pRegParam->pClientName, pRegParam->coreId, pRegParam->instanceId, result, clientId);		
            clientId = 0;			
        }
    }
    return clientId;
}


/*======================================================================*/
/**
MMPM_Deregister_Ext

@brief MMPM_Deregister_Ext is used to deregister access to the device.

    All usecases associated with the device will be removed, with its requested resources released.
    The device register ID will be 0.

@param clientId - the registered client ID.

@return On success MMPM_STATUS_SUCCESS.

@callgraph
@callergraph
 */
/*============================================================================*/
MMPM_STATUS     MMPM_Deregister_Ext(uint32          clientId)
{
    MMPM_STATUS result = MMPM_STATUS_NOTINITIALIZED;
    int ret = 0;

    if(Adsppm_Client_Ctx.initialized)
    {
	    ADSPPM_QDI_DBG_MSG_1(MSG_LEGACY_HIGH, "clientid=%d deregistered", clientId); 
        ret = qurt_qdi_handle_invoke(Adsppm_Client_Ctx.handle, ADSPPM_DEREGISTER, clientId, &result);
		
    }
    if(ret < 0)
    {
	    ADSPPM_QDI_DBG_MSG_3(MSG_LEGACY_ERROR, "clientid=%d deregister QDI failed, qdiret=%d, result=%d from acpm,set to failed", 
		                     clientId, ret, result); 
        result = MMPM_STATUS_FAILED;
    }
    return result;
}

/**
MMPM_Request_Ext

@brief MMPM_Request_Ext is used to make multiple request calls within one call.

    Multiple requests will be executed and the API call will end when all requests are done execution.

    When each request inside the multiple requests is executed, its behavior is identical to that of MMPM_Request.

    For individual return status for each request, Client needs to parse the pStsArray inside the pRscExtParam.

@param clientId - the registered client ID.
@param pRscExtParam - A pointer to the MmpmRscExtParamType structure.that contains multiple requests' parameters

@return On success MMPM_STATUS_SUCCESS.

@callgraph
@callergraph
 */
/*============================================================================*/
MMPM_STATUS     MMPM_Request_Ext(uint32 clientId,
        MmpmRscExtParamType *pRscExtParam)
{
    MMPM_STATUS result = MMPM_STATUS_NOTINITIALIZED;
    int ret = 0;

    if((Adsppm_Client_Ctx.initialized) && (NULL != pRscExtParam))
    {
	    mmpm_print_reqdata(clientId, pRscExtParam);
        ret = qurt_qdi_handle_invoke(Adsppm_Client_Ctx.handle, ADSPPM_REQUEST, clientId, pRscExtParam, &result);
    }
    if(ret < 0)
    {
	    ADSPPM_QDI_DBG_MSG_3(MSG_LEGACY_ERROR, "clientid=%d MMPM_Request_Ext QDI failed, qdiret=%d, result=%d from acpm,set to failed", 
		                     clientId, ret, result); 
        result = MMPM_STATUS_FAILED;
    }
    return result;
}

/*======================================================================*/
/**
MMPM_Release_Ext

@brief MMPM_Release_Ext is used to make multiple release calls within one call.

    Multiple releases will be executed and the API call will end when all releases are done execution.

    When each release inside the multiple releases is executed, its behavior is identical to that of MMPM_Release.

    For individual return status for each release, Client needs to parse the pStsArray inside the pRscExtParam.

@param clientId - the registered client ID.
@param pRscExtParam - A pointer to the MmpmRscExtParamType structure.that contains multiple releases' parameters

@return On success MMPM_STATUS_SUCCESS.

@callgraph
@callergraph
 */
/*============================================================================*/
MMPM_STATUS     MMPM_Release_Ext(uint32 clientId,
        MmpmRscExtParamType *pRscExtParam)
{
    MMPM_STATUS result = MMPM_STATUS_NOTINITIALIZED;
    int ret= 0;

    if(Adsppm_Client_Ctx.initialized)
    {
	//   mmpm_print_reqdata(clientId, pRscExtParam);
	  if((NULL != pRscExtParam) && (NULL != pRscExtParam->pReqArray))
	  {
	    ADSPPM_QDI_DBG_MSG_2(MSG_LEGACY_HIGH, "Client %d: release ", clientId, pRscExtParam->pReqArray[0].rscId);
	  }
        ret = qurt_qdi_handle_invoke(Adsppm_Client_Ctx.handle, ADSPPM_RELEASE, clientId, pRscExtParam, &result);
    }
    if(ret < 0)
    {
	   
	    ADSPPM_QDI_DBG_MSG_3(MSG_LEGACY_ERROR, "clientid=%d MMPM_Release_Ext QDI failed, qdiret=%d, result=%d from acpm,set to failed", 
		                     clientId, ret, result); 
        result = MMPM_STATUS_FAILED;
    }
    return result;
}
/**
MMPM_GetInfo

@brief MMPM_GetInfo is used to retrive resources info, this API call does NOT need pre-registration.

       Based on the information id specified in the MmpmInfoDataType structure, the MMPM
       returns the requested information to the registered device ID.

@param pInfoData - A pointer to the MmpmInfoDataType structure.

@return On success MMPM_STATUS_SUCCESS.

@callgraph
@callergraph
 */
/*============================================================================*/
MMPM_STATUS     MMPM_GetInfo(MmpmInfoDataType   *pInfoData)
{
    MMPM_STATUS result = MMPM_STATUS_NOTINITIALIZED;
    int ret = 0;

    if(Adsppm_Client_Ctx.initialized)
    {
        ret = qurt_qdi_handle_invoke(Adsppm_Client_Ctx.handle, ADSPPM_GET_INFO, pInfoData, &result);
    }
    if(ret < 0)
    {
	     ADSPPM_QDI_DBG_MSG_2(MSG_LEGACY_ERROR, "MMPM_GetInfo QDI failed, qdiret=%d, result=%d from acpm,set to failed", 
		                     ret, result); 
        result = MMPM_STATUS_FAILED;
    }
    return result;
}


/**
MMPM_SetParameter

@brief MMPM_SetParameter is used to set various client specific and global parameters.


@param pParamConfig - A pointer to the MmpmParameterConfigType structure.

@return On success MMPM_STATUS_SUCCESS.

@callgraph
@callergraph
*/
/*============================================================================*/
MMPM_STATUS     MMPM_SetParameter(uint32 clientId, MmpmParameterConfigType *pParamConfig)
{
    MMPM_STATUS result = MMPM_STATUS_NOTINITIALIZED;
    int ret= 0;

    if(Adsppm_Client_Ctx.initialized)
    {
        ret = qurt_qdi_handle_invoke(Adsppm_Client_Ctx.handle, ADSPPM_SET_PARAM, clientId, pParamConfig, &result);
    }
    if(ret < 0)
    {
	    ADSPPM_QDI_DBG_MSG_3(MSG_LEGACY_ERROR, "clientid=%d MMPM_SetParameter QDI failed, qdiret=%d, result=%d from acpm,set to failed", 
		                     clientId, ret, result); 
        result = MMPM_STATUS_FAILED;
    }
    return result;
}

/** for corebsp image init
 */
void ADSPPM_Init_Client(void)
{
    Adsppm_Client_Ctx.initialized = 0;
    Adsppm_Client_Ctx.handle = qurt_qdi_open(ADSPPM_QDI_DRV_NAME);
    //init mmpm user dispatcher thread
    Adsppmcb_init_Client();
    if(Adsppm_Client_Ctx.handle >= 0)
    {
        Adsppm_Client_Ctx.initialized = 1;
    }
	else
	{
	     ADSPPM_QDI_DBG_MSG_0(MSG_LEGACY_ERROR, "ADSPPM_Init_Client failed"); 
	}
}


/*======================================================================*/
/* MMPM_Deinit */
/*============================================================================*/
/* Deinitialize core MMPM modules.  */
void ADSPPM_Deinit_Client(void)
{
    ADSPPM_QDI_DBG_MSG_0(MSG_LEGACY_HIGH, "ADSPPM_Deinit_Client"); 
    qurt_qdi_close(Adsppm_Client_Ctx.handle);
    Adsppm_Client_Ctx.initialized = 0;
}

/*======================================================================*/
/* MMPM call back dispatcher thread */
/*============================================================================*/
/* dispatch thread in mmpm user process */

static void Adsppm_callback_dispatcher(void *arg)
{
    Adsppm_cbinfo_Client_t info;
    int ret =0;
    for (;;) {
        ret = qurt_qdi_handle_invoke(Adsppm_Client_Ctx.handle, ADSPPM_GET_CB, &info);
        if((0 ==ret) && (NULL != info.pfn))
        {
            info.callbackParam.callbackData = (void*) &info.callbackValue;
            (*info.pfn)(&info.callbackParam);
        }
    }
}

static void Adsppmcb_init_Client(void)
{
    qurt_thread_attr_t attr;
    qurt_thread_t tid;
    qurt_thread_attr_init(&attr);
    qurt_thread_attr_set_stack_size(&attr, STKSIZE);
    qurt_thread_attr_set_stack_addr(&attr, malloc(STKSIZE));
    qurt_thread_attr_set_priority(&attr, qurt_thread_get_priority(qurt_thread_get_id()));
    qurt_thread_create(&tid, &attr, Adsppm_callback_dispatcher, NULL);
}

uint32 MMPM_Init(char * cmd_line)
{
    MMPM_STATUS result = MMPM_STATUS_SUCCESS;
    ADSPPM_Init_Client();
    if(0 == Adsppm_Client_Ctx.initialized)
    {
        result = MMPM_STATUS_FAILED;
    }
    return result;
}

/** for corebsp image init
 */
void MMPM_Init_Ext(void)
{
    MMPM_Init(NULL);
}


