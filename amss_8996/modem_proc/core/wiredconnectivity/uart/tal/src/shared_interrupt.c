/*==================================================================================================

FILE: shared_interrupt.c

DESCRIPTION: This module defines the functions allowing multiple UART chips to have interrupts 
             processed in a single function call as opposed to having to having the thread
             continually be pinged with interrupts.

                     Copyright (c) 2015 Qualcomm Technologies Incorporated
                                        All Rights Reserved
                                     QUALCOMM Proprietary/GTDR

==================================================================================================*/
/*==================================================================================================
                                           INCLUDE FILES
==================================================================================================*/

#include "shared_interrupt.h"

/*==================================================================================================
                                             CONSTANTS
==================================================================================================*/

#define UART_INTERRUPT_MAX 16   //4 UARTs, 4 interrupts each

/*==================================================================================================
                                             TYPEDEFS
==================================================================================================*/

typedef struct shared_driver_node
{
   SharedIntHandle       handle;   //handle to which client has registered the interrupt
   DALInterruptID        irq;      //interrupt id
   DALISR                isr;      //client's callback function
   void                 *context;  //data to be passed to client's callback
} SHARED_DRIVER_NODE;

/*==================================================================================================
                                             LOCAL VARIABLES
==================================================================================================*/

static SHARED_DRIVER_NODE  shared_interrupt_list[UART_INTERRUPT_MAX];
static uint32              interrupts_registered = 0;
static uint32              current_handle = 1;
static DALSYSSyncHandle    driver_sync = NULL;
static DalDeviceHandle    *dal_handle = NULL;

/*==================================================================================================
                                        LOCAL FUNCTION PROTOTYPES
==================================================================================================*/
static DALResult   add_node(SharedIntHandle handle, DALInterruptID irq, DALISR isr, void *context);
static DALResult   remove_node(SharedIntHandle handle, DALInterruptID irq);
static boolean     irq_is_registered(DALInterruptID irq);
static void       *shared_interrupt_callback(void *irq);

/*==================================================================================================
                                             LOCAL FUNCTIONS
==================================================================================================*/

/*==================================================================================================
 
FUNCTION: add_node 
 
DESCRIPTION: 
   adds a node to the list of registered interrupts 
 
==================================================================================================*/
static DALResult add_node(SharedIntHandle handle, DALInterruptID irq, DALISR isr, void *context)
{
   if (interrupts_registered < UART_INTERRUPT_MAX)
   {
      shared_interrupt_list[interrupts_registered].handle = handle;
      shared_interrupt_list[interrupts_registered].irq = irq;
      shared_interrupt_list[interrupts_registered].isr = isr;
      shared_interrupt_list[interrupts_registered].context = context;
      interrupts_registered++;
      return DAL_SUCCESS;
   }
   else
   {
      return DAL_ERROR;
   }
}

/*==================================================================================================
 
FUNCTION: remove_node 
 
DESCRIPTION: 
   removes the desired node from the list of registered interrupts 
 
==================================================================================================*/
static DALResult remove_node(SharedIntHandle handle, DALInterruptID irq)
{
   uint32 i;

   for (i = 0; i < interrupts_registered; i++)
   {
      if (shared_interrupt_list[i].handle == handle &&
          shared_interrupt_list[i].irq == irq)
      {
         shared_interrupt_list[i] = shared_interrupt_list[interrupts_registered - 1];
         interrupts_registered--;
         return DAL_SUCCESS;
      }
   }
   return DAL_ERROR;
}

/*==================================================================================================
 
FUNCTION: irq_is_registered 
 
DESCRIPTION: 
   checks to see if a particular interrupt has been registered with our shared driver.
 
==================================================================================================*/
static boolean irq_is_registered(DALInterruptID irq)
{
   uint32 i;

   for (i = 0; i < interrupts_registered; i++)
   {
      if (irq == shared_interrupt_list[i].irq)
      {
         return TRUE;
      }
   }
   return FALSE;
}

/*==================================================================================================
 
FUNCTION: shared_interrupt_callback
 
DESCRIPTION: 
   Call the relevant ISR for any interrupt registered that shared the interrupt number we have
   received. 
 
==================================================================================================*/
static void *shared_interrupt_callback(void *irq)
{
   uint32 i;

   DALSYS_SyncEnter(driver_sync);
   for (i = 0; i < interrupts_registered; i++)
   {
      if (shared_interrupt_list[i].irq == (DALInterruptID) irq)
      {
         (shared_interrupt_list[i].isr)(shared_interrupt_list[i].context);
      }
   }
   DALSYS_SyncLeave(driver_sync);
   DalInterruptController_InterruptDone(dal_handle, (DALInterruptID)irq);
   return NULL;
}

/*==================================================================================================
                                             GLOBAL FUNCTIONS
==================================================================================================*/
/*==================================================================================================
 
FUNCTION: shared_interrupt_init
 
DESCRIPTION: 
   Initialize the SyncObject used by our driver. Called during RC_INIT 
 
==================================================================================================*/
void shared_interrupt_init()
{
   DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE, &driver_sync, NULL);
}

/*==================================================================================================
 
FUNCTION: shared_interrupt_register
 
DESCRIPTION: 
   Registers the interrupt with our shared driver. If this is the first time this interrupt is
   registered, then it is also registered with the DAL layer to use our shared_interrupt_callback
   function as its ISR. 
 
==================================================================================================*/
DALResult shared_interrupt_register(SharedIntHandle handle, DALInterruptID irq, DALISR isr,
                                    void *context, uint32 trigger)
{
   DALResult result;

   DALSYS_SyncEnter(driver_sync);
   if (irq_is_registered(irq) == FALSE)
   {
      result = DalInterruptController_RegisterISR(dal_handle, irq, shared_interrupt_callback,
                                                  (void *) irq, trigger);
      if (result != DAL_SUCCESS) { goto done; }
   }

   result = add_node(handle, irq, isr, context);

done:
   DALSYS_SyncLeave(driver_sync);
   return result;
}

/*==================================================================================================
 
FUNCTION: shared_interrupt_unregister
 
DESCRIPTION: 
   Unregisters the the particular interrupt from our shared driver. If no other client has this
   particular interrupt registered any longer, it is also unregistered form the DAL layer. 
 
==================================================================================================*/
DALResult shared_interrupt_unregister(SharedIntHandle handle, DALInterruptID irq)
{
   DALResult result;

   DALSYS_SyncEnter(driver_sync);
   //remove the interrupt from the current client from interrupt list
   if (remove_node(handle, irq) != DAL_SUCCESS)
   {
      result = DAL_ERROR;
      goto done;
   }
   //check to see if any client still has interrupt "registered". If so, we cannot unregister the
   //ISR from the DAL
   if (irq_is_registered(irq) == TRUE)
   {
      result = DAL_SUCCESS;
      goto done;
   }
   result = DalInterruptController_Unregister(dal_handle, irq);

done:
   DALSYS_SyncLeave(driver_sync);
   return result;
}

/*==================================================================================================
 
FUNCTION: shared_interrupt_open
 
DESCRIPTION: 
   Returns a new SharedIntHandle to the client. If this is the first time this is called, we also
   attach ourselves to the DAL layer
 
==================================================================================================*/
DALResult shared_interrupt_open(SharedIntHandle *pHandle)
{
   DALResult result;
   uint32 i;

   DALSYS_SyncEnter(driver_sync);
   if (dal_handle == NULL)
   {
      result = DAL_DeviceAttach(DALDEVICEID_INTERRUPTCONTROLLER, &dal_handle);
      if (result != DAL_SUCCESS) { goto done1; }

      result = DalDevice_Open(dal_handle, DAL_OPEN_SHARED);
      if (result != DAL_SUCCESS) { goto done2; }
   }

   *pHandle = (SharedIntHandle) current_handle;
   current_handle++;

   result = DAL_SUCCESS;
   goto done1;

done2:
   DAL_DeviceDetach(dal_handle);
done1:
   DALSYS_SyncLeave(driver_sync);
   return result;
}

/*==================================================================================================
 
FUNCTION: shared_interrupt_close
 
DESCRIPTION: 
   If no more interrupts are rigistered with our driver, we close and detach our DAL handle.
   Otherwise, a call to this function is a no-op.
 
==================================================================================================*/
void shared_interrupt_close(SharedIntHandle handle)
{
   (void *) handle;

   DALSYS_SyncEnter(driver_sync);
   if (interrupts_registered == 0 && dal_handle != NULL)
   {
      DalDevice_Close(dal_handle);
      DAL_DeviceDetach(dal_handle);
      dal_handle = NULL;
   }
   DALSYS_SyncLeave(driver_sync);
}
