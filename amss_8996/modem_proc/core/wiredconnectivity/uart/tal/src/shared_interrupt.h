#ifndef __SHARED_INTERRUPT_H__
#define __SHARED_INTERRUPT_H__

/*==================================================================================================

FILE: shared_interrupt.h

DESCRIPTION: This module defines the functions allowing multiple chips to have interrupts 
             processed in a single function call as opposed to having to having the thread
             continually be pinged with interrupts.

                     Copyright (c) 2015 Qualcomm Technologies Incorporated
                                        All Rights Reserved
                                     QUALCOMM Proprietary/GTDR

==================================================================================================*/

/*==================================================================================================
                                           INCLUDE FILES
==================================================================================================*/

#include "comdef.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include "DDIInterruptController.h"

/*==================================================================================================
                                             TYPEDEFS
==================================================================================================*/

typedef void* SharedIntHandle;

/*==================================================================================================
                                         FUNCTION PROTOTYPES
==================================================================================================*/

void       shared_interrupt_init();
DALResult  shared_interrupt_register(SharedIntHandle handle, DALInterruptID irq, DALISR isr,
                                     void *context, uint32 trigger);
DALResult  shared_interrupt_unregister(SharedIntHandle handle, DALInterruptID irq);
DALResult  shared_interrupt_open(SharedIntHandle *pHandle);
void       shared_interrupt_close(SharedIntHandle handle);

#endif
