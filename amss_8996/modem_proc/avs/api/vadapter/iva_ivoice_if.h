#ifndef __IVA_IVOICE_IF_H__
#define __IVA_IVOICE_IF_H__

/**
  @file  iva_ivoice_if.h
  @brief This file contains voice interface definitions of the IMS Voice
         Adapter.
*/

/*
  ============================================================================

   Copyright (C) 2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

                             Edit History

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/avs/api/vadapter/iva_ivoice_if.h#1 $
  $Author: mplcsds1 $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------


  ============================================================================
*/


/*----------------------------------------------------------------------------
  Include files for Module
----------------------------------------------------------------------------*/

#include "iva_icommon_if.h"


/****************************************************************************
 * IVA VOICE INTERFACE DEFINITION                                           *
 ****************************************************************************/

/** Open command (synchronous API).
 *
 * Use this command to acquire a handle to a new IMS Voice Adapter instance.
 *
 * The client must use the returned handle (ret_handle) provided by
 * the Open command to execute any subsequent commands.
 *
 * The client will use "vsid" associated with a subscription to indicate the 
 * voice session, it is interested in receiving events for.
 *
 * Upon a successful Open, the command shall return IVA_EOK to the client.
 *
 */
#define IVA_IVOICE_CMD_OPEN ( 0x0001315F )

typedef struct iva_ivoice_cmd_open_t iva_ivoice_cmd_open_t;

struct iva_ivoice_cmd_open_t {

  uint32_t* ret_handle;
    /**<
     * Returns the handle that IMS must use when making subsequent commands.
     */

  uint32_t vsid;
    /**< Voice System ID as defined by DCN 80-NF711-1 Rev E. */

  iva_icommon_event_callback_fn_t event_cb;
    /**<
     * Central event callback function, which receives asynchronous events
     * from the server.
     *
     * Operational contract:
     *
     * - The client may only queue the incoming event and signal a worker
     * thread to process the event. The client must not perform any other
     * processing in the callback context.
     *
     * - The client may not call any APIs on the server in the callback
     * context. This will cause synchronization issues for the driver and
     * may lead to a system failure or deadlock.
     *
     * - The client may not perform any blocking operations or acquire any
     * locks in the event callback context that lead to a system deadlock.
     *
     * - The client may spend no more than 5 us while in the callback
     * context.
     *
     * - It is highly recommended to use atomic operations for
     * synchronization needs.
     *
     * Failure to meet the operational contract may lead to an undefined
     * state with system stability and performance issues.
     *
     */

  void* session_context;
    /**<
     * Pointer to the session context. The client stores its session
     * context pointer here to retrieve its session control data structure,
     * which the client uses to queue and signal events into its worker
     * thread.
     *
     * The session_context is returned to the client each time an event
     * callback is triggered.
     */

};


/** Close command (synchronous blocking API.)
 *
 * Use this command to close the previously opened IMS Voice Adapter instance.
 *
 * The client may not use the open handle after receiving successful close
 * indication.
 *
 * Upon a successful close, the command shall return IVA_EOK to the caller.
 *
 */
#define IVA_IVOICE_CMD_CLOSE ( 0x00013160 )

typedef struct iva_ivoice_cmd_close_t iva_ivoice_cmd_close_t;

struct iva_ivoice_cmd_close_t {

  uint32_t handle;
    /**< Open handle. */

};


/** Request vocoder resource command (synchronous API.)
 *
 * The client requests that a vocoder resource be available.
 *
 * If the vocoder resource is available, the IVA_IVOICE_EVENT_GRANT_VOCODER
 * event is sent to the client immediately in IMS Voice Adapter context.
 * If the vocoder resource is not available with IMS voice adapter, then 
 * IVA_IVOICE_EVENT_GRANT_VOCODER event will be sent to the client when IVA 
 * acquires a voice resource.
 *
 * The vocoder resource can be taken back by the Voice Agent any time through
 * IMS Voice Adapter by sending IVA_IVOICE_EVENT_REVOKE_VOCODER, the client
 * should release the vocoder resource and send an acknowledgement back to the
 * IMS Voice Adapter.
 *
 */
#define IVA_IVOICE_CMD_REQUEST_VOCODER ( 0x00013161 )

typedef struct iva_ivoice_cmd_request_vocoder_t iva_ivoice_cmd_request_vocoder_t;

struct iva_ivoice_cmd_request_vocoder_t {

  uint32_t handle;
    /**< Open handle. */

};


/** Grant vocoder resource event (asynchronous event).
 *
 * IMS Voice Adapter sends this event to the client when the vocoder resource
 * is available.
 *
 * The client shall acknowledge with IVA_IVOICE_CMD_SET_GRANT_VOCODER_ACCEPTED
 * command to the IMS Voice Adapter.
 *
 * This event does not have any parameters.
 */
#define IVA_IVOICE_EVENT_GRANT_VOCODER ( 0x00013162 )


/** Set grant vocoder accepted command (synchronous API.)
 *
 * This command is sent by the client as an acknowledgement for accepting
 * the IVA_IVOICE_EVENT_GRANT_VOCODER event.
 *
 */
#define IVA_IVOICE_CMD_SET_GRANT_VOCODER_ACCEPTED ( 0x00013163 )

typedef struct iva_ivoice_cmd_set_grant_vocoder_accepted_t iva_ivoice_cmd_set_grant_vocoder_accepted_t;

struct iva_ivoice_cmd_set_grant_vocoder_accepted_t {

  uint32_t handle;
    /**< Open handle. */

};


/** Revoke vocoder resource event (asynchronous event).
 *
 * As a respone to this event, the client shall release the vocoder resource, 
 * perform all the required resource cleanup, and send an acknowledgement 
 * IVA_IVOICE_CMD_SET_RELEASE_VOCODER_DONE back to the IVA.
 *
 * This event does not have any parameters.
 *
 */
#define IVA_IVOICE_EVENT_REVOKE_VOCODER ( 0x00013164 )


/** Set release vocoder done command. (synchronous API.)
 *
 * This command is an acknowledgement to IMS voice adapter indicating
 * successful release of vocoder resource by the client.
 *
 * This commannd is usually sent by the client as a response to the event 
 * IVA_IVOICE_EVENT_REVOKE_VOCODER.
 *
 * It is also possible and acceptable for the client to send this command to
 * IVA, if the client had released the vocoder resource on its own.
 */
#define IVA_IVOICE_CMD_SET_RELEASE_VOCODER_DONE ( 0x00013165 )

typedef struct iva_ivoice_cmd_set_release_vocoder_done_t iva_ivoice_cmd_set_release_vocoder_done_t;

struct iva_ivoice_cmd_set_release_vocoder_done_t {

  uint32_t handle;
    /**< Open handle. */

};


#endif /* __IVA_IVOICE_IF_H__ */

