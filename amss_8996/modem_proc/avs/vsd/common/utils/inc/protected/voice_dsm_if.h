#ifndef __VOICE_DSM_IF_H__
#define __VOICE_DSM_IF_H__

/**
  @file  voice_dsm_if.h
  @brief This is the public header file that clients of GVA should include.
         This file includes all other GVA public header files and contains
         single entry point into the GVA.
*/

/*
  ============================================================================

   Copyright (C) 2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

                             Edit History

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/avs/vsd/common/utils/inc/protected/voice_dsm_if.h#1 $
  $Author: mplcsds1 $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------


  ============================================================================
*/

/****************************************************************************
  Include files for Module
****************************************************************************/

/* SYSTEM UTILS APIs. */
#include "dsmutil.h"
#include "mmdefs.h"

/* SELF APIs. */
//#include "amrsup.h"
#include "voice_amr_if.h"


/****************************************************************************
  VOICE DSM STRUCTURE DEFINATION
*****************************************************************************/


/****************************************************************************
 * VOICE DSM UTILITY ROUTINES                                               *
 ****************************************************************************/


/*===========================================================================

FUNCTION mvs_dsm_amr_init

DESCRIPTION
  Initialize AMR DSM buffers.

DEPENDENCIES
  To be called once only.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void voice_dsm_amr_q_init( 
  voice_amr_dsm_queue_t* amr_queue,
  uint32_t queue_len
);


/*===========================================================================

FUNCTION mvs_dsm_amr_ul_proc

DESCRIPTION
  Put AMR packets into uplink DSM queues.  Put packets into loopback buffers
if necessary.

DEPENDENCIES
  mvs_dsm_amr_activate has to be called first for getting into ACTIVE state.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void voice_dsm_queue_ul_amr_speech (
  amrsup_core_speech_type *ul_speech,
  voice_amr_chan_state_t* chan_state,
  voice_amr_dsm_queue_t* amr_dsm_q
);


/*===========================================================================

FUNCTION mvs_dsm_amr_dl_proc

DESCRIPTION
  Extract and decode AMR packets from downlink DSM queues in normal mode.
  Extract and decode AMR packets from UL2DL DSM queues in UL2DL DSM loopback.
  Decode AMR packets from UL2DL VOC buffer in UL2DL VOC loopback.
  Put AMR packets to be decoded into DL2UL buffer in DL2UL loopback.

DEPENDENCIES
  mvs_dsm_amr_activate has to be called first for getting into ACTIVE state.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
extern void voice_dsm_dequeue_dl_amr_apeech (
 amrsup_core_speech_type* dl_speech,
 voice_amr_dsm_queue_t* amr_dsm_q
);



/* <EJECT> */
/*===========================================================================

FUNCTION mvs_clear_ul_dsm_amr_queue

DESCRIPTION
  Clears Ulink DSM queue. It prevents increase in RTD, by clearing UL DSM 
  Queue. It should be called before queuing first vocoder packet after silence frames. 

DEPENDENCIES
  mvs_dsm_amr_init has to be called first.

RETURN VALUE
  void

SIDE EFFECTS
  It has to be carefully called, ONLY when silence frame is/are
  present in UL DSM Queue.   
===========================================================================*/

extern void voice_dsm_amr_q_empty (
  voice_amr_dsm_queue_t *amr_queues
);


#endif  /* __VOICE_DSM_IF_H__ */

