#ifndef __VS_TASK_H__
#define __VS_TASK_H__

/*
  ============================================================================

   Copyright (C) 2014-2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/avs/vsd/inc/protected/vs_task.h#1 $
  $DateTime: 2016/03/28 23:02:49 $
  $Author: mplcsds1 $
*/

#define TASK_PRIORITY( task_pri_order ) ( 255 - task_pri_order )

/* VS High Priority Task */
#define VS_HIGH_TASK_NAME ( "VS_HIGH" )
#define VS_HIGH_TASK_PRIORITY ( 181 )
#define VS_HIGH_TASK_STACK_SIZE ( 2 * 1024 )

/* VS Medium Priority Task */
#define VS_MED_TASK_NAME ( "VS_MED" )
#define VS_MED_TASK_PRIORITY ( 173 )
#define VS_MED_TASK_STACK_SIZE ( 2 * 1024 )

/* VS Low Priority Task */
#define VS_LOW_TASK_NAME ( "VS_LOW" )
#define VS_LOW_TASK_PRIORITY ( 136 )
#define VS_LOW_TASK_STACK_SIZE ( 4 * 1024 )

/* MVS Task */
#define MVS_TASK_NAME ( "MVS" )
#define MVS_TASK_PRIORITY ( 173 )
#define MVS_TASK_STACK_SIZE ( 4 * 1024 )

/* MVA Task */
#define MVA_TASK_NAME ( "MVA" )
#define MVA_TASK_PRIORITY ( 171 )
#define MVA_TASK_STACK_SIZE ( 2 * 1024 )

/* IVA Task */
#define IVA_TASK_NAME ( "IVA" )
#define IVA_TASK_PRIORITY ( 181 )
#define IVA_TASK_STACK_SIZE ( 2 * 1024 )

/* GVA Task */
#define GVA_TASK_NAME ( "GVA" )
#define GVA_TASK_PRIORITY ( 181 )
#define GVA_TASK_STACK_SIZE ( 2 * 1024 )

/* WVA Task */
#define WVA_TASK_NAME ( "WVA" )
#define WVA_TASK_PRIORITY ( 181 )
#define WVA_TASK_STACK_SIZE ( 2 * 1024 )

/* TVA Task */
#define TVA_TASK_NAME ( "TVA" )
#define TVA_TASK_PRIORITY ( 181 )
#define TVA_TASK_STACK_SIZE ( 2 * 1024 )

/* CVA Task */
#define CVA_TASK_NAME ( "CVA" )
#define CVA_TASK_PRIORITY ( 181 )
#define CVA_TASK_STACK_SIZE ( 2 * 1024 )

/* VAGENT Task */
#define VAGENT_TASK_NAME ( "VAGENT" )
#define VAGENT_TASK_PRIORITY ( 181 )
#define VAGENT_TASK_STACK_SIZE ( 2 * 1024 )

/* VOCSVC Task */
#define VOCSVC_TASK_NAME ( "VOCSVC" )
#define VOCSVC_TASK_PRIORITY ( 89 )
#define VOCSVC_TASK_STACK_SIZE ( 1024 )

#endif /* __VS_TASK_H__ */

