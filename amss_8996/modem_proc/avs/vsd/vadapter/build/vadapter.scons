#=================================================================
# IVA SConscript
#=================================================================
Import('env')

env = env.Clone()

VADAPTER_SRCPATH = "../src"
env.VariantDir('${BUILDPATH}', VADAPTER_SRCPATH, duplicate=0)

env.Append(CPPDEFINES = [
    "MSG_BT_SSID_DFLT=MSG_SSID_VS",
])

env.PublishPrivateApi('VADAPTER', [
    "${INC_ROOT}/avs/vsd/vadapter/inc/private",
])

env.RequirePublicApi([  
   'PUBLIC',
   'ONEX',   
], area='ONEX')

env.RequirePublicApi(['VADAPTER'])
env.RequireRestrictedApi(['VADAPTER'])

VADAPTER_SRCPATH = [   
  '${BUILDPATH}/iva_module.c',
  '${BUILDPATH}/gva_module.c',
  '${BUILDPATH}/wva_module.c',
  '${BUILDPATH}/tva_module.c',
  '${BUILDPATH}/cva_module.c',
]

IMAGES = ['MULTIMEDIA_MODEM', 'MULTIMEDIA_QDSP6_SW']

env.AddLibrary(IMAGES, ('${BUILDPATH}/vadapter/vadapter'), VADAPTER_SRCPATH)

#-------------------------------------------------------------------------------
# RCINIT_GROUP_6 is only for logging purpose. It does not dictate the actual start sequence.
# RCINIT_TASK_QURTTASK is also only for logging purpose.

if env['PRODUCT_LINE'].startswith("MPSS.BO"):
  print "Skipping RCINIT Registration of VADAPTER Tasks"
else:
  print "RCINIT Registration of VADAPTER Tasks"
  if 'USES_RCINIT' in env:
    env.AddRCInitTask(
        IMAGES,
        {
            'sequence_group'             : 'RCINIT_GROUP_6',
            'thread_name'                : 'CVA',
            'thread_entry'               : 'RCINIT_NULL',
            'thread_type'                : 'RCINIT_TASK_QURTTASK',
            'stack_size_bytes'           : '2048',
            'priority_amss_order'        : 'APR_VS_HIGH_PRI_ORDER',
        }
    )

    env.AddRCInitTask(
        IMAGES,
        {
            'sequence_group'             : 'RCINIT_GROUP_6',
            'thread_name'                : 'GVA',
            'thread_entry'               : 'RCINIT_NULL',
            'thread_type'                : 'RCINIT_TASK_QURTTASK',
            'stack_size_bytes'           : '2048',
            'priority_amss_order'        : 'APR_VS_HIGH_PRI_ORDER',
        }
    )

    env.AddRCInitTask(
        IMAGES,
        {
            'sequence_group'             : 'RCINIT_GROUP_6',
            'thread_name'                : 'WVA',
            'thread_entry'               : 'RCINIT_NULL',
            'thread_type'                : 'RCINIT_TASK_QURTTASK',
            'stack_size_bytes'           : '2048',
            'priority_amss_order'        : 'APR_VS_HIGH_PRI_ORDER',
        }
    )

    env.AddRCInitTask(
        IMAGES,
        {
            'sequence_group'             : 'RCINIT_GROUP_6',
            'thread_name'                : 'TVA',
            'thread_entry'               : 'RCINIT_NULL',
            'thread_type'                : 'RCINIT_TASK_QURTTASK',
            'stack_size_bytes'           : '2048',
            'priority_amss_order'        : 'APR_VS_HIGH_PRI_ORDER',
        }
    )

    env.AddRCInitTask(
        IMAGES,
        {
            'sequence_group'             : 'RCINIT_GROUP_4',
            'thread_name'                : 'IVA',
            'thread_entry'               : 'RCINIT_NULL',
            'thread_type'                : 'RCINIT_TASK_QURTTASK',
            'stack_size_bytes'           : '2048',
            'priority_amss_order'        : 'APR_VS_HIGH_PRI_ORDER',
        }
    )
#------------------------------------------------------------------------------- 

#-------------------------------------------------------------------------------
# RCINIT Init and Task Fields and Initialization
#-------------------------------------------------------------------------------
RCINIT_IVAINIT_FN = {
  'sequence_group'             : 'RCINIT_GROUP_4',       # required
  'init_name'                  : 'iva_task',             # required
  'init_function'              : 'iva_task',             # required
}

if 'USES_RCINIT' in env:
   env.AddRCInitFunc( IMAGES, RCINIT_IVAINIT_FN )