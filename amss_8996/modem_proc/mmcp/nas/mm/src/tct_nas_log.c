#include "tct_nas_log.h"
#include "mcfg_fs.h"
#include <stringl/stringl.h>
#include "time_svc.h"
#include "mm_v.h"


#define LOG_FILE_WRITE_TYPE_APPEND MCFG_FS_O_RDWR | MCFG_FS_O_CREAT | MCFG_FS_O_APPEND
#define LOG_FILE_WRITE_TYPE_START MCFG_FS_O_RDWR | MCFG_FS_O_CREAT

uint16 current_seq_sim[2] = {0, 0};
#define current_seq current_seq_sim[mm_as_id]

int tct_log_fd_sim[2] = {0, 0};
#define tct_log_fd tct_log_fd_sim[mm_as_id]

int tct_current_file_sim[2] = {TCT_FIRST_FILE, TCT_FIRST_FILE};
#define tct_current_file tct_current_file_sim[mm_as_id]

boolean first_write_log_sim[2] = {TRUE, TRUE};
#define first_write_log first_write_log_sim[mm_as_id]


void tct_log_open_efs_file(boolean storage_full)
{
  int fd = NULL;
  char *path = NULL;
  int flags;

  if (first_write_log)
  {
    /* delete before first write */
    mcfg_fs_delete(TCT_NAS_LOG_FILE, MCFG_FS_TYPE_EFS, (mcfg_fs_sub_id_e_type)mm_as_id);
    mcfg_fs_delete(TCT_NAS_LOG_FILE_OTHER, MCFG_FS_TYPE_EFS, (mcfg_fs_sub_id_e_type)mm_as_id);
  }

  if (!storage_full)
  {
    if (tct_current_file == TCT_FIRST_FILE)
    {
      path = TCT_NAS_LOG_FILE;
    }
    else
    {
      path = TCT_NAS_LOG_FILE_OTHER;
    }

    flags = LOG_FILE_WRITE_TYPE_APPEND;
  }
  else
  {
    mcfg_fclose(tct_log_fd, MCFG_FS_TYPE_EFS);
    tct_log_fd = -1;

    if (tct_current_file == TCT_FIRST_FILE)
    {
      tct_current_file = TCT_SECOND_FILE;
      path = TCT_NAS_LOG_FILE_OTHER;
    }
    else
    {
      tct_current_file = TCT_FIRST_FILE;
      path = TCT_NAS_LOG_FILE;
    }

    flags = LOG_FILE_WRITE_TYPE_START;
    //delete before write
    mcfg_fs_delete(path, MCFG_FS_TYPE_EFS, (mcfg_fs_sub_id_e_type)mm_as_id);
  }

  tct_log_fd = mcfg_fopen(path,
                          flags,
                          MCFG_FS_ALLPERMS,
                          MCFG_FS_TYPE_EFS,
                          (mcfg_fs_sub_id_e_type)mm_as_id);
}

void tct_log_write_efs_file(void* data_ptr, uint32 size, boolean storage_full)
{
  int fd = -1;
  uint32 sub_id = 0;
  mcfg_fs_status_e_type status;

  //open file
  tct_log_open_efs_file(storage_full);

  if (first_write_log)
  {
    first_write_log =  FALSE;
  }

  status = mcfg_fwrite(tct_log_fd,
                       data_ptr,
                       size,
                       MCFG_FS_TYPE_EFS);

  // if(status != MCFG_FS_STATUS_OK)
  // {
  //   MSG_HIGH_DS_3(MM_SUB, "mcfg_fs_write error, sub_id %d, status=%d, errno %d", mm_as_id, status,
  //                  mcfg_fs_errno(MCFG_FS_TYPE_EFS));
  // }

  mcfg_fclose(tct_log_fd, MCFG_FS_TYPE_EFS);
}


void tct_log_nas_message_id_and_reject_cause(uint32 message_id_in, uint32 direction_in, uint32 reject_cause_in, uint8 protocol_in)
{

  tct_log_nas_message_id_reject_cause_esm_info_and_apn(message_id_in,
                                                       direction_in, reject_cause_in, FALSE, NULL, 0, protocol_in);
}

/*

  encodes as:

  +ROOT_TAG_NAS_MESSAGE
  +total len

    -SUB_TAG_BASIC_INFO   \
    -sub_record_len       |
    -current_seq          |
    -message_id           |
    -time                 |---> basic info record, mandatory
    -mm_as_id+1           |
    -direction            |
    -procotol             /

    -SUB_TAG_REJECT_CAUSE \
    -sub_record_len       |---> reject cause record, optional
    -reject_cause         /

    -SUB_TAG_ESM_INFO     \
    -sub_record_len       |---> esm info record, optional
    -esm_info             /

    -SUB_TAG_APN          \
    -sub_record_len       |---> esm info record, optional
    -apn                  /
 */
void tct_log_nas_message_id_reject_cause_esm_info_and_apn(uint32 message_id_in, uint32 direction_in, uint32 reject_cause_in, boolean esm_info_in, char* apn_in, uint32 apn_size_in, uint8 protocol_in)
{

  boolean storage_full = FALSE;
  byte data[TCT_MAX_LOG_RECORD_SIZE];
  uint32 index = 0;
  uint32 basic_infos_start_index = 0;
  uint32 time = time_get_uptime_secs();

  uint16 message_id = (uint16)message_id_in;
  uint8 direction = (uint8)direction_in;
  uint8 reject_cause = reject_cause_in;
  uint8 procotol = protocol_in;
  uint8 copy_size = apn_size_in > TCT_MAX_APN_LENGTH ? TCT_MAX_APN_LENGTH: apn_size_in;

  memset((void *)data, 0, TCT_MAX_LOG_RECORD_SIZE);
  current_seq++;

  /* must not exceed max num */
  if (current_seq > TCT_MAX_LOG_NUM)
  {
    storage_full = TRUE;
    current_seq = 1;
  }

  //write tag, current just support ROOT_TAG_NAS_MESSAGE
  data[0] = (uint8)ROOT_TAG_NAS_MESSAGE;
  //current size is 0
  data[1] = 0;
  //add len for tag, len
  index += 2;


  //add sub tlv, basic info is mandatory for nas message
  data[2] = (uint8)SUB_TAG_BASIC_INFO;
  index += 2;

  basic_infos_start_index = index;

  memscpy((void *)&data[index], TCT_MAX_LOG_RECORD_SIZE - index, (void *)&current_seq, 2);
  index += 2;

  memscpy((void *)&data[index], TCT_MAX_LOG_RECORD_SIZE - index, (void *)&message_id, 2);
  index += 2;

  memscpy((void *)&data[index], TCT_MAX_LOG_RECORD_SIZE - index, (void *)&time, 4);
  index += 4;

  data[index] = mm_as_id + 1;
  index++;

  data[index] = direction;
  index++;

  data[index] = procotol;
  index++;
  //set len for basic info tlv
  data[3] = index - basic_infos_start_index;

  //add reject cause sub tlv
  if (reject_cause_in > 0)
  {

    data[index] = (uint8)SUB_TAG_REJECT_CAUSE;
    index++;

    data[index] = 1;
    index++;

    data[index] = reject_cause;
    index++;
  }

  //add esm_info sub tlv
  if (esm_info_in == 1)
  {
    data[index] = (uint8)SUB_TAG_ESM_INFO;
    index++;

    data[index] = 1;
    index++;

    data[index] = 1;
    index++;
  }

  //add apn sub tlv
  if (copy_size > 1)
  {

    data[index] = (uint8)SUB_TAG_APN;
    index++;

    data[index] = copy_size - 1;
    index++;

    memscpy((void *)&data[index], TCT_MAX_LOG_RECORD_SIZE - index, (void *)&apn_in[1], copy_size - 1);
    index += copy_size;
  }

  //set total size
  data[1] = index - 2;

  MSG_HIGH_DS_6(MM_SUB, "TCT Log, sub = %d, seq = %d, procotol %d, message id %d, reject_cause=%d, esm_info %d",
               mm_as_id + 1, current_seq, protocol_in, message_id_in, reject_cause_in, esm_info_in);

  tct_log_write_efs_file((void *)data, index, storage_full);
}

