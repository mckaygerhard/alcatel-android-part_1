#ifndef TCT_NAS_LOG_H
#define TCT_NAS_LOG_H

#include "comdef.h"

#define TCT_MAX_APN_LENGTH 19

#define TCT_MAX_LOG_NUM 200
#define TCT_MAX_LOG_RECORD_SIZE 50

//top tag
#define ROOT_TAG_NAS_MESSAGE 0x81
#define ROOT_TAG_EVENT 0x82

//sub tag
#define SUB_TAG_BASIC_INFO 1
#define SUB_TAG_REJECT_CAUSE 2
#define SUB_TAG_ESM_INFO 3
#define SUB_TAG_APN 4

//direction
#define UE_TO_NETWORK 1
#define NETWORK_TO_UE 2

//file path
#define TCT_NAS_LOG_FILE "/nv/item_files/modem/nas/nas_log"
#define TCT_NAS_LOG_FILE_OTHER "/nv/item_files/modem/nas/nas_log_second"

//first or second file
#define TCT_FIRST_FILE 0
#define TCT_SECOND_FILE 1



//protocol modules, keep same as com_iei.h
// #define PD_CC                   0x03
// #define PD_MM                   0x05
// #define PD_RR                   0x06
// #define PD_SMS                  0x09
// #define PD_SS                   0x0b
// #define PD_TEST                 0x0f
// #define PD_GMM                  0x08
// #define PD_GPRS_SM              0x0a
// #define PD_GTTP                 0x04
#define TCT_PROTOCOL_EMM  0x55
#define TCT_PROTOCOL_ESM  0x56
#define TCT_PROTOCOL_MM   0x05
#define TCT_PROTOCOL_GMM  0x08
#define TCT_PROTOCOL_SM   0x0a
#define TCT_PROTOCOL_CC   0x03
#define TCT_PROTOCOL_SMS  0x09
#define TCT_PROTOCOL_SS   0x0b
#define TCT_PROTOCOL_RR   0x06



#define TCT_EMM_SERVICE_REQUEST 255

typedef struct tct_log_record_basic_info
{
  uint16 seq;
  uint16 message_id;
  uint32 time;
  uint8 sub;
  uint8 direction;
  uint8 protocol;
} tct_log_record_basic_info_type;

typedef struct tct_log_record
{
  tct_log_record_basic_info_type basic_infos;
  uint8 esm_info;
  uint8 reject_cause;
  uint8 apn_len;
  char apn[TCT_MAX_APN_LENGTH+1];
} tct_log_record_type;

void tct_log_nas_message_id_and_reject_cause(uint32 message_id_in, uint32 direction_in, uint32 reject_cause_in, uint8 protocol_in);
void tct_log_nas_message_id_reject_cause_esm_info_and_apn(uint32 message_id_in, uint32 direction_in, uint32 reject_cause_in, boolean esm_info_in, char* apn_in, uint32 apn_size_in, uint8 protocol_in);

#endif