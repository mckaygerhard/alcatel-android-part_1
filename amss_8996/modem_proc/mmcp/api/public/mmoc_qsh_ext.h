#ifndef MMOC_QSH_EXT_H
#define MMOC_QSH_EXT_H
/*===========================================================================

                    M U L T I M O D E   C O N T R O L L E R ( MMoC )   D E B U G   H E A D E R   F I L E

DESCRIPTION
  This header file contains debug macros and definitions necessary to
  interface with mmocdbg.C


Copyright (c) 1991 - 2015 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

===========================================================================*/



/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mmcp/api/public/mmoc_qsh_ext.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/09/15   KC      Initial release.

===========================================================================*/

#include "comdef.h"    /* Definition for basic types and macros */
#include "qsh.h"
#include "sys.h"

#define MMOC_QSH_MAJOR_VER 1

#define MMOC_QSH_MINOR_VER 1


/* Enumeration for the type of Dump */
typedef enum {
  MMOC_QSH_DUMP_TAG_MINI, /* Denotes Mini dump */
  MMOC_QSH_DUMP_TAG_MAX
}mmoc_qsh_dump_tag_e;


/* this is evolved from MMOC_MAX_DEBUG_BUFFER_SIZE  */
#define MMOC_DBG_MAX_DEBUG_BUFFER_SIZE         10

/* this is evolved from MMOC_MAX_RPT_COUNT  */
#define MMOC_DBG_MAX_RPT_COUNT                 10

/* this is evolved from MMOC_MAX_TASK_NAME_LEN  */
#define MMOC_DBG_MAX_TASK_NAME_LEN             10

/* this is evolved from MMOC_DBG_BUF_TRANS_ADDL_INFO_SIZE  */
#define MMOC_DBG_BUF_DEBUG_TRANS_ADDL_INFO_SIZE  6

/* this is evolved from SD_SS_MAX  */
#define MMOC_DBG_SS_MAX 4

/* this is evolved from MAX_AS_IDS  */
#define MMOC_DBG_MAX_AS_IDS 2

/* this is evolved from SYS_MAX_ACQ_PREF_RAT_LIST_NUM */
#define MMOC_DBG_MAX_ACQ_PREF_RAT_LIST_NUM 10
    /**< Maximum number of acquisition lists. */

/* Enumeration for the type of SUBS */
typedef enum {
  MMOC_DBG_SUBS_NONE, /* SUBS none */
  MMOC_DBG_SUBS_CDMA, /* CDMA SUBS data  */
  MMOC_DBG_SUBS_GW, /* GW SUBS data   */
  MMOC_DBG_SUBS_GW_HYBR, /* GW HYBR SUBS data   */
  MMOC_DBG_SUBS_GW_HYBR_3, /* GW HYBR 3 SUBS data   */
  MMOC_DBG_SUBS_MAX
}mmoc_subs_data_type_e;


/* this structure is evolved from mmoc_cmd_deact_from_dormant_s_type  */
typedef PACK(struct)
{
  uint8 ss;
  /* Indicates the SS for which protocol has to be deactivated if dormant */
  /* sd_ss_e_type */

  uint16 prot;
    /* Indicates the modes (protocols) which has to be deactivated */
    /* sd_ss_mode_pref_e_type */

  boolean is_activate_main;
}mmoc_dbg_cmd_deact_from_dormant_s_type;


/* this structure is evolved from sys_addtl_action_s_type  */
typedef PACK(struct)
{
  /* Addtional action to MMOC */
  uint8 action;


  int8 ue_mode;
  /* additional payload */
   /* sys_ue_mode_e_type */

  boolean is_ue_mode_substate_srlte;
  /* to remember if we are operation in SRLTE mode or not */
}mmoc_dbg_addtl_action_s_type;


/* this structure is evolved from sd_rat_acq_order_s_type */
typedef PACK(struct)
{
  uint8 acq_sys_mode[MMOC_DBG_MAX_ACQ_PREF_RAT_LIST_NUM];
  /* Version field can be used to distinguish between different TOT
  ** tables. It is not used in code for any purpose
  */
   /* sys_sys_mode_e_type */

  uint16 version;
  /* The number of RATs that are part of the technology order table
  ** This should be equal to the number of cellular technologies
  ** supported by the phone
  */


  uint16 num_rat;
  /* This array will list the cellular technologies in order of priority
  */
}mmoc_dbg_rat_acq_order_s_type;


/* this structure is evolved from sd_ho_sys_param_s_type */
typedef PACK(struct)
{

  uint16            band;
  /* sd_band_e_type */
  /**< Serving system's band class.
      */

  uint16              chan;
  /* sys_channel_num_type */
  /**< Serving system's CDMA channel.
      */

  word                      sid;
      /**< Serving system's SID.
      */

  word                      nid;
      /**< Serving system's NID.
      */

  word                      mcc;
      /**< Serving system's MCC.
      */
  byte                      imsi_11_12;
      /**< Serving system's IMISI_11_12.
      */

  uint8         srch_win_n;
  /* sys_srch_win_type */
  /**< Neighbor set window search size.
      */

  uint32         base_lat;
  /* sys_base_lat_type */
      /**< Base station latitude.
      */

  uint32        base_long;
  /* sys_base_long_type */
      /**< Base station latitude.
      */

  uint16          base_id;
  /* sys_base_id_type */
      /**< Base station Id.
      */

  byte                      ltm_off;
      /* LTM Offset
      */

  uint8  daylt_savings;
  /* sys_daylt_savings_e_type */
  /* Daylight savings ind
      */

  byte                      leap_secs;
      /* Leap seconds
      */

}mmoc_dbg_ho_sys_param_s_type;


/* this structure is evolved from mmoc_cmd_prot_ho_ind_s_type */
typedef PACK(struct)
{
  mmoc_dbg_ho_sys_param_s_type sys_param;
   /* sd_ho_sys_param_s_type */
  uint8 actd_reason;
   /* prot_act_e_type */

  uint8 prot_state;
   /* prot_state_e_type */
}mmoc_dbg_cmd_prot_ho_ind_s_type;


/* this structure is evolved from mmoc_ho_ind_trans_info_s_type */
typedef PACK(struct)
{
  mmoc_dbg_cmd_prot_ho_ind_s_type cmd_info;
   /* mmoc_cmd_prot_ho_ind_s_type */
  uint8 ss;
   /* sd_ss_e_type */

  uint8 prev_prot_state;
   /* prot_state_e_type */
}mmoc_dbg_ho_ind_trans_info_s_type;



/* this structure is evolved from prot_cmd_pref_sys_chgd_s_type */
typedef PACK(struct)
{

  /* Reason for changing the SS-Preference.  */
  /* sd_ss_pref_reas_e_type */
  uint8               pref_reas;

  /* New origination mode (normal, OTASP, emergency, other, etc.).
  */
    /* sd_ss_orig_mode_e_type */
  uint8               orig_mode;

  /* New mode preference (AMPS, CDMA, any, etc.) - ignored when
  ** orig_mode = OTASP or emergency.
  */
    /* sd_ss_mode_pref_e_type */
  uint16               mode_pref;

  /* New band preference (BC0, BC1, any, etc.) - ignored when
  ** orig_mode = OTASP  or emergency.
  */
    /* sd_ss_band_pref_e_type */
  uint64               band_pref;

  /* New LTE band preference - ignored when
  ** orig_mode = OTASP  or emergency.
  */
  /* sd_ss_band_pref_e_type */
  sys_lte_band_mask_e_type               lte_band_pref;

  /*
  ** TDS-CDMA band preference
  */
  /* sd_ss_band_pref_e_type */
  uint64               tds_band_pref;

  /* New prl preference.
  */
  /* sd_ss_prl_pref_e_type */
  uint16                prl_pref;

  /* New roam preference (any, home, affiliated, etc.) - ignored
  ** when orig_mode = OTASP or emergency.
  */
  /* sd_ss_roam_pref_e_type */
  uint16               roam_pref;

  /* New hybrid preference (none, CDMA/HDR).
  */
  /* sd_ss_hybr_pref_e_type */
  uint8               hybr_pref;

  /* The band-class that is associated with OTASP origination mode.
  ** Note that this parameter is ignored if orig_mode != OTASP.
  */
  /* sd_band_e_type */
  uint16                       otasp_band;

  /* The PCS frequency block/Cellular system that is associated
  ** with OTASP origination mode. Note that this parameter is
  ** ignored if orig_mode != OTASP.
  */
  /* sd_blksys_e_type */
  uint8                     otasp_blksys;

  /* Type of the system to be avoided, valid only if
  ** pref_reason = SD_SS_PREF_REAS_AVOID_SYS.
  */
  /* sd_ss_avoid_sys_e_type */
  uint8               avoid_type;

  /* Time in seconds for which the system is to be avoided,
  ** valid only if pref_reason = SD_SS_PREF_REAS_AVOID_SYS.
  */
  dword                                avoid_time;

  /* New service domain preference (CS, PS, CS_PS etc).
  ** Applicable for GSM/WCDMA modes.
  */
  /* sd_ss_srv_domain_pref_e_type */
  uint8         domain_pref;

  /* Preference for the order of acquisition between modes.
  ** ( WCDMA before GSM, GSM before WCDMA etc ).
  */
  /* sd_ss_acq_order_pref_e_type */
  uint8          acq_order_pref;

  /* Indicate ss pref update reason
  */
  /* sd_ss_pref_update_reason_e_type */
  uint8      pref_update_reas;

  /*  To indicate sub action to be performed by MMOC during
  **  pref sys chgd cmd
  */
  /* sys_addtl_action_s_type */
  mmoc_dbg_addtl_action_s_type               addl_action;

  /* The user/phone mode preference
  */
  /* sd_ss_mode_pref_e_type */
  uint16                user_mode_pref;

  /* Rat acquisition preference order including LTE
  */
  /* sd_rat_acq_order_s_type */
  mmoc_dbg_rat_acq_order_s_type               rat_acq_order;

  /* Request Id
  */
  uint16                                sys_sel_pref_req_id;

  /* Indicate ss pref camp mode
  */
  /* sd_ss_pref_camp_mode_e_type */
  uint8           camp_mode_pref;

  /* CSG identifier
  */
  /* sys_csg_id_type */
  uint32                       csg_id;

  /* RAT specified for CSG
  */

  /* sys_radio_access_tech_e_type */
  int8          csg_rat;

  int8          voice_domain_pref;
  /* sys_voice_domain_pref_e_type */
  /* voice domain pref
  */

}mmoc_dbg_prot_cmd_pref_sys_chgd_s_type;


/* this structure is evolved from prot_cmd_gw_get_net_s_type */
typedef PACK(struct)
{
  uint64 band_pref;
   /* sd_ss_band_pref_e_type */

  sys_lte_band_mask_e_type lte_band_pref;
   /* sd_ss_band_pref_e_type */

  uint64 tds_band_pref;
   /* sd_ss_band_pref_e_type */

  uint16 mode_pref;
   /* sd_ss_mode_pref_e_type */

  uint8 network_list_type;
   /* sd_network_list_type_e_type */
}mmoc_dbg_prot_cmd_gw_get_net_s_type;



/* this structure is evolved from prot_cmd_msm_sys_chgd_s_type */
typedef PACK(struct)
{
  uint16 band;
   /* sys_band_class_e_type */

  uint16 chan;
   /* sys_channel_num_type */

  uint16 sid;


  uint16 nid;
}mmoc_dbg_prot_cmd_msm_sys_chgd_s_type;


/* this structure is evolved from mmoc_subs_data_s_type */
typedef PACK(struct)
{

  uint8          as_id;
    /* sys_modem_as_id_e_type */
    /* subscription-id of this subscription */

  uint16                          nv_context;
    /* NV Context used for this subscription */

  uint32   session_type;
    /* mmgsdi_session_type_enum_type */
    /* Session type used for this subscription */

  boolean                         is_subs_avail;
    /* Avail/Not Avail */

  boolean                         is_perso_locked;
    /* Perso locked or not */

  boolean                         is_subsc_chg;
    /* Has Subscription changed */

  uint8                    ss;
    /* sd_ss_e_type */
    /* SS on which this subscription is to be provisioned */

  uint8          orig_mode;
    /* sd_ss_orig_mode_e_type */


  uint16          mode_pref;
  /* sd_ss_mode_pref_e_type */

  uint64          band_pref;
  /* sd_ss_band_pref_e_type */
  /* 2G and 3G band preference, excluding TDSCDMA */

  uint16          roam_pref;
  /* sd_ss_roam_pref_e_type */

  uint8          hybr_pref;
  /* sd_ss_hybr_pref_e_type */


  /* LTE band preference */
  /* sd_ss_band_pref_e_type */
  sys_lte_band_mask_e_type          lte_band_pref;


  /* TDSCDMA band preference */
  /* sd_ss_band_pref_e_type */
  uint64          tds_band_pref;
  /* Preferences for the subscription */

  uint32          subs_capability;
  /* subs_capability_e_type */


  int8 curr_ue_mode;

   boolean                        is_ue_mode_substate_srlte;
}mmoc_dbg_subs_data_s_type;



/* this structure is evolved from mmoc_cmd_subsc_chgd_s_type */
typedef PACK(struct)
{
  mmoc_dbg_subs_data_s_type cdma;
   /* mmoc_subs_data_s_type */
  mmoc_dbg_subs_data_s_type gw;
   /* mmoc_subs_data_s_type */
  mmoc_dbg_subs_data_s_type gw_hybr;
   /* mmoc_subs_data_s_type */
  mmoc_dbg_subs_data_s_type gw_hybr_3;
   /* mmoc_subs_data_s_type */
  uint8 chg_type;
   /* mmoc_subsc_chg_e_type */

  uint8 prot_subsc_chg;
   /* prot_subsc_chg_e_type */

  uint8 nam;
   /* byte */

  boolean hybr_gw_subs_chg;


  boolean hybr_3_gw_subs_chg;


  uint8 ds_pref;
   /* sys_modem_dual_standby_pref_e_type */

  uint8 active_ss;


  uint8 device_mode;
   /* sys_modem_device_mode_e_type */
}mmoc_dbg_cmd_subsc_chgd_s_type;

typedef union
{

  mmoc_dbg_prot_cmd_pref_sys_chgd_s_type        pref_sys_chgd;

  mmoc_dbg_prot_cmd_gw_get_net_s_type           gw_get_net;

  mmoc_dbg_prot_cmd_msm_sys_chgd_s_type         msm_sys_chgd;

  uint8          gw_sim_state;
  /* prot_cmd_gw_sim_state_e_type */
}mmoc_dbg_prot_gen_cmd_param_s_type;


/* this structure is evolved from prot_gen_cmd_s_type */
typedef PACK(struct)
{
  uint8 cmd_type;
   /* prot_gen_cmd_e_type */

  uint8 trans_id;
   /* prot_trans_type */

  uint8 ss;
   /* sd_ss_e_type */

  uint8 prot_state;
   /* prot_state_e_type */

  mmoc_dbg_prot_gen_cmd_param_s_type param;


}mmoc_dbg_prot_gen_cmd_s_type;


/* this structure is evolved from mmoc_gen_cmd_trans_info_s_type */
typedef PACK(struct)
{
  mmoc_dbg_prot_gen_cmd_s_type cmd_info;
   /* prot_gen_cmd_s_type */
  boolean is_activate_main;
}mmoc_dbg_gen_cmd_trans_info_s_type;


/* A type for parameters that are associated with protocol redirection
** command, coming from protocol
*/
typedef PACK(struct) {

  /* Reason for the protocol redirection.
  */
  /* prot_act_e_type */
  uint8                      actd_reason;

  /* Protocol which sent the autonomous activation for redirection.
  */
  /* prot_state_e_type */
  uint8                    prot_state;

} mmoc_dbg_cmd_prot_redir_ind_s_type;


/* A type for parameters that holds the information regarding the transaction
** MMOC_TRANS_PROT_REDIR_IND being processed.
*/
typedef PACK(struct) {

  /* System Selection instance on which Redirected protocol is activated
  */
    /* sd_ss_e_type */
  uint8                        ss;

  /* Protocol state of System Selection instance prior to redirection
  */
    /* prot_state_e_type */
  uint8                   prev_prot_state;

  /* Protocol redirection command received
  */
  mmoc_dbg_cmd_prot_redir_ind_s_type      cmd_info;

} mmoc_dbg_redir_ind_trans_info_s_type;


/* A type for parameters that are associated with suspend ss
** command to MMoC task.
*/
typedef PACK(struct) {

  /* Indicates the instance to be woken up from pwr_save.
  */
    /* sd_ss_e_type */
  uint8                         ss;

  /* Indicates whether to suspend or resume
  */
  boolean                              is_suspend;

  /* Indicates whether to activate protocol or not
  */
  boolean                              ignore_protocol_activate;

  /* Indicates suspension reason
  */
    /* prot_deact_e_type */
  uint8                    susp_reason;

} mmoc_dbg_cmd_suspend_ss_s_type;

/* A type for parameters that are associated with information on deactivate
** protocol command.
*/
typedef PACK(struct) {

  /* Protocol deactivate reason (Powerdown, reset etc).
  */
    /* prot_deact_e_type */
  uint8                    reason;

  /* Transaction Id that should be used for acknowledging this command.
  */
    /* prot_trans_type */
  uint8                      trans_id;

} mmoc_dbg_prot_deact_s_type;



/* this structure is evolved from mmoc_trans_info_s_type */
typedef PACK(struct)
{
  mmoc_dbg_cmd_subsc_chgd_s_type subsc_chgd;
   /* mmoc_cmd_subsc_chgd_s_type */
  mmoc_dbg_gen_cmd_trans_info_s_type gen_prot_cmd;
   /* mmoc_gen_cmd_trans_info_s_type */
  mmoc_dbg_ho_ind_trans_info_s_type ho_ind_info;
   /* mmoc_ho_ind_trans_info_s_type */
  mmoc_dbg_cmd_deact_from_dormant_s_type deact_from_dormant;
   /* mmoc_cmd_deact_from_dormant_s_type */

  mmoc_dbg_redir_ind_trans_info_s_type     redir_ind_info;
   /* mmoc_redir_ind_trans_info_s_type */

  mmoc_dbg_cmd_suspend_ss_s_type           suspend_ss;
   /* mmoc_cmd_suspend_ss_s_type */

  mmoc_dbg_prot_deact_s_type                    gen_deact_rpt;
   /* prot_deact_s_type */
}mmoc_dbg_trans_info_s_type;




/* this structure is evolved from mmoc_state_info_s_type */
typedef PACK(struct)
{

  /* Current transaction that the MMoC is processing. Set to MMOC_TRANS_NULL
  ** if no transactions are being processed.
  */
  uint8                    curr_trans;
  /* mmoc_trans_e_type */

  /* Current state of the transaction that the MMoC is processing.
  */
  uint8              trans_state;
  /* mmoc_trans_state_e_type */

  /* Information related to the transaction being processed. Valid only
  ** when curr_trans != MMOC_TRANS_NULL.
  */
  mmoc_dbg_trans_info_s_type               trans_info;
  /* mmoc_trans_info_s_type */

  /* Current protocol state corresponding to SS-Main and SS-HDR instances.
  */
  uint8                    prot_state[MMOC_DBG_SS_MAX];
  /* prot_state_e_type */

  /* Flag to indicate if SD was initialized with subscription
  ** information.
  */
  boolean                              is_sd_initialized;

  /* Flag to indicate if sd_init is called after starting of subs transaction
  */
  boolean                              is_sd_init_called;

  /* Last trans Id for the command issued by MMoC. Incremented after
  ** sending every acknowledgeable command.
  ** Cannot be set to MMOC_TRANS_AUTONOMOUS or MMOC_TRANS_PENDING.
  */
  uint8                      trans_id;
  /* prot_trans_type */

  /* Current NAM selection.
  */
  byte                                 curr_nam;

  /* Current GSM/WCDMA/LTE subscription availability status.
  */
  boolean                              is_gwl_subs_avail;

  /* Current GSM/WCDMA 2 subscription availability status.
  */
  boolean                              is_gw_hybr_subs_avail;

  /* Current GSM/WCDMA 3 subscription availability status.
  */
  boolean                              is_gw_hybr_3_subs_avail;

  /* Current CDMA/AMPS/HDR subscription availability status.
  */
  boolean                              is_cdma_subs_avail;

  /* Current CDMA/AMPS/HDR subscription availability status.
  */

  /* Current operating mode status of the phone.
  */
  uint8                curr_oprt_mode;
  /* prot_oprt_mode_e_type */

  /* Flag to indicate if RESET_MODEM cmd is being processed.
  */
  int8                 true_oprt_mode;
  /* sys_oprt_mode_e_type */

  /* Indicates if phone status chgd command to all supported protocols
  ** have already been sent.
  */
  boolean                              is_ph_stat_sent;

  /* Context for current protocol to which the deactivate request was
  ** sent.
  */
  uint8                         deact_req_index;
  /* sd_ss_e_type */


  /* Insanity count to sanity check if the MMoC is stuck in a transaction.
  */
  byte                                 insanity_count;


  /* Flag to indicate if phone is in standby sleep.
  */
  boolean                              is_standby_sleep;

  /* Enumeration of protocols that have a change in
  ** subscription available status
  */
  uint8                prot_subsc_chg;
  /* prot_subsc_chg_e_type */

  /* Flag to indicate if redirection is allowed
  */
  boolean                              is_redir_allowed;

  /* Current dormant protocols for all SS
  */
  uint16               prot_dormant[MMOC_DBG_SS_MAX];
  /* sd_ss_mode_pref_e_type */

  /* Flag indicating if the Hybr GW Subscription has changed
  */
  boolean                              hybr_gw_subs_chg;

  /* Flag indicating if the Hybr GW 3 Subscription has changed
  */
  boolean                              hybr_3_gw_subs_chg;

  /* Previous Standby Preference - Dual standby/Single standby
  */
  uint8   prev_standby_pref;
  /* sys_modem_dual_standby_pref_e_type */

  /* Current Standby Preference - Dual standby/Single standby
  */
  uint8   standby_pref;
  /* sys_modem_dual_standby_pref_e_type */

  /* Previous active ss, this needs to be in sync with standby pref
  */
  uint8                                prev_active_ss;

  /* Current active ss, this needs to be in sync with standby pref
  */
  uint8                                active_ss;

  /* Current device mode */
  int8         device_mode;
  /* sys_modem_device_mode_e_type */

  int8         prev_device_mode;
  /* sys_modem_device_mode_e_type */

  /* Sanity timer value depending on the standby preference
  */
  uint8                                max_sanity_time_multiple;

  /* Scan permission allow MDM/MSM to scan. This is used in
  ** SVLTE2
  */
  boolean                              is_scan_permission;

  /* Denotes the stack that needs to be activated on getting deact CNF from
  ** HDR protocol. It can be either MAIN or HYBR2
  */
  uint8                         hdr_deact_activate_ss;
  /* sd_ss_e_type */

  /* Indicate if GWL deactivate command has been sent.
  */
  boolean                              is_gwl_deact_sent[MMOC_DBG_SS_MAX];

  /* stack suspend status
  */
  boolean                              is_suspend[MMOC_DBG_SS_MAX];

  /* Indicates whether AUTO DEACT IND is buffered or not */
  boolean                              is_buffer_auto_deact_ind;

//done arun khanna need to write dword or not
  dword                 last_stop_req_sent_timestamp;


  uint8                onebuild_feature;
  /* sys_overall_feature_t */
    /* Identifies feature that is enabled in this instance of common build */

  uint8                   subs_feature[2];
  /* sys_subs_feature_t */


  /*subs capability info*/
  int8               subs_capability[MMOC_DBG_MAX_AS_IDS];
  /* subs_capability_e_type */

/* bitmask of ss which gets deactivated with dual-switch
  */
  uint8                         ss_with_ps;
  /* sd_ss_e_type */

  /* bitmask of ss which gets deactivated with dual-switch
  */
  uint8                                dual_switch_ss;

 /*Last protocol state  */
 /* prot_state_e_type */
  uint8                    main_last_prot_state;


  /* Bitmask of stacks suspended due to emergency call */
  uint8                                emerg_susp_ss_mask;

  uint32 pri_slot;
  /* mmgsdi_session_type_enum_type */
  /* Mapping of session to corresponding slot, used for cross mapping, reading MLPL/MSPL */

  /* Indicate if activating main is pending */
  boolean                              is_activate_main_pending;

  /* Indicate if the actual transaction is complete and now mmoc is processing
  ** the pending action on main.
  */
  boolean                              is_post_process;
}mmoc_dbg_state_info_s_type;



/* this structure is evolved from mmoc_dbg_buf_rpt_s_type */
typedef PACK(struct)
{
  uint8 prot_state[MMOC_DBG_SS_MAX];
  /* Protocol states for each of the stack when report is received
  */
   /* prot_state_e_type */


  uint8 rpt_name;
   /* Name of the report received
   */

  uint8 task_name;
   /* Name of the task from where report is queued */
}mmoc_buf_rpt_dbg_s_type;

/* this structure is evolved from mmoc_dbg_buf_trans_s_type */
typedef PACK(struct)
{
  /* Transaction Id associated with the transaction.
  */
   /* prot_trans_type */
  uint8 trans_id;

  /* Set of reports received for the current transaction
  */
  mmoc_buf_rpt_dbg_s_type rpt_queue[MMOC_DBG_MAX_RPT_COUNT];
   /* mmoc_dbg_buf_rpt_s_type */


   /* Additional information stored for certain transaction types
   */
  uint8 addl_info[MMOC_DBG_BUF_DEBUG_TRANS_ADDL_INFO_SIZE];


  /* Transaction name associated with the transaction
  */
  uint8 trans_name;
}mmoc_buf_trans_dbg_s_type;

/* this structure is evolved from mmoc_debug_buffer_s_type */
typedef PACK(struct)
{
  mmoc_buf_trans_dbg_s_type dbg_buf[MMOC_DBG_MAX_DEBUG_BUFFER_SIZE];
   /* mmoc_dbg_buf_trans_s_type */
  uint8 dbg_buf_idx;
}mmoc_debug_buffer_dbg_s_type;


typedef PACK(struct) mmoc_qsh_dump_tag_mini_s{
 qsh_dump_tag_hdr_s hdr; /* this header will have a tag and length */


 /* A type for messages [commands/reports] store in cmregprx debug buffer
 */
 mmoc_debug_buffer_dbg_s_type dbg_buffer;


 /* A type for parameters that holds the state and other information of the
 ** RegProxy.
   */
 mmoc_dbg_state_info_s_type state_info;
}mmoc_qsh_dump_tag_mini_s_type;


/*===========================================================================

FUNCTION mmocdbg_qsh_init

DESCRIPTION
  Initilize CM QSH interface.

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/

void  mmocdbg_qsh_init();

#endif /* MMOC_QSH_EXT_H */

