#ifndef _DS_QMI_NAS_MMGSDI_H_
#define _DS_QMI_NAS_MMGSDI_H_

/*===========================================================================

                         DS_QMI_NAS_MMGSDI.H

DESCRIPTION

 The Qualcomm Network Access Services MMGSDI Interface header file.

Copyright (c) 2010 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mmcp/mmode/qmi/src/qmi_nas_mmgsdi.h#1 $ $DateTime: 2016/03/28 23:03:19 $ $Author: mplcsds1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
06/03/10    hs     Initial version
===========================================================================*/

#endif // !_DS_QMI_NAS_MMGSDI_H_

