/**
  @file policy_generic.c

  @brief  Generic policy.  This policy is entirely rules-based, with the
          addition of OOS handling.
*/

/*
    Copyright (c) 2013-2015 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mmcp/policyman/src/policyman_generic.c#1 $
  $DateTime: 2016/03/28 23:03:19 $
  $Author: mplcsds1 $
*/

#include "policyman_generic.h"
#include "policyman_device_config.h"
#include "policyman_dbg.h"
#include "policyman_plmn.h"
#include "policyman_rat_capability.h"
#include "policyman_rat_order.h"
#include "policyman_serving_system.h"
#include "policyman_set.h"
#include "policyman_state.h"
#include "policyman_timer.h"
#include "policyman_util.h"

#include "sys.h"
#include <stringl/stringl.h>



typedef struct
{
  POLICYMAN_POLICY_BASE;

} policyman_policy_generic_t;


#define POLICY_TO_GENERIC_POLICY(p) ((policyman_policy_generic_t *) (p))

#define GENERIC_OOS_TIMER    1



/*=============================================================================
  Generic policy APIs
=============================================================================*/


/*-------- policy_generic_dtor --------*/
static void
policy_generic_dtor(
  void  *pPolicy
  )
{
  policyman_policy_dtor(pPolicy);
}


/*-------- policy_generic_new_instance --------*/
policyman_policy_t  *
policy_generic_new_instance(
  char const  *pName
  )
{
  policyman_policy_generic_t  *pPolicy = NULL;
  
  if (
          strcmp(pName, "generic") == 0
      ||  strcmp(pName, "rat capability") == 0
      ||  strcmp(pName, "device configuration") == 0
      ||  strcmp(pName, "sglte") == 0
     )
  {
    pPolicy = policyman_mem_alloc(sizeof(policyman_policy_generic_t));
    ref_cnt_obj_init(pPolicy, policy_generic_dtor);

    pPolicy->pName = "generic";
    pPolicy->schema_ver = 2;
    pPolicy->id = POLICYMAN_POLICY_ID_GENERIC;
  }

  return (policyman_policy_t *) pPolicy;
}


/*-------- policy_generic_init_status --------*/
static void
policy_generic_init_status(
  policyman_policy_t  *pPolicy,
  policyman_status_t  status
  )
{
  switch (status)
  {
    case POLICYMAN_STATUS_SUCCESS:
      POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_STAR);
      POLICYMAN_MSG_HIGH_0("Generic policy: successfully initialized");
      POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_STAR);
      break;

    case POLICYMAN_STATUS_ERR_CONFIG_FAILURE:
      POLICYMAN_MSG_HIGH_0("Generic policy: configuration failed");
      break;

    case POLICYMAN_STATUS_ERR_POLICY_NOT_ENABLED:
      POLICYMAN_MSG_HIGH_0("Generic policy: policy disabled in XML");
      break;

    case POLICYMAN_STATUS_ERR_INVALID_VERSION:
      POLICYMAN_MSG_HIGH_0("Generic policy: version mismatch between XML and policy");
      break;

    case POLICYMAN_STATUS_ERR_MALFORMED_XML:
      POLICYMAN_MSG_HIGH_0("Generic policy: cannot parse XML file");
      break;

    default:
      POLICYMAN_MSG_HIGH_1("Generic policy: unknown status %d", status);
      break;
  }
}

/*-------- policy_generic_configure --------*/
static policyman_status_t
policy_generic_configure(
  policyman_policy_t   *pPolicy,
  policy_execute_ctx_t *pCtx
  )
{
  POLICYMAN_MSG_HIGH_0("Configuring Generic policy");

  POLICYMAN_MSG_HIGH_0("GENERIC: policy configured");
  return POLICYMAN_STATUS_SUCCESS;
}
/*-------- policy_generic_notify_service --------*/
static void
policy_generic_notify_service(
  policyman_policy_t   *pPolicy,
  policy_execute_ctx_t *pCtx
  )
{
  sys_oprt_mode_e_type         oprt_mode = policyman_ph_get_oprt_mode(pCtx->pState, POLICY_SUBS(pPolicy));

  if (oprt_mode == SYS_OPRT_MODE_ONLINE)
  {
    POLICYMAN_MSG_HIGH_0("calling for rule execution from generic_notify_service");
    policyman_policy_execute(pPolicy, pCtx);
  }
}


/*-------- policy_generic_handle_user_pref_update --------*/
static void policy_generic_handle_user_pref_update(
  policyman_policy_t       *pPolicy,
  policy_execute_ctx_t     *pCtx
)
{  
  sys_oprt_mode_e_type  oprt_mode  = policyman_ph_get_oprt_mode(pCtx->pState, POLICY_SUBS(pPolicy));
  policyman_ss_info_t  *pSsInfo    = policyman_state_get_serving_system_info(pCtx->pState, pCtx->asubs_id);
  size_t                index;

  /* reset the SS state info and preconditions if UE entered LPM mode
  */
  if (oprt_mode == SYS_OPRT_MODE_LPM)
  {

    for (index = SYS_MODEM_STACK_ID_1; index < SYS_MODEM_STACK_ID_MAX; index++)
    {
      policyman_ss_set_default_value(pSsInfo, index);
    }
    policyman_state_reset_precondition_met(pCtx->pState, POLICYMAN_PRECOND_SS | POLICYMAN_PRECOND_LOCATION, pCtx->asubs_id);
  }
}

/*-------- policyman_is_srlte_plmn --------*/
policyman_status_t policyman_is_srlte_plmn(
  sys_plmn_id_s_type  *pPlmnId,
  boolean              checkMccOnly,
  boolean             *pSvdPossible
)
{
  policyman_set_t    *pMccSet;
  policyman_set_t    *pPlmnSet;
  policyman_status_t  retval = POLICYMAN_STATUS_SUCCESS;
  boolean             is_srlte_on_any_plmn = FALSE;
  boolean            *pIsSrlteOnAnyPlmn = NULL;


  pMccSet  = (policyman_set_t *)mre_named_object_find_with_subs( "sxlte_mccs",
                                                                 POLICYMAN_NAMED_MCC_SET,
                                                                 SYS_MODEM_AS_ID_1,
                                                                 policyman_get_namespace_info() );

  pPlmnSet = (policyman_set_t *)mre_named_object_find_with_subs( "sxlte_plmns",
                                                                 POLICYMAN_NAMED_PLMN_SET,
                                                                 SYS_MODEM_AS_ID_1,
                                                                 policyman_get_namespace_info() );

  pIsSrlteOnAnyPlmn = (boolean *)mre_named_object_find_with_subs( "pm:srlte_on_any_plmn",
                                                                  POLICYMAN_NAMED_BOOLEAN,
                                                                  SYS_MODEM_AS_ID_1,
                                                                  policyman_get_namespace_info() );
  if (NULL != pIsSrlteOnAnyPlmn) 
  {
    is_srlte_on_any_plmn = *pIsSrlteOnAnyPlmn;
  }

  if (!is_srlte_on_any_plmn)
  {
    // Same as pm:srlte_on_any_plmn set if have sxlte_mccs with no sxlte_plmns
    is_srlte_on_any_plmn = ( (NULL != pMccSet) && (NULL == pPlmnSet) );
  }

  /* If only MCC check is needed, compare against MCC of the PLMNs in list
   */
  if (checkMccOnly || is_srlte_on_any_plmn)
  {
    sys_mcc_type       *listMcc;
    sys_mcc_type        mcc;
    size_t              nElems;
    size_t              i;

    /* find a set of SVD MCCs, if not found then return error
      */
    if (pMccSet == NULL)
    {
      retval = POLICYMAN_STATUS_ERR_NOT_PRESENT;
      goto Done;
    }

    mcc    = policyman_plmn_get_mcc(pPlmnId);
    nElems = policyman_set_num_elems(pMccSet);
    
    for (i = 0; i < nElems; i++)
    {
      listMcc = (sys_mcc_type *)policyman_set_elem_ptr(pMccSet, i);

      if (listMcc == NULL)
      {
        POLICYMAN_MSG_ERROR_0("policyman_svd_possible_on_plmn: listMcc is NULL");
        retval = POLICYMAN_STATUS_ERR_INVALID_ARGS;
        goto Done;
      }

      if (mcc == *listMcc)
      {
        *pSvdPossible = TRUE;
        break;
      }
    }
  }
  else
  {    
    // TODO: Alias 'sxlte_plmns' to 'svd_plmns' in XML and use 'svd_plmns' here
    /* find a set of SVD PLMNs, if not found then return error
      */
    if (pPlmnSet == NULL)
    {
      POLICYMAN_MSG_ERROR_0("policyman_svd_possible_on_plmn: set 'sxlte_plmns' is NULL");
      retval = POLICYMAN_STATUS_ERR_NOT_PRESENT;
      goto Done;
    }

    *pSvdPossible = policyman_set_contains(pPlmnSet, pPlmnId);
  }

Done:
  return retval;
}


/*-------- policyman_svd_possible_on_plmn --------*/
/**
@brief  Function to determine if SVD is possible on PLMN
*/
policyman_status_t policyman_svd_possible_on_plmn(
  sys_plmn_id_s_type  *pPlmnId,
  boolean              mccOnly,
  boolean             *pSvdPossible
)
{
  sys_subs_feature_t     feature = SYS_SUBS_FEATURE_MODE_NORMAL;
  policyman_status_t     retval  = POLICYMAN_STATUS_SUCCESS;
  
  if (NULL == pPlmnId)
  {
    retval = POLICYMAN_STATUS_ERR_INVALID_ARGS;
    POLICYMAN_MSG_ERROR_0("policyman_svd_possible_on_plmn: invalid arguments");
    goto Done;
  }

  /* Get Device config Policy Item
   */
  {
    policyman_item_t    *pItem;
    retval = policyman_get_current_device_config((policyman_item_t **)&pItem); 

    if (retval != POLICYMAN_STATUS_SUCCESS)
    {
     POLICYMAN_MSG_ERROR_0("policyman_svd_possible_on_plmn: internal error");
     goto Done;
    }

   (void)policyman_device_config_get_subs_feature(pItem, 0, SYS_MODEM_AS_ID_1, &feature);  
    ref_cnt_obj_release(pItem);
  }
  
  *pSvdPossible = FALSE;
  POLICYMAN_MSG_HIGH_0("queried PLMN is ");
  policyman_plmn_print(pPlmnId);

  if (  feature == SYS_SUBS_FEATURE_MODE_SRLTE
     || feature == SYS_SUBS_FEATURE_MODE_DUAL_MULTIMODE
     )
  {
    retval = policyman_is_srlte_plmn(pPlmnId, mccOnly, pSvdPossible);
  }

Done:

 POLICYMAN_MSG_HIGH_3( "policyman_svd_possible_on_plmn: feature=%d , retval=%d, svdpossible=%d",
                       feature,
                       retval,
                       *pSvdPossible );

  return retval;
}

/*-------- policyman_is_svd_allowed --------*/
policyman_status_t policyman_is_svd_allowed(
  boolean *pIsSvd
)
{
  /****************************************************************************
    This implementation is a hack.  It relies on knowledge of what is in
    mre_namespace_info_t to get the set in which the "sxlte_allowed" boolean
    will be put, relying on the fact that SXLTE will only ever be on the
    primary subs.
  ****************************************************************************/
  
  mre_namespace_info_t  *pInfo;

  if (!pIsSvd)
  {
    return POLICYMAN_STATUS_ERR_INVALID_ARGS;
  }

  pInfo = policyman_get_namespace_info();

  *pIsSvd = FALSE;
  (void) mre_boolean_get_value_from_set(
              "sxlte_allowed",
              pIsSvd,
              pInfo->perSubsNamedObjs[0]
              );

  return POLICYMAN_STATUS_SUCCESS;
}

/*-------- policyman_is_svd_operation_allowed --------*/
boolean policyman_is_svd_operation_allowed(
  sys_modem_as_id_e_type subsId
)
{
  policyman_item_t      *pItem = NULL;
  policyman_status_t    status;
  sys_subs_feature_t    feature; 
  boolean               retval = FALSE;

  /* Check range for subsId
   */
  if(subsId < SYS_MODEM_AS_ID_1 || subsId > SYS_MODEM_AS_ID_3)
  {
    return retval;
  }

  /* Get Device config Policy Item
   */
  status = policyman_get_current_device_config((policyman_item_t **)&pItem); 

  /* UE is allowed for SVD if subs feature mode is not NORMAL and not SRLTE
   */
  if(status == POLICYMAN_STATUS_SUCCESS)
  {
    switch(subsId)
    {
      case SYS_MODEM_AS_ID_1:       

        /* get SUBS feature mode
             */
        status = policyman_device_config_get_subs_feature(pItem, 0, subsId, &feature);
        if(status == POLICYMAN_STATUS_SUCCESS)
        {
          retval = !(   feature == SYS_SUBS_FEATURE_MODE_NORMAL
                     || feature == SYS_SUBS_FEATURE_MODE_SRLTE
                    );
        }
       break;

      /* Any other subscription is NORMAL
          */
      case SYS_MODEM_AS_ID_2:
      case SYS_MODEM_AS_ID_3:
      default:
        break;
    }
  }

  REF_CNT_OBJ_RELEASE_IF(pItem);

  return retval;
}


/*-----------------------------------------------------------------------------
  VTable for policy_generic
-----------------------------------------------------------------------------*/
policyman_policy_vtbl_t  policyman_policy_generic_vtbl =
{
  policy_generic_new_instance,
  policy_generic_init_status,
  policy_generic_configure,
  NULL,
  policy_generic_notify_service,
  policy_generic_handle_user_pref_update,
  NULL
};


