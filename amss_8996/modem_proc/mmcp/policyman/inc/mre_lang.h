#ifndef _MRE_LANG_H_
#define _MRE_LANG_H_

/**
  @file mre_lang.h

  @brief  Code for the "language" used in the Modem Rules Engine XML.
*/

/*
    Copyright (c) 2013-2015 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mmcp/policyman/inc/mre_lang.h#1 $
  $DateTime: 2016/03/28 23:03:19 $
  $Author: mplcsds1 $
*/

#include "mre_xml.h"


/*=============================================================================
  Conditions
=============================================================================*/

mre_condition_t * mre_condition_new(
  size_t                    condSize,
  pfn_evaluate_t            pEvaluate,
  destructor_ptr            pDtor
);

mre_status_t mre_condition_true_new(
  mre_xml_element_t const  *pElem,
  mre_policy_t             *pPolicy,
  mre_condition_t         **ppCondition
);


mre_status_t mre_boolean_test_new(
  mre_xml_element_t const  *pElem,
  mre_policy_t             *pPolicy,
  mre_condition_t         **ppCondition
);


mre_status_t mre_condition_not_new(
  mre_xml_element_t const  *pElem,
  mre_policy_t             *pPolicy,
  mre_condition_t         **ppCondition
);


mre_status_t mre_condition_or_new(
  mre_xml_element_t const  *pElem,
  mre_policy_t             *pPolicy,
  mre_condition_t         **ppCondition
);

mre_status_t mre_condition_and_new(
  mre_xml_element_t const  *pElem,
  mre_policy_t             *pPolicy,
  mre_condition_t         **ppCondition
);


/*=============================================================================
  Actions
=============================================================================*/

mre_action_t * mre_action_new(
  size_t                    actionSize,
  pfn_execute_t             pExecute,
  destructor_ptr            pDtor
);

mre_status_t mre_continue_new(
  mre_xml_element_t const  *pElem,
  mre_policy_t             *pPolicy,
  mre_action_t            **ppAction
);


mre_status_t mre_boolean_define_new(
  mre_xml_element_t const  *pElem,
  mre_policy_t             *pPolicy,
  mre_action_t            **ppAction
);


mre_status_t mre_boolean_set_new(
  mre_xml_element_t const  *pElem,
  mre_policy_t             *pPolicy,
  mre_action_t            **ppAction
);

mre_status_t mre_if_action_new(
  mre_xml_element_t const  *pElem,
  mre_policy_t             *pPolicy,
  mre_action_t            **ppAction
);

#endif /* _MRE_LANG_H_ */
