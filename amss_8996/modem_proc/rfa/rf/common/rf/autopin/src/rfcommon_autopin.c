/*!
  @file
  rfcommon_autopin.c

  @brief
  This module contains implementations of common AutoPin.
*/

/*==============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfa/rf/common/rf/autopin/src/rfcommon_autopin.c#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
10/29/15   whz     Remove timers completely
10/20/15   whz     Move buffer cleanup from disable to enable API
10/14/15   whz     Remove timers as we schedule AutoPin every time
09/22/15   kma     Updated debug msg
09/15/15   whz     Revert CL905111 but provide another fix
09/14/15   whz     Remove unncessary FBRx AG check
                   Clear Pin accum when disabling Tx
08/25/15   cdb     Update default loop speed for LTE
07/21/15   cdb     Add EFS support 
07/16/15   cdb     Abstract Thor/AMAM specific processing 
07/15/15   cdb     Add support to override Autopin operation
07/02/15   whz     Do not clean-up timers in exit_mode
06/09/15   whz     Provide buffer to store FBRx IQ
06/04/15   whz     Initial version

==============================================================================*/


#include "rfcommon_autopin.h"
#include "msg.h"
#include "rf_dispatch.h"
#include "rf_cmd_interface.h"
#include "rf_task.h"
#include "rflm_dm_api.h"
#include "modem_mem.h"
#include "rflm_autopin.h"
#include "rflm_api_fbrx_fw.h"
#include "rfcommon_autopin_db.h"

typedef enum
{
    FTM_AUTOPIN_CMD_OVR_CTRL = 1,     /* Enable/Disable AutoPin override mode */
    FTM_AUTOPIN_CMD_RUN_FREEZE,       /* Override to freeze run decision execution */
    FTM_AUTOPIN_CMD_ACCUM_FREEZE,     /* Override to freeze pin accum updates */
    FTM_AUTOPIN_CMD_ACCUM_OVERRIDE,   /* Override to Pin accum values */
    FTM_AUTOPIN_CMD_DEBUG_CTRL,       /* Reserved for generic debug */
    FTM_AUTOPIN_CMD_MAX
}ftm_autopin_cmd_type;

static timetick_type rfcommon_autopin_acq_time_ms[RFCOM_TXLIN_NUM_LINEARIZERS][RFLM_MAX_TECH];

static timetick_type rfcommon_autopin_trk_time_ms[RFCOM_TXLIN_NUM_LINEARIZERS][RFLM_MAX_TECH];

static boolean rfcommon_autopin_debug = FALSE;

uint32 rfcommon_autopin_tx_iqbuf[RFLM_AUTOPIN_MAX_TX_SAMPLE_SIZE] = { 0 };
uint32 rfcommon_autopin_tx_proc_iqbuf[RFLM_AUTOPIN_MAX_TX_SAMPLE_SIZE] = { 0 };

uint32 rfcommon_autopin_rx_iqbuf[RFLM_AUTOPIN_MAX_RX_SAMPLE_SIZE] = { 0 };
uint32 rfcommon_autopin_rx_proc_iqbuf[RFLM_AUTOPIN_MAX_RX_SAMPLE_SIZE] = { 0 };

/* default algorithm parameters */
#define RFLM_AUTOPIN_DEFAULT_ACQ_TIME1 (100)  /* ms */
#define RFLM_AUTOPIN_DEFAULT_ACQ_TIME2  (10)  /* ms */
#define RFLM_AUTOPIN_DEFAULT_TRK_TIME (1000)  /* ms */
#define RFLM_AUTOPIN_DEFAULT_TRK_THRESH (0)
#define RFLM_AUTOPIN_DEFAULT_STEP_SIZE (1)    /* 0.1dB dB10 */

void rfcommon_autopin_get_nv_info(rfm_mode_enum_type rfm_tech)
{
  uint8 pa_state;
  uint32 acq_time;
  autopin_efs_record_t* efs_record;

  rflm_tech_id_t tech = rfcommon_autopin_mc_convert_rfm_mode_to_rflm_tech(rfm_tech);

  efs_record = rfcommon_autopin_efs_get_record(tech);

  if (efs_record==NULL)
  {
    /* no valid EFS file exist. Use default values */
    switch (tech)
    {
      case RFLM_TECH_1X:
      case RFLM_TECH_HDR:
      case RFLM_TECH_WCDMA:
      case RFLM_TECH_TDSCDMA:
        acq_time = RFLM_AUTOPIN_DEFAULT_ACQ_TIME1;
        break;
      default: 
        acq_time = RFLM_AUTOPIN_DEFAULT_ACQ_TIME2;
        break;
  }
    for (pa_state = 0; pa_state < RFCOM_TXLIN_NUM_LINEARIZERS; pa_state++)
    {
      rfcommon_autopin_acq_time_ms[pa_state][tech] = acq_time;
      rfcommon_autopin_trk_time_ms[pa_state][tech] = RFLM_AUTOPIN_DEFAULT_TRK_TIME;
    }
    rflm_autopin_set_params(tech, RFLM_AUTOPIN_DEFAULT_TRK_THRESH, RFLM_AUTOPIN_DEFAULT_STEP_SIZE);
  }
  else
  {
    /* valid EFS file exists, use efs record values for this tech */
    for (pa_state = 0; pa_state < RFCOM_TXLIN_NUM_LINEARIZERS; pa_state++)
    {
      rfcommon_autopin_acq_time_ms[pa_state][tech] = efs_record->loop_speed_acq;
      rfcommon_autopin_trk_time_ms[pa_state][tech] = efs_record->loop_speed_trk;
    }
    rflm_autopin_set_params(tech, efs_record->trk_thresh, efs_record->pin_step);
  }
}


rflm_tech_id_t rfcommon_autopin_mc_convert_rfm_mode_to_rflm_tech(rfm_mode_enum_type rfm_tech)
{
  rflm_tech_id_t rflm_tech;
  
  switch (rfm_tech)
  {
  case RFM_IMT_MODE:
  case RFM_IMT_MODE_2:
    rflm_tech = RFLM_TECH_WCDMA;
    break;

  case RFM_1X_MODE:
    rflm_tech = RFLM_TECH_1X;
    break;

  case RFM_1XEVDO_MODE:
  case RFM_SVDO_MODE:
    rflm_tech = RFLM_TECH_HDR;
    break;

  case RFM_LTE_MODE:
  case RFM_LTE_FTM_MODE:
    rflm_tech = RFLM_TECH_LTE;
    break;

  case RFM_TDSCDMA_MODE:
  case RFM_TDSCDMA_MODE_FTM:
    rflm_tech = RFLM_TECH_TDSCDMA;
    break;

  default:
    rflm_tech = RFLM_MAX_TECH;
    MSG_FATAL("AutoPin doesn't support tech %d",
              rfm_tech, 0, 0);
  }

  return rflm_tech;
}


/* Check if AutoPin feature is enabled for rfm_tech
 */
boolean rfcommon_autopin_is_enabled(rfm_mode_enum_type rfm_tech)
{
  rflm_tech_id_t tech = rfcommon_autopin_mc_convert_rfm_mode_to_rflm_tech(rfm_tech);
  return rflm_autopin_is_enabled(tech);
}


/* Do common and Tech specific AutoPin initialization
 * Need to be called in Tech's mc enter_mode
 */
rfcommon_autopin_error_type rfcommon_autopin_mc_enter_mode(rfm_mode_enum_type rfm_tech)
{
  rflm_tech_id_t tech = rfcommon_autopin_mc_convert_rfm_mode_to_rflm_tech(rfm_tech);

  if (!rfcommon_autopin_is_enabled(rfm_tech))
  {
    MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR,
          "AutoPin is not enabled for tech %d",
          rfm_tech);
    
    return RFCOMMON_AUTOPIN_ERROR;
  }

  rfcommon_autopin_get_nv_info(rfm_tech);

  return RFCOMMON_AUTOPIN_SUCCESS;
}


/* Do AutoPin cleanup
 * Need to be called in Tech's mc exit_mode
 */
rfcommon_autopin_error_type rfcommon_autopin_mc_exit_mode(rfm_mode_enum_type rfm_tech)
{
  rflm_tech_id_t tech = rfcommon_autopin_mc_convert_rfm_mode_to_rflm_tech(rfm_tech);

  if (!rfcommon_autopin_is_enabled(rfm_tech))
  {
    MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR,
          "AutoPin is not enabled for tech %d",
          rfm_tech);

    return RFCOMMON_AUTOPIN_ERROR;
  }

  return RFCOMMON_AUTOPIN_SUCCESS;
}


/* Do Tx specific initialization
 * Needs to be called when a new Tx handle is turned on 
 * (for example, in Tech's mc enable_tx) 
 * The associated AutoPin buffer in DM will be configured 
 */
rfcommon_autopin_error_type rfcommon_autopin_mc_enable_tx(rflm_handle_tx_t tx_handle,
                                                          rfm_mode_enum_type rfm_tech)
{
  rflm_tech_id_t tech = rfcommon_autopin_mc_convert_rfm_mode_to_rflm_tech(rfm_tech);

  rflm_autopin_dm_template_t *autopin_template = NULL;

  uint8 pa_state;

  rflm_fbrx_get_static_settings_in_t in = { 0 };
  rflm_fbrx_get_static_settings_out_t out = { 0 };

  if (!rfcommon_autopin_is_enabled(rfm_tech))
  {
    MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR,
          "AutoPin is not enabled for tech %d",
          rfm_tech);
    
    return RFCOMMON_AUTOPIN_ERROR;
  }

  autopin_template = (rflm_autopin_dm_template_t *)rflm_dm_get_autopin_buf_ptr(tx_handle);

  if (autopin_template == NULL)
  {
    MSG_2(MSG_SSID_RF, MSG_LEGACY_ERROR,
          "Failed to get AutoPin buffer from DM for tech %d, Tx handle %d",
          rfm_tech, tx_handle);
    
    return RFCOMMON_AUTOPIN_ERROR;
  }

  autopin_template->autopin_ctl.tech = tech;

  if (rflm_fbrx_get_static_settings(tx_handle, &in, &out) != RFLM_ERR_NONE)
  {
    MSG_2(MSG_SSID_RF, MSG_LEGACY_ERROR,
          "AutoPin get FBRx static settings failed for tech %d, Tx handle %d",
          rfm_tech, tx_handle);
  }

  autopin_template->autopin_ctl.txiqbuf.iq_cap_buf = rfcommon_autopin_tx_iqbuf;

  autopin_template->autopin_ctl.txiqbuf.iq_proc_buf = rfcommon_autopin_tx_proc_iqbuf;

  autopin_template->autopin_ctl.txiqbuf.num_samp = out.static_settings.num_tx_in_samples_c0;

  autopin_template->autopin_ctl.rxiqbuf.iq_cap_buf = rfcommon_autopin_rx_iqbuf;
  
  autopin_template->autopin_ctl.rxiqbuf.iq_proc_buf = rfcommon_autopin_rx_proc_iqbuf;

  autopin_template->autopin_ctl.rxiqbuf.num_samp = out.static_settings.num_rx_in_samples;

  memset(autopin_template->autopin_ctl.pin_accum, 0, sizeof(rflm_db10_t)*RFLM_TXAGC_PA_STATE_MAX);

  memset(autopin_template->autopin_ctl.run_state, RFLM_AUTOPIN_DEFAULT, sizeof(rflm_autopin_state)*RFLM_TXAGC_PA_STATE_MAX);

  autopin_template->autopin_ctl.init_done = TRUE;
  
  MSG_2(MSG_SSID_RF, MSG_LEGACY_HIGH,
          "AutoPin Tx is enabled for tech %d handle %d",
          rfm_tech, tx_handle);
  
  return RFCOMMON_AUTOPIN_SUCCESS;
}


/* Do Tx specific cleanup
 * Needs to be called when the Tx handle is turned off 
 * (for example, in Tech's mc disable_tx)
 * The associated DM AutoPin buffer for the Tx handle will NOT be cleaned up
 */
rfcommon_autopin_error_type rfcommon_autopin_mc_disable_tx_preserv(rflm_handle_tx_t tx_handle,
                                                                   rfm_mode_enum_type rfm_tech)
{
  rflm_tech_id_t tech = rfcommon_autopin_mc_convert_rfm_mode_to_rflm_tech(rfm_tech);

  uint8 pa_state;
  
  if (!rfcommon_autopin_is_enabled(rfm_tech))
  {
    return RFCOMMON_AUTOPIN_ERROR;
  }

  MSG_2(MSG_SSID_RF, MSG_LEGACY_HIGH,
        "AutoPin Tx is disabled for tech %d handle %d",
        rfm_tech, tx_handle);
  
  return RFCOMMON_AUTOPIN_SUCCESS;
}


/* Do Tx specific cleanup
 * Needs to be called when the Tx handle is turned off 
 * (for example, in Tech's mc disable_tx)
 * The associated DM AutoPin buffer for the Tx handle will be cleaned up
 */
rfcommon_autopin_error_type rfcommon_autopin_mc_disable_tx(rflm_handle_tx_t tx_handle,
                                                           rfm_mode_enum_type rfm_tech)
{
  rflm_autopin_dm_template_t *autopin_template = NULL;

  autopin_template = (rflm_autopin_dm_template_t *)rflm_dm_get_autopin_buf_ptr(tx_handle);

  if (autopin_template == NULL)
  {
    MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR,
          "Failed to get AutoPin buffer from DM for tech %d",
          rfm_tech);
    
    return RFCOMMON_AUTOPIN_ERROR;
  }

  autopin_template->autopin_ctl.tech = RFLM_MAX_TECH;
  
  if (rflm_autopin_in_progress == TRUE)
  {
    // Issue a warning but continue
    MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR,
          "Common FW FBRx process ongoing when disabling Tx for tech %d",
          rfm_tech);
  }
  
  autopin_template->autopin_ctl.init_done = FALSE;
  
  return rfcommon_autopin_mc_disable_tx_preserv(tx_handle, rfm_tech);
}


/*------------------------------------------------------------------------------------------------*/
/*!
  @brief
  Override AutoPin execution
 
  @details
  Set overrides to control AutoiPin loop and accumulator
 
  @param tx_handle
  TxLM handle to which the override applies
 
  @param ovr_cmd_id
  Override command ID
 
  @param ovr_enable
  Enable/disable the override applicable to the command ID

  @param ovr_val1
  Usage depend on command ID
  
  @return
  Status of execution
*/
rfcommon_autopin_error_type rfcommon_autopin_override_ctrl(uint8 tx_handle,
                                                           uint8 ovr_cmd_id,
                                                           uint8 ovr_enable,
                                                           uint8 ovr_val1,
                                                           uint8 ovr_val2)
{
  rflm_autopin_dm_template_t *autopin_template = NULL;
  rflm_autopin_proc_results_t meas_result;
  
  autopin_template = (rflm_autopin_dm_template_t *)rflm_dm_get_autopin_buf_ptr((rflm_handle_tx_t)tx_handle);
  if (autopin_template == NULL)
  {
    MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR,
          "Failed to get AutoPin buffer from DM for Tx handle %d",
          tx_handle);
    return RFCOMMON_AUTOPIN_ERROR;
  }
  
  switch (ovr_cmd_id)
  {
    case FTM_AUTOPIN_CMD_OVR_CTRL:
    case FTM_AUTOPIN_CMD_RUN_FREEZE:
    case FTM_AUTOPIN_CMD_ACCUM_FREEZE:
    case FTM_AUTOPIN_CMD_ACCUM_OVERRIDE:
        RF_MSG_3(RF_MED, "autopin override cmd %d, enable %d, handle %d", ovr_cmd_id, ovr_enable, tx_handle);
        if (ovr_cmd_id == FTM_AUTOPIN_CMD_OVR_CTRL)
        {
          /* Enable/disable debug mode for specified TX handle */
          autopin_template->override_ctl.debug_active = ovr_enable;
        } 
        else if (ovr_cmd_id == FTM_AUTOPIN_CMD_RUN_FREEZE)
        {
          /* freeze run decision execution for specified handle */
          autopin_template->override_ctl.pin_run_freeze = ovr_enable;
        }
        else if (ovr_cmd_id == FTM_AUTOPIN_CMD_ACCUM_FREEZE)
        {
          /* freeze Pin accumulator updates for specified handle */
          autopin_template->override_ctl.pin_accum_freeze = ovr_enable;
        }
        else if (ovr_cmd_id == FTM_AUTOPIN_CMD_ACCUM_OVERRIDE)
        {
          /* override Pin accumulator for specified handle */
          if (ovr_val1 >= 0x80)
          {
            autopin_template->override_ctl.pin_accum_ovr_val[ovr_val2] = (int32)(0x80-ovr_val1);
          }
          else
          {
            autopin_template->override_ctl.pin_accum_ovr_val[ovr_val2] = ovr_val1;
          }             
          autopin_template->override_ctl.pin_accum_override = ovr_enable;
          RF_MSG_2(RF_MED, "pin_accum_override value = %d, pa_state = %d", 
                       autopin_template->override_ctl.pin_accum_ovr_val[ovr_val2],
                       ovr_val2);
        }
      break;
    case FTM_AUTOPIN_CMD_DEBUG_CTRL:
        switch (ovr_val1)
        {
          case 1 :
              /* simulated FW result */ 
              meas_result.seq_nr = 0;
              meas_result.valid_result = TRUE;
              if (ovr_val2 >= 0x80)
              {
                meas_result.meas_error = (int32)(0x80-ovr_val2);
              }
              else
              {
                meas_result.meas_error = ovr_val2;
              }
              RF_MSG_2(RF_MED, "meas_error = %d, handle = %d", meas_result.meas_error, tx_handle);
              rflm_autopin_update_proc_result( (rflm_handle_tx_t)tx_handle, &meas_result);
            break;
          default:
            RF_MSG_1(RF_ERROR,"Bad debug id: %d", ovr_val1);
            break;
        }
      break;
    default:
      RF_MSG_1(RF_ERROR,"rfcommon_autopin_override_ctrl: Bad ovr_cmd_id: %d", ovr_cmd_id);
      break;
  }
  return RFCOMMON_AUTOPIN_SUCCESS;
}
