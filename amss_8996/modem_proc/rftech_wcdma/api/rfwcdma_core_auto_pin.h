#ifndef RFWCDMA_CORE_AUTO_PIN_H
#define RFWCDMA_CORE_AUTO_PIN_H
/*!
  @file
  rfwcdma_core_auto_pin.h

  @brief
  Provides WCDMA auto pin Compensation functionality

  @details

*/

/*===========================================================================

Copyright (c) 2008 - 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$DateTime: 2016/03/28 23:07:43 $ $Author: mplcsds1 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rftech_wcdma/api/rfwcdma_core_auto_pin.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/12/15   aa      Initial version.


============================================================================*/
#include "comdef.h"
#include "rfcom.h"
#include "rfcommon_time_profile.h"
#include "rfcommon_locks.h"
#include "rf_dispatch.h"

/*----------------------------------------------------------------------------*/

void
rfwcdma_auto_pin_update_handler
(
  void* cmd_ptr,
  rf_dispatch_cid_info_type *cid_info,
  void *cb_data 
);



#endif /* RFWCDMA_CORE_AUTO_PIN_H */

