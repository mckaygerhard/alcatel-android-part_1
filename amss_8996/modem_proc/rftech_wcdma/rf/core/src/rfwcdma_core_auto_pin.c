/*!
  @file
  rfwcdma_core_temp_comp.c

  @brief
  Provides WCDMA Temperature Compensation functionality

*/

/*===========================================================================

Copyright (c) 2008 -2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$DateTime: 2016/03/28 23:07:43 $ $Author: mplcsds1 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rftech_wcdma/rf/core/src/rfwcdma_core_auto_pin.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/18/15   aa      Add Reference NV pin value to autopin accumulator
06/11/15   aa      Initial version.

============================================================================*/
#include "rfwcdma_core_temp_comp.h"
#include "rfwcdma_core_auto_pin.h"
#include "rflm_wcdma_msg.h"
#include "rfcommon_autopin.h"
#include "DDIAdc.h"
#include "comdef.h"
#include "rfwcdma_core_txlin.h"
#include "rfwcdma_core_txplim.h"
#include "rfwcdma_core_rxctl.h"
#include "rfnv_wcdma.h"
#include "rfdevice_wcdma_intf.h"
#include "rfcommon_nv_mm.h"
#include "ftm.h"
#include "pm.h"
#include "rfc_card.h"
#include "rfwcdma_mdsp.h"
#include "rfwcdma_mdsp_async.h"
#include "rfwcdma_mc.h"
#include "rfm_internal.h"
#include "rfcommon_math.h"
#include "rfwcdma_core.h"
#include "rfwcdma_core_util.h"
#include "rfwcdma_core_xpt.h"
#include "rfcommon_fbrx_api.h"
#include "rfwcdma_data.h"
#include "rfcommon_locks.h"
#include "rflm_wcdma_sw_api.h"
#include "rfdevice_hdet_cmn_intf.h"
#include "rfdevice_hdet_wcdma_intf.h"
#include "rf_cmd_interface.h"
#include "rfm_device_types.h"


static boolean rfwcdma_core_auto_pin_is_enabled = TRUE;

extern rfwcdma_mc_state_type rfwcdma_mc_state;
extern rfwcdma_core_txlin_record_type rfwcdma_core_current_txlin_data;
extern rfwcdma_core_temp_comp_value_type rfwcdma_core_temp_comp_value;
int16 pout_comp_auto_pin_ref[RFCOM_TXLIN_NUM_LINEARIZERS] = {0};
int16 pin_comp_auto_pin_ref[RFCOM_TXLIN_NUM_LINEARIZERS] = {0};


/*----------------------------------------------------------------------------*/
/*!
  @brief
  Auto Pin update Handler function
 
  @details
  This function will be called by RF APPS task dispatcher when auto pin
  command is posted to the command Q beloging to RF APPS task. This function
  will perform Linearizer update and applies Pin value
*/
void
rfwcdma_auto_pin_update_handler
(
  void *cmd_ptr,
  rf_dispatch_cid_info_type *cid_info,
  void *cb_data
  )
{
  rf_time_type autopin_time;      /* prep_wakeup Time */
  rf_time_tick_type autopin_tick;      /* prep_wakeup Time */
  rfm_device_enum_type device = RFM_DEVICE_0;
  rf_path_enum_type path = RF_PATH_0;
  uint8  i;
  rflm_wcdma_process_auto_pin_payload_type *auto_pin_data = NULL;
  rfwcdma_core_txagc_comp_type update_type[RFCOM_TXLIN_NUM_LINEARIZERS];
  int16 pin_comp_offset;
  rfwcdma_data_status_type* radio_status = NULL;
  uint32 handle_id;
  /* Start Profiling of API */
  autopin_tick = rf_time_get_tick();

  /* Sanity - check if data ptr from MSGR is valid */
  if (cmd_ptr == NULL)
  {
    RF_MSG(RF_ERROR, "rfwcdma_auto_pin_update_handler: NULL MSGR data ptr!");
    return;
  }

  /* Get the payload from MSGR cmd_ptr */
  auto_pin_data = (rflm_wcdma_process_auto_pin_payload_type *)((rf_cmd_type *)cmd_ptr)->payload;
  device = rfwcdma_data_get_tx_device();
  update_type[auto_pin_data->pa_state] = TX_AGC_AUTO_PIN_COMP;
  pin_comp_offset = auto_pin_data->pin_error;
  handle_id = auto_pin_data->handle_id;

  /* Throw F3 Message */
  RF_MSG_3(RF_ERROR, "rfwcdma_auto_pin_update_handler : START on device %d, pin = %d, tx_handle = %d", device, pin_comp_offset,  auto_pin_data->handle_id  );

  radio_status = rfwcdma_get_radio_status(device);

  if ( radio_status == NULL)
  {
     RF_MSG_1(RF_ERROR, "rfwcdma_auto_pin_update_handler : NULL radio status for device %d", device );
     return;
  }
  
  RF_MSG_3(RF_ERROR, "rfwcdma_auto_pin_update_handler : rflm handle on device %d = %d, passed tx_handle = %d", device, radio_status->rflm_handle  ,   auto_pin_data->handle_id  );

  if ( radio_status->rflm_handle == auto_pin_data->handle_id  )//rfwcdma_data_get_rf_state(device) == RFWCDMA_STATE_TX)
  {

    /*Temp compensation should not happen during RF Calibration(FTM state)*/
    if ( (!IS_FTM_IN_TEST_MODE()) || ( rfm_get_calibration_state() == FALSE ) )
    {
      pin_comp_offset += pin_comp_auto_pin_ref[auto_pin_data->pa_state]  ; 
      //RF_MSG_3(RF_ERROR, "rfwcdma_auto_pin_update_handler :  pin error = %d, pin ref = %d , PA state  = %d", pin_comp_offset, pin_comp_auto_pin_ref[auto_pin_data->pa_state], auto_pin_data->pa_state);
      rfwcdma_core_txlin_auto_pin_comp_update(device, handle_id, pin_comp_offset, auto_pin_data->pa_state );
    }


  }
  else
  {
    RF_MSG_1(RF_HIGH, "rfwcdma_temp_comp_update_handler: Skipping "
             "RadioState %d", rfwcdma_data_get_rf_state(device));
  }

/* get time spent in API */
  autopin_time = rf_time_get_elapsed(autopin_tick, RF_USEC);

  RF_MSG_1(RF_HIGH, "rfwcdma_auto_pin_update_handler : TimeTaken %d us",
           autopin_time);

} /* rfwcdma_auto_pin_update_handler */


