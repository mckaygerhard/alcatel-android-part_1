
#===============================================================================
#
#
# GENERAL DESCRIPTION
#
# Copyright (c) 2015 QUALCOMM Technologies Incorporated. All Rights Reserved
#
# Qualcomm Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of QUALCOMM Technologies Incorporated.
#
#-------------------------------------------------------------------------------
#
# $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfc_thor/build/dynamic_mem_list.py#1 $
#
#                      EDIT HISTORY FOR FILE
#                      
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 07/31/15   fhuo    Add the check for duplicate RFC folders(i.e., cards exist both 
#                    in RF_card and RF_card_internal folder)
# 07/17/15   fhuo    Add the 2 cards memory sharing feature
#===============================================================================

from glob import glob
import re
import os


class DynError(Exception):
   def __init__(self, value):
      self.value = "Dynamic Memory Reclaim Error: " + value
   def __str__(self):
      return repr(self.value)
# End of class DynError

#get the directory name of the card
def dynrec_get_dir (card_dir):
    card_absdir = os.path.abspath(card_dir)
    #print(card_absdir)

    dynrec_dir = re.search(r'(modem_proc.*)', card_absdir, re.M|re.I)
    #print(dynrec_dir.group())

    if (dynrec_dir):
        dynrec_card_dir = dynrec_dir.group().replace('\\', '/')
        return(dynrec_card_dir)
#end of dynrec_get_dir

#write the text header of the dynrec.list file
def dynrec_write_text_header ():
    f_dynrec.write("/******************************************************************************\n")
    f_dynrec.write("                                TEXT and RODATA\n")
    f_dynrec.write("******************************************************************************/\n")
#end of dynrec_write_text_header

#write the data header of the dynrec.list file
def dynrec_write_data_header ():
    f_dynrec.write("/******************************************************************************\n")
    f_dynrec.write("                                DATA\n")
    f_dynrec.write("******************************************************************************/\n")
#end of dynrec_write_data_header

#write a text entry in the dynrec.list file
def dynrec_write_text_entry (card_dir,card_id1, card_id2='0'):
    text_start1 = "__dynrec_text_start_" + str(card_id1) + " = .;\n"
    f_dynrec.write(text_start1)
    #write to file the second card if the RFC folder is shared between two cards
    if(card_id2 !='0'):
        text_start2 = "__dynrec_text_start_" + str(card_id2) + " = .;\n"
        f_dynrec.write(text_start2)
    dynrec_dir = dynrec_get_dir(card_dir)
    f_dynrec.write("*" + dynrec_dir + "/?*:(.text .text.* .rodata .rodata.*)\n")
    f_dynrec.write(". = ALIGN(4K);\n")
    text_end1 = "__dynrec_text_end_" + str(card_id1) + " = .;\n"
    f_dynrec.write(text_end1)
    if(card_id2!='0'):
        text_end2 = "__dynrec_text_end_" + str(card_id2) + " = .;\n"
        f_dynrec.write(text_end2)
    f_dynrec.write("\n")
    #print(text_start)
#end of dynrec_write_text_entry

#write a data entry in the dynrec.list file
def dynrec_write_data_entry (card_dir, card_id1, card_id2='0'):
    data_start1 = "__dynrec_data_start_" + str(card_id1) + " = .;\n"
    #write to file the second card if the RFC folder is shared between two cards
    f_dynrec.write(data_start1)
    if(card_id2 !='0'):
        data_start2 = "__dynrec_data_start_" + str(card_id2) + " = .;\n"
        f_dynrec.write(data_start2)
    dynrec_dir = dynrec_get_dir(card_dir)
    f_dynrec.write("*" + dynrec_dir + "/?*:(.data .data.*)\n")
    f_dynrec.write(". = ALIGN(4K);\n")
    data_end1 = "__dynrec_data_end_" + str(card_id1) + " = .;\n"
    f_dynrec.write(data_end1)
    if(card_id2!='0'):
        data_end2 = "__dynrec_data_end_" + str(card_id2) + " = .;\n"
        f_dynrec.write(data_end2)
    f_dynrec.write("\n")
    #print(data_start)
#end of dynrec_write_text_entry

#extract the card list from rfc_hwid.h
def extract_card_list (rfc_ids):    
    rfc_name_list = []
    rfc_hwid_list =[]
    for rfc_id_def in rfc_ids:
        rfc_card_exist = re.search(r'(rf.*) = \(uint8\)(\d+)', rfc_id_def, re.M|re.I)
        if rfc_card_exist:
            #print rfc_card_exist.group()
            rfc_name= re.sub(r'RF_HW','RFC',rfc_card_exist.group(1).strip(),re.M|re.I).lower()
            rfc_hwid = rfc_card_exist.group(2)
            rfc_name_list.append(rfc_name)
            rfc_hwid_list.append(rfc_hwid)
    return [rfc_name_list, rfc_hwid_list]
#end of extrct_card_list


#get the orig card and memory share card(if exists) ID of the card that are present
def dynrec_find_hwid (card_fold_name, rfc_name_list):
    dynrec_card_dir = card_fold_name.replace('\\', '/')
    searchObj = re.search(r'rfc_(wtr.*?)/', dynrec_card_dir, re.M|re.I)    
    if searchObj:
        rfc_card = 'rfc_'+searchObj.group(1)
        #look for a match between the rfc card in rfc_hwid.h and existence of rfc folder for that card 
        if (rfc_card in rfc_name_list):
            #look for the memory sharing scenario. '_mem_share' is used as the keyword to
            #identify two cards share the same rfc folder
            if (rfc_card+'_mem_share' in rfc_name_list):
                #return both IDs in case memory share is found
                return [rfc_name_list.index(rfc_card), rfc_name_list.index(rfc_card+'_mem_share')]
            else:
                return [rfc_name_list.index(rfc_card), 0]
        # This is the error case
        else:
            return [0,0]        
#end dynrec_find_hwid

# define hard coded test card
test_card_id = "rfc_0"
test_card_path = "../../rfa/rfc/rf_card/rfc_configurable_test_card"

# define hard coded efs card
efs_card_id = "rfc_191"
efs_card_path = "../../rfa/rfc/rf_card/rfc_efs_card"

# define all folders for reclaiming
folderpath_lst = ("rf_card", "rf_card_internal", "rf_card_tp")

# This script shall run under modem_proc\rfc_xxxx\build folder. The passed in parameter will 
# indicate the generated "dynrec.lst" file location.
# The following folders will be generated and written in dynrec.lst for potential memory reclaim.
#       rfa\rfc\rfc_card   for test card and efs card
#       rfc_xxxx\rfc_card  for regular card
#       rfc_xxxx\rfc_internal for internal card
#       rfc_xxxx\rfc_tp for tp cards

#main function
def dynamic_mem_list(dynrec_path):
    #stores card names read from rfc_hwid.h
    hwid_card_list = [] 
    #stores card ids read from rfc_hwid.h
    hwid_id_list = []
    #stores the card name if the same card read from rfc_hwid.h is found in any of the card folders
    rfc_card_list = [] 
    #stores the card ID if the same card read from rfc_hwid.h is found in any of the card folders
    rfc_id1_list = [] 
    #stores the ID of the memory sharing card, 0 is no memory share card is found
    rfc_id2_list = [] 

    f_hwid = open('../api/rfc_hwid.h', 'r')
    rfc_ids = f_hwid.readlines()
    f_hwid.close()
    
    #read from rfc_hwid.h and store the corresponding card names and IDs
    [hwid_card_list, hwid_id_list] = extract_card_list(rfc_ids)

    # populate rfc_card_list, rfc_id1_list, rfc_id2_list lists. Only the cards that have been defined 
    # in rfc_hwid.h will be included.
    for folderpath in folderpath_lst:
        subfolder = "../" + folderpath + "/" 
        subfolder_cards = subfolder + "rfc_*/"
        
        paths = glob(subfolder_cards)
        for tfc_c_fold in paths:
           [dynrec_id1, dynrec_id2] = dynrec_find_hwid(tfc_c_fold, hwid_card_list)
           if (dynrec_id1 != 0):
              rfc_card_list.append(tfc_c_fold)
              rfc_id1_list.append('rfc_'+str(hwid_id_list[dynrec_id1]))
              if (dynrec_id2 == 0):
                 rfc_id2_list.append(str(dynrec_id2))
              else:
                 rfc_id2_list.append('rfc_'+str(hwid_id_list[dynrec_id2]))
        #Hardcode for each rfc_folder      
        rfc_card_list.append(subfolder)
        rfc_id1_list.append(folderpath+'_unspecified')
        rfc_id2_list.append('0')
		
    #search for duplicate card files in the rfc folder to avoid dynamic 
    #reclaim malfunction
    temp_list = list(rfc_id1_list)
    temp_list.sort()
    for index in range(0, len(temp_list)-1):
        if(temp_list[index] == temp_list[index+1]):
            return temp_list[index].replace("rfc_","")

    #generate the dynrec.lst under the builder folder 
    dynrec_filepath = dynrec_path + "/dynrec.lst"
    
	
    #Start writing to the list file 
    global f_dynrec
    f_dynrec = open(dynrec_filepath, 'w')

    dynrec_write_text_header ()
    #write hard coded test card  and efs card text entry
    dynrec_write_text_entry(test_card_path, test_card_id)
    dynrec_write_text_entry(efs_card_path, efs_card_id)

    #write text information to file 
    length = len(rfc_id1_list)
		
    for index in range(0,length):
        if dynrec_id1 != 0:
            dynrec_write_text_entry( rfc_card_list[index],rfc_id1_list[index],rfc_id2_list[index])
      	
    dynrec_write_data_header ()
    #write hard coded test card  and efs card data entry                    
    dynrec_write_data_entry(test_card_path, test_card_id)
    dynrec_write_data_entry(efs_card_path, efs_card_id)
    
    #write data information to file 
    for index in range(0,length):
        if dynrec_id1 != 0:
            dynrec_write_data_entry( rfc_card_list[index],rfc_id1_list[index],rfc_id2_list[index])
        
    f_dynrec.close()
    return '0'

