/*!
  @file rf_device_factory.cpp 

  @brief
  This is the rf device factory which creates all devices.



*/

/*===========================================================================

  Copyright (c) 2015 - QUALCOMM Technologies Inc. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================


                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfc_thor/target/msm8996/src/rf_device_factory.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
10/23/15   dbc     Added support for 3rd Party XSW
08/20/15   hm      Added GRFC Tuner device support
07/20/15   dbc     Added support for QFE4320
07/14/15   px      Add 3rd party RFFE coupler support
06/22/15   px      Added 3rd party external LNA(GEN_LNA) device support
06/10/15   yb      Removed QFE2520
04/28/15   Saul    QFE3340FC. Debug Updates
04/08/15   tks     Added support for WTR3950 
01/05/15   ndb     Added Third party Coupler 
10/23/14   vv      Added QFE3100 support
09/15/14   Saul    Initial revision.


===========================================================================*/

/*===========================================================================

                            INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "rfcommon_atuner_manager_factory.h"
#include "rf_device_factory.h"
#include "rfdevice_qpa.h"
#include "rfdevice_qasm.h"
#include "rfdevice_qxsw.h"

#ifndef FEATURE_RF_HAS_TP_CARDS
#include "rfdevice_elna.h"
#endif

#include "rfdevice_qcoupler.h"
#include "rfdevice_test_device.h"
#include "rfdevice_rxtx_common_adapter.h"
#ifndef FEATURE_GNSS_ONLY_NO_WWAN
#include "rfdevice_pa_common.h"
#include "rfdevice_asm_common.h" 
#include "rfdevice_asm_tuner.h" 
#include "rfdevice_coupler_common.h" 
#include "rfdevice_asm_common_xsw.h" 


#endif
#include "rfc_class.h"
#include "DALSys.h" /* DALSYS_BusyWait() */
#define FEATURE_RF_HAS_WTR3925
#define FEATURE_RF_HAS_WTR3950

extern "C"
{
#include "msg.h"
#include "rfcommon_nv_mm.h"

#ifdef FEATURE_CDMA
#if defined(FEATURE_CDMA1X) || defined(FEATURE_HDR)
#include "rfdevice_trx_cdma_rx_adapter.h"
#include "rfdevice_trx_cdma_tx_adapter.h"
#ifdef FEATURE_RF_HAS_WTR3925
#include "wtr3925_cdma_device_intf.h"
#endif //FEATURE_RF_HAS_WTR3925
#include "wtr4905_cdma_device_intf.h"
#endif
#endif 

#ifdef FEATURE_GSM 
#ifdef FEATURE_RF_HAS_WTR3925
#include "wtr3925_gsm_device_intf.h"
#include "rfdevice_trx_gsm_rx_adapter.h"
#include "rfdevice_trx_gsm_tx_adapter.h"
#endif //FEATURE_RF_HAS_WTR3925
#include "wtr4905_gsm_device_intf.h"
#endif 

#ifdef FEATURE_WCDMA
#include "rfdevice_trx_wcdma_rx_adapter.h"
#include "rfdevice_trx_wcdma_tx_adapter.h"
#ifdef FEATURE_RF_HAS_WTR3925
#include "wtr3925_wcdma_device_intf.h"
#include "wtr3925_common_device_intf.h"
#include "wtr3925_physical_device.h"
#endif  //FEATURE_RF_HAS_WTR3925
#include "wtr4905_wcdma_device_intf.h"
#include "wtr4905_common_device_intf.h"
#include "wtr4905_physical_device.h"
#endif

#ifdef FEATURE_LTE
#include "rfdevice_trx_lte_rx_adapter.h"
#include "rfdevice_trx_lte_tx_adapter.h"
#ifdef FEATURE_RF_HAS_WTR3950
#include "wtr3950_common_device_intf.h"
#include "wtr3950_physical_device.h"
#include "wtr3950_lte_device_intf.h"
#endif /* #ifdef FEATURE_RF_HAS_WTR3950 */
#ifdef FEATURE_RF_HAS_WTR3925
#include "wtr3925_lte_device_intf.h"
#endif /* #ifdef FEATURE_RF_HAS_WTR3925 */
#include "wtr4905_lte_device_intf.h"
#endif /* #ifdef FEATURE_LTE */

#ifdef FEATURE_TDSCDMA
#include "rfdevice_trx_tdscdma_rx_adapter.h"
#include "rfdevice_trx_tdscdma_tx_adapter.h"

#ifdef FEATURE_RF_HAS_WTR3925
#include "wtr3925_tdscdma_device_intf.h"
#endif /* #ifdef FEATURE_RF_HAS_WTR3925 */
    
#include "wtr4905_tdscdma_device_intf.h"
    
#endif 
}
#ifdef FEATURE_CGPS
#ifdef FEATURE_RF_HAS_WTR3925
#include "wtr3925_gnss_device_intf.h"
#endif //FEATURE_RF_HAS_WTR3925
#include "wtr4905_gnss_device_intf.h"
#endif 

#ifdef FEATURE_RF_HAS_QFE2340
#include "qfe2340_typedef.h"
#include "qfe2340_pa.h"
#include "qfe2340_asm.h"
#include "qfe2340_typedef_ag.h"
#include "qfe2340v3p0_pa.h"
#include "qfe2340v3p0_asm.h"
#include "qfe2340v3p0_typedef_ag.h"
#endif

#include "rfdevice_rxtx_common_class.h"
#include "rfc_cmn_transceiver_hdet_adapter.h"

#ifdef FEATURE_RF_HAS_QTUNER
#include "tp_antenna_tuner_physical_device.h" 



#ifdef FEATURE_RF_ASDIV
#include "rfcommon_asdiv_tuner_manager.h"
#endif
#include "qfe2550_physical_device.h"
extern "C"
{
  #include "rfdevice_antenna_tuner_intf.h"
  #include "rfdevice_hdet_cmn_intf.h"
}
#endif /*FEATURE_RF_HAS_QTUNER */

/*Physical Device includes  -- QFE devices*/
#include "rfdevice_papm_physical_device.h"
#include "qfe1035_physical_device_ag.h"
#include "qfe1040_physical_device_ag.h"
#include "qfe1045_physical_device_ag.h"
#include "qfe2340_physical_device_ag.h"
#include "qfe3320_physical_device_ag.h"
#include "qfe3335_physical_device_ag.h"
#include "qfe3345_physical_device_ag.h"
#include "qfe4335_physical_device_ag.h"
#include "qfe4345_physical_device_ag.h"
#include "qfe4320_physical_device_ag.h"
#include "qfe2340fc_physical_device_ag.h"
#include "qfe3340fc_physical_device.h"
#include "qfe2082fc_physical_device_ag.h"
#ifndef FEATURE_RF_HAS_TP_CARDS
#include "qfe4455fc_physical_device.h"
#include "qfe4465fc_physical_device.h"
#include "qfe2080fc_physical_device.h"
#include "qfe2081fc_physical_device.h"
#endif

/*Physical Device includes  -- Third party devices*/
#include "rfdevice_physical_third_party.h"

#ifdef FEATURE_RF_ASDIV
/*----------------------------------------------------------------------------*/
/*!
  @brief
  Device factory handler for creating an ASD Tuner Manager instance
 
  @details
  
  @param cfg
  Device config settings like manafacturer id, product id, slave id, group id
  etc needed to create this device. This information is expected from RFC. 
 
  @return rfdevice_class*
  Returns instance of rfdevice_class
*/
rfdevice_class* 
rf_device_factory_create_asd_tuner_device ( void )
{
  rfdevice_class *instance = NULL;
  
  /* Instantiate an ASD Tuner Manager */  
  instance = create_rfdevice_asd_tuner_manager(); 

  return instance; 
} /* rf_device_facotry_create_asd_tuner_device */
#endif

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Device factory handler to create TRX HDET adapter instance
 
  @details
 
  @param cfg
  Device config settings like manafacturer id, product id, slave id, group id
  etc needed to create this device. This information is expected from RFC.
 
  @return rfdevice_class*
  Returns instance of rfdevice_class
*/
static rfdevice_class* 
rf_device_factory_create_trx_hdet_adapter_device
( 
  rfc_device_cfg_info_type* cfg 
)
{
  rfdevice_class *instance = NULL;
  rfdevice_class *hdet_device = NULL;
  rfc_intf *rfc_cmn = rfc_intf::get_instance();
  rfdevice_rxtx_common_class* trx_ptr = NULL;

  RF_MSG_1( RF_MED, "rf_device_factory_create_trx_hdet_adapter_device() called:"
                    " dev_instance = rf_asic_id = %d", cfg->rf_asic_id);

  hdet_device = rfc_cmn->get_cmn_rf_device_object( RFDEVICE_HDET,
                                                   cfg->rf_asic_id);
  if( hdet_device != NULL)
  {
    RF_MSG( RF_MED, "TRX HDET adapter obj NOT created since "
                    "RFDEVICE_HDET obj has already been created");
    return NULL;
  }

  trx_ptr = (rfdevice_rxtx_common_class *)rfc_cmn->
      get_cmn_rf_device_object( cfg->associated_rf_device_type,   // RFDEVICE_TRANSCEIVER
                                (uint8)cfg->associated_rf_asic_id /* trx_instance */);

  if( trx_ptr != NULL )
  {
    if ( trx_ptr->rfdevice_id_get() == WTR4905 )
    {
      instance = wtr4905_common_create_hdet_device( trx_ptr );
    }
    else
    {
      instance = new rfc_cmn_transceiver_hdet_adapter(
                                      trx_ptr->get_instance());
    }
    RF_MSG( RF_MED, "TRX HDET adapter obj created since "
                    "RFDEVICE_HDET obj has NOT been created");
  }
  else
  {
    RF_MSG( RF_ERROR, "rf_device_factory_create_trx_hdet_adapter_device:"
                      " NULL trx_ptr. TRX HDET adapter obj NOT created");
  }
  return instance;
}


/*----------------------------------------------------------------------------*/
/*!
  @brief
  Programs the device's USID based on the device configuration.

  @details
  This is the external API to the RFC for programming device ID. This only applies
  to RFFE devices. Before creating the device objects, program each device's USID.
 
  @param cfg
  Device configuration settings of the device which needs to be created. Typically, this
  configuration should be autogenerated in RF card specific autogen file.
 
  @return TRUE if device id programming is successful else FALSE.
*/
boolean rf_device_factory_program_device_id(rfc_phy_device_info_type* cfg)
{
  boolean status = TRUE;  


  /*NULL pointer check*/
  if (cfg == NULL)
  {
    RF_MSG( RF_ERROR, "rf_device_factory_program_device_id  failed !  cfg structure is NULL");
    return FALSE;
  }

 /*Program the device USIDs for each physical device as per the phy cfg structure supplied*/
  switch(cfg->rf_device_id) 
  {

  case GEN_PA:
    status = rfdevice_pa_program_device_id_phy(cfg);
    if (status == FALSE)
    {
      RF_MSG_5( RF_ERROR, 
                "RFC rf_device_factory ERROR with GEN_PA Physical device ID %d instance %d M_ID %d P_ID %d P_REV %d."
                "Cannot use RFC. Ensure this information is correct and that respective device AG/factory supports the combo.", 
                cfg->rf_device_id, cfg->phy_dev_instance, cfg->manufacturer_id, cfg->product_id, cfg->product_rev );
    }
    break;

  case GEN_DEVICE:
  case GEN_ASM:
  case GEN_LNA:
  case GEN_COUPLER:
  case QFE2550:
  case TP_TUNER:
  case QFE1101:
  case QFE1100:
  case QFE2101:
  case QFE3100:
    status = rffe_program_assigned_usid(cfg);
    if (status == FALSE)
    {
      RF_MSG_5( RF_ERROR, 
                "RFC rf_device_factory ERROR with RFFE Physical device ID %d instance %d M_ID %d P_ID %d P_REV %d."
                "Cannot use RFC. Ensure this information is correct and that respective device AG/factory supports the combo.",
                cfg->rf_device_id, cfg->phy_dev_instance, cfg->manufacturer_id, cfg->product_id, cfg->product_rev );
    }
  break;

/* Buffer device support*/
  case TP_BUFFER:
    status = rffe_program_assigned_usid(cfg);
    status &= program_usid_list(cfg);
    if (status == FALSE)
    {
      RF_MSG_5( RF_ERROR, 
                "RFC rf_device_factory ERROR with TP_BUFFER Physical device ID %d instance %d M_ID %d P_ID %d P_REV %d."
                "Cannot use RFC. Ensure this information is correct and that respective device AG/factory supports the combo.",
                cfg->rf_device_id, cfg->phy_dev_instance, cfg->manufacturer_id, cfg->product_id, cfg->product_rev );
    }
  break;     

  case QFE2720:
  case QFE2340:
  case QFE1035:
  case QFE1040:
  case QFE1045:
  case QFE3335:
  case QFE3345:
  case QFE3335_ET:
  case QFE3345_ET:  
  case QFE3320:
  case QFE4335:
  case QFE4345:  
  case QFE4455FC:
  case QFE4465FC:
  case QFE2340FC:
  case QFE2080FC:
  case QFE2081FC:
  case QFE2082FC:
    status = rffe_program_assigned_usid_ext(cfg);
    if (status == FALSE)
    {
      RF_MSG_5( RF_ERROR, 
                "RFC rf_device_factory ERROR with QFE Physical device ID %d instance %d M_ID %d P_ID %d P_REV %d."
                "Cannot use RFC. Ensure this information is correct and that respective device AG/factory supports the combo.",
                cfg->rf_device_id, cfg->phy_dev_instance, cfg->manufacturer_id, cfg->product_id, cfg->product_rev );
    }
    break;
  case QFE3340FC:
    status = rffe_program_assigned_usid_ext_product_id(cfg);
    break;
  default:
    RF_MSG_1(RF_ERROR, "RFC rf_device_factory WARNING RF device ID %d not supported. Should not call this API with unsupported devices.", cfg->rf_device_id);
    //status = FALSE; DO NOT RETURN FALSE, API IS CALLED EVEN FOR OTHER DEVICES
    break;
  }

  return status;
}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Creates tuner manager based on the tuner instance provided

  @details
  This is a temporary function which would create a tuner manager based on the
  tuner device provided. This API needs to be removed when the tuner manager is
  part RFC AG.

  
  @param tuner_obj
  Instance of the tuner object
 
  @return rfdevice_class*
  Returns an instance of rfdevice_class (super class of the devices) which is to be stored
  by RFC.
*/
rfdevice_class* 
rf_device_factory_create_tuner_manager
(
  rfdevice_class* device_obj,
   rfcommon_atuner_manager_factory_type manager_type
)
{
  rfcommon_atuner_manager_factory *tuner_mgr_factory = NULL;
  rfdevice_class *tuner_mgr_obj = NULL;
  
  if(device_obj == NULL)
  {
    RF_MSG( RF_ERROR, "rf_device_factory_create_tuner_manager() failed as"
                      " NULL tuner device provided!");
    return tuner_mgr_obj;
  }

  /* Create the instance for Tuner mananger factory */
  tuner_mgr_factory = new rfcommon_atuner_manager_factory (  );

  /* Check if tuner_mgr_factory is valid*/
  if(tuner_mgr_factory != NULL)
  {
    /*Using the tuner manager factory, we can create the tuner 
      manager of interest*/
    /*!Note: By default creating AOL tuner manager for now*/
    tuner_mgr_obj = tuner_mgr_factory->create_atuner_manager( manager_type, 
                                                                 device_obj);
  }
  else
  {
    RF_MSG( RF_ERROR, "rf_device_factory_create_tuner_manager() failed as"
                      "Tuner manager factory instance cannot be created!");
  }

  /*Tuner manager factory*/
  delete tuner_mgr_factory;

  /*Return Tuner manager created */
  return tuner_mgr_obj;
  
} /*rf_device_factory_create_tuner_manager*/


/*!
  @brief
  Creates device instance based on the device configuration provided.

  @details
  This is the external API to the RFC for creating device instances. The device
  factory instantiates the common and tech specific instances for a specific device
  based on the device configuration parameter passed in. Common instances will have
  references to tech specific instances. Every device type (ex: WTR1605,
  WTR2605) is allowed one handle. All tech specific instance creation and header file
  includes need to be featurized.
 
  This API will only dispatch the call to device specific create handler. All logic for
  creating device needs to be maintained in the device specific handler.
  
  @param cfg
  Device configuration settings of the device which needs to be created. Typically, this
  configuration should be autogenerated in RF card specific autogen file.
 
 
  @return
  Returns an instance of rfdevice_class (super class of the devices) which is to be stored
  by RFC.
*/
rfdevice_class* 
rf_device_factory_create_device
(
  rfc_device_cfg_info_type* cfg
)
{  
  rfdevice_class* instance = NULL;
  //*NULL Pointer Checks*/
  if (cfg == NULL)
  {     
    /* crashing phone as critical params missing */   
    ERR_FATAL("rf_device_factory_create_device creation failed. cfg == NULL", 0, 0, 0);
    
  }

  switch(cfg->rf_device_id) 
  {

    case TRX_HDET:
      instance = rf_device_factory_create_trx_hdet_adapter_device (cfg);
      break;

   default:
      break;
  }

  return instance;
}

/*!
  @brief
  Factory method to create instances of 3rd party logical device instances

  @details
  Third party devices could have multiple logical components.
  This API creates the logical components  for 3rd party devices
  and associates it with its parent physical device.

  @param rfdevice_physical_third_party_p
  Pointer to the parent phsyical device 
 
  @param logical_device_cfg
  Pointer to the logical device cfg of the 3rd party device
 
  @return
  Pointer to the logical device object of the 3rd party device, or NULL if no device could
  be created.

  @retval NULL
  The requested device could not be created successfully.
*/
rfdevice_logical_component* create_gen_device_object(
                                                        rfdevice_physical_device     *rfdevice_physical_third_party_p,
                                                        rfc_logical_device_info_type      *logical_device_cfg
                                                    )
{

  if ( (NULL == rfdevice_physical_third_party_p) || (NULL == logical_device_cfg))
  {
     RF_MSG( RF_ERROR, " create_gen_device_object() -  NULL pointer detected!" );
     return NULL;
  }

  switch(logical_device_cfg->rf_device_id) 
  {
  
   case GEN_ASM:
     return (create_gen_asm_object( rfdevice_physical_third_party_p , logical_device_cfg ) );

   case GEN_PA:
     return (create_gen_pa_object( rfdevice_physical_third_party_p , logical_device_cfg ) );

   case GEN_COUPLER:
     return (create_gen_coupler_object( rfdevice_physical_third_party_p , logical_device_cfg ) );
 
   case GEN_ASM_XSW:
     return (create_gen_asm_common_xsw_object( rfdevice_physical_third_party_p , logical_device_cfg ) );
 
   case GEN_ASM_GRFC_TUNER:
     return (create_gen_asm_tuner_object( rfdevice_physical_third_party_p , logical_device_cfg ) );
     
#ifndef FEATURE_RF_HAS_TP_CARDS
   case GEN_LNA:
     return (create_gen_elna_object( rfdevice_physical_third_party_p , logical_device_cfg ) );
#endif

   default:
      RF_MSG_1(RF_ERROR, "RFC rf_device_factory ERROR GEN device ID %d not supported.", logical_device_cfg->rf_device_id);
     return NULL;
  }

}

/*!
  @brief
  Factory method to create instances of physical device interfaces.

  @details
  The physical device object represents the hardware device in the system.
  This method will construct a software object that represents the physical
  device, and returns a pointer to the physical device abstraction.

  @param cfg
  Pointer to a device configuration structure that describes what type of
  device to try and create.

  @return
  Pointer to the physical device that was created, or NULL if no device could
  be created.

  @retval NULL
  The requested device could not be created successfully.
*/
rfdevice_physical_device*
rf_device_factory_create_phys_device
(
  rfc_phy_device_info_type* cfg
)
{
  /*NULL Pointer Checks*/
  if (cfg == NULL)
  {         
    /* crashing phone as critical params missing */  
    ERR_FATAL("rf_device_factory_create_phys_device creation failed. cfg == NULL", 0, 0, 0);
  }

  /* Execute RFFE Scan only for RFFE devices */
  if ( cfg->rf_device_comm_protocol == RFDEVICE_COMM_PROTO_RFFE) 
  {
    if ( !rffe_scan(cfg) )
    {
      RF_MSG_1( RF_ERROR, "rf_device_factory_create_phys_device: RFC ERROR Device %d not found",
                                                                cfg->rf_device_id );
      return NULL;
    }
  }
  switch(cfg->rf_device_id) 
  {
  case TEST_DEVICE:
    return new test_device_physical_device(cfg);

  #ifdef FEATURE_RF_HAS_WTR3925
  case WTR3925:
    return new wtr3925_physical_device(cfg);
  #endif

  #ifdef FEATURE_RF_HAS_WTR3950
  case WTR3950:
    return new wtr3950_physical_device(cfg);
  #endif

  case WTR4905:
    return new wtr4905_physical_device(cfg);

  #ifdef FEATURE_RF_HAS_QTUNER

  case QFE2550:
    return new qfe2550_physical_device(cfg,FALSE);
    
  case TP_TUNER:
    return new tp_antenna_tuner_physical_device(cfg);   
    
  #endif /* FEATURE_RF_HAS_QTUNER */
    break;
  case QFE1035:
    return new qfe1035_physical_device(cfg);

  case QFE1040:
    return new qfe1040_physical_device(cfg);

  case QFE1045:
    return new qfe1045_physical_device(cfg);

  case QFE3335:
  case QFE3335_ET:
    return new qfe3335_physical_device(cfg);

  case QFE3345:
  case QFE3345_ET:
    return new qfe3345_physical_device(cfg);

  case QFE4335:
    return new qfe4335_physical_device(cfg);

  case QFE4345:
    return new qfe4345_physical_device(cfg);

  case QFE2340:
    return new qfe2340_physical_device(cfg);

  case QFE2340FC:
    return new qfe2340fc_physical_device(cfg);

#ifndef FEATURE_RF_HAS_TP_CARDS
  case QFE4455FC:
    return new qfe4455fc_physical_device(cfg);
    
  case QFE4465FC:
    return new qfe4465fc_physical_device(cfg);
	
  case QFE2080FC:
    return new qfe2080fc_physical_device(cfg);
    
  case QFE2081FC:
    return new qfe2081fc_physical_device(cfg);
#endif

  case QFE3340FC:
    return new qfe3340fc_physical_device(cfg);

  case QFE2082FC:
    return new qfe2082fc_physical_device(cfg);

  case QFE1100:
  case QFE1101:
  case QFE2101:  
  case QFE3100:
    return new papm_physical_device(cfg);

  case QFE3320:
    return new qfe3320_physical_device(cfg);

  case QFE4320:
    return new qfe4320_physical_device(cfg);

  case GEN_DEVICE:
  case GEN_PA:
  case GEN_ASM:
  case GEN_COUPLER:
  case GEN_LNA:
  case GEN_ASM_GRFC_TUNER:
    return new rfdevice_physical_third_party(cfg);
    
  default:
    RF_MSG_1( RF_ERROR, "rf_device_factory_create_phys_device RFC ERROR RF device ID %d not supported.", cfg->rf_device_id);
    return NULL;
  }
}


/*!
  @brief

  @details
*/
boolean
rf_device_factory_create_gnss_device
( 
  rfc_device_info_type* cfg
)
{
  boolean status = TRUE;
  /*NULL Pointer Checks*/
  if (cfg == NULL)
  {
    /* crashing phone as critical params missing */   
    ERR_FATAL("rf_device_factory_create_gnss_device creation failed. cfg == NULL", 
              0, 0, 0);
    
  }
#ifdef FEATURE_CGPS
  switch( cfg->rf_asic_info[0].device_id )
  {
  case WTR3925:
   status &= 
     rfdevice_wtr3925_gnss_set_instance_id( cfg->rf_asic_info[0].instance );
   break;

  case WTR4905:
   status &= 
     rfdevice_wtr4905_gnss_set_instance_id( cfg->rf_asic_info[0].instance );
   break;

  default:
    RF_MSG_1( RF_ERROR, "rf_device_factory_create_gnss_device. RFC ERROR Unsupported device_id %d ",
              cfg->rf_asic_info[0].device_id );
    status = FALSE;
    break;

  } /* switch( cfg->rf_asic_info[0].device_id ) */

  if(status == FALSE)
  {
    RF_MSG_2( RF_ERROR, "rf_device_factory_create_gnss_device Failed!"
                        "device_id %d, instance %d ",
              cfg->rf_asic_info[0].device_id,
              cfg->rf_asic_info[0].instance );
  }
#endif
  return status;
} /* rf_device_factory_create_gnss_device */
