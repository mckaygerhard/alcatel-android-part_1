/*!
  @file
  rfc_wtr3925_3dlca_apac11_1100_cmn_device_remap.cpp
 
  @brief
  Manually generated RFC file to remap old device cal params with new RFC physical device mappings.
*/

/*==============================================================================

  Copyright (c) 2015 -  Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfc_thor/rf_card/rfc_wtr3925_3dlca_apac11_1100/common/src/rfc_wtr3925_3dlca_apac11_1100_cmn_device_remap.cpp#1 $
  
when       who     what, where, why
--------   ---     -------------------------------------------------------------
06/12/15   zhw     Set remapper_present flag to TRUE
04/10/15   Saul     Initial version

==============================================================================*/
#include "comdef.h"

#include "rfc_wtr3925_3dlca_apac11_1100_cmn_ag.h" 

boolean
rfc_wtr3925_3dlca_apac11_1100_cmn_cdma_remapped_device_info
(
   rfc_cal_device_remap_info_type *source,
   rfc_cal_device_remap_info_type *remapped
);

boolean
rfc_wtr3925_3dlca_apac11_1100_cmn_gsm_remapped_device_info
(
   rfc_cal_device_remap_info_type *source,
   rfc_cal_device_remap_info_type *remapped
);

boolean
rfc_wtr3925_3dlca_apac11_1100_cmn_lte_remapped_device_info
(
   rfc_cal_device_remap_info_type *source,
   rfc_cal_device_remap_info_type *remapped
);

boolean
rfc_wtr3925_3dlca_apac11_1100_cmn_tds_remapped_device_info
(
   rfc_cal_device_remap_info_type *source,
   rfc_cal_device_remap_info_type *remapped
);

boolean
rfc_wtr3925_3dlca_apac11_1100_cmn_wcdma_remapped_device_info
(
   rfc_cal_device_remap_info_type *source,
   rfc_cal_device_remap_info_type *remapped
);

boolean rfc_wtr3925_3dlca_apac11_1100_cmn_ag::rfc_get_remapped_device_info
( 
 rfc_cal_device_remap_info_type *source, 
 rfc_cal_device_remap_info_type *remapped 
)
{
  boolean api_status = TRUE;

  remapped->remapper_present = TRUE;

  switch ( source->tech )
  {
  case RFCOM_1X_MODE:
  case RFCOM_1XEVDO_MODE:
    api_status &= rfc_wtr3925_3dlca_apac11_1100_cmn_cdma_remapped_device_info( source, remapped );
    break;

  case RFCOM_GSM_MODE:
    api_status &= rfc_wtr3925_3dlca_apac11_1100_cmn_gsm_remapped_device_info( source, remapped );
    break;

  case RFCOM_LTE_MODE:
    api_status &= rfc_wtr3925_3dlca_apac11_1100_cmn_lte_remapped_device_info( source, remapped );
    break;

  case RFCOM_TDSCDMA_MODE:
    api_status &= rfc_wtr3925_3dlca_apac11_1100_cmn_tds_remapped_device_info( source, remapped );
    break;

  case RFCOM_WCDMA_MODE:
    api_status &= rfc_wtr3925_3dlca_apac11_1100_cmn_wcdma_remapped_device_info( source, remapped );
    break;

  default:
    api_status = FALSE;
    RF_MSG_1( RF_ERROR, "rfc_get_remapped_device_info - Mode %d not supported.", source->tech );
    break;
  }

  return api_status;
}

boolean
rfc_wtr3925_3dlca_apac11_1100_cmn_cdma_remapped_device_info
(
   rfc_cal_device_remap_info_type *source,
   rfc_cal_device_remap_info_type *remapped
)
{
  boolean api_status = TRUE;

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_cdma_bc0_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_cdma_bc0_device_info */
  if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC0 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_cdma_bc0_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_cdma_bc0_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC0 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }
#if 0
  ?/* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_cdma_bc0_device_info TO ? */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC0 ) )
  { ? }
#endif
  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_cdma_bc0_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_cdma_bc0_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFM_CDMA_BC0 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_cdma_bc0_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_7_cdma_bc0_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC0 ) )
  { remapped->device = RFM_DEVICE_7; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_cdma_bc1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_cdma_bc1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC1 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }
  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_cdma_bc1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_cdma_bc1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC1 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }
#if 0
  ?/* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_cdma_bc1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_cdma_bc1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC1 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }
#endif
  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_cdma_bc1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_cdma_bc1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC1 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_cdma_bc6_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_cdma_bc6_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC6 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_cdma_bc6_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_cdma_bc6_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC6 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }
#if 0
  ?/* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_cdma_bc6_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_cdma_bc6_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC6 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }
#endif
  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_cdma_bc6_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_cdma_bc6_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC6 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_cdma_bc10_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_cdma_bc10_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC10 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_cdma_bc10_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_cdma_bc10_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC10 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }
#if 0
  ?/* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_cdma_bc10_device_info TO ? */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC10 ) )
  { ? }

  ?/* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_cdma_bc10_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_cdma_bc10_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFM_CDMA_BC10 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }
#endif
  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_cdma_bc10_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_7_cdma_bc10_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFM_CDMA_BC10 ) )
  { remapped->device = RFM_DEVICE_7; remapped->alt_path == 0; }

  /* Must be a new case that is not accounted for. Need to add above handling for new case. */
  else
  { api_status = FALSE; }

  return api_status;
} /* rfc_wtr3925_3dlca_apac11_1100_cmn_cdma_remapped_device_info() */

boolean
rfc_wtr3925_3dlca_apac11_1100_cmn_gsm_remapped_device_info
(
   rfc_cal_device_remap_info_type *source,
   rfc_cal_device_remap_info_type *remapped
)
{
  boolean api_status = TRUE;

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_gsm_g850_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_gsm_g850_device_info */
  if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM850 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_gsm_g850_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_gsm_g850_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM850 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_gsm_g850_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_gsm_g850_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM850 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_gsm_g900_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_gsm_g900_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM900 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_gsm_g900_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_gsm_g900_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM900 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_gsm_g900_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_gsm_g900_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM900 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_gsm_g1800_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_gsm_g1800_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM1800 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_gsm_g1800_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_gsm_g1800_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM1800 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_gsm_g1800_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_gsm_g1800_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM1800 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_gsm_g1900_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_gsm_g1900_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM1900 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_gsm_g1900_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_gsm_g1900_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM1900 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_gsm_g1900_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_gsm_g1900_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_GSM1900 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* Must be a new case that is not accounted for. Need to add above handling for new case. */
  else
  { api_status = FALSE; }

  return api_status;
} /* rfc_wtr3925_3dlca_apac11_1100_cmn_gsm_remapped_device_info() */

boolean
rfc_wtr3925_3dlca_apac11_1100_cmn_lte_remapped_device_info
(
   rfc_cal_device_remap_info_type *source,
   rfc_cal_device_remap_info_type *remapped
)
{
  boolean api_status = TRUE;

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b1_device_info */
  if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B1 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B1 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B1 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B1 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B1 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B1 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B1 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b1_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b1_alt_path_1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B1 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b1_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b1_alt_path_1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B1 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B2 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B2 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B2 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B2 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B2 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B2 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B2 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B3 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B3 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B3 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B3 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B3 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B3 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B3 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B4 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B4 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B4 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B4 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B4 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B4 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B4 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b5_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b5_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B5 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b5_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b5_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B5 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b5_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_7_lte_b5_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B5 ) )
  { remapped->device = RFM_DEVICE_7; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b5_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b5_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B5 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b5_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b5_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B5 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b7_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b7_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B7 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b7_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b7_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B7 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b7_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b7_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B7 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b7_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b7_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B7 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b7_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b7_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B7 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b7_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b7_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B7 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b7_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b7_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B7 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b7_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b7_alt_path_1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B7 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b7_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b7_alt_path_1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B7 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b8_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b8_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B8 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b8_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b8_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B8 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b8_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_7_lte_b8_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B8 ) )
  { remapped->device = RFM_DEVICE_7; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b8_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b8_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B8 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b8_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b8_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B8 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B11 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B11 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B11 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B11 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B11 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B11 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B11 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b17_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b17_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B17 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b17_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b17_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B17 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b17_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_7_lte_b17_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B17 ) )
  { remapped->device = RFM_DEVICE_7; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b17_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b17_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B17 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b17_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b17_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B17 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b18_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b18_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B18 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b18_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b18_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B18 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b18_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b18_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B18 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b18_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b18_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B18 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b18_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b18_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B18 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b18_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b18_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B18 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b18_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b18_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B18 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b26_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b26_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B26 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b26_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b26_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B26 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b26_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_7_lte_b26_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B26 ) )
  { remapped->device = RFM_DEVICE_7; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b26_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b26_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B26 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b26_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b26_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B26 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b28_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b28_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B28 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b28_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b28_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B28 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b28_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_7_lte_b28_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B28 ) )
  { remapped->device = RFM_DEVICE_7; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b28_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b28_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B28 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b28_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b28_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B28 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b28_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b28_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B28_B ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b28_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b28_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B28_B ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b28_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_7_lte_b28_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B28_B ) )
  { remapped->device = RFM_DEVICE_7; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b28_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_lte_b28_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B28_B ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b28_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_lte_b28_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B28_B ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b38_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b38_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B38 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b38_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b38_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B38 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b38_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b38_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B38 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_4_lte_b38_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b38_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( ( source->device == RFM_DEVICE_2 )||  ( source->device == RFM_DEVICE_4 )) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B38 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_5_lte_b38_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b38_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( ( source->device == RFM_DEVICE_3 )||  ( source->device == RFM_DEVICE_5 )) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B38 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b39_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b39_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B39 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b39_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b39_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B39 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b39_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b39_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B39 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_4_lte_b39_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b39_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( ( source->device == RFM_DEVICE_2 )||  ( source->device == RFM_DEVICE_4 )) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B39 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_5_lte_b39_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b39_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( ( source->device == RFM_DEVICE_3 )||  ( source->device == RFM_DEVICE_5 )) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B39 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b40_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b40_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B40 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b40_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b40_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B40 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b40_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b40_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B40 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b40_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b40_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B40 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b40_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b40_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B40 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b40_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b40_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B40 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b40_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b40_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B40 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b40_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b40_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B40 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b40_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b40_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B40 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b41_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b41_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b41_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b41_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b41_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b41_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b41_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b41_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b41_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b41_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b41_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b41_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B41 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b41_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b41_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B41 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b41_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b41_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b41_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b41_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b41_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b41_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_B ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b41_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b41_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_B ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b41_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b41_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_B ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b41_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b41_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_B ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b41_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b41_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_B ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b41_b_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b41_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B41_B ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b41_b_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b41_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B41_B ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b41_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b41_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_B ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b41_b_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b41_b_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_B ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_lte_b41_c_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b41_c_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_C ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_lte_b41_c_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b41_c_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_C ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_lte_b41_c_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_lte_b41_c_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_C ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b41_c_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b41_c_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_C ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b41_c_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b41_c_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_C ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_lte_b41_c_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_lte_b41_c_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B41_C ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_lte_b41_c_alt_path_1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_lte_b41_c_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 1 ) && ( source->band == (int)RFCOM_BAND_LTE_B41_C ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx4_lte_b41_c_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_lte_b41_c_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_4 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_C ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx5_lte_b41_c_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_lte_b41_c_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_5 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_LTE_B41_C ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* Must be a new case that is not accounted for. Need to add above handling for new case. */
  else
  { api_status = FALSE; }

  return api_status;
} /* rfc_wtr3925_3dlca_apac11_1100_cmn_lte_remapped_device_info() */

boolean
rfc_wtr3925_3dlca_apac11_1100_cmn_tds_remapped_device_info
(
   rfc_cal_device_remap_info_type *source,
   rfc_cal_device_remap_info_type *remapped
)
{
  boolean api_status = TRUE;

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_tdscdma_b34_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_tdscdma_b34_device_info */
  if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_TDSCDMA_B34 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_tdscdma_b34_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_tdscdma_b34_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_TDSCDMA_B34 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_tdscdma_b34_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_tdscdma_b34_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_TDSCDMA_B34 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_tdscdma_b39_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_tdscdma_b39_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_TDSCDMA_B39 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_tdscdma_b39_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_tdscdma_b39_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_TDSCDMA_B39 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_tdscdma_b39_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_tdscdma_b39_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_TDSCDMA_B39 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* Must be a new case that is not accounted for. Need to add above handling for new case. */
  else
  { api_status = FALSE; }

  return api_status;
} /* rfc_wtr3925_3dlca_apac11_1100_cmn_tds_remapped_device_info() */

boolean
rfc_wtr3925_3dlca_apac11_1100_cmn_wcdma_remapped_device_info
(
   rfc_cal_device_remap_info_type *source,
   rfc_cal_device_remap_info_type *remapped
)
{
  boolean api_status = TRUE;
  
  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_wcdma_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_wcdma_b1_device_info */
  if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_IMT ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_wcdma_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_wcdma_b1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_IMT ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_wcdma_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_wcdma_b1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_IMT ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_wcdma_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_wcdma_b1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_IMT ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_wcdma_b1_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_wcdma_b1_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_IMT ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_wcdma_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_wcdma_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_1900 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_wcdma_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_wcdma_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_1900 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_wcdma_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_wcdma_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_1900 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_wcdma_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_wcdma_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_1900 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_wcdma_b2_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_wcdma_b2_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_1900 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_wcdma_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_wcdma_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC3 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_wcdma_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_wcdma_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC3 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_wcdma_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_wcdma_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC3 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_wcdma_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_wcdma_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC3 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_wcdma_b3_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_wcdma_b3_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC3 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_wcdma_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_wcdma_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC4 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_wcdma_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_wcdma_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC4 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_wcdma_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_wcdma_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC4 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_wcdma_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_wcdma_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC4 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_wcdma_b4_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_wcdma_b4_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC4 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_wcdma_b5_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_wcdma_b5_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_800 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_wcdma_b5_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_wcdma_b5_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_800 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_wcdma_b5_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_7_wcdma_b5_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_800 ) )
  { remapped->device = RFM_DEVICE_7; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_wcdma_b5_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_wcdma_b5_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_800 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_wcdma_b5_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_wcdma_b5_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_800 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_wcdma_b8_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_wcdma_b8_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC8 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_wcdma_b8_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_wcdma_b8_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC8 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_wcdma_b8_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_7_wcdma_b8_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC8 ) )
  { remapped->device = RFM_DEVICE_7; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_wcdma_b8_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_4_wcdma_b8_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC8 ) )
  { remapped->device = RFM_DEVICE_4; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_wcdma_b8_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_5_wcdma_b8_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC8 ) )
  { remapped->device = RFM_DEVICE_5; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx0_wcdma_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_0_wcdma_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC11 ) )
  { remapped->device = RFM_DEVICE_0; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx1_wcdma_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_1_wcdma_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_1 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC11 ) )
  { remapped->device = RFM_DEVICE_1; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_tx0_wcdma_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_tx_on_rfm_device_6_wcdma_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_TX ) && ( source->device == RFM_DEVICE_0 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC11 ) )
  { remapped->device = RFM_DEVICE_6; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx2_wcdma_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_2_wcdma_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_2 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC11 ) )
  { remapped->device = RFM_DEVICE_2; remapped->alt_path = 0; }

  /* REMAP rf_card_wtr3925_3dlca_apac11_1100_rx3_wcdma_b11_device_info TO rf_card_wtr3925_3dlca_apac11_1100_rx_on_rfm_device_3_wcdma_b11_device_info */
  else if ( ( source->rxtx == RFC_CONFIG_RX ) && ( source->device == RFM_DEVICE_3 ) && ( source->alt_path == 0 /*Warning: not specified*/ ) && ( source->band == (int)RFCOM_BAND_BC11 ) )
  { remapped->device = RFM_DEVICE_3; remapped->alt_path = 0; }

  /* Must be a new case that is not accounted for. Need to add above handling for new case. */
  else
  { api_status = FALSE; }

  return api_status;
} /* rfc_wtr3925_3dlca_apac11_1100_cmn_wcdma_remapped_device_info() */
