
/*
WARNING: This file is auto-generated.

Generated using: rfc_autogen.exe
Generated from:  V5.20.780 of RFC_HWSWCD.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2015 Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfc_thor/rf_card/rfc_wtr3925_ssku_pamid/common/src/rfc_wtr3925_ssku_pamid_cmn_ag.cpp#1 $ 


=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include <stringl/stringl.h>
#include "rfc_msm_signal_info_ag.h" 
#include "rfc_wtr3925_ssku_pamid_cmn_ag.h" 
#include "DDITlmm.h" 
#include "rfc_common.h" 
#include "rfcommon_core_sbi.h" 

#ifdef FEATURE_GSM
#include "rfc_wtr3925_ssku_pamid_gsm_config_ag.h" 
#endif 

#ifdef FEATURE_WCDMA
#include "rfc_wtr3925_ssku_pamid_wcdma_config_ag.h" 
#endif 

#ifdef FEATURE_CDMA1X
#include "rfc_wtr3925_ssku_pamid_cdma_config_ag.h" 
#endif 

#ifdef FEATURE_LTE
#include "rfc_wtr3925_ssku_pamid_lte_config_ag.h" 
#endif 

#ifdef FEATURE_TDSCDMA
#include "rfc_wtr3925_ssku_pamid_tdscdma_config_ag.h" 
#endif 

#ifdef FEATURE_CGPS
#include "rfc_wtr3925_ssku_pamid_gnss_config_ag.h" 
#endif 



rfc_phy_device_info_type rfc_wtr3925_ssku_pamid_phy_devices_list[] = 
{
  { /*Device: WTR3925 */ 
    WTR3925, /* PHY_DEVICE_NAME */ 
    0, /* PHY_DEVICE_INSTANCE */ 
    RFC_NO_ALTERNATE_PART, /* PHY_DEVICE_ALT_PART_NUM_OF_INSTANCE */ 
    RFDEVICE_COMM_PROTO_RFFE, /* PHY_DEVICE_COMM_PROTOCOL */ 
    {    0,0 /* 0 not specified */,}, /* PHY_DEVICE_COMM_BUS */ 
    0x217, /* PHY_DEVICE_MANUFACTURER_ID */ 
    0xC0, /* PHY_DEVICE_PRODUCT_ID */ 
    0, /* PHY_DEVICE_PRODUCT_REV */ 
    0x1, /* DEFAULT USID RANGE START */
    0x1, /* DEFAULT USID RANGE END */
    0x1, /* PHY_DEVICE_ASSIGNED_USID */ 
    0 /*Warning: Not specified*/, /* RFFE_GROUP_ID */ 
    FALSE, /* INIT */ 
    RFC_TX_MODEM_CHAIN_0, /* ASSOCIATED_DAC */ 
  }, /* END - Device: WTR3925 */ 

  { /*Device: QFE3100 */ 
    QFE3100, /* PHY_DEVICE_NAME */ 
    1, /* PHY_DEVICE_INSTANCE */ 
    RFC_NO_ALTERNATE_PART, /* PHY_DEVICE_ALT_PART_NUM_OF_INSTANCE */ 
    RFDEVICE_COMM_PROTO_RFFE, /* PHY_DEVICE_COMM_PROTOCOL */ 
    {    1,0 /* 0 not specified */,}, /* PHY_DEVICE_COMM_BUS */ 
    0x217, /* PHY_DEVICE_MANUFACTURER_ID */ 
    0x32, /* PHY_DEVICE_PRODUCT_ID */ 
    0, /* PHY_DEVICE_PRODUCT_REV */ 
    0x4, /* DEFAULT USID RANGE START */
    0x4, /* DEFAULT USID RANGE END */
    0x4, /* PHY_DEVICE_ASSIGNED_USID */ 
    0 /*Warning: Not specified*/, /* RFFE_GROUP_ID */ 
    TRUE, /* INIT */ 
    RFC_INVALID_PARAM, /* ASSOCIATED_DAC */ 
  }, /* END - Device: QFE3100 */ 

  { /*Device: B11_21PA */ 
    GEN_PA /* B11_21PA */, /* PHY_DEVICE_NAME */ 
    2, /* PHY_DEVICE_INSTANCE */ 
    RFC_NO_ALTERNATE_PART, /* PHY_DEVICE_ALT_PART_NUM_OF_INSTANCE */ 
    RFDEVICE_COMM_PROTO_RFFE, /* PHY_DEVICE_COMM_PROTOCOL */ 
    {    1,0 /* 0 not specified */,}, /* PHY_DEVICE_COMM_BUS */ 
    0x134, /* PHY_DEVICE_MANUFACTURER_ID */ 
    0x15, /* PHY_DEVICE_PRODUCT_ID */ 
    ( 0 | RFC_SKIP_RFFE_DETECT_BIT_IND ), /* PHY_DEVICE_PRODUCT_REV */ 
    0xF, /* DEFAULT USID RANGE START */
    0xF, /* DEFAULT USID RANGE END */
    0x6, /* PHY_DEVICE_ASSIGNED_USID */ 
    0 /*Warning: Not specified*/, /* RFFE_GROUP_ID */ 
    FALSE, /* INIT */ 
    RFC_INVALID_PARAM, /* ASSOCIATED_DAC */ 
  }, /* END - Device: B11_21PA */ 

  { /*Device: QFE3340FC */ 
    QFE3340FC, /* PHY_DEVICE_NAME */ 
    3, /* PHY_DEVICE_INSTANCE */ 
    RFC_NO_ALTERNATE_PART, /* PHY_DEVICE_ALT_PART_NUM_OF_INSTANCE */ 
    RFDEVICE_COMM_PROTO_RFFE, /* PHY_DEVICE_COMM_PROTOCOL */ 
    {    1,0 /* 0 not specified */,}, /* PHY_DEVICE_COMM_BUS */ 
    0x217, /* PHY_DEVICE_MANUFACTURER_ID */ 
    0x124, /* PHY_DEVICE_PRODUCT_ID */ 
    0x02, /* PHY_DEVICE_PRODUCT_REV */ 
    0xF, /* DEFAULT USID RANGE START */
    0xF, /* DEFAULT USID RANGE END */
    0xF, /* PHY_DEVICE_ASSIGNED_USID */ 
    0 /*Warning: Not specified*/, /* RFFE_GROUP_ID */ 
    FALSE, /* INIT */ 
    RFC_INVALID_PARAM, /* ASSOCIATED_DAC */ 
  }, /* END - Device: QFE3340FC */ 

  { /*Device: QFE4465FC */ 
    QFE4465FC, /* PHY_DEVICE_NAME */ 
    4, /* PHY_DEVICE_INSTANCE */ 
    RFC_NO_ALTERNATE_PART, /* PHY_DEVICE_ALT_PART_NUM_OF_INSTANCE */ 
    RFDEVICE_COMM_PROTO_RFFE, /* PHY_DEVICE_COMM_PROTOCOL */ 
    {    1,0 /* 0 not specified */,}, /* PHY_DEVICE_COMM_BUS */ 
    0x217, /* PHY_DEVICE_MANUFACTURER_ID */ 
    0x47, /* PHY_DEVICE_PRODUCT_ID */ 
    0x00, /* PHY_DEVICE_PRODUCT_REV */ 
    0xC, /* DEFAULT USID RANGE START */
    0xC, /* DEFAULT USID RANGE END */
    0xC, /* PHY_DEVICE_ASSIGNED_USID */ 
    0 /*Warning: Not specified*/, /* RFFE_GROUP_ID */ 
    FALSE, /* INIT */ 
    RFC_INVALID_PARAM, /* ASSOCIATED_DAC */ 
  }, /* END - Device: QFE4465FC */ 

  { /*Device: QFE4455FC */ 
    QFE4455FC, /* PHY_DEVICE_NAME */ 
    5, /* PHY_DEVICE_INSTANCE */ 
    RFC_NO_ALTERNATE_PART, /* PHY_DEVICE_ALT_PART_NUM_OF_INSTANCE */ 
    RFDEVICE_COMM_PROTO_RFFE, /* PHY_DEVICE_COMM_PROTOCOL */ 
    {    3,0 /* 0 not specified */,}, /* PHY_DEVICE_COMM_BUS */ 
    0x217, /* PHY_DEVICE_MANUFACTURER_ID */ 
    0x46, /* PHY_DEVICE_PRODUCT_ID */ 
    0x00, /* PHY_DEVICE_PRODUCT_REV */ 
    0xC, /* DEFAULT USID RANGE START */
    0xC, /* DEFAULT USID RANGE END */
    0xC, /* PHY_DEVICE_ASSIGNED_USID */ 
    0 /*Warning: Not specified*/, /* RFFE_GROUP_ID */ 
    FALSE, /* INIT */ 
    RFC_INVALID_PARAM, /* ASSOCIATED_DAC */ 
  }, /* END - Device: QFE4455FC */ 

  { /*Device: QFE2082FC */ 
    QFE2082FC, /* PHY_DEVICE_NAME */ 
    6, /* PHY_DEVICE_INSTANCE */ 
    RFC_NO_ALTERNATE_PART, /* PHY_DEVICE_ALT_PART_NUM_OF_INSTANCE */ 
    RFDEVICE_COMM_PROTO_RFFE, /* PHY_DEVICE_COMM_PROTOCOL */ 
    {    1,0 /* 0 not specified */,}, /* PHY_DEVICE_COMM_BUS */ 
    0x217, /* PHY_DEVICE_MANUFACTURER_ID */ 
    0x16, /* PHY_DEVICE_PRODUCT_ID */ 
    0x00, /* PHY_DEVICE_PRODUCT_REV */ 
    0xA, /* DEFAULT USID RANGE START */
    0xA, /* DEFAULT USID RANGE END */
    0xA, /* PHY_DEVICE_ASSIGNED_USID */ 
    0 /*Warning: Not specified*/, /* RFFE_GROUP_ID */ 
    FALSE, /* INIT */ 
    RFC_INVALID_PARAM, /* ASSOCIATED_DAC */ 
  }, /* END - Device: QFE2082FC */ 

  { /*Device: QFE2081FC */ 
    QFE2081FC, /* PHY_DEVICE_NAME */ 
    7, /* PHY_DEVICE_INSTANCE */ 
    RFC_NO_ALTERNATE_PART, /* PHY_DEVICE_ALT_PART_NUM_OF_INSTANCE */ 
    RFDEVICE_COMM_PROTO_RFFE, /* PHY_DEVICE_COMM_PROTOCOL */ 
    {    1,0 /* 0 not specified */,}, /* PHY_DEVICE_COMM_BUS */ 
    0x217, /* PHY_DEVICE_MANUFACTURER_ID */ 
    0x15, /* PHY_DEVICE_PRODUCT_ID */ 
    0x00, /* PHY_DEVICE_PRODUCT_REV */ 
    0x9, /* DEFAULT USID RANGE START */
    0x9, /* DEFAULT USID RANGE END */
    0x9, /* PHY_DEVICE_ASSIGNED_USID */ 
    0 /*Warning: Not specified*/, /* RFFE_GROUP_ID */ 
    FALSE, /* INIT */ 
    RFC_INVALID_PARAM, /* ASSOCIATED_DAC */ 
  }, /* END - Device: QFE2081FC */ 

  { /*Device: QFE2080FC */ 
    QFE2080FC, /* PHY_DEVICE_NAME */ 
    8, /* PHY_DEVICE_INSTANCE */ 
    RFC_NO_ALTERNATE_PART, /* PHY_DEVICE_ALT_PART_NUM_OF_INSTANCE */ 
    RFDEVICE_COMM_PROTO_RFFE, /* PHY_DEVICE_COMM_PROTOCOL */ 
    {    3,0 /* 0 not specified */,}, /* PHY_DEVICE_COMM_BUS */ 
    0x217, /* PHY_DEVICE_MANUFACTURER_ID */ 
    0x14, /* PHY_DEVICE_PRODUCT_ID */ 
    0x00, /* PHY_DEVICE_PRODUCT_REV */ 
    0x8, /* DEFAULT USID RANGE START */
    0x8, /* DEFAULT USID RANGE END */
    0x8, /* PHY_DEVICE_ASSIGNED_USID */ 
    0 /*Warning: Not specified*/, /* RFFE_GROUP_ID */ 
    FALSE, /* INIT */ 
    RFC_INVALID_PARAM, /* ASSOCIATED_DAC */ 
  }, /* END - Device: QFE2080FC */ 

  { /*Device: QFE1040 */ 
    QFE1040, /* PHY_DEVICE_NAME */ 
    9, /* PHY_DEVICE_INSTANCE */ 
    RFC_NO_ALTERNATE_PART, /* PHY_DEVICE_ALT_PART_NUM_OF_INSTANCE */ 
    RFDEVICE_COMM_PROTO_RFFE, /* PHY_DEVICE_COMM_PROTOCOL */ 
    {    2,0 /* 0 not specified */,}, /* PHY_DEVICE_COMM_BUS */ 
    0x217, /* PHY_DEVICE_MANUFACTURER_ID */ 
    0x10, /* PHY_DEVICE_PRODUCT_ID */ 
    0, /* PHY_DEVICE_PRODUCT_REV */ 
    0x8, /* DEFAULT USID RANGE START */
    0x8, /* DEFAULT USID RANGE END */
    0x3, /* PHY_DEVICE_ASSIGNED_USID */ 
    0 /*Warning: Not specified*/, /* RFFE_GROUP_ID */ 
    FALSE, /* INIT */ 
    RFC_INVALID_PARAM, /* ASSOCIATED_DAC */ 
  }, /* END - Device: QFE1040 */ 

  { /*Device: RFDEVICE_INVALID */ 
    RFDEVICE_INVALID, /* PHY_DEVICE_NAME */ 
    0 /*Warning: Not specified*/, /* PHY_DEVICE_INSTANCE */ 
    0 /*Warning: Not specified*/, /* PHY_DEVICE_ALT_PART_NUM_OF_INSTANCE */ 
    RFDEVICE_COMM_PROTO_INVALID, /* PHY_DEVICE_COMM_PROTOCOL */ 
    {    0 /* 0 not specified */,0 /* 0 not specified */,}, /* PHY_DEVICE_COMM_BUS */ 
    0 /*Warning: Not specified*/, /* PHY_DEVICE_MANUFACTURER_ID */ 
    0 /*Warning: Not specified*/, /* PHY_DEVICE_PRODUCT_ID */ 
    0 /*Warning: Not specified*/, /* PHY_DEVICE_PRODUCT_REV */ 
    0 /*Warning: Not specified*/, /* DEFAULT USID RANGE START */
    0 /*Warning: Not specified*/, /* DEFAULT USID RANGE END */
    0 /*Warning: Not specified*/, /* PHY_DEVICE_ASSIGNED_USID */ 
    0 /*Warning: Not specified*/, /* RFFE_GROUP_ID */ 
    FALSE, /* INIT */ 
    RFC_INVALID_PARAM, /* ASSOCIATED_DAC */ 
  }, /* END - Device: RFDEVICE_INVALID */ 

};


rfc_logical_device_info_type rfc_wtr3925_ssku_pamid_logical_devices_list[] = 
{
  { /*Device: WTR3925 */ 
    RFDEVICE_TRANSCEIVER, /* DEVICE_MODULE_TYPE */ 
    WTR3925, /* DEVICE_MODULE_NAME */ 
    0, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    0, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: WTR3925 */ 

  { /*Device: QFE3100 */ 
    RFDEVICE_PAPM, /* DEVICE_MODULE_TYPE */ 
    QFE3100, /* DEVICE_MODULE_NAME */ 
    0, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    1, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE3100 */ 

  { /*Device: B11_21PA */ 
    RFDEVICE_PA, /* DEVICE_MODULE_TYPE */ 
    GEN_PA /* B11_21PA */, /* DEVICE_MODULE_NAME */ 
    0, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    2, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: B11_21PA */ 

  { /*Device: QFE3340FC */ 
    RFDEVICE_PA, /* DEVICE_MODULE_TYPE */ 
    QFE3340FC, /* DEVICE_MODULE_NAME */ 
    1, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    3, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE3340FC */ 

  { /*Device: QFE4465FC */ 
    RFDEVICE_PA, /* DEVICE_MODULE_TYPE */ 
    QFE4465FC, /* DEVICE_MODULE_NAME */ 
    2, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    4, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE4465FC */ 

  { /*Device: QFE4455FC */ 
    RFDEVICE_PA, /* DEVICE_MODULE_TYPE */ 
    QFE4455FC, /* DEVICE_MODULE_NAME */ 
    3, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    5, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE4455FC */ 

  { /*Device: QFE4465FC_GSM */ 
    RFDEVICE_PA, /* DEVICE_MODULE_TYPE */ 
    QFE4465FC_GSM, /* DEVICE_MODULE_NAME */ 
    4, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    4, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE4465FC_GSM */ 

  { /*Device: QFE4455FC_GSM */ 
    RFDEVICE_PA, /* DEVICE_MODULE_TYPE */ 
    QFE4455FC_GSM, /* DEVICE_MODULE_NAME */ 
    5, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    5, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE4455FC_GSM */ 

  { /*Device: QFE3340FC */ 
    RFDEVICE_ASM, /* DEVICE_MODULE_TYPE */ 
    QFE3340FC, /* DEVICE_MODULE_NAME */ 
    0, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    3, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE3340FC */ 

  { /*Device: QFE4465FC */ 
    RFDEVICE_ASM, /* DEVICE_MODULE_TYPE */ 
    QFE4465FC, /* DEVICE_MODULE_NAME */ 
    1, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    4, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE4465FC */ 

  { /*Device: QFE4465FC_TDD */ 
    RFDEVICE_ASM, /* DEVICE_MODULE_TYPE */ 
    QFE4465FC_TDD, /* DEVICE_MODULE_NAME */ 
    2, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    4, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE4465FC_TDD */ 

  { /*Device: QFE4455FC */ 
    RFDEVICE_ASM, /* DEVICE_MODULE_TYPE */ 
    QFE4455FC, /* DEVICE_MODULE_NAME */ 
    3, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    5, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE4455FC */ 

  { /*Device: QFE2082FC */ 
    RFDEVICE_ASM, /* DEVICE_MODULE_TYPE */ 
    QFE2082FC, /* DEVICE_MODULE_NAME */ 
    4, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    6, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE2082FC */ 

  { /*Device: QFE2081FC */ 
    RFDEVICE_ASM, /* DEVICE_MODULE_TYPE */ 
    QFE2081FC, /* DEVICE_MODULE_NAME */ 
    5, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    7, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE2081FC */ 

  { /*Device: QFE2080FC */ 
    RFDEVICE_ASM, /* DEVICE_MODULE_TYPE */ 
    QFE2080FC, /* DEVICE_MODULE_NAME */ 
    6, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    8, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE2080FC */ 

  { /*Device: QFE1040_HB_W0 */ 
    RFDEVICE_ASM, /* DEVICE_MODULE_TYPE */ 
    QFE1040_HB_W0, /* DEVICE_MODULE_NAME */ 
    7, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    9, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE1040_HB_W0 */ 

  { /*Device: QFE1040_MB_W0 */ 
    RFDEVICE_ASM, /* DEVICE_MODULE_TYPE */ 
    QFE1040_MB_W0, /* DEVICE_MODULE_NAME */ 
    8, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    9, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE1040_MB_W0 */ 

  { /*Device: QFE1040_LB_W0 */ 
    RFDEVICE_ASM, /* DEVICE_MODULE_TYPE */ 
    QFE1040_LB_W0, /* DEVICE_MODULE_NAME */ 
    9, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    9, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE1040_LB_W0 */ 

  { /*Device: QFE2082FC */ 
    RFDEVICE_COUPLER, /* DEVICE_MODULE_TYPE */ 
    QFE2082FC, /* DEVICE_MODULE_NAME */ 
    0, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    6, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE2082FC */ 

  { /*Device: QFE2081FC */ 
    RFDEVICE_COUPLER, /* DEVICE_MODULE_TYPE */ 
    QFE2081FC, /* DEVICE_MODULE_NAME */ 
    1, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    7, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE2081FC */ 

  { /*Device: QFE2080FC */ 
    RFDEVICE_COUPLER, /* DEVICE_MODULE_TYPE */ 
    QFE2080FC, /* DEVICE_MODULE_NAME */ 
    2, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    8, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: QFE2080FC */ 

  { /*Device: RFDEVICE_INVALID */ 
    RFDEVICE_TYPE_INVALID, /* DEVICE_MODULE_TYPE */ 
    RFDEVICE_INVALID, /* DEVICE_MODULE_NAME */ 
    0 /*Warning: Not specified*/, /* DEVICE_MODULE_TYPE_INSTANCE */ 
    0 /*Warning: Not specified*/, /* ASSOCIATED_PHY_DEVICE_INSTANCE */ 
  }, /* END - Device: RFDEVICE_INVALID */ 

};


rfc_signal_info_type rfc_wtr3925_ssku_pamid_sig_info[RFC_WTR3925_SSKU_PAMID_SIG_NUM + 1] = 
{
  { RFC_MSM_TIMING_PA_CTL , RFC_LOW, DAL_GPIO_NO_PULL, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_TIMING_PA_CTL */ 
  { RFC_MSM_TIMING_PA_RANGE , RFC_LOW, DAL_GPIO_NO_PULL, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_TIMING_PA_RANGE */ 
  { RFC_MSM_TIMING_ASM_CTL , RFC_LOW, DAL_GPIO_NO_PULL, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_TIMING_ASM_CTL */ 
  { RFC_MSM_TIMING_PAPM_CTL , RFC_LOW, DAL_GPIO_NO_PULL, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_TIMING_PAPM_CTL */ 
  { RFC_MSM_TIMING_TX_TX_RF_ON0 , RFC_LOW, DAL_GPIO_NO_PULL, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_TIMING_TX_TX_RF_ON0 */ 
  { RFC_MSM_TIMING_TX_RX_RF_ON0 , RFC_LOW, DAL_GPIO_NO_PULL, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_TIMING_TX_RX_RF_ON0 */ 
  { RFC_MSM_RF_PATH_SEL_16 , RFC_LOW, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_RF_PATH_SEL_16 */ 
  { RFC_MSM_RF_PATH_SEL_17 , RFC_LOW, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_RF_PATH_SEL_17 */ 
  { RFC_MSM_RF_PATH_SEL_24 , RFC_LOW, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_RF_PATH_SEL_24 */ 
  { RFC_MSM_RFFE1_CLK , RFC_CONFIG_ONLY, DAL_GPIO_PULL_DOWN, DAL_GPIO_8MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_RFFE1_CLK */ 
  { RFC_MSM_RFFE1_DATA , RFC_CONFIG_ONLY, DAL_GPIO_PULL_DOWN, DAL_GPIO_8MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_RFFE1_DATA */ 
  { RFC_MSM_RFFE2_CLK , RFC_CONFIG_ONLY, DAL_GPIO_PULL_DOWN, DAL_GPIO_8MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_RFFE2_CLK */ 
  { RFC_MSM_RFFE2_DATA , RFC_CONFIG_ONLY, DAL_GPIO_PULL_DOWN, DAL_GPIO_8MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_RFFE2_DATA */ 
  { RFC_MSM_RFFE3_CLK , RFC_CONFIG_ONLY, DAL_GPIO_PULL_DOWN, DAL_GPIO_8MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_RFFE3_CLK */ 
  { RFC_MSM_RFFE3_DATA , RFC_CONFIG_ONLY, DAL_GPIO_PULL_DOWN, DAL_GPIO_8MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_RFFE3_DATA */ 
  { RFC_MSM_RFFE4_CLK , RFC_CONFIG_ONLY, DAL_GPIO_PULL_DOWN, DAL_GPIO_8MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_RFFE4_CLK */ 
  { RFC_MSM_RFFE4_DATA , RFC_CONFIG_ONLY, DAL_GPIO_PULL_DOWN, DAL_GPIO_8MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_RFFE4_DATA */ 
  { RFC_MSM_GPDATA0_1 , RFC_CONFIG_ONLY, DAL_GPIO_PULL_DOWN, DAL_GPIO_6MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_GPDATA0_1 */ 
  { RFC_MSM_GPDATA0_0 , RFC_CONFIG_ONLY, DAL_GPIO_PULL_DOWN, DAL_GPIO_6MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_GPDATA0_0 */ 
  { RFC_MSM_INTERNAL_GNSS_BLANK , RFC_LOW, DAL_GPIO_NO_PULL, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_INTERNAL_GNSS_BLANK */ 
  { RFC_MSM_TX_GTR_TH , RFC_LOW, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_TX_GTR_TH */ 
  { RFC_MSM_PA_IND , RFC_LOW, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA, (DALGpioIdType)NULL }, /* RFC_WTR3925_SSKU_PAMID_PA_IND */ 
  { (rfc_msm_signal_type)RFC_SIG_LIST_END, (rfc_logic_type)RFC_ENCODED_REVISION, DAL_GPIO_NO_PULL, DAL_GPIO_2MA, (DALGpioIdType)NULL } /* LAST SIG INDICATOR */ 
};


rfc_common_data* rfc_wtr3925_ssku_pamid_cmn_ag::get_instance(rf_hw_type rf_hw)
{
  if (rfc_common_data_ptr == NULL)
  {
    rfc_common_data_ptr = (rfc_common_data *)new rfc_wtr3925_ssku_pamid_cmn_ag(rf_hw);
  }
  return( (rfc_common_data *)rfc_common_data_ptr);
}

//constructor
rfc_wtr3925_ssku_pamid_cmn_ag::rfc_wtr3925_ssku_pamid_cmn_ag(rf_hw_type rf_hw)
  :rfc_common_data(rf_hw)
{
}


uint32 rfc_wtr3925_ssku_pamid_cmn_ag::sig_info_table_get(rfc_signal_info_type **rfc_info_table)
{
  if (NULL == rfc_info_table)
  {
    return 0;
  }

  *rfc_info_table = &rfc_wtr3925_ssku_pamid_sig_info[0];

#ifdef FEATURE_GSM
  // Create GSM RFC AG Data Object
  rfc_gsm_data *rfc_gsm_data = rfc_wtr3925_ssku_pamid_gsm_ag::get_instance(); 
  if (rfc_gsm_data == NULL)
  {
    RF_MSG_1( RF_ERROR, "rfc_wtr3925_ssku_pamid RFC ERROR GSM Data Object is NOT Created for HWID %d. Cannot operate in this tech.", m_rf_hw );
    return 0;
}
#endif /* FEATURE_GSM */

#ifdef FEATURE_WCDMA
  // Create WCDMA RFC AG Data Object
  rfc_wcdma_data *rfc_wcdma_data = rfc_wtr3925_ssku_pamid_wcdma_ag::get_instance(); 
  if (rfc_wcdma_data == NULL)
  {
    RF_MSG_1( RF_ERROR, "rfc_wtr3925_ssku_pamid RFC ERROR WCDMA Data Object is NOT Created for HWID %d. Cannot operate in this tech.", m_rf_hw );
    return 0;
}
#endif /* FEATURE_WCDMA */

#ifdef FEATURE_CDMA1X
  // Create CDMA RFC AG Data Object
  rfc_cdma_data *rfc_cdma_data = rfc_wtr3925_ssku_pamid_cdma_ag::get_instance(); 
  if (rfc_cdma_data == NULL)
  {
    RF_MSG_1( RF_ERROR, "rfc_wtr3925_ssku_pamid RFC ERROR CDMA Data Object is NOT Created for HWID %d. Cannot operate in this tech.", m_rf_hw );
    return 0;
}
#endif /* FEATURE_CDMA */

#ifdef FEATURE_LTE
  // Create LTE RFC AG Data Object
  rfc_lte_data *rfc_lte_data = rfc_wtr3925_ssku_pamid_lte_ag::get_instance(); 
  if (rfc_lte_data == NULL)
  {
    RF_MSG_1( RF_ERROR, "rfc_wtr3925_ssku_pamid RFC ERROR LTE Data Object is NOT Created for HWID %d. Cannot operate in this tech.", m_rf_hw );
    return 0;
}
#endif /* FEATURE_LTE */

#ifdef FEATURE_TDSCDMA
  // Create TDSCDMA RFC AG Data Object
  rfc_tdscdma_data *rfc_tdscdma_data = rfc_wtr3925_ssku_pamid_tdscdma_ag::get_instance(); 
  if (rfc_tdscdma_data == NULL)
  {
    RF_MSG_1( RF_ERROR, "rfc_wtr3925_ssku_pamid RFC ERROR TDSCDMA Data Object is NOT Created for HWID %d. Cannot operate in this tech.", m_rf_hw );
    return 0;
}
#endif /* FEATURE_TDSCDMA */

#ifdef FEATURE_CGPS
  // Create GNSS RFC AG Data Object
  rfc_gnss_data *rfc_gnss_data = rfc_wtr3925_ssku_pamid_gnss_ag::get_instance(); 
  if (rfc_gnss_data == NULL)
  {
    RF_MSG_1( RF_ERROR, "rfc_wtr3925_ssku_pamid RFC ERROR GNSS Data Object is NOT Created for HWID %d. Cannot operate in this tech.", m_rf_hw );
    return 0;
}
#endif /* FEATURE_GNSS */

  return RFC_WTR3925_SSKU_PAMID_SIG_NUM;
}

rfc_phy_device_info_type* rfc_wtr3925_ssku_pamid_cmn_ag::get_phy_device_cfg( void )
{
  return (&rfc_wtr3925_ssku_pamid_phy_devices_list[0]);
}

rfc_logical_device_info_type* rfc_wtr3925_ssku_pamid_cmn_ag::get_logical_device_cfg( void )
{
  return (&rfc_wtr3925_ssku_pamid_logical_devices_list[0]);
}



rfm_concurrency_restriction_type rfc_wtr3925_ssku_pamid_concurrency_restrictions[] = 
{
  /*Restriction Group Index 0*/
  {
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC6 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) ),
          /* Bit mask element 2 */ 
          (0),
    },
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC6 - 0) ) | ( (uint64)1 << (SYS_BAND_BC14 - 0) ) | ( (uint64)1 << (SYS_BAND_BC15 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND25 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 1*/
  {
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) ),
          /* Bit mask element 2 */ 
          (0),
    },
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC6 - 0) ) | ( (uint64)1 << (SYS_BAND_BC14 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND25 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 2*/
  {
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) ),
          /* Bit mask element 2 */ 
          (0),
    },
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC14 - 0) ) | ( (uint64)1 << (SYS_BAND_BC15 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND25 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 3*/
  {
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC15 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) ),
          /* Bit mask element 2 */ 
          (0),
    },
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC6 - 0) ) | ( (uint64)1 << (SYS_BAND_BC14 - 0) ) | ( (uint64)1 << (SYS_BAND_BC15 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND25 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 4*/
  {
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC14 - 0) ) ),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND25 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
    },
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC6 - 0) ) | ( (uint64)1 << (SYS_BAND_BC14 - 0) ) | ( (uint64)1 << (SYS_BAND_BC15 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND25 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 5*/
  {
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC0 - 0) ) | ( (uint64)1 << (SYS_BAND_BC10 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_850 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_EGSM_900 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_V_850 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_VIII_900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_XIX_850 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND5 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND8 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND12 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND13 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND17 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND18 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND19 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND20 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND26 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND28 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND29 - 128) ) ),
    },
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC0 - 0) ) | ( (uint64)1 << (SYS_BAND_BC10 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_850 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_EGSM_900 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_V_850 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_VIII_900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_XIX_850 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND5 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND8 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND12 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND13 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND17 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND18 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND19 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND20 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND26 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND28 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND29 - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 6*/
  {
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_XI_1500 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND7 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND30 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND38 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND40 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND41 - 128) ) ),
    },
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_XI_1500 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND7 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND30 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND38 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND40 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND41 - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 7*/
  {
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) ),
    },
    {((uint32)1 << RFM_DEVICE_0) | ((uint32)1 << RFM_DEVICE_2) },
    {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_XI_1500 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 8*/
  {
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC6 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) ),
          /* Bit mask element 2 */ 
          (0),
    },
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC6 - 0) ) | ( (uint64)1 << (SYS_BAND_BC14 - 0) ) | ( (uint64)1 << (SYS_BAND_BC15 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND25 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 9*/
  {
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND25 - 128) ) ),
    },
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC6 - 0) ) | ( (uint64)1 << (SYS_BAND_BC14 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 10*/
  {
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) ),
          /* Bit mask element 2 */ 
          (0),
    },
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC14 - 0) ) | ( (uint64)1 << (SYS_BAND_BC15 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND25 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 11*/
  {
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC15 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) ),
          /* Bit mask element 2 */ 
          (0),
    },
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC6 - 0) ) | ( (uint64)1 << (SYS_BAND_BC14 - 0) ) | ( (uint64)1 << (SYS_BAND_BC15 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 12*/
  {
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
    },
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC6 - 0) ) | ( (uint64)1 << (SYS_BAND_BC14 - 0) ) | ( (uint64)1 << (SYS_BAND_BC15 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_XI_1500 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND25 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 13*/
  {
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) ),
    },
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC6 - 0) ) | ( (uint64)1 << (SYS_BAND_BC14 - 0) ) | ( (uint64)1 << (SYS_BAND_BC15 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IX_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND25 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 14*/
  {
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC0 - 0) ) | ( (uint64)1 << (SYS_BAND_BC10 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_850 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_EGSM_900 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_V_850 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_VIII_900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_XIX_850 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND5 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND8 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND12 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND13 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND17 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND18 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND19 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND20 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND26 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND28 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND29 - 128) ) ),
    },
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_BC0 - 0) ) | ( (uint64)1 << (SYS_BAND_BC10 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_850 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_EGSM_900 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_V_850 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_VIII_900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_XIX_850 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND5 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND8 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND12 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND13 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND17 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND18 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND19 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND20 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND26 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND28 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND29 - 128) ) ),
    },
    FALSE,
  },
  /*Restriction Group Index 15*/
  {
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_XI_1500 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND7 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND30 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND38 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND40 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND41 - 128) ) ),
    },
    {((uint32)1 << RFM_DEVICE_1) | ((uint32)1 << RFM_DEVICE_3) },
    {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_XI_1500 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND7 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND30 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND38 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND40 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND41 - 128) ) ),
    },
    FALSE,
  },
};


rfm_devices_configuration_type rfc_wtr3925_ssku_pamid_logical_device_properties = 
{
  224, /* HWID */
  { /* device_support */ 
    { /* Logical Device 0 */ 
      /* bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_GSM_850 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_EGSM_900 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) | ( (uint64)1 << (SYS_BAND_BC0 - 0) ) | ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC10 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_V_850 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_VIII_900 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND5 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND7 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND8 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND12 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND13 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND17 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND20 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND21 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND26 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND29 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND38 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND40 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND41 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND30 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
        },
      },
      /* preferred_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_GSM_850 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_EGSM_900 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) | ( (uint64)1 << (SYS_BAND_BC0 - 0) ) | ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC10 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_V_850 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_VIII_900 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND5 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND7 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND8 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND12 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND13 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND17 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND21 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND26 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND29 - 128) ) ),
        },
      },
      /* ho_rxd_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      ( RFM_DEVICE_RX_SUPPORTED),
      RFM_DEVICE_1, /* VCO Based Preferred Associated Rx Device for RFM_DEVICE_0 (Valid for Rx Devs only)*/
      RFM_DEVICE_4, /* WTR Based Preferred Associated Tx Device for RFM_DEVICE_0 (Valid for Rx Devs only)*/
    }, /* End Logical Device 0 */ 
    { /* Logical Device 1 */ 
      /* bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_GSM_850 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_EGSM_900 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) | ( (uint64)1 << (SYS_BAND_BC0 - 0) ) | ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC10 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_V_850 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_VIII_900 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND5 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND7 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND8 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND12 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND13 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND17 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND20 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND21 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND26 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND29 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND38 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND40 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND41 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND30 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
        },
      },
      /* preferred_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* ho_rxd_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      ( RFM_DEVICE_RX_SUPPORTED),
      RFM_DEVICE_0, /* VCO Based Preferred Associated Rx Device for RFM_DEVICE_1 (Valid for Rx Devs only)*/
      RFM_DEVICE_4, /* WTR Based Preferred Associated Tx Device for RFM_DEVICE_1 (Valid for Rx Devs only)*/
    }, /* End Logical Device 1 */ 
    { /* Logical Device 2 */ 
      /* bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_GSM_850 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_EGSM_900 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) | ( (uint64)1 << (SYS_BAND_BC0 - 0) ) | ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC10 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_V_850 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_VIII_900 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND5 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND7 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND8 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND12 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND13 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND17 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND20 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND21 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND26 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND28 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND29 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND38 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND40 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND41 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND30 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
        },
      },
      /* preferred_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND20 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND28 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND38 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND40 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND41 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND30 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
        },
      },
      /* ho_rxd_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      ( RFM_DEVICE_RX_SUPPORTED),
      RFM_DEVICE_3, /* VCO Based Preferred Associated Rx Device for RFM_DEVICE_2 (Valid for Rx Devs only)*/
      RFM_DEVICE_4, /* WTR Based Preferred Associated Tx Device for RFM_DEVICE_2 (Valid for Rx Devs only)*/
    }, /* End Logical Device 2 */ 
    { /* Logical Device 3 */ 
      /* bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_GSM_850 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_EGSM_900 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) | ( (uint64)1 << (SYS_BAND_BC0 - 0) ) | ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC10 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_V_850 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_VIII_900 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND5 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND7 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND8 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND12 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND13 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND17 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND20 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND21 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND26 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND28 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND29 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND38 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND40 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND41 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND30 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
        },
      },
      /* preferred_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* ho_rxd_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      ( RFM_DEVICE_RX_SUPPORTED),
      RFM_DEVICE_2, /* VCO Based Preferred Associated Rx Device for RFM_DEVICE_3 (Valid for Rx Devs only)*/
      RFM_DEVICE_4, /* WTR Based Preferred Associated Tx Device for RFM_DEVICE_3 (Valid for Rx Devs only)*/
    }, /* End Logical Device 3 */ 
    { /* Logical Device 4 */ 
      /* bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_GSM_850 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_EGSM_900 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) | ( (uint64)1 << (SYS_BAND_BC0 - 0) ) | ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC10 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_V_850 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_VIII_900 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND5 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND7 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND8 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND12 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND13 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND17 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND20 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND21 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND26 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND28 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND38 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND40 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND41 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND30 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
        },
      },
      /* preferred_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          ( ( (uint64)1 << (SYS_BAND_GSM_850 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_EGSM_900 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_DCS_1800 - 0) ) | ( (uint64)1 << (SYS_BAND_GSM_PCS_1900 - 0) ) | ( (uint64)1 << (SYS_BAND_BC0 - 0) ) | ( (uint64)1 << (SYS_BAND_BC1 - 0) ) | ( (uint64)1 << (SYS_BAND_BC10 - 0) ) ),
          /* Bit mask element 1 */ 
          ( ( (uint64)1 << (SYS_BAND_WCDMA_I_IMT_2000 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_II_PCS_1900 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_III_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_IV_1700 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_V_850 - 64) ) | ( (uint64)1 << (SYS_BAND_WCDMA_VIII_900 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND1 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND2 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND3 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND4 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND5 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND7 - 64) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND8 - 64) ) ),
          /* Bit mask element 2 */ 
          ( ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND11 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND12 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND13 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND17 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND20 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND21 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND26 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND28 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND34 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND38 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND39 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND40 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND41 - 128) ) | ( (uint64)1 << (SYS_BAND_LTE_EUTRAN_BAND30 - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDA - 128) ) | ( (uint64)1 << (SYS_BAND_TDS_BANDF - 128) ) ),
        },
      },
      /* ho_rxd_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      ( RFM_DEVICE_TX_SUPPORTED),
      RFM_INVALID_DEVICE, /* VCO Based Preferred Associated Rx Device for RFM_DEVICE_4 (Valid for Rx Devs only)*/
      RFM_INVALID_DEVICE, /* WTR Based Preferred Associated Tx Device for RFM_DEVICE_4 (Valid for Rx Devs only)*/
    }, /* End Logical Device 4 */ 
    { /* Logical Device 5 */ 
      /* bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* preferred_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* ho_rxd_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      ( 0),
      RFM_INVALID_DEVICE, /* VCO Based Preferred Associated Rx Device for RFM_DEVICE_5 (Valid for Rx Devs only)*/
      RFM_INVALID_DEVICE, /* WTR Based Preferred Associated Tx Device for RFM_DEVICE_5 (Valid for Rx Devs only)*/
    }, /* End Logical Device 5 */ 
    { /* Logical Device 6 */ 
      /* bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* preferred_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* ho_rxd_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      ( 0),
      RFM_INVALID_DEVICE, /* VCO Based Preferred Associated Rx Device for RFM_DEVICE_6 (Valid for Rx Devs only)*/
      RFM_INVALID_DEVICE, /* WTR Based Preferred Associated Tx Device for RFM_DEVICE_6 (Valid for Rx Devs only)*/
    }, /* End Logical Device 6 */ 
    { /* Logical Device 7 */ 
      /* bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* preferred_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* ho_rxd_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      ( 0),
      RFM_INVALID_DEVICE, /* VCO Based Preferred Associated Rx Device for RFM_DEVICE_7 (Valid for Rx Devs only)*/
      RFM_INVALID_DEVICE, /* WTR Based Preferred Associated Tx Device for RFM_DEVICE_7 (Valid for Rx Devs only)*/
    }, /* End Logical Device 7 */ 
    { /* Logical Device 8 */ 
      /* bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* preferred_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* ho_rxd_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      ( 0),
      RFM_INVALID_DEVICE, /* VCO Based Preferred Associated Rx Device for RFM_DEVICE_8 (Valid for Rx Devs only)*/
      RFM_INVALID_DEVICE, /* WTR Based Preferred Associated Tx Device for RFM_DEVICE_8 (Valid for Rx Devs only)*/
    }, /* End Logical Device 8 */ 
    { /* Logical Device 9 */ 
      /* bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* preferred_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      /* ho_rxd_bands_supported */ 
      {
        {
          /* Bit mask element 0 */ 
          (0),
          /* Bit mask element 1 */ 
          (0),
          /* Bit mask element 2 */ 
          (0),
        },
      },
      ( 0),
      RFM_INVALID_DEVICE, /* VCO Based Preferred Associated Rx Device for RFM_DEVICE_9 (Valid for Rx Devs only)*/
      RFM_INVALID_DEVICE, /* WTR Based Preferred Associated Tx Device for RFM_DEVICE_9 (Valid for Rx Devs only)*/
    }, /* End Logical Device 9 */ 
  },
  {
    16, /* num_restriction_groups */ 
    &rfc_wtr3925_ssku_pamid_concurrency_restrictions[0], /* concurrency_restrictions */ 
  },
  #ifdef FEATURE_CUST_1
  {
    0, /* num_dglna_combos */ 
    NULL, /* No DGLNA Combos */ 
  },
  #endif //FEATURE_CUST_1
  FALSE, /* antenna_swap_supported */ 
  RFM_SINGLE_RX_DSDS_SUPPORTED | RFM_DUAL_RX_DSDS_SUPPORTED | RFM_SLTE_SUPPORTED | RFM_SRLTE_SUPPORTED | RFM_SHDR_SUPPORTED, /* concurrency_features */ 
};

boolean rfc_wtr3925_ssku_pamid_cmn_ag::get_logical_path_config(rfm_devices_configuration_type* dev_cfg)
{
  if ( dev_cfg == NULL )
  {
    MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR, "rfm_get_devices_configuration: Invalid Container", 0);
    return FALSE;
  }

  memscpy(dev_cfg,
          sizeof(rfm_devices_configuration_type),
          &rfc_wtr3925_ssku_pamid_logical_device_properties,
          sizeof(rfm_devices_configuration_type));

  return TRUE;
}

const rfm_devices_configuration_type* rfc_wtr3925_ssku_pamid_cmn_ag::get_logical_device_properties( void )
{
  return &rfc_wtr3925_ssku_pamid_logical_device_properties;
}



rfc_cmn_properties_type rfc_wtr3925_ssku_pamid_cmn_properties = 
{
  RFC_ENCODED_REVISION,   
  RFC_ATTENUATION_STATE_HIGH,
};

boolean rfc_wtr3925_ssku_pamid_cmn_ag::get_cmn_properties(rfc_cmn_properties_type **ptr)
{
  if (NULL==ptr)
  {
    return FALSE;
  }

  *ptr = &rfc_wtr3925_ssku_pamid_cmn_properties;
  return TRUE;
}

boolean rfc_wtr3925_ssku_pamid_cmn_ag::rfc_get_remapped_device_info
(
  rfc_cal_device_remap_info_type *source_device_info,
  rfc_cal_device_remap_info_type *remapped_device_info
)
{
  return TRUE;
}