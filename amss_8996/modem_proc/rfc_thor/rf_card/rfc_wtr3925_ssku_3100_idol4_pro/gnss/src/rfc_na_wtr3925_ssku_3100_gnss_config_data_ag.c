/* Copyright (C) 2016 Tcl Corporation Limited */

/*
WARNING: This file is auto-generated.

Generated using: D:\Builds\MDM9645\TH.2.1-00597_2015_10_06_12_22_47\modem_proc\rfc_thor\common\etc\rfc_autogen.exe
Generated from:  v5.17.703 of C:\Temp\Downloads\RFC_HWSWCD_v5.17.703.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2015 Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //components/rel/rfc_thor.mpss/2.2.1/rf_card/rfc_na_wtr3925_ssku_3100/gnss/src/rfc_na_wtr3925_ssku_3100_gnss_config_data_ag.c#7 $


=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include "rfc_na_wtr3925_ssku_3100_cmn_ag.h"
#include "rfc_common.h"
#include "rfcom.h"
#include "wtr3925_typedef_ag.h"
#include "qfe4335_port_typedef_ag.h"
#include "qfe4345_port_typedef_ag.h"
#include "rfdevice_coupler.h"



rfc_device_info_type rf_card_na_wtr3925_ssku_3100_gnss_device_info =
{
  RFC_ENCODED_REVISION,
  0 /* Warning: Not Specified */,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  1,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR3925,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        0  /* Invalid TRx port*/ ,  /* PORT */
        ( RFDEVICE_RX_GAIN_STATE_MAPPING_INVALID ),  /* RF_ASIC_BAND_AGC_LUT_MAPPING */
        FALSE,  /* TXAGC_LUT */
        WTR3925_FBRX_ATTN_DEFAULT,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_na_wtr3925_ssku_3100_gnss_sig_cfg =
{
  RFC_ENCODED_REVISION,
  {
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


