
#ifndef RFC_WTR3925_WTR3950_3DLCA_LTEU_CMN_AG
#define RFC_WTR3925_WTR3950_3DLCA_LTEU_CMN_AG


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated using: rfc_autogen.exe
Generated from:  V5.20.780 of RFC_HWSWCD.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2015 Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfc_thor/rf_card/rfc_wtr3925_wtr3950_3dlca_lteu/common/inc/rfc_wtr3925_wtr3950_3dlca_lteu_cmn_ag.h#1 $ 


=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include "rfc_common.h" 



typedef enum
{
  RFC_WTR3925_WTR3950_3DLCA_LTEU_TIMING_PA_CTL,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_TIMING_PA_RANGE,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_TIMING_ASM_CTL,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_TIMING_TUNER_CTL,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_TIMING_PAPM_CTL,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_TIMING_TX_TX_RF_ON0,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_TIMING_TX_RX_RF_ON0,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RF_PATH_SEL_00,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RF_PATH_SEL_02,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RF_PATH_SEL_03,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RF_PATH_SEL_16,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RF_PATH_SEL_17,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RF_PATH_SEL_18,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RF_PATH_SEL_20,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RF_PATH_SEL_21,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RF_PATH_SEL_23,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RF_PATH_SEL_24,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RF_PATH_SEL_04,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE1_CLK,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE1_DATA,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE2_CLK,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE2_DATA,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE3_CLK,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE3_DATA,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE4_CLK,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE4_DATA,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE5_CLK,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE5_DATA,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE6_CLK,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_RFFE6_DATA,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_GPDATA0_1,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_GPDATA0_0,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_GPDATA1_0,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_INTERNAL_GNSS_BLANK,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_TX_GTR_TH,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_PA_IND,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_SIG_NUM,
  RFC_WTR3925_WTR3950_3DLCA_LTEU_SIG_INVALID,
}wtr3925_wtr3950_3dlca_lteu_sig_type;


#ifdef __cplusplus

#include "rfc_common_data.h"

class rfc_wtr3925_wtr3950_3dlca_lteu_cmn_ag:public rfc_common_data
{
  public:
    uint32 sig_info_table_get(rfc_signal_info_type **rfc_info_table);
    rfc_phy_device_info_type* get_phy_device_cfg( void );
    rfc_logical_device_info_type* get_logical_device_cfg( void );
    boolean get_logical_path_config(rfm_devices_configuration_type* dev_cfg);
    const rfm_devices_configuration_type* get_logical_device_properties( void );
    boolean get_cmn_properties(rfc_cmn_properties_type **ptr);
    static rfc_common_data * get_instance(rf_hw_type rf_hw);
    boolean rfc_get_remapped_device_info 
     ( 
       rfc_cal_device_remap_info_type *source_device_info, 
       rfc_cal_device_remap_info_type *remapped_device_info 
     );


  protected:
    rfc_wtr3925_wtr3950_3dlca_lteu_cmn_ag(rf_hw_type rf_hw);
};

#endif   /*  __cplusplus  */


#ifdef __cplusplus
}
#endif



#endif


