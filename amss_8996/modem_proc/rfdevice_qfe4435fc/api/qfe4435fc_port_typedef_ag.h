#ifndef QFE4435FC_TYPEDEF_AG_H
#define QFE4435FC_TYPEDEF_AG_H

/*=============================================================================

          RF DEVICE  A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the QFE4435FC PA.

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.

$ Header: $
$ Author: $
$ DateTime: $ 

=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "comdef.h"

/*----------------------------------------------------------------------------*/
/*!
   It defines the QFE4435FC pa_port enum.
*/
typedef enum 
{
  QFE4435FC_WCDMA_BAND5_PORT_TX_LB4, 
  QFE4435FC_WCDMA_BAND8_PORT_TX_LB1, 
  QFE4435FC_CDMA_BAND0_PORT_TX_LB4, 
  QFE4435FC_CDMA_BAND10_PORT_TX_LB4, 
  QFE4435FC_LTE_BAND5_PORT_TX_LB4, 
  QFE4435FC_LTE_BAND8_PORT_TX_LB1, 
  QFE4435FC_LTE_BAND12_PORT_TX_LB6, 
  QFE4435FC_LTE_BAND13_PORT_TX_LB5, 
  QFE4435FC_LTE_BAND17_PORT_TX_LB6, 
  QFE4435FC_LTE_BAND18_PORT_TX_LB4, 
  QFE4435FC_LTE_BAND19_PORT_TX_LB4, 
  QFE4435FC_LTE_BAND20_PORT_TX_LB2, 
  QFE4435FC_LTE_BAND26_PORT_TX_LB4, 
  QFE4435FC_LTE_BAND27_PORT_TX_LB3, 
  QFE4435FC_LTE_BAND28_PORT_TX_LB8, 
  QFE4435FC_LTE_BAND28_B_PORT_TX_LB7, 
  QFE4435FC_PORT_NUM, 
  QFE4435FC_PORT_INVALID, 
} qfe4435fc_pa_port_data_type;


#endif