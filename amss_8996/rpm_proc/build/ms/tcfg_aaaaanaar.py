#==============================================================================
# Target Build ID Config Script.
#
# Copyright (c) 2010 Qualcomm Technologies Inc.  All Rights Reserved
#==============================================================================
import os

def exists(env):
   return env.Detect('tcfg_AAAAANAAR_data')

def generate(env):
   # Save the tcfg_bid_data in the env
   env['TCFG_BID_IMAGE'] = 'RPM_PROC'
   if os.getenv('MP_MACRO') == "true":
       env.AddUsesFlags('USES_NO_DEBUG', from_builds_file = True)

