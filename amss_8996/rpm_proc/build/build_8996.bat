@echo off
rem ==========================================================================
rem
rem  RPM build system launcher
rem
rem Copyright (c) 2014 by QUALCOMM, Incorporated.
rem All Rights Reserved.
rem QUALCOMM Proprietary/GTDR
rem ==========================================================================
rem $Header: //components/rel/rpm.bf/1.6/build/build_8996.bat#2 $

SET BUILD_ASIC=8996
SET MSM_ID=8996
SET HAL_PLATFORM=8996
SET TARGET_FAMILY=8996
SET CHIPSET=msm8996
SET SECPOLICY=USES_SEC_POLICY_MULTIPLE_DEFAULT_SIGN

python build_common.py %*
