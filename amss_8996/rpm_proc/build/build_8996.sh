# ==========================================================================
#
#  RPM build system launcher
#
# Copyright (c) 2014 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
# ==========================================================================
# $Header: //components/rel/rpm.bf/1.6/build/build_8996.sh#2 $

export BUILD_ASIC=8996
export MSM_ID=8996
export HAL_PLATFORM=8996
export TARGET_FAMILY=8996
export CHIPSET=msm8996
export SECPOLICY=USES_SEC_POLICY_MULTIPLE_DEFAULT_SIGN

cd './rpm_proc/build/'

python ./build_common.py $@
