#ifndef VSENSE_H
#define VSENSE_H


/*=======================================================================
 *! \file vsense.h
 *  \n
 *  \brief This file contains vsense related function prototypes,
 *         enums and driver data structure type.   
 *  \n  
 *  \n &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
 */
/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/25/14   aks     created

========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/


/*----------------------------------------------------------------------------
 * Function : vsense_init
 * -------------------------------------------------------------------------*/
/*!
    Description: Initialize the vsense driver 
    @param
      void
    @return
    void
    
-------------------------------------------------------------------------*/
void vsense_init(void);


#endif 
