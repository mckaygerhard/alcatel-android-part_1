#ifndef __PM_FG_ADC_USR_H__
#define __PM_FG_ADC_USR_H__

/*! \file pm_fg_adc_usr.h
 *  \n
 *  \brief This header file contains enums and API definitions for PMIC BMS
 *         Fuel Gauge ADC User module.
 *  \n
 *  \n &copy; Copyright 2014-2015 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/api/systemdrivers/pmic/pm_fg_adc_usr.h#1 $

when        who     what, where, why
--------    ---     ---------------------------------------------------------- 
11/10/14    sm      Added APIs for ibat and vbat calibration
08/13/14    sm      Changed variable types for get_vbat, get_ibat, get_bcl_v_gain_batt, 
                    get_bcl_i_gain_rsense, get_bcl_i_offset_rsense, get_bcl_i_gain_batfet, 
                    get_bcl_i_gain_batfet, get_bcl_i_sense_source, 
06/25/14    va      Driver update, added protected call
05/12/14    va      Initial Release 
===========================================================================*/

#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"

/*===========================================================================
                        EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/
/**
* @brief This function returns status RDY bit after battery parameter update request is serviced *
* @details
*  After the first readings from ADC are obtained, this bit is set to 1; At reset and shutdown, this bit gets automatically cleared 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in]enable  enable/disable BCL monitoring
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_adc_usr_get_bcl_values(uint32 device_index, boolean *enable);


/**
* @brief This function returns battery ADC Voltage *
* @details
*  8 bit signed partial ADC value, MSB = 0 is positive voltage (positive number), only positive voltages are captured, 1 LSB = 39 mV 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out]vbat_adc  Battery Voltage
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_adc_usr_get_vbat(uint32 device_index, uint32 *vbat_adc);

/**
* @brief This function returns votlage gain correction value for battery voltage *
* @details
*  This function returns gain correction value for battery 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in] gainCorrection  for battery volatge
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_adc_usr_get_bcl_v_gain_batt(uint32 device_index, int32 *v_gain_correction);

/**
* @brief This function returns battery ADC Voltage after gain calibration*
* @details
*  calibrated_value = raw_data * (1 + gain)
* 
* @param[in] pmic_device_index  Primary: 0 Secondary: 1
* @param[out]calibrated_vbat    Calibrated Battery Voltage
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_adc_usr_get_calibrated_vbat(uint32 pmic_device, uint32 *calibrated_vbat);

#endif /* __PM_FG_ADC_USR_H__ */
