/*==============================================================================
@file DALProps.c

$Header: //components/rel/rpm.bf/1.6/core/dal/framework/sys/src/rpm/DevCfg_Init.c#9 $

Copyright (c) 2010, 2011
Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
==============================================================================*/

#include "DALStdDef.h"
#include "DALStdErr.h"
#include "DALSys.h"
#include "DALPropDef.h"
#include "DALSysCmn.h"
#include <string.h>
#include "CoreVerify.h"
#include "DevConfig.h"
#include "Chipinfo.h"
#include "image_layout.h"

extern const DALPROP_Multi_Target_Data DALPROP_Multi_Target_Data_Instance;
extern const unsigned DALPROP_Target_Data_array_count;

DALProps DALPROP_PropsInfo;

extern void Add_Heap_Section(char* base, char* limit);

extern char Image$$DAL_CONFIG_SECTIONS$$Base[];
extern char Image$$DAL_CONFIG_SECTIONS$$Limit[];

void DALSYS_PropsInit(void)
{
    unsigned i;

    const DALPROP_Target_Data* target_data = NULL;
    ChipInfoFamilyType chip_family = Chipinfo_GetFamily();
    if(chip_family == CHIPINFO_FAMILY_MSM8996SG)
    {
        chip_family = CHIPINFO_FAMILY_MSM8996;
    }	    

    //First, find the correct dev config for this target.
    for(i=0; i<DALPROP_Multi_Target_Data_Instance.supported_target_count; i++)
    {
        if((DALPROP_Multi_Target_Data_Instance.supported_targets)[i]->supported_chipset == chip_family)
        {
            target_data = DALPROP_Multi_Target_Data_Instance.supported_targets[i];
        }
        else
        {
            rpm_heap_add_section(
                (DALPROP_Multi_Target_Data_Instance.supported_targets[i])->section_start,
                (DALPROP_Multi_Target_Data_Instance.supported_targets[i])->section_limit,
                RPM_HEAP_SECTION_PRIORITY_HIGH);    //Add as high priority as this is data ram.
        }
    }

    CORE_VERIFY(target_data!=NULL);

    CORE_VERIFY_PTR(DALPROP_PropsInfo.pDALPROP_PropBin    = (const byte*)malloc(target_data->size_of_DALPROP_PropBin));
    CORE_VERIFY_PTR(DALPROP_PropsInfo.pDALPROP_StructPtrs = (void *)malloc(target_data->size_of_DALPROP_StructPtrs));
    CORE_VERIFY_PTR(DALPROP_PropsInfo.pDevices            = (StringDevice *)malloc(target_data->driver_list_count * sizeof(StringDevice)));

    DALPROP_PropsInfo.dwDeviceSize = target_data->driver_list_count;

    memcpy((void *)(DALPROP_PropsInfo.pDALPROP_StructPtrs), target_data->struct_ptrs, target_data->size_of_DALPROP_StructPtrs);
    memcpy((void *)(DALPROP_PropsInfo.pDALPROP_PropBin), target_data->prop_bin, target_data->size_of_DALPROP_PropBin);

    //Now copy over the driver list...
    //To do - add asserts etc
    for(i=0; i<target_data->driver_list_count; i++)
    {
        StringDevice* target   = (StringDevice*)&(DALPROP_PropsInfo.pDevices[i]);
        unsigned buffer_length = strlen(target_data->the_driver_list[i].pszName)+1;
        target->pszName = malloc(buffer_length);
        CORE_VERIFY(target->pszName !=NULL );
        strlcpy(target->pszName, target_data->the_driver_list[i].pszName, buffer_length);
        target->dwHash = target_data->the_driver_list[i].dwHash;
        target->dwOffset = target_data->the_driver_list[i].dwOffset;
        target->pFunctionName = target_data->the_driver_list[i].pFunctionName;
        target->dwNumCollision = target_data->the_driver_list[i].dwNumCollision;
        target->pdwCollisions = NULL;
    }

    //Now add the devconfig image section back to the heap.
    rpm_heap_add_section(Image$$DAL_CONFIG_SECTIONS$$Base,
                         Image$$DAL_CONFIG_SECTIONS$$Limit,
                         RPM_HEAP_SECTION_PRIORITY_HIGH);    //Add as high priority as this is data ram.
    
}
