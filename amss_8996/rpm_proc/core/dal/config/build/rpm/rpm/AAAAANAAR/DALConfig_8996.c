#include "DALPropDef.h"
#include "DALPropDef.h"
#include "DALPropDef.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "dalconfig.h"
#include "dalconfig.h"
#include "dalconfig.h"
#include "dalconfig.h"
#include "dalconfig.h"
#include "dalconfig.h"
#include "DALStdDef.h" 
#include "DALSysTypes.h" 

#include "DevConfig.h"
#include "Chipinfo.h"
#ifndef DAL_CONFIG_IMAGE_RPM 
#define DAL_CONFIG_IMAGE_RPM 
#endif 
extern void * icb_rpm_system_8996_xml;
extern void * SourceConfig_8996_xml;
extern void * RPMClockConfig_8996_xml;
extern void * SystemNOCClockConfig_8996_xml;
extern void * SYSNOCHSAXIClockConfig_8996_xml;
extern void * ConfigNOCClockConfig_8996_xml;
extern void * PeriphNOCClockConfig_8996_xml;
extern void * QDSSATClockConfig_8996_xml;
extern void * QDSSTraceClockConfig_8996_xml;
extern void * QDSSSTMClockConfig_8996_xml;
extern void * QDSSTSCTRDiv2ClockConfig_8996_xml;
extern void * IPAClockConfig_8996_xml;
extern void * CEClockConfig_8996_xml;
extern void * SPMIAHBClockConfig_8996_xml;
extern void * SPMISERClockConfig_8996_xml;
extern void * HMSSAHBClockConfig_8996_xml;
extern void * VSenseClockConfig_8996_xml;
extern void * MMSSAXIClockConfig_8996_xml;
extern void * ClockLogDefaultConfig_8996_xml;
extern void * num_of_smps_8996_xml;
extern void * num_of_ldo_8996_xml;
extern void * num_of_vs_8996_xml;
extern void * num_of_boost_byp_8996_xml;
extern void * num_of_boost_8996_xml;
extern void * num_of_pbs_client_8996_xml;
extern void * num_of_fg_8996_xml;
extern void * mx_rail_8996_xml;
extern void * cx_rail_8996_xml;
extern void * gfx_rail_8996_xml;
extern void * ebi_rail_8996_xml;
extern void * pm_npa_rpm_pam_node_rsrcs_8996_xml;
extern void * num_of_pm_rpm_nodes_8996_xml;
extern void * ldo_rail_8996_xml;
extern void * smps_rail_8996_xml;
extern void * vs_rail_8996_xml;
extern void * clk_info_8996_xml;
extern void * boost_byp_rail_8996_xml;
extern void * boost_rail_8996_xml;
extern void * smps_dep_8996_xml;
extern void * ldo_dep_8996_xml;
extern void * vs_dep_8996_xml;
extern void * clk_dep_8996_xml;
extern void * boost_dep_8996_xml;
extern void * boost_byp_dep_8996_xml;
extern void * pm_mpm_active_cfg_8996_xml;
extern void * pm_mpm_sleep_cfg_8996_xml;
extern void * pm_mpm_cmd_index_8996_xml;
extern void * sleep_enter_info_8996_xml;
extern void * sleep_exit_info_8996_xml;
extern void * clk_reg_8996_xml;
extern void * smps_reg_8996_xml;
extern void * ldo_reg_8996_xml;
extern void * vs_reg_8996_xml;
extern void * boost_reg_8996_xml;
extern void * fg_reg_8996_xml;
extern void * boost_byp_reg_8996_xml;
extern void * pbs_client_reg_8996_xml;
extern void * fts_volt_8996_xml;
extern void * fts2p5_volt_8996_xml;
extern void * hfs_volt_8996_xml;
extern void * boost_volt_8996_xml;
extern void * boost_byp_volt_8996_xml;
extern void * nmos_volt_8996_xml;
extern void * n600_volt_8996_xml;
extern void * n1200_volt_8996_xml;
extern void * pmos_volt_8996_xml;
extern void * ln_volt_8996_xml;
extern void * fts_range_8996_xml;
extern void * fts2p5_range_8996_xml;
extern void * hfs_range_8996_xml;
extern void * boost_range_8996_xml;
extern void * boost_byp_range_8996_xml;
extern void * nmos_range_8996_xml;
extern void * n600_range_8996_xml;
extern void * n1200_range_8996_xml;
extern void * pmos_range_8996_xml;
extern void * ln_range_8996_xml;
extern void * ldo_settling_time_8996_xml;
extern void * smps_settling_time_8996_xml;
extern void * vs_settling_time_8996_xml;
extern void * boost_settling_time_8996_xml;
extern void * clk_settling_time_8996_xml;
extern void * clk_sleep_reg_8996_xml;
extern void * clk_xo_reg_8996_xml;
extern void * clk_common_8996_xml;
extern void * vsense_config_data_8996_xml;

const void * DALPROP_StructPtrs_8996_xml[87] =  {
	&icb_rpm_system_8996_xml,
	&SourceConfig_8996_xml,
	&RPMClockConfig_8996_xml,
	&SystemNOCClockConfig_8996_xml,
	&SYSNOCHSAXIClockConfig_8996_xml,
	&ConfigNOCClockConfig_8996_xml,
	&PeriphNOCClockConfig_8996_xml,
	&QDSSATClockConfig_8996_xml,
	&QDSSTraceClockConfig_8996_xml,
	&QDSSSTMClockConfig_8996_xml,
	&QDSSTSCTRDiv2ClockConfig_8996_xml,
	&IPAClockConfig_8996_xml,
	&CEClockConfig_8996_xml,
	&SPMIAHBClockConfig_8996_xml,
	&SPMISERClockConfig_8996_xml,
	&HMSSAHBClockConfig_8996_xml,
	&VSenseClockConfig_8996_xml,
	&MMSSAXIClockConfig_8996_xml,
	&ClockLogDefaultConfig_8996_xml,
	&num_of_smps_8996_xml,
	&num_of_ldo_8996_xml,
	&num_of_vs_8996_xml,
	&num_of_boost_byp_8996_xml,
	&num_of_boost_8996_xml,
	&num_of_pbs_client_8996_xml,
	&num_of_fg_8996_xml,
	&mx_rail_8996_xml,
	&cx_rail_8996_xml,
	&gfx_rail_8996_xml,
	&ebi_rail_8996_xml,
	&pm_npa_rpm_pam_node_rsrcs_8996_xml,
	&num_of_pm_rpm_nodes_8996_xml,
	&ldo_rail_8996_xml,
	&smps_rail_8996_xml,
	&vs_rail_8996_xml,
	&clk_info_8996_xml,
	&boost_byp_rail_8996_xml,
	&boost_rail_8996_xml,
	&smps_dep_8996_xml,
	&ldo_dep_8996_xml,
	&vs_dep_8996_xml,
	&clk_dep_8996_xml,
	&boost_dep_8996_xml,
	&boost_byp_dep_8996_xml,
	&pm_mpm_active_cfg_8996_xml,
	&pm_mpm_sleep_cfg_8996_xml,
	&pm_mpm_cmd_index_8996_xml,
	&sleep_enter_info_8996_xml,
	&sleep_exit_info_8996_xml,
	&clk_reg_8996_xml,
	&smps_reg_8996_xml,
	&ldo_reg_8996_xml,
	&vs_reg_8996_xml,
	&boost_reg_8996_xml,
	&fg_reg_8996_xml,
	&boost_byp_reg_8996_xml,
	&pbs_client_reg_8996_xml,
	&fts_volt_8996_xml,
	&fts2p5_volt_8996_xml,
	&hfs_volt_8996_xml,
	&boost_volt_8996_xml,
	&boost_byp_volt_8996_xml,
	&nmos_volt_8996_xml,
	&n600_volt_8996_xml,
	&n1200_volt_8996_xml,
	&pmos_volt_8996_xml,
	&ln_volt_8996_xml,
	&fts_range_8996_xml,
	&fts2p5_range_8996_xml,
	&hfs_range_8996_xml,
	&boost_range_8996_xml,
	&boost_byp_range_8996_xml,
	&nmos_range_8996_xml,
	&n600_range_8996_xml,
	&n1200_range_8996_xml,
	&pmos_range_8996_xml,
	&ln_range_8996_xml,
	&ldo_settling_time_8996_xml,
	&smps_settling_time_8996_xml,
	&vs_settling_time_8996_xml,
	&boost_settling_time_8996_xml,
	&clk_settling_time_8996_xml,
	&clk_sleep_reg_8996_xml,
	&clk_xo_reg_8996_xml,
	&clk_common_8996_xml,
	&vsense_config_data_8996_xml,
	 0 };
const uint32 DALPROP_PropBin_8996_xml[] = {

			0x000006d4, 0x00000028, 0x00000224, 0x00000224, 0x00000224, 
			0x00000002, 0x02000145, 0x00000270, 0x0200009b, 0x0000069c, 
			0x74737973, 0x70006d65, 0x5f63696d, 0x5f627261, 0x65736162, 
			0x6464615f, 0x776f0072, 0x0072656e, 0x65746e69, 0x70757272, 
			0x6d730074, 0x6e695f64, 0x655f7274, 0x6c62616e, 0x43006465, 
			0x6b636f6c, 0x72756f53, 0x00736563, 0x5f636367, 0x5f6d7072, 
			0x636f7270, 0x6c63665f, 0x6367006b, 0x79735f63, 0x6f6e5f73, 
			0x78615f63, 0x6c635f69, 0x6367006b, 0x79735f63, 0x6f6e5f73, 
			0x69705f63, 0x5f6d656d, 0x5f697861, 0x006b6c63, 0x5f636367, 
			0x5f737973, 0x5f636f6e, 0x615f7368, 0x635f6978, 0x67006b6c, 
			0x635f6363, 0x6e5f6766, 0x615f636f, 0x635f6268, 0x67006b6c, 
			0x705f6363, 0x70697265, 0x6f6e5f68, 0x68615f63, 0x6c635f62, 
			0x6367006b, 0x64715f63, 0x615f7373, 0x6c635f74, 0x6367006b, 
			0x64715f63, 0x745f7373, 0x65636172, 0x696b6c63, 0x6c635f6e, 
			0x6367006b, 0x64715f63, 0x735f7373, 0x635f6d74, 0x67006b6c, 
			0x715f6363, 0x5f737364, 0x74637374, 0x69645f72, 0x635f3276, 
			0x67006b6c, 0x695f6363, 0x635f6170, 0x67006b6c, 0x635f6363, 
			0x635f3165, 0x67006b6c, 0x735f6363, 0x5f696d70, 0x5f626861, 
			0x006b6c63, 0x5f636367, 0x696d7073, 0x7265735f, 0x6b6c635f, 
			0x63636700, 0x736d685f, 0x68615f73, 0x6c635f62, 0x6367006b, 
			0x64765f63, 0x765f6164, 0x6c635f73, 0x6367006b, 0x64765f63, 
			0x5f786364, 0x635f7376, 0x67006b6c, 0x765f6363, 0x786d6464, 
			0x5f73765f, 0x006b6c63, 0x5f636367, 0x5f73736d, 0x635f7376, 
			0x6d006b6c, 0x5f73736d, 0x67616d6d, 0x615f6369, 0x635f6978, 
			0x43006b6c, 0x6b636f6c, 0x44676f4c, 0x75616665, 0x0073746c, 
			0x41464544, 0x5f544c55, 0x51455246, 0x434e4555, 0x54510059, 
			0x52454d49, 0x5f43415f, 0x45534142, 0x49545100, 0x5f52454d, 
			0x45534142, 0x00000000, 0x00000012, 0x00000000, 0x00000000, 
			0xff00ff00, 0x00000002, 0x00000007, 0x04000000, 0x00000002, 
			0x0000001a, 0x00000004, 0x00000002, 0x00000020, 0x00000030, 
			0xff00ff00, 0x00000008, 0x0000002a, 0x01010106, 0x01000101, 
			0xff00ff00, 0x00000012, 0x0000003b, 0x00000001, 0x00000012, 
			0x00000048, 0x00000002, 0x00000012, 0x0000005a, 0x00000003, 
			0x00000012, 0x0000006e, 0x00000003, 0x00000012, 0x00000088, 
			0x00000004, 0x00000012, 0x0000009f, 0x00000005, 0x00000012, 
			0x000000b3, 0x00000006, 0x00000012, 0x000000ca, 0x00000007, 
			0x00000012, 0x000000da, 0x00000008, 0x00000012, 0x000000f2, 
			0x00000009, 0x00000012, 0x00000103, 0x0000000a, 0x00000012, 
			0x0000011b, 0x0000000b, 0x00000012, 0x00000127, 0x0000000c, 
			0x00000012, 0x00000133, 0x0000000d, 0x00000012, 0x00000144, 
			0x0000000e, 0x00000012, 0x00000155, 0x0000000f, 0x00000012, 
			0x00000166, 0x00000010, 0x00000012, 0x00000176, 0x00000010, 
			0x00000012, 0x00000187, 0x00000010, 0x00000012, 0x00000198, 
			0x00000010, 0x00000012, 0x000001a7, 0x00000011, 0x00000012, 
			0x000001bb, 0x00000012, 0xff00ff00, 0x00000012, 0x00000065, 
			0x00000013, 0x00000012, 0x00000067, 0x00000014, 0x00000012, 
			0x00000068, 0x00000015, 0x00000012, 0x00000081, 0x00000016, 
			0x00000012, 0x00000069, 0x00000017, 0x00000012, 0x00000089, 
			0x00000018, 0x00000012, 0x0000008a, 0x00000019, 0x00000012, 
			0x0000006a, 0x0000001a, 0x00000012, 0x0000006b, 0x0000001b, 
			0x00000012, 0x0000006c, 0x0000001c, 0x00000012, 0x00000084, 
			0x0000001d, 0x00000012, 0x00000072, 0x0000001e, 0x00000012, 
			0x00000073, 0x0000001f, 0x00000012, 0x00000086, 0x00000020, 
			0x00000012, 0x00000087, 0x00000021, 0x00000012, 0x00000088, 
			0x00000022, 0x00000012, 0x00000074, 0x00000023, 0x00000012, 
			0x00000082, 0x00000024, 0x00000012, 0x00000085, 0x00000025, 
			0x00000012, 0x0000006e, 0x00000026, 0x00000012, 0x0000006d, 
			0x00000027, 0x00000012, 0x00000070, 0x00000028, 0x00000012, 
			0x00000071, 0x00000029, 0x00000012, 0x0000006f, 0x0000002a, 
			0x00000012, 0x00000083, 0x0000002b, 0x00000012, 0x0000012d, 
			0x0000002c, 0x00000012, 0x0000012e, 0x0000002d, 0x00000012, 
			0x0000012f, 0x0000002e, 0x00000012, 0x00000075, 0x0000002f, 
			0x00000012, 0x00000076, 0x00000030, 0xff00ff00, 0x00000012, 
			0x00000001, 0x00000031, 0x00000012, 0x00000002, 0x00000032, 
			0x00000012, 0x00000003, 0x00000033, 0x00000012, 0x00000004, 
			0x00000034, 0x00000012, 0x00000005, 0x00000035, 0x00000012, 
			0x00000025, 0x00000036, 0x00000012, 0x00000021, 0x00000037, 
			0x00000012, 0x00000024, 0x00000038, 0x00000012, 0x00000006, 
			0x00000039, 0x00000012, 0x0000001e, 0x0000003a, 0x00000012, 
			0x00000007, 0x0000003b, 0x00000012, 0x00000008, 0x0000003c, 
			0x00000012, 0x00000022, 0x0000003d, 0x00000012, 0x00000009, 
			0x0000003e, 0x00000012, 0x0000000a, 0x0000003f, 0x00000012, 
			0x0000000b, 0x00000040, 0x00000012, 0x0000000c, 0x00000041, 
			0x00000012, 0x0000000d, 0x00000042, 0x00000012, 0x0000000e, 
			0x00000043, 0x00000012, 0x0000001f, 0x00000044, 0x00000012, 
			0x0000000f, 0x00000045, 0x00000012, 0x00000010, 0x00000046, 
			0x00000012, 0x00000020, 0x00000047, 0x00000012, 0x00000011, 
			0x00000048, 0x00000012, 0x00000012, 0x00000049, 0x00000012, 
			0x00000013, 0x0000004a, 0x00000012, 0x00000014, 0x0000004b, 
			0x00000012, 0x00000015, 0x0000004c, 0x00000012, 0x00000016, 
			0x0000004d, 0x00000012, 0x00000017, 0x0000004e, 0x00000012, 
			0x00000018, 0x0000004f, 0x00000012, 0x00000019, 0x00000050, 
			0x00000012, 0x00000023, 0x00000051, 0x00000012, 0x0000001a, 
			0x00000052, 0x00000012, 0x0000001b, 0x00000053, 0x00000012, 
			0x0000001c, 0x00000054, 0xff00ff00, 0x00000002, 0x000001cc, 
			0x0124f800, 0x00000002, 0x000001de, 0x00082000, 0x00000002, 
			0x000001ed, 0x00084000, 0xff00ff00, 0x00000012, 0x00000001, 
			0x00000055, 0xff00ff00 };



const StringDevice driver_list_8996_xml[] = {
			{"/dev/icb/rpm",2545934574u, 548, NULL, 0, NULL },
			{"DALDEVICEID_SPMI_DEVICE",3290583706u, 564, NULL, 0, NULL },
			{"/core/mproc/smd",1450287328u, 604, NULL, 0, NULL },
			{"/rpm/pmic/target",3536625265u, 892, NULL, 0, NULL },
			{"/rpm/pmic/common",2887753651u, 1256, NULL, 0, NULL },
			{"/sysdrivers/vsense",192366965u, 1732, NULL, 0, NULL }
};


extern char Image$$8996_xml_DEVCONFIG_DATA$$Base[];
extern char Image$$8996_xml_DEVCONFIG_DATA$$ZI$$Limit[];

const DALPROP_Target_Data DALPROP_Target_Data_8996_xml = {
    DALPROP_StructPtrs_8996_xml,
    sizeof(DALPROP_StructPtrs_8996_xml),
    DALPROP_PropBin_8996_xml,
    sizeof(DALPROP_PropBin_8996_xml),
    driver_list_8996_xml,
    sizeof(driver_list_8996_xml)/sizeof(const StringDevice),
    DALCONFIG_CHIPINFO_FAMILY_MSM8996_xml,
    Image$$8996_xml_DEVCONFIG_DATA$$Base,
    Image$$8996_xml_DEVCONFIG_DATA$$ZI$$Limit,
};

const DALPROP_Target_Data* DALPROP_Target_Data_array[] =
{
    &DALPROP_Target_Data_8996_xml,
};

const DALPROP_Multi_Target_Data DALPROP_Multi_Target_Data_Instance =
{
    sizeof(DALPROP_Target_Data_array)/sizeof(const DALPROP_Target_Data*),
    DALPROP_Target_Data_array,
};
