//===========================================================================
//  Copyright (c) 2014 - 2014,2016 QUALCOMM Technologies Incorporated.  All Rights Reserved.  
//  QUALCOMM Proprietary and Confidential. 
//===========================================================================
////////////////////////////////////////////////////////////////////////////////////////////////

#include "ddr_phy.h"
#include "target_config.h"

// ***********************************************************************
/// Calculate CDC mask
// ***********************************************************************
uint32 DDR_PHY_hal_cfg_cdc_mask( uint16 coarse_fine, uint8 rank, uint16 hp_mode )
{
    uint32 mask = 0;

    if (rank == 0) {
        if (hp_mode == 1) {
            if (coarse_fine == 1) { //[4:0]
                mask = 0xFFFFFFE0; 
            }
            else { // fine delay [9:5]
                mask  = 0xFFFFFC1F; 
            }
        }
        else {
            mask = 0xFE0FFFFF; // lpcdc [24:20]
        }
    }
    else {
        if (hp_mode == 1) {
            if (coarse_fine == 1) { // [14:10]
                mask  = 0xFFFF83FF; 
            } 
            else { // Fine delay [19:15]
                mask  = 0xFFF07FFF; 
            }
        }
        else {
            mask = 0xC1FFFFFF; // lpcdc [29:25]
        }
    }    

    return mask;

} 

// ***********************************************************************
/// Calculate CDC shift
// ***********************************************************************

uint32 DDR_PHY_hal_cfg_cdc_shift( uint16 coarse_fine, uint8 rank, uint16 hp_mode )
{
    uint32 shift = 0;

    if (rank == 0) {
        if (hp_mode == 1) {
            if (coarse_fine == 1) { //[4:0]
                shift = 0;
            }
            else { // fine delay [9:5]
                shift  = 5; 
            }
        }
        else {
            shift = 20;
        }
    }
    else {
        if (hp_mode == 1) {    
            if (coarse_fine == 1) { // [14:10]
                shift  = 10; 
            } 
            else { // Fine delay [19:15]
                shift  = 15; 
            }
        }
        else {
            shift = 25;
        }
    }

    return shift;
}

void DDR_PHY_hal_cfg_wrlvl_dq_set( uint32 _inst_, uint32 rank, uint32 dq_retmr, uint32 dq_half_cycle, uint32 dq_full_cycle )
{

    if (rank == 1) {
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_RETMR_R1, dq_retmr);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_HALF_CYCLE_R1, dq_half_cycle);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_FULL_CYCLE_R1, dq_full_cycle);
    } else {
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_RETMR_R0, dq_retmr);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_HALF_CYCLE_R0, dq_half_cycle);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_FULL_CYCLE_R0, dq_full_cycle);
  }
  HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
  HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);

}

void DDR_PHY_hal_cfg_wrlvl_half( uint32 _inst_, uint32 rank, uint32 dq_half_cycle )
{

  if (rank == 1) {
      HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_HALF_CYCLE_R1, dq_half_cycle);
  } else {
      HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_HALF_CYCLE_R0, dq_half_cycle);
  }
  HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
  HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);

}

void DDR_PHY_hal_cfg_wrlvl_retmr( uint32 _inst_, uint32 rank, uint32 dqs_retmr )
{

    if (rank == 1) {
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQS_RETMR_R1, dqs_retmr);
    } else {
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQS_RETMR_R0, dqs_retmr);
    }
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);   
}

void DDR_PHY_hal_cfg_cdcext_wrlvl_update( uint32 _inst_, uint8 period_index, uint8 rank, uint32 fine_delay, uint32 coarse_delay )
{
    uint32 ctl;


    ///  read corresponding extension wrlvl delay value out
    if (period_index == 0) {
            ctl = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_0_CTL_CFG );
    }
    else if (period_index == 1) {
            ctl = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_1_CTL_CFG );
    }
    else if (period_index == 2) {
            ctl = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_2_CTL_CFG );
    }
    else if (period_index == 3) {
            ctl = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_3_CTL_CFG );
    }
    else if (period_index == 4) {
            ctl = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_4_CTL_CFG );
    }
    else if (period_index == 5) {
            ctl = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_5_CTL_CFG );
    }
    else if (period_index == 6) {
            ctl = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_6_CTL_CFG );
    }
    else {
            ctl = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_7_CTL_CFG );
    }
    ///  modify wrlvl delay value
    if (rank) {
        ctl = ctl & 0xFFFF83FF;
        coarse_delay = coarse_delay << 10;
        ctl = ctl | coarse_delay;
        ctl = ctl & 0xFFF07FFF;
        fine_delay = fine_delay << 15;
        ctl = ctl | fine_delay;
    } else {
        ctl = ctl & 0xFFFFFFE0;
        ctl = ctl | coarse_delay;
        fine_delay = fine_delay << 5;
        ctl = ctl & 0xFFFFFC1F;
        ctl = ctl | fine_delay;
    }
    ///  write wrlvl delay value back to the corresponding cdc extension cfg
    if (period_index == 0) {
            HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_0_CTL_CFG, ctl);
    }
    else if (period_index == 1) {
            HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_1_CTL_CFG, ctl);
    }
    else if (period_index == 2) {
            HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_2_CTL_CFG, ctl);
    }
    else if (period_index == 3) {
            HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_3_CTL_CFG, ctl);
    }
    else if (period_index == 4) {
            HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_4_CTL_CFG, ctl);
    }
    else if (period_index == 5) {
            HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_5_CTL_CFG, ctl);
    }
    else if (period_index == 6) {
            HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_6_CTL_CFG, ctl);
    }
    else  {
            HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WRLVL_7_CTL_CFG, ctl);
    }
}


// ***********************************************************************
/// ----------------------------------------------------------
///  HAL           : RCW
///  status        : 1: enable rcw training, 0: disable
/// ----------------------------------------------------------
// ***********************************************************************
void DDR_PHY_rd_mon_status( uint32 _inst_, uint8 pass[1] )
{
    uint32 tmp;


    tmp = HWIO_INX (_inst_, DDR_PHY_DDRPHY_BISC_TRAINING_STA );
    pass[0] = (tmp >> 10) & 0x1;
}

// ***********************************************************************
/// ----------------------------------------------------------
///  Read valude of rcw coarse delay
/// ----------------------------------------------------------
// ***********************************************************************
uint8 DDR_PHY_hal_sta_rcw_fine( uint32 _inst_, uint8 rank )
{

    if (rank) {
        return (((HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCRCW_CTL_CFG ) & 0x7C00) >> 15));
    } else {
        return (((HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCRCW_CTL_CFG ) & 0x3E0) >> 5));
    }
}

// ***********************************************************************
/// ----------------------------------------------------------
///  Read value of rcw coarse delay
/// ----------------------------------------------------------
// ***********************************************************************
uint8 DDR_PHY_hal_sta_rcw_coarse( uint32 _inst_, uint8 rank )
{

    if (rank) {
        return (((HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCRCW_CTL_CFG ) & 0x7C00) >> 10));
    } else {
        return ((HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCRCW_CTL_CFG ) & 0x1F));
    }
}

// ***********************************************************************
/// ----------------------------------------------------------
///  Read value of number of half cycle
/// ----------------------------------------------------------
// ***********************************************************************
uint8 DDR_PHY_hal_sta_rcw_num_cycle( uint32 _inst_ )
{
    uint8 half;
    uint8 full;
     
    // half = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_RD_RANK_EN_CFG, RD_EN_HALF_CYCLE_R0);
    // full = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_RD_RANK_EN_CFG, RD_EN_FULL_CYCLE_R0);
    
    half = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_2_CFG, RD_EN_IST_HALF_CYCLE_R0);
    full = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_2_CFG, RD_EN_IST_FULL_CYCLE_R0);
	
	return (half + (full * 2));

   
}

// ***********************************************************************
/// ----------------------------------------------------------
///  HAL : CONFIG WRLV SLAVE
/// ----------------------------------------------------------
// ***********************************************************************
void DDR_PHY_hal_cfg_cdc_slave_wrlvl( uint32 _inst_, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint8 rank )
{
    uint32 cdc;
    uint32 mask;
    uint32 shift;

        // Read the CDC delay register
    cdc = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_CTL_CFG);

        mask  = DDR_PHY_hal_cfg_cdc_mask(coarse_delay, rank, hp_mode);
        shift = DDR_PHY_hal_cfg_cdc_shift(coarse_delay, rank, hp_mode);

        // Clear space for delay in CDC
        cdc = cdc & mask;

        // Shift delay to insert in CDC
        delay = delay<<shift;

        // Insert delay into CDC
        cdc = cdc | delay;

        // Write CDC delay register 
    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_CTL_CFG, cdc); 

    // Disable load 
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, LOAD, 0);

        // Gate the clock
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, GATE, 1);

        // Disable the register
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, EN  , 0);

        // Pulse load
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, LOAD, 1);
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, LOAD, 0);

        // Enable the register
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, EN  , 1);

        // Un-Gate the clock
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, GATE, 0);
}


/// ----------------------------------------------------------
///  HAL : CONFIG WR SLAVE
/// ----------------------------------------------------------
// ***********************************************************************
void DDR_PHY_hal_cfg_cdc_slave_wr( uint32 _inst_, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint8 rank )
{
    uint32 cdc;
    uint32 mask;
    uint32 shift;
     
    // Read the CDC delay register
    cdc = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG);

    mask  = DDR_PHY_hal_cfg_cdc_mask(coarse_delay, rank, hp_mode);
    shift = DDR_PHY_hal_cfg_cdc_shift(coarse_delay, rank, hp_mode);

    // Clear space for delay in CDC
    cdc = cdc & mask;

    // Shift delay to insert in CDC
    delay = delay<<shift;

    // Insert delay into CDC
    cdc = cdc | delay;

    // Write CDC delay register 
    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG, cdc); 

    // Disable load 
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG, LOAD, 0);
    
    // Gate the clock
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG, GATE, 1);
    
    // Disable the register
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG, EN  , 0);
    
    // Pulse load
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG, LOAD, 1);
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG, LOAD, 0);
    
    // Enable the register
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG, EN  , 1);
    
    // Un-Gate the clock
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG, GATE, 0);
    cdc = HWIO_INXF  (_inst_, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG, GATE);

}


// ***********************************************************************
/// ----------------------------------------------------------
///  HAL           : CONFIG RCW SLAVE
///  rank          : rank  0 - 1, select rank of ddr
///  delay         : delay value 0-31
///  coarse_delay  : 0: fine delay 1: coarse delay
///  hp_mode       : 0: lp mode, 1 : hp mode
/// ----------------------------------------------------------
// ***********************************************************************
void DDR_PHY_hal_cfg_cdc_slave_rcw( uint32 _inst_, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint8 rank )
{
    uint32 cdc;
    uint32 mask;
    uint32 shift;

        // Read the CDC delay register
    cdc = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCRCW_CTL_CFG);

        mask  = DDR_PHY_hal_cfg_cdc_mask(coarse_delay, rank, hp_mode);
        shift = DDR_PHY_hal_cfg_cdc_shift(coarse_delay, rank, hp_mode);

        // Clear space for delay in CDC
        cdc = cdc & mask;

        // Shift delay to insert in CDC
        delay = delay<<shift;

        // Insert delay into CDC
        cdc = cdc | delay;

        // Write CDC delay register 
    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CMCDCRCW_CTL_CFG, cdc); 

    // Disable load 
        // Gate the clock
        // Disable the register
    HWIO_OUTXF3 (_inst_, DDR_PHY_DDRPHY_CMCDCRCW_TOP_CFG, EN, LOAD, GATE, 0, 0, 1);

        // Pulse load
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCRCW_TOP_CFG, LOAD, 1);
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCRCW_TOP_CFG, LOAD, 0);

        // Enable the register
        // Un-Gate the clock
    HWIO_OUTXF2 (_inst_, DDR_PHY_DDRPHY_CMCDCRCW_TOP_CFG, GATE, EN, 0, 1);

}


// ***********************************************************************
/// ----------------------------------------------------------
///  HAL           : RCW
///  enable        : 1: enable rcw training, 0: disable
///  num_cycle     : 1: half cycle, 2: full cycle , 3: one and half cycle
/// ----------------------------------------------------------
// ***********************************************************************
void DDR_PHY_hal_cfg_cdc_rcw( uint32 _inst_, uint32 enable, uint32 num_cycle )
{
    if (enable) {
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCRDT2_I0_TOP_CFG, RCW_TYPE_EN, enable);    
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_2_CFG, LPDDR4_MODE, enable);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, RCW_TRAINING_EN, enable);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_2_CFG, RCW_COARSE_CTL, 0x0);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
    }
    else {
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_2_CFG, LPDDR4_MODE, enable);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, RCW_TRAINING_EN, enable);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_2_CFG, RCW_COARSE_CTL, 0x0);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCRDT2_I0_TOP_CFG, RCW_TYPE_EN, enable);    
    }
 
    if (enable) {
		HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_2_CFG, RD_EN_IST_HALF_CYCLE_R0, (num_cycle % 2));   
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_2_CFG, RD_EN_IST_FULL_CYCLE_R0, (num_cycle / 2)); 
	
      HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
      HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);  
    }
}

// ***********************************************************************
/// ----------------------------------------------------------
///  HAL : CONFIG RD SLAVE
/// ----------------------------------------------------------
// ***********************************************************************
void DDR_PHY_hal_cfg_cdc_slave_rd( uint32 _inst_, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint8 rank )
{
    uint32 cdc;
    uint32 mask;
    uint32 shift;

        // Read the CDC delay register
    cdc = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCRD_CTL_CFG);

        mask  = DDR_PHY_hal_cfg_cdc_mask(coarse_delay, rank, hp_mode);
        shift = DDR_PHY_hal_cfg_cdc_shift(coarse_delay, rank, hp_mode);

        // Clear space for delay in CDC
        cdc = cdc & mask;

        // Shift delay to insert in CDC
        delay = delay<<shift;

        // Insert delay into CDC
        cdc = cdc | delay;

        // Write CDC delay register 
    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CMCDCRD_CTL_CFG, cdc); 

    // Disable load 
        // Gate the clock
        // Disable the register
    HWIO_OUTXF3 (_inst_, DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, EN, LOAD, GATE, 0, 0, 1);

        // Pulse load
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, LOAD, 1);
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, LOAD, 0);

        // Enable the register
        // Un-Gate the clock
    HWIO_OUTXF2 (_inst_, DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, GATE, EN, 0, 1);

}

void DDR_PHY_hal_cfg_pbit_dq_delay( uint32 _inst_, uint16 dq_num, uint16 txrx, uint16 sel, uint16 delay )
{
    uint32 temp = 0;
    uint32 shift = 0;
    uint32 mask = 0;
    
    shift = (0x5 * sel);
    mask = (~(0x1F << shift));
  
    /// select source of rank select b/w memory controller and csr default is from memory controller
    /// DDRPHY_CMHUB_TOP_0_CFG.BIT_RANK_SRC_SEL = rank_src_sel;
    if (dq_num == 0) {
            if (txrx) 
            {          
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE0_PBIT_CTL_DQ_0_RXTX_CFG, TX0_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE0_PBIT_CTL_DQ_0_RXTX_CFG, TX0_CTL, temp);
            } 
            else 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE0_PBIT_CTL_DQ_0_RXTX_CFG, RX0_CTL);
                temp = ((delay << shift) | (mask & temp));                
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE0_PBIT_CTL_DQ_0_RXTX_CFG, RX0_CTL, temp);
            }

    }
    
    else if (dq_num == 1) {
            if (txrx) 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE1_PBIT_CTL_DQ_1_RXTX_CFG, TX1_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE1_PBIT_CTL_DQ_1_RXTX_CFG, TX1_CTL, temp);
            } 
            else 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE1_PBIT_CTL_DQ_1_RXTX_CFG, RX1_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE1_PBIT_CTL_DQ_1_RXTX_CFG, RX1_CTL, temp);
            }
    }
    
    else if (dq_num == 2) {
            if (txrx) 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE2_PBIT_CTL_DQ_2_RXTX_CFG, TX2_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE2_PBIT_CTL_DQ_2_RXTX_CFG, TX2_CTL, temp);
            }
            else 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE2_PBIT_CTL_DQ_2_RXTX_CFG, RX2_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE2_PBIT_CTL_DQ_2_RXTX_CFG, RX2_CTL, temp);
            }
    }
    
    else if (dq_num == 3) {
            if (txrx) 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE3_PBIT_CTL_DQ_3_RXTX_CFG, TX3_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE3_PBIT_CTL_DQ_3_RXTX_CFG, TX3_CTL, temp);
            } 
            else 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE3_PBIT_CTL_DQ_3_RXTX_CFG, RX3_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE3_PBIT_CTL_DQ_3_RXTX_CFG, RX3_CTL, temp);
            }
    }
    
    else if (dq_num == 4) {
            if (txrx) 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE4_PBIT_CTL_DQ_4_RXTX_CFG, TX4_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE4_PBIT_CTL_DQ_4_RXTX_CFG, TX4_CTL, temp);
            } 
            else 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE4_PBIT_CTL_DQ_4_RXTX_CFG, RX4_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE4_PBIT_CTL_DQ_4_RXTX_CFG, RX4_CTL, temp);
            }
    }
    
    else if (dq_num == 5) {
            if (txrx) 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE5_PBIT_CTL_DQ_5_RXTX_CFG, TX5_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE5_PBIT_CTL_DQ_5_RXTX_CFG, TX5_CTL, temp);
            } 
            else 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE5_PBIT_CTL_DQ_5_RXTX_CFG, RX5_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE5_PBIT_CTL_DQ_5_RXTX_CFG, RX5_CTL, temp);
            }
    }
    
    else if (dq_num == 6) {
            if (txrx) 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE6_PBIT_CTL_DQ_6_RXTX_CFG, TX6_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE6_PBIT_CTL_DQ_6_RXTX_CFG, TX6_CTL, temp);
            } 
            else 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE6_PBIT_CTL_DQ_6_RXTX_CFG, RX6_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE6_PBIT_CTL_DQ_6_RXTX_CFG, RX6_CTL, temp);
            }
    }
    
    else if (dq_num == 7) {
            if (txrx) 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE7_PBIT_CTL_DQ_7_RXTX_CFG, TX7_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE7_PBIT_CTL_DQ_7_RXTX_CFG, TX7_CTL, temp);
            } 
            else 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE7_PBIT_CTL_DQ_7_RXTX_CFG, RX7_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE7_PBIT_CTL_DQ_7_RXTX_CFG, RX7_CTL, temp);
            }
    }
    
    else if (dq_num == 8) {
            if (txrx) 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE8_PBIT_CTL_DQ_8_RXTX_CFG, TX8_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE8_PBIT_CTL_DQ_8_RXTX_CFG, TX8_CTL, temp);
            } 
            else 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE8_PBIT_CTL_DQ_8_RXTX_CFG, RX8_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE8_PBIT_CTL_DQ_8_RXTX_CFG, RX8_CTL, temp);
            }
    }
    
    else if (dq_num == 9) {
            if (txrx) 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE9_PBIT_CTL_DQ_9_RXTX_CFG, TX9_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE9_PBIT_CTL_DQ_9_RXTX_CFG, TX9_CTL, temp);
            } 
            else 
            {
                temp = HWIO_INXF(_inst_, DDR_PHY_DDRPHY_CMSLICE9_PBIT_CTL_DQ_9_RXTX_CFG, RX9_CTL);
                temp = ((delay << shift) | (mask & temp));
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMSLICE9_PBIT_CTL_DQ_9_RXTX_CFG, RX9_CTL, temp);
            }
    }
}
// ***********************************************************************
/// DDRPHY write leveling ROUTINE
/// seq_msg(INFO, 0, "DDRPHY Write Leveling CFG delay value setting");
/// -----------------------------------------------------------------
///     HALs for write leveling ROUTINE
/// 
/// -----------------------------------------------------------------
// ***********************************************************************
uint32 DDR_PHY_hal_sta_wrlvl_coarse( uint32 _inst_, uint8 rank )
{

    if (rank) {
        return (((HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_CTL_CFG) & 0x7C00) >> 10));
    } else {
        return ((HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_CTL_CFG) & 0x1F));
    }
}


uint32 DDR_PHY_hal_sta_wrlvl_fine( uint32 _inst_, uint8 rank )
{

    if (rank) {
        return (((HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_CTL_CFG) & 0xF8000) >> 15));
    } else {
        return (((HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMCDCWRLVL_CTL_CFG) & 0x3E0) >> 5));
    }
}


uint32 DDR_PHY_hal_sta_wrlvl_training( uint32 _inst_ )
{

    return ((HWIO_INXF (_inst_, DDR_PHY_DDRPHY_BISC_TRAINING_STA, TRAINING_STATUS) & 0xFF));
}


uint32 DDR_PHY_hal_sta_wrlvl_retmr( uint32 _inst_, uint8 rank )
{
    uint32 retmr;


    if (rank) {
        retmr = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQS_RETMR_R1);
    } else {
        retmr = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQS_RETMR_R0);
    }
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
    
    return ((retmr));
}


uint32 DDR_PHY_hal_sta_wrlvl_dq_retmr( uint32 _inst_, uint8 rank )
{
    uint32 retmr;


    if (rank) {
        retmr = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_RETMR_R1);
    } else {
        retmr = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_RETMR_R0);
    }
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
    
    return ((retmr));
}


uint32 DDR_PHY_hal_sta_wrlvl_half( uint32 _inst_, uint8 rank )
{
    uint32 half;


    if (rank) {
        half = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQS_HALF_CYCLE_R1);
    } else {
        half = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQS_HALF_CYCLE_R0);
    }
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
    
    return ((half));
}

uint32 DDR_PHY_hal_sta_wrlvl_dq_half( uint32 _inst_, uint8 rank )
{
    uint32 half;


    if (rank) {
        half = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_HALF_CYCLE_R1);
    } else {
        half = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_HALF_CYCLE_R0);
    }
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
    
    return ((half));
}

uint32 DDR_PHY_hal_sta_wrlvl_full( uint32 _inst_, uint8 rank )
{
    uint32 full;


    if (rank) {
        full = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQS_FULL_CYCLE_R1);
    } else {
        full = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQS_FULL_CYCLE_R0);
    }
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
    
    return ((full));
}


uint32 DDR_PHY_hal_sta_wrlvl_dq_full( uint32 _inst_, uint8 rank )
{
    uint32 full;


    if (rank) {
        full = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_FULL_CYCLE_R1);
    } else {
        full = HWIO_INXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_FULL_CYCLE_R0);
    }
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
    HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
    
    return ((full));
}


uint32 DDR_PHY_cdcext_shift_mask( uint16 rank, uint16 coarse_delay, uint16 hp_mode, uint8 shift_mask)
{
    uint32 shift;
    uint32 mask;

    ///  Decode Mode, Delay and Rank
    if (hp_mode == 1) {
        if (coarse_delay == 1) {
            ///  Rank 0
            if (rank == 1) {
                shift = 10; // 0xA
                                mask  = 0xFFFF83FF; 
            } else {
                shift = 0;
                                mask  = 0xFFFFFFE0; 
            }
        } else         ///  Fine delay
        if (rank == 1) {
            shift = 15; // 0xF
                        mask  = 0xFFF07FFF; 
        } else {
            shift = 5;
                        mask  = 0xFFFFFC1F; 
        }
    } else     if (rank == 1) {
        shift = 25; // 0x19
                mask  = 0xC1FFFFFF; 
    } else {
        shift = 20; // 0x12
                mask  = 0xFE0FFFFF; 
    }
    ///  Return selected range
    if (shift_mask == 0) {
        return (shift);
    } 
        else  {
        return (mask);
    }
}

uint32 DDR_PHY_cdcext_shift_mask_reg(uint32 reg, uint32 delay, uint32 shift, uint32 mask)
{
  reg   = reg & mask;
  delay = delay << shift;
  reg   = reg | delay;
  
  return (reg);
  
}

void DDR_PHY_hal_cfg_cdcext_slave_rd( uint32 _inst_, uint16 rank, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint16 prfs_no )
{
    uint32 reg;
    uint32 shift;
    uint32 mask;

    shift = DDR_PHY_cdcext_shift_mask(rank, coarse_delay, hp_mode, 0);
    mask  = DDR_PHY_cdcext_shift_mask(rank, coarse_delay, hp_mode, 1);
    
    if (prfs_no == 0) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_0_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_0_CTL_CFG, reg);
    }
    else if (prfs_no == 1) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_1_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_1_CTL_CFG, reg);
    }
    else if (prfs_no == 2) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_2_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_2_CTL_CFG, reg);
    }
    else if (prfs_no == 3) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_3_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_3_CTL_CFG, reg);
    }
    else if (prfs_no == 4) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_4_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_4_CTL_CFG, reg);
    }
    else if (prfs_no == 5) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_5_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_5_CTL_CFG, reg);
    }
    else if (prfs_no == 6) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_6_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_6_CTL_CFG, reg);
    }
    else if (prfs_no == 7) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_7_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_RD_7_CTL_CFG, reg);
    }
}


void DDR_PHY_hal_cfg_cdcext_slave_wr( uint32 _inst_, uint16 rank, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint16 prfs_no )
{
    uint32 reg;
    uint32 shift;
    uint32 mask;

    shift = DDR_PHY_cdcext_shift_mask(rank, coarse_delay, hp_mode, 0);
    mask  = DDR_PHY_cdcext_shift_mask(rank, coarse_delay, hp_mode, 1);        

    if (prfs_no == 0) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_0_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_0_CTL_CFG, reg);
    }
    else if (prfs_no == 1) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_1_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_1_CTL_CFG, reg);
    }
    else if (prfs_no == 2) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_2_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_2_CTL_CFG, reg);
    }
    else if (prfs_no == 3) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_3_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_3_CTL_CFG, reg);
    }
    else if (prfs_no == 4) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_4_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_4_CTL_CFG, reg);
    }
    else if (prfs_no == 5) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_5_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_5_CTL_CFG, reg);
    }
    else if (prfs_no == 6) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_6_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_6_CTL_CFG, reg);
    }
    else if (prfs_no == 7) {
          reg = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_7_CTL_CFG);
          reg = DDR_PHY_cdcext_shift_mask_reg(reg,delay,shift,mask);
          HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CDCEXT_WR_7_CTL_CFG, reg);
    }
}

void DDR_PHY_hal_cfg_wrlvlext_ctl_update( uint32 _inst_, uint8 period_index, uint8 rank, uint32 retmr, uint32 half_cycle, uint32 full_cycle )
{

    ///  wrlvl extension retmr and delay values
      if (period_index == 0) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQS_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQS_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQS_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQS_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQS_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQS_FULL_CYCLE_R0, full_cycle);
            }
      }
      else if (period_index == 1) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQS_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQS_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQS_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQS_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQS_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQS_FULL_CYCLE_R0, full_cycle);
            }
      }
      else if (period_index == 2) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQS_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQS_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQS_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQS_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQS_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQS_FULL_CYCLE_R0, full_cycle);
            }
      }
      else if (period_index == 3) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQS_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQS_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQS_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQS_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQS_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQS_FULL_CYCLE_R0, full_cycle);
            }
      }
      else if (period_index == 4) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQS_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQS_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQS_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQS_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQS_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQS_FULL_CYCLE_R0, full_cycle);
            }
      }
      else if (period_index == 5) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQS_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQS_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQS_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQS_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQS_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQS_FULL_CYCLE_R0, full_cycle);
            }
      }
      else if (period_index == 6) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQS_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQS_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQS_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQS_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQS_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQS_FULL_CYCLE_R0, full_cycle);
            }
      }
      else {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQS_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQS_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQS_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQS_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQS_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQS_FULL_CYCLE_R0, full_cycle);
            }
      }
}


void DDR_PHY_hal_cfg_wrext_ctl_update( uint32 _inst_, uint8 period_index, uint8 rank, uint32 retmr, uint32 half_cycle, uint32 full_cycle )
{

    ///  wrlvl extension retmr and delay values
    if (period_index == 0) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQ_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQ_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQ_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQ_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQ_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQ_FULL_CYCLE_R0, full_cycle);
            }
    }
    else if (period_index == 1) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQ_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQ_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQ_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQ_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQ_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQ_FULL_CYCLE_R0, full_cycle);
            }
    }
    else if (period_index == 2) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQ_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQ_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQ_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQ_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQ_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQ_FULL_CYCLE_R0, full_cycle);
            }
    }
    else if (period_index == 3) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQ_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQ_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQ_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQ_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQ_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQ_FULL_CYCLE_R0, full_cycle);
            }
    }
    else if (period_index == 4) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQ_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQ_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQ_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQ_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQ_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQ_FULL_CYCLE_R0, full_cycle);
            }
    }
    else if (period_index == 5) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQ_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQ_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQ_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQ_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQ_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQ_FULL_CYCLE_R0, full_cycle);
            }
    }
    else if (period_index == 6) {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQ_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQ_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQ_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQ_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQ_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQ_FULL_CYCLE_R0, full_cycle);
            }
    }
    else {
            if (rank == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQ_RETMR_R1, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQ_HALF_CYCLE_R1, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQ_FULL_CYCLE_R1, full_cycle);
            } else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQ_RETMR_R0, retmr);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQ_HALF_CYCLE_R0, half_cycle);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQ_FULL_CYCLE_R0, full_cycle);
            }
    }
}


void DDR_PHY_hal_cfg_wrlvl_full( uint32 _inst_, uint32 rank, uint32 dq_full_cycle )
{
   if (rank == 1) {
       HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_FULL_CYCLE_R1, dq_full_cycle);
   } else {
       HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_FULL_CYCLE_R0, dq_full_cycle);
   }
   HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
   HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);

}


void DDR_PHY_hal_cfg_wrlvlext_ctl_dqs_dq_struct_half( uint32 _inst_, uint8 period_index, uint8 dqs_half, uint8 dq_half )
{

    ///  wrlvl extension half
      if (period_index == 0) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQS_STRUCT_HALF_CYCLE_R1,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQS_STRUCT_HALF_CYCLE_R0,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQ_STRUCT_HALF_CYCLE_R1, dq_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG, DQ_STRUCT_HALF_CYCLE_R0, dq_half);
      }
      else if (period_index == 1) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQS_STRUCT_HALF_CYCLE_R1,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQS_STRUCT_HALF_CYCLE_R0,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQ_STRUCT_HALF_CYCLE_R1, dq_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG, DQ_STRUCT_HALF_CYCLE_R0, dq_half);
      }
      else if (period_index == 2) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQS_STRUCT_HALF_CYCLE_R1,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQS_STRUCT_HALF_CYCLE_R0,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQ_STRUCT_HALF_CYCLE_R1, dq_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG, DQ_STRUCT_HALF_CYCLE_R0, dq_half);
      }
      else if (period_index == 3) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQS_STRUCT_HALF_CYCLE_R1,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQS_STRUCT_HALF_CYCLE_R0,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQ_STRUCT_HALF_CYCLE_R1, dq_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG, DQ_STRUCT_HALF_CYCLE_R0, dq_half);
      }
      else if (period_index == 4) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQS_STRUCT_HALF_CYCLE_R1,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQS_STRUCT_HALF_CYCLE_R0,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQ_STRUCT_HALF_CYCLE_R1, dq_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG, DQ_STRUCT_HALF_CYCLE_R0, dq_half);
      }
      else if (period_index == 5) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQS_STRUCT_HALF_CYCLE_R1,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQS_STRUCT_HALF_CYCLE_R0,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQ_STRUCT_HALF_CYCLE_R1, dq_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQ_STRUCT_HALF_CYCLE_R0, dq_half);
      }
      else if (period_index == 6) {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQS_STRUCT_HALF_CYCLE_R1,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQS_STRUCT_HALF_CYCLE_R0,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQ_STRUCT_HALF_CYCLE_R1, dq_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG, DQ_STRUCT_HALF_CYCLE_R0, dq_half);
      }
      else {
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQS_STRUCT_HALF_CYCLE_R1,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQS_STRUCT_HALF_CYCLE_R0,dqs_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQ_STRUCT_HALF_CYCLE_R1, dq_half);
                HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG, DQ_STRUCT_HALF_CYCLE_R0, dq_half);
      }
}
