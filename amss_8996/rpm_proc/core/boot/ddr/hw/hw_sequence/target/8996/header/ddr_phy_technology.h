/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014,2016 Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                              EDIT HISTORY
$Header: //components/rel/rpm.bf/1.6/core/boot/ddr/hw/hw_sequence/target/8996/header/ddr_phy_technology.h#18 $
$DateTime: 2016/05/03 05:33:38 $
$Author: pwbldsvc $

==============================================================================*/

#define VREF_MP_HP                  2
#define PINS_PER_PHY_CONNECTED_CA   6

#define CA_PATTERN_NUM              4 

#define COARSE_VREF_MAX_VALUE       0x37
#define COARSE_VREF_MAX_VALUE_WRITE 0x32 //Limit VREF to max number of possible steps.
#define COARSE_CDC_MAX_VALUE        0x18
#define COARSE_VREF                 (COARSE_VREF_MAX_VALUE + 1)
#define COARSE_CDC                  (COARSE_CDC_MAX_VALUE + 1)

#define FINE_VREF_MAX_VALUE         0x8 
#define FINE_CDC_MAX_VALUE          0x8 
#define FINE_VREF                   (FINE_VREF_MAX_VALUE + 1)
#define FINE_CDC                    (FINE_CDC_MAX_VALUE + 1)
#define FINE_RD_CDC                 0x10
#define PERBIT_CDC_MAX_VALUE        0xF 
#define DBI_WR_PBIT_OFFSET          0

#define COARSE_STEP_IN_PS           50
#define FINE_STEP_IN_PS             8
#define FINE_STEPS_PER_COARSE       6   //50/8.

#define WRITE_FINE_CDC_MIN          5
#define WRITE_FINE_CDC_MAX          10

#define DQS_VREF                    0xF
#define MIN_DTTS_TRACKING_PRFS      0x6

#define LOW_SPEED_HALF_CYC_CONV_THRESHOLD       450
#define LOW_SPEED_NO_HALF_CYC_COARSE_CDC_VALUE  0x10 //0x12 //0x14 //0x10
#define LOW_SPEED_NO_HALF_CYC_FINE_CDC_VALUE    0x0
#define LOW_SPEED_RD_COARSE_CDC_VALUE           0xA


#define F_RANGE_0                   250000
#define F_RANGE_1                   350000
#define F_RANGE_2                   600000
#define F_RANGE_3                   800000
#define F_RANGE_4                   1066000
#define F_RANGE_5                   1488000
#define F_RANGE_6                   1603000
#define F_RANGE_7                   1894000

