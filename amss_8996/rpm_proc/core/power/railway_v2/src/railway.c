// railway.c - railway top-level implementation
//
// Copyright 2011 - 2012 by QUALCOMM Technologies, Inc.
// All Rights Reserved
// Confidental and Proprietary
//
// $ Header: $
// $ Author: $
// $ DateTime: $
//

#include "CoreVerify.h"
#include "DALSys.h"

#include "railway.h"

#include "railway_internal.h"
#include "railway_config.h"
#include "rpmserver.h"
#include "swevent.h"
#include "alloca.h"
#include "pm_npa_irep.h"


__attribute__((section("railway_dram_reclaim_pool")))
static void initialize_rail(uint32 rail_num)
{
    const railway_rail_config_t *rail_config = &RAILWAY_CONFIG_DATA->rails[rail_num];

    railway_rail_state_t* rail_state = &railway.rail_state[rail_num];

    memcpy(rail_state->corner_uvs, rail_config->default_uvs, sizeof(uint32[RAILWAY_CORNERS_COUNT]));

    // Compute what our initial request should be.
    railway_corner corner = rail_config->initial_corner;
    uint32 uv             = rail_state->corner_uvs[corner];

    // Update our initial state.
    rail_state->current_active.mode       = corner;
    rail_state->current_active.microvolts = uv;

    rail_state->rail_server_handle = rpm_create_client(rail_config->vreg_type, rail_config->vreg_num, 0);
    
    switch(rail_config->vreg_type & VREG_TYPE_MASK)
    {
        case VREG_TYPE_SMPS:
        {
            rail_state->rail_pmic_server_vote = malloc(sizeof(pm_npa_smps_int_rep));
            CORE_VERIFY_PTR(rail_state->rail_pmic_server_vote);
            memset(rail_state->rail_pmic_server_vote, 0, sizeof(pm_npa_smps_int_rep));
        }
        break;
        case VREG_TYPE_LDO:
        {
            rail_state->rail_pmic_server_vote = malloc(sizeof(pm_npa_ldo_int_rep));
            CORE_VERIFY_PTR(rail_state->rail_pmic_server_vote);
            memset(rail_state->rail_pmic_server_vote, 0, sizeof(pm_npa_ldo_int_rep));
        }
        break;
        default:
            CORE_VERIFY(0); //Bad rail type
    }
}

void railway_set_pmic_voltage(int rail_num, unsigned microvolts)
{
    const railway_rail_config_t *rail_config = &RAILWAY_CONFIG_DATA->rails[rail_num];
    const railway_rail_state_t *rail_data = &railway.rail_state[rail_num];
    
    switch(rail_config->vreg_type & VREG_TYPE_MASK)
    {
        case VREG_TYPE_SMPS:
        {
            pm_npa_smps_int_rep* request = (pm_npa_smps_int_rep*)rail_data->rail_pmic_server_vote;

            if(microvolts)
                request->sw_en = TRUE;
            else
                request->sw_en = FALSE;
            request->uvol = microvolts;
            
            rpm_issue_request(rail_config->vreg_type, rail_config->vreg_num, rail_data->rail_server_handle, sizeof(pm_npa_smps_int_rep), request);       
        }
        break;
        case VREG_TYPE_LDO:
        {
            pm_npa_ldo_int_rep* request = (pm_npa_ldo_int_rep*)rail_data->rail_pmic_server_vote;
        
            if(microvolts)
                request->sw_en = TRUE;
            else
                request->sw_en = FALSE;
            request->output_uvol = microvolts;
            
            rpm_issue_request(rail_config->vreg_type, rail_config->vreg_num, rail_data->rail_server_handle, sizeof(pm_npa_ldo_int_rep), request);
        }
        break;
        default:
            CORE_VERIFY(0); //Bad rail type
    }
}

void railway_transition_individual_rail(int rail_num)
{
    railway_rail_state_t *rail_data = &railway.rail_state[rail_num];
    const railway_settings* target = &rail_data->constrained_target;
    unsigned target_voltage = target->microvolts;

    railway_walk_cbs(rail_data, RAILWAY_PRECHANGE_CB, target);

    SWEVENT(RAILWAY_CHANGE_VOLTAGE, rail_num, target->microvolts);

    // Only set the actual voltage if it's changing. It could be that we're just changing modes.
    // E.g. if CPR has taken the voltage down in one mode to the point that it is the same voltage as another mode.
    if(target_voltage!=railway.rail_state[rail_num].current_active.microvolts)
    {
        railway_track_rail_residence(rail_num, target->mode, railway.rail_state[rail_num].current_active.mode);	    
        railway_set_pmic_voltage(rail_num, target_voltage);
    }
    
    memcpy(&rail_data->current_active, target, sizeof(railway_settings));
    railway_walk_cbs(rail_data, RAILWAY_POSTCHANGE_CB, target);
}

void railway_quantize_constrained_target(int rail)
{
    const railway_corner* supported_corners = RAILWAY_CONFIG_DATA->rails[rail].supported_corners;
    const railway_corner highest_supported_corner = supported_corners[RAILWAY_CONFIG_DATA->rails[rail].supported_corners_count-1];

    //Assert that we don't have an explicit request greater than our highest supported corner.
    CORE_VERIFY(railway.rail_state[rail].corner_uvs[highest_supported_corner]
            >=railway.rail_state[rail].constrained_target.microvolts);

    //Assert that we don't have a vote for a corner higher than our highest-supported-corner
    CORE_VERIFY(highest_supported_corner>=railway.rail_state[rail].constrained_target.mode);

    railway_corner quantized_corner=highest_supported_corner;

    // -2 because we don't need to check the highest supported corner - we start there.
    for(int i=RAILWAY_CONFIG_DATA->rails[rail].supported_corners_count-2; i>=0; i--)
    {
        //Check that we're not going lower than the constrained target's mode.
        if(supported_corners[i]<railway.rail_state[rail].constrained_target.mode)
        {
            break;
        }

        if(railway.rail_state[rail].constrained_target.microvolts>
            railway.rail_state[rail].corner_uvs[supported_corners[i]])
        {
            break;
        }
        quantized_corner = supported_corners[i];
    }

    railway.rail_state[rail].constrained_target.mode = quantized_corner;
    railway.rail_state[rail].constrained_target.microvolts = railway.rail_state[rail].corner_uvs[quantized_corner];
}

bool railway_transition_in_progress = false;
static bool railway_init_locked = true;

void railway_unlock(void)
{
    railway_init_locked = false;
}

//Top level fn for transitioning a rail. Actually transitions all rails, not just rail_num.
void railway_transition_rail(int rail_num)
{
    CORE_VERIFY(!railway_init_locked);
    CORE_VERIFY(!railway_transition_in_progress);    //We don't support this fn becoming re-entrant for now. Guard against it.
    railway_transition_in_progress = true;

    CORE_VERIFY_PTR(railway.rail_state[rail_num].rail_group_fns);
    
    railway.rail_state[rail_num].rail_group_fns->transition_function();
    
    railway_transition_in_progress = false;
}

__attribute__((section("railway_dram_reclaim_pool")))
void railway_transitioner_init(void)
{
    for(uint32 i = 0; i < RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        initialize_rail(i);
    }
}

void railway_get_sleep_modes(int cx_id, int mx_id, railway_corner *cx_mode, railway_corner *mx_mode)
{
    CORE_VERIFY(cx_id < RAILWAY_CONFIG_DATA->num_rails);
    CORE_VERIFY(mx_id < RAILWAY_CONFIG_DATA->num_rails);

    const railway_rail_config_t *cx_config = &RAILWAY_CONFIG_DATA->rails[cx_id];
    const railway_rail_config_t *mx_config = &RAILWAY_CONFIG_DATA->rails[mx_id];

    *cx_mode = railway_aggregated_voltage_target(cx_id, true);
    *mx_mode = railway_aggregated_voltage_target(mx_id, true);

    for(unsigned i = 0; i < cx_config->supported_corners_count; i++)
    {

      if((*cx_mode) <= cx_config->supported_corners[i])
      {
          *cx_mode = cx_config->supported_corners[i];
          break;          		  
      }	      
    }	    

    for(unsigned i = 0; i < mx_config->supported_corners_count; i++)
    {

      if((*mx_mode) <= mx_config->supported_corners[i])
      {
          *mx_mode = mx_config->supported_corners[i];
          break;          		  
      }	      
    }	    
}

railway_corner railway_get_lowest_active(int rail)
{
    int index = 0;
    CORE_VERIFY(rail<RAILWAY_CONFIG_DATA->num_rails);
    railway_corner next_corner = RAILWAY_CONFIG_DATA->rails[rail].supported_corners[index];

    while((next_corner == RAILWAY_RETENTION) || (next_corner == RAILWAY_NO_REQUEST))
    {
        index++;
        CORE_VERIFY(index < RAILWAY_CONFIG_DATA->rails[rail].supported_corners_count);
        next_corner = RAILWAY_CONFIG_DATA->rails[rail].supported_corners[index];
    }
    CORE_VERIFY(next_corner < RAILWAY_CORNERS_COUNT);
    return next_corner;
}

