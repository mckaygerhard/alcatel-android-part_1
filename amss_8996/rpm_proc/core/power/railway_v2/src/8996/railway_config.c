/*
===========================================================================

FILE:         railway_config.c

DESCRIPTION:
  Per target railway configurations

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/power/railway_v2/src/8996/railway_config.c#15 $
$Date: 2015/07/08 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include "CoreVerify.h"
#include "kvp.h"
#include "rpmserver.h"
#include "railway_config.h"
#include "railway_core_rails.h"
#include "railway_ssc_rails.h"
#include "image_layout.h"
#include "time_service.h"
#include "pm_railway.h"


/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */
#define RAIL_RESIDENCE_COUNT 2
const railway_rail_groups railway_8996_rail_groups = 
{
    .group_fns = (const railway_rail_group_fns*[])
    {
        &railway_core_rail_fns,
        &railway_ssc_rail_fns
    },
    .num_groups = 2,
};

const railway_rail_groups * const RAILWAY_RAIL_GROUP_DATA = &railway_8996_rail_groups;


//
// BEGIN config data; should migrate to the system enumeration data method
//
__attribute__((section("cram_save_pool")))
static const railway_config_data_t temp_config_data =
{
    .rails     = (railway_rail_config_t[])
    {
        // Must init VDDMX first, as voting on the other rails will cause Mx changes to occur.
        {
            .vreg_name      = "vddmx",

            .vreg_type      = RPM_SMPS_A_REQ,
            .vreg_num       = 2,

            .pmic_step_size = 5000,     // not used

            .initial_corner = RAILWAY_NOMINAL,

            .default_uvs = (const uint32[])
            {
                0,                      // RAILWAY_NO_REQUEST
                590000,                 // RAILWAY_RETENTION          - rounded 587500
                850000,                 // RAILWAY_SVS_KRAIT
                850000,                 // RAILWAY_SVS_SOC
                850000,                 // RAILWAY_SVS_HIGH
                915000,                 // RAILWAY_NOMINAL            - rounded 912500
                990000,                 // RAILWAY_TURBO              - rounded 987500
                990000,                 // RAILWAY_TURBO_HIGH         - rounded 987500
                990000,                 // RAILWAY_SUPER_TURBO        - rounded 987500
                990000,                 // RAILWAY_SUPER_TURBO_NO_CPR - rounded 987500
            },

            .supported_corners = (railway_corner[])
            {
                RAILWAY_RETENTION,
                RAILWAY_SVS_SOC,
                RAILWAY_NOMINAL,
                RAILWAY_SUPER_TURBO,
            },
            .supported_corners_count = 4,
        },

        // VDDCX
        {
            .vreg_name      = "vddcx",

            .vreg_type      = RPM_SMPS_A_REQ,
            .vreg_num       = 1,

            .pmic_step_size = 5000,     // not used

            .initial_corner = RAILWAY_NOMINAL,

            .default_uvs = (const uint32[])
            {
                0,                      // RAILWAY_NO_REQUEST
                400000,                 // RAILWAY_RETENTION
                640000,                 // RAILWAY_SVS_KRAIT          - rounded 637500
                715000,                 // RAILWAY_SVS_SOC            - rounded 712500
                715000,                 // RAILWAY_SVS_HIGH           - rounded 712500
                865000,                 // RAILWAY_NOMINAL            - rounded 862500
                990000,                 // RAILWAY_TURBO              - rounded 987500
                990000,                 // RAILWAY_TURBO_HIGH         - rounded 987500
                990000,                 // RAILWAY_SUPER_TURBO        - rounded 987500
                990000,                 // RAILWAY_SUPER_TURBO_NO_CPR - rounded 987500
            },

            .supported_corners = (railway_corner[])
            {
                RAILWAY_RETENTION,
                RAILWAY_SVS_KRAIT,      // aka RAILWAY_SVS_LOW or SVS2
                RAILWAY_SVS_SOC,
                RAILWAY_NOMINAL,
                RAILWAY_SUPER_TURBO,
            },
            .supported_corners_count = 5,
        },

        // VDDA_EBI
        {
            .vreg_name      = "vdda_ebi",

            .vreg_type      = RPM_LDO_A_REQ,
            .vreg_num       = 3,

            .pmic_step_size = 12500,    // not used

            .initial_corner = RAILWAY_RETENTION,

            .default_uvs = (const uint32[])
            {
                0,                      // RAILWAY_NO_REQUEST
                587500,                 // RAILWAY_RETENTION
                850000,                 // RAILWAY_SVS_KRAIT
                850000,                 // RAILWAY_SVS_SOC
                850000,                 // RAILWAY_SVS_HIGH
                912500,                 // RAILWAY_NOMINAL
                987500,                 // RAILWAY_TURBO
                987500,                 // RAILWAY_TURBO_HIGH
                987500,                 // RAILWAY_SUPER_TURBO
                987500,                 // RAILWAY_SUPER_TURBO_NO_CPR
            },

            .supported_corners = (railway_corner[])
            {
                  RAILWAY_RETENTION,
                  RAILWAY_SVS_KRAIT,
                  RAILWAY_SVS_SOC,
                  RAILWAY_NOMINAL,
                  RAILWAY_SUPER_TURBO,
            },
            .supported_corners_count = 5,
        },
        // VDD_SSC_MX
        {
            .vreg_name      = "vdd_ssc_mx",

            .vreg_type      = RPM_LDO_A_REQ,
            .vreg_num       = 31,

            .pmic_step_size = 12500,     // not used

            .initial_corner = RAILWAY_NO_REQUEST,

            .default_uvs = (const uint32[])
            {
                0,                      // RAILWAY_NO_REQUEST
                587500,                 // RAILWAY_RETENTION          - rounded 587500
                850000,                 // RAILWAY_SVS_KRAIT
                850000,                 // RAILWAY_SVS_SOC
                850000,                 // RAILWAY_SVS_HIGH
                912500,                 // RAILWAY_NOMINAL
                987500,                 // RAILWAY_TURBO
                987500,                 // RAILWAY_TURBO_HIGH
                987500,                 // RAILWAY_SUPER_TURBO
                987500,                 // RAILWAY_SUPER_TURBO_NO_CPR
            },

            .supported_corners = (railway_corner[])
            {
                RAILWAY_NO_REQUEST,
                RAILWAY_RETENTION,
                RAILWAY_SVS_SOC,
                RAILWAY_NOMINAL,
                RAILWAY_SUPER_TURBO,
            },
            .supported_corners_count = 5,
        },
        // VDD_SSC_CX
        {
            .vreg_name      = "vdd_ssc_cx",

            .vreg_type      = RPM_LDO_A_REQ,
            .vreg_num       = 26,

            .pmic_step_size = 12500,     // not used

            .initial_corner = RAILWAY_NO_REQUEST,

            .default_uvs = (const uint32[])
            {
                0,                      // RAILWAY_NO_REQUEST
                400000,                 // RAILWAY_RETENTION
                650000,                 // RAILWAY_SVS_KRAIT
                725000,                 // RAILWAY_SVS_SOC
                725000,                 // RAILWAY_SVS_HIGH
                862500,                 // RAILWAY_NOMINAL
                987500,                 // RAILWAY_TURBO
                987500,                 // RAILWAY_TURBO_HIGH
                987500,                 // RAILWAY_SUPER_TURBO
                987500,                 // RAILWAY_SUPER_TURBO_NO_CPR
            },

            .supported_corners = (railway_corner[])
            {
                RAILWAY_NO_REQUEST,
                RAILWAY_RETENTION,
                RAILWAY_SVS_KRAIT,      // aka RAILWAY_SVS_LOW or SVS2
                RAILWAY_SVS_SOC,
                RAILWAY_NOMINAL,
                RAILWAY_SUPER_TURBO,
            },
            .supported_corners_count = 6,
        },
    },

    .num_rails = 5,
};
//
// END config data
//

__attribute__((section("cram_save_pool")))
const railway_config_data_t * const RAILWAY_CONFIG_DATA = &temp_config_data;

/* -----------------------------------------------------------------------
**                           FUNCTIONS
** ----------------------------------------------------------------------- */

void railway_init_early_proxy_votes(void)
{
}

void railway_init_proxies_and_pins(void)
{
    kvp_t              *kvp     = kvp_create(0);
    uint32              cx_req  = (uint32)RAILWAY_NOMINAL;

    kvp_clear(kvp);

    // Init VDDCX at NOMINAL on behalf of APPS
    kvp_reset(kvp);
    kvp_put(kvp, RAILWAY_INTERNAL_KEY_CORNER_LEVEL_KEY, sizeof(uint32), (void *)&cx_req);
    rpm_send_init_proxy_vote(RPM_SMPS_A_REQ,  // Cx is S1
                             1,
                             0,               // APPS is 0
                             kvp);
}

typedef struct
{
    uint64 time;
    uint32 corner;
    uint32 reserved;
} corner_stat;

typedef struct
{
    uint32 rail;
    uint32 num_corners;
    uint32 current_corner;
    uint32 last_entered;
} residence_info_type;

typedef struct
{
    uint32 num_rails;	
    uint32 reserved;	
    residence_info_type mx;
    corner_stat mx_residence[4];
    residence_info_type cx;
    corner_stat cx_residence[5];
} rail_residence_type;

static rail_residence_type* rail_residence;

static unsigned get_residence_id(railway_corner corner, const railway_rail_config_t *rail_config)
{
    for(unsigned i = 0; i < rail_config->supported_corners_count; i++)
    {    
        if(rail_config->supported_corners[i] == corner)
        {
            return i;
        }
    }
    abort(); //something is out of wack. We are trying to save residencies for an unsupported corner
}

void railway_track_rail_residence(int rail_num, railway_corner new_corner, railway_corner old_corner)
{
    uint64 now = time_service_now();
    const railway_rail_config_t *rail_config = &RAILWAY_CONFIG_DATA->rails[rail_num];
    switch(rail_num)
    {
        case 0: //index in temp_config_data for mx
             if(rail_residence->mx.current_corner == RAILWAY_CORNERS_COUNT)
               rail_residence->mx_residence[get_residence_id(old_corner, rail_config)].time = now;
             else		  
               rail_residence->mx_residence[rail_residence->mx.current_corner].time += (now - rail_residence->mx.last_entered);
             rail_residence->mx.current_corner = get_residence_id(new_corner, rail_config);
             rail_residence->mx.last_entered = now;
             break;
        case 1: //index in temp_config_data for cx
             if(rail_residence->cx.current_corner == RAILWAY_CORNERS_COUNT)
               rail_residence->cx_residence[get_residence_id(old_corner, rail_config)].time = now;
             else		  
               rail_residence->cx_residence[rail_residence->cx.current_corner].time += (now - rail_residence->cx.last_entered);
             rail_residence->cx.current_corner = get_residence_id(new_corner, rail_config);
             rail_residence->cx.last_entered = now;
             break;
        default:      
              break;
    }
}

__attribute__((section("railway_dram_reclaim_pool")))
static void railway_residence_init()
{
  rail_residence = (rail_residence_type*)rpm_image_section_array[RPM_RAIL_RESIDENCIES].section_base;
  memset((void*)rail_residence, 0, sizeof(rail_residence_type));
  CORE_VERIFY(((char*)rail_residence+sizeof(rail_residence_type)) <= rpm_image_section_array[RPM_RAIL_RESIDENCIES].section_end);

  rail_residence->num_rails = 2; 
  rail_residence->mx.rail = 0x0000786d; // "mx" in little endian
  rail_residence->cx.rail = 0x00007863; // "cx" in little endian
  rail_residence->mx.num_corners = 4; 
  rail_residence->cx.num_corners = 5; 

  //MX config
  rail_residence->mx_residence[0].corner = 0x00746572; // "ret" in little endian
  rail_residence->mx_residence[1].corner = 0x00737673; // "svs" in little endian
  rail_residence->mx_residence[2].corner = 0x006d6f6e; // "nom" in little endian 
  rail_residence->mx_residence[3].corner = 0x62727574; // "turb" in little endian
  rail_residence->mx_residence[0].time = 0;
  rail_residence->mx_residence[1].time = 0;  
  rail_residence->mx_residence[2].time = 0; 
  rail_residence->mx_residence[3].time = 0; 
  rail_residence->mx.current_corner = RAILWAY_CORNERS_COUNT;
  rail_residence->mx.last_entered = 0;

  //CX config
  rail_residence->cx_residence[0].corner = 0x00746572; // "ret" in little endian
  rail_residence->cx_residence[1].corner = 0x32737673; // "svs2" in little endian
  rail_residence->cx_residence[2].corner = 0x00737673; // "svs" in little endian
  rail_residence->cx_residence[3].corner = 0x006d6f6e; // "nom" in little endian 
  rail_residence->cx_residence[4].corner = 0x62727574; // "turb" in little endian
  rail_residence->cx_residence[0].time = 0;
  rail_residence->cx_residence[1].time = 0;
  rail_residence->cx_residence[2].time = 0;  
  rail_residence->cx_residence[3].time = 0; 
  rail_residence->cx_residence[4].time = 0; 
  rail_residence->cx.current_corner = RAILWAY_CORNERS_COUNT;
  rail_residence->cx.last_entered = 0;
}

__attribute__((section("railway_dram_reclaim_pool")))
void railway_target_init(void)
{
  uint32 power_grid_rev = 0;
  pm_get_grid_revision(&power_grid_rev);

  if(power_grid_rev == PM_GRID_REV_1)
  {	  
    railway_rail_config_t *vdda_rail = &RAILWAY_CONFIG_DATA->rails[2];
    vdda_rail->vreg_num = 8;
    vdda_rail->vreg_type = RPM_SMPS_A_REQ;
  }    

    railway_residence_init();
}


