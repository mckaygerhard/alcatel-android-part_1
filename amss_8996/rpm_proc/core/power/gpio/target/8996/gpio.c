/*============================================================================
  FILE:         gpio.c

  OVERVIEW:     TLMM GPIO API source file for 8996.

  DEPENDENCIES: None

                Copyright (c) 2015 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary

  $Header:
  $DateTime:
  $Author:
============================================================================*/

#include "gpio.h"
#include "tlmm_hwio.h"
#include "HALhwio.h"

#define TLMM_DIR_CONN_INTR_COUNT_SENSORS    2
#define TLMM_DIR_CONN_INTR_COUNT_LPASS      6
#define TLMM_DIR_CONN_INTR_COUNT_RPM        1
#define TLMM_DIR_CONN_INTR_COUNT_HMSS       8
#define TLMM_DIR_CONN_INTR_COUNT_MSS        2

/*==============================================================================
**
**      Function Definitions
**
**==============================================================================
*/

/*==============================================================================
**
** gpio_configure_gpio
**
** Writes the config value to GPIO_CFG[n] register.
**
** PARAMs: gpio_num - GPIO pin number
**         config   - CPIO_CFG[n] configuration value
**
** RETURN: None
**
**/
void gpio_configure_gpio(uint32 gpio_num, uint32 config)
{
    HWIO_OUTI(TLMM_GPIO_CFGn, gpio_num, config);
}

/*==============================================================================
**
** gpio_read_gpio_in
**
** Reads and returns the GPIO_IN bit of GPIO_IN_OUT[n] register.
**
** PARAMs: gpio_num - GPIO pin number
**
** RETURN: GPIO_IN bit
**
**/
uint32 gpio_read_gpio_in(uint32 gpio_num)
{
    return HWIO_INFI(TLMM_GPIO_IN_OUTn, gpio_num, GPIO_IN);
}

/*==============================================================================
**
** gpio_write_gpio_out
**
** Writes the value to GPIO_OUT bit of GPIO_IN_OUT[n] register.
**
** PARAMs: gpio_num - GPIO pin number
**         value    - the value of GPIO_OUT bit
**
** RETURN: None
**
**/
void gpio_write_gpio_out(uint32 gpio_num, uint32 value)
{
    HWIO_OUTFI(TLMM_GPIO_IN_OUTn, gpio_num, GPIO_OUT, value);
}

/*==============================================================================
**
** gpio_is_summary_intr
**
** Checks if a summary interrupt from GPIO[n] is routed to the target processor.
**
** PARAMs: target   - Target processor type
**         gpio_num - GPIO pin number
**
** RETURN: TRUE if routed, FALSE if not routed
**
**/
bool gpio_is_summary_intr(gpio_target_id_t target, uint32 gpio_num)
{
    return (target == HWIO_INFI(TLMM_GPIO_INTR_CFGn, gpio_num, TARGET_PROC));
}

/*==============================================================================
**
** gpio_is_dir_conn_intr_enabled
**
** Checks if GPIO[n] is being used as a Direct Connect Interrupt.
**
** PARAMs: gpio_num - GPIO pin number
**
** RETURN: TRUE if enabled, FALSE if not enabled
**
**/
bool gpio_is_dir_conn_intr_enabled(uint32 gpio_num)
{
    return HWIO_INFI(TLMM_GPIO_INTR_CFGn, gpio_num, DIR_CONN_EN);
}

/*==============================================================================
**
** gpio_is_dir_conn_intr
**
** Checks if there exists a direct connect interrupt connected to GPIO[n]
** routed to the target processor.
**
** PARAMs: target   - Target processor type
**         gpio_num - GPIO pin number
**
** RETURN: TRUE if exists, FALSE if doesn't exist
**
**/
bool gpio_is_dir_conn_intr(gpio_target_id_t target, uint32 gpio_num)
{
    int i = 0;
    bool gpio_found = FALSE;
    uint32 gpio_sel = 0;

    switch (target)
    {
        case GPIO_TARGET_PROC_ID_SENSORS:
            for (i = 0; i<TLMM_DIR_CONN_INTR_COUNT_SENSORS; i++)
            {
                gpio_sel = HWIO_INFI(TLMM_DIR_CONN_INTRn_CFG_SENSORS, i, GPIO_SEL);
                if (gpio_sel == gpio_num)
                {
                    gpio_found = TRUE;
                    break;
                }
            }
            break;
        case GPIO_TARGET_PROC_ID_LPASS:
            for (i = 0; i<TLMM_DIR_CONN_INTR_COUNT_LPASS; i++)
            {
                gpio_sel = HWIO_INFI(TLMM_DIR_CONN_INTRn_CFG_LPA_DSP, i, GPIO_SEL);
                if (gpio_sel == gpio_num)
                {
                    gpio_found = TRUE;
                    break;
                }
            }
            break;
        case GPIO_TARGET_PROC_ID_RPM:
            for (i = 0; i<TLMM_DIR_CONN_INTR_COUNT_RPM; i++)
            {
                gpio_sel = HWIO_INFI(TLMM_DIR_CONN_INTRn_CFG_RPM, i, GPIO_SEL);
                if (gpio_sel == gpio_num)
                {
                    gpio_found = TRUE;
                    break;
                }
            }
            break;
        case GPIO_TARGET_PROC_ID_HMSS:
            for (i = 0; i<TLMM_DIR_CONN_INTR_COUNT_HMSS; i++)
            {
                gpio_sel = HWIO_INFI(TLMM_DIR_CONN_INTRn_CFG_HMSS, i, GPIO_SEL);
                if (gpio_sel == gpio_num)
                {
                    gpio_found = TRUE;
                    break;
                }
            }
            break;
        case GPIO_TARGET_PROC_ID_MSS:
            for (i = 0; i<TLMM_DIR_CONN_INTR_COUNT_MSS; i++)
            {
                gpio_sel = HWIO_INFI(TLMM_DIR_CONN_INTRn_CFG_GSS, i, GPIO_SEL);
                if (gpio_sel == gpio_num)
                {
                    gpio_found = TRUE;
                    break;
                }
            }
            break;
    }
    return gpio_found;
}
