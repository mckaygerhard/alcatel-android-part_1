/*===========================================================================
                              rbcpr.c

SERVICES:

DESCRIPTION:

INITIALIZATION AND SEQUENCING REQUIREMENTS:
  Description...

Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/


//===========================================================================
//                     Includes and Variable Definitions
//===========================================================================

//---------------------------------------------------------------------------
// Include Files
//---------------------------------------------------------------------------
#include <stdlib.h>
#include "CoreVerify.h"
#include "cpr_rpm.h"

#include "ClockDefs.h"
#include "Clock.h"
#include <stdbool.h>
#include "cortex-m3.h"
#include "swevent.h"
#include "stringl/stringl.h"
//#include "rbcpr_stats.h"
#include "Chipinfo.h"
#include "cpr_voltage_ranges.h"
#include "cpr_target_quotients.h"
#include "cpr_enablement.h"
#include "cpr_image.h"
#include "page_select.h"

//---------------------------------------------------------------------------
// Type Declarations
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Global Constant Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Local Object Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Static Variable Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Forward Declarations
//---------------------------------------------------------------------------
void RBCPRTaskISR(void* task);
void* RBCPRTaskInit(cpr_closed_loop_rail_t rail, uint32 client_interrupt_id);
void RBCPRTaskCornerChange(void* task);
void cpr_post_mode_switch_reenable(cpr_closed_loop_rail_t rail, const cpr_corner_params_t* corner_params, uint32 rail_uv);
void cpr_pre_mode_switch_disable(cpr_closed_loop_rail_t rail, const cpr_corner_params_t* corner_params);
void cpr_handler(cpr_closed_loop_rail_t rail);

size_t cpr_image_memscpy(void *dst, size_t dst_size, const void *src, size_t src_size)
{
    return memscpy(dst, dst_size, src, src_size);
}

void cpr_image_cancel_task(cpr_closed_loop_rail_t rail)
{
    //In case the ISR has already triggered and scheduled the CPR task,
    //need to flag the task as not needing to call rbcpr_handler
    RBCPRTaskCornerChange(rail->image_rail_state->task);
}

cpr_image_closed_loop_rail_t cpr_image_alloc_image_rail_state(void)
{
    cpr_image_closed_loop_rail_t image_rail_state = (cpr_image_closed_loop_rail_t)malloc(sizeof(cpr_image_closed_loop_rail_s));
    CORE_VERIFY_PTR(image_rail_state);
    memset(image_rail_state, 0, sizeof(cpr_image_closed_loop_rail_s));
    return image_rail_state;
}

typedef struct
{
    cpr_rail_id_t   cpr_rail_id;
    const char*     railway_name;
} cpr_rpm_railway_rail_name_mapping;

const static cpr_rpm_railway_rail_name_mapping cpr_railway_name_map[] = 
{
    { CPR_RAIL_MX,       "vddmx"      },
    { CPR_RAIL_CX,       "vddcx"      },
    { CPR_RAIL_GFX,      "vddgfx"     },
    { CPR_RAIL_VDDA_EBI, "vdda_ebi"   },
    { CPR_RAIL_SSC_MX,   "vdd_ssc_mx" },
    { CPR_RAIL_SSC_CX,   "vdd_ssc_cx" },
};

const char* cpr_rpm_railway_rail_name(cpr_rail_id_t rail_id)
{
    for(int i=0; i<(sizeof(cpr_railway_name_map)/sizeof(cpr_rpm_railway_rail_name_mapping)); i++)
    {
        if(rail_id == cpr_railway_name_map[i].cpr_rail_id)
        {
            return cpr_railway_name_map[i].railway_name;
        }
    }
    
    CORE_VERIFY(0);
}

typedef struct cpr_image_rail_voltage_control_handle_s
{
    int32           railway_rail_id;
    railway_voter_t railway_voter;
    
} cpr_image_rail_voltage_control_handle_s;

cpr_image_rail_voltage_control_handle_t cpr_image_alloc_voltage_control_handle(cpr_rail_id_t cpr_rail_id)
{
    cpr_image_rail_voltage_control_handle_t control_handle = (cpr_image_rail_voltage_control_handle_t)malloc(sizeof(cpr_image_rail_voltage_control_handle_s));
    CORE_VERIFY_PTR(control_handle);
    memset(control_handle, 0, sizeof(cpr_image_rail_voltage_control_handle_s));
    
    control_handle->railway_rail_id = rail_id(cpr_rpm_railway_rail_name(cpr_rail_id));
    CORE_VERIFY(control_handle->railway_rail_id!=RAIL_NOT_SUPPORTED_BY_RAILWAY);
    
    return control_handle;
}

void cpr_image_set_rail_mode_voltage(cpr_image_rail_voltage_control_handle_t voltage_control_handle, cpr_voltage_mode_t voltage_mode, uint32 voltage_uv)
{
    CORE_VERIFY_PTR(voltage_control_handle);
    railway_set_corner_voltage(voltage_control_handle->railway_rail_id, (railway_corner)voltage_mode, voltage_uv);
}

uint32 cpr_image_get_rail_mode_voltage(cpr_image_rail_voltage_control_handle_t voltage_control_handle, cpr_voltage_mode_t voltage_mode)
{
    CORE_VERIFY_PTR(voltage_control_handle);
    return railway_get_corner_voltage(voltage_control_handle->railway_rail_id, (railway_corner)voltage_mode);
}

const cpr_corner_params_t* cpr_image_get_current_corner_params(cpr_closed_loop_rail_t rail)
{
    railway_settings settings;
    railway_get_current_settings(rail->voltage_control_handle->railway_rail_id, &settings);
    return cpr_corner_params(rail, settings.mode);
}

void cpr_image_rail_transition_voltage(cpr_image_rail_voltage_control_handle_t voltage_control_handle)
{
    railway_transition_rail(voltage_control_handle->railway_rail_id);
}

void cpr_image_set_rail_mode(cpr_image_rail_voltage_control_handle_t voltage_control_handle, cpr_voltage_mode_t voltage_mode)
{
    if(!voltage_control_handle->railway_voter)
    {
        voltage_control_handle->railway_voter = railway_create_voter(voltage_control_handle->railway_rail_id, true, RAILWAY_CPR_SETTLING_VOTER);
    }
    
    railway_corner_vote(voltage_control_handle->railway_voter, (railway_corner)voltage_mode);
    railway_transition_rail(voltage_control_handle->railway_rail_id);
}

cpr_voltage_mode_t cpr_image_get_rail_mode(cpr_image_rail_voltage_control_handle_t voltage_control_handle)
{
    //Check that we went to the correct voltage
    railway_settings settings;
    railway_get_current_settings(voltage_control_handle->railway_rail_id, &settings);
    return (cpr_voltage_mode_t)settings.mode;
}

static void cpr_image_enable_clock(cpr_clock_state* clock_state)
{
    if(!clock_state->enabled)
    {
        clock_state->enabled = true;
        Clock_EnableClock(clock_state->clk_id);
    }
}

void cpr_image_enable_clocks(cpr_closed_loop_rail_t rail)
{
    CORE_VERIFY_PTR(rail);
    CORE_VERIFY_PTR(rail->rail_config);
    CORE_VERIFY_PTR(rail->image_rail_state);
    
    CORE_VERIFY(DAL_SUCCESS == Clock_GetClockId(rail->rail_config->ref_clk_resource, &rail->image_rail_state->clock[REF_CLK].clk_id));
    cpr_image_enable_clock(&rail->image_rail_state->clock[REF_CLK]);

    CORE_VERIFY(DAL_SUCCESS == Clock_GetClockId(rail->rail_config->ahb_clk_resource, &rail->image_rail_state->clock[AHB_CLK].clk_id));
    cpr_image_enable_clock(&rail->image_rail_state->clock[AHB_CLK]);
}

#define CPR_INVALID_CORNER -1

static void cpr_init_corner_lookup(cpr_closed_loop_rail_t rail)
{
    for(int i=0; i<RAILWAY_CORNERS_COUNT; i++)
    {
        rail->image_rail_state->corner_lookup_index[i] = CPR_INVALID_CORNER;
    }

    for(int i=0; i<rail->target_params_count; i++)
    {
        railway_corner corner = (railway_corner)rail->target_params[i].voltage_mode;
        CORE_VERIFY(corner<RAILWAY_CORNERS_COUNT);
        rail->image_rail_state->corner_lookup_index[corner] = i;
    }
}

cpr_corner_params_t* cpr_corner_params(cpr_closed_loop_rail_t rail, railway_corner corner)
{
    CORE_VERIFY(corner<RAILWAY_CORNERS_COUNT);
    int corner_lookup = rail->image_rail_state->corner_lookup_index[corner];
    CORE_VERIFY(corner_lookup != CPR_INVALID_CORNER);
    return &rail->target_params[corner_lookup];
}

void cpr_image_clear_interrupt_at_interrupt_controller(uint32 interrupt_id)
{
   interrupt_clear(interrupt_id);
}

static void cpr_pre_switch_railway_callback(const railway_settings *settings, void* cookie)
{
    CORE_VERIFY_PTR(settings);
    CORE_VERIFY_PTR(cookie);
    
    cpr_closed_loop_rail_t rail = (cpr_closed_loop_rail_t)cookie;
    
    const cpr_corner_params_t* corner_params = cpr_corner_params(rail, settings->mode);

    cpr_pre_mode_switch_disable(rail, corner_params);
}

static void cpr_post_switch_railway_callback(const railway_settings *settings, void* cookie)
{
    CORE_VERIFY_PTR(settings);
    CORE_VERIFY_PTR(cookie);

    cpr_closed_loop_rail_t rail = (cpr_closed_loop_rail_t)cookie;
    
    const cpr_corner_params_t* corner_params = cpr_corner_params(rail, settings->mode);

    cpr_post_mode_switch_reenable(rail, corner_params, settings->microvolts);
}

/*===========================================================================
FUNCTION: rbcpr_isr

DESCRIPTION: Interrupt service routine for the CPR interrupt

RETURN VALUE:
===========================================================================*/
static void cpr_isr( void ) __irq
{
    uint32 isr = interrupt_current_isr();
    boolean found = false;
    
    cpr_closed_loop_rail_t node = cpr_closed_loop_rail_root;
    
    while(node)
    {
        if(node->rail_config->interrupt_id == isr)
        {
            found = true;
            break;
        }
        node = node->next_link;
    }

    CORE_VERIFY(found);
    RBCPRTaskISR(node->image_rail_state->task);
}

void cpr_image_prepare_for_voltage_settling(void)
{
    railway_unlock();
}

void rbcpr_core_dump(void)
{
    cpr_closed_loop_rail_t node = cpr_closed_loop_rail_root;
    
    while(node)
    {
        cpr_image_closed_loop_rail_t image_rail_state = node->image_rail_state;
        if(image_rail_state->clock[REF_CLK].enabled && image_rail_state->clock[AHB_CLK].enabled)
        {
            HAL_cpr_save_hw_state(&node->rail_config->hal_handle, image_rail_state->hal_core_dump);
        }
        node = node->next_link;
    }
}

void cpr_image_init_image_closed_loop(cpr_closed_loop_rail_t rail)
{
    const cpr_closed_loop_rail_config_t* rail_config = rail->rail_config;

    cpr_init_corner_lookup(rail);
        
    rail->image_rail_state->task = RBCPRTaskInit(rail, rail_config->interrupt_id);

    rail->image_rail_state->hal_core_dump = HAL_cpr_alloc_cpr_dump_buffer();
    /* Now register the callbacks with RPMFW */
    int32 railway_rail_id = rail->voltage_control_handle->railway_rail_id;

    railway_set_callback(railway_rail_id, RAILWAY_PRECHANGE_CB, cpr_pre_switch_railway_callback, (void*)rail);
    railway_set_callback(railway_rail_id, RAILWAY_POSTCHANGE_CB, cpr_post_switch_railway_callback, (void*)rail);

    //Set up the ISR handler
    uint8 interrupt_id = rail_config->interrupt_id;
    
    interrupt_set_isr(interrupt_id, cpr_isr);
    interrupt_configure(interrupt_id, RISING_EDGE);
    interrupt_clear(interrupt_id);
    interrupt_enable(interrupt_id);
}

uint32 cpr_image_get_chip_info_version(void)
{
    return Chipinfo_GetVersion();
}

cpr_foundry_id cpr_image_get_foundry_info(void)
{
    cpr_foundry_id cpr_foundry;
    uint32 chipInfo_foundry = Chipinfo_GetFoundryId();
    
    switch(chipInfo_foundry)
    {
        case(CHIPINFO_FOUNDRYID_TSMC):
            cpr_foundry = CPR_FOUNDRY_TSMC;
            break;
        case(CHIPINFO_FOUNDRYID_GF):
            cpr_foundry = CPR_FOUNDRY_GF;
            break;
        case(CHIPINFO_FOUNDRYID_SS):
            cpr_foundry = CPR_FOUNDRY_SS;
            break;
        case(CHIPINFO_FOUNDRYID_IBM):
            cpr_foundry = CPR_FOUNDRY_IBM;
            break;
        case(CHIPINFO_FOUNDRYID_UMC):
            cpr_foundry = CPR_FOUNDRY_UMC;
            break;
        default:
            CORE_VERIFY(0); // Chip foundry is not valid
    }
    
    
    return cpr_foundry;
}

void cpr_image_prepare_to_access_smem(void)
{
    set_page_select(2);
}

void cpr_image_smem_access_complete(void)
{
    set_page_select(0);
}
