/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_device_hw_version.h"
#include "cpr_image.h"

uint32 cpr_chip_version;
cpr_foundry_id cpr_foundry;

void cpr_device_hw_version_init(void)
{
    static bool cpr_device_hw_version_init_done = false;
    if(cpr_device_hw_version_init_done)
    {
        return;
    }
    cpr_device_hw_version_init_done = true;

    cpr_chip_version = cpr_image_get_chip_info_version();

    cpr_foundry = cpr_image_get_foundry_info();
}

bool cpr_device_hw_version_matches_this_device(const cpr_config_hw_version_range* hw_range)
{
    cpr_device_hw_version_init();

    for(int j=0; j<hw_range->foundry_range_count; j++)
    {
        const cpr_config_foundry_range* foundry_range = &hw_range->foundry_range[j];
        if((cpr_foundry==foundry_range->foundry_id) &&
           (cpr_chip_version>=foundry_range->min_version) &&
           (cpr_chip_version<foundry_range->max_version))
        {
            return true;
        }
    }
    return false;
}

