/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_enablement.h"
#include "CoreVerify.h"


static const cpr_enablement_versioned_rail_config_t* cpr_enablement_find_device_config(const cpr_enablement_rail_config_t* enablement_config)
{
    for(int i=0; i<enablement_config->versioned_rail_config_count; i++)
    {
        const cpr_enablement_versioned_rail_config_t* versioned_enablement_config = enablement_config->versioned_rail_config[i];
        const cpr_config_hw_version_range* hw_versions = &versioned_enablement_config->hw_versions;

        if(cpr_device_hw_version_matches_this_device(hw_versions))
        {
                return versioned_enablement_config;
        }
    }   
    return NULL;
}

uint32 cpr_enablement_number_of_rails(void)
{
    return cpr_bsp_enablement_config.rail_enablement_config_count;
}

void cpr_enablement_rail_info(uint32 index, cpr_rail_id_t* rail_id, const cpr_enablement_versioned_rail_config_t** rail_enablement_versioned_config)
{
    CORE_VERIFY(index<cpr_bsp_enablement_config.rail_enablement_config_count);
   
    const cpr_enablement_rail_config_t* rail_enablement_config = cpr_bsp_enablement_config.rail_enablement_config[index];
    *rail_id = rail_enablement_config->rail_id;
    *rail_enablement_versioned_config = cpr_enablement_find_device_config(rail_enablement_config);
}

const cpr_enablement_versioned_rail_config_t* cpr_enablement_find_versioned_rail_config(cpr_rail_id_t cpr_rail_id)
{
    for(int i = 0; i < cpr_bsp_enablement_config.rail_enablement_config_count; i++)
    {
        if(cpr_bsp_enablement_config.rail_enablement_config[i]->rail_id == cpr_rail_id)
        {
            return cpr_enablement_find_device_config(cpr_bsp_enablement_config.rail_enablement_config[i]);
        }
    }
    return NULL;
}
