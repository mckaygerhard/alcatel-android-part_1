/**
 * @file:  cpr_isr.c
 * 
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/07/06 10:20:19 $
 * $Header: //components/rel/rpm.bf/1.6/core/power/cpr/common/src/cpr_isr.c#2 $
 * $Change: 8524365 $
 * 
 */
#include "cortex-m3.h"
#include "CoreVerify.h"
#include "cpr_image.h"
#include "cpr_closed_loop.h"

void cpr_handler(cpr_closed_loop_rail_t);

boolean cpr_isr_get_interrupt(cpr_rail_id_t railId, uint32 *interrupt)
{
    cpr_closed_loop_rail_t rail = cpr_closed_loop_rail_root;
    
    while(rail)
    {
        if(rail->rail_id == railId) {
            *interrupt = rail->rail_config->interrupt_id;
            return true;
        }
        rail = rail->next_link;
    }
    
    return false;
}

void cpr_isr_process(cpr_rail_id_t railId)
{
    cpr_closed_loop_rail_t rail = cpr_closed_loop_rail_root;
    
    while(rail)
    {
        if(rail->rail_id == railId) {
            break;
        }
        rail = rail->next_link;
    }
    
    CORE_VERIFY_PTR(rail);
    
    cpr_handler(rail);
    cpr_image_clear_interrupt_at_interrupt_controller(rail->rail_config->interrupt_id);
}
