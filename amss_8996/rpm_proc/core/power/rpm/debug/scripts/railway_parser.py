import sys, re, os
from base_parser import Parser
from target_data import *

class RAILWAYChangeVoltage:
    __metaclass__ = Parser
    id = 0x28A
    def parse(self, data):
        return 'railway_change_voltage (rail: %s) (new microvolts: %d)' % (get_rail_name(data[0]), data[1])
