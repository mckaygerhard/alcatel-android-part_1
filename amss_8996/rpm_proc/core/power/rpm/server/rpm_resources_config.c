/*
===========================================================================

FILE:         rpm_resources_config.c

DESCRIPTION:
  Per target resource configurations

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/power/rpm/server/rpm_resources_config.c#2 $
$Date: 2015/05/15 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include <stddef.h>
#include "rpm_resources_config.h"

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

#if (DAL_CONFIG_TARGET_ID == 0x8996)||(DAL_CONFIG_TARGET_ID == 0x8998)

static const rpm_resource_config_t temp_config_data[] =
{
    { RPM_SMPS_A_REQ,       2 },  // VDDMX
    { RPM_SMPS_A_REQ,       1 },  // VDDCX
    { RPM_LDO_A_REQ ,       3 },  // VDDA_EBI
    { RPM_LDO_A_REQ ,      31 },  // VDD_SSC_MX
    { RPM_LDO_A_REQ ,      26 },  // VDD_SSC_CX
    { (rpm_resource_type)0, 0 }
};

#else

static const rpm_resource_config_t *const temp_config_data = NULL;

#endif

const rpm_resource_config_t * const RESOURCE_CONFIG_DATA = temp_config_data;

