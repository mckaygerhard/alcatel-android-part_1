#ifndef __MISC_RESOURCE_H__
#define __MISC_RESOURCE_H__

/*===========================================================================

  misc_resource.h - resource used to share information with the RPM

  Copyright (c) 2013 Qualcomm Technologies Incorporated.  
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "comdef.h"
#include "rpmserver.h"
#include "CoreVerify.h"
#include "railway.h"

//keys
#define RPM_MISC_REQUEST_VPU            0x00757076 // 'vpu' in little endian
#define RPM_MISC_REQUEST_TEMP_CX        0x63706d74 // 'tmpc' in little endian
#define RPM_MISC_REQUEST_TEMP_GFX       0x67706d74 // 'tmpg' in little endian
#define RPM_MISC_DDR_HEALTH_LOC_SIZE    0x31726464 // 'ddr1' in little endian
#define RPM_MISC_LIMIT_CX               0x63746D6C // 'lmtc' in little endian
#define RPM_MISC_NPA_DUMP               0x706D7564 // 'dump' in little endian

//valid_keys masks
#define RPM_MISC_REQUEST_VPU_MASK           0x00000001
#define RPM_MISC_REQUEST_TEMP_CX_MASK       0x00000002
#define RPM_MISC_REQUEST_TEMP_GFX_MASK      0x00000004
#define RPM_MISC_DDR_HEALTH_LOC_SIZE_MASK   0x00000008
#define RPM_MISC_LIMIT_CX_MASK              0x00000010
#define RPM_MISC_NPA_DUMP_MASK              0x00000020

/* The internal representation of the GPIO toggle config                     *
 * GFX_temp         - temperature factor for GFX                             *
 * CX_temp          - temperature factor for CX                              *
 * VPU_on           - VPU on or off                                          *
 * DDR_address      - 64 bit address for DDR Health                          *
 * DDR_buffer_size  - size of the buffer in DDR for DDR Health               *
 * reserved   - reserved for future use                                      */

typedef enum {
    thermal_no_info         = 0,
    thermal_cold_critical   = 1,
    thermal_cold            = 2,
    thermal_cool            = 3,
    thermal_normal          = 4,
    thermal_warm            = 5,
    thermal_hot             = 6,
    thermal_hot_critical    = 7,
    
    thermal_range_size      = 0x7FFFFFFF, //force enum to sizeof(uint32_t)
} thermal_range;

typedef enum {
    VPU_OFF                 = 0,
    VPU_ON,
    NUM_VPU_STATES,
    
    vpu_state_size          = 0x7FFFFFFF, //force enum to sizeof(uint32_t)
} vpu_state_t;
 
typedef struct
{
    thermal_range GFX_temp;
    thermal_range CX_temp;
    vpu_state_t VPU_state;
    railway_corner CX_limit;
    uint64_t DDR_address;
    uint64_t NPA_dump_trigger_time;
    uint32_t DDR_buffer_size;
    uint32_t valid_keys;
} misc_inrep;

typedef struct {
    uint8 client_id;
    misc_inrep* votes;
    misc_inrep* old_votes;
} misc_callback_data;

typedef void (*misc_cb_type)(const misc_callback_data* data);
typedef void (*misc_init_type)(void);

typedef struct {
    misc_cb_type module_apply_client_vote;
    misc_init_type init;
    uint32 keys_mask;
} misc_module;

typedef struct {
    uint8 num_modules;
    const misc_module* modules;
} misc_registration;

typedef struct {
    uint64_t address;
    uint32_t size;
    uint32_t reserved;
} ddr_loc_size;
    
void misc_xlate(rpm_translation_info *info);
static void misc_apply(rpm_application_info *info);
void misc_init(void);

#endif
