/*===========================================================================

  misc_resource.c - resource used to share information with the RPM

  Copyright (c) 2013 Qualcomm Technologies Incorporated.  
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

//framework copied from gpio_toggle as a placeholder
  
#include "misc_resource.h"
#include "comdef.h"
#include <stdlib.h>
#include "time_service.h"
#include "rpmserver.h"
#include "CoreVerify.h"

extern const misc_registration * MISC_CONFIG_DATA;
extern void misc_target_init(void);

void misc_xlate(rpm_translation_info *info)
{
    unsigned        type, length;
    const char      *value_ptr;
    misc_inrep      *req = info->dest_buffer;

    // Read until we run out of KVPs
    while( !kvp_eof(info->new_kvps) )
    {
        if(!kvp_get( info->new_kvps, &type, &length, &value_ptr ))
        {
            abort();
        }

        CORE_VERIFY_PTR(value_ptr);

        switch(type)
        {
            case RPM_MISC_REQUEST_VPU:
                CORE_VERIFY(length == sizeof(vpu_state_t));
                req->VPU_state = *(vpu_state_t*)value_ptr;
                req->valid_keys |= RPM_MISC_REQUEST_VPU_MASK;
                break;
            case RPM_MISC_REQUEST_TEMP_CX:
                CORE_VERIFY(length == sizeof(thermal_range));
                req->CX_temp = *(thermal_range*)value_ptr;
                req->valid_keys |= RPM_MISC_REQUEST_TEMP_CX_MASK;
                break;
            case RPM_MISC_REQUEST_TEMP_GFX:
                CORE_VERIFY(length == sizeof(thermal_range));
                req->GFX_temp = *(thermal_range*)value_ptr;
                req->valid_keys |= RPM_MISC_REQUEST_TEMP_GFX_MASK;
                break;
            case RPM_MISC_DDR_HEALTH_LOC_SIZE:
                CORE_VERIFY(length == sizeof(ddr_loc_size));
                req->DDR_address = ((ddr_loc_size*)value_ptr)->address;
                req->DDR_buffer_size = ((ddr_loc_size*)value_ptr)->size;
                req->valid_keys |= RPM_MISC_DDR_HEALTH_LOC_SIZE_MASK;
                break;
            case RPM_MISC_LIMIT_CX:
                CORE_VERIFY(length == sizeof(uint32));
                req->CX_limit = railway_translate_master_corner(*(uint32*)value_ptr);
                req->valid_keys |= RPM_MISC_LIMIT_CX_MASK;
                break;
#ifdef MISC_NPA_DUMP		
            case RPM_MISC_NPA_DUMP:
                CORE_VERIFY(length == sizeof(uint64));
                req->NPA_dump_trigger_time = *(uint64*)value_ptr;
                req->valid_keys |= RPM_MISC_NPA_DUMP_MASK;
                break;
#endif
            default:
            {
                //unknown key
            }
        }
    }
}

static void misc_apply(rpm_application_info *info)
{    
    int i;
    misc_callback_data data;
    //update state
    misc_inrep *new_req = (misc_inrep *)info->new_state;
    
    data.votes = new_req;
    data.old_votes = (misc_inrep *)info->old_state;
    data.client_id = info->client;
    
    //invoke callbacks
    for (i = 0; i < MISC_CONFIG_DATA->num_modules; i++) {
        if (new_req->valid_keys & MISC_CONFIG_DATA->modules[i].keys_mask) {
            MISC_CONFIG_DATA->modules[i].module_apply_client_vote(&data);
        }
    }
}

void misc_init(void)
{
    unsigned i;
    
    //register with the RPM server
    rpm_register_resource(RPM_MISC_REQ, 1, sizeof(misc_inrep), misc_xlate, misc_apply, 0);

    misc_target_init();

    // call module init functions
    for (i = 0; i < MISC_CONFIG_DATA->num_modules; i++) {
        MISC_CONFIG_DATA->modules[i].init();
    }
}

