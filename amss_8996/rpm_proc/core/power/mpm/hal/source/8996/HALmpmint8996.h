#ifndef HALMPMINT8996_H
#define HALMPMINT8996_H

/*
===========================================================================

FILE:         HALmpmint8996.h

DESCRIPTION:
  Target-specific enumerations for HALmpmint.

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/power/mpm/hal/source/8996/HALmpmint8996.h#4 $
$Date: 2015/01/22 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2014 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include "comdef.h"

/* -----------------------------------------------------------------------
**                           TYPES
** ----------------------------------------------------------------------- */

/*
 * HAL_mpmint_IsrType
 *
 * List of possible interrupt sources.  All of these are not necessarily
 * supported on the HW.
 *
 * NOTE: This list must be in the same order as the lists in the
 *       MPM driver.
 */
typedef enum
{
  /* Hard Wired */
  HAL_MPMINT_QTIMER_ISR                 = 0,
  HAL_MPMINT_PEN_ISR,
  HAL_MPMINT_TSENS_UPPER_LOWER_ISR,

  /* High Voltage */
  HAL_MPMINT_GPIO1_ISR,
  HAL_MPMINT_GPIO5_ISR,
  HAL_MPMINT_GPIO9_ISR,
  HAL_MPMINT_GPIO11_ISR,
  HAL_MPMINT_GPIO66_ISR,
  HAL_MPMINT_GPIO22_ISR,
  HAL_MPMINT_GPIO24_ISR,
  HAL_MPMINT_GPIO26_ISR,
  HAL_MPMINT_GPIO34_ISR,
  HAL_MPMINT_GPIO36_ISR,
  HAL_MPMINT_GPIO37_ISR,
  HAL_MPMINT_GPIO38_ISR,
  HAL_MPMINT_GPIO40_ISR,
  HAL_MPMINT_GPIO42_ISR,
  HAL_MPMINT_GPIO46_ISR,
  HAL_MPMINT_GPIO50_ISR,
  HAL_MPMINT_GPIO53_ISR,
  HAL_MPMINT_GPIO54_ISR,
  HAL_MPMINT_GPIO56_ISR,
  HAL_MPMINT_GPIO57_ISR,
  HAL_MPMINT_GPIO58_ISR,
  HAL_MPMINT_GPIO59_ISR,
  HAL_MPMINT_GPIO60_ISR,
  HAL_MPMINT_GPIO61_ISR,
  HAL_MPMINT_GPIO62_ISR,
  HAL_MPMINT_GPIO63_ISR,
  HAL_MPMINT_GPIO64_ISR,
  HAL_MPMINT_GPIO71_ISR,
  HAL_MPMINT_GPIO73_ISR,
  HAL_MPMINT_GPIO77_ISR,
  HAL_MPMINT_GPIO78_ISR,
  HAL_MPMINT_GPIO79_ISR,
  HAL_MPMINT_GPIO80_ISR,
  HAL_MPMINT_GPIO82_ISR,
  HAL_MPMINT_GPIO86_ISR,
  HAL_MPMINT_GPIO91_ISR,
  HAL_MPMINT_GPIO92_ISR,
  HAL_MPMINT_GPIO95_ISR,
  HAL_MPMINT_GPIO97_ISR,
  HAL_MPMINT_GPIO101_ISR,
  HAL_MPMINT_GPIO104_ISR,
  HAL_MPMINT_GPIO106_ISR,
  HAL_MPMINT_GPIO108_ISR,
  HAL_MPMINT_GPIO112_ISR,
  HAL_MPMINT_GPIO113_ISR,
  HAL_MPMINT_GPIO110_ISR,
  HAL_MPMINT_OPEN0_ISR,
  HAL_MPMINT_GPIO127_ISR,
  HAL_MPMINT_GPIO115_ISR,
  HAL_MPMINT_PCIE_USB3_PHY_ISR,
  HAL_MPMINT_HDMI_PHY_ISR,
  HAL_MPMINT_GPIO116_ISR,
  HAL_MPMINT_GPIO117_ISR,
  HAL_MPMINT_GPIO118_ISR,
  HAL_MPMINT_GPIO119_ISR,
  HAL_MPMINT_GPIO120_ISR,
  HAL_MPMINT_GPIO121_ISR,
  HAL_MPMINT_GPIO122_ISR,
  HAL_MPMINT_GPIO123_ISR,
  HAL_MPMINT_GPIO124_ISR,
  HAL_MPMINT_GPIO125_ISR,
  HAL_MPMINT_GPIO126_ISR,
  HAL_MPMINT_GPIO129_ISR,
  HAL_MPMINT_GPIO131_ISR,
  HAL_MPMINT_GPIO132_ISR,
  HAL_MPMINT_GPIO133_ISR,
  HAL_MPMINT_GPIO145_ISR,
  HAL_MPMINT_OPEN1_ISR,
  HAL_MPMINT_SDC1_DAT1_ISR,
  HAL_MPMINT_SDC1_DAT3_ISR,
  HAL_MPMINT_SDC2_DAT1_ISR,
  HAL_MPMINT_SDC2_DAT3_ISR,
  HAL_MPMINT_OPEN2_ISR,
  HAL_MPMINT_OPEN3_ISR,
  HAL_MPMINT_SDC2_CMD_ISR,
  HAL_MPMINT_SRST_N_ISR,
  HAL_MPMINT_USB_PHY_DMSE_HS1_ISR,
  HAL_MPMINT_USB_PHY_DMSE_HS2_ISR,
  HAL_MPMINT_USB_PHY_DPSE_HS1_ISR,
  HAL_MPMINT_USB_PHY_DPSE_HS2_ISR,
  HAL_MPMINT_OPEN4_ISR,
  HAL_MPMINT_OPEN5_ISR,
  HAL_MPMINT_OPEN6_ISR,
  HAL_MPMINT_OPEN7_ISR,

  /* Always On */
  HAL_MPMINT_MPM_WAKE_SPMI_ISR,
  HAL_MPMINT_OPEN8_ISR,
  HAL_MPMINT_OPEN9_ISR,
  HAL_MPMINT_Q6_SPM_BRINGUP_REQ_ISR,
  HAL_MPMINT_SSC_TMR_TIMEOUT_ISR,
  HAL_MPMINT_OPEN10_ISR,
  HAL_MPMINT_OPEN11_ISR,
  HAL_MPMINT_UIM_CONTROLLER_CARD_ISR,
  HAL_MPMINT_UIM_CONTROLLER_BATT_ISR,

  HAL_MPMINT_NUM = 96,

  /* Interrupts below this point are legacy definitions, not supported by this
   * version of the hardware. */
  HAL_MPMINT_NOT_DEFINED_ISR            = HAL_MPMINT_NUM,
} HAL_mpmint_IsrType;

#ifdef __cplusplus
}
#endif

#endif /* HALMPMINT8996_H */

