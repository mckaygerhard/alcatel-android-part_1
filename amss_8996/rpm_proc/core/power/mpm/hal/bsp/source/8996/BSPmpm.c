/*
===========================================================================

FILE:         BSPmpm.c

DESCRIPTION:
  This is the platform hardware abstraction layer implementation for the
  MPM hardware block.
  This platform is for the RPM on the 8996.

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/power/mpm/hal/bsp/source/8996/BSPmpm.c#5 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2014 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include <stdint.h>
#include "BSPmpm.h"

/* -----------------------------------------------------------------------
**                           TYPES
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */


/* wakeup delays for CXO warm-up in 32kHz ticks */
/* Need to be >= 500 usec for PM8994 (CR 701462)*/
static const uint32_t cxoDelay = 0x11; /* 531 usec to be safe */

/* target backoff for deep sleep exit in 19.2Mhz ticks */
/*                                        VDD min   */
static const uint32_t deepSleepExitDelay = 3578;

/*
 * Target specific MPM Hardware configuration.
 */
BSP_mpm_ConfigDataType MPM_BSP_DATA =
{
  /* MPM Configuration */
  {
    /* Wakeup Delays */
    {
      cxoDelay, deepSleepExitDelay
    },
#ifdef MX_VMIN_HIGH_CURRENT_WORK_AROUND
    //CR966070
    /* IO Cfg */
    {
      /* Freeze  Clamp  SW Ebi1     Warm Boot    Warm Boot    VREF_PWRSAVE
       * IOs     IOs    Ctl Enable  Freeze EBI1  Freeze EBI2                   */
      FALSE,      TRUE,   TRUE,       TRUE,        TRUE,       TRUE
    },
#else
    /* IO Cfg */
    {
      /* Freeze  Clamp  SW Ebi1     Warm Boot    Warm Boot    VREF_PWRSAVE
       * IOs     IOs    Ctl Enable  Freeze EBI1  Freeze EBI2                   */
      TRUE,      TRUE,   TRUE,       TRUE,        TRUE,       TRUE
    },
#endif
    /* Debug Through Power Collapse */
    FALSE,

    /* Debug Bus Enable (bit 0)     Debug Bus Enable (bit 1) */
    FALSE,                          FALSE,
  },

  /* Peripherals Configuration */
  {
    /* Pen Debounce Cfg */
    {
      /* Enable */
      FALSE,

      /* Delay */
      HAL_MPM_PEN_DEBOUNCE_DELAY_0400US
    },
  },

  /* Voltage Rail Configuration */
  {
    1500, //step size: 1.5mV/uS
  }

}; /* End BSP_mpm_ConfigData */

