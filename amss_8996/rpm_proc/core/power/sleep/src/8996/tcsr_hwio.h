#ifndef __TCSR_HWIO_H__
#define __TCSR_HWIO_H__
/*
===========================================================================
*/
/**
  @file tcsr_hwio.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8996 (Istari) v2 [istari_v2.1_p3q2r16.0]
 
  This file contains HWIO register definitions for the following modules:
    TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/rpm.bf/1.6/core/power/sleep/src/8996/tcsr_hwio.h#1 $
  $DateTime: 2015/04/14 19:14:08 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * MODULE: TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW
 *--------------------------------------------------------------------------*/

#define TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE                                  (CORE_TOP_CSR_BASE      + 0x000b9000)

#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID0_ADDR                                        (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000000)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID0_RMSK                                              0xff
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID0_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID0_ADDR, HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID0_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID0_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID0_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID0_STEP_7_0_BMSK                                     0xff
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID0_STEP_7_0_SHFT                                      0x0

#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID1_ADDR                                        (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000004)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID1_RMSK                                              0xff
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID1_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID1_ADDR, HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID1_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID1_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID1_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID1_STEP_15_8_BMSK                                    0xff
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID1_STEP_15_8_SHFT                                     0x0

#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID2_ADDR                                        (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000008)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID2_RMSK                                              0xff
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID2_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID2_ADDR, HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID2_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID2_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID2_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID2_MINOR_7_0_BMSK                                    0xff
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID2_MINOR_7_0_SHFT                                     0x0

#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID3_ADDR                                        (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x0000000c)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID3_RMSK                                              0xff
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID3_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID3_ADDR, HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID3_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID3_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID3_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID3_MAJOR_BMSK                                        0xf0
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID3_MAJOR_SHFT                                         0x4
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID3_MINOR_11_8_BMSK                                    0xf
#define HWIO_TCSR_VREF_QREFS_VBG_REVISION_ID3_MINOR_11_8_SHFT                                    0x0

#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT0_ADDR                                     (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000010)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT0_RMSK                                           0xff
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT0_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT0_ADDR, HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT0_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT0_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT0_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT0_DEBUG_BUS_7_0_BMSK                             0xff
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT0_DEBUG_BUS_7_0_SHFT                              0x0

#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT1_ADDR                                     (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000014)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT1_RMSK                                           0xff
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT1_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT1_ADDR, HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT1_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT1_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT1_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT1_DEBUG_BUS_15_8_BMSK                            0xff
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT1_DEBUG_BUS_15_8_SHFT                             0x0

#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT2_ADDR                                     (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000018)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT2_RMSK                                           0xff
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT2_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT2_ADDR, HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT2_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT2_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT2_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT2_DEBUG_BUS_23_16_BMSK                           0xff
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT2_DEBUG_BUS_23_16_SHFT                            0x0

#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT3_ADDR                                     (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x0000001c)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT3_RMSK                                           0xff
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT3_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT3_ADDR, HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT3_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT3_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT3_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT3_DEBUG_BUS_31_24_BMSK                           0xff
#define HWIO_TCSR_VREF_QREFS_VBG_DEBUG_BUS_STAT3_DEBUG_BUS_31_24_SHFT                            0x0

#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_ADDR                                        (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000020)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_RMSK                                              0xff
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_ADDR, HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_CAL_STAT_BMSK                                     0xe0
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_CAL_STAT_SHFT                                      0x5
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_CAL_DONE_BMSK                                     0x10
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_CAL_DONE_SHFT                                      0x4
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_CAL_VALUE_BMSK                                     0xf
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS0_CAL_VALUE_SHFT                                     0x0

#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_ADDR                                        (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000024)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_RMSK                                              0xff
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_ADDR, HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_VBG_CAL_VAL_BMSK                                  0x80
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_VBG_CAL_VAL_SHFT                                   0x7
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_APCS_SYSBANDGAP_EN_HMSS_BMSK                      0x40
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_APCS_SYSBANDGAP_EN_HMSS_SHFT                       0x6
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_FUSE_VALUE_QREF_VBG_BMSK                          0x3f
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS1_FUSE_VALUE_QREF_VBG_SHFT                           0x0

#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS2_ADDR                                        (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000028)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS2_RMSK                                              0xff
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS2_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS2_ADDR, HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS2_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS2_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS2_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS2_VBG_MUX_SEL_BMSK                                  0xc0
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS2_VBG_MUX_SEL_SHFT                                   0x6
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS2_FUSE_VALUE_VTEC_VBG_BMSK                          0x3f
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS2_FUSE_VALUE_VTEC_VBG_SHFT                           0x0

#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_ADDR                                        (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x0000002c)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_RMSK                                              0xff
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_ADDR, HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_RSVD_7_BMSK                                       0x80
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_RSVD_7_SHFT                                        0x7
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_VBG_CAL_EN_BMSK                                   0x40
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_VBG_CAL_EN_SHFT                                    0x6
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_VBG_CAL_BYPASS_BMSK                               0x20
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_VBG_CAL_BYPASS_SHFT                                0x5
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_RESTART_CAL_PULSE_BMSK                            0x10
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_RESTART_CAL_PULSE_SHFT                             0x4
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_APCS_SYSBANDGAP_EN_LOW_GATED_BMSK                  0x8
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_APCS_SYSBANDGAP_EN_LOW_GATED_SHFT                  0x3
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_APCS_SYSBANDGAP_EN_HIGH_GATED_BMSK                 0x4
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_APCS_SYSBANDGAP_EN_HIGH_GATED_SHFT                 0x2
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_CAL_AUTO_EN_BMSK                                   0x2
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_CAL_AUTO_EN_SHFT                                   0x1
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_SLEEP_B_BMSK                                       0x1
#define HWIO_TCSR_VREF_QREFS_VBG_TRIM_STATUS3_SLEEP_B_SHFT                                       0x0

#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_ADDR                                         (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000030)
#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_RMSK                                               0xff
#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_ADDR, HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_RSVD_7_4_BMSK                                      0xf0
#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_RSVD_7_4_SHFT                                       0x4
#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_REFCLK_SEL_BMSK                                     0xe
#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_REFCLK_SEL_SHFT                                     0x1
#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_REFCLK_RXTAP_EN_BMSK                                0x1
#define HWIO_TCSR_VREF_QREFS_VBG_REFCLK_CTRL_REFCLK_RXTAP_EN_SHFT                                0x0

#define HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_ADDR                                          (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000034)
#define HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_RMSK                                                0xff
#define HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_ADDR, HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_RSVD_7_1_BMSK                                       0xfe
#define HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_RSVD_7_1_SHFT                                        0x1
#define HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_SW_RESET_BMSK                                        0x1
#define HWIO_TCSR_VREF_QREFS_VBG_RESET_CTRL_SW_RESET_SHFT                                        0x0

#define HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_ADDR                                        (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000038)
#define HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_RMSK                                              0xff
#define HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_ADDR, HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_RSVD_7_1_BMSK                                     0xfe
#define HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_RSVD_7_1_SHFT                                      0x1
#define HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_PWRDN_B_BMSK                                       0x1
#define HWIO_TCSR_VREF_QREFS_VBG_PWRDOWN_CTRL_PWRDN_B_SHFT                                       0x0

#define HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_ADDR                                     (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x0000003c)
#define HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_RMSK                                           0xff
#define HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_ADDR, HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_DEBUG_BUS_SEL_BMSK                             0xf0
#define HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_DEBUG_BUS_SEL_SHFT                              0x4
#define HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_ATESTMUX_SEL_BMSK                               0xf
#define HWIO_TCSR_VREF_QREFS_VBG_TEST_DEBUG_CTRL_ATESTMUX_SEL_SHFT                               0x0

#define HWIO_TCSR_VREF_QREFS_VBG_CFG0_ADDR                                                (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000040)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG0_RMSK                                                      0xff
#define HWIO_TCSR_VREF_QREFS_VBG_CFG0_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CFG0_ADDR, HWIO_TCSR_VREF_QREFS_VBG_CFG0_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG0_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CFG0_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG0_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_CFG0_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_CFG0_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_CFG0_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG0_RSVD_7_2_BMSK                                             0xfc
#define HWIO_TCSR_VREF_QREFS_VBG_CFG0_RSVD_7_2_SHFT                                              0x2
#define HWIO_TCSR_VREF_QREFS_VBG_CFG0_VBG_MUX_SEL_BMSK                                           0x3
#define HWIO_TCSR_VREF_QREFS_VBG_CFG0_VBG_MUX_SEL_SHFT                                           0x0

#define HWIO_TCSR_VREF_QREFS_VBG_CFG1_ADDR                                                (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000044)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG1_RMSK                                                      0xff
#define HWIO_TCSR_VREF_QREFS_VBG_CFG1_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CFG1_ADDR, HWIO_TCSR_VREF_QREFS_VBG_CFG1_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG1_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CFG1_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG1_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_CFG1_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_CFG1_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_CFG1_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG1_RSVD_7_0_BMSK                                             0xff
#define HWIO_TCSR_VREF_QREFS_VBG_CFG1_RSVD_7_0_SHFT                                              0x0

#define HWIO_TCSR_VREF_QREFS_VBG_CFG2_ADDR                                                (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000048)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG2_RMSK                                                      0xff
#define HWIO_TCSR_VREF_QREFS_VBG_CFG2_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CFG2_ADDR, HWIO_TCSR_VREF_QREFS_VBG_CFG2_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG2_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CFG2_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG2_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_CFG2_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_CFG2_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_CFG2_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG2_RSVD_7_4_BMSK                                             0xf0
#define HWIO_TCSR_VREF_QREFS_VBG_CFG2_RSVD_7_4_SHFT                                              0x4
#define HWIO_TCSR_VREF_QREFS_VBG_CFG2_CAL_IDLE_TIMEOUT_CNT_BMSK                                  0xf
#define HWIO_TCSR_VREF_QREFS_VBG_CFG2_CAL_IDLE_TIMEOUT_CNT_SHFT                                  0x0

#define HWIO_TCSR_VREF_QREFS_VBG_CFG3_ADDR                                                (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x0000004c)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG3_RMSK                                                      0xff
#define HWIO_TCSR_VREF_QREFS_VBG_CFG3_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CFG3_ADDR, HWIO_TCSR_VREF_QREFS_VBG_CFG3_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG3_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CFG3_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG3_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_CFG3_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_CFG3_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_CFG3_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_CFG3_RSVD_7_0_BMSK                                             0xff
#define HWIO_TCSR_VREF_QREFS_VBG_CFG3_RSVD_7_0_SHFT                                              0x0

#define HWIO_TCSR_VREF_QREFS_VBG_CTRL0_ADDR                                               (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000050)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL0_RMSK                                                     0xff
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL0_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CTRL0_ADDR, HWIO_TCSR_VREF_QREFS_VBG_CTRL0_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL0_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CTRL0_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL0_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_CTRL0_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_CTRL0_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_CTRL0_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL0_RSVD_7_6_BMSK                                            0xc0
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL0_RSVD_7_6_SHFT                                             0x6
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL0_DELAY_LOW_TIMER_CNT_BMSK                                 0x3f
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL0_DELAY_LOW_TIMER_CNT_SHFT                                  0x0

#define HWIO_TCSR_VREF_QREFS_VBG_CTRL1_ADDR                                               (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000054)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL1_RMSK                                                     0xff
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL1_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CTRL1_ADDR, HWIO_TCSR_VREF_QREFS_VBG_CTRL1_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL1_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CTRL1_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL1_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_CTRL1_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_CTRL1_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_CTRL1_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL1_RSVD_7_6_BMSK                                            0xc0
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL1_RSVD_7_6_SHFT                                             0x6
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL1_DELAY_HIGH_TIMER_CNT_BMSK                                0x3f
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL1_DELAY_HIGH_TIMER_CNT_SHFT                                 0x0

#define HWIO_TCSR_VREF_QREFS_VBG_CTRL2_ADDR                                               (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000058)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL2_RMSK                                                     0xff
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL2_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CTRL2_ADDR, HWIO_TCSR_VREF_QREFS_VBG_CTRL2_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL2_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CTRL2_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL2_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_CTRL2_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_CTRL2_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_CTRL2_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL2_MAX_CAL_TIMIER_CNT_BMSK                                  0xff
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL2_MAX_CAL_TIMIER_CNT_SHFT                                   0x0

#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_ADDR                                               (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x0000005c)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_RMSK                                                     0xff
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CTRL3_ADDR, HWIO_TCSR_VREF_QREFS_VBG_CTRL3_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_CTRL3_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_CTRL3_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_CTRL3_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_CTRL3_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_RSVD_7_6_BMSK                                            0xc0
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_RSVD_7_6_SHFT                                             0x6
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_APCS_SYSBANDGAP_EN_BMSK                                  0x20
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_APCS_SYSBANDGAP_EN_SHFT                                   0x5
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_VBG_CAL_BYPASS_B_BMSK                                    0x10
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_VBG_CAL_BYPASS_B_SHFT                                     0x4
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_VTECH_EN_BMSK                                             0x8
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_VTECH_EN_SHFT                                             0x3
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_VBG_EN_BMSK                                               0x4
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_VBG_EN_SHFT                                               0x2
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_CAL_AUTO_EN_BMSK                                          0x2
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_CAL_AUTO_EN_SHFT                                          0x1
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_CAL_EN_BMSK                                               0x1
#define HWIO_TCSR_VREF_QREFS_VBG_CTRL3_CAL_EN_SHFT                                               0x0

#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_ADDR                                          (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000060)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_RMSK                                                0xff
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_ADDR, HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_RSVD_7_6_BMSK                                       0xc0
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_RSVD_7_6_SHFT                                        0x6
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_APCS_SYSBANDGAP_EN_OVERRIDE_EN_BMSK                 0x20
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_APCS_SYSBANDGAP_EN_OVERRIDE_EN_SHFT                  0x5
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_CAL_DONE_OVERRIDE_EN_BMSK                           0x10
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_CAL_DONE_OVERRIDE_EN_SHFT                            0x4
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_RSVD_3_2_BMSK                                        0xc
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_RSVD_3_2_SHFT                                        0x2
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_CAL_VALUE_OVERRIDE_EN_BMSK                           0x2
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_CAL_VALUE_OVERRIDE_EN_SHFT                           0x1
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_FUSE_OVERRIDE_EN_BMSK                                0x1
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL0_FUSE_OVERRIDE_EN_SHFT                                0x0

#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_ADDR                                          (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000064)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_RMSK                                                0x7f
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_ADDR, HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_FUSE_OVERRIDE_VAL1_BMSK                             0x7f
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL1_FUSE_OVERRIDE_VAL1_SHFT                              0x0

#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_ADDR                                          (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x00000068)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_RMSK                                                0xff
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_ADDR, HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_VTECH_TRIM_BUSS_SEL_BMSK                            0x80
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_VTECH_TRIM_BUSS_SEL_SHFT                             0x7
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_FUSE_OVERRIDE_VAL2_BMSK                             0x7f
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL2_FUSE_OVERRIDE_VAL2_SHFT                              0x0

#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_ADDR                                          (TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW_REG_BASE      + 0x0000006c)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_RMSK                                                0xff
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_IN          \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_ADDR, HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_RMSK)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_INM(m)      \
        in_dword_masked(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_ADDR, m)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_OUT(v)      \
        out_dword(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_ADDR,v)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_ADDR,m,v,HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_IN)
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_RSVD_7_5_BMSK                                       0xe0
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_RSVD_7_5_SHFT                                        0x5
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_CAL_DONE_BMSK                                       0x10
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_CAL_DONE_SHFT                                        0x4
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_CAL_VALUE_OVERRIDE_BMSK                              0xf
#define HWIO_TCSR_VREF_QREFS_VBG_FUSE_CTRL3_CAL_VALUE_OVERRIDE_SHFT                              0x0


#endif /* __TCSR_HWIO_H__ */

