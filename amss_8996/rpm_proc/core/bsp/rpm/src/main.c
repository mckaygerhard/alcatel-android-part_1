/*=============================================================================

                                MAIN

GENERAL DESCRIPTION
  This file contains the initial operations for the RPM.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2009-2015 by Qualcomm Technologies, Inc.  All Rights Reserved.

  $Header: //components/rel/rpm.bf/1.6/core/bsp/rpm/src/main.c#25 $
  $Author: pwbldsvc $
  $DateTime: 2015/07/08 14:15:57 $
=============================================================================*/

#include <assert.h>
#include "comdef.h"

#include "cortex-m3.h"
#include "npa_init.h"
#include "comdef.h"
#include "smem.h"
#include "rpmserver.h"
#include "mpm.h"
#include "vmpm.h"
#include "dog.h"
#include "time_service.h"
#include "QDSSLite.h"
#include "icb_rpm.h"
#include "swevent.h"
#include "railway.h"
#include "cpu.h"
#include "kvp.h"
#include "ddr_drivers.h"
#include "system_db_rpm.h"
#include "rpm_settling_timer.h"
#include "low_power_led.h"
#include "rpm_mpu.h"

#include "HALhwio.h"
#include "hw_version.h"
#include "coredump.h"
#include "Clock.h"
#include "PlatformInfo.h"
#include "version.h"
#include "timetick.h"
#include "version.h"
#include "image_layout.h"
#include "glink.h"
#include "CoreVerify.h"
#include "gcc_rpm_branch_ena_vote_hwio.h"

#if (defined(MSM8996_STUBS))
#include "sleep_v.h"
#elif (defined(MSM8998_STUBS))
#include "pmapp_npa.h"
__attribute__((section("clk_cram_reclaim_pool")))
boolean Clock_Init(void){return TRUE;}
DALResult Clock_EnableClock (uint32 nClock) {return 1;}
DALResult Clock_DisableClock (uint32 nClock) {return 1;}
DALResult Clock_GetClockId (const char *szClock,  uint32 *pnId) {return 1;}
boolean Clock_IsQDSSOn(void) {return TRUE;}
DALResult Clock_GetPowerDomainId (const char *szPowerDomain, uint32 *pnPowerDomainId) { return 1;}
DALResult Clock_ProcessorRestore (ClockSleepModeType eMode, uint32 nFlags) {return 1;}
DALResult Clock_ProcessorSleep (ClockSleepModeType eMode, uint32 nFlags ) {return 1;}
void Clock_LogState ( void ){}
DALResult Clock_IsClockOn( ClockIdType nClock, boolean * pbIsOn) {return 1;}
void Clock_SetSPMStates( kvp_t *kvp ){}
__attribute__((section("ddr_stub")))
void ddr_init(void) {}
__attribute__((section("icb_cram_reclaim_pool")))
void icb_dummy_func(void) {}
__attribute__((section("icb_stub")))
void icb_init(void) {void (*fptr)(void) = icb_dummy_func; fptr();}
void pm_init(void){}
__attribute__((section("pm_cram_reclaim_pool")))
pm_err_flag_type pm_pon_wdog_cfg(uint8 pmic_index, uint32 s1_timer, uint32 s2_timer, pm_pon_reset_cfg_type reset_cfg_type) {return PM_ERR_FLAG__INVALID;}
__attribute__((section("pm_dram_reclaim_pool")))
pm_err_flag_type pm_pon_wdog_enable(uint8 pmic_index, pm_on_off_type enable) {return PM_ERR_FLAG__INVALID;}
pm_err_flag_type pm_pon_wdog_pet(uint8 pmic_index) {return PM_ERR_FLAG__INVALID;}
unsigned pm_railway_set_mode(pm_railway_type_info_type rail, pm_sw_mode_type mode) {return 0;}
unsigned pm_railway_set_voltage(pm_railway_type_info_type rail, unsigned voltage_uv) {return 0;}
pm_err_flag_type pm_rtc_get_time(uint8 pmic_index, uint32 *time_ptr) {return PM_ERR_FLAG__INVALID;}
#include "pm_app_led.h"
pm_err_flag_type pm_rpm_enable_led_with_dim_ctrl(pm_led_rgb led_rgb, boolean enable, pm_led_volt_source led_volt_src, pm_led_dim_level dim_level) {return PM_ERR_FLAG__INVALID;}
pm_err_flag_type pm_clk_sleep_src_status(uint8 pmic_chip, unsigned clk_type, pm_on_off_type* on_off){return PM_ERR_FLAG__INVALID;}
pm_err_flag_type pm_clk_xo_set_warmup_time(uint8 pmic_chip, unsigned clk_type, uint32 timer_value){return PM_ERR_FLAG__INVALID;}
pm_err_flag_type pm_rpm_enter_sleep (unsigned sleep_mode){return PM_ERR_FLAG__INVALID;}
pm_err_flag_type pm_rpm_exit_sleep (unsigned sleep_mode){return PM_ERR_FLAG__INVALID;}
pm_err_flag_type pm_mpm_cmd_config(void * mpm_cmd_ptr){return PM_ERR_FLAG__INVALID;}
void xport_rpm_init(void){}
void xport_rpm_toc_init(void){}
int xport_rpm_isr(void *ctx_ptr){return 0;}
#include "smem_list.h"
void smem_init( void ){}
void *smem_alloc(smem_mem_type smem_type, uint32 buf_size){return NULL;}
void *smem_get_addr(smem_mem_type smem_type, uint32 *buf_size) {return NULL;}
void* smem_alloc_rpm(smem_host_type other_host, smem_mem_type smem_type, uint32 buf_size){return NULL;}
void smem_list_append( smem_list_type *list, void *item ){}
int32 smem_list_delete( smem_list_type *list, void *item ){return 0;}
void *smem_list_first( smem_list_type *list ){return NULL;}
void smem_list_init( smem_list_type *list ){}
void *smem_list_next( void * item ){return NULL;}
void smem_spin_lock( uint32 lock ){}
void smem_spin_unlock( uint32 lock ){}
boolean smem_version_set(smem_mem_type type, uint32 version, uint32 mask){return 0;}
__attribute__((section("cpr_cram_reclaim_pool")))
void cpr_init_closed_loop_init(void){}
__attribute__((section("cpr_dram_reclaim_pool")))
void cpr_init_initial_voltage_config(void){}
void rbcpr_core_dump(void){}
boolean cpr_isr_get_interrupt(unsigned railId, uint32 *interrupt){return false;}
void cpr_isr_process(unsigned railId){}
void rbcpr_exit_sleep(void){}
void rbcpr_prepare_for_sleep(void){}
#include "Chipinfo.h"
ChipInfoVersionType Chipinfo_GetVersion (void){return 0;}
ChipInfoFamilyType Chipinfo_GetFamily (void){return CHIPINFO_FAMILY_UNKNOWN;}
void vsense_init(void){}
#include "sleep_v.h"
#else
#include "sleep_v.h"
#endif

//prng stack protection init
void __init_stack_chk_guard(void);

void xpu_init(void);
void time_service_init(void);
void sched_run(void);
uint64_t sched_get_next_start(void);
unsigned int npa_num_pending_events(void);
void npa_process_event(void);
void exceptions_init(void);
void exceptions_enable(void);
void pm_init(void);
void debug_init(void);
void swevent_init(void);
void swevent_qdss_init(void);
void prevent_further_heap_frees(void);
void busywait_init(void);
void rpm_settling_timer_init(void);
void message_ram_init(void);

void gpio_toggle_init(void);

void misc_init(void);

//Get rid of this once we're back to one railway.
void railway_init_v2(void);
void railway_init_early_proxy_votes(void);
void populate_image_header(void);
void DALSYS_PropsInit(void);
void zqcal_task_init(void);
void acc_init(void);
void railway_sw_mode_poke(void);
void railway_unlock(void);
void debug_cookie_init(void);
void rpm_init_done(void);
void rpm_server_init_done(void);
void glink_ssr_init( void );
void glink_init(void);
void xport_rpm_toc_init(void);
void xport_rpm_init(void);
#if (defined (VSENSE_FEATURE_ENABLED))
void vsense_init(void);
#endif
void cpr_init_closed_loop_init(void);
void cpr_init_initial_voltage_config(void);


#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]))

typedef void (*init_fcn)(void);

static void rpm_mpu_enable_data_ram_protection(void) { rpm_mpu_enable_region(RPM_MPU_RGN_DATA_RAM, TRUE); }
static void rpm_mpu_enable_code_ram_protection(void) { rpm_mpu_enable_region(RPM_MPU_RGN_CODE_RAM_HEAP, TRUE); rpm_mpu_enable_region(RPM_MPU_RGN_CODE_RAM, TRUE);}

__attribute__((section("proxy_cram_reclaim_pool")))
const init_fcn init_fcns[] =
{
  populate_image_header,
  npa_init,
  pm_init,
  smem_init,
  railway_init_v2,
  cpr_init_initial_voltage_config,  //Init default voltage levels into Railway.
  message_ram_init,
  acc_init,
  railway_init_early_proxy_votes,
  xpu_init, /* cookie set here also indicates to SBL that railway is ready */
  PlatformInfo_Init, /* Needs to be before Clock_Init. */
  rpm_reclaim_data_ram_pool,
  rpm_mpu_enable_data_ram_protection, /* must be after rpm_reclaim_data_ram_pool() to make sure
                                         no instruction fetched from the data ram reclaim pool */
  (init_fcn)Clock_Init,
  __init_stack_chk_guard,
  ddr_init,
  smem_init,
  cpr_init_closed_loop_init,
  railway_unlock,
  glink_init,  
  xport_rpm_toc_init,  
  xport_rpm_init,  
  version_init,     /* Needs to be after glink_init */
  rpmserver_init,
  rpm_server_init_done,
  railway_init_proxies_and_pins,
  icb_init,
  cpu_init,
  debug_init,
  vmpm_init,
  glink_ssr_init,
  sleep_init,
  QDSSInit,
  exceptions_enable,
  swevent_qdss_init,
  system_db_init,
  rpm_settling_timer_init,
  low_power_led_init,
  zqcal_task_init,
  gpio_toggle_init,
  misc_init,
  rpm_set_changer_common_init,
  #if (defined (VSENSE_FEATURE_ENABLED))
  vsense_init,
  #endif  
};

#define DOG_BARK_AFTER_BOOT 0x400 // ~31.2 ms
#define DOG_BITE_AFTER_BOOT 0x800 // ~62.5 ms

__attribute__((section("proxy_cram_reclaim_pool")))
static void init_time_proxy_votes(void)
{
  kvp_t *kvp;
  uint32 req = 1;
  static unsigned ENABLED_KEY = 0x62616e45;

  extern void Clock_SetSPMStates( kvp_t *kvp );

  //init time request for CXO on behalf of apps
  kvp = kvp_create(0);
  kvp_clear(kvp);
  kvp_put(kvp, ENABLED_KEY, sizeof(req), &req);
  rpm_send_init_proxy_vote(RPM_CLOCK_0_REQ, /*CXO is CLK0*/ 0, /*Apps is master 0*/ 0, kvp);
  
  Clock_SetSPMStates(kvp);
  kvp_destroy(kvp);
}

int main(void)
{
  unsigned i;
#ifdef INIT_PRNG_NO_TZ
  extern uint32 g_prng_config_enable;

  if (g_prng_config_enable == 0)
  {
    g_prng_config_enable = 1;
  }
#endif
  
  debug_cookie_init();
  
  DALSYS_PropsInit();
  exceptions_init();
  rpm_core_dump_init();
  rpm_mpu_init();
  busywait_init();
  timetick_init();
  dog_init();

  // We need to have the clock to message RAM turned on *before* we intiailize
  // the rpmserver driver.  This is *before* we initialize the clock driver,
  // so we have to just do it ourselves.
  // Sets the vote bit for msg_ram_hclk
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA, 1);

  mpm_init();
  time_service_init();
  swevent_init();

  SWEVENT(RPM_BOOT_STARTED);

  for(i = 0; i < ARRAY_SIZE(init_fcns); ++i)
  {
    dog_kick();
    init_fcns[i]();
  }

  dog_set_bark_time(DOG_BARK_AFTER_BOOT);
  dog_set_bite_time(DOG_BITE_AFTER_BOOT);

  init_time_proxy_votes();

  prevent_further_heap_frees();

  SWEVENT(RPM_BOOT_FINISHED);

  rpm_init_done();
  rpm_reclaim_code_ram_pool();
  rpm_mpu_enable_code_ram_protection(); /* must be after rpm_reclaim_code_ram_pool() to make sure 
                                           no instruction fetched from code ram reclaim pool */

  #define ever ;;
  for(ever)
  {
    assert(!intlock_nest_level);
    do
    {
      pmic_wdog_pet(0); //pet the pmic wdog as need, configure if needed.
      sched_run();
      assert(!intlock_nest_level);

      if(npa_num_pending_events() > 0)
      {
        npa_process_event();
        assert(!intlock_nest_level);
      }
      else
        break; // nothing to do--try to sleep
    } while(1);

    assert(!intlock_nest_level);
    INTLOCK();
    if(!npa_num_pending_events() && (sched_get_next_start() >= time_service_now()))
      sleep_perform_lpm();
    INTFREE();
    assert(!intlock_nest_level);
  }
}

