
#include <string.h>
#include "cortex-m3.h"
#include "comdef.h"
#include "rpm_hwio.h"
#include "HALhwio.h"
#include "coredump.h"
#include "image_layout.h"
#include "CoreVerify.h"
#include "swevent.h"
#include "busywait.h"
#include "Chipinfo.h"
#include "apcs_qgic_hwio.h"
#include "dog.h"

#define WATCHDOG_IRQ_NUM 49

void xport_rpm_isr(void) __irq;
void dog_bark_isr(void) __irq;
__weak void vmpm_isr(void) __irq;
__weak void settling_timer_isr(void) __irq;

typedef void (*ExecFuncPtr)(void) __irq;

extern void __main(void);

void tz_abort_sw_event(void)
{
    SWEVENT(RPM_TZ_HALT_INT_RECEIVED);
}

__asm void abort_isr(void) __irq
{
    IMPORT abort

    b abort             // Use b to not overwrite EXEC_RETURN value to make rpm_parse_faults.cmm work
}

__asm void tz_abort_isr(void) __irq
{
    IMPORT tz_abort_sw_event
    IMPORT abort

    sub sp, sp, #0x8    // Eight byte stack alignment is a requirement of AAPCS
    str lr, [sp]        // Save EXEC_RETURN value

    bl tz_abort_sw_event

    ldr lr, [sp]        // Restore EXEC_RETURN value
    add sp, sp, #0x8

    b abort             // Use b to not overwrite EXEC_RETURN value to make rpm_parse_faults.cmm work
}

void dcc_crc_error_abort_isr(void) __irq
{
    SWEVENT(RPM_DCC_CRC_ERR_INT_RECEIVED);
    abort();
}

void apps_non_secure_wd_bite_abort_isr(void) __irq
{
    SWEVENT(RPM_NON_SECURE_WD_BITE_INT_RECEIVED);

    if((Chipinfo_GetVersion() < CHIPINFO_VERSION(3,0)) && (Chipinfo_GetFamily() == CHIPINFO_FAMILY_MSM8996))
    {	  
        //busywait 500mS. This # is important per CR807848
	//Disable the rpm wdog because we are spinning for > rpm wdog timeout
        dog_enable(0);
        busywait(500000); 
        HWIO_OUTF(APCS_QGICDR_GICD_CTLR, ENABLE_GRP1, 0);
        HWIO_OUTF(APCS_QGICDR_GICD_CTLR, ENABLE_GRP0, 0);
        SWEVENT(RPM_CLEAR_APCS_QGICDR_COMPLETE);
    }	
    abort();
}

//This needs to be aligned to at least 0x200 bytes (see arm documentation for why).
static ExecFuncPtr vector_table[16+64] __attribute__((at(0x090800))) =
{
  // RPM's standard system-level Cortex-M3 exceptions come first.
  (ExecFuncPtr)0,       // Initial stack pointer - not used
  (ExecFuncPtr)__main,  // Image entry point
  abort_isr,            // NMI
  abort_isr,            // Hard fault
  abort_isr,            // Memory management
  abort_isr,            // Bus fault
  abort_isr,            // Usage fault
  0, 0, 0, 0,           // Reserved
  abort_isr,            // SVC
  abort_isr,            // Debug Monitor
  0,                    // Reserved
  abort_isr,            // PendSVC
  abort_isr,            // SysTick

  // RPM's NVIC interrupts start here.
  abort_isr,            // 0  - HYP SMD interrupt
  abort_isr,            // 1  - HYP VMPM interrupt
  abort_isr,            // 2  - HYP reserved interrupt
  abort_isr,            // 3  - APPS L3 shutdown_req
  dcc_crc_error_abort_isr, // 4  - dcc_crc_fail_int
  vmpm_isr,             // 5  - MPSS VMPM interrupt
  abort_isr,            // 6  - Monaco bringup_req
  apps_non_secure_wd_bite_abort_isr,            // 7  - <spare / reserved>
  xport_rpm_isr,        // 8  - APPS HLOS SMD interrupt
  vmpm_isr,             // 9  - APPS HLOS VMPM interrupt
  abort_isr,            // 10 - APPS HLOS reserved interrupt
  xport_rpm_isr,        // 11 - TZ SMD interrupt
  tz_abort_isr,         // 12 - TZ HALT interrupt
  abort_isr,            // 13 - TZ reserved interrupt
  abort_isr,            // 14 - Monaco shutdown_req
  abort_isr,            // 15 - dcc_task_done_int
  abort_isr,            // 16 - APCS_dbgCorePwrUpInt
  abort_isr,            // 17 - cpr_mx_rbif_irq[0]
  abort_isr,            // 18 - LPASS shutdown_req
  abort_isr,            // 19 - LPASS bringup_req
  xport_rpm_isr,        // 20 - LPASS SMD interrupt
  vmpm_isr,             // 21 - LPASS VMPM interrupt
  abort_isr,            // 22 - LPASS reserved interrupt
  abort_isr,            // 23 - cpr_mx_rbif_irq[1]
  abort_isr,            // 24 - Hydra[0] L2 bringup_req
  abort_isr,            // 25 - Hydra[0] L2 shutdown_req
  abort_isr,            // 26 - Hydra[1] L2 bringup_req
  abort_isr,            // 27 - Hydra[1] L2 shutdown_req
  abort_isr,            // 28 - APPS L3 bringup_req
  abort_isr,            // 29 - bimc_irq
  xport_rpm_isr,        // 30 - SSC SMD interrupt
  vmpm_isr,             // 31 - SSC VMPM interrupt
  abort_isr,            // 32 - SSC reserved interrupt
  abort_isr,            // 33 - <spare / reserved>
  abort_isr,            // 34 - vsense_alarm_irq
  abort_isr,            // 35 - qdss_clk_irq
  abort_isr,            // 36 - qdss_CSYSPWRUPREQ
  abort_isr,            // 37 - qdss_JTAGSW
  abort_isr,            // 38 - mmss_summary_irq
  abort_isr,            // 39 - tlmm summary_irq
  abort_isr,            // 40 - tlmm dir_conn_irq
  abort_isr,            // 41 - tsens_upper_lower_int
  abort_isr,            // 42 - MPM summary interrupt
  abort_isr,            // 43 - <spare / reserved>
  abort_isr,            // 44 - spdm_realtime_irq[0]
  abort_isr,            // 45 - cpr_mx_rbif_irq[2]
  xport_rpm_isr,              // 46 - MPSS SMD interrupt
  abort_isr,            // 47 - pmic_arbiter_done
  abort_isr,            // 48 - spmi_periph_irq
  dog_bark_isr,         // 49 - WDOG bark IRQ
  abort_isr,            // 50 - QTimer[0] IRQ
  settling_timer_isr,   // 51 - QTimer[1] IRQ
  abort_isr,            // 52 - Debug CTI[0]
  abort_isr,            // 53 - Debug CTI[1]
  abort_isr,            // 54 - cpr_cx_rbif_irq
  abort_isr,            // 55 - cpr_mx_rbif_irq[3]
  abort_isr,            // 56 - SSC bringup_req
  abort_isr,            // 57 - SSC shutdown_req
  abort_isr,            // 58 - MPSS reserved interrupt
  abort_isr,            // 59 - bus_timeout_irq
  abort_isr,            // 60 - cpr_gfx_rbif_irq
  abort_isr,            // 61 - <spare / reserved>
  abort_isr,            // 62 - <spare / reserved>
  abort_isr,            // 63 - <spare / reserved>
};

void exceptions_init(void)
{
  int i, num_ints;

  // Disable interrupts globally.
  __disable_irq();

  // Default all RPM interrupts to rising edge.
  HWIO_OUT(RPM_INTR_POLARITY_0, 0xFFFFFFFF);
  HWIO_OUT(RPM_INTR_POLARITY_1, 0xFFFFFFFF);
  HWIO_OUT(RPM_INTR_EDG_LVL_0,  0xFFFFFFFF);
  HWIO_OUT(RPM_INTR_EDG_LVL_1,  0xFFFFFFFF);

  // Figure out how many interrupts are supported on this platform.
  num_ints = ((SCS.IntCtrlType & 0x1F) + 1) << 5;

  // Disable all interrupts.
  for(i = 0; i < num_ints / 32; ++i)
    SCS.NVIC.Disable[i] = 0xFFFFFFFF;

  // Clear all interrupts.
  for(i = 0; i < num_ints / 32; ++i)
    SCS.NVIC.Clear[i] = 0xFFFFFFFF;

  // Set all interrupt levels as pre-empting.
  SCS.AIRC = 0x05FA0000;

  // based on the size of vector_table and method of vector_table indexing (i (upto < num_ints) + 16)
  CORE_VERIFY( num_ints <= 64 );

  // Set all interrupts to lowest priority.
  for(i = 0; i < num_ints / 4; ++i)
    SCS.NVIC.Priority[i] = 0xFFFFFFFF;

  // Configure the exception vector table.
  SCS.ExceptionTableOffset = (unsigned)&vector_table;

  // Enable traps on divides by 0 and double-word stack alignment.
  SCS.ConfigCtrl |= 0x210;

  // Memory/Bus/Usage faults get highest user priority.
  SCS.SystemPriority[0] = 0;

  // Other system exceptions get middling priority.
  SCS.SystemPriority[1] = 0xC0000000; // SVC = 0xC0
  SCS.SystemPriority[2] = 0xFFFF0080; // SysTick and PendSV = 0xFF, Debug = 0x80

  // Enable the usage, bus, and memory management fault handlers.
  SCS.SystemHandlerCtrlAndState = 0x70000;

  // Now that we're all set, go through and re-enable interrupts that have handlers.
  for(i = 0; i < num_ints; ++i)
  {
    // Check for weakly linked handler that doesn't exist yet.
    // Note the +16 that skips system handlers.
    if(!vector_table[i + 16])
        vector_table[i + 16] = abort_isr;

    if(abort_isr != vector_table[i + 16])
      SCS.NVIC.Enable[i/32] = (1 << (i % 32));
  }

  // Give the watchdog bark IRQ the same priority as a fault.
  interrupt_set_priority(WATCHDOG_IRQ_NUM, 0);

  // Enable fault handling.
  set_basepri(0);
  INTLOCK();
  __enable_irq();
}

void exceptions_enable(void)
{
    INTFREE();
}

void interrupt_soft_trigger(unsigned num)
{
    CORE_VERIFY(num < 64);
    STIR.SoftwareTriggerInterrupt = num;
}

void interrupt_set_isr(unsigned num, isr_type isr)
{
    vector_table[16+num] = isr;
}

void interrupt_configure(unsigned num, interrupt_config config)
{
    volatile unsigned *RPM_INTERRUPT_POLARITY = (unsigned int *)HWIO_RPM_INTR_POLARITY_0_ADDR;
    volatile unsigned *RPM_INTERRUPT_EDGE = (unsigned int *)HWIO_RPM_INTR_EDG_LVL_0_ADDR;

    switch(config)
    {
        case RISING_EDGE:
            RPM_INTERRUPT_POLARITY[num/32] |=  (1 << (num % 32));
            RPM_INTERRUPT_EDGE[num/32]     |=  (1 << (num % 32));
            break;

        case FALLING_EDGE:
            RPM_INTERRUPT_POLARITY[num/32] &= ~(1 << (num % 32));
            RPM_INTERRUPT_EDGE[num/32]     |=  (1 << (num % 32));
            break;

        case LEVEL_HIGH:
            RPM_INTERRUPT_POLARITY[num/32] |=  (1 << (num % 32));
            RPM_INTERRUPT_EDGE[num/32]     &= ~(1 << (num % 32));
            break;

        case LEVEL_LOW:
            RPM_INTERRUPT_POLARITY[num/32] &= ~(1 << (num % 32));
            RPM_INTERRUPT_EDGE[num/32]     &= ~(1 << (num % 32));
            break;
    }
}

void interrupt_set_priority(unsigned num, unsigned priority)
{
    unsigned priority_word = SCS.NVIC.Priority[num/4];
    unsigned priority_offset = 8 * (num % 4);
    unsigned priority_mask = ~(0xff << priority_offset);

    SCS.NVIC.Priority[num/4] = (priority_word & priority_mask) | ((0xff & priority) << priority_offset);
}

unsigned intlock_nest_level = 0;

void lock_ints(void)
{
#ifdef VIRTIO_WORKAROUND
  // Disable interrupts globally.
  __disable_irq();
#endif

  unsigned _was_masked = get_basepri();
  set_basepri(0x10);
  assert(!intlock_nest_level || _was_masked);
  ++intlock_nest_level;

#ifdef VIRTIO_WORKAROUND
  __enable_irq();
#endif
}

void unlock_ints(void)
{
#ifdef VIRTIO_WORKAROUND
  // Disable interrupts globally.
  __disable_irq();
#endif

  unsigned _is_masked = get_basepri();
  assert(intlock_nest_level && _is_masked);
  if(!--intlock_nest_level)
  {
    set_basepri(0);
  }

#ifdef VIRTIO_WORKAROUND
  __enable_irq();
#endif
}
/*===========================================================================
FUNCTION: is_any_interrupt_pending

DESCRIPTION: 
ARM TRM says that ISRPENDING indicates a pending interrupt irrespective
of whether it is enabled or not. So This cannot be used for checking.
VECTPENDING gives the highest priority pending exception, but it also includes the 
effect of the BASEPRI and FAULTMASK registers, but not any effect of the PRIMASK register. 
We�re in an intlocked context, and we use BASEPRI as our intlock mechanism. 
BASEPRI will be 16, and thus VECTPENDING will effectively mask out all of the other interrupts.
Instead we check NVIC Enabled and Set registers to understand whether interrupts
are pending.

RETURN VALUE: True if any interrupt is pending
===========================================================================*/
bool is_any_interrupt_pending(void)
{
  int num_ints, i;
  bool result = false;
  num_ints = ((SCS.IntCtrlType & 0x1F) + 1) << 5;
  for(i = 0; ((i < num_ints / 32) && !result); i++)
  {
    if (SCS.NVIC.Enable[i] & SCS.NVIC.Set[i])
      result = true;
  }
  return result;
}

