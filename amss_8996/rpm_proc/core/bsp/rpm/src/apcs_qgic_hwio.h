#ifndef __APCS_QGIC_HWIO_H__
#define __APCS_QGIC_HWIO_H__
/*
===========================================================================
*/
/**
  @file apcs_qgic_hwio.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8996 (Istari) [istari_v1.0_p2q1r8.1.8_MTO]
 
  This file contains HWIO register definitions for the following modules:
    APCS_QGICDR_QGICDR_GICD

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/rpm.bf/1.6/core/bsp/rpm/src/apcs_qgic_hwio.h#1 $
  $DateTime: 2015/03/23 14:47:31 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * MODULE: APCS_QGICDR_QGICDR_GICD
 *--------------------------------------------------------------------------*/

#define                                                                    HMSS_BASE 0x69800000
#define APCS_QGICDR_QGICDR_GICD_REG_BASE                                  (HMSS_BASE      + 0x003c0000)

#define HWIO_APCS_QGICDR_GICD_CTLR_ADDR                                   (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000000)
#define HWIO_APCS_QGICDR_GICD_CTLR_RMSK                                   0x800000f7
#define HWIO_APCS_QGICDR_GICD_CTLR_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CTLR_ADDR, HWIO_APCS_QGICDR_GICD_CTLR_RMSK)
#define HWIO_APCS_QGICDR_GICD_CTLR_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CTLR_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_CTLR_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_CTLR_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_CTLR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_CTLR_ADDR,m,v,HWIO_APCS_QGICDR_GICD_CTLR_IN)
#define HWIO_APCS_QGICDR_GICD_CTLR_RWP_BMSK                               0x80000000
#define HWIO_APCS_QGICDR_GICD_CTLR_RWP_SHFT                                     0x1f
#define HWIO_APCS_QGICDR_GICD_CTLR_E1NWF_BMSK                                   0x80
#define HWIO_APCS_QGICDR_GICD_CTLR_E1NWF_SHFT                                    0x7
#define HWIO_APCS_QGICDR_GICD_CTLR_DS_BMSK                                      0x40
#define HWIO_APCS_QGICDR_GICD_CTLR_DS_SHFT                                       0x6
#define HWIO_APCS_QGICDR_GICD_CTLR_ARE_NS_BMSK                                  0x20
#define HWIO_APCS_QGICDR_GICD_CTLR_ARE_NS_SHFT                                   0x5
#define HWIO_APCS_QGICDR_GICD_CTLR_ARE_S_BMSK                                   0x10
#define HWIO_APCS_QGICDR_GICD_CTLR_ARE_S_SHFT                                    0x4
#define HWIO_APCS_QGICDR_GICD_CTLR_ENABLE_GRP1_SEC_BMSK                          0x4
#define HWIO_APCS_QGICDR_GICD_CTLR_ENABLE_GRP1_SEC_SHFT                          0x2
#define HWIO_APCS_QGICDR_GICD_CTLR_ENABLE_GRP1_BMSK                              0x2
#define HWIO_APCS_QGICDR_GICD_CTLR_ENABLE_GRP1_SHFT                              0x1
#define HWIO_APCS_QGICDR_GICD_CTLR_ENABLE_GRP0_BMSK                              0x1
#define HWIO_APCS_QGICDR_GICD_CTLR_ENABLE_GRP0_SHFT                              0x0

#define HWIO_APCS_QGICDR_GICD_TYPER_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000004)
#define HWIO_APCS_QGICDR_GICD_TYPER_RMSK                                   0x1fffcff
#define HWIO_APCS_QGICDR_GICD_TYPER_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_TYPER_ADDR, HWIO_APCS_QGICDR_GICD_TYPER_RMSK)
#define HWIO_APCS_QGICDR_GICD_TYPER_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_TYPER_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_TYPER_A3V_BMSK                               0x1000000
#define HWIO_APCS_QGICDR_GICD_TYPER_A3V_SHFT                                    0x18
#define HWIO_APCS_QGICDR_GICD_TYPER_IDBITS_BMSK                             0xf80000
#define HWIO_APCS_QGICDR_GICD_TYPER_IDBITS_SHFT                                 0x13
#define HWIO_APCS_QGICDR_GICD_TYPER_DVIS_BMSK                                0x40000
#define HWIO_APCS_QGICDR_GICD_TYPER_DVIS_SHFT                                   0x12
#define HWIO_APCS_QGICDR_GICD_TYPER_LPIS_BMSK                                0x20000
#define HWIO_APCS_QGICDR_GICD_TYPER_LPIS_SHFT                                   0x11
#define HWIO_APCS_QGICDR_GICD_TYPER_MBIS_BMSK                                0x10000
#define HWIO_APCS_QGICDR_GICD_TYPER_MBIS_SHFT                                   0x10
#define HWIO_APCS_QGICDR_GICD_TYPER_LSPI_BMSK                                 0xf800
#define HWIO_APCS_QGICDR_GICD_TYPER_LSPI_SHFT                                    0xb
#define HWIO_APCS_QGICDR_GICD_TYPER_TZ_BMSK                                    0x400
#define HWIO_APCS_QGICDR_GICD_TYPER_TZ_SHFT                                      0xa
#define HWIO_APCS_QGICDR_GICD_TYPER_CPU_NUM_BMSK                                0xe0
#define HWIO_APCS_QGICDR_GICD_TYPER_CPU_NUM_SHFT                                 0x5
#define HWIO_APCS_QGICDR_GICD_TYPER_IT_LINES_BMSK                               0x1f
#define HWIO_APCS_QGICDR_GICD_TYPER_IT_LINES_SHFT                                0x0

#define HWIO_APCS_QGICDR_GICD_IIDR_ADDR                                   (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000008)
#define HWIO_APCS_QGICDR_GICD_IIDR_RMSK                                       0xffff
#define HWIO_APCS_QGICDR_GICD_IIDR_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IIDR_ADDR, HWIO_APCS_QGICDR_GICD_IIDR_RMSK)
#define HWIO_APCS_QGICDR_GICD_IIDR_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IIDR_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_IIDR_REVISION_BMSK                              0xf000
#define HWIO_APCS_QGICDR_GICD_IIDR_REVISION_SHFT                                 0xc
#define HWIO_APCS_QGICDR_GICD_IIDR_IMPLEMENTER_BMSK                            0xfff
#define HWIO_APCS_QGICDR_GICD_IIDR_IMPLEMENTER_SHFT                              0x0

#define HWIO_APCS_QGICDR_GICD_ANSACR_ADDR                                 (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000020)
#define HWIO_APCS_QGICDR_GICD_ANSACR_RMSK                                        0x3
#define HWIO_APCS_QGICDR_GICD_ANSACR_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ANSACR_ADDR, HWIO_APCS_QGICDR_GICD_ANSACR_RMSK)
#define HWIO_APCS_QGICDR_GICD_ANSACR_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ANSACR_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_ANSACR_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_ANSACR_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_ANSACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_ANSACR_ADDR,m,v,HWIO_APCS_QGICDR_GICD_ANSACR_IN)
#define HWIO_APCS_QGICDR_GICD_ANSACR_GICD_CLK_TMR_CTLR_BMSK                      0x2
#define HWIO_APCS_QGICDR_GICD_ANSACR_GICD_CLK_TMR_CTLR_SHFT                      0x1
#define HWIO_APCS_QGICDR_GICD_ANSACR_GICD_CGCR_BMSK                              0x1
#define HWIO_APCS_QGICDR_GICD_ANSACR_GICD_CGCR_SHFT                              0x0

#define HWIO_APCS_QGICDR_GICD_CGCR_ADDR                                   (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000024)
#define HWIO_APCS_QGICDR_GICD_CGCR_RMSK                                      0x10006
#define HWIO_APCS_QGICDR_GICD_CGCR_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CGCR_ADDR, HWIO_APCS_QGICDR_GICD_CGCR_RMSK)
#define HWIO_APCS_QGICDR_GICD_CGCR_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CGCR_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_CGCR_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_CGCR_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_CGCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_CGCR_ADDR,m,v,HWIO_APCS_QGICDR_GICD_CGCR_IN)
#define HWIO_APCS_QGICDR_GICD_CGCR_TOP_BMSK                                  0x10000
#define HWIO_APCS_QGICDR_GICD_CGCR_TOP_SHFT                                     0x10
#define HWIO_APCS_QGICDR_GICD_CGCR_DI_SPI_STATE_BMSK                             0x4
#define HWIO_APCS_QGICDR_GICD_CGCR_DI_SPI_STATE_SHFT                             0x2
#define HWIO_APCS_QGICDR_GICD_CGCR_DI_SPI_DEMET_BMSK                             0x2
#define HWIO_APCS_QGICDR_GICD_CGCR_DI_SPI_DEMET_SHFT                             0x1

#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_ADDR                           (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000028)
#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_RMSK                             0xffffff
#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_ADDR, HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_RMSK)
#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_ADDR,m,v,HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_IN)
#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_WAKE_GUARD_TIMER_BMSK            0xff0000
#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_WAKE_GUARD_TIMER_SHFT                0x10
#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_SHTDWN_TIMER_BMSK                  0xff00
#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_SHTDWN_TIMER_SHFT                     0x8
#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_HYSTER_TIMER_BMSK                    0xff
#define HWIO_APCS_QGICDR_GICD_CLK_TMR_CTLR_HYSTER_TIMER_SHFT                     0x0

#define HWIO_APCS_QGICDR_GICD_HW_VERSION_ADDR                             (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000030)
#define HWIO_APCS_QGICDR_GICD_HW_VERSION_RMSK                             0xffffffff
#define HWIO_APCS_QGICDR_GICD_HW_VERSION_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_HW_VERSION_ADDR, HWIO_APCS_QGICDR_GICD_HW_VERSION_RMSK)
#define HWIO_APCS_QGICDR_GICD_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_HW_VERSION_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_HW_VERSION_MAJOR_BMSK                       0xf0000000
#define HWIO_APCS_QGICDR_GICD_HW_VERSION_MAJOR_SHFT                             0x1c
#define HWIO_APCS_QGICDR_GICD_HW_VERSION_MINOR_BMSK                        0xfff0000
#define HWIO_APCS_QGICDR_GICD_HW_VERSION_MINOR_SHFT                             0x10
#define HWIO_APCS_QGICDR_GICD_HW_VERSION_STEP_BMSK                            0xffff
#define HWIO_APCS_QGICDR_GICD_HW_VERSION_STEP_SHFT                               0x0

#define HWIO_APCS_QGICDR_GICD_ARB_CTLR_ADDR                               (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000034)
#define HWIO_APCS_QGICDR_GICD_ARB_CTLR_RMSK                                  0x30000
#define HWIO_APCS_QGICDR_GICD_ARB_CTLR_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ARB_CTLR_ADDR, HWIO_APCS_QGICDR_GICD_ARB_CTLR_RMSK)
#define HWIO_APCS_QGICDR_GICD_ARB_CTLR_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ARB_CTLR_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_ARB_CTLR_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_ARB_CTLR_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_ARB_CTLR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_ARB_CTLR_ADDR,m,v,HWIO_APCS_QGICDR_GICD_ARB_CTLR_IN)
#define HWIO_APCS_QGICDR_GICD_ARB_CTLR_FORCE_RUN_ARBITER_BMSK                0x20000
#define HWIO_APCS_QGICDR_GICD_ARB_CTLR_FORCE_RUN_ARBITER_SHFT                   0x11
#define HWIO_APCS_QGICDR_GICD_ARB_CTLR_ARB_STATUS_BMSK                       0x10000
#define HWIO_APCS_QGICDR_GICD_ARB_CTLR_ARB_STATUS_SHFT                          0x10

#define HWIO_APCS_QGICDR_GICD_SETSPI_NSR_ADDR                             (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000040)
#define HWIO_APCS_QGICDR_GICD_SETSPI_NSR_RMSK                                  0x3ff
#define HWIO_APCS_QGICDR_GICD_SETSPI_NSR_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_SETSPI_NSR_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_SETSPI_NSR_SPI_ID_BMSK                           0x3ff
#define HWIO_APCS_QGICDR_GICD_SETSPI_NSR_SPI_ID_SHFT                             0x0

#define HWIO_APCS_QGICDR_GICD_CLRSPI_NSR_ADDR                             (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000048)
#define HWIO_APCS_QGICDR_GICD_CLRSPI_NSR_RMSK                                  0x3ff
#define HWIO_APCS_QGICDR_GICD_CLRSPI_NSR_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_CLRSPI_NSR_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_CLRSPI_NSR_SPI_ID_BMSK                           0x3ff
#define HWIO_APCS_QGICDR_GICD_CLRSPI_NSR_SPI_ID_SHFT                             0x0

#define HWIO_APCS_QGICDR_GICD_SETSPI_SR_ADDR                              (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000050)
#define HWIO_APCS_QGICDR_GICD_SETSPI_SR_RMSK                                   0x3ff
#define HWIO_APCS_QGICDR_GICD_SETSPI_SR_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_SETSPI_SR_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_SETSPI_SR_SPI_ID_BMSK                            0x3ff
#define HWIO_APCS_QGICDR_GICD_SETSPI_SR_SPI_ID_SHFT                              0x0

#define HWIO_APCS_QGICDR_GICD_CLRSPI_SR_ADDR                              (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000058)
#define HWIO_APCS_QGICDR_GICD_CLRSPI_SR_RMSK                                   0x3ff
#define HWIO_APCS_QGICDR_GICD_CLRSPI_SR_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_CLRSPI_SR_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_CLRSPI_SR_SPI_ID_BMSK                            0x3ff
#define HWIO_APCS_QGICDR_GICD_CLRSPI_SR_SPI_ID_SHFT                              0x0

#define HWIO_APCS_QGICDR_GICD_IGROUPRn_ADDR(n)                            (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000080 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_IGROUPRn_RMSK                               0xffffffff
#define HWIO_APCS_QGICDR_GICD_IGROUPRn_MAXn                                       17
#define HWIO_APCS_QGICDR_GICD_IGROUPRn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IGROUPRn_ADDR(n), HWIO_APCS_QGICDR_GICD_IGROUPRn_RMSK)
#define HWIO_APCS_QGICDR_GICD_IGROUPRn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IGROUPRn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_IGROUPRn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_IGROUPRn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_IGROUPRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_IGROUPRn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_IGROUPRn_INI(n))
#define HWIO_APCS_QGICDR_GICD_IGROUPRn_INT_NS_BMSK                        0xffffffff
#define HWIO_APCS_QGICDR_GICD_IGROUPRn_INT_NS_SHFT                               0x0

#define HWIO_APCS_QGICDR_GICD_ISENABLERn_ADDR(n)                          (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000100 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_ISENABLERn_RMSK                             0xffffffff
#define HWIO_APCS_QGICDR_GICD_ISENABLERn_MAXn                                     17
#define HWIO_APCS_QGICDR_GICD_ISENABLERn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ISENABLERn_ADDR(n), HWIO_APCS_QGICDR_GICD_ISENABLERn_RMSK)
#define HWIO_APCS_QGICDR_GICD_ISENABLERn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ISENABLERn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_ISENABLERn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_ISENABLERn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_ISENABLERn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_ISENABLERn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_ISENABLERn_INI(n))
#define HWIO_APCS_QGICDR_GICD_ISENABLERn_INT_BMSK                         0xffffffff
#define HWIO_APCS_QGICDR_GICD_ISENABLERn_INT_SHFT                                0x0

#define HWIO_APCS_QGICDR_GICD_ICENABLERn_ADDR(n)                          (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000180 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_ICENABLERn_RMSK                             0xffffffff
#define HWIO_APCS_QGICDR_GICD_ICENABLERn_MAXn                                     17
#define HWIO_APCS_QGICDR_GICD_ICENABLERn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICENABLERn_ADDR(n), HWIO_APCS_QGICDR_GICD_ICENABLERn_RMSK)
#define HWIO_APCS_QGICDR_GICD_ICENABLERn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICENABLERn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_ICENABLERn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_ICENABLERn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_ICENABLERn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_ICENABLERn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_ICENABLERn_INI(n))
#define HWIO_APCS_QGICDR_GICD_ICENABLERn_INT_BMSK                         0xffffffff
#define HWIO_APCS_QGICDR_GICD_ICENABLERn_INT_SHFT                                0x0

#define HWIO_APCS_QGICDR_GICD_ISPENDRn_ADDR(n)                            (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000200 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_ISPENDRn_RMSK                               0xffffffff
#define HWIO_APCS_QGICDR_GICD_ISPENDRn_MAXn                                       17
#define HWIO_APCS_QGICDR_GICD_ISPENDRn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ISPENDRn_ADDR(n), HWIO_APCS_QGICDR_GICD_ISPENDRn_RMSK)
#define HWIO_APCS_QGICDR_GICD_ISPENDRn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ISPENDRn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_ISPENDRn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_ISPENDRn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_ISPENDRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_ISPENDRn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_ISPENDRn_INI(n))
#define HWIO_APCS_QGICDR_GICD_ISPENDRn_INT_BMSK                           0xffffffff
#define HWIO_APCS_QGICDR_GICD_ISPENDRn_INT_SHFT                                  0x0

#define HWIO_APCS_QGICDR_GICD_ICPENDRn_ADDR(n)                            (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000280 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_ICPENDRn_RMSK                               0xffffffff
#define HWIO_APCS_QGICDR_GICD_ICPENDRn_MAXn                                       17
#define HWIO_APCS_QGICDR_GICD_ICPENDRn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICPENDRn_ADDR(n), HWIO_APCS_QGICDR_GICD_ICPENDRn_RMSK)
#define HWIO_APCS_QGICDR_GICD_ICPENDRn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICPENDRn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_ICPENDRn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_ICPENDRn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_ICPENDRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_ICPENDRn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_ICPENDRn_INI(n))
#define HWIO_APCS_QGICDR_GICD_ICPENDRn_INT_BMSK                           0xffffffff
#define HWIO_APCS_QGICDR_GICD_ICPENDRn_INT_SHFT                                  0x0

#define HWIO_APCS_QGICDR_GICD_ISACTIVERn_ADDR(n)                          (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000300 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_ISACTIVERn_RMSK                             0xffffffff
#define HWIO_APCS_QGICDR_GICD_ISACTIVERn_MAXn                                     17
#define HWIO_APCS_QGICDR_GICD_ISACTIVERn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ISACTIVERn_ADDR(n), HWIO_APCS_QGICDR_GICD_ISACTIVERn_RMSK)
#define HWIO_APCS_QGICDR_GICD_ISACTIVERn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ISACTIVERn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_ISACTIVERn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_ISACTIVERn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_ISACTIVERn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_ISACTIVERn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_ISACTIVERn_INI(n))
#define HWIO_APCS_QGICDR_GICD_ISACTIVERn_INT_BMSK                         0xffffffff
#define HWIO_APCS_QGICDR_GICD_ISACTIVERn_INT_SHFT                                0x0

#define HWIO_APCS_QGICDR_GICD_ICACTIVERn_ADDR(n)                          (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000380 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_ICACTIVERn_RMSK                             0xffffffff
#define HWIO_APCS_QGICDR_GICD_ICACTIVERn_MAXn                                     17
#define HWIO_APCS_QGICDR_GICD_ICACTIVERn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICACTIVERn_ADDR(n), HWIO_APCS_QGICDR_GICD_ICACTIVERn_RMSK)
#define HWIO_APCS_QGICDR_GICD_ICACTIVERn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICACTIVERn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_ICACTIVERn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_ICACTIVERn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_ICACTIVERn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_ICACTIVERn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_ICACTIVERn_INI(n))
#define HWIO_APCS_QGICDR_GICD_ICACTIVERn_INT_BMSK                         0xffffffff
#define HWIO_APCS_QGICDR_GICD_ICACTIVERn_INT_SHFT                                0x0

#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_ADDR(n)                         (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000400 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_RMSK                            0xf8f8f8f8
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_MAXn                                     7
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IPRIORITYRn_ADDR(n), HWIO_APCS_QGICDR_GICD_IPRIORITYRn_RMSK)
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IPRIORITYRn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_IPRIORITYRn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_IPRIORITYRn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_IPRIORITYRn_INI(n))
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_INT3_BMSK                       0xf8000000
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_INT3_SHFT                             0x1b
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_INT2_BMSK                         0xf80000
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_INT2_SHFT                             0x13
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_INT1_BMSK                           0xf800
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_INT1_SHFT                              0xb
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_INT0_BMSK                             0xf8
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRn_INT0_SHFT                              0x3

#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_ADDR(m)                         (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000400 + 0x4 * (m))
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_RMSK                            0xf8f8f8f8
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_MAXm                                   143
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_INI(m)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IPRIORITYRm_ADDR(m), HWIO_APCS_QGICDR_GICD_IPRIORITYRm_RMSK)
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_INMI(m,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IPRIORITYRm_ADDR(m), mask)
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_OUTI(m,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_IPRIORITYRm_ADDR(m),val)
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_OUTMI(m,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_IPRIORITYRm_ADDR(m),mask,val,HWIO_APCS_QGICDR_GICD_IPRIORITYRm_INI(m))
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_INT3_BMSK                       0xf8000000
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_INT3_SHFT                             0x1b
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_INT2_BMSK                         0xf80000
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_INT2_SHFT                             0x13
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_INT1_BMSK                           0xf800
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_INT1_SHFT                              0xb
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_INT0_BMSK                             0xf8
#define HWIO_APCS_QGICDR_GICD_IPRIORITYRm_INT0_SHFT                              0x3

#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_ADDR(n)                          (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000800 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_RMSK                              0xf0f0f0f
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_MAXn                                      7
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ITARGETSRn_ADDR(n), HWIO_APCS_QGICDR_GICD_ITARGETSRn_RMSK)
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ITARGETSRn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_INT3_BMSK                         0xf000000
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_INT3_SHFT                              0x18
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_INT2_BMSK                           0xf0000
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_INT2_SHFT                              0x10
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_INT1_BMSK                             0xf00
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_INT1_SHFT                               0x8
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_INT0_BMSK                               0xf
#define HWIO_APCS_QGICDR_GICD_ITARGETSRn_INT0_SHFT                               0x0

#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_ADDR(m)                          (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000800 + 0x4 * (m))
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_RMSK                              0xf0f0f0f
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_MAXm                                    143
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_INI(m)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ITARGETSRm_ADDR(m), HWIO_APCS_QGICDR_GICD_ITARGETSRm_RMSK)
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_INMI(m,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ITARGETSRm_ADDR(m), mask)
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_OUTI(m,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_ITARGETSRm_ADDR(m),val)
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_OUTMI(m,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_ITARGETSRm_ADDR(m),mask,val,HWIO_APCS_QGICDR_GICD_ITARGETSRm_INI(m))
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_INT3_BMSK                         0xf000000
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_INT3_SHFT                              0x18
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_INT2_BMSK                           0xf0000
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_INT2_SHFT                              0x10
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_INT1_BMSK                             0xf00
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_INT1_SHFT                               0x8
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_INT0_BMSK                               0xf
#define HWIO_APCS_QGICDR_GICD_ITARGETSRm_INT0_SHFT                               0x0

#define HWIO_APCS_QGICDR_GICD_ICFGR0_ADDR                                 (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000c00)
#define HWIO_APCS_QGICDR_GICD_ICFGR0_RMSK                                 0xaaaaaaaa
#define HWIO_APCS_QGICDR_GICD_ICFGR0_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICFGR0_ADDR, HWIO_APCS_QGICDR_GICD_ICFGR0_RMSK)
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICFGR0_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT15_BMSK                           0x80000000
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT15_SHFT                                 0x1f
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT14_BMSK                           0x20000000
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT14_SHFT                                 0x1d
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT13_BMSK                            0x8000000
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT13_SHFT                                 0x1b
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT12_BMSK                            0x2000000
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT12_SHFT                                 0x19
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT11_BMSK                             0x800000
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT11_SHFT                                 0x17
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT10_BMSK                             0x200000
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT10_SHFT                                 0x15
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT9_BMSK                               0x80000
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT9_SHFT                                  0x13
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT8_BMSK                               0x20000
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT8_SHFT                                  0x11
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT7_BMSK                                0x8000
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT7_SHFT                                   0xf
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT6_BMSK                                0x2000
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT6_SHFT                                   0xd
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT5_BMSK                                 0x800
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT5_SHFT                                   0xb
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT4_BMSK                                 0x200
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT4_SHFT                                   0x9
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT3_BMSK                                  0x80
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT3_SHFT                                   0x7
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT2_BMSK                                  0x20
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT2_SHFT                                   0x5
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT1_BMSK                                   0x8
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT1_SHFT                                   0x3
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT0_BMSK                                   0x2
#define HWIO_APCS_QGICDR_GICD_ICFGR0_INT0_SHFT                                   0x1

#define HWIO_APCS_QGICDR_GICD_ICFGR1_ADDR                                 (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000c04)
#define HWIO_APCS_QGICDR_GICD_ICFGR1_RMSK                                 0xaaaaaaaa
#define HWIO_APCS_QGICDR_GICD_ICFGR1_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICFGR1_ADDR, HWIO_APCS_QGICDR_GICD_ICFGR1_RMSK)
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICFGR1_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_ICFGR1_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_ICFGR1_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_ICFGR1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_ICFGR1_ADDR,m,v,HWIO_APCS_QGICDR_GICD_ICFGR1_IN)
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT15_BMSK                           0x80000000
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT15_SHFT                                 0x1f
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT14_BMSK                           0x20000000
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT14_SHFT                                 0x1d
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT13_BMSK                            0x8000000
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT13_SHFT                                 0x1b
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT12_BMSK                            0x2000000
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT12_SHFT                                 0x19
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT11_BMSK                             0x800000
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT11_SHFT                                 0x17
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT10_BMSK                             0x200000
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT10_SHFT                                 0x15
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT9_BMSK                               0x80000
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT9_SHFT                                  0x13
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT8_BMSK                               0x20000
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT8_SHFT                                  0x11
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT7_BMSK                                0x8000
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT7_SHFT                                   0xf
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT6_BMSK                                0x2000
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT6_SHFT                                   0xd
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT5_BMSK                                 0x800
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT5_SHFT                                   0xb
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT4_BMSK                                 0x200
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT4_SHFT                                   0x9
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT3_BMSK                                  0x80
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT3_SHFT                                   0x7
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT2_BMSK                                  0x20
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT2_SHFT                                   0x5
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT1_BMSK                                   0x8
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT1_SHFT                                   0x3
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT0_BMSK                                   0x2
#define HWIO_APCS_QGICDR_GICD_ICFGR1_INT0_SHFT                                   0x1

#define HWIO_APCS_QGICDR_GICD_ICFGRn_ADDR(n)                              (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000c00 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_ICFGRn_RMSK                                 0xaaaaaaaa
#define HWIO_APCS_QGICDR_GICD_ICFGRn_MAXn                                         35
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICFGRn_ADDR(n), HWIO_APCS_QGICDR_GICD_ICFGRn_RMSK)
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_ICFGRn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_ICFGRn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_ICFGRn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_ICFGRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_ICFGRn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_ICFGRn_INI(n))
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT15_BMSK                           0x80000000
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT15_SHFT                                 0x1f
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT14_BMSK                           0x20000000
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT14_SHFT                                 0x1d
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT13_BMSK                            0x8000000
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT13_SHFT                                 0x1b
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT12_BMSK                            0x2000000
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT12_SHFT                                 0x19
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT11_BMSK                             0x800000
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT11_SHFT                                 0x17
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT10_BMSK                             0x200000
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT10_SHFT                                 0x15
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT9_BMSK                               0x80000
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT9_SHFT                                  0x13
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT8_BMSK                               0x20000
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT8_SHFT                                  0x11
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT7_BMSK                                0x8000
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT7_SHFT                                   0xf
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT6_BMSK                                0x2000
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT6_SHFT                                   0xd
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT5_BMSK                                 0x800
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT5_SHFT                                   0xb
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT4_BMSK                                 0x200
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT4_SHFT                                   0x9
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT3_BMSK                                  0x80
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT3_SHFT                                   0x7
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT2_BMSK                                  0x20
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT2_SHFT                                   0x5
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT1_BMSK                                   0x8
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT1_SHFT                                   0x3
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT0_BMSK                                   0x2
#define HWIO_APCS_QGICDR_GICD_ICFGRn_INT0_SHFT                                   0x1

#define HWIO_APCS_QGICDR_GICD_IGRPMODRn_ADDR(n)                           (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000d00 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_IGRPMODRn_RMSK                              0xffffffff
#define HWIO_APCS_QGICDR_GICD_IGRPMODRn_MAXn                                      17
#define HWIO_APCS_QGICDR_GICD_IGRPMODRn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IGRPMODRn_ADDR(n), HWIO_APCS_QGICDR_GICD_IGRPMODRn_RMSK)
#define HWIO_APCS_QGICDR_GICD_IGRPMODRn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IGRPMODRn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_IGRPMODRn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_IGRPMODRn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_IGRPMODRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_IGRPMODRn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_IGRPMODRn_INI(n))
#define HWIO_APCS_QGICDR_GICD_IGRPMODRn_INT_NS_BMSK                       0xffffffff
#define HWIO_APCS_QGICDR_GICD_IGRPMODRn_INT_NS_SHFT                              0x0

#define HWIO_APCS_QGICDR_GICD_NSACR0_ADDR                                 (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000e00)
#define HWIO_APCS_QGICDR_GICD_NSACR0_RMSK                                 0xffffffff
#define HWIO_APCS_QGICDR_GICD_NSACR0_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_NSACR0_ADDR, HWIO_APCS_QGICDR_GICD_NSACR0_RMSK)
#define HWIO_APCS_QGICDR_GICD_NSACR0_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_NSACR0_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_NSACR0_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_NSACR0_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_NSACR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_NSACR0_ADDR,m,v,HWIO_APCS_QGICDR_GICD_NSACR0_IN)
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI15_BMSK                 0xc0000000
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI15_SHFT                       0x1e
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI14_BMSK                 0x30000000
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI14_SHFT                       0x1c
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI13_BMSK                  0xc000000
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI13_SHFT                       0x1a
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI12_BMSK                  0x3000000
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI12_SHFT                       0x18
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI11_BMSK                   0xc00000
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI11_SHFT                       0x16
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI10_BMSK                   0x300000
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI10_SHFT                       0x14
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI9_BMSK                     0xc0000
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI9_SHFT                        0x12
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI8_BMSK                     0x30000
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI8_SHFT                        0x10
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI7_BMSK                      0xc000
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI7_SHFT                         0xe
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI6_BMSK                      0x3000
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI6_SHFT                         0xc
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI5_BMSK                       0xc00
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI5_SHFT                         0xa
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI4_BMSK                       0x300
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI4_SHFT                         0x8
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI3_BMSK                        0xc0
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI3_SHFT                         0x6
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI2_BMSK                        0x30
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI2_SHFT                         0x4
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI1_BMSK                         0xc
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI1_SHFT                         0x2
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI0_BMSK                         0x3
#define HWIO_APCS_QGICDR_GICD_NSACR0_NS_ACCESS_SGI0_SHFT                         0x0

#define HWIO_APCS_QGICDR_GICD_NSACRn_ADDR(n)                              (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000e00 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_NSACRn_RMSK                                 0xffffffff
#define HWIO_APCS_QGICDR_GICD_NSACRn_MAXn                                         35
#define HWIO_APCS_QGICDR_GICD_NSACRn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_NSACRn_ADDR(n), HWIO_APCS_QGICDR_GICD_NSACRn_RMSK)
#define HWIO_APCS_QGICDR_GICD_NSACRn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_NSACRn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_NSACRn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_NSACRn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_NSACRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_NSACRn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_NSACRn_INI(n))
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT15_BMSK                 0xc0000000
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT15_SHFT                       0x1e
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT14_BMSK                 0x30000000
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT14_SHFT                       0x1c
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT13_BMSK                  0xc000000
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT13_SHFT                       0x1a
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT12_BMSK                  0x3000000
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT12_SHFT                       0x18
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT11_BMSK                   0xc00000
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT11_SHFT                       0x16
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT10_BMSK                   0x300000
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT10_SHFT                       0x14
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT9_BMSK                     0xc0000
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT9_SHFT                        0x12
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT8_BMSK                     0x30000
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT8_SHFT                        0x10
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT7_BMSK                      0xc000
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT7_SHFT                         0xe
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT6_BMSK                      0x3000
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT6_SHFT                         0xc
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT5_BMSK                       0xc00
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT5_SHFT                         0xa
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT4_BMSK                       0x300
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT4_SHFT                         0x8
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT3_BMSK                        0xc0
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT3_SHFT                         0x6
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT2_BMSK                        0x30
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT2_SHFT                         0x4
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT1_BMSK                         0xc
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT1_SHFT                         0x2
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT0_BMSK                         0x3
#define HWIO_APCS_QGICDR_GICD_NSACRn_NS_ACCESS_INT0_SHFT                         0x0

#define HWIO_APCS_QGICDR_GICD_SGIR_ADDR                                   (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000f00)
#define HWIO_APCS_QGICDR_GICD_SGIR_RMSK                                    0x3ff800f
#define HWIO_APCS_QGICDR_GICD_SGIR_OUT(v)      \
        out_dword(HWIO_APCS_QGICDR_GICD_SGIR_ADDR,v)
#define HWIO_APCS_QGICDR_GICD_SGIR_T_FILTER_BMSK                           0x3000000
#define HWIO_APCS_QGICDR_GICD_SGIR_T_FILTER_SHFT                                0x18
#define HWIO_APCS_QGICDR_GICD_SGIR_T_LIST_BMSK                              0xff0000
#define HWIO_APCS_QGICDR_GICD_SGIR_T_LIST_SHFT                                  0x10
#define HWIO_APCS_QGICDR_GICD_SGIR_NSATT_BMSK                                 0x8000
#define HWIO_APCS_QGICDR_GICD_SGIR_NSATT_SHFT                                    0xf
#define HWIO_APCS_QGICDR_GICD_SGIR_INT_ID_BMSK                                   0xf
#define HWIO_APCS_QGICDR_GICD_SGIR_INT_ID_SHFT                                   0x0

#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_ADDR(n)                          (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000f10 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_RMSK                              0xf0f0f0f
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_MAXn                                      3
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CPENDSGIRn_ADDR(n), HWIO_APCS_QGICDR_GICD_CPENDSGIRn_RMSK)
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CPENDSGIRn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_CPENDSGIRn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_CPENDSGIRn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_CPENDSGIRn_INI(n))
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_SGI3_BMSK                         0xf000000
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_SGI3_SHFT                              0x18
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_SGI2_BMSK                           0xf0000
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_SGI2_SHFT                              0x10
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_SGI1_BMSK                             0xf00
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_SGI1_SHFT                               0x8
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_SGI0_BMSK                               0xf
#define HWIO_APCS_QGICDR_GICD_CPENDSGIRn_SGI0_SHFT                               0x0

#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_ADDR(n)                          (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00000f20 + 0x4 * (n))
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_RMSK                              0xf0f0f0f
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_MAXn                                      3
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_SPENDSGIRn_ADDR(n), HWIO_APCS_QGICDR_GICD_SPENDSGIRn_RMSK)
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_SPENDSGIRn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_SPENDSGIRn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_SPENDSGIRn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_SPENDSGIRn_INI(n))
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_SGI3_BMSK                         0xf000000
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_SGI3_SHFT                              0x18
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_SGI2_BMSK                           0xf0000
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_SGI2_SHFT                              0x10
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_SGI1_BMSK                             0xf00
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_SGI1_SHFT                               0x8
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_SGI0_BMSK                               0xf
#define HWIO_APCS_QGICDR_GICD_SPENDSGIRn_SGI0_SHFT                               0x0

#define HWIO_APCS_QGICDR_GICD_IROUTERn_ADDR(n)                            (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x00006000 + 0x8 * (n))
#define HWIO_APCS_QGICDR_GICD_IROUTERn_RMSK                               0x80000101
#define HWIO_APCS_QGICDR_GICD_IROUTERn_MAXn                                      575
#define HWIO_APCS_QGICDR_GICD_IROUTERn_INI(n)        \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IROUTERn_ADDR(n), HWIO_APCS_QGICDR_GICD_IROUTERn_RMSK)
#define HWIO_APCS_QGICDR_GICD_IROUTERn_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_IROUTERn_ADDR(n), mask)
#define HWIO_APCS_QGICDR_GICD_IROUTERn_OUTI(n,val)    \
        out_dword(HWIO_APCS_QGICDR_GICD_IROUTERn_ADDR(n),val)
#define HWIO_APCS_QGICDR_GICD_IROUTERn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_QGICDR_GICD_IROUTERn_ADDR(n),mask,val,HWIO_APCS_QGICDR_GICD_IROUTERn_INI(n))
#define HWIO_APCS_QGICDR_GICD_IROUTERn_ROUTE_MODE_BMSK                    0x80000000
#define HWIO_APCS_QGICDR_GICD_IROUTERn_ROUTE_MODE_SHFT                          0x1f
#define HWIO_APCS_QGICDR_GICD_IROUTERn_AFFINITY_LVL_1_BMSK                     0x100
#define HWIO_APCS_QGICDR_GICD_IROUTERn_AFFINITY_LVL_1_SHFT                       0x8
#define HWIO_APCS_QGICDR_GICD_IROUTERn_AFFINITY_LVL_0_BMSK                       0x1
#define HWIO_APCS_QGICDR_GICD_IROUTERn_AFFINITY_LVL_0_SHFT                       0x0

#define HWIO_APCS_QGICDR_GICD_PIDR0_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000ffe0)
#define HWIO_APCS_QGICDR_GICD_PIDR0_RMSK                                        0xff
#define HWIO_APCS_QGICDR_GICD_PIDR0_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR0_ADDR, HWIO_APCS_QGICDR_GICD_PIDR0_RMSK)
#define HWIO_APCS_QGICDR_GICD_PIDR0_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR0_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_PIDR0_PART_NUM_BMSK                               0xff
#define HWIO_APCS_QGICDR_GICD_PIDR0_PART_NUM_SHFT                                0x0

#define HWIO_APCS_QGICDR_GICD_PIDR1_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000ffe4)
#define HWIO_APCS_QGICDR_GICD_PIDR1_RMSK                                        0xff
#define HWIO_APCS_QGICDR_GICD_PIDR1_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR1_ADDR, HWIO_APCS_QGICDR_GICD_PIDR1_RMSK)
#define HWIO_APCS_QGICDR_GICD_PIDR1_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR1_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_PIDR1_DESIGNER_BMSK                               0xf0
#define HWIO_APCS_QGICDR_GICD_PIDR1_DESIGNER_SHFT                                0x4
#define HWIO_APCS_QGICDR_GICD_PIDR1_PART_NUM_BMSK                                0xf
#define HWIO_APCS_QGICDR_GICD_PIDR1_PART_NUM_SHFT                                0x0

#define HWIO_APCS_QGICDR_GICD_PIDR2_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000ffe8)
#define HWIO_APCS_QGICDR_GICD_PIDR2_RMSK                                        0xff
#define HWIO_APCS_QGICDR_GICD_PIDR2_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR2_ADDR, HWIO_APCS_QGICDR_GICD_PIDR2_RMSK)
#define HWIO_APCS_QGICDR_GICD_PIDR2_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR2_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_PIDR2_ARCH_VERSION_BMSK                           0xf0
#define HWIO_APCS_QGICDR_GICD_PIDR2_ARCH_VERSION_SHFT                            0x4
#define HWIO_APCS_QGICDR_GICD_PIDR2_USES_JEP_CODE_BMSK                           0x8
#define HWIO_APCS_QGICDR_GICD_PIDR2_USES_JEP_CODE_SHFT                           0x3
#define HWIO_APCS_QGICDR_GICD_PIDR2_DESIGNER_BMSK                                0x7
#define HWIO_APCS_QGICDR_GICD_PIDR2_DESIGNER_SHFT                                0x0

#define HWIO_APCS_QGICDR_GICD_PIDR3_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000ffec)
#define HWIO_APCS_QGICDR_GICD_PIDR3_RMSK                                        0xf0
#define HWIO_APCS_QGICDR_GICD_PIDR3_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR3_ADDR, HWIO_APCS_QGICDR_GICD_PIDR3_RMSK)
#define HWIO_APCS_QGICDR_GICD_PIDR3_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR3_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_PIDR3_REVISION_BMSK                               0xf0
#define HWIO_APCS_QGICDR_GICD_PIDR3_REVISION_SHFT                                0x4

#define HWIO_APCS_QGICDR_GICD_PIDR4_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000ffd0)
#define HWIO_APCS_QGICDR_GICD_PIDR4_RMSK                                         0xf
#define HWIO_APCS_QGICDR_GICD_PIDR4_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR4_ADDR, HWIO_APCS_QGICDR_GICD_PIDR4_RMSK)
#define HWIO_APCS_QGICDR_GICD_PIDR4_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR4_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_PIDR4_DESIGNER_BMSK                                0xf
#define HWIO_APCS_QGICDR_GICD_PIDR4_DESIGNER_SHFT                                0x0

#define HWIO_APCS_QGICDR_GICD_PIDR5_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000ffd4)
#define HWIO_APCS_QGICDR_GICD_PIDR5_RMSK                                        0xff
#define HWIO_APCS_QGICDR_GICD_PIDR5_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR5_ADDR, HWIO_APCS_QGICDR_GICD_PIDR5_RMSK)
#define HWIO_APCS_QGICDR_GICD_PIDR5_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR5_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_PIDR5_RESRVD_BY_ARM_BMSK                          0xff
#define HWIO_APCS_QGICDR_GICD_PIDR5_RESRVD_BY_ARM_SHFT                           0x0

#define HWIO_APCS_QGICDR_GICD_PIDR6_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000ffd8)
#define HWIO_APCS_QGICDR_GICD_PIDR6_RMSK                                        0xff
#define HWIO_APCS_QGICDR_GICD_PIDR6_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR6_ADDR, HWIO_APCS_QGICDR_GICD_PIDR6_RMSK)
#define HWIO_APCS_QGICDR_GICD_PIDR6_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR6_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_PIDR6_RESRVD_BY_ARM_BMSK                          0xff
#define HWIO_APCS_QGICDR_GICD_PIDR6_RESRVD_BY_ARM_SHFT                           0x0

#define HWIO_APCS_QGICDR_GICD_PIDR7_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000ffdc)
#define HWIO_APCS_QGICDR_GICD_PIDR7_RMSK                                        0xff
#define HWIO_APCS_QGICDR_GICD_PIDR7_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR7_ADDR, HWIO_APCS_QGICDR_GICD_PIDR7_RMSK)
#define HWIO_APCS_QGICDR_GICD_PIDR7_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_PIDR7_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_PIDR7_RESRVD_BY_ARM_BMSK                          0xff
#define HWIO_APCS_QGICDR_GICD_PIDR7_RESRVD_BY_ARM_SHFT                           0x0

#define HWIO_APCS_QGICDR_GICD_CIDR0_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000fff0)
#define HWIO_APCS_QGICDR_GICD_CIDR0_RMSK                                        0xff
#define HWIO_APCS_QGICDR_GICD_CIDR0_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CIDR0_ADDR, HWIO_APCS_QGICDR_GICD_CIDR0_RMSK)
#define HWIO_APCS_QGICDR_GICD_CIDR0_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CIDR0_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_CIDR0_COMP_ID_0_BMSK                              0xff
#define HWIO_APCS_QGICDR_GICD_CIDR0_COMP_ID_0_SHFT                               0x0

#define HWIO_APCS_QGICDR_GICD_CIDR1_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000fff4)
#define HWIO_APCS_QGICDR_GICD_CIDR1_RMSK                                        0xff
#define HWIO_APCS_QGICDR_GICD_CIDR1_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CIDR1_ADDR, HWIO_APCS_QGICDR_GICD_CIDR1_RMSK)
#define HWIO_APCS_QGICDR_GICD_CIDR1_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CIDR1_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_CIDR1_COMP_ID_1_BMSK                              0xff
#define HWIO_APCS_QGICDR_GICD_CIDR1_COMP_ID_1_SHFT                               0x0

#define HWIO_APCS_QGICDR_GICD_CIDR2_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000fff8)
#define HWIO_APCS_QGICDR_GICD_CIDR2_RMSK                                        0xff
#define HWIO_APCS_QGICDR_GICD_CIDR2_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CIDR2_ADDR, HWIO_APCS_QGICDR_GICD_CIDR2_RMSK)
#define HWIO_APCS_QGICDR_GICD_CIDR2_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CIDR2_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_CIDR2_COMP_ID_2_BMSK                              0xff
#define HWIO_APCS_QGICDR_GICD_CIDR2_COMP_ID_2_SHFT                               0x0

#define HWIO_APCS_QGICDR_GICD_CIDR3_ADDR                                  (APCS_QGICDR_QGICDR_GICD_REG_BASE      + 0x0000fffc)
#define HWIO_APCS_QGICDR_GICD_CIDR3_RMSK                                        0xff
#define HWIO_APCS_QGICDR_GICD_CIDR3_IN          \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CIDR3_ADDR, HWIO_APCS_QGICDR_GICD_CIDR3_RMSK)
#define HWIO_APCS_QGICDR_GICD_CIDR3_INM(m)      \
        in_dword_masked(HWIO_APCS_QGICDR_GICD_CIDR3_ADDR, m)
#define HWIO_APCS_QGICDR_GICD_CIDR3_COMP_ID_3_BMSK                              0xff
#define HWIO_APCS_QGICDR_GICD_CIDR3_COMP_ID_3_SHFT                               0x0


#endif /* __APCS_QGIC_HWIO_H__ */

