l1_file_name = Z:\b\rpm_proc\tools\build\scons\sectools\resources\data_prov_assets\Encryption\Unified\default\l1_key.bin
l2_file_name = Z:\b\rpm_proc\tools\build\scons\sectools\resources\data_prov_assets\Encryption\Unified\default\l2_key.bin
l3_file_name = Z:\b\rpm_proc\tools\build\scons\sectools\resources\data_prov_assets\Encryption\Unified\default\l3_key.bin
Signing image: Z:\b\rpm_proc\build\ms\bin\AAAAANAAR\rpm.mbn
attestation_certificate_extensions = Z:\b\rpm_proc\tools\build\scons\sectools\resources\data_prov_assets\General_Assets\Signing\openssl\v3_attest.ext
ca_certificate_extensions = Z:\b\rpm_proc\tools\build\scons\sectools\resources\data_prov_assets\General_Assets\Signing\openssl\v3.ext
openssl_configfile = Z:\b\rpm_proc\tools\build\scons\sectools\resources\data_prov_assets\General_Assets\Signing\openssl\opensslroot.cfg
Generating new Root certificate and a random key
Generating new Attestation CA certificate and a random key
Generating new Attestation certificate and a random key

Attestation Certificate Properties:
| SW_ID                | 0x000000000000000A  |
| HW_ID                | 0x0000000000000000  |
| SOC_HW_VERSION       | None                |
| MASK_SOC_HW_VERSION  | None                |
| DEBUG                | 0x0000000000000002  |
| OEM_ID               | 0x0000              |
| SW_SIZE              | 168                 |
| MODEL_ID             | 0x0000              |
| SHA256               | True                |
| APP_ID               | None                |
| CRASH_DUMP           | None                |
| ROT_EN               | None                |
| Exponent             | 3                   |
| TCG_MIN              | None                |
| TCG_MAX              | None                |
| FID_MIN              | None                |
| FID_MAX              | None                |

Signed & Encrypted image is stored at Z:\b\rpm_proc\core\bsp\rpm\build\RPM_AAAAANAAR\sign_and_encrypt\default\rpm\rpm.mbn
Image Z:\b\rpm_proc\core\bsp\rpm\build\RPM_AAAAANAAR\sign_and_encrypt\default\rpm\rpm.mbn signature is valid
Image Z:\b\rpm_proc\core\bsp\rpm\build\RPM_AAAAANAAR\sign_and_encrypt\default\rpm\rpm.mbn is encrypted

Base Properties: 
| Integrity Check             | True  |
| Signed                      | True  |
| Encrypted                   | True  |
| Size of signature           | 256   |
| Size of one cert            | 2048  |
| Num of certs in cert chain  | 3     |
| Number of root certs        | 1     |
| Cert chain size             | 6144  |

ELF Properties: 
Elf Header: 
| Magic                      | ELF                           |
| Class                      | ELF32                          |
| Data                       | 2's complement, little endian  |
| Version                    | 1 (Current)                    |
| OS/ABI                     | No extensions or unspecified   |
| ABI Version                | 0                              |
| Type                       | EXEC (Executable file)         |
| Machine                    | Advanced RISC Machines ARM     |
| Version                    | 0x1                            |
| Entry address              | 0x00000009                     |
| Program headers offset     | 0x00000034                     |
| Section headers offset     | 0x00000000                     |
| Flags                      | 0x05000002                     |
| ELF header size            | 52                             |
| Program headers size       | 32                             |
| Number of program headers  | 2                              |
| Section headers size       | 40                             |
| Number of section headers  | 0                              |
| String table section index | 0                              |

Elf Program Headers: 
| S.No | Type | Offset | VirtAddr | PhysAddr | FileSize | MemSize |   Flags    | Align |
|------|------|--------|----------|----------|----------|---------|------------|-------|
|  1   | LOAD |0x03000 | 0x00000  | 0x200000 | 0x27e90  | 0x27e90 | 0x80000007 | 0x4   |
|  2   | LOAD |0x2ae90 | 0x90000  | 0x290000 | 0x0d17c  | 0x0f17c | 0x00000007 | 0x20  |

Hash Segment Properties: 
| Header Size  | 40B  |

Header: 
| cert_chain_ptr  | 0x002a01a8  |
| cert_chain_size | 0x00001800  |
| code_size       | 0x00000080  |
| flash_parti_ver | 0x00000003  |
| image_dest_ptr  | 0x002a0028  |
| image_id        | 0x00000017  |
| image_size      | 0x00001980  |
| image_src       | 0x00000000  |
| sig_ptr         | 0x002a00a8  |
| sig_size        | 0x00000100  |

SecElf Properties: 
| image_type         | 0     |
| testsig_serialnum  | None  |

