#line 1 "Z:\\b\\rpm_proc\\core\\bsp\\rpm\\src\\rpm.scl"











 

#line 20 "Z:\\b\\rpm_proc\\core\\bsp\\rpm\\src\\rpm.scl"











#line 39 "Z:\\b\\rpm_proc\\core\\bsp\\rpm\\src\\rpm.scl"



 


RPMSS_CODE 0x0 0x28000
{
  CODE_RAM 0x0 0x28000
  {
    startup.o(RPM_ENTRY, +FIRST)
    * (InRoot$$Sections)
    * (+RO-CODE)
    * (+RO-DATA)

    *(ddr_abort_funcs)

  }

  
  DDR_CODE_RAM +0x0 FIXED NOCOMPRESS (0x6400) 
  {



    *rpm_proc/core/boot/ddr*.lib (+RO)

  }

  DDR_CODE_RAM_END (ImageBase(DDR_CODE_RAM) + (0x6400)) EMPTY 0x0
  {
  }

  
  CODE_RAM_RECLAIM_POOL +0x0 FIXED NOCOMPRESS
  {
    *(pm_cram_reclaim_pool)
    *(clk_cram_reclaim_pool)
    *(icb_cram_reclaim_pool)
    *(qdss_cram_reclaim_pool)
    *(cpr_cram_reclaim_pool)
    *(rpmserver_cram_reclaim_pool)
    *(sleep_cram_reclaim_pool)
    *(mpm_cram_reclaim_pool)
    *(proxy_cram_reclaim_pool)
    *(glink_cram_reclaim_pool)
  }

  INITIAL_CODE_RAM_HEAP +0x0 EMPTY 0x0
  {
  }

  
  CODE_RAM_SAVE ((0x0 + 0x28000) - 0x200) FIXED 0x200
  {
    *(cram_save_pool)
  }

  CODE_RAM_SAVE_END ImageBase(CODE_RAM_SAVE)+0x200 EMPTY 0x0
  {
  }
}



RPMSS_DATA 0x90000 0x14000
{
  IMAGE_INFO_HEADER +0x0 EMPTY 0x0
  {
    
  }

  RPM_IMAGE_ID ImageBase(IMAGE_INFO_HEADER)+0x40 FIXED NOCOMPRESS ZEROPAD
  {
    oem_uuid.o (+RW)
    qc_version.o (+RW)
    oem_version.o (+RW)
  }

  RAIL_RESIDENCIES +0x0 ALIGN 4 EMPTY 0x0
  {
  }

  SLEEP_STATS ImageBase(RAIL_RESIDENCIES)+0x100 EMPTY 0x0
  {
  }

  CPR_STATS ImageBase(SLEEP_STATS)+0x60 EMPTY 0x0
  {
  }

  DATA_RAM ImageBase(CPR_STATS)+0x100 FIXED NOCOMPRESS ZEROPAD
  {
    * (+RW)
    * (+ZI)
  }


  8996_xml_DEVCONFIG_DATA +0x0 FIXED NOCOMPRESS ZEROPAD
  {
    8996_devcfg*.o (+RW,+ZI,+RO-DATA)
  }


#line 153 "Z:\\b\\rpm_proc\\core\\bsp\\rpm\\src\\rpm.scl"

  DAL_CONFIG_SECTIONS +0x0 FIXED NOCOMPRESS ZEROPAD
  {
    
    DALConfig*.o (+RW,+ZI,+RO-DATA)
  }

  STACK_OVERFLOW +0x0 ALIGN 32 EMPTY 0x0
  {
  }

  STACK ImageBase(STACK_OVERFLOW)+0x20 EMPTY 0x0
  {
  }

  
  DATA_RAM_RECLAIM_POOL ImageBase(STACK)+0xA00 FIXED NOCOMPRESS ZEROPAD
  {
    *(mpu_dram_reclaim_pool)
    *(pm_dram_reclaim_pool)
    *(railway_dram_reclaim_pool)
    *(swevent_dram_reclaim_pool)
    *(xpu_dram_reclaim_pool)
    *(mpm_dram_reclaim_pool)
    *(npa_dram_reclaim_pool)
    *(cpr_dram_reclaim_pool)
  }

  DATA_RAM_HEAP +0x0 ALIGN 4 EMPTY 0x0
  {
  }

  RPM_LOG (((0x90000 + 0x14000) - 0x2000) - 0x2400) EMPTY 0x0
  {
  }

  
  DDR_STRUCT_DATA  ((0x90000 + 0x14000) - 0x2000)  EMPTY  0x2000
  {
    
  }
}

