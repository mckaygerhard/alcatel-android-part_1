/*! \file pm_boost_byp.c
*
*  \brief Implementation file for BOOST BYPASS public APIs.
*  \n
*  &copy; Copyright 2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/drivers/boost_byp/src/pm_boost_byp.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/09/14   kt      Created
===========================================================================*/
/*===========================================================================

                     INCLUDE FILES

===========================================================================*/
#include "pm_boost_byp.h"
#include "pm_boost_byp_driver.h"

/*===========================================================================

                     API IMPLEMENTATION

===========================================================================*/

pm_err_flag_type pm_boost_byp_sw_mode(uint8 pmic_chip, uint8 peripheral_index, pm_boost_byp_mode_type mode)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_boost_byp_data_type *boost_byp_ptr = pm_boost_byp_get_data(pmic_chip);
    uint8 periph_type = 0;

    if (boost_byp_ptr == NULL)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if(peripheral_index >= boost_byp_ptr->pm_pwr_data.num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (mode >= PM_BBYP_MODE_INVALID)
    {
        err_flag = PM_ERR_FLAG__MACRO_NOT_RECOGNIZED;
    }
    else
    {
        pm_register_address_type          reg = 0;
        pm_register_mask_type             mask = 0;
        pm_register_data_type             data = 0;
        pm_pwr_data_type                  *pwr_data = &(boost_byp_ptr->pm_pwr_data);

        periph_type = pwr_data->pwr_specific_info[peripheral_index].periph_type;

        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->MODE_CTL);

        switch(mode)
        {
        case PM_BBYP_MODE_FORCE_BYPASS: // Force bypass mode
          {
              mask = 0x80;
              data = 0x00;
          }
          break;
        case PM_BBYP_MODE_AUTO_BOOST: // Auto boost mode
          {
              mask = 0x80;
              data = 0x80;
          }
          break;
        default:
          err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
          break;
        }

        if (PM_ERR_FLAG__SUCCESS == err_flag)
        {
            err_flag = pm_comm_write_byte_mask(boost_byp_ptr->comm_ptr->slave_id, reg, mask, data, 0);
        }
    }

    if (PM_ERR_FLAG__SUCCESS == err_flag)
    {
        PM_LOG_MSG_DEBUG(PMIC_DRV_PWR_MODE, pmic_chip, periph_type, peripheral_index, mode);
    }
    else
    {
        PM_LOG_MSG_ERROR(PMIC_DRV_PWR_MODE_ERROR, pmic_chip, periph_type, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_boost_byp_pin_ctrl_volt_level(uint8 pmic_chip, uint8 peripheral_index, pm_volt_level_type volt_level)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_boost_byp_data_type *boost_byp_ptr = pm_boost_byp_get_data(pmic_chip);
    uint8 periph_type = 0;

    if (boost_byp_ptr == NULL)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if(peripheral_index >= boost_byp_ptr->pm_pwr_data.num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else
    {
        pm_register_address_type          reg = 0;
        pm_register_data_type             vset = 0;
        uint32                            vmin = 0;  // microvolt
        uint32                            vStep = 0; // microvolt
        uint32                            range = 0; // 0 to 4
        pm_pwr_data_type                  *pwr_data = &(boost_byp_ptr->pm_pwr_data);

        periph_type = pwr_data->pwr_specific_info[peripheral_index].periph_type;

        range = pwr_data->pwr_specific_info[peripheral_index].pwr_range;

        if ((volt_level <= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMax) && 
            (volt_level >= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin) )
        {
            vmin = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin;
            vStep = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].VStep;
        }
       
        if (vStep > 0)
        {
            // Calculate VSET
            vset = (pm_register_data_type)((volt_level - vmin)/vStep);

            // Set VSET
            reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->OUTPUT_VOLTAGE_PIN);

            err_flag = pm_comm_write_byte(boost_byp_ptr->comm_ptr->slave_id, reg, vset, 0);  // 0:7
        }
        else
        {
            err_flag = PM_ERR_FLAG__VLEVEL_OUT_OF_RANGE;
        }
    }

    if (PM_ERR_FLAG__SUCCESS == err_flag)
    {
        PM_LOG_MSG_DEBUG(PMIC_DRV_PWR_PIN_VOLT_LVL, pmic_chip, periph_type, peripheral_index, volt_level);
    }
    else
    {
        PM_LOG_MSG_ERROR(PMIC_DRV_PWR_PIN_VOLT_LVL_ERROR, pmic_chip, periph_type, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_boost_byp_pin_ctrl_volt_level_status(uint8 pmic_chip, uint8 peripheral_index, pm_volt_level_type *volt_level)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_boost_byp_data_type *boost_byp_ptr = pm_boost_byp_get_data(pmic_chip);
    uint8 periph_type = 0;

    if (boost_byp_ptr == NULL)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if(peripheral_index >= boost_byp_ptr->pm_pwr_data.num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (volt_level == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        pm_register_address_type          reg = 0;
        pm_register_data_type             vset = 0;
        uint32                            vmin = 0;  // microvolt
        uint32                            vStep = 0; // microvolt
        uint32                            range = 0; // 0
        pm_pwr_data_type                  *pwr_data = &(boost_byp_ptr->pm_pwr_data);

        periph_type = pwr_data->pwr_specific_info[peripheral_index].periph_type;

        range = pwr_data->pwr_specific_info[peripheral_index].pwr_range;

        // Get VSET
        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->OUTPUT_VOLTAGE_PIN);

        err_flag = pm_comm_read_byte(boost_byp_ptr->comm_ptr->slave_id, reg, &vset, 0);

        if (err_flag == PM_ERR_FLAG__SUCCESS)
        {
            // Calculate pin output voltage level
            vmin = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin; // microvolt
            vStep = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].VStep; //microvolt

            *volt_level = vmin + vset * vStep;
        }
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_LOG_MSG_ERROR(PMIC_DRV_PWR_PIN_VOLT_LVL_ERROR, pmic_chip, periph_type, peripheral_index, err_flag);
    }

    return err_flag;
}
