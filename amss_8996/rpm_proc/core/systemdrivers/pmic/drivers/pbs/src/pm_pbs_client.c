/*! \file pm_pbs_client.c
*  \n
*  \brief This file contains the implementation of the public APIs for PBS Client module.
*  \n
*  \details Implementation file for PBS Client module. Each of the APIs checks for
*           access and then if necessary directs the call to Driver implementation.
*  \n  
*  &copy; Copyright 2015 QUALCOMM Technologies Incorporated, All Rights Reserved
*/
/*===========================================================================
EDIT HISTORY FOR MODULE

This document is created by a code generator, therefore this section will
not contain comments describing changes made to the module.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/drivers/pbs/src/pm_pbs_client.c#1 $ 

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/16/13   kt      Created.
===========================================================================*/
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_pbs_client_driver.h"

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/

pm_err_flag_type pm_pbs_client_sw_trigger(uint8 pmic_chip, uint8 pbs_client_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type trig_ctl = 0;
    pm_register_data_type trig_data = 0x1;
    pm_pbs_client_data_type *pbs_client_ptr = pm_pbs_client_get_data(pmic_chip);

    if (pbs_client_ptr == NULL) 
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if (pbs_client_index >= pbs_client_ptr->num_of_peripherals)
    {   
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED ;
    }
    else
    {
        trig_ctl = pbs_client_ptr->pbs_client_reg_table->base_address + 
                   (pbs_client_index*pbs_client_ptr->pbs_client_reg_table->peripheral_offset) + 
                   pbs_client_ptr->pbs_client_reg_table->trig_ctl;

        err_flag = pm_comm_write_byte(pbs_client_ptr->comm_ptr->slave_id, trig_ctl, trig_data, 0);
    }

    return err_flag;
}
