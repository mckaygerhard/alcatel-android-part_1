/*! \file pm_fg_adc_usr.c
*  \n
*  \brief Implementation file for PMIC Fuel Gauge ADC User module.
*  \n
*  &copy; Copyright 2014-2015 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/drivers/fg/src/pm_fg_adc_usr.c#1 $

when          who     what, where, why
--------      ---     ---------------------------------------------------------- 
11/10/14      sm      Added APIs for ibat and vbat calibration
                      Removed LSB correction from get_ibat and get_vbat APIs
09/24/14      sm      Corrections in APIs to get VBAT and IBAT
08/20/14      al      Updating comm lib 
08/29/14      al      KW fixes
08/27/14      va      Driver cleanup update
08/13/14      sm      Corrected the calculations in APIs to get vbat and ibat 
06/25/14      va      Driver update, added protected call
05/12/14      va      Initial Release 
===========================================================================*/

#include "pm_fg_adc_usr.h"
#include "pm_fg_driver.h"

/*===========================================================================
                        TYPE DEFINITIONS 
===========================================================================*/
#define RAW_VBAT_LSB          39
#define GAIN_LSB_DENOM       400  // Gain LSB is 0.32/128 = 1/400

/*===========================================================================
                         FUNCTION DEFINITIONS
===========================================================================*/
/**
* @brief This function returns status RDY bit after battery parameter update request is serviced *
* @details
*  After the first readings from ADC are obtained, this bit is set to 1; At reset and shutdown, this bit gets automatically cleared 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in]enable  enable/disable BCL monitoring
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_adc_usr_get_bcl_values(uint32 pmic_device, boolean *enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_adc_usr_bcl_values = 0x00;
  pm_register_data_type data = 0x00;
  uint8 mask = 0x80;

  pm_fg_data_type* fg_adc_usr_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_adc_usr_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == enable)
  {
    return PM_ERR_FLAG__INVALID_POINTER;
  }
  else
  {
    fg_adc_usr_bcl_values = fg_adc_usr_ptr->fg_register->adc_usr_register->base_address + fg_adc_usr_ptr->fg_register->adc_usr_register->fg_adc_usr_bcl_values;
    err_flag = pm_comm_read_byte_mask(fg_adc_usr_ptr->comm_ptr->slave_id, fg_adc_usr_bcl_values, mask, &data, 0);
    
    if (data == mask)
    {
      *enable = TRUE;
    }
    else
    {
      *enable = FALSE;
    }
  }

  return err_flag;

}


/**
* @brief This function returns battery ADC Voltage *
* @details
*  8 bit signed partial ADC value, MSB = 0 is positive voltage (positive number), only positive voltages are captured, 1 LSB = 39 mV 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out]vbat_adc  Battery Voltage
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_adc_usr_get_vbat(uint32 pmic_device, uint32 *vbat_adc)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_adc_usr_vbat = 0x00;
  pm_register_address_type  fg_adc_usr_vbat_cp = 0x00;
  pm_register_data_type data = 0x00;
  pm_register_data_type data1 = 0x00;

  int8  temp_data = 0;
  int32  temp_data1 = 0;

  //Compare data thrice in copy register and original register to make sure we have right data
  uint32  total_count = 3;
  uint32  count = 0;

  pm_fg_data_type* fg_adc_usr_ptr = pm_fg_get_data(pmic_device);

  *vbat_adc = 0;

  if (NULL == fg_adc_usr_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    fg_adc_usr_vbat = fg_adc_usr_ptr->fg_register->adc_usr_register->base_address + fg_adc_usr_ptr->fg_register->adc_usr_register->fg_adc_usr_vbat;
    fg_adc_usr_vbat_cp = fg_adc_usr_ptr->fg_register->adc_usr_register->base_address + fg_adc_usr_ptr->fg_register->adc_usr_register->fg_adc_usr_vbat_cp;

    do {
      err_flag = pm_comm_read_byte(fg_adc_usr_ptr->comm_ptr->slave_id, fg_adc_usr_vbat, &data1, 0);
      err_flag = pm_comm_read_byte(fg_adc_usr_ptr->comm_ptr->slave_id, fg_adc_usr_vbat_cp, &data, 0);

      if(data == data1)
      {
        break;
      }

      if((total_count - 1) == count)
      {
        data = 0;
        err_flag = PM_ERR_FLAG__INVALID;
      }

      count++;

    }while ( count < total_count);

    //Convert uint8 to int8
    temp_data = data;

    //Convert int8 to int32
    temp_data1  = temp_data;

    *vbat_adc = temp_data1 * RAW_VBAT_LSB;
  }

  return err_flag;
}


/**
* @brief This function returns votlage gain correction value for battery voltage *
* @details
*  This function returns gain correction value for battery 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in] gainCorrection  for battery volatge
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_adc_usr_get_bcl_v_gain_batt(uint32 pmic_device, int32 *v_gain_correction)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_adc_usr_v_gain_batt = 0x00;
  pm_register_data_type data = 0x00;

  int8   temp_data = 0;
  int32  temp_data1 = 0;

  pm_fg_data_type* fg_adc_usr_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_adc_usr_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    fg_adc_usr_v_gain_batt = fg_adc_usr_ptr->fg_register->adc_usr_register->base_address + fg_adc_usr_ptr->fg_register->adc_usr_register->fg_adc_usr_bcl_v_gain_batt;
    err_flag = pm_comm_read_byte(fg_adc_usr_ptr->comm_ptr->slave_id, fg_adc_usr_v_gain_batt, &data, 0);
    
    //Convert uint8 to int8
    temp_data = data;

    //Convert int8 to int32
    temp_data1 = temp_data;

    *v_gain_correction = temp_data1;
  }

  return err_flag;

}

pm_err_flag_type pm_fg_adc_usr_get_calibrated_vbat(uint32 pmic_device, uint32 *calibrated_vbat)
{
  pm_err_flag_type   errFlag = PM_ERR_FLAG__SUCCESS;

  uint32 raw_vbat = 0;
  static int32 gain = 0;
  
  errFlag = pm_fg_adc_usr_get_vbat(pmic_device, &raw_vbat);
  
  if(!gain)
  {
    //Gain is constant w.r.t to every part, so we should read it only once.
    errFlag |= pm_fg_adc_usr_get_bcl_v_gain_batt( pmic_device,  &gain );
  }

  /* Applying gain calibration to the raw value*/
  // Twos_complement(VBAT_registerval) *39 * (1+ Twos_Complement(V_GAIN_registerval) * (.32/128))
  *calibrated_vbat = (uint32)(((raw_vbat * (GAIN_LSB_DENOM + gain)))/GAIN_LSB_DENOM);

  return errFlag;
}

