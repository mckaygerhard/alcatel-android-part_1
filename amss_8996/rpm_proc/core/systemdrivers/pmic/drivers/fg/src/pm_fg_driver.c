/*! \file pm_fg_driver.c
*  \n
*  \brief This file contains FG peripheral driver initialization during which the driver
*         driver data is stored.
*  \n
*  \n &copy; Copyright 2014-2015 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/drivers/fg/src/pm_fg_driver.c#1 $

when        who     what, where, why
--------    ---     ---------------------------------------------------------------- 
09/22/14    aab     Porting FG driver to SBL        
08/20/14    al      Updating comm lib 
08/29/14    al      KW fixes
06/09/14    al      Arch update 
04/01/14    va      New file
================================================================================== */

#include "pm_fg_driver.h"
#include "pm_target_information.h"
#include "device_info.h"
#include "CoreVerify.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the FG driver data */
static pm_fg_data_type *pm_fg_data_arr[PM_MAX_NUM_PMICS];

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void pm_fg_driver_init(pm_comm_info_type *comm_ptr, peripheral_info_type *peripheral_info, uint8 pmic_index)
{
    pm_fg_data_type *fg_ptr = NULL;

    fg_ptr = pm_fg_data_arr[pmic_index];

    if (NULL == fg_ptr)
    {
        pm_malloc( sizeof(pm_fg_data_type), (void**)&fg_ptr);
                                                    
        /* Assign Comm ptr */
        fg_ptr->comm_ptr = comm_ptr;

        /* fg Register Info - Obtaining Data through dal config */
        fg_ptr->fg_register = (fg_register_ds*)pm_target_information_get_common_info(PM_PROP_FG_REG);
        CORE_VERIFY_PTR(fg_ptr->fg_register);

        fg_ptr->num_of_peripherals = pm_target_information_get_periph_count_info(PM_PROP_FG_NUM, pmic_index);
        CORE_VERIFY(fg_ptr->num_of_peripherals != 0);

        pm_fg_data_arr[pmic_index] = fg_ptr;
    }
}


pm_fg_data_type* pm_fg_get_data(uint8 pmic_index)
{
  if(pmic_index <PM_MAX_NUM_PMICS) 
  {
      return pm_fg_data_arr[pmic_index];
  }

  return NULL;
}

uint8 pm_fg_get_num_peripherals(uint8 pmic_index)
{
  if((pmic_index < PM_MAX_NUM_PMICS) && (pm_fg_data_arr[pmic_index] !=NULL))
  {
      return pm_fg_data_arr[pmic_index]->num_of_peripherals;
  }

  return 0;
}

