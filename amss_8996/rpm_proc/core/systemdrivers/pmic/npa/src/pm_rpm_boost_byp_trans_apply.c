/*! \file pm_rpm_boost_byp_trans_apply.c
 *  \n
 *  \brief This file contains implementation for PMIC Boost Bypass Translation
 *         and Apply (Aggregate) functions.
 *  \n  
 *  \n &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/npa/src/pm_rpm_boost_byp_trans_apply.c#7 $

when        who     what, where, why
--------    ---     ----------------------------------------------------------
11/22/13    kt      Created.
========================================================================== */
/*===========================================================================

                        INCLUDE FILES

===========================================================================*/
#include "npa.h"
#include "rpmserver.h"
#include "pm_npa.h"

#include "pm_target_information.h"
#include "pm_rpm_boost_byp_trans_apply.h"
#include "CoreVerify.h"
#include "pm_comm.h"
#include "device_info.h"

static pm_npa_boost_byp_data_type *pm_rpm_boost_byp_req_data[PM_MAX_NUM_PMICS];

// internal function prototypes
void pm_rpm_boost_byp_translation(rpm_translation_info *info);
void pm_rpm_boost_byp_apply(rpm_application_info *info);
int pm_rpm_boost_byp_aggregate(rpm_application_info *info);
void pm_rpm_boost_byp_execute_driver(rpm_application_info *info, pm_npa_boost_byp_int_rep* previous_aggregation);
pm_npa_boost_byp_data_type* pm_rpm_boost_byp_get_resource_data(uint8 pmic_index);
void pm_rpm_boost_byp_dependency_execute(rpm_application_info *info, pm_npa_boost_byp_int_rep* shadow_data);

void pm_rpm_boost_byp_translation(rpm_translation_info *info)
{
    unsigned type, length, *value;
    pm_npa_boost_byp_int_rep *cache;

    cache = (pm_npa_boost_byp_int_rep *)(info->dest_buffer);
    
    while(kvp_get(info->new_kvps, &type, &length, (const char **)&value))
    {
        // Need size to match.
        if(sizeof(npa_resource_state) != length)
            continue;

        switch(type)
        {
        case PM_NPA_KEY_SOFTWARE_ENABLE:
            {
                cache->sw_en = *value;
                break;
            }
        case PM_NPA_KEY_BYPASS_ALLOWED_KEY:
            {
                cache->byp_allowed = *value;
                break;
            }
        case PM_NPA_KEY_BOOST_BYP_MODE:
            {
                cache->mode = *value;
                break;
            }
        case PM_NPA_KEY_MICRO_VOLT:
            {
                cache->uvol = *value;
                break;
            }
        case PM_NPA_KEY_PIN_CTRL_MICRO_VOLT:
            {
                cache->pin_uvol = *value;
                break;
            }
        default:
            {
                //should never go here
            }
        }
    }
}

void pm_rpm_boost_byp_apply(rpm_application_info *info)
{
    int change_detected = 0;
    pm_npa_boost_byp_int_rep     previous_aggregation = *((pm_npa_boost_byp_int_rep*)(info->current_aggregation));

    // Aggregate the results, but don't call driver
    change_detected = pm_rpm_boost_byp_aggregate(info);

    if(change_detected != 0 )
    {
        pm_rpm_boost_byp_dependency_execute(info, &previous_aggregation);
    }
}

void pm_rpm_boost_byp_dependency_execute(rpm_application_info *info, pm_npa_boost_byp_int_rep* previous_aggregation)
{
    // Execute the driver calls once all dependencies have been executed    
    pm_rpm_boost_byp_execute_driver(info, previous_aggregation);
}

int pm_rpm_boost_byp_aggregate(rpm_application_info *info)
{
    int change_detected = 0;
    pm_npa_boost_byp_data_type *boost_byp_data = (pm_npa_boost_byp_data_type*)info->cb_data;
    unsigned clientType = 0;
    pm_npa_boost_byp_int_rep *clientRequest = NULL;
    pm_npa_ldo_int_rep* ldoClientRequest = NULL;
    void *state = NULL;
    unsigned clientCount = 0;
    pm_volt_level_type ceiling_voltage = 0; //voltage rounded to next high
    uint32             dummy_vset = 0;

    unsigned numberOfClients = rpm_get_num_clients(info->resource_handle);

    // For each client aggregate the correct values.
    pm_npa_boost_byp_int_rep aggregatedIntRep = {0};
    uint8 pmic_index = GET_PMIC_INDEX_BOOST_BYP(boost_byp_data->resourceType);
    CORE_VERIFY(pmic_index < PM_MAX_NUM_PMICS);

    /* if access not allowed, just return */
    if (boost_byp_data->railInfo[pmic_index][info->id -1].AccessAllowed == 0)
    {
        return change_detected;
    }
    
    for(clientCount = 0; clientCount < numberOfClients; clientCount++)
    {
        rpm_get_current_request(info->resource_handle, clientCount, &clientType, (const void **)&state);        

         // EE Client and Internal BOOST BYPASS clients
        if ((clientType == 0) || (clientType == PM_RPM_NPA_CLIENT_BOOST_BYP_REQ))
        {
            if (boost_byp_data->railInfo[pmic_index][info->id -1].AccessAllowed)
            {
                if (info->client == clientCount)
                {       
                    clientRequest = (pm_npa_boost_byp_int_rep*)info->new_state;
                }
                else
                {
                    clientRequest = (pm_npa_boost_byp_int_rep*)state;
                }
                
                if (clientRequest != NULL)
                {
                    // Aggregate Software Enable
                    aggregatedIntRep.sw_en = MAX(aggregatedIntRep.sw_en, clientRequest->sw_en);

                    // Aggregate Bypass Allowed
                    aggregatedIntRep.byp_allowed = MAX(aggregatedIntRep.byp_allowed, clientRequest->byp_allowed);
                  
                    // Aggregate Software mode
                    aggregatedIntRep.mode = MAX(aggregatedIntRep.mode, clientRequest->mode);

                    // Aggregate Maximum Output Voltage
                    aggregatedIntRep.uvol = MAX(aggregatedIntRep.uvol, clientRequest->uvol);

                    // Aggregate Maximum Pin Output Voltage
                    aggregatedIntRep.pin_uvol = MAX(aggregatedIntRep.pin_uvol, clientRequest->pin_uvol);
                  
                }
            }
        }
        else if(clientType == PM_RPM_NPA_INT_CLIENT_LDO_DEPENDENT)
        {
            if(info->client == clientCount)
            {
                ldoClientRequest = (pm_npa_ldo_int_rep*)info->new_state;
            }
            else
            {            
                ldoClientRequest = (pm_npa_ldo_int_rep*)state;
            }

            // A valid internal LDO client should have resource_id greater than zero
            if(ldoClientRequest->resource_id > 0) 
            {
                // Aggregate Software Enable - Child needs power
                aggregatedIntRep.sw_en = MAX(aggregatedIntRep.sw_en, ldoClientRequest->sw_en);

                // Keep the Boost bypass Always enabled if the child LDO is in pin control
                if(ldoClientRequest->pc_en == 1)
                {
                    aggregatedIntRep.sw_en = 1;
                }

                // Ignoring voltage requests if ldo is sw disabled
                if(ldoClientRequest->sw_en > 0)
                {
                    // Aggregate Maximum Output Voltage
                    aggregatedIntRep.uvol = MAX(aggregatedIntRep.uvol, ldoClientRequest->input_uvol);
                }
            } //resource_id>0
        }
    }

    // input checking
    if (boost_byp_data->railInfo[pmic_index][info->id -1].AlwaysOn)
    {
        aggregatedIntRep.sw_en = 1;
    }

    if(aggregatedIntRep.mode < boost_byp_data->railInfo[pmic_index][info->id - 1].MinPwrMode)
    {
        aggregatedIntRep.mode = boost_byp_data->railInfo[pmic_index][info->id - 1].MinPwrMode;
    }

    if((aggregatedIntRep.uvol < boost_byp_data->railInfo[pmic_index][info->id - 1].MinVoltage*1000) && (aggregatedIntRep.uvol > 0))
    {
        aggregatedIntRep.uvol = boost_byp_data->railInfo[pmic_index][info->id - 1].MinVoltage*1000;
    }
    else if(aggregatedIntRep.uvol > boost_byp_data->railInfo[pmic_index][info->id - 1].MaxVoltage*1000)
    {
        aggregatedIntRep.uvol = boost_byp_data->railInfo[pmic_index][info->id - 1].MaxVoltage*1000;
    }

    //check if rounding off vset is needed
    pm_pwr_volt_calculate_vset_celing_uv(&(boost_byp_data->boostBypDriverData->pm_pwr_data), boost_byp_data->boostBypDriverData->comm_ptr, 
                            info->id - 1, (pm_volt_level_type)aggregatedIntRep.uvol, &dummy_vset, &ceiling_voltage);
    
    if(ceiling_voltage != 0)
    {
        aggregatedIntRep.uvol =  ceiling_voltage; 
    }
    ceiling_voltage = 0; 
    // Make sure pin output voltage is always >= output voltage
    if(aggregatedIntRep.uvol > aggregatedIntRep.pin_uvol)
    {
        aggregatedIntRep.pin_uvol = aggregatedIntRep.uvol;
    }

    if((aggregatedIntRep.pin_uvol < boost_byp_data->railInfo[pmic_index][info->id - 1].MinPinVoltage*1000) && (aggregatedIntRep.pin_uvol > 0))
    {
        aggregatedIntRep.pin_uvol = boost_byp_data->railInfo[pmic_index][info->id - 1].MinPinVoltage*1000;
    }
    else if(aggregatedIntRep.pin_uvol > boost_byp_data->railInfo[pmic_index][info->id - 1].MaxPinVoltage*1000)
    {
        aggregatedIntRep.pin_uvol = boost_byp_data->railInfo[pmic_index][info->id - 1].MaxPinVoltage*1000;
    }
    
    //check if rounding off vset is needed
    pm_pwr_volt_calculate_vset_celing_uv(&(boost_byp_data->boostBypDriverData->pm_pwr_data), boost_byp_data->boostBypDriverData->comm_ptr, 
                            info->id - 1, (pm_volt_level_type)aggregatedIntRep.pin_uvol, &dummy_vset, &ceiling_voltage);
    
    if(ceiling_voltage != 0)
    {
        aggregatedIntRep.pin_uvol =  ceiling_voltage; 
    }
    
    change_detected = pm_rpm_int_copy(&aggregatedIntRep, (pm_npa_boost_byp_int_rep*)info->current_aggregation, sizeof(pm_npa_boost_byp_int_rep));

    return change_detected;
}

void pm_rpm_boost_byp_execute_driver(rpm_application_info *info, pm_npa_boost_byp_int_rep* previous_aggregation)
{
    pm_npa_boost_byp_int_rep *boost_byp_int_rep =(pm_npa_boost_byp_int_rep *)info->current_aggregation;
    pm_npa_boost_byp_data_type *boost_byp_data = (pm_npa_boost_byp_data_type *)info->cb_data;
    pm_boost_byp_data_type* boost_byp_ptr = NULL;
    uint8  internal_resource_index = (uint8)(info->id - 1);
    uint8 pmic_index = GET_PMIC_INDEX_BOOST_BYP(boost_byp_data->resourceType);
    pm_volt_level_type curr_volt_level = 0;

    /* Get the boost bypass driver data from the call back data */
    boost_byp_ptr = boost_byp_data->boostBypDriverData;

    if(boost_byp_ptr != NULL)
    {
        /* Set Power Mode */
        if(boost_byp_int_rep->mode != previous_aggregation->mode) 
        {
            pm_boost_byp_sw_mode(pmic_index, internal_resource_index, (pm_boost_byp_mode_type)boost_byp_int_rep->mode);
        }

        /* Set the Output Voltage */
        if( (boost_byp_int_rep->uvol != previous_aggregation->uvol) && (boost_byp_int_rep->uvol != 0) )
        {
            /* CR761922: Added SW based voltage stepping or Boost-bypass to avoid HW damage. When changing
            voltage of the boost bypass, SW programs the voltage higher one LSB (50mV) at a time and the
            sw overhead delay of ~5us between each voltage step program due to SPMI transaction is sufficient. */
    
            /* Get the current Boost bypass voltage */
            pm_pwr_volt_level_status_alg(&(boost_byp_ptr->pm_pwr_data), boost_byp_ptr->comm_ptr, internal_resource_index, &curr_volt_level);
    
            if((boost_byp_int_rep->uvol > curr_volt_level) && (curr_volt_level != 0))
            {
                curr_volt_level = curr_volt_level + PM_BBYP_VOLT_STEP_SIZE_UV;
    
                while(boost_byp_int_rep->uvol > curr_volt_level)
               {
                   pm_pwr_volt_level_alg(&(boost_byp_ptr->pm_pwr_data), boost_byp_ptr->comm_ptr, internal_resource_index, curr_volt_level);
    
                   curr_volt_level = curr_volt_level + PM_BBYP_VOLT_STEP_SIZE_UV;
               }
        }
            pm_pwr_volt_level_alg(&(boost_byp_ptr->pm_pwr_data), boost_byp_ptr->comm_ptr, internal_resource_index,
                           (pm_volt_level_type)boost_byp_int_rep->uvol);
		    
        }

        /* Set the Pin Output Voltage */
        if( (boost_byp_int_rep->pin_uvol != previous_aggregation->pin_uvol) && (boost_byp_int_rep->pin_uvol != 0) )
        {
            pm_boost_byp_pin_ctrl_volt_level(pmic_index, internal_resource_index, (pm_volt_level_type)boost_byp_int_rep->pin_uvol);
        }

        /* Set the enable state */
        if(boost_byp_int_rep->sw_en != previous_aggregation->sw_en)
        {
            pm_pwr_sw_en_pin_ctl_alg(&(boost_byp_ptr->pm_pwr_data), boost_byp_ptr->comm_ptr, internal_resource_index,
                                            (pm_on_off_type)boost_byp_int_rep->sw_en,(uint8)PM_OFF);
        }
    }
}

__attribute__((section("pm_dram_reclaim_pool")))
void pm_rpm_boost_byp_register_resources(rpm_resource_type resourceType, uint32 num_npa_resources, uint8 pmic_index)
{
    pm_npa_boost_byp_data_type *boost_byp_data = NULL;
    uint32 boost_byp_index = 0;
    pm_boost_byp_data_type *boost_byp_ptr = NULL;
    uint8 range = 0; 

    
    if(num_npa_resources > 0 )
    {
        //return if there was no driver initialized for this pmic 
        boost_byp_ptr = pm_boost_byp_get_data(pmic_index);
        if(boost_byp_ptr == NULL)
        	   return; 
        
        boost_byp_data = pm_rpm_boost_byp_get_resource_data(pmic_index);
        if(boost_byp_data == NULL)
        {
            pm_malloc( sizeof(pm_npa_boost_byp_data_type), (void**)&boost_byp_data);

            boost_byp_data->boostBypDriverData = boost_byp_ptr;
            boost_byp_data->resourceType = resourceType;
            boost_byp_data->depInfo = (pm_pwr_resource_dependency_info**)pm_target_information_get_specific_info(PM_PROP_BOOST_BYP_DEP);
            CORE_VERIFY_PTR(boost_byp_data->depInfo);
            boost_byp_data->railInfo = (pm_rpm_boost_byp_rail_info_type**)pm_target_information_get_specific_info(PM_PROP_BOOST_BYP_RAIL);
            CORE_VERIFY_PTR(boost_byp_data->railInfo);

            //verify if the range can meet the Boost bypass min, max voltage limits
            for(boost_byp_index = 0; boost_byp_index < num_npa_resources ; boost_byp_index++)
            { 
                  //skip over invalid peripherals
                 if(boost_byp_ptr->pm_pwr_data.pwr_specific_info[boost_byp_index].periph_type == 0)
                 {
                     continue;
                 }
                 range = boost_byp_ptr->pm_pwr_data.pwr_specific_info[boost_byp_index].pwr_range;

                 /* Check if Min/Max voltage is within the range */
                 CORE_VERIFY(((boost_byp_data->railInfo[pmic_index][boost_byp_index].MinVoltage)*1000 >= boost_byp_ptr->pm_pwr_data.pwr_specific_info[boost_byp_index].pwr_vset[range].RangeMin ) && 
                  ((boost_byp_data->railInfo[pmic_index][boost_byp_index].MaxVoltage)*1000 <= boost_byp_ptr->pm_pwr_data.pwr_specific_info[boost_byp_index].pwr_vset[range].RangeMax));

                 /* Check if Min/Max Pin voltage is within the range */
                 CORE_VERIFY(((boost_byp_data->railInfo[pmic_index][boost_byp_index].MinPinVoltage)*1000 >= boost_byp_ptr->pm_pwr_data.pwr_specific_info[boost_byp_index].pwr_vset[range].RangeMin ) && 
                  ((boost_byp_data->railInfo[pmic_index][boost_byp_index].MaxPinVoltage)*1000 <= boost_byp_ptr->pm_pwr_data.pwr_specific_info[boost_byp_index].pwr_vset[range].RangeMax));
            }

            rpm_register_resource(resourceType, num_npa_resources + 1 , sizeof(pm_npa_boost_byp_int_rep), pm_rpm_boost_byp_translation, pm_rpm_boost_byp_apply, (void *)boost_byp_data);
            pm_rpm_boost_byp_req_data[pmic_index] = boost_byp_data;
        }
    } 
}

__attribute__((section("pm_dram_reclaim_pool")))
void pm_rpm_boost_byp_register_resource_dependencies(rpm_resource_type resourceType, uint32 num_npa_resources, uint8 pmic_index)
{
    uint8 count = 0;
    pm_pwr_resource *dependency = NULL;
    pm_pwr_resource_dependency_info *depInfo = NULL;
    pm_npa_boost_byp_int_rep *current_agg = NULL;
    pm_boost_byp_data_type* boost_byp_ptr = NULL;
    pm_volt_level_type vol = 0;
    pm_volt_level_type pin_vol = 0;
    pm_on_off_type en_status = PM_OFF;
    pm_npa_boost_byp_data_type *boost_byp_data = NULL;
   
    if(num_npa_resources > 0)
    {
        //return if there was no driver initialized for this pmic  
        if(pm_rpm_boost_byp_req_data[pmic_index] == NULL)
        	return;

        boost_byp_data = pm_rpm_boost_byp_req_data[pmic_index];
        
        if(boost_byp_data->depInfo != NULL)
        {
            depInfo = boost_byp_data->depInfo[pmic_index];
        }
       
        for(count = 1; count < num_npa_resources + 1; count++)
        {
            rpm_get_aggregated_request_buffer(resourceType, count, (const void**)&current_agg);

            if(current_agg != NULL)
            {
                boost_byp_ptr = boost_byp_data->boostBypDriverData;
                CORE_VERIFY_PTR(boost_byp_ptr);
                DALSYS_memset(current_agg, 0, sizeof(pm_npa_boost_byp_int_rep));
                //skip over invalid peripherals
                if(boost_byp_ptr->pm_pwr_data.pwr_specific_info[count-1].periph_type == 0)
                {
                 	continue;
                }
                pm_pwr_volt_level_status_alg(&(boost_byp_ptr->pm_pwr_data), boost_byp_ptr->comm_ptr, count-1, &vol);
                current_agg->uvol = vol;
                pm_pwr_sw_enable_status_alg(&(boost_byp_ptr->pm_pwr_data), boost_byp_ptr->comm_ptr, count-1, &en_status);
                current_agg->sw_en = en_status;
                pm_boost_byp_pin_ctrl_volt_level_status(pmic_index, count-1, &pin_vol);
                current_agg->pin_uvol = pin_vol;

                current_agg->mode = PM_BBYP_MODE_INVALID;
            }
        }

        if(depInfo)
        {
            for(count = 1; count < num_npa_resources + 1; count++)
            {
                if(depInfo[count].parent_source_dependency != NULL)
                {
                    dependency = depInfo[count].parent_source_dependency;

                    // create the internal client
                    rpm_register_internal_client_type(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_BOOST_BYP_DEPENDENT, sizeof(pm_npa_boost_byp_int_rep));
                    
                    depInfo[count].parent_source_dependency_client_handle = 
                        rpm_create_client(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_BOOST_BYP_DEPENDENT);                   
                }
            }
        }
    }  
}


pm_npa_boost_byp_data_type* pm_rpm_boost_byp_get_resource_data(uint8 pmic_index)
{
  if(pmic_index < PM_MAX_NUM_PMICS) 
  {
    return  pm_rpm_boost_byp_req_data[pmic_index];
  }

  return NULL;
}

