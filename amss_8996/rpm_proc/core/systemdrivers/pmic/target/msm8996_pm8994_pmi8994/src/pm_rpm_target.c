/*! \file pm_rpm_target.c
*  \n
*  \brief This file contains PMIC functions to run sleep enter and sleep exit settings.
*  \n
*  &copy; Copyright 2013-2015 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/target/msm8996_pm8994_pmi8994/src/pm_rpm_target.c#25 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/02/15  al       Moving turn off L25 to pre_init
08/1/14   aks      Initial version.
===========================================================================*/
/*===========================================================================

                     INCLUDE FILES

===========================================================================*/
#include "assert.h"
#include "err.h"
#include "CoreVerify.h"

#include "pm_qc_pmic.h"
#include "pm_comm.h"
#include "bare_metal_info.h"
#include "pm_target_information.h"
#include "pmapp_npa.h"
#include "railway.h"
#include "pm_sleep.h"
#include "pm_sleep_config.h"
#include "pm_rpm_ldo_trans_apply.h"
#include "msmhwiobase.h"
#include "HALhwio.h"
#include "security_control_core_hwio.h"
#include "pm_config.h"
#include "npa.h"
#include "hwio_tlmm_csr.h"
#include "pm_config.h"
#include "Chipinfo.h"
#include "pm_rpm_smps_trans_apply.h"
#include "pm_pbs_client_driver.h"
#include "timetick.h"
#include "pm_rpm_target.h"
#include "pm_fg_adc_usr.h"
#include "pm_railway.h"

/*===========================================================================

                 LOCAL CONSTANT AND MACRO DEFINITIONS

===========================================================================*/

#define SMPS_S1A_SLAVE_ADDR                          0x01
#define SMPS_S1A_BASE                                0x00001400
#define SMPS_S1A_MODE_TYPE_ADDR                     (SMPS_S1A_BASE + 0x00000045)

#define SMPS_S12A_SLAVE_ADDR                          0x01
#define SMPS_S12A_BASE                                0x00003500
#define SMPS_S12A_MODE_TYPE_ADDR                     (SMPS_S12A_BASE + 0x00000045)

#define SMPS_S5C_SLAVE_ADDR                          0x05
#define SMPS_S5C_BASE                                0x00002000
#define SMPS_S5C_MODE_TYPE_ADDR                     (SMPS_S5C_BASE + 0x00000045)

#define SMPS_S8A_SLAVE_ADDR                          0x01
#define SMPS_S8A_BASE                                0x00002900
#define SMPS_S8A_GANG_CTL2_TYPE_ADDR                 (SMPS_S8A_BASE + 0x000000C1)
#define SMPS_S8A_SW_EN_ADDR                          (SMPS_S8A_BASE + 0x00000046)
#define SMPS_S8A_STATUS_ADDR                         (SMPS_S8A_BASE + 0x00000008)

#define PWM_MODE                                     0x80

#define PM_NUM_OF_PM8994_LDO   32
#define PM_NUM_OF_PM8994_VS    2    
#define UFS_MASK      0xD  //UFS mask for detecting UFS enabled

#define PM_MSS_PBS_LOG_ARRAY_SIZE 3

// Battery status read
#define PMI_PMIC_INDEX  1  //PMIC index for interface pmic

#define SMBCHG_SLAVE_ADDRESS                         0x2
#define SMBCHG_BAT_IF_BASE_ADDRESS                   0x1200
#define SMBCHG_BAT_IF_BAT_PRES_STS_ADDRESS           (SMBCHG_BAT_IF_BASE_ADDRESS + 0x08)
#define SMBCHG_BAT_IF_BAT_PRES_MASK                   0x80


#define LDO_L3A_SLAVE_ADDR                          0x01
#define LDO_L3A_BASE                                0x00004200
#define LDO_L3A_STATUS_ADDR                         (LDO_L3A_BASE + 0x00000008)
#define LDO_L3A_SW_EN_ADDR                          (LDO_L3A_BASE + 0x00000046)

#define SW_EN_MASK                                  0x80
#define VREG_GOOD_MASK                              0x81   

#define CLK_SLAVE_ADDRESS                            0x0
#define XO_BASE_ADDRESS                              0x5000
#define CLK_DIST_BASE_ADDRESS                        0x5900
#define XO_ADJ_FINE_ADDRESS                         (XO_BASE_ADDRESS + 0x5C)
#define CLK_DIST_SPARE_REG_1                        (CLK_DIST_BASE_ADDRESS + 0x51)
#define CLK_DIST_SPARE_REG_2                        (CLK_DIST_BASE_ADDRESS + 0x52)

#define XO_TRIM_OFFSET                              0x02
 

/*===========================================================================

                        STATIC VARIABLES

===========================================================================*/

static pm_sleep_cfg_type* sleep_enter_settings = NULL;
static pm_sleep_cfg_type* sleep_exit_settings = NULL;

static npa_client_handle         handle_ddr = NULL;


static boolean program_px1_alt = FALSE;
static uint8 ebi_settling_err = 0; //0x1 ->settling err, 0x2 spmi read error
/* PBS trigger MSS rail OCP enable sequence log info */
typedef struct
{
    uint64           timestamp;
    pm_err_flag_type err_flag;
}pm_mss_pbs_trig_log_type;

pm_mss_pbs_trig_log_type pm_mss_pbs_trig_log_arr[PM_MSS_PBS_LOG_ARRAY_SIZE];

extern pm_pwr_resource *smps3_a_child_dep_v2[];
extern pm_pwr_resource smps_a[];

static uint32 pm_power_grid_rev = PM_GRID_REV_MAX;
/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/

/**
 * @name pm_rpm_sleep_init
 *
 * @brief Initializes the sleep settings LUT.
 *
 * @param  sleep_mode: can be vdd_min/XO shutdown
 *
 * @return None.
 *
 * @sideeffects None.
 *
 * @sideeffects Sleep settings should be available in config.
 *
 */
__attribute__((section("pm_dram_reclaim_pool")))
void pm_rpm_sleep_init()
{
    sleep_enter_settings = (pm_sleep_cfg_type*)pm_target_information_get_specific_info(PM_PROP_SLEEP_ENTER_INFO);
    sleep_exit_settings = (pm_sleep_cfg_type*)pm_target_information_get_specific_info(PM_PROP_SLEEP_EXIT_INFO);
    CORE_VERIFY((sleep_enter_settings != NULL) && (sleep_exit_settings != NULL));
}

/**
 * @name pm_rpm_enter_sleep
 *
 * @brief This function calls the driver layer PMIC calls to set
 *        the Rails to the state prior to entering sleep.
 *
 * @param  sleep_mode: can be vdd_min/XO shutdown
 *
 * @return PMIC error code.
 *
 * @sideeffects Will over-write any client configuration
 *              requests. Any additions to the below function
 *              should be cautiously be added.
 *
 */
pm_err_flag_type
pm_rpm_enter_sleep (pm_sleep_mode_type sleep_mode)
{
    uint32 i = 0;
    uint8  read_byte = 0;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    const pm_sleep_cfg_type* p_settings;

    if (sleep_mode > PM_SLEEP_MODE_VDD_MIN)
    {
      return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }
    p_settings = &sleep_enter_settings[(uint16)sleep_mode];

    /* Two config data arrays on enter sleep: XO_SHUTDOWN and VDD_MIN.
       "The VDD_MIN one has a copy of the XO_SHUTDOWN data most likely."   */
    while( (p_settings->p_cfg[i].regAddr != 0xFFFF) &&
          (err_flag == PM_ERR_FLAG__SUCCESS) )
    {
      err_flag |= pm_comm_write_byte( p_settings->p_cfg[i].slaveID,
                              p_settings->p_cfg[i].regAddr,
                              p_settings->p_cfg[i].data, 0 );
      i++;
    }

    err_flag |= pm_comm_read_byte( CLK_SLAVE_ADDRESS, XO_ADJ_FINE_ADDRESS,  &read_byte,  0);
    err_flag |= pm_comm_write_byte(CLK_SLAVE_ADDRESS, CLK_DIST_SPARE_REG_1, read_byte,   0);
    err_flag |= pm_comm_write_byte(CLK_SLAVE_ADDRESS, CLK_DIST_SPARE_REG_2, read_byte + XO_TRIM_OFFSET, 0);

    return err_flag;
}

/**
 * @name pm_rpm_exit_sleep
 *
 * @brief This function calls the driver layer PMIC calls to set
 *        the Rails back to the prior state of entering sleep.
 *
 * @param sleep_mode: can be vdd_min/XO shutdown
 *
 * @return PMIC error code.
 *
 * @sideeffects Will restore any client configuration requests.
 *              Any additions to the below function should be
 *              cautiously be added.
 *
 */
pm_err_flag_type
pm_rpm_exit_sleep (pm_sleep_mode_type sleep_mode)
{
    uint32 i = 0;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    const pm_sleep_cfg_type* p_settings;
    uint8 status = 0;
    uint8 sw_en = 0;
    uint32 power_grid_rev ;
    uint32 slave_id = SMPS_S8A_SLAVE_ADDR;
    uint16 swen_perph_address  = SMPS_S8A_SW_EN_ADDR;
    uint16 status_perph_address = SMPS_S8A_STATUS_ADDR;

    (void)ebi_settling_err ;
    if (sleep_mode > PM_SLEEP_MODE_VDD_MIN)
    {
       return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }
    //Check power grid rev , for power grid revs less than 1 
    // the EBI rail is on LDO3 .
    pm_get_grid_revision(&power_grid_rev); 
    if (power_grid_rev < PM_GRID_REV_1)
    {
      slave_id = LDO_L3A_SLAVE_ADDR ;
      swen_perph_address = LDO_L3A_SW_EN_ADDR;
      status_perph_address = LDO_L3A_STATUS_ADDR;
    }

    //uint32 current_time_tick = 0;
    //Check if the DDR PHY rail is settled
    err_flag = pm_comm_read_byte(slave_id, swen_perph_address, &sw_en, 0 );
    if(err_flag != PM_ERR_FLAG__SUCCESS)
    {
       ebi_settling_err = 0x2;
       abort();
    }
    if( (sw_en & SW_EN_MASK) == SW_EN_MASK )
    {
       err_flag = pm_comm_read_byte(slave_id, status_perph_address, &status, 0 );
       if(err_flag != PM_ERR_FLAG__SUCCESS)
       {
         ebi_settling_err = 0x2;
         abort();
       }
       //current_time_tick = time_service_now()
       if ((status & VREG_GOOD_MASK) != VREG_GOOD_MASK )
       {
           DALSYS_BusyWait(50); //give it another 50us.. being generous here
           pm_comm_read_byte(slave_id, status_perph_address, &status, 0 );
           if ((status & VREG_GOOD_MASK) != VREG_GOOD_MASK )
           {
             ebi_settling_err = 0x1;
             abort();
           }
       }
    }
    
    
    
    p_settings = &sleep_exit_settings[(uint16)sleep_mode];

    /* Two config data arrays on exit sleep: XO_SHUTDOWN and VDD_MIN.
      "The VDD_MIN one has a copy of the XO_SHUTDOWN data most likely."  */
    while( (p_settings->p_cfg[i].regAddr != 0xFFFF) &&
          (err_flag == PM_ERR_FLAG__SUCCESS) )
    {
      err_flag |= pm_comm_write_byte( p_settings->p_cfg[i].slaveID,
                              p_settings->p_cfg[i].regAddr,
                              p_settings->p_cfg[i].data, 0) ;
      i++;
    }

    return err_flag;
}

void pm_get_grid_revision(uint32 *power_grid_rev)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  uint8 data = 0;
  
  if (pm_power_grid_rev != PM_GRID_REV_MAX)
  {
     *power_grid_rev = pm_power_grid_rev;
     return; 
  }

  err_flag |= pm_comm_read_byte(SMPS_S8A_SLAVE_ADDR, SMPS_S8A_GANG_CTL2_TYPE_ADDR, &data, 0);

  if(err_flag != PM_ERR_FLAG__SUCCESS)
  {
    abort();
  }
  if ((data & 0x80) == 0x80)
  {
    *power_grid_rev = PM_GRID_REV_0;
  }
  else
  {
    *power_grid_rev = PM_GRID_REV_1;
  }

  pm_power_grid_rev = *power_grid_rev;
}

pm_err_flag_type pm_npa_rpm_smps_auto_mode_config (uint32  ddr_cfg)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint32 power_grid_rev = 0;
    if(ddr_cfg > PMIC_NPA_MODE_ID_DDR_CFG_9)
    {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    if(handle_ddr == NULL)
    {
       pm_get_grid_revision(&power_grid_rev);  
      //create handle for DDR
      if(power_grid_rev == PM_GRID_REV_1)
      {
          handle_ddr = npa_create_sync_client (PMIC_NPA_GROUP_ID_DDR, "ddr", NPA_CLIENT_REQUIRED);
      } 
      else 
      {
          handle_ddr = npa_create_sync_client (PMIC_NPA_GROUP_ID_DDR2, "ddr", NPA_CLIENT_REQUIRED);
      }
  
      if(handle_ddr == NULL)
      {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
      }
    }

    
    npa_issue_required_request (handle_ddr, ddr_cfg);

    return err_flag;
}

pm_err_flag_type pm_npa_rpm_verify_smps_mode(uint32  ddr_cfg)
{
  uint8 s1a = 0;
  uint8 s5c = 0;
  uint8 s12a = 0;
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  if (ddr_cfg > PMIC_NPA_MODE_ID_DDR_CFG_5)
  {

    err_flag = pm_comm_read_byte(SMPS_S1A_SLAVE_ADDR, SMPS_S1A_MODE_TYPE_ADDR, &s1a, 0);
    if (err_flag != PM_ERR_FLAG__SUCCESS)
    {
      abort();
    }
    if ((s1a & PWM_MODE) != PWM_MODE)
    {
      abort();
    }
  }
  if (ddr_cfg > PMIC_NPA_MODE_ID_DDR_CFG_4)
  {

    if (program_px1_alt)
    {
      err_flag = pm_comm_read_byte(SMPS_S5C_SLAVE_ADDR, SMPS_S5C_MODE_TYPE_ADDR, &s5c, 0);
      if (err_flag != PM_ERR_FLAG__SUCCESS)
      {
        abort();
      }
      if ((s5c & PWM_MODE) != PWM_MODE)
      {
        abort();
      }
    }

    err_flag = pm_comm_read_byte(SMPS_S12A_SLAVE_ADDR, SMPS_S12A_MODE_TYPE_ADDR, &s12a, 0);
    if (err_flag != PM_ERR_FLAG__SUCCESS)
    {
      abort();
    }
    if ((s12a & PWM_MODE) != PWM_MODE)
    {
      abort();
    }

  }

  return PM_ERR_FLAG__SUCCESS;
}

pm_err_flag_type pm_rpm_check_battery_status(pm_battery_status_type *batt_status)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   uint8 data = 0;
   pm_model_type pmic_model;

   if (NULL == batt_status)
   {
      return PM_ERR_FLAG__INVALID_POINTER;
   }
   pmic_model = pm_get_pmic_model(PMI_PMIC_INDEX);
   if ( (pmic_model != PMIC_IS_PMI8994) && (pmic_model != PMIC_IS_PMI8996) )
   {
      err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
      batt_status->err_flag = err_flag;

      return err_flag;
   }

   batt_status->batt_present = FALSE;

   //Check Battery presence
   err_flag |= pm_comm_read_byte(SMBCHG_SLAVE_ADDRESS, SMBCHG_BAT_IF_BAT_PRES_STS_ADDRESS, &data, 0);

   if((err_flag == PM_ERR_FLAG__SUCCESS) && 
      ((data & SMBCHG_BAT_IF_BAT_PRES_MASK) == SMBCHG_BAT_IF_BAT_PRES_MASK))
   {
      batt_status->batt_present = TRUE;
   }

   if (batt_status->batt_present == TRUE)
   {
      //Check if Vbatt ADC is ready
      err_flag |= pm_fg_adc_usr_get_bcl_values(PMI_PMIC_INDEX, &(batt_status->adc_ready));

      if (batt_status->adc_ready == TRUE)
      {
         //Read calibrated Vbatt ADC
         err_flag |= pm_fg_adc_usr_get_calibrated_vbat(PMI_PMIC_INDEX, &(batt_status->vbat_adc)); 
      }
   }

   batt_status->err_flag = err_flag;

   return err_flag;
}

__attribute__((section("pm_dram_reclaim_pool")))
void pm_rpm_platform_init(void)
{
    
}


__attribute__((section("pm_dram_reclaim_pool")))
  void pm_rpm_platform_pre_init(void)
{
  pm_pwr_resource_dependency_info **ldo_dep_info = NULL;
  pm_pwr_resource_dependency_info **vs_dep_info = NULL;
  pm_pwr_resource *ldo_dep = NULL;
  pm_pwr_resource *vs_dep = NULL;
  uint8 ldo_index = 0;
  uint8 vs_index = 0;
  pm_model_type pmic_model;
  pm_rpm_ldo_rail_info_type    **rail_info = NULL;
  pm_rpm_ldo_rail_info_type    *ldo_rail_info = NULL;
  uint32                       fast_boot_option;

  pmic_model = pm_get_pmic_model(PMI_PMIC_INDEX);

  if ((pmic_model != PMIC_IS_PMI8994) && (pmic_model != PMIC_IS_PMI8996))
  {
    /* Get the LDO dependency info */
    ldo_dep_info = (pm_pwr_resource_dependency_info **)pm_target_information_get_specific_info(PM_PROP_LDO_DEP);

    /* Null pointer checks for the dependency info */
    CORE_VERIFY_PTR(ldo_dep_info);
    CORE_VERIFY_PTR(ldo_dep_info[0]);

    /* Make the parent NULL for PM8994 LDOs (pmic index 0) dependent on PMi */
    for (ldo_index = 1; ldo_index <= PM_NUM_OF_PM8994_LDO; ldo_index++)
    {
      ldo_dep = ldo_dep_info[0][ldo_index].parent_source_dependency;
      if (ldo_dep != NULL)
      {
        /* Check for PMi resource types */
        if (((ldo_dep->resource_type) & 0xFF000000) == 0x62000000)
        {
          ldo_dep_info[0][ldo_index].parent_source_dependency = NULL;
        }
      }
    }

    /* Get the VS dependency info */
    vs_dep_info = (pm_pwr_resource_dependency_info **)pm_target_information_get_specific_info(PM_PROP_VS_DEP);

    /* Null pointer checks for the dependency info */
    CORE_VERIFY_PTR(vs_dep_info);
    CORE_VERIFY_PTR(vs_dep_info[0]);

    /* Make the parent NULL for PM8994 VS (pmic index 0) dependent on PMi */
    for (vs_index = 1; vs_index <= PM_NUM_OF_PM8994_VS; vs_index++)
    {
      vs_dep = vs_dep_info[0][vs_index].parent_source_dependency;
      if (vs_dep != NULL)
      {
        /* Check for PMi resource types */
        if (((vs_dep->resource_type) & 0xFF000000) == 0x62000000)
        {
          vs_dep_info[0][vs_index].parent_source_dependency = NULL;
        }
      }
    }
  }

  //Detect if UFS is present
  fast_boot_option = HWIO_INF(BOOT_CONFIG, FAST_BOOT);
  if ((fast_boot_option & UFS_MASK) == FALSE)
  {
    //turn off the UFS regulator if UFS not used - LDO25
    pm_comm_write_byte(1, 0x5846, 0x0, 0x0);;
  }
  else
  {
    //If UFS is configured to be used , mark the LDO25
    //as always on. DO NOT TURN IT OFF IN SLEEP
    rail_info = (pm_rpm_ldo_rail_info_type **)pm_target_information_get_specific_info(PM_PROP_LDO_RAIL);
    CORE_VERIFY_PTR(rail_info);
    CORE_VERIFY_PTR(rail_info[0]);
    ldo_rail_info = rail_info[0];
    ldo_rail_info[PM_LDO_25].AlwaysOn = PM_ALWAYS_ON;
  }

  if (CHIPINFO_FAMILY_MSM8996 == Chipinfo_GetFamily())
  {
    /*Enable LDO15 for Istari*/
    if (NULL == rail_info || NULL == ldo_rail_info)
    {
      rail_info = (pm_rpm_ldo_rail_info_type **)pm_target_information_get_specific_info(PM_PROP_LDO_RAIL);
      CORE_VERIFY_PTR(rail_info);
      CORE_VERIFY_PTR(rail_info[0]);
      ldo_rail_info = rail_info[0];
    }
    ldo_rail_info[PM_LDO_15].AlwaysOn = PM_ALWAYS_ON;
  }
}

pm_err_flag_type 
pm_rpm_target_execute_smps_pre_enable(uint8 pmic_index, uint8 smps_index, pm_on_off_type sw_en, 
                                      boolean *continue_enable_flag)
{
    static uint8 pm_mss_log_arr_index = 0;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    // Soft trigger PBS_CLIENT11 sequence to trigger SMPS7A (MSS rail) 
    // OCP enable sequence with OCP work-around. The enable sequence
    // will triggered for only sw enable request and the sw disable 
    // request will be ignored.
    if((smps_index == 6) && (pmic_index == 0))
    {
        *continue_enable_flag = FALSE;

        if(sw_en == PM_ON)
        {
            err_flag = pm_pbs_client_sw_trigger(pmic_index, PM_PBS_CLIENT_11);

            // Enable the MSS rail in NPA layer if 
            // the PBS trigger is not successful, 
            if(err_flag != PM_ERR_FLAG__SUCCESS)
            {
                *continue_enable_flag = TRUE;
            }

            // log the PBS trigger info for MSS rail OCP enable sequence
            pm_mss_pbs_trig_log_arr[pm_mss_log_arr_index].timestamp = timetick_get64();
            pm_mss_pbs_trig_log_arr[pm_mss_log_arr_index].err_flag = err_flag;

            // increment the mss log array index
            pm_mss_log_arr_index++;

            if(pm_mss_log_arr_index >= PM_MSS_PBS_LOG_ARRAY_SIZE)
            {
                // reset the mss log array index
                pm_mss_log_arr_index = 0;
            }
        }
    }

    return err_flag;
}

  static uint32 ssc_cx_prev_volt=0;
  static uint32 ssc_mx_prev_volt=0;
  static uint32 ssc_cx_cur_volt=0;
  static uint32 ssc_mx_cur_volt=0;
  static uint32 ssc_cx_proposed_volt=0;
  static uint32 ssc_mx_proposed_volt=0;
pm_err_flag_type 
pm_rpm_target_ldo_volt_lvl_status(uint8 pmic_index, uint8 ldo_index, rpm_application_info *info,
                                    pm_npa_ldo_int_rep *previous_aggregation )
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS; 
   pm_npa_ldo_int_rep *ldo_int_rep =(pm_npa_ldo_int_rep *)info->current_aggregation;
   pm_npa_ldo_data_type *ldo_data = (pm_npa_ldo_data_type *)info->cb_data;
   pm_ldo_data_type *ldo_ptr = ldo_data->ldoDriverData;
   
   if(ldo_index == 25)
   {
     ssc_cx_prev_volt = previous_aggregation->output_uvol;
     pm_pwr_volt_level_status_alg(&ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, ldo_index, 
                              (pm_volt_level_type*)&ssc_cx_cur_volt);
     ssc_cx_proposed_volt = ldo_int_rep->output_uvol;
    }
    else if(ldo_index == 30)
    {
      ssc_mx_prev_volt = previous_aggregation->output_uvol;
      pm_pwr_volt_level_status_alg(&ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, ldo_index, 
                              (pm_volt_level_type*)&ssc_mx_cur_volt);
      ssc_mx_proposed_volt = ldo_int_rep->output_uvol;  
    }
    (void)ssc_cx_prev_volt;
    (void)ssc_mx_prev_volt;
    (void)ssc_cx_cur_volt;
    (void)ssc_mx_cur_volt;
    (void)ssc_cx_proposed_volt;
    (void)ssc_mx_proposed_volt;
 
   return err_flag;

}

