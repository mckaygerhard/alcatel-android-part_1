#ifndef __PM_MPM__IN_H__
#define __PM_MPM_IN_H__

/**
 * @file pm_mpm_internal.h
 *
 * Header file for internal declarations PMIC MPM VDD command driver.
 */
/*
  ====================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================

  $Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/framework/inc/pm_mpm_internal.h#3 $
  $DateTime: 2015/07/10 15:46:48 $
  $Author: pwbldsvc $

  ====================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "pm_mpm.h"

#define MAX_OF_TWO(x,y)     ((x) > (y) ? (x) : (y))
#define MAX_OF_THREE(x,y,z)  ((x) > (y) ? (((x) > (z))? (x): (z)): ((y) > (z)? (y):(z)))

uint32 pm_mpm_vdda_ebi_turn_on_delay(void);
/*=========================================================================
      Type Definitions
==========================================================================*/

/**
  * Structure to store the index of the dynamically changing
  * elements for the SPMI sequence.
  */
typedef struct
{
  uint32 sleep_level_index;    /* retention index */
  uint32 active_level_index;   /* active index  */
  uint32 sw_mode_level_index;  /* SW mode before sleep index */
} pm_mpm_cmd_index_type;


/*----------------------------------------------------------------------------
 * Function : pm_mpm_cmd_init
 * -------------------------------------------------------------------------*/
/*!
   Get DAL device configuration. This code runs once.
     @dependencies
     pm_target_information_get_target_info
     @return
     None.
*/
void  pm_mpm_cmd_init( void );

void pm_mpm_platform_init(void);

#endif /* __PM_MPM_IN_H__ */

