#ifndef PM_RPM_TARGET_H
#define PM_RPM_TARGET_H

/*! \file pm_rpm_target.h
*  \n
*  \brief This file contains PMIC function prototypes for sleep related functions.
*  \n
*  &copy; Copyright 2013-2015 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/framework/inc/pm_rpm_target.h#5 $  

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/17/13   HS      Initial version.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_npa_device.h"
#include "pm_err_flags.h"
#include "pm_rpm_ldo_trans_apply.h"

#define   PM_MAX_SLEEP_MODE       2

/* Battery status type */
typedef struct
{
   uint32           vbat_adc;
   boolean          batt_present;
   boolean          adc_ready;
   pm_err_flag_type err_flag;
}pm_battery_status_type;

/*===========================================================================

FUNCTION pm_rpm_sleep_init

DESCRIPTION
    Initializes the sleep settings LUT.

INPUT PARAMETERS
  NONE

RETURN VALUE
  NONE

DEPENDENCIES
  Sleep settings should be available in config.

SIDE EFFECTS
  None

===========================================================================*/

void pm_rpm_sleep_init(void);


/*===========================================================================

FUNCTION pm_rpm_platform_init

DESCRIPTION
    This function initialize the platform specific settings.

INPUT PARAMETERS
  NONE

RETURN VALUE
  NONE

DEPENDENCIES
  PMIC APIs should be available.

===========================================================================*/
void
pm_rpm_platform_init (void);

/** 
 * @name pm_rpm_target_execute_smps_pre_enable 
 *  
 * @brief Target specific function called from npa layer to 
 *        execute pre SMPS enable dependencies
 * 
 * @param pmic_index[in]: PMIC chip index 
 *        smps_index[in]: SMPS peripheral index
 *        sw_en[in]: SMPS Aggregated sw enable request
 *        continue_enable_flag[out]: flag to indicate if SMPS
 *        enable related execution needs to be continued or not.
 *
 * @return pm_err_flag_type 
 *  
 * @sideeffects None
 *
 */
pm_err_flag_type 
pm_rpm_target_execute_smps_pre_enable (uint8 pmic_index, uint8 smps_index, pm_on_off_type sw_en, 
                                       boolean *continue_enable_flag);

/**
* @brief This function returns the battery status.
* 
* @param[out] batt_status  Battery present status and voltage.
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__SUCCESS = SUCCESS else ERROR.
*
*/
pm_err_flag_type 
pm_rpm_check_battery_status(pm_battery_status_type *batt_status);

/*===========================================================================

FUNCTION pm_rpm_platform_pre_init

DESCRIPTION
    This function initialize the platform specific settings prior
    to drivers initialization.

INPUT PARAMETERS
  NONE

RETURN VALUE
  NONE

DEPENDENCIES 
  NONE 

===========================================================================*/
void
pm_rpm_platform_pre_init (void);

/*===========================================================================

FUNCTION pm_rpm_target_ldo_volt_lvl_status

DESCRIPTION
    This function is used to log the voltage level for the LDO 

INPUT PARAMETERS
  uint8 pmic_index 
  uint8 ldo_index
  rpm_application_info *info
  pm_npa_ldo_int_rep *previous_aggregation

RETURN VALUE
  pm_err_flag_type 

DEPENDENCIES 
  NONE 

===========================================================================*/

pm_err_flag_type pm_rpm_target_ldo_volt_lvl_status
	          (uint8 pmic_index, uint8 ldo_index, rpm_application_info *info,
	           pm_npa_ldo_int_rep *previous_aggregation );

#endif /* PM_RPM_TARGET_H */
