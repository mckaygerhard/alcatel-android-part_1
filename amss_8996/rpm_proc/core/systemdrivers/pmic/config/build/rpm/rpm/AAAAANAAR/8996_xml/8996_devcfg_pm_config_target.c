#include "devcfg_pm_config_target.h"
#include "devcfg_pm_config_common.h"
/*! \file pm_config_target.c
 *
 *  \brief This file contains customizable target specific driver settings & PMIC registers.
 *         This file info is taken from Target Component Resource Specific Settings from PDM
 *         that is maintained for each of the targets separately.
 *
 *  &copy; Copyright 2013 - 2015 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/config/msm8996/pm_config_target.c#26 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
11/19/15   al      Disabling L13 and L21 error settling 
10/21/15   al      Update L31A, 26A, L13 and L27
01/17/14   kt      Created.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES

===========================================================================*/
#include "pmapp_npa.h"
#include "pm_npa.h"
#include "pm_target_information.h"
#include "pm_rpm_ldo_trans_apply.h"
#include "pm_rpm_smps_trans_apply.h"
#include "pm_rpm_vs_trans_apply.h"
#include "pm_rpm_clk_buffer_trans_apply.h"
#include "pm_rpm_boost_trans_apply.h"
#include "pm_rpm_boost_byp_trans_apply.h"
#include "pm_config.h"
#include "pm_sleep_config.h"


extern pm_pwr_volt_info_type fts2p5_volt[];
extern pm_pwr_volt_info_type n600_volt[];
extern pm_pwr_settling_time_info_type smps_settling_time[];
extern pm_pwr_settling_time_info_type ldo_settling_time[];

uint32   num_of_ldo[]        = {32,    1,       1, 2} ;
uint32   num_of_smps[]       = {12,    3,       5, 0};
uint32   num_of_vs[]         = { 2, NULL,    NULL, NULL} ;
uint32   num_of_boost[]      = { 0,    1,    NULL, NULL};
uint32   num_of_boost_byp[]  = { 0,    1,    NULL, NULL};
uint32   num_of_pbs_client[] = {16, 12, 8, 5};
uint32   num_of_fg[]         = { 0, 1, 0, 0};

/* LpHpCurrentThreshold, SafetyHeadRoom, IsSawControlled, AccessAllowed, AlwaysOn, MinPwrMode, BypassDisallowed, DroopDetect, MinVoltage, MaxVoltage, MinSettlingTime, SettlingErrorEnabled, SettlingEnabled,  Reserved */
pm_rpm_ldo_rail_info_type ldo_rail_a[] = 
{
    {10,  25, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1000, 1300, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO1 N1200_STEPPER
    {10,  25, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED,PM_DROOP_DETECT_DIS, 1200, 1300, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO2 N300
    {10, 200, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS,  375, 1100, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO3 N1200_STEPPER
    { 5,  25, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1200, 1250, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO4 N300
    { 5, 300, 0, PM_ACCESS_DISALLOWED,PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1380, 1740, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO5 LN_LDO
    { 5,  25, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1200, 1250, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO6 P150
    { 5, 300, 0, PM_ACCESS_DISALLOWED,PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1740, 2000, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO7 LN_LDO
    { 5, 300, 0, PM_ACCESS_DISALLOWED,PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1800, 1800, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO8 LV_P50 
    {10, 300, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1700, 3050, 0, PM_SETTLING_ERR_DIS,PM_SETTLING_EN, 0}, // LDO9 P150
    {10, 300, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1700, 3050, 0, PM_SETTLING_ERR_DIS,PM_SETTLING_EN, 0}, // LDO10 P150
    { 5,  25, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1100, 1250, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO11 N300
    {10, 300, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1800, 1800, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO12 LV_P300
    {10, 300, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1800, 2950, 0, PM_SETTLING_ERR_DIS,PM_SETTLING_EN, 0}, // LDO13 P150
    {10, 300, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1700, 3050, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO14 P150
    {10, 300, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1800, 1800, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO15 P300
    { 5,  25, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 2700, 2800, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO16 P150 
    {10, 250, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 2500, 2700, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO17 P300
    {10, 200, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 2700, 2900, 0, PM_SETTLING_ERR_DIS,PM_SETTLING_EN,0},  // LDO18 P300
    {10, 100, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 2850, 3100, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO19 P600
    {10, 300, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 2700, 2950, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO20 P600
    {10, 300, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 2700, 2950, 0, PM_SETTLING_ERR_DIS,PM_SETTLING_EN, 0}, // LDO21 P600
    {10, 300, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1750, 3300, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO22 P150
    {10, 200, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 2700, 2900, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO23 P600
    { 5, 300, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 3075, 3075, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO24 P150
    { 5, 100, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED,PM_DROOP_DETECT_DIS, 1200, 1200, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO25 P600
    {10, 100, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED,PM_DROOP_DETECT_DIS,  375, 1000, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO26 N600_STEPPER
    {10, 150, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1000, 1200, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO27 N300
    {10,  25, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED,PM_DROOP_DETECT_DIS,  925, 1000, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO28 N300
    { 5,  25, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 2750, 2850, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO29 P300
    {10, 300, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1800, 1800, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO30 P50
    {10, 200, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED,PM_DROOP_DETECT_DIS,  375, 1000, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO31 N600_STEPPER
    {10, 125, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,   PM_DROOP_DETECT_DIS, 1800, 1800, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO32 P50
   
};

/* LpHpCurrentThreshold, SafetyHeadRoom, IsSawControlled, AccessAllowed, AlwaysOn, MinPwrMode, BypassDisallowed, DroopDetect, MinVoltage, MaxVoltage, MinSettlingTime, SettlingErrorEnabled, SettlingEnabled, Reserved */
pm_rpm_ldo_rail_info_type ldo_rail_d[] = 
{
    { 5,  25, 0, PM_ACCESS_DISALLOWED, PM_ALWAYS_ON, PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,    PM_DROOP_DETECT_DIS, 1500, 1800, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO1 LN_LDO (XO)
    { 5,  25, 0, PM_ACCESS_DISALLOWED, PM_NONE,      PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED,    PM_DROOP_DETECT_DIS, 1300, 1610, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO2 LN_LDO (RF)
};

/* LpHpCurrentThreshold, IsSawControlled, AccessAllowed, AlwaysOn, MinPwrMode, MinFreq, MaxFreq, MinVoltage, MaxVoltage, MinSettlingTime, SettlingErrorEnabled, SettlingEnabled, Reserved */
pm_rpm_smps_rail_info_type smps_rail_a[] = 
{
    {500, 0, PM_ACCESS_ALLOWED,   PM_ALWAYS_ON, PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_3p2_MHz, PM_CLK_3p2_MHz, PM_DROOP_DETECT_EN, 375,  1100, 0,PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //FTS1
    {500, 0, PM_ACCESS_ALLOWED,   PM_ALWAYS_ON, PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_3p2_MHz, PM_CLK_3p2_MHz, PM_DROOP_DETECT_EN, 375,  1100, 0,PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //FTS2
    {100, 0, PM_ACCESS_ALLOWED,   PM_ALWAYS_ON, PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_2p13_MHz,PM_CLK_6p4_MHz, PM_DROOP_DETECT_EN, 1200, 1300, 0,PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //HFS3 
    {100, 0, PM_ACCESS_ALLOWED,   PM_ALWAYS_ON, PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_1p6_MHz, PM_CLK_3p2_MHz, PM_DROOP_DETECT_DIS, 1800, 1800, 0,PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //HFS4
    {100, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_1p6_MHz, PM_CLK_1p6_MHz, PM_DROOP_DETECT_DIS, 2100, 2100, 0,PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //HFS5
    {500, 0, PM_ACCESS_DISALLOWED,PM_NONE,      PM_NPA_SW_MODE_SMPS__INVALID,PM_SWITCHING_FREQ_FREQ_NONE, PM_SWITCHING_FREQ_FREQ_NONE, PM_DROOP_DETECT_DIS, 80, 80, 0,PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //FTS6
    {100, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_2p13_MHz,PM_CLK_2p13_MHz, PM_DROOP_DETECT_DIS, 495,  1115, 0,PM_SETTLING_ERR_DIS, PM_SETTLING_DIS, 0}, //HFS7
    {500, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_3p2_MHz, PM_CLK_3p2_MHz, PM_DROOP_DETECT_DIS, 375,  1200, 0,PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //FTS8
    {500, 0, PM_ACCESS_DISALLOWED,PM_NONE,      PM_NPA_SW_MODE_SMPS__INVALID,PM_SWITCHING_FREQ_FREQ_NONE, PM_SWITCHING_FREQ_FREQ_NONE, PM_DROOP_DETECT_DIS, 80, 80, 0,PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //FTS9
    {500, 0, PM_ACCESS_DISALLOWED,PM_NONE,      PM_NPA_SW_MODE_SMPS__INVALID,PM_SWITCHING_FREQ_FREQ_NONE, PM_SWITCHING_FREQ_FREQ_NONE, PM_DROOP_DETECT_DIS, 80, 80, 0,PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //FTS10
    {500, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_3p2_MHz, PM_CLK_6p4_MHz, PM_DROOP_DETECT_DIS, 375,  1100, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //FTS11
    {500, 0, PM_ACCESS_ALLOWED,   PM_NONE,      PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_3p2_MHz, PM_CLK_3p2_MHz, PM_DROOP_DETECT_DIS,1125, 1125, 0,PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //FTS12
   
};

/* AccessAllowed, AlwaysOn, MinSettlingTime, SettlingErrorEnabled, SettlingEnabled, Reserved  */
pm_rpm_vs_rail_info_type vs_rail_a[] = 
{
    {PM_ACCESS_ALLOWED, PM_NONE, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //LVS 1 LV300
    {PM_ACCESS_ALLOWED, PM_NONE, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //LVS 2 LV100
};

/* AccessAllowed, AlwaysOn, SettlingErrorEnabled, SettlingEnabled, reserved */
pm_rpm_clk_info_type clk_info_a[]=
{
    //{PM_ACCESS_DISALLOWED, PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},  //XO *managed by SBL settings
    {PM_ACCESS_DISALLOWED,PM_ALWAYS_ON, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},//BB_CLK1 *PBS puts in PIN control
    {PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},     //BB_CLK2
    {PM_ACCESS_DISALLOWED,PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},     //EMPTY
    {PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},     //RF_CLK1
    {PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},     //RF_CLK2
    {PM_ACCESS_DISALLOWED,PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},     //EMPTY
    {PM_ACCESS_DISALLOWED,PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},     //EMPTY
    {PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},     //LN_BB_CLK
    {PM_ACCESS_DISALLOWED,PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},     //CLK_DIST *not supported
    {PM_ACCESS_DISALLOWED,PM_ALWAYS_ON, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},//SLEEP_CLK
    {PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},     //DIV_CLK1
    {PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},     //DIV_CLK2
    {PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}      //DIV_CLK3
};

/* AccessAllowed, AlwaysOn, SettlingErrorEnabled, SettlingEnabled, reserved */
pm_rpm_clk_info_type clk_info_d[]=
{
    //{PM_ACCESS_DISALLOWED, PM_NONE},   //XO *managed by SBL settings
	{PM_ACCESS_DISALLOWED,PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},      //EMPTY
	{PM_ACCESS_DISALLOWED,PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},      //EMPTY
	{PM_ACCESS_DISALLOWED,PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},      //EMPTY
	{PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},	     //RF_CLK1
	{PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},      //RF_CLK2
	{PM_ACCESS_DISALLOWED,PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},      //EMPTY
	{PM_ACCESS_DISALLOWED,PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},      //EMPTY
	{PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},      //LN_BB_CLK
	{PM_ACCESS_DISALLOWED,PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},	     //CLK_DIST *not supported
	{PM_ACCESS_DISALLOWED,PM_ALWAYS_ON, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //SLEEP_CLK
	{PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},      //DIV_CLK1
	{PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},      //DIV_CLK2
	{PM_ACCESS_ALLOWED,   PM_NONE, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}       //DIV_CLK3
};


/* LpHpCurrentThreshold, SafetyHeadRoom, IsSawControlled, AccessAllowed, AlwaysOn, MinPwrMode, BypassDisallowed, DroopDetect, MinVoltage, MaxVoltage, MinSettlingTime, SettlingErrorEnabled ,Reserved */
pm_rpm_ldo_rail_info_type ldo_rail_b[] = 
{
    {10, 25, 0, PM_ACCESS_DISALLOWED, PM_ALWAYS_ON, PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED, PM_DROOP_DETECT_DIS, 1500, 3075, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN,0}, // LDO1 N1200_STEPPER
};

/* LpHpCurrentThreshold, IsSawControlled, AccessAllowed, AlwaysOn, MinPwrMode, MinFreq, MaxFreq, MinVoltage, MaxVoltage, MinSettlingTime, SettlingErrorEnabled, SettlingEnabled, Reserved */
pm_rpm_smps_rail_info_type smps_rail_b[] = 
{
    {100, 0, PM_ACCESS_ALLOWED,   PM_NONE, PM_NPA_SW_MODE_SMPS__AUTO, PM_CLK_1p6_MHz, PM_CLK_3p2_MHz, PM_DROOP_DETECT_DIS, 1000, 1350, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN,0}, //HFS1
    {500, 0, PM_ACCESS_DISALLOWED,PM_NONE, PM_NPA_SW_MODE_SMPS__AUTO, PM_CLK_3p2_MHz, PM_CLK_6p4_MHz, PM_DROOP_DETECT_DIS,  500,  1100, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN,0}, //FTS2
    {500, 0, PM_ACCESS_DISALLOWED,PM_NONE, PM_NPA_SW_MODE_SMPS__INVALID,PM_SWITCHING_FREQ_FREQ_NONE, PM_SWITCHING_FREQ_FREQ_NONE, PM_DROOP_DETECT_DIS, 80, 80, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN,0}, //FTS3
};

/* AccessAllowed, AlwaysOn, MinPwrMode, SettlingErrorEnabled ,Reserved, MinVoltage, MaxVoltage, MinPinVoltage, MaxPinVoltage */
pm_rpm_boost_byp_rail_info_type boost_byp_rail_b[] = 
{
    {PM_ACCESS_ALLOWED, PM_ALWAYS_ON, PM_BBYP_MODE_AUTO_BOOST, 0, 3150, 5650, 3300, 5650},
};

/* AccessAllowed, AlwaysOn, MinSettlingTime, SettlingErrorEnabled, SettlingEnabled, Reserved  */
pm_rpm_boost_rail_info_type boost_rail_b[] = 
{
    {PM_ACCESS_ALLOWED, PM_NONE, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},  //5V BOOST
};

/* LpHpCurrentThreshold, SafetyHeadRoom, IsSawControlled, AccessAllowed, AlwaysOn, MinPwrMode, BypassDisallowed, DroopDetect, MinVoltage, MaxVoltage, MinSettlingTime, SettlingErrorEnabled Reserved */
pm_rpm_ldo_rail_info_type ldo_rail_c[] = 
{
    {10, 25, 0, PM_ACCESS_ALLOWED, PM_NONE, PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_ALLOWED, PM_DROOP_DETECT_DIS, 1000, 1300, 0,  PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, // LDO1 N1200_STEPPER
};

/* LpHpCurrentThreshold, IsSawControlled, AccessAllowed, AlwaysOn, MinPwrMode, MinFreq, MaxFreq, MinVoltage, MaxVoltage, MinSettlingTime, SettlingErrorEnabled, SettlingEnabled, Reserved */
pm_rpm_smps_rail_info_type smps_rail_c[] = 
{
    {100, 0, PM_ACCESS_ALLOWED,   PM_NONE,     PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_1p6_MHz, PM_CLK_3p2_MHz, PM_DROOP_DETECT_DIS, 1000, 1350, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //HFS1
    {500, 0, PM_ACCESS_DISALLOWED,   PM_NONE,  PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_3p2_MHz, PM_CLK_3p2_MHz, PM_DROOP_DETECT_DIS,  500,  1115, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //FTS2
    {500, 0, PM_ACCESS_DISALLOWED,PM_NONE,     PM_NPA_SW_MODE_SMPS__INVALID,PM_SWITCHING_FREQ_FREQ_NONE, PM_SWITCHING_FREQ_FREQ_NONE, PM_DROOP_DETECT_DIS, 80, 80, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},//FTS3 Does not exist
    {500, 0, PM_ACCESS_DISALLOWED,PM_NONE,     PM_NPA_SW_MODE_SMPS__INVALID,PM_SWITCHING_FREQ_FREQ_NONE, PM_SWITCHING_FREQ_FREQ_NONE, PM_DROOP_DETECT_DIS, 80, 80, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0},//FTS4 HW Controlled
    {500, 0, PM_ACCESS_ALLOWED,   PM_ALWAYS_ON,PM_NPA_SW_MODE_SMPS__AUTO,   PM_CLK_3p2_MHz, PM_CLK_3p2_MHz, PM_DROOP_DETECT_EN,  500,  1190, 0, PM_SETTLING_ERR_EN, PM_SETTLING_EN, 0}, //FTS5
};

pm_rpm_ldo_rail_info_type* ldo_rail[]=
{
    ldo_rail_a, ldo_rail_b, ldo_rail_c,ldo_rail_d
};

pm_rpm_smps_rail_info_type* smps_rail[]=
{
    smps_rail_a, smps_rail_b, smps_rail_c, NULL
};

pm_rpm_vs_rail_info_type* vs_rail[]=
{
    vs_rail_a , NULL, NULL, NULL
};

pm_rpm_clk_info_type* clk_info[]=
{
    clk_info_a , NULL, NULL, clk_info_d
};

pm_rpm_boost_rail_info_type* boost_rail[]=
{
    NULL, boost_rail_b, NULL, NULL
};

pm_rpm_boost_byp_rail_info_type* boost_byp_rail[]=
{
    NULL, boost_byp_rail_b, NULL, NULL
};

/* resource_type, peripheral_type, resource_index, volt_setting, settling_time_info */
pm_pwr_resource_info_type mx_rail[1] =
{
    {RPM_SMPS_A_REQ, PM_HW_MODULE_FTS, 2}
};

pm_pwr_resource_info_type cx_rail[1] =
{
    {RPM_SMPS_A_REQ, PM_HW_MODULE_FTS, 1}
};

pm_pwr_resource_info_type gfx_rail[1] =
{
    {RPM_SMPS_B_REQ, PM_HW_MODULE_FTS, 2}
};

pm_pwr_resource_info_type ebi_rail[1] =
{
    {RPM_LDO_A_REQ, PM_HW_MODULE_LDO, 26}
};

/* Sleep configuration for enter XO_SHUTDOWN or VDD_MIN. */
static const pm_sleep_reg_type enter_xo_shutdown[] =
{
    {0, 0x5146, 0x01}, // BB_CLK1_EN_CTL: Set BBCLK_NOT_FORCE, and follow Pin
	{0, 0xFFFF, 0x00}, // invalid setting, used to check the end of the array.
};

static const pm_sleep_reg_type enter_vdd_min[] =
{
    {0, 0x5146, 0x01}, // BB_CLK1_EN_CTL: Set BBCLK_NOT_FORCE, and follow Pin
	{0, 0xFFFF, 0x00}, // invalid setting, used to check the end of the array.
};

const pm_sleep_cfg_type sleep_enter_info[] = {
    {enter_xo_shutdown},
    {enter_vdd_min}
};

static const pm_sleep_reg_type exit_xo_shutdown[] =
{
    {0, 0x5146, 0x81}, // BB_CLK1_EN_CTL: Force BBCLK enable and follow Pin
	{0, 0xFFFF, 0x00}, // invalid setting, used to check the end of the array.
};

static const pm_sleep_reg_type exit_vdd_min[] =
{
    {0x0, 0x5146, 0x81},    // BB_CLK1_EN_CTL: Force BBCLK enable and follow Pin
    {0xE, 0x540,  0x01},    // Sending resend all command.
    {0xE, 0x614,  0x01},    // HW bug fix, clear SPMI INT LATCHED bit.
    {0x0, 0xFFFF, 0x00},    // check the end of the array.
};

const pm_sleep_cfg_type sleep_exit_info[] = {
    {exit_xo_shutdown},
    {exit_vdd_min}
};



