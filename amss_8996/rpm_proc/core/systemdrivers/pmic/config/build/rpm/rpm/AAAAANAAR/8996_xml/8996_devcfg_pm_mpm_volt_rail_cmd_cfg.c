#include "devcfg_pm_config_target.h"
#include "devcfg_pm_config_common.h"
/*! \file
*  \n
*  \brief  pm_mpm_volt_rail_cmd_cfg.c
*  \n
*  \n This file contains pmic configuration data specific for PM8994 mpm shutdown/wakeup
*  \n voltage rail cmds.
*  \n
*  \n &copy; Copyright 2014 Qualcomm Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/config/msm8996/pm_mpm_volt_rail_cmd_cfg.c#8 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/12/14   vtw     Updated with 8994 MPM sequence settings.
10/31/13   vtw     Created.
========================================================================== */

/*=========================================================================
      Include Files
==========================================================================*/

#include "pm_mpm.h"
#include "pm_mpm_internal.h"
#include "pm_railway.h"

#define VDDA_EBI_TURN_ON_DELAY  200  // delay in us to turn on VDDA_EBI
/*===========================================================================
Data Declarations
===========================================================================*/

/*This enum is in strict order. If you need to
add /delete any entry from the sleep_cfg array
this enum should be updated as well*/
typedef enum 
{
  PM_MPM_SLEEP_CMD_CX_VOLT,
  PM_MPM_SLEEP_CMD_EBI_EN,
  PM_MPM_SLEEP_CMD_MX_VOLT,
  PM_MPM_SLEEP_CMD_PBS_TRIG, 
  PM_MPM_SLEEP_CMD_END,
}pm_mpm_sleep;

/*This enum is in strict order. If you need to
add /delete any entry from the active_cfg array
this enum should be updated as well*/
typedef enum 
{
  PM_MPM_ACTIVE_CMD_PBS_TRIG,
  PM_MPM_ACTIVE_CMD_MX_MODE,
  PM_MPM_ACTIVE_CMD_CX_MODE,
  PM_MPM_ACTIVE_CMD_MX_VOLT,
  PM_MPM_ACTIVE_CMD_EBI_EN,
  PM_MPM_ACTIVE_CMD_CX_VOLT,
  PM_MPM_ACTIVE_CMD_END,
}pm_mpm_active;


/**
  * MPM PMIC sleep register settings.
  */
pm_mpm_cfg_type
pm_mpm_sleep_cfg[] =
{
  [PM_MPM_SLEEP_CMD_PBS_TRIG]        = {0x0E, 0x07642, 0x1, 0x6}, /* Program  SW PBS trigger. */
  [PM_MPM_SLEEP_CMD_CX_VOLT]         = {0x1, 0x01441, 0x0, 0x0}, /* Set VDD_CX retention. */
  [PM_MPM_SLEEP_CMD_MX_VOLT]         = {0x1, 0x01741, 0x0, 0x0}, /* Set VDD_MX retention */
  [PM_MPM_SLEEP_CMD_END]             = {0x0, 0x0,     0x0, 0x0}, /* End of sleep sequence. */
  [PM_MPM_SLEEP_CMD_EBI_EN]          = {0x1, 0x02946, 0x0, 0x0}, /* DDR PHY PC */
};

/**
  * MPM PMIC active register settings.
  */
pm_mpm_cfg_type
pm_mpm_active_cfg[] =
{
  [PM_MPM_ACTIVE_CMD_MX_MODE]           = {0x1, 0x01745, 0x0, 0x0},  /* Retore MX to mode before sleep. */
  [PM_MPM_ACTIVE_CMD_CX_MODE]           = {0x1, 0x01445, 0x0, 0x0},  /* Retore CX to mode before sleep. */
  [PM_MPM_ACTIVE_CMD_MX_VOLT]           = {0x1, 0x01741, 0x0, 0x0},  /* Turn on MX with active voltage.  */
  [PM_MPM_ACTIVE_CMD_CX_VOLT]           = {0x1, 0x01441, 0x0, 0x0},  /* Turn on CX with active voltage.  */
  [PM_MPM_ACTIVE_CMD_PBS_TRIG]          = {0x0E, 0x07642, 0x1, 0x6},  /* Program SW PBS trigger. */
  [PM_MPM_ACTIVE_CMD_END]               = {0x0, 0x0,     0x0, 0x0},  /* End active sequence. */
  [PM_MPM_ACTIVE_CMD_EBI_EN]            = {0x1, 0x02946, 0x80, 0x0}, /* DDR PHY Restore */
};

/**
  * MPM cmd index buffer.
  */
const pm_mpm_cmd_index_type pm_mpm_cmd_index[] =
{
  {
    PM_MPM_SLEEP_CMD_CX_VOLT, /* CX retention index */
    PM_MPM_ACTIVE_CMD_CX_VOLT, /* CX active index  */
    PM_MPM_ACTIVE_CMD_CX_MODE, /* CX SW mode before sleep */
  },
  {
    PM_MPM_SLEEP_CMD_MX_VOLT, /* MX retention index */
    PM_MPM_ACTIVE_CMD_MX_VOLT, /* MX active index  */
    PM_MPM_ACTIVE_CMD_MX_MODE, /* MX SW mode before sleep */
  }
};

__attribute__((section("pm_dram_reclaim_pool")))
void pm_mpm_platform_init()
{
  uint32 power_grid_rev;
  pm_get_grid_revision(&power_grid_rev);
  if(power_grid_rev < PM_GRID_REV_1)
  {
    pm_mpm_sleep_cfg[PM_MPM_SLEEP_CMD_EBI_EN].slave_addr              =  0x04246; /* LDO_3 ADDR  */
    pm_mpm_active_cfg[PM_MPM_ACTIVE_CMD_EBI_EN].slave_addr            =  0x04246; /* LDO_3 ADDR  */

  }
}

uint32 pm_mpm_vdda_ebi_turn_on_delay()
{
  return VDDA_EBI_TURN_ON_DELAY; // LDO3 turn on delay
}
