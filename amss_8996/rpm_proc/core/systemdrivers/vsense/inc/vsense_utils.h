#ifndef VSENSE_UTILS_H
#define VSENSE_UTILS_H
/*
===========================================================================


  @file vsense_utilsl.h

=======================================================================
 *! \file vsense.h
 *  \n
 *  \brief This file contains vsense utils related function prototypes,
 *            
 *  \n  
 *  \n &copy; Copyright 2014 QUALCOMM Technologies Incorporated, All Rights Reserved
 *
* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/25/14   aks     created

========================================================================== */


uint64 vsense_convert_time_to_timetick(uint64 time_us);

uint64 vsense_convert_timetick_to_time(uint64 time_tick);

#endif

