#include "vsense_internal.h"
/*
==============================================================================

FILE:         vsense_cfg.c

DESCRIPTION:
  This file contains vsense driver data for DAL based driver.

==============================================================================

            Copyright (c) 2013-2014 QUALCOMM Technologies Inc.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR

==============================================================================

                             Edit History



when       who     what, where, why
--------   ---     -----------------------------------------------------------
08/20/14   aks     Initial vsense driver for 8994v2
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/




/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 * vsense configuration data.
 */


 vsense_type_dal_cfg vsense_config_data = {17,24, VSENSE_TYPE_INTERRUPT_MODE, 5000000}; // [11 code points] for temp compensation + 4 code points above /below current voltage 
 

