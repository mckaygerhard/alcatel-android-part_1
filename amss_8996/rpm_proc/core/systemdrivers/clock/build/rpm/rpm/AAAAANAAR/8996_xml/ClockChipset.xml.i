<driver name="Clock">
   <device id=DALDEVICEID_CLOCK>
      <!-- Clock sources -->
      <props name="ClockSources" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        SourceConfig
      </props>
      <!-- RPM Clocks -->
      <props name="gcc_rpm_proc_fclk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        RPMClockConfig
      </props>
      <!-- System NOC Clocks -->
      <props name="gcc_sys_noc_axi_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        SystemNOCClockConfig
      </props>
     <!-- System NOC PIMEM Clocks -->
     <props name="gcc_sys_noc_pimem_axi_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
       SystemNOCClockConfig
     </props>
     <!-- System NOC HS Clocks -->
     <props name="gcc_sys_noc_hs_axi_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
       SYSNOCHSAXIClockConfig
     </props>
     <!-- Config NOC Clocks -->
      <props name="gcc_cfg_noc_ahb_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        ConfigNOCClockConfig
      </props>
      <!-- Peripheral NOC Clocks -->
      <props name="gcc_periph_noc_ahb_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        PeriphNOCClockConfig
      </props>
     <!-- QDSS AT Clocks -->
      <props name="gcc_qdss_at_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        QDSSATClockConfig
      </props>
      <!-- QDSS Trace Clocks -->
      <props name="gcc_qdss_traceclkin_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        QDSSTraceClockConfig
      </props>
      <!-- QDSS STM Clocks -->
      <props name="gcc_qdss_stm_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        QDSSSTMClockConfig
      </props>
      <!-- QDSS TSCTR Div2 Clocks -->
      <props name="gcc_qdss_tsctr_div2_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        QDSSTSCTRDiv2ClockConfig
      </props>
      <!-- IPA Clocks -->
      <props name="gcc_ipa_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        IPAClockConfig
      </props>
      <!-- CE1 Clocks -->
      <props name="gcc_ce1_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        CEClockConfig
      </props>
      <!-- SPMI AHB Clocks -->
      <props name="gcc_spmi_ahb_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        SPMIAHBClockConfig
      </props>
      <!-- SPMI SER Clocks -->
      <props name="gcc_spmi_ser_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        SPMISERClockConfig
      </props>
      <!-- HMSS AHB Clocks -->
      <props name="gcc_hmss_ahb_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        HMSSAHBClockConfig
      </props>
     <!-- VSense VDDA Clocks -->
     <props name="gcc_vdda_vs_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
       VSenseClockConfig
     </props>
     <!-- VSense VDD Cx Clocks -->
     <props name="gcc_vddcx_vs_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
       VSenseClockConfig
     </props>
     <!-- VSense VDD Mx Clocks -->
     <props name="gcc_vddmx_vs_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
       VSenseClockConfig
     </props>
     <!-- VSense VDD MSS Clocks -->
     <props name="gcc_mss_vs_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
       VSenseClockConfig
     </props>
     <!-- MMSS NOC AXI Clocks -->
     <props name="mmss_mmagic_axi_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
       MMSSAXIClockConfig
     </props>
     <!-- Clock Log Defaults -->
      <props name="ClockLogDefaults" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
        ClockLogDefaultConfig
      </props>
   </device>
</driver>
