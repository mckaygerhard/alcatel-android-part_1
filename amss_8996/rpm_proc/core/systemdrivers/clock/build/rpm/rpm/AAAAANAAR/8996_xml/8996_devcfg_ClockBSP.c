#include "devcfg_ClockChipset.h"
/*
==============================================================================

FILE:         ClockBSP.c

DESCRIPTION:
  This file contains clock bsp data for the DAL based driver.

==============================================================================

            Copyright (c) 2014-2015 QUALCOMM Technologies Inc.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR

==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/clock/config/msm8996/ClockBSP.c#21 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
08/14/15   vph     Set MMSS NOC AXI frequency levels at Fmax corner only
06/16/14   vph     Initial BSP driver for 8996
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBSP.h"


/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 * Clock source configuration data.
 */
const ClockSourceConfigType SourceConfig[] =
{

  /*-----------------------------------------------------------------------*/
  /* XO                                                                    */
  /*-----------------------------------------------------------------------*/  

  {
    .eSource      = HAL_CLK_SOURCE_XO,
    .HALConfig    = { HAL_CLK_SOURCE_NULL },
    .nConfigMask  = 0,
    .nFreqHz      = 19200000,
    .eVRegLevel   = CLOCK_VREG_LEVEL_LOW_MINUS,
  },

  /*-----------------------------------------------------------------------*/
  /* GPLL0 - General purpose PLL                                           */
  /*-----------------------------------------------------------------------*/  

  {
    .eSource     = HAL_CLK_SOURCE_GPLL0,
    .HALConfig   =  {
        .eSource   =   HAL_CLK_SOURCE_XO,
        .eVCO      =   HAL_CLK_PLL_VCO3,
        .nPreDiv   =   1,
        .nPostDiv  =   1,
        .nL        =   31,
        .nAlpha    =   0,
        .nAlphaU   =   0x40,
                    },
    .nConfigMask = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    .nFreqHz     = 600000000,
    .eVRegLevel  = CLOCK_VREG_LEVEL_LOW_MINUS,
  },

  /*-----------------------------------------------------------------------*/
  /* GPLL0_DIV2 - Branch of GPLL0                                          */
  /*-----------------------------------------------------------------------*/

  {
    .eSource      = HAL_CLK_SOURCE_GPLL0_DIV2,
    .HALConfig    = {
        .eSource  =   HAL_CLK_SOURCE_GPLL0,
        .eVCO     =   HAL_CLK_PLL_VCO3,
        .nPreDiv  =   0,
        .nPostDiv =   0,
        .nL       =   0x0,
        .nAlpha   =   0x0,
        .nAlphaU  =   0x0,
                    },
    .nConfigMask  = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    .nFreqHz      = 300000000,
    .eVRegLevel   = CLOCK_VREG_LEVEL_LOW_MINUS,
  },

  /*-----------------------------------------------------------------------*/
  /* GPLL1 - General purpose PLL                                           */
  /*-----------------------------------------------------------------------*/

  {
    .eSource      = HAL_CLK_SOURCE_GPLL1,
    .HALConfig    = {
        .eSource  =   HAL_CLK_SOURCE_XO,
        .eVCO     =   HAL_CLK_PLL_VCO4, // HW work-around. Require VCO 4 since 533MHz is closed to Fmin when using VCO=3
        .nPreDiv  =   1,
        .nPostDiv =   1,
        .nL       =   27,
        .nAlpha   =   0xAAAAAAAA,
        .nAlphaU  =   0xC2,
                    },
    .nConfigMask  = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    .nFreqHz      = 533000000,
    .eVRegLevel   = CLOCK_VREG_LEVEL_LOW_MINUS,
  },

  /*-----------------------------------------------------------------------*/
  /* GPLL2 -                                                               */
  /*-----------------------------------------------------------------------*/

  {
    .eSource      = HAL_CLK_SOURCE_GPLL2,
    .HALConfig    = {
        .eSource  =   HAL_CLK_SOURCE_XO,
        .eVCO     =   HAL_CLK_PLL_VCO3,
        .nPreDiv  =   1,
        .nPostDiv =   1,
        .nL       =   55,
        .nAlpha   =   0x55555555,
        .nAlphaU  =   0x55,
                     },
    .nConfigMask  = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    .nFreqHz      = 106600000,
    .eVRegLevel   = CLOCK_VREG_LEVEL_LOW_MINUS,
  },

  /*-----------------------------------------------------------------------*/
  /* GPLL3                                                                 */
  /*-----------------------------------------------------------------------*/  

  {
    .eSource      = HAL_CLK_SOURCE_GPLL3,
    .HALConfig    = {
        .eSource  =   HAL_CLK_SOURCE_XO,
        .eVCO     =   HAL_CLK_PLL_VCO3,
        .nPreDiv  =   1,
        .nPostDiv =   1,
        .nL       =   48,
        .nAlpha   =   0x0,
        .nAlphaU  =   0x98,
                    },
    .nConfigMask  = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    .nFreqHz      = 933000000,
    .eVRegLevel   = CLOCK_VREG_LEVEL_LOW_MINUS,
  },

  /*-----------------------------------------------------------------------*/
  /* MMPLL0                                                                */
  /*-----------------------------------------------------------------------*/  

  {
    .eSource      = HAL_CLK_SOURCE_MMPLL0,
    .HALConfig    = {
        .eSource  =   HAL_CLK_SOURCE_XO,
        .eVCO     =   HAL_CLK_PLL_VCO3,
        .nPreDiv  =   1,
        .nPostDiv =   1,
        .nL       =   41,
        .nAlpha   =   0xAAAAAAAA,
        .nAlphaU  =   0xAA,
                    },
    .nConfigMask  = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    .nFreqHz      = 800000000,
    .eVRegLevel   = CLOCK_VREG_LEVEL_LOW,
  },

  /*-----------------------------------------------------------------------*/
  /* MMPLL1                                                                */
  /*-----------------------------------------------------------------------*/  

  {
    .eSource      = HAL_CLK_SOURCE_MMPLL1,
    .HALConfig    = {
        .eSource  =   HAL_CLK_SOURCE_XO,
        .eVCO     =   HAL_CLK_PLL_VCO3,
        .nPreDiv  =   1,
        .nPostDiv =   1,
        .nL       =   42,
        .nAlpha   =   0,
        .nAlphaU  =   0x30,
                    },
    .nConfigMask  = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    .nFreqHz      = 810000000,
    .eVRegLevel   = CLOCK_VREG_LEVEL_LOW,
  },

  { HAL_CLK_SOURCE_NULL }
  
};


/* =========================================================================
**    nFreqHz       { eSource, nDiv2x, nM, nN, n2D },      eVRegLevel         
** =========================================================================*/

/*
 * RPM clock configurations.
 */
const ClockMuxConfigType RPMClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,         1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 300000000, { HAL_CLK_SOURCE_GPLL0_DIV2, 2, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 400000000, { HAL_CLK_SOURCE_GPLL0,      3, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 600000000, { HAL_CLK_SOURCE_GPLL0,      1, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};

/*
 * System NOC clock configurations
 */
const ClockMuxConfigType SystemNOCClockConfig[] =
{
  {   9600000, { HAL_CLK_SOURCE_XO,          4, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  19200000, { HAL_CLK_SOURCE_XO,          1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  50000000, { HAL_CLK_SOURCE_GPLL0_DIV2, 12, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 100000000, { HAL_CLK_SOURCE_GPLL0,      12, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 150000000, { HAL_CLK_SOURCE_GPLL0,       8, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 200000000, { HAL_CLK_SOURCE_GPLL0,       6, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 240000000, { HAL_CLK_SOURCE_GPLL0,       5, 0, 0, 0 }, CLOCK_VREG_LEVEL_HIGH      },
  { 0 }
};

/*
 * SYSNOCHSAXI clock configurations
 */
const ClockMuxConfigType SYSNOCHSAXIClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,          2, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  37500000, { HAL_CLK_SOURCE_GPLL0_DIV2, 16, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 100000000, { HAL_CLK_SOURCE_GPLL0_DIV2,  6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 200000000, { HAL_CLK_SOURCE_GPLL0,       6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 300000000, { HAL_CLK_SOURCE_GPLL0,       4, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 400000000, { HAL_CLK_SOURCE_GPLL0,       3, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 533000000, { HAL_CLK_SOURCE_GPLL1,       1, 0, 0, 0 }, CLOCK_VREG_LEVEL_HIGH      },
  { 0 }
};


/*
 * Config NOC clock configurations
 */
const ClockMuxConfigType ConfigNOCClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 37500000, { HAL_CLK_SOURCE_GPLL0, 32, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 75000000, { HAL_CLK_SOURCE_GPLL0, 16, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};

/*
 * Peripheral NOC clock configurations.
 */
const ClockMuxConfigType PeriphNOCClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  37500000, { HAL_CLK_SOURCE_GPLL0, 32, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  {  50000000, { HAL_CLK_SOURCE_GPLL0, 24, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  {  75000000, { HAL_CLK_SOURCE_GPLL0, 16, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 100000000, { HAL_CLK_SOURCE_GPLL0, 12, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },  
  { 0 }
};

/*
 * QDSS AT clock configurations
 */
const ClockMuxConfigType QDSSATClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,          1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  60000000, { HAL_CLK_SOURCE_GPLL0_DIV2, 10, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 120000000, { HAL_CLK_SOURCE_GPLL0,      10, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 240000000, { HAL_CLK_SOURCE_GPLL0,       5, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};

/*
 * QDSS Trace clock configurations.
 */
const ClockMuxConfigType QDSSTraceClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  75000000, { HAL_CLK_SOURCE_GPLL0_DIV2, 8, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 150000000, { HAL_CLK_SOURCE_GPLL0,  8, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 300000000, { HAL_CLK_SOURCE_GPLL0,  4, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};

/*
 * QDSS STM clock configurations.
 */
const ClockMuxConfigType QDSSSTMClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS  },
  {  50000000, { HAL_CLK_SOURCE_GPLL0_DIV2, 12, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 100000000, { HAL_CLK_SOURCE_GPLL0, 12, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW        },
  { 200000000, { HAL_CLK_SOURCE_GPLL0,  6, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL    },
  { 0 }
};

/*
 * QDSS TSCTR Div-2 clock configurations for V2 or higher
 */
const ClockMuxConfigType QDSSTSCTRDiv2ClockConfig[] =
{
  {   9600000, { HAL_CLK_SOURCE_XO,         1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  75000000, { HAL_CLK_SOURCE_GPLL0_DIV2, 4, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 150000000, { HAL_CLK_SOURCE_GPLL0,      4, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 300000000, { HAL_CLK_SOURCE_GPLL0,      2, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};

/*
 * RBCPR clock configurations.
 */
const ClockMuxConfigType RBCPRClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 0 }
};

/*
 * IPA clock configurations. 
 */
const ClockMuxConfigType IPAClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW     },
  {  37500000, { HAL_CLK_SOURCE_GPLL0, 32, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW     },
  {  75000000, { HAL_CLK_SOURCE_GPLL0, 16, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW     },
  { 150000000, { HAL_CLK_SOURCE_GPLL0,  8, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL },
  { 200000000, { HAL_CLK_SOURCE_GPLL0,  6, 0, 0, 0 }, CLOCK_VREG_LEVEL_HIGH    },
  {  0 }
};

/*
 * CE (Crypto Engine) clock configurations.
 */
const ClockMuxConfigType CEClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,          1, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  85710000, { HAL_CLK_SOURCE_GPLL0,      14, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 171430000, { HAL_CLK_SOURCE_GPLL0,       7, 0,  0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 },
};

/*
 * SPMI AHB clock configurations.
 */
const ClockMuxConfigType SPMIAHBClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 37500000, { HAL_CLK_SOURCE_GPLL0, 32, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 75000000, { HAL_CLK_SOURCE_GPLL0, 16, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};

/*
 * SPMI SER clock configurations.
 */
const ClockMuxConfigType SPMISERClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO,  1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 0 }
};

/*
 * HMSS AHB clock configurations. Only two custom configs for now.
 */
const ClockMuxConfigType HMSSAHBClockConfig[] =
{
  {  1200000, { HAL_CLK_SOURCE_XO, 32, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 19200000, { HAL_CLK_SOURCE_XO,  1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 0 }
};

/*
 * Voltage Sense clock configurations.
 */
const ClockMuxConfigType VSenseClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,    1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 400000000, { HAL_CLK_SOURCE_GPLL0, 3, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 600000000, { HAL_CLK_SOURCE_GPLL0, 1, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};

/*
 * MMSS NOC AXI clock configurations.
 */
const ClockMuxConfigType MMSSAXIClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,         1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  75000000, { HAL_CLK_SOURCE_GPLL0_DIV2, 8, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 171429000, { HAL_CLK_SOURCE_GPLL0,      7, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW       },
  { 320000000, { HAL_CLK_SOURCE_MMPLL0,     5, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 405000000, { HAL_CLK_SOURCE_MMPLL1,     4, 0, 0, 0 }, CLOCK_VREG_LEVEL_HIGH      },
  { 0 }
};

/*
 * Clock Log Default Configuration.
 *
 * NOTE: An .nGlobalLogFlags value of 0x12 will log only clock frequency
 *       changes and source state changes by default.
 */
const ClockLogType ClockLogDefaultConfig[] =
{
  {
    /* .nLogSize        = */ 4096,
    /* .nGlobalLogFlags = */ 0x12
  }
};
