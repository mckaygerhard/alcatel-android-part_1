/*
==============================================================================

FILE:         HALclkGCCMain.c

DESCRIPTION:
   The main auto-generated file for GCC.


==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/hal/clk/hw/msm8996/src/GCC/HALclkGCCMain.c#12 $

when         who     what, where, why
----------   ---     ----------------------------------------------------------- 
06/16/2014           Auto-generated.


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"
#include "Chipinfo.h"

/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/


/*
 * Clock domains
 */
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBIMCClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBIMCDDRXOClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBIMCDDRCPLL0ROOTClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBIMCHMSSAXIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCHMSSAHBClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCIPAClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCMMSSBIMCGFXClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCCE1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCCONFIGNOCClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCPERIPHNOCClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCPIMEMAXIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCQDSSATClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCQDSSSTMClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCQDSSTRACECLKINClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCQDSSTSCTRClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCRBCPRCXClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCRPMClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSPDMBIMCCY4XClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSPDMDEBUGCY4XClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSPDMFFClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSPDMPNOCCYClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSPDMRPMCYClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSPDMSNOCCY2XClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSPMIAHBClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSPMISERClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSYSNOCHSAXIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSYSTEMNOCClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCMSSQ6BIMCAXIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCMSSVSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCVDDAVSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCVDDCXVSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCVDDMXVSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCVSCTRLClkDomain;

/*
 * Power domains
 */
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCBIMCPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCIPAPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCAGGRE0NOCPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCAGGRE1NOCPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCAGGRE2NOCPowerDomain;

/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * aGCCSourceMap
 *
 * GCC HW source mapping
 * 
 * NOTES:
 * - HAL_clk_SourceMapType is an array of mapped sources
 *   - see HALclkInternal.h.
 *
 * - If source index is reserved/not used in a clock diagram, please tie that
 *   to HAL_CLK_SOURCE_GROUND.
 *
 * - {HAL_CLK_SOURCE_NULL, HAL_CLK_SOURCE_INDEX_INVALID} is used to indicate
 *   the end of the mapping array. If we reach this element during our lookup,
 *   we'll know we could not find the matching source enum for the register
 *   value, or vice versa.
 * 
 */
HAL_clk_SourceMapType aGCCSourceMap[] =
{
  {HAL_CLK_SOURCE_XO,         0},
  {HAL_CLK_SOURCE_GPLL0,      1},
  {HAL_CLK_SOURCE_GPLL2,      2},
  {HAL_CLK_SOURCE_GPLL3,      3},
  {HAL_CLK_SOURCE_GPLL1,      4},
  {HAL_CLK_SOURCE_GPLL0_DIV2, 6 },
  {HAL_CLK_SOURCE_NULL,       HAL_CLK_SOURCE_INDEX_INVALID}
};


/*
 * HAL_clk_mGCCClockDomainControl
 *
 * Functions for controlling GCC clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mGCCClockDomainControl =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aGCCSourceMap
};

/*
 * HAL_clk_mGCCNoRootClockDomainControl
 *
 * Functions for controlling GCC clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mGCCNoRootClockDomainControl =
{
   /* .ConfigMux          = */ HAL_clk_GenericNoRootConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aGCCSourceMap
};


/*
 * HAL_clk_aGCCClockDomainDesc
 *
 * List of GCC clock domains
*/
 __attribute__((section("clk_cram_reclaim_pool")))
static HAL_clk_ClockDomainDescType * HAL_clk_aGCCClockDomainDesc [] =
{
  &HAL_clk_mGCCBIMCClkDomain,
  &HAL_clk_mGCCBIMCDDRXOClkDomain,
  &HAL_clk_mGCCBIMCDDRCPLL0ROOTClkDomain,
  &HAL_clk_mGCCBIMCHMSSAXIClkDomain,
  &HAL_clk_mGCCHMSSAHBClkDomain,
  &HAL_clk_mGCCIPAClkDomain,
  &HAL_clk_mGCCMMSSBIMCGFXClkDomain,
  &HAL_clk_mGCCCE1ClkDomain,
  &HAL_clk_mGCCCONFIGNOCClkDomain,
  &HAL_clk_mGCCPERIPHNOCClkDomain,
  &HAL_clk_mGCCPIMEMAXIClkDomain,
  &HAL_clk_mGCCQDSSATClkDomain,
  &HAL_clk_mGCCQDSSSTMClkDomain,
  &HAL_clk_mGCCQDSSTRACECLKINClkDomain,
  &HAL_clk_mGCCQDSSTSCTRClkDomain,
  &HAL_clk_mGCCRBCPRCXClkDomain,
  &HAL_clk_mGCCRPMClkDomain,
  &HAL_clk_mGCCSPDMBIMCCY4XClkDomain,
  &HAL_clk_mGCCSPDMDEBUGCY4XClkDomain,
  &HAL_clk_mGCCSPDMFFClkDomain,
  &HAL_clk_mGCCSPDMPNOCCYClkDomain,
  &HAL_clk_mGCCSPDMRPMCYClkDomain,
  &HAL_clk_mGCCSPDMSNOCCY2XClkDomain,
  &HAL_clk_mGCCSPMIAHBClkDomain,
  &HAL_clk_mGCCSPMISERClkDomain,
  &HAL_clk_mGCCSYSNOCHSAXIClkDomain,
  &HAL_clk_mGCCSYSTEMNOCClkDomain,
  &HAL_clk_mGCCMSSQ6BIMCAXIClkDomain,
  &HAL_clk_mGCCMSSVSClkDomain,
  &HAL_clk_mGCCVDDAVSClkDomain,
  &HAL_clk_mGCCVDDCXVSClkDomain,
  &HAL_clk_mGCCVDDMXVSClkDomain,
  &HAL_clk_mGCCVSCTRLClkDomain,
  NULL
};


/*
 * HAL_clk_aGCCPowerDomainDesc
 */
 __attribute__((section("clk_cram_reclaim_pool")))
static HAL_clk_PowerDomainDescType * HAL_clk_aGCCPowerDomainDesc [] =
{
  &HAL_clk_mGCCAGGRE0NOCPowerDomain,
  &HAL_clk_mGCCAGGRE1NOCPowerDomain,
  &HAL_clk_mGCCAGGRE2NOCPowerDomain,
  &HAL_clk_mGCCBIMCPowerDomain,
  &HAL_clk_mGCCIPAPowerDomain,
  NULL
};

/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/


/* ===========================================================================
**  HAL_clk_PlatformInitGCCMain
**
** ======================================================================== */

__attribute__((section("clk_cram_reclaim_pool")))
void HAL_clk_PlatformInitGCCMain (void)
{

  /*
   * Install all clock and power domains
   */
  HAL_clk_InstallClockDomains(HAL_clk_aGCCClockDomainDesc, CLK_CTL_BASE);
  HAL_clk_InstallPowerDomains(HAL_clk_aGCCPowerDomainDesc, CLK_CTL_BASE);

} /* END HAL_clk_PlatformInitGCCMain */

