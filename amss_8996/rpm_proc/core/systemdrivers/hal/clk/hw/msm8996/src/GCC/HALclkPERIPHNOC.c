/*
==============================================================================

FILE:         HALclkPERIPHNOC.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   PERIPHNOC clocks.

   List of clock domains:
   -HAL_clk_mGCCPERIPHNOCClkDomain


   List of power domains:



==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/hal/clk/hw/msm8996/src/GCC/HALclkPERIPHNOC.c#2 $

when         who     what, where, why
----------   ---     ----------------------------------------------------------- 
06/16/2014           Auto-generated.


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;


/* ============================================================================
**    Data
** ==========================================================================*/

                                    
/*                           
 *  HAL_clk_mPERIPHNOCClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mPERIPHNOCClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_aggre1_pnoc_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_AGGRE1_PNOC_AHB_CBCR), HWIO_OFFS(GCC_AGGRE1_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_AGGRE1_PNOC_AHB_CLK
  },
  {
    /* .szClockName      = */ "gcc_periph_noc_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_PERIPH_NOC_AHB_CBCR), HWIO_OFFS(GCC_PERIPH_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_PERIPH_NOC_AHB_CLK
  },
  {
    /* .szClockName      = */ "gcc_periph_noc_cfg_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_PERIPH_NOC_CFG_AHB_CBCR), HWIO_OFFS(GCC_PERIPH_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_PERIPH_NOC_CFG_AHB_CLK
  },
};


/*
 * HAL_clk_mGCCPERIPHNOCClkDomain
 *
 * PERIPH NOC clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCPERIPHNOCClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_PERIPH_NOC_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mPERIPHNOCClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mPERIPHNOCClkDomainClks)/sizeof(HAL_clk_mPERIPHNOCClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


