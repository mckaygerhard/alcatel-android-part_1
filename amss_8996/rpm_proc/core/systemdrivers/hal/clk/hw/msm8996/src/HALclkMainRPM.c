/*
==============================================================================

FILE:         HALclkMainRPM.c

DESCRIPTION:
  This file contains the main platform initialization code for the clock
  HAL on the RPM processor on MSM8994.

==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/hal/clk/hw/msm8996/src/HALclkMainRPM.c#9 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
06/12/15   vph     Add MMSS for support rate matching.
11/27/13   vph     Initial version for MSM8994

==============================================================================
            Copyright (c) 2012-2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include "HALclkInternal.h"
#include "HALclkGeneric.h"
#include "HALclkGenericPLL.h"
#include "HALhwio.h"
#include "HALclkHWIO.h"
#include "assert.h"


/* List of clocks being turned off during deep sleep */
typedef struct
{
  boolean bSPMIClk;        /* gcc_spmi_ser_clk */
  boolean bImemAXIClk;     /* gcc_imem_axi_clk */
  boolean bMsgRAMClk;      /* gcc_msg_ram_ahb_clk */
  boolean bSecCtrlAccClk;  /* gcc_sec_ctrl_acc_clk */
  boolean bSecCtrlClk;     /* gcc_sec_ctrl_clk */
  boolean bSPMIAHBClk;     /* gcc_spmi_ahb_clk */
  boolean bDDRDimCfgClk;   /* gcc_ddr_dim_cfg_clk */
  boolean bDDRDimSleepClk; /* gcc_ddr_dim_sleep_clk */
  boolean bDCCAHBClk;      /* gcc_dcc_ahb_clk */
  boolean bPIMEMClk;       /* gcc_pimem_axi_clk */
} HAL_MiscClkStateType;  


/* ============================================================================
**    Prototypes
** ==========================================================================*/

void HAL_clk_PlatformInitSources(void);


/* ============================================================================
**    Externs
** ==========================================================================*/

extern void HAL_clk_PlatformInitGCCMain(void);
extern void HAL_clk_PlatformInitMMSSMain(void);

/* ============================================================================
**    Data
** ==========================================================================*/
#define MAX_RETRY 120

static HAL_MiscClkStateType HAL_MiscClkState;

/*
 * HAL_clk_aInitFuncs
 *
 * Declare array of module initialization functions.
 */
static HAL_clk_InitFuncType HAL_clk_afInitFuncs[] =
{
  /*
   * Sources
   */
  HAL_clk_PlatformInitSources,
  
  /*
   * GCC
   */
  HAL_clk_PlatformInitGCCMain,

  /*
   * MMSS
   */
  HAL_clk_PlatformInitMMSSMain,

  NULL
};


/*
 * Declare the base pointers for HWIO access.
 */
uint32 HAL_clk_nHWIOBaseTop     = CLK_CTL_BASE_PHYS;
uint32 HAL_clk_nHWIOBaseTopMMSS = MMSS_BASE_PHYS;


/*
 * HAL_clk_aHWIOBases
 *
 * Declare array of HWIO bases in use on this platform.
 */
static HAL_clk_HWIOBaseType HAL_clk_aHWIOBases[] =
{
  { CLK_CTL_BASE_PHYS,  CLK_CTL_BASE_SIZE, &HAL_clk_nHWIOBaseTop },
  { MMSS_BASE_PHYS,     MMSS_BASE_SIZE,    &HAL_clk_nHWIOBaseTopMMSS },
  { 0, 0, NULL }
};


/*
 * HAL_clk_Platform;
 * Platform data.
 */
HAL_clk_PlatformType HAL_clk_Platform =
{
  HAL_clk_afInitFuncs,
  HAL_clk_aHWIOBases
};


/*
 * GPLL contexts
 */
static HAL_clk_PLLContextType HAL_clk_aPLLContextGPLL[] =
{
  {
    HWIO_OFFS(GCC_GPLL0_MODE),
    HAL_CLK_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL0),
    HAL_CLK_PLL_SPARK
  },
  {
    HWIO_OFFS(GCC_GPLL1_MODE),
    HAL_CLK_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL1),
    HAL_CLK_PLL_SPARK
  },
  {
    HWIO_OFFS(GCC_GPLL2_MODE),
    HAL_CLK_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL2),
    HAL_CLK_PLL_SPARK
  },
  {
    HWIO_OFFS(GCC_GPLL3_MODE),
    HAL_CLK_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL3),
    HAL_CLK_PLL_SPARK
  },
  {
    HWIO_OFFS(MMSS_MMPLL0_PLL_MODE),
    HAL_CLK_FMSK(MMSS_MMSS_PLL_VOTE_RPM, ENABLE_MMPLL0),
    HAL_CLK_PLL_SPARK
  },
  {
    HWIO_OFFS(MMSS_MMPLL1_PLL_MODE),
    HAL_CLK_FMSK(MMSS_MMSS_PLL_VOTE_RPM, ENABLE_MMPLL1),
    HAL_CLK_PLL_SPARK
  }
};


/* ============================================================================
**    Functions
** ==========================================================================*/


/* ===========================================================================
**  HAL_clk_PlatformInitSources
**
** ======================================================================== */

void HAL_clk_PlatformInitSources (void)
{
  /*
   * Install PLL handlers.
   */
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL0,   &HAL_clk_aPLLContextGPLL[0], CLK_CTL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL1,   &HAL_clk_aPLLContextGPLL[1], CLK_CTL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL2,   &HAL_clk_aPLLContextGPLL[2], CLK_CTL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL3,   &HAL_clk_aPLLContextGPLL[3], CLK_CTL_BASE);
  
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MMPLL0,   &HAL_clk_aPLLContextGPLL[4], MMSS_BASE);
  
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MMPLL1,   &HAL_clk_aPLLContextGPLL[5], MMSS_BASE);
} /* END HAL_clk_PlatformInitSources */


/* ===========================================================================
**  HAL_clk_Save
**
** ======================================================================== */

void HAL_clk_Save (void)
{
  /*
   * Nothing to save.
   */

} /* END HAL_clk_Save */


/* ===========================================================================
**  HAL_clk_Restore
**
** ======================================================================== */

void HAL_clk_Restore (void)
{
  /*
   * Nothing to restore.
   */
  
} /* END HAL_clk_Restore */


/* Now that there are 6 clocks to turn off, a loop over a table is appropriate to save space.
 * Each of these clocks has a CBCR with a CLK_OFF bit to poll when turned off, and a single
 * bit to enable it.  
 *
 * pStatusVar  : points to a status value that is used to hold the pre-shutdown state.
 * nCBCRReg    : the address of the CBCR register
 * nEnableReg  : the address of the enable register.
 * nEnableMask : the bit to set(enable) or clear(disable) the clock in the nEnableReg.
 */
const struct {
  boolean* pStatusVar;
  uint32   nCBCRReg;
  uint32   nEnableReg;
  uint32   nEnableMask;
} HAL_clk_DisableDuringSleep[] = 
{
  { 
    &HAL_MiscClkState.bDDRDimCfgClk, 
    HWIO_ADDR(GCC_DDR_DIM_CFG_CBCR),
    HWIO_ADDR(GCC_DDR_DIM_CFG_CBCR),
    HWIO_FMSK(GCC_DDR_DIM_CFG_CBCR, CLK_ENABLE) 
  },  
  { 
    &HAL_MiscClkState.bDDRDimSleepClk, 
    HWIO_ADDR(GCC_DDR_DIM_SLEEP_CBCR),
    HWIO_ADDR(GCC_DDR_DIM_SLEEP_CBCR),
    HWIO_FMSK(GCC_DDR_DIM_SLEEP_CBCR, CLK_ENABLE) 
  },  
  { 
    &HAL_MiscClkState.bSPMIClk, 
    HWIO_ADDR(GCC_SPMI_SER_CBCR),
    HWIO_ADDR(GCC_SPMI_SER_CBCR),
    HWIO_FMSK(GCC_SPMI_SER_CBCR, CLK_ENABLE) 
  },  
  { 
    &HAL_MiscClkState.bImemAXIClk, 
    HWIO_ADDR(GCC_IMEM_AXI_CBCR),
    HWIO_ADDR(GCC_RPM_CLOCK_BRANCH_ENA_VOTE),
    HWIO_FMSK(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, IMEM_AXI_CLK_ENA)
  },
  { &HAL_MiscClkState.bMsgRAMClk,
    HWIO_ADDR(GCC_MSG_RAM_AHB_CBCR),
    HWIO_ADDR(GCC_RPM_CLOCK_BRANCH_ENA_VOTE),
    HWIO_FMSK(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA)
  },
  { 
    &HAL_MiscClkState.bSecCtrlAccClk, 
    HWIO_ADDR(GCC_SEC_CTRL_ACC_CBCR),
    HWIO_ADDR(GCC_SEC_CTRL_ACC_CBCR),
    HWIO_FMSK(GCC_SEC_CTRL_ACC_CBCR, CLK_ENABLE) 
  },  
  { 
    &HAL_MiscClkState.bSecCtrlClk, 
    HWIO_ADDR(GCC_SEC_CTRL_CBCR),
    HWIO_ADDR(GCC_SEC_CTRL_CBCR),
    HWIO_FMSK(GCC_SEC_CTRL_CBCR, CLK_ENABLE) 
  },  
  { 
    &HAL_MiscClkState.bSPMIAHBClk, 
    HWIO_ADDR(GCC_SPMI_AHB_CBCR),
    HWIO_ADDR(GCC_SPMI_AHB_CBCR),
    HWIO_FMSK(GCC_SPMI_AHB_CBCR, CLK_ENABLE),
  },
  { 
    &HAL_MiscClkState.bPIMEMClk, 
    HWIO_ADDR(GCC_PIMEM_AXI_CBCR),
    HWIO_ADDR(GCC_PIMEM_AXI_CBCR),
    HWIO_FMSK(GCC_PIMEM_AXI_CBCR, CLK_ENABLE),
  }
};


/* ===========================================================================
**  HAL_clk_DeepSleepDisableMiscClk
** ======================================================================== */
void HAL_clk_DeepSleepDisableMiscClk(void)
{
  int i;
  boolean status;
  uint32 shadow;
  uint32 nRetry = 0;

  /* Enable all the clocks */
  for(i=0; i<sizeof( HAL_clk_DisableDuringSleep ) / sizeof( HAL_clk_DisableDuringSleep[0] ); i++)
  {
    status = ( (inpdw(HAL_clk_DisableDuringSleep[i].nCBCRReg) & HWIO_FMSK(GCC_SPMI_SER_CBCR, CLK_OFF)) ? FALSE : TRUE);
    *(HAL_clk_DisableDuringSleep[i].pStatusVar) = status;

    if ( status == TRUE )
    {
      /* Masked write to disable the clock */
      shadow = inpdw(HAL_clk_DisableDuringSleep[i].nEnableReg);
      shadow &= (~HAL_clk_DisableDuringSleep[i].nEnableMask);
      outpdw(HAL_clk_DisableDuringSleep[i].nEnableReg, shadow);
    }
  }

  /* Now wait for the statuses, this is faster */
  for(i=0; i<sizeof( HAL_clk_DisableDuringSleep ) / sizeof( HAL_clk_DisableDuringSleep[0] ); i++)
  {
    if( *HAL_clk_DisableDuringSleep[i].pStatusVar == TRUE )
    {
      // Test to make sure that clock gets turn off
      nRetry  = 0;
      while( ((inpdw(HAL_clk_DisableDuringSleep[i].nCBCRReg) & HWIO_FMSK(GCC_SPMI_SER_CBCR, CLK_OFF)) == 0) &&
             (nRetry < MAX_RETRY) )
      {
        nRetry++;
        HAL_clk_BusyWait( 1 );
      }
      assert (nRetry < MAX_RETRY);
    }
  }

  /*
   * Need to handle special case for GCC_DCC_AHB_CLK, 
   * since this clock has HW signal which forces on when JTAG connect
   * so it cannot handle like previous clocks.  Just clear CLK_ENABLE bit
   */
  HAL_MiscClkState.bDCCAHBClk = HWIO_INF( GCC_DCC_AHB_CBCR, CLK_ENABLE );
  if ( HAL_MiscClkState.bDCCAHBClk )
  {
    HWIO_OUTF( GCC_DCC_AHB_CBCR, CLK_ENABLE, 0 );
  }
}


/* ===========================================================================
**  HAL_clk_MiscClkRestoreDuringWakeup
** ======================================================================== */
void HAL_clk_MiscClkRestoreDuringWakeup(void)
{
  int i;
  uint32 shadow;

  /*
   * Need to handle special case for GCC_DCC_AHB_CLK, 
   * since this clock has HW signal which force on when JTAG connect, 
   * so it cannot handle like previous clocks.  Just set CLK_ENABLE bit back
   * if it was previous enabled
   */
  if ( HAL_MiscClkState.bDCCAHBClk )
  {
    HWIO_OUTF( GCC_DCC_AHB_CBCR, CLK_ENABLE, 1 );
  }

  /* Run them in the opposite order, as this is usually desired in shutdown / restart sequences */
  for(i= (sizeof( HAL_clk_DisableDuringSleep ) / sizeof( HAL_clk_DisableDuringSleep[0]))-1; i>=0; i--)
  {
    if( *HAL_clk_DisableDuringSleep[i].pStatusVar == TRUE )
    {
      /* Masked write to enable the clock */
      shadow = inpdw(HAL_clk_DisableDuringSleep[i].nEnableReg);
      shadow |= HAL_clk_DisableDuringSleep[i].nEnableMask;
      outpdw(HAL_clk_DisableDuringSleep[i].nEnableReg, shadow);
    }
  }

  for(i=(sizeof( HAL_clk_DisableDuringSleep ) / sizeof( HAL_clk_DisableDuringSleep[0] ))-1; i>=0; i--)
  {
    if( *HAL_clk_DisableDuringSleep[i].pStatusVar == TRUE )
    {
      /* Wait for clock to enable */
      while( (inpdw(HAL_clk_DisableDuringSleep[i].nCBCRReg) & HAL_CLK_BRANCH_CTRL_REG_CLK_OFF_FMSK) != 0 );
    }
  }
}

 
/* ===========================================================================
**  HAL_clk_GetDeviceID
**
** ======================================================================== */
uint16 HAL_clk_GetDeviceID ( void )
{
  return HWIO_INF(TLMM_HW_REVISION_NUMBER, PRODUCT_DEVICE_ID);
} /* END HAL_clk_GetDeviceID */


/* ===========================================================================
**  HAL_clk_GPLL0_disable_deep_sleep
**
** ======================================================================== */
void HAL_clk_GPLL0_disable_deep_sleep(void)
{
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_VOTE_FSM_ENA ,0);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_BYPASSNL,0);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_RESET_N ,0);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_OUTCTRL ,0);
}


/* ===========================================================================
**  HAL_clk_GPLL0_FSM_reset_deep_sleep
**
** ======================================================================== */
void HAL_clk_GPLL0_FSM_reset_deep_sleep(void)
{
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_VOTE_FSM_RESET ,1);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_BYPASSNL,0);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_RESET_N ,0);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_OUTCTRL ,0);
   
  HAL_clk_BusyWait( 1 );
        
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_VOTE_FSM_ENA ,1);
  
  HAL_clk_BusyWait( 1 );
  
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_VOTE_FSM_RESET ,0);
}  


/* ===========================================================================
**  HAL_clk_IsDDRPLL0Active
**    Check the active DDR PLL is PLL0 or not
** ======================================================================== */
boolean HAL_clk_IsDDRPLL0Active ( void )
{
  return ( HWIO_INF(GCC_BIMC_MISC, BIMC_DDR_JCPLL_SEL_STATUS) == 0 );
}


/* ===========================================================================
**  HAL_clk_ForceAggre2ClockEnable
**    This API is using for turning on extra clocks (USB, UFS, IPA) for
** ICB driver to retain A2NOC registers in SW around A2NOC power collapse.
** ======================================================================== */
void HAL_clk_ForceAggre2ClockEnable ( boolean bEnable )
{
  static boolean bAggre2UFS, bAggre2USB, bAggre2IPA;

  if ( bEnable )
  {
    bAggre2UFS = (HWIO_INF(GCC_AGGRE2_UFS_AXI_CBCR,  CLK_ENABLE) == 1);
    bAggre2USB = (HWIO_INF(GCC_AGGRE2_USB3_AXI_CBCR, CLK_ENABLE) == 1);
    bAggre2IPA = (HWIO_INF(GCC_AGGRE2_NOC_IPA_CBCR,  CLK_ENABLE) == 1);

    HWIO_OUTF( GCC_AGGRE2_UFS_AXI_CBCR,  CLK_ENABLE, 1);
    HWIO_OUTF( GCC_AGGRE2_USB3_AXI_CBCR, CLK_ENABLE, 1);
    HWIO_OUTF( GCC_AGGRE2_NOC_IPA_CBCR,  CLK_ENABLE, 1);
  }
  else
  {
    if ( !bAggre2UFS )
      HWIO_OUTF( GCC_AGGRE2_UFS_AXI_CBCR,  CLK_ENABLE, 0);

    if ( !bAggre2USB )
      HWIO_OUTF( GCC_AGGRE2_USB3_AXI_CBCR, CLK_ENABLE, 0);
    
    if ( !bAggre2IPA )
      HWIO_OUTF( GCC_AGGRE2_NOC_IPA_CBCR,  CLK_ENABLE, 0);
  }
}
