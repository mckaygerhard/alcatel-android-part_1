/*
==============================================================================

FILE:         HALclkSYSTEMNOC.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   SYSTEMNOC clocks.

   List of clock domains:
     - HAL_clk_mGCCSYSNOCHSAXIClkDomain
     - HAL_clk_mGCCSYSTEMNOCClkDomain


   List of power domains:



==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/hal/clk/hw/msm8996/src/GCC/HALclkSYSTEMNOC.c#3 $

==============================================================================
            Copyright (c) 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;


/* ============================================================================
**    Data
** ==========================================================================*/

                                    
/*                           
 *  HAL_clk_mSYSNOCHSAXIClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mSYSNOCHSAXIClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_sys_noc_hs_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_SYS_NOC_HS_AXI_CBCR), HWIO_OFFS(GCC_SYSTEM_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_SYS_NOC_HS_AXI_CLK
  },
};


/*
 * HAL_clk_mGCCSYSNOCHSAXIClkDomain
 *
 * SYSNOCHSAXI clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCSYSNOCHSAXIClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_SYS_NOC_HS_AXI_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mSYSNOCHSAXIClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mSYSNOCHSAXIClkDomainClks)/sizeof(HAL_clk_mSYSNOCHSAXIClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mSYSNOCClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mSYSTEMNOCClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_aggre0_snoc_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_AGGRE0_SNOC_AXI_CBCR), HWIO_OFFS(GCC_AGGRE0_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_AGGRE0_SNOC_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_aggre1_snoc_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_AGGRE1_SNOC_AXI_CBCR), HWIO_OFFS(GCC_AGGRE1_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_AGGRE1_SNOC_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_aggre2_snoc_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_AGGRE2_SNOC_AXI_CBCR), HWIO_OFFS(GCC_AGGRE2_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_AGGRE2_SNOC_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_ce1_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_CE1_AXI_CBCR), HWIO_OFFS(GCC_CE1_BCR), HAL_CLK_FMSK(PROC_CLK_BRANCH_ENA_VOTE, CE1_AXI_CLK_ENA) },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_CE1_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_qdss_etr_usb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_QDSS_ETR_USB_CBCR), HWIO_OFFS(GCC_QDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_QDSS_ETR_USB_CLK
  },
  {
    /* .szClockName      = */ "gcc_smmu_aggre0_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_SMMU_AGGRE0_AXI_CBCR), HWIO_OFFS(GCC_AGGRE0_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_SMMU_AGGRE0_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_smmu_aggre1_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_SMMU_AGGRE1_AXI_CBCR), HWIO_OFFS(GCC_AGGRE1_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_SMMU_AGGRE1_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_smmu_aggre2_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_SMMU_AGGRE2_AXI_CBCR), HWIO_OFFS(GCC_AGGRE2_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_SMMU_AGGRE2_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_sys_noc_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_SYS_NOC_AXI_CBCR), HWIO_OFFS(GCC_SYSTEM_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_SYS_NOC_AXI_CLK
  },
};


/*
 * HAL_clk_mGCCSYSTEMNOCClkDomain
 *
 * SYSTEMNOC clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCSYSTEMNOCClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_SYSTEM_NOC_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mSYSTEMNOCClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mSYSTEMNOCClkDomainClks)/sizeof(HAL_clk_mSYSTEMNOCClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

void HAL_clk_EnableSNOCDCD( boolean Enable )
{
  HWIO_OUTF(GCC_SNOC_DCD_CONFIG, DCD_ENABLE, Enable);
}  




