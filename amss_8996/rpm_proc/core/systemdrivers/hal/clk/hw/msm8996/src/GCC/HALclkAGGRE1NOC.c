/*
==============================================================================

FILE:         HALclkAGGRE1NOC.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   AGGRE1NOC clocks.

   List of clock domains:


   List of power domains:
     - HAL_clk_mGCCAGGRE1NOCPowerDomain



==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/hal/clk/hw/msm8996/src/GCC/HALclkAGGRE1NOC.c#2 $

when         who     what, where, why
----------   ---     ----------------------------------------------------------- 
07/08/2014           Auto-generated.


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_clk_mGCCAGGRE1NOCPowerDomain
 *
 * AGGRE1_NOC power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mGCCAGGRE1NOCPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_AGGRE1_NOC",
  /* .nGDSCRAddr              = */ HWIO_OFFS(GCC_AGGRE1_NOC_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

void HAL_clk_AGGRE1NOCDisable (boolean Disable)
{
  /* Toggle Aggre NOCs Domain on/off. */
  HWIO_OUTF(GCC_AGGRE1_NOC_GDSCR, SW_COLLAPSE, Disable);
  if(!Disable)
  {
     while( HWIO_INF(GCC_AGGRE1_NOC_GDSCR, PWR_ON) == 0); 
  }
}

