/*
==============================================================================

FILE:         HALclkMMSS.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MMSS clocks.

   List of clock domains:
     - HAL_clk_mGCCMMSSBIMCGFXClkDomain


   List of power domains:



==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/hal/clk/hw/msm8996/src/GCC/HALclkMMSS.c#4 $

when         who     what, where, why
----------   ---     ----------------------------------------------------------- 
07/08/2014           Auto-generated.


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCNoRootClockDomainControl;
extern void HAL_clk_ConfigMMSSMISC( boolean Enable );

/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mMMSSBIMCGFXClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mMMSSBIMCGFXClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_bimc_gfx_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_BIMC_GFX_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_BIMC_GFX_CLK
  },
  {
    /* .szClockName      = */ "gcc_mmss_bimc_gfx_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_MMSS_BIMC_GFX_CBCR), HWIO_OFFS(GCC_MMSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_MMSS_BIMC_GFX_CLK
  },
};


/*
 * HAL_clk_mGCCMMSSBIMCGFXClkDomain
 *
 * MMSSBIMCGFX clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCMMSSBIMCGFXClkDomain =
{
  /* .nCGRAddr             = */ 0,
  /* .pmClocks             = */ HAL_clk_mMMSSBIMCGFXClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mMMSSBIMCGFXClkDomainClks)/sizeof(HAL_clk_mMMSSBIMCGFXClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCNoRootClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

void HAL_clk_ConfigMMSSMISC( boolean Disable )
{
  HWIO_OUTF(GCC_MMSS_MISC, GPLL0_DIV_SRC_DISABLE, Disable);
  HWIO_OUTF(GCC_MMSS_MISC, GPLL0_SRC_DISABLE, Disable);
}

