/*
==============================================================================

FILE:         HALclkRBCPRCX.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   RBCPR CX clocks.

   List of clock domains:
   -HAL_clk_mGCCRBCPRCXClkDomain


   List of power domains:



==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/hal/clk/hw/msm8996/src/GCC/HALclkRBCPRCX.c#1 $

when          who     what, where, why
----------    ---     ----------------------------------------------------------- 
06/16/2014            Auto-generated.


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;


/* ============================================================================
**    Data
** ==========================================================================*/

                                    
/*                           
 *  HAL_clk_mRBCPRCXClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mRBCPRCXClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_rbcpr_cx_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_RBCPR_CX_CBCR), HWIO_OFFS(GCC_RBCPR_CX_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_RBCPR_CX_CLK
  },
};


/*
 * HAL_clk_mGCCRBCPRCXClkDomain
 *
 * RBCPR CX clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCRBCPRCXClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_RBCPR_CX_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mRBCPRCXClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mRBCPRCXClkDomainClks)/sizeof(HAL_clk_mRBCPRCXClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


