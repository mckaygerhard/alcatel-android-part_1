/*=============================================================================

FILE:         TFunnel.c

DESCRIPTION:  

================================================================================
                Copyright 2011 QUALCOMM Technologies Incorporated.
                         All Rights Reserved.
                QUALCOMM Proprietary and Confidential
================================================================================
  $Header: //components/rel/rpm.bf/1.6/core/debugtrace/lite/src/TFunnel.c#4 $
==============================================================================*/
#include "TFunnel.h"
#include "halqdss_tfunnel.c"  //lite_inline
#include "qdss_tfunnel_config.h"

//Funnel configuration is platform specific.
//However as of now for the B-family we need only one configuration below
//to just enable STM traffic. If need to manage multiple configurations
//the below code could be separated in to platform/chipset specific directories

#define QDSS_NUM_FUNNELS  3
static uint32  qdss_funnels[QDSS_NUM_FUNNELS] = 
   {
      QDSS_IN_FUN0_BASE,
      QDSS_IN_FUN1_BASE,
      QDSS_MERG_FUN_BASE_V2
   };

#define IN_FUN0  0
#define IN_FUN1  1
#define MERG_FUN 2


__attribute__((section("qdss_cram_reclaim_pool")))
__inline void TFunnelPreInit(void)
{
   HAL_qdss_tfunnel_NumFunnels(QDSS_NUM_FUNNELS); // set num funnels
   qdss_funnels[MERG_FUN] = QDSS_MERG_FUN_BASE();
   HAL_qdss_tfunnel_MemAllocTable((uint32)qdss_funnels);
}

__attribute__((section("qdss_cram_reclaim_pool")))
__inline void TFunnelInit(void)
{
   HAL_qdss_tfunnel_UnlockAccessAll();

   //enable STM port of the in funnel for STM
   HAL_qdss_tfunnel_EnablePort(QDSS_IN_FUN_STM_PORT,QDSS_STM_IN_FUN);

   //Enable the Merge funnel port for the above in funnel
   HAL_qdss_tfunnel_EnablePort(QDSS_STM_IN_FUN,MERG_FUN);
   HAL_qdss_tfunnel_LockAccessAll();
}

#ifdef QDSS_SSC_FLUSH_HANG_WORKAROUND

static   boolean qdss_ssc_port_was_open = FALSE;

void QDSS_Close_SSC_FunnelPort(void) 
{
   HAL_qdss_tfunnel_UnlockAccess(QDSS_SSC_IN_FUN);
   if (HAL_qdss_tfunnel_isPortEnabled(QDSS_IN_FUN_SSC_PORT, QDSS_SSC_IN_FUN)) {
      qdss_ssc_port_was_open = TRUE;
      HAL_qdss_tfunnel_DisablePort(QDSS_IN_FUN_SSC_PORT,QDSS_SSC_IN_FUN);
   }
   else {
      qdss_ssc_port_was_open = FALSE;
   }
   HAL_qdss_tfunnel_LockAccess(QDSS_SSC_IN_FUN);
}


void QDSS_Open_SSC_FunnelPort(void) 
{
   if (!qdss_ssc_port_was_open) {
      return;
   }

   HAL_qdss_tfunnel_UnlockAccess(QDSS_SSC_IN_FUN);
   HAL_qdss_tfunnel_EnablePort(QDSS_IN_FUN_SSC_PORT,QDSS_SSC_IN_FUN);
   HAL_qdss_tfunnel_LockAccess(QDSS_SSC_IN_FUN);
}

#else

/*Null functions for targets that don't need the workaround */
void QDSS_Close_SSC_FunnelPort(void) {}
void QDSS_Open_SSC_FunnelPort(void)  {}

#endif




