#ifndef QDSS_TFUNNEL_CONFIG_H
#define QDSS_TFUNNEL_CONFIG_H

/*=============================================================================

FILE:         qdss_tfunnel_config.h

DESCRIPTION:  

================================================================================
              Copyright (c) 2014 Qualcomm Technologies Incorporated.
                         All Rights Reserved.
                QUALCOMM Proprietary and Confidential
==============================================================================*/
#include "qdss_chip_version.h"


#define QDSS_IN_FUN0_BASE  (0x63021000)
#define QDSS_IN_FUN1_BASE  (0x63022000)

#define QDSS_MERG_FUN_BASE_V1 (0x63023000)
#define QDSS_MERG_FUN_BASE_V2 (0x63025000)


static __inline  uint32 QDSS_MERG_FUN_BASE(void) 
{
   if (qdss_is_8996_v1()) {
      return QDSS_MERG_FUN_BASE_V1;
   }
   else {
      return QDSS_MERG_FUN_BASE_V2;
   }
}



#define QDSS_STM_IN_FUN            0
#define QDSS_IN_FUN_STM_PORT       7


//The below is needed to workaround an SSC HW bug.
//comment the below line out if this is not needed in another target.
#define QDSS_SSC_FLUSH_HANG_WORKAROUND 1


#define QDSS_SSC_IN_FUN                1
#define QDSS_IN_FUN_SSC_PORT           3

#endif //QDSS_TFUNNEL_CONFIG_H

