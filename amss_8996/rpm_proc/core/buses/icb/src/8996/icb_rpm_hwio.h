#ifndef __ICB_RPM_HWIO_H__
#define __ICB_RPM_HWIO_H__
/*
===========================================================================
*/
/**
  @file icb_rpm_hwio.h

  @brief Additional HWIO definitions for ICB RPM driver.
*/
/*
===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

===========================================================================

  $Header: //components/rel/rpm.bf/1.6/core/buses/icb/src/8996/icb_rpm_hwio.h#2 $
  $DateTime: 2014/10/09 14:35:16 $
  $Author: pwbldsvc $

===========================================================================
*/
#ifndef A1_NOC_AGGRE1_NOC_BASE
#define A1_NOC_AGGRE1_NOC_BASE  0x560000
#endif /* A1_NOC_AGGRE1_NOC_BASE */

#ifndef A2_NOC_AGGRE2_NOC_BASE
#define A2_NOC_AGGRE2_NOC_BASE  0x580000
#endif /* A2_NOC_AGGRE2_NOC_BASE */

#endif /* __ICB_RPM_HWIO_H__ */
