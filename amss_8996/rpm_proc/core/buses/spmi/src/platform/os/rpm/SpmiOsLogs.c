/**
 * @file:  SpmiOsLogs.c
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2014/07/30 21:54:31 $
 * $Header: //components/rel/rpm.bf/1.6/core/buses/spmi/src/platform/os/rpm/SpmiOsLogs.c#1 $
 * $Change: 6345305 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */

#include "SpmiOsLogs.h"

//******************************************************************************
// Global Data
//******************************************************************************

ULogHandle spmiLogHandle;
