/**
 * @file:  PmicArbConstants.h
 * @brief: 
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/05/13 12:32:11 $
 * $Header: //components/rel/rpm.bf/1.6/core/buses/spmi/src/core/hal/bear/PmicArbConstants.h#3 $
 * $Change: 8121576 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 11/25/13 Initial Version
 */
#ifndef PMICARBCONSTANTS_H
#define	PMICARBCONSTANTS_H

#include "PmicArbCoreHwio.h"

#define PMIC_ARB_MAX_PERIPHERAL_SUPPORT (HWIO_PMIC_ARBq_CHNLn_CMD_MAXn + 1)

#endif

