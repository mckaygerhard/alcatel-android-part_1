/*=============================================================================

  FILE:     i2c_slpi_8996.c

  OVERVIEW: This file has the devices for 8996 platform for slpi. 
 
Copyright (c) 2009-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.

  ===========================================================================*/

/*=========================================================================
  EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.slpi/1.0/buses/i2c/config/i2c_ssc_8996.c#5 $
  $DateTime: 2015/07/22 17:36:16 $$Author: pwbldsvc $

  When     Who    What, where, why
  -------- ---    -----------------------------------------------------------
  25/03/11 LK     Removed gsbi11,gsbi12 since they are dedicated to sps.
  12/03/09 LK     Created.

  ===========================================================================*/

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/

#include "I2cDriverTypes.h"
#include "I2cPlatSvc.h"
#include "I2cDevice.h"

#define I2C_NUM_PLATFORM_DEVICES         (2)


/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/

const uint32              i2cDeviceNum = I2C_NUM_PLATFORM_DEVICES;

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/


#ifdef BUILD_FOR_ISLAND
#define ATTRIBUTE_ISLAND_CODE __attribute__((section("RX.island")))
#define ATTRIBUTE_ISLAND_CONST __attribute__((section("RO.island")))
#define ATTRIBUTE_ISLAND_DATA __attribute__((section("RW.island")))
#else
#define ATTRIBUTE_ISLAND_CODE /* empty */
#define ATTRIBUTE_ISLAND_CONST /* empty */
#define ATTRIBUTE_ISLAND_DATA /* empty */
#endif


/*-------------------------------------------------------------------------
 * Global Data Definitions
 * ----------------------------------------------------------------------*/
I2cPlat_PropertyType i2cPlatPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{
   I2CPLAT_PROPERTY_INIT(0x20004021,0x20004031,2,"SSC",0x6B7000,CLOCK_SCC_BLSP_H_CLK,CLOCK_SCC_QUP_I2C3_CLK), //SSC_2, SSC_3, Fn select 1, QUP 2 (index 0)
   I2CPLAT_PROPERTY_INIT(0x20004041,0x20004041,2,"SSC",0x6B6000,CLOCK_SCC_BLSP_H_CLK,CLOCK_SCC_QUP_I2C2_CLK), //SSC_4, SSC_5, Fn select 1, QUP 1
   // I2CPLAT_PROPERTY_INIT(0x20004081,0x20004091,2,"SSC",0x6B5000,CLOCK_SCC_BLSP_H_CLK,CLOCK_SCC_QUP_I2C1_CLK), //SSC_8, SSC_9, Fn select 1, QUP 0
   
};

I2cDev_PropertyType i2cDevPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{
   I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_CHIP_VER_BASED),
   I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_CHIP_VER_BASED),
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_25770_KHZ),
};

I2cDrv_DriverProperty i2cDrvPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{
   I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_3, FALSE, 0),
   I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_2, FALSE, 0),
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_1, FALSE, 0),
};

I2cDrv_DescType i2cDrvDescArray[I2C_NUM_PLATFORM_DEVICES] ATTRIBUTE_ISLAND_DATA ;


