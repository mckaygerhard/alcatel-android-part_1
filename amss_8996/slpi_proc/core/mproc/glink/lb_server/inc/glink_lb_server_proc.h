#ifndef GLINK_LB_SERVER_PROC_H
#define GLINK_LB_SERVER_PROC_H

/**
 * @file glink_lb_server_proc.h
 *
 * Declares the process specific functions and macros for the 
 * G-LINK Loopback Server. 
 */

/** \defgroup glink_lb_server GLINK LB SERVER
 * \ingroup MPROC
 *
 * Provides all processor specific functions for the G-LINK 
 * Loopback Server. 
 *
 */
/*@{*/

/*==============================================================================
     Copyright (c) 2015 Qualcomm Technologies Incorporated. 
     All rights reserved.
     Qualcomm Confidential and Proprietary
==============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.slpi/1.0/mproc/glink/lb_server/inc/glink_lb_server_proc.h#4 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/30/15   bc      Initial revision
===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================

                        INCLUDE FILES

===========================================================================*/


/*===========================================================================

                      MACRO DECLARATIONS

===========================================================================*/

#define GLINK_LB_SERVER_THIS_SUBSYSTEM        "dsps"
#define GLINK_LB_SERVER_CTRL_CHANNEL_NAME     "LOOPBACK_CTL_DSPS"



/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/


/*===========================================================================

                        PUBLIC VARIABLE DECLARATIONS

===========================================================================*/

/*===========================================================================

                        PUBLIC FUNCTION DECLARATIONS

===========================================================================*/


#endif   /* GLINK_LB_SERVER_PROC_H */
