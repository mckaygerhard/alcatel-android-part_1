/*
==============================================================================

FILE:         HALclkMain.c

DESCRIPTION:
  This file contains the main platform initialization code for the clock
  HAL on the Sensors Subsystem Core (SSC) processor on MSM8996.

==============================================================================

                             Edit History

$Header: //components/rel/core.slpi/1.0/systemdrivers/hal/clk/hw/msm8996/src/HALclkMain.c#6 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
07/11/14   dcf     Created.

==============================================================================
Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include "HALclkInternal.h"
#include "HALclkGeneric.h"
#include "HALclkGenericPLL.h"
#include "HALhwio.h"
#include "HALclkHWIO.h"


/* ============================================================================
**    Prototypes
** ==========================================================================*/

void HAL_clk_PlatformInitSources(void);


/* ============================================================================
**    Externs
** ==========================================================================*/

extern void HAL_clk_PlatformInitGCCMain(void);
extern void HAL_clk_PlatformInitSSCMain(void);


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_clk_aInitFuncs
 *
 * Declare array of module initialization functions.
 */
static HAL_clk_InitFuncType HAL_clk_afInitFuncs[] =
{
  /*
   * Sources
   */
  HAL_clk_PlatformInitSources,
  
  /*
   *
   */
  HAL_clk_PlatformInitGCCMain,
  HAL_clk_PlatformInitSSCMain,

  NULL
};


/*
 * Declare the base pointers for HWIO access.
 */
uint32 HAL_clk_nHWIOBaseTop;
uint32 HAL_clk_nHWIOBaseSSC;
uint32 HAL_clk_nHWIOBaseTCSR;


/*
 * HAL_clk_aHWIOBases
 *
 * Declare array of HWIO bases in use on this platform.
 */
static HAL_clk_HWIOBaseType HAL_clk_aHWIOBases[] =
{
  { CLK_CTL_BASE_PHYS,      CLK_CTL_BASE_SIZE,      &HAL_clk_nHWIOBaseTop },
  { SSC_BASE_PHYS,          SSC_BASE_SIZE,          &HAL_clk_nHWIOBaseSSC },
  { CORE_TOP_CSR_BASE_PHYS, CORE_TOP_CSR_BASE_SIZE, &HAL_clk_nHWIOBaseTCSR },
  { 0, 0, NULL }
};


/*
 * HAL_clk_Platform;
 * Platform data.
 */
HAL_clk_PlatformType HAL_clk_Platform =
{
  HAL_clk_afInitFuncs,
  HAL_clk_aHWIOBases
};


/*
 * GPLL contexts
 */
static HAL_clk_PLLContextType HAL_clk_aPLLContextGPLL[] =
{
  {
    HWIO_OFFS(GCC_GPLL0_MODE),
    HAL_CLK_FMSK(GCC_SSC_GPLL_ENA_VOTE, GPLL0),
    HAL_CLK_PLL_PRIUS
  }
};


/*
 * SSC PLL contexts - voting is support.
 */

static HAL_clk_PLLContextType HAL_clk_aPLLContextSSCPLL[] =
{
  {
    HWIO_OFFS(SSC_SCC_SCC_PEEL_PLL_MODE),
    {0, 0},
    HAL_CLK_PLL_PEEL,
    
  },
  {
    HWIO_OFFS(SSC_QDSP6SS_PLL_MODE),
    {0, 0},
    HAL_CLK_PLL_SPARK,
  },
};


/* ============================================================================
**    Functions
** ==========================================================================*/


/* ===========================================================================
**  HAL_clk_PlatformInitSources
**
** ======================================================================== */

void HAL_clk_PlatformInitSources (void)
{
  /*
   * Install PLL handlers.
   */
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL0, &HAL_clk_aPLLContextGPLL[0], CLK_CTL_BASE);

  /*
   * Install the SSC PLL handlers.
   */

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_SSCPLL0, &HAL_clk_aPLLContextSSCPLL[0], SSC_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_SSCPLL1, &HAL_clk_aPLLContextSSCPLL[1], SSC_BASE);


} /* END HAL_clk_PlatformInitSources */


/* ===========================================================================
**  HAL_clk_Save
**
** ======================================================================== */

void HAL_clk_Save (void)
{
  /*
   * Nothing to save.
   */

} /* END HAL_clk_Save */


/* ===========================================================================
**  HAL_clk_Restore
**
** ======================================================================== */

void HAL_clk_Restore (void)
{
  /*
   * Nothing to restore.
   */
  
} /* END HAL_clk_Restore */

