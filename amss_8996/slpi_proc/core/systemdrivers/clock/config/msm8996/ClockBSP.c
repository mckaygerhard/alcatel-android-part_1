/*
==============================================================================

FILE:         ClockBSP.c

DESCRIPTION:
  This file contains clock regime bsp data for DAL based driver.

==============================================================================

                             Edit History

$Header: //components/rel/core.slpi/1.0/systemdrivers/clock/config/msm8996/ClockBSP.c#20 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------- 
10/19/14   dcf     Created. 

==============================================================================
            Copyright (c) 2014 - 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBSP.h"
#include "ClockSSCCPU.h"
#include "comdef.h"

/*=========================================================================
      Data Declarations
==========================================================================*/

/*
 *  SourceFreqConfig_SLEEPCLK
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_SLPCLK[] =
{
  {
    .nFreqHz    = 32768,
    .HALConfig  = { HAL_CLK_SOURCE_NULL },
    .eVRegLevel = VCS_CORNER_OFF,
    .HWVersion  = { {0, 0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_XO
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_XO[] =
{
  {
    .nFreqHz    = 19200 * 1000,
    .HALConfig  = { HAL_CLK_SOURCE_NULL },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion  = { {0, 0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_GPLL0
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_GPLL0[] =
{
  {
    /* .nFreqHz    = */  600000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  31,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x40,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_LPAPLL0
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_SSC_PEEL_PLL[] =
{
  {
    /* .nFreqHz    = */  231997440,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_SLEEPCLK,
      /* .eVCO           = */  HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  7080,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0xFF, 0xFF} },
  },
  {
    /* .nFreqHz    = */  249987072,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_SLEEPCLK,
      /* .eVCO           = */  HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  7629,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x0, 0x00}, {0xFF, 0xFF} },
  },

  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_SSC_SPARK_PLL
 *
 *  Set of source frequency configurations.
 */

static ClockSourceFreqConfigType SourceFreqConfig_SSC_SPARK_PLL[] =
{
  {
    /* .nFreqHz    = */  549978112,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_SLEEPCLK,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  16784,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0,
      /* .nAlphaU        = */  0x0,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0xFF, 0xFF} },
  },

  /* Calibration Frequency for V3 */
  {
    /* .nFreqHz    = */  624984064,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_SLEEPCLK,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  19073,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0,
      /* .nAlphaU        = */  0x0,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x0, 0x00}, {0xFF, 0xFF} },
  },

  {
    /* .nFreqHz    = */  699990016,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_SLEEPCLK,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  21362,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0,
      /* .nAlphaU        = */  0x0,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0xFF, 0xFF} },
  },
  {
    /* .nFreqHz    = */  899940352,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_SLEEPCLK,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  27464,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x0,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 * Extra calibration frequency for V1 and V2.
 */
const ClockSourceFreqConfigType CPUCalibrationFreqV2 =
{
    /* .nFreqHz    = */  799997952,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_SLEEPCLK,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  24414,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0,
      /* .nAlphaU        = */  0x0,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x0, 0x00}, {0x3, 0x0} },
};



/*
 *  ClockSourcesToInit
 *
 *  Array of sources and settings to initialize at runtime.
 */
const ClockSourceInitType ClockSourcesToInit[] =
{
   { HAL_CLK_SOURCE_NULL,    NULL}
};


/*
 * Clock source configuration data.
 */
const ClockSourceConfigType SourceConfig[] =
{
  {
    SOURCE_NAME(SLEEPCLK),

    .nConfigMask            = 0,
    .pSourceFreqConfig      = SourceFreqConfig_SLPCLK,
  },
  {
    SOURCE_NAME(XO),

    .nConfigMask            = 0,
    .pSourceFreqConfig      = SourceFreqConfig_XO,
  },
  {
    SOURCE_NAME(GPLL0),

    .nConfigMask            = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    .pSourceFreqConfig      = SourceFreqConfig_GPLL0,
  },
  {
  SOURCE_NAME(SSCPLL0),

    .nConfigMask            = 0,
    .pSourceFreqConfig      = SourceFreqConfig_SSC_PEEL_PLL,
  },
  {
  SOURCE_NAME(SSCPLL1),

    .nConfigMask            = CLOCK_CONFIG_PLL_AUX_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_AUX2_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
    .pSourceFreqConfig      = SourceFreqConfig_SSC_SPARK_PLL,
    .pCalibrationFreqConfig = &SourceFreqConfig_SSC_SPARK_PLL[1],
    .eDisableMode           = HAL_CLK_SOURCE_DISABLE_MODE_FREEZE

  },

  /* last entry */
  { SOURCE_NAME(NULL) }

};


/* =========================================================================
**    nFreqHz       { eSource, nDiv2x, nM, nN, n2D },      eVRegLevel         
** =========================================================================*/

/*----------------------------------------------------------------------*/
/* SSC   Clock Configurations                                           */
/*----------------------------------------------------------------------*/


/*
 * SCCSCC100M clock configurations
 */
const ClockMuxConfigType SCC100MClockConfig[] =
{
  {   25777493, { HAL_CLK_SOURCE_SSCPLL0,        18,      0,      0,      0       }, VCS_CORNER_LOW_MINUS, { {0x00, 0x00}, {0x03, 0x00}, DALCHIPINFO_FAMILY_MSM8996   }  },
  {   99994829, { HAL_CLK_SOURCE_SSCPLL0,         5,      0,      0,      0       }, VCS_CORNER_LOW_MINUS, { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }  },
  {   99994829, { HAL_CLK_SOURCE_SSCPLL0,         5,      0,      0,      0       }, VCS_CORNER_LOW_MINUS, { {0x03, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996   }  },
  { 0 }
};


/*
 * BLSP1QUP1I2CAPPS clock configurations
 */
const ClockMuxConfigType BLSP1QUP1I2CAPPSClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, VCS_CORNER_LOW,         },
  { 0 }
};


/*
 * BLSP1QUP1SPIAPPS clock configurations
 */
const ClockMuxConfigType BLSP1QUP1SPIAPPSClockConfig[] =
{
  {     960000, { HAL_CLK_SOURCE_XO,             20,     1,      2,      2       }, VCS_CORNER_LOW_MINUS,   },
  {    4800000, { HAL_CLK_SOURCE_XO,             8,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {    9600000, { HAL_CLK_SOURCE_XO,             4,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   15000000, { HAL_CLK_SOURCE_GPLL0,          20,     1,      4,      4       }, VCS_CORNER_LOW,         },
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   25000000, { HAL_CLK_SOURCE_GPLL0,          24,     1,      2,      2       }, VCS_CORNER_LOW,         },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  { 0 }
};


/*
 * BLSP1UART1APPS clock configurations
 */
const ClockMuxConfigType BLSP1UART1APPSClockConfig[] =
{
  {    3686400, { HAL_CLK_SOURCE_GPLL0,          2,      96,     15625,  15625   }, VCS_CORNER_LOW,         },
  {    7372800, { HAL_CLK_SOURCE_GPLL0,          2,      192,    15625,  15625   }, VCS_CORNER_LOW,         },
  {   14745600, { HAL_CLK_SOURCE_GPLL0,          2,      384,    15625,  15625   }, VCS_CORNER_LOW,         },
  {   16000000, { HAL_CLK_SOURCE_GPLL0,          10,     2,      15,     15      }, VCS_CORNER_LOW,         },
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   24000000, { HAL_CLK_SOURCE_GPLL0,          10,     1,      5,      5       }, VCS_CORNER_LOW,         },
  {   32000000, { HAL_CLK_SOURCE_GPLL0,          2,      4,      75,     75      }, VCS_CORNER_NOMINAL,     },
  {   40000000, { HAL_CLK_SOURCE_GPLL0,          30,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  {   46400000, { HAL_CLK_SOURCE_GPLL0,          2,      29,     375,    375     }, VCS_CORNER_NOMINAL,     },
  {   48000000, { HAL_CLK_SOURCE_GPLL0,          25,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  {   51200000, { HAL_CLK_SOURCE_GPLL0,          2,      32,     375,    375     }, VCS_CORNER_NOMINAL,     },
  {   56000000, { HAL_CLK_SOURCE_GPLL0,          2,      7,      75,     75      }, VCS_CORNER_NOMINAL,     },
  {   58982400, { HAL_CLK_SOURCE_GPLL0,          2,      1536,   15625,  15625   }, VCS_CORNER_NOMINAL,     },
  {   60000000, { HAL_CLK_SOURCE_GPLL0,          20,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  {   63157895, { HAL_CLK_SOURCE_GPLL0,          19,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  { 0 }
};


/*
 * Clock Log Default Configuration.
 *
 * NOTE: An .nGlobalLogFlags value of 0x12 will log only clock frequency
 *       changes and source state changes by default.
 */
const ClockLogType ClockLogDefaultConfig[] =
{
  {
    /* .nLogSize        = */ 4096,
    /* .nGlobalLogFlags = */ 0x12
  }
};


/*
 * Clock Flag Init Config.
 */
const ClockFlagInitType ClockFlagInitConfig[] =
{
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"ssc_q6core",               CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"scc_cfg_ahb_clk",          CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"scc_crif_clk",             CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"scc_csr_h_clk",            CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"scc_data_h_clk",           CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"scc_q6_ahbm_clk",          CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"scc_q6_ahbs_clk",          CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"scc_smem_clk",             CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"scc_ahb_timeout_clk",      CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_NONE,         (void*)0,                          0                       } 
};

/*
 * Resources that need to be published to domains outside GuestOS.
 */
static const char *ClockPubResource[] =
{
   CLOCK_NPA_RESOURCE_QDSS
};

ClockNPAResourcePubType ClockResourcePub = 
{
   SENSOR_PD,
   ClockPubResource,
   1
};

/*
 * Initial CX voltage level.
 */
const VCSCornerType CXVRegInitLevelConfig[] =
{
  VCS_CORNER_NOMINAL
};


const HAL_clk_HWIOBaseType ClockHWIOBaseConfig =
{
  0x01800000,  /* SSC_BASE */
  0x00800000   /* SSC_SIZE */
};



/*=========================================================================
      Type Definitions and Macros
==========================================================================*/


/*=========================================================================
      Type Definitions
==========================================================================*/


/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 * Mux configuration for different CPU frequencies. 
 */
static ClockCPUConfigType Clock_Q6Config [] =
{
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   19200 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW_MINUS,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_XO[0]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCD,
    /* .Mux               */ {
    /* .nFreq             */   115998720,
    /* .HALConfig         */   { HAL_CLK_SOURCE_SSCPLL0, 4, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW_MINUS,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_SSC_PEEL_PLL[0]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCD,
    /* .Mux               */ {
    /* .nFreq             */   231997440,
    /* .HALConfig         */   { HAL_CLK_SOURCE_SSCPLL0, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_SSC_PEEL_PLL[0]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   349995008,
    /* .HALConfig         */   { HAL_CLK_SOURCE_SSCPLL1, 4, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_NOMINAL,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_SSC_SPARK_PLL[1]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   449970176,
    /* .HALConfig         */   { HAL_CLK_SOURCE_SSCPLL1, 4, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_TURBO,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_SSC_SPARK_PLL[3]
                             },
    /* .nStrapACCVal      */ 0x00000020
  }
};


/*
 * Mux configuration for different CPU frequencies. 
 */
static ClockCPUConfigType Clock_Q6ConfigV3 [] =
{
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   19200 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW_MINUS,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_XO[0]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCD,
    /* .Mux               */ {
    /* .nFreq             */   124977152,
    /* .HALConfig         */   { HAL_CLK_SOURCE_SSCPLL0, 4, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW_MINUS,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_SSC_PEEL_PLL[1]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCD,
    /* .Mux               */ {
    /* .nFreq             */   249987072,
    /* .HALConfig         */   { HAL_CLK_SOURCE_SSCPLL0, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_SSC_PEEL_PLL[1]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   549978112,
    /* .HALConfig         */   { HAL_CLK_SOURCE_SSCPLL1, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_NOMINAL,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_SSC_SPARK_PLL[0]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   699990016,
    /* .HALConfig         */   { HAL_CLK_SOURCE_SSCPLL1, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_TURBO,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_SSC_SPARK_PLL[2]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
};


/*
 * Voltage warmup table mapping used for coming out of retention.
 */
const Clock_VRegWarmUpType Clock_VRegWarmupTable[VCS_CORNER_NUM_OF_CORNERS] =
{
  {VCS_CORNER_OFF,          0},
  {VCS_CORNER_RETENTION,    375000},
  {VCS_CORNER_LOW_MINUS,    675000},
  {VCS_CORNER_LOW,          762500},
  {VCS_CORNER_LOW_PLUS,     900000},
  {VCS_CORNER_NOMINAL,      900000},
  {VCS_CORNER_NOMINAL_PLUS, 985000},
  {VCS_CORNER_TURBO,        985000}
};


/*
 * Enumeration of CPU performance levels.  More performance 
 * levels exist here than are actually implemented.
 */
enum
{
  CLOCK_CPU_PERF_LEVEL_0,
  CLOCK_CPU_PERF_LEVEL_1,
  CLOCK_CPU_PERF_LEVEL_2,
  CLOCK_CPU_PERF_LEVEL_3,
  CLOCK_CPU_PERF_LEVEL_4,

  CLOCK_CPU_PERF_LEVEL_TOTAL
};


/*
 * Enumeration of generic CPU performance levels.  More performance 
 * levels exist here than are actually implemented.
 */
static const uint32 Clock_Q6PerfLevels[] =
{
  CLOCK_CPU_PERF_LEVEL_0,
  CLOCK_CPU_PERF_LEVEL_1,
  CLOCK_CPU_PERF_LEVEL_2,
  CLOCK_CPU_PERF_LEVEL_3,
  CLOCK_CPU_PERF_LEVEL_4,
};


/*
 * Performance level configuration data for the Q6 clock.
 */
static ClockCPUPerfConfigType Clock_Q6PerfConfig[] =
{
  {
    .HWVersion      = {{0, 0}, {0xFF, 0xFF}},
    .nMinPerfLevel  = CLOCK_CPU_PERF_LEVEL_1,
    .nMaxPerfLevel  = CLOCK_CPU_PERF_LEVEL_4,
    .anPerfLevel    = (uint32*)Clock_Q6PerfLevels,
    .nNumPerfLevels = ARR_SIZE(Clock_Q6PerfLevels)
  },
};


/*
 * Image BSP data
 */
const ClockImageBSPConfigType ClockImageBSPConfig[] =
{
  {
    .HWVersion               = {{0, 0}, {0x3, 0x0}},
    .bEnableDCS              = TRUE,
    .pCPUConfig              = Clock_Q6Config,
    .pCPUPerfConfig          = Clock_Q6PerfConfig,
    .nNumCPUPerfLevelConfigs = ARR_SIZE(Clock_Q6PerfConfig),
  },
  {
    .HWVersion               = {{0x3, 0}, {0xFF, 0xFF}},
    .bEnableDCS              = TRUE,
    .pCPUConfig              = Clock_Q6ConfigV3,
    .pCPUPerfConfig          = Clock_Q6PerfConfig,
    .nNumCPUPerfLevelConfigs = ARR_SIZE(Clock_Q6PerfConfig),
  }
};

