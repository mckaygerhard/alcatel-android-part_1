#ifndef __DALTLMMPROPDEF_H__
#define __DALTLMMPROPDEF_H__
#include "DALStdDef.h" 
/*
============================================================================

                 D A L T L M M   S T A T E   S T R U C T U R E
                             H E A D E R   F I L E

FILE:         DALTLMMDefines.h

DESCRIPTION:  
              This file contains definitions for use solely with the
              DALTLMM Properties XML file.  It's use is not recommended by
              any drivers and must not be used in any DALTLMM APIs.

============================================================================

                             Edit History

$Header: //components/rel/core.slpi/1.0/systemdrivers/tlmm/config/DALTLMMPropDef.h#2 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------
07/30/10   dcf     Created the initial revision. 
 
============================================================================
             Copyright (c) 2010 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
============================================================================
*/


/*==========================================================================

                     DEFINITIONS FOR THIS MODULE

==========================================================================*/


/*
 * TLMMGpioIdType
 *
 * GPIO Identifier for configuring a GPIO based on device config data for
 * a particular platform.
 *
 *  nGpioNumber:     GPIO Pin Number.
 *  nFunctionSelect: The function select associated with this GPIO.
 */
typedef struct {  
   uint32 nGpioNumber;
   uint32 nFunctionSelect;
}TLMMGpioIdType;


#endif /* __DALTLMMPROPDEF_H__ */

