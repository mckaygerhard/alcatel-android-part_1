/*==============================================================================

FILE:      GPIOInt_SummaryKernel.c

DESCRIPTION
  This modules implements the API to utilize the Dal GPIO interrupt controller. 
  The definitions in this module are implemented internally and should not be 
  exposed for external client usage directly without using the accompanying DDI
  interface file for this module.

REFERENCES

       Copyright � 2015 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
==============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.slpi/1.0/systemdrivers/GPIOInt/src/kernel/GPIOInt_SummaryKernel.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
3/18/2015  cpaulo  First draft created. 
===========================================================================*/
#include "DalDevice.h"
#include "DALDeviceId.h"
#include "DALStdErr.h"
#include "DDITimetick.h"
#include "DDIGPIOMgr.h"
#include "DDITlmm.h"
#include "busywait.h"
#include "DDIInterruptController.h"
#include "HALgpioint.h"
#include "DALGlbCtxt.h"
#include "GPIOInt.h"
#include "GPIOInt_Summary.h"
#include "DDIHWIO.h"
#include "uGPIOInt.h"
#include "uGPIOIntKernel.h"
#include "qurt.h"
#include "GPIOIntQDI.h"

static DalDeviceHandle* intr_handle = NULL;


/*==========================================================================

  FUNCTION      GPIOIntr_Isr

  DESCRIPTION   See GPIOInt.h

==========================================================================*/

void 
GPIOIntr_Isr
(
  void *pCtxt
)
{
  uint32 gpio, group;
  uint32 nPD = 0;
  GPIOIntDevCtxt *device = (GPIOIntDevCtxt *)pCtxt;
  

  (void)DALSYS_SyncEnter(device->gpioint_synchronization);

  /*
   * Retrieve the interrupt from the hardware
   */
  group = device->gpioint_cntrl->summary_param;
  HAL_gpioint_GetPending((HAL_gpioint_GroupType)group, &gpio);

  /*
   * If nothing pending we are done
   */
  while (gpio != HAL_GPIOINT_NONE)
  {
    if ( DAL_SUCCESS != DalGPIOMgr_GetGPIOCurrentPD( device->hGPIOMgr, gpio, &nPD ) )
    {
      /* TODO: Handle error case */
      return;
    }

    if (nPD == 0) 
    {
      /*
       * Run the ISR
       */
      GPIOIntr_RunIsr(device,gpio);
    }
    else
    {
      /*
       * Send the GPIO Interrupt to the correct PD
       */
      GPIOIntQDI_SetSummarySignal(nPD, gpio);

      /*
       * Wait for Ack from User PD
       */
      GPIOIntQDI_WaitOnAckSummarySignal(nPD);
    }

    /* 
     *  Clear the interrupt from the TLMM after it's handled 
     */
    HAL_gpioint_Clear(gpio);

    /*
     * Retrieve the next interrupt from the hardware
     */
    HAL_gpioint_GetPending((HAL_gpioint_GroupType)group, &gpio);
  }
  
  (void)DALSYS_SyncLeave(device->gpioint_synchronization);

} /* END GPIOIntr_isr */


/*==========================================================================

  FUNCTION      GPIOIntr_LogEvent

  DESCRIPTION   See GPIOInt.h

==========================================================================*/

void 
GPIOIntr_LogEvent
(
  GPIOIntDevCtxt *device,
  uint32 gpio
)
{
#ifdef GPIOINT_KERNELPDLOG
  GPIOIntCntrlType *gpioint_cntrl = device->gpioint_cntrl;
  uint32 index = gpioint_cntrl->log.index;
  gpioint_cntrl->log.entries[index].gpio = gpio;
  DalTimetick_GetTimetick64(device->timetick_handle, &gpioint_cntrl->log.entries[index].timestamp);

  gpioint_cntrl->state[gpio].last =
  gpioint_cntrl->log.entries[index].timestamp;
  gpioint_cntrl->state[gpio].cnt++;

  if (++gpioint_cntrl->log.index >= MAX_INTERRUPT_LOG_ENTRIES)
  {
    gpioint_cntrl->log.index = 0;
  }
#endif
} /* END GPIOIntr_log_event */


/*==========================================================================

  FUNCTION      GPIOIntr_DefaultIsr

  DESCRIPTION   See GPIOInt.h

==========================================================================*/

void 
GPIOIntr_DefaultIsr
(
  GPIOIntDevCtxt *device,
  uint32 gpio
)
{
  (void)DALSYS_SyncEnter(device->gpioint_synchronization);
  device->gpioint_cntrl->state[gpio].unhandled++ ;
  HAL_gpioint_Disable(gpio);
  (void)DALSYS_SyncLeave(device->gpioint_synchronization);
} /* END GPIOIntdefault_isr */


/*==========================================================================

  FUNCTION      GPIOIntr_RunIsr

  DESCRIPTION   See GPIOInt.h

==========================================================================*/

void 
GPIOIntr_RunIsr
(
  GPIOIntDevCtxt *device,
  uint32 gpio
)
{ 
  boolean bIsEdgeTriggered = TRUE;
  HAL_gpioint_TriggerType nTrigger = HAL_GPIOINT_TRIGGER_HIGH;

  /* 
   * Get the trigger type to determine when to clear the interrupt
   */
  HAL_gpioint_GetTrigger( gpio, &nTrigger );

  if( nTrigger == HAL_GPIOINT_TRIGGER_HIGH ||
      nTrigger == HAL_GPIOINT_TRIGGER_LOW )
  {
    bIsEdgeTriggered = FALSE;
  }

  /*
   * Log the GPIO that fired
   */
  GPIOIntr_LogEvent(device,gpio);

  /*
   * Clear the interrupt before if it's edge triggered
   */
  if( bIsEdgeTriggered )
  {
    HAL_gpioint_Clear(gpio);
  }

  (void)DALSYS_SyncLeave(device->gpioint_synchronization);
  /*
   * Get the ISR and run it
   */
  if((device->gpioint_cntrl->state[gpio].event == NULL)&&(device->gpioint_cntrl->state[gpio].isr == NULL))
  {
    GPIOIntr_DefaultIsr(device,gpio);
  }
  else if(device->gpioint_cntrl->state[gpio].event)
  {
    DALSYS_EventCtrl(device->gpioint_cntrl->state[gpio].event, DALSYS_EVENT_CTRL_TRIGGER);
  }
  else 
  {
    device->gpioint_cntrl->state[gpio].isr(device->gpioint_cntrl->state[gpio].isr_param);
  }
  (void)DALSYS_SyncEnter(device->gpioint_synchronization);
  /*
   * Clear the gpio interrupt if it is not owned by the apps processor.
   * During power collapse, the apps interrupt will be preserved to trigger
   * an apps interrupt once gpios are switched back to the apps processor.
   * The apps GPIO monitoring ISR (GPIOIntr_monitor_apps_isr) has disabled
   * the interrupt at this point. 
   * Clear the interrupt after the ISR is called if it's level triggered. 
   */
  if( !bIsEdgeTriggered )
  {
    HAL_gpioint_Clear(gpio);
  }

} /* END GPIOIntr_run_isr */

DALResult
GPIOIntrSummaryInternal_Init(GPIOIntDevCtxt *device, DalDeviceHandle* pInterruptHandle)
{
  DALResult eResult;
  intr_handle = pInterruptHandle;

  eResult = DalInterruptController_RegisterISR(device->intr_handle, 
                   device->gpioint_cntrl->summary_intr_id,
                   (DALISR)GPIOIntr_Isr, (DALISRCtx)device, DALINTRCTRL_ENABLE_LEVEL_HIGH_TRIGGER);


  /* Initialize QDI server */
  GPIOIntQDI_Init();

  return eResult;
}


DALResult
GPIOIntrSummaryInternal_UnConfigureGPIOInterrupt( uint32 gpio, uint8 uimage )
{
  if(uimage != 0)
  {
    uGPIOInt_ClearDirectConnectGPIOMapping(gpio);
  }
  return DAL_SUCCESS;
}

