#ifndef __MSMHWIOBASE_H__
#define __MSMHWIOBASE_H__
/*
===========================================================================
*/
/**
  @file msmhwiobase.h
  @brief Auto-generated HWIO base include file.
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.slpi/1.0/api/systemdrivers/hwio/msm8996/msmhwiobase.h#5 $
  $DateTime: 2014/12/10 10:09:58 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * BASE: SPDM_WRAPPER_TOP
 *--------------------------------------------------------------------------*/

#define SPDM_WRAPPER_TOP_BASE                                       0xe0048000
#define SPDM_WRAPPER_TOP_BASE_SIZE                                  0x00006000
#define SPDM_WRAPPER_TOP_BASE_PHYS                                  0x00048000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_START_ADDRESS_BASE                           0xe0168000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_SIZE                      0x00006000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_PHYS                      0x00068000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_END_ADDRESS_BASE_PHYS                        0x0006ffff

/*----------------------------------------------------------------------------
 * BASE: SECURITY_CONTROL
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_BASE                                       0xe0270000
#define SECURITY_CONTROL_BASE_SIZE                                  0x00010000
#define SECURITY_CONTROL_BASE_PHYS                                  0x00070000

/*----------------------------------------------------------------------------
 * BASE: RPM
 *--------------------------------------------------------------------------*/

#define RPM_BASE                                                    0xe0300000
#define RPM_BASE_SIZE                                               0x00090000
#define RPM_BASE_PHYS                                               0x00200000

/*----------------------------------------------------------------------------
 * BASE: CLK_CTL
 *--------------------------------------------------------------------------*/

#define CLK_CTL_BASE                                                0xe0400000
#define CLK_CTL_BASE_SIZE                                           0x000a0000
#define CLK_CTL_BASE_PHYS                                           0x00300000

/*----------------------------------------------------------------------------
 * BASE: MPM2_MPM
 *--------------------------------------------------------------------------*/

#define MPM2_MPM_BASE                                               0xe05a0000
#define MPM2_MPM_BASE_SIZE                                          0x0000e000
#define MPM2_MPM_BASE_PHYS                                          0x004a0000

/*----------------------------------------------------------------------------
 * BASE: CONFIG_NOC
 *--------------------------------------------------------------------------*/

#define CONFIG_NOC_BASE                                             0xe0600000
#define CONFIG_NOC_BASE_SIZE                                        0x00001000
#define CONFIG_NOC_BASE_PHYS                                        0x00500000

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_NOC
 *--------------------------------------------------------------------------*/

#define SYSTEM_NOC_BASE                                             0xe0720000
#define SYSTEM_NOC_BASE_SIZE                                        0x0000b000
#define SYSTEM_NOC_BASE_PHYS                                        0x00520000

/*----------------------------------------------------------------------------
 * BASE: A2_NOC_AGGRE2_NOC
 *--------------------------------------------------------------------------*/

#define A2_NOC_AGGRE2_NOC_BASE                                      0xe0880000
#define A2_NOC_AGGRE2_NOC_BASE_SIZE                                 0x00009000
#define A2_NOC_AGGRE2_NOC_BASE_PHYS                                 0x00580000

/*----------------------------------------------------------------------------
 * BASE: CORE_TOP_CSR
 *--------------------------------------------------------------------------*/

#define CORE_TOP_CSR_BASE                                           0xe0900000
#define CORE_TOP_CSR_BASE_SIZE                                      0x000c0000
#define CORE_TOP_CSR_BASE_PHYS                                      0x00700000

/*----------------------------------------------------------------------------
 * BASE: TLMM
 *--------------------------------------------------------------------------*/

#define TLMM_BASE                                                   0xe0c00000
#define TLMM_BASE_SIZE                                              0x00300000
#define TLMM_BASE_PHYS                                              0x01000000

/*----------------------------------------------------------------------------
 * BASE: SSC
 *--------------------------------------------------------------------------*/

#define SSC_BASE                                                    0xe1800000
#define SSC_BASE_SIZE                                               0x00800000
#define SSC_BASE_PHYS                                               0x01800000

/*----------------------------------------------------------------------------
 * BASE: QDSS_QDSS_ISTARI
 *--------------------------------------------------------------------------*/

#define QDSS_QDSS_ISTARI_BASE                                       0xe2000000
#define QDSS_QDSS_ISTARI_BASE_SIZE                                  0x00800000
#define QDSS_QDSS_ISTARI_BASE_PHYS                                  0x03000000

/*----------------------------------------------------------------------------
 * BASE: PMIC_ARB
 *--------------------------------------------------------------------------*/

#define PMIC_ARB_BASE                                               0xe3000000
#define PMIC_ARB_BASE_SIZE                                          0x02000000
#define PMIC_ARB_BASE_PHYS                                          0x04000000

/*----------------------------------------------------------------------------
 * BASE: PERIPH_SS
 *--------------------------------------------------------------------------*/

#define PERIPH_SS_BASE                                              0xe5000000
#define PERIPH_SS_BASE_SIZE                                         0x00400000
#define PERIPH_SS_BASE_PHYS                                         0x07400000

/*----------------------------------------------------------------------------
 * BASE: LPASS
 *--------------------------------------------------------------------------*/

#define LPASS_BASE                                                  0xee000000
#define LPASS_BASE_SIZE                                             0x00400000
#define LPASS_BASE_PHYS                                             0x09000000


#endif /* __MSMHWIOBASE_H__ */
