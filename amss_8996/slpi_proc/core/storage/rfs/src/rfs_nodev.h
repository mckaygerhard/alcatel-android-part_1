/***********************************************************************
 * rfs_nodev.h
 *
 * RFS Nodev Operations.
 * Copyright (C) 2013 QUALCOMM Technologies, Inc.
 *
 * APIs to hook up stubs for RFS APIs when there is no underlying
 * Filesystem implementation.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.slpi/1.0/storage/rfs/src/rfs_nodev.h#1 $ $DateTime: 2014/06/27 13:53:49 $ $Author: coresvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2013-11-14   zzz   Create

===========================================================================*/

#ifndef __RFS_NODEV_H__
#define __RFS_NODEV_H__

void rfs_nodev_init (void);

#endif /* not __RFS_NODEV_H__ */
