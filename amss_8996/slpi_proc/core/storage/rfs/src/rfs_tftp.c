/***********************************************************************
 * rfs_tftp.c
 *
 * RFS wrapper on top of TFTP.
 * Copyright (C) 2013-2014 QUALCOMM Technologies, Inc.
 *
 * RFS implementation on top of TFTP. This maps the RFS APIs and data
 * structres to TFTP APIs and data structures and vice versa.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.slpi/1.0/storage/rfs/src/rfs_tftp.c#1 $ $DateTime: 2014/06/27 13:53:49 $ $Author: coresvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2014-01-20   dks   Fix compilation on windows builds.
2014-01-13   dks   Add wrappers for rex critical section APIs.
2013-12-26   rp    Add tftp-client module.
2013-11-24   rp    Support TFTP extension options.
2013-11-12   dks   Create

===========================================================================*/

#include "rfs_config_i.h"

#ifdef FEATURE_RFS_USE_TFTP

#include "rfs_tftp.h"
#include "rfs_ops.h"
#include "rfs_fcntl.h"
#include "rfs_types.h"
#include "rfs_errno.h"
#include "tftp_client.h"
#include "tftp_malloc.h"
#include "tftp_assert.h"
#include "tftp_threads.h"
#include "err.h"
#include <string.h>

struct rfs_file_desc
{
  int used;
  char filename [RFS_PATH_MAX];
  uint32 offset;
  int oflag;
};

static struct rfs_file_desc rfs_fd [RFS_MAX_FILE_DESCRIPTORS];

static struct rfs_ops_type rfs_tftp_ops;

/* Critical section for accessing the FD table */
static tftp_mutex_handle rfs_tftp_cs;

static void
rfs_tftp_lock_init (void)
{
  uint32 result;

  memset (&rfs_tftp_cs, 0, sizeof (rfs_tftp_cs));
  result = tftp_thread_mutex_init (&rfs_tftp_cs);
  DEBUG_ASSERT (result == 0);
}

static void
rfs_tftp_lock (void)
{
  uint32 result;

  result = tftp_thread_lock (&rfs_tftp_cs);
  DEBUG_ASSERT (result == 0);
}

static void
rfs_tftp_unlock (void)
{
  uint32 result;

  result = tftp_thread_unlock (&rfs_tftp_cs);
  DEBUG_ASSERT (result == 0);
}

static int
rfs_tftp_find_free_fd (void)
{
  int i;

  rfs_tftp_lock ();

  for (i = 0; i < RFS_MAX_FILE_DESCRIPTORS; i++)
  {
    if (rfs_fd[i].used == 0)
    {
      break;
    }
  }

  rfs_tftp_unlock ();

  if (i == RFS_MAX_FILE_DESCRIPTORS)
  {
    i = -1;
  }

  return i;
}

static int
rfs_tftp_find_fd_by_filename (const char *path)
{
  int i;

  rfs_tftp_lock ();

  for (i = 0; i < RFS_MAX_FILE_DESCRIPTORS; i++)
  {
    if (strncmp (rfs_fd[i].filename, path, sizeof (rfs_fd[i].filename)) == 0)
    {
      break;
    }
  }

  rfs_tftp_unlock ();

  if (i == RFS_MAX_FILE_DESCRIPTORS)
  {
    i = -1;
  }

  return i;
}

static int
rfs_tftp_check_valid_fd (int fd)
{
  if (fd >= RFS_MAX_FILE_DESCRIPTORS)
    return 0;

  rfs_tftp_lock ();

  if (rfs_fd[fd].used == 0)
    return 0;

  rfs_tftp_unlock ();

  return 1;
}

static int
rfs_tftp_is_path_valid (const char *path)
{
  if (path == NULL)
    return 0;

  if (path[0] == '\0')
    return 0;

  return 1;
}

static int
rfs_tftp_map_tftp_to_rfs_error (enum tftp_client_result_type client_res)
{
  switch (client_res)
  {
    case TFTP_CLIENT_RESULT_ENOENT:
      return RFS_ENOENT;

    case TFTP_CLIENT_RESULT_ENOMEM:
      return RFS_ENOMEM;

    case TFTP_CLIENT_RESULT_ENODEV:
      return RFS_ENODEV;

    default:
      return RFS_ESYSTEM;
  }
}

int
rfs_tftp_open (const char *path, int oflag)
{
  int fd_index, result;
  struct rfs_file_desc *fd;
  uint32 out_buf_size;
  enum tftp_client_result_type client_res;

  if (!rfs_tftp_is_path_valid (path))
  {
    return RFS_EINVAL;
  }

  fd_index = rfs_tftp_find_free_fd ();
  if (fd_index < 0)
  {
    return RFS_EMFILE;
  }

  client_res = tftp_stat (path, &out_buf_size);
  if (client_res != TFTP_CLIENT_RESULT_SUCCESS)
  {
    /* If create flag not set, return error */
    if ((oflag & RFS_O_CREAT) == 0)
    {
      return RFS_ENOENT;
    }
    else
    {
      /* Create file */
      client_res = tftp_put (path, "", 0, &out_buf_size);
      if (client_res != TFTP_CLIENT_RESULT_SUCCESS)
      {
        result = rfs_tftp_map_tftp_to_rfs_error (client_res);
        return result;
      }
    }
  }

  if (oflag & RFS_O_TRUNC)
  {
    client_res = tftp_put (path, "", 0, &out_buf_size);
    if (client_res != TFTP_CLIENT_RESULT_SUCCESS)
    {
      result = rfs_tftp_map_tftp_to_rfs_error (client_res);
      return result;
    }
  }

  ASSERT (fd_index >= 0);
  fd = &rfs_fd[fd_index];
  fd->used = 1;
  strncpy (fd->filename, path, sizeof (fd->filename));
  fd->oflag = oflag;
  fd->offset = 0;

  return fd_index;
}

int
rfs_tftp_close (int filedes)
{
  if (!rfs_tftp_check_valid_fd (filedes))
  {
    return RFS_EBADF;
  }

  memset (&rfs_fd[filedes], 0x0, sizeof (rfs_fd[filedes]));

  return 0;
}

int32
rfs_tftp_read (int filedes, void *buf, uint32 nbyte)
{
  struct rfs_file_desc *fd;
  int32 read_size;
  uint32 out_buf_size;
  enum tftp_client_result_type client_res;

  if (!rfs_tftp_check_valid_fd (filedes))
  {
    return RFS_EBADF;
  }

  fd = &rfs_fd[filedes];
  if ( ((fd->oflag & RFS_O_RDONLY) == 0) &&
       ((fd->oflag & RFS_O_RDWR) == 0))
  {
    return RFS_EBADF;
  }

  if (buf == NULL)
  {
    return RFS_EINVAL;
  }

  if (nbyte == 0)
  {
    return 0;
  }

  /* TODO: Handle encrpytion and compression */

  client_res = tftp_read (fd->filename, fd->offset, buf, nbyte, &out_buf_size);
  if (client_res !=  TFTP_CLIENT_RESULT_SUCCESS)
  {
    read_size = rfs_tftp_map_tftp_to_rfs_error (client_res);
  }
  else
  {
    read_size = (int32)out_buf_size;
  }

  return read_size;
}

int32
rfs_tftp_write (int filedes, const void *buf, uint32 nbyte)
{
  struct rfs_file_desc *fd;
  int32 wrote_size;
  uint32 out_buf_size;
  enum tftp_client_result_type client_res;

  if (!rfs_tftp_check_valid_fd (filedes))
  {
    return RFS_EBADF;
  }

  fd = &rfs_fd[filedes];
  if ( ((fd->oflag & RFS_O_WRONLY) == 0) &&
       ((fd->oflag & RFS_O_RDWR) == 0))
  {
    return RFS_EBADF;
  }

  if (buf == NULL)
  {
    return RFS_EINVAL;
  }

  if (nbyte == 0)
  {
    return 0;
  }

  /* TODO: Handle encrpytion and compression */

  client_res = tftp_write (fd->filename, fd->offset, buf, nbyte, &out_buf_size);
  if (client_res != TFTP_CLIENT_RESULT_SUCCESS)
  {
    wrote_size = rfs_tftp_map_tftp_to_rfs_error (client_res);
  }
  else
  {
    wrote_size = (int32)out_buf_size;
    fd->offset += wrote_size;
  }

  return wrote_size;
}

int32
rfs_tftp_seek (int filedes, int32 offset, int whence)
{
  int32 new_offset;
  uint32 file_size;
  struct rfs_file_desc *fd;
  enum tftp_client_result_type client_res;

  if (!rfs_tftp_check_valid_fd (filedes))
  {
    return RFS_EBADF;
  }

  fd = &rfs_fd[filedes];

  switch (whence)
  {
    case RFS_SEEK_CUR:
      new_offset = fd->offset + offset;
      break;

    case RFS_SEEK_END:
      client_res = tftp_stat (fd->filename, &file_size);
      if (client_res != TFTP_CLIENT_RESULT_SUCCESS)
      {
        ERR_FATAL ("%d, Filesystem corrupted. Open file deleted%d%d\n",
                    client_res, 0, 0);
      }
      new_offset = file_size + offset;
      break;

    case RFS_SEEK_SET:
      new_offset = 0 + offset;
      break;

    default:
      return RFS_EINVAL;
  }


  if (new_offset < 0)
  {
    return RFS_EPERM;
  }

  fd->offset = new_offset;

  return new_offset;
}

int
rfs_tftp_unlink (const char *path)
{
  int fd_index, result = 0;
  struct rfs_file_desc *fd;
  uint32 file_size;
  enum tftp_client_result_type client_res;

  if (!rfs_tftp_is_path_valid (path))
  {
    return RFS_EINVAL;
  }

  fd_index = rfs_tftp_find_fd_by_filename (path);
  if (fd_index >= 0)
  {
    return RFS_EBUSY;
  }

  fd = &rfs_fd[fd_index];

  client_res = tftp_stat (fd->filename, &file_size);
  if (client_res != TFTP_CLIENT_RESULT_SUCCESS)
  {
    result = rfs_tftp_map_tftp_to_rfs_error (client_res);
    goto End;
  }

  fd = &rfs_fd[fd_index];
  client_res = tftp_put (fd->filename, "", 0, &file_size);
  result = rfs_tftp_map_tftp_to_rfs_error (client_res);

End:
  return result;
}


int
rfs_tftp_stat (const char *path, struct rfs_stat_buf *buf)
{
  int result;
  uint32 size;
  enum tftp_client_result_type client_res;

  if (!rfs_tftp_is_path_valid (path))
  {
    return RFS_EINVAL;
  }

  if (buf == NULL)
  {
    return RFS_EINVAL;
  }

  client_res = tftp_stat (path, &size);
  if (client_res != TFTP_CLIENT_RESULT_SUCCESS)
  {
    result = rfs_tftp_map_tftp_to_rfs_error (client_res);
    return result;
  }

  memset (buf, 0x0, sizeof (struct rfs_stat_buf));
  buf->st_size = size;

  return 0;
}

int32
rfs_tftp_put (const char *path, void *buf, uint32 length, int oflag)
{
  int32 result;
  uint32 out_buf_size;
  enum tftp_client_result_type client_res;

  if (!rfs_tftp_is_path_valid (path))
  {
    return RFS_EINVAL;
  }

  if (buf == NULL)
  {
    return RFS_EINVAL;
  }

  if ((oflag & RFS_O_RDONLY) != 0)
  {
    return RFS_EINVAL;
  }

#if 0

  result = tftp_stat (path);
  if (result < 0 && ((oflag & RFS_O_CREAT) == 0))
  {
    return RFS_ENOENT;
  }
#endif

  /* TODO: Handle encrpytion and compression */

  client_res = tftp_put (path, buf, length, &out_buf_size);
  if (client_res != TFTP_CLIENT_RESULT_SUCCESS)
  {
    result = rfs_tftp_map_tftp_to_rfs_error (client_res);
    return result;
  }

  return (int32)out_buf_size;
}

int32
rfs_tftp_get (const char *path, void *buf, uint32 length)
{
  int32 result;
  uint32 out_buf_size;
  enum tftp_client_result_type client_res;

  if (!rfs_tftp_is_path_valid (path))
  {
    return RFS_EINVAL;
  }

  if (buf == NULL)
  {
    return RFS_EINVAL;
  }

  if (length == 0)
  {
    return 0;
  }
#if 0
  result = tftp_stat (path);
  if (result < 0)
  {
    return RFS_ENOENT;
  }
#endif
  /* TODO: Handle encrpytion and compression */

  client_res = tftp_get (path, buf, length, &out_buf_size);
  if (client_res != TFTP_CLIENT_RESULT_SUCCESS)
  {
    result = rfs_tftp_map_tftp_to_rfs_error (client_res);
    return result;
  }

  return (int32)out_buf_size;
}


int
rfs_tftp_init (void)
{
  int result = -1;
  enum tftp_client_result_type client_res;

  rfs_tftp_lock_init ();
  rfs_tftp_lock ();

  tftp_malloc_init ();

  client_res = tftp_client_init ();
  if (client_res != TFTP_CLIENT_RESULT_SUCCESS)
  {
    goto End;
  }

  memset (rfs_fd, 0x0, sizeof (rfs_fd));
  memset (&rfs_tftp_ops, 0x0, sizeof (rfs_tftp_ops));

  rfs_tftp_ops.open = rfs_tftp_open;
  rfs_tftp_ops.close = rfs_tftp_close;
  rfs_tftp_ops.read = rfs_tftp_read;
  rfs_tftp_ops.write = rfs_tftp_write;
  rfs_tftp_ops.seek = rfs_tftp_seek;
  rfs_tftp_ops.unlink = rfs_tftp_unlink;
  rfs_tftp_ops.stat = rfs_tftp_stat;
  rfs_tftp_ops.put = rfs_tftp_put;
  rfs_tftp_ops.get = rfs_tftp_get;

  rfs_ops_register (&rfs_tftp_ops);
  result = 0;

End:
  rfs_tftp_unlock ();
  return result;
}

#endif /* FEATURE_RFS_USE_TFTP */
