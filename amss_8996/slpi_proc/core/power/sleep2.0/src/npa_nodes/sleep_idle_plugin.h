#ifndef SLEEP_IDLE_PLUGIN_H
#define SLEEP_IDLE_PLUGIN_H
/*============================================================================
  FILE:         sleep_idle_plugin.h

  OVERVIEW:     This file provides the externs and declarations needed for the
                sleep idle plugin subsystem.
 
  DEPENDENCIES: None

                Copyright (c) 2011-2014 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
==============================================================================
$Header: //components/rel/core.slpi/1.0/power/sleep2.0/src/npa_nodes/sleep_idle_plugin.h#2 $
$DateTime: 2014/11/14 16:17:09 $
============================================================================*/
#include "sleep_plugin_clients.h"

/*==============================================================================
                           FUNCTION DECLARATIONS
 =============================================================================*/
/**
 * sleepIdlePlugin_initialize
 * 
 * @brief Initialize the sleep idle plugin subsystem.  This function registers 
 *        the idle plugin node. 
 */
void sleepIdlePlugin_initialize( void );

#endif /* SLEEP_IDLE_PLUGIN_H */

