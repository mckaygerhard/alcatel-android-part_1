/*==============================================================================
  FILE:         ssc_ret_lpr.c

  OVERVIEW:     This file provides the LPR definition for SSC retention low
                power mode.

  DEPENDENCIES: None

                Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/sleep2.0/src/lpr/ssc_ret_lpr.c#1 $
$DateTime: 2014/09/05 16:56:15 $
==============================================================================*/

#include "sleep_lpr.h"

/*===========================================================================
                                     NOTE
 ==========================================================================*/

/**
 * This file is provided just for LPR dependency purpose and so functions
 * are stubs. Once sysdrivers team provides the implementation, it should 
 * be moved to their area along with xml file. 
 *
 * This file has used sleep coding conventions but can be changed to 
 * different one when moved.
 *
 * Having stubs now allows Sleep to be code complete now without actually
 * depending or waiting on sysdriver delivery. 
 */

/*===========================================================================
                              GLOBAL FUNCTIONS
 ==========================================================================*/
/**
 * SSCRetLPR_initialize
 *
 * @brief Init function for SSC retention LPR
 */
void SSCRetLPR_initialize(void)
{
  /* One time setup - if any */
}

/**
 * SSCRetLPR_enter
 *
 * @brief Enter function for SSC retention low power mode.
 */
void SSCRetLPR_enter(uint64 wakeup_tick)
{
  /* Perform Handshake with AON and disable L2VIC as wakeup source - Instead 
   * of writing entire register, please write to appropriate field using 
   * HWIO macros. */
}

/**
 * SSCRetLPR_exit
 *
 * @brief Exit function for SSC retention low power mode.
 */
void SSCRetLPR_exit(void)
{
  /* Undo what was done in enter function. */
}
