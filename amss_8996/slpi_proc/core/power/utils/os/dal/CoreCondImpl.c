/*==============================================================================
@file CoreCondImpl.c

Implementation for DAL specific condition variables

Copyright (c) 2009-2012 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary

$Header: //components/rel/core.slpi/1.0/power/utils/os/dal/CoreCondImpl.c#3 $
==============================================================================*/
#include "CoreCond.h"

int Core_CondInit( CoreCondType *cond, CoreCondAttrType attr )
{
  (void) cond; (void) attr;

  /* UNIMPLEMENTED */
  return -1;
}
