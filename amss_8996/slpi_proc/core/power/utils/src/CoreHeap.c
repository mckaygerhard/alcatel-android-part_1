/*============================================================================
@file CoreHeap.c

Implements CoreHeap.h

Copyright (c) 2013-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.

$Header: //components/rel/core.slpi/1.0/power/utils/src/CoreHeap.c#4 $
============================================================================*/
#include "CoreHeap.h"
#include "DALSys.h"

#ifdef __cplusplus
extern "C" {
#endif

void* Core_Malloc( unsigned int size )
{
  void *ptr = NULL;
  return ( DALSYS_Malloc( size, &ptr ) == DAL_SUCCESS ? ptr : NULL );
}

void Core_Free( void *ptr )
{
  if ( ptr )
  {
    DALSYS_Free( ptr );
  }
}

#ifdef __cplusplus
}
#endif
