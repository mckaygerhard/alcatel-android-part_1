/*==============================================================================
  FILE:         uSleep_os.c

  OVERVIEW:     This file provides uSleep API and functions that reside in
                normal mode memory space

  DEPENDENCIES: None
  
                Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/uSleep/src/os/qurt/ddr/uSleep_os.c#11 $
$DateTime: 2015/05/14 11:37:00 $
==============================================================================*/
#include "DALStdDef.h"
#include "CoreVerify.h"
#include "uSleep.h"
#include "uSleepi.h"
#include "uSleep_util.h"
#include "uSleep_timer.h"
#include "uSleep_target.h"
#include "uSleep_trans.h"
#include "uSleep_lpr.h"
#include "uSleep_log.h"
#include "uSleep_ddr_log.h"
#include "uSleep_qdi.h"
#include "CoreTime.h"
#include "timer.h"
#include "island_mgr.h"
#include "sleep_os.h"
#include "uSleep_os.h"
#include "client_os.h"
#include "uSleep_node.h"
#include "uSleep_solver.h"
#include "simple_solver.h"
#include "sleep_solver.h"
#include "sleepActive.h"

/*==============================================================================
                           INTERNAL TRANSITION FUNCTIONS
 =============================================================================*/
/** 
 * uSleepOS_beginEntryTransition
 * 
 * @brief Entry function to uSleep called from STM context. 
 *        uSleep entry is now committed.
 *  
 * @note Assumes function is called from STM via main sleep function 
 */
static void uSleepOS_beginEntryTransition(void)
{
  uint64                        startTime     = CoreTimetick_Get64();
  uSleep_internal_state         state         = uSleep_getStateInternal();
  uSleep_transition_profiling   *profileData  = uSleep_getProfilingDataPtr();
  const sleep_solver_deadlines  *deadlines    = sleepActive_GetSolverHardDeadlines();

  CORE_VERIFY_PTR(profileData);

  uSleepDDRLog_QDSSPrintf(USLEEP_DDR_LOG_LEVEL_TRANSITION, 
                          USLEEP_ENTER_NUM_ARGS,
                          USLEEP_ENTER_STR, 
                          USLEEP_ENTER);

  /* Set timestamp of the start of the uSleep entry point */
  profileData->entry_start_time = startTime;

  /* Record number of island entries */
  profileData->total_entries++;

  /* Verify we are currently in normal mode before beginning transition
   * to island mode */
  CORE_VERIFY(uSLEEP_INTERNAL_STATE_IN_DDR == state);

  /* Only defer normal operational timers, island ones will be done as part of
   * low power mode entry in island. */
  timer_defer_match_interrupt_64();

  /* Set full entry state */
  uSleep_setStateInternal(uSLEEP_INTERNAL_STATE_FULL_ENTRY);
  
  /* Update the PMI handler function to the uImage version */
  uSleepTarget_setPMIHandler();

  /* Call LPR init functions */
  uSleepLPR_init();

  /* Transfer normal operational wakeup time to island system */
  uSleepTrans_setDDRWakeupTimeInternal(deadlines->normal);

  /* Call notification CB's while still in normal operational mode */
  uSleepTrans_transitionNotify(USLEEP_STATE_ENTER);

  /* Update the idle function pointer to the main transition function.
   * This is done because we must switch to an island function pointer. */
  uSleepOS_setIdleFunctionPtr(uSleepTrans_performIslandTransition);

  return;
}

/** 
 * uSleepOS_prepareForSleep
 * 
 * @brief Prepares uSleep by ensuring various sleep options & 
 *        settings allow entry. If the function returns, it will have
 *        already placed the system in STM.         
 *    
 * @return Time of STM entry (in ticks) or 0 if unable to enter 
 *         any sleep (i.e. early exit cases)
 */
static uint64 uSleepOS_prepareForSleep(void)
{
  /* Use the main sleep prepare function which performs all the
   * required actions */
  return(sleepOS_prepareForSleep(SLEEP_OS_IDLE_MODE_LPI));
}

/*==============================================================================
                             EXTERNAL TRANSITION FUNCTIONS
 =============================================================================*/
/*
 * uSleepOS_mainEntry
 */ 
uint32 uSleepOS_mainEntry(void)
{
  uint64                      sleepStart;
  uSleep_idle_entry_ptr_type  uSleepFcnPtr;
  
  /* Update transition times if necessary */
  uSleepTrans_updateTimes();

  /* Allow island debug if option is enabled */
  uSleepTarget_enableIslandDebug(TRUE);

  /* Execute common sleep entry checks */
  sleepStart    = uSleepOS_prepareForSleep();
  uSleepFcnPtr  = uSleepOS_getIdleFunctionPtr();

  if((0 == sleepStart) || (uSleepFcnPtr != uSleepOS_mainEntry))
  {
    uSleepDDRLog_printf(USLEEP_DDR_LOG_LEVEL_TRANSITION, 1,
                        "uSleep early exit (ptr: 0x%x)",
                        (uint32)uSleepFcnPtr);
    qurt_power_exit();

    /* Disallow island debug if option is enabled */
    uSleepTarget_enableIslandDebug(FALSE);

    return 0;
  }

  /***** Begin transition to uImage *****/
  uSleepOS_beginEntryTransition();

  return 0;
}

/*==============================================================================
                               EXTERNAL FUNCTIONS
 =============================================================================*/
/*
 * uSleepOS_initialize
 */
void uSleepOS_initialize(void)
{
  /* Setup uSleep logging system */
  uSleepLog_initialize();

  /* Setup default uSleep entry point */
  uSleepOS_setIdleFunctionPtr(uSleepOS_mainEntry);

  /* Transition initialization */
  uSleepTrans_initialize();

  /* Setup guest OS client */
  uSleep_guestClientInit();

  /* Set uSleep initial internal state */
  uSleep_setStateInternal(uSLEEP_INTERNAL_STATE_IN_DDR);

  /* Set the solver function */
  uSleepSolver_setSolverFunction(simpleSolver_solve);

  /* Target specific initialization */
  uSleepTarget_initialize();

  /* Setup the handler for the DDR timer while in uImage */
  uSleepTimer_init();

  /* Setup uSleep QDI interface */
  uSleep_QDIInit();

  /* Setup uSleep Node */
  uSleepNode_initialize();

  /* Init internal signals */
  qurt_signal_init(&g_uSleepExitSignal);
  qurt_signal_init(&g_uSleepEnterSignal);

  return;
}
