/*==============================================================================
  FILE:         uSleep_target.c

  OVERVIEW:     This file provides uSleep target specfic functions while in
                island mode

  DEPENDENCIES: Object file generated from source is marked as island section
  
                Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/uSleep/src/target/uSleep_target.c#3 $
$DateTime: 2015/03/03 12:29:47 $
==============================================================================*/
#include "DALStdDef.h"
#include "CoreVerify.h"
#include "uSleep_target.h"
#include "qurt.h"
#include "HALhwio.h"
#include "uSleep_HALhwio.h"
#include "uSleep_log.h"
#include "uSleep_util.h"
#include "uTimetick.h"

/*==============================================================================
                       EXTERNAL FUNCTION DEFINITIONS
 =============================================================================*/
/* 
 * uSleepTarget_enterIdle 
 */
void uSleepTarget_enterIdle(void)
{
  /* Wait for wakeup interrupt */ 
  qurt_power_wait_for_active();
  return;
}

/* 
 * uSleepTarget_configPowerMode
 */
void uSleepTarget_configPowerMode(target_power_mode mode, boolean enablePMI)
{
  uint32 wakeupIn;
  uint32 clkGatingMode; 
  uint32 wakeIRQ;
  uint32 saveRestore;

  if(TARGET_POWER_MODE_CLOCKGATE == mode)
  {
    wakeupIn      = 0;
    clkGatingMode = 1;
    wakeIRQ       = 0;
    saveRestore   = 0;
  }
  else
  {   
    wakeupIn      = 1;
    clkGatingMode = 0;
    wakeIRQ       = (TRUE == enablePMI) ? 1 : 0;
    saveRestore   = 1;
  }

  HWIO_OUT( SSC_QDSP6SS_SLPC_CFG, 
            HWIO_FVAL( SSC_QDSP6SS_SLPC_CFG, EXTHW_WAKE_EN,       0x1             ) |
            HWIO_FVAL( SSC_QDSP6SS_SLPC_CFG, MEM_PU_PERI_STAGGER, 0x1             ) |
            HWIO_FVAL( SSC_QDSP6SS_SLPC_CFG, MEM_PD_PERI_STAGGER, 0x0             ) |
            HWIO_FVAL( SSC_QDSP6SS_SLPC_CFG, MEM_PU_ARRY_STAGGER, 0x1             ) |
            HWIO_FVAL( SSC_QDSP6SS_SLPC_CFG, MEM_PD_ARRY_STAGGER, 0x0             ) |
            HWIO_FVAL( SSC_QDSP6SS_SLPC_CFG, WAKEUP_IN_EN,        wakeupIn        ) |
            HWIO_FVAL( SSC_QDSP6SS_SLPC_CFG, CLK_GATING_MODE,     clkGatingMode   ) |
            HWIO_FVAL( SSC_QDSP6SS_SLPC_CFG, PD_HS_MODE,          0x1             ) |
            HWIO_FVAL( SSC_QDSP6SS_SLPC_CFG, SAW_DBG,             0x0             ) |
            HWIO_FVAL( SSC_QDSP6SS_SLPC_CFG, PD_SRC_SEL,          0x0             ) |
            HWIO_FVAL( SSC_QDSP6SS_SLPC_CFG, EXT_WAKEUP_ENA,      0x0             ) );

  HWIO_OUT( SSC_QDSP6SS_SPMCTL_EN_STATERET,
            HWIO_FVAL( SSC_QDSP6SS_SPMCTL_EN_STATERET, WAKE_IRQ, wakeIRQ     ) |
            HWIO_FVAL( SSC_QDSP6SS_SPMCTL_EN_STATERET, RESTORE,  saveRestore ) |
            HWIO_FVAL( SSC_QDSP6SS_SPMCTL_EN_STATERET, SAVE,     saveRestore ) );

  return;
}

/* 
 * uSleepTarget_programWakeupTimer 
 */
void uSleepTarget_programWakeupTimer(uint64 wakeupTime)
{
  uSleep_setLastSleepWakeupInternal(wakeupTime);

  /* Program the wakeup time */
  uTimetick_SetWakeUpTimerInterrupt(wakeupTime);
  uTimetick_EnableWakeUpTimerInterrupt(TRUE);

  uSleepLog_printf(USLEEP_LOG_LEVEL_PROFILING, (1*2),
                   " Set wakeup (match: 0x%llx)",
                   MICRO_ULOG64_DATA(wakeupTime));

  /* Verify wakeup time is valid as late as possible */
  CORE_VERIFY(uTimetick_Get() < wakeupTime);

  return;
}

/* 
 * uSleepTarget_disableWakeupTimer 
 */
void uSleepTarget_disableWakeupTimer(void)
{
  uSleepLog_printf(USLEEP_LOG_LEVEL_PROFILING, 0,
                   "Disable wakeup timer");

  /* To stop the AON wakeup timer interrupt, since this function is 
   * shared with uSleep, we must use the uTimetick API. */ 
  uTimetick_EnableWakeUpTimerInterrupt(FALSE);
  return;
}

/* 
 * uSleepTarget_enterLowPowerMode 
 */
void uSleepTarget_enterLowPowerMode(uSleep_LPR *mode, uint64 wakeupTime)
{
  uSleepLog_printf(USLEEP_LOG_LEVEL_PROFILING, 0,
                   "Entering LPM",
                   mode->name);

  CORE_VERIFY(NULL != mode->enter_func);
  mode->enter_func(wakeupTime);

  return;
}

/* 
 * uSleepTarget_exitLowPowerMode 
 */
void uSleepTarget_exitLowPowerMode(uSleep_LPR *mode)
{
  uSleepLog_printf(USLEEP_LOG_LEVEL_PROFILING, 0, 
                   "Exiting LPM");

  CORE_VERIFY(NULL != mode->exit_func);
  mode->exit_func();

  return;
}

/* 
 * uSleepTarget_configureIslandTransitionPowerMode
 */ 
void uSleepTarget_configureIslandTransitionPowerMode(boolean clockGate)
{
  uSleep_power_mode mode;
  
  if(TRUE == clockGate)
  {
    uSleepLog_printf(USLEEP_LOG_LEVEL_TRANSITION, 0,
                     "Config transition LPM");

    uSleepTarget_configPowerMode(TARGET_POWER_MODE_CLOCKGATE, FALSE);
    qurt_power_override_wait_for_idle(0);
  }
  else
  {
    mode = uSleep_getLowPowerModeInternal();

    uSleepLog_printf(USLEEP_LOG_LEVEL_TRANSITION, 0,
                     "Unconfig transition LPM");

    if(USLEEP_RETENTION_ENABLE == mode ||
       USLEEP_CXO_SHUTDOWN_ENABLE == mode)
    {
      uSleepTarget_configPowerMode(TARGET_POWER_MODE_APCR, TRUE);
      qurt_power_override_wait_for_idle(0);
    }
    else
    {
      uSleepTarget_configPowerMode(TARGET_POWER_MODE_APCR, FALSE);
      qurt_power_override_wait_for_idle(1);
    }
  }

  return;
}

