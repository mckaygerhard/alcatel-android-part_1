#ifndef CLIENT_USER_H
#define CLIENT_USER_H
/*==============================================================================
  FILE:         client_user.h

  OVERVIEW:     uSleep client types

  DEPENDENCIES: None
  
                Copyright (c) 2014 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/uSleep/src/client_user/client_user.h#1 $
$DateTime: 2014/11/14 16:17:09 $
==============================================================================*/
#include "DALStdDef.h"

/*==============================================================================
                              EXTERNAL VARIABLES
 =============================================================================*/
extern int            g_uSleepQDIClientHandle; /* Island section QDI handle */
extern uSleep_signals g_uSleepCallbackSignal;

#endif /* CLIENT_USER_H */

