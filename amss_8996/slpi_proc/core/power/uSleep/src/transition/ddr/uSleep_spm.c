/*==============================================================================
  FILE:         uSleep_spm.c

  OVERVIEW:     This file provides uSleep SPM initialization routines

  DEPENDENCIES: None
  
                Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/uSleep/src/transition/ddr/uSleep_spm.c#6 $
$DateTime: 2015/03/10 08:32:28 $
==============================================================================*/
#include "DALStdDef.h"
#include "DDIInterruptController.h"
#include "CoreVerify.h"
#include "uSleep_spm.h"
#include "uSleep_trans.h"
#include "spm_common.h"
#include "uSleep_HALhwio.h"
#include "HALhwio.h"

/*==============================================================================
                            INTERNAL VARIABLES
 =============================================================================*/
/* SPM command sequence AXI bus isolation to enter island mode */
static spm_cmd_t g_uSleepSpmCmdSeqIslandEntry[] =
{
  /* 0 */
  SPM_SEQ_PWR_CTL   (QDSP6_BUS_HALT,           0), /* Assert bus halt request 23 */ 
  SPM_SEQ_EVENT_CTL (EVENT_3,                  0), /* Wait for bus halt          */ 
  SPM_SEQ_PWR_CTL   (QDSP6_BUS_CLK,            0), /* Bus clock off           29 */
  SPM_SEQ_PWR_CTL   (QDSP6_BUS_INT_CLAMP_SET,  0), /* Bus interface clamp     24 */

  /* 4 */
  SPM_SEQ_PWR_CTL   (QDSP6_BUS_PWR_CLAMP_SET,  0), /* Bus power clamp         25 */
  SPM_SEQ_PWR_CTL   (QDSP6_BUS_CLK,            0), /* Bus clk on              29 */
  SPM_SEQ_PWR_CTL   (QDSP6_BUS_HALT,           0), /* Deassert bus halt       23 */
  SPM_SEQ_END_CTL   (0,                        0), /* End of Program */
};

/* SPM command sequence AXI bus de-isolation to exit island mode */
static spm_cmd_t g_uSleepSpmCmdSeqIslandExit[] =
{
  /* 0 */
  SPM_SEQ_PWR_CTL (QDSP6_BUS_CLK,           0), /* Bus clk off */
  SPM_SEQ_PWR_CTL (QDSP6_BUS_PWR_CLAMP_CLR, 0), /* Clear bus power clamp */
  SPM_SEQ_PWR_CTL (QDSP6_BUS_FIFO_RESET,    0), /* Assert FIFO reset */
  SPM_SEQ_PWR_CTL (QDSP6_BUS_FIFO_RESET,    0), /* Deassert FIFO reset */

  /* 4 */
  SPM_SEQ_PWR_CTL (QDSP6_BUS_INT_CLAMP_CLR, 0), /* Clear bus interface clamp */
  SPM_SEQ_PWR_CTL (QDSP6_BUS_CLK,           0), /* Bus clk on */
  SPM_SEQ_END_CTL (0,                       0), /* End of Program */
};

/* Entry and exit sequence data */
static uSleep_spm_island_sequence g_uSleepSPMSequence = 
{
  {0, g_uSleepSpmCmdSeqIslandEntry, sizeof(g_uSleepSpmCmdSeqIslandEntry)},
  {0, g_uSleepSpmCmdSeqIslandExit, sizeof(g_uSleepSpmCmdSeqIslandExit)}
};

/* Entry and exit interrupt registration handle */
static DalDeviceHandle *g_uSleepDALInterruptCtrlHndl;

/*==============================================================================
                          INTERNAL FUNCTION DEFINITIONS
 =============================================================================*/
/**
 * uSleepSPM_configSWTrigger
 * 
 * @brief Sets the SPM start address for the required entry and exit
 *        sequences
 * 
 * @param seqIdx:       Starting index of sequence
 * @param triggerType:  Enum to indicate entry or exit sequence 
 */
static void uSleepSPM_configSWTrigger(uint32                   seqIdx, 
                                      uSleep_spm_soft_trigger  triggerType)
{
  CORE_VERIFY(triggerType < USLEEP_SW_TRIGGER_LAST);

  HWIO_OUTFI(SSC_QDSP6SS_SPM_TRIG_CFGn, triggerType, SPM_STARTADDR, seqIdx);

  return;
}

/*==============================================================================
                          EXTERNAL FUNCTION DEFINITIONS
 =============================================================================*/
/* 
 * uSleepSPM_setupSequenceTransition 
 */
void uSleepSPM_setupSequenceTransition(void)
{
  /* Setup configuration register for bus isolation sequence */
  HWIO_OUT(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CLK,
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CLK, AXIM2,  0)   |
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CLK, AXIM,   1)   |
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CLK, CORE,   1));

  HWIO_OUT(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CTL,
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CTL, AXIM2_FIFO_ARESET,  0) |
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CTL, AXIM2_HALTREQ,      0) |
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CTL, AXIM_FIFO_ARESET,   1) |
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CTL, AXIM_HALTREQ,       1) |
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CTL, BUSM_HALTREQ,       0));

  HWIO_OUT(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CLAMP,
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CLAMP, AXIM_CLAMP_E_CLEAR, 1)   |
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CLAMP, AXIM_CLAMP_E_SET,   1)   |
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CLAMP, AXIM_CLAMP_L_CLEAR, 1)   |
           HWIO_FVAL(SSC_QDSP6SS_SPMCTL_EN_ISOINTF_CLAMP, AXIM_CLAMP_L_SET,   1));

  HWIO_OUTF(SSC_QDSP6SS_SPM_EVENT_CTL, AXIM2_HALTACK_OVRRD, 1);
  HWIO_OUTF(SSC_QDSP6SS_SPM_EVENT_CTL, AXIM_HALTACK_OVRRD,  0);
  HWIO_OUTF(SSC_QDSP6SS_SPM_EVENT_CTL, ALL_HALTACK_OVRRD,   1);

  HWIO_OUT(SSC_SSC_IEC_CTL,
           HWIO_FVAL(SSC_SSC_IEC_CTL, L2_MISS_ISLAND_EXIT_EN,     0)  |
           HWIO_FVAL(SSC_SSC_IEC_CTL, ISO_AHB_MASTER_BYPASS,      0)  |
           HWIO_FVAL(SSC_SSC_IEC_CTL, ISO_AHB_SLAVE_BYPASS,       0)  |
           HWIO_FVAL(SSC_SSC_IEC_CTL, AHB_REQPEND_CLR,            1)  |
           HWIO_FVAL(SSC_SSC_IEC_CTL, ENTER_ISLAND_MODE_IRQ_CLR,  1));

  /* Program (de)isolation sequences into SPM */
  CORE_VERIFY(spm_program_cmd_seq(0, 
                                  g_uSleepSPMSequence.enter.sequence,
                                  g_uSleepSPMSequence.enter.size,
                                  &g_uSleepSPMSequence.enter.index) == SPM_SUCCESS);


  uSleepSPM_configSWTrigger(g_uSleepSPMSequence.enter.index, USLEEP_SW_TRIGGER_ENTER);

  CORE_VERIFY(spm_program_cmd_seq(0, 
                                  g_uSleepSPMSequence.exit.sequence,
                                  g_uSleepSPMSequence.exit.size,
                                  &g_uSleepSPMSequence.exit.index) == SPM_SUCCESS);

  uSleepSPM_configSWTrigger(g_uSleepSPMSequence.exit.index, USLEEP_SW_TRIGGER_EXIT);

  /* Attaching to interrupt controller and registering interrupt handlers */
  CORE_VERIFY(DAL_DeviceAttach(DALDEVICEID_INTERRUPTCONTROLLER,
                               &g_uSleepDALInterruptCtrlHndl) == DAL_SUCCESS );

  /* Registering for island entry complete interrupt */
  CORE_VERIFY(DAL_SUCCESS == DalInterruptController_RegisterISR(
      g_uSleepDALInterruptCtrlHndl,
      USLEEP_SPM_ISLAND_ENTRY_IRQ,
      (DALIRQ)uSleepTrans_islandEntryHandler,
      (const DALIRQCtx) 0,
      DALINTRCTRL_ENABLE_CONFIG_EX(DALINTRCTRL_ENABLE_LEVEL_HIGH_TRIGGER,
                                   0, 1)));

  /* Registering for island exit complete interrupt */
  CORE_VERIFY(DAL_SUCCESS == DalInterruptController_RegisterISR(
      g_uSleepDALInterruptCtrlHndl,
      USLEEP_SPM_ISLAND_EXIT_IRQ,
      (DALIRQ)uSleepTrans_islandExitHandler,
      (const DALIRQCtx) 0,
      DALINTRCTRL_ENABLE_CONFIG_EX(DALINTRCTRL_ENABLE_RISING_EDGE_TRIGGER,
                                   0, 1)));

  return;
}

