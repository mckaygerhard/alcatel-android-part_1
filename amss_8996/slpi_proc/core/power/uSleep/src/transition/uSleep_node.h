#ifndef USLEEP_NODE_H
#define USLEEP_NODE_H
/*==============================================================================
  FILE:         uSleep_node.h

  OVERVIEW:     Types & prototypes for the various node requirements for
                uImage transition

  DEPENDENCIES: None
  
                Copyright (c) 2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/uSleep/src/transition/uSleep_node.h#3 $
$DateTime: 2015/04/28 18:18:47 $
==============================================================================*/


/*==============================================================================
                            DEFINITIONS & TYPES
 =============================================================================*/
/* LPR node name that is used as a client of the internal uSleep client to
 * control uImage entry.  If this LPR is disabled, then uImage entry is not
 * allowed */
#define USLEEP_CPU_VDD_NODE "/core/cpu/vdd"

/*==============================================================================
                           FUNCTION DEFINITIONS
 =============================================================================*/
/** 
 * uSleepNode_initialize
 * 
 * @brief Initializes /core/uSleep node
 */
void uSleepNode_initialize(void);

#endif /* USLEEP_NODE_H */

