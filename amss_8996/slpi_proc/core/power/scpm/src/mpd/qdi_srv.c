/*
* Copyright (c) 2013-2015 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

/*
 * @file qdi_srv.c - implementation of the QDI driver in the guest OS
 *
 *  Created on: Jun 11, 2012
 *      Author: yrusakov
 */

#include <limits.h>
#include "stdlib.h"
#include "qurt.h"
#include "qurt_qdi_constants.h"
#include "qurt_qdi_driver.h"
#include "qurt_qdi.h"
#include "adsppm_qdi.h"
#include "mmpm.h"
#include "adsppm.h"
#include "ULog.h"
#include "ULogFront.h"
#include "adsppm_utils.h"
#include "adsppmcb.h"
#include "asyncmgr.h"


//#define ADSPPMQDI_ULOG 1

// Macro to align an address to the nearest 8-byte boundary
#define ADSPPM_ALIGN8(val) (((val) + 7) & ~0x00000007)


#define COMPARE_PARAMETERS(fieldName, pd, os) \
if(pd != os) \
{ULOG_RT_PRINTF_2(gAdsppm.hLog, \
"    [ERROR] "fieldName": userPD=%u != guestOS=%u", pd, os);}


QDI_Adsppm_Ctx_t gAdsppm;


void print_request_data(int client_handle, MmpmRscExtParamType *pUserReq, MmpmRscExtParamType *pReqData)
{
#ifdef ADSPPMQDI_ULOG
    int i, j;
    if((NULL != pUserReq) && (NULL != gAdsppm.hLog))
    {
        ULOG_RT_PRINTF_3(gAdsppm.hLog,
            "print_request_data: User PD, client_handle=%d, apiType=%u, numOfReq=%u",
            client_handle, pUserReq->apiType, pUserReq->numOfReq);
        COMPARE_PARAMETERS("apiType", pUserReq->apiType, pReqData->apiType);
        COMPARE_PARAMETERS("numOfReq", pUserReq->numOfReq, pReqData->numOfReq);

        if(NULL != pUserReq->pReqArray)
        {
            for(i = 0; i < pUserReq->numOfReq; i++)
            {
                ULOG_RT_PRINTF_1(gAdsppm.hLog,
                    "    User PD rscId, rscId=%u", pUserReq->pReqArray[i].rscId);
                COMPARE_PARAMETERS("rscId",
                    pUserReq->pReqArray[i].rscId,
                    pReqData->pReqArray[i].rscId);

                switch(pUserReq->pReqArray[i].rscId)
                {
                case MMPM_RSC_ID_MIPS_EXT:
                {
                    MmpmMipsReqType *pPDpMipsExt = pUserReq->pReqArray[i].rscParam.pMipsExt;
                    MmpmMipsReqType *pOSpMipsExt = pReqData->pReqArray[i].rscParam.pMipsExt;

                    if(NULL != pPDpMipsExt)
                    {
                        ULOG_RT_PRINTF_4(gAdsppm.hLog,
                            "    MIPS: User PD pMipsExt, mipsTotal=%u, mipsPerThread=%u, codeLocation=%u, reqOperation=%u",
                            pPDpMipsExt->mipsTotal,
                            pPDpMipsExt->mipsPerThread,
                            pPDpMipsExt->codeLocation,
                            pPDpMipsExt->reqOperation);
                        COMPARE_PARAMETERS("mipsTotal",
                            pPDpMipsExt->mipsTotal,
                            pOSpMipsExt->mipsTotal);
                        COMPARE_PARAMETERS("mipsPerThread",
                            pPDpMipsExt->mipsPerThread,
                            pOSpMipsExt->mipsPerThread);
                        COMPARE_PARAMETERS("codeLocation",
                            pPDpMipsExt->codeLocation,
                            pOSpMipsExt->codeLocation);
                        COMPARE_PARAMETERS("reqOperation",
                            pPDpMipsExt->reqOperation,
                            pOSpMipsExt->reqOperation);
                    }
                    break;
                }

                case MMPM_RSC_ID_GENERIC_BW:
                {
                    MmpmGenBwReqType *pPDpGenBwReq = pUserReq->pReqArray[i].rscParam.pGenBwReq;
                    MmpmGenBwReqType *pOSpGenBwReq = pReqData->pReqArray[i].rscParam.pGenBwReq;

                    if((NULL != pPDpGenBwReq) && (NULL != pPDpGenBwReq->pBandWidthArray))
                    {
                        ULOG_RT_PRINTF_1(gAdsppm.hLog,
                            "    BANDWIDTH: User PD pGenBwReq, numOfBw=%u",
                            pPDpGenBwReq->numOfBw);
                        COMPARE_PARAMETERS("numOfBw",
                            pPDpGenBwReq->numOfBw,
                            pOSpGenBwReq->numOfBw);

                        for(j = 0; j < pPDpGenBwReq->numOfBw; j++)
                        {

                            ULOG_RT_PRINTF_6(gAdsppm.hLog,
                                "        User PD pBandWidthArray[%u], masterPort=%u, slavePort=%u, bwBytePerSec=%u, usagePercentage=%u, usageType=%u",
                                j,
                                pPDpGenBwReq->pBandWidthArray[j].busRoute.masterPort,
                                pPDpGenBwReq->pBandWidthArray[j].busRoute.slavePort,
                                (uint32) pPDpGenBwReq->pBandWidthArray[j].bwValue.busBwValue.bwBytePerSec,
                                pPDpGenBwReq->pBandWidthArray[j].bwValue.busBwValue.usagePercentage,
                                pPDpGenBwReq->pBandWidthArray[j].bwValue.busBwValue.usageType);
                            COMPARE_PARAMETERS("masterPort",
                                pPDpGenBwReq->pBandWidthArray[j].busRoute.masterPort,
                                pOSpGenBwReq->pBandWidthArray[j].busRoute.masterPort);
                            COMPARE_PARAMETERS("slavePort",
                                pPDpGenBwReq->pBandWidthArray[j].busRoute.slavePort,
                                pOSpGenBwReq->pBandWidthArray[j].busRoute.slavePort);
                            COMPARE_PARAMETERS("bwBytePerSec",
                                (uint32) pPDpGenBwReq->pBandWidthArray[j].bwValue.busBwValue.bwBytePerSec,
                                (uint32) pOSpGenBwReq->pBandWidthArray[j].bwValue.busBwValue.bwBytePerSec);
                            COMPARE_PARAMETERS("usagePercentage",
                                pPDpGenBwReq->pBandWidthArray[j].bwValue.busBwValue.usagePercentage,
                                pOSpGenBwReq->pBandWidthArray[j].bwValue.busBwValue.usagePercentage);
                            COMPARE_PARAMETERS("usageType",
                                pPDpGenBwReq->pBandWidthArray[j].bwValue.busBwValue.usageType,
                                pOSpGenBwReq->pBandWidthArray[j].bwValue.busBwValue.usageType);
                        }
                    }
                    break;
                }

                default:
                    break;
                }
            }
        }
    }
#endif
}


#ifdef ADSPPM_TEST
static void print_getinfo_data(MmpmInfoDataType *pUserInfoData, MmpmInfoDataType *pInfoData)
{
#ifdef ADSPPMQDI_ULOG
    if((NULL != pUserInfoData) && (NULL != pInfoData))
    {
        ULOG_RT_PRINTF_3(gAdsppm.hLog,
            "print_getinfo_data: User PD, infoId=%u, coreId=%u, instanceId=%u",
            pUserInfoData->infoId, pUserInfoData->coreId, pUserInfoData->instanceId);
        COMPARE_PARAMETERS("infoId",
            pUserInfoData->infoId,
            pInfoData->infoId);
        COMPARE_PARAMETERS("coreId",
            pUserInfoData->coreId,
            pInfoData->coreId);
        COMPARE_PARAMETERS("instanceId",
            pUserInfoData->instanceId,
            pInfoData->instanceId);

        switch(pInfoData->infoId)
        {
        case MMPM_INFO_ID_CLK_FREQ:
        case MMPM_INFO_ID_CORE_CLK_MAX:
            if((NULL != pUserInfoData->info.pInfoClkFreqType) && (NULL != pInfoData->info.pInfoClkFreqType))
            {
                ULOG_RT_PRINTF_3(gAdsppm.hLog,
                    "    User PD pInfoClkFreqType, clkId=%u, clkFreqHz=%u, forceMeasure=%u",
                    pUserInfoData->info.pInfoClkFreqType->clkId,
                    pUserInfoData->info.pInfoClkFreqType->clkFreqHz,
                    pUserInfoData->info.pInfoClkFreqType->forceMeasure);
                COMPARE_PARAMETERS("clkId",
                    pUserInfoData->info.pInfoClkFreqType->clkId,
                    pInfoData->info.pInfoClkFreqType->clkId);
                COMPARE_PARAMETERS("clkFreqHz",
                    pUserInfoData->info.pInfoClkFreqType->clkFreqHz,
                    pInfoData->info.pInfoClkFreqType->clkFreqHz);
                COMPARE_PARAMETERS("forceMeasure",
                    pUserInfoData->info.pInfoClkFreqType->forceMeasure,
                    pInfoData->info.pInfoClkFreqType->forceMeasure);
            }
            break;

        case MMPM_INFO_ID_MIPS_MAX:
            ULOG_RT_PRINTF_1(gAdsppm.hLog,
                "    User PD mipsValue, mipsValue=%u", pUserInfoData->info.mipsValue);
            COMPARE_PARAMETERS("mipsValue",
                pUserInfoData->info.mipsValue,
                pInfoData->info.mipsValue);
            break;

        case MMPM_INFO_ID_BW_MAX:
            ULOG_RT_PRINTF_1(gAdsppm.hLog,
                "    User PD bwValue, bwValue=%u", pUserInfoData->info.bwValue);
            COMPARE_PARAMETERS("bwValue",
                pUserInfoData->info.bwValue,
                pInfoData->info.bwValue);
            break;

        default:
            break;
        }
    }
#endif
}
#endif

// 32-bit addition with overflow check.  The boolean flag pointed to by
// overflowFlag is set to true if overflow is detected; otherwise it is left
// unmodified.
static uint32 add_overflow_check(uint32 a, uint32 b, boolean* overflowFlag)
{
    uint32 result = a + b;
    if(((UINT_MAX - a) < b) && (overflowFlag != NULL))
        *overflowFlag = TRUE;
    return result;
}

// 32-bit multiplication with overflow check.  The boolean flag pointed to by
// overflowFlag is set to true if overflow is detected; otherwise it is left
// unmodified.
static uint32 mult_overflow_check(uint32 a, uint32 b, boolean* overflowFlag)
{
    uint32 result = a * b;
    if(((UINT_MAX / a) < b) && (overflowFlag != NULL))
        *overflowFlag = TRUE;
    return result;
}

// Alignment of an integer to the next multiple of 8, with overflow check.  The
// boolean flag pointed to by overflowFlag is set to true if overflow is
// detected; otherwise it is left unmodified.
static uint32 align8_overflow_check(uint32 a, boolean* overflowFlag)
{
    uint32 result = ADSPPM_ALIGN8(a);
    if(((UINT_MAX - 7) < a) && (overflowFlag != NULL))
        *overflowFlag = TRUE;
    return result;
}


// Calculate the size of the request while taking into account padding
// needed to maintain the memory address alignment for the various structures
// in the request.
static uint32 calculate_padded_request_size(MmpmRscExtParamType *pUserReq,
    boolean* overflowFlag)
{
    uint32 size = 0, i;

    if(NULL != pUserReq)
    {
        // Top level structure
        size = add_overflow_check(size,
            align8_overflow_check(sizeof(MmpmRscExtParamType), overflowFlag),
            overflowFlag);

        if(NULL != pUserReq->pReqArray)
        {
            // Request array
            size = add_overflow_check(size,
                align8_overflow_check(mult_overflow_check(pUserReq->numOfReq,
                sizeof(MmpmRscParamType), overflowFlag), overflowFlag),
                overflowFlag);
            // Loop through all requests
            for(i = 0; i < pUserReq->numOfReq; i++)
            {
                switch(pUserReq->pReqArray[i].rscId)
                {
                case MMPM_RSC_ID_MIPS_EXT:
                    size = add_overflow_check(size,
                        align8_overflow_check(sizeof(MmpmMipsReqType),
                        overflowFlag), overflowFlag);
                    break;
                case MMPM_RSC_ID_GENERIC_BW:
                    size = add_overflow_check(size,
                        align8_overflow_check(sizeof(MmpmGenBwReqType),
                        overflowFlag), overflowFlag);
                    if(NULL != pUserReq->pReqArray[i].rscParam.pGenBwReq)
                    {
                        if(NULL != pUserReq->pReqArray[i].rscParam.pGenBwReq->pBandWidthArray)
                        {
                            size = add_overflow_check(size,
                                align8_overflow_check(mult_overflow_check(
                                pUserReq->pReqArray[i].rscParam.pGenBwReq->numOfBw,
                                sizeof(MmpmGenBwValType), overflowFlag),
                                overflowFlag), overflowFlag);
                        }
                    }
                    break;
                default:
                    break;
                }
            }
        }
    }
    return size;
}


static int copy_request_data(int client_handle, MmpmRscExtParamType *pUserReq, MmpmRscExtParamType *pReqData)
{
    int i, result = 0;
    unsigned char *pData = (unsigned char *)pReqData;

    if(NULL != pUserReq)
    {
        // Top level structure
        result = qurt_qdi_copy_from_user(
            client_handle,
            (void *)pReqData,
            (void *)pUserReq,
            sizeof(MmpmRscExtParamType));
        pData += ADSPPM_ALIGN8(sizeof(MmpmRscExtParamType));

        if((result >= 0) && (NULL != pUserReq->pReqArray))
        {
            // Set pointer to the flat structure
            pReqData->pReqArray = (MmpmRscParamType *)pData;
            result = qurt_qdi_copy_from_user(
                client_handle,
                (void *)pReqData->pReqArray,
                (void *)pUserReq->pReqArray,
                pUserReq->numOfReq * sizeof(MmpmRscParamType));
            // Request structure array
            pData += ADSPPM_ALIGN8(pUserReq->numOfReq * sizeof(MmpmRscParamType));

            // Loop through all requests
            if(result >= 0)
            {
                for(i = 0; i < pUserReq->numOfReq; i++)
                {
                    switch(pUserReq->pReqArray[i].rscId)
                    {
                    case MMPM_RSC_ID_MIPS_EXT:
                        if(NULL != pUserReq->pReqArray[i].rscParam.pMipsExt)
                        {
                            pReqData->pReqArray[i].rscParam.pMipsExt = (MmpmMipsReqType *)pData;
                            result = qurt_qdi_copy_from_user(
                                client_handle,
                                (void *)(pReqData->pReqArray[i].rscParam.pMipsExt),
                                (void *)(pUserReq->pReqArray[i].rscParam.pMipsExt),
                                sizeof(MmpmMipsReqType));
                            pData += ADSPPM_ALIGN8(sizeof(MmpmMipsReqType));
                        }
                        break;

                    case MMPM_RSC_ID_GENERIC_BW:
                        pReqData->pReqArray[i].rscParam.pGenBwReq = (MmpmGenBwReqType *)pData;
                        if(NULL != pUserReq->pReqArray[i].rscParam.pGenBwReq)
                        {
                            if(NULL != pUserReq->pReqArray[i].rscParam.pGenBwReq->pBandWidthArray)
                            {
                                result = qurt_qdi_copy_from_user(
                                    client_handle,
                                    (void *)(pReqData->pReqArray[i].rscParam.pGenBwReq),
                                    (void *)(pUserReq->pReqArray[i].rscParam.pGenBwReq),
                                    sizeof(MmpmGenBwReqType));
                                pData += ADSPPM_ALIGN8(sizeof(MmpmGenBwReqType));
                                pReqData->pReqArray[i].rscParam.pGenBwReq->pBandWidthArray = (MmpmGenBwValType *)pData;
                                if(result >= 0)
                                {
                                    result = qurt_qdi_copy_from_user(
                                        client_handle,
                                        (void *)(pReqData->pReqArray[i].rscParam.pGenBwReq->pBandWidthArray),
                                        (void *)(pUserReq->pReqArray[i].rscParam.pGenBwReq->pBandWidthArray),
                                        pUserReq->pReqArray[i].rscParam.pGenBwReq->numOfBw * sizeof(MmpmGenBwValType));
                                    pData += ADSPPM_ALIGN8(
                                        pUserReq->pReqArray[i].rscParam.pGenBwReq->numOfBw *
                                        sizeof(MmpmGenBwValType));
                                }
                            }
                        }
                        break;

                    default:
                        break;
                    }

                    if(result < 0)
                    {
                        break;
                    }
                }
            }
        }
        print_request_data(client_handle, pUserReq, pReqData);
    }
    return result;
}


static MmpmRscExtParamType *get_request_data_from_user(
    int client_handle,
    MmpmRscExtParamType *pUserReq)
{
    MmpmRscExtParamType *pData = NULL;
    boolean overflowFlag = FALSE;
    uint32 size = calculate_padded_request_size(pUserReq, &overflowFlag);
    if(overflowFlag)
    {
        ULOG_RT_PRINTF_0(gAdsppm.hLog,
            "detected integer overflow while handling request");
    }
    if((size > sizeof(MmpmRscExtParamType)) && !overflowFlag)
    {
        pData = malloc(size);
        if(NULL != pData)
        {
            if(0 > copy_request_data(client_handle, pUserReq, pData))
            {
                free(pData);
                pData = NULL;
            }
        }
    }
    return pData;
}


static int copy_request_status_to_user(int client_handle, MmpmRscExtParamType *pReqData, MmpmRscExtParamType *pUserReq)
{
    int ret = -1;
    if((NULL != pReqData) && (NULL != pUserReq))
    {
        if((NULL != pReqData->pStsArray) && (NULL != pUserReq->pStsArray))
        {
            ret = qurt_qdi_copy_to_user(
                client_handle,
                (void *)pUserReq->pStsArray,
                (void *)pReqData->pStsArray,
                pReqData->numOfReq * sizeof(MMPM_STATUS));
        }
    }
    return ret;
}


#ifdef ADSPPM_TEST
// Calculate the size of the GetInfo request while taking into account padding
// needed to maintain the memory address alignment for the various structures
// in the request.
static int calculate_getinfo_padded_size(MmpmInfoDataType *pGetInfoData)
{
    int size = 0;
    if(NULL != pGetInfoData)
    {
        // Top level structure
        size += ADSPPM_ALIGN8(sizeof(MmpmInfoDataType));

        switch(pGetInfoData->infoId)
        {
        case MMPM_INFO_ID_CLK_FREQ:
        case MMPM_INFO_ID_CORE_CLK_MAX:
            // Padding for alignment not necessary if it's the last part
            // of the request, but do it anyway for consistency
            size += ADSPPM_ALIGN8(sizeof(MmpmInfoClkFreqType));
            break;

        case MMPM_INFO_ID_MIPS_MAX:
            break;

        case MMPM_INFO_ID_BW_MAX:
            break;

        default:
            break;

        }
    }
    return size;
}


static int copy_getinfo_data_from_user(int client_handle, MmpmInfoDataType *pUserInfoData, MmpmInfoDataType *pInfoData)
{
    int result = 0;
    unsigned char *pData = (unsigned char *)pInfoData;
    if(NULL != pUserInfoData)
    {
        // Top level structure
        result = qurt_qdi_copy_from_user(
            client_handle,
            (void *)pInfoData,
            (void *)pUserInfoData,
            sizeof(MmpmInfoDataType));
        pData += ADSPPM_ALIGN8(sizeof(MmpmInfoDataType));
        if(result >= 0)
        {
            switch(pUserInfoData->infoId)
            {
            case MMPM_INFO_ID_CLK_FREQ:
            case MMPM_INFO_ID_CORE_CLK_MAX:
                if(NULL != pUserInfoData->info.pInfoClkFreqType)
                {
                    pInfoData->info.pInfoClkFreqType = (MmpmInfoClkFreqType *)pData;
                    result = qurt_qdi_copy_from_user(
                        client_handle,
                        (void *)(pInfoData->info.pInfoClkFreqType),
                        (void *)(pUserInfoData->info.pInfoClkFreqType),
                        sizeof(MmpmInfoClkFreqType));
                    pData += ADSPPM_ALIGN8(sizeof(MmpmInfoClkFreqType));
                }
                break;

            case MMPM_INFO_ID_MIPS_MAX:
                break;

            case MMPM_INFO_ID_BW_MAX:
                break;

            default:
                break;
            }
        }
        print_getinfo_data(pUserInfoData, pInfoData);
    }
    return result;
}


static int copy_getinfo_data_to_user(int client_handle, MmpmInfoDataType *pUserInfoData, MmpmInfoDataType *pInfoData)
{
    int result = -1;
    if((NULL != pUserInfoData) && (NULL != pInfoData))
    {
        switch(pInfoData->infoId)
        {
        case MMPM_INFO_ID_CLK_FREQ:
        case MMPM_INFO_ID_CORE_CLK_MAX:
            if((NULL != pUserInfoData->info.pInfoClkFreqType) &&
                (NULL != pInfoData->info.pInfoClkFreqType))
            {
                result = qurt_qdi_copy_to_user(
                    client_handle,
                    (void *)(pUserInfoData->info.pInfoClkFreqType),
                    (void *)(pInfoData->info.pInfoClkFreqType),
                    sizeof(MmpmInfoClkFreqType));
            }
            break;

        case MMPM_INFO_ID_MIPS_MAX:
            result = qurt_qdi_copy_to_user(
                client_handle,
                (void *)&(pUserInfoData->info.mipsValue),
                (void *)&(pInfoData->info.mipsValue),
                sizeof(uint32));
            break;

        case MMPM_INFO_ID_BW_MAX:
            result = qurt_qdi_copy_to_user(
                client_handle,
                (void *)&(pUserInfoData->info.bwValue),
                (void *)&(pInfoData->info.bwValue),
                sizeof(uint64));
            break;

        default:
            break;
        }
        print_getinfo_data(pUserInfoData, pInfoData);
    }
    return result;
}


static MmpmInfoDataType *get_getinfo_data_from_user(int client_handle, MmpmInfoDataType *pUserInfoData)
{
    MmpmInfoDataType *pData = NULL;
    int size = calculate_getinfo_padded_size(pUserInfoData);
    if(size > sizeof(MmpmInfoDataType))
    {
        pData = malloc(size);
        if(NULL != pData)
        {
            if(0 > copy_getinfo_data_from_user(client_handle, pUserInfoData, pData))
            {
                free(pData);
                pData = NULL;
            }
        }
    }
    return pData;
}
#endif


void QDI_Adsppm_do_callback(QDI_Adsppm_cbinfo_t *ptr, MmpmCallbackParamType *pValue)
{
    QDI_Adsppm_CbqElem_t *cbq;
    MmpmCompletionCallbackDataType *pReturnCallbackValue =
        (MmpmCompletionCallbackDataType *)pValue->callbackData;
    cbq = malloc(sizeof(QDI_Adsppm_CbqElem_t));
    if(cbq)
    {
        cbq->cbinfo = ptr;
        cbq->callbackParam.eventId = pValue->eventId;
        cbq->callbackParam.clientId = pValue->clientId;
        cbq->callbackParam.callbackDataSize = pValue->callbackDataSize;
        cbq->callbackParam.callbackData = &cbq->callbackValue;
        cbq->callbackValue.reqTag = pReturnCallbackValue->reqTag;
        cbq->callbackValue.result = pReturnCallbackValue->result;
        qurt_rmutex_lock(&(ptr->object->mtx));
        cbq->next = ptr->object->cbqueue;
        ptr->object->cbqueue = cbq;
        qurt_rmutex_unlock(&(ptr->object->mtx));
        qurt_signal_set(&ptr->object->sigLocal, 1);
    }
}


static int QDI_Adsppm_get_callback(int client_handle, QDI_Adsppm_Ctx_t *pCbDrv, void *buf)
{
    Adsppm_cbinfo_Client_t info;
    QDI_Adsppm_CbqElem_t *cbq = NULL;
    QDI_Adsppm_CbqElem_t *ptr = NULL;
    QDI_Adsppm_CbqElem_t *prev = NULL;
    unsigned signalRead;
    int result = QURT_EOK;

    if(buf == NULL)
        return -1;

    for(;;)
    {
        qurt_rmutex_lock(&pCbDrv->mtx);
        ptr = pCbDrv->cbqueue;
        while(ptr)
        {
            // Remember non-NULL item
            prev = cbq;
            cbq = ptr;
            // Get to the end of the list
            ptr = ptr->next;
        }
        if(cbq)
        {
            if(prev)
            {
                prev->next = NULL;
            }
            else
            {
                // Removing the only element from the queue
                pCbDrv->cbqueue = NULL;
            }
            qurt_rmutex_unlock(&pCbDrv->mtx);
            break;
        }
        else
        {
            qurt_rmutex_unlock(&pCbDrv->mtx);
            result = qurt_signal_wait_cancellable(
                &pCbDrv->sigLocal, 1, QURT_SIGNAL_ATTR_WAIT_ANY, &signalRead);
            if(QURT_ECANCEL == result)
            {
                ULOG_RT_PRINTF_1(gAdsppm.hLog,
                    "%s: signal wait is cancelled", __FUNCTION__);
                break;
            }
            else
            {
                qurt_signal_clear(&pCbDrv->sigLocal, signalRead);
            }
        }
    }

    if(QURT_EOK == result)
    {
        info.pfn = cbq->cbinfo->pfn;
        // Pointer to the data should be substituted on the client's side
        info.callbackParam.callbackDataSize = cbq->callbackParam.callbackDataSize;
        info.callbackParam.clientId = cbq->callbackParam.clientId;
        info.callbackParam.eventId = cbq->callbackParam.eventId;
        info.callbackValue.reqTag = cbq->callbackValue.reqTag;
        info.callbackValue.result = cbq->callbackValue.result;
        result = qurt_qdi_copy_to_user(client_handle, buf, &info, sizeof(info));
    }
    else
    {
        // Wait on signal probably got cancelled.
        // Clear the data to be returned to the caller, to be safe.
        memset(&info, 0, sizeof(Adsppm_cbinfo_Client_t));
        qurt_qdi_copy_to_user(client_handle, buf, &info, sizeof(Adsppm_cbinfo_Client_t));
    }

    if(NULL != cbq)
        free(cbq);

    return result;
}


void QDI_Adsppm_Release(qurt_qdi_obj_t *qdiobj)
{
    Adsppmcb_Deinit_Srv();
    memset(&gAdsppm, 0, sizeof(QDI_Adsppm_Ctx_t));
}


int QDI_Adsppm_Invocation(
    int client_handle,
    qurt_qdi_obj_t *obj,
    int method,
    qurt_qdi_arg_t a1,
    qurt_qdi_arg_t a2,
    qurt_qdi_arg_t a3,
    qurt_qdi_arg_t a4,
    qurt_qdi_arg_t a5,
    qurt_qdi_arg_t a6,
    qurt_qdi_arg_t a7,
    qurt_qdi_arg_t a8,
    qurt_qdi_arg_t a9)
{
    QDI_Adsppm_cbinfo_t CbInfo;
    int result = 0;
    MmpmStatusType ret = MMPM_STATUS_SUCCESS;
    MmpmRegParamType *pRegParam = NULL;
    uint32 clientId = 0;
    MmpmRscExtParamType *pRscExtParam = NULL;
#ifdef ADSPPM_TEST
    MmpmInfoDataType *pInfoData = NULL;
#endif

    gAdsppm.qdiobj = *obj;

    switch(method)
    {
    case QDI_OPEN:
        gAdsppm.qdiobj.invoke = QDI_Adsppm_Invocation;
        gAdsppm.qdiobj.refcnt = QDI_REFCNT_INIT;
        gAdsppm.qdiobj.release = QDI_Adsppm_Release;

        gAdsppm.cbqueue = NULL;
        qurt_rmutex_init(&gAdsppm.mtx);
        qurt_signal_init(&gAdsppm.sigLocal);
        result = qurt_qdi_new_handle_from_obj_t(client_handle, &gAdsppm.qdiobj);
        break;

    case ADSPPM_REGISTER:
        pRegParam = malloc(sizeof(MmpmRegParamType));
        if(NULL != pRegParam)
        {
            result = qurt_qdi_copy_from_user(
                client_handle,
                (void *)pRegParam,
                (void *)a1.ptr,
                sizeof(MmpmRegParamType));
            if(0 > result)
            {
                result = -1;
            }
            else
            {
                if(CALLBACK_NONE != pRegParam->callBackFlag)
                {
                    CbInfo.object = &gAdsppm;
                    CbInfo.pfn = pRegParam->MMPM_Callback; // Call back function
                    pRegParam->MMPM_Callback = Adsppmcb_notify_callbacks_Srv;
                }
                clientId =  MMPM_Register_Ext(pRegParam);
                result = qurt_qdi_copy_to_user(
                    client_handle,
                    (void *)a2.ptr,
                    (void *)&clientId,
                    sizeof(clientId));
                if(clientId != 0)
                {
                    CbInfo.clientId = clientId;
                    Adsppmcb_register_callback_Srv(&CbInfo);
                }
                else
                {
                    result = -1;
                }
            }
            free(pRegParam);
        }
        else
        {
            result = -1;
        }
        break;

    case ADSPPM_REQUEST:
        pRscExtParam = get_request_data_from_user(
            client_handle,
            (MmpmRscExtParamType *)a2.ptr);
        if(NULL != pRscExtParam)
        {
            ret = MMPM_Request_Ext(a1.num, pRscExtParam);
            result = qurt_qdi_copy_to_user(
                client_handle,
                (void *)a3.ptr,
                (void *)&ret,
                sizeof(ret));
            if(result >= 0)
            {
                // Copy status array to user
                result = copy_request_status_to_user(
                    client_handle,
                    pRscExtParam,
                    (MmpmRscExtParamType *)a2.ptr);
            }
            if(MMPM_STATUS_SUCCESS != ret)
            {
                result = -1;
            }
            free(pRscExtParam);
        }
        else
        {
            result = -1;
        }
        break;

    case ADSPPM_RELEASE:
        pRscExtParam = malloc(sizeof(MmpmRscExtParamType));
        if(NULL != pRscExtParam)
        {
            result = qurt_qdi_copy_from_user(
                client_handle,
                (void *)pRscExtParam,
                (void *)a2.ptr,
                sizeof(MmpmRscExtParamType));
            if(result < 0)
            {
                result = -1;
            }
            else
            {
                ret = MMPM_Release_Ext(a1.num, pRscExtParam);
                result = qurt_qdi_copy_to_user(
                    client_handle,
                    (void *)a3.ptr,
                    (void *)&ret,
                    sizeof(ret));
                if(MMPM_STATUS_SUCCESS != ret)
                {
                    result = -1;
                }
            }
            free(pRscExtParam);
        }
        else
        {
            result = -1;
        }
        break;

    case ADSPPM_DEREGISTER:
        Adsppmcb_deRegister_callback_Srv(a1.num);
        ret = MMPM_Deregister_Ext(a1.num);
        result = qurt_qdi_copy_to_user(
            client_handle,
            (void *)a2.ptr,
            (void *)&ret,
            sizeof(ret));
        if(MMPM_STATUS_SUCCESS != ret)
        {
            result = -1;
        }
        break;

#ifdef ADSPPM_TEST
    case ADSPPM_GET_INFO:
        pInfoData = get_getinfo_data_from_user(
            client_handle,
            (MmpmInfoDataType *)a1.ptr);
        if(NULL != pInfoData)
        {
            ret = MMPM_GetInfo(pInfoData);
            result = qurt_qdi_copy_to_user(
                client_handle,
                (void *)a2.ptr,
                (void *)&ret,
                sizeof(ret));
            if(MMPM_STATUS_SUCCESS != ret)
            {
                result = -1;
            }
            else
            {
                result = copy_getinfo_data_to_user(
                    client_handle,
                    (MmpmInfoDataType *)a1.ptr,
                    pInfoData);
            }
            free(pInfoData);
        }
        else
        {
            result = -1;
        }
        break;
#endif

    case ADSPPM_GET_CB:
        result = QDI_Adsppm_get_callback(client_handle, &gAdsppm, a1.ptr);
        break;

    default:
        result = qurt_qdi_method_default(
            client_handle, obj, method,
            a1, a2, a3, a4, a5, a6, a7, a8, a9);
        break;
    }
    return result;
}


const QDI_Adsppm_Opener_t adsppm_opener =
{
    {
        QDI_Adsppm_Invocation,
        QDI_REFCNT_PERM,
        0
    } // 0 as object is PERM, never released
};


/**
 * @fn QDI_adsppm_init - registers driver with QDI framework
 * @brief This function is called by RCINIT after ADSPPM is initialized.
 */
void QDI_adsppm_init(void)
{
    gAdsppm.log_buffer_size = ADSPPM_QDIULOG_BUFFER_SIZE;
    ULogFront_RealTimeInit(
        &gAdsppm.hLog,
        "ADSPPM QDISRV Log",
        gAdsppm.log_buffer_size,
        ULOG_MEMORY_LOCAL,
        ULOG_LOCK_OS);
    Adsppmcb_Init_Server();
    qurt_qdi_devname_register(ADSPPM_QDI_DRV_NAME, (void *)&adsppm_opener);
}

