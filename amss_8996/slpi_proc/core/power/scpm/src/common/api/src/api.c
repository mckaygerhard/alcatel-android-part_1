/*==============================================================================

FILE:      api.c

DESCRIPTION: MMPM API wrapper

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A



* Copyright (c) 2013-2015 Qualcomm Technologies, Inc.
* All Rights Reserved.

* Qualcomm Technologies, Inc. Confidential and Proprietary.

==============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //components/rel/core.slpi/1.0/power/scpm/src/common/api/src/api.c#1 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ---------------------------------------------------------------
04/03/12   alisonc      Initial version

===================================================================================*/

#include "mmpm.h"
#include "adsppm.h"
#include "adsppm_utils.h"
#include "stdlib.h"
#include "core.h"
#include "asyncmgr.h"
#include "coreUtils.h"
#include "asic.h"


// This global flag set to 1 to disable MMPM_Request_Ext at runtime
// Returns only MMPM_STATUS_SUCCESS
static volatile uint32 gDISABLEADSPPMREQUESTFLAG = 0;

extern uint32 gADSPPMINITSTATE;

#ifdef ADSPPM_TEST
static const AdsppmClkIdType mmpmToAdsppmClk[MMPM_CLK_ID_LPASS_ENUM_MAX] =
{
    AdsppmClk_None,
    AdsppmClk_HwRsp_Core,
    AdsppmClk_Midi_Core,
    AdsppmClk_AvSync_Xo,
    AdsppmClk_AvSync_Bt,
    AdsppmClk_AvSync_Fm,
    AdsppmClk_Slimbus_Core,
    AdsppmClk_Avtimer_core,
    AdsppmClk_Atime_core,
    AdsppmClk_Atime2_core,
    AdsppmClk_Adsp_Core,
    AdsppmClk_Ahb_Root
};
#endif

static const AdsppmBusPortIdType mmpmToAdsppmBusPort[MMPM_BW_PORT_ID_MAX] =
{
    AdsppmBusPort_None,
    AdsppmBusPort_Adsp_Master,
    AdsppmBusPort_Dml_Master,
    AdsppmBusPort_Aif_Master,
    AdsppmBusPort_Slimbus_Master,
    AdsppmBusPort_Midi_Master,
    AdsppmBusPort_HwRsmp_Master,
    AdsppmBusPort_Ext_Ahb_Master,
    AdsppmBusPort_Spdif_Master,
    AdsppmBusPort_Hdmirx_Master,
    AdsppmBusPort_Sif_Master,
    AdsppmBusPort_Dml_Slave,
    AdsppmBusPort_Aif_Slave,
    AdsppmBusPort_Slimbus_Slave,
    AdsppmBusPort_Midi_Slave,
    AdsppmBusPort_HwRsmp_Slave,
    AdsppmBusPort_AvSync_Slave,
    AdsppmBusPort_Lpm_Slave,
    AdsppmBusPort_Sram_Slave,
    AdsppmBusPort_Ext_Ahb_Slave,
    AdsppmBusPort_Ddr_Slave,
    AdsppmBusPort_Ocmem_Slave,
    AdsppmBusPort_PerifNoc_Slave,
    AdsppmBusPort_Spdif_Slave,
    AdsppmBusPort_Hdmirx_Slave,
    AdsppmBusPort_Sif_Slave,
    AdsppmBusPort_Bstc_Slave,
    AdsppmBusPort_Dcodec_Slave,
    AdsppmBusPort_Core,
};

/**
 * @const busPortName - defines Bus ports name to be used by MMPM ULOG internally
 *                      keep updated with MmpmBwPortIdType in mmpm.h
 */
const char *MMPMBusPortName[MMPM_BW_PORT_ID_MAX] =
{
    "NONE",
    "ADSP_MASTER",
    "DML_MASTER",
    "AIF_MASTER",
    "SLIMBUS_MASTER",
    "MIDI_MASTER",
    "HWRSMP_MASTER",
    "EXT_AHB_MASTER",
    "SPDIF_MASTER",
    "HDMI_RX_MASTER",
    "SIF_MASTER",
    "DML_SLAVE",
    "AIF_SLAVE",
    "SLIMBUS_SLAVE",
    "MIDI_SLAVE",
    "HWRSMP_SLAVE",
    "AVSYNC_SLAVE",
    "LPM_SLAVE",
    "SRAM_SLAVE",
    "EXT_AHB_SLAVE",
    "DDR_SLAVE",
    "OCMEM_SLAVE",
    "PERIFNOC_SLAVE",
    "SPDIF_SLAVE",
    "HDMI_RX_SLAVE",
    "SIF_SLAVE",
    "BSTC_SLAVE",
    "DCODEC_SLAVE",
    "CORE",
};


MMPM_STATUS apiProcessSingleRequest(AdsppmRscIdType adsppmRscId, MmpmApiType apiType, uint32 *pAsyncRscMap)
{
    MMPM_STATUS sts = MMPM_STATUS_SUCCESS;
    *pAsyncRscMap |= (1<<adsppmRscId);
    if(MMPM_API_TYPE_ASYNC != apiType)
    {
        sts = (MMPM_STATUS) RM_RequestResource(adsppmRscId);
    }
    return sts;
}


MMPM_STATUS apiReleaseSingleResource(uint32 clientId, AdsppmRscIdType adsppmRscId, uint32 extData, MmpmApiType apiType, uint32 *asyncRscMap)
{
    MMPM_STATUS sts = (MMPM_STATUS) RM_ReleaseRequest(clientId, adsppmRscId, extData);
    switch(sts)
    {
    case MMPM_STATUS_SUCCESS:
        sts = apiProcessSingleRequest(adsppmRscId, apiType, asyncRscMap);
        break;

    case MMPM_STATUS_RESOURCENOTREQUESTED:
        sts = MMPM_STATUS_SUCCESS;
        break;

    default:
        break;
    }
    return sts;
}


/******************************************************************************
 *                     MMPM_Register_Ext
 *****************************************************************************/
uint32 MMPM_Register_Ext(MmpmRegParamType *pRegParam)
{
    MMPM_STATUS sts = MMPM_STATUS_SUCCESS;
    uint32 clientId = 0;
    ADSPPM_LOG_FUNC_ENTER;
    ADSPPM_LOG_FUNC_PROFILESTART;

    if(!gDISABLEADSPPMREQUESTFLAG)
    {
        if(ADSPPM_IsInitialized())
        {
            if(NULL == pRegParam)
            {
                sts = MMPM_STATUS_NULLPOINTER;
            }
            else
            {
                if((MMPM_CORE_ID_LPASS_START >= pRegParam->coreId) ||
                    (MMPM_CORE_ID_LPASS_END <= pRegParam->coreId) ||
                    (NULL == pRegParam->pClientName) ||
                    (MAX_ADSPPM_CLIENT_NAME <= strlen(pRegParam->pClientName)) ||
                    (MMPM_CORE_INSTANCE_NONE == pRegParam->instanceId) ||
                    (MMPM_CORE_INSTANCE_MAX <= pRegParam->instanceId))
                {
                    sts = MMPM_STATUS_BADPARM;
                    ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                        "bad parameters or null client name or client name is too long, coreId=%u",
                        pRegParam->coreId);
                }
                else
                {
                    AdsppmClientTypeType clientType =
                        ((pRegParam->pwrCtrlFlag & PWR_CTRL_PERIODIC_CLIENT) == PWR_CTRL_PERIODIC_CLIENT)?
                        Adsppm_ClientType_Periodic:Adsppm_ClientType_Normal;

                    // Keep MMPM core ID and ADSPPM core id enum definition identical
                    clientId = RM_CreateClient(
                        (AdsppmCoreIdType) (pRegParam->coreId - MMPM_CORE_ID_LPASS_START),
                        (AdsppmInstanceIdType) (pRegParam->instanceId),
                        pRegParam->pClientName,
                        (void *) pRegParam->MMPM_Callback,
                        clientType);
                }
            }
        }
        else
        {
            sts = MMPM_STATUS_NOTINITIALIZED;
            ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "MMPM not initialized");
        }
    }
    else
    {
        return 1;
    }

    ADSPPM_LOG_FUNC_PROFILEEND;
    ADSPPM_LOG_FUNC_EXIT(sts);
    return clientId;
}


/******************************************************************************
 *                     MMPM_Deregister_Ext
 *****************************************************************************/
MMPM_STATUS MMPM_Deregister_Ext(uint32 clientId)
{
    MMPM_STATUS sts = MMPM_STATUS_NOTINITIALIZED;
    ADSPPM_LOG_FUNC_ENTER;
    ADSPPM_LOG_FUNC_PROFILESTART;

    if(!gDISABLEADSPPMREQUESTFLAG)
    {
        if(ADSPPM_IsInitialized())
        {
            if(clientId)
            {
                sts = (MMPM_STATUS) RM_DeleteClient(clientId);
                if(MMPM_STATUS_SUCCESS == sts)
                {
                    AdsppmRscIdType rsc;
                    for(rsc = Adsppm_Rsc_Id_Power; rsc < Adsppm_Rsc_Id_Max; rsc++)
                    {
                        sts = (MMPM_STATUS) RM_RequestResource(rsc);
                        if(MMPM_STATUS_SUCCESS != sts)
                        {
                            break;
                        }
                    }
                }
            }
            else
            {
                sts = MMPM_STATUS_BADPARM;
                ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                    "bad clientid in deregister=%u", clientId);
            }
        }
        else
        {
            sts = MMPM_STATUS_NOTINITIALIZED;
            ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "Not initialized");
        }
    }
    else
    {
        sts = MMPM_STATUS_SUCCESS;
    }

    ADSPPM_LOG_FUNC_PROFILEEND;
    ADSPPM_LOG_FUNC_EXIT(sts);
    return sts;
}


MMPM_STATUS apiProcessBwRequest(uint32 clientId, MmpmRscIdType resourceId, MmpmGenBwReqType *pGenBwReq, MmpmApiType apiType, uint32 *pAsyncRscMap)
{
    MMPM_STATUS sts = MMPM_STATUS_SUCCESS;
    AdsppmBwReqType adsppmBwReqData;
    uint32 bwNum;

    ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_INFO,
        "Client %u: BW request", clientId);

    if(MAX_NUM_OF_ADSPPM_BW_ARRAY < pGenBwReq->numOfBw)
    {
        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
            "Number of BW entries (%u) is too big",
            pGenBwReq->numOfBw);
        sts = MMPM_STATUS_BADPARM;
    }
    else
    {
        memset(&adsppmBwReqData, 0, sizeof(AdsppmBwReqType));
        adsppmBwReqData.numOfBw = pGenBwReq->numOfBw;
        // Check Port ID and translate port ID to adsppm
        for(bwNum = 0; bwNum < pGenBwReq->numOfBw; bwNum++)
        {
            MmpmGenBwValType *pBwInfo = &pGenBwReq->pBandWidthArray[bwNum];
            if((MMPM_BW_PORT_ID_NONE == pBwInfo->busRoute.masterPort) ||
                (MMPM_BW_PORT_ID_DML_SLAVE <= pBwInfo->busRoute.masterPort) ||
                (MMPM_BW_PORT_ID_DML_SLAVE > pBwInfo->busRoute.slavePort) ||
                (MMPM_BW_PORT_ID_CORE <= pBwInfo->busRoute.slavePort) ||
                (MMPM_BW_USAGE_LPASS_NONE == pBwInfo->bwValue.busBwValue.usageType) ||
                (MMPM_BW_USAGE_LPASS_ENUM_MAX <= pBwInfo->bwValue.busBwValue.usageType))
            {
                sts = MMPM_STATUS_BADPARM;
                ADSPPM_LOG_PRINTF_3(ADSPPM_LOG_LEVEL_ERROR,
                    "BW route or usageType is invalid: master=%u, slave=%u, usageType=%u",
                    pBwInfo->busRoute.masterPort,
                    pBwInfo->busRoute.slavePort,
                    pBwInfo->bwValue.busBwValue.usageType);
                break;
            }
            else
            {
                adsppmBwReqData.pBwArray[bwNum].busRoute.masterPort =
                    mmpmToAdsppmBusPort[pBwInfo->busRoute.masterPort];
                adsppmBwReqData.pBwArray[bwNum].busRoute.slavePort =
                    mmpmToAdsppmBusPort[pBwInfo->busRoute.slavePort];
                adsppmBwReqData.pBwArray[bwNum].bwValue.busBwValue.bwBytePerSec =
                    pBwInfo->bwValue.busBwValue.bwBytePerSec;
                adsppmBwReqData.pBwArray[bwNum].bwValue.busBwValue.usagePercentage =
                    pBwInfo->bwValue.busBwValue.usagePercentage;
                adsppmBwReqData.pBwArray[bwNum].bwValue.busBwValue.usageType =
                    (AdsppmBwUsageLpassType) pBwInfo->bwValue.busBwValue.usageType;
                adsppmBwReqData.pBwArray[bwNum].bwOperation = AdsppmBusBWOperation_BW;

                ADSPPM_LOG_PRINTF_5(ADSPPM_LOG_LEVEL_INFO,
                    "    MMPM   M=%s(%u), S=%s(%u), Operation=%u",
                    MMPMBusPortName[pBwInfo->busRoute.masterPort],
                    pBwInfo->busRoute.masterPort,
                    MMPMBusPortName[pBwInfo->busRoute.slavePort],
                    pBwInfo->busRoute.slavePort,
                    adsppmBwReqData.pBwArray[bwNum].bwOperation);
                ADSPPM_LOG_PRINTF_5(ADSPPM_LOG_LEVEL_INFO,
                    "    ADSPPM M=%s(%u), S=%s(%u), Operation=%u",
                    adsppm_getBusPortName(adsppmBwReqData.pBwArray[bwNum].busRoute.masterPort),
                    adsppmBwReqData.pBwArray[bwNum].busRoute.masterPort,
                    adsppm_getBusPortName(adsppmBwReqData.pBwArray[bwNum].busRoute.slavePort),
                    adsppmBwReqData.pBwArray[bwNum].busRoute.slavePort,
                    adsppmBwReqData.pBwArray[bwNum].bwOperation);
                ADSPPM_LOG_PRINTF_3(ADSPPM_LOG_LEVEL_INFO,
                    "    BW=%u at %u, usage=%u",
                    (uint32) adsppmBwReqData.pBwArray[bwNum].bwValue.busBwValue.bwBytePerSec,
                    adsppmBwReqData.pBwArray[bwNum].bwValue.busBwValue.usagePercentage,
                    adsppmBwReqData.pBwArray[bwNum].bwValue.busBwValue.usageType);
                ADSPPM_QDSS_EVENT_5(ADSPPM_API_BW_REQ,
                    clientId,
                    adsppmBwReqData.pBwArray[bwNum].busRoute.masterPort,
                    adsppmBwReqData.pBwArray[bwNum].busRoute.slavePort,
                    (uint32) adsppmBwReqData.pBwArray[bwNum].bwValue.busBwValue.bwBytePerSec,
                    adsppmBwReqData.pBwArray[bwNum].bwValue.busBwValue.usagePercentage);
            }
        }
        if(MMPM_STATUS_SUCCESS == sts)
        {
            sts = (MMPM_STATUS) RM_AddRequest(clientId, Adsppm_Rsc_Id_BW, &adsppmBwReqData, 0);
        }
    }
    return sts;
}


/******************************************************************************
 *                     MMPM_Request_Ext
 *****************************************************************************/
MMPM_STATUS MMPM_Request_Ext(uint32 clientId, MmpmRscExtParamType *pRscExtParam)
{
    MMPM_STATUS sts = MMPM_STATUS_SUCCESS;
    AdsppmRscIdType adsppmRscId = Adsppm_Rsc_Id_None;
    ADSPPM_LOG_FUNC_ENTER;
    ADSPPM_LOG_FUNC_PROFILESTART;

    if(!gDISABLEADSPPMREQUESTFLAG)
    {
        if(ADSPPM_IsInitialized())
        {
            if((NULL == pRscExtParam) || (0 == clientId))
            {
                sts = MMPM_STATUS_BADPARM;
                ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                    "null pointer of pRscExtParam or clientid is 0, clientid=%u", clientId);
            }
            else
            {
                if((NULL == pRscExtParam->pReqArray) || (NULL == pRscExtParam->pStsArray))
                {
                    sts = MMPM_STATUS_NULLPOINTER;
                }
                else
                {
                    if(Adsppm_Rsc_Id_Max > pRscExtParam->numOfReq)
                    {
                        uint32 index = 0;
                        uint32 asyncRscMap = 0;

                        // For each request in the array, do initial validation of the request
                        // and pass valid requests to the resource manager
                        for(index = 0; index < pRscExtParam->numOfReq; index++)
                        {
                            // Current request from array
                            MmpmRscParamType *pRequest = &pRscExtParam->pReqArray[index];
                            // Parameters for current request
                            MmpmRscParamStructType *pParams = &pRequest->rscParam;
                            // Status for current request
                            MMPM_STATUS *pStatus = &pRscExtParam->pStsArray[index];
                            // Init return sts array
                            *pStatus = MMPM_STATUS_SUCCESS;

                            switch(pRequest->rscId)
                            {
                            case MMPM_RSC_ID_MIPS_EXT:
                            {
                                ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_INFO,
                                    "Client %u: MIPS request", clientId);

                                AdsppmMipsRequestType adsppmMipsReqData;
                                MmpmMipsReqType *pMipsParams = pParams->pMipsExt;
                                adsppmRscId = Adsppm_Rsc_Id_Mips;

                                if((MMPM_BW_PORT_ID_MAX <= pMipsParams->codeLocation) ||
                                    (MMPM_MIPS_REQUEST_NONE == pMipsParams->reqOperation) ||
                                    (MMPM_MIPS_REQUEST_ENUM_MAX <= pMipsParams->reqOperation))
                                {
                                    *pStatus = MMPM_STATUS_BADPARM;
                                    ADSPPM_LOG_PRINTF_2(ADSPPM_LOG_LEVEL_ERROR,
                                        "bad parameters in codelocation(%u) or reqoperation(%u)",
                                        pMipsParams->codeLocation,
                                        pMipsParams->reqOperation);
                                }
                                else
                                {
                                    memset(&adsppmMipsReqData,0, sizeof(AdsppmMipsRequestType));
                                    // Make sure MMPM MIPS request data structure is
                                    // the same as ADSPPM mips request data structure
                                    memscpy(&adsppmMipsReqData, sizeof(AdsppmMipsRequestType),
                                        pMipsParams, sizeof(MmpmMipsReqType));

                                    ADSPPM_LOG_PRINTF_4(ADSPPM_LOG_LEVEL_INFO,
                                        "    total=%u, floor=%u, location=%u, operation=%u",
                                        adsppmMipsReqData.mipsTotal,
                                        adsppmMipsReqData.mipsPerThread,
                                        adsppmMipsReqData.codeLocation,
                                        adsppmMipsReqData.reqOperation);
                                    ADSPPM_QDSS_EVENT_5(ADSPPM_API_MIPS_REQ,
                                        clientId, adsppmMipsReqData.mipsTotal,
                                        adsppmMipsReqData.mipsPerThread,
                                        adsppmMipsReqData.codeLocation,
                                        adsppmMipsReqData.reqOperation);

                                    *pStatus = (MMPM_STATUS) RM_AddRequest(
                                        clientId, Adsppm_Rsc_Id_Mips, &adsppmMipsReqData, 0);
                                }
                            }
                                break;

                            case MMPM_RSC_ID_GENERIC_BW:
                                adsppmRscId = Adsppm_Rsc_Id_BW;
                                *pStatus = apiProcessBwRequest(
                                    clientId, pRequest->rscId, pParams->pGenBwReq, pRscExtParam->apiType, &asyncRscMap);
                                break;

                            default:
                                *pStatus = MMPM_STATUS_UNSUPPORTED;
                                ADSPPM_LOG_PRINTF_2(ADSPPM_LOG_LEVEL_ERROR,
                                    "Client %u: Unsupported resource %u requested",
                                    clientId, pRequest->rscId);
                                break;
                            }

                            if(MMPM_STATUS_SUCCESS == *pStatus)
                            {
                                *pStatus = apiProcessSingleRequest(adsppmRscId, pRscExtParam->apiType, &asyncRscMap);
                            }
                        }

                        if(MMPM_API_TYPE_ASYNC == pRscExtParam->apiType)
                        {
                            sts = (MMPM_STATUS) AM_AsyncRequest(clientId, asyncRscMap, pRscExtParam->reqTag);
                        }

                        // Update return sts
                        for(index = 0; index < pRscExtParam->numOfReq; index++)
                        {
                            if(MMPM_STATUS_SUCCESS != pRscExtParam->pStsArray[index])
                            {
                                sts = MMPM_STATUS_FAILED;
                                ADSPPM_LOG_PRINTF_3(ADSPPM_LOG_LEVEL_ERROR,
                                    "Request for rscId %u failed (%u) for client %u",
                                    pRscExtParam->pReqArray[index].rscId,
                                    pRscExtParam->pStsArray[index], clientId);
                            }
                        }
                    }
                    else
                    {
                        sts = MMPM_STATUS_BADPARM;
                        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                            "Invalid number of requests in bundle %u",
                            pRscExtParam->numOfReq);
                    }
                } //pRscExtParam->pReqArray; pRscExtParam->pStsArray NULL pointer
            } //pRscExtParam NULL pointer
        }
        else
        {
            sts = MMPM_STATUS_NOTINITIALIZED;
            ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "Not initialized");
        }
    }

    ADSPPM_LOG_FUNC_PROFILEEND;
    ADSPPM_LOG_FUNC_EXIT(sts);
    return sts;
}


/******************************************************************************
 *                     MMPM_Release_Ext
 *****************************************************************************/
MMPM_STATUS MMPM_Release_Ext(uint32 clientId, MmpmRscExtParamType *pRscExtParam)
{
    MMPM_STATUS sts = MMPM_STATUS_NOTINITIALIZED;
    AdsppmRscIdType adsppmRscId = Adsppm_Rsc_Id_None;
    ADSPPM_LOG_FUNC_ENTER;
    ADSPPM_LOG_FUNC_PROFILESTART;

    if(!gDISABLEADSPPMREQUESTFLAG)
    {
        if(ADSPPM_IsInitialized())
        {
            if((NULL == pRscExtParam) || (0 == clientId))
            {
                sts = MMPM_STATUS_BADPARM;
                ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                    "NULL pointer for pRscExtParam or clientID=0, clientid=%u", clientId);
            }
            else
            {
                if((NULL == pRscExtParam->pReqArray) || (NULL == pRscExtParam->pStsArray))
                {
                    sts = MMPM_STATUS_NULLPOINTER;
                }
                else
                {
                    if(Adsppm_Rsc_Id_Max > pRscExtParam->numOfReq)
                    {
                        uint32 index = 0;
                        uint32 asyncRscMap = 0;
                        sts = MMPM_STATUS_SUCCESS;

                        for(index = 0; index < pRscExtParam->numOfReq; index++)
                        {
                            // Init return sts array
                            pRscExtParam->pStsArray[index] = MMPM_STATUS_SUCCESS;

                            uint32 extData = 0;
                            switch(pRscExtParam->pReqArray[index].rscId)
                            {
                            case MMPM_RSC_ID_MIPS_EXT:
                                adsppmRscId = Adsppm_Rsc_Id_Mips;
                                ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_INFO,
                                    "Client %u: mipsext release", clientId);
                                ADSPPM_QDSS_EVENT_1(ADSPPM_API_MIPS_REL, clientId);
                                break;

                            case MMPM_RSC_ID_GENERIC_BW:
                                adsppmRscId = Adsppm_Rsc_Id_BW;
                                ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_INFO,
                                    "Client %u: genBW release", clientId);
                                ADSPPM_QDSS_EVENT_1(ADSPPM_API_BW_REL, clientId);
                                break;

                            default:
                                sts = MMPM_STATUS_UNSUPPORTED;
                                ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                                    "Unsupported rscId = %u", pRscExtParam->pReqArray[index].rscId);
                                break;
                            }

                            ADSPPM_LOG_PRINTF_2(ADSPPM_LOG_LEVEL_INFO_EXT,
                                "Releasing rscId=%u for clientId %u", adsppmRscId, clientId);
                            pRscExtParam->pStsArray[index] = apiReleaseSingleResource(
                                clientId, adsppmRscId, extData, pRscExtParam->apiType, &asyncRscMap);
                        }

                        if(MMPM_API_TYPE_ASYNC == pRscExtParam->apiType)
                        {
                            sts = (MMPM_STATUS) AM_AsyncRequest(
                                clientId, asyncRscMap, pRscExtParam->reqTag);
                        }

                        // Update return sts
                        for(index = 0; index < pRscExtParam->numOfReq; index++)
                        {
                            if(MMPM_STATUS_SUCCESS != pRscExtParam->pStsArray[index])
                            {
                                sts = MMPM_STATUS_FAILED;
                                ADSPPM_LOG_PRINTF_3(ADSPPM_LOG_LEVEL_ERROR,
                                    "Release for rscId %u failed (%u) for client %u",
                                    pRscExtParam->pReqArray[index].rscId,
                                    pRscExtParam->pStsArray[index],
                                    clientId);
                            }
                        }
                    }
                    else
                    {
                        sts = MMPM_STATUS_BADPARM;
                        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                            "Invalid number of requests in bundle %u", pRscExtParam->numOfReq);
                    }
                } // pRscExtParam->pReqArray, pRscExtParam->pStsArray NULL pointer
            } // pRscExtParam NULL pointer
        }
        else
        {
            sts = MMPM_STATUS_NOTINITIALIZED;
            ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "Not initialized");
        }
    }
    else
    {
        sts = MMPM_STATUS_SUCCESS;
    }

    ADSPPM_LOG_FUNC_PROFILEEND;
    ADSPPM_LOG_FUNC_EXIT(sts);
    return sts;
}


/******************************************************************************
 *                     MMPM_GetInfo
 *****************************************************************************/
MMPM_STATUS MMPM_GetInfo(MmpmInfoDataType *pInfoData)
{
#ifdef ADSPPM_TEST
    MMPM_STATUS sts = MMPM_STATUS_NOTINITIALIZED;
    AdsppmCoreIdType coreId;
    uint32 clientId = 0;
    uint32 callBackFunc;
    AdsppmInfoClkFreqType clkInfoFreq;
    AdsppmClientTypeType clientType;
    uint32 mipsValue;
    uint64 bwValue;
    ADSPPM_LOG_FUNC_ENTER;
    ADSPPM_LOG_FUNC_PROFILESTART;

    if(!gDISABLEADSPPMREQUESTFLAG)
    {
        if(ADSPPM_IsInitialized())
        {
            switch(pInfoData->infoId)
            {
            case MMPM_INFO_ID_CLK_FREQ:
            case MMPM_INFO_ID_CORE_CLK_MAX:
                if((MMPM_CLK_ID_LPASS_NONE == pInfoData->info.pInfoClkFreqType->clkId) ||
                    (MMPM_CLK_ID_LPASS_ENUM_MAX <= pInfoData->info.pInfoClkFreqType->clkId))
                {
                    sts = MMPM_STATUS_BADPARM;
                }
                else
                {
                    clkInfoFreq.clkId = mmpmToAdsppmClk[pInfoData->info.pInfoClkFreqType->clkId];
                    clkInfoFreq.forceMeasure = pInfoData->info.pInfoClkFreqType->forceMeasure;
                    clkInfoFreq.clkFreqHz = 0;

                    if(MMPM_INFO_ID_CLK_FREQ == pInfoData->infoId)
                    {
                        sts = (MMPM_STATUS) RM_GetResourceInfo(
                            Adsppm_Rsc_Id_Core_Clk,
                            (void *) &clkInfoFreq,
                            Adsppm_Info_Type_Current_Value);
                    }
                    else
                    {
                        sts = (MMPM_STATUS) RM_GetResourceInfo(
                            Adsppm_Rsc_Id_Core_Clk,
                            (void *) &clkInfoFreq,
                            Adsppm_Info_Type_Max_Value);
                    }
                    if(MMPM_STATUS_SUCCESS == sts)
                    {
                        pInfoData->info.pInfoClkFreqType->clkFreqHz = clkInfoFreq.clkFreqHz;
                    }
                }
                break;

            case MMPM_INFO_ID_MIPS_MAX:
                sts = (MMPM_STATUS) RM_GetResourceInfo(
                    Adsppm_Rsc_Id_Mips,
                    (void *) &mipsValue,
                    Adsppm_Info_Type_Max_Value);
                if(MMPM_STATUS_SUCCESS == sts)
                {
                    pInfoData->info.mipsValue= mipsValue;
                }
                break;

            case MMPM_INFO_ID_BW_MAX:
                sts = (MMPM_STATUS) RM_GetResourceInfo(
                    Adsppm_Rsc_Id_BW,
                    (void *) &bwValue,
                    Adsppm_Info_Type_Max_Value);
                if(MMPM_STATUS_SUCCESS == sts)
                {
                    pInfoData->info.bwValue= bwValue;
                }
                break;

            default:
                sts = (MMPM_STATUS) RM_GetClientInfo(
                    clientId, &coreId, &callBackFunc, &clientType);
                break;
            }
        }
        else
        {
            sts = MMPM_STATUS_NOTINITIALIZED;
            ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "Not initialized");
        }
    }
    else
    {
        sts = MMPM_STATUS_SUCCESS;
    }

    ADSPPM_LOG_FUNC_PROFILEEND;
    ADSPPM_LOG_FUNC_EXIT(sts);
    return sts;
#else
    return MMPM_STATUS_UNSUPPORTED;
#endif
}


/******************************************************************************
 *                     MMPM_SetParameter
 *****************************************************************************/
MMPM_STATUS MMPM_SetParameter(uint32 clientId, MmpmParameterConfigType *pParamConfigData)
{
    return MMPM_STATUS_UNSUPPORTED;
}


/******************************************************************************
 *                     MMPM_Init
 *****************************************************************************/
uint32 MMPM_Init(char *cmd_line)
{
    MMPM_STATUS sts = MMPM_STATUS_SUCCESS;
    if(!gDISABLEADSPPMREQUESTFLAG)
    {
        // Do initialization only once. This cannot be protected using critical section
        // since this is done before initializing the lock for critical section
        if(!ADSPPM_IsInitialized())
        {
            sts = (MMPM_STATUS) ADSPPM_Init();
        }
    }
    else
    {
        sts = MMPM_STATUS_SUCCESS;
    }
    return (uint32)sts;
}

// For CoreBSP image initialization
void MMPM_Init_Ext(void)
{
    MMPM_Init(NULL);
}


/******************************************************************************
 *                     MMPM_Deinit
 *****************************************************************************/
/**
 * De-initialize core MMPM modules
 */
void MMPM_Deinit(void)
{
    return;
}


/**
 * Async API completion callback notification
 */
uint32 API_doClientCallback(AsyncMgrPipeDataType *receiveData)
{
    uint32 result = 0;
    AdsppmCallbackParamType callbackParam;
    MmpmCompletionCallbackDataType callbackData;
    AdsppmCoreCtxType *pAdsppmCoreCtx = GetAdsppmCoreContext();
    AdsppmHandleType *pAdsppmHandle = NULL;
    ADSPPM_LOG_FUNC_ENTER;
    ADSPPM_LOG_FUNC_PROFILESTART;

    memset(&callbackParam, 0, sizeof(AdsppmCallbackParamType));
    callbackData.reqTag = receiveData->reqTag;
    callbackData.result = receiveData->result;
    callbackParam.eventId = Adsppm_Callback_Event_Id_Async_Complete;
    callbackParam.clientId = receiveData->clientId;
    callbackParam.callbackDataSize = sizeof(callbackData);
    callbackParam.callbackData = &(callbackData);
    pAdsppmHandle = coreUtils_Q_LinearSearch(
        &pAdsppmCoreCtx->adsppmHandleQ,
        (coreUtils_Q_CompareFuncType)findAdsppmHandle,
        (void *) &(callbackParam.clientId));

    if(NULL != pAdsppmHandle)
    {
        if(0 != pAdsppmHandle->ADSPPM_Callback)
        {
            // Callback clents call back function
            result = pAdsppmHandle->ADSPPM_Callback(&callbackParam);
        }
    }

    ADSPPM_LOG_FUNC_PROFILEEND;
    ADSPPM_LOG_FUNC_EXIT(result);
    return result;
}


/**
 * @func API_Init - Initialize all components within ADSPPM API
 * @return completion status @see Adsppm_Status
 */
Adsppm_Status API_Init(void)
{
    Adsppm_Status result = Adsppm_Status_Success;
    ADSPPM_LOG_FUNC_ENTER;

    result = AM_AsyncInit();
    if(Adsppm_Status_Success == result)
    {
        gADSPPMINITSTATE |= Adsppm_State_CoreAMAsyncInit;
    }

    ADSPPM_LOG_FUNC_EXIT(result);
    return result;
}

