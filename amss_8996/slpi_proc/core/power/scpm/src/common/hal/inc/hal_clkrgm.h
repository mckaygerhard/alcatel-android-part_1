/*
* Copyright (c) 2013-2015 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

/*
 * hal_clkrgm.h
 *
 *  Created on: Feb 13, 2012
 *      Author: yrusakov
 */

#ifndef HAL_CLKRGM_H_
#define HAL_CLKRGM_H_

#include "adsppm.h"
#include "asic.h"


typedef enum
{
    Hal_ClkRgmDisable = 0,
    Hal_ClkRgmEnable
} Hal_ClkRgmEnableDisableType;

typedef enum
{
    Hal_ClkRgmMemCollapse = 0,
    Hal_ClkRgmMemRetain
} Hal_ClkRgmMemStateType;


Adsppm_Status ClkRgm_Init(void);

Adsppm_Status ClkRgm_SetClock(AdsppmClkIdType clkId, uint32 freq);

Adsppm_Status ClkRgm_EnableClock(AdsppmClkIdType clkId, Hal_ClkRgmEnableDisableType stateRequested);

#ifdef ADSPPM_TEST
Adsppm_Status ClkRgm_GetClockFrequency(AdsppmClkIdType clkId, uint32 *pFreq);

Adsppm_Status ClkRgm_CalcClockFrequency(AdsppmClkIdType clkId, uint32 *pFreq);
#endif


#endif /* HAL_CLKRGM_H_ */

