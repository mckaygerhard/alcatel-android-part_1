/*
* Copyright (c) 2013-2015 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

/*
 * hal_clkrgm.c
 *
 *  Created on: Feb 13, 2012
 *      Author: yrusakov
 */

#include "adsppm.h"
#include "adsppm_utils.h"
#include "hal_clkrgm.h"
#include "asic.h"
#include "DDIClock.h"
#include "DALStdDef.h"
#include "DALSys.h"
#include "DALDeviceId.h"
#include "ClockDefs.h"
#include "npa.h"
#include "string.h"
#include "stdlib.h"


#define ADSPPM_ADSP_POWER_ENABLE 1

typedef struct
{
    const AsicClkDescriptorType *pClkDescriptor; //!< Pointer to the clock descriptor from ACM
    union
    {
        ClockIdType       clkId;     //!< Clock Id for DAL clocks
        npa_client_handle npaHandle; //!< NPA handle for NPA clocks
    } clkHandle;
    uint32                      currentFreq;     //!< Clock frequency currently set
    Hal_ClkRgmEnableDisableType currentState;    //!< Clock enablement state
    uint32                      requestCount;    //!< Number of clients requesting this clock
} HalClk_ClockDescriptorType;

typedef struct
{
    DALSYSSyncHandle               lock;
    DalDeviceHandle                *pHandle;
    HalClk_ClockDescriptorType     clockDesc[AdsppmClk_EnumMax];
} Hal_ClkRgmCtxType;

static Hal_ClkRgmCtxType gClkRgmCtx;



// Call Clock Regime to enable clock
static Adsppm_Status clkrgm_EnableDalClock(HalClk_ClockDescriptorType *pClkHalDesc)
{
    Adsppm_Status result = Adsppm_Status_Success;
    DALResult dalResult = DAL_SUCCESS;
    dalResult = DalClock_EnableClock(gClkRgmCtx.pHandle, pClkHalDesc->clkHandle.clkId);
    if(DAL_SUCCESS != dalResult)
    {
        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
            "DalClock_EnableClock returned failure: %s",
            pClkHalDesc->pClkDescriptor->clkName);
        result = Adsppm_Status_Failed;
    }
    else
    {
        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_INFO,
            "Enabled %s",
            pClkHalDesc->pClkDescriptor->clkName);
    }
    return result;
}


// Call Clock Regime to disable clock
static Adsppm_Status clkrgm_DisableDalClock(HalClk_ClockDescriptorType *pClkHalDesc)
{
    Adsppm_Status result = Adsppm_Status_Success;
    DALResult dalResult = DAL_SUCCESS;
    dalResult = DalClock_DisableClock(gClkRgmCtx.pHandle, pClkHalDesc->clkHandle.clkId);
    if(DAL_SUCCESS != dalResult)
    {
        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
            "DalClock_DisableClock returned failure: %s",
            pClkHalDesc->pClkDescriptor->clkName);
        result = Adsppm_Status_Failed;
    }
    else
    {
        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_INFO,
            "Disabled %s",
            pClkHalDesc->pClkDescriptor->clkName);
    }
    return result;
}


// Call Clock Regime to enable HW control
static Adsppm_Status clkrgm_EnableDalClockHwCtl(HalClk_ClockDescriptorType *pClkHalDesc)
{
    Adsppm_Status result = Adsppm_Status_Success;
    DALResult dalResult = DAL_SUCCESS;
    dalResult = DalClock_ConfigClock(
        gClkRgmCtx.pHandle, pClkHalDesc->clkHandle.clkId, CLOCK_CONFIG_LPASS__HW_CTL_ON);
    if(DAL_SUCCESS != dalResult)
    {
        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
            "DalClock_ConfigClock returned failure: %s",
            pClkHalDesc->pClkDescriptor->clkName);
        result = Adsppm_Status_Failed;
    }
    else
    {
        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_INFO_EXT,
            "Enabled DCG for %s",
            pClkHalDesc->pClkDescriptor->clkName);
    }
    return result;
}


// Call Clock Regime to disable HW control
static Adsppm_Status clkrgm_DisableDalClockHwCtl(HalClk_ClockDescriptorType *pClkHalDesc)
{
    Adsppm_Status result = Adsppm_Status_Success;
    DALResult dalResult = DAL_SUCCESS;
    dalResult = DalClock_ConfigClock(
        gClkRgmCtx.pHandle, pClkHalDesc->clkHandle.clkId, CLOCK_CONFIG_LPASS__HW_CTL_OFF);
    if(DAL_SUCCESS != dalResult)
    {
        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
            "DalClock_ConfigClock returned failure: %s",
            pClkHalDesc->pClkDescriptor->clkName);
        result = Adsppm_Status_Failed;
    }
    else
    {
        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_INFO_EXT,
            "Disabled DCG for %s",
            pClkHalDesc->pClkDescriptor->clkName);
    }
    return result;
}


static Adsppm_Status clk_EnableClock(AdsppmClkIdType clkId, HalClk_ClockDescriptorType *pClkHalDesc)
{
    Adsppm_Status result = Adsppm_Status_UnSupported;
    switch(pClkHalDesc->pClkDescriptor->clkType)
    {
    case AsicClk_TypeDalFreqSet:
    case AsicClk_TypeDalEnable:
        result = clkrgm_EnableDalClock(pClkHalDesc);
        break;
    default:
        break;
    }
    if(Adsppm_Status_Success == result)
    {
        ADSPPM_QDSS_EVENT_1(ADSPPM_CLK_ENABLE, clkId);
        pClkHalDesc->currentState = Hal_ClkRgmEnable;
    }
    return result;
}


static Adsppm_Status clk_DisableClock(AdsppmClkIdType clkId, HalClk_ClockDescriptorType *pClkHalDesc)
{
    Adsppm_Status result = Adsppm_Status_Success;
    if(Adsppm_Status_Success == result)
    {
        switch(pClkHalDesc->pClkDescriptor->clkType)
        {
        case AsicClk_TypeDalFreqSet:
        case AsicClk_TypeDalEnable:
            result = clkrgm_DisableDalClock(pClkHalDesc);
            break;
        default:
            result = Adsppm_Status_UnSupported;
            break;
        }
    }
    if(Adsppm_Status_Success == result)
    {
        ADSPPM_QDSS_EVENT_1(ADSPPM_CLK_DISABLE, clkId);
        pClkHalDesc->currentState = Hal_ClkRgmDisable;
    }
    return result;
}


static Adsppm_Status clkrgm_EnableClockBranch(AdsppmClkIdType clkId, HalClk_ClockDescriptorType *pClkHalDesc)
{
    Adsppm_Status result = Adsppm_Status_Success;
    switch(pClkHalDesc->pClkDescriptor->clkCntlType)
    {
    case AsicClk_CntlAlwaysON_DCG:
    case AsicClk_CntlSW_DCG:
        // Enable clock if DCG feature is not enabled
        if(AsicFeatureState_Enabled == ACM_GetFeatureStatus(AsicFeatureId_Ahb_DCG))
        {
            pClkHalDesc->currentState = Hal_ClkRgmEnable;
            break;
        }
        // No break statement here to proceed to disable clock
    case AsicClk_CntlAlwaysON:
    case AsicClk_CntlSW:
        result = clk_EnableClock(clkId, pClkHalDesc);
        break;
    default:
        // Don't enable Always OFF Clocks
        break;
    }
    return result;
}


static Adsppm_Status clkrgm_DisableClockBranch(AdsppmClkIdType clkId, HalClk_ClockDescriptorType *pClkHalDesc)
{
    Adsppm_Status result = Adsppm_Status_Success;
    switch(pClkHalDesc->pClkDescriptor->clkCntlType)
    {
    case AsicClk_CntlSW_DCG:
        // Disable clock if DCG feature is not enabled
        if(AsicFeatureState_Enabled == ACM_GetFeatureStatus(AsicFeatureId_Ahb_DCG))
        {
            pClkHalDesc->currentState = Hal_ClkRgmDisable;
            break;
        }
        // No break statement here to proceed to disable clock
    case AsicClk_CntlOff:
    case AsicClk_CntlSW:
        result = clk_DisableClock(clkId, pClkHalDesc);
        break;
    default:
        // Don't disable AON clocks,
        // or AON DCG clocks if DCG is enabled
        break;
    }
    return result;
}


static Adsppm_Status clkrgm_InitClocks(void)
{
    Adsppm_Status result = Adsppm_Status_Success;
    DALResult dalResult = DAL_SUCCESS;
    AdsppmClkIdType i = AdsppmClk_None;
    HalClk_ClockDescriptorType *pClkHalDesc = NULL;

    // Get information for each clock from device config, clock regime, npa framework
    for(i = AdsppmClk_None; (Adsppm_Status_Success == result) && (i < AdsppmClk_EnumMax); i++)
    {
        pClkHalDesc = &gClkRgmCtx.clockDesc[i];
        pClkHalDesc->pClkDescriptor = ACMClk_GetClockDescriptor((AdsppmClkIdType)i);
        pClkHalDesc->currentFreq = 0;
        pClkHalDesc->requestCount = 0;

        if(NULL != pClkHalDesc->pClkDescriptor)
        {
            switch(pClkHalDesc->pClkDescriptor->clkType)
            {
            case AsicClk_TypeNpa:
                // Register with NPA node
                // Require ClkRegime to be initialized before ADSPPM,
                // so the npa clocks have to be available at this point
                //npa_resource_available_cb(pClk->pClkDescriptor->clkName, NpaClkAvailableCb, NULL);
                pClkHalDesc->clkHandle.npaHandle = npa_create_sync_client(
                    pClkHalDesc->pClkDescriptor->clkName, "ADSPPM", NPA_CLIENT_REQUIRED);
                if(NULL == pClkHalDesc->clkHandle.npaHandle)
                {
                    ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                        "Failed to create NPA client for %s",
                        pClkHalDesc->pClkDescriptor->clkName);
                    result = Adsppm_Status_Failed;
                }
                break;
            case AsicClk_TypeDalFreqSet:
            case AsicClk_TypeDalEnable:
            case AsicClk_TypeDalDomainSrc:
                // Get clock Id from ClkRegime based on clock name
                dalResult = DalClock_GetClockId(
                    gClkRgmCtx.pHandle, pClkHalDesc->pClkDescriptor->clkName, &(pClkHalDesc->clkHandle.clkId));
                if(DAL_SUCCESS != dalResult)
                {
                    ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                        "Failed to get ClkId for %s",
                        pClkHalDesc->pClkDescriptor->clkName);
                    result = Adsppm_Status_Failed;
                }
                break;
            default:
                break;
            }
        }
        else
        {
            ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                "Failed to get Clock descriptor for clock %u", i);
            result = Adsppm_Status_Failed;
        }
    }

    // Set initial state for each clock
    // It needs to be set in a different for loop from the one above
    // Reason is that there are clock source dependencies among clocks
    for(i = AdsppmClk_None; (Adsppm_Status_Success == result) && (i < AdsppmClk_EnumMax); i++)
    {
        pClkHalDesc = &gClkRgmCtx.clockDesc[i];

        // Only proceed for clocks that are present in the chip
        // Otherwise just skip the setting
        if(pClkHalDesc->pClkDescriptor->clkType != AsicClk_TypeNone)
        {
            // Enable/Disable HW control if clock is of DCG type
            if((pClkHalDesc->pClkDescriptor->clkCntlType == AsicClk_CntlAlwaysON_DCG) ||
                (pClkHalDesc->pClkDescriptor->clkCntlType == AsicClk_CntlSW_DCG))
            {
                result = (AsicFeatureState_Enabled == ACM_GetFeatureStatus(AsicFeatureId_Ahb_DCG))?
                    clkrgm_EnableDalClockHwCtl(pClkHalDesc):
                    clkrgm_DisableDalClockHwCtl(pClkHalDesc);
            }

            if(Adsppm_Status_Success == result)
            {
                switch(pClkHalDesc->pClkDescriptor->clkCntlType)
                {
                case AsicClk_CntlOff:
                    // Disable AOFF clock
                    pClkHalDesc->currentState = Hal_ClkRgmEnable;
                    result = ClkRgm_EnableClock(i, Hal_ClkRgmDisable);
                    break;
                case AsicClk_CntlAlwaysON:
                case AsicClk_CntlAlwaysON_DCG:
                    // Enable AON clock
                    // If DCG is enabled, call is ignore downstream
                    pClkHalDesc->currentState = Hal_ClkRgmDisable;
                    // If they are of type FreqSet, set them to 19.2 MHz
                    if(pClkHalDesc->pClkDescriptor->clkType == AsicClk_TypeDalFreqSet)
                    {
                        result = ClkRgm_SetClock(i, 19200000);
                    }
                    else
                    {
                        result = ClkRgm_EnableClock(i, Hal_ClkRgmEnable);
                    }
                    break;
                case AsicClk_CntlSW:
                case AsicClk_CntlSW_DCG:
                    // Disable clock
                    // If DCG is enabled, call is ignore downstream
                    pClkHalDesc->currentState = Hal_ClkRgmEnable;
                    result = ClkRgm_EnableClock(i, Hal_ClkRgmDisable);
                    break;
                default:
                    // Don't touch other clocks here
                    break;
                }
            }
        }
    }

    return result;
}


Adsppm_Status ClkRgm_Init(void)
{
    Adsppm_Status result = Adsppm_Status_Success;
    DALResult dalResult = DAL_SUCCESS;
    ADSPPM_LOG_FUNC_ENTER;
    ADSPPM_LOG_FUNC_PROFILESTART;

    // Create mutex
    dalResult = DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE, (DALSYSSyncHandle*) &(gClkRgmCtx.lock), NULL);
    if(DAL_SUCCESS != dalResult)
    {
        ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "Failed to create lock");
        result = Adsppm_Status_NotInitialized;
    }
    else
    {
        // Register handle for Clock Regime
        dalResult = DAL_ClockDeviceAttach(DALDEVICEID_CLOCK, &(gClkRgmCtx.pHandle));
        if(DAL_SUCCESS != dalResult)
        {
            ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "DAL_ClockDeviceAttach failed");
            result = Adsppm_Status_Failed;
        }
        else
        {
            // Initialize Clocks
            result = clkrgm_InitClocks();
            if(Adsppm_Status_Success != result)
            {
                ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "clkrgm_InitClocks failed");
            }
        }

        // Destroy lock if init failed
        if(Adsppm_Status_Success != result)
        {
            DALSYS_DestroyObject((DALSYSSyncHandle *) &(gClkRgmCtx.lock));
            gClkRgmCtx.lock = NULL;
        }
    }

    ADSPPM_LOG_FUNC_PROFILEEND;
    ADSPPM_LOG_FUNC_EXIT(result);
    return result;
}


#ifdef ADSPPM_TEST
/**
 * @fn ClkRgm_CalcClockFrequency - get clock frequency through clock regime calculate from oscillator
 * @param pFreq - out put frequency in Hz
 */
Adsppm_Status ClkRgm_CalcClockFrequency(AdsppmClkIdType clkId, uint32 *pFreq)
{
    Adsppm_Status result = Adsppm_Status_Success;
    npa_query_type sQueryResult;
    npa_query_status eNpaStatus = NPA_QUERY_SUCCESS;
    DALResult dalResult = DAL_SUCCESS;
    ADSPPM_LOG_FUNC_ENTER;
    ADSPPM_LOG_FUNC_PROFILESTART;

    if((AdsppmClk_None < clkId) && (AdsppmClk_EnumMax > clkId) && (NULL != pFreq))
    {
        if(NULL != gClkRgmCtx.lock)
        {
            HalClk_ClockDescriptorType *pClkHalDesc = &gClkRgmCtx.clockDesc[clkId];
            if((NULL != pClkHalDesc->pClkDescriptor) && (AdsppmClk_None != pClkHalDesc->pClkDescriptor->clkId))
            {
                if(pClkHalDesc->currentState == Hal_ClkRgmEnable)
                {
                    switch(pClkHalDesc->pClkDescriptor->clkType)
                    {
                    case AsicClk_TypeNpa:
                        eNpaStatus = npa_query_by_name(
                            pClkHalDesc->pClkDescriptor->clkName,
                            NPA_QUERY_CURRENT_STATE,
                            &sQueryResult);
                        if(0 != eNpaStatus)
                        {
                            ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                                "npa_query_by_name: Failed due to NPA error %u",
                                eNpaStatus);
                            result = Adsppm_Status_Failed;
                        }
                        else
                        {
                            *pFreq = sQueryResult.data.state;
                        }
                        break;
                    default:
                        if(pClkHalDesc->clkHandle.clkId != NULL)
                        {
                            dalResult = DalClock_GetClockFrequency(
                                gClkRgmCtx.pHandle, pClkHalDesc->clkHandle.clkId, pFreq);
                            if(DAL_SUCCESS != dalResult)
                            {
                                ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR,
                                    "DalClock_GetClockFrequency returned failure");
                                result = Adsppm_Status_Failed;
                            }
                            else
                            {
                                ADSPPM_LOG_PRINTF_3(ADSPPM_LOG_LEVEL_INFO,
                                    "Got clock frequency %u for %s. currentset freq=%u",
                                    *pFreq, pClkHalDesc->pClkDescriptor->clkName,
                                    pClkHalDesc->currentFreq);
                            }
                        }
                        else
                        {
                            if(AdsppmClk_None != pClkHalDesc->pClkDescriptor->clkSrcId)
                            {
                                // Recursive call here.
                                // We are relying on the lock to work in recursive cases
                                // as it is stated in the API description
                                result = ClkRgm_CalcClockFrequency(pClkHalDesc->pClkDescriptor->clkSrcId, pFreq);
                            }
                        }
                        break;
                    }
                }
                else
                {
                    *pFreq = 0;
                }
            }
            else
            {
                ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_INFO,
                    "bad pClk->pClkDescriptor->clkId");
                result = Adsppm_Status_BadParm;
            }
        }
        else
        {
            result = Adsppm_Status_NotInitialized;
        }
    }
    else
    {
        result = Adsppm_Status_BadParm;
        ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR,
            "bad clk or pFreq");
    }

    ADSPPM_LOG_FUNC_PROFILEEND;
    ADSPPM_LOG_FUNC_EXIT(result);
    return result;
}


Adsppm_Status ClkRgm_GetClockFrequency(AdsppmClkIdType clkId, uint32 *pFreq)
{
    Adsppm_Status result = Adsppm_Status_Success;
    npa_query_type sQueryResult;
    npa_query_status eNpaStatus = NPA_QUERY_SUCCESS;
    ADSPPM_LOG_FUNC_ENTER;
    ADSPPM_LOG_FUNC_PROFILESTART;

    if((AdsppmClk_None < clkId) && (AdsppmClk_EnumMax > clkId) && (NULL != pFreq))
    {
        if(NULL != gClkRgmCtx.lock)
        {
            HalClk_ClockDescriptorType *pClkHalDesc = &gClkRgmCtx.clockDesc[clkId];
            if(pClkHalDesc->currentState == Hal_ClkRgmEnable)
            {
                switch(pClkHalDesc->pClkDescriptor->clkType)
                {
                case AsicClk_TypeNpa:
                    eNpaStatus = npa_query_by_name(
                        pClkHalDesc->pClkDescriptor->clkName,
                        NPA_QUERY_CURRENT_STATE,
                        &sQueryResult);
                    if(0 != eNpaStatus)
                    {
                        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                            "npa_query_by_name: Failed due to NPA error %u",
                            eNpaStatus);
                        result = Adsppm_Status_Failed;
                    }
                    else
                    {
                        *pFreq = sQueryResult.data.state;
                    }
                    break;
                default:
                    *pFreq = pClkHalDesc->currentFreq;
                    break;
                }
            }
            else
            {
                *pFreq = 0;
            }
        }
        else
        {
            result = Adsppm_Status_NotInitialized;
        }
    }
    else
    {
        result = Adsppm_Status_NullPointer;
    }

    ADSPPM_LOG_FUNC_PROFILEEND;
    ADSPPM_LOG_FUNC_EXIT(result);
    return result;
}
#endif


/**
 * @fn ClkRgm_SetClock - Set clock frequency via ClkRegime
 * @param freq - frequency in Hz
 */
Adsppm_Status ClkRgm_SetClock(AdsppmClkIdType clkId, uint32 freq)
{
    Adsppm_Status result = Adsppm_Status_Success;
    ADSPPM_LOG_FUNC_ENTER;
    ADSPPM_LOG_FUNC_PROFILESTART;

    if((AdsppmClk_None < clkId) && (AdsppmClk_EnumMax > clkId))
    {
        if(NULL != gClkRgmCtx.lock)
        {
            HalClk_ClockDescriptorType *pClkHalDesc = &gClkRgmCtx.clockDesc[clkId];
            DALResult dalResult = DAL_SUCCESS;
            if(NULL != pClkHalDesc->pClkDescriptor)
            {
                //Lock HW layer
                adsppm_lock(gClkRgmCtx.lock);

                if(pClkHalDesc->currentFreq != freq)
                {
                    uint32 previousFreq = pClkHalDesc->currentFreq;
                    switch(pClkHalDesc->pClkDescriptor->clkType)
                    {
                    case AsicClk_TypeNpa:
                        //Use NPA node
                        if(NULL != pClkHalDesc->clkHandle.npaHandle)
                        {
                            if(0 < freq)
                            {
                                ADSPPM_LOG_PRINTF_2(ADSPPM_LOG_LEVEL_INFO,
                                    "NPA request for %s, state: %u",
                                    pClkHalDesc->pClkDescriptor->clkName, freq);
                                ADSPPM_QDSS_EVENT_3(ADSPPM_CLK_SET, clkId, freq, freq);
                                //issue NPA request for specified frequency
                                //Translate from Hz to kHz first
                                npa_issue_required_request(pClkHalDesc->clkHandle.npaHandle, freq/1000);
                            }
                        }
                        else
                        {
                            ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "NULL NPA handle");
                            result = Adsppm_Status_Failed;
                        }
                        break;
                    case AsicClk_TypeDalEnable:
                        if(0 < freq)
                        {
                            // Use clock source from clock descriptor
                            if(AdsppmClk_None != pClkHalDesc->pClkDescriptor->clkSrcId)
                            {
                                ADSPPM_LOG_PRINTF_2(ADSPPM_LOG_LEVEL_INFO,
                                    "Requested freq %u for %s",
                                    freq, pClkHalDesc->pClkDescriptor->clkName);
                                result = ClkRgm_SetClock(pClkHalDesc->pClkDescriptor->clkSrcId, freq);
                            }
                        }
                        break;
                    case AsicClk_TypeDalFreqSet:
                    case AsicClk_TypeDalDomainSrc:
                        //Call clock regime to set at least requested frequency
                        //Disable clock branch if 0 frequency is requested
                        if(0 < freq)
                        {
                            //attempt to set frequency at least as requested
                            dalResult = DalClock_SetClockFrequency(
                                gClkRgmCtx.pHandle, pClkHalDesc->clkHandle.clkId,
                                freq, CLOCK_FREQUENCY_HZ_AT_LEAST, &pClkHalDesc->currentFreq);
                            if(DAL_SUCCESS != dalResult)
                            {
                                // Try to request cap the frequency
                                // if the previous request has failed
                                // because of exceeding max for this clock
                                dalResult = DalClock_SetClockFrequency(
                                    gClkRgmCtx.pHandle, pClkHalDesc->clkHandle.clkId,
                                    freq, CLOCK_FREQUENCY_HZ_CLOSEST, &pClkHalDesc->currentFreq);
                                if(DAL_SUCCESS != dalResult)
                                {
                                    ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR,
                                        "DalClock_SetClockFrequency returned failure");
                                    result = Adsppm_Status_Failed;
                                }
                                else
                                {
                                    ADSPPM_LOG_PRINTF_3(ADSPPM_LOG_LEVEL_INFO,
                                        "Failed to set freq %u for %s. Capped freq=%u",
                                        freq, pClkHalDesc->pClkDescriptor->clkName, pClkHalDesc->currentFreq);
                                }
                            }
                            else
                            {
                                ADSPPM_LOG_PRINTF_3(ADSPPM_LOG_LEVEL_INFO,
                                    "Requested frequency %u for %s. Set freq=%u",
                                    freq, pClkHalDesc->pClkDescriptor->clkName, pClkHalDesc->currentFreq);
                                ADSPPM_QDSS_EVENT_3(ADSPPM_CLK_SET,
                                    pClkHalDesc->pClkDescriptor->clkId, freq, pClkHalDesc->currentFreq);
                            }
                        }
                        break;
                    default:
                        break;
                    }

                    if(Adsppm_Status_Success == result)
                    {
                        pClkHalDesc->currentFreq = freq;
                        if((0 < freq) && (0 == previousFreq))
                        {
                            result = ClkRgm_EnableClock(clkId, Hal_ClkRgmEnable);
                        }
                        if(0 == freq)
                        {
                            result = ClkRgm_EnableClock(clkId, Hal_ClkRgmDisable);
                        }
                    }
                }

                //Unlock HW layer
                adsppm_unlock(gClkRgmCtx.lock);
            }
            else
            {
                ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "invalid HalClk_ClockDescriptor");
                result = Adsppm_Status_BadParm;
            }
        }
        else
        {
            result = Adsppm_Status_NotInitialized;
        }
    }
    else
    {
        ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "AdsppmClkId out of range");
        result = Adsppm_Status_BadParm;
    }

    ADSPPM_LOG_FUNC_PROFILEEND;
    ADSPPM_LOG_FUNC_EXIT(result);
    return result;
}


Adsppm_Status ClkRgm_EnableClock(AdsppmClkIdType clkId, Hal_ClkRgmEnableDisableType stateRequested)
{
    Adsppm_Status result = Adsppm_Status_Success;
    Hal_ClkRgmEnableDisableType state = stateRequested;
    ADSPPM_LOG_FUNC_ENTER;
    ADSPPM_LOG_FUNC_PROFILESTART;

    if((AdsppmClk_None < clkId) && (AdsppmClk_EnumMax > clkId))
    {
        if(NULL != gClkRgmCtx.lock)
        {
            HalClk_ClockDescriptorType *pClkHalDesc = &gClkRgmCtx.clockDesc[clkId];
            if(NULL != pClkHalDesc->pClkDescriptor)
            {
                // Lock HW layer
                adsppm_lock(gClkRgmCtx.lock);

                // Update reference count
                ADSPPM_LOG_PRINTF_2(ADSPPM_LOG_LEVEL_INFO_EXT,
                    "Clock=%s, requested state=%u",
                    pClkHalDesc->pClkDescriptor->clkName, stateRequested);
                ADSPPM_LOG_PRINTF_2(ADSPPM_LOG_LEVEL_DEBUG,
                    "current counter=%u, current state=%u",
                    pClkHalDesc->requestCount, pClkHalDesc->currentState);
                if(Hal_ClkRgmEnable == stateRequested)
                {
                    pClkHalDesc->requestCount++;
                }
                else
                {
                    if(pClkHalDesc->requestCount > 0) pClkHalDesc->requestCount--;
                    //Override the requested state if the request counter is not 0
                    if(pClkHalDesc->requestCount > 0) state = Hal_ClkRgmEnable;
                }
                ADSPPM_LOG_PRINTF_2(ADSPPM_LOG_LEVEL_DEBUG,
                    "resulting counter=%u, resulting state=%u",
                    pClkHalDesc->requestCount, state);

                if(pClkHalDesc->currentState != state)
                {
                    switch(pClkHalDesc->pClkDescriptor->clkType)
                    {
                    case AsicClk_TypeNpa:
                        // Use NPA node
                        // Require ClkRegime to be initialized before ADSPPM,
                        // so the npa clocks have to be available at this point
                        if(NULL != pClkHalDesc->clkHandle.npaHandle)
                        {
                            if(Hal_ClkRgmEnable == state)
                            {
                                if(0 == pClkHalDesc->currentFreq)
                                {
                                    npa_query_type query;
                                    query.type = NPA_QUERY_TYPE_VALUE;
                                    // Get min frequency for this clock
                                    if(NPA_QUERY_SUCCESS == npa_query_by_client(
                                        pClkHalDesc->clkHandle.npaHandle, CLOCK_NPA_QUERY_MIN_FREQ_KHZ, &query))
                                    {
                                        ADSPPM_LOG_PRINTF_2(ADSPPM_LOG_LEVEL_INFO,
                                            "NPA request for %s, state: %u",
                                            pClkHalDesc->pClkDescriptor->clkName, query.data.value);
                                        ADSPPM_QDSS_EVENT_3(ADSPPM_CLK_SET,
                                            clkId, query.data.value, query.data.value);
                                        // Issue NPA request for min frequency
                                        npa_issue_required_request(pClkHalDesc->clkHandle.npaHandle, query.data.value);
                                        pClkHalDesc->currentFreq = query.data.value;
                                        pClkHalDesc->currentState = Hal_ClkRgmEnable;
                                    }
                                    else
                                    {
                                        ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR,
                                            "NPA query returned failure");
                                        result = Adsppm_Status_Failed;
                                    }
                                }
                                else
                                {
                                    pClkHalDesc->currentState = Hal_ClkRgmEnable;
                                }
                            }
                            else
                            {
                                ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_INFO,
                                    "Complete NPA request for %s",
                                    pClkHalDesc->pClkDescriptor->clkName);
                                ADSPPM_QDSS_EVENT_3(ADSPPM_CLK_SET, clkId, 0, 0);

                                // Release NPA request
                                npa_complete_request(pClkHalDesc->clkHandle.npaHandle);
                                pClkHalDesc->currentFreq = 0;
                                pClkHalDesc->currentState = Hal_ClkRgmDisable;
                            }
                        }
                        else
                        {
                            ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
                                "NULL npa handle for %s",
                                pClkHalDesc->pClkDescriptor->clkName);
                            result = Adsppm_Status_Failed;
                        }
                        break;
                    case AsicClk_TypeDalFreqSet:
                    case AsicClk_TypeDalEnable:
                    case AsicClk_TypeDalDomainSrc:
                        if(Hal_ClkRgmEnable == state)
                        {
                            result = clkrgm_EnableClockBranch(clkId, pClkHalDesc);
                        }
                        else
                        {
                            result = clkrgm_DisableClockBranch(clkId, pClkHalDesc);
                        }
                        break;
                    default:
                        break;
                    }
                }

                // Unlock HW layer
                adsppm_unlock(gClkRgmCtx.lock);
            }
            else
            {
                ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR,
                    "invalid HalClk_ClockDescriptor");
                result = Adsppm_Status_BadParm;
            }
        }
        else
        {
            result = Adsppm_Status_NotInitialized;
        }
    }
    else
    {
        ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR,
            "AdsppmClkId out of range");
        result = Adsppm_Status_BadParm;
    }

    ADSPPM_LOG_FUNC_PROFILEEND;
    ADSPPM_LOG_FUNC_EXIT(result);
    return result;
}

