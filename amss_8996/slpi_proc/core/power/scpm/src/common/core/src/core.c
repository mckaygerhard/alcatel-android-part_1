/*
* Copyright (c) 2013-2015 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

/*
 * core.c
 *
 *  Created on: Feb 13, 2012
 *      Author: yrusakov
 */

#include "core.h"
#include "adsppm_utils.h"
#include "core_internal.h"
#include "mipsmgr.h"
#include "requestmgr.h"
#include "busmgr.h"
#include "hal_clkrgm.h"


static AdsppmCoreCtxType gAdsppmCoreCtx;

extern uint32 gADSPPMINITSTATE;


/**
 * @fn GetAdsppmCoreContext - retrieves pointer to the global ADSPPM context
 * @return pointer to the global ADSPPM context
 */
AdsppmCoreCtxType *GetAdsppmCoreContext(void)
{
    return &gAdsppmCoreCtx;
}


/**
 * @fn findAdsppmHandle - find the client context or handle in linklist
 */
int findAdsppmHandle(AdsppmHandleType *pAdsppmHandle, uint32 *clientId)
{
    int match = 0;
    if(pAdsppmHandle->clientId == *clientId)
    {
         match = 1;
    }
    return match;
}


uint32 IsPeriodicUseCase(void)
{
    return gAdsppmCoreCtx.periodicUseCase;
}


void SetPeriodicUseCase(uint32 periodic)
{
    gAdsppmCoreCtx.periodicUseCase = periodic;
}


/**
 * @fn Core_EnterSleep - Prepare LPASS resources for power collapse.
 * Currently it will just ramp down AHB clock.
 * TODO: Handle the clock gates as well
 * @return completion status
 */
Adsppm_Status Core_EnterSleep(void)
{
    Adsppm_Status result = Adsppm_Status_Success;
#if 0
    ADSPPM_LOG_FUNC_ENTER;
    result = AHBM_SetSleepAHBClock();
    ADSPPM_LOG_FUNC_EXIT(result);
#endif
    return result;
}


/**
 * @fn Core_ExitSleep - Restore LPASS resources after power collapse.
 * Currently it will just ramp up AHB clock.
 * TODO: Handle the clock gates as well
 * @return completion status
 */
Adsppm_Status Core_ExitSleep(void)
{
    Adsppm_Status result = Adsppm_Status_Success;
#if 0
    ADSPPM_LOG_FUNC_ENTER;
    result = AHBM_SetActiveAHBClock();
    ADSPPM_LOG_FUNC_EXIT(result);
#endif
    return result;
}


/**
 * @fn Core_Init - Initialize all components within ADSPPM Core
 * @return completion status @see Adsppm_Status
 */
Adsppm_Status Core_Init(void)
{
    Adsppm_Status result = Adsppm_Status_Success;
    ADSPPM_LOG_FUNC_ENTER;

    memset(&gAdsppmCoreCtx, 0, sizeof(AdsppmCoreCtxType));

    // Set periodic usecase by default to ensure quick powerdown
    gAdsppmCoreCtx.periodicUseCase = 1;

    if(DAL_SUCCESS != DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE, (DALSYSSyncHandle *)&gAdsppmCoreCtx.lock, NULL))
    {
        result = Adsppm_Status_Failed;
    }
    if(Adsppm_Status_Success == result)
    {
        gADSPPMINITSTATE |= Adsppm_State_CoreCtxLockInit;
        result = RM_Init();
        if(Adsppm_Status_Success == result)
        {
            gADSPPMINITSTATE |= Adsppm_State_CoreRMInit;
            result = MIPS_Init();
            if(Adsppm_Status_Success == result)
            {
                gADSPPMINITSTATE |= Adsppm_State_CoreMIPSInit;
                result = BusMgr_Init();
                if(Adsppm_Status_Success == result)
                {
                    gADSPPMINITSTATE |= Adsppm_State_CoreBUSInit;
                }
            }
        }
    }
    if((Adsppm_Status_Success == result) && (AsicFeatureState_Enabled == ACM_GetFeatureStatus(AsicFeatureId_InitialState)))
    {
        AdsppmRscIdType rsc;
        for(rsc = Adsppm_Rsc_Id_Power; rsc < Adsppm_Rsc_Id_Max; rsc++)
        {
            result = RM_RequestResource(rsc);
            if(Adsppm_Status_Success != result)
            {
                // Put failed resource ID in high 4 bits
                gADSPPMINITSTATE |= rsc << 28;
                break;
            }
        }
    }
    ADSPPM_LOG_FUNC_EXIT(result);
    return result;
}


#ifdef ADSPPM_TEST
Adsppm_Status CLK_GetInfo(AdsppmInfoClkFreqType *pClockInfo)
{
    Adsppm_Status sts = Adsppm_Status_Success;
    ADSPPM_LOG_FUNC_ENTER;
    if(NULL == pClockInfo)
    {
        ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_ERROR, "pClockInfo is NULL");
        sts = Adsppm_Status_BadParm;
    }
    else
    {
        if(pClockInfo->forceMeasure)
        {
            sts = ClkRgm_CalcClockFrequency(pClockInfo->clkId, &pClockInfo->clkFreqHz);
        }
        else
        {
            sts = ClkRgm_GetClockFrequency(pClockInfo->clkId, &pClockInfo->clkFreqHz);
        }
    }
    return sts;
    ADSPPM_LOG_FUNC_EXIT(sts);
}
#endif

