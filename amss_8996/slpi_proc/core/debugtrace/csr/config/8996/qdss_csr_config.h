#ifndef QDSS_CSR_CONFIG_H
#define QDSS_CSR_CONFIG_H

/*=============================================================================

FILE:         qdss_csr_config.h

DESCRIPTION:  

================================================================================
Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.
==============================================================================*/


#define QDSS_QDSSCSR_REG_BASE_PHYS      (0x03001000) 
#define QDSS_SSC_QDSSCSR_BASE_PHYS      (0x01E00000)


#endif //QDSS_CSR_CONFIG_H
