/*=========================================================================
                       QDSS INIT 

GENERAL DESCRIPTION 
   Provides APIs to initialize QDSS driver components and to also flush trace 
   data from QDSS trace buffers.


 
EXTERNALIZED FUNCTIONS
   QDSSInit
   QDSSisEnabled
   QDSSFlush

 
 
INITIALIZATION AND SEQUENCING REQUIREMENTS
   QDSSInit is the first function that needs to be invoked in order to 
   initialized QDSS driver components. QDSSInit assumes that the DAL component 
   has been initialized prior to executing QDSSInit.

   

      Copyright (c) 2011-2012 by Qualcomm Technologies, Inc.  All Rights Reserved.
==========================================================================*/

/*========================================================================== 
 $Header: //components/rel/core.slpi/1.0/debugtrace/common/src/qdss_init.c#12 $
==========================================================================*/  

#include "comdef.h"
#include "DALSys.h"
#include "DALDeviceId.h"
#include "DDISTMCfg.h"
#include "DDITFunnel.h"
#include "DDISTMTrace.h"
#include "DDITMC.h"
#include "DDIClock.h"
#include "ClockDefs.h"
#include "npa.h"
#include <err.h>
#include "qdss.h"
#include "qdss_utils.h"
#include "DDITimetick.h"

#include "tracer.h"
#include "tracer_event_ids.h"


typedef enum{
   QDSS_OFF=0,
   QDSS_READY,
   QDSS_IN_USE,
   QDSS_ENUM_LAST=0x7FFFFFFF
}QDSSState;


struct qdss_clock{
   DALDEVICEHANDLE hClock;
   ClockIdType ssc_at_clk_id;
   ClockIdType ssc_q6_atbm_clk_id;
   ClockIdType ssc_pclkdbg_clk_id;
   ClockIdType ssc_dbg_tsctr_clk_id;
   int use_count;
};


typedef struct {
	QDSSState eQDSSState;

   DALDEVICEHANDLE hTrace;
   uint32 local_stm_port;

   DALDEVICEHANDLE hTraceChip;
   uint32 chip_stm_port;

   DALDEVICEHANDLE hTimetick;

   npa_client_handle npa_client;   
   DALDEVICEHANDLE hSTMCfg,hFunnel,hTMC;
   struct qdss_clock clk;
   uint16 sync_tag;
} QDSSContext;

static int32 QDSSchangeState(QDSSState new_state);
static boolean QDSSisReady(void);



static QDSSContext qdss_ctxt={QDSS_OFF};


void QDSSInit(void)
{
   if(qdss_ctxt.eQDSSState == QDSS_OFF)
   {
      /*Initialize QDSS drivers only once*/
      QDSSchangeState(QDSS_READY);
   }   

#ifdef QDSS_STANDALONE_MODE
   QDSSOn();
#endif   
   return;
}

int32 QDSSOn(void)
{
   if (QDSSisReady())
   {
      return QDSSchangeState(QDSS_IN_USE);

   }
   else if(QDSSisInUse()==TRUE)
   {
      return 0;
   }
   else
   {
      return -1;
   }
}
   

int32 QDSSOff(void)
{
   if(QDSSisInUse()==TRUE)
   {
      return QDSSchangeState(QDSS_READY);
   } else if(QDSSisReady())
   {
      return 0;
   } else
   {
      return -1;
   }
}



static boolean QDSSisReady(void)
{
   return (qdss_ctxt.eQDSSState==QDSS_READY);
}

boolean QDSSisInUse(void)
{
   return (qdss_ctxt.eQDSSState==QDSS_IN_USE);
}

void QDSSErrInit(void)
{
   if(qdss_ctxt.eQDSSState == QDSS_READY)
   {
      err_crash_cb_register(QDSSFlush);
   }
}

void QDSSFlush(void)
{
/**pre-requisite is that STM should configure :
  HWIO_OUTF(QDSS_STMSPTRIGCSR,ATBTRIGEN_DIR,HIGH);
  HWIO_OUTF(QDSS_STMSPTRIGCSR,TRIGCTL,1/0);//single-shot or multi-shot mode
  ETB/ETF should configure FFCR=0x623=TrigOnFl|TrigOnTrigEvt|FOnTrigEvt|EnTI|EnFt
**/
/*pre-requisite for flush is QDSS is in IN_USE state*/   
   if((QDSSisInUse()==TRUE) && (qdss_ctxt.local_stm_port!=NULL) )
   {
      DalSTMTrace_Trigger(qdss_ctxt.hTrace,qdss_ctxt.local_stm_port,TRACE_TRIG_GUARANTEED|TRACE_TRIG_TIMESTAMPED);
   }
}

static int32 QDSSAttachDrivers(void)
{
   int nErr;
#ifdef QDSS_STANDALONE_MODE

   TRY(nErr,DAL_TMCDeviceAttach("DALDEVICEID_TMC",&qdss_ctxt.hTMC));
   TRY(nErr,DAL_STMCfgDeviceAttach("DALDEVICEID_STM_CONFIG",&qdss_ctxt.hSTMCfg));

#endif /*QDSS_STANDALONE_MODE*/

   TRY(nErr,DAL_STMTraceDeviceAttach("DALDEVICEID_STM_TRACE_SLPI",&qdss_ctxt.hTrace));
   if (qdss_ctxt.hTrace==NULL) {
      THROW(nErr,-1);
   }

   TRY(nErr,DalSTMTrace_NewPort(qdss_ctxt.hTrace,&qdss_ctxt.local_stm_port));



   TRY(nErr,DAL_STMTraceDeviceAttach("DALDEVICEID_STM_TRACE",&qdss_ctxt.hTraceChip));
   if (qdss_ctxt.hTraceChip==NULL) {
      THROW(nErr,-1);
   }

   TRY(nErr,DalSTMTrace_NewPort(qdss_ctxt.hTraceChip,&qdss_ctxt.chip_stm_port));


   CATCH(nErr) {}
   return nErr;   
}

#ifdef QDSS_STANDALONE_MODE

/**
* QDSS_STANDALONE_MODE - typically used on Virtio
* NPA is not functional.
* The assumption is QDSS clocks will be enabled by platform of via JTAG
**/

static int32 QDSSChipClkOn(void) 
{
   return 0;
}

static int32 QDSSChipClkOff(void) 
{
   return 0;
}

#else

static int32 QDSSChipClkOn(void)
{
   if (NULL==qdss_ctxt.npa_client) {
      qdss_ctxt.npa_client = npa_create_sync_client("/clk/qdss", "qdss", NPA_CLIENT_REQUIRED);
   }

   if (NULL==qdss_ctxt.npa_client) {
      return -1;
   }

   npa_issue_required_request(qdss_ctxt.npa_client, CLOCK_QDSS_LEVEL_DEBUG);    return 0;
}

static int32 QDSSChipClkOff(void)
{
   if(NULL==qdss_ctxt.npa_client) {
      return -1;
   }
   npa_issue_required_request(qdss_ctxt.npa_client, CLOCK_QDSS_LEVEL_OFF);
   return 0;
}

#endif



#define _TRY(nErr, func)  (func)

int QDSSLocalClkOn(void)
{
   int nErr;

   if (NULL==qdss_ctxt.clk.hClock) {
      TRY(nErr,DAL_ClockDeviceAttach(DALDEVICEID_CLOCK, 
                                     &qdss_ctxt.clk.hClock));

      _TRY(nErr,DalClock_GetClockId(qdss_ctxt.clk.hClock,
                                   "scc_at_clk",
                                   &qdss_ctxt.clk.ssc_at_clk_id));

      _TRY(nErr,DalClock_GetClockId(qdss_ctxt.clk.hClock,
                                   "scc_q6_atbm_clk",
                                    &qdss_ctxt.clk.ssc_q6_atbm_clk_id));

      _TRY(nErr,DalClock_GetClockId(qdss_ctxt.clk.hClock,
                                   "scc_pclkdbg_clk",
                                    &qdss_ctxt.clk.ssc_pclkdbg_clk_id));

      _TRY(nErr,DalClock_GetClockId(qdss_ctxt.clk.hClock,
                                   "scc_dbg_tsctr_clk",
                                   &qdss_ctxt.clk.ssc_dbg_tsctr_clk_id));
   }

   TRY(nErr,DalClock_EnableClock(qdss_ctxt.clk.hClock,qdss_ctxt.clk.ssc_at_clk_id));

   TRY(nErr,DalClock_EnableClock(qdss_ctxt.clk.hClock,qdss_ctxt.clk.ssc_q6_atbm_clk_id));

   TRY(nErr,DalClock_EnableClock(qdss_ctxt.clk.hClock,qdss_ctxt.clk.ssc_pclkdbg_clk_id));

   TRY(nErr,DalClock_EnableClock(qdss_ctxt.clk.hClock,qdss_ctxt.clk.ssc_dbg_tsctr_clk_id));

   CATCH(nErr) {}

   return nErr;
}

int QDSSLocalClkOff(void)
{
   //SSC qdss clocks are off by default due to save power 
   //However, if they are turned on for debugging/tracing
   //the clocks will remain on.
   return 0;
}

int _QDSSLocalClkOff(void)
{
   int nErr;

   if (NULL == qdss_ctxt.clk.hClock) {
      return -1;
   }

   TRY(nErr,DalClock_DisableClock(qdss_ctxt.clk.hClock,qdss_ctxt.clk.ssc_at_clk_id));

   TRY(nErr,DalClock_DisableClock(qdss_ctxt.clk.hClock,qdss_ctxt.clk.ssc_q6_atbm_clk_id));

   TRY(nErr,DalClock_DisableClock(qdss_ctxt.clk.hClock,qdss_ctxt.clk.ssc_pclkdbg_clk_id));

   TRY(nErr,DalClock_DisableClock(qdss_ctxt.clk.hClock,qdss_ctxt.clk.ssc_dbg_tsctr_clk_id));
   CATCH(nErr) {}
   return nErr;
}




static int32 QDSSClkOn(void)
{
   int nErr1,nErr2;

   nErr1 = QDSSLocalClkOn();
   nErr2 = QDSSChipClkOn();

   return (nErr1|nErr2);

}

static int32 QDSSClkOff(void)
{
   int nErr1,nErr2;

   nErr1 = QDSSLocalClkOff();
   nErr2 = QDSSChipClkOff();

   return (nErr1|nErr2);
}


static int32 QDSSchangeState(QDSSState new_state) 
{
   int32 ret=-1;
   
   switch(qdss_ctxt.eQDSSState){
      case QDSS_OFF:
         if (new_state==QDSS_READY)
         {
            if(QDSSAttachDrivers()==0)
            {
/*TBD: efuse check before changing to READY*/    
/*TBD: in standalone mode skip efuse check */
               qdss_ctxt.eQDSSState=new_state;
               ret=0;
            }
         }
         break;
      case QDSS_READY:
         if(new_state==QDSS_IN_USE)
         {
            if(QDSSClkOn()==0)
            {
               qdss_ctxt.eQDSSState=new_state;
               ret=0;               
            }
         }
         break;
      case QDSS_IN_USE:
         if(new_state==QDSS_READY)
         {
            if(QDSSClkOff()==0)
            {
               qdss_ctxt.eQDSSState=new_state;
               ret=0;               
            }
         }
         break;
      default:
         break;
   }
   return ret;
}

void tracer_log_sync_event(uint32 port, tracer_event_id_t id, uint64 tag);
void tracer_stp_send_event(uint32 port, tracer_event_id_t id);
int qdss_stm_async_gen(void);

//For Each D16 takes up 3 bytes, For 1K of padding we need this much
#define QDSS_END_MARKER_PADDING 342

void QDSSEndMarker(tracer_event_id_t id)
{
   int i;
   if (qdss_ctxt.local_stm_port!=NULL)  {
      tracer_stp_send_event(qdss_ctxt.local_stm_port,id);
      /* On 8996 ATB bridge that connects SSC ATB to main QDDS ATB
         lacks proper low power mode support. Sensor island trace data
         that is left in the ATB bridge buffers after it is drained and 
         flushed from SSC is lost or corrupted. 
         HW CR QCTDD02182478 tracks the ATB async bridge issue.
        
         This workaround pads the end of island mode trace with a series
         of end-markers, so that what is left on the ATB bridge when it enters
         retention is END markers. We pad sufficient number of END marker
         packets so that at least one of them will cross the ATB bridge and is
         available in the main QDSS intact. Parser on the PC side when it sees
         and END marker, will skip everything until the next ASYNC.
         This way loss/corruption of valid trace held in ASYNC brdige is avoided
      */
      for (i=0; i< QDSS_END_MARKER_PADDING; i++) {
         DalSTMTrace_Data16(qdss_ctxt.hTrace,
                            qdss_ctxt.local_stm_port,
                            TRACE_DATA_MARKED,
                            0x1111);
      }
   }
}


static uint64 QDSSGetTimeTicks(void)
{
   int nErr;
   uint64 now=0;

   if (0==qdss_ctxt.hTimetick) {
      TRY(nErr,DalTimetick_Attach("SystemTimer", &qdss_ctxt.hTimetick));
      TRY(nErr,DalTimetick_Enable(qdss_ctxt.hTimetick,1));
      TRY(nErr,DalTimetick_InitTimetick64(qdss_ctxt.hTimetick));
   }
   TRY(nErr,DalTimetick_GetTimetick64(qdss_ctxt.hTimetick, &now));

   CATCH(nErr) {};
   return now;
}




void QDSSSyncMarkers(void)
{
   DalSTMTrace_Data16
      (qdss_ctxt.hTraceChip,
       qdss_ctxt.chip_stm_port,
       TRACE_DATA_GUARANTEED | TRACE_DATA_MARKED | TRACE_DATA_TIMESTAMPED,
       qdss_ctxt.sync_tag);

   DalSTMTrace_Data16
      (qdss_ctxt.hTrace,
       qdss_ctxt.local_stm_port,
       TRACE_DATA_GUARANTEED | TRACE_DATA_MARKED | TRACE_DATA_TIMESTAMPED,
       qdss_ctxt.sync_tag);

   qdss_ctxt.sync_tag++;
}

void QDSSLogSyncEvents(tracer_event_id_t id)
{
   uint64 tag=0;

   if( (!QDSSisInUse()) ||
       (qdss_ctxt.local_stm_port==NULL) ||
       (qdss_ctxt.chip_stm_port == NULL)) {
      return;
   }

   QDSSSyncMarkers();

   tag=QDSSGetTimeTicks();
   tracer_log_sync_event(qdss_ctxt.chip_stm_port,id,tag);
   tracer_log_sync_event(qdss_ctxt.local_stm_port,id,tag);
}


void QDSSModeSwitchMarker(tracer_event_id_t id)
{  
   if (qdss_ctxt.local_stm_port!=NULL)  {
      qdss_stm_async_gen();
      tracer_stp_send_event(qdss_ctxt.local_stm_port,id);
   }
}

#define SSC_STM_TSCTR_FREQ  25000000 //Default

uint32 qdss_get_ssc_tsctr_freq(void) 
{
   uint32 freq = 0;

   if (qdss_ctxt.clk.hClock) {
      DalClock_GetClockFrequency(qdss_ctxt.clk.hClock,
                                 qdss_ctxt.clk.ssc_dbg_tsctr_clk_id,
                                 &freq);
   }

   if (0==freq) {
      freq=SSC_STM_TSCTR_FREQ;
   }

#ifdef QDSS_SLPI_V1
   freq = (freq >> 1); //tsctr div-2
#endif
   
   return freq;
}

