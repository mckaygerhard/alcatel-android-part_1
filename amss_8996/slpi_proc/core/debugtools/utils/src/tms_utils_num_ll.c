/** vi: tw=128 ts=3 sw=3 et
@file tms_utils_num_ll.c
@brief This file contains the API for the TMS Utilities, API 0.2.0
*/
/*=============================================================================
NOTE: The @brief description above does not appear in the PDF.
The tms_mainpage.dox file contains the group/module descriptions that
are displayed in the output PDF generated using Doxygen and LaTeX. To
edit or update any of the group/module text in the PDF, edit the
tms_mainpage.dox file or contact Tech Pubs.
===============================================================================*/
/*=============================================================================
Copyright (c) 2014 QUALCOMM Technologies Incorporated.
All rights reserved.
QUALCOMM Confidential and Proprietary.
=============================================================================*/
/*=============================================================================
Edit History
$Header: //components/rel/core.slpi/1.0/debugtools/utils/src/tms_utils_num_ll.c#1 $
$DateTime: 2014/09/25 11:54:23 $
$Change: 6661231 $
$Author: pwbldsvc $
===============================================================================*/

#if !defined(TMS_UTILS_EXCLUDE_MSG_SWEVT)
#include "msg.h"
#endif

#if !defined(TMS_UTILS_EXCLUDE_TRACER_SWEVT)
#include "tracer.h"
#include "tms_utils_tracer_swe.h"
#endif

// Function must remain reentrant and not utilize NHLOS or external library calls which
// are not reentrant or potentially cause any type of NHLOS blocking to occur.

#include "tms_utils.h"

#define ASCII_ZERO      '0'         // ASCII ZERO
#define ASCII_SIGN      '-'         // ASCII
#define ASCII_PLUS      '+'         // ASCII

#define ASCII_A         'a'         // ASCII

int64_t tms_utils_num_int64(tms_chr const* in_buf_p, size_t in_buf_sz)
{
   int64_t rc = 0;
   tms_chr sign;
   tms_chr num_chr;
   tms_chr const* num_str = in_buf_p;

   // Skip leading space

   while (num_str - in_buf_p < in_buf_sz && isspace(*num_str))
   {
      num_str++;
   }

   // Bail out check

   if (num_str - in_buf_p >= in_buf_sz)
   {
      return rc;
   }

   // Record presense of a SIGN

   sign = num_chr = *num_str++;

   // Bail out check

   if (num_str - in_buf_p >= in_buf_sz)
   {
      return rc;
   }

   // Skip over any SIGN presence

   if (ASCII_SIGN == num_chr || ASCII_PLUS == num_chr)
   {
      num_chr = *num_str++;
   }

   // Bail out check

   if (num_str - in_buf_p >= in_buf_sz)
   {
      return rc;
   }

   // Loop Across Digits Obtain Value

   while (num_str - in_buf_p < in_buf_sz && isdigit(num_chr))
   {
      rc = 10 * rc + (num_chr - ASCII_ZERO);

      num_chr = *num_str++;
   }

   // If SIGN presence then adjust return

   if (ASCII_SIGN == sign)
   {
      return 0 - rc;
   }

   else
   {
      return rc;
   }
}

int32_t tms_utils_num_int32(tms_chr const* in_buf_p, size_t in_buf_sz)
{
   return (int32_t)tms_utils_num_int64(in_buf_p, in_buf_sz);
}
