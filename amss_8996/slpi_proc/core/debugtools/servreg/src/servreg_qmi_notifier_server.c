/*
#============================================================================
#  Name:
#    servreg_qmi_notifier_server.c
#
#  Description:
#    Service Registry notifier file for root image. This module serves as the end-point
#    of communication via QMI.
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/
#include "stdarg.h"
#include "stdlib.h"
#include <stringl/stringl.h>
#include "comdef.h"             /* Definitions for byte, word, etc.     */
#include "err.h"
#include "qurt.h"
#include "msg.h"
#include "tms_utils.h"
#include "qmi_csi.h"
#include "qmi_csi_target_ext.h"
#include "timer.h"
#include "rcinit.h"

#include "service_registry_notifier_v01.h"
#include "servreg_internal.h"
#include "servreg_locator.h"
#include "servreg_localdb.h"
#include "servreg_monitor.h"
#include "servreg_monitor_qurt.h"
#include "servreg_notifier.h"
#include "servreg_qmi_notifier_server.h"

#define SERVREG_NOTIF_STATE_CHANGE_MASK                   0x2
#define SERVREG_QMI_NOTIF_SERVER_TIMER_MASK               0x40
#define SERVREG_QMI_NOTIF_SERVER_TIMEOUT_MS               5000
#define SERVREG_QMI_NOTIF_SERVER_REQ_TIMEOUT_MS           1000
#define SERVREG_QMI_NOTIF_SERVER_INT_TIMEOUT_MS           5000

static struct
{
   qurt_thread_t tid;

} servreg_qmi_notif_ind_server_internal;

/* QMI message related and keeping it global because it should be valid for the lifetime of the user_handle */
struct servreg_notif_client_type_s
{
   qmi_client_handle  client_handle;
   boolean            connected;
   struct servreg_notif_client_type_s* next;
};
typedef struct servreg_notif_client_type_s servreg_notif_client_type_t, * servreg_notif_client_type_p;

servreg_notif_client_type_p qmi_notif_client_head = SERVREG_NULL;
static qmi_csi_service_handle servreg_notif_qmi_server_handler = NULL;

qurt_anysignal_t servreg_notif_signal;

typedef enum
{
   SERVREG_NOTIF_REGISTER_LISTENER_REQ      = QMI_SERVREG_NOTIF_REGISTER_LISTENER_REQ_V01,
   SERVREG_NOTIF_QUERY_STATE_REQ            = QMI_SERVREG_NOTIF_QUERY_STATE_REQ_V01,
   SERVREG_NOTIF_STATE_UPDATED_IND          = QMI_SERVREG_NOTIF_STATE_UPDATED_IND_V01,
   SERVREG_NOTIF_STATE_UPDATED_IND_ACK_REQ  = QMI_SERVREG_NOTIF_STATE_UPDATED_IND_ACK_REQ_V01,
}servreg_notif_msg_req_type;

/* QMI client list entry structure */
struct servreg_qmi_notif_server_entry_s
{
   /* Client handle that is registering a listener */
   qmi_client_handle client_handle;

   struct servreg_qmi_notif_server_entry_s * next;
};
typedef struct servreg_qmi_notif_server_entry_s servreg_qmi_notif_server_entry_t, * servreg_qmi_notif_server_entry_p;

/* Service Registry Notifier ROOT node structure. List of SERVREG_MON_HANDLE'S*/
struct servreg_qmi_notif_server_node_s
{
   uint32_t notif_signature;

   /* Service mon handle for the service the client is interested in */
   SERVREG_MON_HANDLE sr_mon_handle;

   struct servreg_qmi_notif_server_entry_s * qmi_client_list_head;

   /* Service state */
   SERVREG_SERVICE_STATE curr_state;

   struct servreg_qmi_notif_server_node_s* next;
};
typedef struct servreg_qmi_notif_server_node_s servreg_qmi_notif_server_node_t, * servreg_qmi_notif_server_node_p;

/* Type casts as accessor functions */
#define sr_qmi_notif_server_node2sr_qmi_notif_server_handle(x)        ((SERVREG_QMI_NOTIF_SERVER_HANDLE)x)
#define sr_qmi_notif_server_handle2sr_qmi_notif_server_node(x)        ((servreg_qmi_notif_server_node_p)x)

/* Pool Allocations */
/* Pool allocation for QMI client list entry */
struct servreg_qmi_notif_server_entry_pool_s
{
   struct servreg_qmi_notif_server_entry_s servreg_qmi_notif_server_entry_pool[SERVREG_QMI_NOTIF_SERVER_ENTRY_POOL_SIZE];
   struct servreg_qmi_notif_server_entry_pool_s * next;
};
typedef struct servreg_qmi_notif_server_entry_pool_s servreg_qmi_notif_server_entry_pool_t, * servreg_qmi_notif_server_entry_pool_p;

/* Pool Allocations Notif node */
struct servreg_qmi_notif_server_node_pool_s
{
   struct servreg_qmi_notif_server_node_s servreg_qmi_notif_server_node_pool[SERVREG_QMI_NOTIF_SERVER_NODE_POOL_SIZE];
   struct servreg_qmi_notif_server_node_pool_s * next;
};
typedef struct servreg_qmi_notif_server_node_pool_s servreg_qmi_notif_server_node_pool_t, * servreg_qmi_notif_server_node_pool_p;

/* Internal structure */
struct servreg_qmi_notif_server_node_internal_s
{
   servreg_qmi_notif_server_entry_pool_p servreg_qmi_notif_server_entry_pool_head_p;
   servreg_qmi_notif_server_node_pool_p servreg_qmi_notif_server_node_pool_head_p;
   servreg_qmi_notif_server_entry_p servreg_qmi_notif_server_entry_pool_free_p;
   servreg_qmi_notif_server_node_p servreg_qmi_notif_server_node_pool_free_p;
   servreg_mutex_t mutex;
   servreg_mutex_t mutex_create;
   SERVREG_BOOL dynamic_use;
   unsigned long init_flag;
};

struct servreg_qmi_notif_server_node_internal_s servreg_qmi_notif_server_node_internal;
servreg_qmi_notif_server_entry_pool_t servreg_qmi_notif_server_entry_pool_static;
servreg_qmi_notif_server_node_pool_t servreg_qmi_notif_server_node_pool_static;

/* Head node of the servreg notification list */
servreg_qmi_notif_server_node_p servreg_qmi_notif_server_list_head = SERVREG_NULL;

/* Static functions in this file */
static servreg_qmi_notif_server_entry_p servreg_qmi_notif_server_entry_pool_init(void);
static servreg_qmi_notif_server_entry_p servreg_qmi_notif_server_entry_pool_alloc(void);
static servreg_qmi_notif_server_entry_p servreg_qmi_notif_server_entry_pool_free(servreg_qmi_notif_server_entry_p sr_qmi_notif_server_entry);
static servreg_qmi_notif_server_node_p servreg_qmi_notif_server_node_pool_init(void);
static servreg_qmi_notif_server_node_p servreg_qmi_notif_server_node_pool_alloc(void);
static servreg_qmi_notif_server_node_p servreg_qmi_notif_server_node_pool_free(servreg_qmi_notif_server_node_p servreg_qmi_notif_server_node_p);
static void servreg_qmi_notif_server_node_internal_init(void);

static SERVREG_QMI_NOTIF_SERVER_HANDLE servreg_get_qmi_notif_server_node(SERVREG_MON_HANDLE sr_mon_handle);
static SERVREG_QMI_NOTIF_SERVER_HANDLE servreg_get_qmi_notif_server_entry(SERVREG_MON_HANDLE sr_mon_handle, qmi_client_handle client_handle);
static SERVREG_QMI_NOTIF_SERVER_HANDLE servreg_create_qmi_notif_server_node(SERVREG_MON_HANDLE sr_mon_handle, qmi_client_handle client_handle);
static SERVREG_RESULT servreg_delete_qmi_notif_server_node(SERVREG_QMI_NOTIF_SERVER_HANDLE sr_qmi_notif_server_handle, qmi_client_handle client_handle);

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_server_entry_pool_init
 *
 * Description:
 *     Initializes the memory pool for domain list entry structure
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_qmi_notif_server_entry_p : Returns the first free dl_entry space from the pool
 * =====================================================================  */
static servreg_qmi_notif_server_entry_p servreg_qmi_notif_server_entry_pool_init(void)
{
   servreg_qmi_notif_server_entry_pool_p next_pool = SERVREG_NULL;

   if (SERVREG_NULL == servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_entry_pool_head_p)
   {
      next_pool = &servreg_qmi_notif_server_entry_pool_static;
   }
   else if (SERVREG_TRUE == servreg_qmi_notif_server_node_internal.dynamic_use)
   {
      next_pool = (servreg_qmi_notif_server_entry_pool_p)servreg_malloc(sizeof(servreg_qmi_notif_server_entry_pool_t));
   }

   if (SERVREG_NULL != next_pool)
   {
      int i;

      for (i = 0; i < SERVREG_LOC_ENTRY_POOL_SIZE; i++)
      {
         if (i != (SERVREG_LOC_ENTRY_POOL_SIZE - 1))
         {
            next_pool->servreg_qmi_notif_server_entry_pool[i].next = &(next_pool->servreg_qmi_notif_server_entry_pool[i + 1]);
         }
         else
         {
            next_pool->servreg_qmi_notif_server_entry_pool[i].next = servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_entry_pool_free_p;
         }

         next_pool->servreg_qmi_notif_server_entry_pool[i].client_handle = NULL;
      }

      servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_entry_pool_free_p = &(next_pool->servreg_qmi_notif_server_entry_pool[0]);
      next_pool->next = servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_entry_pool_head_p;
      servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_entry_pool_head_p = next_pool;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_qmi_notif_server_entry_pool_init() malloc failed");
      return SERVREG_NULL;
   }

   return servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_entry_pool_free_p;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_server_entry_pool_alloc
 *
 * Description:
 *     Gives the first available free and allocated space from the memory
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_qmi_notif_server_entry_p : the first available free and allocated space from the memory
 * =====================================================================  */
static servreg_qmi_notif_server_entry_p servreg_qmi_notif_server_entry_pool_alloc(void)
{
   servreg_qmi_notif_server_entry_p ret;
   servreg_qmi_notif_server_entry_p sr_qmi_notif_server_entry;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_server_node_internal.mutex));

   if (SERVREG_NULL == servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_entry_pool_free_p)
   {
      sr_qmi_notif_server_entry = servreg_qmi_notif_server_entry_pool_init();
   }
   else
   {
      sr_qmi_notif_server_entry = servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_entry_pool_free_p;
   }

   if (SERVREG_NULL != sr_qmi_notif_server_entry)
   {
      servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_entry_pool_free_p = sr_qmi_notif_server_entry->next;
      sr_qmi_notif_server_entry->next = SERVREG_NULL;
      ret = sr_qmi_notif_server_entry;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_qmi_notif_server_entry_pool_alloc() alloc failed");
      ret = SERVREG_NULL;
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_server_node_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_server_entry_pool_free
 *
 * Description:
 *     Reclaims back the sr_qmi_notif_server_entry to the memory pool
 *
 * Parameters:
 *     sr_qmi_notif_server_entry : space to be reclaimed back
 *
 * Returns:
 *    servreg_qmi_notif_server_entry_p : The next available free space in the memory pool
 * =====================================================================  */
static servreg_qmi_notif_server_entry_p servreg_qmi_notif_server_entry_pool_free(servreg_qmi_notif_server_entry_p sr_qmi_notif_server_entry)
{
   servreg_qmi_notif_server_entry_p ret = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_server_node_internal.mutex));

   if(SERVREG_NULL != sr_qmi_notif_server_entry)
   {
      sr_qmi_notif_server_entry->client_handle = NULL;

      sr_qmi_notif_server_entry->next = servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_entry_pool_free_p;
      servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_entry_pool_free_p = sr_qmi_notif_server_entry;
      ret = sr_qmi_notif_server_entry;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_qmi_notif_server_entry_pool_free() sr_qmi_notif_server_entry is NULL and cannot be freed");
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_server_node_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_server_node_pool_init
 *
 * Description:
 *     Initializes the memory pool for notifier node structure
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_qmi_notif_server_node_p : Returns the first free notifier node space from the pool
 * =====================================================================  */
static servreg_qmi_notif_server_node_p servreg_qmi_notif_server_node_pool_init(void)
{
   servreg_qmi_notif_server_node_pool_p next_pool = SERVREG_NULL;

   if (SERVREG_NULL == servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_node_pool_head_p)
   {
      next_pool = &servreg_qmi_notif_server_node_pool_static;
   }
   else if (SERVREG_TRUE == servreg_qmi_notif_server_node_internal.dynamic_use)
   {
      next_pool = (servreg_qmi_notif_server_node_pool_p)servreg_malloc(sizeof(servreg_qmi_notif_server_node_pool_t));
   }

   if (SERVREG_NULL != next_pool)
   {
      int i;

      for (i = 0; i < SERVREG_QMI_NOTIF_SERVER_NODE_POOL_SIZE; i++)
      {
         if (i != (SERVREG_QMI_NOTIF_SERVER_NODE_POOL_SIZE - 1))
         {
            next_pool->servreg_qmi_notif_server_node_pool[i].next = &(next_pool->servreg_qmi_notif_server_node_pool[i + 1]);
         }
         else
         {
            next_pool->servreg_qmi_notif_server_node_pool[i].next = servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_node_pool_free_p;
         }

         next_pool->servreg_qmi_notif_server_node_pool[i].notif_signature = SERVREG_QMI_NOTIF_SERVER_SIGNATURE;
         next_pool->servreg_qmi_notif_server_node_pool[i].sr_mon_handle = SERVREG_NULL;
         next_pool->servreg_qmi_notif_server_node_pool[i].qmi_client_list_head = SERVREG_NULL;
         next_pool->servreg_qmi_notif_server_node_pool[i].curr_state = SERVREG_SERVICE_STATE_UNINIT;
      }

      servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_node_pool_free_p = &(next_pool->servreg_qmi_notif_server_node_pool[0]);
      next_pool->next = servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_node_pool_head_p;
      servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_node_pool_head_p = next_pool;
   }
   else
   {
      return SERVREG_NULL;
   }

   return servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_node_pool_free_p;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_server_node_pool_alloc
 *
 * Description:
 *     Gives the first available free and allocated space from the memory
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_qmi_notif_server_node_p : the first available free and allocated space from the memory
 * =====================================================================  */
static servreg_qmi_notif_server_node_p servreg_qmi_notif_server_node_pool_alloc(void)
{
   servreg_qmi_notif_server_node_p ret;
   servreg_qmi_notif_server_node_p sr_qmi_notif_server_node;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_server_node_internal.mutex));

   if (SERVREG_NULL == servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_node_pool_free_p)
   {
      sr_qmi_notif_server_node = servreg_qmi_notif_server_node_pool_init();
   }
   else
   {
      sr_qmi_notif_server_node = servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_node_pool_free_p;
   }

   if (SERVREG_NULL != sr_qmi_notif_server_node)
   {
      servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_node_pool_free_p = sr_qmi_notif_server_node->next;
      sr_qmi_notif_server_node->next = SERVREG_NULL;
      ret = sr_qmi_notif_server_node;
   }
   else
   {
      ret = SERVREG_NULL;
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_server_node_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_server_node_pool_free
 *
 * Description:
 *     Reclaims back the sr_qmi_notif_server_node to the memory pool
 *
 * Parameters:
 *     sr_qmi_notif_server_node : space to be reclaimed back
 *
 * Returns:
 *    servreg_qmi_notif_server_node_p : The next available free space in the memory pool
 * =====================================================================  */
static servreg_qmi_notif_server_node_p servreg_qmi_notif_server_node_pool_free(servreg_qmi_notif_server_node_p sr_qmi_notif_server_node)
{
   servreg_qmi_notif_server_node_p ret = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_server_node_internal.mutex));

   if(SERVREG_NULL != sr_qmi_notif_server_node)
   {
      sr_qmi_notif_server_node->sr_mon_handle = SERVREG_NULL;
      sr_qmi_notif_server_node->qmi_client_list_head = SERVREG_NULL;
      sr_qmi_notif_server_node->curr_state = SERVREG_SERVICE_STATE_UNINIT;

      sr_qmi_notif_server_node->next = servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_node_pool_free_p;
      servreg_qmi_notif_server_node_internal.servreg_qmi_notif_server_node_pool_free_p = sr_qmi_notif_server_node;
      ret = sr_qmi_notif_server_node;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_qmi_notif_server_node_pool_free() sr_qmi_notif_server_node is NULL and cannot be freed");
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_server_node_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_server_node_internal_init
 *
 * Description:
 *     Initialization of the internal memory pools and other internals
 *     for sr notifier nodes
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_qmi_notif_server_node_internal_init(void)
{
   servreg_mutex_init_dal(&(servreg_qmi_notif_server_node_internal.mutex));
   servreg_mutex_init_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

   servreg_mutex_lock_dal(&(servreg_qmi_notif_server_node_internal.mutex));

   servreg_qmi_notif_server_node_internal.dynamic_use = TRUE;
   servreg_qmi_notif_server_node_pool_init();
   servreg_qmi_notif_server_entry_pool_init();

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_server_node_internal.mutex));
}

/** =====================================================================
 * Function:
 *     servreg_get_qmi_notif_server_node
 *
 * Description:
 *     Checks if a node already exists with the given name. If it does 
 *     exists it returns a pointer to that node.
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by just domain name.
 *
 * Returns:
 *    SERVREG_QMI_NOTIF_SERVER_HANDLE : handle to the sr notif node
 * =====================================================================  */
static SERVREG_QMI_NOTIF_SERVER_HANDLE servreg_get_qmi_notif_server_node(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_qmi_notif_server_node_p sr_qmi_notif_server_node = SERVREG_NULL;
   SERVREG_QMI_NOTIF_SERVER_HANDLE sr_qmi_notif_server_handle = SERVREG_NULL;

   sr_qmi_notif_server_node = servreg_qmi_notif_server_list_head;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

   while(SERVREG_NULL != sr_qmi_notif_server_node)
   {
      /* Check if the same service is already registered */
      if(sr_mon_handle == sr_qmi_notif_server_node->sr_mon_handle)
      {
         sr_qmi_notif_server_handle = sr_qmi_notif_server_node2sr_qmi_notif_server_handle(sr_qmi_notif_server_node);
         break;
      }
      sr_qmi_notif_server_node = sr_qmi_notif_server_node->next;
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

   return sr_qmi_notif_server_handle;
}


/** =====================================================================
 * Function:
 *     servreg_get_qmi_notif_server_entry
 *
 * Description:
 *     Checks if a qmi entry already exists with the given name. If it does 
 *     exists it returns a pointer to that node.
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by just domain name.
 *
 * Returns:
 *    SERVREG_QMI_NOTIF_SERVER_HANDLE : handle to the sr notif node
 * =====================================================================  */
static SERVREG_QMI_NOTIF_SERVER_HANDLE servreg_get_qmi_notif_server_entry(SERVREG_MON_HANDLE sr_mon_handle, qmi_client_handle client_handle)
{
   servreg_qmi_notif_server_node_p sr_qmi_notif_server_node = SERVREG_NULL;
   SERVREG_QMI_NOTIF_SERVER_HANDLE sr_qmi_notif_server_handle = SERVREG_NULL;
   servreg_qmi_notif_server_entry_p sr_qmi_notif_server_entry = SERVREG_NULL;
   SERVREG_BOOL sr_entry_found = SERVREG_FALSE;

   sr_qmi_notif_server_node = servreg_qmi_notif_server_list_head;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

   while(SERVREG_NULL != sr_qmi_notif_server_node)
   {
      /* Check if the same remote qmi client is already registered as listener for the same service */
      if(sr_mon_handle == sr_qmi_notif_server_node->sr_mon_handle)
      {
         sr_qmi_notif_server_entry = sr_qmi_notif_server_node->qmi_client_list_head;

         /* Walk through the qmi_client list and find the matching qmi_client handle */
         while(SERVREG_NULL != sr_qmi_notif_server_entry)
         {
            if(client_handle == sr_qmi_notif_server_entry->client_handle)
            {
               sr_qmi_notif_server_handle = sr_qmi_notif_server_node2sr_qmi_notif_server_handle(sr_qmi_notif_server_node);
               sr_entry_found = SERVREG_TRUE;
               break;
            }
            else
            {
               sr_qmi_notif_server_entry = sr_qmi_notif_server_entry->next;
            }
         }
      }

      if(SERVREG_TRUE != sr_entry_found)
      {
         sr_qmi_notif_server_node = sr_qmi_notif_server_node->next;
      }
      else
      {
         break;
      }
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

   return sr_qmi_notif_server_handle;
}

/** =====================================================================
 * Function:
 *     servreg_create_qmi_notif_server_node
 *
 * Description:
 *     Creates a srnotif node with the given sr_mon_handle
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by just domain name.
 *     curr_state : curr_state of the service
 *
 * Returns:
 *    SERVREG_QMI_NOTIF_SERVER_HANDLE : handle to the sr notif node
 * =====================================================================  */
static SERVREG_QMI_NOTIF_SERVER_HANDLE servreg_create_qmi_notif_server_node(SERVREG_MON_HANDLE sr_mon_handle, qmi_client_handle client_handle)
{
   servreg_qmi_notif_server_node_p sr_qmi_notif_server_node = SERVREG_NULL;  
   SERVREG_QMI_NOTIF_SERVER_HANDLE sr_qmi_notif_server_handle = SERVREG_NULL, sr_qmi_notif_server_handle_with_entry = SERVREG_NULL;
   servreg_qmi_notif_server_entry_p sr_qmi_notif_server_entry = SERVREG_NULL;

   /* Check if the srnotif node with that event name exists */
   sr_qmi_notif_server_handle = servreg_get_qmi_notif_server_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

   if(SERVREG_NULL == sr_qmi_notif_server_handle)
   {
      sr_qmi_notif_server_node = servreg_qmi_notif_server_node_pool_alloc();

      if(SERVREG_NULL != sr_qmi_notif_server_node)
      {
         /*  Insert to head of list */
         sr_qmi_notif_server_node->next = servreg_qmi_notif_server_list_head;

         /* Update head */
         servreg_qmi_notif_server_list_head = sr_qmi_notif_server_node;

         /* qmi entry */
         sr_qmi_notif_server_entry = servreg_qmi_notif_server_entry_pool_alloc();
         
         if(SERVREG_NULL != sr_qmi_notif_server_entry)
         {
            sr_qmi_notif_server_entry->client_handle = client_handle;
         }
         else
         {
            ERR_FATAL( "SERVREG_QMI_NOTIF: sr_qmi_notif_server_entry malloc failed", 0, 0, 0);
         }

         /* Insert the domain_entry in the head of the list */
         sr_qmi_notif_server_entry->next = sr_qmi_notif_server_node->qmi_client_list_head;

         /* Update the head */
         sr_qmi_notif_server_node->qmi_client_list_head = sr_qmi_notif_server_entry;

         sr_qmi_notif_server_node->sr_mon_handle = sr_mon_handle;
         sr_qmi_notif_server_node->curr_state = servreg_get_service_curr_state(sr_mon_handle);

         sr_qmi_notif_server_handle = sr_qmi_notif_server_node2sr_qmi_notif_server_handle(sr_qmi_notif_server_node);
      }
      else
      {
         ERR_FATAL( "SERVREG_QMI_NOTIF: sr_qmi_notif_server_node malloc failed", 0, 0, 0);
      }
   }
   else
   {
      /* Check if the srnotif entry exists */
      sr_qmi_notif_server_handle_with_entry = servreg_get_qmi_notif_server_entry(sr_mon_handle, client_handle);

      if(SERVREG_NULL == sr_qmi_notif_server_handle_with_entry)
      {
         sr_qmi_notif_server_node = sr_qmi_notif_server_handle2sr_qmi_notif_server_node(sr_qmi_notif_server_handle);

         /* qmi entry */
         sr_qmi_notif_server_entry = servreg_qmi_notif_server_entry_pool_alloc();
         
         if(SERVREG_NULL != sr_qmi_notif_server_entry)
         {
            sr_qmi_notif_server_entry->client_handle = client_handle;
         }
         else
         {
            ERR_FATAL( "SERVREG_QMI_NOTIF: sr_qmi_notif_server_entry malloc failed", 0, 0, 0);
         }

         /* Insert the domain_entry in the head of the list */
         sr_qmi_notif_server_entry->next = sr_qmi_notif_server_node->qmi_client_list_head;

         /* Update the head */
         sr_qmi_notif_server_node->qmi_client_list_head = sr_qmi_notif_server_entry;

         //sr_qmi_notif_server_node->curr_state = servreg_get_service_curr_state(sr_mon_handle);

         sr_qmi_notif_server_handle = sr_qmi_notif_server_node2sr_qmi_notif_server_handle(sr_qmi_notif_server_node);
      }
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

   return sr_qmi_notif_server_handle;
}

/** =====================================================================
 * Function:
 *     servreg_delete_qmi_notif_server_node
 *
 * Description:
 *     Deletes a sr notif node given the sr notif handle
 *
 * Parameters:
 *     sr_qmi_notif_server_handle  : Handle to the notifier node to be deleted
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
static SERVREG_RESULT servreg_delete_qmi_notif_server_node(SERVREG_QMI_NOTIF_SERVER_HANDLE sr_qmi_notif_server_handle, qmi_client_handle client_handle_del)
{
   servreg_qmi_notif_server_node_p sr_qmi_notif_server_node_curr = SERVREG_NULL, sr_qmi_notif_server_node_del = SERVREG_NULL, sr_qmi_notif_server_node_prev = SERVREG_NULL;
   servreg_qmi_notif_server_entry_p sr_qmi_notif_server_entry_curr = SERVREG_NULL, sr_qmi_notif_server_entry_prev = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

   sr_qmi_notif_server_node_curr = servreg_qmi_notif_server_list_head;
   sr_qmi_notif_server_node_del = sr_qmi_notif_server_handle2sr_qmi_notif_server_node(sr_qmi_notif_server_handle);

   if(SERVREG_NULL != sr_qmi_notif_server_node_del)
   {
      if(SERVREG_QMI_NOTIF_SERVER_SIGNATURE == sr_qmi_notif_server_node_del->notif_signature)
      {
         /* First look for the correct service handle in the handle list */
         while(SERVREG_NULL != sr_qmi_notif_server_node_curr)
         {
            if(sr_qmi_notif_server_node_curr == sr_qmi_notif_server_node_del)
            {
               sr_qmi_notif_server_entry_curr = sr_qmi_notif_server_node_del->qmi_client_list_head;

               /* Once the handle is found, then look for the exact client in the client list */
               while(SERVREG_NULL != sr_qmi_notif_server_entry_curr)
               {
                  if(client_handle_del == sr_qmi_notif_server_entry_curr->client_handle)
                  {
                     if(SERVREG_NULL == sr_qmi_notif_server_entry_prev)
                     {
                        sr_qmi_notif_server_node_del->qmi_client_list_head = sr_qmi_notif_server_entry_curr->next;
                     }
                     else
                     {
                        sr_qmi_notif_server_entry_prev = sr_qmi_notif_server_entry_curr->next;
                     }

                     sr_qmi_notif_server_entry_curr->next = SERVREG_NULL;
                     /* Delete the qmi notif entry */
                     servreg_qmi_notif_server_entry_pool_free(sr_qmi_notif_server_entry_curr);
                     break;
                  }

                  sr_qmi_notif_server_entry_prev = sr_qmi_notif_server_entry_curr;
                  sr_qmi_notif_server_entry_curr = sr_qmi_notif_server_entry_curr->next;
               }

               /* Delete the server table entry if no clients listening for that event */
               if(SERVREG_NULL == sr_qmi_notif_server_node_del->qmi_client_list_head)
               {
                  if(SERVREG_NULL == sr_qmi_notif_server_node_prev)
                  {
                     servreg_qmi_notif_server_list_head = sr_qmi_notif_server_node_del->next;
                  }
                  else
                  {
                     sr_qmi_notif_server_node_prev = sr_qmi_notif_server_node_del->next;
                  }

                  sr_qmi_notif_server_node_del->next = SERVREG_NULL;

                  servreg_qmi_notif_server_node_pool_free(sr_qmi_notif_server_node_del);
               }

               break;
            }

            sr_qmi_notif_server_node_prev = sr_qmi_notif_server_node_curr;
            sr_qmi_notif_server_node_curr = sr_qmi_notif_server_node_curr->next;  
            
         } /* while() */
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_delete_qmi_notif_server_node() sr_qmi_notif_server_handle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_delete_qmi_notif_server_node() sr_qmi_notif_server_handle is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_validate_connection_handle
 *
 * Description:
 *     Validate the qmi connection handle received.
 *
 * Parameters:
 *     sconnection_handle : QMI connection handle
 *
 * Returns:
 *    SERVREG_TRUE or SERVREG_FALSE
 * =====================================================================  */
static SERVREG_BOOL servreg_qmi_validate_connection_handle(void *connection_handle)
{
   servreg_notif_client_type_p client = SERVREG_NULL;
   SERVREG_BOOL ret = SERVREG_FALSE;

   client = qmi_notif_client_head;

   /* Validate handle. */
   if(connection_handle != NULL)
   {
      while(SERVREG_NULL != client)
      {
         if(client == (servreg_notif_client_type_p)connection_handle)
         {
            ret = SERVREG_TRUE;
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_qmi_validate_connection_handle() connection_handle is MATCHES \n");
            break;
         }
         client = client->next;
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_qmi_validate_connection_handle() connection_handle does not MATCH \n");
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_qmi_validate_connection_handle() connection_handle is NULL \n");
   }

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_send_qmi_ind_msg_state_change
 *
 * Description:
 *     Check which notif node state has changed and return that notif node's handle
 *
 * Parameters:
 *     servreg_qmi_notif_server_list_head : Head pointer of the notifier handle list for the user PD.
 *
 * Returns:
 *    SERVREG_QMI_NOTIF_SERVER_HANDLE : handle to the sr notif node
 * =====================================================================  */
void servreg_send_qmi_ind_msg_state_change(void)
{
   servreg_qmi_notif_server_node_p sr_qmi_notif_server_node = SERVREG_NULL;
   servreg_qmi_notif_server_entry_p sr_qmi_notif_server_entry = SERVREG_NULL;
   SERVREG_SERVICE_STATE new_curr_state;
   qmi_csi_error err;
   qmi_servreg_notif_state_updated_ind_msg_v01 ind;
   SERVREG_NAME service_name = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

   sr_qmi_notif_server_node = servreg_qmi_notif_server_list_head;

   while(SERVREG_NULL != sr_qmi_notif_server_node)
   {
      new_curr_state = servreg_get_service_curr_state(sr_qmi_notif_server_node->sr_mon_handle);

      if(sr_qmi_notif_server_node->curr_state != new_curr_state)
      {
         /* Send IND message to all the qmi clients if the notif node state has changed */
         sr_qmi_notif_server_node->curr_state = new_curr_state;

         ind.curr_state = (qmi_servreg_notif_service_state_enum_type_v01)new_curr_state;
         service_name = servreg_get_service_name(sr_qmi_notif_server_node->sr_mon_handle);

         if(SERVREG_NULL != service_name)
         {
            strlcpy(ind.service_name, service_name, QMI_SERVREG_NOTIF_NAME_LENGTH_V01+1);
            ind.transaction_id = servreg_get_transaction_id(sr_qmi_notif_server_node->sr_mon_handle);

            /* Get the head of the qmi client list */
            sr_qmi_notif_server_entry = sr_qmi_notif_server_node->qmi_client_list_head;

            /* Walk through the qmi_client list and send out notifications via IND QMI msg */
            while(SERVREG_NULL != sr_qmi_notif_server_entry)
            {

               err = qmi_csi_send_ind((sr_qmi_notif_server_entry->client_handle),
                                      SERVREG_NOTIF_STATE_UPDATED_IND,
                                      &ind,
                                      sizeof(qmi_servreg_notif_state_updated_ind_msg_v01));

               MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_send_qmi_ind_msg_state_change() qmi_csi_send_ind err= %d" ,err);

               sr_qmi_notif_server_entry = sr_qmi_notif_server_entry->next;
            }
         }

         /* Break form the outer while loop as the state change node was found */
         break;
      }
      sr_qmi_notif_server_node = sr_qmi_notif_server_node->next;
   }

   servreg_mutex_unlock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

   return;
}

/** =====================================================================
 * Function:
 *     servreg_notif_server_process_req
 *
 * Description:
 *     This function processes incoming requests for the QMI servreg_notif
 *     services handler.
 *
 * Parameters:
 *      connection_handle: Client connection making request.
 *      req_handle       : QMI request handle
 *      msg_id           : QMI msg request ID
 *      req_c_struct     : QMI request structure
 *      req_c_struct_len : QMI request structure length
 *      service_cookie   : Ignored
 *
 * Returns:
 *     qmi_csi_cb_error
 * =====================================================================  */
qmi_csi_cb_error servreg_notif_server_process_req(void *connection_handle,
                                          qmi_req_handle  req_handle,
                                          unsigned int    msg_id,
                                          void           *req_c_struct,
                                          unsigned int    req_c_struct_len,
                                          void           *service_cookie)
{
   qmi_csi_cb_error retval = QMI_CSI_CB_NO_ERR;
   servreg_notif_client_type_p client = (servreg_notif_client_type_p)connection_handle;
   SERVREG_RESULT ret = SERVREG_FAILURE;
   SERVREG_QMI_NOTIF_SERVER_HANDLE sr_qmi_notif_server_handle = SERVREG_NULL;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;

   if(NULL != req_handle)
   {
      if(SERVREG_TRUE == servreg_qmi_validate_connection_handle(connection_handle) && SERVREG_TRUE == client->connected)
      {
         switch(msg_id)
         {
            case SERVREG_NOTIF_REGISTER_LISTENER_REQ:
            {
               qmi_servreg_notif_register_listener_req_msg_v01 *request = (qmi_servreg_notif_register_listener_req_msg_v01 *)(req_c_struct);
               qmi_servreg_notif_register_listener_resp_msg_v01 resp_send;
               //qmi_csi_error err;

               /* Check if the service belongs to the correct subsystem*/
               if(SERVREG_TRUE == servreg_is_domain_local(request->service_name))
               {
                  /* Register Listener */
                  if(SERVREG_TRUE == request->enable)
                  {
                     sr_mon_handle = servreg_alloc_monitor_handle(request->service_name, SERVREG_NULL);

                     ret = servreg_register_listener_qurt(sr_mon_handle, &servreg_notif_signal, SERVREG_NOTIF_STATE_CHANGE_MASK);

                     if(SERVREG_SUCCESS == ret)
                     {
                        /* Create a qmi notif server node that keep tracks of all the remote qmi listener clients */
                        sr_qmi_notif_server_handle = servreg_create_qmi_notif_server_node(sr_mon_handle, client->client_handle);

                        if(SERVREG_NULL != sr_qmi_notif_server_handle)
                        {
                           MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() REGISTER_LISTENER_REQ success \n");

                           resp_send.curr_state_valid = 1;
                           resp_send.curr_state = (qmi_servreg_notif_service_state_enum_type_v01)servreg_get_service_curr_state(sr_mon_handle);
                           resp_send.resp.result = QMI_RESULT_SUCCESS_V01;
                           resp_send.resp.error  = QMI_ERR_NONE_V01;
                        }
                        else
                        {
                           ERR_FATAL( "SERVREG_QMI_NOTIF: servreg_create_qmi_notif_server_node() failed", 0, 0, 0);
                        }
                     }
                     else
                     {
                        MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() REGISTER_LISTENER_REQ fail \n");

                        ret = servreg_free_monitor_handle(sr_mon_handle);

                        if(SERVREG_SUCCESS != ret)
                        {
                           MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() could not free the monitor handle \n");
                        }

                        resp_send.resp.result = QMI_RESULT_FAILURE_V01;;
                        resp_send.resp.error  = QMI_ERR_INVALID_HANDLE_V01;
                     }
                  }
                  else /* Deregister Listener */
                  {
                     sr_mon_handle = servreg_get_sr_mon_handle(request->service_name);

                     /* Check if the remote qmi client has registered as a listener */
                     sr_qmi_notif_server_handle = servreg_get_qmi_notif_server_entry(sr_mon_handle, client->client_handle);

                     if(SERVREG_NULL != sr_qmi_notif_server_handle)
                     {
                        /* Delete the client handle from the qmi notif server node */
                        ret = servreg_delete_qmi_notif_server_node(sr_qmi_notif_server_handle, client->client_handle);

                        if(SERVREG_SUCCESS == ret)
                        {
                           ret = servreg_deregister_listener_qurt(sr_mon_handle, &servreg_notif_signal, SERVREG_NOTIF_STATE_CHANGE_MASK);

                           if(SERVREG_SUCCESS == ret)
                           {
                              MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() REGISTER_LISTENER_REQ success \n");
                              resp_send.resp.result = QMI_RESULT_SUCCESS_V01;
                              resp_send.resp.error  = QMI_ERR_NONE_V01;
                           }
                           else
                           {
                              ERR_FATAL( "SERVREG_QMI_NOTIF: servreg_deregister_listener_qurt() failed", 0, 0, 0);
                           }
                        }
                        else
                        {
                           ERR_FATAL( "SERVREG_QMI_NOTIF: servreg_delete_qmi_notif_server_node() failed", 0, 0, 0);
                        }
                     }
                     else
                     {
                        MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() DE-REGISTER_LISTENER_REQ fail \n");

                        resp_send.resp.result = QMI_RESULT_FAILURE_V01;;
                        resp_send.resp.error  = QMI_ERR_INVALID_HANDLE_V01;
                     }
                  }
               }
               else
               {
                  resp_send.resp.result = QMI_RESULT_FAILURE_V01;;
                  resp_send.resp.error  = QMI_ERR_INVALID_DATA_FORMAT_V01;
               }

               qmi_csi_send_resp(req_handle, msg_id, &resp_send, sizeof(qmi_servreg_notif_register_listener_resp_msg_v01));

            }
            break;

            case SERVREG_NOTIF_QUERY_STATE_REQ:
            {
               qmi_servreg_notif_query_state_req_msg_v01 *request = (qmi_servreg_notif_query_state_req_msg_v01 *)(req_c_struct);
               qmi_servreg_notif_query_state_resp_msg_v01 resp_send;

               /* First check if the monitor node exists */
               sr_mon_handle = servreg_get_sr_mon_handle(request->service_name);
               if(SERVREG_NULL != sr_mon_handle)
               {
                  MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() STATE_REQ get success\n");
                  resp_send.curr_state_valid = 1;
                  resp_send.curr_state = (qmi_servreg_notif_service_state_enum_type_v01)servreg_get_service_curr_state(sr_mon_handle);
                  resp_send.resp.result = QMI_RESULT_SUCCESS_V01;
                  resp_send.resp.error  = QMI_ERR_NONE_V01;
               }
               else
               {
                  MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() STATE_REQ get success\n");
                  resp_send.curr_state_valid = 1;
                  resp_send.curr_state = SERVREG_NOTIF_SERVICE_STATE_UNINIT_V01;
                  resp_send.resp.result = QMI_RESULT_SUCCESS_V01;
                  resp_send.resp.error  = QMI_ERR_NONE_V01;
               }

               qmi_csi_send_resp(req_handle, msg_id, &resp_send, sizeof(qmi_servreg_notif_query_state_resp_msg_v01));
            }
            break;

            case SERVREG_NOTIF_STATE_UPDATED_IND_ACK_REQ:
            {
               qmi_servreg_notif_set_ack_req_msg_v01 *request = (qmi_servreg_notif_set_ack_req_msg_v01 *)(req_c_struct);
               qmi_servreg_notif_set_ack_resp_msg_v01 resp_send;

               /* First check if the monitor node exists */
               sr_mon_handle = servreg_get_sr_mon_handle(request->service_name);
               if(SERVREG_NULL != sr_mon_handle)
               {
                  /* Check if the remote qmi client has registered as a listener */
                  sr_qmi_notif_server_handle = servreg_get_qmi_notif_server_entry(sr_mon_handle, client->client_handle);

                  if(SERVREG_NULL != sr_qmi_notif_server_handle)
                  {
                     /* Check if the ACK is being sent for the correct state change transaction id */
                     if(request->transaction_id == servreg_get_transaction_id(sr_mon_handle))
                     {
                        ret = servreg_set_ack(sr_mon_handle, request->transaction_id);
                        
                        if(SERVREG_SUCCESS == ret)
                        {
                           MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() ACK_REQ set success\n");
                           resp_send.resp.result = QMI_RESULT_SUCCESS_V01;
                           resp_send.resp.error  = QMI_ERR_NONE_V01;
                        }
                        else
                        {
                           MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() ACK_REQ set failed \n");
                           resp_send.resp.result = QMI_RESULT_FAILURE_V01;;
                           resp_send.resp.error  = QMI_ERR_INVALID_HANDLE_V01;
                        }
                     }
                     else
                     {
                        MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() ACK_REQ transaction_id error \n");
                        resp_send.resp.result = QMI_RESULT_FAILURE_V01;;
                        resp_send.resp.error  = QMI_ERR_INVALID_HANDLE_V01;
                     }
                  }
                  else
                  {
                     MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() ACK_REQ remote listener not registered \n");
                     resp_send.resp.result = QMI_RESULT_FAILURE_V01;;
                     resp_send.resp.error  = QMI_ERR_INVALID_HANDLE_V01;
                  }
               }
               else
               {
                  MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() ACK_REQ monitor handle not created \n");
                  resp_send.resp.result = QMI_RESULT_FAILURE_V01;;
                  resp_send.resp.error  = QMI_ERR_INVALID_HANDLE_V01;
               }

               qmi_csi_send_resp(req_handle, msg_id, &resp_send, sizeof(qmi_servreg_notif_set_ack_resp_msg_v01));
            }
            break;

            default:
            {
               qmi_response_type_v01 resp_send;

               MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() Unhandled QMI request id: 0x%x", msg_id);

               resp_send.result = QMI_RESULT_FAILURE_V01;
               resp_send.error  = QMI_ERR_INVALID_MESSAGE_ID_V01;
               qmi_csi_send_resp(req_handle, msg_id, &resp_send, sizeof(qmi_response_type_v01));
            }
            break;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() connection_handle is invalid");
         retval = QMI_CSI_CB_INTERNAL_ERR;
      }
  }
  else
  {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_process_req() req_handle is NULL");
      retval = QMI_CSI_CB_INTERNAL_ERR;
  }

   return retval;
}

/** =====================================================================
 * Function:
 *     servreg_notif_server_disconnect
 *
 * Description:
 *       This function processes incoming requests for the servreg_notif 
 *       QMI services to disconnect a client.
 *
 * Parameters:
 *      connection_handle: Client connection to disconnect
 *      service_cookie   : Ignored
 *
 * Returns:
 *     none
 * =====================================================================  */
void servreg_notif_server_disconnect(void *connection_handle, void *service_cookie)
{
   servreg_notif_client_type_p client_curr = SERVREG_NULL, client_prev = SERVREG_NULL;
   servreg_qmi_notif_server_node_p sr_qmi_notif_server_node = SERVREG_NULL;
   servreg_qmi_notif_server_entry_p sr_qmi_notif_server_entry_curr = SERVREG_NULL, sr_qmi_notif_server_entry_prev = SERVREG_NULL;

   client_curr = qmi_notif_client_head;

   if(SERVREG_NULL != connection_handle)
   {
      while(SERVREG_NULL != client_curr)
      {
         if(client_curr == (servreg_notif_client_type_p)connection_handle && (SERVREG_FALSE != client_curr->connected))
         {
            if(SERVREG_NULL == client_prev)
            {
               /* Delete the first node */
               qmi_notif_client_head = client_curr->next;
            }
            else
            {
               client_prev->next = client_curr->next;
            }

            client_curr->next = SERVREG_NULL;


            /* Also delete the reference to the qmi client handle in the qmi server table */
            sr_qmi_notif_server_node = servreg_qmi_notif_server_list_head;

            servreg_mutex_lock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

            while(SERVREG_NULL != sr_qmi_notif_server_node)
            {
               sr_qmi_notif_server_entry_curr = sr_qmi_notif_server_node->qmi_client_list_head;

               /* Walk through the qmi_client list and find the matching qmi_client handle */
               while(SERVREG_NULL != sr_qmi_notif_server_entry_curr)
               {
                  if(client_curr->client_handle == sr_qmi_notif_server_entry_curr->client_handle)
                  {
                     if(SERVREG_NULL == sr_qmi_notif_server_entry_prev)
                     {
                        /* Delete the first node */
                        sr_qmi_notif_server_node->qmi_client_list_head = sr_qmi_notif_server_entry_curr->next;
                     }
                     else
                     {
                        sr_qmi_notif_server_entry_prev->next = sr_qmi_notif_server_entry_curr->next;
                     }

                     sr_qmi_notif_server_entry_curr->next = SERVREG_NULL;

                     /* Reclaim back the qmi entry node */
                     servreg_qmi_notif_server_entry_pool_free(sr_qmi_notif_server_entry_curr);
                  }

                  sr_qmi_notif_server_entry_prev = sr_qmi_notif_server_entry_curr;
                  sr_qmi_notif_server_entry_curr = sr_qmi_notif_server_entry_curr->next;
               }
               sr_qmi_notif_server_node = sr_qmi_notif_server_node->next;
            }

            servreg_mutex_unlock_dal(&(servreg_qmi_notif_server_node_internal.mutex_create));

            /* Delete the client node */
            servreg_free(client_curr);
            MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_notif_server_disconnect() client released connection");

            break; /* exit while() loop */
         }

         client_prev = client_curr;
         client_curr = client_curr->next;
      }
      //MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_disconnect() Disconnect invalid client");
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_disconnect() connection handle is NULL");
   }
   return;
}

/** =====================================================================
 * Function:
 *     servreg_notif_server_connect
 *
 * Description:
 *       This function processes incoming requests for the servreg_notif 
 *       QMI services to connect a client.
 *
 * Parameters:
 *      client_handle    : Handle used by the infrastructure
 *                         to identify different services.
 *      service_cookie   : Ignored
 *      connection_handle: Client connection to created
 *                         internally.
 *
 * Returns:
 *     qmi_csi_cb_error
 * =====================================================================  */
qmi_csi_cb_error servreg_notif_server_connect(qmi_client_handle client_handle, void *service_cookie, void **connection_handle)
{
   qmi_csi_cb_error retval = QMI_CSI_CB_NO_MEM;
   servreg_notif_client_type_p client = SERVREG_NULL; //(servreg_notif_client_type*)(&qmi_notif_client_head);

   client = (servreg_notif_client_type_p)servreg_malloc(sizeof(servreg_notif_client_type_t));

   if(SERVREG_NULL != client)
   {
      /* Found empty slot */
      client->client_handle = client_handle;
      client->connected = SERVREG_TRUE;
      *connection_handle = client;
      client->next = SERVREG_NULL;

      /* Insert to the head of the list */
      client->next = qmi_notif_client_head;
      /* Update head */
      qmi_notif_client_head = client;

      retval = QMI_CSI_CB_NO_ERR;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_notif_server_connect() malloc failed and connection failed with server");
   }

   return retval;
}

/** =====================================================================
 * Function:
 *     servreg_qmi_server_init
 *
 * Description:
 *     This function initializes the QMI servreg_notif services handler.
 *
 * Parameters:
 *    os_params: OS parameters.
 *
 * Returns:
 *     qmi_csi_service_handle
 * =====================================================================  */
qmi_csi_service_handle servreg_qmi_server_init(qmi_csi_os_params *os_params)
{
   qmi_idl_service_object_type servreg_notif_server_obj = servreg_notif_get_service_object_v01();
   qmi_csi_options options;
   qmi_csi_error qmi_csi_return;
   QMI_CSI_OPTIONS_INIT(options);

   unsigned int ser_instance_id = servreg_get_local_instance_id();

   QMI_CSI_OPTIONS_SET_INSTANCE_ID(options,ser_instance_id);

   qmi_csi_return = qmi_csi_register_with_options(servreg_notif_server_obj,
                                                  servreg_notif_server_connect,
                                                  servreg_notif_server_disconnect,
                                                  servreg_notif_server_process_req,
                                                  NULL,
                                                  os_params,
                                                  &options,
                                                  &servreg_notif_qmi_server_handler);

   if(QMI_CSI_NO_ERR != qmi_csi_return)
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QMI_NOTIF: in servreg_qmi_server_init() QMI notif server init failed");
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QMI_NOTIF: in servreg_qmi_server_init() QMI notif server init success");
   }

   return servreg_notif_qmi_server_handler;
}

/** =====================================================================
 * Thread: QURT
 *     servreg_qmi_server_ind_task
 *
 * Description:
 *     Service Registry QMI notifier server task
 *
 * Parameters:
 *    param: Task init parameter
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_qmi_server_ind_task(void *argv __attribute__((unused)))
{
   uint32_t q_mask;

   qurt_anysignal_init(&servreg_notif_signal);

   /* Block for start signal */
   rcinit_handshake_startup();    

   /* Task forever loop */
   for (;;)
   {
      q_mask = qurt_anysignal_wait(&servreg_notif_signal, SERVREG_NOTIF_STATE_CHANGE_MASK);

      if (q_mask & SERVREG_NOTIF_STATE_CHANGE_MASK)
      {
         qurt_anysignal_clear(&servreg_notif_signal, SERVREG_NOTIF_STATE_CHANGE_MASK);
         servreg_send_qmi_ind_msg_state_change();
      }
   }
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_server_init
 *
 * Description:
 *     Initialization function for the service registry QMI notifier module
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_qmi_notif_server_init(void)
{
   qurt_thread_attr_t qurt_attr;
   static unsigned long stack[SERVREG_QMI_NOTIF_SERVER_TASK_STACK/sizeof(unsigned long)];

   servreg_qmi_notif_server_node_internal_init();

   servreg_qmi_notif_task_init();

   /* Qurt task to handle QMI indication messages */
   qurt_thread_attr_init(&qurt_attr);
   qurt_thread_attr_set_name(&qurt_attr, SERVREG_QMI_NOTIF_IND_SERVER_TASK_NAME);
   qurt_thread_attr_set_stack_addr(&qurt_attr, stack);
   qurt_thread_attr_set_stack_size(&qurt_attr, sizeof(stack));
   qurt_thread_attr_set_priority(&qurt_attr, qurt_thread_get_priority(qurt_thread_get_id()) - 1);
   qurt_thread_attr_set_affinity(&qurt_attr, QURT_THREAD_ATTR_AFFINITY_DEFAULT);

   qurt_thread_create(&servreg_qmi_notif_ind_server_internal.tid, &qurt_attr, servreg_qmi_server_ind_task, NULL);

   return;
}
