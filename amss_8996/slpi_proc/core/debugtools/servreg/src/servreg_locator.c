/*
#============================================================================
#  Name:                                                                     
#    servreg_locator.c 
#
#  Description:                                                              
#    Service Registry Locator feature. Clients query APSS to get the list of 
#    domains the service is running on.
#                                                                            
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
#============================================================================
*/
#include "stdarg.h"
#include "stdlib.h"
#include <stringl/stringl.h>
#include "comdef.h"             /* Definitions for byte, word, etc.     */
#include "msg.h"
#include "qmi_client.h"
#include "qmi_cci_target_ext.h"
#include "err.h"
#include "timer.h"
#include "rcecb.h"

#include "service_registry_locator_v01.h"
#include "servreg_internal.h"
#include "servreg_locator.h"
#include "servreg_localdb.h"
#include "servreg_qdi.h"
#include "servreg_notifier.h"

#define SERVREG_QMI_CLIENT_WAIT_SIG          (0x1 << 5)
#define SERVREG_QMI_CLIENT_TIMER_SIG         (0x1 << 6)
#define SERVREG_QMI_TIMEOUT_MS               100
#define SERVREG_LOC_REQ_TIMEOUT_MS           1000
#define SERVREG_LOC_RETRY_TIME_MS            200
#define SERVREG_LOC_MAX_RETRY_COUNT          3
#define SERVREG_LOC_INIT_NAME                "servreg_loc_init"

extern void servreg_monitor_init(void);

/* Flag indicating if the QMI client initialization is done */
static SERVREG_SERVICE_STATE servreg_loc_client_init_status = SERVREG_SERVICE_STATE_UNINIT;

/* Indicates the maximum number of times we retry to establish a QMI connection to the HLOS service locator */
static uint32_t servreg_loc_retry_count = SERVREG_LOC_MAX_RETRY_COUNT;

/* QMI message related and keeping it global because it should be valid for the lifetime of the user_handle */
static qmi_cci_os_signal_type  os_params;
static qmi_idl_service_object_type servreg_loc_service_object;
static qmi_client_type servreg_locator_notifier, servreg_locator_client_handle;
static qmi_service_info servreg_loc_apss_server_info;
//qurt_anysignal_t servreg_loc_signal;

/* Servloc availability timer specific */
static timer_type servreg_loc_retry_timer = { 0 };
static timer_group_type servreg_loc_retry_group = { 0 };

/* Domain list entry structure */
struct servreg_dl_entry_s
{
   SERVREG_NAME domain_name;
   uint32_t instance_id;
   SERVREG_BOOL service_data_valid;
   uint32_t service_data;
   struct servreg_dl_entry_s * next;
};
typedef struct servreg_dl_entry_s servreg_dl_entry_t, * servreg_dl_entry_p;

/* Domain list table structure */
struct servreg_dl_table_s
{
   uint32_t domain_list_signature;
   SERVREG_NAME service_name;
   SERVREG_RESULT domain_list_status;
   uint32_t total_domains;
   struct servreg_dl_entry_s * domain_list_head;
   struct servreg_dl_table_s * next;
};
typedef struct servreg_dl_table_s servreg_dl_table_t, * servreg_dl_table_p;

/* Type casts as accessor functions */
#define sr_dl_table2sr_dl_handle(x)    ((SERVREG_DL_HANDLE)x)
#define sr_dl_handle2sr_dl_table(x)    ((servreg_dl_table_p)x)

/* Pool Allocations */
/* Pool allocation for domain list entry */
struct servreg_dl_entry_pool_s
{
   struct servreg_dl_entry_s servreg_dl_entry_pool[SERVREG_LOC_ENTRY_POOL_SIZE];
   struct servreg_dl_entry_pool_s * next;
};
typedef struct servreg_dl_entry_pool_s servreg_dl_entry_pool_t, * servreg_dl_entry_pool_p;

/* Pool allocation for domain list table */
struct servreg_dl_table_pool_s
{
   struct servreg_dl_table_s servreg_dl_table_pool[SERVREG_LOC_STRUCT_POOL_SIZE];
   struct servreg_dl_table_pool_s* next;
};
typedef struct servreg_dl_table_pool_s servreg_dl_table_pool_t, * servreg_dl_table_pool_p;

/* Internal structure for domain list */
struct servreg_dl_internal_s
{
   servreg_dl_entry_pool_p servreg_dl_entry_pool_head_p;
   servreg_dl_table_pool_p servreg_dl_table_pool_head_p;
   servreg_dl_entry_p servreg_dl_entry_pool_free_p;
   servreg_dl_table_p servreg_dl_table_pool_free_p;
   servreg_mutex_t mutex;
   servreg_mutex_t mutex_create;
   SERVREG_BOOL dynamic_use;
   unsigned long init_flag;
};

/* Pool allocation internals */
struct servreg_dl_internal_s servreg_dl_internal;
servreg_dl_entry_pool_t servreg_dl_entry_pool_static;
servreg_dl_table_pool_t servreg_dl_table_pool_static;

typedef enum
{
   SERVREG_LOC_GET_DOMAIN_LIST_REQ           = QMI_SERVREG_LOC_GET_DOMAIN_LIST_REQ_V01,
   SERVREG_LOC_REGISTER_SERVICE_LIST_REQ     = QMI_SERVREG_LOC_REGISTER_SERVICE_LIST_REQ_V01,
   SERVREG_LOC_DATABASE_UPDATED_IND          = QMI_SERVREG_LOC_DATABASE_UPDATED_IND_V01,
}servreg_loc_msg_req_type;

/* Static Functions defined in this file */
static servreg_dl_entry_p servreg_dl_entry_pool_init(void);
static servreg_dl_entry_p servreg_dl_entry_pool_alloc(void);
static servreg_dl_entry_p servreg_dl_entry_pool_free(servreg_dl_entry_p srdl_entry);
static servreg_dl_table_p servreg_dl_table_pool_init(void);
static servreg_dl_table_p servreg_dl_table_pool_alloc(void);
static servreg_dl_table_p servreg_dl_table_pool_free(servreg_dl_table_p sr_dl_table);
static void servreg_dl_internal_init(void);

static void servreg_loc_retry_timout_cb(timer_cb_data_type unused);
static void servreg_loc_client_ind_cb(qmi_client_type client_handle, uint32_t msg_id, void *ind_buf, uint32_t ind_buf_len, void *ind_cb_data);
static SERVREG_RESULT servreg_locator_client_init(void);
static void servreg_set_servreg_loc_availability(void);

/** =====================================================================
 * Function:
 *     servreg_dl_entry_pool_init
 *
 * Description:
 *     Initializes the memory pool for domain list entry structure
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_dl_entry_p : Returns the first free dl_entry space from the pool
 * =====================================================================  */
static servreg_dl_entry_p servreg_dl_entry_pool_init(void)
{
   servreg_dl_entry_pool_p next_pool = SERVREG_NULL;

   if (SERVREG_NULL == servreg_dl_internal.servreg_dl_entry_pool_head_p)
   {
      next_pool = &servreg_dl_entry_pool_static;
   }
   else if (SERVREG_TRUE == servreg_dl_internal.dynamic_use)
   {
      next_pool = (servreg_dl_entry_pool_p)servreg_malloc(sizeof(servreg_dl_entry_pool_t));
   }

   if (SERVREG_NULL != next_pool)
   {
      int i;

      for (i = 0; i < SERVREG_LOC_ENTRY_POOL_SIZE; i++)
      {
         if (i != (SERVREG_LOC_ENTRY_POOL_SIZE - 1))
         {
            next_pool->servreg_dl_entry_pool[i].next = &(next_pool->servreg_dl_entry_pool[i + 1]);
         }
         else
         {
            next_pool->servreg_dl_entry_pool[i].next = servreg_dl_internal.servreg_dl_entry_pool_free_p;
         }

         next_pool->servreg_dl_entry_pool[i].domain_name = SERVREG_NULL;
         next_pool->servreg_dl_entry_pool[i].instance_id = 0;
         next_pool->servreg_dl_entry_pool[i].service_data_valid = SERVREG_FALSE;
         next_pool->servreg_dl_entry_pool[i].service_data = 0;
      }

      servreg_dl_internal.servreg_dl_entry_pool_free_p = &(next_pool->servreg_dl_entry_pool[0]);
      next_pool->next = servreg_dl_internal.servreg_dl_entry_pool_head_p;
      servreg_dl_internal.servreg_dl_entry_pool_head_p = next_pool;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_dl_entry_pool_init() malloc failed");
      return SERVREG_NULL;
   }

   return servreg_dl_internal.servreg_dl_entry_pool_free_p;
}

/** =====================================================================
 * Function:
 *     servreg_dl_entry_pool_alloc
 *
 * Description:
 *     Gives the first available free and allocated space from the memory
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_dl_entry_p : the first available free and allocated space from the memory
 * =====================================================================  */
static servreg_dl_entry_p servreg_dl_entry_pool_alloc(void)
{
   servreg_dl_entry_p ret;
   servreg_dl_entry_p sr_dl_entry;

   servreg_mutex_lock_dal(&(servreg_dl_internal.mutex));

   if (SERVREG_NULL == servreg_dl_internal.servreg_dl_entry_pool_free_p)
   {
      sr_dl_entry = servreg_dl_entry_pool_init();
   }
   else
   {
      sr_dl_entry = servreg_dl_internal.servreg_dl_entry_pool_free_p;
   }

   if (SERVREG_NULL != sr_dl_entry)
   {
      servreg_dl_internal.servreg_dl_entry_pool_free_p = sr_dl_entry->next;
      sr_dl_entry->next = SERVREG_NULL;
      ret = sr_dl_entry;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_dl_entry_pool_alloc() alloc failed");
      ret = SERVREG_NULL;
   }

   servreg_mutex_unlock_dal(&(servreg_dl_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_dl_entry_pool_free
 *
 * Description:
 *     Reclaims back the sr_dl_entry to the memory pool
 *
 * Parameters:
 *     sr_dl_entry : space to be reclaimed back
 *
 * Returns:
 *    servreg_dl_entry_p : The next available free space in the memory pool
 * =====================================================================  */
static servreg_dl_entry_p servreg_dl_entry_pool_free(servreg_dl_entry_p sr_dl_entry)
{
   servreg_dl_entry_p ret = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_dl_internal.mutex));

   if(SERVREG_NULL != sr_dl_entry)
   {
      /* Free the space that was dynamically allocated for the domain name */
      servreg_free(sr_dl_entry->domain_name);
      sr_dl_entry->domain_name = SERVREG_NULL;
      sr_dl_entry->instance_id = 0;
      sr_dl_entry->service_data_valid = SERVREG_FALSE;
      sr_dl_entry->service_data = 0;

      sr_dl_entry->next = servreg_dl_internal.servreg_dl_entry_pool_free_p;
      servreg_dl_internal.servreg_dl_entry_pool_free_p = sr_dl_entry;
      ret = sr_dl_entry;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_dl_entry_pool_free() sr_dl_entry is NULL and cannot be freed");
   }

   servreg_mutex_unlock_dal(&(servreg_dl_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_dl_table_pool_init
 *
 * Description:
 *     Initializes the memory pool for domain list table structure
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_dl_table_p : Returns the first free dl_table space from the pool
 * =====================================================================  */
static servreg_dl_table_p servreg_dl_table_pool_init(void)
{
   servreg_dl_table_pool_p next_pool = SERVREG_NULL;

   if (SERVREG_NULL == servreg_dl_internal.servreg_dl_table_pool_head_p)
   {
      next_pool = &servreg_dl_table_pool_static;
   }
   else if (SERVREG_TRUE == servreg_dl_internal.dynamic_use)
   {
      next_pool = (servreg_dl_table_pool_p)servreg_malloc(sizeof(servreg_dl_table_pool_t));
   }

   if (SERVREG_NULL != next_pool)
   {
      int i;

      for (i = 0; i < SERVREG_LOC_STRUCT_POOL_SIZE; i++)
      {
         if (i != (SERVREG_LOC_STRUCT_POOL_SIZE - 1))
         {
            next_pool->servreg_dl_table_pool[i].next = &(next_pool->servreg_dl_table_pool[i + 1]);
         }
         else
         {
            next_pool->servreg_dl_table_pool[i].next = servreg_dl_internal.servreg_dl_table_pool_free_p;
         }

         next_pool->servreg_dl_table_pool[i].domain_list_signature = SERVREG_DL_SIGNATURE;
         next_pool->servreg_dl_table_pool[i].service_name = SERVREG_NULL;
         next_pool->servreg_dl_table_pool[i].domain_list_status = SERVREG_FAILURE;
         next_pool->servreg_dl_table_pool[i].total_domains = 0;
         next_pool->servreg_dl_table_pool[i].domain_list_head = SERVREG_NULL;
      }

      servreg_dl_internal.servreg_dl_table_pool_free_p = &(next_pool->servreg_dl_table_pool[0]);
      next_pool->next = servreg_dl_internal.servreg_dl_table_pool_head_p;
      servreg_dl_internal.servreg_dl_table_pool_head_p = next_pool;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_dl_table_pool_init() alloc failed");
      return SERVREG_NULL;
   }

   return servreg_dl_internal.servreg_dl_table_pool_free_p;
}

/** =====================================================================
 * Function:
 *     servreg_dl_table_pool_alloc
 *
 * Description:
 *     Gives the first available free and allocated space from the memory
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_dl_table_p : the first available free and allocated space from the memory
 * =====================================================================  */
static servreg_dl_table_p servreg_dl_table_pool_alloc(void)
{
   servreg_dl_table_p ret;
   servreg_dl_table_p sr_dl_table;

   servreg_mutex_lock_dal(&(servreg_dl_internal.mutex));

   if (SERVREG_NULL == servreg_dl_internal.servreg_dl_table_pool_free_p)
   {
      /* If free static node not available then get a dynamic node */
      sr_dl_table = servreg_dl_table_pool_init();
   }
   else
   {
      sr_dl_table = servreg_dl_internal.servreg_dl_table_pool_free_p;
   }

   if (SERVREG_NULL != sr_dl_table)
   {
      servreg_dl_internal.servreg_dl_table_pool_free_p = sr_dl_table->next;
      sr_dl_table->next = SERVREG_NULL;
      ret = sr_dl_table;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_dl_table_pool_alloc() alloc failed");
      ret = SERVREG_NULL;
   }

   servreg_mutex_unlock_dal(&(servreg_dl_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_dl_table_pool_free
 *
 * Description:
 *     Reclaims back the sr_dl_entry to the memory pool
 *
 * Parameters:
 *     sr_dl_table : space to be reclaimed back
 *
 * Returns:
 *    servreg_dl_table_p : The next available free space in the memory pool
 * =====================================================================  */
static servreg_dl_table_p servreg_dl_table_pool_free(servreg_dl_table_p sr_dl_table)
{
   servreg_dl_table_p ret = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_dl_internal.mutex));

   if(SERVREG_NULL != sr_dl_table)
   {
      /* Free the space that was dynamically allocated for the domain name */
      servreg_free(sr_dl_table->service_name);
      sr_dl_table->service_name = SERVREG_NULL;
      sr_dl_table->domain_list_status = SERVREG_FAILURE;
      sr_dl_table->total_domains = 0;
      sr_dl_table->domain_list_head = SERVREG_NULL;

      sr_dl_table->next = servreg_dl_internal.servreg_dl_table_pool_free_p; 
      servreg_dl_internal.servreg_dl_table_pool_free_p = sr_dl_table;
      ret = sr_dl_table;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_dl_table_pool_free() sr_dl_table is NULL and cannot be freed");
   }
   servreg_mutex_unlock_dal(&(servreg_dl_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_dl_internal_init
 *
 * Description:
 *     Initialization of the internal memory pools and other internals
 *     for domain list table and entries
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_dl_internal_init(void)
{
   DALSYS_InitMod(NULL);

   servreg_mutex_init_dal(&(servreg_dl_internal.mutex));
   servreg_mutex_init_dal(&(servreg_dl_internal.mutex_create));

   servreg_mutex_lock_dal(&(servreg_dl_internal.mutex));

   servreg_dl_internal.dynamic_use = TRUE;
   servreg_dl_table_pool_init();
   servreg_dl_entry_pool_init();

   servreg_mutex_unlock_dal(&(servreg_dl_internal.mutex));
}

/** =====================================================================
* Function:
*     servreg_loc_retry_timout_cb
*
* Description:
*     Timer driven function called that checks if Service Locator Server
*     on APPS is available to complete the subsystem Locator initialization
*
* Parameters:
*     unused : parameter not used
*
* Returns:
*     none
* =====================================================================  */
static void servreg_loc_retry_timout_cb(timer_cb_data_type unused)
{
   SERVREG_RESULT ret = SERVREG_FAILURE;
   time_timetick_type remaining_time = 0;

   if(servreg_loc_retry_count != 0)
   {
      servreg_loc_retry_count = servreg_loc_retry_count - 1;

      MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: servreg_locator_init() retry attempt = %d ", (SERVREG_LOC_MAX_RETRY_COUNT - servreg_loc_retry_count));

      ret = servreg_locator_client_init();

      if(SERVREG_SUCCESS == ret)
      {
         /* Clear the timer */
         timer_stop(&servreg_loc_retry_timer, T_NONE, &remaining_time);

         timer_undef(&servreg_loc_retry_timer);

         servreg_set_servreg_loc_availability();

      }
   }
   else
   {
      /* Clear the timer */
      timer_stop(&servreg_loc_retry_timer, T_NONE, &remaining_time);

      timer_undef(&servreg_loc_retry_timer);

      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: Could not connect to HLOS Service Locator after 3 re-try attempts or Locator not available.");

      //servreg_set_servreg_loc_availability();
   }
}

/** =====================================================================
 * Function:
 *     servreg_loc_client_ind_cb
 *
 * Description: 
 *     Called by the QCCI infrastructure when an INDICATION message
 *     is received by the server on APSS (TN in our case when there 
 *     is a database update)
 *
 * Parameters:
 *     client_handle : Handle used by the infrastructure to 
 *                     identify different clients.
 *     msg_id        : Message ID
 *     ind_buf       : Pointer to the raw/un-decoded indication
 *     ind_buf_len   : Length of the indication
 *     ind_cb_data   : User-data
 *
 * Returns:
 *             None
 * =====================================================================  */
static void servreg_loc_client_ind_cb(qmi_client_type client_handle, uint32_t msg_id, void *ind_buf, uint32_t ind_buf_len, void *ind_cb_data)
{
   void *ind_msg;
   timetick_type response_time = 0;
   qmi_client_error_type qmi_err;
   uint32_t decoded_size;

   MSG_3(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: in servreg_loc_client_ind_cb() request: client_handle = %d, msg_id =%d, response_time = 0x%x", client_handle, msg_id, response_time);

   qmi_err = qmi_idl_get_message_c_struct_len(servreg_loc_service_object, QMI_IDL_INDICATION, msg_id, &decoded_size);

   if(QMI_NO_ERR != qmi_err)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: Received error from QMI framework call %d", qmi_err);
      return;
   }

   ind_msg = servreg_malloc(decoded_size);
   if(!ind_msg) 
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_loc_client_ind_cb() indication buff allocation failed");
      return;
   }

   qmi_err = qmi_client_message_decode(client_handle, QMI_IDL_INDICATION, msg_id, ind_buf, ind_buf_len, ind_msg, decoded_size);
   if (qmi_err != QMI_NO_ERR)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_loc_client_ind_cb() Received error from QMI framework call %d" , qmi_err);
      servreg_free(ind_msg);
      return;
   }

   servreg_free(ind_msg);

   return;
}

/** =====================================================================
 * Function:
 *     servreg_locator_client_init
 *
 * Description:
 *     Initializes the service locator QMI client
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     SERVREG_SUCCESS, SERVREG_FAILURE
 * =====================================================================  */
static SERVREG_RESULT servreg_locator_client_init(void)
{
   qmi_client_error_type qmi_client_return;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   //qurt_anysignal_init(&servreg_loc_signal);
   //os_params.signal = servreg_loc_signal;
   os_params.timed_out = 0;
   os_params.sig = SERVREG_QMI_CLIENT_WAIT_SIG;
   os_params.timer_sig = SERVREG_QMI_CLIENT_TIMER_SIG;
   os_params.timer_inited = SERVREG_TRUE;

   if(SERVREG_SERVICE_STATE_UP == servreg_loc_client_init_status)
   {
      ret = SERVREG_SUCCESS;
   }
   else
   {
      servreg_loc_service_object = servreg_loc_get_service_object_v01();

      if (servreg_loc_service_object)
      {
         /* Initialize the qmi client notifier */
         qmi_client_return = qmi_client_notifier_init(servreg_loc_service_object, &os_params, &servreg_locator_notifier);

         if(QMI_NO_ERR == qmi_client_return)
         {
            /* wait for server to come up */
            /*QMI_CCI_OS_SIGNAL_WAIT(&os_params, SERVREG_QMI_TIMEOUT_MS);
            if(os_params.timed_out)
            {
                MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_locator_client_init() Service locator server on APSS discovery timeout");
                qmi_client_release(servreg_locator_notifier);
                ret = SERVREG_FAILURE;
            }
            else
            {*/
               QMI_CCI_OS_SIGNAL_CLEAR(&os_params);

               qmi_client_return = qmi_client_get_service_instance(servreg_loc_service_object, QMI_SERVREG_LOC_SERVICE_INSTANCE_APSS_V01, &servreg_loc_apss_server_info);
               if(QMI_NO_ERR == qmi_client_return)
               {
                  qmi_client_return = qmi_client_init(&servreg_loc_apss_server_info, servreg_loc_service_object, (void *)servreg_loc_client_ind_cb, NULL, &os_params, &servreg_locator_client_handle);
                  if(QMI_NO_ERR == qmi_client_return)
                  {
                     ret = SERVREG_SUCCESS;
                     MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: in servreg_locator_client_init() qmi_client_init SUCCESS \n");

                  }
                  else
                  {
                     MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_locator_client_init() qmi_client_init returned failure qmi_client_return = %d\n", qmi_client_return);
                     qmi_client_release(servreg_locator_notifier);
                     ret = SERVREG_FAILURE;
                  }
               }
               else
               {
                  MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_locator_client_init() qmi_client_get_service_instance returned failure qmi_client_return = %d", qmi_client_return);
                  qmi_client_release(servreg_locator_notifier);
                  ret = SERVREG_FAILURE;
               }
            //}
         }
         else
         {
            MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC:in servreg_locator_client_init() qmi_client_notifier_init qmi_client_return = %d ",qmi_client_return);
            ret = SERVREG_FAILURE;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_locator_client_init() servreg_loc_service_object in NULL \n");
         ret = SERVREG_FAILURE;
      }
   }
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_set_servreg_loc_availability
 *
 * Description:
 *     Set the state of Service Locator feature as UP
 *
 * Parameters:
 *     None

 * Returns:
 *     None
 * =====================================================================  */
static void servreg_set_servreg_loc_availability(void)
{
   RCECB_HANDLE rcecb_handle = RCECB_NULL;
   RCEVT_HANDLE rcevt_handle = RCEVT_NULL;

   rcecb_handle = rcecb_create_name(SERVREG_LOC_INIT_NAME);
   rcevt_handle = rcevt_create_name(SERVREG_LOC_INIT_NAME);

   if(RCECB_NULL != rcecb_handle)
   {
      if(0 < rcecb_getregistrants_handle(rcecb_handle))
      {
         /* Signal the listeners who have registered for servreg_loc availability via callbacks */
         if(RCECB_NULL == rcecb_signal_handle(rcecb_handle))
         {
            ERR_FATAL( "SERVREG_LOC: in servreg_locator_init() rcecb_signal_name() failed", 0, 0, 0);
         }
      }
   }
   else
   {
      ERR_FATAL( "SERVREG_LOC: in servreg_locator_init() rcecb_create_name() failed", 0, 0, 0);
   }

   if(RCEVT_NULL != rcevt_handle)
   {
      if(0 < rcevt_getregistrants_handle(rcecb_handle))
      {
         /* Signal the listeners who have registered for servreg_loc availability */
         if(RCEVT_NULL == rcevt_signal_handle(rcecb_handle))
         {
            ERR_FATAL( "SERVREG_LOC: in servreg_locator_init() rcevt_signal_name() failed", 0, 0, 0);
         }
      }
   }
   else
   {
      ERR_FATAL( "SERVREG_LOC: in servreg_locator_init() rcevt_create_name() failed", 0, 0, 0);
   }

   servreg_loc_client_init_status = SERVREG_SERVICE_STATE_UP;
}

/** =====================================================================
 * Function:
 *     servreg_alloc_domainlist_handle
 *
 * Description:
 *     Allocates domain list handle. 
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     SERVREG_DL_HANDLE : domain list handle
 *                         Check for return value and if it is NOT NULL
 * =====================================================================  */
SERVREG_DL_HANDLE servreg_alloc_domainlist_handle(void)
{
   SERVREG_DL_HANDLE dlhandle = SERVREG_NULL;
   servreg_dl_table_p sr_dl_table = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_dl_internal.mutex_create));

   /* Allocate new dl table from the pool */
   sr_dl_table = servreg_dl_table_pool_alloc();
   if (SERVREG_NULL != sr_dl_table)
   {
      dlhandle = sr_dl_table2sr_dl_handle(sr_dl_table);
      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: in servreg_alloc_domainlist_handle() dlhandle alloc success");
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_alloc_domainlist_handle() sr_dl_table malloc failed");
   }

   servreg_mutex_unlock_dal(&(servreg_dl_internal.mutex_create));
   return dlhandle;
}

/** =====================================================================
 * Function:
 *     servreg_get_domainlist
 *
 * Description:
 *     This queries the APSS(HLOS or TN) via a QMI message to get the list of
 *     all domains where the service runs
 *
 * Parameters:
 *    service_name : Service name that the client is interested in i.e "provider/service" info
 *    dlhandle     : Allocated domain list handle.
 *
 * Returns:
 *    Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_get_domainlist(SERVREG_NAME service_name, SERVREG_DL_HANDLE dlhandle)
{
   uint32_t i = 0;
   servreg_dl_table_p sr_dl_table = SERVREG_NULL;
   servreg_dl_entry_p srdl_entry = SERVREG_NULL;
   qmi_client_error_type qmi_client_return;
   qmi_servreg_loc_get_domain_list_req_msg_v01 servreg_loc_dl_req;
   qmi_servreg_loc_get_domain_list_resp_msg_v01 *servreg_loc_dl_resp;
   SERVREG_RESULT ret = SERVREG_FAILURE;
   uint32_t domians_recvd = 0, len1 = 0, len2 = 0;

   servreg_mutex_lock_dal(&(servreg_dl_internal.mutex_create));

   sr_dl_table = sr_dl_handle2sr_dl_table(dlhandle);

   if (SERVREG_NULL != sr_dl_table)
   {
      if(SERVREG_DL_SIGNATURE == sr_dl_table->domain_list_signature)
      {
         len1 = servreg_nmelen(service_name) + 1;
         sr_dl_table->service_name = (SERVREG_NAME)servreg_malloc(sizeof(char) * len1);

         if (SERVREG_NULL != sr_dl_table->service_name)
         {
            strlcpy(sr_dl_table->service_name, service_name, len1);
         }
         else
         {
            servreg_dl_table_pool_free(sr_dl_table);
            ERR_FATAL( "SERVREG_LOC: buf_p malloc failed", 0, 0, 0);
         }

         /* Do heap allocation of the response message because the response packet is huge */
         servreg_loc_dl_resp = (qmi_servreg_loc_get_domain_list_resp_msg_v01*)servreg_malloc(sizeof(qmi_servreg_loc_get_domain_list_resp_msg_v01));

         /* Update the service_name in sr_dl_table */
         strlcpy(servreg_loc_dl_req.service_name, service_name, len1);

         do
         {
            /* QMI message to send request for the domain list */
            qmi_client_return = qmi_client_send_msg_sync(servreg_locator_client_handle,  /* user_handle */
                                           SERVREG_LOC_GET_DOMAIN_LIST_REQ,              /* msg_id */
                                           (void*)&servreg_loc_dl_req,                        /* *req_c_table */
                                           sizeof(servreg_loc_dl_req),                        /* req_c_table_len*/
                                           (void*)servreg_loc_dl_resp,                       /* *resp_c_table*/
                                           sizeof(qmi_servreg_loc_get_domain_list_resp_msg_v01),                       /* resp_c_table_len*/
                                           SERVREG_LOC_REQ_TIMEOUT_MS                    /* timeout_msecs */
                                           );

            if(QMI_NO_ERR == qmi_client_return)
            {
               if(QMI_RESULT_SUCCESS_V01 == servreg_loc_dl_resp->resp.result)
               {
                  /* Update the total_domains in sr_dl_table */
                  sr_dl_table->total_domains = servreg_loc_dl_resp->total_domains;

                  /* Update the list in sr_dl_table with domain entry names */
                  for(i = 0; i < servreg_loc_dl_resp->domain_list_len; i++)
                  {
                     srdl_entry = servreg_dl_entry_pool_alloc();

                     if(SERVREG_NULL != srdl_entry)
                     {
                        len2 = servreg_nmelen((SERVREG_NAME)&servreg_loc_dl_resp->domain_list[i].name) + 1;
                        srdl_entry->domain_name = (SERVREG_NAME)servreg_malloc(sizeof(char) * len2);

                        if (SERVREG_NULL != srdl_entry->domain_name)
                        {
                           strlcpy(srdl_entry->domain_name, (char *)&servreg_loc_dl_resp->domain_list[i].name, len2);
                        }
                        else
                        {
                           servreg_dl_table_pool_free(sr_dl_table);
                           ERR_FATAL( "SERVREG_LOC: malloc failed", 0, 0, 0);
                        }

                        srdl_entry->instance_id = servreg_loc_dl_resp->domain_list[i].instance_id;

                        if(SERVREG_TRUE == servreg_loc_dl_resp->domain_list[i].service_data_valid)
                        {
                           srdl_entry->service_data_valid = servreg_loc_dl_resp->domain_list[i].service_data_valid;
                           srdl_entry->service_data = servreg_loc_dl_resp->domain_list[i].service_data;
                        }

                        /* Keep a record of the qmi_instance id for that domain */
                        servreg_create_qmi_table_entry(srdl_entry->domain_name, srdl_entry->instance_id);

                        /* Insert the domain_entry in the head of the list */
                        srdl_entry->next = sr_dl_table->domain_list_head;

                         /* Update the head */
                        sr_dl_table->domain_list_head = srdl_entry;
                     }
                     else
                     {
                        ERR_FATAL( "SERVREG_LOC: srdl_entry malloc failed", 0, 0, 0);
                     }
                  } /* for() */

                  ret = SERVREG_SUCCESS;
                  /* Update the offset field in case the entire domain list is not received */
                  servreg_loc_dl_req.domain_offset_valid = 1;
                  servreg_loc_dl_req.domain_offset = servreg_loc_dl_resp->domain_list_len;
                  domians_recvd = domians_recvd + servreg_loc_dl_resp->domain_list_len;

                  sr_dl_table->domain_list_status = ret;

                  if(domians_recvd != servreg_loc_dl_resp->total_domains)
                  {
                     MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: in servreg_get_domainlist() in progress. domain_recvd = %d, total_domains = %d", domians_recvd, servreg_loc_dl_resp->total_domains);
                  }
                  else
                  {
                     MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: in servreg_get_domainlist() QMI RESP success");
                  }
               }
               else
               {
                  MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_get_domainlist() ERROR qmi response = %d\n", servreg_loc_dl_resp->resp.error);
                  ret = SERVREG_FAILURE;
                  sr_dl_table->domain_list_status = ret;
                  break;
               }
            }
            else
            {
               MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_get_domainlist() ERROR qmi_client_return= %d\n", qmi_client_return);
               ret = SERVREG_FAILURE;
               sr_dl_table->domain_list_status = ret;
               break;
            }

         }while(domians_recvd != servreg_loc_dl_resp->total_domains);

         servreg_free(servreg_loc_dl_resp);
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_get_domainlist() dlhandle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_get_domainlist() sr_dl_table is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }

   servreg_mutex_unlock_dal(&(servreg_dl_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_get_num_entries
 *
 * Description:
 *      This gives the total number of domains the service runs on
 *
 * Parameters:
 *      dlhandle :  Allocated handle.
 *
 * Returns:
 *      Total number of domains the service runs on
 * =====================================================================  */
uint32_t servreg_get_num_entries(SERVREG_DL_HANDLE dlhandle)
{
   servreg_dl_table_p sr_dl_table = SERVREG_NULL;
   uint32_t ret = 0;

   servreg_mutex_lock_dal(&(servreg_dl_internal.mutex_create));

   sr_dl_table = sr_dl_handle2sr_dl_table(dlhandle);
   if (SERVREG_NULL != sr_dl_table)
   {
      if(SERVREG_DL_SIGNATURE == sr_dl_table->domain_list_signature)
      {
         ret = sr_dl_table->total_domains;
         MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: in servreg_get_num_entries() total_domains = %d", ret);
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_get_num_entries() dlhandle has invalid signature \n");
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_get_num_entries() sr_dl_table is NULL \n");
   }

   servreg_mutex_unlock_dal(&(servreg_dl_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_get_entry
 *
 * Description:
 *     Given an offset this function returns the domain name
 *
 * Parameters:
 *     dlhandle : Allocated handle.
 *     offset   : offset at which the client wants the domain name.
 *     domain   : Result stored here. The Domain name at the offset.
 *
 * Returns:
 *    Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_get_entry(SERVREG_DL_HANDLE dlhandle, uint32_t num, char** domain)
{
   servreg_dl_entry_p sr_entry_p = SERVREG_NULL;
   uint32_t count = 1;
   servreg_dl_table_p sr_dl_table = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   servreg_mutex_lock_dal(&(servreg_dl_internal.mutex_create));
   sr_dl_table = sr_dl_handle2sr_dl_table(dlhandle);

   if(SERVREG_NULL != sr_dl_table)
   {
      if(SERVREG_DL_SIGNATURE == sr_dl_table->domain_list_signature)
      {
         if(num <= sr_dl_table->total_domains && num > 0)
         {
            sr_entry_p = sr_dl_table->domain_list_head;

            while (SERVREG_NULL != sr_entry_p)
            {
               if (num == count)
               {
                  *domain = sr_entry_p->domain_name;
                  break;
               }
               count++;
               sr_entry_p = sr_entry_p->next;
            }

            ret = SERVREG_SUCCESS;
            MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: in servreg_get_entry() returned domain \n");
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_get_entry() given num is out of bounds \n");
            ret = SERVREG_INVALID_PARAM;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_get_entry() dlhandle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_get_entry() dlhandle is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }

   servreg_mutex_unlock_dal(&(servreg_dl_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_free_domainlist_handle
 *
 * Description:
 *     Deallocates the domain list handle
 *
 * Parameters:
 *     dlhandle : Allocated handle.
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_free_domainlist_handle(SERVREG_DL_HANDLE dlhandle)
{
   servreg_dl_entry_p srdl_entry = SERVREG_NULL;
   servreg_dl_table_p sr_dl_table = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   servreg_mutex_lock_dal(&(servreg_dl_internal.mutex_create));

   sr_dl_table = sr_dl_handle2sr_dl_table(dlhandle);
   if(SERVREG_NULL != sr_dl_table)
   {
      if(SERVREG_DL_SIGNATURE == sr_dl_table->domain_list_signature)
      {
         while(SERVREG_NULL != sr_dl_table->domain_list_head)
         {
            srdl_entry = sr_dl_table->domain_list_head;
            sr_dl_table->domain_list_head = sr_dl_table->domain_list_head->next;

            /* Reclaim the locator entry back to the free pool */
            servreg_dl_entry_pool_free(srdl_entry);
         }

         /* Reclaim the locator table back to the free pool */
         if(SERVREG_NULL != servreg_dl_table_pool_free(sr_dl_table))
         {
            ret = SERVREG_SUCCESS;
            MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: in servreg_free_domainlist_handle() given dlhandle freed");
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_free_domainlist_handle() dlhandle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_free_domainlist_handle() sr_dl_table is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }

   servreg_mutex_unlock_dal(&(servreg_dl_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_register_servloc_availability_signal
 *
 * Description:
 *     Clients register to get notified when Service Locator feature is available
 *
 * Parameters:
 *     servreg_signal_type : NHLOS specific notification signal type information
 *     servreg_signal      : NHLOS specific notification signal and mask information
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_register_servloc_availability_signal(SERVREG_SIGEX_TYPE servreg_signal_type, SERVREG_SIGEX servreg_signal)
{
   SERVREG_RESULT ret = SERVREG_FAILURE;
   RCEVT_HANDLE rcevt_handle = RCEVT_NULL;

   /* Event indicates startup completes */
   if(servreg_signal_type == SERVREG_SIGEX_TYPE_SIGQURT)
   {
      rcevt_handle = SERVREG_RCEVT_REGISTER_NAME_QURT(SERVREG_LOC_INIT_NAME, servreg_signal);
   }
   else
   {
      rcevt_handle = SERVREG_RCEVT_REGISTER_NAME_REX(SERVREG_LOC_INIT_NAME, servreg_signal);
   }

   if(RCEVT_NULL != rcevt_handle)
   {
      ret = SERVREG_SUCCESS;
      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: in servreg_register_servloc_availability_signal() client registered successfully");
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_register_servloc_availability_signal() registration failed \n");
   }

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_register_servloc_availability_cb
 *
 * Description:
 *     Clients register to get notified when Service Locator feature is available
 *
 * Parameters:
 *     callback : Callback function registered
 *     cb_p1    : Callback function parameter
 *     cb_p2    : Callback function parameter 2
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_register_servloc_availability_cb(SERVREG_FNSIG_CALLBACK const callback, SERVREG_CB_PARAM const cb_p1, SERVREG_CB_PARAM const cb_p2)
{
   SERVREG_RESULT ret = SERVREG_FAILURE;

   if(SERVREG_NULL != callback)
   {
      /* Event indicates startup completes */
      if(RCECB_NULL != rcecb_register_parm2_name(SERVREG_LOC_INIT_NAME, (RCECB_CALLBACK_FNSIG_P2)callback, (RCECB_PARM)cb_p1, (RCECB_PARM)cb_p2))
      {
         ret = SERVREG_SUCCESS;
         MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: in servreg_register_servloc_availability_cb() client registered successfully");
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_register_servloc_availability_cb() registration failed \n");
         ret = SERVREG_INVALID_PARAM;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_LOC: in servreg_register_servloc_availability_cb() callback NULL \n");
      ret = SERVREG_INVALID_PARAM;
   }
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_get_servloc_availability
 *
 * Description:
 *     Clients use this function to query the current state of Service Locator feature
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     Service state of Service Locator feature.
 *     Refer to the enum SERVREG_RESULT for list of possible results.
 * =====================================================================  */
SERVREG_SERVICE_STATE servreg_get_servloc_availability(void)
{
   MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: in servreg_get_servloc_availability() status = %d \n", servreg_loc_client_init_status);
   return servreg_loc_client_init_status;
}

/** =====================================================================
 * Function:
 *     servreg_locator_init
 *
 * Description:
 *     Initialization function for the service locator module
 *
 * Parameters:
 *      None
 *
 * Returns:
 *      None
 * =====================================================================  */
void servreg_locator_init(void)
{
   SERVREG_RESULT ret = SERVREG_FAILURE;

   /* Static memory pool allocation init */
   servreg_dl_internal_init();

   ret = servreg_locator_client_init();

   /* Initialize the service monitor framework */
   servreg_monitor_init();

   /* Initialize the QDI driver for servreg for root/user pd */
   servreg_qdi_init();

   /* Initialize the service notifier (QDI and QMI) framework */
   servreg_notifier_init();

   if(SERVREG_SUCCESS == ret)
   {
      servreg_set_servreg_loc_availability();

      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: servreg_locator_init() SUCCESS ");
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_LOC: servreg_locator_init() not set, re-try 3 times ");

      /* Set timer to check for the ACK state as deferrable timer to avoid unnecessary wakeup of Q6 */
      timer_group_set_deferrable(&servreg_loc_retry_group, TRUE);

      if(TE_SUCCESS != timer_def_osal(&servreg_loc_retry_timer,
                                     &servreg_loc_retry_group,
                                     TIMER_FUNC1_CB_TYPE,
                                     servreg_loc_retry_timout_cb, NULL))
      {
          ERR_FATAL("SERVREG_LOC: Timer def failed", 0, 0, 0);
      }

      if(TE_SUCCESS != timer_set_64(&servreg_loc_retry_timer,
                                   SERVREG_LOC_RETRY_TIME_MS,
                                   SERVREG_LOC_RETRY_TIME_MS,
                                   T_MSEC))
      {
          ERR_FATAL("SERVREG_LOC: Timer set failed", 0, 0, 0);
      }
   }
}
