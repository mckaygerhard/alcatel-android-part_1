/*
#============================================================================
#  Name:
#    servreg_localdb.c 
#
#  Description:
#    File containing API's to access Service Registry local database
#                                                                            
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
#============================================================================
*/
#include "stdarg.h"
#include "stdlib.h"
#include <stringl/stringl.h>
#include "comdef.h"             /* Definitions for byte, word, etc.     */
#include "err.h"

#include "servreg_internal.h"
#include "servreg_localdb.h"

/* From servreg_local_db_augtogen.c file */
extern char *servreg_local_services[];
extern char *servreg_local_domain;
extern char *servreg_soc_name;
extern char *servreg_domain_name;
extern char *servreg_subdomain_name;
extern uint32_t servreg_service_count;
extern uint32_t servreg_qmi_instance_id;

#define SERVREG_SSR_NAME_PREFIX       "ssr:"
#define SERVREG_SSR_NAME_PREFIX_LEN   4
#define SERVREG_SSR_NAME_SUFFIX       ":before_shutdown"
#define SERVREG_SSR_NAME_SUFFIX_LEN   16

/** =====================================================================
 * Function:
 *     servreg_get_local_domain
 *
 * Description:
 *     Function to get the local domain info i.e "soc/domain/subdomain" string
 *     Queries the local database
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     string containing soc/domain/subdomain info
 * =====================================================================  */
SERVREG_NAME servreg_get_local_domain(void)
{
   return servreg_local_domain;
}

/** =====================================================================
 * Function:
 *     servreg_get_local_soc_scope
 *
 * Description:
 *     Function to get the local soc scope info
 *     Queries the local database
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     "soc" string
 * =====================================================================  */
SERVREG_NAME servreg_get_local_soc_scope(void)
{
   return servreg_soc_name;
}

/** =====================================================================
 * Function:
 *     servreg_get_local_domain_scope
 *
 * Description:
 *     Function to get the local domain scope
 *     Queries the local database
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     "domain" string
 * =====================================================================  */
SERVREG_NAME servreg_get_local_domain_scope(void)
{
   return servreg_domain_name;
}

/** =====================================================================
 * Function:
 *     servreg_get_local_subdomain_scope
 *
 * Description:
 *     Function to get the local subdomain scope
 *     Queries the local database
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     "subdomain" string
 * =====================================================================  */
SERVREG_NAME servreg_get_local_subdomain_scope(void)
{
   return servreg_subdomain_name;
}

/** =====================================================================
 * Function:
 *     servreg_get_local_instance_id
 *
 * Description:
 *     Function to get the local qmi instance id
 *     Queries the local database
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     qmi_instance_id for the notifier module
 * =====================================================================  */
uint32_t servreg_get_local_instance_id(void)
{
   return servreg_qmi_instance_id;
}

/** =====================================================================
 * Function:
 *     servreg_name_check
 *
 * Description:
 *     This function checks if the given name is a valid service name or not.
 *
 * Parameters:
 *     name : "soc/domain/subdomain/provider/service" or just "soc/domain/subdomain" info
 *
 * Returns:
 *     SERVREG_SUCCESS       : If name is valid
 *     SERVREG_INVALID_PARAM : If name is invalid
 * =====================================================================  */
SERVREG_RESULT servreg_name_check(SERVREG_NAME domain, SERVREG_NAME service)
{
   SERVREG_NAME sub_name = SERVREG_NULL, str_domain = domain, str_service = service;
   SERVREG_RESULT ret = SERVREG_INVALID_PARAM;
   uint32_t d_count = 0, s_count = 0;

   if(SERVREG_NULL != str_domain)
   {
      while(*str_domain != '\0')
      {
         if(strncmp(str_domain, "/", 1) == 0)
         {
            d_count = d_count + 1;
         }
         str_domain ++;
      }

      if(SERVREG_NULL != str_service)
      {
         while(*str_service != '\0')
         {
            if(strncmp(str_service, "/", 1) == 0)
            {
               s_count = s_count + 1;
            }
            str_service++;
         }

         if(d_count == 2)
         {
            sub_name = strstr(domain , servreg_soc_name);

            if(NULL != sub_name && sub_name == domain)
            {
               if(s_count == 1)
               {
                  ret = SERVREG_SUCCESS;
               }
            }
         }
      }
      else
      {
         /* domain = "soc/domain/subdomain/provider/service" or just domain = "soc/domain/subdomain" */
         if(d_count == 2 || d_count == 4)
         {
            sub_name = strstr(domain , servreg_soc_name);

            if(NULL != sub_name && sub_name == domain)
            {
               ret = SERVREG_SUCCESS;
            }
         }
      }
   }

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_get_ssr_name
 *
 * Description:
 *     This function takes in the service name and returns the before shutdown ssr message name
 *
 * Parameters:
 *     name : "soc/domain/subdomain/provider/service" or just "soc/domain/subdomain" info
 *
 * Returns:
 *     ssr name : "ssr:####:before_shutdown" #### will be the name extracted from the input param domain
 * =====================================================================  */
SERVREG_NAME servreg_get_ssr_name(SERVREG_NAME domain)
{
   SERVREG_NAME sub_name = SERVREG_NULL, ssr_name = SERVREG_NULL, sub_name1 = SERVREG_NULL, sub_name2 = SERVREG_NULL;
   uint32_t sub_name_len = 0, len = 0;

   /* Using strstr function. As name can be domain+service or just domain */
   sub_name = strstr(domain , "/");
   sub_name1 = strstr(domain , "/");

   if(NULL != sub_name)
   {
      sub_name++;
      sub_name1++;

      while(*sub_name != '/')
      {
         sub_name_len++;
         sub_name++;
      }

      /* First create a null terminated string that has only the domain info from the soc/domain/subdomain string */
      sub_name2 = (SERVREG_NAME)servreg_malloc((sub_name_len + 1) * sizeof(char));

      if(SERVREG_NULL != sub_name2)
      {
         strlcpy(sub_name2, sub_name1, sub_name_len + 1);

         len = SERVREG_SSR_NAME_PREFIX_LEN + sub_name_len + SERVREG_SSR_NAME_SUFFIX_LEN + 1;
         ssr_name = (SERVREG_NAME)servreg_malloc(len * sizeof(char));
         if(SERVREG_NULL != ssr_name)
         {
            strlcpy(ssr_name, SERVREG_SSR_NAME_PREFIX, len);
            strlcat(ssr_name, sub_name2, len);
            strlcat(ssr_name, SERVREG_SSR_NAME_SUFFIX, len);

            servreg_free(sub_name2);
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_DB: servreg_get_ssr_name() malloc failed");
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_DB: servreg_get_ssr_name() malloc failed");
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_DB: servreg_get_ssr_name() strstr failed");
   }
   return ssr_name;

}

/** =====================================================================
 * Function:
 *     servreg_is_local
 *
 * Description:
 *     This function checks if the domain is local or remote
 *     Given domain is said to be local if "soc/domian/subdomain" scopes matches with
 *     the local "soc/domian/subdomain" string
 *
 * Parameters:
 *     name : "soc/domain/subdomain/provider/service" or just "soc/domain/subdomain" info
 *
 * Returns:
 *     TRUE  : If domain is local
 *     FALSE : If domain is remote
 * =====================================================================  */
SERVREG_BOOL servreg_is_local(SERVREG_NAME name)
{
   SERVREG_NAME sub_name = SERVREG_NULL;
   SERVREG_BOOL ret = SERVREG_FALSE;

   /* Using strstr function. As name can be domain+service or just domain */
   sub_name = strstr(name , servreg_local_domain);

   if(NULL != sub_name)
   {
      ret = SERVREG_TRUE;
   }
   else
   {
      ret = SERVREG_FALSE;
   }
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_is_domain_local
 *
 * Description:
 *     This function checks if the domain is local or remote for root-pd vs user-pd
 *     if root-pd : name is local if "soc/domain" matches with that of the root-pd's "soc/domain"
 *     if user-pd : local if "soc/domian/subdomain" scopes matches with the local "soc/domian/subdomain" string
 *
 * Parameters:
 *     name : "soc/domain/subdomain/provider/service" or just "soc/domain/subdomain" info
 *
 * Returns:
 *     TRUE  : If domain is local
 *     FALSE : If domain is remote
 * =====================================================================  */
SERVREG_BOOL servreg_is_domain_local(SERVREG_NAME name)
{
   SERVREG_NAME sub_name = SERVREG_NULL;
   SERVREG_BOOL ret = SERVREG_FALSE;

   if(strcmp(servreg_subdomain_name, "root_pd") == 0)
   {
      /* Check if the service name is in the same subsystem */
      sub_name = strstr(name , servreg_domain_name);
      if(NULL != sub_name)
      {
         ret = SERVREG_TRUE;
      }
      else
      {
         ret = SERVREG_FALSE;
      }
   }
   else
   {
      ret = servreg_is_local(name);
   }

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_concat
 *
 * Description:
 *     This functions concatenates "soc/domain/subdomain" and "provider/service"
 *     string
 *
 * Parameters:
 *     domain : "soc/domain/subdomain" info
 *     service : "provider/service" info
 *
 * Returns:
 *     concatenated name : "soc/domain/subdomain/provider/service"
 * =====================================================================  */
SERVREG_NAME servreg_concat(SERVREG_NAME domain, SERVREG_NAME service)
{
   SERVREG_NAME servreg_name = SERVREG_NULL;
   uint32_t len = 0;

   if(SERVREG_NULL != domain)
   {
      if(SERVREG_NULL != service)
      {
         len = servreg_nmelen(domain) + 1 + servreg_nmelen(service) + 1;
         servreg_name = (SERVREG_NAME)servreg_malloc(len * sizeof(char));
         if(SERVREG_NULL != servreg_name)
         {
            strlcpy(servreg_name, domain, len);
            strlcat(servreg_name, "/", len);
            strlcat(servreg_name, service, len);
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_DB: servreg_concat() malloc failed");
         }
      }
      else
      {
         len = servreg_nmelen(domain) + 1;
         servreg_name = (SERVREG_NAME)servreg_malloc(len * sizeof(char));
         if(SERVREG_NULL != servreg_name)
         {
            strlcpy(servreg_name, domain, len);
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_DB: servreg_concat() malloc failed");
         }
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_DB: in servreg_concat() domain is NULL ");
   }

   return servreg_name;
}
