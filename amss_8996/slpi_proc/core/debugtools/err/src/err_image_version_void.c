/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

         E R R O R     R E P O R T I N G    S E R V I C E S

GENERAL DESCRIPTION
  This module updates image versioning info to coredump

Copyright (c) 2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        Edit History

$Header: //components/rel/core.slpi/1.0/debugtools/err/src/err_image_version_void.c#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/19/14   din     Adapted from err.c

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "erri.h"
#include "stringl.h"

/*===========================================================================

                 Defines and variable declarations for module

===========================================================================*/


/*===========================================================================

                              Function definitions

===========================================================================*/

/*===========================================================================

FUNCTION ERR_UPDATE_IMAGE_VERSIONING_INFO

DESCRIPTION
  Updates Image versioning info to coresump

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void err_update_image_versioning_info (void)
{ 

} /* err_update_image_versioning_info */


