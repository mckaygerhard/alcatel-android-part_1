# vi: tw=128 ts=3 sw=3 et
# @file dnt_builder_err.py
# @brief This file contains the API for the Error Services, API 0.1.0
#===============================================================================
# NOTE: The @brief description above does not appear in the PDF.
# The tms_mainpage.dox file contains the group/module descriptions that
# are displayed in the output PDF generated using Doxygen and LaTeX. To
# edit or update any of the group/module text in the PDF, edit the
# tms_mainpage.dox file or contact Tech Pubs.
#===============================================================================
#===============================================================================
# Copyright (c) 2014 Qualcomm Technologies Incorporated.
# All Rights Reserved.
# Qualcomm Confidential and Proprietary.
#===============================================================================
#===============================================================================
# Edit History
# $Header: //components/rel/core.slpi/1.0/bsp/build/scripts/dnt_builder_err.py#2 $
# $DateTime: 2014/09/22 12:19:06 $
# $Change: 6636715 $
# $Author: pwbldsvc $
#===============================================================================

import sys
if sys.hexversion < 0x02070000:
   raise error("python version not supported, update to 2.7 or better")

import csv
import os
import fnmatch
from SCons.Script import *
import operator
from operator import itemgetter
from collections import OrderedDict
import inspect

# debug instruments exposed by changing command line as so:
# build.cmd build_target BUILD_VER=00000 --debug=stacktrace --debuginfo=err

#===============================================================================
# Description
#===============================================================================

# This SCONS tool is the build time support for ERR build time services.

#===============================================================================
# Examples
#===============================================================================

#if 'USES_ERR_FATAL_CB' in env:
# env.AddErrFatalCbFunc(
#  BUILD_TAGS,
#  {
#   'name'                  : 'unique_name',
#   'entry'                 : 'function_entry'
#   'version'               : 'XX.YY.ZZ'        # Function Version [ 00.00.00 .. 65535.65535.65535 ]
#   'description'           : 'Textual description of the function operation, assumptions, etc...'
#  })

#===============================================================================
# IMAGE OWNER Installation Notes, Best Practices
#===============================================================================

# 1. Insure build_tags is the variable name that is used to reflect
# the list of build tag aliases associated to the image/library.
# Edit the scons file if necessary; do not attempt to use a different
# variable name.
#
# build_tags = [ ... ]
#
# 2. BUILD TAGS ARE IMPORTANT; if any specific intermediate output
# is not present, or empty (0 bytes size), that is an indication that
# the build_tags list may not match client specified build_tags. Have
# the tech area *add* a specific build_tag alias to the tech area SCONS
# build file. An IMAGE OWNER should never add a tech area build tag to
# the SCONS file of the image/library.
#
# 3. Edit the InitImageVars() parameters to specify 'build_tags = build_tags'
# The IMAGE OWNER needs to own and protect build_tags from arbitrary
# modification. BUILD TAGS ARE IMPORTANT.
#
# 4. Edit the InitImageVars() parameters to specify 'dnt_builder.py' is
# included with the list of other tools.
#
#env.InitImageVars(
#   ...
#   build_tags = build_tags,
#   ...
#   tools = ['buildspec_builder.py',
#            ...,
#            '${BUILD_ROOT}/core/bsp/build/scripts/dnt_builder.py',
#            ...,
#   )
#
# 5. Edit the UsesFlags section to include the following line. It must
# follow the InitImageVars() call, and it must precede all LoadAreaSoftwareUnits()
# calls. When there are no LoadAreaSoftwareUnits() calls, it must precede all
# link steps in the *_img.scons file.
#
# env.AddUsesFlags('USES_ERR_FATAL_CB_PLAYBOOK') # MUST OCCUR BEFORE FIRST LoadAreaSoftwareUnits
#
# For every IMAGE being created (*_img.scons)
#
# 6. Copy, Paste the following fragment into *_img.scons files.
#
# 7. There should be limited modification to the SCONS scripting below.
# Modify Variable PLAYLISTS only to reference intermediate outputs by path.
# Do not modify or decorate any of the output files; tech areas have expectations
# of existing names. Do not pollute the inherited construction environment with
# changes to CFLAGS, RequiredPublicApi(), or RequireRestrictedApi(). Construction
# environment of the *_img.scons file is inherited to all dependents loaded.

"""
#--- ERR_FATAL_CB Playbook Extension, Image Specific Details---------------- BEG

PLAYLISTS = [ # IMAGE_OWNER ADDS SPECIFIC PLAYLISTS, ONE PER LIBRARY, EXPLICIT, NOT AUTO_MAGIC
   '${BUILD_ROOT}/build/bsp/core_root_libs/build/AAAAAAAA/err_fatal_cb_paylist.pl', # LIBRARY EXPANDS AS $SHORT_BUILDPATH
   #'${BUILD_ROOT}/build/bsp/ssc_root_libs/build/AAAAAAAA/err_fatal_cb_playlist.pl', # LIBRARY EXPANDS AS $SHORT_BUILDPATH
]

# Follows all LoadAreaSoftwareUnits(). Precedes Link Step Details.
# Image Owner supplies PLAYLISTS. Avoid other customization this step.

# FOR SCONS TOOL EMITTERS TO PLACE OUTPUT PROPERLY
if not os.path.exists(env.RealPath('${SHORT_BUILDPATH}')):
   if Execute(Mkdir(env.RealPath('${SHORT_BUILDPATH}'))):
      raise

# ONLY WHEN DNT_BUILDER SCONS TOOL LOADED
if 'USES_ERR_FATAL_CB' in env and 'USES_ERR_FATAL_CB_PLAYBOOK' in env:

   # NEVER POLLUTE ENV CONSTRUCTION ENVIRONMENT WHICH GETS INHERITED
   playbook_env = env.Clone()

   # PLAYBOOK C OUTPUT THIS IMAGE (REQUIRED)
   output_c = playbook_env.RealPath('${SHORT_BUILDPATH}/${TARGET_NAME}_err_fatal_cb_autogen.c')
   playbook_env.AddErrFatalCbPlaybook(build_tags, output_c, PLAYLISTS)
   output_o = playbook_env.AddObject(build_tags, output_c)
   playbook_env.Depends(build_tags, output_c)      # Manage explicit detail outside of AU
   image_objs.append(output_o)                     # Manage explicit detail outside of AU

   # PLAYBOOK TXT OUTPUT THIS IMAGE (REQUIRED)
   output_txt = playbook_env.RealPath('${SHORT_BUILDPATH}/${TARGET_NAME}_err_fatal_cb_output_log.txt')
   playbook_env.AddErrFatalCbPlaybook(build_tags, output_txt, None)
   playbook_env.AddArtifact(build_tags, output_txt)
   playbook_env.Depends(output_txt, output_c)      # Manage explicit detail outside of AU
   image_units.append(output_txt)                  # Manage explicit detail outside of AU

#--- ERR_FATAL_CB Playbook Extension, Image Specific Details---------------- END
"""

# For every LIBRARY being created (*_libs.scons)
#
# 8. Copy, Paste the following fragment into *_libs.scons files.
#
# 9. There should be no modification to the SCONS scripting below.
# Do not modify or decorate any of the output files; tech areas have expectations
# of existing names. Do not pollute the inherited construction environment with
# changes to CFLAGS, RequiredPublicApi(), or RequireRestrictedApi(). Construction
# environment of the *_libs.scons file is inherited to all dependents loaded.

"""
#--- ERR_FATAL_CB Playbook Extension, Library Specific Details ------------- BEG

PLAYLISTS = [ ] # NONE USED

# Follows all LoadAreaSoftwareUnits(). Precedes Link Step Details.
# Image Owner supplies PLAYLISTS. Avoid other customization this step.

# FOR SCONS TOOL EMITTERS TO PLACE OUTPUT PROPERLY
if not os.path.exists(env.RealPath('${SHORT_BUILDPATH}')):
   if Execute(Mkdir(env.RealPath('${SHORT_BUILDPATH}'))):
      raise

# ONLY WHEN DNT_BUILDER SCONS TOOL LOADED
if 'USES_ERR_FATAL_CB' in env and 'USES_ERR_FATAL_CB_PLAYBOOK' in env:

   # NEVER POLLUTE ENV CONSTRUCTION ENVIRONMENT WHICH GETS INHERITED
   playbook_env = env.Clone()

   # PLAYLIST OUTPUT THIS LIBRARY
   output_pl = playbook_env.RealPath('${SHORT_BUILDPATH}/err_fatal_cb_playlist.pl')
   playbook_env.AddErrFatalCbPlaylist(build_tags, output_pl)
   playbook_env.AddArtifact(build_tags, output_pl)
   playbook_env.Depends(build_tags, output_pl)     # Manage explicit detail outside of AU
   image_units.append(output_pl)                   # Manage explicit detail outside of AU

   # PLAYBOOK TXT OUTPUT THIS LIBRARY
   output_txt = playbook_env.RealPath('${SHORT_BUILDPATH}/err_fatal_cb_playlist.txt')
   playbook_env.AddErrFatalCbPlaybook(build_tags, output_txt, None)
   playbook_env.AddArtifact(build_tags, output_txt)
   playbook_env.Depends(output_txt, output_pl)     # Manage explicit detail outside of AU
   image_units.append(output_txt)                  # Manage explicit detail outside of AU

#--- ERR_FATAL_CB Playbook Extension, Library Specific Details ------------- END
"""

#===============================================================================
# Module Scope Definitions
#===============================================================================

TOOL_USES_FLAG = 'USES_ERR_FATAL_CB'            # Specific Tool USES_ Flag
TOOL_ENV       = 'ENV_ERR_FATAL_CB'             # Specific Tool Construction Environment
TOOL_COLLECTOR = 'ERR_FATAL_CB_COLLECTOR'       # Specific Tool Collector

# Tool Keywords

TOOL_KEY_NAME         = 'name'                  # keyword
TOOL_KEY_NAME_MAXLEN  = 15                      # maximum value length
TOOL_KEY_HASH         = 'hash'                  # keyword
TOOL_KEY_ENTRY        = 'entry'                 # keyword
TOOL_KEY_VERSION      = 'version'               # keyword
TOOL_KEY_DESCRIPTION  = 'description'           # keyword
TOOL_KEY_DEFINED      = 'defined'               # INTERNAL, NOT CLIENT USED

# Tool Playbook, Playlist Output Extensions

PLAYLIST_EXT_PL   = '.pl'                       # Playlist Raw
PLAYBOOK_EXT_C    = '.c'                        # Playbook Source
PLAYBOOK_EXT_PB   = '.pb'                       # Playbook Raw
PLAYBOOK_EXT_TXT  = '.txt'                      # Playbook Text

#===============================================================================
# SCONS Hooks
#===============================================================================

# exists: determines if the compilers, assemblers, linkers, tools,
# etc.. are available and present. this controls the generation of a
# construction environment assosicated to env.

def exists(env):
   ''' SCONS Tool, Exists, Check Conditions to Install a Construction Environment '''
   tool_debug(env, 'exists()')

   # Problem in SCONS Configuration
   if env is None or not isinstance(env, Environment):
      raise

   return True

# generate: this generates a construction environment for use by
# scons; the environment is provided to use.

def generate(env):
   ''' SCONS Tool, Generate, Generate a Construction Environment '''
   tool_debug(env, 'generate()')

   # Problem in SCONS Configuration
   if env is None or not isinstance(env, Environment):
      raise

   # This tool supplies a USES_ flag for clients to utilize in the
   # construction environment.
   env.AddUsesFlags(TOOL_USES_FLAG)

   # This tool saves 'globals' build information into the construction
   # environment that is provided by client calls of exported
   # methods. Two symbols are setup to assist in keeping policy data
   # seperated from nominal data -- even though there is one
   # construction environment...

   env[TOOL_ENV] = env        # use first env for construction env task/function dictionary
   env_tool = env[TOOL_ENV]   # construct env task/function dictionary

   # Initialization of the construction environment 'globals'
   # information.

   env_tool[TOOL_COLLECTOR]= []

   # Associate this tool to the construction environment.

   action = env.GetBuilderAction(tool_action, action_source = None)
   scanner = env.Scanner(tool_target_scanner)
   builder = env.Builder(action = action, target_scanner = scanner, emitter = tool_emitter)

   # Name of Tool Builder

   env.Append(BUILDERS = {'ErrFatalCbBuilder' : builder}) # internalized

   # Client exposed methods to the tool. These function as 'pseudo
   # builders' to collect client information, validate, and then save
   # to the 'globals' construction environment.

   env.AddMethod(tool_method_add_function, 'AddErrFatalCbFunc')      # method for use by *_client.scons
   env.AddMethod(tool_method_add_playbook, 'AddErrFatalCbPlaybook')  # method for use by *_img.scons
   env.AddMethod(tool_method_add_playlist, 'AddErrFatalCbPlaylist')  # method for use by *_libs.scons


#===============================================================================

def tool_update_file_playbook_c(env, target, content):
   ''' Output TGT File '''
   tool_debug(env, 'tool_update_file_playbook('+target+')')
   with open(target, 'w') as output:
      output.write(env.CreateFileBanner(os.path.basename(str(target)), style='C'))
      output.write('\n')
      for line in content:
         output.write(line)

def tool_update_file_playbook_txt(env, target, content):
   ''' Output LOG File '''
   tool_debug(env, 'tool_update_file_playbook_txt('+target+')')
   with open(target, 'w') as output:
      for line in content:
         output.write(line)

def tool_update_file_playlist(env, target):
   ''' Output PL File '''
   tool_debug(env, 'tool_update_file_playlist('+target+')')
   sorted_entry = sorted(env[TOOL_COLLECTOR], key=lambda k: k[TOOL_KEY_NAME])
   with open(target, 'w') as output:
      for entry in sorted_entry:
         output.write(str(entry) + '\n')

#===============================================================================

# emitter: run while reading sconscripts; used to tweak the targets
# and sources of the build step (eg, library building might also
# create intermediates that need to be added to targets, or cleanup
# artifacts). there is no file system activity here, just the
# specification of dependency.

# Returns: tuple of 2 lists, [target], [source]

def tool_emitter(target, source, env):
   ''' SCONS Tool, Emitter, Sanitize Target/Source Dependency '''
   tool_debug(env, 'tool_emitter('+str(target[0])+')')

   targets = []
   sources = []

   # Nodes from source list

   if source != None:
      for s in source:
         if s != None:
            sources.append(File(env.RealPath(str(s))))

   # Anticipated target dependency information. The associated files
   # are created with the target as intended side effect. Note, each
   # of the targets appended will be fed to the target_scanner later.

   # Pull apart target and source pathing information for utilization
   # by tool_emitter.

   if target != None:
      for t in target:
         if t != None:

            target_full = env.RealPath(str(t))
            target_path = os.path.split(target_full)[0]
            target_stem, target_ext = os.path.splitext(target_full)

            if target_ext == PLAYLIST_EXT_PL:

               output = File(target_stem + PLAYLIST_EXT_PL)
               env.Clean(target, output)
               targets.append(output)

            elif target_ext == PLAYBOOK_EXT_C:

               output = File(target_stem + PLAYBOOK_EXT_C)
               env.Clean(target, output)
               targets.append(output)

            elif target_ext == PLAYBOOK_EXT_TXT:

               output = File(target_stem + PLAYBOOK_EXT_TXT)
               env.Clean(target, output)
               targets.append(output)

   return targets, sources

#===============================================================================

#target_scanner: run when explicit dependcies are up-to-date and it
#is otherwise possible for the step to be executed to build the file.
#this may add implicity dependency that must exist before the step
#can run. (eg, rebuild libraries before a program is linked). here is
#where some files called out by tool_emitter will be created if they
#do not exist.

# realize that this routine is subject to concurrent execution, and
# must not make any changes to the construction environment. also, it
# should noted that concurrent calling for a single node potentially
# would overwrite a file being concurrently created by a different
# node.

def tool_target_scanner(node, env, path):
   ''' SCONS Tool, Target Scanner, ... '''
   tool_debug(env, 'tool_target_scanner('+str(node)+')')

   returnList = [node]

   # Ignore output creation on appropriate SCONS options
   if env.GetOption('clean') or env.GetOption('no_exec'):
      tool_debug(env, 'TARGET CLEAN/NO_EXEC during tool scanner')
      return returnList

   # 'globals' information saved in the construction environment
   env_tool = env[TOOL_ENV] # construct env task/function dictionary

   target_full = env.RealPath(str(node))
   target_path = os.path.split(target_full)[0]
   target_stem, target_ext = os.path.splitext(target_full)

   # dependency target supplied by emitter

   if target_ext == PLAYLIST_EXT_PL:
      tool_update_file_playlist(env_tool, target_full)

   return returnList


#===============================================================================

# action: this is the portion of the tool that takes the source list
# and creates a target from it.

def tool_action(target, source, env):
   ''' SCONS Tool, Action Function, ... '''
   tool_debug(env, 'tool_action('+str(target[0])+')')

   # Ignore output creation on appropriate SCONS options
   if env.GetOption('clean') or env.GetOption('no_exec'):
      tool_debug(env, 'TARGET CLEAN/NO_EXEC during tool action')
      return 0 # Indicate SUCCESS back

   # 'globals' information saved in the construction environment
   env_tool = env[TOOL_ENV] # construct env task/function dictionary

   target_full = env.RealPath(str(target[0]))
   target_path = os.path.split(target_full)[0]
   target_stem, target_ext = os.path.splitext(target_full)

   # Loading of Playlist files to create the Playbook; tool_emitter has already
   # sanatized the list to those that exist.

   playlists = map(lambda x: str(x), source)
   for playlist in playlists:
      playlist_stem, playlist_ext = os.path.splitext(env.RealPath(playlist))
      if playlist_ext == PLAYLIST_EXT_PL:
         tool_debug(env, 'Loading Playlist ' + playlist)
         with open(playlist, 'r') as input:
            for line in input:
               entry = eval(line)
               if isinstance(entry, OrderedDict):
                  if entry.has_key(TOOL_KEY_NAME):
                     tool_method_add_function(env, None, entry)
                  else:
                     tool_error(env, 'Specified Playlist incorrect internal format, no type {:s}'.format(playlist))
               else:
                  tool_error(env, 'Specified Playlist incorrect internal format, no matching instance {:s}'.format(playlist))

   # Prepare the Playbook output
   PlaybookOutput = []

   #generate header includes
   PlaybookOutput.append('/*build_id {:s}*/\n'.format(env.get('BUILD_ID')))
   PlaybookOutput.append('\n#if defined(__cplusplus)\nextern "C" {\n#endif\n')
   PlaybookOutput.append('\n/* ******************************** */\n')

   #loop through entries collected
   ents = sorted(env_tool[TOOL_COLLECTOR], key=lambda k: k[TOOL_KEY_NAME])

   for entry in ents:
      PlaybookOutput.append('''
/*
name        {0:s}
hash        {1:s}
entry       {2:s}
version     {3:s}.{4:s}.{5:s}
description {6:s}
*/
void {2:s}(void); // forward reference
'''.format(entry[TOOL_KEY_NAME], entry[TOOL_KEY_HASH], entry[TOOL_KEY_ENTRY],
   str(entry[TOOL_KEY_VERSION]['major']), str(entry[TOOL_KEY_VERSION]['minor']), str(entry[TOOL_KEY_VERSION]['patch']),
   entry[TOOL_KEY_DESCRIPTION]))

   PlaybookOutput.append('''
/* ******************************** */

const struct { void* a1; } err_fatal_cb_internal_ro[] = { // CB Registrants
''')

   for entry in ents:
      PlaybookOutput.append('''{{ {1:s} }}, // {0:s}
'''.format(entry[TOOL_KEY_NAME], entry[TOOL_KEY_ENTRY]))

   PlaybookOutput.append('''{ (void*)0 } // END OF LIST
};

/* ******************************** */

struct { unsigned long a1; } err_fatal_cb_internal_rw[] = { // Storage Space Per CB (4 Byte)
''')

   for entry in ents:
      PlaybookOutput.append('''{{ 0 }}, // {0:s}
'''.format(entry[TOOL_KEY_NAME], entry[TOOL_KEY_ENTRY]))

   PlaybookOutput.append('''{ 0 } // END OF LIST
};

/* ******************************** */

''')

   PlaybookOutput.append('\n#if defined(__cplusplus)\n}\n#endif\n')

   #form output log
   PlaybookOutputLog = []

   PlaybookOutputLog.append('''
****************************************
ERR_FATAL_CB OUTPUT LOG
****************************************
''')

   for entry in ents:
      PlaybookOutputLog.append('''
name        {0:s}
hash        {1:s}
entry       {2:s}
version     {3:s}.{4:s}.{5:s}
description {6:s}
'''.format(entry[TOOL_KEY_NAME], entry[TOOL_KEY_HASH], entry[TOOL_KEY_ENTRY],
   str(entry[TOOL_KEY_VERSION]['major']), str(entry[TOOL_KEY_VERSION]['minor']), str(entry[TOOL_KEY_VERSION]['patch']),
   entry[TOOL_KEY_DESCRIPTION]))

   #append output log to end of file
   PlaybookOutput.append('\n/*\n')
   for line in PlaybookOutputLog:
      PlaybookOutput.append(line)
   PlaybookOutput.append('\n*/\n')

   # Output what was requested from builder based on extension

   # Intermediate Output
   if target_ext == PLAYLIST_EXT_PL:
      tool_update_file_playlist(env_tool, target_full)

   if target_ext == PLAYBOOK_EXT_C:
      tool_update_file_playbook_c(env_tool, target_full, PlaybookOutput)

   # Diagnostic Output
   if target_ext == PLAYBOOK_EXT_TXT:
      tool_update_file_playbook_txt(env_tool, target_full, PlaybookOutputLog)

   return 0 # Indicate SUCCESS back

#===============================================================================

def tool_method_add_playlist(env, build_ids, output):
   ''' Method Call, Calls pseudoBuilder '''
   tool_debug(env, 'tool_method_add_playlist('+str(output[0])+')')

   if not isinstance(output, list):
      output = [output]

   output_full = env.RealPath(str(output[0]))
   output_path = os.path.split(output_full)[0]
   output_stem, output_ext = os.path.splitext(output_full)

   if output_ext == '':
      output_full = output_stem + PLAYLIST_EXT_PL
      env.ErrFatalCbBuilder(target=output_full, source=None)
      #env.AddArtifact(build_ids, output_full)

   elif output_ext == PLAYLIST_EXT_PL:
      env.ErrFatalCbBuilder(target=output_full, source=None)
      #env.AddArtifact(build_ids, output_full)

   else:
      tool_error(env, 'Method call with unrecognised file type "' + output_ext + '"')

   return output, input

#===============================================================================

def tool_method_add_playbook(env, build_ids, output, input):
   ''' Method Call, Calls pseudoBuilder '''
   tool_debug(env, 'tool_method_add_playbook('+str(output[0])+')')

   if not isinstance(input, list):
      input = [input]

   if not isinstance(output, list):
      output = [output]

   output_full = env.RealPath(str(output[0]))
   output_path = os.path.split(output_full)[0]
   output_stem, output_ext = os.path.splitext(output_full)

   if output_ext == '':
      output_full = output_stem + PLAYBOOK_EXT_C
      env.ErrFatalCbBuilder(target=output_full, source=input)
      env.RequirePublicApi(['DEBUGTOOLS', 'TMS',]) # Required to Compile
      #env.AddObject(build_ids, output_full)

   elif output_ext == PLAYLIST_EXT_PL:
      env.ErrFatalCbBuilder(target=output_full, source=input)
      #env.AddArtifact(build_ids, output_full)

   if output_ext == PLAYBOOK_EXT_C:
      env.ErrFatalCbBuilder(target=output_full, source=input)
      env.RequirePublicApi(['DEBUGTOOLS', 'TMS',]) # Required to Compile
      #env.AddObject(build_ids, output_full)

   elif output_ext == PLAYBOOK_EXT_TXT:
      env.ErrFatalCbBuilder(target=output_full, source=input)
      #env.AddArtifact(build_ids, output_full)

   else:
      tool_error(env, 'Method call with unrecognised file type "' + output_ext + '"')

   return output, input

#===============================================================================

def tool_method_add_function(env, build_ids, input_dict):
   ''' Method Call, Adds Function
   Validates input and stores to 'globals' construction environment
   Runs before tool_emitter() function '''

   # 'globals' information saved in the construction environment
   env_tool = env[TOOL_ENV] # construct env task/function dictionary

   # caller python frame of interest
   frame, filename, line_number, function_name, lines, index = inspect.getouterframes(inspect.currentframe())[2]

   # define the layout of the ordered dict, all fields in expected
   # ordering of the fields

   output_dict = OrderedDict()
   output_dict[TOOL_KEY_NAME] = None
   output_dict[TOOL_KEY_HASH] = None
   output_dict[TOOL_KEY_ENTRY] = None
   output_dict[TOOL_KEY_VERSION] = None
   output_dict[TOOL_KEY_DESCRIPTION] = None
   output_text = '..../'+filename.replace('\\','/').lower().replace(env.get('BUILD_ROOT', None).lower()+'/', '')
   output_dict[TOOL_KEY_DEFINED] = { 'file': output_text, 'line': line_number }

   #read in name
   if not input_dict.has_key(TOOL_KEY_NAME):
      tool_error(env, TOOL_KEY_NAME + ' required')
   if len(input_dict[TOOL_KEY_NAME]) > TOOL_KEY_NAME_MAXLEN:
      tool_error(env, TOOL_KEY_NAME + ' exceeds signifigant length shorten to ' + str(TOOL_KEY_NAME_MAXLEN) + ' ' + input_dict[TOOL_KEY_NAME])

   output_dict[TOOL_KEY_NAME] = input_dict[TOOL_KEY_NAME].lower()
   output_dict[TOOL_KEY_HASH] = '{:#x}'.format(tool_hash(output_dict[TOOL_KEY_NAME]))

   # Process input on SCONS Command Options
   if env.GetOption('clean') or env.GetOption('no_exec'):
      tool_debug(env, 'TARGET CLEAN/NO_EXEC {:s} "{:s}" {:d}'.format(output_dict[TOOL_KEY_NAME], output_dict[TOOL_KEY_DEFINED]['file'], output_dict[TOOL_KEY_DEFINED]['line']))
      return None # RETURN, IGNORED

   # Ignore input on specified BUILD_ID not being active
   if build_ids is not None and not env.IsTargetEnable(build_ids):
      tool_debug(env, 'TARGET NOT ACTIVE {:s} "{:s}" {:d}'.format(output_dict[TOOL_KEY_NAME], output_dict[TOOL_KEY_DEFINED]['file'], output_dict[TOOL_KEY_DEFINED]['line']))
      return None # RETURN, IGNORED

   #name unique checks
   for entry in env_tool[TOOL_COLLECTOR]:
      if entry.has_key(TOOL_KEY_NAME) and output_dict[TOOL_KEY_NAME] == entry[TOOL_KEY_NAME]:
         tool_error(env, 'Duplicate name {:s} "{:s}" {:d}'.format(output_dict[TOOL_KEY_NAME], output_dict[TOOL_KEY_DEFINED]['file'], output_dict[TOOL_KEY_DEFINED]['line']))
      if entry.has_key(TOOL_KEY_HASH) and output_dict[TOOL_KEY_HASH] == entry[TOOL_KEY_HASH]:
         tool_error(env, 'Duplicate name hash {:s} "{:s}" {:d}'.format(output_dict[TOOL_KEY_NAME], output_dict[TOOL_KEY_DEFINED]['file'], output_dict[TOOL_KEY_DEFINED]['line']))

   #read in entry
   if input_dict.has_key(TOOL_KEY_ENTRY):
      output_dict[TOOL_KEY_ENTRY] = input_dict[TOOL_KEY_ENTRY]
   else:
      tool_error(env, 'Entry required {:s} "{:s}" {:d}'.format(output_dict[TOOL_KEY_NAME], output_dict[TOOL_KEY_DEFINED]['file'], output_dict[TOOL_KEY_DEFINED]['line']))

   #read in version, Semantic Versioning 1.0.0, http://semver.org
   major = 0
   minor = 0
   patch = 0
   if input_dict.has_key(TOOL_KEY_VERSION):
      import re
      try:
         (major, minior, patch) = [t(s) for t,s in zip((int,int,int),re.search('^(\d+)\.(\d+)\.(\d+)$', input_dict[TOOL_KEY_VERSION]).groups())]
      except:
         tool_error(env, 'Version format incorrect {:s} "{:s}" {:d}'.format(output_dict[TOOL_KEY_NAME], output_dict[TOOL_KEY_DEFINED]['file'], output_dict[TOOL_KEY_DEFINED]['line']))
      output_dict[TOOL_KEY_VERSION] = { 'major' : major, 'minor' : minor, 'patch' : patch }
   else:
      tool_error(env, 'Version required {:s} "{:s}" {:d}'.format(output_dict[TOOL_KEY_NAME], output_dict[TOOL_KEY_DEFINED]['file'], output_dict[TOOL_KEY_DEFINED]['line']))

   #read in description
   if input_dict.has_key(TOOL_KEY_DESCRIPTION):
      output_dict[TOOL_KEY_DESCRIPTION] = input_dict[TOOL_KEY_DESCRIPTION]
   else:
      output_dict[TOOL_KEY_DESCRIPTION] = '-'

   #add to environment descriptor
   env_tool[TOOL_COLLECTOR].append(output_dict)
   tool_debug(env, 'tool_method_add_function {:s} "{:s}" {:d}'.format(output_dict[TOOL_KEY_NAME], output_dict[TOOL_KEY_DEFINED]['file'], output_dict[TOOL_KEY_DEFINED]['line']))

   return None


#===============================================================================
# Support Functions
#===============================================================================

def tool_error(env, Info):
   ''' FATAL Information, Stops Build '''
   env.PrintError('ERR ' + Info)
   raise


def tool_warning(env, Info):
   ''' Non-FATAL Information, Build Continues '''
   env.PrintWarning('ERR ' + Info)


def tool_info(env, Info):
   ''' Informative Information, Build Continues '''
   env.PrintInfo('ERR ' + Info)


def tool_debug(env, Info):
   ''' Diagnostic Information, Build Continues '''
   env.PrintDebugInfo('err', 'ERR ' + Info)


def tool_hash(name):
   ''' Tool Utility Function '''
   tmp = 0
   hash = 0
   for i in range(len(name)):
      hash = (hash << 4) + ord(name[i])
      tmp = hash & 0xf0000000
      if tmp != 0:
         hash ^= (tmp >> 24)
      hash &= ~tmp
   return hash

def tool_randstr(size):
   ''' Tool Utility Function '''
   import string
   import random
   allowed = string.ascii_uppercase # add any other allowed characters here
   tool_randstr = ''.join([allowed[random.randint(0, len(allowed) - 1)] for x in xrange(size)])
   return tool_randstr
