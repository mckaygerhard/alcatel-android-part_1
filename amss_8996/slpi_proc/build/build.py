#! /usr/bin/python 
#=================================================================================
#   File Name: build.py
#
#   Python Script to call the QC-SCons build System for Badger
#
# Copyright (c) 2014-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
# Qualcomm Technologies Proprietary and Confidential.
#
#---------------------------------------------------------------------------------
#
#  $Header: //components/rel/build.slpi/1.0/build.py#17 $
#  $DateTime: 2016/04/26 16:27:52 $
#  $Change: 10342560 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
#   when     who            what, where, why
# --------   ---        ----------------------------------------------------------
# 12/09/15   bd             Updated Hexagon tool version from 7.2.09 to 7.2.11
# 03/12/15   Naresh         Updated for SLPI strip/pack
# 12/12/14   sm             Fix the Python version check.
# 03/04/13   Haseeb Khan    Script to call the QC-SCons build System for Badger    
# 06/12/13   corinc         Moved Build.py to adsp_proc\build\
# 12/08/13   Haseeb Khan    Added Support to compile using verbose levels, 
#                           individual module compilation & with any Scons Options
# 04/15/14   corinc         Add tool version support from command line for both LLVM & gcc
# 04/29/14   corinc         Moved functions to new file adspprocess.py
# 04/29/14   corinc         reverted back because of BuildProducts.txt copying failure in Tiberium
# 05/07/14   corinc         re-architect build system phase 3
# 05/08/14   corinc         fixed the issue for alias
# 05/09/14   corinc         moved 2 OpenDSP code to be functions to new OpenDSP.py file
#=================================================================================

# First, validate the Python version is correct.
import sys

# Print out the Python version in use from the PATH.
if sys.version:
   print "\n\nPython Version:\n", sys.version
   
   # Check to see if the current version is Python 2.7.6 or higher.
   if sys.version_info[0] != 2 or sys.version_info[1] != 7 or sys.version_info[2] < 6:
       print "Error: Required Python installation is 2.7.6 or higher. Please verify if the correct Python installation is correctly added to PATH.\n"
       sys.exit(0)

# The version information could not be determined.  Exit.
else:
   print 'Error: Python installation not detected.\n'
   print 'Please verify if the correct Python installation is correctly added to PATH.\n'
   sys.exit(0)

# Import the remainder of the modules.
import os
import re
import subprocess
import time
import optparse
import fnmatch
import shutil, errno
import adspprocess
import OpenDSP
from buildSysInfo import defaultSysInfo
from buildSysInfo import buildSysInfo

# Checking current working directory.
cwd_dir = os.getcwd()
adspproc_dir = os.getcwd()
print "\n\n checking slpi_proc, current working directory: %s" % adspproc_dir

# Now changing the directory to slpi_proc
adsp_dir = "slpi_proc"
bsp_dir  = "bsp"
ms_dir   = "ms"

if os.path.exists(adsp_dir):
    os.chdir(adsp_dir)
    print "1. Changed to \\slpi_proc\n\n"
    pass
elif os.path.exists(bsp_dir) and os.path.exists(ms_dir):
    os.chdir('../')
    print "2. in \\slpi_proc\\build, moved up to \\slpi_proc \n\n"
    pass
else:
    print "3. Your are already in \\slpi_proc\n\n"
    pass # do nothing!

     
#=================================================================================
#=================================================================================
#                  Software Paths & other definitions are below
#                    (edit these based on requirement)
#=================================================================================
#=================================================================================

#=============   For Windows Environment (Recommended locations)   =============
#Hexagon tools path for windows
#Donot specify path till version folder, path will be appended later using 'q6_rtos_tools_version'
q6_tools_path_win = r"C:/Qualcomm/HEXAGON_Tools"

#Python installation path for windows
#This python path is for internal builds in Qualcomm
#User need not modify it as python path will be directly taken from local path
python_win = r"C:/CRMapps/Apps/Python276-64"

#=============   End of Windows Environment (Recommended locations)   =============


#=============   For Linux Environment (Recommended locations)   =============
#Hexagon tools from Local Linux machine
#Note: Hexagon tools from local Linux machine will be preferred if both(Local and Network paths) are present
q6_tools_path_linux = 'None'
if os.environ.get('HOME', None):   
   home_dir = os.environ.get('HOME', None)
   print '\nHOME directory is:', home_dir
   #Donot specify path till version folder, path will be appended later using 'q6_rtos_tools_version'
   q6_tools_path_linux = os.environ.get("HEXAGON_ROOT", None) #home_dir + "/Qualcomm/HEXAGON_Tools"

#Hexagon tools from Network Location
#Donot specify path till version folder, path will be appended later using 'q6_rtos_tools_version'
q6_tools_netpath_linux = '/pkg/qct/software/hexagon/releases/tools'

#Python installation path for Linux
#This python path is for internal builds in Qualcomm
#User need not modify it as python path will be directly taken from local path
python_linux = "/pkg/qct/software/python/2.7.6/bin/"




#=============   End of Linux Environment (Recommended locations)   =============

#=================================================================================
#=================================================================================
#                  Software Paths & other definitions ends here
#                    (edit these based on requirement)
#=================================================================================
#=================================================================================



#===================================================================================
#=================     Required Versions        ====================================
#                   (edit these based on requirement)
#===================================================================================
#=== Please update, if you wish to change to new hexagon toolset and q6 version. ===
#=== This would reflect current required versions for build system               ===
#===    and compilation would error out if the required versions are changed.    ===
#===================================================================================

#=================================================================================
#=================   End fof Required Versions   =================================
#                 (edit these based on requirement)
#=================================================================================



#=================================================================================
#=================================================================================
#                  Main build.py starts here
#=================================================================================
#=================================================================================

#=================================================================================
print '\nbuild.py usage help...'
usage = "Usage: python %prog -b <buildid> -c <chipset> -p <protection_domain> -o <others> -f <flags> -k \n \
                 --Command for build/clean of dsp image \n \
                   cd slpi_proc \n \
                      To Build dsp image:           python build.py -b 0x8fffffff -c msm8974 -o all   (by default -p mapped to 'mpd') \n \
                      To Build sim-only dsp image:  python build.py -b 0x8fffffff -c msm8974 -o sim   (by default -p mapped to 'mpd') \n \
                      To Clean dsp image:           python build.py -b 0x8fffffff -c msm8974 -o clean (by default -p mapped to 'mpd') \n\n \
                 --Command for build/clean of dsp image with default parameters \n \
                   cd slpi_proc \n \
                      To Build dsp image:           python build.py -o all    (this will take default parameters as: -b 0x8fffffff, -c msm8974 and -p mpd) \n \
                      To Build sim-only dsp image:  python build.py -o sim    (this will take default parameters as: -b 0x8fffffff, -c msm8974 and -p mpd) \n \
                      To Clean dsp image:           python build.py -o clean  (this will take default parameters as: -b 0x8fffffff, -c msm8974 and -p mpd) \n\n \
                 --Command for Only CoSim (Assuming the dsp image is already built) \n \
                   cd slpi_proc \n \
                      Only Build CoSim & Run CoSim:  python build.py -o cosim \n \
                      Only Run CoSim:                python build.py -o cosim_run \n\n \
                 --Command for build/clean of dsp image & CoSim \n \
                   cd slpi_proc \n \
                      To Build dsp image & Build CoSim, Run CoSim:           python build.py -o all cosim \n \
                      To Build dsp image & Run CoSim:                        python build.py -o all cosim_run \n \
                      To Build sim-only dsp image & Build CoSim, Run CoSim:  python build.py -o sim cosim \n \
                      To Build sim-only dsp image & Run CoSim:               python build.py -o sim cosim_run \n \
                      To Build dsp image & Run CoSim for OEM_ROOT:           python build.py -o all oemroot_cosim_run \n \
                      To Clean dsp image:                                    python build.py -o clean \n\n \
                 --Command to create HAP packages \n \
                   cd slpi_proc \n \
                      To create HK11 package:        python build.py -o pkg_hk11 \n \
                      To create HY11/HY31 package:   python build.py -o pkg_oem \n \
                      To create HD11 package:        python build.py -o pkg_hd11 \n \
                      To create HY22 package:        python build.py -o pkg_isv \n \
                      To create HCBSP_Test package:  python build.py -o pkg_hcbsp \n\n \
                 --Command to use Verbose levels \n \
                      To compile with verbose level 2:          python build.py -v 2 \n \
                      0[=off] or 1[=limited] or 2[=detailed] or 3[raw, no formatting] \n\n \
                 --Command to Compile Individual Module \n \
                      To Compile Individual Module in avs:      python build.py -m avs/aud/services/static_svcs/audio_dev_mgr \n \
                      (specify path just before 'build' folder)  \n\n \
                 --Command to add SCons Options \n \
                      To add SCons Options(seperated by comma): python build.py -s --implicit-deps-unchanged,--ignore-errors \n\n \
                 --For help:  python build.py -h"
parser = optparse.OptionParser(usage=usage)
parser.add_option('-b', '--buildid' , help='buildid option [Default: 0x8fffffff]', dest='buildid', action='store')
parser.add_option('-c', '--chipset' , help='chipset option [Default: msm8974]', dest='chipset', action='store')
parser.add_option('-p', '--pid' , help='protection domain option [Default: mpd]', dest='protection_domain', action='store')
parser.add_option("-o", "--others", help='other variable number of options: <ARG1> <ARG2> ... \
                   Eg: all sim clean cosim cosim_run oemroot_cosim_run \
                   To create packages: pkg_hk11, pkg_oem, pkg_hd11, \
                   pkg_isv, pkg_hcbsp              \
                   all: Build the dsp image.           \
                   sim: Build the sim-only dsp image.           \
                   clean: Clean the dsp image.                \
                   cosim: Build CoSim & Run CoSim will be Enabled. \
                   cosim_run: Only Run CoSim will be Enabled. \
                   oemroot_cosim_run: Only Run CoSim for OEM_ROOT will be Enabled.\nAny new arguments, in future, can be added here. ', \
                   dest='other_option', action="callback", callback=adspprocess.other_options_cb)
parser.add_option('-f', '--flags' , help='Flags to pass to the build system: <FLAG1> <FLAG2> ... \
                   Any new flags, in future, can be added here', \
                   dest='flags', action="callback", callback=adspprocess.other_options_cb)
parser.add_option('-k', '--kloc' , help='To enable klocwork [Default: Disabled]', dest='kloc', action='store_true')
parser.add_option('-t', '--test' , help='To automate testing of opendsp packages [Default: Disabled]', dest='test', action='store_true')
parser.add_option('-v', '--verbose' , help='To compile with specified verbose levels [Default 1] \
                   0[=off] or 1[=limited] or 2[=detailed] or 3[raw, no formatting]', dest='verbose', action='store')
parser.add_option('-m', '--module' , help='To Compile Individual Module (specify path just before \'build\' folder) [Default: Disabled]', dest='module', action='store')
parser.add_option('-a', '--alias' , help='To specify image alias [core_libs, avs_libs,adsp_link, sensor_img, msm8974_MPD, etc. if not specified, script will assign the target specific image alias based on chipset information ]', dest='alias', action='store')
parser.add_option('-s', '--sconsargs' , help='To add SCons Options(seperated by comma) like --implicit-deps-unchanged,--implicit-cache,--ignore-errors etc...', dest='sconsargs', action='store')
parser.add_option('--tv', '--toolversion' , help='To specify HexagonTools version [Default: --tv 5.1.04], GCC tool version is 5.x.x and before, LLVM tool version is 6.x.x and beyond', dest='toolversion', action='store')

(opts, args) = parser.parse_args()
print opts
print args
(opts, args) = adspprocess.postprocess_command_options(opts, args)
print opts
print args

parser.print_help()
print "\n\n"

alloptions = ['buildid', 'chipset', 'protection_domain', 'other_option', 'flags', 'kloc','verbose', 'module', 'alias', 'toolversion', 'sconsargs'] 

#=================================================
#  Initialize the build system info flags
#=================================================
defSysInfo=defaultSysInfo()
bldSysInfo=buildSysInfo()

############################################################################################
######## If 'build/default_pack.txt' present then extract default parameters from this file ##
########       If user options specified, will override these default values              ##
############################################################################################
default_params_filelist = adspprocess.find_files('build', 'default_pack.txt')
if default_params_filelist:
   default_params_file = open( "build/default_pack.txt", "r" )
   lines = []
   for line in default_params_file:      
      if not re.search('#', line):
         default_chipset_match = re.search('--chipset (\w\w\w\w\w\w\w)', line)
         default_pid_match = re.search('--pid (\w\w\w)', line)
         default_buildid_match = re.search('--buildid (\w\w\w\w\w\w\w\w\w\w)', line)
         default_others_match = re.search('--others (.+)', line)
         default_flags_match = re.search('--flags (.+)', line)
         default_verbose_match = re.search('--verbose (\d+)', line)
         default_module_match = re.search('--module (.+)', line)
         default_alias_match = re.search('--alias (.+)', line)
         default_sconsargs_match = re.search('--sconsargs (.+)', line)
         
         if default_chipset_match:
            defSysInfo.default_chipset = default_chipset_match.group(1)
         if default_pid_match:
            defSysInfo.default_pid = default_pid_match.group(1)
         if default_buildid_match:
            defSysInfo.default_buildid = default_buildid_match.group(1)   
         if default_others_match:
            defSysInfo.default_others = default_others_match.group(1)
         if default_flags_match:
            defSysInfo.default_flags = default_flags_match.group(1)
         if default_verbose_match:
            defSysInfo.default_verbose = default_verbose_match.group(1)
         if default_module_match:
            defSysInfo.default_module = default_module_match.group(1)  
         if default_alias_match:
            defSysInfo.default_alias = default_alias_match.group(1)
         if default_sconsargs_match:
            defSysInfo.default_sconsargs = default_sconsargs_match.group(1)   
         
   default_params_file.close()   

############################################################################################      

############################################################################################
######## If 'build/default_test.txt' present then extract default parameters from this file ##
######## File 'build/default_test.txt' used for testing purposes on CRM                     ##
######## This is ONLY for CRM i.e., with -k option ONLY                                   ##
######## Also, enables only for opendsp packages as it needs 'build/default_test.txt'       ##
########       If user options specified, will override these default values              ##
############################################################################################

default_testparams_filelist = adspprocess.find_files('build', 'default_test.txt')
if ((default_testparams_filelist and opts.kloc) or opts.test):
   default_testparams_file = open( "build/default_test.txt", "r" )
   lines = []
   for line in default_testparams_file:      
      default_testothers_match = re.search('--others (.+)', line)
      if default_testothers_match:
         defSysInfo.default_testothers = default_testothers_match.group(1)   
   default_testparams_file.close()   

############################################################################################
# Check all build options and set in the build system information flags
adspprocess.allOptionsCheck(opts, alloptions, defSysInfo, bldSysInfo)

#=================================================================================
# try to find BLD_ENV_BUILD_ID enviroment variable
env_bld_id = os.environ.get('BLD_ENV_BUILD_ID', None)
if env_bld_id and not opts.chipset:
   print 'Extracting CHIPSET from BLD_ENV_BUILD_ID'   
   adspprocess.set_chipset_target(env_bld_id, bldSysInfo.chipset_param, opts.chipset)       
   bldSysInfo.chipset_flag = 1
else:
   if bldSysInfo.chipset_param:
      #print 'Chipset Parameter:', bldSysInfo.chipset_param
      if not env_bld_id:
         print 'BLD_ENV_BUILD_ID enviroment variable is None'
      else:
         print '-c option provided from command-line over-rides chipset extraction from BLD_ENV_BUILD_ID', bldSysInfo.chipset_param
      adspprocess.set_chipset_target(env_bld_id, bldSysInfo.chipset_param, opts.chipset)

#------------------------------------------------------------------------------#
# Set the Q6 Version, and Toolchain.
#------------------------------------------------------------------------------#

# First, check to see if a toolchain has been passed in via the --tv argument.
if opts.toolversion != "":
   q6_rtos_tools_version = opts.toolversion
   q6_version = 'v60'

# Next, check all defined chipsets, and populate accordingly.
elif os.environ.get('CHIPSET', None) in ['msm8996']:
   q6_version = 'v60'
   q6_rtos_tools_version = '7.2.11'
elif os.environ.get('CHIPSET', None) in ['msm8998']:
   q6_version = 'v62'
   q6_rtos_tools_version = '7.4.02'

# If none are found, default the toolchain and version.  However, if we cannot
# find 'CHIPSET' in the environment, exit.
else:
   if os.environ.get('CHIPSET', None):
      print "WARNING:  Hexagon Tools not defined for CHIPSET=" + os.environ.get('CHIPSET', None)
      print "WARNING:  Defaulting to Hexagon Tools 7.2.11, v60"
      q6_version = 'v60'
      q6_rtos_tools_version = '7.2.11'
   else:
      print "ERROR:  Unable to locate 'CHIPSET' to set Hexagon Tools.  Exiting."
      sys.exit(0)

# Set the Hexagon Tools in the environment, to be picked up in SCons.
os.environ['HEXAGON_RTOS_RELEASE'] = q6_rtos_tools_version
os.environ['HEXAGON_Q6VERSION'] = q6_version
os.environ['Q6VERSION'] = q6_version

#==================================================================
# Set the tool version and path info after command parsing is done
#For internal builds in Qualcomm
kw_path = "\\\\qctdfsrt\\prj\\qct\\asw\\StaticAnalysis\\Windows\\Klocwork\\Server\\bin"
sagen_path = "\\\\stone\\aswcrm\\smart\\nt\\bin"


q6_tools_path_win = q6_tools_path_win.replace('\\', '/').rstrip('/')
python_win = python_win.replace('\\', '/').rstrip('/')
q6_tools_path_linux = q6_tools_path_linux.rstrip('/')
q6_tools_netpath_linux = q6_tools_netpath_linux.rstrip('/')
python_linux = python_linux.rstrip('/')

tool_version = ''.join(['uname -a'])
proc = subprocess.Popen(tool_version, stdout=subprocess.PIPE, shell=True)
(out, err) = proc.communicate()
if out:
   platform_match = re.search('(\w\w\w\w\w)', out)
   match = platform_match.group(1)
   print "\nOperating System::", out
else:
   match = 0

# get the main version number to specify the compiler bin path
q6_tools_major_version = int(q6_rtos_tools_version[0])
#print "Set HEXAGON_RTOS_RELEASE = q6_rtos_tools_version"
#print "q6_tools_major_version = ", q6_tools_major_version


q6_tools_linux = 'None'
if os.environ.get('HOME', None):
   q6_tools_linux = q6_tools_path_linux + "/" + q6_rtos_tools_version   
if not os.path.exists(q6_tools_linux): 
   q6_tools_path_linux = q6_tools_netpath_linux
   q6_tools_linux = q6_tools_path_linux + "/" + q6_rtos_tools_version

if match == 'Linux':
   #For Linux: If Hexagon tools installed at location other than Recommended location
   #then this code would try and search hexagon tools in local path
   if not os.path.exists(q6_tools_linux):
      (q6_tools_path_linux, q6_tools_linux) = adspprocess.search_exe_tool('which')
else:
   #For Windows: If Hexagon tools installed at location other than Recommended location
   #then this code would try and search hexagon tools in local path 
   where_tools_path = q6_tools_path_win  + "/" + q6_rtos_tools_version
   if not os.path.exists(where_tools_path):      
      q6_tools_path_win = adspprocess.search_exe_tool('where')    

# try to find CRM_BUILDID enviroment variable
env_bld_ver = os.environ.get('CRM_BUILDID', None)
if env_bld_ver:
    print 'Extracting QDSP6_BUILD_VERSION from CRM_BUILDID'
    build_ver = os.environ['CRM_BUILDID']
    build_ver_match = re.search(r'([\w\.\d-]+?)(AAAA\w+)', build_ver)
    if not build_ver_match:
       build_ver_match = re.search(r'([\w\.\d-]+)(-\d)', build_ver)
    os.environ['QDSP6_BUILD_VERSION'] = build_ver_match.group(1)
    bldSysInfo.buildid_flag = 1
else:    
    if bldSysInfo.buildid_param:       
        print 'CRM_BUILDID enviroment variable is None'
        os.environ['QDSP6_BUILD_VERSION'] = bldSysInfo.chipset_param.upper() + "_" + bldSysInfo.buildid_param
        build_ver_match = re.search('(..)\w', os.environ['QDSP6_BUILD_VERSION'])
        # if build_ver_match.group(1) != '0x':
          # print 'ERROR: QDSP6_BUILD_VERSION should be a 32-bit hexa-decimal value with 0x \n       Eg: 0x8fffffff'
          # sys.exit(0)

if bldSysInfo.build_flags or defSysInfo.default_flags:
      print 'flags Parameter:', bldSysInfo.flags_param
      os.environ['BUILD_FLAGS'] = bldSysInfo.flags_param
else:
      os.environ['BUILD_FLAGS'] = ''
      bldSysInfo.flags_param = ''
        
if bldSysInfo.build_verbose_flag or defSysInfo.default_verbose:
      os.environ['BUILD_VERBOSE'] = bldSysInfo.opts_verbose
else:
      os.environ['BUILD_VERBOSE'] = ''

if bldSysInfo.build_filter_flag or defSysInfo.default_module:      
      #opts_module = opts.module
      bldSysInfo.opts_module = bldSysInfo.opts_module.replace('\\', '/')      
      os.environ['BUILD_FILTER'] = bldSysInfo.opts_module
      extract_component = bldSysInfo.opts_module.split('/', 1)
      extract_component[0] = extract_component[0].lower()      
      if extract_component[0] == 'sensors':
         os.environ['BUILD_COMPONENT'] = 'sensor_img'
      else:
         os.environ['BUILD_COMPONENT'] = extract_component[0]+'_libs'
else:
      os.environ['BUILD_FILTER'] = ''
      os.environ['BUILD_COMPONENT'] = ''
      bldSysInfo.opts_module = ''
      extract_component = ''

# check after build_filter_flag
# if users use build_filter to specifiy tech area unit without specifying image alias, image alias will be extraced from component path and append a "_libs" or "_img" accordingly
# if the alias generated by extracting from build filter option doesn't work, users need to specify the image alias 
if bldSysInfo.image_alias_flag or defSysInfo.default_alias:
      os.environ['BUILD_COMPONENT'] = bldSysInfo.opts_alias
 
if bldSysInfo.build_sconsargs_flag or defSysInfo.default_sconsargs:
      #opts_sconsargs = opts.sconsargs
      os.environ['BUILD_SCONSARGS'] = bldSysInfo.opts_sconsargs
else:
      os.environ['BUILD_SCONSARGS'] = '' 
      bldSysInfo.opts_sconsargs = ''
    
    
os.environ['BUILD_FLAVOR'] = 'mpd'
os.environ['BUILD_ACT'] = ''
if bldSysInfo.mpd_flag:
   os.environ['BUILD_FLAVOR'] = 'mpd'
if bldSysInfo.spd_flag:
   os.environ['BUILD_FLAVOR'] = 'spd'
if bldSysInfo.clean_flag:
   os.environ['BUILD_ACT'] = 'clean'
if bldSysInfo.sim_flag:
   os.environ['BUILD_ACT'] = 'SIM'
if bldSysInfo.all_flag:
   os.environ['BUILD_ACT'] = ''
if bldSysInfo.sim_check:
   os.system("python -c 'from utils import check_sim;check_sim()'")   
   sys.exit(0)

tool_version = ''.join(['uname -a'])
proc = subprocess.Popen(tool_version, stdout=subprocess.PIPE, shell=True)
(out, err) = proc.communicate()
if out:
   platform_match = re.search('(\w\w\w\w\w)', out)
   match = platform_match.group(1)
   print "\nOperating System::", out
else:
   match = 0




if match == 'Linux':
   print '\n\nIt is Linux environment!!!\n\n'
   if os.environ.get('HEXAGON_ROOT', None):
      q6_tools_path_linux = os.environ.get('HEXAGON_ROOT', None)
   else:
      os.environ['HEXAGON_ROOT'] = q6_tools_path_linux  
   
   os.environ['Q6_TOOLS_ROOT'] =  q6_tools_linux
   os.environ['Q6_ROOT'] = os.environ.get('Q6_TOOLS_ROOT', None)
   q6_root = os.environ['Q6_ROOT']
   if q6_tools_major_version <= 6:
      q6_root_eclipse = ''.join([q6_root, '/eclipse'])
      q6_root_gnu_bin = ''.join([q6_root, '/gnu/bin'])
      q6_root_qc_lib = ''.join([q6_root, '/qc/lib/iss/v5'])
      q6_root_qc_bin = ''.join([q6_root, '/qc/bin'])
   else: # LLVM path >= version 7
      q6_root_eclipse = ''.join([q6_root, '/eclipse'])
      q6_root_gnu_bin = ''.join([q6_root, '/Tools/bin'])
      q6_root_qc_lib = ''.join([q6_root, '/Tools/lib/iss/v5'])
      q6_root_qc_bin = ''.join([q6_root, '/Tools/bin'])

   os.environ['PYTHON_ROOT'] = python_linux
   python_root = os.environ['PYTHON_ROOT']
# at this stage Python version is 2.7.x, set PYTHONPATH in os env, 
# because the PATHONPATH could still be set to 2.6.x and below
   if os.environ.get('PYTHONPATH') is not None:
      os.environ['PYTHONPATH'] = python_linux
   if os.environ.get('PYTHON_PATH') is not None:
      os.environ['PYTHON_PATH'] = python_linux

else:
   if os.environ.get('HEXAGON_ROOT', None):
      q6_tools_path_win = os.environ.get('HEXAGON_ROOT', None)
      q6_tools_path_win = q6_tools_path_win.replace('\\', '/')
      os.environ['HEXAGON_ROOT'] = q6_tools_path_win
   else:
      q6_tools_path_win = q6_tools_path_win.replace('\\', '/')
      os.environ['HEXAGON_ROOT'] = q6_tools_path_win
   
   # HEXAGON_TOOLS_ROOT is not used in build system. Added to support TestFramework as it uses HEXAGON_TOOLS_ROOT
   if os.environ.get('HEXAGON_TOOLS_ROOT', None):
      q6_tools_4tfwk = os.environ.get('HEXAGON_TOOLS_ROOT', None)
      q6_tools_4tfwk = q6_tools_4tfwk.replace('\\', '/')
      os.environ['HEXAGON_TOOLS_ROOT'] = q6_tools_4tfwk
   else:
      os.environ['HEXAGON_TOOLS_ROOT'] = q6_tools_path_win + "/" + q6_rtos_tools_version      
   
      
   q6_tools_win = q6_tools_path_win + "/" + q6_rtos_tools_version
   os.environ['Q6_TOOLS_ROOT'] = q6_tools_win
   q6_tools_root = os.environ['Q6_TOOLS_ROOT']
   os.environ['Q6_ROOT'] = os.environ.get('Q6_TOOLS_ROOT', None)
   q6_root = os.environ['Q6_ROOT']
   
   if q6_tools_major_version <= 6:
      q6_root_eclipse = ''.join([q6_root, '/eclipse'])
      q6_root_gnu_bin = ''.join([q6_root, '/gnu/bin'])
      q6_root_qc_lib = ''.join([q6_root, '/qc/lib/iss/v5'])
      q6_root_qc_bin = ''.join([q6_root, '/qc/bin'])
   else: # LLVM path >= version 7
      q6_root_eclipse = ''.join([q6_root, '/eclipse'])
      q6_root_gnu_bin = ''.join([q6_root, '/Tools/bin'])
      q6_root_qc_lib = ''.join([q6_root, '/Tools/lib/iss/v5'])
      q6_root_qc_bin = ''.join([q6_root, '/Tools/bin'])

   os.environ['PYTHON_ROOT'] = python_win
   python_root = os.environ['PYTHON_ROOT']   
   if os.environ.get('PYTHONPATH') is not None:
      os.environ['PYTHONPATH'] = python_win
   if os.environ.get('PYTHON_PATH') is not None:
      os.environ['PYTHON_PATH'] = python_win

print "Current OS Environment:"
print os.environ
print '\n'
local_path = os.environ['PATH']
print '\nOld PATH is:\n', os.environ['PATH']
print '\n\n'

if match != 'Linux':
   cwd_dir = os.getcwd()
   print "\n\nCurrent working directory is %s" % cwd_dir
   path_remove_cmd = ''.join([cwd_dir + "/hap/remove_cygwin.cmd"])
   if os.path.isfile(path_remove_cmd):   
      print path_remove_cmd
      proc = subprocess.Popen(path_remove_cmd, shell=True)
      (out, err) = proc.communicate()
      file_withpath = open("path_with_cygwin_removed.txt", "r")
      line_path = file_withpath.readline()
      local_path = line_path.replace('PATH=', '')
      file_withpath.close()
      os.remove('path_with_cygwin_removed.txt')

cmddbg_dir = os.getcwd()
print "\n\nbuild command directory is %s \n" % cmddbg_dir

if os.path.isfile("./build/build.py"):
   build_cmd = ''.join(['.\\tools\\build\\scons\\SCons\\scons --directory=./build ' , '--chipset=', os.environ.get('CHIPSET', None), ' ', \
                     '--build_version=', os.environ.get('QDSP6_BUILD_VERSION', None), ' ', '--build_flavor=', os.environ.get('BUILD_FLAVOR', None), ' ', \
                     '--build_act=', os.environ.get('BUILD_ACT'), ' ', '--build_flags=', os.environ.get('BUILD_FLAGS', None), ' ', \
                     '--build_verbose=', os.environ.get('BUILD_VERBOSE', None), ' ', '--build_component=', os.environ.get('BUILD_COMPONENT', None), ' ', \
                     '--build_filter=', os.environ.get('BUILD_FILTER', None), ' ', '--build_sconsargs=', os.environ.get('BUILD_SCONSARGS', None)])
   linux_build_cmd = ''.join(['./tools/build/scons/SCons/scons --directory=./build ', '--chipset=', os.environ.get('CHIPSET', None), ' ', \
                     '--build_version=', os.environ.get('QDSP6_BUILD_VERSION', None), ' ', '--build_flavor=', os.environ.get('BUILD_FLAVOR', None), ' ', \
                     '--build_act=', os.environ.get('BUILD_ACT'), ' ', '--build_flags=', os.environ.get('BUILD_FLAGS', None), ' ', \
                     '--build_verbose=', os.environ.get('BUILD_VERBOSE', None), ' ', '--build_component=', os.environ.get('BUILD_COMPONENT', None), ' ', \
                     '--build_filter=', os.environ.get('BUILD_FILTER', None), ' ', '--build_sconsargs=', os.environ.get('BUILD_SCONSARGS', None)])
   kw_build_cmd = ''.join([kw_path, '\\', 'kwinject -T kw_trace.txt ', build_cmd])
   
else:
   build_cmd = ''.join(['.\\tools\\build\\scons\\SCons\\scons ', '--chipset=', os.environ.get('CHIPSET', None), ' ', \
                     '--build_version=', os.environ.get('QDSP6_BUILD_VERSION', None), ' ', '--build_flavor=', os.environ.get('BUILD_FLAVOR', None), ' ', \
                     '--build_act=', os.environ.get('BUILD_ACT'), ' ', '--build_flags=', os.environ.get('BUILD_FLAGS', None), ' ', \
                     '--build_verbose=', os.environ.get('BUILD_VERBOSE', None), ' ', '--build_component=', os.environ.get('BUILD_COMPONENT', None), ' ', \
                     '--build_filter=', os.environ.get('BUILD_FILTER', None), ' ', '--build_sconsargs=', os.environ.get('BUILD_SCONSARGS', None)])
   linux_build_cmd = ''.join(['./tools/build/scons/SCons/scons ', '--chipset=', os.environ.get('CHIPSET', None), ' ', \
                     '--build_version=', os.environ.get('QDSP6_BUILD_VERSION', None), ' ', '--build_flavor=', os.environ.get('BUILD_FLAVOR', None), ' ', \
                     '--build_act=', os.environ.get('BUILD_ACT'), ' ', '--build_flags=', os.environ.get('BUILD_FLAGS', None), ' ', \
                     '--build_verbose=', os.environ.get('BUILD_VERBOSE', None), ' ', '--build_component=', os.environ.get('BUILD_COMPONENT', None), ' ', \
                     '--build_filter=', os.environ.get('BUILD_FILTER', None), ' ', '--build_sconsargs=', os.environ.get('BUILD_SCONSARGS', None)])
   kw_build_cmd = ''.join([kw_path, '\\', 'kwinject -T kw_trace.txt ', build_cmd])
   

if match != 'Linux':
   print "Setting paths now..."   
   new_path = ''.join([python_root, ';', q6_root_gnu_bin, ';', q6_root_qc_lib, ';', q6_root_qc_bin]) 

if match == 'Linux':
   if bldSysInfo.all_flag or bldSysInfo.sim_flag or bldSysInfo.clean_flag:
      latestCmdFile = open('./build/latest_build_command.txt', 'w')
      latestCmdFile.write(' '.join(sys.argv))
      latestCmdFile.close()
      
      new_path = ''.join([python_root, ':', q6_root_gnu_bin, ':', q6_root_qc_lib, ':', q6_root_qc_bin, ':'])
      new_engg_path = ''.join([new_path, ':', local_path, ':'])
      os.environ['PATH'] = new_engg_path
      print 'New PATH w.r.t Linux:\n', os.environ['PATH']
      adspprocess.summary_build(opts, defSysInfo, bldSysInfo)   
      print '\n\nBuild Command on Linux:\n', linux_build_cmd
      print "\n"   
      proc = subprocess.Popen(linux_build_cmd, shell=True)
      (out, err) = proc.communicate()   
else: #The complete code below till end is for non-linux case      
   if bldSysInfo.all_flag or bldSysInfo.sim_flag or bldSysInfo.clean_flag:      
      latestCmdFile = open('./build/latest_build_command.txt', 'w')
      latestCmdFile.write(' '.join(sys.argv))
      latestCmdFile.close()
      
      arg_flag = 0
      if bldSysInfo.other_option_flag:
         arg_flag = adspprocess.verify_args('\Aklocwork\Z', opts.other_option)         
      if arg_flag or bldSysInfo.klocwork_flag:	
         os.environ['BUILDSPEC'] = 'KLOCWORK'         
         new_kw_path = ''.join([new_path, ';', local_path, ';'])
         os.environ['PATH'] = new_kw_path
         print 'New PATH w.r.t klockwork:\n', os.environ['PATH']
         adspprocess.summary_build(opts, defSysInfo, bldSysInfo)
                  
         print '\n\nBuild Command with Klocwork:\n', kw_build_cmd
         print "\n"         
         proc = subprocess.Popen(kw_build_cmd, shell=True)
         (out, err) = proc.communicate()
               
         temp_dir = ".."
         os.chdir(temp_dir)
         cwd_dir = os.getcwd()      
         sagenconfig_run_cmd = ''.join([sagen_path, '\\', 'sagenconfig -r ', os.getcwd(), ' -t trace -f ./slpi_proc/kw_trace.txt'])      
         proc = subprocess.Popen(sagenconfig_run_cmd, shell=True)
         (out, err) = proc.communicate()
         adsp_dir = "slpi_proc"
         os.chdir(adsp_dir)
         cwd_dir = os.getcwd()      
      else:         
         new_engg_path = ''.join([new_path, ';', local_path])
         os.environ['PATH'] = new_engg_path
         print 'New PATH w.r.t engineering build:\n', os.environ['PATH']         
         adspprocess.summary_build(opts, defSysInfo, bldSysInfo)
               
         print '\n\nBuild Command for Engineering build:\n', build_cmd
         print "\n"         
         proc = subprocess.Popen(build_cmd, shell=True)
         (out, err) = proc.communicate()
         
if bldSysInfo.all_flag or bldSysInfo.sim_flag:
   cwd_dir = os.getcwd()
   print 'Current working directory', cwd_dir
   build_artifacts = ''.join([cwd_dir + "/obj/qdsp6v5_ReleaseG/slpi.mbn"])
   if os.path.exists(build_artifacts):
      print '\n\nCompilation Result:  SUCCESS'
   else:
      print '\n\nCompilation Result:  FAIL'

# Bit-exactness Verification
OpenDSP.bixExactnessVerify(bldSysInfo)
   
# HAP packages creation
OpenDSP.HAPPackagesCreate(bldSysInfo)

#=====================================================
# Print Build system flag information for debugging
#=====================================================
#defSysInfo.printDefaultInfo()
#bldSysInfo.printBuildSysInfo()
      
#=================================================================================            
#=================================================================================
#                  Main build.py ends here
#=================================================================================
#=================================================================================            
            
            

