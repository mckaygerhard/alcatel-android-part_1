#===============================================================================
#
# QFP services framework
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2015 - 2016 Qualcomm Technologies, Inc.  
# All Rights Reserved.
# Qualcomm Technologies Proprietary and Confidential.
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/qfp.slpi/1.0/framework/build/framework.scons#2 $
#  $DateTime: 2016/04/07 11:29:38 $
#  $Author: pwbldsvc $
#  $Change: 10225373 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 2016-03-25  BD     Remove source code for HD22
# 07/05/2015  LD     Initial version
#
#===============================================================================

Import('env')
env.Append(CCFLAGS = " -mllvm -diff-and-merge-extract-regions=0 ")

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'KERNEL',
   'MPROC',
   'SERVICES',
   'SNS_INT_SRVC',
   'SYSTEMDRIVERS',
   'BUSES'
]


env.RequirePublicApi(CBSP_API, area='core')

#-------------------------------------------------------------------------------
# Internal depends within Sensors
#-------------------------------------------------------------------------------
SENSORS_API = [
   'SNS_QUEUE',
   'SNS_MEMMGR',
   'SNS_EVMGR',
   'SNS_COMMON',
   'SNS_DEBUG_DSPS',
]

env.RequireRestrictedApi(SENSORS_API)


#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/Sensors/qfp/framework/src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
env.Append(CPPPATH = [
   "${BUILD_ROOT}/Sensors/common/inc",
   "${BUILD_ROOT}/Sensors/common/util/memmgr/inc",
   "${BUILD_ROOT}/Sensors/common/util/queue/inc",
   "${BUILD_ROOT}/Sensors/common/smr/inc",
   "${BUILD_ROOT}/core/api/mproc",
   "${BUILD_ROOT}/core/mproc/qmi",
   "${BUILD_ROOT}/core/mproc/qmi/src/qcsi",
   "${BUILD_ROOT}/core/mproc/qmi/src/common",
   "${BUILD_ROOT}/Sensors/api",
   "${BUILD_ROOT}/Sensors/common/idl/inc",
   "${BUILD_ROOT}/core/mproc/qmi/src/qcsi",
   "${BUILD_ROOT}/core/mproc/qmi/src/qcci",
   "${BUILD_ROOT}/core/mproc/qmi/src/common",
   "${BUILD_ROOT}/core/api/systemdrivers/pmic",
   "${BUILD_ROOT}/core/api/power/uSleep",
   "${BUILD_ROOT}/Sensors/pm/inc",
   "${BUILD_ROOT}/Sensors/smgr/inc",
   "${BUILD_ROOT}/Sensors/ssm/framework/inc",
   "${BUILD_ROOT}/Sensors/qfp/framework/inc",
   "${BUILD_ROOT}/Sensors/qfp/algo/common/inc",
   "${BUILD_ROOT}/Sensors/qfp/algo/ffd/inc",
   "${BUILD_ROOT}/Sensors/qfp/sd/inc"
])


#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
FRAMEWORK_SOURCES = [
      '${BUILDPATH}/sns_qfp_debug.c',
]

FRAMEWORK_U_SOURCES = [
      '${BUILDPATH}/sns_qfp_init.c',
      '${BUILDPATH}/sns_qfp_main.c',
      '${BUILDPATH}/sns_qfp_ffd_alg.c',
]
   
#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
# Sources that should not be shared
if 'SENSORS_DD_DEV_FLAG' in env:
  QFP_CLEAN_SOURCES  = env.FindFiles(['*.*'], '${BUILD_ROOT}/Sensors/qfp/framework/src')
  QFP_CLEAN_SOURCES += env.FindFiles(['*.h'], '${BUILD_ROOT}/Sensors/qfp/framework/inc')

  # Add binary library
  sns_qfp_lib = env.AddBinaryLibrary(
    ['CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SENSOR_SW'],
    '${BUILDPATH}/qfp_fw_dsps', FRAMEWORK_SOURCES)

  sns_qfp_u_lib = env.AddBinaryLibrary(
    ['CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SENSOR_SW'],
    '${BUILDPATH}/qfp_u_fw_dsps', FRAMEWORK_U_SOURCES)

  # Clean sources
  env.CleanPack(['CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SENSOR_SW'], QFP_CLEAN_SOURCES)
else:
  sns_qfp_lib = env.AddLibrary(
    ['CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SENSOR_SW'],
    '${BUILDPATH}/qfp_fw_dsps', FRAMEWORK_SOURCES)

  sns_qfp_u_lib = env.AddLibrary(
    ['CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SENSOR_SW'],
    '${BUILDPATH}/qfp_u_fw_dsps', FRAMEWORK_U_SOURCES)

if 'USES_ISLAND' in env:
   qfpfw_sections = ['.text.uQFP', '.data.uQFP']
   env.AddIslandLibrary(['CORE_QDSP6_SENSOR_SW'], sns_qfp_u_lib, qfpfw_sections)

