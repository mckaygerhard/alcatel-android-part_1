/*=============================================================================
  FILE: sns_qfp_init.c

  This file contains the initialization subroutine for the QFP service
  manager

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/framework/src/sns_qfp_init.c#1 $  */
/* $DateTime: 2016/02/25 10:35:39 $ */
/* $Author: kshyamas $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order. 

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2016-02-03  LB   Fix continuous mode island mode voting
  2015-07-05  LD   Initial version

=============================================================================*/


/*-----------------------------------------------------------------------------
* Include Files
* ---------------------------------------------------------------------------*/

#include "sns_common.h"
#include "sns_osa.h"
#include "qmi_csi.h"
#include <qmi_client.h>
#include <qmi_idl_lib_internal.h>
#include "sns_init.h"
#include "sensor1.h"
#include "sns_qfp_sm_priv.h"
#include "sns_qfp_v01.h"
#include "sns_qfp_priv.h"

// TODO override QFP stack size from sns_common.h, put a larger stack size
// here needed for sns_qfp_process_debug_msg (debug response message is 
// defined locally, about 4KB). Remove this once we fix the code there
//#define SNS_MODULE_STK_SIZE_DSPS_QFP 0x2000 /* 8KB */
// TODO! should allocate the debug allocations on heap and save the 4KB here. 
// This is currently commented out because it causes a compilation error when it is not. 

/* -----------------------------------------------------------------------
** Static variables
** ----------------------------------------------------------------------- */
// Stack memory for the QFP SM Thread
SNS_QFP_UIMAGE_DATA static OS_STK qfp_sm_stack[SNS_MODULE_STK_SIZE_DSPS_QFP];

// Primary data structure used by QFP SM framework
SNS_QFP_UIMAGE_DATA sns_qfp_sm_type_s sns_qfp;
SNS_QFP_UIMAGE_DATA sns_pm_handle_t sns_qfp_pm_handle;

/*=============================================================================
  FUNCTION:  sns_qfp_handle_req_cb
=============================================================================*/
/*!
   @brief    qmi_csi_process_req callback for QFP service manager

   @detail  This Function implements the qmi_csi_process_req_callback function 
            as defined in qmi_csi.h. When QFP receives an signal indicating a
            new event has been received, QFP calls qmi_csi_handle_event. The
            QCSI framework then decodes the received message and then calls 
            this function.

   @param[i]   *connection_handle  Handle provided to QCSI in 
                                   sns_qfp_qmi_csi_connect_cb
   @param[i]   req_handle          Handle provided by framework to identify
                                   this transaction
   @param[i]   msg_id              Message ID for the received message
   @param[i]   req_c_struct        C structure with decoded message
   @param[i]   req_c_struct_len    C data structure size
   @param[i]   service_cookie      Handle provided by QFP to QCSI to when
                                   registering service

   @return     err                 Returns QMI_CSI_CB_NO_ERR if successful.
                                   Otherwise returns QMI_CSI_CB_UNSUPPORTED_ERR

 */
/*===========================================================================*/
static qmi_csi_cb_error sns_qfp_handle_req_cb
(
void                     *connection_handle,
qmi_req_handle           req_handle,
unsigned int             msg_id,
void                     *req_c_struct,
unsigned int             req_c_struct_len,
void                     *service_cookie
)
{
  uint8_t i;
  bool cookie_valid = false;

  SNS_QFP_PRINTF0(LOW, "QFP: QMI callback");
  sns_qfp_service *srv = (sns_qfp_service *) service_cookie;

  for (i = 0; i < SNS_QFP_MAX_SERVICES; i++)
  {
    if (srv == &sns_qfp.qfp_service[i])
    {
      cookie_valid = true;
      break;
    }
  }

  if (cookie_valid == true)
  {
    // Check if QFP main service Request message
    if (srv->service == SNS_QFP_SRV_MAIN)
    {
      sns_err_code_e err;
      err = sns_qfp_proc_req( srv,  
                              (sns_qfp_qmi_connection_s*)connection_handle,
                              req_handle, msg_id, req_c_struct, 
                              req_c_struct_len );
      if ( SNS_ERR_BAD_MSG_ID == err )
      {
        SNS_QFP_PRINTF1(HIGH, "QFP: unknown message id %d", (int)msg_id);
        return QMI_CSI_CB_UNSUPPORTED_ERR;
      }
      else if ( SNS_SUCCESS != err )
      {
        SNS_QFP_PRINTF1(ERROR, "QFP: req error %d", err);
        return QMI_CSI_CB_INTERNAL_ERR;
      }
    }
    else
    {
      SNS_QFP_PRINTF1(LOW, "QFP: QMI callback: invalid Service %d",
                      (int)srv->service);
    }
  }
  else
  {
    SNS_QFP_PRINTF0(LOW, "QFP: QMI callback: invalid service cookie");
  }
  return QMI_CSI_CB_NO_ERR;
}


/*=============================================================================
  FUNCTION:  sns_qfp_qmi_csi_connect_cb
=============================================================================*/
/*!
  @brief qmi_csi_connect callback for QFP

  @detail   This Function implements the qmi_csi_connect callback function 
            defined in qmi_csi.h. The QCSI framework calls this callback 
            function when it receives a request from each service user (client)

   @param[i]   client_handle        Handle used by QCSI framework to identify 
                                    the client
   @param[i]   service_cookie       Service specific data used by QFP to 
                                    identify a service.
   @param[o]   connection_handle    The QFP service returns this handle to QCSI
                                    framework to represent this client 
                                    connection

   @return     err                  Returns QMI_CSI_CB_NO_ERR if successful.
                                    Otherwise returns QMI_CSI_CB_CONN_REFUSED
 */
/*===========================================================================*/

qmi_csi_cb_error sns_qfp_qmi_csi_connect_cb(qmi_client_handle client_handle,
                                            void *service_cookie,
                                            void **connection_handle)
{
  uint8_t i;
  qmi_csi_cb_error err = QMI_CSI_CB_CONN_REFUSED;
  sns_qfp_service * srv = (sns_qfp_service *) service_cookie;
  bool cookie_valid = false;

  for (i = 0; i < SNS_QFP_MAX_SERVICES; i++)
  {
    if (srv == &sns_qfp.qfp_service[i])
    {
      cookie_valid = true;
      break;
    }
  }

  if (cookie_valid == true)
  {
    for (i = 0; i < SNS_QFP_MAX_CLIENT_CONNECTIONS; i++)
    {
      if (!(srv->connection_handle[i].connection_valid))
      {
        srv->connection_handle[i].connection_valid = true;
        srv->connection_handle[i].client_handle = client_handle;
        *connection_handle = (void*)&(srv->connection_handle[i]);
        err = QMI_CSI_CB_NO_ERR;
        break;
      }
    }
  }

  return err;
}

/*=============================================================================
  FUNCTION:  sns_qfp_qmi_csi_disconnect_cb
=============================================================================*/
/*!
  @brief       qmi_csi_disconnect callback for QFP 

  @detail      This callback function is called by QCSI framework when the 
               service user (client) disconnects.

   @param[i]   *connection_handle   Handle provided to QCSI in 
                                    sns_qfp_qmi_csi_connect_cb
   @param[i]   *service_cookie      Handle provided by QFP to QCSI to when 
                                    registering service

  @return   none
 */
/*===========================================================================*/

void sns_qfp_qmi_csi_disconnect_cb
(
void* connection_handle,
void* service_cookie
)
{
  sns_qfp_service * srv = (sns_qfp_service *) service_cookie;
  uint8_t i;
  bool cookie_valid = false;

  for (i = 0; i < SNS_QFP_MAX_SERVICES; i++)
  {
    if (srv == &sns_qfp.qfp_service[i])
    {
      cookie_valid = true;
      break;
    }
  }

  if (cookie_valid == true)
  {
    for (i = 0; i < SNS_QFP_MAX_CLIENT_CONNECTIONS; i++)
    {
      if (&srv->connection_handle[i] == connection_handle)
      {
        srv->connection_handle[i].connection_valid = false;
        srv->connection_handle[i].client_handle = NULL;
        if ( srv->service_disconnect_fcn != NULL )
        {
          srv->service_disconnect_fcn(service_cookie, connection_handle);
        }
        break;
      }
    }
  }
}

/*=============================================================================
  FUNCTION:  sns_qfp_sm_task
=============================================================================*/
/*!
  @brief       Main loop for the QFP service manager thread

  @detail      This function containst the main loop for the QFP SM thread.
               Control never returns from this function. The QFP SM thread waits
               for an event and then calls the appropriate function to handle
               each event.

   @param[i]   *sns_qfp   The Primary data structure used by the QFP framework


  @return   none
 */
/*===========================================================================*/
SNS_QFP_UIMAGE_CODE void sns_qfp_sm_task(sns_qfp_sm_type_s *sns_qfp)
{
  uint8_t qfp_task_index;

  while (true)
  {
    OS_FLAGS        sig_flags = 0;
    OS_FLAGS        wait_flags = 0;
    uint8_t         err;
    sns_qfp_service *srv;
    sns_qfp_type_s  *ctx;

    srv = sns_qfp->qfp_service;
    ctx = (sns_qfp_type_s *) srv->data;

    wait_flags = (SNS_QFP_QMI_WAIT_SIG + SNS_QFP_FINGER_DETECT_SIG);

    // The OS will wait for a signal. When a signal is recieved, it will 
    // consume it and also return the signal that was recieved. 
    sig_flags = sns_os_sigs_pend(sns_qfp->qfp_flag_grp,
                                 wait_flags,
                                 OS_FLAG_WAIT_SET_ANY + OS_FLAG_CONSUME,
                                 0,
                                 &err);
    if (QFP_BIT_TEST(sig_flags, SNS_QFP_QMI_WAIT_SIG))
    {
      SNS_QFP_PRINTF0(HIGH, "QFP: QMI signal");
      sns_qfp_pm_vote(SNS_IMG_MODE_BIG);

      for (qfp_task_index = 0; qfp_task_index < SNS_QFP_MAX_SERVICES; 
          qfp_task_index++)
      {
        if (sns_qfp->qfp_service[qfp_task_index].service_valid == true)
        {
          qmi_csi_handle_event(sns_qfp->qfp_service[qfp_task_index].service_handle,
                               &sns_qfp->qfp_service[qfp_task_index].os_params);
        }
      }

      sns_qfp_pm_vote(SNS_IMG_MODE_NOCLIENT);
    }
    /* Handle signal functions */
    sns_qfp_proc_signals(&sns_qfp->qfp_service[SNS_QFP_SRV_MAIN],
                         sig_flags);
  }
}


/*=============================================================================
  FUNCTION:  qfp_sm_init_thread
==============================================================================*/
/*!
  @brief       Initializes the QFP service manager thread

  @detail      This function performs all the initializations required by the
               QFP SM thread Registering for QCCI and QCSI also occurs in this
               function.

  @param[i]    *arg      NULL pointer that is not used

  @return   none
 */
/*===========================================================================*/

STATIC void qfp_sm_init_thread( void * args)
{
  uint8_t err;
  sns_err_code_e sns_err;
  uint8_t i;

  SNS_QFP_PRINTF0(LOW, "QFP: qfp_sm_init_thread: Enter");

  for (i = 0; i < SNS_QFP_MAX_SERVICES; i++)
  {
    sns_qfp.qfp_service[i].service_valid = NULL;
  }

  /*=============Initialize services========================================*/

  // Main service
  SNS_QFP_PRINTF0(LOW, "QFP: Starting main Service");
  sns_err = sns_qfp_init(&sns_qfp.qfp_service[0]);
  if (sns_err == SNS_SUCCESS)
  {
    sns_qfp.qfp_service[0].service = SNS_QFP_SRV_MAIN;
    sns_qfp.qfp_service[0].service_cookie_ptr = (void *)&sns_qfp.qfp_service[0];
    sns_qfp.qfp_service[0].service_disconnect_fcn = NULL;

    sns_os_sigs_add(sns_qfp.qfp_flag_grp,
                    (SNS_QFP_QMI_WAIT_SIG + SNS_QFP_FINGER_DETECT_SIG));
    sns_os_set_qmi_csi_params(sns_qfp.qfp_flag_grp,
                              SNS_QFP_QMI_WAIT_SIG,
                              &sns_qfp.qfp_service[0].os_params,&err);
    SNS_ASSERT(err == OS_ERR_NONE);

    QMI_CSI_OPTIONS_INIT(sns_qfp.qfp_service[0].service_options);
    sns_qfp.qfp_service[0].service_status = 
    (uint32_t) qmi_csi_register_with_options(
                                            SNS_QFP_SVC_get_service_object_v01(),
                                            sns_qfp_qmi_csi_connect_cb,
                                            sns_qfp_qmi_csi_disconnect_cb,
                                            sns_qfp_handle_req_cb,
                                            sns_qfp.qfp_service[0].service_cookie_ptr,
                                            &sns_qfp.qfp_service[0].os_params,
                                            &sns_qfp.qfp_service[0].service_options,
                                            &sns_qfp.qfp_service[0].service_handle);
    sns_qfp.qfp_service[0].service_valid = true;
  }

  {
    sns_pm_err_code_e pm_err;
    pm_err = sns_pm_client_init( &sns_qfp_pm_handle, NULL, "QFP", SNS_PM_CLIENT_ID_QFP);
    if (SNS_PM_SUCCESS != pm_err)
    {
      SNS_QFP_PRINTF1(ERROR, "QFP: PM client init fails, err %d", pm_err);
    }
  }

  // Init done
  sns_init_done();

  sns_qfp_sm_task(&sns_qfp);

  // Main task loop, does not return
}

/*=============================================================================
  FUNCTION:  sns_qfp_sm_init
=============================================================================*/
/*!
  @brief    Creates QFP service manager thread

  @detail   This function creates the QFP SM thread. See sns_init.h

  @return   err   Returns SNS_SUCCESS if successful.
 */
/*===========================================================================*/

sns_err_code_e sns_qfp_sm_init( void )
{
  uint8_t        err;

  SNS_QFP_PRINTF0(LOW, "QFP: Creating QFP thread");

  // Initialize events
  sns_qfp.qfp_flag_grp =
  sns_os_sigs_create((SNS_QFP_QMI_WAIT_SIG + SNS_QFP_FINGER_DETECT_SIG),
                     &err);
  SNS_ASSERT(sns_qfp.qfp_flag_grp != NULL);

  // Create the QFP main thread
  err = sns_os_task_create_ext(qfp_sm_init_thread,
                               NULL,
                               &qfp_sm_stack[SNS_MODULE_STK_SIZE_DSPS_QFP-1],
                               SNS_MODULE_PRI_DSPS_SSM,
                               0,
                               &qfp_sm_stack[0],
                               SNS_MODULE_STK_SIZE_DSPS_QFP,
                               (void *)0,
                               OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR | OS_TASK_OPT_ISLAND,
                               (uint8_t *)"SNS_QFP");
  SNS_QFP_PRINTF1(HIGH, "QFP: after creating thread, returned: %d", err);
  return err;
}

