/*=============================================================================
  @file sns_qfp_main.c

  QFP main service

  This file contains the functions required by the QFP main service

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/framework/src/sns_qfp_main.c#4 $  */
/* $DateTime: 2016/06/27 05:38:18 $ */
/* $Author: pwbldsvc $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2016-06-15  ND   Start finger detect immediately after enable request 
  2016-04-15  ND   Add CBGE timer counter to check and trigger CBGE in APSS
  2016-03-22  GJU  Use consolidated SNS_OS_FREE rather than SNS_OS_U_FREE
  2016-02-04  LB   Added separate ffd_alloc function
  2016-02-03  LB   Fix continuous mode island mode voting
  2016-02-02  LB   Separated algorithm memory allocation and initialization
  2016-01-20  ND   Allocate maximum number of BGE basis vectors at initialization
  2015-10-28  ND   Low-power wake-up
  2014-07-05  LD   Initial version

=============================================================================*/


/*-----------------------------------------------------------------------------
* Include Files
* ---------------------------------------------------------------------------*/

#include "sns_common.h"
#include "sns_common_v01.h"
#include "sns_debug_api.h"

#include "sns_em.h"
#include "sns_debug_str.h"
#include "sns_qfp_sm_priv.h"
#include "sns_osa.h"
#include "sns_pm.h"

#include "sns_qfp_priv.h"
#include "sns_qfp_v01.h"
#include "sns_qfp_sd_spi_base.h"
#include "sns_qfp_debug.h"
#include "sns_qfp_ffd_alg.h"
#include "ffd_api.h"

/*-----------------------------------------------------------------------------
* Global variables
* ---------------------------------------------------------------------------*/
// Version numbers
#define SNS_QFP_VERSION_MAJOR 1
#define SNS_QFP_VERSION_MINOR 20

/*-----------------------------------------------------------------------------
* Prototypes
* ---------------------------------------------------------------------------*/
static sns_err_code_e sns_qfp_handle_cancel_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_version_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_open_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_close_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_keep_alive_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_finger_detect_enable_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_finger_detect_disable_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_simple_item_write_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_basis_data_create_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_basis_data_write_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_basis_data_close_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_debug_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_change_polling_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

static sns_err_code_e sns_qfp_handle_basis_norm_vector_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
);

SNS_QFP_UIMAGE_CODE static void sns_qfp_proc_finger_detect_sig(sns_qfp_service* srv,
                                                               OS_FLAGS sig_flags);

static sns_err_code_e sns_qfp_srv_disconnected_cb(void* service_cookie, 
                                           void* connection_handle);

/*=========================================================================
  FUNCTION:  sns_qfp_init
=========================================================================*/
sns_err_code_e sns_qfp_init(sns_qfp_service* srv)
{
  sns_err_code_e status = SNS_SUCCESS;
  uint32_t size;
  sns_qfp_type_s* ctx;

  SNS_QFP_PRINTF0(MED, "QFP main svc init entry");
  srv->service_disconnect_fcn = sns_qfp_srv_disconnected_cb;
  size = sizeof(sns_qfp_type_s);
  srv->data = SNS_OS_U_MALLOC(SNS_DBG_MOD_QFP, size);
  if (srv->data == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "QFP main service fail to allocate memory");
    return SNS_ERR_NOMEM;
  }

  SNS_OS_MEMSET(srv->data, 0, size);
  ctx = (sns_qfp_type_s*)srv->data;
  ctx->state = SNS_QFP_STATE_INIT;

  status = ffd_alg_alloc(ctx);
  if (status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "failed to allocate resources for ffd alg: %d",
                    (int)status);
    return status;
  }

  ctx->island_mode_status = SNS_IMG_MODE_NOCLIENT;

  SNS_QFP_PRINTF1(MED, "QFP main svc init returning %d", (int)status);
  return status;
}

/*=========================================================================
  FUNCTION:  sns_qfp_proc_req
=========================================================================*/
sns_err_code_e sns_qfp_proc_req(
                               sns_qfp_service* srv,
                               sns_qfp_qmi_connection_s *connection_handle,
                               qmi_req_handle req_handle,
                               unsigned int msg_id,
                               void *req_c_struct,
                               unsigned int req_c_struct_len
                               )
{
  sns_err_code_e status = SNS_SUCCESS;

  SNS_QFP_PRINTF1(MED, "QFP process request, message id %d",
                  (int)msg_id);
  switch (msg_id)
  {
  case SNS_QFP_CANCEL_REQ_V01:
    status = sns_qfp_handle_cancel_req(srv,
                                       connection_handle,
                                       req_handle,
                                       req_c_struct,
                                       req_c_struct_len);
    break;
  case SNS_QFP_VERSION_REQ_V01:
    status = sns_qfp_handle_version_req(srv,
                                        connection_handle,
                                        req_handle,
                                        req_c_struct,
                                        req_c_struct_len);
    break;
  case SNS_QFP_OPEN_REQ_V01:
    status = sns_qfp_handle_open_req(srv,
                                     connection_handle,
                                     req_handle,
                                     req_c_struct,
                                     req_c_struct_len);
    break;
  case SNS_QFP_CLOSE_REQ_V01:
    status = sns_qfp_handle_close_req(srv,
                                      connection_handle,
                                      req_handle,
                                      req_c_struct,
                                      req_c_struct_len);
    break;
  case SNS_QFP_KEEP_ALIVE_REQ_V01:
    status = sns_qfp_handle_keep_alive_req(srv,
                                           connection_handle,
                                           req_handle,
                                           req_c_struct,
                                           req_c_struct_len);
    break;
  case SNS_QFP_FINGER_DETECT_ENABLE_REQ_V01:
    status = sns_qfp_handle_finger_detect_enable_req(srv,
                                                     connection_handle, 
                                                     req_handle,
                                                     req_c_struct,
                                                     req_c_struct_len);
    break;
  case SNS_QFP_FINGER_DETECT_DISABLE_REQ_V01:
     status = sns_qfp_handle_finger_detect_disable_req(srv,
                                                       connection_handle, 
                                                       req_handle,
                                                       req_c_struct,
                                                       req_c_struct_len);
     break;
  case SNS_QFP_SIMPLE_ITEM_WRITE_REQ_V01:
    status = sns_qfp_handle_simple_item_write_req(srv,
                                                  connection_handle, 
                                                  req_handle,
                                                  req_c_struct,
                                                  req_c_struct_len);
    break;
  case SNS_QFP_FFD_BASIS_DATA_CREATE_REQ_V01:
    status = sns_qfp_handle_basis_data_create_req(srv,
                                                  connection_handle, 
                                                  req_handle,
                                                  req_c_struct,
                                                  req_c_struct_len);
    break;
  case SNS_QFP_FFD_BASIS_DATA_WRITE_REQ_V01:
    status = sns_qfp_handle_basis_data_write_req(srv,
                                                 connection_handle, 
                                                 req_handle,
                                                 req_c_struct,
                                                 req_c_struct_len);
    break;
  case SNS_QFP_FFD_BASIS_DATA_CLOSE_REQ_V01:
    status = sns_qfp_handle_basis_data_close_req(srv,
                                                 connection_handle, 
                                                 req_handle,
                                                 req_c_struct,
                                                 req_c_struct_len);
    break;
  case SNS_QFP_DEBUG_REQ_V01:
    status = sns_qfp_handle_debug_req(srv,
                                      connection_handle, 
                                      req_handle, 
                                      req_c_struct,
                                      req_c_struct_len);
    break;
  case SNS_QFP_FFD_CHANGE_POLLING_REQ_V01:
    status = sns_qfp_handle_change_polling_req(srv,
                                               connection_handle,
                                               req_handle,
                                               req_c_struct,
                                               req_c_struct_len);
    break;
  case SNS_QFP_FFD_BASIS_NORM_VECTOR_REQ_V01:
    status = sns_qfp_handle_basis_norm_vector_req(srv,
                                                  connection_handle, 
                                                  req_handle, 
                                                  req_c_struct,
                                                  req_c_struct_len);
    break;
  default:
    SNS_QFP_PRINTF1(HIGH, "QFP: bad message id %d", (int)msg_id);
    status = SNS_ERR_BAD_MSG_ID;
    break;
  }

  return status;
}

/*-----------------------------------------------------------------------------
* Private Functions
* ---------------------------------------------------------------------------*/
static ffd_basis_config_t* sns_qfp_get_basis_config(sns_qfp_type_s* ctx, 
                                                    uint32_t item_id)
{
  switch (item_id)
  {
  case SNS_QFP_FFD_ITEM_BASIS_S1_V01:
    return &ctx->ffd_config.basis_s1;
  case SNS_QFP_FFD_ITEM_BASIS_S1_OFF_V01:
    return &ctx->ffd_config.basis_s1_off;
  case SNS_QFP_FFD_ITEM_BASIS_S2_V01:
    return &ctx->ffd_config.basis_s2;
  case SNS_QFP_FFD_ITEM_BASIS_S2_OFF_V01:
    return &ctx->ffd_config.basis_s2_off;
  default:
    return NULL;
  }
}

/*=========================================================================
  FUNCTION:  sns_qfp_notify_polling_isr
=========================================================================*/
/*!
  @brief This function is called when dynamic polling GPIO is fired.
  It then notifies the service of the GPIO change.

  @return sensor1_error_e
*/
static sensor1_error_e sns_qfp_notify_polling_isr()
{
  sensor1_error_e err = SENSOR1_SUCCESS;

  sns_os_sigs_post(sns_qfp.qfp_flag_grp,
                   SNS_QFP_POLLING_RATE_SIG,
                   OS_FLAG_SET,
                   &err);

  if (err != OS_ERR_NONE)
  {
    SNS_QFP_PRINTF1(ERROR,
                    "QFP: Failed to set polling rate signal, err %d",
                    err);
    goto End;
  }

End:
  return err;

}

/*=========================================================================
  FUNCTION:  sns_qfp_finger_detect_timer_cb
=========================================================================*/
/*!
  @brief Callback registered for finger detect expiry

  @return None
*/
SNS_QFP_UIMAGE_CODE SMGR_STATIC void sns_qfp_finger_detect_timer_cb(void *argPtr)
{
  uint8_t err = OS_ERR_NONE;

  SNS_QFP_PRINTF0(LOW, "QFP: sns_qfp_finger_detect_timer_cb called");

  // Post the signal to the working thread

  sns_os_sigs_post(sns_qfp.qfp_flag_grp,
                   SNS_QFP_FINGER_DETECT_SIG,
                   OS_FLAG_SET,
                   &err);

  if (err != OS_ERR_NONE)
  {
    SNS_QFP_PRINTF1(ERROR,
                    "QFP: Failed to set finger detect signal, err %d",
                    err);
  }
}

/*=========================================================================
  FUNCTION:  sns_qfp_register_finger_detect_timer
=========================================================================*/
/*!
  @brief Register timer for finger detection

  @param[i] poll_time: polling time in ms

  @return Sensors error code
*/
sns_err_code_e sns_qfp_register_finger_detect_timer(
  sns_em_timer_obj_t *finger_detect_timer_obj,
  uint32_t poll_time)
{
  sns_err_code_e err = SNS_SUCCESS;
  sns_pm_err_code_e pm_err;

  if (poll_time == 0)
  {
    // Cancel timer
    if (*finger_detect_timer_obj)
    {
      err = sns_em_delete_utimer_obj(*finger_detect_timer_obj);

      if (err == SNS_SUCCESS)
      {
        SNS_QFP_PRINTF0(HIGH, "QFP: Finger detect timer cancelled successfully");
      }
      else
      {
        SNS_QFP_PRINTF1(ERROR,
                        "QFP: Finger detect timer cancelled unsuccessfully, err = %d",
                        err);
      }
      *finger_detect_timer_obj = NULL;

      // Cancel wakeup interval
      pm_err = sns_pm_set_wakeup_interval(sns_qfp_pm_handle, 0);
      if (pm_err != SNS_PM_SUCCESS)
      {
        SNS_QFP_PRINTF1(ERROR,
                        "QFP: Failed to cancel wake-up delay interval, err = %d",
                        pm_err);
      }
    }
    else
    {
      SNS_QFP_PRINTF0(HIGH, "QFP: Finger detect timer object was NULL, not cancelled");
    }
  }
  else if (poll_time > 0)
  {
    // Setup wakeup interval
    SNS_QFP_PRINTF1(LOW,
                    "QFP: Setup wake-up delay interval to %d",
                    poll_time * 1000);
    pm_err = sns_pm_set_wakeup_interval(sns_qfp_pm_handle, poll_time * 1000);

    if (pm_err != SNS_PM_SUCCESS)
    {
      SNS_QFP_PRINTF2(ERROR,
                      "QFP: Failed to setup wake-up delay interval to %d, err = %d",
                      poll_time * 1000,
                      pm_err);
    }

    SNS_QFP_PRINTF0(HIGH, "QFP: Create the finger detect timer object");
  
    // Create the finger detect timer object
    err = sns_em_create_utimer_obj(sns_qfp_finger_detect_timer_cb,
                                   NULL,
                                   SNS_EM_TIMER_TYPE_PERIODIC,
                                   finger_detect_timer_obj);
  
    // Register the finger detect timer object
    if (err == SNS_SUCCESS)
    {
      SNS_QFP_PRINTF2(HIGH,
                      "QFP: Register the finger detect timer with poll_time = %d, localtick = %d",
                      poll_time,
                      sns_em_convert_usec_to_localtick(poll_time * 1000));
      err = sns_em_register_utimer(*finger_detect_timer_obj,
                                   sns_em_convert_usec_to_localtick(poll_time * 1000));
      if (err == SNS_SUCCESS)
      {
        SNS_QFP_PRINTF0(HIGH, "QFP: Finger detect timer registered successfully");
      }
      else
      {
        SNS_QFP_PRINTF1(ERROR,
                        "QFP: Finger detect timer registered unsuccessfully, err = %d",
                        err);
      }
    }
    else
    {
      SNS_QFP_PRINTF1(ERROR,
                      "QFP: Failed to create finger detect timer object, err = %d",
                      err);
    }
  }

  return err;
}

/*=========================================================================
  FUNCTION:  sns_qfp_send_finger_state_ind
  =========================================================================*/
/*!
  @brief Send indication to registered client about the current finger
  detection state.
  
  @param[in] connection client connection
  @param[in] detected if true finger was detected. This flag will be sent
             together with indication.  
 
  @return Sensors error code
*/
/*=======================================================================*/
static sns_err_code_e sns_qfp_send_finger_state_ind(
  sns_qfp_qmi_connection_s* connection,
  bool detected
)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_finger_detect_ind_msg_v01 ind_msg;
  qmi_csi_error qmi_err;
  
  if (connection == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "QFP send finger state ind: no client");
    status = SNS_ERR_NOTALLOWED;
    goto End;
  }

  if (!connection->connection_valid)
  {
    SNS_QFP_PRINTF0(ERROR, "QFP send finger state ind: invalid client connection");
    status = SNS_ERR_NOTALLOWED;
    goto End;
  }

  ind_msg.flags = detected ? SNS_QFP_FINGER_DETECT_IND_FLAG_DETECTED_V01 : 0;
    
  ind_msg.timestamp = sns_em_get_timestamp();
  qmi_err = qmi_csi_send_ind(connection->client_handle,
                             SNS_QFP_FINGER_DETECT_IND_V01,
                             &ind_msg,
                             sizeof(ind_msg));

  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP send finger state ind: error %d sending QMI indication",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
    goto End;
  }

End:
  return status;
}

/*=========================================================================
  FUNCTION:  sns_qfp_disable_finger_detect
  =========================================================================*/
/*!
  @brief Disable the finger detection sensor. Release resources,
  update state machine and send indication to registered client.
  
  @param[in] srv service context
  @param[in] detected if true finger was detected. This flag will be sent
             together with indication.
 
  @note detected flag will be false if sensor is disabled/cancelled before
        any detection. This indication will always be sent one time, except
        when HLOS client forcibly disconnects from the service.

  @return Sensors error code
*/
SNS_QFP_UIMAGE_CODE static sns_err_code_e sns_qfp_disable_finger_detect(
  sns_qfp_service* srv,
  bool detected
)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_type_s* ctx = NULL;
  sns_qfp_qmi_connection_s* connection_handle;

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "QFP send ind: bad srv ptr");
    status = SNS_ERR_BAD_PTR;
    goto End;
  }

  ctx = (sns_qfp_type_s*)srv->data;
  
  if (ctx->state != SNS_QFP_STATE_RUNNING)
  {
    SNS_QFP_PRINTF0(ERROR, "QFP send ind: finger detection disabled");
    status = SNS_ERR_NOTALLOWED;
    goto End;
  }

  connection_handle = (sns_qfp_qmi_connection_s*)ctx->finger_detect_connection_handle;
  (void)sns_qfp_register_finger_detect_timer(&ctx->finger_detect_timer_obj, 0);
  ctx->state = SNS_QFP_STATE_IDLE;
  ctx->finger_detect_connection_handle = NULL;
  ffd_alg_deinit(ctx);

  status = sns_qfp_send_finger_state_ind(connection_handle, detected);
  if (status != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP send ind: error %d sending finger state indication",
                    (int)status);
    goto End;
  }

End:
  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_cancel_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_cancel_req(
                                        sns_qfp_service* srv,
                                        sns_qfp_qmi_connection_s *connection_handle,
                                        qmi_req_handle req_handle,
                                        void *req_c_struct,
                                        unsigned int req_c_struct_len
                                        )
{
  sns_err_code_e status = SNS_SUCCESS;
  qmi_csi_error qmi_err;
  sns_common_cancel_resp_msg_v01 resp_msg;

  // nothing to be done for now
  resp_msg.resp.sns_result_t = 0;
  resp_msg.resp.sns_err_t = SENSOR1_SUCCESS;
  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_CANCEL_RESP_V01,
                              &resp_msg,
                              sizeof(sns_common_cancel_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP Cancel: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_version_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_version_req(
                                         sns_qfp_service* srv,
                                         sns_qfp_qmi_connection_s *connection_handle,
                                         qmi_req_handle req_handle,
                                         void *req_c_struct,
                                         unsigned int req_c_struct_len
                                         )
{
  sns_err_code_e status = SNS_SUCCESS;
  qmi_csi_error qmi_err;
  sns_common_version_resp_msg_v01 resp_msg;

  resp_msg.resp.sns_result_t = 0;
  resp_msg.resp.sns_err_t = SENSOR1_SUCCESS;
  resp_msg.interface_version_number = SNS_QFP_SVC_V01_IDL_MINOR_VERS;
  resp_msg.max_message_id = SNS_QFP_SVC_V01_MAX_MESSAGE_ID;
  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_VERSION_RESP_V01,
                              &resp_msg,
                              sizeof(sns_common_version_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP Version: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_open_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_open_req(
                                   sns_qfp_service* srv,
                                   sns_qfp_qmi_connection_s *connection_handle,
                                   qmi_req_handle req_handle,
                                   void *req_c_struct,
                                   unsigned int req_c_struct_len
)
{
  sns_err_code_e status = SNS_SUCCESS, tmp_status;
  sensor1_error_e err = SENSOR1_SUCCESS;
  sns_qfp_type_s* ctx = NULL;
  qmi_csi_error qmi_err;
  sns_qfp_open_req_msg_v01* req_ptr;
  sns_qfp_open_resp_msg_v01 resp_msg;
  spi_device_id_t device_id;

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "QFP Open: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }
  if (req_c_struct == NULL ||
      req_c_struct_len < sizeof(sns_qfp_open_req_msg_v01))
  {
    SNS_QFP_PRINTF0(ERROR, "QFP Open: bad req_c_struct");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  ctx = (sns_qfp_type_s*)srv->data;

  if (ctx->state == SNS_QFP_STATE_RUNNING)
  {
    (void)sns_qfp_disable_finger_detect(srv, false);
  }

  if ((ctx->state != SNS_QFP_STATE_INIT) && (ctx->state != SNS_QFP_STATE_IDLE))
  {
    SNS_QFP_PRINTF1(ERROR, "QFP Open: bad state %d", (int)ctx->state);
    err = SENSOR1_ENOTALLOWED;
    goto End;
  }

  if (ctx->spi_handle == NULL)
  {
    tmp_status = sns_qfp_sd_spi_alloc_handle(&ctx->spi_handle);
    if (tmp_status != SNS_SUCCESS)
    {
      SNS_QFP_PRINTF0(ERROR, "QFP Open: fail to allocate spi_handle");
      err = SENSOR1_ENOMEM;
      goto End;
    }
  }

  req_ptr = (sns_qfp_open_req_msg_v01*)req_c_struct;
  // caller specifies SPI port as 1=SPI_1, 2=SPI_2, ...
  device_id = (spi_device_id_t)(req_ptr->port_id - 1);
  tmp_status = sns_qfp_sd_spi_init(ctx->spi_handle,
                                   device_id,
                                   req_ptr->slave_index,
                                   req_ptr->freq);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP Open: fail to init SPI, error %d",
                    (int)tmp_status);
    err = SENSOR1_EFAILED;
    goto End;
  }

  if (ctx->rid_handle == NULL)
  {
    tmp_status = sns_qfp_sd_rid_alloc_handle(&ctx->rid_handle);
    if (tmp_status != SNS_SUCCESS)
    {
      SNS_QFP_PRINTF1(ERROR, "QFP Open: fail to allocate rid handle, error %d",
                      (int)tmp_status);
      err = SENSOR1_EFAILED;
      goto End;
    }
  }

  // the open request both initializes the SPI device and marks it as owned
  // by external subsystem. SLPI cannot use the device until a close request
  // is received
  ctx->state = SNS_QFP_STATE_BUSY;

  // Print version number
  SNS_QFP_PRINTF2(HIGH,
                  "fingerprint: ffd: version <%02d.%03d>",
                  SNS_QFP_VERSION_MAJOR,
                  SNS_QFP_VERSION_MINOR);

End:
  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to open request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_OPEN_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_open_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP Open: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_close_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_close_req(
                                    sns_qfp_service* srv,
                                    sns_qfp_qmi_connection_s *connection_handle,
                                    qmi_req_handle req_handle,
                                    void *req_c_struct,
                                    unsigned int req_c_struct_len
)
{
  sns_err_code_e status = SNS_SUCCESS;
  sensor1_error_e err = SENSOR1_SUCCESS;
  sns_qfp_type_s* ctx = NULL;
  qmi_csi_error qmi_err;
  sns_qfp_close_resp_msg_v01 resp_msg;

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "QFP Close: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  ctx = (sns_qfp_type_s*)srv->data;
  if (ctx->state != SNS_QFP_STATE_BUSY)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP Close: bad state %d", (int)ctx->state);
    err = SENSOR1_ENOTALLOWED;
    goto End;
  }

  // SPI device is now owned by SLPI
  ctx->state = SNS_QFP_STATE_IDLE;
End:
  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to close request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_CLOSE_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_close_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP Close: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_keep_alive_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_keep_alive_req(
                                            sns_qfp_service* srv,
                                            sns_qfp_qmi_connection_s *connection_handle,
                                            qmi_req_handle req_handle,
                                            void *req_c_struct,
                                            unsigned int req_c_struct_len
                                            )
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_pm_err_code_e pm_err;
  sns_qfp_type_s* ctx = NULL;
  sensor1_error_e err = SENSOR1_SUCCESS;
  qmi_csi_error qmi_err;
  sns_qfp_keep_alive_req_msg_v01* req_ptr;
  sns_qfp_keep_alive_resp_msg_v01 resp_msg;

  SNS_QFP_PRINTF0(LOW, "sns_qfp_handle_keep_alive_req: QFP Keep Alive: enter");

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "QFP Keep Alive: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }
  if (req_c_struct == NULL ||
      req_c_struct_len < sizeof(sns_qfp_keep_alive_req_msg_v01))
  {
    SNS_QFP_PRINTF0(ERROR, "QFP Keep Alive: bad req_c_struct");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  ctx = (sns_qfp_type_s*)srv->data;
  if (ctx->state == SNS_QFP_STATE_INIT)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP Keep Alive: bad state %d", (int)ctx->state);
    err = SENSOR1_ENOTALLOWED;
    goto End;
  }

  req_ptr = (sns_qfp_keep_alive_req_msg_v01*)req_c_struct;

  ctx->keep_alive_enabled = req_ptr->enable;
  if (req_ptr->enable)
  {
    SNS_QFP_PRINTF0(HIGH, "sns_qfp_handle_keep_alive_req: enable keep alive");
    // keep alive: vote against sleep
    pm_err = sns_pm_set_latency(sns_qfp_pm_handle, 1);
    if (pm_err != SNS_PM_SUCCESS)
    {
      SNS_QFP_PRINTF1(ERROR, "QFP Keep Alive: set latency error %d",
                      (int)pm_err);
      err = SENSOR1_EFAILED;
      goto End;
    }
  }
  else
  {
    SNS_QFP_PRINTF0(HIGH, "sns_qfp_handle_keep_alive_req: cancel keep alive");
    // cancel keep alive
    pm_err = sns_pm_set_latency(sns_qfp_pm_handle, 0);
    if (pm_err != SNS_PM_SUCCESS)
    {
      SNS_QFP_PRINTF1(ERROR, "QFP Keep Alive: set latency error %d",
                      (int)pm_err);
      err = SENSOR1_EFAILED;
      goto End;
    }
  }

  End:
  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to keep alive request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_KEEP_ALIVE_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_keep_alive_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP Keep Alive: error %d sending QMI response ",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_finger_detect_enable_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_finger_detect_enable_req(
                                               sns_qfp_service* srv,
                                               sns_qfp_qmi_connection_s *connection_handle,
                                               qmi_req_handle req_handle,
                                               void *req_c_struct,
                                               unsigned int req_c_struct_len
                                               )
{
  sns_err_code_e status = SNS_SUCCESS, tmp_status;
  sns_qfp_type_s* ctx = NULL;
  sensor1_error_e err = SENSOR1_SUCCESS;
  qmi_csi_error qmi_err;
  sns_qfp_finger_detect_enable_req_msg_v01* req_ptr;
  sns_qfp_finger_detect_enable_resp_msg_v01 resp_msg;
  bool ffd_alg_initialized = false;

  SNS_QFP_PRINTF0(HIGH, "sns_qfp_handle_finger_detect_enable_req: QFP finger detect: enter");

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "QFP finger detect: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }
  if (req_c_struct == NULL ||
      req_c_struct_len < sizeof(sns_qfp_finger_detect_enable_req_msg_v01))
  {
    SNS_QFP_PRINTF0(ERROR, "QFP finger detect: bad req_c_struct");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  ctx = (sns_qfp_type_s*)srv->data;
  if (ctx->state != SNS_QFP_STATE_IDLE)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP finger detect: bad state %d", (int)ctx->state);
    err = SENSOR1_ENOTALLOWED;
    goto End;
  }

  req_ptr = (sns_qfp_finger_detect_enable_req_msg_v01 *) req_c_struct;

  if (req_ptr->poll_time == 0)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP finger detect: invalid poll time %d", 
                    (int)req_ptr->poll_time);
    err = SENSOR1_EBAD_PARAM;
    goto End;
  }

  if (req_ptr->finger_detect_mode_valid
      && (req_ptr->finger_detect_mode != SNS_QFP_FINGER_DETECT_MODE_ONE_SHOT_V01)
      && (req_ptr->finger_detect_mode != SNS_QFP_FINGER_DETECT_MODE_CONTINUOUS_V01))
  {
    SNS_QFP_PRINTF1(ERROR, "QFP finger detect: invalid finger detect mode %d", 
                    (int)req_ptr->finger_detect_mode);
    err = SENSOR1_EBAD_PARAM;
    goto End;
  }

  // Set s1_disabled flag
  if (req_ptr->finger_detect_mode == SNS_QFP_FINGER_DETECT_MODE_ONE_SHOT_V01)
  {
    ctx->ffd_config.s1_disabled = 0;
  }
  else if (req_ptr->finger_detect_mode != SNS_QFP_FINGER_DETECT_MODE_CONTINUOUS_V01)
  {
    ctx->ffd_config.s1_disabled = 1;
  }

  // initialize ffd
  tmp_status = ffd_alg_init(ctx);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "failed to initialize ffd alg: %d",
                    (int)tmp_status);
    err = SENSOR1_EFAILED;
    goto End;
  }
  ffd_alg_initialized = true;
  // Create and register the finger detect timer
  SNS_QFP_PRINTF1(HIGH,
                  "QFP finger detect: Going to create & register, or cancel the finger detect timer, poll_time = %d",
                  (int) req_ptr->poll_time);
  tmp_status = sns_qfp_register_finger_detect_timer(&(ctx->finger_detect_timer_obj),
                                                req_ptr->poll_time);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP finger detect: failed to register timer: %d", 
                    (int)tmp_status);
    err = SENSOR1_EFAILED;
    goto End;
  }

  ctx->state = SNS_QFP_STATE_RUNNING;
  ctx->finger_detect_result_reported = false;
  ctx->last_finger_detect_result = false;  
  ctx->current_poll_state = 1;
  ctx->finger_detect_poll_time = req_ptr->poll_time;

  ctx->finger_detect_low_poll_time = 1000;
  if (req_ptr->low_poll_time_valid) 
  {
      ctx->finger_detect_low_poll_time = req_ptr->low_poll_time;
  }

  ctx->finger_detect_mode = SNS_QFP_FINGER_DETECT_MODE_ONE_SHOT_V01;
  if (req_ptr->finger_detect_mode_valid)
  {
    ctx->finger_detect_mode = req_ptr->finger_detect_mode;
  }
  
  ctx->finger_detect_connection_handle = connection_handle;

  // Reset the CBGE timer counter
  ctx->cbge_timer_counter = ctx->cbge_timer;

  // Immediately post the signal to the working thread
  sns_os_sigs_post(sns_qfp.qfp_flag_grp,
                   SNS_QFP_FINGER_DETECT_SIG,
                   OS_FLAG_SET,
                   &tmp_status);

  if (tmp_status != SENSOR1_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR,
                    "QFP finger detect: failed to set finger detect signal, tmp_status %d",
                    tmp_status);
    err = SENSOR1_EFAILED;
    goto End;
  }

End:
  if (err != SENSOR1_SUCCESS)
  {
    if (ffd_alg_initialized)
    {
      ffd_alg_deinit(ctx);
    }
  }
  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to finger detect request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_FINGER_DETECT_ENABLE_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_finger_detect_enable_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP finger detect: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_finger_detect_disable_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_finger_detect_disable_req(
                                               sns_qfp_service* srv,
                                               sns_qfp_qmi_connection_s *connection_handle,
                                               qmi_req_handle req_handle,
                                               void *req_c_struct,
                                               unsigned int req_c_struct_len
                                               )
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_type_s* ctx = NULL;
  sensor1_error_e err = SENSOR1_SUCCESS;
  qmi_csi_error qmi_err;
  sns_qfp_finger_detect_disable_resp_msg_v01 resp_msg;

  SNS_QFP_PRINTF0(HIGH, "sns_qfp_handle_finger_detect_disable_req: QFP finger detect: enter");

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "QFP finger detect: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  ctx = (sns_qfp_type_s*)srv->data;

  if (ctx->state != SNS_QFP_STATE_RUNNING) 
  {
    SNS_QFP_PRINTF0(HIGH, "QFP finger detect: already disabled");
    goto End;
  }

  if (ctx->finger_detect_connection_handle != connection_handle)
  {
    SNS_QFP_PRINTF2(ERROR, 
      "disable request sent by different client(0x%p) enabled by: 0x%p",
      connection_handle, ctx->finger_detect_connection_handle);
    err = SENSOR1_ENOTALLOWED;
    goto End;
  }

  (void)sns_qfp_disable_finger_detect(srv, false);

End:
  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to finger detect disable request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_FINGER_DETECT_DISABLE_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_finger_detect_disable_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP finger detect: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_simple_item_write_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_simple_item_write_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_type_s* ctx = NULL;
  sensor1_error_e err = SENSOR1_SUCCESS;
  qmi_csi_error qmi_err;
  sns_qfp_simple_item_write_req_msg_v01* req_ptr;
  sns_qfp_simple_item_write_resp_msg_v01 resp_msg;

  SNS_QFP_PRINTF0(HIGH, "handle_simple_item_write_req: enter");

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "QFP finger detect: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }
  if (req_c_struct == NULL ||
      req_c_struct_len < sizeof(sns_qfp_simple_item_write_req_msg_v01))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_simple_item_write_req: bad req_c_struct");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  ctx = (sns_qfp_type_s*)srv->data;
  if (ctx->state == SNS_QFP_STATE_RUNNING)
  {
    SNS_QFP_PRINTF0(ERROR, "can't change configuration items while running");
    err = SENSOR1_ENOTALLOWED;
    goto End;
  }

  req_ptr = (sns_qfp_simple_item_write_req_msg_v01 *) req_c_struct;
  // all items has 32 bit value for now (int or float)
  if (req_ptr->data_len < 4)
  {
    SNS_QFP_PRINTF1(ERROR, "data too short (%d, need 4)", 
                    (int)req_ptr->data_len);
    err = SENSOR1_EBAD_PARAM;
    goto End;
  }

  switch (req_ptr->item_id)
  {
  case SNS_QFP_FFD_ITEM_THRESHOLD_S1_V01:
    ctx->ffd_config.threshold_s1 = *(float32_t*)(&req_ptr->data[0]);
    break;
  case SNS_QFP_FFD_ITEM_THRESHOLD_S1_OFF_V01:
    ctx->ffd_config.threshold_s1_off = *(float32_t*)(&req_ptr->data[0]);
    break;
  case SNS_QFP_FFD_ITEM_THRESHOLD_S2_V01:
    ctx->ffd_config.threshold_s2 = *(float32_t*)(&req_ptr->data[0]);
    break;
  case SNS_QFP_FFD_ITEM_THRESHOLD_S2_OFF_V01:
    ctx->ffd_config.threshold_s2_off = *(float32_t*)(&req_ptr->data[0]);
    break;
  case SNS_QFP_FFD_ITEM_NUM_IMAGE_RUNS_S1_V01:
    ctx->ffd_config.num_image_runs_s1 = *(uint32_t*)(&req_ptr->data[0]);
    break;
  case SNS_QFP_FFD_ITEM_THRESHOLD_S1_RUBBING_V01:
    ctx->ffd_config.threshold_s1_rubbing = 0.0;
    // ndagan interm workaround: use s1 rubbing unused parameter to contorl the CBGE timer
    // ndagan TODO: should be handled correctly
    ctx->cbge_timer = *(float32_t*)(&req_ptr->data[0]);
    break;
  case SNS_QFP_FFD_ITEM_THRESHOLD_S2_HOME_TOUCH_V01:
    ctx->ffd_config.threshold_s2_home_touch = *(float32_t*)(&req_ptr->data[0]);
    break;
  case SNS_QFP_FFD_ITEM_THRESHOLD_S2_HOME_LIFT_V01:
    ctx->ffd_config.threshold_s2_home_lift = *(float32_t*)(&req_ptr->data[0]);
    break;
  case SNS_QFP_FFD_ITEM_FGOFF_DILUATION_S1_V01:
    ctx->ffd_config.fgoff_dilution_s1 = *(uint32_t*)(&req_ptr->data[0]);
    break;
  case SNS_QFP_FFD_ITEM_ENABLE_ISLAND_MODE_V01:
    ctx->ffd_config.enable_island_mode = *(uint32_t*)(&req_ptr->data[0]);
    break;
  case SNS_QFP_FFD_ITEM_S1_DISABLE_V01:
    ctx->ffd_config.s1_disabled = *(uint32_t*)(&req_ptr->data[0]);
    break;
  case SNS_QFP_FFD_ITEM_FGOFF_DILUATION_HOME_V01:
    ctx->ffd_config.fgoff_dilution_home = *(uint32_t*)(&req_ptr->data[0]);
    break;
  default:
    SNS_QFP_PRINTF1(ERROR, "handle_simple_item_write: invalid item id %d",
                    (int)req_ptr->item_id);
    err = SENSOR1_EBAD_PARAM;
    goto End;
  }

End:
  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to simple item write request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_SIMPLE_ITEM_WRITE_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_simple_item_write_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "QFP finger detect: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_basis_data_create_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_basis_data_create_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_type_s* ctx = NULL;
  sensor1_error_e err = SENSOR1_SUCCESS;
  qmi_csi_error qmi_err;
  sns_qfp_ffd_basis_data_create_req_msg_v01* req_ptr;
  sns_qfp_ffd_basis_data_create_resp_msg_v01 resp_msg;
  ffd_basis_config_t* bconfig;
  uint32_t size;
  uint32_t malloc_size;

  SNS_QFP_PRINTF0(HIGH, "handle_basis_data_create_req: enter");

  resp_msg.handle_valid = false;

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_basis_data_create: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }
  if (req_c_struct == NULL ||
      req_c_struct_len < sizeof(sns_qfp_ffd_basis_data_create_req_msg_v01))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_basis_data_create: bad req_c_struct");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  ctx = (sns_qfp_type_s*)srv->data;
  if (ctx->state == SNS_QFP_STATE_RUNNING)
  {
    SNS_QFP_PRINTF0(ERROR, "can't upload basis data while running");
    err = SENSOR1_ENOTALLOWED;
    goto End;
  }
  if (ctx->upload_state.data != NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "another basis file upload is in progress");
    err = SENSOR1_ENOTALLOWED;
    goto End;
  }

  req_ptr = (sns_qfp_ffd_basis_data_create_req_msg_v01 *) req_c_struct;
  bconfig = sns_qfp_get_basis_config(ctx, req_ptr->item_id);
  if (bconfig == NULL)
  {
    SNS_QFP_PRINTF1(ERROR, "invalid item id: %d", (int)req_ptr->item_id);
    err = SENSOR1_EBAD_PARAM;
    goto End;
  }

  if (bconfig->data != NULL)
  {
    SNS_OS_FREE(bconfig->data);
    bconfig->data = NULL;
  }
  bconfig->num_vectors = req_ptr->num_vectors;
  bconfig->vector_size = req_ptr->vector_size;
  size = bconfig->num_vectors * bconfig->vector_size * sizeof(ffd_algo_basis_data_t);
  malloc_size = SNS_QFP_MAX_BG_NUM_VECTORS * bconfig->vector_size * sizeof(ffd_algo_basis_data_t);

  SNS_QFP_PRINTF3(HIGH,
                  "request to allocate %d vectors each of size %d (total %d) for basis data", 
                  (int)SNS_QFP_MAX_BG_NUM_VECTORS,
                  (int)bconfig->vector_size,
                  malloc_size);

  ctx->upload_state.data = (uint8_t *) SNS_OS_U_MALLOC(SNS_DBG_MOD_QFP, malloc_size);
  if (ctx->upload_state.data == NULL)
  {
    SNS_QFP_PRINTF1(ERROR, "failed to allocate %d bytes for basis data", 
                    (int) malloc_size);
    err = SENSOR1_ENOMEM;
    goto End;
  }
  ctx->upload_state.item_id = req_ptr->item_id;
  ctx->upload_state.size = size;
  ctx->upload_state.uploaded = 0;
  resp_msg.handle = 0;
  resp_msg.handle_valid = true;

End:
  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to basis data create request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_FFD_BASIS_DATA_CREATE_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_ffd_basis_data_create_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "handle_basis_data_create: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_basis_data_write_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_basis_data_write_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_type_s* ctx = NULL;
  sensor1_error_e err = SENSOR1_SUCCESS;
  qmi_csi_error qmi_err;
  sns_qfp_ffd_basis_data_write_req_msg_v01* req_ptr;
  sns_qfp_ffd_basis_data_write_resp_msg_v01 resp_msg;

  SNS_QFP_PRINTF0(HIGH, "handle_basis_data_write_req: enter");

  resp_msg.written_valid = false;

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_basis_data_write: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }
  if (req_c_struct == NULL ||
      req_c_struct_len < sizeof(sns_qfp_ffd_basis_data_write_req_msg_v01))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_basis_data_write: bad req_c_struct");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  ctx = (sns_qfp_type_s*)srv->data;
  if (ctx->upload_state.data == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "no basis upload in progress");
    err = SENSOR1_ENOTALLOWED;
    goto End;
  }

  req_ptr = (sns_qfp_ffd_basis_data_write_req_msg_v01 *) req_c_struct;
  if (req_ptr->handle != 0)
  {
    // currently we only support a single handle
    SNS_QFP_PRINTF0(ERROR, "handle_basis_data_write: invalid handle");
    err = SENSOR1_EBAD_PARAM;
    goto End;
  }
  resp_msg.written = SNS_OS_MEMSCPY(
     ctx->upload_state.data + ctx->upload_state.uploaded,
     ctx->upload_state.size - ctx->upload_state.uploaded,
     &req_ptr->data, 
     req_ptr->data_len);
  resp_msg.written_valid = true;

  ctx->upload_state.uploaded += resp_msg.written;

End:
  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to basis data write request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_FFD_BASIS_DATA_WRITE_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_ffd_basis_data_write_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "handle_basis_data_write: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_basis_data_close_req
===========================================================================*/
sns_err_code_e sns_qfp_handle_basis_data_close_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_type_s* ctx = NULL;
  sensor1_error_e err = SENSOR1_SUCCESS;
  qmi_csi_error qmi_err;
  sns_qfp_ffd_basis_data_close_req_msg_v01* req_ptr;
  sns_qfp_ffd_basis_data_close_resp_msg_v01 resp_msg;
  ffd_basis_config_t* bconfig;
  uint8_t* data = NULL;

  SNS_QFP_PRINTF0(HIGH, "handle_basis_data_close_req: enter");

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_basis_data_close: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }
  if (req_c_struct == NULL ||
      req_c_struct_len < sizeof(sns_qfp_ffd_basis_data_close_req_msg_v01))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_basis_data_close: bad req_c_struct");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  req_ptr = (sns_qfp_ffd_basis_data_close_req_msg_v01 *) req_c_struct;
  if (req_ptr->handle != 0)
  {
    // currently we only support a single handle
    SNS_QFP_PRINTF0(ERROR, "handle_basis_data_close: invalid handle");
    err = SENSOR1_EBAD_PARAM;
    goto End;
  }

  ctx = (sns_qfp_type_s *) srv->data;
  data = ctx->upload_state.data;
  if (data == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "no basis upload in progress");
    err = SENSOR1_ENOTALLOWED;
    goto End;
  }

  ctx->upload_state.data = NULL;
  bconfig = sns_qfp_get_basis_config(ctx, ctx->upload_state.item_id);
  if (bconfig == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "handle_basis_data_close: can't get basis config, unexpected");
    err = SENSOR1_EFAILED;
    goto End;
  }
  if (ctx->upload_state.size > ctx->upload_state.uploaded)
  {
    SNS_QFP_PRINTF2(ERROR, 
       "handle_basis_data_close: upload not complete. Uploaded %d need %d",
       (int)ctx->upload_state.uploaded, (int)ctx->upload_state.size);
    err = SENSOR1_EFAILED;
    goto End;
  }

  bconfig->data = (ffd_algo_basis_data_t *) data;
  data = NULL;

End:
  if (data != NULL)
  {
    SNS_OS_FREE(data);
    data = NULL;
  }
  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to basis data close request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_FFD_BASIS_DATA_CLOSE_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_ffd_basis_data_close_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "handle_basis_data_close: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_change_polling_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_change_polling_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_type_s* ctx = NULL;
  sensor1_error_e err = SENSOR1_SUCCESS;
  qmi_csi_error qmi_err;
  sns_qfp_ffd_change_polling_req_msg_v01* req_ptr;
  sns_qfp_ffd_change_polling_resp_msg_v01 resp_msg;

  SNS_QFP_PRINTF0(HIGH, "handle_change_polling_req: enter");

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_change_polling_req: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }
  if (req_c_struct == NULL ||
      req_c_struct_len < sizeof(sns_qfp_ffd_change_polling_req_msg_v01))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_change_polling_req: bad req_c_struct");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  req_ptr = (sns_qfp_ffd_change_polling_req_msg_v01 *) req_c_struct;
  ctx = (sns_qfp_type_s*)srv->data;

  if (ctx->current_poll_state != req_ptr->state) 
  {
    SNS_QFP_PRINTF1(MED, "handle_change_polling_req: got new state %d", req_ptr->state);
    ctx->current_poll_state = req_ptr->state;
    err = sns_qfp_notify_polling_isr();
  } 
  else 
  {
    SNS_QFP_PRINTF1(MED, "handle_change_polling_req: Ignoring request to change to the same state %d", req_ptr->state);
  }
  
End:
  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to change polling request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_FFD_CHANGE_POLLING_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_ffd_change_polling_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "handle_change_polling_req: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_debug_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_debug_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_type_s* ctx = NULL;
  sensor1_error_e err = SENSOR1_SUCCESS;
  qmi_csi_error qmi_err;
  sns_qfp_debug_req_msg_v01* req_ptr;
  sns_qfp_debug_resp_msg_v01 resp_msg;

  SNS_QFP_PRINTF0(HIGH, "handle_debug_req: enter");

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_debug: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }
  if (req_c_struct == NULL ||
      req_c_struct_len < sizeof(sns_qfp_debug_req_msg_v01))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_debug: bad req_c_struct");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  req_ptr = (sns_qfp_debug_req_msg_v01 *) req_c_struct;
  ctx = (sns_qfp_type_s*)srv->data;

  err = sns_qfp_process_debug_msg(ctx, req_ptr, &resp_msg);
End:
  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to debug req request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_DEBUG_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_debug_resp_msg_v01));
  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR, "handle_debug: error %d sending QMI response",
                    (int)qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_handle_basis_norm_vector_req
===========================================================================*/
static sns_err_code_e sns_qfp_handle_basis_norm_vector_req(
  sns_qfp_service* srv,
  sns_qfp_qmi_connection_s *connection_handle,
  qmi_req_handle req_handle,
  void *req_c_struct,
  unsigned int req_c_struct_len
)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_type_s* ctx = NULL;
  sensor1_error_e err = SENSOR1_SUCCESS;
  qmi_csi_error qmi_err;
  sns_qfp_ffd_basis_norm_vector_req_msg_v01* req_ptr;
  sns_qfp_ffd_basis_norm_vector_resp_msg_v01 resp_msg;
  ffd_basis_config_t* bconfig;
  uint32_t size;
  uint32_t malloc_size;

  SNS_QFP_PRINTF0(HIGH, "handle_basis_norm_vector_req: enter");

  if ((srv == NULL) || (srv->data == NULL))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_basis_norm_vector_req: bad srv ptr");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }
  if (req_c_struct == NULL ||
      req_c_struct_len < sizeof(sns_qfp_ffd_basis_norm_vector_req_msg_v01))
  {
    SNS_QFP_PRINTF0(ERROR, "handle_basis_norm_vector_req: bad req_c_struct");
    err = SENSOR1_EBAD_PTR;
    goto End;
  }

  ctx = (sns_qfp_type_s *) srv->data;

  req_ptr = (sns_qfp_ffd_basis_norm_vector_req_msg_v01 *) req_c_struct;
  bconfig = sns_qfp_get_basis_config(ctx, req_ptr->item_id);
  if (bconfig == NULL)
  {
    SNS_QFP_PRINTF1(ERROR, "invalid item id: %d", (int)req_ptr->item_id);
    err = SENSOR1_EBAD_PARAM;
    goto End;
  }

  if (bconfig->norm_vector != NULL)
  {
    SNS_OS_FREE(bconfig->norm_vector);
    bconfig->norm_vector = NULL;
  }

  size = bconfig->num_vectors * sizeof(norm_str);
  malloc_size = SNS_QFP_MAX_BG_NUM_VECTORS * sizeof(norm_str);
  SNS_QFP_PRINTF2(LOW,
                  "request to allocate %d vectors (total %d) for basis norm vector", 
                  (int) SNS_QFP_MAX_BG_NUM_VECTORS,
                  malloc_size);

  bconfig->norm_vector = (norm_str *) SNS_OS_U_MALLOC(SNS_DBG_MOD_QFP, malloc_size);
  
  if (bconfig->norm_vector == NULL)
  {
    SNS_QFP_PRINTF1(ERROR,
                    "failed to allocate %d bytes for basis norm vector", 
                    (int) malloc_size);
    err = SENSOR1_ENOMEM;
    goto End;
  }
  else
  {
    SNS_QFP_PRINTF1(LOW,
                    "copy BG-E normalization vector %d bytes",
                    (int) req_ptr->data_len);

    SNS_OS_MEMSCPY(bconfig->norm_vector,
                   size,
                   &req_ptr->data, 
                   req_ptr->data_len);
  }

End:

  resp_msg.resp.sns_result_t = (err != SENSOR1_SUCCESS) ? 1 : 0;
  resp_msg.resp.sns_err_t = err;
  SNS_QFP_PRINTF0(HIGH, "QFP: Responding to basis norm vector request");

  qmi_err = qmi_csi_send_resp(req_handle,
                              SNS_QFP_FFD_BASIS_NORM_VECTOR_RESP_V01,
                              &resp_msg,
                              sizeof(sns_qfp_ffd_basis_norm_vector_resp_msg_v01));

  if (qmi_err != QMI_CSI_NO_ERR)
  {
    SNS_QFP_PRINTF1(ERROR,
                    "handle_basis_norm_vector_req: error %d sending QMI response",
                    (int) qmi_err);
    status = SNS_ERR_FAILED;
  }

  return status;
}

/*===========================================================================
  FUNCTION:   sns_qfp_pm_vote
===========================================================================*/
SNS_QFP_UIMAGE_CODE void sns_qfp_pm_vote(sns_pm_img_mode_e img_mode)
{
  sns_qfp_service *srv = &sns_qfp.qfp_service[0];
  sns_qfp_type_s *ctx = (sns_qfp_type_s *)srv->data;

  if (ctx == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "NULL service context");
    return;
  }

  if ((img_mode != SNS_IMG_MODE_MICRO) && (img_mode != SNS_IMG_MODE_BIG)
      && (img_mode != SNS_IMG_MODE_NOCLIENT))
  {
    SNS_QFP_PRINTF1(ERROR, "Image mode vote: invalid image mode, %d", img_mode);
    return;
  }
  
  if (sns_qfp_pm_handle != NULL)
  {
    if ((SNS_IMG_MODE_MICRO == img_mode) && 
        (SNS_QFP_FINGER_DETECT_MODE_ONE_SHOT_V01 != ctx->finger_detect_mode))
    {
      SNS_QFP_PRINTF1(LOW, "Image mode vote: ignore voting for micro image, finger_detect_mode = %d", ctx->finger_detect_mode);
      return;
    }

    if (ctx->island_mode_status == img_mode)
    {
      SNS_QFP_PRINTF1(LOW, "Image mode vote: skip voting (no change), image mode=%d", ctx->island_mode_status);
      return;
    }

    ctx->island_mode_status = img_mode;

    if (!ctx->ffd_config.enable_island_mode && (SNS_IMG_MODE_MICRO == img_mode))
    {
      SNS_QFP_PRINTF0(LOW, "Image mode vote: skip voting for micro image, island mode disabled in config");
      return;
    }
    
    SNS_QFP_PRINTF1(LOW, "Image mode vote: vote for image mode = %d", img_mode);
    sns_pm_vote_img_mode(sns_qfp_pm_handle, img_mode);
  }
}

/*=========================================================================
  FUNCTION:  sns_qfp_proc_signals
  =========================================================================*/
/*!
  @brief process OS signals for service

  @param[i] srv: service pointer
  @param[i] sig_flags: set signals

  @return None
*/
/*=======================================================================*/
void sns_qfp_proc_signals(sns_qfp_service* srv,
                          OS_FLAGS sig_flags)
{
  sns_qfp_type_s *ctx = (sns_qfp_type_s *) srv->data;
  sns_err_code_e status = SNS_SUCCESS;

  if (QFP_BIT_TEST(sig_flags, SNS_QFP_FINGER_DETECT_SIG))
  {
    sns_qfp_pm_vote(SNS_IMG_MODE_MICRO);
    sns_qfp_proc_finger_detect_sig(srv, sig_flags);
  }

  if (ctx->finger_detect_mode == SNS_QFP_FINGER_DETECT_MODE_ONE_SHOT_V01 && 
      QFP_BIT_TEST(sig_flags, SNS_QFP_POLLING_RATE_SIG))
  {
    if (ctx->current_poll_state)
    {
        // unregister timer callback
        (void)sns_qfp_register_finger_detect_timer(&ctx->finger_detect_timer_obj, 0);

        // register timer with different polling rate
        status = sns_qfp_register_finger_detect_timer(&(ctx->finger_detect_timer_obj),
                                                      ctx->finger_detect_poll_time);
        if (status != SNS_SUCCESS)
        {
          SNS_QFP_PRINTF1(ERROR, "failed to register timer: %d",
                          (int)status);
        } else {
            SNS_QFP_PRINTF1(MED, "changed polling rate to %d", ctx->finger_detect_poll_time);
        }
    } 
    else
    {
        // unregister timer callback
        (void)sns_qfp_register_finger_detect_timer(&ctx->finger_detect_timer_obj, 0);

        // register timer with different polling rate
        status = sns_qfp_register_finger_detect_timer(&(ctx->finger_detect_timer_obj),
                                                      ctx->finger_detect_low_poll_time);
        if (status != SNS_SUCCESS)
        {
          SNS_QFP_PRINTF1(ERROR, "failed to register timer: %d",
                          (int)status);
        } else {
            SNS_QFP_PRINTF1(MED, "changed polling rate to %d", ctx->finger_detect_low_poll_time);
        }
    }
  }
}

/*=========================================================================
  FUNCTION:  sns_qfp_proc_finger_detect_sig
  =========================================================================*/
/*!
  @brief Process the finger detect timer signal

  @param[i] srv: service pointer
  @param[i] sig_flags: set signals

  @return None
*/
void sns_qfp_proc_finger_detect_sig(sns_qfp_service* srv,
                          OS_FLAGS sig_flags)
{
  sns_qfp_type_s* ctx;
  sns_err_code_e status;
  sns_qfp_ffd_alg_run_result_t result;
  bool detected;
  bool no_detection_change;

  SNS_QFP_PRINTF0(LOW, "sns_qfp_proc_finger_detect_sig: called");

  if (srv == NULL || srv->data == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "srv context is NULL");
    return;
  }

  ctx = (sns_qfp_type_s*)srv->data;
  if (ctx->state != SNS_QFP_STATE_RUNNING)
  {
    SNS_QFP_PRINTF1(ERROR, "timer received in invalid state: %d", 
                    (int)ctx->state);
    return;
  }

  status = sns_qfp_ffd_alg_run(ctx, &result);
  if (status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "failed to run ffd algorithm: %d", (int)status);
    return;
  }

  detected = (result.result & SNS_QFP_FFD_ALG_RUN_RESULT_FINGER_DETECTED) ?
                true : false;
  no_detection_change = (result.result & SNS_QFP_FFD_ALG_RUN_RESULT_FINGER_NO_CHANGE) ?
                true : false;

  if (SNS_QFP_FINGER_DETECT_MODE_ONE_SHOT_V01 == ctx->finger_detect_mode)
  {
    ctx->cbge_timer_counter--;
    SNS_QFP_PRINTF1(LOW, "CBGE timer counter = %d", ctx->cbge_timer_counter);

    if ((detected) || (ctx->cbge_timer_counter <= 0))
    {
      sns_qfp_pm_vote(SNS_IMG_MODE_BIG);
      (void)sns_qfp_disable_finger_detect(srv, detected);
      sns_qfp_pm_vote(SNS_IMG_MODE_NOCLIENT);
    }
  }
  else if (SNS_QFP_FINGER_DETECT_MODE_CONTINUOUS_V01 == ctx->finger_detect_mode)
  {
    if ((no_detection_change == false) &&
        ((!ctx->finger_detect_result_reported) || (detected != ctx->last_finger_detect_result)))
    {
      sns_qfp_pm_vote(SNS_IMG_MODE_BIG);
      status = sns_qfp_send_finger_state_ind((sns_qfp_qmi_connection_s*)ctx->finger_detect_connection_handle,
                                             detected);
      sns_qfp_pm_vote(SNS_IMG_MODE_NOCLIENT);
      if (status != SNS_SUCCESS)
      {
        SNS_QFP_PRINTF1(ERROR, "failed to send finger state indication: %d", (int)status);
        return;
      }

      ctx->finger_detect_result_reported = true;
      ctx->last_finger_detect_result = detected;
    }
    
  }
  else
  {
    SNS_QFP_PRINTF1(ERROR, "invalid finger detect mode (%d)", ctx->finger_detect_mode);    
  }
}

/*=======================================================================*/

/*=========================================================================
  FUNCTION:  sns_qfp_srv_disconnected_cb
  =========================================================================*/
/*!
  @brief Called when a client disconnects from the service

  @param[i] service_cookie: sns_qfp_service pointer
  @param[i] connection_handle: connection handle of disconnecting client

  @return None
*/
/*=======================================================================*/
sns_err_code_e sns_qfp_srv_disconnected_cb(void* service_cookie, 
                                           void* connection_handle)
{
  sns_qfp_service* srv;
  sns_qfp_type_s* ctx = NULL;

  if (service_cookie == NULL || connection_handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "sns_qfp_srv_disconnected_cb, bad arguments");
    return SNS_ERR_BAD_PTR;
  }

  srv = (sns_qfp_service*)service_cookie;
  if (srv->data == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "sns_qfp_srv_disconnected_cb, bad context");
    return SNS_ERR_BAD_PTR;
  }

  ctx = (sns_qfp_type_s*)srv->data;
  if (ctx->state == SNS_QFP_STATE_RUNNING)
  {
    if (ctx->finger_detect_connection_handle == connection_handle)
    {
      (void)sns_qfp_disable_finger_detect(srv, false);
    }
  }

  return SNS_SUCCESS;
}
