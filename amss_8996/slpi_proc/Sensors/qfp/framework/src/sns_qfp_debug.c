/*=============================================================================
  @file sns_qfp_debug.c

  Implements debug and diagnostics messages for the QFP service
  For internal usage only, may be removed in production


  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/framework/src/sns_qfp_debug.c#1 $  */
/* $DateTime: 2016/02/25 10:35:39 $ */
/* $Author: kshyamas $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2016-02-02  LB   Added use of algorithm init/deinit functions
  2015-10-28  ND   Low-power wake-up
  2014-08-11  LD   Initial version

=============================================================================*/


/*-----------------------------------------------------------------------------
* Include Files
* ---------------------------------------------------------------------------*/
#include "sns_common.h"
#include "sns_common_v01.h"
#include "sns_debug_api.h"

#include "sns_qfp_priv.h"
#include "sns_qfp_v01.h"
#include "sns_qfp_sd_spi_base.h"
#include "sns_qfp_sd_reg_rw.h"
#include "sns_qfp_sd_regmap.h"
#include "sns_qfp_sd_misc.h"
#include "sns_qfp_debug_cmd.h"
#include "ffd_api.h"
#include "ffd_types.h"
#include "ffd_algo.h"

/*-----------------------------------------------------------------------------
* extern functions
* ---------------------------------------------------------------------------*/
extern sns_err_code_e ffd_alg_init(sns_qfp_type_s* ctx);
extern void ffd_alg_deinit(sns_qfp_type_s* ctx);
extern sns_err_code_e sns_qfp_ffd_alg_scan_images(
  sns_qfp_type_s* ctx,
  uint32_t image_pixels,
  ffd_image_on_off_e image_on_off
);

/*-----------------------------------------------------------------------------
* Private functions prototypes
* ---------------------------------------------------------------------------*/
static sensor1_error_e sns_qfp_debug_read_chip_id(
  sns_qfp_type_s* ctx,
  sns_qfp_debug_req_msg_v01* req,
  sns_qfp_debug_resp_msg_v01* resp
);

static sensor1_error_e sns_qfp_debug_read_image(
  sns_qfp_type_s* ctx,
  sns_qfp_debug_req_msg_v01* req,
  sns_qfp_debug_resp_msg_v01* resp
);

static sensor1_error_e sns_qfp_debug_spi_test(
  sns_qfp_type_s* ctx,
  sns_qfp_debug_req_msg_v01* req,
  sns_qfp_debug_resp_msg_v01* resp
);

static sensor1_error_e sns_qfp_debug_energy_estimator(
  sns_qfp_type_s* ctx,
  sns_qfp_debug_req_msg_v01* req,
  sns_qfp_debug_resp_msg_v01* resp
);

/*-----------------------------------------------------------------------------
* Exported functions implementation
* ---------------------------------------------------------------------------*/

sensor1_error_e sns_qfp_process_debug_msg(
   sns_qfp_type_s* ctx,
   sns_qfp_debug_req_msg_v01* req,
   sns_qfp_debug_resp_msg_v01* resp
)
{
  sensor1_error_e err = SENSOR1_SUCCESS;

  if (ctx == NULL || req == NULL || resp == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "bad arguments");
    return SENSOR1_EBAD_PTR;
  }

  resp->req_id = req->req_id;
  resp->req_id_valid = true;
  resp->data_len = 0;
  resp->data_valid = false;
  switch (req->req_id)
  {
  case SNS_QFP_DEBUG_READ_CHIP_ID:
  {
    err = sns_qfp_debug_read_chip_id(ctx, req, resp);
    break;
  }
  case SNS_QFP_DEBUG_READ_IMAGE:
  {
    err = sns_qfp_debug_read_image(ctx, req, resp);
    break;
  }
  case SNS_QFP_DEBUG_SPI_TEST:
    err = sns_qfp_debug_spi_test(ctx, req, resp);
    break;
  case SNS_QFP_DEBUG_ENERGY_ESTIMATOR:
    err = sns_qfp_debug_energy_estimator(ctx, req, resp);
    break;
  default:
    SNS_QFP_PRINTF1(ERROR, "unknown debug request id: %d", (int)req->req_id);
    err = SENSOR1_EBAD_PARAM;
    break;
  }

  return err;
}

/*-----------------------------------------------------------------------------
* Private functions implementation
* ---------------------------------------------------------------------------*/
static sensor1_error_e sns_qfp_debug_read_chip_id(
  sns_qfp_type_s* ctx,
  sns_qfp_debug_req_msg_v01* req,
  sns_qfp_debug_resp_msg_v01* resp
)
{
  sensor1_error_e err = SENSOR1_SUCCESS;
  sns_err_code_e tmp_status;
  sns_qfp_debug_read_chip_id_resp_t* aresp = 
    (sns_qfp_debug_read_chip_id_resp_t*)(&resp->data[0]);

  // Wakeup Ontario
  tmp_status = sns_qfp_sd_ontario_wakeup(ctx->spi_handle);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(HIGH, "sns_qfp_sd_ontario_wakeup failed (%d)",
                    (int) tmp_status);
  }

  tmp_status = sns_qfp_sd_read_chip_id(ctx->spi_handle, &aresp->chip_id);
  if (tmp_status != SNS_SUCCESS)
  {
    err = SENSOR1_EFAILED;
  }
  resp->data_len = sizeof(sns_qfp_debug_read_chip_id_resp_t);
  resp->data_valid = true;
  SNS_QFP_PRINTF1(HIGH, "qfp debug: chip id is 0x%X", (int)aresp->chip_id);

  // Put Ontario to idle mode
  tmp_status = sns_qfp_sd_ontario_idle(ctx->spi_handle);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(HIGH, "sns_qfp_sd_ontario_idle failed (%d)",
                    (int) tmp_status);
  }

  return err;
}

static sensor1_error_e sns_qfp_debug_read_image(
  sns_qfp_type_s* ctx,
  sns_qfp_debug_req_msg_v01* req,
  sns_qfp_debug_resp_msg_v01* resp
)
{
  sensor1_error_e err = SENSOR1_SUCCESS;
  sns_err_code_e tmp_status;
  sns_qfp_debug_read_image_req_t* areq = 
        (sns_qfp_debug_read_image_req_t*)(&req->data[0]);
  sns_qfp_debug_read_image_resp_t* aresp = 
        (sns_qfp_debug_read_image_resp_t*)(&resp->data[0]);
  uint32_t image_offset = offsetof(sns_qfp_debug_read_image_resp_t, image_data);
  uint32_t image_size_out;

  if ((!req->data_valid) || 
      req->data_len < sizeof(sns_qfp_debug_read_image_req_t))
  {
    SNS_QFP_PRINTF2(ERROR, "request data too small (%d, need %d)",
                    (int)req->data_len, 
                    sizeof(sns_qfp_debug_read_image_req_t));
    err = SENSOR1_EBAD_PARAM;
    goto End;
  }

  // Wakeup Ontario
  tmp_status = sns_qfp_sd_ontario_wakeup(ctx->spi_handle);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(HIGH, "sns_qfp_sd_ontario_wakeup failed (%d)",
                    (int) tmp_status);
  }

  tmp_status = sns_qfp_sd_set_tbenable(ctx->spi_handle, (areq->tb_enable != 0));
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "set tbenable failed: %d", (int)tmp_status);
    err = SENSOR1_EFAILED;
    goto End;
  }

  tmp_status = sns_qfp_sd_read_image_data(
     ctx->rid_handle, 
     ctx->spi_handle,
     areq->image_pixels,
     &aresp->image_data[0],
     SNS_QFP_DEBUG_MSG_MAX_SIZE_V01 - image_offset,
     &image_size_out
  );

  aresp->image_size = areq->image_pixels * sizeof(uint16_t);

  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "read image data failed: %d", (int)tmp_status);
    err = SENSOR1_EFAILED;
    goto End;
  }

  resp->data_len = aresp->image_size + image_offset;
  resp->data_valid = true;
End:
  // Put Ontario to idle mode
  tmp_status = sns_qfp_sd_ontario_idle(ctx->spi_handle);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(HIGH, "sns_qfp_sd_ontario_idle failed (%d)",
                    (int) tmp_status);
  }

  return err;
}

static sensor1_error_e sns_qfp_debug_spi_test(
  sns_qfp_type_s* ctx,
  sns_qfp_debug_req_msg_v01* req,
  sns_qfp_debug_resp_msg_v01* resp
)
{
  sensor1_error_e err = SENSOR1_SUCCESS;
  sns_err_code_e tmp_status;
  uint32_t saved_freq, asize, before, after;
  uint8_t *buffer = NULL, *abuffer;
  sns_qfp_debug_spi_test_req_t* areq = 
      (sns_qfp_debug_spi_test_req_t*)(&req->data[0]);
  sns_qfp_debug_spi_test_resp_t* aresp = 
      (sns_qfp_debug_spi_test_resp_t*)(&resp->data[0]);

  if ((!req->data_valid) || 
      req->data_len < sizeof(sns_qfp_debug_spi_test_req_t))
  {
    SNS_QFP_PRINTF2(ERROR, "request data too small (%d, need %d)",
                    (int)req->data_len, 
                    sizeof(sns_qfp_debug_spi_test_req_t));
    err = SENSOR1_EBAD_PARAM;
    goto End;
  }

  // Wakeup Ontario
  tmp_status = sns_qfp_sd_ontario_wakeup(ctx->spi_handle);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(HIGH, "sns_qfp_sd_ontario_wakeup failed (%d)",
                    (int) tmp_status);
  }

  tmp_status = sns_qfp_sd_spi_get_clk_freq(ctx->spi_handle, &saved_freq);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "failed go get SPI clock freq: %d", 
                    (int)tmp_status);
    err = SENSOR1_EFAILED;
    goto End;
  }

  asize = areq->size + ALIGN64_SPARE;
  buffer = (uint8_t*)SNS_OS_MALLOC(SNS_DBG_MOD_QFP, asize);
  if (buffer == NULL)
  {
    SNS_QFP_PRINTF1(ERROR, "failed to allocate %d bytes",
                    (int)asize);
    err = SENSOR1_ENOMEM;
    goto End;
  }
  abuffer = (uint8_t*)ALIGN64(buffer);
  SNS_OS_MEMSET(abuffer, 0, areq->size);

  tmp_status = sns_qfp_sd_spi_set_clk_freq(ctx->spi_handle, areq->freq);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "failed to set SPI clk freq: %d", 
                    (int)tmp_status);
    err = SENSOR1_EFAILED;
    goto End;
  }

  before = sns_em_get_timestamp();
  tmp_status = spi_qfp_sd_spi_full_duplex(ctx->spi_handle, abuffer, areq->size);
  after = sns_em_get_timestamp();
  (void)sns_qfp_sd_spi_set_clk_freq(ctx->spi_handle, saved_freq);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "SPI transfer failed: %d", (int)tmp_status);
    err = SENSOR1_EFAILED;
    goto End;
  }

  resp->data_len = sizeof(sns_qfp_debug_spi_test_resp_t);
  resp->data_valid = true;
  aresp->amount = areq->size;
  aresp->time = sns_em_convert_dspstick_to_usec(after - before);
  SNS_QFP_PRINTF2(HIGH, "SPI test: transferred %d bytes in %d microseconds", 
                  (int)aresp->amount, (int)aresp->time);

End:
  // Put Ontario to idle mode
  tmp_status = sns_qfp_sd_ontario_idle(ctx->spi_handle);
  if (tmp_status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(HIGH, "sns_qfp_sd_ontario_idle failed (%d)",
                    (int) tmp_status);
  }

  if (buffer != NULL)
  {
    SNS_OS_FREE(buffer);
  }
  return err;
}

static sensor1_error_e sns_qfp_debug_energy_estimator(
  sns_qfp_type_s* ctx,
  sns_qfp_debug_req_msg_v01* req,
  sns_qfp_debug_resp_msg_v01* resp
)
{
  sensor1_error_e err = SENSOR1_SUCCESS;
  sns_err_code_e sns_err = SNS_SUCCESS;
  uint32_t image_size, width, height;
  ffd_proc_input_t input;
  ffd_proc_output_t output;
  sns_qfp_debug_energy_estimator_req_t* areq = 
      (sns_qfp_debug_energy_estimator_req_t*)(&req->data[0]);
  sns_qfp_debug_energy_estimator_resp_t* aresp = 
      (sns_qfp_debug_energy_estimator_resp_t*)(&resp->data[0]);
  uint32_t image_offset = offsetof(sns_qfp_debug_energy_estimator_resp_t, fgon_fgoff_data);

  if ((!req->data_valid) || 
      req->data_len < sizeof(sns_qfp_debug_energy_estimator_req_t))
  {
    SNS_QFP_PRINTF2(ERROR, "request data too small (%d, need %d)",
                    (int)req->data_len, 
                    sizeof(sns_qfp_debug_energy_estimator_req_t));
    err = SENSOR1_EBAD_PARAM;
    goto End;
  }

  image_size = areq->image_pixels * sizeof(uint16_t);

  if (areq->debug_stage == 0)
  {
    sns_err = ffd_alg_init(ctx);
    if (sns_err != SNS_SUCCESS)
    {
      SNS_QFP_PRINTF1(ERROR, "failed to initialize ffd, sns_err = %d", sns_err);
      err = SENSOR1_EFAILED;
      goto End;
    }

    if (areq->image_pixels == FFD_PHASE1_IMAGE_SIZE)
    {
      width = FFD_PHASE1_IMAGE_WIDTH;
      height = FFD_PHASE1_IMAGE_HEIGHT;
    }
    else if (areq->image_pixels == FFD_PHASE2_IMAGE_SIZE)
    {
      width = FFD_PHASE2_IMAGE_WIDTH;
      height = FFD_PHASE2_IMAGE_HEIGHT;
    }
    else
    {
      SNS_QFP_PRINTF1(ERROR, "unsupported image size: %d",
                      (int)areq->image_pixels);
      err = SENSOR1_EBAD_PARAM;
      goto End;
    }

    SNS_QFP_PRINTF0(LOW, "start scan images");
    sns_err = sns_qfp_ffd_alg_scan_images(ctx, areq->image_pixels, FFD_IMAGE_ON_OFF);
    if (sns_err != SNS_SUCCESS)
    {
      SNS_QFP_PRINTF1(ERROR, "fail to scan images, sns_err = %d", sns_err);
      err = SENSOR1_EFAILED;
      goto End;
    }
  
    // run energy estimator
    input.width = width;
    input.height = height;
    input.on = (uint16_t *) ctx->fg_on_buffer;
    input.off = (uint16_t *) ctx->fg_off_buffer;
    input.ffd_proc_on_off = FFD_PROC_ON_OFF;
    input.ffd_proc_type = FFD_PROC_WAKE_UP;
    sns_err = ffd_process(ctx->ffd_handle, &input, &output);
    if (sns_err != SNS_SUCCESS)
    {
      SNS_QFP_PRINTF1(ERROR, "ffd_process failed: sns_err = %d", sns_err);
      err = SENSOR1_EFAILED;
      goto End;
    }

    aresp->image_size = image_size;
    aresp->result = (uint32_t) output.result;
    aresp->energy = output.energy;
    aresp->energy_off = output.energy_off;
    resp->data_len = aresp->image_size + image_offset;
    resp->data_valid = true;

    // Copy fgon image to the response structure
    SNS_QFP_PRINTF1(LOW,
                    "getting fgon buffer, image_size = %d",
                    image_size);
    if (ctx->fg_on_buffer)
    {
      SNS_OS_MEMSCPY(&(aresp->fgon_fgoff_data[0]),
                     image_size,
                     ctx->fg_on_buffer,
                     image_size);
    }
    else
    {
      SNS_QFP_PRINTF0(ERROR, "fgon buffer is NULL");
    }
  }
  else
  {
    SNS_QFP_PRINTF1(LOW,
                    "getting fgoff buffer, image_size = %d",
                    image_size);
    aresp->image_size = image_size;
    aresp->result = 0;
    aresp->energy = 0;
    aresp->energy_off = 0;
    resp->data_len = aresp->image_size + image_offset;
    resp->data_valid = true;
    // Copy fgoff image to the response structure
    if (ctx->fg_off_buffer)
    {
      SNS_OS_MEMSCPY(&(aresp->fgon_fgoff_data[0]),
                     image_size,
                     ctx->fg_off_buffer,
                     image_size);
    }
    else
    {
      SNS_QFP_PRINTF0(ERROR, "fgoff buffer is NULL");
    }
    ffd_alg_deinit(ctx);
  }

End:
  
  return err;
}

