#ifndef SNS_QFP_FFD_ALG_H
#define SNS_QFP_FFD_ALG_H
/*=============================================================================
  @file sns_qfp_ffd_alg.h
 
  This module operates the fast finger detection algorithm flow. It reads one
  or more images from the FP sensor, runs the FFD algorithm and return finger
  detection result.

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/framework/inc/sns_qfp_ffd_alg.h#1 $  */
/* $DateTime: 2016/02/25 10:35:39 $ */
/* $Author: kshyamas $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order. 

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2016-02-16  RD   Memory optimizations
  2016-02-02  LB   Added separate algorithm init/deinit functions
  2014-08-16  LD   Initial version

=============================================================================*/


/*-----------------------------------------------------------------------------
* Include Files
* ---------------------------------------------------------------------------*/
#include "sns_common.h"
#include "sns_qfp_priv.h"

/*-----------------------------------------------------------------------------
* Definitions
* ---------------------------------------------------------------------------*/
/** bit masks for result field in sns_qfp_alg_ffd_run_result_t */
#define SNS_QFP_FFD_ALG_RUN_RESULT_FINGER_DETECTED      1
#define SNS_QFP_FFD_ALG_RUN_RESULT_FINGER_NO_CHANGE     2

/** structure with results reported by sns_qfp_ffd_alg_run */
typedef struct {
  /* result of algorithm */
  uint32_t result;
} sns_qfp_ffd_alg_run_result_t;


/*-----------------------------------------------------------------------------
* Prototypes
* ---------------------------------------------------------------------------*/
/**
 * @brief Runs the fast finger detection algorithm and report 
 *        results
 * @param[in] ctx QFP service context 
 * @param[out] result will be filled with results 
 * @return SNS_SUCCESS if successful 
 */
SNS_QFP_UIMAGE_CODE sns_err_code_e sns_qfp_ffd_alg_run(sns_qfp_type_s* ctx, 
                                                       sns_qfp_ffd_alg_run_result_t* result);

#endif /* #ifndef SNS_QFP_FFD_ALG_H */
