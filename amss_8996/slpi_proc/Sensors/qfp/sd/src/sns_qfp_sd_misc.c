/*==============================================================================

    S E N S O R S   FP SENSOR FINGER DETECT  SPI D R I V E R

DESCRIPTION

    This file contains various functions for accessing the FP sensor.
    It is a small subset of functions implemented in TZ driver, needed
    for the finger detect functioanlity.

Copyright (c) 2015 by QUALCOMM Technologies, Incorporated.
All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential
==============================================================================*/
/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/qfp.slpi/1.0/sd/src/sns_qfp_sd_misc.c#1 $


when         who     what, where, why
----------   ---     -----------------------------------------------------------
08/12/15     LD      Initial version
==============================================================================*/

#include "sns_em.h"
#include "sns_qfp_sd_misc.h"
#include "sns_qfp_sm_priv.h"
#include "sns_qfp_sd_reg_rw.h"
#include "sns_qfp_sd_regmap.h"
#include "utimer_osal.h"

sns_err_code_e sns_qfp_sd_read_chip_id(
  sns_qfp_sd_spi_handle_t spi_handle,
  uint32_t* chip_id
)
{
  uint8_t data = 0;
  sns_err_code_e status = SNS_SUCCESS;

  if(NULL == chip_id)
  {
    SNS_QFP_PRINTF0(ERROR, "chip_id is NULL");
    return SNS_ERR_BAD_PTR;
  }
  if (spi_handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "SPI device not open");
    return SNS_ERR_NOTALLOWED;
  }

  STATUS_CHECK(sns_qfp_sd_read_8bits(spi_handle, CHIP_ID_0, &data));
  *chip_id = 0xFF & data;
  STATUS_CHECK(sns_qfp_sd_read_8bits(spi_handle, CHIP_ID_1, &data));
  *chip_id |= (0xFF & data) << 8;
  STATUS_CHECK(sns_qfp_sd_read_8bits(spi_handle, CHIP_ID_2, &data));
  *chip_id |= (0xFF & data) << 16;
  STATUS_CHECK(sns_qfp_sd_read_8bits(spi_handle, CHIP_ID_3, &data));
  *chip_id |= (0xFF & data) << 24;
End:
  if (status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "read chip id failed: %d", (int)status);
  }
  return status;
}

sns_err_code_e sns_qfp_sd_enable_all_modules(
  sns_qfp_sd_spi_handle_t spi_handle
)
{
  return SNS_SUCCESS;
}

sns_err_code_e sns_qfp_sd_ontario_wakeup(
  sns_qfp_sd_spi_handle_t spi_handle
)
{
  uint8_t data[4];
  sns_err_code_e status = SNS_SUCCESS;

  SNS_QFP_PRINTF0(LOW, "Ontario wakeup");

  if (spi_handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "SPI device not open");
    return SNS_ERR_NOTALLOWED;
  }

  /* Start Ontario wake-up procedure */

  STATUS_CHECK(sns_qfp_sd_spi_flush(spi_handle));

  // 1 NOP
  data[0] = data[1] = data[2] = data[3] = 0;
  STATUS_CHECK(spi_qfp_sd_spi_full_duplex(spi_handle, &data[0], sizeof(data[0])));

  // Clock request bypass mode
  data[0] = 0xDA;
  data[1] = 0xBA;
  data[2] = 0x80;
  data[3] = 0x00;
  STATUS_CHECK(spi_qfp_sd_spi_full_duplex(spi_handle, &data[0], sizeof(data)));

  // 1 NOP
  data[0] = data[1] = data[2] = data[3] = 0;
  STATUS_CHECK(spi_qfp_sd_spi_full_duplex(spi_handle, &data[0], sizeof(data[0])));

  // Enable onchip RC LDO
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, REG1_CLK, REG1_CLK_LDO_MODE_ENABLE));

  // Disable clock
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, CLK_REQ_CTL, 0xFF));

  STATUS_CHECK(sns_qfp_sd_spi_flush(spi_handle));

  // 1 NOP
  data[0] = data[1] = data[2] = data[3] = 0;
  STATUS_CHECK(spi_qfp_sd_spi_full_duplex(spi_handle, &data[0], sizeof(data[0])));

  // Clock request bypass mode
  data[0] = 0xDA;
  data[1] = 0xBA;
  data[2] = 0x80;
  data[3] = 0x00;
  STATUS_CHECK(spi_qfp_sd_spi_full_duplex(spi_handle, &data[0], sizeof(data)));

  // 1 NOP
  data[0] = data[1] = data[2] = data[3] = 0;
  STATUS_CHECK(spi_qfp_sd_spi_full_duplex(spi_handle, &data[0], sizeof(data[0])));

  STATUS_CHECK(sns_qfp_sd_spi_flush(spi_handle));

  /* End Ontario wake-up procedure */
End:
  return status;
}

sns_err_code_e sns_qfp_sd_ontario_idle(
  sns_qfp_sd_spi_handle_t spi_handle
)
{
  uint8_t data[4];
  sns_err_code_e status = SNS_SUCCESS;

  SNS_QFP_PRINTF0(LOW, "Ontario idle");

  if (spi_handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "SPI device not open");
    return SNS_ERR_NOTALLOWED;
  }

  /* Start Ontario idle procedure */

  STATUS_CHECK(sns_qfp_sd_spi_flush(spi_handle));

  // Disable onchip RC LDO
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, REG1_CLK, REG1_CLK_LDO_MODE_DISABLE));

  // Disable clock
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, CLK_REQ_CTL, 0xFF));

  STATUS_CHECK(sns_qfp_sd_spi_flush(spi_handle));

  // 1 NOP
  data[0] = data[1] = data[2] = data[3] = 0;
  STATUS_CHECK(spi_qfp_sd_spi_full_duplex(spi_handle, &data[0], sizeof(data[0])));

  // Clock request bypass mode
  data[0] = 0xDA;
  data[1] = 0xBA;
  data[2] = 0x80;
  data[3] = 0x00;
  STATUS_CHECK(spi_qfp_sd_spi_full_duplex(spi_handle, &data[0], sizeof(data)));

  // 1 NOP
  data[0] = data[1] = data[2] = data[3] = 0;
  STATUS_CHECK(spi_qfp_sd_spi_full_duplex(spi_handle, &data[0], sizeof(data[0])));

  // Disable clock
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, CLK_REQ_CTL, 0xFF));

  STATUS_CHECK(sns_qfp_sd_spi_flush(spi_handle));

  /* End Ontario idle procedure */

End:
  return status;
}

sns_err_code_e sns_qfp_sd_set_tbenable(
  sns_qfp_sd_spi_handle_t spi_handle,
  bool enable
)
{
  sns_err_code_e status = SNS_SUCCESS;

  if(enable)
  {
    status = sns_qfp_sd_write_8bits(spi_handle, REG4_TX, 0x0);
  }
  else
  {
    status = sns_qfp_sd_write_8bits(spi_handle, REG4_TX, 0x02);
  }

  return status;
}

void sns_qfp_sd_utimer_sleep(
  utimer_timetick_type timeout
)
{
  /* Timer for the client to handle the sleeping */
  utimer_type sleep_utimer;

  /* Signal object for timer callback notification */
  qurt_anysignal_t sleep_signal_obj;

  /* memset the external timer memory to zero */
  SNS_OS_MEMSET(&sleep_utimer,
                0,
                sizeof(utimer_type));

  /* Initialize the qurt signal object */
  qurt_anysignal_init(&sleep_signal_obj);

  /* Create a non-deferrable timer for the client */
  utimer_def_osal(&sleep_utimer,
                  UTIMER_NATIVE_OS_SIGNAL_TYPE,
                  &sleep_signal_obj,
                  0x1);

  /* Set the timer to expire after the specified duration */
  (void)utimer_set_64(&sleep_utimer,
                      timeout,
                      0,
                      UT_USEC);

  /* Wait on the Qurt signal object */
  qurt_anysignal_wait (&sleep_signal_obj, 0x1);

  /* Delete the signal */
//  qurt_anysignal_destroy (&sleep_signal_obj);

  /* Undefine the timer */
  utimer_undef(&sleep_utimer);
}

