/*==============================================================================

    S E N S O R S   FP SENSOR FINGER DETECT  SPI D R I V E R

DESCRIPTION

    This file contains basic functions to configure SPI and read/write
    data blocks. It is based on OntarioSpi.c in Ontario TZ SPI driver

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/
/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/qfp.slpi/1.0/sd/src/sns_qfp_sd_spi_base.c#2 $


when         who     what, where, why
----------   ---     -----------------------------------------------------------
03/22/16     GJU     Use consolidated SNS_OS_FREE rather than SNS_OS_U_FREE
05/10/15     LD      Initial version based on OntarioSpi.c
==============================================================================*/
#include "sns_common.h"
#include "sns_memmgr.h"
#include "sns_qfp_sm_priv.h"
#include "sns_qfp_sd_spi_base.h"
#include "SpiDriver.h"

/* size of the SPI transaction buffer size */
#define SPI_BUFFER_SIZE 256

/** structure with information for accessing the SPI device */
typedef struct {
  /* SPI device ID in use (0-2 for SSC SPI 1..3) */
  spi_device_id_t spi_device_id;
  /* configuration for SPI transactions */
  spi_config_t config;
  /* buffer for SPI data when using buffering mode */
  uint8_t* buffer;
  /* 64 bit aligned pointer to buffer */
  uint8_t* abuffer;
  /* total buffer size */
  uint32_t buffer_total_size;
  /* number of bytes currently in buffer */
  uint32_t buffer_used_size;
  /* true if transaction buffering is enabled */
  bool buffering_enabled;
} sns_qfp_sd_spi_context_t;

//-----------------------------------------------------------------
// prototypes of private functions
//-----------------------------------------------------------------
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e spi_qfp_sd_spi_full_duplex_internal(
  sns_qfp_sd_spi_context_t* ctx,
  uint8_t* address,
  uint32_t size);
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e sns_qfp_sd_spi_flush_internal(
   sns_qfp_sd_spi_context_t* ctx);

//-----------------------------------------------------------------
// implementation of exported functions
//-----------------------------------------------------------------
sns_err_code_e sns_qfp_sd_spi_alloc_handle(sns_qfp_sd_spi_handle_t* handle)
{
  sns_qfp_sd_spi_context_t* ctx;

  if (handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "handle is NULL");
    return SNS_ERR_BAD_PTR;
  }

  *handle = SNS_OS_U_MALLOC(SNS_DBG_MOD_QFP, sizeof(sns_qfp_sd_spi_context_t));
  if (*handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "failed to allocate SPI context memory");
    return SNS_ERR_NOMEM;
  }

  ctx = (sns_qfp_sd_spi_context_t*)(*handle);
  SNS_OS_MEMSET(ctx, 0, sizeof(sns_qfp_sd_spi_context_t));
  // enable transaction buffering by default, to improve performance
  ctx->buffering_enabled = true;

  return SNS_SUCCESS;
}

void sns_qfp_sd_spi_free_handle(sns_qfp_sd_spi_handle_t handle)
{
  if (handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "handle is NULL");
    return;
  }

  SNS_OS_FREE(handle);
}

sns_err_code_e sns_qfp_sd_spi_init(
    sns_qfp_sd_spi_handle_t handle,
    spi_device_id_t device_id,
    uint8_t slave_index,
    uint32_t freq)
{
  sns_err_code_e status = SNS_SUCCESS;
  int32_t res;
  sns_qfp_sd_spi_context_t* ctx;

  if (handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "handle is NULL");
    return SNS_ERR_BAD_PTR;
  }

  ctx = (sns_qfp_sd_spi_context_t*)handle;
  ctx->spi_device_id = device_id;
  // use same values as in the TZ Ontario SPI driver
  ctx->config.spi_clk_polarity = SPI_CLK_IDLE_LOW;
  ctx->config.spi_shift_mode = SPI_INPUT_FIRST_MODE;
  ctx->config.spi_cs_polarity = SPI_CS_ACTIVE_LOW;
  ctx->config.spi_cs_mode = SPI_CS_KEEP_ASSERTED;
  ctx->config.spi_clk_always_on = SPI_CLK_NORMAL;

  ctx->config.spi_bits_per_word   = 8;

  ctx->config.spi_slave_index = (uint8)slave_index;
  ctx->config.min_slave_freq_hz = 0;
  ctx->config.max_slave_freq_hz = (uint32)freq;
  ctx->config.deassertion_time_ns = 0;
  ctx->config.loopback_mode = (boolean)0;

  SNS_QFP_PRINTF3( MED, "Ontario4 open: SPI device id %d slave %d freq %d", 
                   (int)device_id, (int)slave_index, (int)freq);

  res = spi_open(device_id);
  if (res != 0)
  {
    SNS_QFP_PRINTF1(ERROR, "Ontario4: spi_open error %d", res);
    status = SNS_ERR_FAILED;
    goto End;
  }

  res = spi_clk_ctrl(device_id, 1, freq);
  if (res != 0)
  {
    SNS_QFP_PRINTF1(ERROR, "Ontario4: spi_clk_ctrl error %d", res);
    spi_close(device_id);
    status = SNS_ERR_FAILED;
    goto End;
  }

  spi_close(device_id);

End:
  return status;
}

void sns_qfp_sd_spi_deinit(sns_qfp_sd_spi_handle_t handle)
{
  sns_qfp_sd_spi_context_t* ctx;
  int32_t res;

  if (handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "handle is NULL");
    return;
  }

  ctx = (sns_qfp_sd_spi_context_t*)handle;

  res = spi_open(ctx->spi_device_id);
  if (res == 0)
  {
    res = spi_clk_ctrl(ctx->spi_device_id, 0, ctx->config.max_slave_freq_hz);
    if (res != 0)
    {
        SNS_QFP_PRINTF1(ERROR, "Ontario4: spi_clk_ctrl error %d", res);
    }
  }
  else
  {
    SNS_QFP_PRINTF1(ERROR, "Ontario4: spi_open error %d", res);
  }

  spi_close(ctx->spi_device_id);
}

sns_err_code_e spi_qfp_sd_spi_full_duplex(sns_qfp_sd_spi_handle_t handle,
                                          uint8_t* address, uint32_t size)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_sd_spi_context_t* ctx;

  if (handle == NULL || address == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "bad spi_handle or address");
    return SNS_ERR_BAD_PTR;
  }

  if (size == 0)
  {
    SNS_QFP_PRINTF0(ERROR, "size is 0");
    return SNS_ERR_BAD_PARM;
  }


  ctx = (sns_qfp_sd_spi_context_t*)handle;
  status = spi_qfp_sd_spi_full_duplex_internal(ctx, address, size);
  return status;
}

sns_err_code_e spi_qfp_sd_spi_transfer(sns_qfp_sd_spi_handle_t handle,
                                          uint8_t* address, uint32_t size)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_sd_spi_context_t* ctx;

  if (handle == NULL || address == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "handle or address are NULL");
    return SNS_ERR_BAD_PTR;
  }
  if (size == 0)
  {
    SNS_QFP_PRINTF0(ERROR, "size is 0");
    return SNS_ERR_BAD_PARM;
  }

  ctx = (sns_qfp_sd_spi_context_t*)handle;

  if (!ctx->buffering_enabled)
  {
    status = spi_qfp_sd_spi_full_duplex_internal(ctx, address, size);
    return status;
  }

  if (ctx->buffer == NULL)
  {
    // allocate the buffer
    ctx->buffer_used_size = 0;
    ctx->buffer_total_size = SPI_BUFFER_SIZE;
    ctx->buffer = (uint8_t*)SNS_OS_U_MALLOC(SNS_DBG_MOD_QFP, 
                                            ctx->buffer_total_size + ALIGN64_SPARE);
    if (ctx->buffer == NULL)
    {
      SNS_QFP_PRINTF0(ERROR, "failed to allocate transaction buffer");
      status = spi_qfp_sd_spi_full_duplex_internal(ctx, address, size);
      return status;
    }
    ctx->abuffer = (uint8_t*)ALIGN64(ctx->buffer);
  }

  if ((size + ctx->buffer_used_size) > ctx->buffer_total_size)
  {
    status = sns_qfp_sd_spi_flush_internal(ctx);
  }

  (void)SNS_OS_MEMSCPY(&ctx->abuffer[ctx->buffer_used_size], 
                       ctx->buffer_total_size - ctx->buffer_used_size,
                       address, size);
  ctx->buffer_used_size += size;

  return status; 
}

sns_err_code_e sns_qfp_sd_spi_flush(sns_qfp_sd_spi_handle_t handle)
{
  sns_qfp_sd_spi_context_t* ctx;

  if (handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "handle is NULL");
    return SNS_ERR_BAD_PTR;
  }

  ctx = (sns_qfp_sd_spi_context_t*)handle;
  return sns_qfp_sd_spi_flush_internal(ctx);
}

void sns_qfp_sd_spi_enable_buffering(sns_qfp_sd_spi_handle_t handle)
{
  sns_qfp_sd_spi_context_t* ctx;

  if (handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "handle is NULL");
    return;
  }

  ctx = (sns_qfp_sd_spi_context_t*)handle;
  ctx->buffering_enabled = true;
}

void sns_qfp_sd_spi_disable_buffering(sns_qfp_sd_spi_handle_t handle)
{
  sns_qfp_sd_spi_context_t* ctx;

  if (handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "handle is NULL");
    return;
  }

  ctx = (sns_qfp_sd_spi_context_t*)handle;
  (void)sns_qfp_sd_spi_flush_internal(ctx);
  ctx->buffering_enabled = false;
}

sns_err_code_e sns_qfp_sd_spi_set_clk_freq(
  sns_qfp_sd_spi_handle_t handle, uint32_t freq)
{
  sns_qfp_sd_spi_context_t* ctx;

  if (handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "spi_handle is NULL");
    return SNS_ERR_BAD_PTR;
  }

  ctx = (sns_qfp_sd_spi_context_t*)handle;
  ctx->config.max_slave_freq_hz = freq;
  return SNS_SUCCESS;
}

sns_err_code_e sns_qfp_sd_spi_get_clk_freq(
  sns_qfp_sd_spi_handle_t handle, uint32_t* freq)
{
  sns_qfp_sd_spi_context_t* ctx;

  if (handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "spi_handle is NULL");
    return SNS_ERR_BAD_PTR;
  }
  ctx = (sns_qfp_sd_spi_context_t*)handle;
  *freq = ctx->config.max_slave_freq_hz;
  return SNS_SUCCESS;
}

//----------------------------------------------------------------
// private functions
//----------------------------------------------------------------
static sns_err_code_e spi_qfp_sd_spi_full_duplex_internal(
   sns_qfp_sd_spi_context_t* ctx, uint8_t* address, uint32_t size
)
{
  SPI_RESULT        spi_result; //spi_errors.h
  spi_transaction_t read_transaction_info;
  spi_transaction_t write_transaction_info;

  read_transaction_info.buf_virt_addr = (void *)address;
  read_transaction_info.buf_phys_addr = (void *)address;
  read_transaction_info.buf_len       = size;

  write_transaction_info.buf_virt_addr = (void *)address;
  write_transaction_info.buf_phys_addr = (void *)address;
  write_transaction_info.buf_len       = size;

  spi_result = spi_open( ctx->spi_device_id );
  if ( spi_result != SPI_SUCCESS )
  {
    SNS_QFP_PRINTF1(ERROR, "spi_open call failed: %d", (int)spi_result);
    return SNS_ERR_FAILED;
  }

  // Perform a full duplex transfer ----------------------
  spi_result = spi_full_duplex( ctx->spi_device_id,
                                &ctx->config,
                                &write_transaction_info, &read_transaction_info );
  if ( spi_result != SPI_SUCCESS )
  {
    SNS_QFP_PRINTF1(ERROR, "spi_full_duplex call failed: %d", (int)spi_result);
    spi_close(ctx->spi_device_id);
    return SNS_ERR_FAILED;
  }

  spi_close( ctx->spi_device_id );

  return SNS_SUCCESS;
}

static sns_err_code_e sns_qfp_sd_spi_flush_internal(
   sns_qfp_sd_spi_context_t* ctx)
{
  sns_err_code_e status = SNS_SUCCESS;

  if (ctx->buffer_used_size > 0) 
  {
    status = spi_qfp_sd_spi_full_duplex_internal(ctx, ctx->abuffer, ctx->buffer_used_size);
    ctx->buffer_used_size = 0;
  }

  return status; 
}

