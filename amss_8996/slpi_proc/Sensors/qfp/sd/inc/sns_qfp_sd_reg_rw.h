/*==============================================================================

    S E N S O R S   FP SENSOR FINGER DETECT  SPI D R I V E R

DESCRIPTION

    This file contains functions to perform register and block read/writes
    from/to the FP sensor

Copyright (c) 2015 by QUALCOMM Technologies, Incorporated. 
All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential
==============================================================================*/
/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/qfp.slpi/1.0/sd/inc/sns_qfp_sd_reg_rw.h#1 $


when         who     what, where, why
----------   ---     -----------------------------------------------------------
05/10/15     LD      Initial version based on OntarioRegReadWrite.h
==============================================================================*/

#ifndef _SNS_QFP_SD_REG_RW_H
#define _SNS_QFP_SD_REG_RW_H

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "sns_common.h"
#include "sns_qfp_sd_spi_base.h"

#ifndef ENCODING_8BIT_BPW 
// SLPI SPI driver does not change byte order when changing bits_per_word,
// so always use 8 bits here
#define ENCODING_8BIT_BPW 8 // 16
#endif

#ifndef ENCODING_BLOCK_BPW 
#define ENCODING_BLOCK_BPW 8
#endif

/*-------------------------------------------------------------------------
 * Type Definitions
 * ----------------------------------------------------------------------*/
#if (ENCODING_8BIT_BPW == 8)

typedef PACK(struct)
{
  uint8_t opcode/*=0x84*/;
  uint8_t data_size/*=0x02*/; // 2 - represnts 8 bit data
  uint8_t dummy1/*=0x00*/;
  uint8_t num_trailing_bytes/*=0x01*/;
  uint8_t address_byte3;
  uint8_t address_byte2;
  uint8_t address_byte1;
  uint8_t address_byte0;
  uint8_t data;
  uint8_t trailing_dummy1/*=0x00*/; // Changes with the num of trailing bytes mentioned.
} sns_qfp_sd_8bit_write_t;

typedef PACK(struct)
{
  uint8_t opcode/*=0x83*/;
  uint8_t data_size/*=0x02*/; // 2 - represnts 8 bit data
  uint8_t dummy1/*=0x00*/;
  uint8_t num_trailing_bytes/*=0x01*/;
  uint8_t address_byte3;
  uint8_t address_byte2;
  uint8_t address_byte1;
  uint8_t address_byte0;
  uint8_t dummy2/*=0x00*/;
  uint8_t dummy3/*=0x00*/;
  uint8_t dummy4/*=0x00*/;
  uint8_t dummy5/*=0x00*/;
  uint8_t dummy6/*=0x00*/;
  uint8_t data/*=0x00*/;
} sns_qfp_sd_8bit_read_t;

typedef PACK(struct)
{
  uint8_t opcode/*=0x84*/;
  uint8_t data_size/*=0x0*/; // 0 - represnts 32 bit data
  uint8_t dummy1/*=0x00*/;
  uint8_t num_trailing_bytes/*=0x0*/;
  uint8_t address_byte3;
  uint8_t address_byte2;
  uint8_t address_byte1;
  uint8_t address_byte0;
  uint8_t data_byte3;
  uint8_t data_byte2;
  uint8_t data_byte1;
  uint8_t data_byte0;
} sns_qfp_sd_32bit_write_t;

typedef PACK(struct)
{
  uint8_t opcode/*=0x84*/;
  uint8_t data_size/*=0x0*/; // 0 - represnts 32 bit data
  uint8_t dummy1/*=0x00*/;
  uint8_t num_trailing_bytes/*=0x0*/;
  uint8_t address_byte3;
  uint8_t address_byte2;
  uint8_t address_byte1;
  uint8_t address_byte0;
  uint8_t dummy2/*=0x00*/;
  uint8_t dummy3/*=0x00*/;
  uint8_t dummy4/*=0x00*/;
  uint8_t dummy5/*=0x00*/;
  uint8_t data_byte3;
  uint8_t data_byte2;
  uint8_t data_byte1;
  uint8_t data_byte0;
} sns_qfp_sd_32bit_read_t;

#elif (ENCODING_8BIT_BPW == 16)

typedef PACK(struct)
{
  uint8_t data_size/*=0x02*/; // 2 - represnts 8 bit data
  uint8_t opcode/*=0x84*/;
  uint8_t num_trailing_bytes/*=0x01*/;
  uint8_t dummy1/*=0x00*/;
  uint8_t address_byte2;
  uint8_t address_byte3;
  uint8_t address_byte0;
  uint8_t address_byte1;
  uint8_t trailing_dummy1/*=0x00*/; // Changes with the num of trailing dummies mentioned.
  uint8_t data;
  uint8_t trailing_dummy2 /*=0x00*/; // Changes with the no of trailing dummies mentioned.
  uint8_t trailing_dummy3 /*=0x00*/; // Changes with the no of trailing dummies mentioned.
  uint8_t trailing_dummy4 /*=0x00*/; // Changes with the no of trailing dummies mentioned.
  uint8_t trailing_dummy5 /*=0x00*/; // Changes with the no of trailing dummies mentioned.
  uint8_t trailing_dummy6 /*=0x00*/; // Changes with the no of trailing dummies mentioned.
  uint8_t trailing_dummy7 /*=0x00*/; // Changes with the no of trailing dummies mentioned.
} sns_qfp_sd_8bit_write_t;

typedef PACK(struct)
{
  uint8_t data_size/*=0x02*/; // 2 - represnts 8 bit data
  uint8_t opcode/*=0x83*/;
  uint8_t num_trailing_bytes/*=0x01*/;
  uint8_t dummy1/*=0x00*/;
  uint8_t address_byte2;
  uint8_t address_byte3;
  uint8_t address_byte0;
  uint8_t address_byte1;
  uint8_t dummy3/*=0x00*/;
  uint8_t dummy2/*=0x00*/;
  uint8_t dummy5/*=0x00*/;
  uint8_t dummy4/*=0x00*/;
  uint8_t data/*=0x00*/;
  uint8_t dummy6/*=0x00*/;
  uint8_t dummy7/*=0x00*/;
  uint8_t dummy8/*=0x00*/;
} sns_qfp_sd_8bit_read_t;

typedef PACK(struct)
{
  uint8_t data_size/*=0x0*/; // 0 - represnts 32 bit data
  uint8_t opcode/*=0x84*/;
  uint8_t num_trailing_bytes/*=0x0*/;
  uint8_t dummy1/*=0x00*/;
  uint8_t address_byte2;
  uint8_t address_byte3;
  uint8_t address_byte0;
  uint8_t address_byte1;
  uint8_t data_byte2;
  uint8_t data_byte3;
  uint8_t data_byte0;
  uint8_t data_byte1;
  uint8_t dummy2;
  uint8_t dummy3;
  uint8_t dummy4;
  uint8_t dummy5;
} sns_qfp_sd_32bit_write_t;

typedef PACK(struct)
{
  uint8_t data_size/*=0x0*/; // 0 - represnts 32 bit data
  uint8_t opcode/*=0x84*/;
  uint8_t num_trailing_bytes/*=0x0*/;
  uint8_t dummy1/*=0x00*/;
  uint8_t address_byte2;
  uint8_t address_byte3;
  uint8_t address_byte0;
  uint8_t address_byte1;
  uint8_t dummy3/*=0x00*/;
  uint8_t dummy2/*=0x00*/;
  uint8_t dummy5/*=0x00*/;
  uint8_t dummy4/*=0x00*/;
  uint8_t data_byte2;
  uint8_t data_byte3;
  uint8_t data_byte0;
  uint8_t data_byte1;
} sns_qfp_sd_32bit_read_t;

#else

#error "unknown 8bit SPI encoding"

#endif

#if (ENCODING_BLOCK_BPW == 8)

typedef PACK(struct)
{
  uint8_t opcode/*=0x86*/;
  uint8_t data_size/*=0x1*/; // 1 - represnts 16 bit data
  uint8_t dummy1/*=0x00*/;
  uint8_t num_trailing_bytes/*=0x2*/;
  uint8_t address_byte3;
  uint8_t address_byte2;
  uint8_t address_byte1;
  uint8_t address_byte0;
  uint8_t dummy2/*=0x00*/;
  uint8_t dummy3/*=0x00*/;
  uint8_t dummy4/*=0x00*/;
  uint8_t dummy5/*=0x00*/;
  uint8_t dummy6;
  uint8_t dummy7;
  uint8_t dummy8;
  uint8_t dummy9;
} sns_qfp_sd_block_read_header_t;

#elif (ENCODING_BLOCK_BPW == 16) 

typedef PACK(struct)
{
  uint8_t data_size/*=0x1*/; // 1 - represnts 16 bit data
  uint8_t opcode/*=0x86*/;
  uint8_t num_trailing_bytes/*=0x2*/;
  uint8_t dummy1/*=0x00*/;
  uint8_t address_byte2;
  uint8_t address_byte3;
  uint8_t address_byte0;
  uint8_t address_byte1;
  uint8_t dummy3/*=0x00*/;
  uint8_t dummy2/*=0x00*/;
  uint8_t dummy5/*=0x00*/;
  uint8_t dummy4/*=0x00*/;
  uint8_t dummy6;
  uint8_t dummy7;
  uint8_t dummy8;
  uint8_t dummy9;
} sns_qfp_sd_block_read_header_t;

#elif (ENCODING_BLOCK_BPW == 32) 

typedef PACK(struct)
{
  uint8_t num_trailing_bytes/*=0x2*/;
  uint8_t dummy1/*=0x00*/;
  uint8_t data_size/*=0x1*/; // 1 - represnts 16 bit data
  uint8_t opcode/*=0x86*/;
  uint8_t address_byte0;
  uint8_t address_byte1;
  uint8_t address_byte2;
  uint8_t address_byte3;
  uint8_t dummy5/*=0x00*/;
  uint8_t dummy4/*=0x00*/;
  uint8_t dummy3/*=0x00*/;
  uint8_t dummy2/*=0x00*/;
  uint8_t dummy6;
  uint8_t dummy7;
  uint8_t dummy8;
  uint8_t dummy9;
} sns_qfp_sd_block_read_header_t;

#else

#error "unknown block SPI encoding"

#endif

/*-------------------------------------------------------------------------
 * Externalized Function Definitions
 * ----------------------------------------------------------------------*/


/** 
 *  @brief Read the value of a 8 bit register from FP sensor
 *  
 *  @param[in] spi_handle handle for accessing SPI device
 *  @param[in] address register address
 *  @param[out] data data value read from register
 *  
 *  @return SNS_SUCCESS if completed successfully
 */
SNS_QFPSD_UIMAGE_CODE sns_err_code_e sns_qfp_sd_read_8bits(
  sns_qfp_sd_spi_handle_t handle, 
  uint32_t address, uint8_t* data);


/** 
 *  @brief Write a 8 bit register to FP sensor
 *  
 *  @param[in] spi_handle handle for accessing SPI device
 *  @param[in] address register address
 *  @param[out] data data value to write to register
 *  
 *  @return SNS_SUCCESS if completed successfully
 */
SNS_QFPSD_UIMAGE_CODE sns_err_code_e sns_qfp_sd_write_8bits(
  sns_qfp_sd_spi_handle_t handle, 
  uint32_t address, uint8_t data);

/** 
 *  @brief Read the value of a 32 bit register from FP sensor
 *  
 *  @param[in] spi_handle handle for accessing SPI device
 *  @param[in] address register address
 *  @param[out] data data value read from register
 *  
 *  @return SNS_SUCCESS if completed successfully
 */
sns_err_code_e sns_qfp_sd_read_32bits(
  sns_qfp_sd_spi_handle_t handle, 
  uint32_t address, uint32_t* data);

/** 
 *  @brief Write a 32 bit register to FP sensor
 *  
 *  @param[in] spi_handle handle for accessing SPI device
 *  @param[in] address register address
 *  @param[out] data data value to write to register
 *  
 *  @return SNS_SUCCESS if completed successfully
 */
sns_err_code_e sns_qfp_sd_write_32bits(
  sns_qfp_sd_spi_handle_t handle, 
  uint32_t address, uint32_t data);

/** 
 *  @brief Read a block of data from FP sensor
 *  
 *  @param[in] spi_handle handle for accessing SPI device
 *  @param[out] address address where data will be stored
 *  @param[in] size size of block (not including block header)
 *  
 *  @return SNS_SUCCESS if completed successfully
 */
sns_err_code_e sns_qfp_sd_read_block(
  sns_qfp_sd_spi_handle_t handle,
  uint8_t* address, uint32_t size);

#endif // _SNS_QFP_SD_REG_RW_H

