/*==============================================================================

    S E N S O R S   FP SENSOR FINGER DETECT  SPI D R I V E R

DESCRIPTION

    This file contains functions to read an image from the FP sensor over
    SPI bus.
 
Copyright (c) 2015 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential
==============================================================================*/
/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/qfp.slpi/1.0/sd/inc/sns_qfp_sd_image.h#1 $


when         who     what, where, why
----------   ---     -----------------------------------------------------------
08/12/15     LD      Initial version based on OntarioImage.h
==============================================================================*/

#ifndef _SNS_QFP_SD_IMAGE_H
#define _SNS_QFP_SD_IMAGE_H

#include "sns_common.h"
#include "sns_qfp_sd_spi_base.h"
#include "sns_qfp_priv.h"

// size of phase 1 small image in pixels
#define QFP_SD_S1_IMAGE_SIZE    270
// size of phase 2 small image in pixels
#define QFP_SD_S2_IMAGE_SIZE    1782
// size of temperature data in image buffer
#define TEMPERATURE_DATA_SIZE   36

// TODO get the following from property
// temperature offset used for temperature calculation
#define TEMPERATURE_OFFSET      451.47
// temperature slope used for temperature calculation
#define TEMPERATURE_SLOPE       -586.33

/**
 * opaque handle with context information for read image data 
 * (rid) function 
 */
typedef void* sns_qfp_sd_rid_handle_t;

/**
 * @brief Allocate context handle for reading image data 
 * @param[out] handle will be allocated 
 * @return SNS_SUCCESS if successful 
 */
sns_err_code_e sns_qfp_sd_rid_alloc_handle(
  sns_qfp_sd_rid_handle_t* handle
);

/**
 * @brief Deallocate context handle for reading image data 
 * @param[in] handle 
 *  
 * @note handle is deallocated and must not be used again 
 */
void sns_qfp_sd_rid_free_handle(
  sns_qfp_sd_rid_handle_t handle
);

/** 
 *  @brief Read image data from FP sensor
 *  
 *  @param[in] rid_handle context for read image data operation
 *  @param[in] spi_handle handle for accessing SPI device
 *  @param[in] image_pixels requested image size in pixels
 *  @param[out] buffer where raw image data will be stored
 *  @param[in] buffer_size size of buffer in bytes
 *  @param[out] image_size will be filled with output image size
 *        in bytes
 *  
 *  @return SNS_SUCCESS if completed successfully
 *  @note image_size must be one of the supported image sizes
 */
SNS_QFPSD_UIMAGE_CODE sns_err_code_e sns_qfp_sd_read_image_data(
  sns_qfp_sd_rid_handle_t rid_handle,
  sns_qfp_sd_spi_handle_t spi_handle,
  uint32_t image_pixels,
  uint8_t* buffer,
  uint32_t buffer_size,
  uint32_t* image_size
);

#endif // _SNS_QFP_SD_IMAGE_H

