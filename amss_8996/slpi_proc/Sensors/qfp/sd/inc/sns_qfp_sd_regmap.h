/*==============================================================================

    S E N S O R S   FP SENSOR FINGER DETECT  SPI D R I V E R

DESCRIPTION

   This file contains the register maps of the ASIC registers
   (analog, digital and SPI)
   It is a copy of OntarioRegMap.h from stargate TZ SPI driver


Copyright (c) 2015 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential
==============================================================================*/
/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/qfp.slpi/1.0/sd/inc/sns_qfp_sd_regmap.h#1 $


when         who     what, where, why
----------   ---     -----------------------------------------------------------
05/10/15     LD      Initial version copied from OntarioRegMap.h
==============================================================================*/

#ifndef _SNS_QFP_SD_REGMAP_H
#define _SNS_QFP_SD_REGMAP_H

//////////////////////////////
// Analog Registers and bit fileds //
//////////////////////////////

// Analog register addresses

#define REG0_TX                   0x10
#define REG1_TX                   0x11 
#define REG2_TX                   0x12 
#define REG3_TX                   0x13 
#define REG4_TX                   0x14 
#define REG0_BST                  0x15 
#define REG1_BST                  0x16 
#define REG2_BST                  0x17 
#define REG3_BST                  0x18 
#define REG4_BST                  0x19 
#define REG5_BST                  0x1A 
#define REG6_BST                  0x1B 
#define REG1_CLK                  0x1D
#define TRIM10_TX                 0x108
#define TRIM11_TX                 0x109
#define TRIM20_TX                 0x10A
#define TRIM21_TX                 0x10B
#define TRIM30_TX                 0x10C
#define TRIM31_TX                 0x10D
#define T20V_LDO_TEST0            0xC0 
#define T20V_LDO_TEST1            0xC1 
#define T20V_LDO_V                0xC2
#define VLOGIC_LDO_TEST0          0x8C 
#define VLOGIC_LDO_TEST1          0x8D 
#define VLOGIC_LDO_V              0x8E 
#define VGATE_LDO_TEST0           0x86 
#define VGATE_LDO_TEST1           0x87 
#define VGATE_LDO_V               0x88 
#define SPARE_LDO_TEST0           0x80 
#define SPARE_LDO_TEST1           0x81 
#define SPARE_LDO_V               0x82 
#define AP_LDO_TEST0              0xA2 
#define AP_LDO_TEST1              0xA3 
#define AP_LDO_V                  0xA4 
#define DRESETV_LDO_TEST0         0xA8 
#define DRESETV_LDO_TEST1         0xA9 
#define DRESETV_LDO_V             0xAA 
#define DBIAS_TEST0               0xB0 
#define DBIAS_TEST1               0xB1 
#define DBIAS_V1                  0xB2 
#define DBIAS_V2                  0xB3 
#define DBIAS_V3                  0xB4 
#define DBIAS_V4                  0xB5 
#define RBIAS_TEST0               0xB8 
#define RBIAS_TEST1               0xB9 
#define RBIAS_V1                  0xBA 
#define RBIAS_V2                  0xBB 
#define RBIAS_V3                  0xBC 
#define RBIAS_V4                  0xBD 
#define TFT_BIAS_CNTL             0x30 
#define TS_REG0                   0x34 
#define TS_REG1                   0x35 
#define TS_REG2                   0x36 
#define TS_REG3                   0x37 
#define ADC_REG0                  0x38 
#define ADC_REG1                  0x39 
#define ADC_REG2                  0x3A 
#define ADC_REG3                  0x3B 
#define ADC_REG4                  0x3C 
#define ADC_REG5                  0x3D 
#define ADC_REG6                  0x3E 
#define ADC_REG7                  0x3F 
#define ADC_REG8                  0x40 
#define ADC_REG9                  0x41 
#define ADC_REG10                 0x42 
#define ADC_REG11                 0x43 
#define ADC_REG12                 0x44 
#define ADC_REG13                 0x45 
#define ADC_REG14                 0x46 
#define ADC_REG15                 0x47 
#define ADC_REG16                 0x48 
#define ADC_REG17                 0x49 
#define ADC_REG18                 0x4A 
#define ADC_REG19                 0x4B 
#define ADC_REG20                 0x4C
#define REG0_CLK                  0x1C 
#define TRIM_RCLDO                0x10E
#define TRIM_CLK                  0x10F
#define REG0_MISC                 0x50 
#define REG1_MISC                 0x51 
#define REG2_MISC                 0x52 
#define CHIP_ID_0                 0x53 
#define CHIP_ID_1                 0x54 
#define CHIP_ID_2                 0x55 
#define CHIP_ID_3                 0x56 
#define CHIP_STATUS               0x57 
#define REG0_MTRIM                0x111
#define REG1_MTRIM                0x112
#define REG2_MTRIM                0x113
#define REG3_MTRIM                0x114
#define REG4_MTRIM                0x115
#define REG5_MTRIM                0x116
#define REG6_MTRIM                0x117
#define REG7_MTRIM                0x118
#define REG8_MTRIM                0x119
#define REG9_MTRIM                0x11A
#define REG10_MTRIM               0x11B
#define REG11_MTRIM               0x11C
#define REG12_MTRIM               0x11D
#define REG13_MTRIM               0x11E
#define REG14_MTRIM               0x11F
#define REG15_MTRIM               0x120
#define REG16_MTRIM               0x121
#define REG17_MTRIM               0x122
#define REG18_MTRIM               0x123
#define REG19_MTRIM               0x124
#define TRIM_START                0x17F
#define MBG_TEST                  0x78 
#define MBG_CTRL                  0x77 
#define TRIM_REGS                 0x100
#define MBG_TRIM1                 0x100
#define MBG_TRIM2                 0x101
#define MBG_TRIM3                 0x102
#define MBG_TRIM4                 0x103
#define MBG_TRIM5                 0x104
#define MBG_TRIM6                 0x105
#define MBG_TRIM7                 0x106
#define TEMP_CNTL                 0x7A 
#define TEMP_PWM                  0x7B 
#define TEMP_TEST1                0x7C 
#define TEMP_TEST2                0x7D 
#define TEMP_TRIM                 0x107
#define TPP1_CNTL                 0x90 
#define TPP2_CNTL                 0x91 
#define TPP3_CNTL                 0x92 
#define TPP4_CNTL                 0x93 
#define TPP5_CNTL                 0x94 
#define TPP6_CNTL                 0x95

// Bit fileds

#define    REG0_TX_RC_CAP_CTL_0    0xFF<<0
#define    REG1_TX_RC_IB_CTL    0x7<<2
#define    REG1_TX_RC_CAP_CTL_1    0x3<<0
#define    REG2_TX_OSC_CTL    0x1<<7
#define    REG2_TX_NLAP_CTL    0x3<<5
#define    REG2_TX_H_BRIDG_START    0x1<<4
#define    REG2_TX_H_BRIDG_AFTER    0x3<<2
#define    REG2_TX_H_BRIDG_BEFOR    0x3<<0
#define    REG3_TX_DTEST_ON_STBY    0x1<<6
#define    REG3_TX_DTEST_P_N    0x1<<5
#define    REG3_TX_DTEST_RC    0x1<<4
#define    REG3_TX_DTEST_REPLACE_RC    0x3<<2
#define    REG3_TX_DTEST_FREQ    0x3<<0
#define    REG4_TX_IB_CTL    0x1<<4
#define    REG4_TX_ORIDE_STBY    0x3<<2
#define    REG4_TX_ORIDE_ON    0x3<<0
#define    REG0_BST_BST_TX_BLANK    0x1<<7
#define    REG0_BST_EN_ADA_NOFF    0x1<<6
#define    REG0_BST_DISEN_ISAT_SNS    0x1<<5
#define    REG0_BST_DISEN_MIN_NOFF    0x1<<4
#define    REG0_BST_DISEN_DC_CANCEL    0x1<<3
#define    REG0_BST_DISEN_INLMT    0x1<<2
#define    REG0_BST_DISEN_SELF_CLK    0x1<<1
#define    REG0_BST_DISEN_LDO    0x1<<0
#define    REG1_BST_FORCE_NOFF_DLY    0x3<<6
#define    REG1_BST_FORCE_NON_DLY    0x3<<4
#define    REG1_BST_MIN_NON_DLY    0x3<<2
#define    REG1_BST_NFET_DLY    0x3<<0
#define    REG2_BST_SEL_VOUT_VLDO    0x1<<7
#define    REG2_BST_SEL_NON_BLANK    0x1<<6
#define    REG2_BST_SEL_IB_REF    0x1<<5
#define    REG2_BST_VOUT_RES    0x1F<<0
#define    REG3_BST_ISAT_RES    0x3<<6
#define    REG3_BST_CFG_LOCKUP    0x1<<5
#define    REG3_BST_BST_REG_BLANK    0x1<<4
#define    REG3_BST_INLMT_PROG    0xF<<0
#define    REG4_BST_TEST_MODE    0x3<<6
#define    REG4_BST_DTEST2_SEL    0x7<<3
#define    REG4_BST_DTEST1_SEL    0x7<<0
#define    REG5_BST_ISNS_DIV    0x7<<5
#define    REG5_BST_LDO_PROG    0x3<<3
#define    REG5_BST_ATEST_SEL    0x7<<0
#define    TRIM10_TX_BURST_FREQ_TRIM10    0xFF<<0
#define    TRIM11_TX_BURST_FREQ_TRIM11    0xFF<<0
#define    TRIM20_TX_BURST_FREQ_TRIM20    0xFF<<0
#define    TRIM21_TX_BURST_FREQ_TRIM21    0xFF<<0
#define    TRIM30_TX_BURST_FREQ_TRIM30    0xFF<<0
#define    TRIM31_TX_BURST_FREQ_TRIM31    0xFF<<0
#define    T20V_LDO_TEST1_ORIDE_EN    0x3<<6
#define    T20V_LDO_TEST3_CURR    0x3<<0
#define    T20V_LDO_TEST3_ATEST_SEL    0x7<<0
#define    T20V_LDO_V_V1    0x3F<<0
#define    VLOGIC_LDO_TEST1_ORIDE_EN    0x3<<6
#define    VLOGIC_LDO_TEST3_CURR    0x3<<0
#define    VLOGIC_LDO_TEST3_ATEST_SEL    0x7<<0
#define    VLOGIC_LDO_V_V1    0x3F<<0
#define    VGATE_LDO_TEST1_ORIDE_EN    0x3<<6
#define    VGATE_LDO_TEST3_CURR    0x3<<0
#define    VGATE_LDO_TEST3_ATEST_SEL    0x7<<0
#define    VGATE_LDO_V_V1    0x3F<<0
#define    AP_LDO_TEST1_ORIDE_EN    0x3<<6
#define    AP_LDO_TEST3_CURR    0x3<<0
#define    AP_LDO_TEST3_ATEST_SEL    0x7<<0
#define    AP_LDO_V_V1    0x3F<<0
#define    DRESETV_LDO_TEST1_ORIDE_EN    0x3<<6
#define    DRESETV_LDO_TEST3_CURR    0x3<<0
#define    DRESETV_LDO_TEST3_ATEST_SEL    0x7<<0
#define    DRESETV_LDO_V_V1    0x3F<<0
#define    DBIAS_TEST1_ORIDE_EN    0x3<<6
#define    DBIAS_TEST2_ORIDE_ISEL    0x3<<4
#define    DBIAS_TEST3_HIGH_CURR    0x3<<2
#define    DBIAS_TEST4_LOW_CURR    0x3<<0
#define    DBIAS_TEST2_ORIDE_VSEL    0x1<<7
#define    DBIAS_TEST4_Csel    0x1<<4
#define    DBIAS_TEST5_DTEST_VSEL    0x1<<3
#define    DBIAS_TEST6_ATEST_SEL    0x7<<0
#define    DBIAS_V3_V1    0x3F<<0
#define    DBIAS_V4_V2    0x3F<<0
#define    DBIAS_V5_V3    0x3F<<0
#define    DBIAS_V6_V4    0x3F<<0
#define    RBIAS_TEST1_ORIDE_EN    0x3<<6
#define    RBIAS_TEST2_ORIDE_ISEL    0x3<<4
#define    RBIAS_TEST3_HIGH_CURR    0x3<<2
#define    RBIAS_TEST4_LOW_CURR    0x3<<0
#define    RBIAS_TEST2_ORIDE_VSEL    0x1<<7
#define    RBIAS_TEST4_Csel    0x1<<4
#define    RBIAS_TEST5_DTEST_VSEL    0x1<<3
#define    RBIAS_TEST6_ATEST_SEL    0x7<<0
#define    RBIAS_V3_V1    0x3F<<0
#define    RBIAS_V4_V2    0x3F<<0
#define    RBIAS_V5_V3    0x3F<<0
#define    RBIAS_V6_V4    0x3F<<0
#define    TFT_BIAS_CNTL_SPARE    0x7<<5
#define    TFT_BIAS_CNTL_TFT_BIAS_CB    0x1<<4
#define    TFT_BIAS_CNTL_TFT_BIAS    0xF<<0
#define    TS_REG1_IDAC_AUX    0x1<<7
#define    TS_REG2_VBUF_BIAS    0x3<<5
#define    TS_REG3_IDAC_BIAS    0x3<<3
#define    TS_REG4_IDAC_MULT    0x7<<0
#define    TS_REG2_IDAC_LOCAL    0x1<<7
#define    TS_REG3_OUT_SWITCH    0x1<<6
#define    TS_REG4_NBUF_FILT    0x1<<5
#define    TS_REG5_NBUF_IN_SWITCH    0x3<<3
#define    TS_REG6_PBUF_FILT    0x1<<2
#define    TS_REG7_PBUF_IN_SWITCH    0x3<<0
#define    TS_REG3_SPARE    0x3F<<2
#define    TS_REG4_IDAC_MODE    0x3<<0
#define    TS_REG4_SPARE    0xF<<4
#define    TS_REG5_ATEST_CONTROL    0x3<<2
#define    TS_REG6_ATEST_EN    0x1<<1
#define    TS_REG7_SLEEP_CONTROL    0x1<<0
#define    ADC_REG2_OTA1_ADJ    0x3<<3
#define    ADC_REG3_OTA2_ADJ    0x3<<1
#define    ADC_REG4_CMP_ADJ    0x1<<0
#define    ADC_REG2_ATEST2_SEL    0x7<<5
#define    ADC_REG3_CMP_ERR_DIS    0x1<<4
#define    ADC_REG4_AtEST1_SEL    0x7<<1
#define    ADC_REG5_DEM_DIS    0x1<<0
#define    ADC_REG4_S2    0x1<<1
#define    ADC_REG5_S1    0x1<<0
#define    ADC_REG4_AAF6_TO_ATEST    0x1<<7
#define    ADC_REG5_ADC6_TO_VOCM    0x1<<6
#define    ADC_REG6_AAF5_TO_ATEST    0x1<<5
#define    ADC_REG7_ADC5_TO_VOCM    0x1<<4
#define    ADC_REG8_SENS_BIAS_CTL    0xF<<0
#define    ADC_REG5_RX_DTEST_CTL0    0x1<<7
#define    ADC_REG6_RX_DTEST_CTL1    0x3F<<1
#define    ADC_REG7_RX_DTEST_CTL2    0x1<<0
#define    ADC_REG6_ADC4_EN    0x3<<6
#define    ADC_REG7_ADC3_EN    0x3<<4
#define    ADC_REG8_ADC2_EN    0x3<<2
#define    ADC_REG9_ADC1_EN    0x3<<0
#define    ADC_REG7_ALEV2_EN    0x3<<6
#define    ADC_REG8_ALEV1_EN    0x3<<4
#define    ADC_REG9_ADC6_EN    0x3<<2
#define    ADC_REG10_ADC5_EN    0x3<<0
#define    ADC_REG8_ALEV6_EN    0x3<<6
#define    ADC_REG9_ALEV5_EN    0x3<<4
#define    ADC_REG10_ALEV4_EN    0x3<<2
#define    ADC_REG11_ALEV3_EN    0x3<<0
#define    ADC_REG10_EN_TFTbias    0x1<<4
#define    ADC_REG11_EN_LDO    0x3<<2
#define    ADC_REG12_EN_REF    0x3<<0
#define    ADC_REG11_INP_PAIR_HROOM    0x1<<6
#define    ADC_REG12_HAMP_CURR    0x3<<4
#define    ADC_REG13_ADC_CM    0xF<<0
#define    ADC_REG12_LS_VB_CURR    0x1<<6
#define    ADC_REG13_MAST_OTA_CURR    0x3<<4
#define    ADC_REG14_DACREF_CURR    0x1<<3
#define    ADC_REG15_VOCM_BUF_CURR    0x1<<2
#define    ADC_REG16_EN_DTEST    0x1<<1
#define    ADC_REG17_EN_ATEST    0x1<<0
#define    ADC_REG12_CMP_REF    0xF<<4
#define    ADC_REG13_DAC_REF    0xF<<0
#define    ADC_REG14_VOCM_DRV    0x3<<4
#define    ADC_REG15_VOCM    0x3<<2
#define    ADC_REG16_LDO_VOUT    0x3<<0
#define    ADC_REG14_AAF4_TO_ATEST    0x1<<7
#define    ADC_REG15_ADC4_TO_VOCM    0x1<<6
#define    ADC_REG16_AAF3_TO_ATEST    0x1<<5
#define    ADC_REG17_ADC3_TO_VOCM    0x1<<4
#define    ADC_REG18_AAF2_TO_ATEST    0x1<<3
#define    ADC_REG19_ADC2_TO_VOCM    0x1<<2
#define    ADC_REG20_AAF1_TO_ATEST    0x1<<1
#define    ADC_REG21_ADC1_TO_VOCM    0x1<<0
#define    ADC_REG16_DRESETV_SW_EN    0x1<<5
#define    ADC_REG17_PULSE_WIDTH_CTL    0x1<<4
#define    ADC_REG18_VB    0xF<<0
#define    ADC_REG17_DresetV_sw_EN    0x1<<0
#define    ADC_REG17_VDDSW_SEL    0x3<<6
#define    ADC_REG18_VDDTIA_SEL    0x3<<4
#define    ADC_REG19_VDDRXLS_SEL    0x3<<2
#define    ADC_REG20_EN_CI_HV_SEL    0x3<<0
#define    ADC_REG18_TIA_CAP_CTRL    0xF<<4
#define    ADC_REG19_TIA_RES_CTRL    0xF<<0
#define    ADC_REG20_TIA_BIAS_CTRL    0x3<<4
#define    ADC_REG21_VB2_SEL    0xF<<0
#define    ADC_REG20_S2_CTRL    0xF<<4
#define    ADC_REG21_S1_CTRL    0xF<<0
#define    ADC_REG21_INTG_TIAB    0x1<<7
#define    ADC_REG22_EN_VBOS_TEST    0x1<<6
#define    ADC_REG23_EN_VDDTIA_TEST    0x1<<5
#define    ADC_REG24_EN_S12_BOARD    0x1<<4
#define    ADC_REG25_EN_S12_TEST    0x1<<3
#define    ADC_REG26_EN_TIA_TEST    0x1<<2
#define    ADC_REG27_EN_SELF_TIMING    0x1<<1
#define    ADC_REG28_EN_CI    0x1<<0
#define    REG0_CLK_CODE_LAT    0x1<<7
#define    REG0_CLK_DTEST_EN    0x1<<6
#define    REG0_CLK_ADC_CLK    0x1<<5
#define    REG0_CLK_TX_FSM_CLK    0x1<<4
#define    REG0_CLK_DTEST_RC    0x1<<3
#define    REG0_CLK_DTEST_REPLACE_RC    0x1<<2
#define    REG0_CLK_DTEST_SEL    0x3<<0
#define    REG1_CLK_CLK_DELAY    0x3<<5
#define    REG1_CLK_CLK_DIV_BOOST    0x3<<3
#define    REG1_CLK_LDO_VAL    0x3<<1
#define    REG1_CLK_LDO_MODE    0x1<<0
#define    TRIM_CLK_RC_CAP_CTL    0xFF<<0
#define    REG0_MISC_TX_EN    0x1<<7
#define    REG0_MISC_TEMP_EN    0x1<<6
#define    REG0_MISC_COL_EN    0x1<<5
#define    REG0_MISC_LDO20_EN    0x1<<4
#define    REG0_MISC_LDO_EN    0x1<<3
#define    REG0_MISC_MBG_EN    0x1<<2
#define    REG0_MISC_RX_EN    0x1<<1
#define    REG2_MISC_SOFT_RST    0xFF<<0
#define    CHIP_ID_1_BYTE0    0xFF<<0
#define    CHIP_ID_2_BYTE1    0xFF<<0
#define    CHIP_ID_3_BYTE2    0xFF<<0
#define    CHIP_ID_4_BYTE3    0xFF<<0
#define    CHIP_STATUS_BYTE3    0x1<<0
#define    BOOST_STATUS_REGOK    0x1<<3
#define    BOOST_STATUS_LOCKUP    0x1<<2
#define    BOOST_STATUS_INSATDET    0x1<<1
#define    BOOST_STATUS_LDO_COMP    0x1<<0
#define    REG0_MTRIM_ID_VAL    0xFF<<0
#define    REG1_MTRIM_ID_VAL    0xFF<<0
#define    REG2_MTRIM_ID_VAL    0xFF<<0
#define    REG3_MTRIM_ID_VAL    0xFF<<0
#define    REG4_MTRIM_ID_VAL    0xFF<<0
#define    REG5_MTRIM_ID_VAL    0xFF<<0
#define    REG6_MTRIM_ID_VAL    0xFF<<0
#define    REG7_MTRIM_ID_VAL    0xFF<<0
#define    REG8_MTRIM_ID_VAL    0xFF<<0
#define    REG9_MTRIM_ID_VAL    0xFF<<0
#define    REG10_MTRIM_ID_VAL    0xFF<<0
#define    REG11_MTRIM_ID_VAL    0xFF<<0
#define    REG12_MTRIM_ID_VAL    0xFF<<0
#define    REG13_MTRIM_ID_VAL    0xFF<<0
#define    REG14_MTRIM_ID_VAL    0xFF<<0
#define    REG15_MTRIM_ID_VAL    0xFF<<0
#define    REG16_MTRIM_ID_VAL    0xFF<<0
#define    REG17_MTRIM_ID_VAL    0xFF<<0
#define    REG18_MTRIM_ID_VAL    0xFF<<0
#define    REG19_MTRIM_ID_VAL    0xFF<<0
#define    TRIM_START_TRM_START    0xFF<<0
#define    MBG_TEST_REG_FIELD    0xFF<<0
#define    MBG_CTRL_REG_FIELD    0xFF<<0
#define    MBG_TRIM2_REG_FIELD    0xFF<<0
#define    MBG_TRIM3_REG_FIELD    0xFF<<0
#define    MBG_TRIM4_REG_FIELD    0xFF<<0
#define    MBG_TRIM5_REG_FIELD    0xFF<<0
#define    MBG_TRIM6_REG_FIELD    0xFF<<0
#define    MBG_TRIM7_REG_FIELD    0xFF<<0
#define    MBG_TRIM8_REG_FIELD    0xFF<<0
#define    TEMP_CNTL_REG_FIELD    0xFF<<0
#define    TEMP_PWM_REG_FIELD    0xFF<<0
#define    TEMP_TEST2_REG_FIELD    0xFF<<0
#define    TEMP_TEST3_REG_FIELD    0xFF<<0
#define    TEMP_TRIM_REG_FIELD    0xFF<<0
#define    TPP1_CNTL_REG_FIELD    0xFF<<0
#define    TPP2_CNTL_REG_FIELD    0xFF<<0
#define    TPP3_CNTL_REG_FIELD    0xFF<<0
#define    TPP4_CNTL_REG_FIELD    0xFF<<0
#define    TPP5_CNTL_REG_FIELD    0xFF<<0
#define    TPP6_CNTL_REG_FIELD    0xFF<<0
#define    TRIM_REGS_REG_FIELD    0xFF<<0
#define    REG1_CLK_LDO_MODE_ENABLE     0x1
#define    REG1_CLK_LDO_MODE_DISABLE     0x0


//////////////////////////////
// Digital Registers and bit fileds //
//////////////////////////////

#define DIGITAL_REG_OFFSET                                    0x200

// Digital registers
#define    CHIP_CTL                                              DIGITAL_REG_OFFSET+0x00
#define    SPROT_CTL_1                                           DIGITAL_REG_OFFSET+0x04
#define    SPROT_CTL_2                                           DIGITAL_REG_OFFSET+0x05
#define    PAD_HDRIVE_CTL                                        DIGITAL_REG_OFFSET+0x06
#define    CLK_REQ_CTL                                           DIGITAL_REG_OFFSET+0x07
#define    INTR_CTL                                              DIGITAL_REG_OFFSET+0x0B
#define    INTR_EN                                               DIGITAL_REG_OFFSET+0x0C
#define    INTR_STATUS                                           DIGITAL_REG_OFFSET+0x0D
#define    INTR_CLR                                              DIGITAL_REG_OFFSET+0x0E
#define    INTR_SET                                              DIGITAL_REG_OFFSET+0x0F
#define    FILT_CTL_1                                            DIGITAL_REG_OFFSET+0x10
#define    FILT_CTL_2                                            DIGITAL_REG_OFFSET+0x11
#define    FILT_CTL_3                                            DIGITAL_REG_OFFSET+0x12
#define    FILT_CTL_4                                            DIGITAL_REG_OFFSET+0x13
#define    FILT_CTL_5                                            DIGITAL_REG_OFFSET+0x14
#define    START_FILT                                            DIGITAL_REG_OFFSET+0x15
#define    FILT_CTL_6                                            DIGITAL_REG_OFFSET+0x16
#define    GO1_VAL                                               DIGITAL_REG_OFFSET+0x21
#define    GO2_VAL                                               DIGITAL_REG_OFFSET+0x22
#define    GO4_VAL                                               DIGITAL_REG_OFFSET+0x24
#define    TX_SM_OVERRIDE_CTL                                    DIGITAL_REG_OFFSET+0x25
#define    TX_SM_OVERRIDE_VAL                                    DIGITAL_REG_OFFSET+0x26
#define    FIFO_MGR_CTL                                          DIGITAL_REG_OFFSET+0x30
#define    FIFO_STATUS                                           DIGITAL_REG_OFFSET+0x31
#define    FIFO_RD_PTR_LSB                                       DIGITAL_REG_OFFSET+0x32
#define    FIFO_RD_PTR_MSB                                       DIGITAL_REG_OFFSET+0x33
#define    FIFO_WR_PTR_LSB                                       DIGITAL_REG_OFFSET+0x34
#define    FIFO_WR_PTR_MSB                                       DIGITAL_REG_OFFSET+0x35
#define    LAST_FILT_DATA_MSB_n                                  DIGITAL_REG_OFFSET+0x40
#define    LAST_FILT_DATA_LSB_n                                  DIGITAL_REG_OFFSET+0x46
#define    DEBUG_CTL                                             DIGITAL_REG_OFFSET+0x4c
#define    TRIM_CTL                                              DIGITAL_REG_OFFSET+0x50
#define    TRIM_STATUS                                           DIGITAL_REG_OFFSET+0x51
#define    TX_SM_CTL                                             DIGITAL_REG_OFFSET+0x60
#define    START_TX                                              DIGITAL_REG_OFFSET+0x61
#define    TX_SM_OUTVAL_n                                        DIGITAL_REG_OFFSET+0x62
#define    TX_SM_DURATION_n                                      DIGITAL_REG_OFFSET+0x83
#define    BIST_CTL                                              DIGITAL_REG_OFFSET+0xB0
#define    BIST_START                                            DIGITAL_REG_OFFSET+0xB1
#define    BIST_STATUS                                           DIGITAL_REG_OFFSET+0xB2
#define    DMEM_WDATA_MSB_n                                      DIGITAL_REG_OFFSET+0xB4
#define    DMEM_WDATA_LSB_n                                      DIGITAL_REG_OFFSET+0xBA
#define    SEQ_CTL_1                                             DIGITAL_REG_OFFSET+0xE0
#define    SEQ_CTL_2                                             DIGITAL_REG_OFFSET+0xE1
#define    SEQ_CTL_3                                             DIGITAL_REG_OFFSET+0xE2
#define    START_SEQ                                             DIGITAL_REG_OFFSET+0xE4
#define    PC_LSB                                                DIGITAL_REG_OFFSET+0xE6
#define    PC_MSB                                                DIGITAL_REG_OFFSET+0xE7

// Digital register bit fileds

#define    CHIP_CTL_LCLK_GATE_EN    0x1<<0
#define    SPROT_CTL_2_VAL    0xFF<<0
#define    SPROT_CTL_3_VAL    0xFF<<0
#define    PAD_HDRIVE_CTL_BUFFER_STRENGTH    0x3<<0
#define    CLK_REQ_CTL_SHUTOFF    0x1<<0
#define    INTR_CTL_POLARITY    0x1<<0
#define    INTR_EN_BIST_PASS    0x1<<5
#define    INTR_EN_SEQ_DONE    0x1<<4
#define    INTR_EN_SPI_INT    0x1<<3
#define    INTR_EN_FIFO_EMPTY    0x1<<2
#define    INTR_EN_FIFO_FULL    0x1<<1
#define    INTR_EN_FILT_DONE    0x1<<0
#define    INTR_STATUS_BIST_PASS    0x1<<5
#define    INTR_STATUS_SEQ_DONE    0x1<<4
#define    INTR_STATUS_SPI_INT    0x1<<3
#define    INTR_STATUS_FIFO_EMPTY    0x1<<2
#define    INTR_STATUS_FIFO_FULL    0x1<<1
#define    INTR_STATUS_FILT_DONE    0x1<<0
#define    INTR_CLR_BIST_PASS    0x1<<5
#define    INTR_CLR_SEQ_DONE    0x1<<4
#define    INTR_CLR_SPI_INT    0x1<<3
#define    INTR_CLR_FIFO_EMPTY    0x1<<2
#define    INTR_CLR_FIFO_FULL    0x1<<1
#define    INTR_CLR_FILT_DONE    0x1<<0
#define    INTR_SET_BIST_PASS    0x1<<5
#define    INTR_SET_SEQ_DONE    0x1<<4
#define    INTR_SET_SPI_INT    0x1<<3
#define    INTR_SET_FIFO_EMPTY    0x1<<2
#define    INTR_SET_FIFO_FULL    0x1<<1
#define    INTR_SET_FILT_DONE    0x1<<0
#define    FILT_CTL_2_TRUNC_BITS    0x3<<6
#define    FILT_CTL_3_PREFILL_DELAY    0x7<<3
#define    FILT_CTL_4_DYN_CG_EN    0x1<<2
#define    FILT_CTL_5_ENABLE    0x1<<1
#define    FILT_CTL_6_FILT_SW_RESET    0x1<<0
#define    FILT_CTL_3_DECM_FACTOR    0x3F<<2
#define    FILT_CTL_4_ADC6_CAPT_EDGE    0x1<<5
#define    FILT_CTL_5_ADC5_CAPT_EDGE    0x1<<4
#define    FILT_CTL_6_ADC4_CAPT_EDGE    0x1<<3
#define    FILT_CTL_7_ADC3_CAPT_EDGE    0x1<<2
#define    FILT_CTL_8_ADC2_CAPT_EDGE    0x1<<1
#define    FILT_CTL_9_ADC1_CAPT_EDGE    0x1<<0
#define    FILT_CTL_5_ADD_VAL_MSB    0xFF<<0
#define    FILT_CTL_6_ADD_VAL_LSB    0xFF<<0
#define    START_FILT_KICKOFF    0x1<<0
#define    FILT_CTL_7_SHIFT_BITS    0xF<<0
#define    GO1_VAL_VAL    0xFF<<0
#define    GO2_VAL_VAL    0xFF<<0
#define    GO4_VAL_VAL    0xFF<<0
#define    TX_SM_OVERRIDE_CTL_TX_SM_OVERRIDE_EN    0x1<<0
#define    TX_SM_OVERRIDE_VAL_VAL    0xFF<<0
#define    FIFO_MGR_CTL_DATA_SRC    0x1<<3
#define    FIFO_MGR_CTL_PACK_MODE    0x1<<2
#define    FIFO_MGR_CTL_ENABLE    0x1<<1
#define    FIFO_MGR_CTL_RESET    0x1<<0
#define    FIFO_STATUS_FIFO_FULL    0x1<<1
#define    FIFO_STATUS_FIFO_EMPTY    0x1<<0
#define    FIFO_RD_PTR_LSB_VAL    0xFF<<0
#define    FIFO_RD_PTR_MSB_VAL    0xFF<<0
#define    FIFO_WR_PTR_LSB_VAL    0xFF<<0
#define    FIFO_WR_PTR_MSB_VAL    0xFF<<0
#define    LAST_FILT_DATA_MSB_n_VAL    0xFF<<0
#define    LAST_FILT_DATA_LSB_n_VAL    0xFF<<0
#define    DEBUG_CTL_SEL    0xFF<<0
#define    TRIM_CTL_ENABLE    0x1<<6
#define    TRIM_CTL_OTP_TST_MODE_SEL    0x3<<4
#define    TRIM_CTL_CTL    0x3<<2
#define    TRIM_CTL_VPP_SEL    0x1<<1
#define    TRIM_CTL_PE    0x1<<0
#define    TRIM_STATUS_STS    0x7<<0
#define    TX_SM_CTL_ENABLE    0x1<<1
#define    TX_SM_CTL_SW_RESET    0x1<<0
#define    START_TX_KICKOFF    0x1<<0
#define    TX_SM_OUTVAL_n_VAL    0xFF<<0
#define    TX_SM_DURATION_n_DURATION    0xFF<<0
#define    BIST_CTL_ENABLE    0x1<<1
#define    BIST_CTL_SW_RESET    0x1<<0
#define    BIST_START_KICKOFF    0x1<<0
#define    BIST_STATUS_IMEMBIST_PASS    0x1<<3
#define    BIST_STATUS_DMEMBIST_PASS    0x1<<2
#define    BIST_STATUS_IMEMBIST_DONE    0x1<<1
#define    BIST_STATUS_DMEMBIST_DONE    0x1<<0
#define    DMEM_WDATA_MSB_n_VAL    0xFF<<0
#define    DMEM_WDATA_LSB_n_VAL    0xFF<<0
#define    SEQ_CTL_2_IMEM_ACCESSOR     0x1<<3
#define    SEQ_CTL_3_STEP    0x1<<2
#define    SEQ_CTL_4_ENABLE    0x1<<1
#define    SEQ_CTL_5_SW_RESET    0x1<<0
#define    SEQ_CTL_3_PROG_LOC_LSB    0xFF<<0
#define    SEQ_CTL_4_PROG_LOC_MSB    0x7<<0
#define    START_SEQ_KICKOFF    0x1<<0
#define    PC_LSB_VAL    0xFF<<0
#define    PC_MSB_VAL    0x7<<0


///////////////////////////
// SPI Registers and bit fileds //
///////////////////////////


// aditional defines

#define INTR_DIS_ALL 0x0

// TX State Machine Out value

#define RBIAS_CLASS_AB_AMP_VOLTAGE_SELECT_V1 0
#define RBIAS_CLASS_AB_AMP_VOLTAGE_SELECT_V2 0x1 <<0
#define RBIAS_CLASS_AB_AMP_VOLTAGE_SELECT_V3 0x10<<0
#define RBIAS_CLASS_AB_AMP_VOLTAGE_SELECT_V4 0x11<<0

#define RBIAS_CLASS_AB_AMP_CURRENT_MODE_LOW 0x0
#define RBIAS_CLASS_AB_AMP_CURRENT_MODE_HIGH 0x1<<2

#define H_BRIDGE_STANDBY_OFF 0x0
#define H_BRIDGE_STANDBY_ON 0x1<<3

#define H_BRIDGE_CONTROL_IDLE 0x0
#define H_BRIDGE_CONTROL_BURSTING 0x1<<4

#define DBIAS_CLASS_AB_AMP_VOLTAGE_SELECT_V1 0x0
#define DBIAS_CLASS_AB_AMP_VOLTAGE_SELECT_V2 0x1<<5
#define DBIAS_CLASS_AB_AMP_VOLTAGE_SELECT_V3 0x10<<5
#define DBIAS_CLASS_AB_AMP_VOLTAGE_SELECT_V4 0x11<<5

#define DBIAS_CLASS_AB_AMP_CURRENT_MODE_LOW 0x0
#define DBIAS_CLASS_AB_AMP_CURRENT_MODE_HIGH 0x1<<7


///////////////////
// SPI register Map //
///////////////////

#define SPI_REGISTER_OFFSET 0x300

#define SPI_SLAVE_SANITY         SPI_REGISTER_OFFSET+0x0
#define SPI_SLAVE_CORE_VER         SPI_REGISTER_OFFSET+0x4
#define SPI_SLAVE_STATUS         SPI_REGISTER_OFFSET+0x8
#define SPI_SLAVE_CONTROL         SPI_REGISTER_OFFSET+0xC
#define SPI_SLAVE_SW_RESET         SPI_REGISTER_OFFSET+0x10
#define SPI_SLAVE_SW_IRQ         SPI_REGISTER_OFFSET+0x14
#define SPI_SLAVE_IRQ_MASK         SPI_REGISTER_OFFSET+0x18
#define SPI_SLAVE_IRQ_CLEAR     SPI_REGISTER_OFFSET+0x1C
#define SPI_SLAVE_IRQ_FORCE     SPI_REGISTER_OFFSET+0x20
#define SPI_SLAVE_TX             SPI_REGISTER_OFFSET+0x24
#define SPI_SLAVE_RX             SPI_REGISTER_OFFSET+0x28
#define SPI_SLAVE_TEST_BUS_DATA    SPI_REGISTER_OFFSET+0x2C
#define SPI_SLAVE_TEST_BUS_CTRL    SPI_REGISTER_OFFSET+0x30
#define SPI_SLAVE_SW_RST_IRQ    SPI_REGISTER_OFFSET+0x34
#define SPI_SLAVE_CHAR_CFG        SPI_REGISTER_OFFSET+0x38
#define SPI_SLAVE_CHAR_DIN_0    SPI_REGISTER_OFFSET+0x3C
#define SPI_SLAVE_CHAR_DIN_1    SPI_REGISTER_OFFSET+0x40
#define SPI_SLAVE_CHAR_DOUT_0    SPI_REGISTER_OFFSET+0x44
#define SPI_SLAVE_CHAR_DOUT_1    SPI_REGISTER_OFFSET+0x48
#define SPI_SLAVE_TRNS_BYTE_CNT    SPI_REGISTER_OFFSET+0x4C
#define SPI_SLAVE_TRNS_LEN        SPI_REGISTER_OFFSET+0x50
#define SPI_SLAVE_FIFO_LEVEL    SPI_REGISTER_OFFSET+0x54

#endif  // _SNS_QFP_SD_REGMAP_H
