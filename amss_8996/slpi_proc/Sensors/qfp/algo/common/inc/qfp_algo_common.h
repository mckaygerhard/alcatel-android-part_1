#ifndef _QFP_ALGO_COMMON_H
#define _QFP_ALGO_COMMON_H
/*=============================================================================
  @file qfp_algo_common

  Common definitions for QFP algorithms

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/algo/common/inc/qfp_algo_common.h#1 $ */
/* $DateTime: 2016/02/25 10:35:39 $ */
/* $Author: kshyamas $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order. 

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2015-08-09  ld   Initial version

=============================================================================*/ 

/* these types are defined in TZ but not in SLPI. Define them to make it
   easier to port QFP algorithms from TZ */
typedef float float32_t;
typedef double float64_t;

// F3 messages
// also for simulation only case used in testing
#if defined QDSP6
  #if !defined(SIM)
    #ifndef DBG_MEDIUM_PRIO
      #define DBG_MEDIUM_PRIO DBG_MED_PRIO
    #endif // DBG_MEDIUM_PRIO
    #define SNS_QFP_ALG_PRINTF0(level,msg)          MSG(MSG_SSID_SNS,DBG_##level##_PRIO,msg)
    #define SNS_QFP_ALG_PRINTF1(level,msg,p1)       MSG_1(MSG_SSID_SNS,DBG_##level##_PRIO,msg,p1)
    #define SNS_QFP_ALG_PRINTF2(level,msg,p1,p2)    MSG_2(MSG_SSID_SNS,DBG_##level##_PRIO,msg,p1,p2)
    #define SNS_QFP_ALG_PRINTF3(level,msg,p1,p2,p3) MSG_3(MSG_SSID_SNS,DBG_##level##_PRIO,msg,p1,p2,p3)
    #define SNS_QFP_ALG_PRINTF4(level,msg,p1,p2,p3,p4) MSG_4(MSG_SSID_SNS,DBG_##level##_PRIO,msg,p1,p2,p3,p4)
  #else /* SIM */
    #define SNS_QFP_ALG_PRINTF0(level,msg)          printf("\n"); printf(msg)
    #define SNS_QFP_ALG_PRINTF1(level,msg,p1)       printf("\n"); printf(msg,p1)
    #define SNS_QFP_ALG_PRINTF2(level,msg,p1,p2)    printf("\n"); printf(msg,p1,p2)
    #define SNS_QFP_ALG_PRINTF3(level,msg,p1,p2,p3) printf("\n"); printf(msg,p1,p2,p3)
    #define SNS_QFP_ALG_PRINTF4(level,msg,p1,p2,p3,p4) printf("\n"); printf(msg,p1,p2,p3,p4)
  #endif
#elif defined(ADSP_STANDALONE)
  #define SNS_QFP_ALG_PRINTF0(level,msg)          qurt_printf("\n"); qurt_printf(msg)
  #define SNS_QFP_ALG_PRINTF1(level,msg,p1)       qurt_printf("\n"); qurt_printf(msg,p1)
  #define SNS_QFP_ALG_PRINTF2(level,msg,p1,p2)    qurt_printf("\n"); qurt_printf(msg,p1,p2)
  #define SNS_QFP_ALG_PRINTF3(level,msg,p1,p2,p3) qurt_printf("\n"); qurt_printf(msg,p1,p2,p3)
  #define SNS_QFP_ALG_PRINTF4(level,msg,p1,p2,p3,p4) qurt_printf("\n"); qurt_printf(msg,p1,p2,p3,p4)
#endif // QDSP6


#endif /* _QFP_ALGO_COMMON_H */
