#ifndef FFD_ALGO_H
#define FFD_ALGO_H
/*=============================================================================
  @file ffd_algo

  Functions exported by fast finger detection algorithm module

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/algo/ffd/inc/ffd_algo.h#1 $ */
/* $DateTime: 2016/02/25 10:35:39 $ */
/* $Author: kshyamas $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order. 

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2016-02-16  RD   Memory optimizations
  2015-08-09  ld   Initial version

=============================================================================*/ 
#include "sns_common.h"
#include "qfp_algo_common.h"
#include "sns_pm.h"
#include "ffd_types.h"
#include "ffd_api.h"

/* image sizes in pixels */
#define FFD_PHASE1_IMAGE_SIZE    270
#define FFD_PHASE2_IMAGE_SIZE    1782

// image sizes in bytes, for allocation
#define FFD_PHASE1_IMAGE_SIZE_BYTES (FFD_PHASE1_IMAGE_SIZE * sizeof(uint16_t))
#define FFD_PHASE2_IMAGE_SIZE_BYTES (FFD_PHASE2_IMAGE_SIZE * sizeof(uint16_t))
#define FFD_MAX_IMAGE_SIZE_BYTES FFD_PHASE2_IMAGE_SIZE_BYTES + TEMPERATURE_DATA_SIZE

/* width in pixels of full image */
#define FFD_FULL_IMAGE_WIDTH   180
/* height in pixels of full image */
#define FFD_FULL_IMAGE_HEIGHT   80
/* width in pixels of phase 1 small image */
#define FFD_PHASE1_IMAGE_WIDTH   54
/* height in pixels of phase 1 small image */
#define FFD_PHASE1_IMAGE_HEIGHT  5
/* width in pixels of phase 2 small image */
#define FFD_PHASE2_IMAGE_WIDTH   54
/* height in pixels of phase 2 small image*/
#define FFD_PHASE2_IMAGE_HEIGHT  33


SNS_FFD_UIMAGE_CODE sns_err_code_e ffd_algo_energy_estimator(
  fast_finger_detect_t* state,
  ffd_proc_input_t *ffd_input,
  uint32_t small_image_size,
  int32_t* result,
  float32_t *energy,
  float32_t *energy_off);

#endif /* FFD_ALGO_H */

