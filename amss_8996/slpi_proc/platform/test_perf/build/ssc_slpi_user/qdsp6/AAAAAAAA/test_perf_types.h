#ifndef _TEST_PERF_TYPES_H
#define _TEST_PERF_TYPES_H
#include "AEEStdDef.h"
#ifdef __cplusplus
extern "C" {
#endif
/// @file test_perf_types.idl
///
typedef struct test_perf_result test_perf_result;
struct test_perf_result {
   uint64 avg;
   uint64 min;
   uint64 max;
   double stddev;
   int result;
};
#ifdef __cplusplus
}
#endif
#endif //_TEST_PERF_TYPES_H
