/*
 * Copyright (c) 2013 QUALCOMM Technologies Inc. All Rights Reserved.
 * Qualcomm Technologies Confidential and Proprietary
 *
 */
#ifndef APPS_STD_FOPEN_WITH_ENV_H
#define APPS_STD_FOPEN_WITH_ENV_H

#include "AEEstd.h"
#include "apps_std.h"
#include "qtest_stdlib.h"
#include "verify.h"

#if (defined _WIN32)
static char* ADSP_LIBRARY_PATH=";c:\\Program Files\\Qualcomm\\RFSA\\aDSP";
#elif (defined _ANDROID) || (defined ANDROID)
static char* ADSP_LIBRARY_PATH=";/system/lib/rfsa/adsp";
#elif (defined __QNX__)
static char* ADSP_LIBRARY_PATH="/radio/lib/firmware";
#else
static char* ADSP_LIBRARY_PATH="";
#endif

static __inline int apps_std_fopen_with_env_imp(const char* envvarname, const char* delim, const char* name, const char* mode, apps_std_FILE* psout) {
#define     EMTPY_STR      ""
#define     ENV_LEN_GUESS  256
   char     *envList    =  NULL;
   char     *envListBuf =  NULL;
   char     *dirList    =  NULL;
   char     *dirListBuf =  NULL;
   char     *dirName    =  NULL;
   char     *pos        =  NULL;
   char     *absName    =  NULL;
   char     *srcStr     =  NULL;
   uint16   absNameLen  =  0;
   int      nErr        =  0;
   int      envListLen  =  0;
   int      listLen     =  0;
   int      envLenGuess = STD_MAX(ENV_LEN_GUESS, 1 + std_strlen(ADSP_LIBRARY_PATH));

   VERIFY(NULL != delim);
   VERIFY(NULL != name);
   VERIFY(NULL != mode);

   VERIFY(envListBuf = (char*)MALLOC(sizeof(char) * envLenGuess));
   envList = envListBuf;
   *envList = '\0';

   if (0 == apps_std_getenv(envvarname, envList, envLenGuess, &envListLen)) {
      if (envLenGuess < envListLen) {
         FREEIF(envListBuf);
         VERIFY(envListBuf = REALLOC(envListBuf, sizeof(char) * envListLen));
         envList = envListBuf;
         VERIFY(0 == apps_std_getenv(envvarname, envList, envListLen, &listLen));
      }
   } else if(std_strcmp(envvarname, "ADSP_LIBRARY_PATH") == 0) {
      envListLen = listLen = 1 + std_strlcpy(envListBuf, ADSP_LIBRARY_PATH, envLenGuess);
   }

   /*
    * Allocate mem. to copy envvarname.
    * If envvarname not set, allocate mem to create an empty string
    */
   if('\0' != *envList) {
      srcStr = envList;
   } else {
      srcStr = EMTPY_STR;
      envListLen = std_strlen(EMTPY_STR) + 1;
   }
   VERIFY(dirListBuf = (char*)MALLOC(sizeof(char) * envListLen));
   dirList = dirListBuf;
   std_strlcpy(dirList, srcStr, envListLen);

   while(dirList)
   {
      pos = strstr(dirList, delim);
      dirName = dirList;
      if (pos) {
         *pos = '\0';
         dirList = pos + std_strlen(delim);
      } else {
         dirList = 0;
      }

      // Account for slash char
      absNameLen = std_strlen(dirName) + std_strlen(name) + 2;
      VERIFY(absName = (char*)MALLOC(sizeof(char) * absNameLen));
      if ('\0' != *dirName) {
         std_strlcpy(absName, dirName, absNameLen);
         std_strlcat(absName, "/", absNameLen);
         std_strlcat(absName, name, absNameLen);
      } else {
         std_strlcpy(absName, name, absNameLen);
      }

      nErr = apps_std_fopen(absName, mode, psout);
      FREEIF(absName);
      if (0 == nErr) {
         goto bail;
      }
   }

bail:
   FREEIF(absName);
   FREEIF(dirListBuf);
   FREEIF(envListBuf);

   return nErr;
}
#endif // APPS_STD_FOPEN_WITH_ENV_H
