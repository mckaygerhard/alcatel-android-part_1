//
//  Copyright (c) 2014-2015, Qualcomm Technologies, Inc. All rights reserved.
//  Portions Copyright (c) 2013, ARM Limited. All rights reserved.
//
//  This program and the accompanying materials
//  are licensed and made available under the terms and conditions of the BSD License
//  which accompanies this distribution.  The full text of the license may be found at
//  http://opensource.org/licenses/bsd-license.php
//
//  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
//  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
//
//

  INCLUDE AsmMacroIoLib.inc
//  INCLUDE Library/ArmGicLib.h

  EXPORT  InitializeGicV3
  EXPORT  Read_ICC_CTLR_EL3
  EXPORT  Write_ICC_CTLR_EL3
  EXPORT  Read_ICC_CTLR_EL1
  EXPORT  Write_ICC_CTLR_EL1
  EXPORT  Read_ICC_PMR_EL1
  EXPORT  Write_ICC_PMR_EL1
  EXPORT  Write_ICC_BPR0_EL1
  EXPORT  Write_ICC_BPR1_EL1
  EXPORT  Write_ICC_EIOR0_EL1
  EXPORT  Write_ICC_EIOR1_EL1
  EXPORT  Read_ICC_IAR0_EL1
  EXPORT  Read_ICC_IAR1_EL1
  EXPORT  Write_ICC_SGI1R_EL1
  EXPORT  Write_ICC_IGRPEN1_EL1

  PRESERVE8
  AREA    GicV3, CODE, READONLY

/* Initialize GICv3 to expose it as a GICv2 as UEFI does not support GICv3 yet */
InitializeGicV3 FUNCTION
  // GICv3 Initialization not Supported yet
  bx  lr
  ENDFUNC

Write_ICC_IGRPEN1_EL1 FUNCTION
  bx lr
  ENDFUNC

Read_ICC_CTLR_EL3 FUNCTION
  ldr r0, [r0, #0x00]
  bx lr
  ENDFUNC

Write_ICC_CTLR_EL3 FUNCTION
  str r1, [r0, #0x00]
  isb
  bx lr
  ENDFUNC

Read_ICC_CTLR_EL1 FUNCTION
  ldr r0, [r0, #0x00]
  bx lr
  ENDFUNC

Write_ICC_CTLR_EL1 FUNCTION
  str r1, [r0, #0x00]
  isb
  bx lr
  ENDFUNC

Read_ICC_PMR_EL1 FUNCTION
  ldr r0, [r0, #0x04]
  bx lr
  ENDFUNC

Write_ICC_PMR_EL1 FUNCTION
  str r1, [r0, #0x04]
  isb
  bx lr
  ENDFUNC

Write_ICC_BPR0_EL1 FUNCTION
  str r1, [r0, #0x08]
  isb
  bx lr
  ENDFUNC

Write_ICC_BPR1_EL1 FUNCTION
  str r1, [r0, #0x08]
  isb
  bx lr
  ENDFUNC

Write_ICC_EIOR0_EL1 FUNCTION
  str r1, [r0, #0x10]
  isb
  bx lr
  ENDFUNC

Write_ICC_EIOR1_EL1 FUNCTION
  str r1, [r0, #0x10]
  isb
  bx lr
  ENDFUNC

Read_ICC_IAR0_EL1 FUNCTION
  ldr r0, [r0, #0x0C]
  bx lr
  ENDFUNC

Read_ICC_IAR1_EL1 FUNCTION
  ldr r0, [r0, #0x0C]
  bx lr
  ENDFUNC

Write_ICC_SGI1R_EL1 FUNCTION
  bx lr
  ENDFUNC

  END
