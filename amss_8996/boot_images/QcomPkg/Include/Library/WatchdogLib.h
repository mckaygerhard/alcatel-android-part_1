/** @file WatchdogLib.h
   
  Library implementation to support Watchdog Runtime call.

  Copyright (c) 2012, Qualcomm Technologies Inc. All rights reserved. 

**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 06/24/12   rks      Created
=============================================================================*/
#ifndef __WATCHDOGLIB_H__
#define __WATCHDOGLIB_H__

/**
  Disable watchdog. 
   
**/
VOID
EFIAPI
DisableWatchdog(void);


/**
  Reset the MSM. 
   
**/

VOID
EFIAPI
 WatchdogResetRuntime (
  VOID
  );

/**
  Initialize any infrastructure required for LibResetSystem () to function.

  @param  ImageHandle   The firmware allocated handle for the EFI image.
  @param  SystemTable   A pointer to the EFI System Table.
  
  @retval EFI_SUCCESS   The constructor always returns EFI_SUCCESS.

**/

EFI_STATUS
EFIAPI
WatchdogInit (
   IN EFI_HANDLE        ImageHandle,
   IN EFI_SYSTEM_TABLE  *SystemTable

  );


#endif /* __WATCHDOGLIB_H__ */
