#ifndef __MDP_H__
#define __MDP_H__
/*===========================================================================*/
/*!
  \file
    mdp.h

\b  DESCRIPTION

Common definitions header file. Providing common structures needed for MDP

  Copyright (c) 2008-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
*/
/*===========================================================================*/

/*===========================================================================
                             Edit History

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/11/11   axu     Add structure for MMSS_CC settings for
05/16/11   ab      Updated parameters for DSI panel.
05/03/11   ab      Initial version.
===========================================================================*/

#define PCDPANELCONFIG_LCDC       0
#define PCDPANELCONFIG_DSI_VIRTIO 1
#define PCDPANELCONFIG_DSI        2
#define PCDPANELCONFIG_HDMI       3


#endif // __MDP_H__

