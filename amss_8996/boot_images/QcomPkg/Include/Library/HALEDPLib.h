#ifndef HALEDPLIB_H
#define HALEDPLIB_H
/*=============================================================================
 
  File: HALEDPLib.h
 
  Header file for eDP driver
  
 
  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/

#include "../MDPLib/MDPLib_i.h"

/* -----------------------------------------------------------------------
** Function Declarations
-------------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- 
** FUNCTION: EdpPanelInit()
** 
** DESCRIPTION:
**   Initialize the eDP panel
**
 -------------------------------------------------------------------- */
MDP_Status EdpPanelInit(MDP_Panel_AttrType *pPanelConfig);



/* ---------------------------------------------------------------------- 
** FUNCTION: EdpDriverClose()
** 
** DESCRIPTION:
**    De-initializes the EDP driver and closes the interface.
**
 -------------------------------------------------------------------- */
MDP_Status EdpDriverClose(void);


/* ---------------------------------------------------------------------- 
** FUNCTION: EdpDriverGetInfo()
** 
** DESCRIPTION:
**    Read eDP Panel info via AUX EDID.
**
 -------------------------------------------------------------------- */
MDP_Status EdpDriverGetInfo(MDP_Panel_AttrType *pPanelConfig);

#endif  /* #define HALEDPLIB_H */
