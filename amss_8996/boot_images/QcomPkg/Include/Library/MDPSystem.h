#ifndef __MDPSYSTEM_H__
#define __MDPSYSTEM_H__
/*=============================================================================
 
  File: MDPTypes.h
 
  Header file for common driver types
  
 
  Copyright (c) 2011-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include <Uefi.h>
#include <Library/PrintLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include "MDPTypes.h"

/*===========================================================================

                                 Defines 

===========================================================================*/

typedef enum 
{
  MDP_LOGLEVEL_ERROR,
  MDP_LOGLEVEL_WARNING,
  MDP_LOGLEVEL_INFO,
} MDP_LogLevel;


/*===========================================================================

                                Helper Macros

===========================================================================*/
#define MDP_OSAL_MEMZERO(_ptr_, _size_) \
    gBS->SetMem(_ptr_, _size_, 0x0)

#define MDP_OSAL_MEMCPY(_dst_, _src_, _size_) \
    gBS->CopyMem(_dst_, _src_, _size_)

#define MDP_OSAL_DELAYMS(_DelayMs_) \
    gBS->Stall((uint32)(_DelayMs_)*1000)

#define MDP_OSAL_DELAYUS(_DelayUs_) \
    gBS->Stall((uint32)(_DelayUs_))

#define MDP_OSAL_MEMSET(_ptr_, _val_, _size_) \
    gBS->SetMem(_ptr_, _size_, _val_)


#define MDP_MIN(_a_, _b_) \
    (((_a_)<(_b_))?(_a_):(_b_))

#define MDP_MAX(_a_, _b_) \
    (((_a_)>(_b_))?(_a_):(_b_))

    

/*===========================================================================

                                Enumeration

===========================================================================*/



/*===========================================================================

                        Structures 

===========================================================================*/


/*===========================================================================

                        Public Functions

===========================================================================*/

/****************************************************************************
*
** FUNCTION: MDP_Log_Message()
*/
/*!
* \brief
*   This function will perform the logging of Debug messages
*
* \param [in] uLogType   - Log level
*        [in]  pLogString    -  Log String
*
* \retval void
*
****************************************************************************/
void MDP_Log_Message(MDP_LogLevel uLogType, char *pLogString, ...);

/****************************************************************************
*
** FUNCTION: MDP_ReadUnaligned_U32()
*/
/*!
* \brief
* This function reads the uint16 values from aligned boundary and packs them into uint32
*
* if pSrc is at offset of 1 or 3 from the uint32 aligned address
*            reads uint16 from aligned (pSrc -1) address and extracts the LSB 
*            reads next uint16 addresses and 
*            reads the next +1 uint16 address and extracts the MSB
*            Packs these into uint32 pDest address
*
*if pSrc is at offset 2 from the the uint32 aligned address
*            reads uint16 from aligned (pSrc -1) address 
*            reads next uint16 addresses 
*            Packs these into uint32 pDest addr
*
* Otherwise pSrc is on aligned address boundary

*
* \param [in] pDest   - pointer to Uint32
*        [in]  pSrc        - pointer to Uint8 
*
* \retval void
*
****************************************************************************/
void MDP_ReadUnaligned_U32(uint32 *pDest, uint8 * pSrc);

/****************************************************************************
*
** FUNCTION: MDP_OSAL_MB()
*/
/*!
* \brief
*   This function creates a hardware memory barrier (fence) that prevents the CPU from 
*  re-ordering read and write operations.
*
* \param void
*        
*
* \retval void
*
****************************************************************************/

void MDP_OSAL_MB(void);


#endif // __MDPSYSTEM_H__

