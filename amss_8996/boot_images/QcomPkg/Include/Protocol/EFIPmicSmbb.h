/** 
@file  EFIPmicSmbb.h
@brief PMIC smbb for UEFI.
*/
/*=============================================================================
Copyright (c) 2012 - 2013 Qualcomm Technologies, Inc.
All rights reserved.
Qualcomm Technologies Confidential and Proprietary.
    
=============================================================================*/

/*=============================================================================
EDIT HISTORY


when       who     what, where, why
--------   ---     ----------------------------------------------------------- 
09/12/13   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
06/12/13   al      Adding APIs to enable and set IDC max 
04/19/13   al      Adding usb id detection
03/05/13   al      Adding IsUsbInValid 
02/11/13   al      Adding IsBatteryPresent  
02/07/13   sm      Added new APIs to initialize smbb
10/17/12   dy      Added new APIs to Enable Charger, Set Max Current, Get Attached Charger
09/06/12   al      Created new protocol

=============================================================================*/

#ifndef __EFIPMICSMBB_H__
#define __EFIPMICSMBB_H__

/*===========================================================================
INCLUDE FILES
===========================================================================*/

/*===========================================================================
MACRO DECLARATIONS
===========================================================================*/
/** @ingroup efi_pmicSmbb_constants 
Protocol version.
*/

#define PMIC_SMBB_REVISION 0x0000000000010005
/** @} */ /* end_addtogroup efi_pmicSmbb_constants */

/*  Protocol GUID definition */
/** @ingroup efi_pmicSmbb_protocol */
#define EFI_PMIC_SMBB_PROTOCOL_GUID \
    { 0x76b95dad, 0x1e66, 0x4d98, { 0x9c, 0xe7, 0xb2, 0x94, 0xd9, 0x59, 0x68, 0x95 } }

/** @cond */
/*===========================================================================
EXTERNAL VARIABLES
===========================================================================*/
/**
External reference to the PMIC CLK Protocol GUID.
*/
extern EFI_GUID gQcomPmicSmbbProtocolGuid;

/*===========================================================================
TYPE DEFINITIONS
===========================================================================*/
/**
Protocol declaration.
*/
typedef struct _EFI_QCOM_PMIC_SMBB_PROTOCOL   EFI_QCOM_PMIC_SMBB_PROTOCOL;
/** @endcond */

/** @addtogroup efi_pmicSmbb_data_types 
@{ */
/**  Attached charger type. **/
typedef enum 
{
   PM_UEFI_SMBB_ATTACHED_CHGR__NONE,    /**< No charger. */
   PM_UEFI_SMBB_ATTACHED_CHGR__BATT,    /**< Battery. */
   PM_UEFI_SMBB_ATTACHED_CHGR__USB,     /**< USB path. */
   PM_UEFI_SMBB_ATTACHED_CHGR__DCIN,    /**< DCIN path. */
} PM_UEFI_SMBB_ATTACHED_CHGR_TYPE;

/** Battery temperature hot threshold limit. **/
typedef enum 
{
   PM_UEFI_SMBB_BAT_TEMP_HOT_THD_LOW,
   /**< Low hot threshold; 25% of VREF_BAT_THM. */
   PM_UEFI_SMBB_BAT_TEMP_HOT_THD_HIGH
   /**< High hot threshold; 35% of VREF_BAT_THM. */
} PM_UEFI_SMBB_BAT_TEMP_HOT_THD_TYPE;

/** Battery temperature cold threshold limit. **/
typedef enum 
{
   PM_UEFI_SMBB_BAT_TEMP_COLD_THD_LOW,
   /**< Low cold threshold; 70% of VREF_BAT_THM. */
   PM_UEFI_SMBB_BAT_TEMP_COLD_THD_HIGH
   /**< High cold threshold; 80% of VREF_BAT_THM. */
} PM_UEFI_SMBB_BAT_TEMP_COLD_THD_TYPE;
/** @} */ /* end_addtogroup efi_pmicSmbb_data_types */

/*===========================================================================
FUNCTION DEFINITIONS
===========================================================================*/
/* EFI_PM_SMBB_INT */ 
/** @ingroup efi_pmSmbb_int
  @par Summary
  Initializes the SMBB module.

  @param[in]  PmicDeviceIndex  Primary: 0.

  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PM_SMBB_INT)(
    IN UINT32 PmicDeviceIndex
    );

/* EFI_PM_SMBB_USB_OTG_ENABLE */ 
/** @ingroup efi_pmSmbb_usb_otg_enable
  @par Summary
  Enables/disables the USB OTG charger path.

  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  Enable           If TRUE, enable the USB OVP FET; if FALSE, 
                               disable the USB OVP FET.

  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PM_SMBB_USB_OTG_ENABLE)(
    IN UINT32  PmicDeviceIndex, 
    IN BOOLEAN Enable
    );

/* EFI_PM_SMBB_ENABLE_CHARGER */ 
/** @ingroup efi_pmSmbb_enable_charger
  @par Summary
  Enables the charger module to charge the battery.
 
  @param[in]  PmicDeviceIndex    Primary: 0
  @param[in]  ChargerEnable      If TRUE, enable the charger; if FALSE, disable 
                                 the charger.

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PM_SMBB_ENABLE_CHARGER)(
  IN UINT32  PmicDeviceIndex, 
  IN BOOLEAN ChargerEnable
);

/* EFI_PM_SMBB_SET_USB_MAX_CURRENT */
/** @ingroup efi_pmSmbb_set_usb_max_current
  @par Summary
  Sets the maximum USB current.

  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  MaxCurrent         Specifies the maximum current to charge the 
                                 battery in mA:
                                 - 0    -- Disable charging
                                 - 100  -- Typically used for a nonenumerated USB 
                                           PC charger and/or to trickle-charge a 
                                           battery
                                 - 500  -- Typically used for an enumerated USB 
                                           device that can source 500 mA
                                 - 500+ -- USB wall charger
                                 @tablebulletend

  @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PM_SMBB_SET_USB_MAX_CURRENT)(
  IN UINT32 PmicDeviceIndex,
  IN UINT32 MaxCurrent
);

/* EFI_PM_SMBB_GET_CHARGE_PATH */
/** @ingroup efi_pmSmbb_get_charge_path
  @par Summary
  Returns information about the charger attached to the device.
 
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[out] ChargerPath        Charger path; see #PM_UEFI_SMBB_ATTACHED_CHGR_TYPE
                                 for details. 

  @return
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_GET_CHARGE_PATH)(
  IN  UINT32                          PmicDeviceIndex, 
  OUT PM_UEFI_SMBB_ATTACHED_CHGR_TYPE *ChargerPath
);


/* EFI_PMIC_SMBB_IS_BATTERY_PRESENT */
/** @ingroup efi_pmSmbb_is_battery_present
  @par Summary
  Indicates whether the battery is present.
 
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[out] BatteryPresent     Whether or not the battery is present.

  @return
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_IS_BATTERY_PRESENT)(
  IN  UINT32  PmicDeviceIndex,
  OUT BOOLEAN *BatteryPresent
);

/* EFI_PMIC_SMBB_USBIN_VALID */
/** @ingroup efi_pmSmbb_usbin_valid
  @par Summary
  Indicates whether the USB is connected.
 
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[out] Valid            TRUE, if the VBUS is high; otherwise, FALSE.

  @return
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_USBIN_VALID)(
  IN  UINT32  PmicDeviceIndex,
  OUT BOOLEAN *Valid
);

/* EFI_PMIC_SMBB_SET_VDD_MAX */
/** @ingroup efi_pmSmbb_set_vdd_max
  @par Summary
  Sets the voltage up to which the battery can be charged using the USB charger
  path.
 
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  ValueInmV        Specifies the VddMax for the battery voltage
                               in millivolts.

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_SET_VDD_MAX)(
  IN  UINT32  PmicDeviceIndex,
  IN  UINT32  ValueInmV
);

/* EFI_PMIC_SMBB_SET_VDD_SAFE */
/** @ingroup efi_pmSmbb_set_vdd_safe
  @par Summary
  Sets the value up to which the maximum voltage (VddMax) can be set. 
  VddSafe should always be more than VddMax, and VddMax more than VddSafe is 
  ignored. 
 
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  ValueInmV        Specifies the VddSafe for the battery voltage
                               in millivolts.

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_SET_VDD_SAFE)(
  IN  UINT32  PmicDeviceIndex,
  IN  UINT32  ValueInmV
);

/* EFI_PMIC_SMBB_SET_IBAT_MAX */
/** @ingroup efi_pmSmbb_set_ibat_max
  @par Summary
  Sets the battery charging current during fast charging.
 
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  ValueInmA        Specifies the limit for the battery charging
                               current, in milliamps, during fast charging.

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_SET_IBAT_MAX)(
  IN  UINT32  PmicDeviceIndex,
  IN  UINT32  ValueInmA
);
 
/* EFI_PMIC_SMBB_SET_IBAT_SAFE */
/** @ingroup efi_pmSmbb_set_ibat_safe
  @par Summary
  Sets the fast charge battery current limit maximum writable value.
 
  @param[in] PmicDeviceIndex   Primary: 0.
  @param[in] ValueInmA         Specifies the limit for the fast charge battery
                               current maximum writeable value in milliamps.

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_SET_IBAT_SAFE)(
  IN  UINT32  PmicDeviceIndex,
  IN  UINT32  ValueInmA
);

/* EFI_PMIC_SMBB_SET_ITRICKLE */
/** @ingroup efi_pmSmbb_set_itrickle
  @par Summary
  Sets the battery current threshold for trickle charging.
 
  @param[in] PmicDeviceIndex  Primary: 0.
  @param[in] ValueInmA        Specifies the battery charging current,
                              in milliamps, during Auto Trickle Charging (ATC).

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_SET_ITRICKLE)(
  IN  UINT32  PmicDeviceIndex,
  IN  UINT32  ValueInmA
);

/* EFI_PMIC_SMBB_SET_VIN_MIN */
/** @ingroup efi_pmSmbb_set_vin_min
  @par Summary
  Sets the value for minimum input charge voltage.
 
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  ValueInmV        Specifies the value for VinMin in millivolts.

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_SET_VIN_MIN)(
  IN  UINT32  PmicDeviceIndex,
  IN  UINT32  ValueInmV
);

  /* EFI_PMIC_SMBB_CONFIG_BTC */
/** @ingroup efi_pmSmbb_config_btc
  @par Summary
  Configures the Battery Temperature Comparator (BTC).
 
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  EnableBtc        If TRUE, enable the BTC; if FALSE, disable the
                               BTC.
  @param[in]  ColdThd          Cold threshold limit; see 
                               #PM_UEFI_SMBB_BAT_TEMP_COLD_THD_TYPE for
                               details.
  @param[in]  HotThd           Hot threshold limit; see 
                               #PM_UEFI_SMBB_BAT_TEMP_HOT_THD_TYPE for
                               details.

  @return
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_CONFIG_BTC)(
  IN  UINT32                              PmicDeviceIndex,
  IN  BOOLEAN                             EnableBtc,
  IN  PM_UEFI_SMBB_BAT_TEMP_COLD_THD_TYPE ColdThd,
  IN  PM_UEFI_SMBB_BAT_TEMP_HOT_THD_TYPE  HotThd
);

/* EFI_PM_MISC_CONFIG_USB_ID_PULLUP */ 
/** @ingroup efi_pmSmbb_config_usb_id_pullup
  @par Summary
  Enables/disables the USB ID pull up.
 
  @param[in]  PmicDeviceIndex    Primary: 0.
  @param[in]  Pullup             If TRUE, enable the USB ID pull up and the
                                 LV channel detection; if FALSE, disable the
                                 USB ID pull up and the LV channel detection.

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error.
*/

typedef
EFI_STATUS (EFIAPI *EFI_PM_MISC_CONFIG_USB_ID_PULLUP)(
  IN UINT32  PmicDeviceIndex, 
  IN BOOLEAN Pullup
);

/* EFI_PM_SMBB_IS_EOC_REACHED */ 
/** @ingroup efi_pmSmbb_is_eoc_reached
  @par Summary
  Indicates whether the end-of-charge is reached.
 
  @param[in]   PmicDeviceIndex    Primary: 0.
  @param[out]  pReachedEoc        Whether or not the end-of-charge is reached.

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error.
*/

typedef
EFI_STATUS (EFIAPI *EFI_PM_SMBB_IS_EOC_REACHED)(
  IN UINT32   PmicDeviceIndex, 
  OUT BOOLEAN *pReachedEoc
);

/* EFI_PMIC_SMBB_EN_IDC_MAX */
/** @ingroup efi_pmSmbb_en_idc_max
  @par Summary
  Enables/disables the input current limit for the DC charger path.
 
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  Enable           If TRUE, enable the input current limit; 
                               if FALSE, disable the input current limit.

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_EN_IDC_MAX)(
  IN  UINT32  PmicDeviceIndex,
  IN  BOOLEAN Enable
);

/* EFI_PMIC_SMBB_SET_IDC_MAX */
/** @ingroup efi_pmSmbb_set_idc_max
  @par Summary
  Sets the maximum value of the charge current for the DC charger path.
 
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  ValueInmA        Maximum current to set. Range: 100 mA to 2500 mA.

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_SMBB_SET_IDC_MAX)(
  IN  UINT32  PmicDeviceIndex,
  IN  UINT32  ValueInmA
);
/*===========================================================================
PROTOCOL INTERFACE
===========================================================================*/
/** @ingroup efi_pmicSmbb_protocol
@par Summary
  Qualcomm PMIC Switched-Mode Battery Charge and Boost (SMBB) Protocol interface.

@par Parameters
@inputprotoparams{pmic_smbb_proto_params.tex} 
*/

struct _EFI_QCOM_PMIC_SMBB_PROTOCOL {
  UINT64                                Revision;
  EFI_PM_SMBB_INT                       SmbbInit;
  EFI_PM_SMBB_USB_OTG_ENABLE            UsbOtgEnable;
  EFI_PM_SMBB_ENABLE_CHARGER            EnableCharger;
  EFI_PM_SMBB_SET_USB_MAX_CURRENT       SetUsbMaxCurrent;
  EFI_PMIC_SMBB_GET_CHARGE_PATH         GetChargePath;
  EFI_PMIC_SMBB_IS_BATTERY_PRESENT      IsBatteryPresent;
  EFI_PMIC_SMBB_USBIN_VALID             UsbinValid;
  EFI_PMIC_SMBB_SET_VDD_MAX             SetVddMax;
  EFI_PMIC_SMBB_SET_VDD_SAFE            SetVddSafe;
  EFI_PMIC_SMBB_SET_IBAT_MAX            SetIbatMax;
  EFI_PMIC_SMBB_SET_IBAT_SAFE           SetIbatSafe;
  EFI_PMIC_SMBB_SET_ITRICKLE            SetItrickle;
  EFI_PMIC_SMBB_SET_VIN_MIN             SetVinMin;
  EFI_PMIC_SMBB_CONFIG_BTC              ConfigBtc;
  EFI_PM_MISC_CONFIG_USB_ID_PULLUP      ConfigUsbPullup;
  EFI_PM_SMBB_IS_EOC_REACHED            IsEocReached;
  EFI_PMIC_SMBB_EN_IDC_MAX              EnIdcMax;
  EFI_PMIC_SMBB_SET_IDC_MAX             SetIdcMax;
};

#endif  /* __EFIPMICSMBB_H__ */
