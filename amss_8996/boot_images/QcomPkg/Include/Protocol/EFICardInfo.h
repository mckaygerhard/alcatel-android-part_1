/** 
  @file  EFICardInfo.h
  @brief Sdcc Card Info Protocol for UEFI.
*/
/*=============================================================================
  Copyright (c) 2011 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/

/*=============================================================================
                              EDIT HISTORY


 when        who     what, where, why
 --------    ---     -----------------------------------------------------------
 2012/05/03  vishalo Merge in Techpubs Doxyegen comments
 2011/04/19  llg     (Tech Pubs) Edited/added Doxygen comments and markup.
 2012/01/26  vishalo Merge in Techpubs Doxyegen comments
 2011/11/15  bn      Code clean up  
 2011/10/26  bn      Added RPMB support   
 2011/10/21  llg     (Tech Pubs) Edited/added Doxygen comments and markup.
 2011/06/28  bn      Rename file to EFICardInfo.h 
 2011/05/19  bn      Added Card Info Protocol pointer
 2011/04/13  bn      Initial version.
 
=============================================================================*/
#ifndef __EFICARDINFO_H__
#define __EFICARDINFO_H__

/*===========================================================================
  INCLUDE FILES
===========================================================================*/

/*===========================================================================
  MACRO DECLARATIONS
===========================================================================*/
/** @addtogroup efi_cardInfo_constants 
@{ */
/**
  Protocol version.
*/
#define EFI_CARD_INFO_PROTOCOL_REVISION 0x0000000000010000
/** @} */ /* end_addtogroup efi_cardInfo_constants */

/*  Protocol GUID definition */
/** @ingroup efi_cardInfo_protocol */ 
#define EFI_CARD_INFO_PROTOCOL_GUID \
   { 0x6EC6ED4F, 0xD958, 0x40BE, { 0x87, 0x28, 0x6C, 0x59, 0x73, 0x29, 0x66, 0xDF } }

/** @cond */
/*===========================================================================
  EXTERNAL VARIABLES
===========================================================================*/
/**
  External reference to the EFICardInfo Protocol GUID.
 */
extern EFI_GUID gEfiSdccCardInfoProtocolGuid;

/*===========================================================================
  TYPE DEFINITIONS
===========================================================================*/
/**
  Protocol declaration.
 */
typedef struct _EFI_SDCC_CARDINFO_PROTOCOL   EFI_SDCC_CARDINFO_PROTOCOL;
/** @endcond */

/** @addtogroup efi_cardInfo_data_types 
@{ */
/**
 Card information.
*/
typedef struct _SDCC_CARD_INFO{
   UINT16   mfr_id;             /**< Manufacturer ID. */
   UINT16   oem_id;             /**< Original equipment manufacturer ID. */
   UINT32   product_serial_num; /**< Product serial number. */
   UINT8    mfr_date[8];        /**< Manufacture date. */
   UINT32   rpmb_size_in_byte;  /**< Replay protected memory block partition 
                                     size in bytes. */
   UINT32   reliable_write_count;  /**< Reliable write count. */
}/** @cond */SDCC_CARD_INFO/** @endcond */;
/** @} */ /* end_addtogroup efi_cardInfo_data_types */

/*===========================================================================
  FUNCTION DEFINITIONS
===========================================================================*/
/* GET_CARD_INFO */ 
/** @ingroup efi_cardInfo_get_card_info
  @par Summary
  Gets the card information. 

  @param[in]   This              Pointer to the EFI_SDCC_CARDINFO_PROTOCOL 
                                 instance.  
  @param[out]  card_info         Pointer to a variable that the driver returns 
                                 with the retrieved card information; see 
                                 #_SDCC_CARD_INFO for details. 
   
  @return
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid.
*/
typedef
EFI_STATUS
(EFIAPI *GET_CARD_INFO)(
   IN  EFI_SDCC_CARDINFO_PROTOCOL *This,
   OUT SDCC_CARD_INFO*   card_info
);

/*===========================================================================
  PROTOCOL INTERFACE
===========================================================================*/
/** @ingroup efi_cardInfo_protocol
  @par Summary
  Secure Digital Card Controller (SDCC) Card Information Protocol interface.

  @par Parameters
  @inputprotoparams{card_info_proto_params.tex}   
*/
struct _EFI_SDCC_CARDINFO_PROTOCOL 
{
   UINT64          Revision;
   GET_CARD_INFO   GetCardInfo;
};

#endif	/* __EFICARDINFO_H__ */

