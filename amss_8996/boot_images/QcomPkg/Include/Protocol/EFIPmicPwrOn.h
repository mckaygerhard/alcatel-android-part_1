#ifndef __EFIPMICPWRON_H__
#define __EFIPMICPWRON_H__

/** @file  EFIPmicPwrOn.h 
    @brief PMIC-POWERON MODULE RELATED DECLARATION

     This file contains functions and variable declarations to support 
     the PMIC POWERON module.
 */
/*=============================================================================
  Copyright (c) 2012-2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/14/15   al      Adding API to enable/disable PON trigger
05/19/14   sm      Added API to get PBL_PON Status
                   Deprecated API GetBatteryRemovedStatus
12/09/13   al      Typo fixed in comment
11/21/13   al      Adding watchdog APIs
03/15/13   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
01/24/13   al      Adding API to get pmic on/off/reset reason
11/01/12   al	   Battery removal status
7/27/12    al	   Renamed all Enums,added device index and resource index
04/11/12   dy      Added GetWatchdogStatus API
04/03/12   al      Added prototype for AutopowerUp post HardReset 
03/24/12   sm      Added Doxygen comments
03/20/12   sm      New file.
===========================================================================*/

/** @ingroup efi_pmicPwrOn_constants 
  Protocol version. 
*/
#define PMIC_PWRON_REVISION 0x0000000000010006

/* PMIC POWERON UEFI typedefs */
/** @cond */
typedef struct _EFI_QCOM_PMIC_PWRON_PROTOCOL   EFI_QCOM_PMIC_PWRON_PROTOCOL;
/** @endcond */

/* PMIC UEFI POWERON Procotol GUID */
/** @ingroup efi_pmicPwrOn_protocol */
extern EFI_GUID gQcomPmicPwrOnProtocolGuid;

/** @addtogroup efi_pmicPwrOn_data_types 
@{ */
/**
  Reason for the On, Off, or Reset state.
 */
typedef enum 
{
  EFI_PM_PWRON_PON_REASON1,
  /**< PMIC left the Off state due to reason 1. */
  EFI_PM_PWRON_WARM_RESET_REASON1,
  /**< PMIC entered the Warm Reset state due to reason 1. */
  EFI_PM_PWRON_WARM_RESET_REASON2,
  /**< PMIC entered the Warm Reset state due to reason 2. */
  EFI_PM_PWRON_POFF_REASON1,
  /**< PMIC left the On state and commenced a shutdown sequence due to reason 1. */
  EFI_PM_PWRON_POFF_REASON2,
  /**< PMIC left the On state and commenced a shutdown sequence due to reason 2. */
  EFI_PM_PWRON_SOFT_RESET_REASON1,
  /**< PMIC registers were reset due to reason 1. */
  EFI_PM_PWRON_SOFT_RESET_REASON2,
  /**< PMIC registers were reset due to reason 2. */
  EFI_PM_PWRON_REASON_INVALID
  /**< Invalid reason. */
} EFI_PM_PWRON_REASONS_TYPE;


/*! \enum pm_pon_reset_cfg_type
   \brief Reset configure type.
 */
typedef enum
{
  EFI_PM_PWRON_RESET_CFG_WARM_RESET,
  EFI_PM_PWRON_RESET_CFG_NORMAL_SHUTDOWN,
  /**< Shutdown to a state of main battery removal */
  EFI_PM_PWRON_RESET_CFG_D_VDD_BATT_REMOVE_SHUTDOWN,
  /**< Shutdown to a state of main battery and coin cell removal*/
  EFI_PM_PWRON_RESET_CFG_X_VDD_COIN_CELL_REMOVE_SHUTDOWN,
  /**< Shutdown + Auto pwr up */
  EFI_PM_PWRON_RESET_CFG_HARD_RESET,
  /**< Main Batt and coin cell remove Shutdown + Auto pwr up */
  EFI_PM_PWRON_RESET_CFG_D_VDD_BATT_REMOVE_HARD_RESET,
  EFI_PM_PWRON_RESET_CFG_X_VDD_COIN_CELL_REMOVE_HARD_RESET,
  EFI_PM_PWRON_RESET_CFG_IMMEDIATE_X_VDD_COIN_CELL_REMOVE_SHUTDOWN,
  /**< Warm Reset and Main Batt/coin cell remove Shutdown */
  EFI_PM_PWRON_RESET_CFG_WARM_RESET_AND_D_VDD_BATT_REMOVE_SHUTDOWN,
  EFI_PM_PWRON_RESET_CFG_WARM_RESET_AND_X_VDD_COIN_CELL_REMOVE_SHUTDOWN,
  EFI_PM_PWRON_RESET_CFG_WARM_RESET_AND_SHUTDOWN,
  /**< Warm Reset then Main Batt/coin cell remove Shutdown + Auto pwr up */
  EFI_PM_PWRON_RESET_CFG_WARM_RESET_THEN_HARD_RESET,
  EFI_PM_PWRON_RESET_CFG_WARM_RESET_THEN_D_VDD_BATT_REMOVE_HARD_RESET,
  EFI_PM_PWRON_RESET_CFG_WARM_RESET_THEN_X_VDD_COIN_CELL_REMOVE_HARD_RESET,
  EFI_PM_PWRON_RESET_CFG_INVALID  
}EFI_PM_PWRON_RESET_CFG_TYPE;

/**
   PON PBL Status types.
*/
typedef enum
{
  EFI_PM_PWRON_PON_PBL_STATUS_XVDD_RB_OCCURRED = 6,
  /*Poweron status for xVdd_rb occurred*/
  EFI_PM_PWRON_PON_PBL_STATUS_DVDD_RB_OCCURRED = 7,
  /*Poweron status for dVdd_rb occurred*/
  EFI_PM_PWRON_PON_PBL_STATUS_INVALID,
  /*Invalid Reason*/
}EFI_PM_PWRON_PON_PBL_STATUS_TYPE;


/*! \enum pm_pon_trigger_type
   \brief Power On Triggers
 */
typedef enum 
{
   /*! 1 - Trigger from SMPL.                  */
   EFI_PM_PON_TRIGGER_SMPL,
   /*! 2 - Trigger from RTC.                   */
   EFI_PM_PON_TRIGGER_RTC,
   /*! 3 - Trigger from DC Charger.            */
   EFI_PM_PON_TRIGGER_DC_CHG,
   /*! 4 - Trigger from USB Charger.           */
   EFI_PM_PON_TRIGGER_USB_CHG,
   /*! 5 - Trigger from PON1.                  */
   EFI_PM_PON_TRIGGER_PON1,
   /*! 6 - Trigger by CBL_PWR   .              */
   EFI_PM_PON_TRIGGER_CBLPWR,
   /*! 7 - Trigger by Keypad.                  */
   EFI_PM_PON_TRIGGER_KPDPWR,
   EFI_PM_PON_TRIGGER_INVALID
}EFI_PM_PON_TRIGGER_TYPE;
  

/** @} */
/* end_addtogroup efi_pmicPwrOn_data_types */

/*===========================================================================
                          FUNCTION DEFINITIONS
===========================================================================*/
/* EFI_PM_PWRON_HARD_RESET_ENABLE */ 
/** @ingroup efi_pm_PwrOn_hard_reset_enable
  @par Summary
  Enables/disables a hard reset.
  
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  Enable           If TRUE, enable a hard reset; if FALSE, disable 
                               a hard reset.
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef 

EFI_STATUS (EFIAPI *EFI_PM_PWRON_HARD_RESET_ENABLE)(
  IN UINT32 PmicDeviceIndex, 
  IN BOOLEAN Enable
);

/* EFI_PM_PWRON_HARD_RESET_AUTOPOWERUP */ 
/** @ingroup efi_pm_PwrOn_hard_reset_autopowerup
  @par Summary
   Enables/disables auto-powerup after a hard reset.
   
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  Enable           If TRUE, enable auto-powerup after a hard reset; 
                               if FALSE, disable auto-powerup after a hard 
                               reset.
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef 

EFI_STATUS (EFIAPI *EFI_PM_PWRON_HARD_RESET_AUTOPOWERUP)(
  IN UINT32 PmicDeviceIndex, 
  IN BOOLEAN Enable
);
/* EFI_PM_PWRON_HARD_RESET_SET_DEBOUNCE_TIMER */ 
/** @ingroup efi_pm_PwrOn_hard_reset_set_debounce_timer
  @par Summary
  Sets the debounce timer.
   
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  DebounceTimermS  Value in milliseconds to be set for the debounce 
                               timer.
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef 
EFI_STATUS (EFIAPI *EFI_PM_PWRON_HARD_RESET_SET_DEBOUNCE_TIMER)(
  IN UINT32 PmicDeviceIndex, 
  IN INT32 DebounceTimermS
);

/* EFI_PM_PWRON_HARD_RESET_SET_DELAY_TIMER */ 
/** @ingroup efi_pm_PwrOn_hard_reset_set_delay_timer
  @par Summary
  Sets the delay timer.
   
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  DelayTimermS     Value in milliseconds to be set for the delay 
                               timer.
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef 
EFI_STATUS (EFIAPI *EFI_PM_PWRON_HARD_RESET_SET_DELAY_TIMER)(
  IN UINT32 PmicDeviceIndex, 
  IN INT32 DelayTimermS
);

/* EFI_PM_PWRON_GET_POWERON_TRIGGER */ 
/** @ingroup efi_pm_PwrOn_get_poweron_trigger
  @par Summary
  Gets the power-on trigger.
  
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[out] TriggerType      Returns the value of the power-on trigger.
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef 
EFI_STATUS (EFIAPI *EFI_PM_PWRON_GET_POWERON_TRIGGER)(
  IN UINT32 PmicDeviceIndex, 
  OUT UINT8 *TriggerType
);

/* EFI_PM_PWRON_GET_WATCHDOG_STATUS */ 
/** @ingroup efi_pm_PwrOn_get_watchdog_status
  @par Summary
  Gets the MSM watchdog status.
  
  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[out] DogStatus        Returns the boolean value of the MSM watchdog 
                               status.
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef 
EFI_STATUS (EFIAPI *EFI_PM_PWRON_GET_WATCHDOG_STATUS)(
  IN UINT32 PmicDeviceIndex, 
  OUT UINT8 *DogStatus
);

/* EFI_PM_PWRON_GET_BATTERY_REMOVED_STATUS */ 
/** @ingroup efi_pm_PwrOn_get_battery_removal_status
  @par Summary
  Gets the battery removal status.

  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[out] BatteryRemoved   Returns the boolean value of the battery 
                               removal status. 
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef 
EFI_STATUS (EFIAPI *EFI_PM_PWRON_GET_BATTERY_REMOVED_STATUS)(
  IN UINT32 PmicDeviceIndex, 
  OUT BOOLEAN *BatteryRemoved
);


/* EFI_PM_PWRON_WDOG_CFG */ 
/** @ingroup efi_pm_pwron_wdog_cfg
  @par Summary
  Configures the PMIC watchdog 

  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  S1Timer          Select S1 timer in seconds for 
                               bark. Supported time in seconds:
                               0 to 127
  @param[in]  S2Timer          Select S2 timer in seconds for 
                               bark. This is the time after
                               S1Timer bark. Supported time in
                               seconds: 0 to 127
  @param[in] ResetCfgType      Configure the type of reset to be 
                               performed on the event
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/

typedef 
EFI_STATUS (EFIAPI *EFI_PM_PWRON_WDOG_CFG)(
  IN UINT32                      PmicDeviceIndex, 
  IN UINT32                      S1Timer,
  IN UINT32                      S2Timer,
  IN EFI_PM_PWRON_RESET_CFG_TYPE ResetCfgType 
);


/* EFI_PM_PWRON_WDOG_ENABLE */ 
/** @ingroup efi_pm_pwron_wdog_enable
  @par Summary
  Enables or disables the PMIC watchdog 

  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  Enable           If TRUE then enables the 
                               watchdog else disables the
                               watchdog.
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/

typedef 
EFI_STATUS (EFIAPI *EFI_PM_PWRON_WDOG_ENABLE)(
  IN UINT32                      PmicDeviceIndex, 
  IN BOOLEAN                     Enable
);

/* EFI_PM_PWRON_WDOG_PET */ 
/** @ingroup efi_pm_pwron_wdog_pet
  @par Summary
  Pets the PMIC watchdog 

  @param[in]  PmicDeviceIndex  Primary: 0.
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/

typedef 
EFI_STATUS (EFIAPI *EFI_PM_PWRON_WDOG_PET)(
  IN UINT32                      PmicDeviceIndex
);

/* EFI_PM_PWRON_GET_PON_PBL_STATUS */ 
/** @ingroup efi_pm_PwrOn_get_pon_pbl_status
  @par Summary
  Gets the PON PBL status.

  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  PblStatusType    PBL Status type See 
                               #EFI_PM_PWRON_PON_PBL_STATUS_TYPE for details.
  @param[out] Status           Returns the boolean value of the pon 
                               pbl status. 
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef 
EFI_STATUS (EFIAPI *EFI_PM_PWRON_GET_PON_PBL_STATUS)(
  IN  UINT32                            PmicDeviceIndex, 
  IN  EFI_PM_PWRON_PON_PBL_STATUS_TYPE  PblStatusType,
  OUT BOOLEAN                         *Status
);


/* EFI_PM_PWRON_SET_PON_TRIGGER */ 
/** @ingroup efi_pm_pwron_set_pon_trigger
  @par Summary
  Sets the PON trigger.

  @param[in]  PmicDeviceIndex  Primary: 0.
  @param[in]  PonTrigger       PON trigger type See  #EFI_PM_PON_TRIGGER_TYPE for details.
  @param[in]  Enable           TRUE to enable and FALSE to disable PON trigger
 
  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef 
EFI_STATUS (EFIAPI *EFI_PM_PWRON_SET_PON_TRIGGER)(
  IN UINT32 PmicDeviceIndex, 
  IN EFI_PM_PON_TRIGGER_TYPE PonTrigger,
  OUT BOOLEAN Enable
);


/*===========================================================================
  PROTOCOL INTERFACE
===========================================================================*/
/** @ingroup efi_pmicPwrOn_protocol
  @par Summary
  Qualcomm PMIC Power On Protocol interface.

  @par Parameters
  @inputprotoparams{pmic_power_on_proto_params.tex} 
*/
struct _EFI_QCOM_PMIC_PWRON_PROTOCOL {
  UINT64                                       Revision;
  EFI_PM_PWRON_HARD_RESET_ENABLE               HardResetEnable;
  EFI_PM_PWRON_HARD_RESET_AUTOPOWERUP          HardResetAutoPowerUp;
  EFI_PM_PWRON_HARD_RESET_SET_DEBOUNCE_TIMER   SetDebounceTimer;
  EFI_PM_PWRON_HARD_RESET_SET_DELAY_TIMER      SetDelayTimer;
  EFI_PM_PWRON_GET_POWERON_TRIGGER             GetPowerOnTrigger;
  EFI_PM_PWRON_GET_WATCHDOG_STATUS             GetWatchdogStatus;
  EFI_PM_PWRON_GET_BATTERY_REMOVED_STATUS      GetBatteryRemovedStatus;
  EFI_PM_PWRON_WDOG_CFG                        WdogCfg;
  EFI_PM_PWRON_WDOG_ENABLE                     WdogEnable;
  EFI_PM_PWRON_WDOG_PET                        WdogPet;
  EFI_PM_PWRON_GET_PON_PBL_STATUS              GetPonPblStatus;
  EFI_PM_PWRON_SET_PON_TRIGGER                 SetPonTrigger;
};

#endif /* __EFIPMICPWRON_H__ */

