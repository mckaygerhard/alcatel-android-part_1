/** 
@file  EFIPmicNpaTest.h
@brief PMIC NpaTest Protocol for UEFI.
*/
/*=============================================================================
Copyright (c) 2014 Qualcomm Technologies, Inc.
All rights reserved.
Qualcomm Technologies Confidential and Proprietary.
    
=============================================================================*/

/*=============================================================================
EDIT HISTORY


when       who     what, where, why
--------   ---     ----------------------------------------------------------- 
04/18/14   va    Initial draft (Expose Npa Test protocol)

=============================================================================*/

#ifndef __EFIPMICNPATEST_H__
#define __EFIPMICNPATEST_H__

/*===========================================================================
INCLUDE FILES
===========================================================================*/
#include <Library/PmicLib/npa/inc/pm_npa.h>
/*===========================================================================
MACRO DECLARATIONS
===========================================================================*/
/** @addtogroup efi_pmicNpaTest_constants 
@{ */
/**
Protocol version.
*/

#define PMIC_NPATEST_REVISION 0x0000000000010001
/** @} */ /* end_addtogroup efi_pmicNpaTest_constants */

/*  Protocol GUID definition */
/** @ingroup efi_pmicNpaTest_protocol */
#define EFI_PMIC_NPATEST_PROTOCOL_GUID \
    { 0xf4286061, 0x4326, 0x44b0, { 0x98, 0x93, 0xe7, 0x5c, 0xca, 0x36, 0x74, 0x5 } };

/** @cond */
/*===========================================================================
EXTERNAL VARIABLES
===========================================================================*/
/**
External reference to the PMIC NPATEST Protocol GUID.
*/
extern EFI_GUID gQcomPmicNpaTestProtocolGuid;

/*===========================================================================
TYPE DEFINITIONS
===========================================================================*/
/**
Protocol declaration.
*/
typedef struct _EFI_QCOM_PMIC_NPATEST_PROTOCOL   EFI_QCOM_PMIC_NPATEST_PROTOCOL;
/** @endcond */


/*===========================================================================
FUNCTION DEFINITIONS
===========================================================================*/
/* EFI_PM_NPATEST_INT */ 
/** @ingroup
  @par Summary
  Initializes NPATEST module

  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PM_NPATEST_INT)(void);

/* EFI_PM_NPATEST_INT */ 
/** @ingroup
  @par Summary
  Initializes NPATEST module

  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_NPATEST_CREATETESTCLIENTS)(void);

/*PAM RESOURCEINFO */
typedef struct PMIC_NPATEST_RESOURCEINFO{
  uint32 num_of_test_nodes;
  char   *rsrc_test_name;
  pm_npa_node_resource_info *node_rsrc_arr;
  //void   *node_rsrc_arr;
}PMIC_NPATEST_RESOURCEINFO;

/* EFI_PM_NPATEST_GETRESOURCEINFO */ 
/** @ingroup
  @par Summary
  Returns PAM Info

  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_NPATEST_GETRESOURCEINFO)(
                    OUT PMIC_NPATEST_RESOURCEINFO *PamResourceInfo);


/*===========================================================================
PROTOCOL INTERFACE
===========================================================================*/
/** @ingroup efi_pmicNpaTest_protocol
@par Summary
Qualcomm PMIC NPATEST Protocol interface.

@par Parameters
@inputprotoparams{pmic_smbchg_proto_params.tex} 
*/

struct _EFI_QCOM_PMIC_NPATEST_PROTOCOL {
  UINT64                              Revision;
  EFI_PM_NPATEST_INT                  PmicNpaTestInit;
  EFI_PMIC_NPATEST_CREATETESTCLIENTS  CreateTestClients;
  EFI_PMIC_NPATEST_GETRESOURCEINFO    GetResourceInfo;
};


#endif  /* __EFIPMICNPATEST_H__ */
