#ifndef __PM_LDO_H__
#define __PM_LDO_H__

/*===========================================================================
                            PMIC VREG MODULE

DESCRIPTION
  This file contains the implementation of the VREG LDO driver developed
  for the Qualcomm Power Management IC.

Copyright (c) 2011 - 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
===========================================================================*/


/*===========================================================================
                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/11/14   al      Adding API to read VREG_OK
05/20/14   al      Architecture update 
03/26/13   al	   Adding pin ctrl, pull down and sw enable status
07/27/12   al	   Renamed all Enums,added device index and resource index
09/14/11   sm      New File.
===========================================================================*/


/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"


/*===========================================================================
                        TYPE DEFINITIONS
===========================================================================*/

/*!
 *  \brief 
 *  LDO peripheral index. This enum type contains all ldo regulators that you may need. 
 */
enum
{
  PM_LDO_1,
  PM_LDO_2,
  PM_LDO_3,
  PM_LDO_4,
  PM_LDO_5,
  PM_LDO_6,
  PM_LDO_7,
  PM_LDO_8,
  PM_LDO_9,
  PM_LDO_10,
  PM_LDO_11,
  PM_LDO_12,
  PM_LDO_13,
  PM_LDO_14,
  PM_LDO_15,
  PM_LDO_16,
  PM_LDO_17,
  PM_LDO_18,
  PM_LDO_19,
  PM_LDO_20,
  PM_LDO_21,
  PM_LDO_22,
  PM_LDO_23,
  PM_LDO_24,
  PM_LDO_25,
  PM_LDO_26,
  PM_LDO_27,
  PM_LDO_28,
  PM_LDO_29,
  PM_LDO_30,
  PM_LDO_31,
  PM_LDO_32,
  PM_LDO_33,
  PM_LDO_INVALID
};

/*===========================================================================

                        API PROTOTYPE

===========================================================================*/

/**
 * @name 
 *     pm_ldo_pull_down
 *
 * @description
 *     Allows you to enable disable active pulldown.
 * 
 * @param[in]  pmic_chip. Primary: 0. Secondary: 1
 * @param[in]  ldo_peripheral_index:
 *                LDO periphearl 
 * @param[in]  on_off:  
 *                Refer to pm_resources_and_types.h for
 *                the enum info.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_pull_down
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type on_off);

/**
 * @name 
 *     pm_ldo_pull_down_status
 *
 * @description
 *     Allows read pulldown status.
 * 
 * @param[in]  pmic_chip. Primary: 0. Secondary: 1
 * @param[in]  ldo_peripheral_index:
 *                LDO periphearl 
 * @param[out] on_off:  
 *                Refer to pm_resources_and_types.h for
 *                the enum info.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_pull_down_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type* on_off);

/**
 * @name 
 *     pm_ldo_sw_mode
 *
 * @description
 *     Switch between HPM, LPM, and other modes of a regulator.
 * 
 * @param 
 *     sw_mode - Select the different mode of a regulator. Example, HPM, LPM
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_sw_mode
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_sw_mode_type sw_mode);

/**
 * @name 
 *     pm_ldo_sw_mode_status
 *
 * @description
 *     obtain the status of a mode ( HPM, LPM etc ) at an instance.
 *     Note, the mode of a regulator changes dyanmically.
 * 
 * @param 
 *     mode - Select the different mode of a regulator. Example, HPM, LPM
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_sw_mode_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_sw_mode_type* sw_mode);

/**
 * @name 
 *     pm_ldo_pin_ctrled
 *
 * @description
 *     select the pin ( connected to external signal ) that you would like to use
 *     to effect the state ( on/off ) and mode ( HPM, LPM etc ) of a regulator
 * 
 * @param 
 *     pinCtrl - Select a pin
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_pin_ctrled
(uint8 pmic_chip, uint8 ldo_peripheral_index, uint8 select_pin);

/** 
 * @name pm_ldo_pin_ctrled_status 
 *  
 * @brief This function returns the pin ctrled status or h/w 
 *        enable status (On/Off) of the selected power rail.
 * 
 * @param[in]  pmic_chip. Primary: 0. Secondary: 1
 * @param[in]  ldo_peripheral_index:
 *                Starts from 0 (for first SMPS peripheral)
 * @param[out] on_off:  
 *                Variable to return to the caller with pin ctrl
 *                status. Refer to pm_resources_and_types.h for
 *                the enum info.
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Under error conditions
 *          like invalid param entered.
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 */
pm_err_flag_type pm_ldo_pin_ctrled_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type* on_off);

/**
 * @name 
 *     pm_ldo_volt_level
 *
 * @description
 *     set the voltage of a regulator.
 *     Note, differnt type ( LDO, HFS etc ) may have different programmable voltage steps.
 *     Only support the correct programmable steps. Not rounding voltages if the voltage selected
 *     is not part of the programmable steps.
 * 
 * @param 
 *     voltLevel - select the voltage.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_volt_level
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_volt_level_type volt_level);


/**
 * @name 
 *     pm_ldo_volt_level_status
 *
 * @description
 *     reads the voltage of a regulator. Note, differnt type (
 *     LDO, HFS etc ) may have different programmable voltage
 *     steps. Only support the correct programmable steps. Not
 *     rounding voltages if the voltage selected is not part of
 *     the programmable steps.
 * 
 * @param 
 *     voltLevel - read the voltage.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_volt_level_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_volt_level_type *volt_level);

/**
 * @name 
 *     pm_ldo_sw_enable
 *
 * @description
 *     enable or disable a regulator or voltage switcher.
 * 
 * @param 
 *     enableDisableCmd - turn on and off the regulator.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_sw_enable
(uint8 pmic_chip, uint8  ldo_peripheral_index, pm_on_off_type  on_off);

/** 
 * @name pm_ldo_sw_enable_status 
 *  
 * @brief This function returns the s/w enable status (On/Off) 
 *        of the selected power rail.
 * 
 * @param[in]  pmic_chip. Primary: 0. Secondary: 1
 * @param[in]  ldo_peripheral_index:
 *                Starts from 0 (for first SMPS peripheral)
 * @param[out] on_off:  
 *                Variable to return to the caller with s/w
 *                status. Refer to pm_resources_and_types.h for
 *                the enum info.
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Under error conditions
 *          like invalid param entered.
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 */
pm_err_flag_type pm_ldo_sw_enable_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type* on_off);


/** 
 * @name pm_ldo_vreg_ok_status 
 *  
 * @brief This function returns the vreg_ok status.
 * 
 * @param[in]  pmic_chip. Primary: 0. Secondary: 1
 * @param[in]  ldo_peripheral_index:
 *                Starts from 0 (for first LDO peripheral)
 * @param[out] status:  
 *                Variable to return to the caller with vreg_ok status 
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Under error conditions
 *          like invalid param entered.
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 */
pm_err_flag_type pm_ldo_vreg_ok_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, boolean* status);

#endif /* PM_LDO__H */

