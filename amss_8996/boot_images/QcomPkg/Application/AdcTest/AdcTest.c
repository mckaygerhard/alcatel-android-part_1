/*============================================================================
  FILE:         AdcTest.c

  OVERVIEW:     Exercises ADC EFI.

  DEPENDENCIES: None

                Copyright (c) 2011-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 01/23/15   jjo     Exercise APIs in DDIAdc.h.
 04/03/14   jjo     Added internal channels.
 03/27/14   jjo     Remove IADC.
 07/09/13   jjo     Check for invalid device / channel index.
 05/10/13   jjo     Added additional recalibration channels
 03/22/13   jjo     Added support for some targets not having IBATT_EXTERNAL
 03/05/13   jjo     Added support for IADC being removed
 01/15/13   jjo     Add test for synchronous V-I reading
 10/03/12   jjo     Added IADC tests
 09/06/12   jjo     Removed channel name length array
 07/03/12   aus     Replaced DEBUG with AsciiPrint
 07/30/11   sy      Adding TestInterface Library
 06/15/11   jdt     New File

=============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Protocol/EFIAdc.h>
#include <Library/TestInterface.h>
#include "DDIAdc.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
static const CHAR8 *vAdcChannels[] =
{
   EFI_ADC_INPUT_VPH_PWR,
   EFI_ADC_INPUT_PMIC_THERM,
   EFI_ADC_INPUT_PMIC_HARDWARE_ID,
   EFI_ADC_INPUT_PLATFORM_ID,
   EFI_ADC_INPUT_VCHG,
   EFI_ADC_INPUT_USB_IN,
   EFI_ADC_INPUT_USB_ID,
   EFI_ADC_INPUT_VBATT,
   EFI_ADC_INPUT_BATT_ID,
   EFI_ADC_INPUT_BATT_THERM,
   /* Channels internal to ADC driver */
   "VCOIN",
   "CHG_TEMP",
   "SYS_THERM1",
   "SYS_THERM2",
   "SYS_THERM3",
   "PA_THERM",
   "PA_THERM1",
   "XO_THERM",
   "USB_DATA",
   "DC_IN",
   "USB_DP",
   "USB_DN",
};

static UINT32 uNumVAdcChannels = sizeof(vAdcChannels) / sizeof(vAdcChannels[0]);

static const CHAR8 *recalAdcChannels[] =
{
   EFI_ADC_INPUT_VPH_PWR,
   EFI_ADC_INPUT_PMIC_HARDWARE_ID
};

static UINT32 uNumRecalAdcChannels = sizeof(recalAdcChannels) / sizeof(recalAdcChannels[0]);

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/
int AdcTestWrapper(void)
{
   AdcInputPropertiesType adcInputProps;
   AdcRecalibrationResultType recal;
   DalDeviceHandle *phAdcDev;
   uint32 uChannelNameLength;
   uint32 uChannelIdx;
   AdcResultType reading;
   DALResult result;

   AsciiPrint("\n\nAdcTestMain: Next test the ADC DXE wrapper\n");

   /*
    * Attach to ADC
    */
   result = DAL_AdcDeviceAttach(DALDEVICEID_ADC, &phAdcDev);
   if (result != DAL_SUCCESS)
   {
      AsciiPrint("ERROR: AdcTestMain: ADC attach failed\n");
      return -1;
   }

   AsciiPrint("AdcTestMain: attached to ADC\n");

   /*
    * Test reading all the VADC channels
    */
   AsciiPrint("AdcTestMain: Reading all ADC channels\n");

   for (uChannelIdx = 0; uChannelIdx < uNumVAdcChannels; uChannelIdx++)
   {
      uChannelNameLength = AsciiStrLen(vAdcChannels[uChannelIdx]) + 1;
      result = DalAdc_GetAdcInputProperties(phAdcDev, vAdcChannels[uChannelIdx], uChannelNameLength, &adcInputProps);
      if (result != DAL_SUCCESS)
      {
         AsciiPrint("AdcTestMain: channel %a is not present\n", vAdcChannels[uChannelIdx]);
         continue;
      }

      result = DalAdc_Read(phAdcDev, &adcInputProps, &reading);
      if (result != DAL_SUCCESS)
      {
         AsciiPrint("ERROR: AdcTestMain: DalAdc_Read failed\n");
         return -1;
      }

      if (reading.eStatus != ADC_RESULT_VALID)
      {
         AsciiPrint("ERROR: AdcTestMain: eStatus not valid for DalAdc_Read\n");
         return -1;
      }

      AsciiPrint("Channel %a: nPhysical=%d, nMicroV=%d nPercent=%d, nCode=%04x, eStatus=%d\n",
                 vAdcChannels[uChannelIdx],
                 reading.nPhysical,
                 reading.nMicrovolts,
                 reading.nPercent,
                 reading.nCode,
                 reading.eStatus);
   }

   /*
    * Test recalibration
    */
   AsciiPrint("AdcTestMain: Test recalibrating\n");

   for (uChannelIdx = 0; uChannelIdx < uNumRecalAdcChannels; uChannelIdx++)
   {
      uChannelNameLength = AsciiStrLen(recalAdcChannels[uChannelIdx]) + 1;
      result = DalAdc_GetAdcInputProperties(phAdcDev, recalAdcChannels[uChannelIdx], uChannelNameLength, &adcInputProps);
      if (result != DAL_SUCCESS)
      {
         AsciiPrint("AdcTestMain: channel %a is not present\n", vAdcChannels[uChannelIdx]);
         continue;
      }

      result = DalAdc_Recalibrate(phAdcDev, &adcInputProps, &recal);
      if (result != DAL_SUCCESS)
      {
         AsciiPrint("ERROR: AdcTestMain: ADC protocol AdcRecalibrateChannel failed\n");
         return -1;
      }

      if (recal.eStatus != ADC_RESULT_VALID)
      {
         AsciiPrint("ERROR: AdcTestMain: eStatus not valid for AdcRecalibrateChannel\n");
         return -1;
      }

      AsciiPrint("Channel %a: nPhysical1=%d uV, nPhysical2=%d uV, uCode1=%x, uCode2=%04x, eStatus=%d\n",
                 recalAdcChannels[uChannelIdx],
                 recal.nPhysical1_uV,
                 recal.nPhysical2_uV,
                 recal.uCode1,
                 recal.uCode2,
                 recal.eStatus);
   }

   return 0;
}

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/
EFI_STATUS
EFIAPI
AdcTestMain(
   IN EFI_HANDLE ImageHandle,
   IN EFI_SYSTEM_TABLE *SystemTable
   )
{
   EFI_STATUS status = EFI_SUCCESS;
   EFI_ADC_PROTOCOL *adc = NULL;
   UINT32 uChannelIdx;
   UINT32 uChannelNameLength;
   EfiAdcResultType adcResult;
   EfiAdcDeviceChannelType channel;
   EfiAdcRecalibrateChannelResultType adcRecalChanResult;

   TEST_START("AdcTest");

   AsciiPrint("AdcTestMain: First test the ADC protocl\n");

   /*
    * Load ADC protocol
    */
   status = gBS->LocateProtocol(&gEfiAdcProtocolGuid,
                                NULL,
                                (VOID**)&adc);
   if (status != EFI_SUCCESS)
   {
      AsciiPrint("ERROR: AdcTestMain: ADC protocol load failed\n");
      TestStatus("AdcTest", status);
      TEST_STOP("AdcTest");
      return status;
   }

   AsciiPrint("AdcTestMain: ADC protocol loaded\n");

   /*
    * Test reading all the ADC channels
    */
   AsciiPrint("AdcTestMain: Reading all ADC channels\n");

   for (uChannelIdx = 0; uChannelIdx < uNumVAdcChannels; uChannelIdx++)
   {
      uChannelNameLength = AsciiStrLen(vAdcChannels[uChannelIdx]) + 1;
      status = adc->GetChannel( vAdcChannels[uChannelIdx], uChannelNameLength, &channel );
      if (status != EFI_SUCCESS)
      {
         if (status == EFI_UNSUPPORTED)
         {
            AsciiPrint("AdcTestMain: channel %a is not present\n", vAdcChannels[uChannelIdx]);
            continue;
         }
         else
         {
            AsciiPrint("ERROR: AdcTestMain: ADC protocol GetChannel failed\n");
            goto end;
         }
      }

      status = adc->AdcRead( &channel, &adcResult );
      if (status != EFI_SUCCESS)
      {
         AsciiPrint("ERROR: AdcTestMain: ADC protocol AdcRead failed\n");
         goto end;
      }

      if (adcResult.eStatus != EFI_ADC_RESULT_VALID)
      {
         status = EFI_INVALID_PARAMETER;
         AsciiPrint("ERROR: AdcTestMain: eStatus not valid for AdcRead\n");
         goto end;
      }

      AsciiPrint("Channel %a: nPhysical=%d, nMicroV=%d nPercent=%d, nCode=%04x, eStatus=%d\n",
                 vAdcChannels[uChannelIdx],
                 adcResult.nPhysical,
                 adcResult.nMicrovolts,
                 adcResult.nPercent,
                 adcResult.nCode,
                 adcResult.eStatus);
   }

   /*
    * Test recalibration
    */
   AsciiPrint("AdcTestMain: Test recalibrating\n");

   for (uChannelIdx = 0; uChannelIdx < uNumRecalAdcChannels; uChannelIdx++)
   {
      uChannelNameLength = AsciiStrLen(recalAdcChannels[uChannelIdx]) + 1;
      status = adc->GetChannel(recalAdcChannels[uChannelIdx], uChannelNameLength, &channel);
      if (status != EFI_SUCCESS)
      {
         if (status == EFI_UNSUPPORTED)
         {
            AsciiPrint("AdcTestMain: channel %a is not present\n", vAdcChannels[uChannelIdx]);
            continue;
         }
         else
         {
            AsciiPrint("ERROR: AdcTestMain: ADC protocol GetChannel failed\n");
            goto end;
         }
      }

      status = adc->AdcRecalibrateChannel(&channel, &adcRecalChanResult);
      if (status != EFI_SUCCESS)
      {
         AsciiPrint("ERROR: AdcTestMain: ADC protocol AdcRecalibrateChannel failed\n");
         goto end;
      }

      if (adcRecalChanResult.eStatus != EFI_ADC_RESULT_VALID)
      {
         status = EFI_INVALID_PARAMETER;
         AsciiPrint("ERROR: AdcTestMain: eStatus not valid for AdcRecalibrateChannel\n");
         goto end;
      }

      AsciiPrint("Channel %a: nPhysical1=%d uV, nPhysical2=%d uV, uCode1=%x, uCode2=%04x, eStatus=%d\n",
                 recalAdcChannels[uChannelIdx],
                 adcRecalChanResult.nPhysical1_uV,
                 adcRecalChanResult.nPhysical2_uV,
                 adcRecalChanResult.uCode1,
                 adcRecalChanResult.uCode2,
                 adcRecalChanResult.eStatus);
   }

   if (0 != AdcTestWrapper())
   {
      status = EFI_UNSUPPORTED;
      goto end;
   }

end:
   TestStatus("AdcTest", status);
   TEST_STOP("AdcTest");

   return EFI_SUCCESS;
}

