/** @file TzRngpTest.c
   
  Test TZ RNG Lib

  Copyright (c) 2015 Copyright Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/11/15   sm      Initial Revision
=============================================================================*/

#include <Uefi.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/TestInterface.h>
#include <Library/TzRngLib.h>

 #define NUM_TEST_VALUES 10

/**
  The user Entry Point for Application. The user code starts with this function
  as the real entry point for the application.

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.  
  @param[in] SystemTable    A pointer to the EFI System Table.
  
  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurs when executing this entry point.

**/
EFI_STATUS
EFIAPI
TzRngTestEntry (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;
  UINTN RngResult[NUM_TEST_VALUES] = { 0 };
  UINT32 i,j;

  TEST_START("TzRngTest");

  AsciiPrint("Please make sure the following values are all sufficiently unique!:\n\n");

  for(i = 0; i < 10; i++)
  {
    RngResult[i] = GetRNGVal();
    if(sizeof(UINTN) == 8)
      AsciiPrint("Value %d:\t0x%L016X\n", i+1, RngResult[i]);
    else
      AsciiPrint("Value %d:\t0x%08X\n", i+1, RngResult[i]);

    // Check that it isn't 0
    if(RngResult[i] == 0)
    {
        Status = EFI_SECURITY_VIOLATION;
        TestStatus("TzRngTest", Status);
        goto Exit;      
    }

    // Check against all existing values
    for(j = 0; j < i; j++)
    {
      if(RngResult[j] == RngResult[i])
      {
        Status = EFI_SECURITY_VIOLATION;
        TestStatus("TzRngTest", Status);
        goto Exit;
      }
    }
  }

Exit:
  AsciiPrint("\n");
  TEST_STOP("TzRngTest");

  return Status;
}
