/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrv_daltf_ClockRegime_tests.c
  DESCRIPTION:	Source File for all UEFI APT ClockRegime Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#include "SysDrv_daltf_ClockRegime_tests.h"

STATIC CONST TF_ParamDescr ClockRegimeUEFITest_FreqConfParam[] =
{
	{TF_PARAM_STRING,   "Clock Regime Test 1 Clock Name",        "A valid Clock Name\n"},
	{TF_PARAM_UINT32,   "Clock Regime Test 1 Clock Frequency",   "Frequency acc. to clock  Plan\n"}
};

/*====================================================
** DATA **
====================================================*/

//CONST TF_ContextDescr context1 = { (uint8)DIAG_SUBSYS_COREBSP, 0 };

/************************************************************************************************/
/***********************************************
  Support Functions
***********************************************/

/*
STATIC UINT32 ClockRegimeUEFITest_FreqConf(UINT32 dwArg, CHAR8 *pszArg[])
{
	EFI_STATUS               Status = EFI_SUCCESS;
  	CHAR8   *szClockName;  
	UINT32        inFreqHz;
	
	szClockName = pszArg[0];
	inFreqHz    = atoint(pszArg[1]);  
  
	Status = test_clockregime(szClockName, inFreqHz);
  
	if (Status != EFI_SUCCESS)
	{
		AsciiPrint(PRINTSIG"Clock Regime UEFI Test Failed\n");
		return(TF_RESULT_CODE_FAILURE);
	}
  
	return(TF_RESULT_CODE_SUCCESS);
}
*/

UINT32 ClockRegimeUEFITest_FreqConf(UINT32 dwArg, CHAR8 *pszArg[])
{
	DALResult           eResult = DAL_ERROR;
	BOOLEAN             nFlag, bIsEnabled;
	DalDeviceHandle     *hClock = NULL;
	ClockIdType         nClockID;
	UINT32              nFreqHz;
	
	CHAR8   *szClockName;  
	UINT32        inFreqHz;
	
	szClockName = pszArg[0];
	inFreqHz    = atoint(pszArg[1]);
  
  
	//Attach to the clock DAL
	eResult = DAL_DeviceAttach(DALDEVICEID_CLOCK, &hClock);
	if(eResult != DAL_SUCCESS)
	{
		AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
		return(TF_RESULT_CODE_FAILURE);
	}

	AsciiPrint (PRINTSIG"\n==================o===================\n");
	AsciiPrint (PRINTSIG"Clock Application Driver - DAL Framework\n");
	AsciiPrint (PRINTSIG"========================================\n");
 
	eResult = DalClock_GetClockId(hClock, szClockName, &nClockID);
	if(eResult != DAL_SUCCESS)
	{
		/* This clock is either not found or something has gone wrong. 
		* Either way we can't do rest of the tests so try the next clock. */

		AsciiPrint (PRINTSIG"WARNING: Clock \"%a\" ID not obtained!\n", szClockName);
		AsciiPrint (PRINTSIG"========================================\n");
		AsciiPrint(PRINTSIG"Clock ID not obtained! \n ");
		return(TF_RESULT_CODE_FAILURE);   
    }

	/* Check IsClockOn */
    nFlag = DalClock_IsClockOn(hClock, nClockID);
    AsciiPrint (PRINTSIG"IsClockOn: %a\n", (nFlag == TRUE) ? "TRUE":"FALSE");

    /* Check IsClockEnabled */
    nFlag = DalClock_IsClockEnabled(hClock, nClockID);
    bIsEnabled = nFlag;
    AsciiPrint (PRINTSIG"IsClockEnabled: %a\n", (nFlag == TRUE) ? "TRUE":"FALSE");

    /* If clock is not enabled, then enable it */
    if(!bIsEnabled)
	{
		eResult = DalClock_EnableClock(hClock, nClockID);
		if(eResult != DAL_SUCCESS) 
			AsciiPrint (PRINTSIG"WARNING: EnableClock: returned an error!\n");
		else
			AsciiPrint (PRINTSIG"EnableClock: SUCCESS\n");

		/* Recheck IsClockEnabled */
		nFlag = DalClock_IsClockEnabled(hClock, nClockID);
		AsciiPrint (PRINTSIG"Rechecking IsClockEnabled: %a\n", (nFlag == TRUE) ? "TRUE":"FALSE");

		/* Recheck IsClockOn */
		nFlag = DalClock_IsClockOn(hClock, nClockID);
		AsciiPrint (PRINTSIG"Rechecking IsClockOn: %a\n", (nFlag == TRUE) ? "TRUE":"FALSE");

		/* Only change/set clock frequency of the clocks that were enabled by us */
		if(inFreqHz > 0)
		{
			/*Try exact frequency match*/
			eResult = DalClock_SetClockFrequency(hClock, nClockID, inFreqHz, 
													(ClockFrequencyType) 3, (UINT32 *)&nFreqHz);
			if(eResult != DAL_SUCCESS) 
				AsciiPrint (PRINTSIG"WARNING: SetClockFrequency: returned an error!\n");
			else
				AsciiPrint (PRINTSIG"SetClockFrequency: %d\n", nFreqHz);
			    
			/* Get clock frequency	*/
			eResult = DalClock_GetClockFrequency(hClock, nClockID, (UINT32 *)&nFreqHz);
			if(eResult != DAL_SUCCESS)
				AsciiPrint (PRINTSIG"WARNING: GetClockFrequency: returned an error!\n");
			else
				AsciiPrint (PRINTSIG"GetClockFrequency: %d\n", nFreqHz);
		}
    }
	
	/* Calculate clock frequency  */
    eResult = DalClock_CalcClockFrequency(hClock, nClockID, (UINT32 *)&nFreqHz);
    if(eResult != DAL_SUCCESS) 
		AsciiPrint (PRINTSIG"WARNING: CalcClockFrequency: returned an error!\n");
    else
		AsciiPrint (PRINTSIG"CalcClockFrequency: %d\n", nFreqHz);
  	
	/* Only disable clocks that weren't enabled by us */
    if(!bIsEnabled)
    {
		eResult = DalClock_DisableClock(hClock, nClockID);
		if(eResult != DAL_SUCCESS) 
		{
			/* DisableClock */
			AsciiPrint (PRINTSIG"WARNING: DisableClock: returned an error!\n");
		}
		else
			AsciiPrint (PRINTSIG"DisableClock: SUCCESS\n");

		/* Recheck IsClockEnabled */
		nFlag = DalClock_IsClockEnabled(hClock, nClockID);
		AsciiPrint (PRINTSIG"Rechecking IsClockEnabled: %a\n", (nFlag == TRUE) ? "TRUE":"FALSE");
  
		/* Recheck IsClockOn*/
		nFlag = DalClock_IsClockOn(hClock, nClockID);
		AsciiPrint (PRINTSIG"Rechecking IsClockOn: %a\n", (nFlag == TRUE) ? "TRUE":"FALSE");
    }
	
	if(eResult == DAL_SUCCESS)
		AsciiPrint (PRINTSIG"ClockRegime: Clock Regime Frequency Configuration Test successful.\n");
	else {
		AsciiPrint (PRINTSIG"ClockRegime: Clock Regime Frequency Configuration Test unsuccessful.\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	return(TF_RESULT_CODE_SUCCESS);
}

CONST TF_HelpDescr ClockRegimeUEFITest_FreqConfHelp =
{
	"ClockRegimeUEFITest",
    sizeof(ClockRegimeUEFITest_FreqConfParam) / sizeof(TF_ParamDescr),
    ClockRegimeUEFITest_FreqConfParam
};

CONST TF_TestFunction_Descr ClockRegimeUEFITest_FreqConfTest =
{
   PRINTSIG"ClockRegimeUEFITest_FreqConf",
   ClockRegimeUEFITest_FreqConf,
   &ClockRegimeUEFITest_FreqConfHelp,
   &context1,
   0
};

CONST TF_TestFunction_Descr* ClockRegime_daltf_tests[] = {
	&ClockRegimeUEFITest_FreqConfTest
};

DALResult ClockRegime_daltf_init(VOID)
{
	DALResult dalResult = 0;
	UINT16 TF_NumberTests = (sizeof(ClockRegime_daltf_tests) / sizeof(ClockRegime_daltf_tests[0]));
	dalResult = tests_daltf_add_tests(ClockRegime_daltf_tests, TF_NumberTests);
	return dalResult;
}

DALResult ClockRegime_daltf_deinit(VOID)
{
	DALResult dalResult = 0;
	UINT16 TF_NumberTests = (sizeof(ClockRegime_daltf_tests) / sizeof(ClockRegime_daltf_tests[0]));

	dalResult = tests_daltf_remove_tests(ClockRegime_daltf_tests, TF_NumberTests);
	return dalResult;
}


