/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrv_daltf_dal_intctl_tests.h
  DESCRIPTION:	Header File for all UEFI APT dal_intctl Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/
#ifndef _SYSTEMDRIVERS_DALTF_TEST_DAL_INTCTL_
#define _SYSTEMDRIVERS_DALTF_TEST_DAL_INTCTL_

/*=========================================================================

@file         systemdrivers_daltf_dal_intctl_tests.h
@brief        System Drivers Subsystem DAL INTCTL driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the systemdrivers_daltf_dal_intctl_tests.c module.

Copyright (c) 2010 QUALCOMM Technologies Inc. All Rights Reserved. Qualcomm Confidential and Proprietary
==========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.
 
  $Header: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/SysDrvTest/SysDrv_daltf_dal_intctl_tests.h#3 $
  $DateTime: 2015/06/01 21:35:43 $
  $Author: pwbldsvc $

when       who     what, where, why 
--------   ---     --------------------------------------------------------- 
07/12/10   c_pp     Initial version.

==========================================================================*/
 
/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

==========================================================================*/

#include "SysDrvTest.h"
#include "DDIInterruptController.h"

/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/

/*=========================================================================
      Constants and Macros 
==========================================================================*/

#define TEST_DALINTCTL_MAX_DAL_NO            172 // maximum number of DALs on target
#define TEST_DALINTCTL_MAX_INT_TRIG           10
#define TEST_DALOINTCTL_DAL_CFG_OE           0x200

/*=========================================================================
      Variables 
==========================================================================*/


/*===========================================================================

                      FUNCTION DECLARATIONS

===========================================================================*/

//extern CONST TF_ContextDescr context1;
//UINT32 atoint (CHAR8 *);

DALResult systemdrivers_daltf_init_dal_intctl(VOID);
DALResult systemdrivers_daltf_deinit_dal_intctl(VOID);

#endif // _SYSTEMDRIVERS_DALTF_TEST_DAL_INTCTL_

