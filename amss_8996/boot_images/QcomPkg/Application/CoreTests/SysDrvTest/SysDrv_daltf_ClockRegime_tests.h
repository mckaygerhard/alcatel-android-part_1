/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrv_daltf_ClockRegime_tests.h
  DESCRIPTION:	Header File for all UEFI APT ClockRegime Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#ifndef _SysDrv_DALTF_CLOCKREGIME_TESTS_
#define _SysDrv_DALTF_CLOCKREGIME_TESTS_

#include "SysDrvTest.h"

#include <Library/DebugLib.h>
#include "DDIClock.h"

/*====================================================
** STRUCTURES **
====================================================*/

UINT32 ClockRegimeUEFITest_FreqConf(UINT32 dwArg, CHAR8 *pszArg[]); 
 
//Initialize ClockRegime daltf tests
DALResult ClockRegime_daltf_init(VOID);

//De-Initialize ClockRegime daltf tests
DALResult ClockRegime_daltf_deinit(VOID);

#endif
