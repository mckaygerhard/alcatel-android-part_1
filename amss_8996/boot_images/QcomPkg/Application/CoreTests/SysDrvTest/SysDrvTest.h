/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrvTest.h
  DESCRIPTION:	Common headers included for all SysDrv files
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#ifndef _SysDrv_TESTS_
#define _SysDrv_TESTS_

#include "tests_daltf_common.h"   /* Common definitions */

#include <Protocol/EFITlmm.h>

#include "SysDrv_daltf_ClockRegime_tests.h"
#include "SysDrv_daltf_ChipInfo_tests.h"
#include "SysDrv_daltf_TLMM_tests.h"
#include "SysDrv_daltf_Platforminfo_tests.h"
#include "SysDrv_daltf_dal_intctl_tests.h"

#endif
