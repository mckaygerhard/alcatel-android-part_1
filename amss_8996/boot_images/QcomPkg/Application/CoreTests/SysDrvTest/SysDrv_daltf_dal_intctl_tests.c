/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrv_daltf_dal_intctl_tests.c
  DESCRIPTION:	Source File for all UEFI APT dal_intctl Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/
/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

=========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR Dal INTCTL. DRIVER TESTS

==========================================================================*/
#include "SysDrv_daltf_dal_intctl_tests.h"
/*==========================================================================

                  DEFINITIONS AND DECLARATIONS FOR MODULE

==========================================================================*/

/*=========================================================================
      Constants and Macros 
==========================================================================*/

// Dal Intctl. driver test's common Parameters
CONST TF_ParamDescr dalIntctlTestParam[] =
{
    {TF_PARAM_UINT32, "InterruptID", "intrID: 0 - 500 \n"},
};


/*=========================================================================
      Variables 
==========================================================================*/

STATIC volatile UINT32 gFlag = 0;

//STATIC volatile UINT32 pendFlag = 0;

STATIC UINT32 intrID;

STATIC DALISR gIsr;

STATIC UINT32 bState = 0;

UINT32 hDalIntctlBasePtr;

/*=========================================================================
      Functions
==========================================================================*/

//Sub functions called internally
UINT32 dalIntctlTestOpen(DalDeviceHandle **hDalIntctl);
UINT32 dalIntctlTestClose(DalDeviceHandle *hDalIntctl);

//input config functions
UINT32 dalIntctlTestInit(UINT32 dwArg, CHAR8* pszArg[],UINT32 *intrID);

/*test logic implemented functions*/
UINT32 dalIntctlParamGenTest(DalDeviceHandle *hDalIntctl);
UINT32 dalIntctlRegisterISRTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID, DALISR gIsr);
UINT32 dalIntctlUnregisterTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID,DALISR gIsr);
UINT32 dalIntctlIsInterruptEnabledTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID,DALISR gIsr);
UINT32 dalIntctlTriggerInterruptTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID,DALISR gIsr);
UINT32 dalIntctlInterruptStatusTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID,DALISR gIsr);
UINT32 dalIntctlIsInterruptPendingTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID,DALISR gIsr);
UINT32 dalIntctlInterruptClearTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID);

/* Declaration of each ISR corresponding to different tests */
VOID isr_test_dal_intctl_register_isr(VOID* regIsrParam);
VOID isr_test_dal_intctl_unregister(VOID* regIsrParam);
VOID isr_test_dal_intctl_is_interrupt_enabled(VOID* regIsrParam);
VOID isr_test_dal_intctl_interrupt_status(VOID* regIsrParam);
VOID isr_test_dal_intctl_trigger_interrupt(VOID* regIsrParam);
//VOID isr_test_dal_intctl_interrupt_clear(VOID* regIsrParam);
VOID isr_test_dal_intctl_is_interrupt_pending(VOID* regIsrParam);



/*==========================================================================

                         FUNCTIONS FOR MODULE

==========================================================================*/

/*=========================================================================
      ISRs for dal intctl driver tests
==========================================================================*/

VOID isr_test_dal_intctl_register_isr(VOID* regIsrParam){
   AsciiPrint(PRINTSIG"The ISR for the interrupt is called");  
   gFlag = 1;
}

VOID isr_test_dal_intctl_unregister(VOID* regIsrParam){
   AsciiPrint(PRINTSIG"The ISR for the interrupt is called");  
   gFlag = 1;
}

VOID isr_test_dal_intctl_is_interrupt_enabled(VOID* regIsrParam){
   AsciiPrint(PRINTSIG"The ISR for the interrupt is called");  
   gFlag = 1;
}

VOID isr_test_dal_intctl_trigger_interrupt(VOID* regIsrParam){
   AsciiPrint(PRINTSIG"The ISR for the interrupt is called");  
   gFlag = 1;
}

VOID isr_test_dal_intctl_interrupt_status(VOID* regIsrParam){
   AsciiPrint(PRINTSIG"The ISR for the interrupt is called");  
   gFlag = 1;
}

/*VOID isr_test_dal_intctl_interrupt_clear(VOID* regIsrParam){
   AsciiPrint(PRINTSIG"The ISR for the interrupt is called");  
   gFlag = 1;
}*/

VOID isr_test_dal_intctl_is_interrupt_pending(VOID* regIsrParam){
   
   DALResult eDalResult = DAL_SUCCESS;
   DalDeviceHandle *temp = NULL;
   temp = (DalDeviceHandle *)regIsrParam;
      
   eDalResult = DalInterruptController_IsInterruptPending(temp, 21, (VOID *) &bState);
   if(DAL_SUCCESS != eDalResult){
   AsciiPrint(PRINTSIG"API not returning success");  
   }else {
   AsciiPrint(PRINTSIG"Interrupt has been called"); 
   gFlag = 1;
   }
}



/*=========================================================================
      Dal Intctl. driver test functions
==========================================================================*/

/* ============================================================================
**  Function : sysdrvDalIntctlParamGen
** ========================================================================== */
/*!
   @brief
   A Dal Intctl. driver DalTF test case to check which interrupt IDs are not enabled.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS on Success
   TF_RESULT_CODE_FAILURE on Failure
   TF_RESULT_CODE_BAD_PARAM on entering bad parameters

*/

UINT32 sysdrvDalIntctlParamGen(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hDalIntctl = NULL;
   UINT32 testResult;
 
   AsciiPrint(PRINTSIG"Entering sysdrvDalIntctlParamGen function");

   /*This function connects to dal interrupt controller device and returns a handle*/
   testResult = dalIntctlTestOpen(&hDalIntctl);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
       return TF_RESULT_CODE_FAILURE;
   }

   /*This function verifies the if the any interrupts are enabled*/
   testResult = dalIntctlParamGenTest(hDalIntctl);
   if(TF_RESULT_CODE_SUCCESS != testResult) {

      /*This function detaches the handle from the dal interrupt controller device*/
      if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
         return TF_RESULT_CODE_FAILURE;
      }
      return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the dal interrupt controller device*/
   if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
       return TF_RESULT_CODE_FAILURE;
   }
   
   AsciiPrint(PRINTSIG"Test Passed");

   return TF_RESULT_CODE_SUCCESS;
}

// sysdrvDalIntctlRegisterISRTest's Help Data
CONST TF_HelpDescr sysdrvDalIntctlParamGenHelp =
{
   "sysdrvDalIntctlParamGen",0,NULL
};

CONST TF_TestFunction_Descr sysdrvDalIntctlParamGenTest =
{
   PRINTSIG"sysdrvDalIntctlParamGen",
  sysdrvDalIntctlParamGen,
   &sysdrvDalIntctlParamGenHelp,
   &context1,
   0
};

/* ============================================================================
**  Function : sysdrvDalIntctlAttach
** ========================================================================== */
/*!
   @brief
   A DALIntCtl driver DalTF test case to test the DalIntCtl device attach.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS on Success
   TF_RESULT_CODE_FAILURE on Failure

*/

UINT32 sysdrvDalIntctlAttach(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hDalIntctl = NULL;

   AsciiPrint(PRINTSIG"Entering sysdrvDalIntctlAttach function");
       
   /*This function connects to dal interrupt controller device and returns a handle*/ 
   if(TF_RESULT_CODE_SUCCESS != dalIntctlTestOpen(&hDalIntctl)) {
       return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the dal interrupt controller device*/
   if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
       return TF_RESULT_CODE_FAILURE;
   }
   
   AsciiPrint(PRINTSIG"Test Passed");
   
   return TF_RESULT_CODE_SUCCESS;
}

// sysdrvDalIntctlAttachTest's Help Data
CONST TF_HelpDescr sysdrvDalIntctlAttachHelp =
{
   "sysdrvDalIntctlAttach",0,NULL
};

CONST TF_TestFunction_Descr sysdrvDalIntctlAttachTest =
{
   PRINTSIG"sysdrvDalIntctlAttach",
  sysdrvDalIntctlAttach,
   &sysdrvDalIntctlAttachHelp,
   &context1,
   0
};

/* ============================================================================
**  Function : sysdrvDalIntctlRegisterISR
** ========================================================================== */
/*!
   @brief
   A Dal Intctl. driver DalTF test case to test the DALInt_RegisterIsr API.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS on Success
   TF_RESULT_CODE_FAILURE on Failure
   TF_RESULT_CODE_BAD_PARAM on entering bad parameters

*/

UINT32 sysdrvDalIntctlRegisterISR(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hDalIntctl = NULL;
   UINT32 testResult;
   
   AsciiPrint(PRINTSIG"Entering sysdrvDalIntctlRegisterISR function");
   
   /*This function verifies for bad and out of range input parameters*/
   testResult = dalIntctlTestInit(dwArg,pszArg,&intrID);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /*This function connects to dal interrupt controller device and returns a handle*/ 
   testResult = dalIntctlTestOpen(&hDalIntctl);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
       return TF_RESULT_CODE_FAILURE;
   }

   gIsr = (DALISR)isr_test_dal_intctl_register_isr;

   /*This function verifies the ISR invoke after
   registering the ISR using the API*/
   testResult = dalIntctlRegisterISRTest(hDalIntctl,intrID,gIsr);
   if(TF_RESULT_CODE_SUCCESS != testResult) {

      /*This function detaches the handle from the dal interrupt controller device*/
      if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
         return TF_RESULT_CODE_FAILURE;
      }
      return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the dal interrupt controller device*/
   if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
       return TF_RESULT_CODE_FAILURE;
   }
   
   AsciiPrint(PRINTSIG"Test Passed");

   return TF_RESULT_CODE_SUCCESS;
}

// sysdrvDalIntctlRegisterISRTest's Help Data
CONST TF_HelpDescr sysdrvDalIntctlRegisterISRHelp =
{
   "sysdrvDalIntctlRegisterISR",
   sizeof(dalIntctlTestParam) / sizeof(TF_ParamDescr), 
   dalIntctlTestParam
};

CONST TF_TestFunction_Descr sysdrvDalIntctlRegisterISRTest =
{
   PRINTSIG"sysdrvDalIntctlRegisterISR",
  sysdrvDalIntctlRegisterISR,
   &sysdrvDalIntctlRegisterISRHelp,
   &context1,
   0
};


/* ============================================================================
**  Function : sysdrvDalIntctlUnregister
** ========================================================================== */
/*!
   @brief
   A Dal Intctl. driver DalTF test case to test the DalInterruptController_Unregister API.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS on Success
   TF_RESULT_CODE_FAILURE on Failure
   TF_RESULT_CODE_BAD_PARAM on entering bad parameters

*/

UINT32 sysdrvDalIntctlUnregister(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hDalIntctl = NULL;
   UINT32 testResult;
   
   AsciiPrint(PRINTSIG"Entering sysdrvDalIntctlUnregister function");

   /*This function verifies for bad and out of range input parameters*/
   testResult = dalIntctlTestInit(dwArg,pszArg,&intrID);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /*This function connects to dal interrupt controller device and returns a handle*/ 
   testResult = dalIntctlTestOpen(&hDalIntctl);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
       return TF_RESULT_CODE_FAILURE;
   }

   gIsr = (DALISR)isr_test_dal_intctl_unregister;

   /*This function verifies the ISR invoke failure after
   deregistering the ISR using the API*/
   testResult = dalIntctlUnregisterTest(hDalIntctl,intrID,gIsr);
   if(TF_RESULT_CODE_SUCCESS != testResult) {

      /*This function detaches the handle from the dal interrupt controller device*/
      if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
         return TF_RESULT_CODE_FAILURE;
      }
      return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the dal interrupt controller device*/
   if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
       return TF_RESULT_CODE_FAILURE;
   }
   
   AsciiPrint(PRINTSIG"Test Passed");

   return TF_RESULT_CODE_SUCCESS;
}


// sysdrvDalIntctlUnregisterTest's Help Data
CONST TF_HelpDescr sysdrvDalIntctlUnregisterHelp =
{
   "sysdrvDalIntctlUnregister",
   sizeof(dalIntctlTestParam) / sizeof(TF_ParamDescr), 
   dalIntctlTestParam
};

CONST TF_TestFunction_Descr sysdrvDalIntctlUnregisterTest =
{
   PRINTSIG"sysdrvDalIntctlUnregister",
  sysdrvDalIntctlUnregister,
   &sysdrvDalIntctlUnregisterHelp,
   &context1,
   0
};


/* ============================================================================
**  Function : sysdrvDalIntctlTriggerInterrupt
** ========================================================================== */
/*!
   @brief
   A Dal Intctl. driver DalTF test case to test the DALInt_TriggerInterrupt API.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS on Success
   TF_RESULT_CODE_FAILURE on Failure
   TF_RESULT_CODE_BAD_PARAM on entering bad parameters

*/

UINT32 sysdrvDalIntctlTriggerInterrupt(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hDalIntctl = NULL;
   UINT32 testResult;
   
   AsciiPrint(PRINTSIG"Entering sysdrvDalIntctlTriggerInterrupt function");

   /*This function verifies for bad and out of range input parameters*/
   testResult = dalIntctlTestInit(dwArg,pszArg,&intrID);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /*This function connects to dal interrupt controller device and returns a handle*/ 
   testResult = dalIntctlTestOpen(&hDalIntctl);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
       return TF_RESULT_CODE_FAILURE;
   }

   gIsr = (DALISR)isr_test_dal_intctl_trigger_interrupt;

   /*This function verifies the ISR invoke after
   registering the ISR using the API*/
   testResult = dalIntctlTriggerInterruptTest(hDalIntctl,intrID,gIsr);
   if(TF_RESULT_CODE_SUCCESS != testResult) {

      /*This function detaches the handle from the dal interrupt controller device*/
      if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
         return TF_RESULT_CODE_FAILURE;
      }
      return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the dal interrupt controller device*/
   if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
       return TF_RESULT_CODE_FAILURE;
   }
   
   AsciiPrint(PRINTSIG"Test Passed");

   return TF_RESULT_CODE_SUCCESS;
}


// sysdrvGpioIntctlTriggerInterruptTest's Help Data
CONST TF_HelpDescr sysdrvDalIntctlTriggerInterruptHelp =
{
   "sysdrvDalIntctlTriggerInterrupt",
   sizeof(dalIntctlTestParam) / sizeof(TF_ParamDescr), 
   dalIntctlTestParam
};

CONST TF_TestFunction_Descr sysdrvDalIntctlTriggerInterruptTest =
{
   PRINTSIG"sysdrvDalIntctlTriggerInterrupt",
  sysdrvDalIntctlTriggerInterrupt,
   &sysdrvDalIntctlTriggerInterruptHelp,
   &context1,
   0
};

/* ============================================================================
**  Function : sysdrvDalIntctlIsInterruptEnabled
** ========================================================================== */
/*!
   @brief
   A Dal Intctl. driver DalTF test case to test the DALInt_IsInterruptEnabled API.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS on Success
   TF_RESULT_CODE_FAILURE on Failure
   TF_RESULT_CODE_BAD_PARAM on entering bad parameters

*/

UINT32 sysdrvDalIntctlIsInterruptEnabled(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hDalIntctl = NULL;
   UINT32 testResult;
   

   AsciiPrint(PRINTSIG"Entering sysdrvDalIntctlIsInterruptEnabled function");

   /*This function verifies for bad and out of range input parameters*/
   testResult = dalIntctlTestInit(dwArg,pszArg,&intrID);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /*This function connects to dal interrupt controller device and returns a handle*/ 
   testResult = dalIntctlTestOpen(&hDalIntctl);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
       return TF_RESULT_CODE_FAILURE;
   }

   gIsr = (DALISR)isr_test_dal_intctl_is_interrupt_enabled;

   /*This function verifies if the interrupt is enabled using the API
   after registering the ISR*/
   testResult = dalIntctlIsInterruptEnabledTest(hDalIntctl,intrID,gIsr);
   if(TF_RESULT_CODE_SUCCESS != testResult) {

      /*This function detaches the handle from the dal interrupt controller device*/
      if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
         return TF_RESULT_CODE_FAILURE;
      }
      return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the dal interrupt controller device*/
   if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
       return TF_RESULT_CODE_FAILURE;
   }
   
   AsciiPrint(PRINTSIG"Test Passed");

   return TF_RESULT_CODE_SUCCESS;
}


// sysdrvDalIntctlIsInterruptEnabledTest's Help Data
CONST TF_HelpDescr sysdrvDalIntctlIsInterruptEnabledHelp =
{
   "sysdrvDalIntctlIsInterruptEnabled",
   sizeof(dalIntctlTestParam) / sizeof(TF_ParamDescr), 
   dalIntctlTestParam
};

CONST TF_TestFunction_Descr sysdrvDalIntctlIsInterruptEnabledTest =
{
   PRINTSIG"sysdrvDalIntctlIsInterruptEnabled",
  sysdrvDalIntctlIsInterruptEnabled,
   &sysdrvDalIntctlIsInterruptEnabledHelp,
   &context1,
   0
};

/* ============================================================================
**  Function : sysdrvDalIntctlInterruptStatus
** ========================================================================== */
/*!
   @brief
   A Dal Intctl. driver DalTF test case to test the DalInterruptController_InterruptStatus API.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS on Success
   TF_RESULT_CODE_FAILURE on Failure
   TF_RESULT_CODE_BAD_PARAM on entering bad parameters

*/

UINT32 sysdrvDalIntctlInterruptStatus(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hDalIntctl = NULL;
   UINT32 testResult;
   
   AsciiPrint(PRINTSIG"Entering sysdrvDalIntctlInterruptStatus function");

   /*This function verifies for bad and out of range input parameters*/
   testResult = dalIntctlTestInit(dwArg,pszArg,&intrID);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /*This function connects to dal interrupt controller device and returns a handle*/ 
   testResult = dalIntctlTestOpen(&hDalIntctl);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
       return TF_RESULT_CODE_FAILURE;
   }

   gIsr = (DALISR)isr_test_dal_intctl_interrupt_status;

   /*This function verifies if the interrupt is enabled using the API
   after registering the ISR*/
   testResult = dalIntctlInterruptStatusTest(hDalIntctl,intrID,gIsr);
   if(TF_RESULT_CODE_SUCCESS != testResult) {

      /*This function detaches the handle from the dal interrupt controller device*/
      if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
         return TF_RESULT_CODE_FAILURE;
      }
      return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the dal interrupt controller device*/
   if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
       return TF_RESULT_CODE_FAILURE;
   }
   
   AsciiPrint(PRINTSIG"Test Passed");

   return TF_RESULT_CODE_SUCCESS;
}

// sysdrvDalIntctlInterruptStatusTest's Help Data
CONST TF_HelpDescr sysdrvDalIntctlInterruptStatusHelp =
{
   "sysdrvDalIntctlInterruptStatus",
   sizeof(dalIntctlTestParam) / sizeof(TF_ParamDescr), 
   dalIntctlTestParam
};

CONST TF_TestFunction_Descr sysdrvDalIntctlInterruptStatusTest =
{
   PRINTSIG"sysdrvDalIntctlInterruptStatus",
  sysdrvDalIntctlInterruptStatus,
   &sysdrvDalIntctlInterruptStatusHelp,
   &context1,
   0
};


/* ============================================================================
**  Function : sysdrvDalIntctlIsInterruptPending
** ========================================================================== */
/*!
   @brief
   A Dal Intctl. driver DalTF test case to test the DalInterruptController_IsInterruptPending API.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS on Success
   TF_RESULT_CODE_FAILURE on Failure
   TF_RESULT_CODE_BAD_PARAM on entering bad parameters

*/

UINT32 sysdrvDalIntctlIsInterruptPending(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hDalIntctl = NULL;
   UINT32 testResult;
   
   AsciiPrint(PRINTSIG"Entering sysdrvDalIntctlIsInterruptPending function");

   /*This function verifies for bad and out of range input parameters*/
   testResult = dalIntctlTestInit(dwArg,pszArg,&intrID);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /*This function connects to dal interrupt controller device and returns a handle*/ 
   testResult = dalIntctlTestOpen(&hDalIntctl);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
       return TF_RESULT_CODE_FAILURE;
   }

   gIsr = (DALISR)isr_test_dal_intctl_is_interrupt_pending;

   /*This function verifies if the interrupt is enabled using the API
   after registering the ISR*/
   testResult = dalIntctlIsInterruptPendingTest(hDalIntctl,intrID,gIsr);
   if(TF_RESULT_CODE_SUCCESS != testResult) {

      /*This function detaches the handle from the dal interrupt controller device*/
      if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
         return TF_RESULT_CODE_FAILURE;
      }
      return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the dal interrupt controller device*/
   if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
       return TF_RESULT_CODE_FAILURE;
   }
   
   AsciiPrint(PRINTSIG"Test Passed");

   return TF_RESULT_CODE_SUCCESS;
}

// sysdrvDalIntctlInterruptStatusTest's Help Data
CONST TF_HelpDescr sysdrvDalIntctlIsInterruptPendingHelp =
{
   "sysdrvDalIntctlIsInterruptPending",
   sizeof(dalIntctlTestParam) / sizeof(TF_ParamDescr), 
   dalIntctlTestParam
};

CONST TF_TestFunction_Descr sysdrvDalIntctlIsInterruptPendingTest =
{
   PRINTSIG"sysdrvDalIntctlIsInterruptPending",
  sysdrvDalIntctlIsInterruptPending,
   &sysdrvDalIntctlIsInterruptPendingHelp,
   &context1,
   0
};


/* ============================================================================
**  Function : sysdrvDalIntctlInterruptClear
** ========================================================================== */
/*!
   @brief
   A Dal Intctl. driver DalTF test case to test the DalInterruptController_InterruptClear API.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS on Success
   TF_RESULT_CODE_FAILURE on Failure
   TF_RESULT_CODE_BAD_PARAM on entering bad parameters

*/

UINT32 sysdrvDalIntctlInterruptClear(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hDalIntctl = NULL;
   UINT32 testResult;
   
   AsciiPrint(PRINTSIG"Entering sysdrvDalIntctlInterruptClear function");

   /*This function verifies for bad and out of range input parameters*/
   testResult = dalIntctlTestInit(dwArg,pszArg,&intrID);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /*This function connects to dal interrupt controller device and returns a handle*/ 
   testResult = dalIntctlTestOpen(&hDalIntctl);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
       return TF_RESULT_CODE_FAILURE;
   }

   //gIsr = (DALISR)isr_test_dal_intctl_interrupt_clear;

   /*This function verifies if the interrupt is cleared using the API
   without registering the ISR*/
   testResult = dalIntctlInterruptClearTest(hDalIntctl,intrID);
   if(TF_RESULT_CODE_SUCCESS != testResult) {

      /*This function detaches the handle from the dal interrupt controller device*/
      if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
         return TF_RESULT_CODE_FAILURE;
      }
      return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the dal interrupt controller device*/
   if(TF_RESULT_CODE_SUCCESS != dalIntctlTestClose(hDalIntctl)) {
       return TF_RESULT_CODE_FAILURE;
   }
   
   AsciiPrint(PRINTSIG"Test Passed");

   return TF_RESULT_CODE_SUCCESS;
}

// sysdrvDalIntctlInterruptStatusTest's Help Data
CONST TF_HelpDescr sysdrvDalIntctlInterruptClearHelp =
{
   "sysdrvDalIntctlInterruptClear",
   sizeof(dalIntctlTestParam) / sizeof(TF_ParamDescr), 
   dalIntctlTestParam
};

CONST TF_TestFunction_Descr sysdrvDalIntctlInterruptClearTest =
{
   PRINTSIG"sysdrvDalIntctlInterruptClear",
  sysdrvDalIntctlInterruptClear,
   &sysdrvDalIntctlInterruptClearHelp,
   &context1,
   0
};



/*TEST LOGIC IMPLEMENTED FUNCTIONS*/

/*========================================================================== 
                          dalIntctlParamGenTest
  ==========================================================================  
  FUNCTION dalIntctlParamGenTest

  DESCRIPTION
  This function is called to test if the interrupt is enabled or not.

  PARAMETERS
  *hDalIntctl      [IN]  dalintctl. device handle
  intrID          [IN]  interrupt ID
  gIsr              [IN]  ISR function pointer 

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 dalIntctlParamGenTest(DalDeviceHandle *hDalIntctl){

   UINT32 bstate =0;  
   UINT32 i = 0;
   UINT32 cTestableInt = 0;
   UINT32 cNonTestableInt = 0;
   UINT32 cIntTestLen = 500;
  
   for(i = 0; i < cIntTestLen; i++) {
      
	  DalInterruptController_IsInterruptEnabled(hDalIntctl,i,&bstate);
      if(0 == bstate){
         AsciiPrint(PRINTSIG"Non enabled interrupt number = %d \n", cTestableInt);
         cTestableInt++;
      }else{
	     AsciiPrint(PRINTSIG"Enabled interrupt number = %d \n", cNonTestableInt);
         cNonTestableInt++;
      }
   }

   AsciiPrint(PRINTSIG"Number of non-Enabled Interrupts which can be tested: %d", cTestableInt);
   AsciiPrint(PRINTSIG"Number of Enabled Interrupts which are not going to be tested: %d", cNonTestableInt);
   AsciiPrint(PRINTSIG" ");
   
   return TF_RESULT_CODE_SUCCESS;

}

/*========================================================================== 
                          dalIntctlRegisterISRTest
  ==========================================================================  
  FUNCTION dalIntctlRegisterISRTest

  DESCRIPTION
  This function is called to test the DalInterruptController_RegisterISR API.

  PARAMETERS
  *hDalIntctl      [IN]  dalintctl. device handle
  intrID          [IN]  interrupt ID
  gIsr              [IN]  ISR function pointer 

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 dalIntctlRegisterISRTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID,DALISR gIsr){

   //STATIC DalDeviceHandle *hdalIntctl = NULL;
   UINT32 bstate =0;
  
 
   /* Null pointer exception check on ISR function pointer */
   if(NULL == gIsr) {
      AsciiPrint(PRINTSIG"Null pointer exception on ISR function pointer");
      return TF_RESULT_CODE_FAILURE;
   }

   /*Register an ISR for the DAL*/
   DalInterruptController_RegisterISR(hDalIntctl,intrID, gIsr, hDalIntctl,0x00000004);

   /*Verify if interrupt is enabled*/
   DalInterruptController_IsInterruptEnabled(hDalIntctl,intrID, &bstate);
   
   /*Deregister an ISR for the DAL*/
   DalInterruptController_Unregister(hDalIntctl,intrID);

   if(0 == bstate){
      AsciiPrint(PRINTSIG"ISR is not registered for the interrupt %d",intrID);
      return TF_RESULT_CODE_FAILURE;
   }

   /*Verify if interrupt is enabled*/
     DalInterruptController_IsInterruptEnabled(hDalIntctl,intrID, &bstate);

   if(1 == bstate){
      AsciiPrint(PRINTSIG"ISR is not deregistered for the interrupt %d",intrID);
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}

/*========================================================================== 
                        dalIntctlUnregisterTest
  ========================================================================== 
 
  FUNCTION dalIntctlUnregisterTest

  DESCRIPTION
  This function is called to test the DALInt_DeregisterIsr API.

  PARAMETERS
  *hDalIntctl      [IN]  dalintctl. device handle
  intrID          [IN]  Interrupt ID
  gIsr              [IN]  ISR function pointer 

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 dalIntctlUnregisterTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID,DALISR gIsr){

   UINT32 nTimeout = 0;
   UINT32 bstate =1;
   
   /* Null pointer exception check on ISR function pointer */
   if(NULL == gIsr) {
      AsciiPrint(PRINTSIG"Null pointer exception on ISR function pointer");
      return TF_RESULT_CODE_FAILURE;
   }

   /*Register an ISR */
    DalInterruptController_RegisterISR(hDalIntctl,intrID, gIsr, hDalIntctl,0x00000004);

  /*Verify if interrupt is enabled*/
   DalInterruptController_IsInterruptEnabled(hDalIntctl,intrID, &bstate);

   /*Deregister an ISR */
   DalInterruptController_Unregister(hDalIntctl,intrID);


   if(0 == bstate){
      AsciiPrint(PRINTSIG"ISR is not registered for the interrupt %d",intrID);
      return TF_RESULT_CODE_FAILURE;
   }

    /*Verify if interrupt is enabled*/
    DalInterruptController_IsInterruptEnabled(hDalIntctl,intrID, &bstate);
    while (!gFlag && nTimeout <= 100){
      DALSYS_BusyWait(10);
      nTimeout++;
    }
   if(1 == bstate){
      AsciiPrint(PRINTSIG"ISR is not unregistered for the interrupt %d",intrID);
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}


/*========================================================================== 
                      dalIntctlTriggerInterruptTest
 ========================================================================== 

  FUNCTION dalIntctlTriggerInterruptTest

  DESCRIPTION
  This function is called to test the DalInterruptController_InterruptTrigger API.

  PARAMETERS
  *hDalIntctl      [IN]  dalintctl. device handle
  intrID          [IN]  Dal pin num
  gIsr              [IN]  ISR function pointer 

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 dalIntctlTriggerInterruptTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID,DALISR gIsr){
   
   UINT32 bstate = 0;
   UINT32 nTimeout = 0;
   gFlag = 0;

   /* Null pointer exception check on ISR function pointer */
   if(NULL == gIsr) {
      AsciiPrint(PRINTSIG"Null pointer exception on ISR function pointer");
      return TF_RESULT_CODE_FAILURE;
   }

   /*Register an ISR*/
   DalInterruptController_RegisterISR(hDalIntctl,intrID, gIsr, hDalIntctl ,0x00000004);

   /*Software triggering the interrupt*/
   DalInterruptController_InterruptTrigger(hDalIntctl,intrID);

   /*Checking for flag after waiting for safe time after triggering the interrupt*/
   while (!gFlag && nTimeout <= 10){
      DALSYS_BusyWait(10);
      nTimeout++;
   }

   if(0 == gFlag){
      AsciiPrint(PRINTSIG"ISR is not registered for the interrupt %d",intrID);
      return TF_RESULT_CODE_FAILURE;
   }
   
   /*Deregister an ISR*/
    DalInterruptController_Unregister(hDalIntctl,intrID);
	
    DalInterruptController_IsInterruptEnabled(hDalIntctl,intrID, &bstate);
    while (!gFlag && nTimeout <= 100){
      DALSYS_BusyWait(10);
      nTimeout++;
    }
   if(1 == bstate){
      AsciiPrint(PRINTSIG"ISR is not unregistered for the interrupt %d",intrID);
      return TF_RESULT_CODE_FAILURE;
   }
   
   return TF_RESULT_CODE_SUCCESS;
}


/*========================================================================== 
                      dalIntctlIsInterruptEnabledTest
  ========================================================================== 
 
  FUNCTION dalIntctlIsInterruptEnabledTest

  DESCRIPTION
  This function is called to test the DALInt_IsInterruptEnabled API.

  PARAMETERS
  *hDalIntctl      [IN]  dalintctl. device handle
  intrID          [IN]  Interrupt ID
  gIsr              [IN]  ISR function pointer 

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 dalIntctlIsInterruptEnabledTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID,DALISR gIsr){

   UINT32 bstate = 0;
   
   /* Null pointer exception check on ISR function pointer */
   if(NULL == gIsr) {
      AsciiPrint(PRINTSIG"Null pointer exception on ISR function pointer");
      return TF_RESULT_CODE_FAILURE;
   }

   /*Register an ISR for the DAL*/
   DalInterruptController_RegisterISR(hDalIntctl,intrID, gIsr, hDalIntctl,0x00000004);

   /*Verify if interrupt is enabled*/
   DalInterruptController_IsInterruptEnabled(hDalIntctl,intrID, &bstate);

   /*Deregister an ISR for the DAL*/
   DalInterruptController_Unregister(hDalIntctl,intrID);

   if(0 == bstate){
      AsciiPrint(PRINTSIG"The interrupt %d is currently not enabled",intrID);
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}

/*========================================================================== 
                      dalIntctlInterruptStatusTest
  ========================================================================== 
 
  FUNCTION dalIntctlInterruptStatusTest

  DESCRIPTION
  This function is called to test the DalInterruptController_InterruptStatus API.

  PARAMETERS
  *hDalIntctl      [IN]  dalintctl. device handle
  intrID          [IN]  Interrupt ID
  gIsr              [IN]  ISR function pointer 

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 dalIntctlInterruptStatusTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID,DALISR gIsr){

   DALResult eStatus = DAL_ERROR;
   gFlag = 1;
   
   /* Null pointer exception check on ISR function pointer */
   if(NULL == gIsr) {
      AsciiPrint(PRINTSIG"Null pointer exception on ISR function pointer");
      return TF_RESULT_CODE_FAILURE;
   }

   /*Verify if interrupt is enabled*/
   DalInterruptController_InterruptTrigger(hDalIntctl,intrID);
   
   DALSYS_BusyWait(10000);
   
   /* Verify if the interrupt is fired and the status is non-zero*/
   eStatus = DalInterruptController_InterruptStatus(hDalIntctl,intrID);   
   if(DAL_INTERRUPT_SET != eStatus){
    gFlag = 0;
    AsciiPrint(PRINTSIG"DalInterruptController_InterruptStatus API is not showing the triggered interrupt %d",intrID);
   }
   
   /*Clearing the fired interrupt*/
   DalInterruptController_InterruptClear(hDalIntctl,intrID); 
   
   /* Verify if the interrupt is still fired */
   eStatus = DalInterruptController_InterruptStatus(hDalIntctl,intrID);   
   if(DAL_INTERRUPT_SET == eStatus){
    gFlag = 0;
    AsciiPrint(PRINTSIG"DalInterruptController_InterruptStatus API is not showing the cleared interrupt %d",intrID);
   }

   if(1 != gFlag){  
      return TF_RESULT_CODE_FAILURE;   
   }
   
   return TF_RESULT_CODE_SUCCESS;
   }


/*========================================================================== 
                      dalIntctlIsInterruptPendingTest
  ========================================================================== 
 
  FUNCTION dalIntctlIsInterruptPendingTest

  DESCRIPTION
  This function is called to test the DalInterruptController_IsInterruptPending API.

  PARAMETERS
  *hDalIntctl      [IN]  dalintctl. device handle
  intrID          [IN]  Interrupt ID
  gIsr              [IN]  ISR function pointer 

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 dalIntctlIsInterruptPendingTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID,DALISR gIsr){

   /* Null pointer exception check on ISR function pointer */
   if(NULL == gIsr) {
      AsciiPrint(PRINTSIG"Null pointer exception on ISR function pointer");
      return TF_RESULT_CODE_FAILURE;
   }

   /*Register an ISR*/
   DalInterruptController_RegisterISR(hDalIntctl,intrID, gIsr, hDalIntctl ,0x00000004);

   /*Software triggering the interrupt*/
   DalInterruptController_InterruptTrigger(hDalIntctl,intrID);
   
   if(bState == 1){
      AsciiPrint(PRINTSIG"Interrupt pending was queried but deassertion did not happen successfully %d", bState);
      return TF_RESULT_CODE_FAILURE; 
    }
    else {
	AsciiPrint(PRINTSIG"Interrupt pending was queried and deassertion happened successfully!! %d", bState);
    return TF_RESULT_CODE_SUCCESS; 
	}
}
   
      
 /*========================================================================== 
                      dalIntctlInterruptClearTest
  ========================================================================== 

  FUNCTION dalIntctlInterruptClear

  DESCRIPTION
  This function is called to test the DalInterruptController_InterruptClear API.

  PARAMETERS
  *hDalIntctl      [IN]  dalintctl. device handle
  intrID          [IN]  Dal pin num
  
  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 dalIntctlInterruptClearTest(DalDeviceHandle *hDalIntctl,UINT32 DalInterruptID){

   DALResult eStatus = DAL_ERROR;  

   /* Null pointer exception check on ISR function pointer 
   if(NULL == gIsr) {
      AsciiPrint(PRINTSIG"Null pointer exception on ISR function pointer");
      return TF_RESULT_CODE_FAILURE;
   }*/
    
   /*Triggering the interrupt without registering the ISR*/
   DalInterruptController_InterruptTrigger(hDalIntctl,intrID);
   
   /*Checking the status of the interrupt */
   eStatus = DalInterruptController_InterruptStatus(hDalIntctl,intrID);
   
   /*Clearing the interrupt status*/
   DalInterruptController_InterruptClear(hDalIntctl,intrID);
   
   /* Checking if the interrupt is cleared*/
   eStatus = DalInterruptController_InterruptStatus(hDalIntctl,intrID);
	  
   if(DAL_INTERRUPT_SET == eStatus){
      AsciiPrint(PRINTSIG" DalInterruptController_InterruptClear did not clear the interrupt %d",intrID);
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}


/*==========================================================================
                               dalIntctlTestOpen
  ==========================================================================

  FUNCTION dalIntctlTestOpen

  DESCRIPTION
  This function connects to dalintctl DAL and gives the caller a handle to the
  DAL by calling the DalExpander_Attach API.

  PARAMETERS
  **hDalIntctl    [IN]  pointer to the handle of the dalintctl device 

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 dalIntctlTestOpen(DalDeviceHandle **hDalIntctl)
{
   DALResult eDalResult = DAL_SUCCESS;

   /* Null pointer exception check on hDalIntctl handle */
   if(NULL == hDalIntctl) {
      AsciiPrint(PRINTSIG"Null pointer exception on hDalIntctl handle");
      return TF_RESULT_CODE_FAILURE;
   }


   /*Attach to dal interrupt controller device and return a handle*/
   eDalResult = DAL_DeviceAttach(DALDEVICEID_INTERRUPTCONTROLLER, hDalIntctl);
   if ( DAL_SUCCESS != eDalResult){
      AsciiPrint(PRINTSIG"DAL_DeviceAttach: Failed due to Dal result %d", eDalResult);
      return TF_RESULT_CODE_FAILURE;
   }
   return TF_RESULT_CODE_SUCCESS;
}


/*==========================================================================
                             dalIntctlTestClose
  ==========================================================================

  FUNCTION dalIntctlTestClose

  DESCRIPTION
  This function detaches the handle from the dalintctl dal device by calling
  DAL_DeviceDetach API.

  PARAMETERS
  *hDalIntctl    [IN]  Dalintctl device handle

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 dalIntctlTestClose(DalDeviceHandle *hDalIntctl)
{
   DALResult eDalResult = DAL_SUCCESS;

   /* Null pointer exception check on hGpioIntctl handle */
   if(NULL == hDalIntctl) {
      AsciiPrint(PRINTSIG"Null pointer exception on hDalIntctl handle");
      return TF_RESULT_CODE_FAILURE;
   }

   /*Detach the dal interrupt controller device from the handle*/
   eDalResult = DAL_DeviceDetach(hDalIntctl);
   if(DAL_SUCCESS != eDalResult) {
      AsciiPrint(PRINTSIG"DAL_DeviceDetach:Failed due to Dal result %d", eDalResult);
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}

/*==========================================================================
                            dalIntctlTestInit
  ==========================================================================

  FUNCTION dalIntctlTestInit

  DESCRIPTION
  This function verifies the input parameters for dal Intctl. driver's tests.   

  PARAMETERS
  dwArg      [IN]  number of parameters
  pszArg     [IN]  testing parameters 
  intrID     [IN]  Interrupt ID

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_BAD_PARAM on entering bad parameters

  SIDE EFFECTS
  None

==========================================================================*/
UINT32 dalIntctlTestInit(UINT32 dwArg, CHAR8* pszArg[], UINT32 *intrID)
{

   //CHAR8 *pStrToLongEnd  = NULL;
   UINT32 nParamFlag = 0;
   UINT32 exParmCount = sizeof(dalIntctlTestParam) / sizeof(TF_ParamDescr);
   if ( dwArg != exParmCount ){
      AsciiPrint(PRINTSIG"Number of input parameters incorrect: exp %d got %d", exParmCount, dwArg);
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /* Null pointer exception check on intrID pointer */
   if(NULL == intrID) {
      AsciiPrint(PRINTSIG"Null pointer exception on intrID pointer");
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /* verify dal intctl. driver test input parameters*/
  /* nParamFlag = (UINT32)strtol(pszArg[0], &pStrToLongEnd, 10);
   if (NULL != *pStrToLongEnd){
      AsciiPrint(PRINTSIG"Bad Parameter on intrID");
      return TF_RESULT_CODE_BAD_PARAM;
   }*/
   
   nParamFlag = atoint(pszArg[0]);

   /* verify if parameters are out of permissible range*/
   /*if(intrID < nParamFlag){
      AsciiPrint(PRINTSIG"Parameter out of range on intrID");
      return TF_RESULT_CODE_BAD_PARAM;
   }*/

   *intrID = nParamFlag;

   AsciiPrint(PRINTSIG"Interrupt ID Entered: %u", *intrID);

   return TF_RESULT_CODE_SUCCESS;
}



/*----------------------------------------------------------------------------
  Global variables
----------------------------------------------------------------------------*/

CONST TF_TestFunction_Descr* DalIntctl_daltf_tests[] = {
   &sysdrvDalIntctlParamGenTest,
   &sysdrvDalIntctlAttachTest,
   &sysdrvDalIntctlRegisterISRTest,
   &sysdrvDalIntctlUnregisterTest,
   &sysdrvDalIntctlTriggerInterruptTest,
   &sysdrvDalIntctlIsInterruptEnabledTest,
   &sysdrvDalIntctlInterruptStatusTest,
   &sysdrvDalIntctlIsInterruptPendingTest,
   &sysdrvDalIntctlInterruptClearTest,
   
};

/*===========================================================================
  FUNCTION:  systemdrivers_daltf_init_dal_intctl
===========================================================================*/

DALResult systemdrivers_daltf_init_dal_intctl(VOID)
{
  DALResult dalResult = 0;

  UINT16 TF_NumberTests =
    (sizeof(DalIntctl_daltf_tests) / sizeof(DalIntctl_daltf_tests[0]));

  dalResult = tests_daltf_add_tests(DalIntctl_daltf_tests, TF_NumberTests);
  return dalResult;
}



/*===========================================================================
  FUNCTION:  systemdrivers_daltf_deinit_dal_intctl
===========================================================================*/

DALResult systemdrivers_daltf_deinit_dal_intctl(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests =
    (sizeof(DalIntctl_daltf_tests) / sizeof(DalIntctl_daltf_tests[0]));

  dalResult = tests_daltf_remove_tests(DalIntctl_daltf_tests, TF_NumberTests);
  return dalResult;
}

