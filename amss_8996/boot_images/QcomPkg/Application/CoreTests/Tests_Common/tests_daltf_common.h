#ifndef TESTS_DALTF_COMMON_H_
#define TESTS_DALTF_COMMON_H_

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ---------------------------------------------------------
03/17/15   jz      Initial Version

==========================================================================*/


/*----------------------------------------------------------------------------
  Include files
----------------------------------------------------------------------------*/

#include "DDITF.h"
#include <Uefi.h>
//#include <CoreString.h>
//#include "string.h" 
#include "DALDeviceId.h"
#include "DALReg.h"

#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/CacheMaintenanceLib.h>

#include <Protocol/EFIDALTF.h>

/*----------------------------------------------------------------------------
  Macros
----------------------------------------------------------------------------*/
#define atoint AsciiStrDecimalToUintn

#define PRINTSIG "[APPS]"

#define APT_MAX_TEST_NAME_LEN 128

#ifndef MAX_TEST_NAME_LEN
#define MAX_TEST_NAME_LEN (APT_MAX_TEST_NAME_LEN - 1)
#endif
#ifndef MAX_NUM_TESTS
#define MAX_NUM_TESTS 200
#endif

#ifndef DIAG_SUBSYS_COREBSP
#define DIAG_SUBSYS_COREBSP 1
#endif
/*=========================================================================
      Type Definitions
==========================================================================*/

extern CONST TF_ContextDescr context1;

/*===========================================================================
  FUNCTION:  tests_daltf_add_tests
===========================================================================*/

extern DALResult tests_daltf_add_tests
(
  CONST TF_TestFunction_Descr **testDescr,
  UINTN numberOfTests
);

/*===========================================================================
  FUNCTION:  tests_daltf_remove_tests
===========================================================================*/

extern DALResult tests_daltf_remove_tests
(
  CONST TF_TestFunction_Descr **testDescr,
  UINTN numberOfTests
);

#endif /* TESTS_DALTF_COMMON_H_ */
