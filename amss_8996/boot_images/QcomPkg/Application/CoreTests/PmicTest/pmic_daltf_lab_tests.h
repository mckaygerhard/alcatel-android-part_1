#ifndef _PMIC_DALTF_LAB_TEST_
#define _PMIC_DALTF_LAB_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_lab_tests.h
@brief        System Drivers Subsystem LAB driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_lab_tests.c module.


==========================================================================*/

#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmicLabLcdAmoledSel (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLabModuleRdy (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLabEnLabModule (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLabIbbRdyEn (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLabConfigPrechargeCtrl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLabConfigOutputVolt (UINT32 dwArg, CHAR8* pszArg[]);

DALResult pmic_daltf_init_lab(VOID);
#endif

