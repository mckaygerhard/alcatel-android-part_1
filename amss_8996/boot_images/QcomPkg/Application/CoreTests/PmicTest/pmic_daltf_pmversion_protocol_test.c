/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/
/*=========================================================================

@file         pmic_daltf_pmicversion_protocol_test.c
@brief        PMIC Subsystem PMICVERSION protocol level DAL-TF test

GENERAL DESCRIPTION
  This file contains the common code for testing the APIs in PMICVERSION protocol.


==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR PMICVERSION PROTOCOL TESTS

==========================================================================*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/TestInterface.h>

#include "pmic_daltf_pmversion_protocol_test.h"
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

#include <Protocol/EFIPmicVersion.h>
#include "com_dtypes.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////

STATIC UINT32 pmicVersionProtocolGetPmicInfo (UINT32 deviceIndex, EFI_QCOM_PMIC_VERSION_PROTOCOL *PmicVersionProtocol);

///////////////////////////////////////////////////////////////////////////////////////////////////////

CONST TF_ParamDescr pmicVersionProtocolTestParam[] =
{
    {TF_PARAM_UINT32, "device index [0]", "[0]"},
};

CONST TF_HelpDescr pmicVersionProtocolTestHelp =
{
   "pmicVersionProtocolTest",
    sizeof(pmicVersionProtocolTestParam) / sizeof(TF_ParamDescr),
    pmicVersionProtocolTestParam
};

CONST TF_TestFunction_Descr pmicVersionProtocol_Test =
{
   PRINTSIG"pmicVersionProtocol_Test",
   pmicVersionProtocolTest,
   &pmicVersionProtocolTestHelp,
   &context1,
	0
};


UINT32 pmicVersionProtocolTest(UINT32 dwArg, CHAR8* pszArg[])
{
   AsciiPrint(PRINTSIG">>>1<<<\n");
   AsciiPrint(PRINTSIG"PMICVERSION Protocol TEST\n");
   
   EFI_STATUS  Status = EFI_SUCCESS;
   EFI_QCOM_PMIC_VERSION_PROTOCOL           *PmicVersionProtocol = NULL;
   DALResult ret1;
   
   UINT32 deviceIndex = 0;
   
   if (dwArg < 1) {
      AsciiPrint(PRINTSIG"Invalid Params");
      return TF_RESULT_CODE_FAILURE;
   }
   //deviceIndex = atoi (pszArg[0]);
   deviceIndex = atoint (pszArg[0]); 
   Status = gBS->LocateProtocol( &gQcomPmicVersionProtocolGuid,
								 NULL,
								(VOID **)&PmicVersionProtocol
	);

   if (Status != EFI_SUCCESS){
		AsciiPrint(PRINTSIG"PMICVERSION Protocol not present\n");
		return TF_RESULT_CODE_FAILURE;
    }
	
    ret1 = pmicVersionProtocolGetPmicInfo (deviceIndex, PmicVersionProtocol);
    
	if (ret1 != TF_RESULT_CODE_SUCCESS){
		return TF_RESULT_CODE_FAILURE;
	}
	
	return TF_RESULT_CODE_SUCCESS;	
}


UINT32 pmicVersionProtocolGetPmicInfo(UINT32 deviceIndex,EFI_QCOM_PMIC_VERSION_PROTOCOL *PmicVersionProtocol)
{
	AsciiPrint(PRINTSIG"pmicVersionProtocolGetPmicInfo");
	
	EFI_PM_DEVICE_INFO_TYPE PmicDeviceInfo;
	EFI_STATUS  Status = EFI_SUCCESS;
	UINT32 errors = 0;

	Status = PmicVersionProtocol->GetPmicInfo(deviceIndex, &PmicDeviceInfo);				
	if (Status != EFI_SUCCESS){
		AsciiPrint(PRINTSIG"PmicPmicVersionProtocol->GetPmicInfo-FAIL deviceIndex:%d\n",deviceIndex);
		errors++;						
	}else{
		AsciiPrint(PRINTSIG"\npmicVersionProtocolGetPmicInfo\n");
		AsciiPrint(PRINTSIG"PmicModel:");
		switch (PmicDeviceInfo.PmicModel){
			case 0:
				AsciiPrint(PRINTSIG"EFI_PMIC_IS_UNKNOWN\n");
				break;
			case 1:
				AsciiPrint(PRINTSIG"EFI_PMIC_IS_PM8941\n");
				break;
			case 2:
				AsciiPrint(PRINTSIG"EFI_PMIC_IS_PM8841\n");
				break;
			case 3:
				AsciiPrint(PRINTSIG"EFI_PMIC_IS_PM8019\n");
				break;
			case 4:
				AsciiPrint(PRINTSIG"EFI_PMIC_IS_PM8026\n");
				break;
			case 5:
				AsciiPrint(PRINTSIG"EFI_PMIC_IS_PM8110\n");
				break;
			case 6:
				AsciiPrint(PRINTSIG"EFI_PMIC_IS_PMA8084\n");
				break;
			case 7:
				AsciiPrint(PRINTSIG"EFI_PMIC_IS_SMB8962\n");
				break;
			case 0x7FFFFFFF:
				AsciiPrint(PRINTSIG"EFI_PMIC_IS_INVALID\n");
				break;
			default:
			AsciiPrint(PRINTSIG"\nDefault:Check test and sourcecode for any new PMIC entry in EFI_PM_MODEL_TYPE\n");
		}
		
		AsciiPrint(PRINTSIG"PmicAllLayerRevision:%d\n",PmicDeviceInfo.PmicAllLayerRevision);
		AsciiPrint(PRINTSIG"PmicMetalRevision:%d\n",PmicDeviceInfo.PmicMetalRevision);
	}

	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicVersionProtocolGetPmicInfo TEST: passed\n");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicVersionProtocolGetPmicInfo TEST: failed\n");
	return TF_RESULT_CODE_FAILURE;
}


STATIC CONST TF_TestFunction_Descr* pmic_daltf_pmicversion_protocol_tests[] = {
   &pmicVersionProtocol_Test
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
DALResult pmic_daltf_init_pmicversionprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests = (sizeof(pmic_daltf_pmicversion_protocol_tests) / sizeof(pmic_daltf_pmicversion_protocol_tests[0]));
  dalResult = tests_daltf_add_tests(pmic_daltf_pmicversion_protocol_tests, TF_NumberTests);
  return dalResult;
}

/*===========================================================================
  FUNCTION:  pmic_daltf_deinit_pmicversionprotocol
===========================================================================*/

DALResult pmic_daltf_deinit_pmicversionprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests =
    (sizeof(pmic_daltf_pmicversion_protocol_tests) / sizeof(pmic_daltf_pmicversion_protocol_tests[0]));

  dalResult = tests_daltf_remove_tests(pmic_daltf_pmicversion_protocol_tests, TF_NumberTests);
  return dalResult;
}
