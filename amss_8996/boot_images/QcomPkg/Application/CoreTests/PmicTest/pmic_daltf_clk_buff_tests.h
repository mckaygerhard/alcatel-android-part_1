#ifndef _PMIC_DALTF_CLK_BUFF_TEST_
#define _PMIC_DALTF_CLK_BUFF_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_clk_buff_tests.h
@brief        System Drivers Subsystem CLK_BUFF driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_clk_buff_tests.c module.


==========================================================================*/

#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmicClkBuffSetOutputDriveStrength (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicClkBuffEnable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicClkBuffPinCtrlEnable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicClkBuffPinCtrlPolarity (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicClkBuffSetOutClkDiv (UINT32 dwArg, CHAR8* pszArg[]);

DALResult pmic_daltf_init_clk_buff(VOID);
#endif

