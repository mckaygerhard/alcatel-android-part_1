#ifndef _PMIC_DALTF_WLED_TEST_
#define _PMIC_DALTF_WLED_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_wled_tests.h
@brief        System Drivers Subsystem WLED driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_wled_tests.c module.


==========================================================================*/

/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

==========================================================================*/
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmicWledEnableModule (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledEnableModulator (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledEnableISink (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSetDelay (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSetISink (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledEnableExtPower (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSetSlopeTrim (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSetDutyCircle (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSetSlopeComp (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledEnableCabc (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledEnableSync (UINT32 dwArg, CHAR8* pszArg[]);
// UINT32 pmicWledSelPowerMode (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSetBstMaxDuty (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSetFswCtrl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledConfigSinkModulation (UINT32 dwArg, CHAR8* pszArg[]);
// UINT32 pmicWledSetModDimMode (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSelWarmupDly (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSelOvpThr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSetRampTime (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledEnableSoftStart (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSelBstIlimit (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledForceVmin (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSelSlewRate (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSelRz (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSelGm (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledEnableSleepb (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSelVrefCtrl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSelCompCap (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledSelHighPole (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicWledGetStatus (UINT32 dwArg, CHAR8* pszArg[]);
DALResult pmic_daltf_init_wled(VOID);
UINT32 pmicWledL4 (UINT32 dwArg, CHAR8* pszArg[]);
#endif // _PMIC_DALTF_WLED_TEST_

