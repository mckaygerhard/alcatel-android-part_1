#ifndef _PMIC_DALTF_GPIO_TEST_
#define _PMIC_DALTF_GPIO_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_gpio_tests.h
@brief        System Drivers Subsystem GPIO driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_gpio_tests.c module.


==========================================================================*/


/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

==========================================================================*/

#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmicGpioStatusGet(UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioConfigBiasVoltage(UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioConfigDigitalInput (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioConfigDigitalOutput (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioSetVoltageSource(UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioSetOutBufferConfig(UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioSetInversionConfig(UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioSetCurrentSourcePulls(UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioSetExtPinConfig(UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioSetOutBufferDriveStrength(UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioSetSourceConfig (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioSetOutBufferConfig (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioL4 (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicGpioConfigDigitalInOut (UINT32 dwArg, CHAR8* pszArg[]);

DALResult pmic_daltf_init_gpio(VOID);
#endif // _PMIC_DALTF_GPIO_TEST_

