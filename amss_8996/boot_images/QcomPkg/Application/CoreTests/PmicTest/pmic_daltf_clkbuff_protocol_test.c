/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/
/*=========================================================================

@file         pmic_daltf_clkbuff_protocol_test.c
@brief        PMIC Subsystem CLKBUFF protocol level DAL-TF test

GENERAL DESCRIPTION
  This file contains the common code for testing the APIs in CLKBUFF protocol.


==========================================================================*/


/*==========================================================================

                     INCLUDE FILES FOR CLKBUFF PROTOCOL TESTS

==========================================================================*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/TestInterface.h>

#include "pmic_daltf_clkbuff_protocol_test.h"
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

#include <Protocol/EFIPmicVersion.h>
#include <Protocol/EFIPmicClkBuff.h>

#include "com_dtypes.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////

STATIC UINT32 pmicClkBuffProtocolClkBuffEnable (UINT32 deviceIndex, EFI_QCOM_PMIC_CLKBUFF_PROTOCOL *PmicClkBuffProtocol);
STATIC UINT32 pmicClkBuffProtocolSetOutDrvStrength (UINT32 deviceIndex, EFI_QCOM_PMIC_CLKBUFF_PROTOCOL *PmicClkBuffProtocol);
STATIC UINT32 pmicClkBuffProtocolSetOutClkDiv (UINT32 deviceIndex, EFI_QCOM_PMIC_CLKBUFF_PROTOCOL *PmicClkBuffProtocol);
///////////////////////////////////////////////////////////////////////////////////////////////////////

CONST TF_ParamDescr pmicClkBuffProtocolTestParam[] =
{
    {TF_PARAM_UINT32, "device index [0]", "[0]"},
};

CONST TF_HelpDescr pmicClkBuffProtocolTestHelp =
{
   "pmicClkBuffProtocolTest",
    sizeof(pmicClkBuffProtocolTestParam) / sizeof(TF_ParamDescr),
    pmicClkBuffProtocolTestParam
};

CONST TF_TestFunction_Descr pmicClkBuffProtocol_Test =
{
   PRINTSIG"pmicClkBuffProtocol_Test",
   pmicClkBuffProtocolTest,
   &pmicClkBuffProtocolTestHelp,
   &context1,
	0
};


UINT32 pmicClkBuffProtocolTest(UINT32 dwArg, CHAR8* pszArg[])
{
   AsciiPrint(PRINTSIG">>>1<<<\n");
   AsciiPrint(PRINTSIG"CLKBUFF Protocol TEST\n");
   
   EFI_STATUS  Status = EFI_SUCCESS;
   EFI_QCOM_PMIC_CLKBUFF_PROTOCOL           *PmicClkBuffProtocol = NULL;
   DALResult ret1,ret2,ret3;
   
   UINT32 deviceIndex = 0;
   
   if (dwArg < 1) {
      AsciiPrint(PRINTSIG"Invalid Params");
      return TF_RESULT_CODE_FAILURE;
   }
   //deviceIndex = atoi (pszArg[0]);
     deviceIndex = atoint (pszArg[0]);  
   Status = gBS->LocateProtocol( &gQcomPmicClkBuffProtocolGuid,
								 NULL,
								(VOID **)&PmicClkBuffProtocol
	);

   if (Status != EFI_SUCCESS){
		AsciiPrint(PRINTSIG"CLKBUFF Protocol not present\n");
		return TF_RESULT_CODE_FAILURE;
    }
	
    ret1 = pmicClkBuffProtocolClkBuffEnable (deviceIndex, PmicClkBuffProtocol);
    ret2 = pmicClkBuffProtocolSetOutDrvStrength (deviceIndex, PmicClkBuffProtocol);
    ret3 = pmicClkBuffProtocolSetOutClkDiv (deviceIndex, PmicClkBuffProtocol);
    
	
	if (ret1 != TF_RESULT_CODE_SUCCESS || ret2 != TF_RESULT_CODE_SUCCESS || ret3 != TF_RESULT_CODE_SUCCESS){
		AsciiPrint(PRINTSIG"ret1:%d, ret2:%d, ret3:%d\n",ret1,ret2,ret3);
		return TF_RESULT_CODE_FAILURE;
	}
	return TF_RESULT_CODE_SUCCESS;	
}


UINT32 pmicClkBuffProtocolClkBuffEnable(UINT32 deviceIndex,EFI_QCOM_PMIC_CLKBUFF_PROTOCOL *PmicClkBuffProtocol)
{
	AsciiPrint(PRINTSIG"pmicClkBuffProtocolClkBuffEnable\n");
	
	EFI_PM_CLK_BUFF_TYPE		ClkBuffType_index;
	BOOLEAN						Enable;
		
	EFI_STATUS  Status = EFI_SUCCESS;
	UINT32 errors = 0;
	
	
	ClkBuffType_index = EFI_PM_CLK_SLEEP;
	while (ClkBuffType_index < EFI_PM_CLK_TYPE_INVALID){
		Enable = 0;
		while (Enable < 2){
			switch (ClkBuffType_index){
				case 3:
					AsciiPrint(PRINTSIG"Hitting IF\n\n");
					ClkBuffType_index++;
					break;				
				case 8:
					ClkBuffType_index=(EFI_PM_CLK_BUFF_TYPE)11;
					break;
				default:
					break;
			}
			Status = PmicClkBuffProtocol->ClkBuffEnable(deviceIndex, ClkBuffType_index, Enable);
			if (Status != EFI_SUCCESS){
				AsciiPrint(PRINTSIG"PmicClkBuffProtocol->ClkBuffEnable-FAIL deviceIndex:%d,ClkBuffType_index:%d,Enable:%d\n",deviceIndex,ClkBuffType_index, Enable);
				// AsciiPrint(PRINTSIG"CLKBUFF Protocol not present\n");
				errors++;						
			}else{
				// AsciiPrint(PRINTSIG">>>333333<<<\n");
			}
			Enable++;
		}
		ClkBuffType_index++;		
	}
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicClkBuffProtocolClkBuffEnable TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicClkBuffProtocolClkBuffEnable TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicClkBuffProtocolSetOutDrvStrength(UINT32 deviceIndex, EFI_QCOM_PMIC_CLKBUFF_PROTOCOL *PmicClkBuffProtocol)
{
	AsciiPrint(PRINTSIG"pmicClkBuffProtocolSetOutDrvStrength\n");
	
	EFI_PM_CLK_BUFF_TYPE                        ClkBuffType_index;
    EFI_PM_CLK_BUFF_OUTPUT_DRIVE_STRENGTH_TYPE  OutDrvStrength_index;
	
	EFI_STATUS  Status = EFI_SUCCESS;
	
	UINT32 errors = 0;
	
	ClkBuffType_index = EFI_PM_CLK_SLEEP;
	while (ClkBuffType_index < EFI_PM_CLK_TYPE_INVALID){
		OutDrvStrength_index = EFI_PM_CLK_BUFF_OUT_DRV__1X;
		while (OutDrvStrength_index < EFI_PM_CLK_BUFF_OUT_DRV__INVALID){
			
			switch (ClkBuffType_index){
				case 3:
					AsciiPrint(PRINTSIG"Hitting IF\n\n");
					ClkBuffType_index++;
					break;				
				case 8:
					ClkBuffType_index=(EFI_PM_CLK_BUFF_TYPE)11;
					break;
				default:
					break;
			}
			
			Status = PmicClkBuffProtocol->SetOutDrvStrength(deviceIndex, ClkBuffType_index, OutDrvStrength_index);
			if (Status != EFI_SUCCESS){
				AsciiPrint(PRINTSIG"PmicClkBuffProtocol->SetOutDrvStrength-FAIL deviceIndex:%d,ClkBuffType_index:%d,OutDrvStrength_index:%d\n",deviceIndex,ClkBuffType_index, OutDrvStrength_index);
				// AsciiPrint(PRINTSIG"CLKBUFF Protocol not present\n");
				errors++;						
			}else{
				// AsciiPrint(PRINTSIG">>>333333<<<\n");
			}
			OutDrvStrength_index++;
		}
		ClkBuffType_index++;		
	}
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicClkBuffProtocolSetOutDrvStrength TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicClkBuffProtocolSetOutDrvStrength TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicClkBuffProtocolSetOutClkDiv(UINT32 deviceIndex, EFI_QCOM_PMIC_CLKBUFF_PROTOCOL *PmicClkBuffProtocol)
{
	AsciiPrint(PRINTSIG"pmicClkBuffProtocolSetOutClkDiv\n");
	
	EFI_PM_CLK_BUFF_TYPE   ClkBuffType_index;
    UINT32                 Divider[8]={0,1,2,4,8,16,32,64};
	
	EFI_STATUS  Status =  EFI_SUCCESS;
	
	// STATIC UINT32 test_attempt=0;
	UINT32 errors = 0;
	
	ClkBuffType_index = EFI_PM_CLK_SLEEP;
	while (ClkBuffType_index < EFI_PM_CLK_TYPE_INVALID){
		UINT8 i=0;
		while (Divider[i] < 8){
			
			switch (ClkBuffType_index){
				case 3:
					AsciiPrint(PRINTSIG"Hitting IF\n\n");
					ClkBuffType_index++;
					break;				
				case 8:
					ClkBuffType_index=(EFI_PM_CLK_BUFF_TYPE)11;
					break;
				default:
					break;
			}
			
			Status = PmicClkBuffProtocol->SetOutClkDiv(deviceIndex, ClkBuffType_index, Divider[i]);
			if (Status != EFI_SUCCESS){
				AsciiPrint(PRINTSIG"PmicClkBuffProtocol->SetOutClkDiv-FAIL deviceIndex:%d,ClkBuffType_index:%d,Divider:%d\n",deviceIndex,ClkBuffType_index, Divider[i]);
				// AsciiPrint(PRINTSIG"CLKBUFF Protocol not present\n");
				errors++;						
			}else{
				// AsciiPrint(PRINTSIG">>>333333<<<\n");
			}
			i++;
		}
		ClkBuffType_index++;		
	}
				
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicClkBuffProtocolSetOutClkDiv TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicClkBuffProtocolSetOutClkDiv TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}

STATIC CONST TF_TestFunction_Descr* pmic_daltf_clkbuff_protocol_tests[] = {
   &pmicClkBuffProtocol_Test
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
DALResult pmic_daltf_init_clkbuffprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests = (sizeof(pmic_daltf_clkbuff_protocol_tests) / sizeof(pmic_daltf_clkbuff_protocol_tests[0]));
  dalResult = tests_daltf_add_tests(pmic_daltf_clkbuff_protocol_tests, TF_NumberTests);
  return dalResult;
}

/*===========================================================================
  FUNCTION:  pmic_daltf_deinit_clkbuffprotocol
===========================================================================*/

DALResult pmic_daltf_deinit_clkbuffprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests =
    (sizeof(pmic_daltf_clkbuff_protocol_tests) / sizeof(pmic_daltf_clkbuff_protocol_tests[0]));

  dalResult = tests_daltf_remove_tests(pmic_daltf_clkbuff_protocol_tests, TF_NumberTests);
  return dalResult;
}
