#ifndef _PMIC_DALTF_SMBB_TEST_
#define _PMIC_DALTF_SMBB_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_smbb_tests.h
@brief        System Drivers Subsystem SMBB driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_smbb_tests.c module.


==========================================================================*/

#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>
UINT32 pmicSmbbEnableUsbOtgAndSetMode (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbGetBattPresent (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbEnableWdog (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetTtrklAndVtrkl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetItrickle (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetVddAndVin (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetTempThresAndCurrent (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetVbatDetAndVbatWeak (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbGetBattWeakAndVbatAndCharger (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetIusbAndIdc (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetTChgrAndItermChgr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetChgrPathAndSwitchTime (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbGetBmsSignAndSetAndGetIterm (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbEnableIrAndSetAndGetIr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbGetVddSetGetDcUsbOvpCtl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetGetDcUsbOvpForce (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbEnChRejMaxLimAiclLowPowBootTimer (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetBootGetVbatBootGetBattStatus (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbGetVrefGetBatFetGetVcp (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbEnVcpBpdVbatDischg (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetBatTempCompSetGetVrefBatThmCtrl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetAdaptBoostSetGetBoostVset (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbEnBoostSetGetBoostOvpSel (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbSetBoostMaxAndEnBoostModeZero (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbEnBuckAndSetLoopGmCompRes (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbEnVcpCmptrOvrdAndSetMinPon (UINT32 dwArg, CHAR8* pszArg[]);
//UINT32 pmicSmbbBatteryGauging (UINT32 dwArg, CHAR8* pszArg[]);
//UINT32 pmicSmbbBatteryPresent (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicSmbbL4 (UINT32 dwArg, CHAR8* pszArg[]);
DALResult pmic_daltf_init_smbb(VOID);
#endif

