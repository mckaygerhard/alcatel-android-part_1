#ifndef _PMIC_DALTF_BMS_TEST_
#define _PMIC_DALTF_BMS_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/
/*=========================================================================

@file         pmic_daltf_bms_tests.h
@brief        System Drivers Subsystem BMS driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_bms_tests.c module.


==========================================================================*/

/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

==========================================================================*/
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmicBmsReadOutputData (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetOcvThr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetVseneseThr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetCcIntThr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetSwccIntThr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetCntThr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetOcvThrEn (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetStateDlyCtl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsEnOcvUpdateAndStopBmsUpdate (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsEnBmsAndCcReset (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetPonBattAndSocAndAvg (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetOverride (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetErrTol (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetAveSampleCtl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetIrq (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetRsense (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetLastSoc (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetRtcSec (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetFormatVersion (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetFabId (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsSetAndGetLastGoodOcv (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsL4 (UINT32 dwArg, CHAR8* pszArg[]);
DALResult pmic_daltf_init_bms(VOID);
#endif

