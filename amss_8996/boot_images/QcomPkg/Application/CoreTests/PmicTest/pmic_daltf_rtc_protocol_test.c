/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------  
  1/27/2015  nc      Edited for correct case on'EFIPmicRTC.h'

================================================================================*/
/*=========================================================================

@file         pmic_daltf_rtc_protocol_test.c
@brief        PMIC Subsystem RTC protocol level DAL-TF test

GENERAL DESCRIPTION
  This file contains the common code for testing the APIs in RTC protocol.


==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR RTC PROTOCOL TESTS

==========================================================================*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/TestInterface.h>

#include "pmic_daltf_rtc_protocol_test.h"
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

#include <Protocol/EFIPmicRTC.h>
#include <Include/api/systemdrivers/pmic/pm_rtc.h>


#include "com_dtypes.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////

STATIC UINT32 pmicRtcProtocolSetDisplayMode (UINT32 deviceIndex, EFI_QCOM_PMIC_RTC_PROTOCOL *PmicRtcProtocol);
// STATIC UINT32 pmicRtcProtocolSetTimeValue (UINT32 deviceIndex, EFI_QCOM_PMIC_RTC_PROTOCOL *PmicRtcProtocol);
STATIC UINT32 pmicRtcProtocolIsTimeValid (UINT32 deviceIndex, EFI_QCOM_PMIC_RTC_PROTOCOL *PmicRtcProtocol);
STATIC UINT32 pmicRtcProtocolSetTimeValueRaw (UINT32 deviceIndex, EFI_QCOM_PMIC_RTC_PROTOCOL *PmicRtcProtocol);
///////////////////////////////////////////////////////////////////////////////////////////////////////

CONST TF_ParamDescr pmicRtcProtocolTestParam[] =
{
    {TF_PARAM_UINT32, "device index [0]", "[0]"},
};

CONST TF_HelpDescr pmicRtcProtocolTestHelp =
{
   "pmicRtcProtocolTest",
    sizeof(pmicRtcProtocolTestParam) / sizeof(TF_ParamDescr),
    pmicRtcProtocolTestParam
};

CONST TF_TestFunction_Descr pmicRtcProtocol_Test =
{
   PRINTSIG"pmicRtcProtocol_Test",
   pmicRtcProtocolTest,
   &pmicRtcProtocolTestHelp,
   &context1,
	0
};


UINT32 pmicRtcProtocolTest(UINT32 dwArg, CHAR8* pszArg[])
{
   AsciiPrint(PRINTSIG">>>1<<<\n");
   AsciiPrint(PRINTSIG"RTC Protocol TEST\n");
   
   EFI_STATUS  Status = EFI_SUCCESS;
   EFI_QCOM_PMIC_RTC_PROTOCOL           *PmicRtcProtocol = NULL;
   DALResult ret1,ret3,ret4;
   
   UINT32 deviceIndex = 0;
   
   if (dwArg < 1) {
      AsciiPrint(PRINTSIG"Invalid Params");
      return TF_RESULT_CODE_FAILURE;
   }
   //deviceIndex = atoi (pszArg[0]);
   deviceIndex = atoint (pszArg[0]);
      
   Status = gBS->LocateProtocol( &gQcomPmicRtcProtocolGuid,
								 NULL,
								(VOID **)&PmicRtcProtocol
	);

   if (Status != EFI_SUCCESS){
		AsciiPrint(PRINTSIG"RTC Protocol not present\n");
		return TF_RESULT_CODE_FAILURE;
    }
	
    ret1 = pmicRtcProtocolSetDisplayMode (deviceIndex, PmicRtcProtocol);
    // ret2 = pmicRtcProtocolSetTimeValue (deviceIndex, PmicRtcProtocol);
    ret3 = pmicRtcProtocolIsTimeValid (deviceIndex, PmicRtcProtocol);
    ret4 = pmicRtcProtocolSetTimeValueRaw (deviceIndex, PmicRtcProtocol);
    
	
	if (ret1 != TF_RESULT_CODE_SUCCESS || ret3 != TF_RESULT_CODE_SUCCESS || ret3 != TF_RESULT_CODE_SUCCESS){
		AsciiPrint(PRINTSIG"ret1:%d, ret3:%d, ret4:%d\n",ret1,ret3,ret4);
		return TF_RESULT_CODE_FAILURE;
	}
	return TF_RESULT_CODE_SUCCESS;	
}


UINT32 pmicRtcProtocolSetDisplayMode(UINT32 deviceIndex,EFI_QCOM_PMIC_RTC_PROTOCOL *PmicRtcProtocol)
{
	AsciiPrint(PRINTSIG"pmicRtcProtocolSetDisplayMode");
	
	EFI_PM_RTC_DISPLAY_TYPE Mode_index,Mode_status;
	
	EFI_STATUS  Status = EFI_SUCCESS;
	UINT32 errors = 0;
	
	Mode_index = EFI_PM_RTC_12HR_MODE;
	while (Mode_index < EFI_PM_RTC_INVALID_MODE){				
		Status = PmicRtcProtocol->SetDisplayMode(deviceIndex, Mode_index);
		if (Status != EFI_SUCCESS){
			AsciiPrint(PRINTSIG"PmicRtcProtocol->SetDisplayMode-FAIL deviceIndex:%d,Mode_index:%d\n",deviceIndex, Mode_index);
			errors++;						
		}else{
			Status = PmicRtcProtocol->GetDisplayMode(deviceIndex, &Mode_status);
			if (Status != EFI_SUCCESS){
			AsciiPrint(PRINTSIG"PmicRtcProtocol->SetDisplayMode-FAIL deviceIndex:%d,Mode_index:%d\n",deviceIndex, Mode_index);
			errors++;						
			}else{
			   if (Mode_status != Mode_index) {
					AsciiPrint(PRINTSIG"PmicRtcProtocol->SetDisplayMode-FAIL;SET:%d,GET:%d",Mode_index, Mode_status);
					errors++;
				}else {
			
				}
			}
		}
		Mode_index++;		
	}
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicRtcProtocolSetDisplayMode TEST: passed\n");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicRtcProtocolSetDisplayMode TEST: FAILED\n");
	return TF_RESULT_CODE_FAILURE;
}

/*  It is failing now on 8974.This API SetTimeValue will be deprecated soon.
	API is try to start RTC
	and TZ doesn't provide access to UEFI for RTC control register
	so that failure is expected and should get rid of it on newer targets
*/
/*
UINT32 pmicRtcProtocolSetTimeValue(UINT32 deviceIndex,EFI_QCOM_PMIC_RTC_PROTOCOL *PmicRtcProtocol)
{
	AsciiPrint(PRINTSIG"pmicRtcProtocolSetTimeValue");
	
	EFI_PM_RTC_JULIAN_TYPE CurrentTimePtr, CurrentTime_status;
		
	EFI_STATUS  Status = EFI_SUCCESS;
	UINT32 errors = 0;
	
	CurrentTimePtr.year = 1980;
	while (CurrentTimePtr.year <= 2100){				
		CurrentTimePtr.month = 1;
		while (CurrentTimePtr.month <= 12){
			CurrentTimePtr.day = 1;
			while (CurrentTimePtr.day <= 31){
				CurrentTimePtr.hour = 0;
				while (CurrentTimePtr.hour <= 23){
					CurrentTimePtr.minute = 0;
					while (CurrentTimePtr.minute <= 59){
						CurrentTimePtr.second = 0;
						while (CurrentTimePtr.second <= 59){
							CurrentTimePtr.day_of_week = 0;
							while (CurrentTimePtr.day_of_week <= 6){
								Status = PmicRtcProtocol->SetTimeValue(deviceIndex, &CurrentTimePtr);
								if (Status != EFI_SUCCESS){
									// AsciiPrint(PRINTSIG"PmicRtcProtocol->SetTimeValue-FAIL deviceIndex:%d\n",deviceIndex);
									// AsciiPrint(PRINTSIG"Param set:%d,%d,%d,%d,%d,%d,%d\n",CurrentTimePtr.year,CurrentTimePtr.month,CurrentTimePtr.day,CurrentTimePtr.hour,CurrentTimePtr.minute,CurrentTimePtr.second,CurrentTimePtr.day_of_week);
									errors++;						
								}else{
									Status = PmicRtcProtocol->GetTimeValue(deviceIndex, &CurrentTime_status);
									if (Status != EFI_SUCCESS){
									// AsciiPrint(PRINTSIG"PmicRtcProtocol->GetTimeValue-FAIL deviceIndex:%d\n",deviceIndex);
									errors++;						
									}else{
									   // if (Mode_status != Mode_index) {
											// AsciiPrint(PRINTSIG"PmicRtcProtocol->SetDisplayMode-FAIL;SET:%d,GET:%d",Mode_index, Mode_status);
											// errors++;
										// }else {
										AsciiPrint(PRINTSIG"\nPmicRtcProtocol:SETGETTimeValue\n");
										AsciiPrint(PRINTSIG"Param Set:%d,%d,%d,%d,%d,%d,%d\n",CurrentTimePtr.year,CurrentTimePtr.month,CurrentTimePtr.day,CurrentTimePtr.hour,CurrentTimePtr.minute,CurrentTimePtr.second,CurrentTimePtr.day_of_week);
										AsciiPrint(PRINTSIG"Param Get:%d,%d,%d,%d,%d,%d,%d\n",CurrentTime_status.year,CurrentTime_status.month,CurrentTime_status.day,CurrentTime_status.hour,CurrentTime_status.minute,CurrentTime_status.second,CurrentTime_status.day_of_week);
										// }
									}
								}
								CurrentTimePtr.day_of_week++;		
							}
							CurrentTimePtr.second++;
						}
						CurrentTimePtr.minute++;
					}
					CurrentTimePtr.hour++;
				}
				CurrentTimePtr.day++;
			}
			CurrentTimePtr.month++;
		}
		CurrentTimePtr.year+=10;
	}
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicRtcProtocolSetTimeValue TEST: passed\n");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicRtcProtocolSetTimeValue TEST: FAILED\n");
	return TF_RESULT_CODE_FAILURE;
}
*/

UINT32 pmicRtcProtocolIsTimeValid(UINT32 deviceIndex,EFI_QCOM_PMIC_RTC_PROTOCOL *PmicRtcProtocol)
{
	AsciiPrint(PRINTSIG"pmicRtcProtocolIsTimeValid");
	
	EFI_PM_RTC_DISPLAY_TYPE RtcMode_index;
	EFI_PM_RTC_JULIAN_TYPE Time;
	BOOLEAN Valid;
	
	// EFI_STATUS  Status = EFI_SUCCESS;
	UINT32 errors = 0;
	
	RtcMode_index = EFI_PM_RTC_12HR_MODE;
	while (RtcMode_index <= EFI_PM_RTC_INVALID_MODE){
		Time.year = 1980;
		while (Time.year <= 2100){				
			Time.month = 1;
			while (Time.month <= 12){
				Time.day = 1;
				while (Time.day <= 31){
					Time.hour = 0;
					while (Time.hour <= 23){
						Time.minute = 0;
						while (Time.minute <= 59){
							Time.second = 0;
							while (Time.second <= 59){
								Time.day_of_week = 0;
								while (Time.day_of_week <= 6){
									PmicRtcProtocol->IsTimeValid(deviceIndex,RtcMode_index, &Time, &Valid);
									// if (Status != EFI_SUCCESS){
										// AsciiPrint(PRINTSIG"PmicRtcProtocol->IsTimeValid-FAIL deviceIndex:%d\n",deviceIndex);
										// AsciiPrint(PRINTSIG"Param set:%d,%d,%d,%d,%d,%d,%d\n",Time.year,Time.month,Time.day,Time.hour,Time.minute,Time.second,Time.day_of_week);
										// errors++;						
									// }else{
										if (Valid != 1){
											// AsciiPrint(PRINTSIG"\n TIME IS NOT VALID \n");
											// AsciiPrint(PRINTSIG"Param Set:%d,%d,%d,%d,%d,%d,%d\n",Time.year,Time.month,Time.day,Time.hour,Time.minute,Time.second,Time.day_of_week);
											errors++;						
										}else{
										
										}
									// }
									Time.day_of_week++;		
								}
								Time.second++;
							}
							Time.minute++;
						}
						Time.hour++;
					}
					Time.day++;
				}
				Time.month++;
			}
			Time.year+=100;
		}
		RtcMode_index++;
	}
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicRtcProtocolIsTimeValid TEST: passed\n");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicRtcProtocolIsTimeValid TEST: FAILED\n");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicRtcProtocolSetTimeValueRaw(UINT32 deviceIndex,EFI_QCOM_PMIC_RTC_PROTOCOL *PmicRtcProtocol)
{
	AsciiPrint(PRINTSIG"pmicRtcProtocolSetTimeValueRaw");
	
	UINT32 TimeValueRaw=1000;
	UINT32 TimeValueRaw_status;
	
	EFI_STATUS  Status = EFI_SUCCESS;
	UINT32 errors = 0;
	
	// Time.year = 1980;
	// while (Time.year <= 2100){				
		
		Status = PmicRtcProtocol->SetTimeValueRaw(deviceIndex, TimeValueRaw);
		if (Status != EFI_SUCCESS){
			AsciiPrint(PRINTSIG"PmicRtcProtocol->SetTimeValueRaw-FAIL deviceIndex:%d,TimeValueRaw:%d\n",deviceIndex,TimeValueRaw);
			errors++;						
		}else{
			Status = PmicRtcProtocol->GetTimeValueRaw(deviceIndex, &TimeValueRaw_status);
			if (Status != EFI_SUCCESS){
				AsciiPrint(PRINTSIG"PmicRtcProtocol->GetTimeValueRaw-FAIL deviceIndex:%d,TimeValueRaw%d\n",deviceIndex,TimeValueRaw);
				errors++;						
			}else{
				AsciiPrint(PRINTSIG"PmicRtcProtocol->SetTimeValueRaw; SET:%d,GET:%d\n",TimeValueRaw,TimeValueRaw_status);
			}
		}
	// }
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicRtcProtocolSetTimeValueRaw TEST: passed\n");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicRtcProtocolSetTimeValueRaw TEST: FAILED\n");
	return TF_RESULT_CODE_FAILURE;
}


STATIC CONST TF_TestFunction_Descr* pmic_daltf_rtc_protocol_tests[] = {
   &pmicRtcProtocol_Test
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
DALResult pmic_daltf_init_rtcprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests = (sizeof(pmic_daltf_rtc_protocol_tests) / sizeof(pmic_daltf_rtc_protocol_tests[0]));
  dalResult = tests_daltf_add_tests(pmic_daltf_rtc_protocol_tests, TF_NumberTests);
  return dalResult;
}

/*===========================================================================
  FUNCTION:  pmic_daltf_deinit_rtcprotocol
===========================================================================*/

DALResult pmic_daltf_deinit_rtcprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests =
    (sizeof(pmic_daltf_rtc_protocol_tests) / sizeof(pmic_daltf_rtc_protocol_tests[0]));

  dalResult = tests_daltf_remove_tests(pmic_daltf_rtc_protocol_tests, TF_NumberTests);
  return dalResult;
}
