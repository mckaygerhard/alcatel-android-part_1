/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/
/*=========================================================================

@file         pmic_daltf_mpp_protocol_test.c
@brief        PMIC Subsystem MPP protocol level DAL-TF test

GENERAL DESCRIPTION
  This file contains the common code for testing the APIs in MPP protocol.


==========================================================================*/


/*============================ EXPECTED FAILURES =====================================*/ 
// PmicMppProtocol->ConfigAnalogInput-FAIL deviceIndex:0,mpp_type:3,ChSelect_index:5
// PmicMppProtocol->ConfigAnalogInput-FAIL deviceIndex:0,mpp_type:3,ChSelect_index:7
// PmicMppProtocol->ConfigAnalogOutput-FAIL deviceIndex:0,mpp_type:3,Level_index:1,OnOff_index:0
// PmicMppProtocol->ConfigAnalogOutput-FAIL deviceIndex:0,mpp_type:3,Level_index:1,OnOff_index:1
// PmicMppProtocol->ConfigAnalogOutput-FAIL deviceIndex:0,mpp_type:3,Level_index:1,OnOff_index:2
// PmicMppProtocol->ConfigAnalogOutput-FAIL deviceIndex:0,mpp_type:3,Level_index:1,OnOff_index:3
// PmicMppProtocol->ConfigAnalogOutput-FAIL deviceIndex:0,mpp_type:3,Level_index:2,OnOff_index:0
// PmicMppProtocol->ConfigAnalogOutput-FAIL deviceIndex:0,mpp_type:3,Level_index:2,OnOff_index:1
// PmicMppProtocol->ConfigAnalogOutput-FAIL deviceIndex:0,mpp_type:3,Level_index:2,OnOff_index:2
// PmicMppProtocol->ConfigAnalogOutput-FAIL deviceIndex:0,mpp_type:3,Level_index:2,OnOff_index:3
/*==========================================================================*/




/*==========================================================================

                     INCLUDE FILES FOR MPP PROTOCOL TESTS

==========================================================================*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/TestInterface.h>

#include "pmic_daltf_mpp_protocol_test.h"
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

#include <Protocol/EFIPmicVersion.h>
#include <Protocol/EFIPmicMpp.h>

#include "com_dtypes.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////

STATIC UINT32 pmicMppProtocolStatusGet (UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol);
STATIC UINT32 pmicMppProtocolConfigDigitalInput (UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol);
STATIC UINT32 pmicMppProtocolConfigDigitalOutput (UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol);
STATIC UINT32 pmicMppProtocolConfigDigitalInOut (UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol);
STATIC UINT32 pmicMppProtocolConfigAnalogInput (UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol);
STATIC UINT32 pmicMppProtocolConfigISink (UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol);
STATIC UINT32 pmicMppProtocolConfigAnalogOutput (UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol);
STATIC UINT32 pmicMppProtocolConfigDetstOut (UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol);
///////////////////////////////////////////////////////////////////////////////////////////////////////

CONST TF_ParamDescr pmicMppProtocolTestParam[] =
{
    {TF_PARAM_UINT32, "device index [0]", "[0]"},
    {TF_PARAM_UINT32, "mpp type [0-15]", "[0-15]"}
};

CONST TF_HelpDescr pmicMppProtocolTestHelp =
{
   "pmicMppProtocolTest",
    sizeof(pmicMppProtocolTestParam) / sizeof(TF_ParamDescr),
    pmicMppProtocolTestParam
};

CONST TF_TestFunction_Descr pmicMppProtocol_Test =
{
   PRINTSIG"pmicMppProtocol_Test",
   pmicMppProtocolTest,
   &pmicMppProtocolTestHelp,
   &context1,
	0
};


UINT32 pmicMppProtocolTest(UINT32 dwArg, CHAR8* pszArg[])
{
   AsciiPrint(PRINTSIG">>>1<<<\n");
   AsciiPrint(PRINTSIG"MPP Protocol TEST\n");
   
   EFI_STATUS  Status = EFI_SUCCESS;
   EFI_QCOM_PMIC_MPP_PROTOCOL           *PmicMppProtocol = NULL;
   DALResult ret1,ret2,ret3,ret4,ret5,ret6,ret7,ret8;
   
   UINT32 deviceIndex = 0;
   EFI_PM_MPP_WHICH_TYPE mpp_type;
   
   if (dwArg < 2) {
      AsciiPrint(PRINTSIG"Invalid Params");
      return TF_RESULT_CODE_FAILURE;
   }
   //deviceIndex = atoi (pszArg[0]);
   deviceIndex = atoint (pszArg[0]); 
   
   //mpp_type = (EFI_PM_MPP_WHICH_TYPE) atoi (pszArg[1]);
   mpp_type = (EFI_PM_MPP_WHICH_TYPE) atoint (pszArg[1]);
   
   AsciiPrint(PRINTSIG">>>2<<<\n");
   Status = gBS->LocateProtocol( &gQcomPmicMppProtocolGuid,
								 NULL,
								(VOID **)&PmicMppProtocol
	);

   if (Status != EFI_SUCCESS){
		AsciiPrint(PRINTSIG"MPP Protocol not present\n");
		return TF_RESULT_CODE_FAILURE;
    }
	
    ret1 = pmicMppProtocolStatusGet (deviceIndex, mpp_type, PmicMppProtocol);
    ret2 = pmicMppProtocolConfigDigitalInput (deviceIndex, mpp_type, PmicMppProtocol);
    ret3 = pmicMppProtocolConfigDigitalOutput (deviceIndex, mpp_type, PmicMppProtocol);
    ret4 = pmicMppProtocolConfigDigitalInOut (deviceIndex, mpp_type, PmicMppProtocol);
    ret5 = pmicMppProtocolConfigAnalogInput (deviceIndex, mpp_type, PmicMppProtocol);
    ret6 = pmicMppProtocolConfigISink (deviceIndex, mpp_type, PmicMppProtocol);
    ret7 = pmicMppProtocolConfigAnalogOutput (deviceIndex, mpp_type, PmicMppProtocol);
    ret8 = pmicMppProtocolConfigDetstOut (deviceIndex, mpp_type, PmicMppProtocol);
	
	if (ret1 != TF_RESULT_CODE_SUCCESS || ret2 != TF_RESULT_CODE_SUCCESS || ret3 != TF_RESULT_CODE_SUCCESS || ret4 != TF_RESULT_CODE_SUCCESS ||
		ret5 != TF_RESULT_CODE_SUCCESS || ret6 != TF_RESULT_CODE_SUCCESS || ret7 != TF_RESULT_CODE_SUCCESS || ret8 != TF_RESULT_CODE_SUCCESS){
		AsciiPrint(PRINTSIG"ret1:%d, ret2:%d, ret3:%d, ret4:%d, ret5:%d, ret6:%d, ret7:%d, ret8:%d\n",ret1,ret2,ret3,ret4,ret5,ret6,ret7,ret8);
		return TF_RESULT_CODE_FAILURE;
	}
	return TF_RESULT_CODE_SUCCESS;	
}


UINT32 pmicMppProtocolStatusGet(UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol)
{
	AsciiPrint(PRINTSIG"pmicMppProtocolStatusGet\n");
	
	EFI_PM_MPP_STATUS_TYPE					   MppStatus;	
	EFI_STATUS  Status = EFI_SUCCESS;
	UINT32 errors = 0;	
	
	Status = PmicMppProtocol->StatusGet(deviceIndex, mpp_type, &MppStatus);
	if (Status != EFI_SUCCESS){
		AsciiPrint(PRINTSIG"MPP Protocol get status failure as:%d,%d\n",deviceIndex,mpp_type);
		// AsciiPrint(PRINTSIG"MPP Protocol get status failure for index and MPP as:%d,%d\n",deviceIndex,mpp_type);
		errors++;
	}else{	
		AsciiPrint(PRINTSIG">>>GET STATUS<<<\n");
		AsciiPrint(PRINTSIG"UEFI_MPP_CONFIG:%d\n",MppStatus.UEFI_MPP_CONFIG);
		AsciiPrint(PRINTSIG"MPP_DIGITAL_INPUT_STATUS:%d\n",MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_DIGITAL_INPUT_STATUS);
		AsciiPrint(PRINTSIG"MPP_DIGITAL_OUTPUT_STATUS:%d\n",MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_DIGITAL_OUTPUT_STATUS);
		AsciiPrint(PRINTSIG"MPP_BIDIRECTIONAL_STATUS:%d\n",MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_BIDIRECTIONAL_STATUS);
		AsciiPrint(PRINTSIG"MPP_ANALOG_INPUT_STATUS:%d\n",MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_ANALOG_INPUT_STATUS);
		AsciiPrint(PRINTSIG"MPP_ANALOG_OUTPUT_STATUS:%d\n",MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_ANALOG_OUTPUT_STATUS);
		AsciiPrint(PRINTSIG"MPP_CURRENT_SINK_STATUS:%d\n",MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_CURRENT_SINK_STATUS);
		AsciiPrint(PRINTSIG"MPP_DTEST_OUTPUT_STATUS:%d\n",MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_DTEST_OUTPUT_STATUS);	
		AsciiPrint(PRINTSIG">>>GET STATUS OVER<<<\n");
	}
					
	if (errors == 0) {
      AsciiPrint(PRINTSIG"PmicMppProtocolGetStatus TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"PmicMppProtocolGetStatus TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}



UINT32 pmicMppProtocolConfigDigitalInput(UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol)
// UINT32 PmicMppProtocolConfigDigitalInput(UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type)
{
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalInput\n");
	
	EFI_PM_MPP_DLOGIC_LVL_TYPE		Level_index;
	EFI_PM_MPP_DLOGIC_IN_DBUS_TYPE	DBus_index;
	
	EFI_PM_MPP_STATUS_TYPE					   MppStatus;
	// STATIC UINT32 test_attempt=0;
	
	EFI_STATUS  Status = EFI_SUCCESS;
	UINT32 errors = 0;
	
	
	Level_index = EFI_PM_MPP__DLOGIC__LVL_VIO_0;
	while (Level_index < EFI_PM_MPP__DLOGIC__LVL_INVALID){
		DBus_index = EFI_PM_MPP__DLOGIC_IN__DBUS1;
		while (DBus_index < EFI_PM_MPP__DLOGIC_IN__DBUS_INVALID){
			
			// test_attempt++;
			Status = PmicMppProtocol->ConfigDigitalInput(deviceIndex, mpp_type, Level_index, DBus_index);
			if (Status != EFI_SUCCESS){
				AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigDigitalInput-FAIL deviceIndex:%d,mpp_type:%d,Level_index:%d,DBus_index:%d\n",deviceIndex,mpp_type, Level_index, DBus_index);
				// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
				errors++;						
			}else{
				Status = PmicMppProtocol->StatusGet(deviceIndex, mpp_type, &MppStatus);
				if (Status != EFI_SUCCESS){
							AsciiPrint(PRINTSIG"MPP Protocol get status failure for index and mpp as:%d,%d\n",deviceIndex,mpp_type);
							// AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
							errors++;
				}else{ 
					if( MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_DIGITAL_INPUT_STATUS.MPP_DLOGIC_LVL != Level_index || 
						MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_DIGITAL_INPUT_STATUS.MPP_DLOGIC_IN_DBUS != DBus_index ){
						AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalInput get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
						// AsciiPrint(PRINTSIG"Getstatus: %d,%d,%d,%d,%d\n",GpioStatus.GpioModeSelect,GpioStatus.GpioCurrentSourcePulls,GpioStatus.GpioVoltage_source, 						 
						// GpioStatus.GpioOutBufferDriveStrength,GpioStatus.GpioSourceConfig); 							
						errors++;
					}else{
						// AsciiPrint(PRINTSIG">>>333333<<<\n");
						// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
					}
				}
			}
			DBus_index++;
		}
		Level_index++;		
	}
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalInput TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalInput TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicMppProtocolConfigDigitalOutput(UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol)
{
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalOutput\n");
	
	EFI_PM_MPP_DLOGIC_LVL_TYPE		Level_index;
	EFI_PM_MPP_DLOGIC_OUT_CTRL_TYPE	OutputCtrl_index;
	
	EFI_PM_MPP_STATUS_TYPE					   MppStatus;
	EFI_STATUS  Status = EFI_SUCCESS;
	
	// STATIC UINT32 test_attempt=0;
	UINT32 errors = 0;
	
	Level_index = EFI_PM_MPP__DLOGIC__LVL_VIO_0;
	while (Level_index < EFI_PM_MPP__DLOGIC__LVL_INVALID){
		OutputCtrl_index = EFI_PM_MPP__DLOGIC_OUT__CTRL_LOW;
		while (OutputCtrl_index < EFI_PM_MPP__DLOGIC_OUT__CTRL_INVALID){
			
			// test_attempt++;
			Status = PmicMppProtocol->ConfigDigitalOutput(deviceIndex, mpp_type, Level_index, OutputCtrl_index);
			if (Status != EFI_SUCCESS){
				AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigDigitalOutput-FAIL deviceIndex:%d,mpp_type:%d,Level_index:%d,OutputCtrl_index:%d\n",deviceIndex,mpp_type, Level_index, OutputCtrl_index);
				// AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigDigitalOutput -fail attempt %d\n",test_attempt );
				// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
				errors++;						
			}else{
				Status = PmicMppProtocol->StatusGet(deviceIndex, mpp_type, &MppStatus);
				if (Status != EFI_SUCCESS){
							AsciiPrint(PRINTSIG"MPP Protocol get status failure for index and mpp as:%d,%d\n",deviceIndex,mpp_type);
							// AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
							errors++;
				}else{ 
					if( MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_DIGITAL_OUTPUT_STATUS.MPP_DLOGIC_LVL != Level_index || 
						MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_DIGITAL_OUTPUT_STATUS.MPP_DLOGIC_OUT_CTRL != OutputCtrl_index ){
						AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalOutput get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
						// AsciiPrint(PRINTSIG"Getstatus: %d,%d,%d,%d,%d\n",GpioStatus.GpioModeSelect,GpioStatus.GpioCurrentSourcePulls,GpioStatus.GpioVoltage_source, 						 
						// GpioStatus.GpioOutBufferDriveStrength,GpioStatus.GpioSourceConfig); 							
						errors++;
					}else{
						// AsciiPrint(PRINTSIG">>>333333<<<\n");
						// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
					}
				}
			}
			OutputCtrl_index++;
		}
		Level_index++;		
	}
	
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalOutput TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalOutput TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicMppProtocolConfigDigitalInOut(UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol)
{
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalInOut\n");
	
	EFI_PM_MPP_DLOGIC_LVL_TYPE         Level_index;
	EFI_PM_MPP_DLOGIC_INOUT_PUP_TYPE   PullUp_index;
	
	EFI_PM_MPP_STATUS_TYPE			   MppStatus;
	EFI_STATUS  Status =  EFI_SUCCESS;
	
	// STATIC UINT32 test_attempt=0;
	UINT32 errors = 0;
	
	Level_index = EFI_PM_MPP__DLOGIC__LVL_VIO_0;
	while (Level_index < EFI_PM_MPP__DLOGIC__LVL_INVALID){
		PullUp_index = EFI_PM_MPP__DLOGIC_INOUT__PUP_1K;
		while (PullUp_index < EFI_PM_MPP__DLOGIC_INOUT__PUP_INVALID){
			// test_attempt++;
			Status = PmicMppProtocol->ConfigDigitalInOut(deviceIndex, mpp_type, Level_index, PullUp_index);
			if (Status != EFI_SUCCESS){
				AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigDigitalInOut-FAIL deviceIndex:%d,mpp_type:%d,Level_index:%d,PullUp_index:%d\n",deviceIndex,mpp_type, Level_index, PullUp_index);
				// AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigDigitalInOut -fail attempt %d\n",test_attempt );
				// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
				errors++;						
			}else{				
				Status = PmicMppProtocol->StatusGet(deviceIndex, mpp_type, &MppStatus);
				if (Status != EFI_SUCCESS){
							AsciiPrint(PRINTSIG"MPP Protocol get status failure for index and mpp as:%d,%d\n",deviceIndex,mpp_type);
							// AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
							errors++;
				}else{ 
					if( MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_BIDIRECTIONAL_STATUS.MPP_DLOGIC_LVL != Level_index || 
						MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_BIDIRECTIONAL_STATUS.MPP_DLOGIC_INOUT_PUP != PullUp_index ){
						AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalInOut get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
						// AsciiPrint(PRINTSIG"Getstatus: %d,%d,%d,%d,%d\n",GpioStatus.GpioModeSelect,GpioStatus.GpioCurrentSourcePulls,GpioStatus.GpioVoltage_source, 						 
						// GpioStatus.GpioOutBufferDriveStrength,GpioStatus.GpioSourceConfig); 							
						errors++;
					}else{
						// AsciiPrint(PRINTSIG">>>333333<<<\n");
						// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
					}
				}			
			}			
			PullUp_index++;
		}
		Level_index++;
	}
				
	if (errors == 0) {
      AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalInOut TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigDigitalInOut TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicMppProtocolConfigAnalogInput(UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol)
{
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigAnalogInput\n");
	
	EFI_PM_MPP_AIN_CH_TYPE         ChSelect_index;
	
	EFI_PM_MPP_STATUS_TYPE		   MppStatus;
	
	EFI_STATUS  Status = EFI_SUCCESS;
	// STATIC UINT32 test_attempt=0;
	UINT32 errors = 0;
	
	ChSelect_index = EFI_PM_MPP__AIN__CH_AMUX5;
	while (ChSelect_index < EFI_PM_MPP__AIN__CH_INVALID){
		
		// test_attempt++;
		Status = PmicMppProtocol->ConfigAnalogInput(deviceIndex, mpp_type, ChSelect_index);
		if (Status != EFI_SUCCESS){
			AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigAnalogInput-FAIL deviceIndex:%d,mpp_type:%d,ChSelect_index:%d\n",deviceIndex,mpp_type, ChSelect_index);
			// AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigAnalogInput -fail attempt %d\n",test_attempt );
			// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
			errors++;						
		}else{
				Status = PmicMppProtocol->StatusGet(deviceIndex, mpp_type, &MppStatus);
				if (Status != EFI_SUCCESS){
							AsciiPrint(PRINTSIG"MPP Protocol get status failure for index and mpp as:%d,%d\n",deviceIndex,mpp_type);
							// AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
							errors++;
				}else{ 
					if( MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_ANALOG_INPUT_STATUS.MPP_AIN_CH != ChSelect_index){
						AsciiPrint(PRINTSIG"PmicMppProtocolConfigAnalogInput get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
						// AsciiPrint(PRINTSIG"Getstatus: %d,%d,%d,%d,%d\n",GpioStatus.GpioModeSelect,GpioStatus.GpioCurrentSourcePulls,GpioStatus.GpioVoltage_source, 						 
						// GpioStatus.GpioOutBufferDriveStrength,GpioStatus.GpioSourceConfig); 							
						errors++;
					}else{
						// AsciiPrint(PRINTSIG">>>333333<<<\n");
						// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
					}
				}			
		}			
		ChSelect_index++;
	}
				
	if (errors == 0) {
      AsciiPrint(PRINTSIG"PmicMppProtocolConfigAnalogInput TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigAnalogInput TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicMppProtocolConfigISink(UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol)
{
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigISink\n");
	
	EFI_PM_MPP_I_SINK_LEVEL_TYPE    Level_index;
	EFI_PM_MPP_I_SINK_SWITCH_TYPE   OnOff_index;
	
	EFI_PM_MPP_STATUS_TYPE			MppStatus;
	
	EFI_STATUS  Status = EFI_SUCCESS;
	// STATIC UINT32 test_attempt=0;
	UINT32 errors = 0;
	
	Level_index = EFI_PM_MPP__I_SINK__LEVEL_5mA;
	while (Level_index < EFI_PM_MPP__I_SINK__LEVEL_INVALID){
		OnOff_index = EFI_PM_MPP__I_SINK__SWITCH_DIS;
		while (OnOff_index < EFI_PM_MPP__I_SINK__SWITCH_INVALID){
			// test_attempt++;
			Status = PmicMppProtocol->ConfigISink(deviceIndex, mpp_type, Level_index, OnOff_index);
			if (Status != EFI_SUCCESS){
				AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigISink-FAIL deviceIndex:%d,mpp_type:%d,Level_index:%d,OnOff_index:%d\n",deviceIndex,mpp_type, Level_index, OnOff_index);
				// AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigDigitalInOut -fail attempt %d\n",test_attempt );
				// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
				errors++;						
			}else{
				Status = PmicMppProtocol->StatusGet(deviceIndex, mpp_type, &MppStatus);
				if (Status != EFI_SUCCESS){
					AsciiPrint(PRINTSIG"MPP Protocol get status failure for index and mpp as:%d,%d\n",deviceIndex,mpp_type);
					// AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
					errors++;
				}else{ 
					if( MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_CURRENT_SINK_STATUS.MPP_I_SINK_LEVEL != Level_index ||
						MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_CURRENT_SINK_STATUS.MPP_I_SINK_SWITCH != OnOff_index){
						AsciiPrint(PRINTSIG"PmicMppProtocolConfigISink get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
						// AsciiPrint(PRINTSIG"Getstatus: %d,%d,%d,%d,%d\n",GpioStatus.GpioModeSelect,GpioStatus.GpioCurrentSourcePulls,GpioStatus.GpioVoltage_source, 						 
						// GpioStatus.GpioOutBufferDriveStrength,GpioStatus.GpioSourceConfig); 							
						errors++;
					}else{
						// AsciiPrint(PRINTSIG">>>333333<<<\n");
						// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
					}
				}	
			}			
			OnOff_index++;
		}
		Level_index++;
	}
				
	if (errors == 0) {
      AsciiPrint(PRINTSIG"PmicMppProtocolConfigISink TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigISink TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicMppProtocolConfigAnalogOutput(UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol)
{
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigAnalogOutput\n");
	
	EFI_PM_MPP_AOUT_LEVEL_TYPE    Level_index;
	EFI_PM_MPP_AOUT_SWITCH_TYPE   OnOff_index;
	
	EFI_PM_MPP_STATUS_TYPE		  MppStatus;
	
	EFI_STATUS  Status = EFI_SUCCESS;
	// STATIC UINT32 test_attempt=0;
	UINT32 errors = 0;
	
	Level_index = EFI_PM_MPP__AOUT__LEVEL_VREF_1p25_Volts;
	while (Level_index < EFI_PM_MPP__AOUT__LEVEL_INVALID){
		OnOff_index = EFI_PM_MPP__AOUT__SWITCH_OFF;
		while (OnOff_index < EFI_PM_MPP__AOUT__SWITCH_INVALID){
			// test_attempt++;
			Status = PmicMppProtocol->ConfigAnalogOutput(deviceIndex, mpp_type, Level_index, OnOff_index);
			if (Status != EFI_SUCCESS){
				// AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigAnalogOutput-FAIL Status:%d",Status);
				AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigAnalogOutput-FAIL deviceIndex:%d,mpp_type:%d,Level_index:%d,OnOff_index:%d\n",deviceIndex,mpp_type, Level_index, OnOff_index);
				// AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigDigitalInOut -fail attempt %d\n",test_attempt );
				// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
				errors++;						
			}else{
				Status = PmicMppProtocol->StatusGet(deviceIndex, mpp_type, &MppStatus);
				if (Status != EFI_SUCCESS){
					AsciiPrint(PRINTSIG"MPP Protocol get status failure for index and mpp as:%d,%d\n",deviceIndex,mpp_type);
					// AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
					errors++;
				}else{ 
					if( MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_ANALOG_OUTPUT_STATUS.MPP_AOUT_LEVEL != Level_index ||
						MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_ANALOG_OUTPUT_STATUS.MPP_AOUT_SWITCH != OnOff_index){
						AsciiPrint(PRINTSIG">>PmicMppProtocolConfigAnalogOutput get status FAIL<<");
						AsciiPrint(PRINTSIG"Level_index,Set:%d;Get:%d\n",Level_index,MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_ANALOG_OUTPUT_STATUS.MPP_AOUT_LEVEL);
						AsciiPrint(PRINTSIG"OnOff_index,Set:%d;Get:%d\n",OnOff_index,MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_ANALOG_OUTPUT_STATUS.MPP_AOUT_SWITCH );
						
						
						// AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
						// AsciiPrint(PRINTSIG"Getstatus: %d,%d,%d,%d,%d\n",GpioStatus.GpioModeSelect,GpioStatus.GpioCurrentSourcePulls,GpioStatus.GpioVoltage_source, 						 
						// GpioStatus.GpioOutBufferDriveStrength,GpioStatus.GpioSourceConfig); 							
						errors++;
					}else{
						// AsciiPrint(PRINTSIG">>>333333<<<\n");
						// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
					}
				}	
			}			
			OnOff_index++;
		}
		Level_index++;
	}
				
	if (errors == 0) {
      AsciiPrint(PRINTSIG"PmicMppProtocolConfigAnalogOutput TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigAnalogOutput TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicMppProtocolConfigDetstOut(UINT32 deviceIndex, EFI_PM_MPP_WHICH_TYPE mpp_type, EFI_QCOM_PMIC_MPP_PROTOCOL *PmicMppProtocol)
{
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigDetstOut\n");
	
	EFI_PM_MPP_DLOGIC_LVL_TYPE        Level_index;
	EFI_PM_MPP_DLOGIC_OUT_DBUS_TYPE   Dbus_index;
	
	EFI_PM_MPP_STATUS_TYPE		  MppStatus;
	
	EFI_STATUS  Status = EFI_SUCCESS;
	// STATIC UINT32 test_attempt=0;
	UINT32 errors = 0;
	
	Level_index = EFI_PM_MPP__DLOGIC__LVL_VIO_0;
	while (Level_index < EFI_PM_MPP__DLOGIC__LVL_INVALID){
		Dbus_index = EFI_PM_MPP__DLOGIC_OUT__DBUS1;
		while (Dbus_index < EFI_PM_MPP__DLOGIC_OUT__DBUS_INVALID){
			// test_attempt++;
			Status = PmicMppProtocol->ConfigDetstOut(deviceIndex, mpp_type, Level_index, Dbus_index);
			if (Status != EFI_SUCCESS){
				AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigDetstOut-FAIL deviceIndex:%d,mpp_type:%d,Level_index:%d,Dbus_index:%d\n",deviceIndex,mpp_type, Level_index, Dbus_index);
				// AsciiPrint(PRINTSIG"PmicMppProtocol->ConfigDetstOut -fail attempt %d\n",test_attempt );
				// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
				errors++;						
			}else{
				Status = PmicMppProtocol->StatusGet(deviceIndex, mpp_type, &MppStatus);
				if (Status != EFI_SUCCESS){
					AsciiPrint(PRINTSIG"MPP Protocol get status failure for index and mpp as:%d,%d\n",deviceIndex,mpp_type);
					// AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
					errors++;
				}else{ 
					if( MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_DTEST_OUTPUT_STATUS.MPP_DLOGIC_LVL != Level_index ||
						MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_DTEST_OUTPUT_STATUS.MPP_DLOGIC_OUT_DBUS != Dbus_index){
						AsciiPrint(PRINTSIG">>PmicMppProtocolConfigDetstOut get status FAIL<<\n"); 
						AsciiPrint(PRINTSIG"Level_index,Set:%d;Get:%d\n",Level_index,MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_DTEST_OUTPUT_STATUS.MPP_DLOGIC_LVL);
						AsciiPrint(PRINTSIG"Dbus_index,Set:%d;Get:%d\n",Dbus_index,MppStatus.MPP_UEFI_CONFIG_SETTINGS.MPP_DTEST_OUTPUT_STATUS.MPP_DLOGIC_OUT_DBUS );
						// AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput get status does not match with passed input\n"); 
						// AsciiPrint(PRINTSIG"Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
						// AsciiPrint(PRINTSIG"Getstatus: %d,%d,%d,%d,%d\n",GpioStatus.GpioModeSelect,GpioStatus.GpioCurrentSourcePulls,GpioStatus.GpioVoltage_source, 						 
						// GpioStatus.GpioOutBufferDriveStrength,GpioStatus.GpioSourceConfig); 							
						errors++;
					}else{
						// AsciiPrint(PRINTSIG">>>333333<<<\n");
						// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
					}
				}		
			}			
			Dbus_index++;
		}
		Level_index++;
	}
				
	if (errors == 0) {
      AsciiPrint(PRINTSIG"PmicMppProtocolConfigDetstOut TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"PmicMppProtocolConfigDetstOut TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}


STATIC CONST TF_TestFunction_Descr* pmic_daltf_mpp_protocol_tests[] = {
   &pmicMppProtocol_Test
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
DALResult pmic_daltf_init_mppprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests = (sizeof(pmic_daltf_mpp_protocol_tests) / sizeof(pmic_daltf_mpp_protocol_tests[0]));
  dalResult = tests_daltf_add_tests(pmic_daltf_mpp_protocol_tests, TF_NumberTests);
  return dalResult;
}

/*===========================================================================
  FUNCTION:  pmic_daltf_deinit_mppprotocol
===========================================================================*/

DALResult pmic_daltf_deinit_mppprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests =
    (sizeof(pmic_daltf_mpp_protocol_tests) / sizeof(pmic_daltf_mpp_protocol_tests[0]));

  dalResult = tests_daltf_remove_tests(pmic_daltf_mpp_protocol_tests, TF_NumberTests);
  return dalResult;
}
