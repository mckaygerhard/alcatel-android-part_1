#ifndef _PMIC_DALTF_LBC_TEST_
#define _PMIC_DALTF_LBC_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_lbc_tests.h
@brief        System Drivers Subsystem LBC driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_lbc_tests.c module.


==========================================================================*/

#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>
UINT32 pmicLbcEnableUsbOtgAndSetMode (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcGetBattPresent (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcEnableWdog (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetTtrklAndVtrkl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetItrickle (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetVddAndVin (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetTempThresAndCurrent (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetVbatDetAndVbatWeak (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcGetBattWeakAndVbatAndCharger (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetIusbAndIdc (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetTChgrAndItermChgr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetChgrPathAndSwitchTime (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcGetBmsSignAndSetAndGetIterm (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcEnableIrAndSetAndGetIr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcGetVddSetGetDcUsbOvpCtl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetGetDcUsbOvpForce (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcEnChRejMaxLimAiclLowPowBootTimer (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetBootGetVbatBootGetBattStatus (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcGetVrefGetBatFetGetVcp (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcEnVcpBpdVbatDischg (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetBatTempCompSetGetVrefBatThmCtrl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetAdaptBoostSetGetBoostVset (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcEnBoostSetGetBoostOvpSel (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcSetBoostMaxAndEnBoostModeZero (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcEnBuckAndSetLoopGmCompRes (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcEnVcpCmptrOvrdAndSetMinPon (UINT32 dwArg, CHAR8* pszArg[]);
//UINT32 pmicLbcBatteryGauging (UINT32 dwArg, CHAR8* pszArg[]);
//UINT32 pmicLbcBatteryPresent (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLbcL4 (UINT32 dwArg, CHAR8* pszArg[]);
DALResult pmic_daltf_init_lbc(VOID);
#endif

