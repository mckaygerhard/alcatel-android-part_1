#ifndef _PMIC_DALTF_LPG_TEST_
#define _PMIC_DALTF_LPG_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_lpg_tests.h
@brief        System Drivers Subsystem LPG driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_lpg_tests.c module.


==========================================================================*/

#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>
UINT32 pmicLpgPwmAndOutputEnable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLpgPwmSetValueAndPreDivide (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLpgPwmPwmClockSel (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLpgPwmSetPwmBitSize (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLpgPwmSrcSelect (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLpgConfigPwnType (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLpgPatternConfig (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLpgSetAndGetLutConfig (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLpgSetAndGetArrayLutConfig (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLpgPwmRampGenerator (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLpgPwmSetLutIndexAndSetInterval (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicLpgL4 (UINT32 dwArg, CHAR8* pszArg[]);
DALResult pmic_daltf_init_lpg(VOID);
#endif

