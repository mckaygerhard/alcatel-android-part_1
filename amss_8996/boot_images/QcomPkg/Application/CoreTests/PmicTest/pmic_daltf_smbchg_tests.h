#ifndef _PMIC_DALTF_SMBCHG_TEST_
#define _PMIC_DALTF_SMBCHG_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_smbchg_tests.h
@brief        System Drivers Subsystem SMBCHG driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_smbchg_tests.c module.


==========================================================================*/

#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

//###################### SMBCHG_BAT_IF ##########################//
UINT32 pmsmbchgbatifgetbatpresstatus (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgbatifcleardeadbattimer (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgbatifconfigchgcmd (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgbatifsetlowbattvoltthreshold (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgbatifsetbatmissingcfg (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgbatifsetminsysvolt (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgbatifsetdcininputcurrentlimit (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgbatifsetwipwrtmr (UINT32 dwArg, CHAR8* pszArg[]);

//###################### SMBCHG_CHGR ##########################//
UINT32 pmsmbchgchgrvbatsts (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrgetfloatvoltsts (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrgetchgists (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrgetchgrsts (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrsetprechgi (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrsetfastchgi (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrsetfastchgicmpn (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrsetfloatvolt (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrsetfloatvoltcmpn (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrenafvc (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrsetinhibithreshold (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrsetp2fthreshold (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrsetchargeterminationcurrent (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrconfigchgrtempcmpn (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrconfigchgr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgchgrconfigsftytimer (UINT32 dwArg, CHAR8* pszArg[]);

//###################### SMBCHG_DC_CHGPTH ##########################//
UINT32 pmsmbchgdcchgpthsetdcinadptrallowance (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgdcchgpthsetdcincurrentlimit (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgdcchgpthconfigaicl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmsmbchgdcchgpthsetaiclrestarttmr (UINT32 dwArg, CHAR8* pszArg[]);

DALResult pmic_daltf_init_smbchg(VOID);
#endif

