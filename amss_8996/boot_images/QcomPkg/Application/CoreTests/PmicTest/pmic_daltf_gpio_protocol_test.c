/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/
/*=========================================================================

@file         pmic_daltf_gpio_protocol_test.c
@brief        PMIC Subsystem GPIO protocol level DAL-TF test

GENERAL DESCRIPTION
  This file contains the common code for testing the APIs in gpio protocol.


==========================================================================*/


/*==========================================================================

                     INCLUDE FILES FOR GPIO PROTOCOL TESTS

==========================================================================*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/TestInterface.h>

#include "pmic_daltf_gpio_protocol_test.h"
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

#include <Protocol/EFIPmicVersion.h>
#include <Protocol/EFIPmicMpp.h>
#include <Protocol/EFIPmicGpio.h>

#include "com_dtypes.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////

// STATIC UINT32 pmicGpioProtocolConfigDigitalInput(UINT32 deviceIndex, EFI_PM_GPIO_WHICH_TYPE gpio_type);
STATIC UINT32 pmicGpioProtocolGetStatus(UINT32 deviceIndex, EFI_PM_GPIO_WHICH_TYPE gpio_type, EFI_QCOM_PMIC_GPIO_PROTOCOL *PmicGpioProtocol);
STATIC UINT32 pmicGpioProtocolConfigDigitalInput(UINT32 deviceIndex, EFI_PM_GPIO_WHICH_TYPE gpio_type, EFI_QCOM_PMIC_GPIO_PROTOCOL *PmicGpioProtocol);
STATIC UINT32 pmicGpioProtocolConfigDigitalOutput(UINT32 deviceIndex, EFI_PM_GPIO_WHICH_TYPE gpio_type, EFI_QCOM_PMIC_GPIO_PROTOCOL *PmicGpioProtocol);
STATIC UINT32 pmicGpioProtocolSetDigInCtl(UINT32 deviceIndex, EFI_PM_GPIO_WHICH_TYPE gpio_type, EFI_QCOM_PMIC_GPIO_PROTOCOL *PmicGpioProtocol);
///////////////////////////////////////////////////////////////////////////////////////////////////////

CONST TF_ParamDescr pmicGpioProtocolTestParam[] =
{
    {TF_PARAM_UINT32, "device index [0]", "[0]"},
    {TF_PARAM_UINT32, "gpio type [0-43]", "[0-43]"}
};

CONST TF_HelpDescr pmicGpioProtocolTestHelp =
{
   "pmicGpioProtocolTest",
    sizeof(pmicGpioProtocolTestParam) / sizeof(TF_ParamDescr),
    pmicGpioProtocolTestParam
};

CONST TF_TestFunction_Descr pmicGpioProtocol_Test =
{
   PRINTSIG"pmicGpioProtocol_Test",
   pmicGpioProtocolTest,
   &pmicGpioProtocolTestHelp,
   &context1,
	0
};


/*========================================================================== 
  DESCRIPTION
  This function is called to test API of:
  pm_err_flag_type pm_gpio_status_get(unsigned pmic_device_index, pm_gpio_perph_index gpio , pm_gpio_status_type *gpio_status);

  PARAMETERS: unsigned pmic_device_index [0], pm_gpio_perph_index [0-43]

  DEPENDENCIES: none

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None
==========================================================================*/

UINT32 pmicGpioProtocolTest(UINT32 dwArg, CHAR8* pszArg[])
{
   AsciiPrint(PRINTSIG">>>1<<<\n");
   AsciiPrint(PRINTSIG"GPIO Protocol\n");
   
   EFI_STATUS  Status = EFI_SUCCESS;
   EFI_QCOM_PMIC_GPIO_PROTOCOL           *PmicGpioProtocol = NULL;
   DALResult ret1,ret2,ret3,ret4;
   
   UINT32 deviceIndex = 0;
   EFI_PM_GPIO_WHICH_TYPE gpio_type;
   
   if (dwArg < 2) {
      AsciiPrint(PRINTSIG"Invalid Params");
      return TF_RESULT_CODE_FAILURE;
   }
   //deviceIndex = atoi (pszArg[0]);
   deviceIndex = atoint (pszArg[0]);
   
   //gpio_type = (EFI_PM_GPIO_WHICH_TYPE) atoi (pszArg[1]);
   gpio_type = (EFI_PM_GPIO_WHICH_TYPE) atoint (pszArg[1]);
   
   AsciiPrint(PRINTSIG">>>2<<<\n");
   Status = gBS->LocateProtocol( &gQcomPmicGpioProtocolGuid,
								 NULL,
								(VOID **)&PmicGpioProtocol
	);

   if (Status != EFI_SUCCESS){
		AsciiPrint(PRINTSIG"GPIO Protocol not present\n");
		return TF_RESULT_CODE_FAILURE;
    }
	AsciiPrint(PRINTSIG">>>3<<<\n");
    ret1 = pmicGpioProtocolGetStatus (deviceIndex, gpio_type, PmicGpioProtocol);
    ret2 = pmicGpioProtocolConfigDigitalInput (deviceIndex, gpio_type, PmicGpioProtocol);
    ret3 = pmicGpioProtocolConfigDigitalOutput (deviceIndex, gpio_type, PmicGpioProtocol);
    ret4 = pmicGpioProtocolSetDigInCtl (deviceIndex, gpio_type, PmicGpioProtocol);
    // ret = pmicGpioProtocolConfigDigitalInput (deviceIndex, gpio_type);
	if (ret1 != TF_RESULT_CODE_SUCCESS || ret2 != TF_RESULT_CODE_SUCCESS || ret3 != TF_RESULT_CODE_SUCCESS || ret4 != TF_RESULT_CODE_SUCCESS){
		AsciiPrint(PRINTSIG"ret1:%d, ret2:%d, ret3:%d, ret4:%d\n",ret1,ret2,ret3,ret4);
		return TF_RESULT_CODE_FAILURE;
	}
	return TF_RESULT_CODE_SUCCESS;	
}


UINT32 pmicGpioProtocolGetStatus(UINT32 deviceIndex, EFI_PM_GPIO_WHICH_TYPE gpio_type, EFI_QCOM_PMIC_GPIO_PROTOCOL *PmicGpioProtocol)
{
	AsciiPrint(PRINTSIG"pmicGpioProtocolGetStatus\n");
	
	EFI_PM_GPIO_STATUS_TYPE					   GpioStatus;	
	EFI_STATUS  Status = EFI_SUCCESS;
	UINT32 errors = 0;	
	
	Status = PmicGpioProtocol->GetStatus(deviceIndex, gpio_type, &GpioStatus);
	if (Status != EFI_SUCCESS){
		AsciiPrint(PRINTSIG"GPIO Protocol get status failure as:%d,%d\n",deviceIndex,gpio_type);
		// AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
		errors++;
	}else{	
		AsciiPrint(PRINTSIG"GpioModeSelect:%d\n",GpioStatus.GpioModeSelect);
		AsciiPrint(PRINTSIG"GpioVoltage_source:%d\n",GpioStatus.GpioVoltage_source);
		AsciiPrint(PRINTSIG"GpioModeOnOff:%d\n",GpioStatus.GpioModeOnOff);
		AsciiPrint(PRINTSIG"GpioOutBufferConfig:%d\n",GpioStatus.GpioOutBufferConfig);
		AsciiPrint(PRINTSIG"GpioInvertExtPin:%d\n",GpioStatus.GpioInvertExtPin);
		AsciiPrint(PRINTSIG"GpioOutBufferDriveStrength:%d\n",GpioStatus.GpioOutBufferDriveStrength);
		AsciiPrint(PRINTSIG"GpioCurrentSourcePulls:%d\n",GpioStatus.GpioCurrentSourcePulls);
		AsciiPrint(PRINTSIG"GpioSourceConfig:%d\n",GpioStatus.GpioSourceConfig);
		AsciiPrint(PRINTSIG"GpioDtestBufferOnOff:%d\n",GpioStatus.GpioDtestBufferOnOff);
		AsciiPrint(PRINTSIG"GpioExtPinConfig:%d\n",GpioStatus.GpioExtPinConfig);		
	}
					
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicGpioProtocolGetStatus TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicGpioProtocolGetStatus TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}



UINT32 pmicGpioProtocolConfigDigitalInput(UINT32 deviceIndex, EFI_PM_GPIO_WHICH_TYPE gpio_type, EFI_QCOM_PMIC_GPIO_PROTOCOL *PmicGpioProtocol)
// UINT32 pmicGpioProtocolConfigDigitalInput(UINT32 deviceIndex, EFI_PM_GPIO_WHICH_TYPE gpio_type)
{
	AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput\n");
	
    EFI_PM_GPIO_CURRENT_SOURCE_PULLS_TYPE      ISourcePulls_index;
    EFI_PM_GPIO_VOLTAGE_SOURCE_TYPE            VoltageSource_index;
    EFI_PM_GPIO_OUT_BUFFER_DRIVE_STRENGTH_TYPE OutBufferStrength_index;
    EFI_PM_GPIO_SOURCE_CONFIG_TYPE             Source_index;
	EFI_PM_GPIO_STATUS_TYPE					   GpioStatus;
	STATIC UINT32 test_attempt=0;
	
	EFI_STATUS  Status = EFI_SUCCESS;
	UINT32 errors = 0;
	
	// STATIC EFI_QCOM_PMIC_GPIO_PROTOCOL           *PmicGpioProtocol = NULL;
	// Status = gBS->LocateProtocol( &gQcomPmicGpioProtocolGuid,
								 // NULL,
								// (VOID **)&PmicGpioProtocol
	// );

   // if (Status != EFI_SUCCESS){
		// AsciiPrint(PRINTSIG"GPIO Protocol not present\n");
		// AsciiPrint(PRINTSIG"GPIO Protocol not present\n");
		// return TF_RESULT_CODE_FAILURE;
    // }
	
	ISourcePulls_index = EFI_PM_GPIO_I_SOURCE_PULL_UP_30uA;
	while (ISourcePulls_index < EFI_PM_GPIO_CURRENT_SOURCE_PULLS_TYPE__INVALID){
		VoltageSource_index = EFI_PM_GPIO_VIN0;
		while (VoltageSource_index < EFI_PM_GPIO_VOLTAGE_SOURCE_TYPE__INVALID){
			OutBufferStrength_index = EFI_PM_GPIO_OUT_BUFFER_OFF;
			while (OutBufferStrength_index < EFI_PM_GPIO_OUT_BUFFER_DRIVE_STRENGTH__INVALID){
				Source_index = EFI_PM_GPIO_SOURCE_GND;
				while (Source_index < EFI_PM_GPIO_SOURCE_CONFIG_TYPE__INVALID){
					if (Source_index == 1){
						Source_index = (EFI_PM_GPIO_SOURCE_CONFIG_TYPE) 4; // source config type 1,2 and3 are not allowed for this particular API
					}
					// AsciiPrint(PRINTSIG">>>4<<<\n");
					test_attempt++;
					Status = PmicGpioProtocol->ConfigDigitalInput(deviceIndex, gpio_type, ISourcePulls_index, VoltageSource_index, OutBufferStrength_index, Source_index);
					if (Status != EFI_SUCCESS){
						AsciiPrint(PRINTSIG"PmicGpioProtocol->ConfigDigitalInput -fail attempt %d\n",test_attempt );
						// AsciiPrint(PRINTSIG"GPIO Protocol not present\n");
						errors++;						
					}else{
						Status = PmicGpioProtocol->GetStatus(deviceIndex, gpio_type, &GpioStatus);
						if (Status != EFI_SUCCESS){
							AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
							// AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
							errors++;
						}else{
							if ( GpioStatus.GpioModeSelect != EFI_PM_GPIO_INPUT_ON || GpioStatus.GpioCurrentSourcePulls != ISourcePulls_index || GpioStatus.GpioVoltage_source != VoltageSource_index ||
								 GpioStatus.GpioOutBufferDriveStrength != OutBufferStrength_index || GpioStatus.GpioSourceConfig != Source_index ){
								AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput get status does not match with passed input\n"); 
								// AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput get status does not match with passed input\n"); 
								// AsciiPrint(PRINTSIG"Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
								// AsciiPrint(PRINTSIG"Getstatus: %d,%d,%d,%d,%d\n",GpioStatus.GpioModeSelect,GpioStatus.GpioCurrentSourcePulls,GpioStatus.GpioVoltage_source, 						 
								// GpioStatus.GpioOutBufferDriveStrength,GpioStatus.GpioSourceConfig); 							
								errors++;
							}else{
								// AsciiPrint(PRINTSIG">>>333333<<<\n");
								// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
							}
						}
					}
					Source_index++;
				}
				OutBufferStrength_index++;
			}
			VoltageSource_index++;
		}
		ISourcePulls_index++;
	}
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicGpioProtocolConfigDigitalOutput(UINT32 deviceIndex, EFI_PM_GPIO_WHICH_TYPE gpio_type, EFI_QCOM_PMIC_GPIO_PROTOCOL *PmicGpioProtocol)
{
	AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalOutput\n");
	
	EFI_PM_GPIO_OUT_BUFFER_CONFIG_TYPE         OutBufferConfig_index;
    EFI_PM_GPIO_VOLTAGE_SOURCE_TYPE            VoltageSource_index;
    EFI_PM_GPIO_SOURCE_CONFIG_TYPE             Source_index;
	EFI_PM_GPIO_OUT_BUFFER_DRIVE_STRENGTH_TYPE OutBufferStrength_index;
	BOOLEAN                                    OutInversion_index;
	
	EFI_PM_GPIO_STATUS_TYPE					   GpioStatus;
	EFI_STATUS  Status = EFI_SUCCESS;
	
	STATIC UINT32 test_attempt=0;
	UINT32 errors = 0;
	
	OutBufferConfig_index = EFI_PM_GPIO_OUT_BUFFER_CONFIG_CMOS;
	while (OutBufferConfig_index < EFI_PM_GPIO_OUT_BUFFER_CONFIG_INVALID){
		VoltageSource_index = EFI_PM_GPIO_VIN0;
		while (VoltageSource_index < EFI_PM_GPIO_VOLTAGE_SOURCE_TYPE__INVALID){
			OutBufferStrength_index = EFI_PM_GPIO_OUT_BUFFER_OFF;
			while (OutBufferStrength_index < EFI_PM_GPIO_OUT_BUFFER_DRIVE_STRENGTH__INVALID){
				Source_index = EFI_PM_GPIO_SOURCE_GND;
				while (Source_index < EFI_PM_GPIO_SOURCE_CONFIG_TYPE__INVALID){
					// if (Source_index == 1){
						// Source_index = (EFI_PM_GPIO_SOURCE_CONFIG_TYPE) 4;
					// }
					OutInversion_index = 0;
					while (OutInversion_index < 2){
						test_attempt++;
						Status = PmicGpioProtocol->ConfigDigitalOutput(deviceIndex, gpio_type, OutBufferConfig_index, VoltageSource_index, Source_index, OutBufferStrength_index, OutInversion_index);
						if (Status != EFI_SUCCESS){
							AsciiPrint(PRINTSIG"PmicGpioProtocol->ConfigDigitalOutput -fail attempt %d\n",test_attempt );
							// AsciiPrint(PRINTSIG"GPIO Protocol not present\n");
							errors++;						
						}else{
							Status = PmicGpioProtocol->GetStatus(deviceIndex, gpio_type, &GpioStatus);
							if (Status != EFI_SUCCESS){
								AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
								// AsciiPrint(PRINTSIG"GPIO Protocol get status failure for index and gpio as:%d,%d\n",deviceIndex,gpio_type);
								errors++;
							}else{
								if ( GpioStatus.GpioModeSelect != EFI_PM_GPIO_OUTPUT_ON || GpioStatus.GpioOutBufferConfig != OutBufferConfig_index || GpioStatus.GpioVoltage_source != VoltageSource_index ||
									 GpioStatus.GpioOutBufferDriveStrength != OutBufferStrength_index || GpioStatus.GpioSourceConfig != Source_index ){
									AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput get status does not match with passed input\n"); 
									// AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalInput get status does not match with passed input\n"); 
									// AsciiPrint(PRINTSIG"Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
									// AsciiPrint(PRINTSIG"Getstatus: %d,%d,%d,%d,%d\n",GpioStatus.GpioModeSelect,GpioStatus.GpioCurrentSourcePulls,GpioStatus.GpioVoltage_source, 						 
									// GpioStatus.GpioOutBufferDriveStrength,GpioStatus.GpioSourceConfig); 							
									errors++;
								}else{
									// AsciiPrint(PRINTSIG">>>333333<<<\n");
									// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
								}
							}
						}
						OutInversion_index++;
					}
					Source_index++;
				}
				OutBufferStrength_index++;
			}
			VoltageSource_index++;
		}
		OutBufferConfig_index++;
	}
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalOutput TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicGpioProtocolConfigDigitalOutput TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicGpioProtocolSetDigInCtl(UINT32 deviceIndex, EFI_PM_GPIO_WHICH_TYPE gpio_type, EFI_QCOM_PMIC_GPIO_PROTOCOL *PmicGpioProtocol)
{
	AsciiPrint(PRINTSIG"pmicGpioProtocolSetDigInCtl\n");
	
	EFI_PM_GPIO_DIG_IN_TYPE         DigIn_index;
	BOOLEAN                         Enable_index;
	EFI_STATUS  Status = EFI_SUCCESS;
	
	STATIC UINT32 test_attempt=0;
	UINT32 errors = 0;
	
	DigIn_index = EFI_PM_GPIO_DIG_IN_DTEST1;
	while (DigIn_index < EFI_PM_GPIO_DIG_IN_DTEST_TYPE__INVALID){
		Enable_index = 0;
		while (Enable_index < 2){
			test_attempt++;
			Status = PmicGpioProtocol->SetDigInCtl(deviceIndex, gpio_type, DigIn_index, Enable_index);
			if (Status != EFI_SUCCESS){
				AsciiPrint(PRINTSIG"PmicGpioProtocol->SetDigInCtl -fail attempt %d\n",test_attempt );
				// AsciiPrint(PRINTSIG"GPIO Protocol not present\n");
				errors++;						
			}else{
					// AsciiPrint(PRINTSIG">>>333333<<<\n");
					// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 
			}			
			Enable_index++;
		}
		DigIn_index++;
	}
				
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicGpioProtocolSetDigInCtl TEST: passed");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicGpioProtocolSetDigInCtl TEST: failed");
	return TF_RESULT_CODE_FAILURE;
}


STATIC CONST TF_TestFunction_Descr* pmic_daltf_gpio_protocol_tests[] = {
   &pmicGpioProtocol_Test
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
DALResult pmic_daltf_init_gpioprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests = (sizeof(pmic_daltf_gpio_protocol_tests) / sizeof(pmic_daltf_gpio_protocol_tests[0]));
  dalResult = tests_daltf_add_tests(pmic_daltf_gpio_protocol_tests, TF_NumberTests);
  return dalResult;
}

/*===========================================================================
  FUNCTION:  pmic_daltf_deinit_gpioprotocol
===========================================================================*/

DALResult pmic_daltf_deinit_gpioprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests =
    (sizeof(pmic_daltf_gpio_protocol_tests) / sizeof(pmic_daltf_gpio_protocol_tests[0]));

  dalResult = tests_daltf_remove_tests(pmic_daltf_gpio_protocol_tests, TF_NumberTests);
  return dalResult;
}
