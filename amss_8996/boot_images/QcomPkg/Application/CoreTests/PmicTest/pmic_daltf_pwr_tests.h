#ifndef _PMIC_DALTF_PWR_TEST_
#define _PMIC_DALTF_PWR_TEST_

/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/
/*=========================================================================

@file         pmic_daltf_pwr_tests.h
@brief        System Drivers Subsystem PWR driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_pwr_tests.c module.


==========================================================================*/

/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

==========================================================================*/
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmicPwrLdo (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicPwrVs (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicPwrSmps (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicPwm (UINT32 dwArg, CHAR8* pszArg[]);
DALResult pmic_daltf_init_pwr(VOID);

#endif // _PMIC_DALTF_PWR_TEST_

