/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		KernelDalsysTest.h
  DESCRIPTION:	Header File for all UEFI APT TLMM Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#ifndef _KERNEL_DALTF_Dalsys_TESTS_
#define _KERNEL_DALTF_Dalsys_TESTS_

#include "KernelDalsysTest.h"
#include <DALSys.h>

/*====================================================
** STRUCTURES **
====================================================*/

UINT32 TestDalsysSync(UINT32 dwArg, CHAR8 *pszArg[]); 
UINT32 TestDalsysMemObjs1(UINT32 dwArg, CHAR8 *pszArg[]);
UINT32 TestDalsysMemObjs2(UINT32 dwArg, CHAR8 *pszArg[]); 
UINT32 TestDalsysEvents1(UINT32 dwArg, CHAR8 *pszArg[]);  
UINT32 TestDalsysEvents2(UINT32 dwArg, CHAR8 *pszArg[]);  
UINT32 TestDalsysEvents3(UINT32 dwArg, CHAR8 *pszArg[]);  
UINT32 TestDalsysEvents4(UINT32 dwArg, CHAR8 *pszArg[]);  
UINT32 TestDalsysEvents5(UINT32 dwArg, CHAR8 *pszArg[]);  
UINT32 TestDalsysEvents4_Riva(UINT32 dwArg, CHAR8 *pszArg[]);  
UINT32 TestDalsysCallbackEvents(UINT32 dwArg, CHAR8 *pszArg[]); 
UINT32 TestDalsysTimerEvents(UINT32 dwArg, CHAR8 *pszArg[]); 
UINT32 TestDalsysWorkloop(UINT32 dwArg, CHAR8 *pszArg[]);
 
//Initialize adc daltf tests
DALResult kernel_dal_daltf_dalsys_init_all(VOID);

//De-Initialize adc daltf tests
DALResult kernel_dal_daltf_dalsys_deinit_all(VOID);

#endif