/*============================================================================
@file         kernel_dal_daltf_dalsys_mem_tests.c

@brief        DALTF based Dalsys memory API tests

GENERAL DESCRIPTION
  This file contains L1,L2 and L3 tests for DALSys Memory API

Copyright (c) 2012 Qualcomm Technologies Inc. All Rights Reserved. Qualcomm Confidential and Proprietary
============================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/KernelTest/kernel_dal_daltf_dalsys_mem_tests.c#1 $
  $DateTime: 2015/06/01 21:35:43 $
  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------
03/22/2012 vn     Intial Version
03/30/2012 vn	  Fix memory leaks and DALSYS_free calls
05/09/2012 vn	  Add test for DALSYS_CacheCommand API
06/27/2012 vn	  Add MallocZero test
==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR CHIPINFO DRIVER TESTS

==========================================================================*/
#include "kernel_dal_daltf_dalsys_mem_tests.h"
#include "DALSys.h"
#include "DDITimer.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*==========================================================================

                  DEFINITIONS AND DECLARATIONS FOR MODULE

==========================================================================*/

/*=========================================================================
      CONSTants and Macros
==========================================================================*/


CONST TF_ParamDescr DalsysMallocTestParam[] =
{
    {TF_PARAM_UINT32, "Iterations",					"Iterations for the test(Optional)\n"},
	{TF_PARAM_UINT32, "Size of memory in bytes",	"Bytes of data Ex: 64,100,200\n"},
};

CONST TF_ParamDescr DalsysMallocZeroParam[] =
{
    {TF_PARAM_UINT32, "Override",					"1 - free memory\n"},
};

CONST TF_ParamDescr DalsysMemRegTestParam[] =
{
	{TF_PARAM_UINT32, "Iterations",					"Iterations for the test(Optional)\n"},
    {TF_PARAM_UINT32, "Size of memory in bytes",	"Size of memory region requested. Ex: 64,100,200\n"},
	{TF_PARAM_UINT32, "Memory Type",				"Type Attribute\n"},
	{TF_PARAM_UINT32, "Physical Addr",				"DALSYS_MEM_ADDR_NOT_SPECIFIED by default\n"},
	{TF_PARAM_UINT32, "Virtual Addr",				"DALSYS_MEM_ADDR_NOT_SPECIFIED by default\n"},
	{TF_PARAM_UINT32, "Debug",						"1 - Debug ON \n"},
};

CONST TF_ParamDescr DalsysCacheTestParam[] =
{
	{TF_PARAM_UINT32, "Cache command",			"1 - Invalidate \n"
								                "2 - Flush \n"
								                "3 - Clean \n"},
	{TF_PARAM_UINT32, "Virtual Address",			"Virtuall address \n"},
	{TF_PARAM_UINT32, "dwLen",				"Length of the memory region \n"},
};

/*
===============================================================================
**  Function : TestDalsysMallocZero
** ============================================================================
*/
/*!
    @brief
    DALSYS_Malloc for zero bytes of memory

    @details
	Allocate zero bytes of memory
	
    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

	@par API Tested 
	DALSYS_Malloc
	DALSYS_Free

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/
UINT32 TestDalsysMallocZero(UINT32 dwArg, CHAR8* pszArg[])
{	

	UINT32 override=99;
	CHAR8 * endptr;
	UINT32 iret;
	CHAR8 *pMem=NULL;
	
	AsciiPrint(PRINTSIG"TestDalsysMallocZeroTest start\n");
	if ( dwArg != (sizeof(DalsysMallocZeroParam) / sizeof(TF_ParamDescr)))
	{
		AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysMallocZeroParam) / sizeof(TF_ParamDescr), dwArg);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	
	override = strtol(pszArg[0], &endptr, 10);
	if(*endptr != '\0' && override==0) {
		AsciiPrint(PRINTSIG"Param Conversion not successful %c\n", *endptr);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	
	/* Allocate zero bytes of memory*/
	AsciiPrint(PRINTSIG"Allocating zero bytes of memory\n");
	iret = DALSYS_Malloc (0, (VOID **)&pMem);
	if (DAL_SUCCESS != iret) {
		AsciiPrint(PRINTSIG"DALSYS_Malloc failed: iret %d\n", iret);
		return TF_RESULT_CODE_FAILURE;
	}
	
	if (override == 1) {
		iret = DALSYS_Free(pMem);
		if (DAL_SUCCESS != iret) {
			AsciiPrint(PRINTSIG"DALSYS_Free failed: iret %d\n", iret);
			return TF_RESULT_CODE_FAILURE;
		}
	}
	return TF_RESULT_CODE_SUCCESS;
}

// DalsysMallocZeroTest's Help Data
CONST TF_HelpDescr TestDalsysMallocZeroTestHelp =
{
    "TestDalsysMallocZero",
    sizeof(DalsysMallocZeroParam) / sizeof(TF_ParamDescr),
    DalsysMallocZeroParam
};

CONST TF_TestFunction_Descr TestDalsysMallocZeroTest =
{
   PRINTSIG"TestDalsysMallocZero",
   TestDalsysMallocZero,
   &TestDalsysMallocZeroTestHelp,
   &context1
};

/*
===============================================================================
**  Function : TestDalsysMalloc
** ============================================================================
*/
/*!
    @brief
    DALSYS_Malloc and DALSYS_Free API test.

    @details
	Allocate memory using DALSYS_Malloc
	Write data to memory
	Read data back from memory
	De-allocate memory using DALSYS_Free
	Stress operation using iteration
	
    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

	@par API Tested 
	DALSYS_Malloc
	DALSYS_Free

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/
UINT32 TestDalsysMalloc(UINT32 dwArg, CHAR8* pszArg[])
{
	UINT32 iter=0;
	UINT32 count=0;
	UINT32 mSize;
	UINT32 i;
	UINT32 iret;
	CHAR8 *pMem=NULL;
	CHAR8 *pLoc=NULL;
	CHAR8 *endptr;
	
	AsciiPrint(PRINTSIG"TestDalsysMallocTest start\n");
	if ( dwArg != (sizeof(DalsysMallocTestParam) / sizeof(TF_ParamDescr)))
	{
		AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysMallocTestParam) / sizeof(TF_ParamDescr), dwArg);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	
	iter = strtol(pszArg[0], &endptr, 10);
	if(*endptr != '\0' && iter==0) {
		AsciiPrint(PRINTSIG"iter Conversion not successful %c\n", *endptr);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	mSize = strtol(pszArg[1], &endptr, 10);
	if(*endptr != '\0' && mSize==0) {
		AsciiPrint(PRINTSIG"mSize Conversion not successful %c\n", *endptr);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	AsciiPrint(PRINTSIG"Parameters after conversion iter=%d, msize=%d\n", iter,mSize);
	if (mSize > 4*1024) {
		AsciiPrint(PRINTSIG"assume size of memory to alloc should be less than 4k\n");
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	
	if(iter == 0)
		iter++;	// Run the test atleast once
	
	AsciiPrint(PRINTSIG"Allocating %d bytes of memory %d time(s)\n", mSize, iter);
	while (count < iter) {
		count++;
		/* Allocate mSize bytes of data */	
		iret = DALSYS_Malloc (mSize, (VOID **)&pMem);  
		if (iret != DAL_SUCCESS) {
			AsciiPrint(PRINTSIG"DALSYS_Malloc failed: iret %d, iter %d\n", iret, iter);
			return TF_RESULT_CODE_FAILURE;
		}
		
		if(pMem == NULL) {
			AsciiPrint(PRINTSIG"DALSYS_Malloc failed: iret %d, iter %d\n", iret, iter);
			return TF_RESULT_CODE_FAILURE;
		}		
	
		pLoc = (CHAR8 *)pMem;
			
		/* Write data into the allocated memory */
		for (i=0; i < mSize; i++) {
			*pLoc='A';  
			pLoc++;
		}
	
		pLoc = (CHAR8 *)pMem;
	
		/* Check if the data written in the memory exists */
		for (i=0; i < mSize; i++) {
			if(*pLoc != 'A') {
				AsciiPrint(PRINTSIG"Data written at address 0x%X=%c is corrupt\n",pLoc,*pLoc);
				DALSYS_Free(pMem);
				return TF_RESULT_CODE_FAILURE;
			}
			pLoc++;
		}
		iret = DALSYS_Free(pMem);
		if (iret != DAL_SUCCESS) {
			AsciiPrint(PRINTSIG"DALSYS_Free failed: iret %d\n", iret);
			return TF_RESULT_CODE_FAILURE;
		}
	}

	return TF_RESULT_CODE_SUCCESS;
}

// DalsysMallocTest's Help Data
CONST TF_HelpDescr TestDalsysMallocHelp =
{
    "TestDalsysMalloc",
    sizeof(DalsysMallocTestParam) / sizeof(TF_ParamDescr),
    DalsysMallocTestParam
};

CONST TF_TestFunction_Descr TestDalsysMallocTest =
{
   PRINTSIG"TestDalsysMalloc",
   TestDalsysMalloc,
   &TestDalsysMallocHelp,
   &context1
};


/*
===============================================================================
**  Function : TestDalsysMemRegTest
** ============================================================================
*/
/*!
    @brief
    Create different Memory Object with various attributes, various sizes
	
    @details
	Create multiple memory objects of different attributes
	Write data to the allocated memory
	
	Use different attributes to create Mem Object
	
    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

	@par API Tested 
	DALSYS_MemRegionAlloc
	DALSYS_MemInfo
	DALSYS_Free

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/

UINT32 TestDalsysMemReg(UINT32 dwArg, CHAR8* pszArg[])
{
	UINT32 mSize, iter=0;
	UINT32 iType=0;
	UINT32 iRet=0,j,count=0;
	unsigned long long PhyAddr,VirAddr;
	CHAR8 *pMem,*endptr,*pLoc;
	DALSYSMemInfo MemInfo;
	UINT32 iMemInfo=0;
	//UINT32 attributes[] = {	DALSYS_MEM_PROPS_NORMAL,
	//						DALSYS_MEM_PROPS_UNCACHED | DALSYS_MEM_PROPS_PHYS_CONT,
	//						DALSYS_MEM_PROPS_EXT_STRONGLY_ORDERED | DALSYS_MEM_PROPS_EXT_PERM_FULL | DALSYS_MEM_PROPS_EXT,
	//						DALSYS_MEM_PROPS_CACHED_WB | DALSYS_MEM_PROPS_PHYS_CONT	};
	DALSYSMemHandle hMem;
	
	AsciiPrint(PRINTSIG"TestDalsysMemRegTest start\n");
	if ( dwArg != (sizeof(DalsysMemRegTestParam) / sizeof(TF_ParamDescr)))
	{
		AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysMemRegTestParam) / sizeof(TF_ParamDescr), dwArg);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	
	iter = strtol(pszArg[0], &endptr, 10);
	if(*endptr != '\0' && iter==0) {
		AsciiPrint(PRINTSIG"iter Conversion not successful %c\n", *endptr);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	
	if(iter == 0)
		iter++; // Minimum run count 1

	mSize = strtol(pszArg[1], &endptr, 10);
	if(*endptr != '\0' && mSize==0) {
		AsciiPrint(PRINTSIG"mSize Conversion not successful %c\n", *endptr);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	
	iType = strtol(pszArg[2], &endptr, 16);
	if(*endptr != '\0' && iType==0) {
		AsciiPrint(PRINTSIG"iType Conversion not successful %c\n", *endptr);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	
	PhyAddr = strtol(pszArg[3], &endptr, 16);
	if(*endptr != '\0' && iType==0) {
		AsciiPrint(PRINTSIG"iType Conversion not successful %c\n", *endptr);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	
	VirAddr = strtol(pszArg[4], &endptr, 16);
	if(*endptr != '\0' && iType==0) {
		AsciiPrint(PRINTSIG"iType Conversion not successful %c\n", *endptr);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	
	iMemInfo = strtol(pszArg[5], &endptr, 10);
	if(*endptr != '\0' && iMemInfo==0) {
		AsciiPrint(PRINTSIG"iMemInfo Conversion not successful %c\n", *endptr);
		return(TF_RESULT_CODE_BAD_PARAM);
	}	
	
	if (!VirAddr)
		VirAddr = DALSYS_MEM_ADDR_NOT_SPECIFIED;
	if (!PhyAddr)
		PhyAddr = DALSYS_MEM_ADDR_NOT_SPECIFIED;
	
	//n=sizeof(attributes)/sizeof(UINT32);
	
	while(count < iter) {
		count++;
		iRet = DALSYS_MemRegionAlloc(iType,		// Memory Type
								   	VirAddr, 	// Virtual Addr
									PhyAddr,	// Physical Addr
									mSize,	    // Memory Size
									&hMem,		// Memory Handle
									NULL);		// Mem Object
		if(iRet != DAL_SUCCESS) {
			AsciiPrint(PRINTSIG"Failed for Attribute %X with error %d\n", iType,iRet);
			return TF_RESULT_CODE_FAILURE;
		}
		
		if (iMemInfo) {
			iRet = DALSYS_MemInfo(hMem, &MemInfo);
			if(iRet != DAL_SUCCESS) {
				AsciiPrint(PRINTSIG"MemInfo failed with %d\n", iRet);
				DALSYS_DestroyObject(hMem);
				return TF_RESULT_CODE_FAILURE;
			}
		
		if (((count % 50)==0) || (count==1))
			AsciiPrint(PRINTSIG"MemType 0x%X; VirAddr=0x%X; PhyAddr=0x%X; Size=%d; Iter=%d\n",
								iType,MemInfo.VirtualAddr,MemInfo.PhysicalAddr,MemInfo.dwLen,count);
					  
			pLoc = pMem = (CHAR8 *)MemInfo.VirtualAddr;
			
			if(pLoc == NULL) {
				AsciiPrint(PRINTSIG"Virtual address is NULL\n",NULL);
				DALSYS_DestroyObject(hMem);
				return TF_RESULT_CODE_FAILURE;
			}
			for (j=0; j < MemInfo.dwLen; j++) 
				*pLoc++ = 'A';
		
			pLoc = pMem;
			for (j=0; j < MemInfo.dwLen; j++) {
				if (*pLoc != 'A') {
					AsciiPrint(PRINTSIG"Data written at address 0x%X=%c is corrupt\n",pLoc,*pLoc);
					DALSYS_DestroyObject(hMem);
					return TF_RESULT_CODE_FAILURE;
				}
			}		
		}
		/* Clean up all the objects */
		iRet = DALSYS_DestroyObject(hMem);
		if(iRet != DAL_SUCCESS) {
			AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed %d\n",iRet);
			return TF_RESULT_CODE_FAILURE;
		}
	}
	return TF_RESULT_CODE_SUCCESS;
}

// DalsysMallocTest's Help Data
CONST TF_HelpDescr TestDalsysMemRegTestHelp =
{
    "TestDalsysMalloc",
    sizeof(DalsysMemRegTestParam) / sizeof(TF_ParamDescr),
    DalsysMemRegTestParam
};

CONST TF_TestFunction_Descr TestDalsysMemRegTest =
{
   PRINTSIG"TestDalsysMemReg",
   TestDalsysMemReg,
   &TestDalsysMemRegTestHelp,
   &context1
};

/*
===============================================================================
**  Function : TestDalsysCacheTest
** ============================================================================
*/
/*!
    @brief
    L1 test coverage for testing DALSYS_CacheCommand API
	
    @details
	Tests DALSYS_CacheCommand API for various attributes
		
    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

	@par API Tested 
	DALSYS_CacheCommand

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/

UINT32 TestDalsysCache(UINT32 dwArg, CHAR8* pszArg[])
{
	UINT32 virAddr, Len, cmd, iRet;
    CHAR8 *endptr;

	AsciiPrint(PRINTSIG"TestDalsysCacheTest start\n");
	if ( dwArg != (sizeof(DalsysCacheTestParam) / sizeof(TF_ParamDescr)))
	{
		AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
                sizeof(DalsysCacheTestParam) / sizeof(TF_ParamDescr), dwArg);
		return(TF_RESULT_CODE_BAD_PARAM);
	}

	cmd = strtol(pszArg[0], &endptr, 10);	
    if ('\0' == *endptr) {
            AsciiPrint(PRINTSIG"Invalid parameter cmd\n");
            return(TF_RESULT_CODE_BAD_PARAM);
    }
    virAddr = strtol(pszArg[1], &endptr, 10);
    if ('\0' == *endptr) {
            AsciiPrint(PRINTSIG"Invalid parameter virAddr\n");
            return(TF_RESULT_CODE_BAD_PARAM);
    }
    Len = strtol(pszArg[2], &endptr,10);
    if ('\0' == *endptr) {
            AsciiPrint(PRINTSIG"Invalid parameter Len");
            return(TF_RESULT_CODE_BAD_PARAM);
    }

	switch (cmd) {
	case 1:
		iRet = DALSYS_CacheCommand(DALSYS_CACHE_CMD_INVALIDATE, virAddr, Len);
		if (DAL_SUCCESS != iRet) {
			AsciiPrint(PRINTSIG"DALSYS_CacheCommand Invalidate returned %d\n", iRet);
			return(TF_RESULT_CODE_FAILURE);
		}
		break;
	case 2:
		iRet = DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, virAddr, Len);
		if (DAL_SUCCESS != iRet) {
			AsciiPrint(PRINTSIG"DALSYS_CacheCommand flush returned %d\n", iRet);
			return(TF_RESULT_CODE_FAILURE);
		}
		break;
	case 3:
		iRet = DALSYS_CacheCommand(DALSYS_CACHE_CMD_CLEAN, virAddr, Len);
		if (DAL_SUCCESS != iRet) {
			AsciiPrint(PRINTSIG"DALSYS_CacheCommand clean returned %d\n", iRet);
			return(TF_RESULT_CODE_FAILURE);
		}
		break;
	default:
		AsciiPrint(PRINTSIG"DALSYS_CacheCommand bad cmd passed\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	return TF_RESULT_CODE_SUCCESS;
}

// DalsysCacheTest's Help Data
CONST TF_HelpDescr TestDalsysCacheTestHelp =
{
    "TestDalsysCache",
    sizeof(DalsysCacheTestParam) / sizeof(TF_ParamDescr),
    DalsysCacheTestParam
};

CONST TF_TestFunction_Descr TestDalsysCacheTest =
{
   PRINTSIG"TestDalsysCache",
   TestDalsysCache,
   &TestDalsysCacheTestHelp,
   &context1
};



/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_mem_tests
===========================================================================*/

const TF_TestFunction_Descr* kernel_dal_daltf_dalsys_mem_tests[] = {
  &TestDalsysMallocZeroTest,
  &TestDalsysMallocTest,
  &TestDalsysMemRegTest,
  &TestDalsysCacheTest,
};

/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_mem_init_all
===========================================================================*/
/**
  See documentation in header file
*/
DALResult kernel_dal_daltf_dalsys_mem_init_all(VOID)
{
  DALResult dalResult;

  UINT16 TF_NumberTests =
    (sizeof(kernel_dal_daltf_dalsys_mem_tests) / sizeof(kernel_dal_daltf_dalsys_mem_tests[0]));

  dalResult = tests_daltf_add_tests(kernel_dal_daltf_dalsys_mem_tests, TF_NumberTests);
  return dalResult;
}

/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_mem_deinit_all
===========================================================================*/
/**
  See documentation in header file
*/
DALResult kernel_dal_daltf_dalsys_mem_deinit_all(VOID)
{
  DALResult dalResult;
  UINT16 TF_NumberTests =
    (sizeof(kernel_dal_daltf_dalsys_mem_tests) / sizeof(kernel_dal_daltf_dalsys_mem_tests[0]));

  dalResult = tests_daltf_remove_tests(kernel_dal_daltf_dalsys_mem_tests, TF_NumberTests);
  return dalResult;
}