/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		KernelTest.c
  DESCRIPTION:	Main entry file for all Kernel Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  02/13/15   kk      Changes to account for new DAL-TF command handler
  05/17/14   jd      Created
================================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/
#include "KernelDalsysTest.h"

/* ============================================================================
**  Function : KernelInitTestFunc
** ========================================================================== */
/*!
   @brief
   An init DalTF test case.  Add driver Kernel tests into DALTF.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return

*/
static uint32 KernelInitTestFunc(uint32 dwArg, char* pszArg[])
{
	AsciiPrint(PRINTSIG"Add kernel tests\n");

	if(1 == dwArg) {
		if(0 == AsciiStrnCmp("all", pszArg[0], 3)) {
			if ( DAL_SUCCESS != kernel_dal_daltf_dalsys_init_all()) {
				AsciiPrint(PRINTSIG"kernel_dal_daltf_dalsys_init_all failed\n");
				return 1;
			}
  
			if ( DAL_SUCCESS != kernel_dal_daltf_dalsys_sync_init_all()) {
				AsciiPrint(PRINTSIG"kernel_dal_daltf_dalsys_sync_init_all failed\n");
				return 1;
			} 
			  
			if ( DAL_SUCCESS != kernel_dal_daltf_dalsys_mem_init_all()) {
				AsciiPrint(PRINTSIG"kernel_dal_daltf_dalsys_mem_init_all failed\n");
				return 1;
			}
			  
			if ( DAL_SUCCESS != kernel_dal_daltf_dalsys_events_init_all()) {
				AsciiPrint(PRINTSIG"kernel_dal_daltf_dalsys_mem_init_all failed\n");
				return 1;
			}
			  
			if ( DAL_SUCCESS != kernel_daltf_devcfg_init_all()) {
				AsciiPrint(PRINTSIG"kernel_dal_daltf_dalsys_mem_init_all failed\n");
				return 1;
			}
    	} else {
			return TF_RESULT_CODE_BAD_PARAM;
		}
	} else {
		return TF_RESULT_CODE_BAD_PARAM;
	}

   return TF_RESULT_CODE_SUCCESS;
}

// Kernel init test's Parameters
static const TF_ParamDescr KernelInitTestParam[] =
{
    {TF_PARAM_STRING, "all", "all - add all Kernel test"}
};
// Kernel Init Test's Help Data
static const TF_HelpDescr KernelInitTestHelp =
{
    "Kernel Init",
    sizeof(KernelInitTestParam) / sizeof(TF_ParamDescr),
    KernelInitTestParam
};


const TF_TestFunction_Descr KernelInitTest =
{
   PRINTSIG"KernelInitTest",
   KernelInitTestFunc,
   &KernelInitTestHelp,
   &context1
};

/* ============================================================================
**  Function : KernelDeInitTestFunc
** ========================================================================== */
/*!
   @brief
   A de-init DalTF test case.  Remove driver Kernel tests from DALTF.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return

*/
static uint32 KernelDeInitTestFunc(uint32 dwArg, char* pszArg[])
{
   AsciiPrint(PRINTSIG"Remove Kernel tests\n");

	if(1 == dwArg) {
		if(0 == AsciiStrnCmp("all", pszArg[0], 3)) {
			if ( DAL_SUCCESS != kernel_dal_daltf_dalsys_deinit_all()) {
				AsciiPrint(PRINTSIG"kernel_dal_daltf_dalsys_deinit_all failed\n");
				return 1;
			}
    
			if ( DAL_SUCCESS != kernel_dal_daltf_dalsys_sync_deinit_all()) {
				AsciiPrint(PRINTSIG"kernel_dal_daltf_dalsys_sync_deinit_all failed\n");
				return 1;
			}
  
			if ( DAL_SUCCESS != kernel_dal_daltf_dalsys_mem_deinit_all()) {
				AsciiPrint(PRINTSIG"kernel_dal_daltf_dalsys_mem_deinit_all failed\n");
				return 1;
			}
  
			if ( DAL_SUCCESS != kernel_dal_daltf_dalsys_events_deinit_all()) {
				AsciiPrint(PRINTSIG"kernel_dal_daltf_dalsys_mem_deinit_all failed\n");
				return 1;
			}
  
			if ( DAL_SUCCESS != kernel_daltf_devcfg_deinit_all()) {
				AsciiPrint(PRINTSIG"kernel_dal_daltf_dalsys_mem_deinit_all failed\n");
				return 1;
			}		
		} else {
			return TF_RESULT_CODE_BAD_PARAM;
		}
	} else  {
		return TF_RESULT_CODE_BAD_PARAM;
	}

   return TF_RESULT_CODE_SUCCESS;
}

// Kernel deinit test's Parameters
static const TF_ParamDescr KernelDeInitTestParam[] =
{
    {TF_PARAM_STRING, "all", "all - remove all Kernel test"}
};
// KernelDeInitTest's Help Data
static const TF_HelpDescr KernelDeInitTestHelp =
{
    "Sanity De-init",
    sizeof(KernelDeInitTestParam) / sizeof(TF_ParamDescr),
    KernelDeInitTestParam
};

const TF_TestFunction_Descr KernelDeInitTest =
{
   PRINTSIG"KernelDeInitTest",
   KernelDeInitTestFunc,
   &KernelDeInitTestHelp,
   &context1
};



