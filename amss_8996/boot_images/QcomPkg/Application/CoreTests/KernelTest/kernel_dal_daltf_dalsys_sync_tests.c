/*============================================================================
@file         kernel_dal_daltf_dalsys_sync_tests.c

@brief        DALTF based Dalsys synchronization API L1,L2,L3 tests

GENERAL DESCRIPTION
  This file contains L1,L2 and L3 tests for DALSys Synchronization API

Copyright (c) 2012 Qualcomm Technologies Inc. All Rights Reserved. Qualcomm Confidential and Proprietary
============================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/KernelTest/kernel_dal_daltf_dalsys_sync_tests.c#1 $
  $DateTime: 2015/06/01 21:35:43 $
  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------
04/02/2012 vn     Intial Version
04/12/2012 vn	  Add DalsysSyncL2 test
==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR CHIPINFO DRIVER TESTS

==========================================================================*/
#include "kernel_dal_daltf_dalsys_sync_tests.h"
#include "DALSys.h"
#include "DDITimer.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*==========================================================================

                  DEFINITIONS AND DECLARATIONS FOR MODULE

==========================================================================*/

/*=========================================================================
      Constants and Macros
==========================================================================*/
#define TEST_DALSYS_WL_PRIORITY 0
#define TEST_DALSYS_NUM_OF_WL 1
#define TEST_DALSYS_WL_STACK_SIZE 0x00001000



CONST TF_ParamDescr DalsysSyncL1TestParam[] =
{
    {TF_PARAM_STRING, "Override",		"Optional (or leave blank)\n"},
};


/* ============================================================================
                         Global Data for Sync Test
** ===========================================================================*/

static DALSYSEventHandle hEvent1;
static DALSYSEventHandle hEvent2;
static DALSYSEventHandle hEvent3;
static DALSYSSyncHandle  hSync1;
static DALSYSSyncHandle  hSync2;

// static DALSYSSyncHandle  hSync;

DALResult workLoopSyncFcn1( DALSYSEventHandle hEvent, VOID * pCtxt )
{

    int *shared_var1 = NULL;
    AsciiPrint(PRINTSIG"Enter workLoopSyncFcn1\n");

    /*Acquire hSync1*/
	DALSYS_SyncEnter(hSync1);

    /*Acquire ownership on event to wait */
    if (DALSYS_EventCtrl(hEvent1, DALSYS_EVENT_CTRL_ACCQUIRE_OWNERSHIP)!=DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_EVENT_CTRL_ACCQUIRE_OWNERSHIP Failed\n");
        return DAL_ERROR;
    }
    /* Wait until workLoopSyncFcn2 safely acquire lock on hSync2*/
    if (DALSYS_EventWait(hEvent1)!=DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_EventWait Failed\n");
        return DAL_ERROR;
    }

    /* Wait for hSync2 */
    DALSYS_SyncEnter(hSync2);
    shared_var1 = (int*)pCtxt;
    *shared_var1 = 1;
    DALSYS_SyncLeave(hSync2);
    DALSYS_SyncLeave(hSync1);

    /* Notify workLoopSyncFcn2 that shared var is modified */
    if (DALSYS_EventCtrl(hEvent2, DALSYS_EVENT_CTRL_TRIGGER)!=DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_EVENT_CTRL_TRIGGER Failed\n");
        return DAL_ERROR;
    }

    AsciiPrint(PRINTSIG"Exit workLoopSyncFcn1\n");
    return DAL_WORK_LOOP_EXIT;
}

DALResult workLoopSyncFcn2( DALSYSEventHandle hEvent, VOID * pCtxt )
{

    int *shared_var2 = NULL;
    AsciiPrint(PRINTSIG"Enter workLoopSyncFcn2\n");

    /*Acquire hSync2*/
    DALSYS_SyncEnter(hSync2);

    /*Notify workLoopSyncFcn1 that hSync2 has been acquired */
    if (DALSYS_EventCtrl(hEvent1, DALSYS_EVENT_CTRL_TRIGGER)!=DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_EVENT_CTRL_TRIGGER Failed\n");
        return DAL_ERROR;
    }

    /*Check if workLoopSyncFcn1 has not got hSync2 without workLoopSyncFcn2 released it*/
    shared_var2 = (int*)pCtxt;
    if (*shared_var2 !=0 ) {
        AsciiPrint(PRINTSIG"workLoopSyncFcn1 got illegal access to hSync2\n");
        return DAL_ERROR;
    }

    /*Release hSync2 */
    DALSYS_SyncLeave(hSync2);

    /*Acquire ownership on hEvent2 to wait */
    if (DALSYS_EventCtrl(hEvent2, DALSYS_EVENT_CTRL_ACCQUIRE_OWNERSHIP)!=DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_EVENT_CTRL_ACCQUIRE_OWNERSHIP Failed\n");
        return DAL_ERROR;
    }

    /* Wait until workLoopSyncFcn1 to acquire hSync2 and modify shared var*/
    if (DALSYS_EventWait(hEvent2)!=DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_EventWait Failed\n");
        return DAL_ERROR;
    }

    /*check share var state */
    if (*shared_var2 !=1 ) {
        AsciiPrint(PRINTSIG"UnExpected value for shared_var %d\n", *shared_var2);
        return DAL_ERROR;
    }

    /* Trigger main test */
    if (DALSYS_EventCtrl(hEvent3, DALSYS_EVENT_CTRL_TRIGGER)!=DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_EVENT_CTRL_TRIGGER Failed\n");
        return DAL_ERROR;
    }
    AsciiPrint(PRINTSIG"Exit workLoopSyncFcn2\n");
    return DAL_SUCCESS;   
}


/*
===============================================================================
**  Function : TestDalsysSyncL1
** ============================================================================
*/
/*!
    @brief
    DALSYS_SyncCreate, DALSYS_SyncEnter, DALSYS_SyncLeave API test.
	Unit test ported over from Dev team.
    @details
	
    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

	@par API Tested 
	DALSYS_SyncCreate
	DALSYS_SyncEnter
	DALSYS_SyncLeave
	
    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/

UINT32 TestDalsysSyncL1(UINT32 dwArg, CHAR8 *pszArg[])
{
	// static CHAR8 * WLName1 = "DALTF_DALSYS_Sync_SyncL1_WL1";
	// static CHAR8 * WLName2 = "DALTF_DALSYS_Sync_SyncL1_WL2";
    DALSYSWorkLoopHandle hWorkLoop1;
    DALSYSWorkLoopHandle hWorkLoop2;
    DALSYSEventHandle hWLEvent1;
    DALSYSEventHandle hWLEvent2;
    int shared_var = 0;
	
    AsciiPrint(PRINTSIG"Starting DALSYS Sync L1 Tests\n");
	
	/* Create Sync objects */
    if (DAL_SUCCESS != DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
                                         &hSync1,
                                         NULL)) {
        AsciiPrint(PRINTSIG"DALSYS SYNC CREATE FAILED\n");
        return TF_RESULT_CODE_FAILURE;
    }

    if (DAL_SUCCESS != DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
                                         &hSync2,
                                         NULL)) {
        AsciiPrint(PRINTSIG"DALSYS SYNC CREATE FAILED\n");
        return TF_RESULT_CODE_FAILURE;
    }
    
    /*Create events to wait and for workloop*/
    if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                                          &hEvent1,
                                          NULL)) {
        AsciiPrint(PRINTSIG"DALSYS_EventCreate Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }

    if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                                          &hEvent2,
                                          NULL)) {
        AsciiPrint(PRINTSIG"DALSYS_EventCreate Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }

    if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                                          &hEvent3,
                                          NULL)) {
        AsciiPrint(PRINTSIG"DALSYS_EventCreate Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }

    if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT,
                                          &hWLEvent1,
                                          NULL)) {
        AsciiPrint(PRINTSIG"DALSYS_EventCreate Workloop Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }

    if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT,
                                          &hWLEvent2,
                                          NULL)) {
        AsciiPrint(PRINTSIG"DALSYS_EventCreate Workloop Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    
    /*Register workloops and add events */
    if (DAL_SUCCESS != DALSYS_RegisterWorkLoop(0,
												 1,
												 &hWorkLoop1,
												 NULL)) {
        AsciiPrint(PRINTSIG"DALSYS_RegisterWorkLoop1 Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }

    if (DAL_SUCCESS != DALSYS_RegisterWorkLoop(0,
										         1,
											     &hWorkLoop2,
											     NULL)) {
        AsciiPrint(PRINTSIG"DALSYS_RegisterWorkLoop2 Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    
    if (DAL_SUCCESS != DALSYS_AddEventToWorkLoop(hWorkLoop1,
                                                 workLoopSyncFcn1,
                                                 (VOID *)&shared_var,
                                                 hWLEvent1,
                                                 NULL)) {
        AsciiPrint(PRINTSIG"DALSYS_AddEventToWorkLoop1 Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }

    if (DAL_SUCCESS != DALSYS_AddEventToWorkLoop(hWorkLoop2,
                                                 workLoopSyncFcn2,
                                                 (VOID *)&shared_var,
                                                 hWLEvent2,
                                                 NULL)) {
        AsciiPrint(PRINTSIG"DALSYS_AddEventToWorkLoop2 Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    
    /* Trigger workloop for sync test*/
    if (DALSYS_EventCtrl(hWLEvent1, DALSYS_EVENT_CTRL_TRIGGER)!=DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"Workloop1 Trigger FAILED\n");
        return TF_RESULT_CODE_FAILURE;
    }

    if (DALSYS_EventCtrl(hWLEvent2, DALSYS_EVENT_CTRL_TRIGGER)!=DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"Workloop2 Trigger FAILED\n");
        return TF_RESULT_CODE_FAILURE;
    }
    
    /* Wait for workloops to finish before cleanup */
    if (DALSYS_EventWait(hEvent3)!=DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_EventWait 1 Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    /* cleanup objects */
    if (DALSYS_DestroyObject(hSync1) != DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_DestroyObject Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    if (DALSYS_DestroyObject(hSync2) != DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_DestroyObject Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    if (DALSYS_DestroyObject(hEvent1) != DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_DestroyObject Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    if (DALSYS_DestroyObject(hEvent2) != DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_DestroyObject Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    if (DALSYS_DestroyObject(hEvent3) != DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_DestroyObject Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    if (DALSYS_DestroyObject(hWLEvent1) != DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_DestroyObject Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    if (DALSYS_DestroyObject(hWLEvent2) != DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_DestroyObject Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    if (DALSYS_DestroyObject(hWorkLoop1) != DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"DALSYS_DestroyObject hWorkLoop1 Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }
    if (DALSYS_DestroyObject(hWorkLoop2) != DAL_SUCCESS) {
        AsciiPrint(PRINTSIG"\nDALSYS_DestroyObject hWorkLoop2 Failed\n");
        return TF_RESULT_CODE_FAILURE;
    }

    AsciiPrint(PRINTSIG"DALSYS Sync Tests Completed\n");
    return 0;
}

// DalsysSyncTestL1's Help Data
CONST TF_HelpDescr TestDalsysSyncL1Help =
{
    "TestDalsysSyncL1",
    sizeof(DalsysSyncL1TestParam) / sizeof(TF_ParamDescr),
    DalsysSyncL1TestParam
};

CONST TF_TestFunction_Descr TestDalsysSyncL1Test =
{
   PRINTSIG"TestDalsysSyncL1",
   TestDalsysSyncL1,
   &TestDalsysSyncL1Help,
   &context1
};

// End of TestDalsysSyncL1 Test

/* WorkLoop Function for DalsysSyncTryEnter Test */
// DALResult workLoopSyncFcn (DALSYSEventHandle hEvent, VOID * pCtxt )
// {
	// DALSYSEventHandle hEventToTrigger = (int *) pCtxt;
	// UINT32 iRet;
	
	// iRet = DALSYS_SyncEnter (hSync);
	// if (DAL_SUCCESS == iRet) {
		// AsciiPrint(PRINTSIG"Error!!!...Cannot lock hSync here: iRet = %d\n", iRet);
	// }
	// else if (DAL_SYNC_ENTER_FAILED == iRet) {
		// AsciiPrint(PRINTSIG"SUCCESS!!...DALSYS_SyncEnter returned DAL_SYNC_ENTER_FAILED as expected\n");
	// }
	// else {
			// AsciiPrint(PRINTSIG"ERROR!!...Unexpected iRet value %d\n", iRet);
	// }
		
	// if (DAL_SUCCESS != DALSYS_EventCtrl(hEventToTrigger,DALSYS_EVENT_CTRL_TRIGGER)) {
			// AsciiPrint(PRINTSIG"Error!!!...DALSYS_EventCtrl failed in WorkLoop\n");
		// }
	
	// return DAL_SUCCESS;
// }

// /*
// ===============================================================================
// **  Function : TestDalsysSyncTryEnter
// ** ============================================================================
// */
// /*!
    // @brief
	// Test for DALSYS_SyncTryEnter API
	
    // @details
	
    // @param[in] dwArg   number of parameters
    // @param[in] pszArg  testing parameters

	// @par API Tested 
	// DALSYS_SyncCreate
	// DALSYS_SyncTryEnter
		
    // @par Side Effects
    // None

    // @retval 

    // @sa None
// */

// UINT32 TestDalsysSyncTryEnter(UINT32 dwArg, CHAR8* pszArg[])
// {
	// static CHAR8* WLName = "DALTF_DALSYS_SYNC_TryEnter";
	// DALSYSWorkLoopHandle hWorkLoop = NULL;
    // DALSYSEventHandle hWLEvent = NULL;
	// DALSYSEventHandle hEvent = NULL;
	
	// UINT32 iRet;
	
    // if (DAL_SUCCESS != DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
                                         // &hSync,
                                         // NULL)) {
        // AsciiPrint(PRINTSIG"Error!!...DALSYS SYNC CREATE Failed\n");
        // return TF_RESULT_CODE_FAILURE;
    // }
	
	// if (NULL == hSync) {
		// AsciiPrint(PRINTSIG"Error!!..Null Sync Handle\n");
		// return TF_RESULT_CODE_FAILURE;
	// }
	
	// /*Create events to wait and for workloop*/
    // if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                                          // &hEvent,
                                          // NULL)) {
        // AsciiPrint(PRINTSIG"DALSYS_EventCreate Failed\n");
        // return TF_RESULT_CODE_FAILURE;
    // }
	
	// if (NULL == hEvent) {
		// AsciiPrint(PRINTSIG"Error!!..Null Event Handle\n");
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hSync)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hSync) failed\n");
		// }
		// return TF_RESULT_CODE_FAILURE;
	// }		
	
	// /*Create WorkLoop Event*/
    // if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT,
                                          // &hWLEvent,
                                          // NULL)) {
        // AsciiPrint(PRINTSIG"DALSYS_EventCreate Failed\n");
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hSync)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hSync) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hEvent)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hEvent) failed\n");
		// }
        // return TF_RESULT_CODE_FAILURE;
    // }
	
	// if (NULL == hEvent) {
		// AsciiPrint(PRINTSIG"Error!!..Null Event Handle\n");
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hSync)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hSync) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hEvent)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hEvent) failed\n");
		// }
		// return TF_RESULT_CODE_FAILURE;
	// }
	
	// /*Register workloops and add events */
    // if (DAL_SUCCESS != DALSYS_RegisterWorkLoopEx(WLName,
												 // TEST_DALSYS_WL_STACK_SIZE,
											     // TEST_DALSYS_WL_PRIORITY,
											     // TEST_DALSYS_NUM_OF_WL,
											     // &hWorkLoop,
											     // NULL)) {
        // AsciiPrint(PRINTSIG"DALSYS_RegisterWorkLoop1 Failed\n");
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hSync)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hSync) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hEvent)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hEvent) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hWorkLoop)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hWorkLoop) failed\n");
		// }
        // return TF_RESULT_CODE_FAILURE;
    // }
		
	// if (NULL == hWorkLoop) {
		// AsciiPrint(PRINTSIG"Error!!..Null WorkLoop Handle");
        
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hSync)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hSync) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hEvent)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hEvent) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hWorkLoop)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hWorkLoop) failed\n");
		// }
		// return TF_RESULT_CODE_FAILURE;
	// }
	
	// if (DAL_SUCCESS != DALSYS_AddEventToWorkLoop(hWorkLoop,
                                                 // workLoopSyncFcn,
                                                 // (VOID *) hEvent,
                                                 // hWLEvent,
                                                 // NULL)) {
        // AsciiPrint(PRINTSIG"Error!!...DALSYS_AddEventToWorkLoop Failed\n");
        // return TF_RESULT_CODE_FAILURE;
    // }
	
	// // Try to get lock
	// iRet = DALSYS_SyncTryEnter (hSync) ;
	// if (iRet != DAL_SUCCESS) {
		// AsciiPrint(PRINTSIG"Error!!..DALSYS_SyncTryEnter returned %d\n", iRet);
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hSync)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hSync) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hEvent)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hEvent) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hWorkLoop)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hWorkLoop) failed\n");
		// }
		// return TF_RESULT_CODE_FAILURE;
	// }
	
	// if (DAL_SUCCESS != DALSYS_EventCtrl(hWLEvent, DALSYS_EVENT_CTRL_TRIGGER)) {
		// AsciiPrint(PRINTSIG"Error!!..Failed to trigger the WorkLoop event\n");
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hSync)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hSync) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hEvent)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hEvent) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hWorkLoop)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hWorkLoop) failed\n");
		// }
		// return TF_RESULT_CODE_FAILURE;
	// }
	// if (DAL_SUCCESS != DALSYS_EventWait(hEvent)) {
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hSync)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hSync) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hEvent)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hEvent) failed\n");
		// }
		// if (DAL_SUCCESS != DALSYS_DestroyObject (hWorkLoop)) {
			// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hWorkLoop) failed\n");
		// }
		// return TF_RESULT_CODE_FAILURE;
	// }
	// DALSYS_SyncLeave(hSync);
	
	// if (DAL_SUCCESS != DALSYS_DestroyObject (hSync)) {
		// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hSync) failed\n");
		// return TF_RESULT_CODE_FAILURE;
	// }
	// if (DAL_SUCCESS != DALSYS_DestroyObject (hEvent)) {
		// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hEvent) failed\n");
		// return TF_RESULT_CODE_FAILURE;
	// }
	// if (DAL_SUCCESS != DALSYS_DestroyObject (hWorkLoop)) {
		// AsciiPrint(PRINTSIG"DALSYS_DestroyObject (hWorkLoop) failed\n");
		// return TF_RESULT_CODE_FAILURE;
	// }

	// return TF_RESULT_CODE_SUCCESS;
// }

// // TestDalsysSyncTryEnter's Help Data
// CONST TF_HelpDescr TestDalsysSyncTryEnterHelp =
// {
    // "TestDalsysSyncTryEnter",
    // sizeof(DalsysSyncL1TestParam) / sizeof(TF_ParamDescr),
    // DalsysSyncL1TestParam
// };

// CONST TF_TestFunction_Descr TestDalsysSyncTryEnterTest =
// {
   // PRINTSIG"TestDalsysSyncTryEnter",
   // {TestDalsysSyncTryEnter},
   // &TestDalsysSyncTryEnterHelp,
   // &context1
// };


/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_sync_tests
===========================================================================*/

const TF_TestFunction_Descr* kernel_dal_daltf_dalsys_sync_tests[] = {
  &TestDalsysSyncL1Test,
  // &TestDalsysSyncTryEnterTest,
};

/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_sync_init_all
===========================================================================*/
/**
  See documentation in header file
*/
DALResult kernel_dal_daltf_dalsys_sync_init_all(VOID)
{
  DALResult dalResult;

  UINT16 TF_NumberTests =
    (sizeof(kernel_dal_daltf_dalsys_sync_tests) / sizeof(kernel_dal_daltf_dalsys_sync_tests[0]));

  dalResult = tests_daltf_add_tests(kernel_dal_daltf_dalsys_sync_tests, TF_NumberTests);
  return dalResult;
}

/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_sync_deinit_all
===========================================================================*/
/**
  See documentation in header file
*/
DALResult kernel_dal_daltf_dalsys_sync_deinit_all(VOID)
{
  DALResult dalResult;
  UINT16 TF_NumberTests =
    (sizeof(kernel_dal_daltf_dalsys_sync_tests) / sizeof(kernel_dal_daltf_dalsys_sync_tests[0]));

  dalResult = tests_daltf_remove_tests(kernel_dal_daltf_dalsys_sync_tests, TF_NumberTests);
  return dalResult;
}

