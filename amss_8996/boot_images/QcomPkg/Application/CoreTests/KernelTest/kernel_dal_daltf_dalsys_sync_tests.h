/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		KernelDalsysTest.h
  DESCRIPTION:	Header File for all UEFI APT TLMM Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#ifndef _KERNEL_DALTF_Dalsys_SYNC_TESTS_
#define _KERNEL_DALTF_Dalsys_SYNC_TESTS_

#include "KernelDalsysTest.h"
#include <DALSys.h>

/*====================================================
** STRUCTURES **
====================================================*/

UINT32 TestDalsysSyncL1(UINT32 dwArg, CHAR8* pszArg[]); 
// UINT32 TestDalsysSyncTryEnter(UINT32 dwArg, CHAR8* pszArg[]);
 
//Initialize dalsys sync daltf tests
DALResult kernel_dal_daltf_dalsys_sync_init_all(VOID);

//De-Initialize dalsys sync daltf tests
DALResult kernel_dal_daltf_dalsys_sync_deinit_all(VOID);

#endif