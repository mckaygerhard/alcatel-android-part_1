
/*===========================================================================

                    Shared Memory Test Source File


 Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights
 Reserved.
===========================================================================*/

/*===========================================================================
  $Id: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/MprocTest/mproc_daltf_smem_test.c#1 $
 
EDIT HISTORY FOR FILE 

when        who     what, where, why
--------    ---     ----------------------------------------------------------
04/28/2015 pvadlama     Initial version.  
===========================================================================*/
/*============================================================================
TEST OBJECTIVE : L3 Coverage

1. Allocate smem area requested by user.
2. Try to change the smem version to a random number.
3. Try to allocate the same size at the same region and
   verify whether the smem is returning the same address
   every single time.
4. Adversarial test case (allocate a different size smem)

==============================================================================*/
#include "mproc_daltf_smem_test.h"

/*=============================================================================
     Global declarations
===============================================================================*/
static const TF_ContextDescr gSmemTestCmnContext= { (uint8)DIAG_SUBSYS_COREBSP, 0};

/*=============================================================================
 
                                INTERNAL API's
===============================================================================*/
/* ============================================================================
**  Function : SmemLocateProtocol
** ========================================================================== */
   /** 
   @brief 
   SmemLocateProtocol function locates the glink protocol.
   */
EFI_STATUS SmemLocateProtocol(EFI_SMEM_PROTOCOL **smem){
    EFI_STATUS	Status = EFI_SUCCESS;
	Status = gBS->LocateProtocol(&gEfiSMEMProtocolGuid, 
                        NULL,(VOID**) smem);
	return Status;
}

/* ============================================================================
**  Function : GetSmemMemType
** ========================================================================== */
/*
   @brief
   return SMEM Item requested

   @param dwArg      - [IN] # parameters
   @param pszArg[0]  - [IN] SMEM item

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   smem_mem_type
 
*/

smem_mem_type GetSmemMemType(uint32 iSmemItem) {

    switch (iSmemItem) {
    case 0 : 
        return(smem_mem_type)SMEM_ID_VENDOR0;
    case 1:
        return(smem_mem_type)SMEM_ID_VENDOR1;
    case 2:
        return(smem_mem_type)SMEM_ID_VENDOR2;
    default:
        return(smem_mem_type)SMEM_INVALID;
    }   
}

/* Parameter Descriptor for SmemGetMemItem */
static const TF_ParamDescr SmemGetItemTestParam[] = {
    {TF_PARAM_UINT32, "SMEM MEM TYPE ", "Enter type of smem item you want to request\n"
                                         "0 : SMEM_ID_VENDOR0\n"
                                         "1 : SMEM_ID_VENDOR1\n"
                                         "2 : SMEM_ID_VENDOR2"},
    {TF_PARAM_UINT32, "Buffer size ",   "Enter the buffer size to allocate\n"}
};

/* Parameter Descriptor for SmemStress */
static const TF_ParamDescr SmemStressTestParam[] = {
	{TF_PARAM_UINT32, "SMEM MEM TYPE ", "Enter type of smem item you want to request\n"
											 "0 : SMEM_ID_VENDOR0\n"
											 "1 : SMEM_ID_VENDOR1\n"
											 "2 : SMEM_ID_VENDOR2"},
	{TF_PARAM_UINT32, "Buffer size ",    "Enter the buffer size\n"},
    {TF_PARAM_UINT32, "Iterations",      "Enter number of iterations"}
};

/*=============================================================================
 
                                PUBLIC API's
===============================================================================*/

/* ============================================================================
**  Function : SmemGetMemItem
** ========================================================================== */
/*
   @brief
   * Allocate a memory in SMEM for the requested item
   * Query the SMEM item previously requested and compare the size

   @param dwArg      - [IN] # parameters
   @param pszArg[0]  - [IN] SMEM item
   @param pszArg[1]  - [IN] iBufSz

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
   TF_RESULT_CODE_BAD_PARAM otherwise.

*/

uint32 SmemGetMemItem(uint32 dwArg, char* pszArg[]) {
    void         	*pSmemAddr;
    void         	*pSmemAddrGet;
    smem_mem_type 	uSmemItemRequested;
    uint32        	iBufSz;
    uint32        	iBufSzRtrn = 0;
    uint32        	iNumParam  = 0;
    char          	*pEndChar  = NULL;
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_SMEM_PROTOCOL 	*smem = NULL;
 
	/*Locate the respective protocol for use*/
	Status = SmemLocateProtocol(&smem);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == smem){
		AsciiPrint(PRINTSIG"Error: smem received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}

    iNumParam = sizeof(SmemGetItemTestParam) / sizeof(TF_ParamDescr);
    if (dwArg != iNumParam) {
        AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n", 
                  iNumParam, dwArg);
        return TF_RESULT_CODE_BAD_PARAM;
    }

    /*Obtain smem_item requested*/
    uSmemItemRequested = GetSmemMemType(strtol(pszArg[0], &pEndChar, 10));
    if (SMEM_INVALID == uSmemItemRequested) {
        AsciiPrint (PRINTSIG"Error: SMEM ITEM requested is not appropriate for test!\n");
        return TF_RESULT_CODE_BAD_PARAM;
    }

    /*No check on Buffer size as no limit define in ISOD*/
    iBufSz = strtol(pszArg[1], &pEndChar, 10);

	/**Note: A buffer can be allocated from a memory type only once. 
	  Another call with a different size will result in a crash. 
	  This is expected behavior. The adversarial test will test 
	  this functionality.*/ 

    /* Open the LOG AREA to write*/
    Status = smem->SmemAlloc(uSmemItemRequested,iBufSz, &pSmemAddr);
    if((EFI_SUCCESS != Status) || (NULL == pSmemAddr)){
        AsciiPrint (PRINTSIG"Error: Memory allocation for smem item %d failed!\n",
                  uSmemItemRequested);
        return TF_RESULT_CODE_FAILURE;
    }
    AsciiPrint (PRINTSIG"Memory allocated for smem item");

	/*Retrieve the address*/
    Status = smem->SmemGetAddr(uSmemItemRequested, &iBufSzRtrn, &pSmemAddrGet);
	if((EFI_SUCCESS != Status) || (NULL == pSmemAddrGet)){
        AsciiPrint (PRINTSIG"Error: The buffer has not be allocated for smem item %d!\n",
                  uSmemItemRequested);
        return TF_RESULT_CODE_FAILURE;
    }

	/*Compare the address*/
    if (pSmemAddr != pSmemAddrGet){
		AsciiPrint (PRINTSIG"Error: The address retrieved does not match with requested\n");
        return TF_RESULT_CODE_FAILURE;
	}

	/*Compare the buffer size*/
	if(iBufSz != iBufSzRtrn) {
        AsciiPrint (PRINTSIG"Error: The buffer size requested %d is not same as buffer "
                  "size returned %d!\n", 
                  iBufSz,iBufSzRtrn);
        return TF_RESULT_CODE_FAILURE;
    }

    AsciiPrint (PRINTSIG"Buffer Size Requested: %u, obtained: %u\n", iBufSz,iBufSzRtrn);
    AsciiPrint (PRINTSIG"Address Allocated: %d, requested: %d\n", pSmemAddrGet, pSmemAddr);

    return TF_RESULT_CODE_SUCCESS; 
}

/* Help Descriptor for SmemGetMemItem */
static const TF_HelpDescr SmemGetItemTestHelp = {
    "SmemGetMemItem: MPROC_SMEM_L1",
     sizeof(SmemGetItemTestParam) / sizeof(TF_ParamDescr),
     SmemGetItemTestParam
};

/* TestFunction Descriptor for SmemGetMemItem */
const TF_TestFunction_Descr SmemGetItemTest = {
    PRINTSIG "SmemGetMemItem",
    SmemGetMemItem,
    &SmemGetItemTestHelp,
    &gSmemTestCmnContext
};

/* ============================================================================
**  Function : SmemStress
** ========================================================================== */
/*
   @brief
   * This test tries to allocate the smem with given size
   * and verifies whether same address is returned every single time

   @param dwArg      - [IN] # parameters
   @param pszArg[0]  - [IN] Iterations
   
   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
   TF_RESULT_CODE_BAD_PARAM otherwise.

*/

uint32 SmemStress(uint32 dwArg, char* pszArg[]) {
    void          *pSmemAddr = NULL;
    void 		  *prevSmemAddr = NULL;
    smem_mem_type uSmemItemRequested;
    uint32        iBufSz = 0;
    uint32        uIterations = 0;
    uint32        iNumParam  = 0;
    char          *pEndChar  = NULL;
    uint8         i;
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_SMEM_PROTOCOL 	*smem = NULL;
 
	/*Locate the respective protocol for use*/
	Status = SmemLocateProtocol(&smem);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == smem){
		AsciiPrint(PRINTSIG"Error: smem received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}

    iNumParam = sizeof(SmemStressTestParam) / sizeof(TF_ParamDescr);
    if (dwArg != iNumParam) {
        AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n", 
                  iNumParam, dwArg);
        return TF_RESULT_CODE_BAD_PARAM;
    }

    /*Obtain smem_item requested*/
	uSmemItemRequested = GetSmemMemType(strtol(pszArg[0], &pEndChar, 10));
	if (SMEM_INVALID == uSmemItemRequested) {
		AsciiPrint (PRINTSIG"Error: SMEM ITEM requested is not appropriate for test!\n");
		return TF_RESULT_CODE_BAD_PARAM;
	}

    /*No check on Buffer size as no limit define in ISOD*/
	iBufSz = strtol(pszArg[1], &pEndChar, 10);

	/*Number of Iterations*/
	uIterations = strtol(pszArg[0], &pEndChar, 10);

    /*loop here to allocate the same size and verify the address*/
    for (i = 0; i < uIterations; i++) {

        /* Open the LOG AREA to write*/
        Status = smem->SmemAlloc(uSmemItemRequested,iBufSz, &pSmemAddr);
        if((EFI_SUCCESS != Status) || (NULL == pSmemAddr)){
            AsciiPrint (PRINTSIG"Error: Memory allocation for smem item %d failed!\n",
                      uSmemItemRequested);
            return TF_RESULT_CODE_FAILURE;
        }

        /*Compare whether the same address is returned everysingle time*/
        if(NULL != prevSmemAddr){
        	if(pSmemAddr != prevSmemAddr){
        		AsciiPrint (PRINTSIG"Error: Previous address does not match the new address\n");
				return TF_RESULT_CODE_FAILURE;
        	}
        }

        /*Copy the new one to previous address for comparing*/
        prevSmemAddr = pSmemAddr;
    }

    AsciiPrint (PRINTSIG"Same address location returned every time\n");

    return TF_RESULT_CODE_SUCCESS; 
}

/* Help Descriptor for SmemStressTest */
static const TF_HelpDescr SmemStressTestHelp = {
    "SmemStress: MPROC_SMEM_L3",
     sizeof(SmemStressTestParam) / sizeof(TF_ParamDescr),
     SmemStressTestParam
};

/* TestFunction Descriptor for SmemStressTest */
const TF_TestFunction_Descr SmemStressTest = {
    PRINTSIG "SmemStress",
    SmemStress,
    &SmemStressTestHelp,
    &gSmemTestCmnContext
};

/* ============================================================================
**  Function : SmemAdvGetMemItem
** ========================================================================== */
/*
   @brief
   * Allocate a memory in SMEM for the requested item
   * Allocate a different size memory for the same memory item.
   * This is adversarial case. Device crash is expected behavior.

   @param dwArg      - [IN] # parameters
   @param pszArg[0]  - [IN] SMEM item
   @param pszArg[1]  - [IN] iBufSz

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
   TF_RESULT_CODE_BAD_PARAM otherwise.

*/

uint32 SmemAdvGetMemItem(uint32 dwArg, char* pszArg[]) {
    void         	*pSmemAddr;
    smem_mem_type 	uSmemItemRequested;
    uint32        	iBufSz = 0;
    uint32        	iNumParam  = 0;
    char          	*pEndChar  = NULL;
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_SMEM_PROTOCOL 	*smem = NULL;
 
	/*Locate the respective protocol for use*/
	Status = SmemLocateProtocol(&smem);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == smem){
		AsciiPrint(PRINTSIG"Error: smem received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	
    iNumParam = sizeof(SmemGetItemTestParam) / sizeof(TF_ParamDescr);
    if (dwArg != iNumParam) {
        AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n", 
                  iNumParam, dwArg);
        return TF_RESULT_CODE_BAD_PARAM;
    }

    /*Obtain smem_item requested*/
    uSmemItemRequested = GetSmemMemType(strtol(pszArg[0], &pEndChar, 10));
    if (SMEM_INVALID == uSmemItemRequested) {
        AsciiPrint (PRINTSIG"Error: SMEM ITEM requested is not appropriate for test!\n");
        return TF_RESULT_CODE_BAD_PARAM;
    }

    /*No check on Buffer size as no limit define in ISOD*/
    iBufSz = strtol(pszArg[1], &pEndChar, 10);

    /* Open the LOG AREA to allocate once with user given params*/
    Status = smem->SmemAlloc(uSmemItemRequested,iBufSz, &pSmemAddr);
    if (NULL == pSmemAddr) {
        AsciiPrint (PRINTSIG"Error: Memory allocation for smem item %d failed!\n",
                  uSmemItemRequested);
        return TF_RESULT_CODE_FAILURE;
    }
    AsciiPrint (PRINTSIG"Memory allocated for first smem item\n");

	/*Reallocate with same memory item with different buffer size 
	  Changing the buffer size to random number*/
	iBufSz = iBufSz * 2;

	Status = smem->SmemAlloc(uSmemItemRequested,iBufSz, &pSmemAddr);
    if (NULL == pSmemAddr) {
        AsciiPrint (PRINTSIG"Error: Memory allocation for smem item %d failed!\n",
                  uSmemItemRequested);
		AsciiPrint (PRINTSIG"Instead of a crash, graceful API return happened\n");
        return TF_RESULT_CODE_FAILURE;
    }
    AsciiPrint (PRINTSIG"Error: Memory allocated for second smem item\n");

	return TF_RESULT_CODE_FAILURE; 
}

/* Help Descriptor for SmemAdvGetMemItem */
static const TF_HelpDescr SmemAdvGetMemItemHelp = {
    "SmemAdvGetMemItem: MPROC_SMEM_L1",
     sizeof(SmemGetItemTestParam) / sizeof(TF_ParamDescr),
     SmemGetItemTestParam
};

/* TestFunction Descriptor for SmemAdvGetMemItem */
const TF_TestFunction_Descr SmemAdvGetMemItemTest = {
    PRINTSIG "SmemAdvGetMemItem",
    SmemAdvGetMemItem,
    &SmemAdvGetMemItemHelp,
    &gSmemTestCmnContext
};

/*Array of functions that needed to be added during init process*/
const TF_TestFunction_Descr* paSmemDaltfTests[] = {
    &SmemGetItemTest,                    //MPROC_SMEM_L1
    &SmemStressTest,                     //MPROC_SMEM_L3
	&SmemAdvGetMemItemTest,				 //MPROC_SMEM_L1
};

/* ============================================================================
**  Function : smem_daltf_init
** ========================================================================== */
/*
   @brief
   smd_daltf_init

   @param    void

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   DAL_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
   TF_RESULT_CODE_BAD_PARAM otherwise.
*/

DALResult smem_daltf_init(void) {
    DALResult dalResult = DAL_SUCCESS;
    uint16 TF_NumberTests =
    (sizeof(paSmemDaltfTests) / sizeof(paSmemDaltfTests[0]));

    dalResult = tests_daltf_add_tests(paSmemDaltfTests, TF_NumberTests);
    return dalResult;
}

/* ============================================================================
**  Function : smem_daltf_deinit
** ========================================================================== */
/*
   @brief
   smd_daltf_init

   @param    void

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   DAL_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
   TF_RESULT_CODE_BAD_PARAM otherwise.
*/


DALResult smem_daltf_deinit(void) {
    DALResult dalResult = DAL_SUCCESS;
    uint16 TF_NumberTests =
    (sizeof(paSmemDaltfTests) / sizeof(paSmemDaltfTests[0]));

    dalResult = tests_daltf_remove_tests(paSmemDaltfTests, TF_NumberTests);
    return dalResult;
}
