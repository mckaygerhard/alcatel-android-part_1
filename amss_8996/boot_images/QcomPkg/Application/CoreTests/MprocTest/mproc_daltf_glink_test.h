#ifndef _MPROC_GLINK_DALTF_TEST_
#define _MPROC_GLINK_DALTF_TEST_

/*============================================================================

  @file        mproc_daltf_glink_test.h

  @brief       MPROC GLINK TEST

               Copyright (c) 2011 Qualcomm Technologies Incorporated.

               All Rights Reserved.
               Qualcomm Confidential and Proprietary
============================================================================*/

/*===========================================================================
  $Id: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/MprocTest/mproc_daltf_glink_test.h#1 $
 
EDIT HISTORY FOR FILE 

when        who      what, where, why
--------    ---     	----------------------------------------------------
04/28/2015  pvadlama    Initial version. 
===========================================================================*/

/*Common Header files - DAL TF and common C Libraries*/
#include "tests_daltf_common.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "DDITF.h"

/*if below headers not included here, compilation error thrown for DalSys Api's*/
#include "DALSys.h"

/*GLINK API Header file*/
#include "glink.h"

/*Glink test internal functions implementation header file*/
#include "mproc_daltf_glink_internal_test.h"

#define MAX_NAME_LEN 32
#define MAX_MULTI_CHANNEL 10

/*============================================================================
                FUNCTION PROTOTYPES/FORWARD DECLARATION
==============================================================================*/

/*===========================================================================
  FUNCTION:  GlinkOpenSingleLocal
===========================================================================*/
/** 
@brief 
This function is called to open and local a local Glink port with user 
defined parameters.

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/
uint32  GlinkOpenSingleLocal(uint32 dwArg, char *pszArg[]);

/* ============================================================================
**  Function : GlinkStressMaxPortOpen
** ========================================================================== */
/** 
@brief
   * This ADVERSARIAL test open maximum number of allowed ports.
   * Ports are opened only on this processor and this test does not wait for
   * remote port DTR events.
   * Since ports are opened with random names, port closing is not done.
   * Device reset is required after running this test.

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/

uint32 GlinkStressMaxPortOpen(uint32 dwArg, char* pszArg[]);

/* ============================================================================
**  Function : GlinkDataTrasfer
** ========================================================================== */
/** 
@brief 
This function is called to open and close a local Glink port with user 
defined parameters.

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/

uint32 GlinkDataTrasfer(uint32 dwArg, char* pszArg[]);

/* ============================================================================
**  Function : GlinkStressMaxQIntents
** ========================================================================== */
/** 
@brief 
This function is called to open and close a local Glink port with user 
defined parameters.

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/

uint32 GlinkStressMaxQIntents(uint32 dwArg, char* pszArg[]);

/* ============================================================================
**  Function : GlinkOpenAPI
** ========================================================================== */
/**
   @brief
   This function is called to open a Glink logical channel.

   @param pszArg[1]  - [IN] dwArg - Number of inputs passed.
   @param pszArg[2]  - [IN] pszArg - array to input.

   @par Dependencies
   None

   @par Side Effects
   Allocates channel resources and informs remote host about
   channel open.

   @return
   TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
   TF_RESULT_CODE_FAILURE   - If test failed\n
   TF_RESULT_CODE_SUCCESS   - If test passed.

*/

uint32 GlinkOpenAPI(uint32 dwArg, char *pszArg[]);

/* ============================================================================
**  Function : GlinkCloseAPI
** ========================================================================== */
/**
   @brief
   This function is called to close a Glink logical channel.

   @param pszArg[1]  - [IN] dwArg - Number of inputs passed.
   @param pszArg[2]  - [IN] pszArg - array to input.

   @par Dependencies
   None

   @par Side Effects
   Allocates channel resources and informs remote host about
   channel open.

   @return
   TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
   TF_RESULT_CODE_FAILURE   - If test failed\n
   TF_RESULT_CODE_SUCCESS   - If test passed.

*/
uint32 GlinkCloseAPI(uint32 dwArg, char *pszArg[]);

/* ============================================================================
**  Function : GlinkTxAPI
** ========================================================================== */
/**
  @brief
  This function is called to transmit data over a Glink logical channel.

  @param[in]   size       Size of buffer
 
  @param[in]   req_intent Whether to block and request for remote rx intent in
                          case it is not available for this pkt tx
 
  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/
uint32 GlinkTxAPI(uint32 dwArg, char *pszArg[]);

/* ============================================================================
**  Function : GlinkTxvAPI
** ========================================================================== */
/**
  @brief
  This function is called to transmit data in a vector buffer over a Glink logical channel.

  @param[in]   size       Size of buffer
 
  @param[in]   req_intent Whether to block and request for remote rx intent in
                          case it is not available for this pkt tx
 
  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/
uint32 GlinkTxvAPI(uint32 dwArg, char *pszArg[]);

/* ============================================================================
**  Function : GlinkQueueAPI
** ========================================================================== */
/**
  @brief
  This function is called to queue an Rx intent over a Glink logical channel.

  @param[in]   size        Size of buffer

  @param[in]   req_intent  Whether to block and request for remote rx intent in
                       case it is not available for this pkt tx

  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/
uint32 GlinkQueueAPI(uint32 dwArg, char *pszArg[]);

/* ============================================================================
**  Function : GlinkRxDoneAPI
** ========================================================================== */
/**
  @brief
  This function is called to a signal that Rx operation is completed.

  @param[in]   size        Size of buffer

  @param[in]   req_intent  Whether to block and request for remote rx intent in
                       case it is not available for this pkt tx

  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/
uint32 GlinkRxDoneAPI(uint32 dwArg, char *pszArg[]);

/* ============================================================================
**  Function : GlinkSigSetAPI
** ========================================================================== */
/**
  @brief
  This function is called to set the 32 bit control signal field.

  @param[in]   size        Size of buffer

  @param[in]   req_intent  Whether to block and request for remote rx intent in
                       case it is not available for this pkt tx

  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/
uint32 GlinkSigSetAPI(uint32 dwArg, char *pszArg[]);

/* ============================================================================
**  Function : GlinkSigLocalGetAPI
** ========================================================================== */
/**
  @brief
  This function is called to get the local 32 bit control signal field.

  @param[in]   size        Size of buffer

  @param[in]   req_intent  Whether to block and request for remote rx intent in
                       case it is not available for this pkt tx

  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/
uint32 GlinkSigLocalGetAPI(uint32 dwArg, char *pszArg[]);

/* ============================================================================
**  Function : GlinkSigRemoteGetAPI
** ========================================================================== */
/**
  @brief
  This function is called to get the remote 32 bit control signal field.

  @param[in]   size        Size of buffer

  @param[in]   req_intent  Whether to block and request for remote rx intent in
                       case it is not available for this pkt tx

  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/
uint32 GlinkSigRemoteGetAPI(uint32 dwArg, char *pszArg[]);

/* ============================================================================
**  Function : glink_daltf_init
** ========================================================================== */
/**
   @brief - Glink Test Init fuction for DalTF. 

   @param    void

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   DAL_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
   TF_RESULT_CODE_BAD_PARAM otherwise.
*/
DALResult glink_daltf_init(void);
 
/* ============================================================================
**  Function : glink_daltf_deinit
** ========================================================================== */
/**
   @brief - Glink Test De-Init fuction for DalTF. 

   @param    void

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   DAL_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
   TF_RESULT_CODE_BAD_PARAM otherwise.
*/
DALResult glink_daltf_deinit(void); 

#endif /*_MPROC_GLINK_DALTF_TEST_*/
