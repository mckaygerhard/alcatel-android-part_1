#ifndef _MPROC_GLINK_INTERNAL_DALTF_TEST_
#define _MPROC_GLINK_INTERNAL_DALTF_TEST_

/*============================================================================

  @file        mproc_daltf_glink_internal_test.h

  @brief       Glink Internal Test header file.

               Copyright (c) 2011 QUALCOMM Technologies Incorporated.

               All Rights Reserved.
               Qualcomm Confidential and Proprietary
============================================================================*/
/*===========================================================================
  $Id: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/MprocTest/mproc_daltf_glink_internal_test.h#1 $
 
EDIT HISTORY FOR FILE 

when        who      what, where, why
--------    ---      -------------------------------------------------------
12/01/2014  c_sansub Added glink loopback callback functions. 
11/20/2014  pvadlama Added structures/declarations for glink_loopback_* 
11/20/2014  pvadlama Added structures for glink_loopback
10/24/2014  c_arajar Initial version. 
===========================================================================*/

/*Common Header files - DAL TF and common C Libraries*/
#include "mproc_daltf_common.h"

/* Header file for Glink API's*/
#include "glink.h"
#include <Protocol/EFIGLink.h>
#include <string.h>

/* Glink Loopback internal structures*/
#define MAX_NAME_LEN 32


typedef enum {
   LINEAR = 0,
   VECTOR
}loop_buff_type;

typedef enum {
   OPEN = 1,
   CLOSE,
   Q_RX_INT_CONFIG,
   TX_CONFIG,
   RX_DONE_CONFIG,
}loop_req_type;

typedef struct {
   uint32 req_id;
   uint32 req_type;
   uint32 req_size;
}loop_req_head;

typedef struct {
   uint32 delay_ms;
   uint32 name_len;
   char   name[MAX_NAME_LEN];
}loop_open_req;

typedef struct {
   uint32 delay_ms;
   uint32 name_len;
   char   name[MAX_NAME_LEN];
}loop_close_req;

typedef struct {
   uint32 num_intents;
   uint32 intent_size;
   uint32 random_delay;
   uint32 delay_ms;
   uint32 name_len;
   char   name[MAX_NAME_LEN];
}loop_q_rx_int_config_req;

typedef enum {
   NO_TRANSFORM = 0,
   PACKET_COUNT,
   CHECKSUM,
}loop_transform_type;

typedef struct {
   uint32 random_delay;
   uint32 delay_ms;
   uint32 echo_count;
   uint32 transform_type;
   uint32 name_len;
   char   name[MAX_NAME_LEN];
}loop_tx_config_req;

typedef struct {
   uint32 random_delay;
   uint32 delay_ms;
   uint32 name_len;
   char   name[MAX_NAME_LEN];
}loop_rx_done_config_req;

typedef union {
   loop_open_req open;
   loop_close_req close;
   loop_q_rx_int_config_req q_rx_int_conf;
   loop_tx_config_req tx_conf;
   loop_rx_done_config_req rx_done_conf;
}loop_req_payload;

typedef struct {
   loop_req_head header;
   loop_req_payload payload;
}loop_request;

typedef struct {
   uint32 req_id;
   uint32 req_type;
   int    resp;
}loop_response;


/* DalSys Event Pool */
typedef struct {
   DALSYSEventHandle  dalTestConnEvent[TEST_MPROC_NUM_OF_EVENTS];
   DALSYSEventHandle  dalTestLocalDisconnEvent[TEST_MPROC_NUM_OF_EVENTS];
   DALSYSEventHandle  dalTestRemoteDisconnEvent[TEST_MPROC_NUM_OF_EVENTS];
   DALSYSEventHandle  dalTestTxCbEvent[TEST_MPROC_NUM_OF_EVENTS];
   DALSYSEventHandle  dalTestRxIntentReqCbEvent[TEST_MPROC_NUM_OF_EVENTS];
   DALSYSEventHandle  dalTestRxCbEvent[TEST_MPROC_NUM_OF_EVENTS];
   DALSYSEventHandle  dalTestRxIntentCbEvent[TEST_MPROC_NUM_OF_EVENTS];
}GlinkEventPool;


/* Local structure that contains Glink channel properties and info*/
typedef struct {
   glink_handle_type	hGlinkHandle;
   char 			    *name;
   char			        *transport;
   char			        *remote_ss;
   GlinkEventPool       *dalTestEvent;
   uint32               iOptions;
   boolean              bReqIntent;
   boolean              bVector;
   uint32		        iTxDataSize;
   uint32		        *TransmitData;
   uint32               *ReceivedData;
   uint32		        iterations;
}GlinkTestChannelInfoType;

/*Test type defines the input parameters verification*/
typedef enum {
   GLINK_TEST_TYPE_1,
   GLINK_TEST_TYPE_2,
   GLINK_TEST_TYPE_API
}GlinkTestType;

/* Event Type */
typedef enum {
   TEST_GLINK_CONNECTED,
   TEST_GLINK_LOCAL_DISCONNECTED,
   TEST_GLINK_REMOTE_DISCONNECTED,
   TEST_GLINK_TX_NOTIFICATION,
   TEST_GLINK_RX_INTENT_REQ,
   TEST_GLINK_RX_NOTIFICATION,
   TEST_GLINK_RX_INTENT_NOTIFICATION

}GlinkEventType;

/*============================================================================
                INTERNAL FUNCTION PROTOTYPES/FORWARD DECLARATION
==============================================================================*/

/* ============================================================================
**  Function : GlinkLocateProtocol
** ========================================================================== */
   /** 
   @brief 
   GlinkLocateProtocol function locates the glink protocol.
   */
EFI_STATUS GlinkLocateProtocol(EFI_GLINK_PROTOCOL **glink);

/*===========================================================================
  FUNCTION  glink_loopback_construct_req_pkt
===========================================================================*/
/**
  glink_loopback_construct_req_pkt function constructs the request packet
  header for the type of request given by the client.
  
  @param[in] pkt
  @param[in] type

  @return
  None
*/
/*==========================================================================*/
void glink_loopback_construct_req_pkt(loop_request *pkt,
                                      loop_req_type type);

/*===========================================================================
  FUNCTION  glink_loopback_send_request
===========================================================================*/
/**
  glink_loopback_send_request function sends the loopback server a user
  request to apply on the data channel specified in the packet.
  
  @param[in] pCtrlChnlInfoObj
  @param[in] pkt
  @param[in] type
  
  @return
  uint32 : Success(TF_RESULT_CODE_SUCCESS)
  uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/
/*==========================================================================*/
uint32 glink_loopback_send_request(GlinkTestChannelInfoType *pCtrlChnlInfoObj, 
                                   loop_request *pkt,
                                   loop_req_type type);

/*===========================================================================
  FUNCTION  glink_loopback_open
===========================================================================*/
/**
  glink_loopback_open func maps the callback functions, sets the params and
  opens the control channel for the remote proc.
  
  @param[in] pCtrlChnlInfoObj

  @return
  uint32 : Success(TF_RESULT_CODE_SUCCESS)
  uint32 : exceptions(TF_RESULT_CODE_FAILURE)
*/
/*==========================================================================*/
uint32 glink_loopback_open(GlinkTestChannelInfoType *pCtrlChnlInfoObj);

/*===========================================================================
  FUNCTION  glink_loopback_close
===========================================================================*/
/**
  glink_loopback_close function closes the control channel specified by caller.
  
  @param[in] pCtrlChnlInfoObj

  @return
  uint32 : Success(TF_RESULT_CODE_SUCCESS)
  uint32 : exceptions(TF_RESULT_CODE_FAILURE)
*/
/*==========================================================================*/
uint32 glink_loopback_close(GlinkTestChannelInfoType *pCtrlChnlInfoObj);

/*===========================================================================
  FUNCTION  glinkTestLbRxNotificationCb
===========================================================================*/
/**
  glinkTestLbRxNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLbRxNotificationCb(glink_handle_type handle,
                                 const void        *priv,
                                 const void        *pkt_priv,
                                 const void        *ptr,
                                 size_t            size,
                                 size_t            intent_used);

/*===========================================================================
  FUNCTION  glinkTestLbRxvNotificationCb
===========================================================================*/
/**
  glinkTestLbRxvNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLbRxvNotificationCb(glink_handle_type        handle,
                                  const void               *priv,
                                  const void               *pkt_priv,
                                  void                     *iovec,
                                  size_t                   size,
                                  size_t                   intent_used,
                                  glink_buffer_provider_fn vprovider,
                                  glink_buffer_provider_fn pprovider);

/*===========================================================================
  FUNCTION  glinkTestLbTxNotificationCb
===========================================================================*/
/**
  glinkTestLbTxNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLbTxNotificationCb(glink_handle_type handle,
                                 const void        *priv,
                                 const void        *pkt_priv,
                                 const void        *ptr,
                                 size_t            size);

/*===========================================================================
  FUNCTION  glinkTestLbStateNotificationCb
===========================================================================*/
/**
  glinkTestLbStateNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLbStateNotificationCb(glink_handle_type handle,
                                    const void *priv,
                                    glink_channel_event_type  event);

/*===========================================================================
  FUNCTION  glinkTestLbNotifyRxIntentReqCb
===========================================================================*/
/**
  glinkTestLbNotifyRxIntentReqCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
boolean glinkTestLbNotifyRxIntentReqCb(glink_handle_type handle,
                                       const void *priv,
                                       size_t      req_size);

/*===========================================================================
  FUNCTION  glinkTestLbNotifyRxIntentCb
===========================================================================*/
/**
  glinkTestLbNotifyRxIntentCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLbNotifyRxIntentCb(glink_handle_type handle,
                                 const void *priv,
                                 size_t      size);

/*===========================================================================
  FUNCTION  glinkTestLbNotifyRxSigsCb
===========================================================================*/
/**
  glinkTestLbNotifyRxSigsCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLbNotifyRxSigsCb(glink_handle_type handle,
                               const void *priv,
                               uint32      prev, 
                               uint32      curr);

/************************************************************************************/
/************************************************************************************/

/*===========================================================================
  FUNCTION  glinkTestRxNotificationCb
===========================================================================*/
/**
  glinkTestRxNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestRxNotificationCb(glink_handle_type handle,
                               const void        *priv,
                               const void        *pkt_priv,
                               const void        *ptr,
                               size_t            size,
                               size_t            intent_used);

/*===========================================================================
  FUNCTION  glinkTestRxvNotificationCb
===========================================================================*/
/**
  glinkTestRxvNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestRxvNotificationCb(glink_handle_type        handle,
                                const void               *priv,
                                const void               *pkt_priv,
                                void                     *iovec,
                                size_t                   size,
                                size_t                   intent_used,
                                glink_buffer_provider_fn vprovider,
                                glink_buffer_provider_fn pprovider);

/*===========================================================================
  FUNCTION  glinkTestTxNotificationCb
===========================================================================*/
/**
  glinkTestTxNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestTxNotificationCb(glink_handle_type handle,
                               const void        *priv,
                               const void        *pkt_priv,
                               const void        *ptr,
                               size_t            size);

/*===========================================================================
  FUNCTION  glinkTestStateNotificationCb
===========================================================================*/
/**
  glinkTestStateNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestStateNotificationCb(glink_handle_type handle,
                                  const void *priv,
                                  glink_channel_event_type  event);

/*===========================================================================
  FUNCTION  glinkTestNotifyRxIntentReqCb
===========================================================================*/
/**
  glinkTestNotifyRxIntentReqCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
boolean glinkTestNotifyRxIntentReqCb(glink_handle_type handle,
                                     const void *priv,
                                     size_t      req_size);

/*===========================================================================
  FUNCTION  glinkTestNotifyRxIntentCb
===========================================================================*/
/**
  glinkTestNotifyRxIntentCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestNotifyRxIntentCb(glink_handle_type handle,
                               const void *priv,
                               size_t      size);

/*===========================================================================
  FUNCTION  glinkTestNotifyRxSigsCb
===========================================================================*/
/**
  glinkTestNotifyRxSigsCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestNotifyRxSigsCb(glink_handle_type handle,
                             const void *priv,
                             uint32      prev,
                             uint32      curr);

/*===========================================================================
  FUNCTION  glinkTestNotifyRxAbort
===========================================================================*/
/**
  glinkTestNotifyRxAbort

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestNotifyRxAbort(glink_handle_type  handle,
                            const void	*priv,
                            const void	*pkt_priv);

/*===========================================================================
  FUNCTION  glinkTestNotifyTxAbort
===========================================================================*/
/**
  glinkTestNotifyTxAbort

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestNotifyTxAbort(glink_handle_type  handle,
                            const void         *priv,
                            const void         *pkt_priv);


/*===========================================================================
  FUNCTION  glinkEchobackRxNotifCb
===========================================================================*/
/**
  glinkEchobackRxNotifCb
  @param[in] 
  @return 
*/
/*==========================================================================*/
void glinkEchobackRxNotifCb(glink_handle_type handle,
                            const void        *priv,      
                            const void        *pkt_priv,
                            const void        *ptr,
                            size_t            size,
                            size_t            intent_used);

/*===========================================================================
  FUNCTION  glinkEchoBackTxNotifCb
===========================================================================*/
/**
  glinkEchoBackTxNotifCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkEchoBackTxNotifCb(glink_handle_type handle,
                            const void        *priv,
                            const void        *pkt_priv,
                            const void        *ptr,
                            size_t            size);

/*===========================================================================
  FUNCTION  glinkEchobackStateNotifCb
===========================================================================*/
/**
  glinkEchobackStateNotifCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkEchobackStateNotifCb(glink_handle_type handle,
                               const void *priv,
                               glink_channel_event_type  event);

/*===========================================================================
  FUNCTION  glinkEchobackRxIntentReqCb
===========================================================================*/
/**
  glinkTestNotifyRxIntentReqCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
boolean glinkEchobackRxIntentReqCb(glink_handle_type handle,
                                   const void *priv,
                                   size_t      req_size);

/*===========================================================================
  FUNCTION  glinkEchobackNotifyRxIntentCb
===========================================================================*/
/**
  glinkEchobackNotifyRxIntentCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkEchobackNotifyRxIntentCb(glink_handle_type handle,
                                   const void *priv,
                                   size_t      size);
/* ============================================================================
**  Function : GlinkTestValidateChannelName
** ===========================================================================*/
/*
   @brief
   * 
   * 
   * 
   * 

   @param pszArg[0]  - 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_BAD_PARAM if  "fail"

*/

uint32 GlinkTestValidateChannelName(char *pChannelName);

/* ============================================================================
**  Function : GlinkTestValidateRemote_ss
** ===========================================================================*/
/*
   @brief
   * 
   * 
   * 
   * 

   @param pszArg[0]  - 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_BAD_PARAM if  "fail"

*/

uint32 GlinkTestValidateRemote_ss(char *pRemote_ss);

/* ============================================================================
**  Function : GlinkTestValidateTransport
** ===========================================================================*/
/*
   @brief
   * 
   * 
   * 
   * 

   @param pszArg[0]  - 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_BAD_PARAM if  "fail"

*/

uint32 GlinkTestValidateTransport(char *pTransport);	     

/* ============================================================================
**  Function : GlinkTestInputHandler
** ===========================================================================*/
/*
   @brief
   * function to validate and handle input param for tests

   @param pszArg[0]  - 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_BAD_PARAM if  "fail"

*/

uint32 GlinkTestInputHandler(GlinkTestChannelInfoType	*glinkChannelInfoObj,
							uint32                	dwArg,
                            char                  	*pszArg[],
			    			uint32                	iNumParam,
				 			GlinkTestType           iGlinkTest);    

/* ============================================================================
**  Function : GlinkTestCreateEvents
** ========================================================================== */
/*
   @brief
   GlinkTestCreateEvents function is called to create -
   * 
   * 
   * 
   * 

   @param arg[0]    - [IN,OUT]
   @param arg[1]    - [IN] pEvtAttrib : Reference to Event attribute.

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/

uint32 GlinkTestCreateEvents(GlinkEventPool *pGlinkEventPoolObj,
                             uint32           *pEvtAttrib);

/* ============================================================================
**  Function : GlinkTestDestroyEvents
** ========================================================================== */
/*
   @brief
   GlinkTestDestroyEvents function is called to destroy
   * 
   * 
   * 
   * 

   @param arg[0]    - [IN] pGlinkEventPoolObj : Reference to port struct.

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/

uint32 GlinkTestDestroyEvents(GlinkEventPool *pGlinkEventPoolObj);

/* ============================================================================
**  Function : GlinkTestResetEvent
** ========================================================================== */
/*
   @brief
   GlinkTestResetEvent function is used to reset the dalEvent.

   @param arg[0]    - [IN] pEvent : Reference to event handle.

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/

uint32 GlinkTestResetEvent(DALSYSEventHandle  *pEvent);
							
/* ============================================================================
**  Function : GlinkTestSetChannelInfo
** ========================================================================== */
/*
   @brief
   * Configure the channel information and set call backs.

   @param 0          - [IN] pGlinkChannelInfoObj - Reference to input object
   @param 0          - [IN] pGlinkOpenCfgType - Reference to cfg type
  
   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
*/

uint32 GlinkTestSetChannelInfo(GlinkTestChannelInfoType   *pGlinkChannelInfoObj,
                               glink_open_config_type    *pGlinkOpenCfgType,
                               GlinkEventPool    		*pDalEvent);

/* ============================================================================
**  Function : GlinkTestWaitEvent
** ========================================================================== */
/*
   @brief
   Wait for Event based on event types

   @param arg[0]    - [IN] GlinkTestPortInfoType : Reference to port struct.
   @param arg[1]    - [IN] iEventType : Type of Event

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/

uint32 GlinkTestWaitEvent(GlinkTestChannelInfoType   *pGlinkChannelInfoObj,
                          uint32                  iEventType);

/* ============================================================================
**  Function : GlinkUpdateChannelName
** ========================================================================== */
/*
   @brief
   Appends the index number depending on iteration count to the channel name 
   given by the user.

   @param arg[0]    - [IN] 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/

char* GlinkUpdateChannelName(char   *channelName, uint32 index);


/* ============================================================================
**  Function : GlinkAdvOutputHandler
** ========================================================================== */
/*
   @brief
   This function handles output verification for the adversarial cases and returns
   status accordingly.
 
   @param arg[0]    - [IN] 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/
uint32 GlinkAdvOutputHandler(uint32 glinkRetCode, boolean advFlag); 

#endif /*_MPROC_GLINK_INTERNAL_DALTF_TEST_*/
