/* ============================================================================

				Glink Test


 Copyright (c) 2011 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
==============================================================================*/

/* ============================================================================
  $Id: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/MprocTest/mproc_daltf_glink_internal_test.c#1 $
 
EDIT HISTORY FOR FILE

when        who          what, where, why
--------    ---          -----------------------------------------------------
04/28/2015  pvadlama 	 Initial Version
==============================================================================*/
#include "mproc_daltf_glink_internal_test.h"
/*=============================================================================
       Global declarations
===============================================================================*/

/*=============================================================================
                                INTERNAL API's
===============================================================================*/
/* ============================================================================
**  Function : GlinkLocateProtocol
** ========================================================================== */
   /** 
   @brief 
   GlinkLocateProtocol function locates the glink protocol.
   */
EFI_STATUS GlinkLocateProtocol(EFI_GLINK_PROTOCOL **glink){
    EFI_STATUS	Status = EFI_SUCCESS;
	Status = gBS->LocateProtocol(&gEfiGLINKProtocolGuid, 
                        NULL,(VOID**) glink);
	return Status;
}

/*===========================================================================
  FUNCTION  glink_loopback_construct_req_pkt
===========================================================================*/
/**
  glink_loopback_construct_req_pkt function constructs the request packet
  header for the type of request given by the client.
  
  @param[in] pkt
  @param[in] type

  @return
  None
*/
/*==========================================================================*/
void glink_loopback_construct_req_pkt(loop_request *pkt,
                                      loop_req_type type) {
   pkt->header.req_id = 1234;
   pkt->header.req_type = type;

   switch (pkt->header.req_type) {
   case OPEN:
      pkt->header.req_size = sizeof(loop_open_req);
      break;
   case CLOSE:
      pkt->header.req_size = sizeof(loop_close_req);
      break;
   case Q_RX_INT_CONFIG:
      pkt->header.req_size = sizeof(loop_q_rx_int_config_req);
      break;
   case TX_CONFIG:
      pkt->header.req_size = sizeof(loop_tx_config_req);
      break;
   case RX_DONE_CONFIG:
      pkt->header.req_size = sizeof(loop_rx_done_config_req);
      break;
   default:
      AsciiPrint (PRINTSIG"Error:LB: Const_req_pkt unknown type%d:\n", type);
      break;
   }
   return;
}

/*===========================================================================
  FUNCTION  glink_loopback_send_request
===========================================================================*/
/**
  glink_loopback_send_request function sends the loopback server a user
  request to apply on the data channel specified in the packet.
  
  @param[in] *pCtrlChnlInfoObj
  @param[in] pkt
  @param[in] type
  
  @return
  uint32 : Success(TF_RESULT_CODE_SUCCESS)
  uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/
/*==========================================================================*/
uint32 glink_loopback_send_request(GlinkTestChannelInfoType *pCtrlChnlInfoObj, 
                                   loop_request *pkt,
                                   loop_req_type type) {
								   
   uint32            			*iPrivPktData     = NULL;
   int               			iErrCheck         = 0;
   glink_err_type    			glinkLbRetCode    = GLINK_STATUS_FAILURE;
   EFI_STATUS               	Status = EFI_SUCCESS;
   STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	

   if (NULL == pCtrlChnlInfoObj) {
      AsciiPrint (PRINTSIG"Error:LB: Control Channel info structure received is not valid\n");
      return TF_RESULT_CODE_FAILURE;
   }

   if (NULL == pkt) {
      AsciiPrint (PRINTSIG"Error:LB: Request Packet received is NULL\n");
      return TF_RESULT_CODE_FAILURE;
   }

   if (!type) {
      AsciiPrint (PRINTSIG"Error:LB: Request type is not valid\n");
      return TF_RESULT_CODE_FAILURE;
   }

   /*Queueing Rx Intent for the response packet*/
   Status = glink->GlinkQueueRxIntent(pCtrlChnlInfoObj->hGlinkHandle, iPrivPktData, sizeof(loop_response), &glinkLbRetCode); 

   if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkLbRetCode)){
      AsciiPrint (PRINTSIG"Error:LB: Glink Queueing Rx intent failed.\n");
      return TF_RESULT_CODE_FAILURE;
   }

   /*Construct header for the loop request*/
   glink_loopback_construct_req_pkt(pkt, type);
   
   Status = glink->GlinkTx(pCtrlChnlInfoObj->hGlinkHandle, iPrivPktData, pkt, sizeof(loop_request), pCtrlChnlInfoObj->bReqIntent, &glinkLbRetCode); 

   if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkLbRetCode)){
      AsciiPrint (PRINTSIG"Error:LB: Glink Command Transfer failed.\n");
      return TF_RESULT_CODE_FAILURE;
   }

   /* Wait for TEST_GLINK_TX_NOTIFICATION event */
   iErrCheck = GlinkTestWaitEvent(pCtrlChnlInfoObj, TEST_GLINK_TX_NOTIFICATION); 
   if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
        return TF_RESULT_CODE_FAILURE;
   }

   /* Wait for TEST_GLINK_RX_NOTIFICATION event */
   iErrCheck = GlinkTestWaitEvent(pCtrlChnlInfoObj, TEST_GLINK_RX_NOTIFICATION); 
   if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}

/*===========================================================================
  FUNCTION  glink_loopback_open
===========================================================================*/
/**
  glink_loopback_open func maps the callback functions, sets the params and
  opens the control channel for the remote proc.
  
  @param[in] *pCtrlChnlInfoObj

  @return
  uint32 : Success(TF_RESULT_CODE_SUCCESS)
  uint32 : exceptions(TF_RESULT_CODE_FAILURE)
*/
/*==========================================================================*/
uint32 glink_loopback_open(GlinkTestChannelInfoType *pCtrlChnlInfoObj) {

   glink_open_config_type   glinkLbOpenCfg;
   int                      iErrCheck      = 0;
   glink_err_type           glinkLbRetCode = GLINK_STATUS_FAILURE;
   EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}

   if (NULL == pCtrlChnlInfoObj) {
      AsciiPrint (PRINTSIG"Error:LB: Control Channel info type structure received is not valid\n");
      return TF_RESULT_CODE_FAILURE;
   }
   
   if(0 == strcmp("apss", pCtrlChnlInfoObj->remote_ss)){
       glinkLbOpenCfg.name = "LOOPBACK_CTL_APSS";
   }
   else if(0 == strcmp("lpass", pCtrlChnlInfoObj->remote_ss)){
       glinkLbOpenCfg.name = "LOOPBACK_CTL_LPASS";
   }
   else if(0 == strcmp("mpss", pCtrlChnlInfoObj->remote_ss)){
       glinkLbOpenCfg.name = "LOOPBACK_CTL_MPSS";
   }
   else if(0 == strcmp("dsps", pCtrlChnlInfoObj->remote_ss)){
       glinkLbOpenCfg.name = "LOOPBACK_CTL_DSPS";
   }
   else{
       AsciiPrint (PRINTSIG"Error:LB:This Loopback control channel not supported!\n");
       return TF_RESULT_CODE_FAILURE;
   }
   
   
   /*Populate Channel Information*/
   glinkLbOpenCfg.transport 		= pCtrlChnlInfoObj->transport; 
   glinkLbOpenCfg.remote_ss 		= pCtrlChnlInfoObj->remote_ss;
   glinkLbOpenCfg.options			= pCtrlChnlInfoObj->iOptions; 
   glinkLbOpenCfg.priv 		    	= pCtrlChnlInfoObj; 
   glinkLbOpenCfg.notify_rx  	    = glinkTestLbRxNotificationCb;
   glinkLbOpenCfg.notify_rxv 	    = glinkTestLbRxvNotificationCb;
   glinkLbOpenCfg.notify_tx_done        = glinkTestLbTxNotificationCb; 
   glinkLbOpenCfg.notify_state          = glinkTestLbStateNotificationCb; 
   glinkLbOpenCfg.notify_rx_intent_req 	= glinkTestLbNotifyRxIntentReqCb; 
   glinkLbOpenCfg.notify_rx_intent      = glinkTestLbNotifyRxIntentCb; 
   glinkLbOpenCfg.notify_rx_sigs        = glinkTestLbNotifyRxSigsCb;
   glinkLbOpenCfg.notify_rx_abort         = NULL;
   glinkLbOpenCfg.notify_tx_abort         = NULL;
  
   /*Linear or Vector data buffer type*/
   if (pCtrlChnlInfoObj->bVector) {
      glinkLbOpenCfg.notify_rx = NULL;
   } else {
      glinkLbOpenCfg.notify_rxv = NULL; 
   }

   /*Opening a Control Channel*/
   Status = glink->GlinkOpen(&glinkLbOpenCfg, &(pCtrlChnlInfoObj->hGlinkHandle), &glinkLbRetCode);
	
   if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkLbRetCode)){
      AsciiPrint (PRINTSIG"Error:LB: Glink Control Channel open failed. Returned %d\n", glinkLbRetCode);
      return TF_RESULT_CODE_FAILURE;
   }

   /*Waiting for the GLINK_CONNNECTED Event*/
   iErrCheck = GlinkTestWaitEvent(pCtrlChnlInfoObj, TEST_GLINK_CONNECTED);
   if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}

/*===========================================================================
  FUNCTION  glink_loopback_close
===========================================================================*/
/**
  glink_loopback_close function closes the control channel specified by caller.
  
  @param[in] *pCtrlChnlInfoObj

  @return
  uint32 : Success(TF_RESULT_CODE_SUCCESS)
  uint32 : exceptions(TF_RESULT_CODE_FAILURE)
*/
/*==========================================================================*/
uint32 glink_loopback_close(GlinkTestChannelInfoType *pCtrlChnlInfoObj) {

   glink_err_type           glinkLbRetCode;
   int                      iErrCheck         = 0;
   	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}

   
   Status = glink->GlinkClose(pCtrlChnlInfoObj->hGlinkHandle, &glinkLbRetCode); 
   if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkLbRetCode)){
      AsciiPrint (PRINTSIG"Error:LB: Glink Control Channel close failed. Returned %d\n", glinkLbRetCode);
      return TF_RESULT_CODE_FAILURE;
   }

   /* Wait for GLINK_LOCAL_DISCONNECTED event */
   iErrCheck = GlinkTestWaitEvent(pCtrlChnlInfoObj, TEST_GLINK_LOCAL_DISCONNECTED); 
   if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}

/*************************************/
/*** LOOPBACK CALLBACK FUNCTIONS******/
/*************************************/

/*===========================================================================
  FUNCTION  glinkTestLbRxNotificationCb
===========================================================================*/
/**
  glinkTestLbRxNotificationCb

  @param[in] 

  @return 
*/
/*==========================================================================*/
void glinkTestLbRxNotificationCb(glink_handle_type handle, 
                               const void        *priv,
                               const void        *pkt_priv,
                               const void        *ptr,
                               size_t            size,
                               size_t            intent_used) {

  GlinkTestChannelInfoType *pGlinkLbChannelInfoObj = NULL;
  DALResult                dalResult               = DAL_ERROR;
  glink_err_type           glinkLbRetCode;
  	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return;
	}
   

   if (NULL == priv) {
      AsciiPrint (PRINTSIG"Error:LB: glinkTestLbRxNotificationCb: Private Info passed is NULL"); 
      return;
   }

   pGlinkLbChannelInfoObj = (GlinkTestChannelInfoType *)priv; 
 
   /*Received data now points to the ptr data*/
   pGlinkLbChannelInfoObj->ReceivedData = (uint32 *) ptr;
  
   /*Validating the data received before calling done*/
	if (NULL == pGlinkLbChannelInfoObj->ReceivedData) {
		AsciiPrint (PRINTSIG"Error:LB: glinkTestLbRxNotificationCb: Received Buffer is NULL");
		/*Releasing the intent*/
		glink->GlinkRxDone(pGlinkLbChannelInfoObj->hGlinkHandle, ptr, 1, &glinkLbRetCode);
		return;
	}
   
   if ( 0 >= pGlinkLbChannelInfoObj->ReceivedData[2]) {
      /*Received data, triggering the event complete*/
      dalResult = DALSYS_EventCtrl(*(pGlinkLbChannelInfoObj->dalTestEvent->dalTestRxCbEvent),
                                   DALSYS_EVENT_CTRL_TRIGGER);
      if (DAL_SUCCESS != dalResult) {
         AsciiPrint (PRINTSIG"Error:LB: glinkTestLbRxNotificationCb: Failed to trigger dalTestRxCbEvent");
		 /*Releasing the intent*/
		 glink->GlinkRxDone(pGlinkLbChannelInfoObj->hGlinkHandle, ptr, 1, &glinkLbRetCode);
		 pGlinkLbChannelInfoObj->ReceivedData = NULL;
         return;
      }
	  else{
		 /*Releasing the intent*/
		 glink->GlinkRxDone(pGlinkLbChannelInfoObj->hGlinkHandle, ptr, 1, &glinkLbRetCode);
		 pGlinkLbChannelInfoObj->ReceivedData = NULL;
	  }
   }
   else{
		AsciiPrint (PRINTSIG"Error:LB: glinkTestLbRxNotificationCb: Failed to complete request %d, Error code: %d returned", pGlinkLbChannelInfoObj->ReceivedData[1], pGlinkLbChannelInfoObj->ReceivedData[2]);
		/*Releasing the intent*/
		glink->GlinkRxDone(pGlinkLbChannelInfoObj->hGlinkHandle, ptr, 1, &glinkLbRetCode);
		pGlinkLbChannelInfoObj->ReceivedData = NULL;
		return;
   }
}

/*===========================================================================
  FUNCTION  glinkTestLbRxvNotificationCb
===========================================================================*/
/**
  glinkTestLbRxvNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLbRxvNotificationCb(glink_handle_type        handle, 
                                const void               *priv,
                                const void               *pkt_priv,
                                void                     *iovec,
                                size_t                   size,
                                size_t                   intent_used,
                                glink_buffer_provider_fn vprovider,
                                glink_buffer_provider_fn pprovider) {
   if (NULL == priv) {
      AsciiPrint (PRINTSIG"Error:LB: glinkTestLbRxvNotificationCb: Private Info passed is NULL"); 
      return;
   } 
}

/*===========================================================================
  FUNCTION  glinkTestTxNotificationCb
===========================================================================*/
/**
  glinkTestTxNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLbTxNotificationCb(glink_handle_type handle, 
                               const void        *priv,
                               const void        *pkt_priv,
                               const void        *ptr,
                               size_t            size) {

   DALResult                 dalResult              = DAL_SUCCESS;
   GlinkTestChannelInfoType *pGlinkLbChannelInfoObj = NULL;

   if (NULL == priv) {
      AsciiPrint (PRINTSIG"Error:LB: glinkTestLbTxNotificationCb: Private Info passed is NULL"); 
      return;
   }

   pGlinkLbChannelInfoObj = (GlinkTestChannelInfoType *)priv; 

   /*Received Tx done event, triggering the event complete*/
   dalResult = DALSYS_EventCtrl(*(pGlinkLbChannelInfoObj->dalTestEvent->dalTestTxCbEvent), 
                    DALSYS_EVENT_CTRL_TRIGGER);
   if (DAL_SUCCESS != dalResult) {
      AsciiPrint (PRINTSIG"Error:LB: glinkTestLbTxNotificationCb: Failed to trigger dalTestTxCbEvent"); 
      return;
   }
}

/*===========================================================================
  FUNCTION  glinkTestLbStateNotificationCb
===========================================================================*/
/**
  glinkTestLbStateNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLbStateNotificationCb(glink_handle_type handle,
                                    const void *priv,
                                    glink_channel_event_type  event) {

   DALResult                dalResult     	    = DAL_SUCCESS;
   GlinkTestChannelInfoType *pGlinkLbChannelInfoObj = NULL;

   if (NULL == priv) {
      AsciiPrint (PRINTSIG"Error:LB: glinkTestLbStateNotificationCb: Private Info passed is NULL");
      return;
   }

   pGlinkLbChannelInfoObj = (GlinkTestChannelInfoType *)priv;

   if (0 == event) {
      AsciiPrint (PRINTSIG"glinkTestLbStateNotificationCb: Event: GLINK_CONNECTED");

      dalResult = DALSYS_EventCtrl(*(pGlinkLbChannelInfoObj->dalTestEvent->dalTestConnEvent),
                                   DALSYS_EVENT_CTRL_TRIGGER);
      if (DAL_SUCCESS != dalResult) {
         AsciiPrint (PRINTSIG"Error:LB: glinkTestLbTxNotificationCb: Failed to trigger dalTestConnEvent"); 
         return;
      }
   }	else if (1 == event) {
      AsciiPrint (PRINTSIG"glinkTestLbStateNotificationCb: Event: GLINK_LOCAL_DISCONNECTED");
      
      dalResult = DALSYS_EventCtrl(*(pGlinkLbChannelInfoObj->dalTestEvent->dalTestLocalDisconnEvent),
                                   DALSYS_EVENT_CTRL_TRIGGER);
      if (DAL_SUCCESS != dalResult) {
         AsciiPrint (PRINTSIG"Error:LB: glinkTestLbTxNotificationCb: Failed to trigger dalTestLocalDisconnEvent"); 
         return;
      }
   } else if (2 == event) {
      AsciiPrint (PRINTSIG"glinkTestLbStateNotificationCb: Event: GLINK_REMOTE_DISCONNECTED"); 
      
      dalResult = DALSYS_EventCtrl(*(pGlinkLbChannelInfoObj->dalTestEvent->dalTestRemoteDisconnEvent), 
                                   DALSYS_EVENT_CTRL_TRIGGER);
      if (DAL_SUCCESS != dalResult) {
         AsciiPrint (PRINTSIG"Error:LB: glinkTestLbStateNotificationCb: Failed to trigger dalTestRemoteDisconnEvent"); 
         return;
      }
   }
}

/*===========================================================================
  FUNCTION  glinkTestLbNotifyRxIntentReqCb
===========================================================================*/
/**
  glinkTestLbNotifyRxIntentReqCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
boolean glinkTestLbNotifyRxIntentReqCb(glink_handle_type handle, 
                                     const void *priv,
                                     size_t      req_size) {

   DALResult        dalResult     	= DAL_SUCCESS;
   GlinkTestChannelInfoType	*pGlinkLbChannelInfoObj = NULL; 
   glink_err_type         		glinkLbRetCode; 
   	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}

   pGlinkLbChannelInfoObj = (GlinkTestChannelInfoType *)priv; 
   Status = glink->GlinkQueueRxIntent(pGlinkLbChannelInfoObj->hGlinkHandle, 
                                        "testLb",
                                        req_size,
										&glinkLbRetCode);
   if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkLbRetCode)){
      AsciiPrint (PRINTSIG"Error:LB: glinkTestLbNotifyRxIntentReqCb: Queue_Rx_Intent failed with error: %d\n", glinkLbRetCode); 
      return 0;
   }
    
   dalResult = DALSYS_EventCtrl(*(pGlinkLbChannelInfoObj->dalTestEvent->dalTestRxIntentReqCbEvent), 
                                DALSYS_EVENT_CTRL_TRIGGER);
   if (DAL_SUCCESS != dalResult) {
      AsciiPrint (PRINTSIG"Error:LB: glinkTestLbNotifyRxIntentReqCb: Failed to trigger dalTestRxIntentReqCbEvent\n"); 
      return 0;
   }

   return 1;
}

/*===========================================================================
  FUNCTION  glinkTestLbNotifyRxIntentCb
===========================================================================*/
/**
  glinkTestLbNotifyRxIntentCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLbNotifyRxIntentCb(glink_handle_type handle, 
                               const void *priv,
                               size_t      size) {

  AsciiPrint (PRINTSIG"glinkTestLbNotifyRxIntentCb: Received with Size: %d\n", size); 
}

/*===========================================================================
  FUNCTION  glinkTestLbNotifyRxSigsCb
===========================================================================*/
/**
  glinkTestLbNotifyRxSigsCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLbNotifyRxSigsCb(glink_handle_type handle, 
                             const void *priv,
                             uint32      prev,
                             uint32      curr) {

 AsciiPrint (PRINTSIG"glinkTestLbNotifyRxSigsCb: Prev State: %d and current state: %d\n", prev,curr); 
}

/************************************/
/*** GENERIC CALLBACK FUNCTIONS******/
/************************************/

/*===========================================================================
  FUNCTION  glinkTestRxNotificationCb
===========================================================================*/
/**
  glinkTestRxNotificationCb

  @param[in] 

  @return 
*/
/*==========================================================================*/
void glinkTestRxNotificationCb(glink_handle_type handle,
				const void        *priv,      
				const void        *pkt_priv,
				const void        *ptr,
				size_t            size,
				size_t            intent_used){

    DALResult        dalResult     	= DAL_ERROR; 
	GlinkTestChannelInfoType		*pGlinkChannelInfoObj = NULL;
	glink_err_type         		glinkLbRetCode; 
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return; 
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return;
	}
	
	if(NULL == priv){
		AsciiPrint (PRINTSIG"Error: glinkTestRxNotificationCb: Private Info passed is NULL\n");
		return;
	}
	
	pGlinkChannelInfoObj = (GlinkTestChannelInfoType* )priv;
	
	/*Received data now points to the ptr data*/
    pGlinkChannelInfoObj->ReceivedData = (uint32 *) ptr;
	
	/**TODO*/
	/*Validating the data received before calling done
	  Compare data with Txbuffer here and check*/
	
	/*Releasing the intent*/
	Status = glink->GlinkRxDone(pGlinkChannelInfoObj->hGlinkHandle, ptr, 1, &glinkLbRetCode);
	pGlinkChannelInfoObj->ReceivedData = NULL;
		 
	/*Received data, triggering the event complete*/
	dalResult = DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestRxCbEvent),
                                         DALSYS_EVENT_CTRL_TRIGGER);
	if(DAL_SUCCESS != dalResult){
		AsciiPrint (PRINTSIG"Error: glinkTestRxNotificationCb: Failed to trigger dalTestRxCbEvent\n");
		return;
	}
	return;
}

/*===========================================================================
  FUNCTION  glinkTestRxvNotificationCb
===========================================================================*/
/**
  glinkTestRxvNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestRxvNotificationCb( glink_handle_type        handle,
					const void               *priv,
					const void               *pkt_priv,
					void                     *iovec,    
					size_t                   size,      
					size_t                   intent_used, 
					glink_buffer_provider_fn vprovider, 
					glink_buffer_provider_fn pprovider){
	if(NULL == priv){
		AsciiPrint (PRINTSIG"Error: glinkTestRxvNotificationCb: Private Info passed is NULL\n");
		return;
	}
}

/*===========================================================================
  FUNCTION  glinkTestTxNotificationCb
===========================================================================*/
/**
  glinkTestTxNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestTxNotificationCb(glink_handle_type handle,
				const void        *priv,
				const void        *pkt_priv,
				const void        *ptr,
				size_t            size){
					
    /*TODO: Please if the Event trigger is succesful before exiting*/
	
	GlinkTestChannelInfoType		*pGlinkChannelInfoObj = NULL;
	
	if(NULL == priv){
		AsciiPrint (PRINTSIG"Error: glinkTestTxNotificationCb: Private Info passed is NULL\n");
		return;
	}
	
	pGlinkChannelInfoObj = (GlinkTestChannelInfoType* )priv;
	DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestTxCbEvent),
                                         DALSYS_EVENT_CTRL_TRIGGER);
}

/*===========================================================================
  FUNCTION  glinkTestStateNotificationCb
===========================================================================*/
/**
  glinkTestStateNotificationCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestStateNotificationCb(glink_handle_type handle,
					const void *priv,
					glink_channel_event_type  event){
	
	/*TODO: Please if the Event trigger is succesful before exiting*/
	
	GlinkTestChannelInfoType		*pGlinkChannelInfoObj = NULL;
	
	if(NULL == priv){
		AsciiPrint (PRINTSIG"Error: glinkTestStateNotificationCb: Private Info passed is NULL\n");
		return;
	}
	
	pGlinkChannelInfoObj = (GlinkTestChannelInfoType* )priv;

	if( 0 == event){
		AsciiPrint (PRINTSIG"glinkTestStateNotificationCb: Event: GLINK_CONNECTED\n");
		DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestConnEvent),
                                         DALSYS_EVENT_CTRL_TRIGGER);
	}	
	else if( 1 == event){
		AsciiPrint (PRINTSIG"glinkTestStateNotificationCb: Event: GLINK_LOCAL_DISCONNECTED\n");
		DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestLocalDisconnEvent),
                                         DALSYS_EVENT_CTRL_TRIGGER);
	}
	else if( 2 == event){
		AsciiPrint (PRINTSIG"glinkTestStateNotificationCb: Event: GLINK_REMOTE_DISCONNECTED\n");
		DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestRemoteDisconnEvent),
                                        DALSYS_EVENT_CTRL_TRIGGER);
	} 
}

/*===========================================================================
  FUNCTION  glinkTestNotifyRxIntentReqCb
===========================================================================*/
/**
  glinkTestNotifyRxIntentReqCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
boolean glinkTestNotifyRxIntentReqCb(glink_handle_type handle,
					const void *priv,
					size_t      req_size){

	/*TODO: Please if the Event trigger is succesful before exiting*/
	
	GlinkTestChannelInfoType	*pGlinkChannelInfoObj = NULL;
	glink_err_type         		glinkRetCode;
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return TF_RESULT_CODE_FAILURE;
	}

	pGlinkChannelInfoObj = (GlinkTestChannelInfoType* )priv;	
	Status = glink->GlinkQueueRxIntent(pGlinkChannelInfoObj->hGlinkHandle,
						"test",
						req_size,
						&glinkRetCode);
	if(GLINK_STATUS_SUCCESS != glinkRetCode){
			AsciiPrint (PRINTSIG"Error: glinkTestNotifyRxIntentReqCb: Queue_Rx_Intent failed with error: %d\n", glinkRetCode);
			return TF_RESULT_CODE_FAILURE;
		}

	DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestRxIntentReqCbEvent),
                                         DALSYS_EVENT_CTRL_TRIGGER);
					 
	return 1;
}

/*===========================================================================
  FUNCTION  glinkTestNotifyRxIntentCb
===========================================================================*/
/**
  glinkTestNotifyRxIntentCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestNotifyRxIntentCb(glink_handle_type handle,
					const void *priv,
					size_t      size){

        GlinkTestChannelInfoType   *pGlinkChannelInfoObj = NULL;
        DALResult                   dalResult     	= DAL_ERROR;
         
        if (NULL == priv) {
           AsciiPrint (PRINTSIG"Error: glinkTestNotifyRxIntentCb: Private Info passed is NULL\n"); 
           return;
        }

        pGlinkChannelInfoObj = (GlinkTestChannelInfoType*)priv;
        
        dalResult = DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestRxIntentCbEvent), 
                         DALSYS_EVENT_CTRL_TRIGGER);
        if (DAL_SUCCESS != dalResult) {
           AsciiPrint (PRINTSIG"Error: glinkTestNotifyRxIntentCb: Failed to trigger dalTestRxIntentCbEvent\n");
           return; 
        }
        return;
}

/*===========================================================================
  FUNCTION  glinkTestNotifyRxSigsCb
===========================================================================*/
/**
  glinkTestNotifyRxSigsCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestNotifyRxSigsCb(glink_handle_type handle,
					const void *priv,
					uint32      prev,
					uint32      curr){

	AsciiPrint (PRINTSIG"glinkTestNotifyRxSigsCb: Prev State: %u\n", prev);
	AsciiPrint (PRINTSIG"glinkTestNotifyRxSigsCb: Curr State: %u\n", curr);
}

/*===========================================================================
  FUNCTION  glinkTestNotifyRxAbort
===========================================================================*/
/**
  glinkTestNotifyRxAbort

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestNotifyRxAbort(glink_handle_type  handle,
					const void	*priv,
					const void	*pkt_priv){

	AsciiPrint (PRINTSIG"glinkTestNotifyRxAbort: Received\n");
}

/*===========================================================================
  FUNCTION  glinkTestNotifyTxAbort
===========================================================================*/
/**
  glinkTestNotifyTxAbort

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestNotifyTxAbort(glink_handle_type  handle,  
					        const void         *priv,   
                            const void         *pkt_priv){
	AsciiPrint (PRINTSIG"glinkTestNotifyTxAbort: Received\n");
}

/*************************************/
/*** ECHOBACK CALLBACK FUNCTIONS******/
/*************************************/

/*===========================================================================
  FUNCTION  glinkEchobackRxNotifCb
===========================================================================*/
/**
  glinkEchobackRxNotifCb
  @param[in] 
  @return 
*/
/*==========================================================================*/
void glinkEchobackRxNotifCb(glink_handle_type handle,
                            const void        *priv,      
                            const void        *pkt_priv,
                            const void        *ptr,
                            size_t            size,
                            size_t            intent_used){

    DALResult                   dalResult     	= DAL_ERROR; 
	GlinkTestChannelInfoType	*pGlinkChannelInfoObj = NULL;
	
	if(NULL == priv){
		AsciiPrint (PRINTSIG"Error: glinkEchobackRxNotifCb: Private Info passed is NULL\n");
		return;
	}
	
	pGlinkChannelInfoObj = (GlinkTestChannelInfoType* )priv;
	
	/*Received data now points to the ptr data*/
    pGlinkChannelInfoObj->TransmitData = (uint32 *) ptr;
	
	/*Received data, triggering the event complete*/
	dalResult = DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestRxCbEvent),
                                         DALSYS_EVENT_CTRL_TRIGGER);
	if(DAL_SUCCESS != dalResult){
		AsciiPrint (PRINTSIG"Error: glinkEchobackRxNotifCb: Failed to trigger dalTestRxCbEvent\n");
		return;
	}
	return;
}

/*===========================================================================
  FUNCTION  glinkEchoBackTxNotifCb
===========================================================================*/
/**
  glinkEchoBackTxNotifCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkEchoBackTxNotifCb(glink_handle_type handle,
                            const void        *priv,
                            const void        *pkt_priv,
                            const void        *ptr,
                            size_t            size){
    
	GlinkTestChannelInfoType		*pGlinkChannelInfoObj = NULL;
	
	if(NULL == priv){
		AsciiPrint (PRINTSIG"Error: glinkEchoBackTxNotifCb: Private Info passed is NULL\n");
		return;
	}
	
	pGlinkChannelInfoObj = (GlinkTestChannelInfoType* )priv;
	DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestTxCbEvent),
                                         DALSYS_EVENT_CTRL_TRIGGER);
    return;
}

/*===========================================================================
  FUNCTION  glinkEchobackStateNotifCb
===========================================================================*/
/**
  glinkEchobackStateNotifCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkEchobackStateNotifCb(glink_handle_type handle,
                               const void *priv,
                               glink_channel_event_type  event){
	
	GlinkTestChannelInfoType		*pGlinkChannelInfoObj = NULL;
	
	if(NULL == priv){
		AsciiPrint (PRINTSIG"Error: glinkEchobackStateNotifCb: Private Info passed is NULL\n");
		return;
	}
	
	pGlinkChannelInfoObj = (GlinkTestChannelInfoType* )priv;

	if( 0 == event){
		AsciiPrint (PRINTSIG"glinkEchobackStateNotifCb: Event: GLINK_CONNECTED\n");
		DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestConnEvent),
                                         DALSYS_EVENT_CTRL_TRIGGER);
	}	
	else if( 1 == event){
		AsciiPrint (PRINTSIG"glinkEchobackStateNotifCb: Event: GLINK_LOCAL_DISCONNECTED\n");
		DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestLocalDisconnEvent),
                                         DALSYS_EVENT_CTRL_TRIGGER);
	}
	else if( 2 == event){
		AsciiPrint (PRINTSIG"glinkEchobackStateNotifCb: Event: GLINK_REMOTE_DISCONNECTED\n");
		DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestRemoteDisconnEvent),
                                        DALSYS_EVENT_CTRL_TRIGGER);
	} 
}

/*===========================================================================
  FUNCTION  glinkEchobackRxIntentReqCb
===========================================================================*/
/**
  glinkTestNotifyRxIntentReqCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
boolean glinkEchobackRxIntentReqCb(glink_handle_type handle,
                                   const void *priv,
                                   size_t      req_size){

	GlinkTestChannelInfoType	*pGlinkChannelInfoObj = NULL;
	glink_err_type         		glinkRetCode;
		EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return 0;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return 0;
	}

	pGlinkChannelInfoObj = (GlinkTestChannelInfoType* )priv;	
	Status = glink->GlinkQueueRxIntent(pGlinkChannelInfoObj->hGlinkHandle,
						"test",
						req_size,
						&glinkRetCode);
	if(GLINK_STATUS_SUCCESS != glinkRetCode){
			AsciiPrint (PRINTSIG"Error: glinkEchobackRxIntentReqCb: Queue_Rx_Intent failed with error: %d\n", glinkRetCode);
			return 0;
	}
	return 1;
}

/*===========================================================================
  FUNCTION  glinkEchobackNotifyRxIntentCb
===========================================================================*/
/**
  glinkEchobackNotifyRxIntentCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkEchobackNotifyRxIntentCb(glink_handle_type handle,
                                   const void *priv,
                                   size_t      size){
   GlinkTestChannelInfoType		*pGlinkChannelInfoObj = NULL;

   if (NULL == priv) {
      AsciiPrint (PRINTSIG"Error: glinkEchobackNotifyRxIntentCb: Private Info passed is NULL\n"); 
      return;
   }

   pGlinkChannelInfoObj = (GlinkTestChannelInfoType*)priv;
   DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestRxIntentCbEvent),
                   DALSYS_EVENT_CTRL_TRIGGER);
   return; 
}

/*************************************/
/*** END OF CALLBACK FUNCTIONS********/
/*************************************/
/* ============================================================================
**  Function : GlinkTestValidateChannelName
** ===========================================================================*/
/*
   @brief This function validates channel name

   @param pszArg[0]  - 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_BAD_PARAM if  "fail"

*/

uint32 GlinkTestValidateChannelName(char *pChannelName){
	if (NULL != pChannelName){
		/*check to verify that channel name is within permissible length*/
		AsciiPrint (PRINTSIG"ChannelName passed = %s\n",pChannelName);
		if (GLINK_CH_NAME_LEN <= strlen(pChannelName)) {
		    AsciiPrint (PRINTSIG"Error: channel name length is more than max len %d\n" , GLINK_CH_NAME_LEN-1);
		    return TF_RESULT_CODE_BAD_PARAM;
		}
	}
	else{
		AsciiPrint (PRINTSIG"Error: channel Name is NULL\n");
		return TF_RESULT_CODE_BAD_PARAM;
	}
	return TF_RESULT_CODE_SUCCESS;
}

/* ============================================================================
**  Function : GlinkTestValidateRemote_ss
** ===========================================================================*/
/*
   @brief This function validatest the remote subsystem

   @param pszArg[0]  - 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_BAD_PARAM if  "fail"

*/

uint32 GlinkTestValidateRemote_ss(char *pRemote_ss){

	if (NULL != pRemote_ss){
		/*check to verify that remote subsystem given is valid*/
		AsciiPrint (PRINTSIG"Remote Subsystem passed = %s\n",pRemote_ss);
		if (0 == strcmp("apss", pRemote_ss) 
		|| 0 == strcmp("mpss", pRemote_ss) 
		|| 0 == strcmp("lpass", pRemote_ss) 
		|| 0 == strcmp("wcnss", pRemote_ss)
		|| 0 == strcmp("dsps", 	pRemote_ss)		
		|| 0 == strcmp("rpm", 	pRemote_ss)) {
		    return TF_RESULT_CODE_SUCCESS;
		}
		else{
			AsciiPrint (PRINTSIG"Error: Not a valid Remote Subsystem\n");
			return TF_RESULT_CODE_BAD_PARAM;
		}
	}
	else{
		AsciiPrint (PRINTSIG"Error: Remote Subsystem is NULL\n");
		return TF_RESULT_CODE_BAD_PARAM;
	}
	return TF_RESULT_CODE_SUCCESS;

}

/* ============================================================================
**  Function : GlinkTestValidateTransport
** ===========================================================================*/
/*
   @brief This function validates the transport passed 

   @param pszArg[0]  - 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_BAD_PARAM if  "fail"

*/

uint32 GlinkTestValidateTransport(char *pTransport){

	if (NULL != pTransport){
		/*check to verify that remote subsystem given is valid*/
		AsciiPrint (PRINTSIG"Remote Subsystem passed = %s\n",pTransport);
		if (0 == strcmp("SMEM", pTransport) || 0 == strcmp("SMD", pTransport)){
			return TF_RESULT_CODE_SUCCESS;
		}
		else{
			AsciiPrint (PRINTSIG"Error: Not a valid Trasnport\n");
			return TF_RESULT_CODE_BAD_PARAM;
		}
	}
	else{
		AsciiPrint (PRINTSIG"Transport is NULL\n");
	}
	return TF_RESULT_CODE_SUCCESS;
}

/* ============================================================================
**  Function : GlinkTestInputHandler
** ===========================================================================*/
/*
   @brief
   * function to validate and handle input param for tests

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_BAD_PARAM if  "fail"

*/

uint32 GlinkTestInputHandler(GlinkTestChannelInfoType	*glinkChannelInfoObj,
				uint32                	dwArg,
				char                  	*pszArg[],
				uint32                	iNumParam,
				GlinkTestType           iGlinkTest){
	
				char          			*pEndChar  = NULL;
				int                     iErrCheck = 0 ;
	
	if (NULL == glinkChannelInfoObj || NULL == pszArg) {
        AsciiPrint (PRINTSIG"Error: Null exception in InputParamObj at %s\n", __func__);
        return TF_RESULT_CODE_BAD_PARAM;
	}
	if (dwArg != iNumParam) {
        AsciiPrint (PRINTSIG"Error: Number of input parameters incorrect: exp %d got %d\n",
                  iNumParam, dwArg);
        return TF_RESULT_CODE_BAD_PARAM;
	}
        
	if (iGlinkTest == GLINK_TEST_TYPE_API) {
	   /*Transport Name*/
	   glinkChannelInfoObj->transport = pszArg[0];
	   AsciiPrint (PRINTSIG"Transport: %s\n", glinkChannelInfoObj->transport);
	   if (strcmp(glinkChannelInfoObj->transport, "NULL") == 0) {
		  glinkChannelInfoObj->transport = NULL;
	   }

	   /*Remote Subsystem Name (EDGE)*/
	   glinkChannelInfoObj->remote_ss = pszArg[1];
	   AsciiPrint (PRINTSIG"Remote Subsystem: %s\n", glinkChannelInfoObj->remote_ss);
	   if (strcmp(glinkChannelInfoObj->remote_ss, "NULL") == 0) {
		  glinkChannelInfoObj->remote_ss = NULL;
	   }

	   /*Name of the channel*/
	   glinkChannelInfoObj->name = pszArg[2];
	   AsciiPrint (PRINTSIG"Channel: %s\n", glinkChannelInfoObj->name);
	   if (strcmp(glinkChannelInfoObj->name, "NULL") == 0) {
		  glinkChannelInfoObj->name = NULL;
	   }

	   /*Transport Options Configuration*/
	   /*TODO: Obtain the invalid options for a particular transport
	   */
	   glinkChannelInfoObj->iOptions  = (uint32)strtol(pszArg[3], &pEndChar, 10);
	   AsciiPrint (PRINTSIG"Options: %d\n", glinkChannelInfoObj->iOptions);

	}
	
	else if(iGlinkTest == GLINK_TEST_TYPE_1){
		/*Validate Channel name*/
		iErrCheck = GlinkTestValidateChannelName(pszArg[2]);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			return TF_RESULT_CODE_BAD_PARAM;
		}

		/*Validate Remote subsystem*/
		iErrCheck = GlinkTestValidateRemote_ss(pszArg[1]);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			return TF_RESULT_CODE_BAD_PARAM;
		}

		/*Validate Transport*/
		iErrCheck = GlinkTestValidateTransport(pszArg[0]);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		   return TF_RESULT_CODE_BAD_PARAM;
		}
	
		glinkChannelInfoObj->transport = pszArg[0];
		glinkChannelInfoObj->remote_ss = pszArg[1];
		glinkChannelInfoObj->name 	  = pszArg[2];
		glinkChannelInfoObj->iOptions  = (uint32)strtol(pszArg[3], &pEndChar, 10);
		glinkChannelInfoObj->iterations = (uint32)strtol(pszArg[4], &pEndChar, 10);
	}
	
	else if (iGlinkTest == GLINK_TEST_TYPE_2){
		
		/*Validate Channel name*/
		iErrCheck = GlinkTestValidateChannelName(pszArg[2]);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			return TF_RESULT_CODE_BAD_PARAM;
		}

		/*Validate Remote subsystem*/
		iErrCheck = GlinkTestValidateRemote_ss(pszArg[1]);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			return TF_RESULT_CODE_BAD_PARAM;
		}

		/*Validate Transport*/
		iErrCheck = GlinkTestValidateTransport(pszArg[0]);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		   return TF_RESULT_CODE_BAD_PARAM;
		}
	
		glinkChannelInfoObj->transport = pszArg[0];
		glinkChannelInfoObj->remote_ss = pszArg[1];
		glinkChannelInfoObj->name 	  = pszArg[2];
		glinkChannelInfoObj->iOptions  = (uint32)strtol(pszArg[3], &pEndChar, 10);
		glinkChannelInfoObj->bReqIntent  = (uint32)strtol(pszArg[4], &pEndChar, 10); 
		glinkChannelInfoObj->iTxDataSize  = (uint32)strtol(pszArg[5], &pEndChar, 10);
		glinkChannelInfoObj->iterations = (uint32)strtol(pszArg[6], &pEndChar, 10);
	}
	
	return TF_RESULT_CODE_SUCCESS;    
}

/* ============================================================================
**  Function : GlinkTestCreateEvents
** ========================================================================== */
/*
   @brief
   GlinkTestCreateEvents function is called to create events

   @param arg[0]    - [IN,OUT]
   @param arg[1]    - [IN] pEvtAttrib : Reference to Event attribute.

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/

uint32 GlinkTestCreateEvents(GlinkEventPool *pGlinkEventPoolObj,
                             uint32           *pEvtAttrib){
			    
	uint32 iErrCheck = TF_RESULT_CODE_SUCCESS;

    /* Create a dalTestConnEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestCreateEventObj(pGlinkEventPoolObj->dalTestConnEvent,
						pEvtAttrib,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
        return TF_RESULT_CODE_FAILURE;
    }
    /* Create a dalTestLocalDisconnEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestCreateEventObj(pGlinkEventPoolObj->dalTestLocalDisconnEvent,
						pEvtAttrib,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
        return TF_RESULT_CODE_FAILURE;
    }
    /* Create a dalTestRemoteDisconnEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestCreateEventObj(pGlinkEventPoolObj->dalTestRemoteDisconnEvent,
						pEvtAttrib,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
        return TF_RESULT_CODE_FAILURE;
    }
    /* Create a dalTestTxCbEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestCreateEventObj(pGlinkEventPoolObj->dalTestTxCbEvent,
						pEvtAttrib,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
        return TF_RESULT_CODE_FAILURE;
    }
    /* Create a dalTestRxIntentReqCbEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestCreateEventObj(pGlinkEventPoolObj->dalTestRxIntentReqCbEvent,
						pEvtAttrib,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
        return TF_RESULT_CODE_FAILURE;
    }
    /* Create a dalTestRxCbEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestCreateEventObj(pGlinkEventPoolObj->dalTestRxCbEvent,
						pEvtAttrib,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
        return TF_RESULT_CODE_FAILURE;
    }
    /* Create a dalTestConnEvent objects, NUM_OF_EVENT = 2 */
        iErrCheck = mprocTestCreateEventObj(pGlinkEventPoolObj->dalTestRxIntentCbEvent, 
						pEvtAttrib,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
        return TF_RESULT_CODE_FAILURE;
    }    
    return TF_RESULT_CODE_SUCCESS;
}

/* ============================================================================
**  Function : GlinkTestDestroyEvents
** ========================================================================== */
/*
   @brief
   GlinkTestDestroyEvents function is called to destroy 

   @param arg[0]    - [IN] pGlinkEventPoolObj : Reference to port struct.

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/

uint32 GlinkTestDestroyEvents(GlinkEventPool *pGlinkEventPoolObj){
	
	uint32 iErrCheck = TF_RESULT_CODE_SUCCESS;

	/* Destroy a dalTestConnEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestDestroyEventObj(pGlinkEventPoolObj->dalTestConnEvent,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/* Destroy a dalTestLocalDisconnEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestDestroyEventObj(pGlinkEventPoolObj->dalTestLocalDisconnEvent,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/* Destroy a dalTestRemoteDisconnEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestDestroyEventObj(pGlinkEventPoolObj->dalTestRemoteDisconnEvent,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/* Destroy a dalTestTxCbEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestDestroyEventObj(pGlinkEventPoolObj->dalTestTxCbEvent,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/* Destroy a dalTestRxIntentReqCbEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestDestroyEventObj(pGlinkEventPoolObj->dalTestRxIntentReqCbEvent,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/* Destroy a dalTestRxCbEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = mprocTestDestroyEventObj(pGlinkEventPoolObj->dalTestRxCbEvent,
						TEST_MPROC_NUM_OF_EVENTS);
	if (TF_RESULT_CODE_FAILURE == iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
        
        /* Destroy a dalTestRxCbEvent objects, NUM_OF_EVENT = 2 */
        iErrCheck = mprocTestDestroyEventObj(pGlinkEventPoolObj->dalTestRxIntentCbEvent, 
                                             TEST_MPROC_NUM_OF_EVENTS);
        if (TF_RESULT_CODE_FAILURE == iErrCheck) {
           return TF_RESULT_CODE_FAILURE;
        }
	
	return TF_RESULT_CODE_SUCCESS;
}

/* ============================================================================
**  Function : GlinkTestResetEvent
** ========================================================================== */
/*
   @brief
   GlinkTestResetEvent function is used to reset the dalEvent.

   @param arg[0]    - [IN] pEvent : Reference to event handle.

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/

uint32 GlinkTestResetEvent(DALSYSEventHandle  *pEvent) {

    DALResult  dalResult   = DAL_SUCCESS;
    dalResult = DALSYS_EventCtrl(*pEvent, DALSYS_EVENT_CTRL_RESET);
    if ( DAL_SUCCESS != dalResult ) {
        AsciiPrint (PRINTSIG"Error: GlinkTestResetEvent: DalSys Event Reset Failed with error code : %d\n", dalResult);
        return TF_RESULT_CODE_FAILURE;
    }
    return TF_RESULT_CODE_SUCCESS;
}

/* ============================================================================
**  Function : GlinkTestSetChannelInfo
** ========================================================================== */
/*
   @brief
   * Set the port information

   @param 0          - [IN]  - Reference to port struct
   @param 1          - [IN]  - Reference to input object
   @param 2          - [IN]  - Reference to event handle

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
*/

uint32 GlinkTestSetChannelInfo(GlinkTestChannelInfoType   *pGlinkChannelInfoObj,
				glink_open_config_type    *pGlinkOpenCfgType,
				GlinkEventPool    	  *pDalEvent){
	/* Null check */
	if (NULL == pGlinkOpenCfgType || NULL == pGlinkChannelInfoObj) {
		AsciiPrint (PRINTSIG"Error: GlinkTestSetChannelInfo: Cfg/Info object passed is NULL\n");
		return TF_RESULT_CODE_FAILURE;
	}

	/* populate channel information */
	pGlinkOpenCfgType->transport 			= pGlinkChannelInfoObj->transport;
	pGlinkOpenCfgType->remote_ss 			= pGlinkChannelInfoObj->remote_ss;
	pGlinkOpenCfgType->name 				= pGlinkChannelInfoObj->name;
	pGlinkOpenCfgType->options		    	= pGlinkChannelInfoObj->iOptions;
	pGlinkOpenCfgType->priv 		    	= pGlinkChannelInfoObj;
	pGlinkOpenCfgType->notify_rx  	        = glinkTestRxNotificationCb;
	pGlinkOpenCfgType->notify_rxv 	        = NULL; 				//glinkTestRxvNotificationCb;
	pGlinkOpenCfgType->notify_tx_done       = glinkTestTxNotificationCb;
	pGlinkOpenCfgType->notify_state         = glinkTestStateNotificationCb;
	pGlinkOpenCfgType->notify_rx_intent_req = glinkTestNotifyRxIntentReqCb;
	pGlinkOpenCfgType->notify_rx_intent     = glinkTestNotifyRxIntentCb;
	pGlinkOpenCfgType->notify_rx_sigs       = glinkTestNotifyRxSigsCb;
	pGlinkOpenCfgType->notify_rx_abort      = glinkTestNotifyRxAbort;
	pGlinkOpenCfgType->notify_tx_abort      = glinkTestNotifyTxAbort;
	pGlinkChannelInfoObj->dalTestEvent     	= pDalEvent;

    return TF_RESULT_CODE_SUCCESS;
}

/* ============================================================================
**  Function : GlinkTestWaitEvent
** ========================================================================== */
/*
   @brief
   Wait for Event based on event types

   @param arg[0]    - [IN] GlinkTestPortInfoType : Reference to port struct.
   @param arg[1]    - [IN] iEventType : Type of Event

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/

uint32 GlinkTestWaitEvent(GlinkTestChannelInfoType   *pGlinkChannelInfoObj,
                           uint32                    iEventType){

	uint32     iEventIndex = 0;
	DALResult  dalResult   = DAL_SUCCESS;

	if (NULL == pGlinkChannelInfoObj) {
		AsciiPrint (PRINTSIG"Error: pGlinkPortInfoObj in GlinkTestWaitEvent found to be NULL\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	/* If the event type is GLINK_CONNECTED */
	if (0 == iEventType) {
		dalResult =  DALSYS_EventMultipleWait(pGlinkChannelInfoObj->dalTestEvent->dalTestConnEvent,
						      TEST_MPROC_NUM_OF_EVENTS, /*NUM_OF_EVENT = 2*/
						      TEST_MPROC_WAIT_TIME_MIRCO_SEC,
						      &iEventIndex);
		if (dalResult != DAL_SUCCESS){
		AsciiPrint (PRINTSIG"Error: Did not receive GLINK_CONNECTED event\n");
		return TF_RESULT_CODE_FAILURE;
		}
	}

	/* If the event type is GLINK_LOCAL_DISCONNECTED */
	else if (1 == iEventType) {
		dalResult =  DALSYS_EventMultipleWait(pGlinkChannelInfoObj->dalTestEvent->dalTestLocalDisconnEvent,
						      TEST_MPROC_NUM_OF_EVENTS, /*NUM_OF_EVENT = 2*/
						      TEST_MPROC_WAIT_TIME_MIRCO_SEC,
						      &iEventIndex);
		if (dalResult != DAL_SUCCESS){
		AsciiPrint (PRINTSIG"Error: Did not receive GLINK_LOCAL_DISCONNECTED event\n");
		return TF_RESULT_CODE_FAILURE;
		}
	}
	
	/* If the event type is GLINK_REMOTE_DISCONNECTED */
	else if (2 == iEventType) {
		dalResult =  DALSYS_EventMultipleWait(pGlinkChannelInfoObj->dalTestEvent->dalTestRemoteDisconnEvent,
						      TEST_MPROC_NUM_OF_EVENTS, /*NUM_OF_EVENT = 2*/
						      TEST_MPROC_WAIT_TIME_MIRCO_SEC,
						      &iEventIndex);
		if (dalResult != DAL_SUCCESS){
		AsciiPrint (PRINTSIG"Error: Did not receive GLINK_REMOTE_DISCONNECTED event\n");
		return TF_RESULT_CODE_FAILURE;
		}
	}
	
	/* If the event type is TEST_GLINK_TX_NOTIFICATION */	
	else if (3 == iEventType) {
		dalResult =  DALSYS_EventMultipleWait(pGlinkChannelInfoObj->dalTestEvent->dalTestTxCbEvent,
						      TEST_MPROC_NUM_OF_EVENTS, /*NUM_OF_EVENT = 2*/
						      TEST_MPROC_WAIT_TIME_MIRCO_SEC,
						      &iEventIndex);
		if (dalResult != DAL_SUCCESS){
		AsciiPrint (PRINTSIG"Error: Did not receive TEST_GLINK_TX_CB event\n");
		return TF_RESULT_CODE_FAILURE;
		}
	}
	
	/* If the event type is TEST_GLINK_RX_INTENT_REQ */	
	else if (4 == iEventType) {
		dalResult =  DALSYS_EventMultipleWait(pGlinkChannelInfoObj->dalTestEvent->dalTestRxIntentReqCbEvent,
						      TEST_MPROC_NUM_OF_EVENTS, /*NUM_OF_EVENT = 2*/
						      TEST_MPROC_WAIT_TIME_MIRCO_SEC,
						      &iEventIndex);
		if (dalResult != DAL_SUCCESS){
		AsciiPrint (PRINTSIG"Error: Did not receive TEST_GLINK_RX_INTENT_REQ_CB event\n");
		return TF_RESULT_CODE_FAILURE;
		}
	}
	
	/* If the event type is TEST_GLINK_RX_NOTIFICATION */	
	else if (5 == iEventType) {
		dalResult =  DALSYS_EventMultipleWait(pGlinkChannelInfoObj->dalTestEvent->dalTestRxCbEvent,
						      TEST_MPROC_NUM_OF_EVENTS, /*NUM_OF_EVENT = 2*/
						      TEST_MPROC_WAIT_TIME_MIRCO_SEC,
						      &iEventIndex);
		if (dalResult != DAL_SUCCESS){
		AsciiPrint (PRINTSIG"Error: Did not receive TEST_GLINK_RX_CB event\n");
		return TF_RESULT_CODE_FAILURE;
		}
	}
        
	/* If the event type is TEST_GLINK_RX_INTENT_NOTIFICATION */
	else if (6 == iEventType) {
	   dalResult =  DALSYS_EventMultipleWait(pGlinkChannelInfoObj->dalTestEvent->dalTestRxIntentCbEvent, 
											 TEST_MPROC_NUM_OF_EVENTS, /*NUM_OF_EVENT = 2*/
											 TEST_MPROC_WAIT_TIME_MIRCO_SEC,
											 &iEventIndex);
	   if (dalResult != DAL_SUCCESS) {
		  AsciiPrint (PRINTSIG"Error: Did not receive TEST_GLINK_RX_INTENT_NOTIFICATION event\n");
		  return TF_RESULT_CODE_FAILURE;
	   }
	}
	return TF_RESULT_CODE_SUCCESS;
}

/* ============================================================================
**  Function : GlinkUpdateChannelName
** ========================================================================== */
/*
   @brief
   Appends the index number depending on iteration count to the channel name 
   given by the user.

   @param arg[0]    - [IN] 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   char*

*/

char* GlinkUpdateChannelName(char *name, uint32 index){
	char buffer[5];
	uint32 lenght=0, rem=0;
	uint32 temp;
	int i;
	
	temp = ++index;
	if(0 == index){
		buffer[0] = '0';
		buffer[1] = '\0';
		strlcat(name, buffer, 5);
		return name;
	}
	while(temp != 0){
		lenght++;
		temp = temp/10;
	}
	buffer[lenght] = '\0';
	for(i=lenght-1; i>=0; i--){
		rem = index % 10;
		buffer[i] = rem + '0';
		index = index/10;
	}
	strlcat(name, buffer, 5);
	return name;
}

/* ============================================================================
**  Function : GlinkAdvOutputHandler
** ========================================================================== */
/*
   @brief
   This function handles output verification for the adversarial cases and returns
   status accordingly.
 
   @param arg[0]    - [IN] 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/
uint32 GlinkAdvOutputHandler(uint32 glinkRetCode, boolean advFlag) {


   if (advFlag) {
      if (GLINK_STATUS_SUCCESS != glinkRetCode) {
         AsciiPrint (PRINTSIG"Error: Glink Operation Failed with error code: %d as expected\n", glinkRetCode);
         return TF_RESULT_CODE_SUCCESS;
      } else {
         AsciiPrint (PRINTSIG"Error: Glink Operation Successful with wrong value(s), Not An Expected Behavior!\n");
         return TF_RESULT_CODE_FAILURE;
      }
   } else {
      if (GLINK_STATUS_SUCCESS != glinkRetCode) {
         AsciiPrint (PRINTSIG"Error: Glink Operation Failed with error code: %d\n", glinkRetCode);
         return TF_RESULT_CODE_FAILURE;
      } else {
         return TF_RESULT_CODE_SUCCESS;
      }
   }
}

