#ifndef _MPROC_CMN_DALTF_TEST_
#define _MPROC_CMN_DALTF_TEST_

/*============================================================================
 
  @file        mproc_daltf_common_test.h

  @brief       MPROC Common Function.

               Copyright (c) 2011 Qualcomm Technologies Incorporated.
 
               All Rights Reserved.
               Qualcomm Confidential and Proprietary
============================================================================*/
/*============================================================================
  $Id: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/MprocTest/mproc_daltf_common.h#1 $
 
EDIT HISTORY FOR FILE

when        who     what, where, why
--------    ---     -------------------------------------------------------- 
04/28/2015  pvadlama    Initial version. 
============================================================================*/
#include "tests_daltf_common.h" 

/*if below headers not included here, compilation error thrown for basic type*/
//#include "comdef.h" 
#include "DALSys.h"

/*Common Header files - DAL TF and common C Libraries*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "DDITF.h"

#include <Library/CoreHeap.h>
#define TEST_MPROC_DALSYS_ERR_FLAG_DISABLED   0
#define TEST_MPROC_DALSYS_ERR_FLAG_ENABLED    1
#define TEST_MPROC_WAIT_TIME_MIRCO_SEC        60000000
#define TEST_MPROC_NUM_OF_EVENTS              2
#define TEST_MPROC_THROUGHPUT_MULTIPLIER      1954
#define TEST_MPROC_KPI_MAX_RESULT_ENTRIES     15

/* Defined as per convention defined in test_api.idl */
#define TEST_APT_INSTANCE_ID                  1
#define TEST_APT_KPI_INSTANCE_ID              2

/*=============================================================================
                            Exposed API's for Test Functions
===============================================================================*/

/* ============================================================================
**  Function : mprocTestUintToString
** ===========================================================================*/
/*
   @brief : convert unsigned integer to string.
 
   @param arg[0]  - iNumber
   @param arg[1]  - pStringVal 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/
uint32 mprocTestUintToString(uint32 iNumber, 
                             char *pStringVal);

/* ============================================================================
**  Function : mprocTestUintNumOfDigits
** ===========================================================================*/
/*
   @brief : return the number of digits in a unsigned number.
 
   @param arg[0]  - iNumber

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32

*/

uint32 mprocTestUintNumOfDigits(uint32 iNumber);

/* ============================================================================
**  Function : mprocTestMemAllocInit
** ========================================================================== */
/*
   @brief
   * Allocate memory to client buffer
   * Initialize with sequential data

   @param dwArg      - [IN] # parameters
   @param 0          - [IN] pTestBuf : pointer to client buffer
   @param 1          - [IN] iBufSz : size of the buffer requested.
   @param 2          - [IN] iBufLen : length of the buffer.

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
*/

uint32 mprocTestMemAllocInit(uint32 **pTestBuf, 
                             uint32   iBufSz,
                             uint32   iBufLen);


/* ============================================================================
**  Function : mprocTestMemFree
** ========================================================================== */
/*
   @brief
   * Free local buffer allocated for data transfer

   @param dwArg      - [IN] # parameters
   @param 0          - [IN] pTestBuf : pointer to client buffer

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
*/

uint32 mprocTestMemFree(void *pTestBuf);

/* ============================================================================
**  Function : mprocTestDestroyEventObj
** ========================================================================== */
/*
   @brief
   Destroy Dal Event Handles

   @param arg[0]    - [IN] dalTestEvent : Handle to dal Event array
   @param arg[1]    - [IN] iNumofObj : number of event objects.
 
   @par Dependencies
   None

   @par Side Effects
   None

   @return  
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)
 
*/

uint32 mprocTestCreateEventObj(DALSYSEventHandle *dalTestEvent,
                               uint32 *iEvtAttrib, 
                               uint32 iNumofObj);

/* ============================================================================
**  Function : mprocTestCreateEventObj
** ========================================================================== */
/*
   @brief
   create multiple DalEvent object based on args

   @param arg[0]    - [IN] dalTestEvent : Handle to dalEvent Array(default,timeout)
   @param arg[1]    - [IN] iEvtAttrib : Event type (default,timeout)
   @param arg[2]    - [IN] iNumofObj : number of event to be created.
 
   @par Dependencies
   None

   @par Side Effects
   None

   @return  
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)
 
*/

uint32 mprocTestDestroyEventObj(DALSYSEventHandle *dalTestEvent,
                                uint32 iNumofObj);

#endif /*_MPROC_CMN_DALTF_TEST_*/
