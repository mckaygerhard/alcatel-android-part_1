/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		MprocTest.c
  DESCRIPTION:	Main entry file for all Mproc Tests
  
  REVISION HISTORY
  when       who     	what, where, why
  --------   ---     	--------------------------------------------------------
  04/28/15   pvadlama   Initial Version
================================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/
#include "MprocTest.h"

/* ============================================================================
**  Function : mprocInitTestFunc
** ========================================================================== */
/*!
   @brief
   An init DalTF test case.  Add driver mproc tests into DALTF.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return

*/
static uint32 mprocInitTestFunc(uint32 dwArg, char* pszArg[]){
   AsciiPrint(PRINTSIG"Add mproc tests\n");
   if(1 == dwArg){
      if(0 == AsciiStrnCmp("all", pszArg[0], 3)){
    	  if ( DAL_SUCCESS != glink_daltf_init()){
    	    AsciiPrint(PRINTSIG"glink_daltf_init failed\n");
    	    return 1;
    	  }
    	  if ( DAL_SUCCESS != smem_daltf_init()){
    	     AsciiPrint(PRINTSIG"smem_daltf_init failed\n");
    	     return 1;
    	  }
      }
      else{
          return TF_RESULT_CODE_BAD_PARAM;
      }
   }
   else{
      return TF_RESULT_CODE_BAD_PARAM;
   }
   return TF_RESULT_CODE_SUCCESS;
}

/*mproc init test's Parameters*/
static const TF_ParamDescr mprocInitTestParam[] ={
    {TF_PARAM_STRING, "all", "add all mproc test"}
};

/*mproc Init Test's Help Data*/
static const TF_HelpDescr mprocInitTestHelp ={
    "Mproc Init",
    sizeof(mprocInitTestParam) / sizeof(TF_ParamDescr),
    mprocInitTestParam
};

const TF_TestFunction_Descr mprocInitTest ={
   PRINTSIG"MprocInitTest",
   mprocInitTestFunc,
   &mprocInitTestHelp,
   &context1
};

/* ============================================================================
**  Function : mprocDeInitTestFunc
** ========================================================================== */
/*!
   @brief
   A de-init DalTF test case.  Remove driver mproc tests from DALTF.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return

*/
static uint32 mprocDeInitTestFunc(uint32 dwArg, char* pszArg[]){
   AsciiPrint(PRINTSIG"Remove mproc tests\n");
   if(1 == dwArg){
      if(0 == AsciiStrnCmp("all", pszArg[0], 3)){
    	  if ( DAL_SUCCESS != glink_daltf_deinit()){
    	    AsciiPrint(PRINTSIG"glink_daltf_deinit failed\n");
    	    return 1;
    	  }
    	  if ( DAL_SUCCESS != smem_daltf_deinit()){
    	     AsciiPrint(PRINTSIG"smem_daltf_deinit failed\n");
    	     return 1;
    	  }
      }
      else{
          return TF_RESULT_CODE_BAD_PARAM;
      }
   }
   else{
      return TF_RESULT_CODE_BAD_PARAM;
   }
   return TF_RESULT_CODE_SUCCESS;
}

/* mproc deinit test's Parameters*/
static const TF_ParamDescr mprocDeInitTestParam[] ={
    {TF_PARAM_STRING, "all", "remove all mproc test"}
};

/*mprocDeInitTest's Help Data*/
static const TF_HelpDescr mprocDeInitTestHelp ={
    "Mproc De-init",
    sizeof(mprocDeInitTestParam) / sizeof(TF_ParamDescr),
    mprocDeInitTestParam
};

const TF_TestFunction_Descr mprocDeInitTest ={
   PRINTSIG"MprocDeInitTest",
   mprocDeInitTestFunc,
   &mprocDeInitTestHelp,
   &context1
};

