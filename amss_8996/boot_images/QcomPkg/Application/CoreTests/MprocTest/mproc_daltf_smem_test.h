#ifndef _MPROC_SMEM_DALTF_TEST_
#define _MPROC_SMEM_DALTF_TEST_

/*============================================================================

  @file        mproc_daltf_smem_test.h

  @brief       MPROC SMEM API TEST

               Copyright (c) 2011 Qualcomm Technologies Incorporated.

               All Rights Reserved.
               Qualcomm Confidential and Proprietary
============================================================================*/

/*===========================================================================
  $Id: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/MprocTest/mproc_daltf_smem_test.h#1 $
 
EDIT HISTORY FOR FILE 

when        who     what, where, why
--------    ---     ----------------------------------------------------------
04/28/2015  pvadlama    Initial version. 
===========================================================================*/

/*Common Header files - DAL TF and common C Libraries*/
#include "tests_daltf_common.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "DDITF.h"

/*if below headers not included here, compilation error thrown for DalSys Api's*/
#include "DALSys.h"

/*SMEM API Header file*/
#include "smem.h"
#include "smem_type.h"
#include <Protocol/EFISmem.h>

/*============================================================================
                FUNCTION PROTOTYPES/FORWARD DECLARATION
==============================================================================*/

/*===========================================================================
  FUNCTION:  SmemGetMemItem
===========================================================================*/
/** 
@brief 
This function is called to request and verify the requested SMEM
item . The verification is performed based on address and size 
of the smem item returned .

@param [in] dwarg
@param [in] pszArg[]
@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.
@dependencies None.
@par Test Type
Automated.
@par Detail Param List
SMEM MEM TYPE:\n
<ul>
<li>0 - SMEM_ID_VENDOR0</li>
<li>1 - SMEM_ID_VENDOR1</li>
<li>2 - SMEM_ID_VENDOR2</li> 
</ul> 
Buffer size :\n 
<ul>
<li>greater then zero </li>
</ul> 
@note 
SMEM_ID_VENDOR0, SMEM_ID_VENDOR1, and SMEM_ID_VENDOR2 are SMEM 
items that can be used in test code to verify smem apis, as long
as it will never run on the same system as OEM code which might 
use the same smem items. \n
Buffer size requested should be 64 bit aligned and should be 
less than size of SMEM memory.\n 
<b>Preferred Input: </b> 0 , 1024 . 
@par Processor Information
Applicable to all Processors.
@par Target Information
Applicable to all Targets.
@par API Tested 
smem_alloc\n 
smem_get_addr. 
*/
uint32  SmemGetMemItem(uint32 dwArg, char* pszArg[]);

/*===========================================================================
  FUNCTION:  SmemStress
===========================================================================*/
/** 
@brief 
This test tries to allocate the smem with given size
and verifies whether same address is returned every single time

@param [in] dwarg
@param [in] pszArg[]
@return
 
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.
 
@dependencies None.
@par Test Type Automated.
@par Detail Param List
 
Iterations:\n
<ul>
<li>random </li>
</ul> 
 
@note 
SMEM_ID_VENDOR0, SMEM_ID_VENDOR1, and SMEM_ID_VENDOR2 are SMEM 
items that can be used in test code to verify smem apis, as long
as it will never run on the same system as OEM code which might 
use the same smem items. \n
 
@par Processor Information
Applicable to all Processors.
 
@par Target Information
Applicable to all Targets.
 
@par API Tested 
smem_alloc\n 
smem_get_addr. 
*/
uint32  SmemStress(uint32 dwArg, char* pszArg[]);

/*===========================================================================
  FUNCTION:  SmemAdvGetMemItem
===========================================================================*/
/** 
@brief 
Allocate a memory in SMEM for the requested item
Allocate a different size memory for the same memory item.
This is adversarial case. Device crash is expected behavior.

@param [in] dwarg
@param [in] pszArg[]
@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.
@dependencies None.
@par Test Type
Automated.
@par Detail Param List
SMEM MEM TYPE:\n
<ul>
<li>0 - SMEM_ID_VENDOR0</li>
<li>1 - SMEM_ID_VENDOR1</li>
<li>2 - SMEM_ID_VENDOR2</li> 
</ul> 
Buffer size :\n 
<ul>
<li>greater then zero </li>
</ul> 
@note 
SMEM_ID_VENDOR0, SMEM_ID_VENDOR1, and SMEM_ID_VENDOR2 are SMEM 
items that can be used in test code to verify smem apis, as long
as it will never run on the same system as OEM code which might 
use the same smem items. \n
Buffer size requested should be 64 bit aligned and should be 
less than size of SMEM memory.\n 
<b>Preferred Input: </b> 0 , 1024 . 
@par Processor Information
Applicable to all Processors.
@par Target Information
Applicable to all Targets.
@par API Tested 
smem_alloc\n 
smem_get_addr. 
*/
uint32  SmemAdvGetMemItem(uint32 dwArg, char* pszArg[]);

/* smem local Init function for DalTF */
DALResult smem_daltf_init(void);
 
/* smem local deInit function for DalTF */ 
DALResult smem_daltf_deinit(void); 


#endif /*_MPROC_SMEM_DALTF_TEST_*/
