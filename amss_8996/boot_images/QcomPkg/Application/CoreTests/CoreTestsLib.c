/*===============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		CoreTestsLib.c
  DESCRIPTION:	all subsystem init deinit functions to be added in daltf
  
  REVISION HISTORY
  when       who     	what, where, why
  --------   ---     	--------------------------------------------------------
  04/16/15  pvadlama	Added Mproc tests
  03/17/15   jz      	Created
================================================================================*/

/*----------------------------------------------------------------------------
  included files
----------------------------------------------------------------------------*/
#include <Library/CoreTestsLib.h>

/*----------------------------------------------------------------------------
  data declarations
----------------------------------------------------------------------------*/
/**
 *  HWEngines init and deinit functions to be added in daltf
 */
extern const TF_TestFunction_Descr hwenginesDeInitTest;
extern const TF_TestFunction_Descr hwenginesInitTest;

/**
 *  Mproc init and deinit functions to be added in daltf
 */
extern const TF_TestFunction_Descr mprocDeInitTest;
extern const TF_TestFunction_Descr mprocInitTest;

/**
 *  SysDrvTests init and deinit functions to be added in daltf
 */
extern const TF_TestFunction_Descr SysDrvDeInitTest;
extern const TF_TestFunction_Descr SysDrvInitTest;

/**
 *  Pmic init and deinit functions to be added in daltf
 */
 
extern const TF_TestFunction_Descr PmicDeInitTest;
extern const TF_TestFunction_Descr PmicInitTest;

/**
 *  Kernel init and deinit functions to be added in daltf
 */
 
extern const TF_TestFunction_Descr KernelDeInitTest;
extern const TF_TestFunction_Descr KernelInitTest;

/*----------------------------------------------------------------------------
  Global variables
----------------------------------------------------------------------------*/

const TF_TestFunction_Descr* daltf_setup_tests[] = {
   // enable HWEngines
   &hwenginesInitTest,
   &hwenginesDeInitTest,
   
   // enable HWEngines
   &mprocInitTest,
   &mprocDeInitTest,
   
   // enable SysDrv
   &SysDrvInitTest,
   &SysDrvDeInitTest,

   // enable Pmic
   &PmicInitTest,
   &PmicDeInitTest,
   
   // enable Kernel
   &KernelInitTest,
   &KernelDeInitTest,
};

const UINTN daltf_setup_number =
   (sizeof(daltf_setup_tests) / sizeof(TF_TestFunction_Descr*));