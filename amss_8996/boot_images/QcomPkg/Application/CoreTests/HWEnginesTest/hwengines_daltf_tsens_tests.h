/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		hwengines_daltf_tsens_tests.h
  DESCRIPTION:	Header File for all UEFI APT TSENS Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   jd      Created
================================================================================*/

#ifndef _HWEngines_DALTF_TSENS_TESTS_
#define _HWEngines_DALTF_TSENS_TESTS_

#include "HWEnginesTest.h"

#define TSENS_TEST_TEMP_MIN 200
#define TSENS_TEST_TEMP_MAX 700 

/*====================================================
** Declarations **
====================================================*/ 
//Initialize tsens daltf tests
DALResult tsens_daltf_init(VOID);

//De-Initialize tsens daltf tests
DALResult tsens_daltf_deinit(VOID);

#endif
