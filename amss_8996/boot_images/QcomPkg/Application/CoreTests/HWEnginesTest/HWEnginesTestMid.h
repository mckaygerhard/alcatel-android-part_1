/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		HWEnginesTest.h
  DESCRIPTION:	Common headers included for all HWEngines files
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   jd      Created
================================================================================*/

#ifndef _HWEngines_TESTSMid_
#define _HWEngines_TESTSMid_

#include "tests_daltf_common.h"   /* Common definitions */

#include "DDIHWIO.h"
#include "DDIClock.h"

#ifdef USE_DIAG
#include "diagcmd.h"
#endif

#include <Protocol/EFIAdc.h>
#include <Protocol/EFITsens.h>

#include "hwengines_daltf_adc_tests.h"
#include "hwengines_daltf_tsens_tests.h"

#endif
