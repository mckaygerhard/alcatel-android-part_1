/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		HWEnginesTest.c
  DESCRIPTION:	Main entry file for all HWEngines Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  02/13/15   nc      Changes to account for new DAL-TF command handler
  05/17/14   jd      Created
================================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/
#include "HWEnginesTest.h"

/* ============================================================================
**  Function : hwenginesInitTestFunc
** ========================================================================== */
/*!
   @brief
   An init DalTF test case.  Add driver hwengines tests into DALTF.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return

*/
static uint32 hwenginesInitTestFunc(uint32 dwArg, char* pszArg[])
{
   AsciiPrint(PRINTSIG"Add hwengines tests\n");

   if(1 == dwArg)
   {
      if(0 == AsciiStrnCmp("all", pszArg[0], 3))
      {
    	  #if 0
		  if ( DAL_SUCCESS != bam_daltf_init())
    	  {
    	    AsciiPrint(PRINTSIG"bam_daltf_init failed\n");
    	    return 1;
    	  }
		  #endif


    	  /*if ( DAL_SUCCESS != tsens_daltf_init())
    	  {
    	     AsciiPrint(PRINTSIG"tsens_daltf_init failed\n");
    	     return 1;
    	  }

    	  if ( DAL_SUCCESS != adc_daltf_init())
    	  {
    	     AsciiPrint(PRINTSIG"adc_daltf_init failed\n");
    	     return 1;
    	  }*/
      }
      else
      {
          return TF_RESULT_CODE_BAD_PARAM;
      }
   }
   else
   {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   return TF_RESULT_CODE_SUCCESS;
}

// hwengines init test's Parameters
static const TF_ParamDescr hwenginesInitTestParam[] =
{
    {TF_PARAM_STRING, "all", "all - add all hwengines test"}
};
// hwengines Init Test's Help Data
static const TF_HelpDescr hwenginesInitTestHelp =
{
    "HWEngines Init",
    sizeof(hwenginesInitTestParam) / sizeof(TF_ParamDescr),
    hwenginesInitTestParam
};


const TF_TestFunction_Descr hwenginesInitTest =
{
   PRINTSIG"HWEnginesInitTest",
   hwenginesInitTestFunc,
   &hwenginesInitTestHelp,
   &context1
};

/* ============================================================================
**  Function : hwenginesDeInitTestFunc
** ========================================================================== */
/*!
   @brief
   A de-init DalTF test case.  Remove driver hwengines tests from DALTF.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return

*/
static uint32 hwenginesDeInitTestFunc(uint32 dwArg, char* pszArg[])
{
   AsciiPrint(PRINTSIG"Remove hwengines tests\n");

   if(1 == dwArg)
   {
      if(0 == AsciiStrnCmp("all", pszArg[0], 3))
      {
    	  #if 0
		  if ( DAL_SUCCESS != bam_daltf_deinit())
    	  {
    	    AsciiPrint(PRINTSIG"bam_daltf_deinit failed\n");
    	    return 1;
    	  }
		  #endif

    	  /*if ( DAL_SUCCESS != tsens_daltf_deinit())
    	  {
    	     AsciiPrint(PRINTSIG"tsens_daltf_deinit failed\n");
    	     return 1;
    	  }

    	  if ( DAL_SUCCESS != adc_daltf_deinit())
    	  {
    	     AsciiPrint(PRINTSIG"adc_daltf_deinit failed\n");
    	     return 1;
    	  }*/

      }
      else
      {
          return TF_RESULT_CODE_BAD_PARAM;
      }
   }
   else
   {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   return TF_RESULT_CODE_SUCCESS;
}

// hwengines deinit test's Parameters
static const TF_ParamDescr hwenginesDeInitTestParam[] =
{
    {TF_PARAM_STRING, "all", "all - remove all hwengines test"}
};
// hwenginesDeInitTest's Help Data
static const TF_HelpDescr hwenginesDeInitTestHelp =
{
    "Sanity De-init",
    sizeof(hwenginesDeInitTestParam) / sizeof(TF_ParamDescr),
    hwenginesDeInitTestParam
};

const TF_TestFunction_Descr hwenginesDeInitTest =
{
   PRINTSIG"HWEnginesDeInitTest",
   hwenginesDeInitTestFunc,
   &hwenginesDeInitTestHelp,
   &context1
};

