/** @file DisplayApp.c
   
  Test application for DisplayDxe graphics output protocol

  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 07/03/12   aus     Replaced DEBUG with AsciiPrint
 07/30/11   sy      Adding TestInterface Library
 02/18/11   ml      Initial revision

=============================================================================*/

/*=========================================================================
      Include Files
==========================================================================*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/QcomLib.h>
#include <Library/QcomBaseLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Protocol/GraphicsOutput.h>
#include <Library/TestInterface.h>
#include <Protocol/EFIDisplayPwrCtrl.h>
#include <Protocol/EFIDisplayPwr.h>
#include "DisplayImage.h"

extern EFI_GUID gMSFTVendorGuid;

/*=========================================================================
      Type Definitions
==========================================================================*/

/*=========================================================================
     Local Function Declarations
==========================================================================*/

/* Blt operation test */
static EFI_STATUS DisplayBltOperationTest(void);
/* Blt Performance test */
static EFI_STATUS DisplayBltPerformanceTest(void);
/* Backlight brightness control test */
static EFI_STATUS DisplayBacklightBrightnessLevelControlTest(void);
/* Backlight brightness status test */
static EFI_STATUS DisplayBacklightBrightnessLevelStatusTest(void);
/* Display panel power control test */
static EFI_STATUS DisplayPanelPowerControlTest(void);
/* Display panel power status test */
static EFI_STATUS DisplayPanelPowerStatusTest(void);
/* Display panel power state test */
static EFI_STATUS DisplayPanelPowerStateTest(void);
/* DisableDisplayToggleVar() */
static EFI_STATUS DisableDisplayToggleVar(UINTN Argc, CHAR8 **Argv);
/* Event based Display power control test */
static EFI_STATUS DisplayPowerEventTest(void);

/*=========================================================================
     Global Functions
==========================================================================*/

/**
  The user entry point for the display application. The user code starts with this function
  as the real entry point for the application.

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.  
  @param[in] SystemTable    A pointer to the EFI System Table.
  
  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurs when executing this entry point.

**/
EFI_STATUS
EFIAPI
DisplayAppMain (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS   eStatus = EFI_SUCCESS;
  UINTN        Argc;
  CHAR8      **Argv;
  
  // Get the command line arguments
  eStatus = GetCmdLineArgs (ImageHandle, &Argc, &Argv);
  if((EFI_SUCCESS == eStatus) && 
     (Argc > 0))
  {
    // User has provided command line arguments
    // Check whether user has requested for toggling DISABLEDISPLAY
    eStatus = DisableDisplayToggleVar(Argc, Argv);
    return eStatus;
  }

  AsciiPrint ("DisplayAppMain: Starting Display Application.\n");

  /* Blt Performance test */
  eStatus = DisplayBltPerformanceTest();
  if (EFI_SUCCESS == eStatus)
  {
    AsciiPrint("Test Status:Success \n \n");
  }
  else 
  {
    AsciiPrint("Test Status:Failure\n \n");
  }

  /* Blt operation test */ 
  eStatus = DisplayBltOperationTest();
  if (EFI_SUCCESS == eStatus)
  {
    AsciiPrint("Test Status:Success \n \n");
  }
  else 
  {
    AsciiPrint("Test Status:Failure\n \n");
  }

  /* Panel power control test */
  eStatus = DisplayPanelPowerControlTest();
  if (EFI_SUCCESS == eStatus)
  {
    AsciiPrint("Test Status:Success \n \n");
  }
  else 
  {
    AsciiPrint("Test Status:Failure\n \n");
  }

  /* Panel power state test */
  eStatus = DisplayPanelPowerStateTest();
  if (EFI_SUCCESS == eStatus)
  {
    AsciiPrint("Test Status:Success \n \n");
  }
  else 
  {
    AsciiPrint("Test Status:Failure\n \n");
  } 

  /* Panel power control status test */
  eStatus = DisplayPanelPowerStatusTest();
  if (EFI_SUCCESS == eStatus)
  {
    AsciiPrint("Test Status:Success \n \n");
  }
  else 
  {
    AsciiPrint("Test Status:Failure\n \n");
  }

  /* Backlight power control test */
  eStatus = DisplayBacklightBrightnessLevelControlTest();
  if (EFI_SUCCESS == eStatus)
  {
    AsciiPrint("Test Status:Success \n \n");
  }
  else 
  {
    AsciiPrint("Test Status:Failure\n \n");
  }

  /* Backlight power status test */
  eStatus = DisplayBacklightBrightnessLevelStatusTest();
  if (EFI_SUCCESS == eStatus)
  {
    AsciiPrint("Test Status:Success \n \n");
  }
  else 
  {
    AsciiPrint("Test Status:Failure\n \n");
  }

  /* Event based power control test*/
  eStatus = DisplayPowerEventTest();
  if (EFI_SUCCESS == eStatus)
  {
    AsciiPrint("Test Status:Success \n \n");
  }
  else 
  {
    AsciiPrint("Test Status:Failure\n \n");
  }

  return eStatus;
}

/*=========================================================================
     Local Functions
==========================================================================*/

/* Backlight brightness level control test */
static EFI_STATUS DisplayBacklightBrightnessLevelControlTest(void)
{
  EFI_STATUS                           eStatus = EFI_SUCCESS;
  EFI_QCOM_DISPLAY_PWR_CTRL_PROTOCOL  *pDisplayPwrCtrlProtocol;
  
  TEST_START("....Backlight brightness level control test....");
  if (EFI_SUCCESS != (eStatus = gBS->LocateProtocol (&gQcomDisplayPwrCtrlProtocolGuid, 
                                                     NULL, 
                                                     (VOID **)&pDisplayPwrCtrlProtocol)))                     
  {
    AsciiPrint ("DisplayBacklightBrightnessLevelControlTest: LocateProtocol() failed.\n");
    return eStatus;
  }
  else
  {
    UINT32            uBackLightLevel = 0;
    EFI_DISPLAY_TYPE  eDisplayType    = EFI_DISPLAY_TYPE_PRIMARY;
    
    AsciiPrint ("DisplayBacklightBrightnessLevelControlTest: Decrease the display backlight...\n");
    /* Decrease the backlight gradually */ 
    for (uBackLightLevel = 100; uBackLightLevel > 0; uBackLightLevel--)
    {
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->DisplayBacklightBrightnessLevelControl(eDisplayType, uBackLightLevel)))
      {
        AsciiPrint ("DisplayBacklightBrightnessLevelControlTest: DisplayBacklightBrightnessLevelControl() failed.\n");
        return eStatus;
      }   
      gBS->Stall(100000); 
    }
    AsciiPrint ("DisplayBacklightBrightnessLevelControlTest: Increase the display backlight...\n");
    /* Increase the backlight gradually */ 
    for (uBackLightLevel = 0; uBackLightLevel <= 100; uBackLightLevel++)
    {
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->DisplayBacklightBrightnessLevelControl(eDisplayType, uBackLightLevel)))
      {
        AsciiPrint ("DisplayBacklightBrightnessLevelControlTest: DisplayBacklightBrightnessLevelControl() failed.\n");
        return eStatus;
      }
      gBS->Stall(100000);
    }         
  }
  TEST_STOP("....Backlight brightness level control test....");
  return eStatus;
}

/* Backlight brightness status test */
static EFI_STATUS DisplayBacklightBrightnessLevelStatusTest(void)
{
  EFI_STATUS                           eStatus = EFI_SUCCESS;
  EFI_QCOM_DISPLAY_PWR_CTRL_PROTOCOL  *pDisplayPwrCtrlProtocol;
  
  TEST_START("....Backlight brightness level status test....");
  if (EFI_SUCCESS != (eStatus = gBS->LocateProtocol (&gQcomDisplayPwrCtrlProtocolGuid, 
                                                     NULL, 
                                                     (VOID **)&pDisplayPwrCtrlProtocol)))                     
  {
    AsciiPrint ("DisplayBacklightBrightnessLevelStatusTest: LocateProtocol() failed.\n");
    return eStatus;
  }
  else
  {
    UINT32            uRetBackLightLevel  = 0;
    UINT32            uBackLightLevel     = 0;
    EFI_DISPLAY_TYPE  eDisplayType        = EFI_DISPLAY_TYPE_PRIMARY;
    
    AsciiPrint ("DisplayBacklightBrightnessLevelStatusTest: Decrease the display backlight...\n");
    /* Decrease the backlight gradually */ 
    for (uBackLightLevel = 0; uBackLightLevel <= 100; uBackLightLevel++)
    {
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->DisplayBacklightBrightnessLevelControl(eDisplayType, uBackLightLevel)))
      {
        AsciiPrint ("DisplayBacklightBrightnessLevelStatusTest: DisplayBacklightBrightnessLevelControl() failed.\n");
        return eStatus;
      }
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->DisplayBacklightBrightnessLevelStatus(eDisplayType, &uRetBackLightLevel)))
      {
        AsciiPrint ("DisplayBacklightBrightnessLevelStatusTest: DisplayBackLightBrightnessLevelStatus() failed.\n");
        return eStatus;
      }
      AsciiPrint ("DisplayBacklightBrightnessLevelStatusTest: Backlight level - %d\n", uRetBackLightLevel);
      gBS->Stall(100000); 
    }
  }
  TEST_STOP("....Backlight brightness level status test....");
  return eStatus;
}

/* Panel power control test */
static EFI_STATUS DisplayPanelPowerControlTest(void)
{
  EFI_STATUS                           eStatus = EFI_SUCCESS;
  EFI_QCOM_DISPLAY_PWR_CTRL_PROTOCOL  *pDisplayPwrCtrlProtocol;
  
  TEST_START("....Panel power control test....");
  if (EFI_SUCCESS != (eStatus = gBS->LocateProtocol (&gQcomDisplayPwrCtrlProtocolGuid, 
                                                     NULL, 
                                                     (VOID **)&pDisplayPwrCtrlProtocol)))                     
  {
    AsciiPrint ("DisplayPanelPowerControlTest: LocateProtocol() failed.\n");
    return eStatus;
  }
  else
  {
    UINT32                         cntr = 0;
    EFI_DISPLAY_TYPE               eDisplayType = EFI_DISPLAY_TYPE_PRIMARY;
    EFI_DISPLAY_POWER_CTRL_STATE   ePowerState  = EFI_DISPLAY_POWER_STATE_OFF;    
    
    for (cntr = 0; cntr < 2; cntr++)
    {
      AsciiPrint ("DisplayPanelPowerControlTest: Display panel power off...\n");
      /* Turn off the display */
      ePowerState  = EFI_DISPLAY_POWER_STATE_OFF;
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->DisplayPanelPowerControl(eDisplayType, ePowerState)))
      {
        AsciiPrint ("DisplayPanelPowerControlTest: DisplayPanelPowerControl() failed.\n");
        return eStatus;
      } 
      gBS->Stall(5000000);
      AsciiPrint ("DisplayPanelPowerControlTest: Display panel power on...\n");
      /* Turn on the display */
      ePowerState  = EFI_DISPLAY_POWER_STATE_ON;    
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->DisplayPanelPowerControl(eDisplayType, ePowerState)))
      {
        AsciiPrint ("DisplayPanelPowerControlTest: DisplayPanelPowerControl() failed.\n");
        return eStatus;
      }
      gBS->Stall(5000000);
    }
  }
  TEST_STOP("....Panel power control test....");
  return eStatus;
}

/* Display panel power status test */
static EFI_STATUS DisplayPanelPowerStatusTest(void)
{
  EFI_STATUS                           eStatus = EFI_SUCCESS;
  EFI_QCOM_DISPLAY_PWR_CTRL_PROTOCOL  *pDisplayPwrCtrlProtocol;
  
  TEST_START("....Panel power status test....");
  if (EFI_SUCCESS != (eStatus = gBS->LocateProtocol (&gQcomDisplayPwrCtrlProtocolGuid, 
                                                     NULL, 
                                                     (VOID **)&pDisplayPwrCtrlProtocol)))                     
  {
    AsciiPrint ("DisplayPanelPowerStatusTest: LocateProtocol() failed.\n");
    return eStatus;
  }
  else
  {
    UINT32                         cntr            = 0;
    EFI_DISPLAY_POWER_CTRL_STATE   uPanelPwrState  = (EFI_DISPLAY_POWER_CTRL_STATE)0;
    EFI_DISPLAY_TYPE               eDisplayType    = EFI_DISPLAY_TYPE_PRIMARY;
    EFI_DISPLAY_POWER_CTRL_STATE   ePowerState     = EFI_DISPLAY_POWER_STATE_OFF;    
    
    for (cntr = 0; cntr < 3; cntr++)
    {
      /* Turn off the display */
      ePowerState  = EFI_DISPLAY_POWER_STATE_OFF;
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->DisplayPanelPowerControl(eDisplayType, ePowerState)))
      {
        AsciiPrint ("DisplayPanelPowerStatusTest: DisplayPanelPowerControl() failed.\n");
        return eStatus;
      } 
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->DisplayPanelPowerStatus(eDisplayType, &uPanelPwrState)))
      {
        AsciiPrint ("DisplayPanelPowerStatusTest: DisplayPanelPowerStatus() failed.\n");
        return eStatus;
      } 
      AsciiPrint ("DisplayPanelPowerStatusTest: Display panel power state - %d\n", uPanelPwrState ? 1 : 0);
      gBS->Stall(5000000);
      /* Turn on the display */
      ePowerState  = EFI_DISPLAY_POWER_STATE_ON;    
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->DisplayPanelPowerControl(eDisplayType, ePowerState)))
      {
        AsciiPrint ("DisplayPanelPowerStatusTest: DisplayPanelPowerControl() failed.\n");
        return eStatus;
      }
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->DisplayPanelPowerStatus(eDisplayType, &uPanelPwrState)))
      {
        AsciiPrint ("DisplayPanelPowerStatusTest: DisplayPanelPowerStatus() failed.\n");
        return eStatus;
      } 
      AsciiPrint ("DisplayPanelPowerStatusTest: Display panel power state - %d\n", uPanelPwrState ? 1 : 0);
      gBS->Stall(5000000);
    }
  }
  TEST_STOP("....Panel power status test....");
  return eStatus;
}

/* BLT operation test */
static EFI_STATUS DisplayBltOperationTest(void)
{
  EFI_STATUS                     eStatus        = EFI_SUCCESS;
  EFI_GUID                       sGfxOutputGUID = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;
  EFI_GRAPHICS_OUTPUT_PROTOCOL  *pGfxProtocol;
  
  TEST_START("....Blt operation test....");
  if (EFI_SUCCESS != (eStatus = gBS->LocateProtocol (&sGfxOutputGUID, 
                                                     NULL, 
                                                     (VOID **)&pGfxProtocol)))                     
  {
    AsciiPrint ("DisplayBltOperationTest: LocateProtocol() failed.\n");
    return eStatus;
  }
  else
  {
    EFI_GRAPHICS_OUTPUT_MODE_INFORMATION   *pModeInfo;
    UINTN                                   nModeInfoSize;
    EFI_GRAPHICS_OUTPUT_BLT_PIXEL           bgPixel;
    UINTN                                   centerX;
    UINTN                                   centerY;
    UINT32                                  mode = 0;
    
    if (EFI_SUCCESS != (eStatus = pGfxProtocol->SetMode(pGfxProtocol, mode)))
    {
      AsciiPrint ("DisplayBltOperationTest: SetMode() failed.\n");
      return eStatus;
    }
    AsciiPrint ("DisplayBltOperationTest: Set mode - %d\n", mode);
    if (EFI_SUCCESS != (eStatus = pGfxProtocol->QueryMode(pGfxProtocol, 0, &nModeInfoSize, &pModeInfo)))
    {
      AsciiPrint ("DisplayBltOperationTest: QueryMode() failed.\n");
      return eStatus;
    }
    AsciiPrint ("DisplayBltOperationTest: Query mode - %dx%d\n", pModeInfo->HorizontalResolution, pModeInfo->VerticalResolution);
    /* Fill background as white */
    bgPixel.Blue     = 0xFF;
    bgPixel.Green    = 0xFF;
    bgPixel.Red      = 0xFF;
    bgPixel.Reserved = 0xFF;
    
    AsciiPrint ("DisplayBltOperationTest: Blt(EfiBltVideoFill)\n");
    if (EFI_SUCCESS != (eStatus = pGfxProtocol->Blt(pGfxProtocol, 
                                                    (EFI_GRAPHICS_OUTPUT_BLT_PIXEL*)&bgPixel,
                                                    EfiBltVideoFill,
                                                    0, 0,
                                                    0, 0, 
                                                    pModeInfo->HorizontalResolution,
                                                    pModeInfo->VerticalResolution,
                                                    0)))
    {
      AsciiPrint ("DisplayBltOperationTest: Blt(EfiBltVideoFill) failed.\n");
      return eStatus;
    }
    gBS->Stall(5000000);
    /* Copy image to center */
    centerX = (pModeInfo->HorizontalResolution>>1) - (BITMAP_IMAGE_WIDTH>>1);
    centerY = (pModeInfo->VerticalResolution>>1) - (BITMAP_IMAGE_HEIGHT>>1);
    AsciiPrint ("DisplayBltOperationTest: Blt(EfiBltBufferToVideo)\n");
    if (EFI_SUCCESS != (eStatus = pGfxProtocol->Blt(pGfxProtocol, 
                                                    (EFI_GRAPHICS_OUTPUT_BLT_PIXEL*)bitmap_image,
                                                    EfiBltBufferToVideo,
                                                    0, 0,
                                                    centerX, centerY,
                                                    BITMAP_IMAGE_WIDTH,
                                                    BITMAP_IMAGE_HEIGHT,
                                                    0)))
    {
      AsciiPrint ("DisplayBltOperationTest: Blt(EfiBltBufferToVideo) failed.\n");
      return eStatus;
    }
    gBS->Stall(5000000);
    /* Copy image to top left */
    AsciiPrint ("DisplayBltOperationTest: Blt(EfiBltVideoToVideo)\n");
    if (EFI_SUCCESS != (eStatus = pGfxProtocol->Blt(pGfxProtocol, 
                                                    NULL,
                                                    EfiBltVideoToVideo,
                                                    centerX, centerY,
                                                    0, 0,                      
                                                    BITMAP_IMAGE_WIDTH,
                                                    BITMAP_IMAGE_HEIGHT,
                                                    0)))
    {
      AsciiPrint ("DisplayBltOperationTest: Blt(EfiBltVideoToVideo) failed.\n");
      return eStatus;
    }
    gBS->Stall(5000000);
  }
  TEST_STOP("....Blt operation test....");
  return eStatus; 
}
  
/* Panel power state test */
static EFI_STATUS DisplayPanelPowerStateTest(void)
{
  EFI_STATUS                           eStatus = EFI_SUCCESS;
  EFI_DISPLAY_POWER_PROTOCOL          *pDisplayPwrCtrlProtocol;
  
  TEST_START("....Panel power state test....");
  if (EFI_SUCCESS != (eStatus = gBS->LocateProtocol (&gEfiDisplayPowerStateProtocolGuid, 
                                                     NULL, 
                                                     (VOID **)&pDisplayPwrCtrlProtocol)))                     
  {
    AsciiPrint ("DisplayPanelPowerStateTest: LocateProtocol() failed.\n");
    return eStatus;
  }
  else
  {
    UINT32                         cntr = 0;
    EFI_DISPLAY_POWER_STATE        ePowerState  = EfiDisplayPowerStateOff;    
    
    for (cntr = 0; cntr < 2; cntr++)
    {
      AsciiPrint ("DisplayPanelPowerStateTest: Display panel power off...\n");
      /* Turn off the display */
      ePowerState  = EfiDisplayPowerStateOff;
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->SetDisplayPowerState(NULL, ePowerState)))
      {
        AsciiPrint ("DisplayPanelPowerStateTest: SetDisplayPowerState() failed.\n");
        return eStatus;
      } 
      /* Retrieve the power status */ 
      ePowerState = EfiDisplayPowerStateUnknown;
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->GetDisplayPowerState(NULL, &ePowerState)))
      {
        AsciiPrint ("DisplayPanelPowerStateTest: GetDisplayPowerState() failed.\n");
        return eStatus;
      } 
      AsciiPrint ("DisplayPanelPowerStateTest: Display panel power status : %d\n", ePowerState);
      gBS->Stall(5000000);
      AsciiPrint ("DisplayPanelPowerStateTest: Display panel power on...\n");
      /* Turn on the display and backlight */
      ePowerState  = EfiDisplayPowerStateMaximum;   
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->SetDisplayPowerState(NULL, ePowerState)))
      {
        AsciiPrint ("DisplayPanelPowerStateTest: SetDisplayPowerState() failed.\n");
        return eStatus;
      }
      /* Retrieve the power status */ 
      ePowerState = EfiDisplayPowerStateUnknown;
      if (EFI_SUCCESS != (eStatus = pDisplayPwrCtrlProtocol->GetDisplayPowerState(NULL, &ePowerState)))
      {
        AsciiPrint ("DisplayPanelPowerStateTest: GetDisplayPowerState() failed.\n");
        return eStatus;
      } 
      AsciiPrint ("DisplayPanelPowerStateTest: Display panel power status : %d\n", ePowerState);
      gBS->Stall(5000000);
    }
  }
  TEST_STOP("....Panel power state test....");
  return eStatus;
}

/* DisableDisplayToggleVar()
   Toggles DISABLEDISPLAY variable
 */
static EFI_STATUS DisableDisplayToggleVar(UINTN Argc, CHAR8 **Argv)
{
  EFI_STATUS eStatus = EFI_SUCCESS;
  UINT8      uValue  = 0;

  if(0 == AsciiStriCmp(Argv[0], "disabledisplay"))
  {
    uValue = 1;
  }
  else if(0 == AsciiStriCmp(Argv[0], "enabledisplay"))
  {
    uValue = 0;
  }
  else
  {
    eStatus = EFI_INVALID_PARAMETER;
  }
  if(EFI_SUCCESS == eStatus)
  {
    const CHAR16  sDisableDisplay[]  = L"DISABLEDISPLAY";
    // Set attributes = (EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS),
    // which makes sure that the data is persistant after power cycles.
    if (EFI_SUCCESS == (eStatus = gRT->SetVariable((CHAR16*)&sDisableDisplay, 
                                                    &gMSFTVendorGuid,
                                                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                                                    sizeof(uint8),
                                                    (void*)&uValue )))
    {
      AsciiPrint("DisableDisplayToggleVar: Setting DISABLEDISPLAY to %d \n", uValue);
    }
  }
  if(EFI_SUCCESS != eStatus)
  {
    AsciiPrint ("DisableDisplayToggleVar: Setting DISABLEDISPLAY failed. \n");
    AsciiPrint ("Format to set DISABLEDISPLAY: \n");
    AsciiPrint ("    start DispTest disabledisplay \n");
    AsciiPrint ("Format to clear DISABLEDISPLAY: \n");
    AsciiPrint ("    start DispTest enabledisplay \n");
  }
  return eStatus;
}


/**
Performance test for Blt operation. 5KB, 100KB and 1MB blocks of data are copied from Buffer to Video, Video to Video and Blt Fill.

**/
static EFI_STATUS DisplayBltPerformanceTest(void)
{
  EFI_STATUS                     eStatus        = EFI_SUCCESS;
  UINT32                         uStartTime;
  UINT32                         uEndTime;
  UINT32                         uTotalFuncTime = 0; 
  UINT32                         perfno;  
  EFI_GUID                       sGfxOutputGUID = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;
  EFI_GRAPHICS_OUTPUT_PROTOCOL  *pGfxProtocol;
  
  TEST_START("....Blt Performance test....");
  if (EFI_SUCCESS != (eStatus = gBS->LocateProtocol (&sGfxOutputGUID, 
                                                     NULL, 
                                                     (VOID **)&pGfxProtocol)))                     
  {
    AsciiPrint ("DisplayBltOperationTest: LocateProtocol() failed.\n");
  }
  else
  {
    EFI_GRAPHICS_OUTPUT_MODE_INFORMATION   *pModeInfo;
    UINTN                                   nModeInfoSize;
    EFI_GRAPHICS_OUTPUT_BLT_PIXEL           bgPixel;
    UINT32                                 *BltBuffer = NULL;
    UINTN                                   BufferLen;
    UINT32                                  mode = 0;
    
    if (EFI_SUCCESS != (eStatus = pGfxProtocol->SetMode(pGfxProtocol, mode)))
    {
      AsciiPrint("DisplayBltOperationTest: SetMode() failed.\n");
    }
    else if (EFI_SUCCESS != (eStatus = pGfxProtocol->QueryMode(pGfxProtocol, 0, &nModeInfoSize, &pModeInfo)))
    {
      AsciiPrint("DisplayBltOperationTest: QueryMode() failed.\n");
    }
    else
    {
      UINT32 blocksize[] = {5, 100, 1000};  //3 data blocks of size 5, 100 and 1000 KB.
      UINT32 iter;
      
      for (iter = 0; iter < (sizeof (blocksize)/sizeof (blocksize[0])); iter++)
      {
        UINT32 hori  = (UINT32)pModeInfo->HorizontalResolution;
        UINT32 verti = (blocksize[iter] * 1024) / (hori * 4);
        
        if (verti > pModeInfo->VerticalResolution)
        {
          verti = pModeInfo->VerticalResolution;
        }
        
        // Video Fill - fill background white
        bgPixel.Blue     = 0xFF;
        bgPixel.Green    = 0xFF;
        bgPixel.Red      = 0xFF;
        bgPixel.Reserved = 0xFF;
        UINT32 loopv;
          
        // Start Time   
        uStartTime = GetTimerCountms();
        for (loopv = 0; loopv < 10000; loopv++)
        {
          if (EFI_SUCCESS != (eStatus = pGfxProtocol->Blt(pGfxProtocol,
                                                          (EFI_GRAPHICS_OUTPUT_BLT_PIXEL*)&bgPixel,
                                                           EfiBltVideoFill,
                                                           0, 0,
                                                           0, 0,
                                                           pModeInfo->HorizontalResolution,
                                                           verti,
                                                           0)))
          {
            break;
          }
        }
        if (eStatus == EFI_SUCCESS)
        {
          // End time
          uEndTime = GetTimerCountms();
          //Function execution time(ms)
          uTotalFuncTime = (uEndTime - uStartTime);
          AsciiPrint("Performance numbers for %d KB data block: \n", blocksize[iter]);
          if (uTotalFuncTime == 0)
          {
            AsciiPrint("Total time taken for BltVideoFill(%d KB): 0ms\n", blocksize[iter]);
          }
          else
          {
            perfno = (blocksize[iter] * 1000.0) / (uTotalFuncTime);
            if (perfno >= 1024)
            {
              perfno = perfno / 1024;
              AsciiPrint("BltVideoFill:     %dMB/s \n", perfno);
            }
            else
            {
              AsciiPrint("BltVideoFill:     %dKB/s \n", perfno);
            }
          }
        }
        else
        {
          AsciiPrint("DisplayBltOperationTest: Blt(EfiBltVideoFill) %d KB failed. \n", blocksize[iter]);
          break;
        }
        
        // Buffer to Video
        BufferLen = hori * verti* sizeof(EFI_GRAPHICS_OUTPUT_BLT_PIXEL);
        eStatus = gBS->AllocatePool(EfiBootServicesData, (BufferLen), (VOID**)&BltBuffer);
        if ((eStatus != EFI_SUCCESS) || (BltBuffer == NULL))
        {
          eStatus = !EFI_SUCCESS;
          break;
        }
        else
        {
          //Zero out the buffer to make it black
          ZeroMem(BltBuffer, (BufferLen));
          // Start Time
          uStartTime = GetTimerCountms();
          for (loopv = 0; loopv < 10000; loopv++)
          {
          if (EFI_SUCCESS != (eStatus = pGfxProtocol->Blt(pGfxProtocol,
                                                           (EFI_GRAPHICS_OUTPUT_BLT_PIXEL*)BltBuffer,
                                                            EfiBltBufferToVideo,
                                                            0, 0,
                                                            0, 0,
                                                            pModeInfo->HorizontalResolution,
                                                            verti,
                                                            pModeInfo->HorizontalResolution * sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL))))
            {
              break;
            }               
          }
          gBS->FreePool (BltBuffer);
          if (EFI_SUCCESS == eStatus)
          {
            // End time
            uEndTime = GetTimerCountms();
            //Function execution time(ms)
            uTotalFuncTime = (uEndTime - uStartTime);
            if (uTotalFuncTime == 0)
            {
              AsciiPrint("Total time taken for BltBufferToVideo(%d KB): 0ms\n", blocksize[iter]);
            }
            else
            {
              perfno = (blocksize[iter] * 1000) / (uTotalFuncTime);
              if (perfno >= 1024)
              {
                perfno = perfno / 1024;
                AsciiPrint("BltBufferToVideo: %dMB/s \n", perfno);
              }
              else
              {
                AsciiPrint("BltBufferToVideo: %dKB/s \n", perfno);
              }
            }
          }  
          else
          {
            AsciiPrint("DisplayBltOperationTest: Blt(EfiBltBufferToVideo) %d KB failed. \n", blocksize[iter]);
            break;
          }           
            
          // Video to Video copy 
          // Start Time
          uStartTime = GetTimerCountms();
          for (loopv = 0; loopv < 10000; loopv++)
          {
            if (EFI_SUCCESS != (eStatus = pGfxProtocol->Blt(pGfxProtocol,NULL,
                                                            EfiBltVideoToVideo,
                                                            0, 0,
                                                            0, 120,
                                                            pModeInfo->HorizontalResolution,
                                                            verti,
                                                            0)))
            {
              break;
            }
          }
          if (EFI_SUCCESS == eStatus)
          {
            // End time
            uEndTime = GetTimerCountms();
            //Function execution time(ms)
            uTotalFuncTime = (uEndTime - uStartTime);
            if (uTotalFuncTime == 0)
            {
              AsciiPrint("Total time taken for BltVideoToVideo(%d KB): 0ms\n", blocksize[iter]);
            }
            else
            {
              perfno = (blocksize[iter] * 1000.0) / (uTotalFuncTime);
              if (perfno >= 1024)
              {
                perfno = perfno / 1024;
                AsciiPrint("BltVideoToVideo:  %dMB/s \n", perfno);
              }
              else
              {
                AsciiPrint("BltVideoToVideo:  %dKB/s \n", perfno);
              }
            }
          }
          else
          {
            AsciiPrint("DisplayBltOperationTest: Blt(EfiBltVideoToVideo) %d KB failed. \n", blocksize[iter]);
          }
        }
      }
    }
  }
  TEST_STOP("....Blt Performance test....");
  return eStatus; 
}

/* Dummy Power Event handle function needed to create local events for UI_Active/UI_Idle*/
VOID EFIAPI DummyEventHandle( IN EFI_EVENT  Event, IN VOID *Context)
{
  /* Dummy Power Event handle function, do nothing here*/
}
/* Event based display power control test.
*  1. Create UI_Active and UI_Idle events based on the event GUID.
*  2. Signal UI_Idle event, display is expected to be off, all power rails, PLL, clock, GPIO should be turned off
*  3. Signal UI_Acitve event, display is expected to be on, all power rails, PLL, clock, GPIO should be turned on
*  4. Repeat step 2 & 3 one more time
*/
static EFI_STATUS DisplayPowerEventTest(void)
{
  EFI_STATUS    eStatus            = EFI_SUCCESS;
  EFI_GUID      UIActiveEventGuid  = EFI_UI_ACTIVE_EVENT_GUID;
  EFI_GUID      UIIdleEventGuid    = EFI_UI_IDLE_EVENT_GUID;
  EFI_EVENT     UIActiveEvent      = NULL;
  EFI_EVENT     UIIdleEvent        = NULL;

  TEST_START("....Event based Display Power Control Test....");

  //Create UI_Acitve/UI_Idle events
  if (EFI_SUCCESS != (eStatus = gBS->CreateEventEx(EVT_NOTIFY_SIGNAL, TPL_CALLBACK, DummyEventHandle, NULL, &UIActiveEventGuid, &UIActiveEvent)))
  {
    AsciiPrint("DisplayPowerEventTest: Failed to create event UI_Active\n");
  }
  else if (EFI_SUCCESS != (eStatus = gBS->CreateEventEx(EVT_NOTIFY_SIGNAL, TPL_CALLBACK, DummyEventHandle, NULL, &UIIdleEventGuid, &UIIdleEvent)))
  {
    AsciiPrint("DisplayPowerEventTest: Failed to create event UI_Idle\n");
  }
  else
  {
    UINT32                         cntr = 0;

    //Loop to turn of and on display multiply times.
    for (cntr = 0; cntr < 2; cntr++)
    {
      /* Turn off the display */
      AsciiPrint("DisplayPowerEventTest: Display panel power off...\n");
      if (EFI_SUCCESS != (eStatus = gBS->SignalEvent(UIIdleEvent)))
      {
        AsciiPrint("DisplayPowerEventTest: failed signal event UI_Idle.\n");
        break;
      }
      gBS->Stall(5000000);

      /* Turn on the display */
      AsciiPrint("DisplayPowerEventTest: Display panel power on...\n");
      if (EFI_SUCCESS != (eStatus = gBS->SignalEvent(UIActiveEvent)))
      {
        AsciiPrint("DisplayPowerEventTest: failed signal event UI_Active.\n");
        break;
      }
      gBS->Stall(5000000);
    }
  }

  //Close Event handles
  if (NULL != UIActiveEvent)
  {
    if (EFI_SUCCESS != (eStatus = gBS->CloseEvent(UIActiveEvent)))
    {
      AsciiPrint("DisplayPowerEventTest: Failed to Close event UI_Active\n");
    }
  }
  if (NULL != UIIdleEvent)
  {
    if (EFI_SUCCESS != (eStatus = gBS->CloseEvent(UIIdleEvent)))
    {
      AsciiPrint("DisplayPowerEventTest: Failed to Close event UI_Idle\n");
    }
  }

  TEST_STOP("....Event based Display Power Control Test....");

  return eStatus;
}
