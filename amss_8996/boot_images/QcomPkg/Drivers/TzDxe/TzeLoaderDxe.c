/** @file TzeLoaderDxe.c
  This file implements load

  Copyright (c) 2011-2015, Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
   
**/

/*=============================================================================
                              EDIT HISTORY


 when       who      what, where, why
 --------   ---      ----------------------------------------------------------
 04/24/15   sm       Fixed incorrect parameter in LoadImageFromBuffer
 04/23/15   vk       Add UFS support
 03/16/15   sm       Update filname and add better error messages
 03/02/15   vk       LoadFileFromPartition signature update
 02/26/15   vk       Update filename
 12/05/14   vk       Add log message
 12/03/14   sk       Added support for config flags
 10/16/14   vk       LoadFileFromPartition signature update
 08/15/14   sm       Switched to new SCM API
 07/08/14   sk       Fixed KW null-check error
 04/17/14   sm       Removed debug variable
 03/18/14   sm       Updated some types and standardized typecasting
 12/27/13   sp       Updated to new fields of TPMControlAreaMemRegionInfo
 08/01/13   wuc      Load cmnlib and dxhdcp2
 07/25/13   shl      Changed printing level.
 02/21/13   shl      ScmDxe cleanup.
 01/24/13   ag       Use SCMDxe to load apps  
 01/10/13   niting   Updated loading of TZ Apps
 12/14/12   ag       Load TzApps at UEFI specified region
 12/13/12   ag       TzVersion issue
 12/05/12   shl      Reorg initializations, moved them inot TzDxe.c
 12/04/12   ag       Fix warnings
 11/08/12   shl      Added loading SSD key store
 05/17/12   ag       Tz1.4 related app loading changes
 04/03/12   jz       Enable UncachedFreePool call, cleanup
 03/07/12   rs       Added intialization of UEFI Encryption Protocol
 03/06/12   jz       Renamed PcdTZSharedParamsBaseOffset to PcdTZScmCmdBufferBaseOffset
 11/23/11   jz       Memory map PCDs cleanup
 08/15/11   yg       Created new file
=============================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/

#include <Library/BaseMemoryLib.h>
#include <Library/UncachedMemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/EfiFileLib.h>
#include <Library/ArmLib.h>
#include <Library/PcdLib.h>
#include <Guid/FileSystemVolumeLabelInfo.h>
#include "QcomLib.h"
#include <Protocol/EFITzeLoader.h>
#include <Library/QcomBaseLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Protocol/EFIScm.h>
#include <Include/scm_sip_interface.h>
#include <Include/scm_qsee_interface.h>
#include <Library/UefiCfgLib.h>
#include <Library/BootConfig.h>

QCOM_SCM_PROTOCOL *QcomScmProtocol = NULL;

#define TZBSP_computed_VERSION(major, minor, patch) \
  (((major & 0x3FF) << 22) | ((minor & 0x3FF) << 12) | (patch & 0xFFF))


//Fat16 partition; @TODO: move all this once tested
STATIC EFI_GUID TzExecPartitionType = 
  { 0x69B4201F, 0xA5AD, 0x45EB, { 0x9F, 0x49, 0x45, 0xB3, 0x8C, 0xCD, 0xAE, 0xF5 } };

STATIC EFI_GUID TzAppsPartitionType = 
  { 0x14D11C40, 0x2A3D, 0x4F97, { 0x88, 0x2D, 0x10, 0x3A, 0x1E, 0xC0, 0x93, 0x33 } };
 
#define VARIABLE_SERVICES_MBN_FILE           L"\\TZAPPS\\uefisec.mbn" 
#define DXHDCP2_MBN_FILE                     L"\\TZAPPS\\dxhdcp2.mbn"
#define COMMON_LIB_MBN_FILE                  L"\\TZAPPS\\cmnlib.mbn"
#define WINSECAPP_MBN_FILE                   L"\\TZAPPS\\winsecap.mbn" 


#define TZ_FVER_QSEE                         10 /**< QSEE application layer. */


/* Ideally we should not find more than 1 handle */
HandleInfo HandleInfoList[4];
UINT32 MaxHandles;
PartiSelectFilter HandleFilter;

extern EFI_GUID gEfiEmmcGppPartition1Guid;
extern EFI_GUID gEfiEmmcUserPartitionGuid;
extern EFI_GUID gEfiUfsLU4Guid;            //TZAPPS

extern EFI_STATUS EFIAPI GetMemRegionInfoByName (IN CHAR8* RegionName, IN OUT MemRegionInfo* MemoryRegion);

UINT32*          UncachedMem;
UINTN            UncachedMemSize;
#define          UNCACHED_MEM_DEFAULT_SIZE     (320*1024)
static BOOLEAN TZSecureAppLoadCalled;
static CHAR8* MemLabel = "TZ Apps";

/*------------------------------------------------------------------------
                              PUBLIC METHODS
------------------------------------------------------------------------*/

EFI_STATUS
LoadImageFromPartition(
    IN  EFI_TZE_LOADER_PROTOCOL        *This,
    IN  EFI_GUID                       *pPartitionGuid,
    OUT UINT32                         *AppIdPtr
 );

EFI_STATUS
LoadImageFromFile(
      IN  EFI_TZE_LOADER_PROTOCOL      *This,
      IN  CHAR8*                        FilePath,
      OUT UINT32                       *AppIdPtr
    );

EFI_STATUS
LoadImageFromBuffer(
      IN  EFI_TZE_LOADER_PROTOCOL     *This,
      IN  UINT32                      *Buffer,
      IN  UINTN                        BufferSize,
      OUT UINT32                      *AppIdPtr
    );

EFI_STATUS
LoadImageFromFileSystem(
   IN  EFI_TZE_LOADER_PROTOCOL        *This,
   IN  EFI_GUID                       *pPartitionGuid,
   CHAR16                             *Path,
   OUT UINT32                         *AppIdPtr

);

EFI_STATUS
LoadSecureApps(
   IN  EFI_TZE_LOADER_PROTOCOL        *This,
   OUT UINT32                         *AppIdPtr
);

static EFI_TZE_LOADER_PROTOCOL TzeLoaderProtocol = 
{
  EFI_TZE_LOADER_PROTOCOL_REVISION,
  LoadImageFromPartition,
  LoadImageFromFile,
  LoadImageFromBuffer,
  LoadImageFromFileSystem,
  LoadSecureApps
};
/*
* Load app using Tz1.4 qsee scm command
*/
static UINT32 
QseeAppStartSyscall (
  UINTN mdt_len,
  UINTN img_len,
  VOID *pa,
  CHAR8 img_name[QSEE_MAX_NAME_SIZE])
{
   qsee_app_start_req_t *qsee_syscallp;
   UINT32         app_id = 0;
   EFI_STATUS     Status;

   qsee_syscallp= (qsee_app_start_req_t *)AllocatePool(sizeof(qsee_app_start_req_t));
   if (NULL == qsee_syscallp)
   {
      return app_id;
   }

   SetMem(qsee_syscallp, sizeof(qsee_app_start_req_t), 0x0);
   qsee_syscallp->mdt_len = (UINT32)mdt_len;
   qsee_syscallp->img_len = (UINT32)img_len;
   qsee_syscallp->pa = (UINT32)(UINTN)pa;
   CopyMem((VOID *)qsee_syscallp->app_name, img_name, QSEE_MAX_NAME_SIZE); 

   Status = QcomScmProtocol->ScmSendCommand(QcomScmProtocol,
                                            APP_START_APP_CMD,
                                            &(app_id),
                                            qsee_syscallp,
                                            sizeof(qsee_app_start_req_t),
                                            NULL,
                                            0);
  if (Status != EFI_SUCCESS)
   { 
     app_id = 0;
   }
  FreePool(qsee_syscallp);
  return app_id;
}

/* 
*  
* Load Services MBN using Tz2.0 qsee scm command
*/
static EFI_STATUS
QseeLoadServiceImageSyscall (
  UINT32 mdt_len,
  UINT32 img_len,
  VOID   *pa
  )
{
   qsee_load_serv_image_t *qsee_syscallp;
   UINT32                  app_id = 0;
   EFI_STATUS              Status;

   qsee_syscallp= (qsee_load_serv_image_t *)AllocatePool(sizeof(qsee_load_serv_image_t));
   if (NULL == qsee_syscallp)
   {
      return EFI_OUT_OF_RESOURCES;
   }
   SetMem(qsee_syscallp, sizeof(qsee_load_serv_image_t), 0x0);
   qsee_syscallp->mdt_len = mdt_len;
   qsee_syscallp->img_len = img_len;
   qsee_syscallp->pa = (UINT32)(UINTN)pa;

   Status = QcomScmProtocol->ScmSendCommand(QcomScmProtocol,
                                            APP_LOAD_SERV_IMAGE_CMD,
                                            &(app_id),
                                            qsee_syscallp,
                                            sizeof(qsee_load_serv_image_t),
                                            NULL,
                                            0);

   FreePool(qsee_syscallp);
   return Status;
}

/*Adding syscall to get TZ version number
 * WIll send a syscall to TZ and find out the current TZ version number.
 * Tz1.3 will return an error as this functionality is not implemented
 */


static EFI_STATUS
TzGetFeatureVersion (
  UINT32  FeatureId,
  UINT32 *Version
  )
{
  EFI_STATUS                Status;
  UINT64                    Parameters[SCM_MAX_NUM_PARAMETERS] = {0};
  UINT64                    Results[SCM_MAX_NUM_RESULTS] = {0};
  tz_feature_version_req_t *SysCallReq = (tz_feature_version_req_t*)Parameters;
  tz_feature_version_rsp_t *SysCallRsp = (tz_feature_version_rsp_t*)Results;

  SysCallReq->feature_id = FeatureId;

  /* Make a SCM Sys call */
  Status = QcomScmProtocol->ScmSipSysCall (QcomScmProtocol,
                                           TZ_INFO_GET_FEATURE_VERSION_ID,
                                           TZ_INFO_GET_FEATURE_VERSION_ID_PARAM_ID,
                                           Parameters,
                                           Results
                                           );
  if (EFI_ERROR (Status)) 
  {
    DEBUG(( EFI_D_ERROR, "ScmSipSysCall() failed, Status = (0x%x)\r\n", Status));
    goto ErrorExit;
  }
  if (SysCallRsp->common_rsp.status != 1)
  {
    Status = EFI_DEVICE_ERROR;
    DEBUG(( EFI_D_ERROR, "TZ_INFO_GET_FEATURE_VERSION_ID failed, Status = (0x%x)\r\n", SysCallRsp->common_rsp.status));
    goto ErrorExit;
  }

  *Version = SysCallRsp->version;

ErrorExit:
  return Status;
}

/*
* Notify TZ where to load QSEE sec apps
*/
static EFI_STATUS
QseeAppsRegionNotificationSyscall (
  UINT64 applications_region_addr,
  UINT64 applications_region_size
  )
{
   qsee_apps_region_notification_t *qsee_syscallp;
   UINT32 app_id = 0;
   EFI_STATUS              Status;

   qsee_syscallp= (qsee_apps_region_notification_t*)AllocatePool(sizeof(qsee_apps_region_notification_t));
   if (NULL == qsee_syscallp)
   {
      return EFI_OUT_OF_RESOURCES;
   }
   SetMem(qsee_syscallp, sizeof(qsee_apps_region_notification_t), 0x0);
   qsee_syscallp->applications_region_addr = (UINT32)applications_region_addr;
   qsee_syscallp->applications_region_size = (UINT32)applications_region_size;

   Status = QcomScmProtocol->ScmSendCommand(QcomScmProtocol,
                                            APP_REGION_NOTIFICATION_CMD,
                                            &(app_id),
                                            qsee_syscallp,
                                            sizeof(qsee_apps_region_notification_t),
                                            NULL,
                                            0);

   FreePool(qsee_syscallp);
   return Status;
}


/**
  PIL Loader's Entry point
  
  @param  ImageHandle       EFI_HANDLE.
  @param  SystemTable       EFI_SYSTEM_TABLE.

  @return EFI_SUCCESS       Success.
**/
EFI_STATUS
EFIAPI
TzeLoaderProtocolInit (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE    *SystemTable
  )
{
  EFI_HANDLE              handle = NULL;
  EFI_STATUS              Status;

  UncachedMem = (UINT32*)UncachedAllocatePool (UNCACHED_MEM_DEFAULT_SIZE);
  if (UncachedMem == NULL)
    return EFI_OUT_OF_RESOURCES;

  UncachedMemSize =UNCACHED_MEM_DEFAULT_SIZE;

  Status = gBS->InstallMultipleProtocolInterfaces(
                              &handle, 
                              &gEfiTzeLoaderProtocolGuid, (void **)&TzeLoaderProtocol,
                              NULL);

  if (Status != EFI_SUCCESS)
  {
    return Status;
  }
  TZSecureAppLoadCalled = FALSE;

  DEBUG((EFI_D_INFO, "Attempting to load SCM protocol\n"));
  Status = gBS->LocateProtocol (&gQcomScmProtocolGuid, NULL, (VOID**)&QcomScmProtocol);
  ASSERT_EFI_ERROR(Status);

  Status = LoadSecureApps(&TzeLoaderProtocol, NULL);

  ASSERT_EFI_ERROR(Status);

  return Status;
}

EFI_STATUS
LoadImageFromBuffer(
      IN  EFI_TZE_LOADER_PROTOCOL     *This,
      IN  UINT32                      *Buffer,
      IN  UINTN                        BufferSize,
      OUT UINT32                      *AppIdPtr
    )
{

  EFI_STATUS          Status;
  UINT32             *MemLoc;
  UINT32              AppId = 0;


  if ((Buffer == NULL) || (BufferSize == 0))
    return EFI_INVALID_PARAMETER;

    //TZ1.4 App loading
      //Allocate uncahed buffer
    MemLoc = UncachedAllocatePool (BufferSize);
    if (MemLoc == NULL)
      return EFI_OUT_OF_RESOURCES;

    CopyMem(MemLoc, Buffer, BufferSize);

    AppId = QseeAppStartSyscall(0, BufferSize, MemLoc, "");
    UncachedFreePool (MemLoc);

  if (AppIdPtr)
     *AppIdPtr = AppId;

  if ((AppId == 0) || (AppId == 0xFFFFFFFF))   
      Status = EFI_LOAD_ERROR;
  else
      Status = EFI_SUCCESS;

  return Status;
  
}

EFI_STATUS
LoadImageFromFile(
      IN  EFI_TZE_LOADER_PROTOCOL      *This,
      IN  CHAR8*                       FilePath,
      OUT UINT32                       *AppIdPtr
    )
{
  EFI_STATUS        Status;
  EFI_OPEN_FILE     *File;
  VOID              *Address;
  UINTN             Size;

  if (FilePath == NULL)
    return EFI_INVALID_PARAMETER;

  File = EfiOpen (FilePath, EFI_FILE_MODE_READ, 0);
  if (File == NULL)
    return EFI_LOAD_ERROR;

  Status = EfiReadAllocatePool (File, &Address, &Size);
  if (EFI_ERROR (Status))
  {
    EfiClose (File);
    return Status;
  }

   Status= LoadImageFromBuffer(This, Address, Size, AppIdPtr);

  gBS->FreePool (Address);
  EfiClose (File);

  return Status;
}


EFI_STATUS
LoadImageFromPartition(
    IN  EFI_TZE_LOADER_PROTOCOL        *This,
    IN  EFI_GUID                       *pPartitionGuid,
    OUT UINT32                         *AppIdPtr
    )
{
  UINT32                  Attribs = 0;
  EFI_STATUS              Status = EFI_SUCCESS;
  UINT32                  *MemLoc;
  EFI_BLOCK_IO_PROTOCOL   *BlkIo;
  UINTN                   ImgSize;
  UINT32                  AppId = 0;
  CHAR8                   img_name[QSEE_MAX_NAME_SIZE] = {0};

  /* Select the BlkIo handle that represents the partition by the
   * referenced GUID type in GPT partition on Non removable media
   * ie eMMC device */
  Attribs |= BLK_IO_SEL_MEDIA_TYPE_NON_REMOVABLE;
  Attribs |= BLK_IO_SEL_PARTITIONED_GPT;
  Attribs |= BLK_IO_SEL_MATCH_PARTITION_TYPE_GUID;

  MaxHandles = sizeof(HandleInfoList)/sizeof(*HandleInfoList);
  HandleFilter.PartitionType = pPartitionGuid;
  HandleFilter.VolumeName = 0;

  Status = GetBlkIOHandles (Attribs, &HandleFilter, HandleInfoList, &MaxHandles);

  if (Status != EFI_SUCCESS)
    return Status;

  /* Having more than one partition is considered ambiguity, so return
   * invalid param */
  if (MaxHandles > 1)
    return EFI_INVALID_PARAMETER;

  if (MaxHandles == 0)
  {
    DEBUG ((EFI_D_INFO, "PIL Image partition not found\n"));
    return EFI_NOT_FOUND;
  }

  /* Get the Block IO protocol instance from the handle */
  BlkIo = HandleInfoList[0].BlkIo;

  /* Get the partition size and round it up to EFI_PAGE_SIZE */
  ImgSize = BlkIo->Media->LastBlock * BlkIo->Media->BlockSize;
  
  if (ImgSize > UncachedMemSize)
  {
     UncachedFreePool (UncachedMem);
     UncachedMem = UncachedAllocatePool (ImgSize);
     if (UncachedMem == NULL)
       return EFI_OUT_OF_RESOURCES;
     UncachedMemSize = ImgSize;
  }
  MemLoc = (UINT32*)UncachedMem;
  
  do
  {
    Status = BlkIo->ReadBlocks (BlkIo, BlkIo->Media->MediaId, 0, ImgSize, MemLoc);
    if (EFI_ERROR (Status))
      break;

      AppId = QseeAppStartSyscall(0, ImgSize, MemLoc, img_name);

    if (AppIdPtr)
      *AppIdPtr = AppId;

    if ((AppId == 0) || (AppId == 0xFFFFFFFF))   
      Status = EFI_LOAD_ERROR;
    else
      Status = EFI_SUCCESS;
  }
  while (0);

  //UncachedFreePool (MemLoc);

  return Status;
}

static EFI_STATUS
LoadSecureImageFromFileSystem(
   IN  EFI_TZE_LOADER_PROTOCOL        *This,
   IN  EFI_GUID                       *pPartitionGuid,
   CHAR16                             *Path,
   IN BOOLEAN                         ServicesMbn,
   OUT UINT32                         *AppIdPtr

)
{

  UINT32                  Attrib = 0;
  EFI_STATUS              Status = EFI_SUCCESS;
  VOID                    *MemLoc;
  UINT8                   *CfgBuffPtr = NULL;
  UINTN                   ReadFileSize;
  UINT32                  AppId = 0;


  if( (pPartitionGuid == NULL) || (Path == NULL)) 
    return EFI_INVALID_PARAMETER;

  Attrib |= BLK_IO_SEL_PARTITIONED_GPT;
  Attrib |= BLK_IO_SEL_MEDIA_TYPE_NON_REMOVABLE;
  Attrib |= BLK_IO_SEL_MATCH_ROOT_DEVICE;
  Attrib |= BLK_IO_SEL_MATCH_PARTITION_TYPE_GUID;
  Attrib |= BLK_IO_SEL_SELECT_MOUNTED_FILESYSTEM;

  if (boot_from_emmc())
  {
  // Attempt to load from GPP Partition
  Status = LoadFileFromPartition(Attrib, Path, &gEfiEmmcGppPartition1Guid, pPartitionGuid, &CfgBuffPtr,
                                 &ReadFileSize);
  // If failed attempt to load from User Partition
  if (Status != EFI_SUCCESS)
    Status = LoadFileFromPartition(Attrib, Path, &gEfiEmmcUserPartitionGuid, pPartitionGuid, &CfgBuffPtr,
                                 &ReadFileSize);
    if (EFI_ERROR (Status))
    {
    DEBUG ((EFI_D_INFO, "LoadSecureImageFromFileSystem: LoadFileFromPartition failed for %s, status: %p\n", Path, Status));
    return Status;
  }
  }
  else if (boot_from_ufs())
  {
    // Attempt to load from UFS LUN 4
    Status = LoadFileFromPartition(Attrib, Path, &gEfiUfsLU4Guid, pPartitionGuid, &CfgBuffPtr,
                                 &ReadFileSize);
    if (EFI_ERROR (Status)) 
    {
      DEBUG ((EFI_D_INFO, "LoadSecureImageFromFileSystem: LoadFileFromPartition failed for %s, status: %p\n", Path, Status));
      return Status;
    }
  }
  else
  {
    DEBUG ((EFI_D_ERROR, "LoadSecureImageFromFileSystem: Invalid boot device !\n"));
    return EFI_LOAD_ERROR;
  }

  if (ReadFileSize > UncachedMemSize)
  {
     UncachedFreePool (UncachedMem);
     UncachedMem = UncachedAllocatePool (ReadFileSize);
     if (UncachedMem == NULL)
       return EFI_OUT_OF_RESOURCES;
     UncachedMemSize = ReadFileSize;
  }
  MemLoc = (UINT32*)UncachedMem;
/*
  MemLoc = UncachedAllocatePool (ReadFileSize);
  if (MemLoc == NULL)
    return EFI_OUT_OF_RESOURCES;
*/
  CopyMem(MemLoc, CfgBuffPtr, ReadFileSize);
  
  if (ServicesMbn == FALSE)
  { //normal app load
    AppId = QseeAppStartSyscall(0, ReadFileSize, MemLoc, "");
    if (AppIdPtr)
      *AppIdPtr = AppId;

    if ((AppId == 0) || (AppId == 0xFFFFFFFF))   
      Status = EFI_LOAD_ERROR;
    else
      Status = EFI_SUCCESS;
  } else
  {
    Status = QseeLoadServiceImageSyscall(0, ReadFileSize, MemLoc);
  }

  //UncachedFreePool (MemLoc);
  FreePool(CfgBuffPtr); 
  return Status;
}
/**
  Loads image from a partition specified by GUID Partition Type GUID 
  and Path
  
  @param  Path            Directory containging ACPI table files
  @param  pPartitionGuid  GUID eg:  TzAppsPartitionType
  @param AppIdPtr         out  if success holds app id on return
   
  @return EFI_SUCCESS if successful
**/
 
EFI_STATUS
LoadImageFromFileSystem(
   IN  EFI_TZE_LOADER_PROTOCOL        *This,
   IN  EFI_GUID                       *pPartitionGuid,
   CHAR16                             *Path,
   OUT UINT32                         *AppIdPtr

)
{


 return LoadSecureImageFromFileSystem(This, pPartitionGuid, Path,
                              FALSE, AppIdPtr);
}

  /**
  Loads all Secure Apps at bootup
  
  @param  Path            Directory containging ACPI table files
  @param  pPartitionGuid  GUID eg:  TzAppsPartitionType
  @param AppIdPtr         out  if success holds app id on return
   
  @return EFI_SUCCESS if successful
**/
 
EFI_STATUS
LoadSecureApps(
   IN  EFI_TZE_LOADER_PROTOCOL        *This,
   OUT UINT32                         *AppIdPtr
)
{
  MemRegionInfo           TzAppMemRegionInfo;
  EFI_STATUS              Status = EFI_SUCCESS;
  UINT32                  AppId = 0;
  UINT32                  version = 0;
  UINT32                  computed_version = 0;
  UINT32                  CommonMbnLoadFlag = 0;
  UINT32                  DxHdcp2LoadFlag = 0;
  UINT32                  VariableServicesFlag = 0;
  UINT32                  WinsecappFlag = 0;

  if (TZSecureAppLoadCalled == TRUE)
  {
    Status = EFI_LOAD_ERROR;
    goto ErrorExit;
  }

  Status = TzGetFeatureVersion(TZ_FVER_QSEE, &version);
  if(EFI_ERROR(Status))
  {
    goto ErrorExit;
  }

  computed_version = TZBSP_computed_VERSION (1,2,0);
  if (version >= computed_version)
  {
    Status = GetMemRegionInfoByName (MemLabel, &TzAppMemRegionInfo);
    if (Status != EFI_SUCCESS)
    {
        DEBUG ((EFI_D_INFO, "LoadSecureApps: Unable to find TZApps region in config file\n"));
        ASSERT (Status == EFI_SUCCESS);
    }
    
    Status = QseeAppsRegionNotificationSyscall(TzAppMemRegionInfo.MemBase,
                                                 TzAppMemRegionInfo.MemSize);
    ASSERT_EFI_ERROR(Status);
  }
  
  TZSecureAppLoadCalled = TRUE;

  computed_version = TZBSP_computed_VERSION (1,1,0);
  if (version >= computed_version)
  {
    Status = GetConfigValue ("SecurityFlag", &CommonMbnLoadFlag);        // Get this flag from uefiplat.cfg. Depending on the flag, process call to this function
    if ((Status != EFI_SUCCESS) || ((CommonMbnLoadFlag & 0x4) != 0x4))
    {
      DEBUG ((EFI_D_WARN, "CommonMbnLoadFlag not found in uefiplat.cfg. Defaulting to 0.\r\n"));
      // Default to 0. Does not matter what the flag is
      CommonMbnLoadFlag = 0;
      Status = EFI_SUCCESS;   // Continue for Hawker
    }
    else
    {
  Status = LoadSecureImageFromFileSystem (This,
                                          &TzAppsPartitionType,
                                          COMMON_LIB_MBN_FILE,
                                          TRUE,
                                          &AppId);
  DEBUG ((EFI_D_INFO, "LoadSecureApps: Common lib load result %p\n", Status));
  //ASSERT_EFI_ERROR(Status);
  }
  }


  Status = GetConfigValue ("SecurityFlag", &DxHdcp2LoadFlag);        // Get this flag from uefiplat.cfg. Depending on the flag, process call to this function
  if ((Status != EFI_SUCCESS) || ((DxHdcp2LoadFlag & 0x8) != 0x8))
  {
    DEBUG ((EFI_D_WARN, "DxHdcp2LoadFlag not found in uefiplat.cfg. Defaulting to 0.\r\n"));
    // Default to 0. Does not matter what the flag is
    DxHdcp2LoadFlag = 0;
    Status = EFI_SUCCESS;   // Continue for Hawker
  }
  else
  {
  Status = LoadSecureImageFromFileSystem (This,
                                          &TzAppsPartitionType,
                                          DXHDCP2_MBN_FILE,
                                          FALSE,
                                          &AppId);
  DEBUG ((EFI_D_INFO, "LoadSecureApps: Load from filesystem DxHdcp2App: 0x%p\n", Status));
  } 


  Status = GetConfigValue ("SecurityFlag", &VariableServicesFlag);        // Get this flag from uefiplat.cfg. Depending on the flag, process call to this function
  if ((Status != EFI_SUCCESS) || ((VariableServicesFlag & 0x10) != 0x10))
  {
    DEBUG ((EFI_D_WARN, "VariableServicesFlag not found in uefiplat.cfg. Defaulting to 0.\r\n"));
    // Default to 0. Does not matter what the flag is
    VariableServicesFlag = 0;
    Status = EFI_SUCCESS;   // Continue for Hawker
  }
  else
  {
  Status = LoadSecureImageFromFileSystem (This,
                                          &TzAppsPartitionType,
                                          VARIABLE_SERVICES_MBN_FILE,
                                          FALSE,
                                          &AppId);
  DEBUG ((EFI_D_INFO, "LoadSecureApps: Load from filesystem UEFISecApp: 0x%p\n", Status));
  if (EFI_ERROR(Status))
  {
    DEBUG((EFI_D_ERROR, "UEFISecApp failed to load! Status = 0x%x Info :", Status)); 
    ASSERT(!EFI_ERROR(Status));
  }
  } 


  Status = GetConfigValue ("SecurityFlag", &WinsecappFlag);        // Get this flag from uefiplat.cfg. Depending on the flag, process call to this function
  if ((Status != EFI_SUCCESS) || ((WinsecappFlag & 0x20) != 0x20))
  {
    DEBUG ((EFI_D_WARN, "WinsecappFlag not found in uefiplat.cfg. Defaulting to 0.\r\n"));
    // Default to 0. Does not matter what the flag is
    WinsecappFlag = 0;
    Status = EFI_SUCCESS;   // Continue for Hawker
  }
  else
  {
  Status = LoadSecureImageFromFileSystem (This,
                                          &TzAppsPartitionType,
                                          WINSECAPP_MBN_FILE,
                                          FALSE,
                                          &AppId);
  DEBUG ((EFI_D_INFO, "LoadSecureApps: Load from filesystem result Winsecapp: 0x%p\n", Status));

  if(EFI_ERROR(Status))
  {
    Status = LoadImageFromPartition (This,
                                     &TzExecPartitionType,
                                     &AppId);
    DEBUG ((EFI_D_INFO, "LoadSecureApps: Load from partition result Winsecapp: 0x%p\n", Status));

    if(EFI_ERROR(Status))
    {
      DEBUG((EFI_D_ERROR, "WinSecApp failed to load! Status = 0x%x", Status)); 
      ASSERT(!EFI_ERROR(Status));
    }
  }
  }   

  //send appid back for now; is this OK?
  if (AppIdPtr)
    *AppIdPtr = AppId;

ErrorExit:
  return Status;
}
