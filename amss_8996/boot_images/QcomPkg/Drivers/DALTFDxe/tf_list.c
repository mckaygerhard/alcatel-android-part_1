/**
  @file  tf_list.c
  @brief DALTF driver test list APIs.
*/

/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/
/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/23/15   jz      Created.
=============================================================================*/
#include<DDITF.h>
#include<tf_list.h>
#include<Library/UefiLib.h>


const TF_ContextDescr context_dummy = {0, 0};

//build the dummy head function for test case linked list
static uint32 DummyHeadFunc(uint32 dwArg, char* pszArg[])
{
    return 0;
}

static const TF_TestFunction_Descr DummyHeadFunDescr =
{
   "Dummy_Head",
   DummyHeadFunc, //union
   NULL,
   &context_dummy
};


// test list init
TF_Result tf_list_init(TF_TestList* list_ptr, uint32 max_entries)
{
	// init total entry number
	list_ptr->entry_num = 0;
    list_ptr->max_test_num = max_entries;

	// init dummy head
	if(DAL_SUCCESS != DALSYS_Malloc(sizeof(TF_TestEntry),
								    (void**)&(list_ptr->dummy_head)))
		return TF_ERROR_ALLOC;

	// init dummy head node of test list
	list_ptr->dummy_head->test_descr = &DummyHeadFunDescr;
    list_ptr->dummy_head->next = NULL;

	if(DAL_SUCCESS != DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
										&list_ptr->lock_h,
										&list_ptr->lock_obj))
		return TF_ERROR_ALLOC;

	return TF_SUCCESS;
}

// test list deinit - remove all nodes including dummy head
TF_Result tf_list_deinit(TF_TestList* list_ptr)
{
    TF_TestEntry* cur = NULL;
    TF_TestEntry* next = NULL;
    cur = list_ptr->dummy_head;

    while(cur!=NULL){
        next = cur->next;
        DALSYS_Free((void*)cur);
        cur = next;
    }

    DALSYS_DestroyObject(list_ptr->lock_h);
    return TF_SUCCESS;
}

// search test in the list get the function descriptor
TF_Result tf_list_get_test(TF_TestList* list_ptr, const char* test2get, TF_TestEntry** test_entry_ptr)
{
	TF_TestEntry* head = NULL;
	TF_TestEntry* cur = NULL;

	if(list_ptr != NULL)
		head = list_ptr->dummy_head;
	else
		return TF_ERROR_SYNC;

	if(test2get == NULL || head == NULL || head->next == NULL)
        return TF_ERROR_BAD_PARAM;

	LOCK_ENTER(list_ptr);
    cur= head->next;
    while(cur != NULL){
        if(cur->test_descr->pszTestName != NULL && AsciiStrnCmp(cur->test_descr->pszTestName,
        											       test2get,
														   MAX_TEST_NAME_LENGTH) == 0)
        {
        	if(test_entry_ptr != NULL){
				*test_entry_ptr = cur;
			}
			return TF_SUCCESS;
		}
        cur = cur->next;
    }

    LOCK_LEAVE(list_ptr);
    return TF_ERROR_TEST_NONEXIST;
}

// search test in the list get the function descriptor by ID
TF_Result tf_list_get_test_by_id(TF_TestList* list_ptr, uint32 id, TF_TestEntry** test_entry_ptr)
{
	TF_TestEntry* head = NULL;
	TF_TestEntry* cur = NULL;

	if(list_ptr != NULL)
		head = list_ptr->dummy_head;
	else
		return TF_ERROR_SYNC;

	if(head == NULL || head->next == NULL)
        return TF_ERROR_BAD_PARAM;

	cur = head->next;
	if(id <= cur->index){
		LOCK_ENTER(list_ptr);
		while(cur != NULL){
			if(cur->index == id){
				*test_entry_ptr = cur;
				return TF_SUCCESS;
			}
			cur = cur->next;
		}
		LOCK_LEAVE(list_ptr);
	}
    return TF_ERROR_TEST_NONEXIST;
}

// add test into list
TF_Result tf_list_add_test(TF_TestList* list_ptr, const TF_TestFunction_Descr* test2add)
{
	TF_TestEntry* head = NULL;
	TF_TestEntry* new_entry = NULL;

	if(list_ptr != NULL)
		head = list_ptr->dummy_head;
	else
		return TF_ERROR_SYNC;

    if(test2add == NULL || test2add->pszTestName == NULL|| head == NULL){
		return TF_ERROR_BAD_PARAM;
	}

    LOCK_ENTER(list_ptr);

	if(list_ptr->entry_num == list_ptr->max_test_num){
		return TF_ERROR_MEM_FULL;
	}

    //do not add if test2add already in the list
    if (TF_SUCCESS == tf_list_get_test(list_ptr, test2add->pszTestName, NULL)){
        return TF_SUCCESS;
    }
    
    //allocate memory for the newly added test
    if(DAL_SUCCESS != DALSYS_Malloc(sizeof(TF_TestEntry), (void**)&new_entry)){
		return TF_ERROR_ALLOC;
	}

	new_entry->index = list_ptr->entry_num; // assign temporary index
    new_entry->test_descr = test2add;
    new_entry->next = head->next;
    head->next = new_entry;
    list_ptr->entry_num++;

    LOCK_LEAVE(list_ptr);
    return TF_SUCCESS;
}

// remove test from list
TF_Result tf_list_remove_test(TF_TestList* list_ptr, const TF_TestFunction_Descr* test2del)
{
	TF_TestEntry* head = NULL;
	TF_TestEntry* cur = NULL;
	TF_TestEntry* pre = NULL;

	if(list_ptr != NULL)
		head = list_ptr->dummy_head;
	else
		return TF_ERROR_SYNC;

	if(test2del == NULL || test2del->pszTestName == NULL|| head == NULL)
        return TF_ERROR_BAD_PARAM;

    LOCK_ENTER(list_ptr);
    cur = head->next;
    pre = head;

    while(cur != NULL){
        if(cur->test_descr->pszTestName != NULL && AsciiStrnCmp(cur->test_descr->pszTestName,
        												   test2del->pszTestName,
														   MAX_TEST_NAME_LENGTH) == 0)
        {
            pre->next = cur->next;
            DALSYS_Free((void*)cur);
			list_ptr->entry_num--;
            return TF_SUCCESS;
        }
        pre = cur;
        cur = cur->next;
    }

    LOCK_LEAVE(list_ptr);
    return TF_ERROR_TEST_NONEXIST;
}

// iter list and refresh the test index
TF_Result tf_list_iter_list(TF_TestList* list_ptr){
	TF_TestEntry* cur;
    uint32 idx = list_ptr->entry_num;

	if(list_ptr != NULL){
		cur = list_ptr->dummy_head;
		if (cur == NULL)
			return TF_ERROR_TEST_NONEXIST;
		else
			cur = cur->next;
	}
	else
		return TF_ERROR_SYNC;

    LOCK_ENTER(list_ptr);

	while(cur != NULL){
		cur->index = idx--;
		AsciiPrint("[%d]  %a\n", cur->index, cur->test_descr->pszTestName);
		cur = cur->next;
	}

	LOCK_LEAVE(list_ptr);
	return TF_SUCCESS;
}
