/**
  @file  tf_dalfwk.c
  @brief DAL framework implementation file for the test framework driver.
*/

/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/

/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/23/15   jz      Created.
=============================================================================*/
#include<DALReg.h>
#include<DALFramework.h>
#include<DDITF.h>
#include<DALDeviceId.h>
#include<DALSysTypes.h>
#include<tf_driver.h>

#define GET_TFPRIVATECTXT(h) \
    &((TFClientCtxt*)((DalTFHandle*)h->pClientCtxt))->pTFDevCtxt->TFPCtxt

/*=========================================================================
      Type Definitions
==========================================================================*/

/*------------------------------------------------------------------------------
Declaring a "TF" Driver,Device and Client Context
------------------------------------------------------------------------------*/
typedef struct TFDrvCtxt TFDrvCtxt;

/*------------------------------------------------------------------------------
Declaring a private "TF" Vtable
------------------------------------------------------------------------------*/
typedef struct TFDALVtbl TFDALVtbl;
struct TFDALVtbl
{
    DALResult (*TF_DriverInit)(TFDrvCtxt *);
    DALResult (*TF_DriverDeInit)(TFDrvCtxt *);
};

typedef struct TFDevCtxt
{
    //Base Members
    uint32   dwRefs;
    DALDEVICEID DevId;
    uint32   dwDevCtxtRefIdx;
    TFDrvCtxt  *pTFDrvCtxt;
    DALSYS_PROPERTY_HANDLE_DECLARE(hProp);
    uint32 Reserved[16];
    //TF Dev state can be added by developers here
    TFPrivateCtxt TFPCtxt;
}TFDevCtxt;

struct TFDrvCtxt{
    //Base Members
    TFDALVtbl TFDALVtbl;
    uint32  dwNumDev;
    uint32  dwSizeDevCtxt;
    uint32  bInit;
    uint32  dwRefs;
    TFDevCtxt TFDevCtxt[1];
    //TF Drv state can be added by developers here
};

typedef struct TFClientCtxt
{
    //Base Members
    uint32  dwRefs;
    uint32  dwAccessMode;
    void *pPortCtxt;
    TFDevCtxt *pTFDevCtxt;
    DalTFHandle DalTFHandle;
    //TF Client state can be added by developers here
}TFClientCtxt;

/*=========================================================================
      Driver Function Prototypes
==========================================================================*/
DALResult tf_dev_attach
(
	const char* args,
	DALDEVICEID DeviceID,
	DalDeviceHandle **dev_handle_ptr
);

/*=========================================================================
      Driver Info
==========================================================================*/
static DALDEVICEID DalTF_DeviceId[1] =
{
	DALDEVICEID_URAD_DALTF
};

DALREG_DriverInfo DALTF_DriverInfo =
{
	tf_dev_attach,
	1,
	DalTF_DeviceId
};


/*=========================================================================
     Driver Functions
==========================================================================*/

static uint32 tf_add_ref
(
	DalTFHandle* h
)
{
    return DALFW_AddRef((DALClientCtxt *)(h->pClientCtxt));
}

/*=========================================================================
	Following functions are for DALDriver specific functionality
==========================================================================*/
static DALResult tf_drv_init(TFDrvCtxt *drv_ptr)
{
	return DAL_SUCCESS;
}

static DALResult tf_drv_deinit(TFDrvCtxt *drv_ptr)
{
	return DAL_SUCCESS;
}

/*=========================================================================
	Following functions are defined in DalDevice DAL Interface
==========================================================================*/
static uint32 tf_dev_detach
(
	DalDeviceHandle *h
)
{
	return DALFW_Release((DALClientCtxt *)(h->pClientCtxt));
}

// tf_dev_init function init daltf client context
static DALResult tf_dev_init
(
	DalDeviceHandle * h
)
{
	/*if (DAL_SUCCESS != DALSYS_GetDALPropertyHandle(
		DALDEVICEID_URAD_DALTF, daltf_drvctxt.TFDevCtxt[0].hProp))
		return DAL_ERROR;*/
	return tf_init(GET_TFPRIVATECTXT(h));
}
   
static DALResult tf_dev_deinit
(
	DalDeviceHandle * h
)
{
	return tf_deinit(GET_TFPRIVATECTXT(h));
}
   
static DALResult tf_dev_open
(
	DalDeviceHandle * h,
	uint32 id
)
{
	return DAL_SUCCESS;
}
   
static DALResult tf_dev_close
(
		DalDeviceHandle * h
)
{
	return DAL_SUCCESS;
}
   
static DALResult tf_dev_info
(
	DalDeviceHandle * h,
	DalDeviceInfo *pdeviceInfo,
	uint32 dwSize)
{
	return DAL_SUCCESS;
}
   
static DALResult tf_dev_powerevent(
	DalDeviceHandle * h,
	DalPowerCmd PowerCmd, 
    DalPowerDomain PowerDomain)
{
	return DAL_SUCCESS;
}
   
static DALResult tf_dev_sysrequest
(
	DalDeviceHandle* h,
	DalSysReq ReqIdx,
	const void* SrcBuf, 
    uint32 SrcBufLen,
	void* DestBuf,
	uint32 DestBufLen, 
    uint32* DestBufLenReq
)
{
	return DAL_SUCCESS;					   
}

/*=========================================================================
	Following functions are extended in DalTF Interface
==========================================================================*/
static DALResult daltf_addtests
(
	DalDeviceHandle * h,
	const TF_TestFunction_Descr *  tests2add[],
	uint32  num_tests2add
)
{
	return tf_add_tests(GET_TFPRIVATECTXT(h), tests2add, num_tests2add);
}					  

static DALResult daltf_removetests
(
	DalDeviceHandle * h,
	const TF_TestFunction_Descr *  tests2remove[],
	uint32  num_tests2rm
)
{
	return tf_remove_tests(GET_TFPRIVATECTXT(h), tests2remove, num_tests2rm);
}

static DALResult daltf_handlecmd(DalDeviceHandle * h, const char* request)
{
	return tf_handle_cmd(GET_TFPRIVATECTXT(h), request);
}

/*=========================================================================
	Following functions are defined in DalDevice DAL Interface.
==========================================================================*/
static void tf_init_interface(TFClientCtxt* pclientCtxt)
{
    static const DalTF vtbl = {
		{
			tf_dev_attach,
			tf_dev_detach,
			tf_dev_init,
			tf_dev_deinit,
			tf_dev_open,
			tf_dev_close,
			tf_dev_info,
			tf_dev_powerevent,
			tf_dev_sysrequest
		},
		daltf_addtests,
		daltf_removetests,
		daltf_handlecmd
   };
   /*--------------------------------------------------------------------------
   Depending upon client type setup the vtables (entry points)
   --------------------------------------------------------------------------*/
   pclientCtxt->DalTFHandle.dwDalHandleId = DALDEVICE_INTERFACE_HANDLE_ID;
   pclientCtxt->DalTFHandle.pVtbl  = &vtbl;
   pclientCtxt->DalTFHandle.pClientCtxt = pclientCtxt;
}

/*------------------------------------------------------------------------------
Following functions are extended in DalTF Interface.
------------------------------------------------------------------------------*/

// DAL_DeviceAttach will get DALTF Info first and call this attach function
// after DAL::attach is done, it will automatically call DAL::init function
// basically, APIS listed in DALTF struct doesn't necessarily need err handler

DALResult tf_dev_attach
(
	CONST CHAR8 *pszArg,
	DALDEVICEID DeviceId,
	DalDeviceHandle **phDalDevice
)
{
    DALResult nErr;
    static TFDrvCtxt daltf_drvctxt =
    {
      {
    	tf_drv_init,
    	tf_drv_deinit
    	},
      1,
      sizeof(TFDevCtxt)
    };

    static TFClientCtxt clientCtxt;
    TFClientCtxt * pClientCtxt = &clientCtxt;

    *phDalDevice = NULL;

    nErr = DALFW_AttachToDevice(DeviceId, (DALDrvCtxt *)&daltf_drvctxt,
    	    							  (DALClientCtxt *)pClientCtxt);
	if(DAL_SUCCESS == nErr){
		tf_init_interface(pClientCtxt);
		tf_add_ref(&(pClientCtxt->DalTFHandle));
		*phDalDevice = (DalDeviceHandle *)&(pClientCtxt->DalTFHandle);
	}
    return nErr;
}
