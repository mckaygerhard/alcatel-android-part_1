/**
  @file  tf_driver.h
  @brief internal header file for test framework driver interface.
  
*/

/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/

/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/23/15   jz      Created.
=============================================================================*/

#ifndef __TF_DEVICE_H__
#define __TF_DEVICE_H__

#include<DDITF.h>
#include<tf_list.h>
#include<DALSys.h>
#include<tf_handle.h>

#define INSTANCE_NAME_LEN	32

typedef const void *DalTFPropertyValueType;

typedef struct TFPrivateCtxt{
	uint32 						   dwInstance;

	CHAR8 						   szInstanceName[INSTANCE_NAME_LEN];   	/* Name of the instance (proc) in the UE */

	TF_TestList					   test_list;				/* Contains descriptions of all the test function entries in the testing framework. */
	
	TFHandleCtxt				   handle_ctxt;		    /**< current active test input and output data */

}TFPrivateCtxt;

DALResult tf_init(TFPrivateCtxt* ctxt_ptr);

DALResult tf_deinit(TFPrivateCtxt* ctxt_ptr);

DALResult tf_add_tests(TFPrivateCtxt* ctxt_ptr, const TF_TestFunction_Descr*  tests2add[], uint32  num_tests2add);

DALResult tf_remove_tests(TFPrivateCtxt* ctxt_ptr, const TF_TestFunction_Descr*  tests2remove[], uint32  num_tests2rm);

DALResult tf_handle_cmd(TFPrivateCtxt* ctxt_ptr, const char* request);

#endif /*#ifndef __TF_DEVICE_H__*/
