/**
  @file  tf_sample_tests.h
  @brief internal header file of sample tests
*/

/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/
/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/23/15   jz      Created.
=============================================================================*/
#ifndef __TF_SAMPLE_TESTS_H__
#define __TF_SAMPLE_TESTS_H__

#include<DDITF.h>
#include<tf_list.h>

extern const TF_TestFunction_Descr* tf_apSampleTestDescriptions[];

extern const uint16 tf_wNumberSampleTests;

#endif /*#ifndef __TF_SAMPLE_TESTS_H__*/
