/**
  @file  tf_driver.h
  @brief internal header defined DALTF driver command handler
*/

/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/

/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/23/15   jz      Created.
=============================================================================*/

#ifndef __TF_HANDLE_H__
#define __TF_HANDLE_H__

#include<DDITF.h>
#include<DALSys.h>
#include<tf_list.h>
#include<Library/TimetickUEFI.h>

#define LIST_STR      "LIST"
#define RUN_STR       "RUN"
#define STATUS_STR    "STATUS"
#define MESSAGE_STR   "MESSAGE"
#define HELP_STR      "HELP"

#define LIST_LEN      4
#define RUN_LEN       3
#define STATUS_LEN    6
#define MESSAGE_LEN   7
#define HELP_LEN      4

#define TFSIG      "[DALTF]"
#define BUF_SIZE	  256

typedef struct TF_TestResult{
	uint32 max_time;
	uint32 min_time;
	uint32 success_cnt;
	uint32 failure_cnt;
	uint32 total_cnt;
	uint32 last_result;
}TF_TestResult;

typedef struct TF_TestInput{
	TF_TCFunctionPtr pTestFunc;
	uint32 max_run;
	uint32 period_time;
	uint32 num_args;
	char** argv; 			// need allocation when populate
}TF_TestInput;

typedef struct TF_TestData{
	char test_name[MAX_TEST_NAME_LENGTH + 1];
	uint32 busy;
	TF_TestInput test_input;		/* Contains  current running test data. */
	TF_TestResult test_res;			/* Contains  current running test status and result. */
	char raw_data_buf[BUF_SIZE];	/* memory pool 256 bytes */
	DALSYSSyncObj lock_obj;         /* Sync object storage */
	DALSYSSyncHandle lock_h;        /* Handle to our mutex */
}TF_TestData;

typedef struct TFHandleCtxt{
	TF_TestData cur_test;
	TF_TestList * test_list_ptr;		/* Ptr to test list. */
}TFHandleCtxt;

TF_Result tf_handle_init(TFHandleCtxt* h, TF_TestList * list_ptr);

TF_Result tf_handle_deinit(TFHandleCtxt* h);

TF_Result tf_handle_status(TFHandleCtxt* h, char* params);

TF_Result tf_handle_list(TFHandleCtxt* h);

TF_Result tf_handle_run(TFHandleCtxt* h, char* params);

TF_Result tf_handle_help(TFHandleCtxt* h, char* params);

TF_Result tf_handle_message(TFHandleCtxt* h);


#endif /*#ifndef __TF_HANDLE_H__*/
