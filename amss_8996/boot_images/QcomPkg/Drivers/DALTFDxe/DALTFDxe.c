/** @file  DALTFDxe.c
    @brief This file implements DALTF EFI protocol interface.

**/

/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/

/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
03/19/15    jz      create

=============================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/CacheMaintenanceLib.h>
#include <Protocol/EFIDALSYSProtocol.h>
#include <Protocol/EFIDALTF.h>
#include "DALDeviceId.h"
#include "DDITF.h"


/*=========================================================================
      Type Definitions
==========================================================================*/

STATIC DalDeviceHandle *hDALTF = NULL;

/*
 * Defined in tf_dalfwk.c
 */
extern DALREG_DriverInfo DALTF_DriverInfo;

STATIC DALREG_DriverInfo* DALDriverInfoArr[] =
    {
        &DALTF_DriverInfo
    };

/* for 8994 device, loading the gDALModDriverInfoList structure
DALREG_DriverInfoList gDALModDriverInfoList =
{
	1, DALDriverInfoArr
};
*/

/*
 * For dynamic device loading the gDALModDriverInfoList structure is
 * populated by this module. 
 * apply for 8996 and Hawker
 */

static DALSYSConfig DALSYSModConfig = 
    {
        {0, NULL},              // no string devices supported
        {1, DALDriverInfoArr}   // numeric driver info
    };

/*=========================================================================
      Functions
==========================================================================*/

/**
  add test cases into test framework

  This function will add tests in the list given by user

  @param  Tests     [in]          test list to be added.
  @param  Num_Tests [in]          total number of tests in the list

  @return EFI_SUCCESS           The function completed successfully.
  @return EFI_PROTOCOL_ERROR    The function had a protocol error.
**/

STATIC EFI_STATUS EFI_TFAddTests
(
   IN  CONST TF_TestFunction_Descr**	Tests,
   IN  UINTN							Num_Tests
)
{
  DALResult eResult;

  if (Tests == NULL)
  {
    return EFI_INVALID_PARAMETER;
  }

  eResult = DalTF_AddTests(hDALTF, Tests, Num_Tests);

  if(eResult == DAL_SUCCESS)
  {
     return EFI_SUCCESS;
  }
  else
  {
     //for now, just return one common error condition
     //there is no one-to-one mapping from DAL return code to
     //EFI API return codes.
     return EFI_PROTOCOL_ERROR;
  }
}


/**
  remove test cases from test framework.

  This function will remove tests in the list given by user

  @param  Tests     [in]          test list to be removed.
  @param  Num_Tests [in]          total number of tests in the list

  @return EFI_SUCCESS           The function completed successfully.
  @return EFI_PROTOCOL_ERROR    The function had a protocol error.
**/

STATIC EFI_STATUS EFI_TFRemoveTests
(
   IN  CONST TF_TestFunction_Descr**	Tests,
   IN  UINTN							Num_Tests
)
{
  DALResult eResult;

  if (Tests == NULL)
  {
    return EFI_INVALID_PARAMETER;
  }

  eResult = DalTF_RemoveTests(hDALTF, Tests, Num_Tests);

  if(eResult == DAL_SUCCESS)
  {
     return EFI_SUCCESS;
  }
  else
  {
     //for now, just return one common error condition
     //there is no one-to-one mapping from DAL return code to
     //EFI API return codes.
     return EFI_PROTOCOL_ERROR;
  }
}


/**
  process incoming commands.

  This function will process request sent from uart.

  @param  Request    [in]       command string.

  @return EFI_SUCCESS           The function completed successfully.
  @return EFI_PROTOCOL_ERROR    The function had a protocol error.
**/

STATIC EFI_STATUS EFI_TFHandleCommand
(
   IN  CONST CHAR8*		Request
)
{
  DalTF_HandleCommand(hDALTF, Request);
  
  return EFI_SUCCESS;
}

/*=========================================================================
      Data Declarations
==========================================================================*/

STATIC EFI_DALTF_PROTOCOL DALTFProtocol =
{
  EFI_DALTF_PROTOCOL_VERSION,
  EFI_TFAddTests,
  EFI_TFRemoveTests,
  EFI_TFHandleCommand
};

/**
 *  Entry point for the DALTF DXE driver
 */
 
EFI_STATUS EFIAPI 
DALTFDxeEntryPoint
(
  IN EFI_HANDLE ImageHandle,
  IN EFI_SYSTEM_TABLE *SystemTable
)
{
  EFI_HANDLE handle = NULL;
  EFI_STATUS status;
  DALResult eResult;

  // Init DALSys
  DALSYS_InitMod(&DALSYSModConfig);

  //Attach to the DAL TF Device
  eResult = DAL_DeviceAttach(DALDEVICEID_URAD_DALTF, &hDALTF);
  
  if(eResult == DAL_SUCCESS){
    // Publish the DALTF Protocol
    status = gBS->InstallMultipleProtocolInterfaces(
                    &handle,
                    &gEfiDaltfProtocolGuid,
                    (void **)&DALTFProtocol,
					NULL );
  }
  else{
    DALSYS_DeInitMod();
    status = EFI_PROTOCOL_ERROR;
  }

  return status;
}
