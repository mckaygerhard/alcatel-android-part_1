/**
* @file UsbfnDevUtil.h
*
* @brief Utility macros and functions.
*
* @copyright Copyright (C) 2014 by QUALCOMM Technologies, Inc.  All Rights Reserved.
*/
/*==============================================================================
EDIT HISTORY


when       who     what, where, why
--------   ---     -----------------------------------------------------------
08/21/14   ck      Initial verion
=============================================================================*/
#ifndef _USB_DEV_UTIL_H_
#define _USB_DEV_UTIL_H_

#include <Library/DebugLib.h>


#define DBG(EfiD, Str, ...) \
  DEBUG((EfiD, "%a: " Str "\n", __FUNCTION__, ##__VA_ARGS__))


// For debug stack info.
#define FNC_ENTER_MSG() DBG(EFI_D_VERBOSE, "++")
#define FNC_LEAVE_MSG() DBG(EFI_D_VERBOSE, "--")


#endif  //_USB_DEV_UTIL_H_
