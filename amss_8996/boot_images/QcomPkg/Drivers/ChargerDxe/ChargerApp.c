/** @file ChargerApp.c

  Implements the Qualcomm's Charging application

  Copyright (c) 2014 - 2015,  Qualcomm Technologies Inc. All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY


 when         who     what, where, why
 --------     ---     -----------------------------------------------------------
 06/03/15     sm      Moved the SOC threshold check in ChgAppReportBatteryStatus event
 06/03/15     mr      Enabled Charger functionality for 8952 (CR-846387)
 05/21/15     sm      Added API call to pet charger watchdog
 05/18/15     al      ChargerGone Display fix
 05/21/15     al      Adding support for RTC alarm bootup
 05/18/15     al      Display for full charging fix
 04/30/15     va      IChgMax and VddMax configuration support for all Jeita windows
                      JEITA Low temp, high temp handle
 04/28/15     al      Adding continue boot if SoC above limit even if temp high
 04/30/15     al      Adding changes to disable USB as PON and shutdown device if battery missing
 04/28/15     sm      Added changes not to enter LPM and not to reset the device
                      when doing threshold charging
                      Changes for not disabling charging on after threshold charging.
 03/23/15     al      Adding SleepPerformLPM
 03/20/15     sv      Removed work arond for Display turn off issue.
 03/16/15     al      Disable charging only for full charging during exit
 02/24/15     sv      Added work arond for Display turn off issue.
 02/20/15     al      Adding stubs for key press
 02/03/15     al      Moving shutdown API to ChargerLib
 01/29/15     sm      Added check to boot to Tiles if sticky charging is enabled
                      but charger is not connected and battery is above threshold.
 01/13/15     va      Logging for ChargerApp states
 12/16/14     al      Handled Init, Error, post processing and full charging
 12/02/14     sm      New File

=============================================================================*/
#include <Uefi.h>

/**
  EFI interfaces
 */
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/QcomLib.h>
#include <Library/QcomUtilsLib.h>
#include <api/systemdrivers/pmic/pm_uefi.h>
#include <Protocol/EFIDisplayPwrCtrl.h>

#include <Protocol/EFIVariableServices.h>
#include <Library/MemoryAllocationLib.h>

/**
  Charger PROTOCOL interface
 */
#include "Protocol/EFICharger.h"
#include "Protocol/EFIChargerExtn.h"
#include "ChargerLib.h"
#include "ChargerApp.h"
//#include "sleeplib.h"

/**
  EFI interfaces
 */
#include <Protocol/GraphicsOutput.h>
#include <Library/BaseLib.h>
#include <Protocol/EFIDisplayPwr.h>
#include <Protocol/EFIClock.h>
#include <Protocol/EFIPmicVersion.h>

/*===========================================================================*/
/*                  FUNCTIONS PROTOTYPES                                     */
/*===========================================================================*/

#define CHARGER_BATTERY_SYMBOL_NOBATTERY            "fv1:\\battery_symbol_NoBattery.bmp"
#define CHARGER_BATTERY_SYMBOL_NOCHARGER            "fv1:\\battery_symbol_Nocharger.bmp"
#define CHARGER_BATTERY_SYMBOL_SOC10                "fv1:\\battery_symbol_Soc10.bmp"
#define CHARGER_BATTERY_SYMBOL_SOC25                "fv1:\\battery_symbol_Soc25.bmp"
#define CHARGER_BATTERY_SYMBOL_SOC50                "fv1:\\battery_symbol_Soc50.bmp"
#define CHARGER_BATTERY_SYMBOL_SOC75                "fv1:\\battery_symbol_Soc75.bmp"
#define CHARGER_BATTERY_SYMBOL_SOC100               "fv1:\\battery_symbol_Soc100.bmp"
#define CHARGER_BATTERY_SYMBOL_LOWBATTERY           "fv1:\\battery_symbol_LowBattery.bmp"
#define CHARGER_BATTERY_SYMBOL_LOWBATTERYCHARGING   "fv1:\\battery_symbol_LowBatteryCharging.bmp"

#define FULL_BATTERY_SOC                            100
#define EXIT_CHARGERDXE                 L"ExitChargerDxe"

#define DEFAULT_THRESHOLD_SOC         7
#define IDLE_WAIT_DURATION            5000 /*5000 milli sec*/
#define ONE_SEC_WAIT                  10000000 /*tick is 100ns; 100 * 10000000 = 1sec*/
#define SEVEN_SEC_COUNT               7

#define FIVE_SEC_WAIT                 50000000 /*tick is 100ns; 100 * 10000000 = 1sec*/

STATIC BOOLEAN ChargingLoopExit = FALSE;


/*===========================================================================*/
/*                  FUNCTIONS PROTOTYPES                                     */
/*===========================================================================*/

VOID ChargerKeyPressControl(BOOLEAN EnableKeyControl);
VOID ChargerPwrKeyPressControl(BOOLEAN EnableKeyControl);
EFI_STATUS ChgAppDispBattSymbol(CHGAPP_DISP_IMAGE_TYPE DispImageType);

EFI_STATUS ChgAppIdleWait (VOID);
EFI_STATUS IdleWait(UINT32 WaitMilliSec);

EFI_STATUS ChgAppChargedPostProcess(ChgAppChargingType ChargingType);

static UINT32  KeyPressCounter = 7;
static UINT32  CurrentSoc = 0;
static BOOLEAN FullBattChargingEnabled = FALSE;
static BOOLEAN DisplayOn = TRUE;
static UINT32  ThresholdSoc = DEFAULT_THRESHOLD_SOC;
static UINT32  ChargeBatteryTo = DEFAULT_THRESHOLD_SOC;
STATIC EFI_BATTERY_CHARGING_STATUS  ChargerErrors = EfiBatteryChargingStatusNone;


VOID EFIAPI ChargerAppEnterLpm(IN EFI_EVENT Event, IN VOID *Context);
VOID EFIAPI ChargerAppExitLpm(IN EFI_EVENT Event, IN VOID *Context);
VOID EFIAPI DisplayOffTimer(IN EFI_EVENT Event, IN VOID *Context);
VOID EFIAPI ChargerAppShutdownInTimer(IN EFI_EVENT Event, IN VOID *Context);
VOID EFIAPI ChgAppReportBatteryStatus(IN EFI_EVENT Event, IN VOID *Context);

EFI_EVENT EfiChargerEnterLPMEvent = (EFI_EVENT)NULL;
EFI_EVENT EfiChargerExitLPMEvent = (EFI_EVENT)NULL;
EFI_EVENT EventDisplayOffCount = (EFI_EVENT)NULL;
EFI_EVENT EventShutdownInTimerCount = (EFI_EVENT)NULL;
EFI_EVENT UIActiveEvent      = NULL;
EFI_EVENT UIIdleEvent        = NULL;
EFI_EVENT EventChgAppReportBatteryStatus = (EFI_EVENT)NULL;

extern EFI_GUID gEfiEventEnterLPMGuid;
extern EFI_GUID gEfiEventExitLPMGuid;

static EFI_CLOCK_PROTOCOL *ClockProtocol = NULL;

static ChgAppSysShutdownType ChgAppShutdownContext = CHGAPP_RESET_NONE;

extern EFI_STATUS
LoadFromFV(IN   CHAR8 *Name,
           OUT  UINT8 **FileBuffer,
           OUT  UINTN *FileSize);

static EFI_STATUS ChagAppCheckRtcAlarm(void);	   

/*===========================================================================*/
/*                  FUNCTIONS DEFINITIONS                                     */
/*===========================================================================*/


VOID
EFIAPI
ChargerAppDisplayCallback(
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
  (void)Event;
  (void)Context;
  CHARGER_DEBUG((EFI_D_ERROR, "ChargerAppDisplayCallback.. \r\n"));
}



/* Flush the NV Storage tables before turning ON write protection
 * Do this only if the protocol available. During initial stages the
 * the protocol might not be installed */

EFI_STATUS ChgAppFlushVariable(UINT16* variable, BOOLEAN Value)
{
  EFI_VARIABLESERVICES_PROTOCOL *VariableServicesProtocol = NULL;
  EFI_STATUS Status = EFI_SUCCESS;
  UINTN   DataSize = sizeof(Value);
  UINT32  Attributes = (EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS);

  if (NULL == variable)
  {
    return EFI_INVALID_PARAMETER;
  }

  Status = gRT->SetVariable(variable,
                            &gQcomTokenSpaceGuid,
                            Attributes,
                            DataSize,
                            &Value);

  gBS->LocateProtocol(&gEfiVariableServicesProtocolGuid, NULL, (VOID **)&VariableServicesProtocol);

  if (VariableServicesProtocol != NULL)
  {
    VariableServicesProtocol->FlushVariableNV(VariableServicesProtocol);
  }

  return Status;
}


/**
  Duplicates ASCII string

  @param  Source  Null Terminated string of characters
  @param  Length  source String length
  @return  Duplicated String  if successful otherwise return NULL

  note: Caller responsibility to Free returned String
**/
static CHAR8 * AsciiStrNDup(
  CONST CHAR8               *Source,
  UINTN                     Length
)
{
  CHAR8 *Dest = NULL;
  if(NULL == Source || 0 == Length)
    return NULL;

  Dest = AllocatePool (Length + 1);
  if (NULL == Dest)
  {
    return NULL;
  }

  AsciiStrnCpy(Dest, Source, Length + 1);

  return Dest;
}


EFI_STATUS ChargerAppDisplayTimerEvent(BOOLEAN On)
{
  EFI_STATUS Status = EFI_SUCCESS;
  STATIC BOOLEAN OnStatus = FALSE;
  switch (On)
  {
  case TRUE:
    if (FALSE == OnStatus)
    {
      Status = gBS->CreateEvent(EVT_TIMER | EVT_NOTIFY_SIGNAL, TPL_CALLBACK, DisplayOffTimer, NULL, /*GUID Display guys provide*/ &EventDisplayOffCount);
      Status |= gBS->SetTimer(EventDisplayOffCount, TimerPeriodic, ONE_SEC_WAIT);
      OnStatus = TRUE;
    }
    break;
  case FALSE:
    if (TRUE == OnStatus)
    {
      Status = gBS->CloseEvent (EventDisplayOffCount);
      OnStatus = FALSE;
    }
    break;
  default:
    break;
  }

  return Status;
}

//Timer event for 7 seconds timer
VOID EFIAPI DisplayOffTimer
(IN EFI_EVENT        Event,
 IN VOID             *Context
 )
{
  /*periodic timer for key press wait for 7 seconds decrement count.. have 1 second timer
   no key pressed signal Display
   */
  /*bound check*/
  KeyPressCounter = (KeyPressCounter > SEVEN_SEC_COUNT) ? SEVEN_SEC_COUNT : KeyPressCounter;
  if (TRUE == DisplayOn)
  {
    KeyPressCounter--;
  }

  /*@todo: display false then return*/

  if (KeyPressCounter > 0)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "TimerCountdown KeyPressCounter: %d\r\n", KeyPressCounter));
  }

  if (!KeyPressCounter)
  {
    
    if(TRUE == FullBattChargingEnabled)
    {
      //Enter LPM
      gBS->SignalEvent(EfiChargerEnterLPMEvent);
	}
	else
	{
      DisplayOn = FALSE;
      DEBUG((EFI_D_ERROR, "DisplayOffTimer: Turning off Display\n\r"));
      gBS->SignalEvent(UIIdleEvent);
    }

    /*close timer event*/
    ChargerAppDisplayTimerEvent(FALSE);
  }

}


//Timer event for battery removal and charger removal shutdown
VOID EFIAPI ChargerAppShutdownInTimer
(IN EFI_EVENT        Event,
 IN VOID             *Context
 )
{
  DEBUG((EFI_D_ERROR, "ChargerAppShutdownInTimer: ChargerLibForceSysShutdown= %d \n\r", ChgAppShutdownContext));
  ChargerLibForceSysShutdown(ChgAppShutdownContext);
}



EFI_STATUS
KeyNotifyFunc(IN EFI_KEY_DATA     *KeyData)
{
  KeyPressCounter = SEVEN_SEC_COUNT;

  if(TRUE == FullBattChargingEnabled)
  {
    /* Exit LPM */
    CHARGER_DEBUG((EFI_D_ERROR, "KeyNotifyFunc signalling ExitLPMEvent\r\n"));
    gBS->SignalEvent(EfiChargerExitLPMEvent);
  }
  else
  {
    CHARGER_DEBUG((EFI_D_ERROR, "KeyNotifyFunc turning ON display \r\n"));
    DisplayOn = TRUE;
    gBS->SignalEvent(UIActiveEvent);
  }

  CHARGER_DEBUG((EFI_D_ERROR, "KeyNotifyFunc calling ChgAppDispBattSymbol\r\n"));
  ChgAppDispBattSymbol(CHGAPP_DISP_CHARGING_SYMBOL);

  DEBUG((EFI_D_ERROR, "Turning off display in %d seconds \r\n", KeyPressCounter));
  CHARGER_FILE_DEBUG((EFI_D_ERROR, "Turning off display in %d seconds \r\n", KeyPressCounter));

  /*create timer event*/
  ChargerAppDisplayTimerEvent(TRUE);

  return EFI_SUCCESS;
}

EFI_STATUS
PwrKeyNotifyFunc(IN EFI_KEY_DATA     *KeyData)
{
  EFI_STATUS                  Status        = EFI_SUCCESS;

  KeyPressCounter = SEVEN_SEC_COUNT;

  gBS->SignalEvent(EfiChargerExitLPMEvent);

  /* Two times debug print to let other folks know that charging have started*/
  DEBUG((EFI_D_WARN, "Power key pressed \r\n"));
  CHARGER_FILE_DEBUG((EFI_D_WARN, "Power key pressed \r\n"));

  /*create timer event*/
  ChargerAppDisplayTimerEvent(TRUE);

  /*if current SoC is less than minimum threshold SoC then upon power button press do not exit charging*/
  if (CurrentSoc < ThresholdSoc)
  {
    CHARGER_DEBUG((EFI_D_WARN, "PwrKeyNotifyFunc CurrentSoc(%d),  ThresholdSoc(%d)\r\n",CurrentSoc , ThresholdSoc ));

    return EFI_SUCCESS;
  }

  /*set the variable and reset the device*/
  ChgAppFlushVariable(EXIT_CHARGERDXE, TRUE);
  ChargerLibForceSysShutdown(CHGAPP_RESET_COLD);

  return Status;
}

VOID
ChargerKeyPressControl(BOOLEAN EnableKeyControl)
{
  EFI_STATUS        Status = EFI_SUCCESS;
  EFI_KEY_DATA      NotifyKeyData;

  STATIC EFI_HANDLE        VolUpKeyNotifyHandle  = NULL;
  STATIC EFI_HANDLE        VolDwnKeyNotifyHandle = NULL;
  STATIC EFI_HANDLE        HomeKeyNotifyHandle   = NULL;
  STATIC EFI_SIMPLE_TEXT_INPUT_EX_PROTOCOL *SimpleInputEx = NULL;

  /* Get protocol handle if we don't have */
  if (SimpleInputEx == NULL)
  {
    Status = GetNativeKeypad(&SimpleInputEx);
    GetNativeKeypad(&SimpleInputEx);
    if (Status != EFI_SUCCESS)
    {
      CHARGER_DEBUG((EFI_D_WARN, "Couldn't locate SimpleInputEx protocol\r\n"));
      return;
    }
  }

  if (EnableKeyControl)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "key press monitoring enabled. \r\n"));
    /* If we have already registered for notification
     * no more action needed */
    if (VolUpKeyNotifyHandle != NULL)
       return;

    NotifyKeyData.Key.UnicodeChar = 0;
    NotifyKeyData.KeyState.KeyShiftState = 0;
    NotifyKeyData.KeyState.KeyToggleState = 0;

    NotifyKeyData.Key.ScanCode = SCAN_UP;
    Status = SimpleInputEx->RegisterKeyNotify(SimpleInputEx,
                                              &NotifyKeyData,
                                              KeyNotifyFunc,
                                              &VolUpKeyNotifyHandle);

    NotifyKeyData.Key.ScanCode = SCAN_DOWN;
    Status |=   SimpleInputEx->RegisterKeyNotify(SimpleInputEx,
                                                 &NotifyKeyData,
                                                 KeyNotifyFunc,
                                                 &VolDwnKeyNotifyHandle);

    NotifyKeyData.Key.ScanCode = SCAN_HOME;
    Status |= SimpleInputEx->RegisterKeyNotify(SimpleInputEx,
                                               &NotifyKeyData,
                                               KeyNotifyFunc,
                                               &HomeKeyNotifyHandle);

    if (Status != EFI_SUCCESS)
      CHARGER_DEBUG((EFI_D_WARN, "Couldn't Register for Key Notification\r\n"));
  }
  else
  {
    CHARGER_DEBUG((EFI_D_ERROR, "key press monitoring disabled. \r\n"));
    Status |= SimpleInputEx->UnregisterKeyNotify(SimpleInputEx,
                                                 VolUpKeyNotifyHandle);

    Status |= SimpleInputEx->UnregisterKeyNotify(SimpleInputEx,
                                                 VolDwnKeyNotifyHandle);

    Status |= SimpleInputEx->UnregisterKeyNotify(SimpleInputEx,
                                                 HomeKeyNotifyHandle);

    if (Status != EFI_SUCCESS)
      CHARGER_DEBUG((EFI_D_WARN, "Couldn't UnRegister Key Notification\r\n"));
  }
}


VOID
ChargerPwrKeyPressControl(BOOLEAN EnableKeyControl)
{
  EFI_STATUS        Status = EFI_SUCCESS;
  EFI_KEY_DATA      NotifyKeyData;

  STATIC EFI_HANDLE                         PwrKeyNotifyHandle  = NULL;
  STATIC EFI_SIMPLE_TEXT_INPUT_EX_PROTOCOL *SimpleInputEx       = NULL;

  /* Get protocol handle if we don't have */
  if (SimpleInputEx == NULL)
  {
    Status = GetNativeKeypad(&SimpleInputEx);
    if (Status != EFI_SUCCESS)
    {
      CHARGER_DEBUG((EFI_D_WARN, "Couldn't locate SimpleInputEx protocol\r\n"));
      return;
    }
  }

  if (EnableKeyControl)
  {
    /* If we have already registered for notification
     * no more action needed */
    if (PwrKeyNotifyHandle != NULL)
      return;

    NotifyKeyData.Key.UnicodeChar = 0;
    NotifyKeyData.KeyState.KeyShiftState = 0;
    NotifyKeyData.KeyState.KeyToggleState = 0;

    NotifyKeyData.Key.ScanCode = SCAN_SUSPEND;

    if(TRUE == FullBattChargingEnabled)
    {/* if FullBattChargingEnabled is enabled, we need to boot to HLOS when power key is pressed. */
        Status |= SimpleInputEx->RegisterKeyNotify( SimpleInputEx,
                                                   &NotifyKeyData,
                                                   PwrKeyNotifyFunc,
                                                   &PwrKeyNotifyHandle);
    }
    else
    { /* If FullBattChargingEnabled is disabled, power key press should only exit LPM and
         turn on display for 7 seconds */
      Status |= SimpleInputEx->RegisterKeyNotify( SimpleInputEx,
                                                  &NotifyKeyData,
                                                  KeyNotifyFunc,
                                                  &PwrKeyNotifyHandle );
    }

    if (Status != EFI_SUCCESS)
      CHARGER_DEBUG((EFI_D_WARN, "Couldn't Register for Key Notification\r\n"));
  }
  else
  {
    Status = SimpleInputEx->UnregisterKeyNotify(SimpleInputEx,
                                                PwrKeyNotifyHandle);

    if (Status != EFI_SUCCESS)
      CHARGER_DEBUG((EFI_D_WARN, "Couldn't UnRegister Key Notification\r\n"));
  }
}


//EFI_STATUS ChargerAppWaitForAnyKey(BOOLEAN *bKeyPressed)
//{
//
//  EFI_STATUS Status = EFI_SUCCESS;
//  UINT32   ReadKeyAttrib = 0;
//  EFI_INPUT_KEY Key;
//
//  DEBUG((EFI_D_ERROR, "Check if Any key press has happend \r\n "));
//
//  //BdsLibConnectAllConsoles (); make this API as local instead of linking generic bds lib and local API should have only required changes
//  /* Init for KW */
//  ReadKeyAttrib |= READ_KEY_ATTRIB_RESET_AFTER_READ;
//  ReadKeyAttrib |= READ_KEY_ATTRIB_NO_BLOCKING;
//
//  Key.ScanCode = SCAN_NULL;
//  Key.UnicodeChar = CHAR_NULL;
//
//    // Check if HotKey is found
//  Status = ReadAnyKey (&Key, ReadKeyAttrib);
//  if(EFI_SUCCESS == Status )
//  {
//    if(Key.ScanCode != SCAN_NULL && Key.UnicodeChar != CHAR_NULL)
//    {//Timeout
//      DEBUG((EFI_D_ERROR, "** Key Pressed just Boot ** \r\n "));
//      *bKeyPressed = TRUE;
//    }
//    else
//    {
//      DEBUG((EFI_D_ERROR, "** Scan Code NULL ** \r\n "));
//    }
//  }
//  else
//  {
//    DEBUG((EFI_D_ERROR, "** Key NOT pressed Continue or Status (%d)** \r\n ", Status));
//    *bKeyPressed = FALSE;
//  }
//
//  return Status;
//}


VOID
EFIAPI
ChargerAppEnterLpm(
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
  CHARGER_DEBUG((EFI_D_ERROR, "ChargerAppEnterLpm: Signalling LPM Events. \r\n"));

  if(TRUE == DisplayOn)
  {
    DisplayOn = FALSE;
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerAppEnterLpm: Turning OFF display. \r\n"));
    gBS->SignalEvent(UIIdleEvent);
  }

  if(!ClockProtocol)
  {
    gBS->LocateProtocol(&gEfiClockProtocolGuid, NULL, (VOID **)&ClockProtocol);
  }

  if(NULL != ClockProtocol)
  {
    //ClockProtocol->EnterLowPowerMode(ClockProtocol, EFI_CLOCK_LPM_LEVEL2);
  }
}

/*
 * This function processes Charging notifications
 */

VOID
EFIAPI
ChargerAppExitLpm(
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
  CHARGER_DEBUG((EFI_D_ERROR, "ChargerAppExitLpm: Signalling exit LPM. \r\n"));

  if(!ClockProtocol)
  {
    gBS->LocateProtocol(&gEfiClockProtocolGuid, NULL, (VOID **)&ClockProtocol);
  }

  if(NULL != ClockProtocol)
  {
    //ClockProtocol->ExitLowPowerMode(ClockProtocol, EFI_CLOCK_LPM_LEVEL2);
  }

  /* Turn On Display */
  if(FALSE == DisplayOn)
  {
    DisplayOn = TRUE;
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerAppExitLpm: Turning ON display. \r\n"));
    gBS->SignalEvent(UIActiveEvent);
  }

  return;
}



char* GetImageString(UINT32 CurrentSoc)
{
  char* str = NULL;

  if (CurrentSoc <= 7)
  {
    str = AsciiStrNDup(CHARGER_BATTERY_SYMBOL_LOWBATTERY, AsciiStrLen(CHARGER_BATTERY_SYMBOL_LOWBATTERY));
  }
  else if ((CurrentSoc > 7) && (CurrentSoc < 25))
  {
    str = AsciiStrNDup(CHARGER_BATTERY_SYMBOL_SOC10, AsciiStrLen(CHARGER_BATTERY_SYMBOL_SOC10));
  }
  else if ((CurrentSoc >= 25) && (CurrentSoc < 50))
  {
    str = AsciiStrNDup(CHARGER_BATTERY_SYMBOL_SOC25, AsciiStrLen(CHARGER_BATTERY_SYMBOL_SOC25));
  }
  else if ((CurrentSoc >= 50) && (CurrentSoc < 75))
  {
    str = AsciiStrNDup(CHARGER_BATTERY_SYMBOL_SOC50, AsciiStrLen(CHARGER_BATTERY_SYMBOL_SOC50));
  }
  else if ((CurrentSoc >= 75) && (CurrentSoc < 100))
  {
    str = AsciiStrNDup(CHARGER_BATTERY_SYMBOL_SOC75, AsciiStrLen(CHARGER_BATTERY_SYMBOL_SOC75));
  }
  else if (CurrentSoc == 100)
  {
    str = AsciiStrNDup(CHARGER_BATTERY_SYMBOL_SOC100, AsciiStrLen(CHARGER_BATTERY_SYMBOL_SOC100));
  }

  return str;
}


/***************************************
        Display battery symbol on screen
***************************************/

EFI_STATUS ChgAppDispBattSymbol(CHGAPP_DISP_IMAGE_TYPE DispImage)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINTN  FileSizeFv = 0;
  UINT8  *FileBuffer = NULL;
  char   *str = NULL;

  switch (DispImage)
  {
  case CHGAPP_DISP_CHARGING_SYMBOL:
    str = GetImageString(CurrentSoc);
    break;
  case CHGAPP_DISP_BATTERY_MISSING:
    str = AsciiStrNDup(CHARGER_BATTERY_SYMBOL_NOBATTERY, AsciiStrLen(CHARGER_BATTERY_SYMBOL_NOBATTERY));
    break;
  case CHGAPP_DISP_CHARGER_MISSING:
    str = AsciiStrNDup(CHARGER_BATTERY_SYMBOL_NOCHARGER, AsciiStrLen(CHARGER_BATTERY_SYMBOL_NOCHARGER));
    break;
  case CHGAPP_DISP_MIN_SOC_THRESHOLD:
    str = AsciiStrNDup(CHARGER_BATTERY_SYMBOL_LOWBATTERY, AsciiStrLen(CHARGER_BATTERY_SYMBOL_LOWBATTERY));
    break;
  default:
    break;
  }

  if (str)
  {
    Status = LoadFromFV(str, &FileBuffer, &FileSizeFv);
    if (EFI_SUCCESS == Status)
    {
      Status = RenderBGRTImage(FileBuffer, (UINT32)FileSizeFv);
      if(EFI_SUCCESS != Status){
        DEBUG((EFI_D_ERROR, "error from RenderBGRTImage\n\r"));
        CHARGER_FILE_DEBUG((EFI_D_ERROR, "error from RenderBGRTImage\n\r"));
      }
    }
    else
    {
      DEBUG((EFI_D_ERROR, "error from LoadFromFV\n\r"));
      CHARGER_FILE_DEBUG((EFI_D_ERROR, "error from LoadFromFV\n\r"));
    }
    FreePool(str);

    if (FileBuffer)
     FreePool(FileBuffer);
     
     /* This is a bug check to make sure display is ON when we display battery symbol*/
     if(FALSE==DisplayOn)
     {
      DisplayOn = TRUE;
      gBS->SignalEvent(UIActiveEvent);
      CHARGER_DEBUG((EFI_D_WARN, "Turning ON display from ChgAppDispBattSymbol \n\r"));
     }

  }

  return Status;
}


EFI_STATUS ChgAppIdleWait(VOID)
{
  EFI_STATUS Status = EFI_SUCCESS;

  /* Two times debug print to let other folks know that charging have started*/
  DEBUG((EFI_D_ERROR, "ChargerApp Entering idle wait....\r\n"));
  CHARGER_FILE_DEBUG((EFI_D_ERROR, "ChargerApp Entering idle wait....\r\n"));

  while (TRUE)
  {

    /*3sec idle wait*/
    Status = IdleWait(IDLE_WAIT_DURATION);
    Status |= BatteryGetChargerErrors(&ChargerErrors);
    Status |= ChgAppErrorHandler(ChargerErrors);
    Status |= ChagAppCheckRtcAlarm();
    if (EFI_ERROR(Status))
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerApp (ChargerAppIdleWait) - Failed with Status (%d).\r\n", Status));
      break;
    }
  }

  DEBUG((EFI_D_ERROR, "ChargerApp Entering idle wait completed... \r\n"));
  CHARGER_FILE_DEBUG((EFI_D_ERROR, "ChargerApp Entering idle wait completed... \r\n"));

  return Status;
}


EFI_STATUS IdleWait (UINT32 WaitMilliSec)
{

  UINTN           Index;
  EFI_EVENT       TimerEvent;
  EFI_EVENT       WaitList[2];
  UINT64          TimeoutVal;
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 Timeoutms = WaitMilliSec;

  CHARGER_DEBUG((EFI_D_ERROR, "Waiting for %d Sec \r\n ", Timeoutms/1000));
  if (Timeoutms == 0)
    return EFI_INVALID_PARAMETER;

  /* Create a Timer event  */
  Status = gBS->CreateEvent (EVT_TIMER, TPL_CALLBACK, NULL, NULL, &TimerEvent);
  if (Status != EFI_SUCCESS)
    return Status;

  /* Convert to 100ns units */
  TimeoutVal = Timeoutms * 10000;

  /* Set Timer event */
  gBS->SetTimer (TimerEvent, TimerRelative, TimeoutVal);

  /* Wait for some event or the timer */
  WaitList[0] = TimerEvent;

  Status = gBS->WaitForEvent(1, WaitList, &Index);

  gBS->CloseEvent (TimerEvent);

  return Status;
}


/*This is helper function to Error Handler and performs basic error handling*/
void ChgErrorBasicErrorHandler(EFI_BATTERY_CHARGING_STATUS  ChargerErrors)
{
  if(TRUE == FullBattChargingEnabled)
  {
    gBS->SignalEvent(EfiChargerExitLPMEvent);
  }
  else if(FALSE == DisplayOn)
  {
    DisplayOn = TRUE;
    DEBUG((EFI_D_ERROR, "ChgErrorBasicErrorHandler: Turning on Display\n\r"));
    gBS->SignalEvent(UIActiveEvent);
  }

  if (ChargerErrors == EfiBatteryChargingStatusBatteryNotDetected)
  {
   if(NULL == EventShutdownInTimerCount)
    {
      ChgAppShutdownContext = CHGAPP_RESET_AFP;
      gBS->CreateEvent(EVT_TIMER | EVT_NOTIFY_SIGNAL, TPL_CALLBACK, ChargerAppShutdownInTimer, NULL, /*GUID Display guys provide*/ &EventShutdownInTimerCount);
      gBS->SetTimer(EventShutdownInTimerCount, TimerPeriodic, ONE_SEC_WAIT);
      ChgAppDispBattSymbol(CHGAPP_DISP_BATTERY_MISSING);
    }
  }
  else if (ChargerErrors == EfiBatteryChargingSourceNotDetected)
  {
    if(NULL == EventShutdownInTimerCount)
    {
      ChgAppShutdownContext = CHGAPP_RESET_SHUTDOWN;
      gBS->CreateEvent(EVT_TIMER | EVT_NOTIFY_SIGNAL, TPL_CALLBACK, ChargerAppShutdownInTimer, NULL, /*GUID Display guys provide*/ &EventShutdownInTimerCount);
      gBS->SetTimer(EventShutdownInTimerCount, TimerPeriodic, ONE_SEC_WAIT);
      ChgAppDispBattSymbol(CHGAPP_DISP_CHARGER_MISSING);
    }
  }
  else
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerApp Error (%d).\r\n", ChargerErrors));
    gBS->Stall(5000000);
  }

  return;
}


/*for each ChargerError type take default action and defined type of shutdown*/
EFI_STATUS ChgAppErrorHandler(EFI_BATTERY_CHARGING_STATUS  ChargerErrors)
{
  EFI_STATUS Status = EFI_SUCCESS;

  switch (ChargerErrors)
  {
  case EfiBatteryChargingStatusNone:
  case EfiBatteryChargingStatusSuccess:
    /*Don't do anything for these two cases*/
    break;

  case EfiBatteryChargingStatusBatteryNotDetected:
    ChgErrorBasicErrorHandler(ChargerErrors);
    break;
  case EfiBatteryChargingSourceNotDetected:
    if ((CurrentSoc < ChargeBatteryTo) || (TRUE == FullBattChargingEnabled))
    {
      ChgErrorBasicErrorHandler(ChargerErrors);
    }
    break;
  case EfiBatteryChargingSourceCurrentInvalid:
  case EfiBatteryChargingStatusCurrentOutOfRange:
  case EfiBatteryChargingSourceVoltageInvalid:
  case EfiBatteryChargingStatusVoltageOutOfRange:
    ChgErrorBasicErrorHandler(ChargerErrors);
    ChargerLibForceSysShutdown(CHGAPP_RESET_SHUTDOWN);
    break;

  case EfiBatteryChargingStatusExtremeCold:
  case EfiBatteryChargingStatusOverheat:
    ChgErrorBasicErrorHandler(ChargerErrors);
    ChargerLibForceSysShutdown(CHGAPP_RESET_AFP);
    break;
  case EfiBatteryChargingTempBelowChargeLimit:
    if (CurrentSoc < ThresholdSoc)
    {
      ChargerLibForceSysShutdown(CHGAPP_RESET_AFP);
    }
    else
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerApp ChgAppErrorHandler Disable charging and Re-Boot to HLOS if Full Charging mode (%d) \r\n", ChargerErrors));
      /*Reset and continue to boot to HLOS */
      Status |= ChargerEnable(FALSE);
      ChgAppFlushVariable(EXIT_CHARGERDXE, TRUE);
      ChargerLibForceSysShutdown(CHGAPP_RESET_COLD);
    }
    break;
  case EfiBatteryChargingTempCoolOff:
    if (CurrentSoc > ThresholdSoc)
    {
      /*Charging will be disabled during JEITA state transiotion */
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerApp ChgAppErrorHandler Disable charging and Re-Boot to HLOS if Full Charging mode (%d)\r\n", ChargerErrors));
      ChgAppFlushVariable(EXIT_CHARGERDXE, TRUE);
      ChargerLibForceSysShutdown(CHGAPP_RESET_COLD);
    }
    else
    {
      /*Wait for device to cool off*/
      ChargerLibWaitToCoolOff();
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerApp ChgAppErrorHandler JEita COOL OFF (%d)\r\n", ChargerErrors));
    }
    break;
  default:
    ChgErrorBasicErrorHandler(ChargerErrors);
    ChargerLibForceSysShutdown(CHGAPP_RESET_SHUTDOWN);
    break;
  }

  if (EFI_SUCCESS != Status)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerApp - Failed to handle charger Error:(%d) and Status:(%d).\r\n", ChargerErrors, Status));
  }

  return Status;
}

/*After Charging is completed, handle cases for threshold charging and full battery charging*/
EFI_STATUS ChgAppChargedPostProcess(ChgAppChargingType ChargingType)
{
  EFI_STATUS Status = EFI_SUCCESS;

  switch (ChargingType)
  {
    case CHGAPP_FULLBATTERY_CHARGING:
      //if we were sticky charging, reset device after we reach SOC 100
      /*Disable charging and exit LPM */
      Status = ChargerEnable(FALSE);
      gBS->SignalEvent(EfiChargerExitLPMEvent);
      //Register for power key
      if (FALSE == DisplayOn)
      {
        KeyPressCounter = SEVEN_SEC_COUNT;
        ChgAppDispBattSymbol(CHGAPP_DISP_CHARGING_SYMBOL);
      }
      Status = ChgAppIdleWait();
      break;
    case CHGAPP_THRESHOLD_CHARGING:
      //Re-enabling charging so that charging is enabled when booting from UEFI to HLOS
      Status = ChargerEnable(TRUE);
      if (FALSE == DisplayOn)
      {
        DisplayOn = TRUE;
        DEBUG((EFI_D_ERROR, "ChgAppChargedPostProcess: CHGAPP_THRESHOLD_CHARGING Turning on Display\n\r"));
        gBS->SignalEvent(UIActiveEvent);
        ChgAppDispBattSymbol(CHGAPP_DISP_CHARGING_SYMBOL);
      }
      break;
    case CHGAPP_INVALID_CHARGING:
    default:
    ChargerLibForceSysShutdown(CHGAPP_RESET_COLD);
      break;
  }

  return Status;
}

/*This function monitors charging until battery SoC reaches to Target SoC*/
EFI_STATUS ChgAppStartCharging(uint32 TargetSoc)
{
  EFI_STATUS Status = EFI_SUCCESS;

  /* Start Battery Status Report Timer */
  Status = gBS->CreateEvent(EVT_TIMER | EVT_NOTIFY_SIGNAL, TPL_CALLBACK, ChgAppReportBatteryStatus, NULL, &EventChgAppReportBatteryStatus);
  Status |= gBS->SetTimer(EventChgAppReportBatteryStatus, TimerPeriodic, FIVE_SEC_WAIT);

  /* Initialize Sleep Init */
  /* SleepPerformLPM not working currently and so commenting it at the moment */
  //Status |= SleepInit();

  while (FALSE == ChargingLoopExit)
  {
    //CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Enter SleepPerformLPM.. CurrentSoC: %d , ChargeBatteryTo %d ., TargetSoc: %d \r\n", CurrentSoc, ChargeBatteryTo, TargetSoc));

    /*CPU sleep*/
    //SleepPerformLPM();

    /* SleepPerformLPM not working currently and so reverting so Stall */
    /*check for RTC alarm status*/
    ChagAppCheckRtcAlarm();
    gBS->Stall(5000000);
  }

  /*Close event for Battery Status Report Timer*/
  if (NULL != EventChgAppReportBatteryStatus)
  {
    Status = gBS->CloseEvent(EventChgAppReportBatteryStatus);
  }

  return Status;
}


/*This will be called every 5 seconds*/
VOID EFIAPI ChgAppReportBatteryStatus(IN EFI_EVENT Event, IN VOID *Context)
{
  EFI_STATUS Status = EFI_SUCCESS;
  INT32      ChargeCurrent   = 0;
  UINT32     StateOfCharge   = CurrentSoc;
  UINT32     RatedCapacity   = 0;

  Status = ChargerCheckBatteryStatus(&StateOfCharge, &RatedCapacity, &ChargeCurrent, &ChargerErrors);
  CurrentSoc = StateOfCharge;

  Status |= ChgAppErrorHandler(ChargerErrors);
  /*below error requires device to continue this loop and are handled separately*/
  if ((ChargerErrors != EfiBatteryChargingStatusNone)    &&
      (ChargerErrors != EfiBatteryChargingStatusSuccess) &&
      (ChargerErrors != EfiBatteryChargingTempCoolOff) &&
      (ChargerErrors != EfiBatteryChargingSourceNotDetected)&&
      (ChargerErrors != EfiBatteryChargingStatusBatteryNotDetected))
  {
    ChargingLoopExit = TRUE;
  }
  else if ((ChargerErrors == EfiBatteryChargingStatusSuccess) || (ChargerErrors == EfiBatteryChargingStatusNone))
  {
    /*If no error returned pet charger watchdog*/
    //Not checking the return status of the function, because if there is any issue, HW will disable charging
    //automatically and we do not need to take any action.
    ChargerLibPetChgWdog();
  }
  
  if(CurrentSoc >= ChargeBatteryTo)
  {
    Status |= ChargerEnable(FALSE);
    ChargingLoopExit = TRUE;
    Status = gBS->CloseEvent(EventChgAppReportBatteryStatus);
  }
  
}



EFI_STATUS ChgAppInit(void)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32     MaxCurrent      = 1500;
  EFI_GUID   UIActiveEventGuid  = EFI_UI_ACTIVE_EVENT_GUID;
  EFI_GUID   UIIdleEventGuid    = EFI_UI_IDLE_EVENT_GUID;

  Status = ChargerAppDisplayTimerEvent(TRUE);
  Status |= gBS->CreateEventEx(EVT_NOTIFY_SIGNAL, TPL_CALLBACK, ChargerAppDisplayCallback, NULL, &UIActiveEventGuid, &UIActiveEvent);
  Status |= gBS->CreateEventEx(EVT_NOTIFY_SIGNAL, TPL_CALLBACK, ChargerAppDisplayCallback, NULL, &UIIdleEventGuid, &UIIdleEvent);

  if(TRUE == FullBattChargingEnabled)
  {
    Status |= gBS->CreateEventEx(EVT_NOTIFY_SIGNAL, TPL_CALLBACK, ChargerAppEnterLpm, NULL, &gEfiEventEnterLPMGuid, &EfiChargerEnterLPMEvent);
    Status |= gBS->CreateEventEx(EVT_NOTIFY_SIGNAL, TPL_CALLBACK, ChargerAppExitLpm, NULL, &gEfiEventExitLPMGuid, &EfiChargerExitLPMEvent);
  }

  if (EFI_SUCCESS != Status)
  {
    DEBUG((EFI_D_ERROR, "ChargerApp - InitQcomChargerApp Failed ! Error Code: 0x%08X\r\n", Status));
  }


  if(EFI_SUCCESS == ChargerGetMaxCurrent(&MaxCurrent))
  {
    Status = ChargerSetMaxUsbCurrent(MaxCurrent);
  }

  Status = ChargerEnable(TRUE);

  /* i) Register for key press monitoring,
     ii)set timer for every 1 second,
     */
  ChargerKeyPressControl(TRUE);
  ChargerPwrKeyPressControl(TRUE);

  gBS->SetTimer(EventDisplayOffCount, TimerPeriodic, 10000000);

  /* indicate charging started */
  Status |= ChgAppDispBattSymbol(CHGAPP_DISP_CHARGING_SYMBOL);

  return Status;
}


/*This API initializes charger app, charges battery and does post processing after charging is complete*/
EFI_STATUS RunQcomChargerApp(ChgAppConfig ChargerConfig)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32     StateOfCharge   = 0;
  BOOLEAN    AlarmExpired = FALSE;

  FullBattChargingEnabled = ChargerConfig.FullBattChargingEnabled;
  ThresholdSoc = ChargerConfig.SocThreshold;

  if(ChargerConfig.OneTimeStickyChargingEnabled)
  {
    FullBattChargingEnabled = TRUE;
  }
  /* Get Battery SoC to decide if charging required*/
  if (EFI_SUCCESS != BatteryGetSOC(&StateOfCharge))
  {
    DEBUG((EFI_D_ERROR, "ChargerApp - Failed to get SOC! Error Code: 0x%08X\r\n", Status));
    return Status;
  }

  CurrentSoc = StateOfCharge;
  CHARGER_DEBUG((EFI_D_ERROR, "ChargerApp - StateOfCharge: (%d),  MinSocThreshold: (%d), FullBattChargingEnabled: (%d) \r\n",
                     StateOfCharge, ThresholdSoc, FullBattChargingEnabled));

  /*Read Error */
  Status = BatteryGetChargerErrors(&ChargerErrors);
  /*if temperature is outside charging range but SoC is more than threshold then should boot */
  if (((EfiBatteryChargingTempBelowChargeLimit == ChargerErrors) ||
       (EfiBatteryChargingTempCoolOff == ChargerErrors)) &&
       (StateOfCharge > ThresholdSoc)
      )
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerApp - StateOfCharge: (%d), ChargerErrors: (%d) Boot to HLOS since SOC is good \r\n",
                  StateOfCharge, ChargerErrors));
    return Status;
  }
  
  Status = ChargerLibAlarmExpireStatus(&AlarmExpired);
  if((TRUE == AlarmExpired) && (StateOfCharge > ThresholdSoc))
  {
    DEBUG((EFI_D_WARN, "ChargerApp - RTC Alarm Expired. Continue to boot.. \r\n"));
    /*exit and boot to tiles*/
    return Status;
  }

  /*Charge if SOC is less than threshold or full battery charging enabled*/
  if ((StateOfCharge < ThresholdSoc) || (FullBattChargingEnabled && (StateOfCharge < FULL_BATTERY_SOC)))
  {
    /*If Sticky charging is enabled and charger is not connected but battery is charged above threshold, and also onetimesticky charging is not enabled then,
      let device boot to HLOS*/
    if((EfiBatteryChargingSourceNotDetected == ChargerErrors) &&
        FullBattChargingEnabled &&
       (StateOfCharge > ThresholdSoc)&&
       (ChargerConfig.OneTimeStickyChargingEnabled == FALSE)
       )
    {
        DEBUG((EFI_D_INFO, "Battery above threshold. Booting to Tiles."));
        return Status;
    }

    /*handle Error appropriately */
    Status = ChgAppErrorHandler(ChargerErrors);

    /*Initializes and displays the battery symbol*/
    Status |= ChgAppInit();

    /*set charger target SoC*/
    ChargeBatteryTo = (FullBattChargingEnabled) ? FULL_BATTERY_SOC : ThresholdSoc;

    /*if initialization success then start charging upto target SoC*/
    if(EFI_SUCCESS == Status)
    {
      Status = ChgAppStartCharging(ChargeBatteryTo);
    }
    else
    {
      DEBUG((EFI_D_ERROR, "ChargerApp - Charger Initialization error : (%d) \r\n", Status));
      CHARGER_FILE_DEBUG((EFI_D_ERROR, "ChargerApp - Charger Initialization error : (%d) \r\n", Status));
      return Status;
    }

    /* Post processing after Charging completed */
    if (FullBattChargingEnabled)
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerApp - Post Process FullBattChargingEnabled (%d) \r\n", FullBattChargingEnabled));
      Status |= ChgAppChargedPostProcess(CHGAPP_FULLBATTERY_CHARGING);
    }
    else
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerApp - Charged Post Process FullBattChargingEnabled (%d) \r\n", FullBattChargingEnabled));
      Status |= ChgAppChargedPostProcess(CHGAPP_THRESHOLD_CHARGING);
    }
  }
  else
  {
    /* if full charging enabled then wait for power key press to boot further. Otherwise continue booting */
    if (FullBattChargingEnabled)
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerApp - Battery fully Charged \r\n"));
      Status |= ChgAppInit();
      Status |= ChgAppChargedPostProcess(CHGAPP_FULLBATTERY_CHARGING);
    }
  }

  /* de-register Key control */
  ChargerKeyPressControl(FALSE);
  ChargerPwrKeyPressControl(FALSE);

  return Status;
}


static EFI_STATUS ChagAppCheckRtcAlarm(void)
{
  EFI_STATUS Status = EFI_SUCCESS;
  BOOLEAN ExpireStatus = FALSE;
  Status = ChargerLibAlarmExpireStatus(&ExpireStatus);
  if (ExpireStatus)
  {
   if(CurrentSoc > ThresholdSoc)
    {
      if(FullBattChargingEnabled)
      {
        DEBUG((EFI_D_WARN, "RTC Alarm expired. Exit Charging \r\n"));
        CHARGER_DEBUG((EFI_D_WARN, "RTC Alarm expired. Exit Charging \r\n"));
       /*set the variable and reset the device*/
       ChgAppFlushVariable(EXIT_CHARGERDXE, TRUE);
       ChargerLibForceSysShutdown(CHGAPP_RESET_COLD);
      }
    }
  }
  
  return Status;
}




