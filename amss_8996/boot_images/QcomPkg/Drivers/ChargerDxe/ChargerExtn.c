/** @file ChargerExtn.c

  Implements the Charger Extension Protocol
  
  Copyright (c) 2013, Qualcomm Technologies Inc. All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY


 when         who     what, where, why
 --------     ---     -----------------------------------------------------------
 10/04/12     dy      New File

=============================================================================*/
#include <Uefi.h>

/**
  EFI interfaces
 */
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>

/**
  Charger Extn PROTOCOL interface
 */
#include "Protocol/EFIChargerExtn.h"

#include "ChargerLib.h"

/*===========================================================================*/
/*                  FUNCTIONS PROTOTYPES                                     */
/*===========================================================================*/

EFI_STATUS EFI_ChargerGetPowerPath( OUT EFI_CHARGER_ATTACHED_CHGR_TYPE *PowerPath );
EFI_STATUS EFI_ChargerGetBatteryPresence( OUT BOOLEAN *BatteryPresence );

/**
  Charger Extension UEFI Protocol implementation
 */
EFI_BATTERY_CHARGING_EXTN_PROTOCOL ChargerExtnProtocolImplementation = 
{
  EFI_ChargerGetPowerPath,
  EFI_ChargerGetBatteryPresence
};

/**
  GetPowerPath ()

  @brief
  GetPowerPath implementation of EFI_QCOM_CHARGER_EXTN_PROTOCOL
 */
EFI_STATUS
EFI_ChargerGetPowerPath
( 
  OUT EFI_CHARGER_ATTACHED_CHGR_TYPE *PowerPath
)
{
  EFI_STATUS                  Status        = EFI_SUCCESS;
  
  /* Forward command to ChargerLib function */
  Status = BatteryGetPowerPath(PowerPath);
  
  return Status;
}

/**
  GetBatteryPresence ()

  @brief
  GetBatteryPresence implementation of EFI_QCOM_CHARGER_EXTN_PROTOCOL
 */
EFI_STATUS
EFI_ChargerGetBatteryPresence
( 
  OUT BOOLEAN *BatteryPresence
)
{
  EFI_STATUS                  Status        = EFI_SUCCESS;
  
  /* Forward command to ChargerLib function */
  Status = BatteryGetBatteryPresence(BatteryPresence);
  
  return Status;
}
