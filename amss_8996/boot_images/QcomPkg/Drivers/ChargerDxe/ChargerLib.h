/** 
  @file  ChargerLib.h
  @brief Charger Lib API definitions.
*/
/*=============================================================================
  Copyright (c) 2011-2015 QUALCOMM Technologies Incorporated.
  All rights reserved.
  Qualcomm Confidential and Proprietary.
=============================================================================*/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 06/04/15   al      Adding API to check alarm expiration status
 05/21/15   sm      Added API to pet charger watchdog
 04/30/15   va      IChgMax and VddMax configuration support for all Jeita windows
                    JEITA Low temp, high temp handle
 04/30/15   al      Adding USB Suspend API
 04/09/15   va      JEITA algo Fix - Fcc and Fv Max to be set when temperature reading is available (8994)
 03/31/15   va      Charger Log file size to 7 MB
 01/14/15   al      Adding shutdown and battery error handling to ChargerLib
 12/16/14   al      Adding AFP mode support
 12/04/14   sm      Changed API definition for ChargerInit
                          Added ChargerCheckBatteryStatus  and ChargerGetMaxCurrent APIs
 10/25/14   sv      Updates SW Jeita Feature for 8909
 10/26/14   vk      Remove inline
 09/29/14   va      Added SW Jeita Feature
 08/21/14   va      Enable File Logging 
 08/20/14   sv      Updated the Gauge HW enum.
 08/04/14   sm      Added SMBCHG and FG protocols and removed unused protocols
 01/31/14   ps      Added support for Linear battery charger (LBC) and BMS voltage mode
 03/06/13   dy      Added Charger Extension APIs
 03/05/13   sm      Added EfiBatteryGaugeBq30z55 in EFI_BATTERY_GAUGE_HW_TYPE
 10/09/12   dy      Created

=============================================================================*/
#ifndef __CHARGERLIB_H__
#define __CHARGERLIB_H__

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
/**
  Charger PROTOCOL interface
 */
#include "Protocol/EFICharger.h"
#include "Protocol/EFIChargerExtn.h"

/**
File Logging Dependencies 
*/
#include "ULogFront.h"
#include "ULogUEFI.h"

#include "ChargerApp.h"
/*===========================================================================
                     MACRO DEFINATIONS
===========================================================================*/
//Charger Log File Name
#define LOGFILE_IN_EFS "ChargerLog"
#define LOGFILE_SIZE   8630272 // 7MB


#define _ULOG(Expression)   ULogPrint Expression

#if !defined(CHARGER_DEBUG)      
  #define CHARGER_DEBUG(Expression)        \
    do {                           \
      if (ChargerLib_PrintDebugMsg ()) {  \
        _DEBUG (Expression);       \
      }                            \
      if(ChargerLib_PrintDebugMsgToFile()) {  \
        _ULOG (Expression);         \
      }                            \
    } while (FALSE)
  #define CHARGER_FILE_DEBUG(Expression)        \
    do {                           \
      if(ChargerLib_PrintDebugMsgToFile()) {  \
        _ULOG (Expression);         \
      }                            \
    } while (FALSE)
  #define CHARGER_UART_DEBUG(Expression)        \
    do {                           \
      if (ChargerLib_PrintDebugMsg ()) {  \
        _DEBUG (Expression);       \
      }                            \
    } while (FALSE)
#else
  //#define CHARGER_DEBUG(Expression)
#endif

/*===========================================================================
                     TYPE DECLARATIONS
===========================================================================*/
/**
  Charger HW
*/
typedef enum _EFI_BATTERY_CHARGER_HW_TYPE { 
  EfiBatteryChargerNone,
  /**< No charger. */
  
  EfiBatteryChargerQcomPmicSmbChg,
  /**< PMIC SMBCHG Charger. */

  EfiBatteryChargerQcomPmicLbc,
  /**< PMIC Linear Battery Charger. */
  
  EfiBatteryChargerSMB1360,
  /**< SMB1360 Charger. */

} /** @cond */ EFI_BATTERY_CHARGER_HW_TYPE /** @endcond */;

/**
  JEITA Config Data.
*/
typedef struct 
{

  INT32    BattTempLimLow;
  /**< Lower limit of Batery temperature value */
  
  INT32    BattTempLimHigh;
  /**< Upper limit of Batery temperature value */
  
  UINT32   ChgFvMax;
  /**< Value of FV_MAX in mV */
  
  UINT32   ChgFccMax;
  /**< Value of FCC_MAX in mA */
  
  UINT32   WPBattVoltLimHighInmV;
  /**< Max Value for battery voltage in mV for WP Platform*/  
  
  INT32   WPBattCurrLimHighInmA;
  /**< Max value for battery current in mA for WP Platform*/

  BOOLEAN  SWJeitaEnable;
  /**< SW jeita enable/disable */

   /**< Low battery Temperature upto which battery can be charged */
  INT32  BattTempLimLowCharging;

  INT32  BattTempJeitaT1Limit;
  /**< JEITA Standard Lower limit of the battery temperature Limit value.for controlling Maximum current (FCC_MAX) */

  INT32  BattTempJeitaT2Limit;
  /**< JEITA Standard Lower limit of the battery temperature Limit value.for controlling Maximum current (FCC_MAX) */

  INT32  BattTempJeitaT3Limit;
  /**< JEITA Standard normal limit for Maximum current (FCC_MAX) */

  INT32  BattTempJeitaT4Limit;
  /**< JEITA Standard Higher limit of the battery temperature Limit value.for controlling Maximum voltage (FV_MAX) */

  INT32  BattTempJeitaT5Limit;
  /**< JEITA Standard High limit of the battery temperature Limit value.for controlling Maximum voltage (FV_MAX) */

  INT32  BattTempHysteresis;
  /**< Battery temperature Hysteresis Limit value.for Jeita zones */

  UINT32  ChgFccMaxInJeitaT2Limit;
  /**< Fast Charging current Max Limit for JEITA Till T2 Limit */
  UINT32  ChgFccMaxInJeitaT3Limit;
  /**< Fast Charging current Max Limit for JEITA Till T3 Limit */
  UINT32  ChgFccMaxInJeitaT4Limit;
  /**< Fast Charging current Max Limit for JEITA Till T4 Limit */
  UINT32  ChgFccMaxInJeitaT5Limit;
  /**< Fast Charging current Max Limit for JEITA Till T5 Limit */

  UINT32  ChgFvMaxInJeitaT2Limit;
  /**< Floating Voltage Max Limit for JEITA Till T2 Limit */
  UINT32  ChgFvMaxInJeitaT3Limit;
  /**< Floating Voltage Max Limit for JEITA Till T3 Limit */
  UINT32  ChgFvMaxInJeitaT4Limit;
  /**< Floating Voltage Max Limit for JEITA Till T4 Limit */
  UINT32  ChgFvMaxInJeitaT5Limit;
  /**< Floating Voltage Max Limit for JEITA Till T5 Limit */

  UINT32 ConservChgFccMaxInJeitaT2Limit;
  /**< Fast Charging current Max Limit for JEITA Till T2 Limit */
  UINT32 ConservChgFccMaxInJeitaT3Limit;
  /**< Fast Charging current Max Limit for JEITA Till T3 Limit */
  UINT32 ConservChgFccMaxInJeitaT4Limit;
  /**< Fast Charging current Max Limit for JEITA Till T4 Limit */
  UINT32 ConservChgFccMaxInJeitaT5Limit;
  /**< Fast Charging current Max Limit for JEITA Till T5 Limit */

  UINT32 ConservChgFvMaxInJeitaT2Limit;
  /**< Floating Voltage Max Limit for JEITA Till T2 Limit */
  UINT32 ConservChgFvMaxInJeitaT3Limit;
  /**< Floating Voltage Max Limit for JEITA Till T3 Limit */
  UINT32 ConservChgFvMaxInJeitaT4Limit;
  /**< Floating Voltage Max Limit for JEITA Till T4 Limit */
  UINT32 ConservChgFvMaxInJeitaT5Limit;
  /**< Floating Voltage Max Limit for JEITA Till T5 Limit */

  UINT64 ChargerCoolOffPeriodMs;
  /**< Cool off period in milli sec when temperature is above BattTempJeitaT5Limit and less than BattTempLimHigh.*/
  
}EFI_PM_JEITA_CFG_DATA_TYPE;


/**
  Charger Info
*/
typedef struct _EFI_BATTERY_CHARGER_INFO_TYPE {
  EFI_BATTERY_CHARGER_HW_TYPE   ChargerHW;
  /**< Charger HW. */
  UINT32                        Version;
  /**< HW Version. */
} /** @cond */ EFI_BATTERY_CHARGER_INFO_TYPE /** @endcond */;



/**
  Battery Gauge HW
*/
typedef enum _EFI_BATTERY_GAUGE_HW_TYPE { 
  EfiBatteryGaugeNone,
  /**< No battery gauge. */
  
  EfiBatteryGaugeQcomPmicFg,
  /**< PMIC FG. */

  EfiBatteryGaugeQcomPmicBmsVm,
  /**< PMIC BMS Voltage Mode  */

  EfiBatteryGaugeSMB1360
} /** @cond */ EFI_BATTERY_GAUGE_HW_TYPE /** @endcond */;


/**
  Battery Gauge Info
*/
typedef struct _EFI_BATTERY_GAUGE_INFO_TYPE {
  EFI_BATTERY_GAUGE_HW_TYPE     BatteryGaugeHW;
  /**< Battery Gauge HW. */
  UINT32                        Version;
  /**< HW Version. */
} /** @cond */ EFI_BATTERY_GAUGE_INFO_TYPE /** @endcond */;

/**
  Battery JEITA Temperature State
*/
typedef enum _EFI_BATTARY_JEITA_TEMP_STATE{

    JEITA_STATE_INIT = 0,

    /*Temperature is less than equal to 0 */
    JEITA_STATE_T1_LOW_TEMP,

    /* Greater than 0 and Less than equal to 10 */
    JEITA_STATE_T1_T2,

    /* Greater than 10  and Less than equal to 45 */
    JEITA_STATE_T2_T3, 

    /* Greater than 45 and Less than equal to 50 */
    JEITA_STATE_T3_T4,

    /* Greater than 50  and Less than 60 */
    JEITA_STATE_T4_T5,

    /* Greater than 60  and Less than equal to 70 */
    JEITA_STATE_COOL_OFF,

  /* Grater than 70 */
    JEITA_STATE_MAX_JEITA_T,

    /* Invalid State */
    JEITA_STATE_INVALID,

    JEITA_STATE_FORCE_32_BITS = 0x7FFFFFFF

}EFI_BATTARY_JEITA_TEMP_STATE;


/**
  Battery JEITA Temperature Info
*/
typedef struct _EFI_BATTERY_JEITA_T_INFO {

  EFI_BATTARY_JEITA_TEMP_STATE JeitaTState;
  /**< JEITA Temperature Current State. */

  EFI_BATTARY_JEITA_TEMP_STATE JeitaPrevTState;
  /**< JEITA Temperature Previous State. */

  EFI_BATTARY_JEITA_TEMP_STATE JeitaNextTState;
  /**< JEITA Temperature Next State. */

  BOOLEAN  IsFccFvMaxAdjustedT1T2;
  /**< Indicate if IBAT  MAx is Adjusted  */

  BOOLEAN  IsFvFccMaxAdjustedT2T3;
  /**< Indicate if VDD MAx is Adjusted  */

  BOOLEAN  IsFvFccMaxAdjustedT3T4;
  /**< Indicate if VDD MAx is Adjusted  */

  BOOLEAN  IsFvFccMaxAdjustedT4T5;
  /**< Indicate if VDD MAx is Adjusted  */

  BOOLEAN  IsFvMaxAdjustedT5;
  /**< Indicate if T5 is Adjusted  */

} /** @cond */ EFI_BATTERY_JEITA_T_INFO /** @endcond */;

typedef enum{
  CHG_BATT_TYPE_NORMAL =0,
  /**< Battery with id within normal battery range. */
  CHG_BATT_TYPE_SMART =1,
  /**< Battery with id within smart battery range. */
  CHG_BATT_TYPE_BATT_EMULATOR =2,
  /**< Battery with within defined dummy batt id range. */
  CHG_BATT_TYPE_DEBUG_BOARD =3,
  /**< DEBUG board */
  CHG_BATT_TYPE_UNKNOWN =4,
  /**< Battery with id outside any defined range. */
  CHG_BATT_TYPE_INVALID = 0x7FFFFFFF
} ChgBattType;


typedef enum
{
  CHGAPP_RESET_NONE,
  /**< No reset */
  CHGAPP_RESET_SHUTDOWN,
  /**< Normal shutdown */
  CHGAPP_RESET_AFP,
  /**< AFP triggered */
  CHGAPP_RESET_COLD,
  /**< cold reset */
  CHGAPP_RESET_INVALID
} ChgAppSysShutdownType; 


typedef enum
{
  /*ChargerLib_Shutdown_AFP*/
  CHARGERLIB_HANDLE_SHUTDOWN,
  /* ChargerLib_Charger*/
  CHARGERLIB_HANDLE_CHARGE,
  /*ChargerLib_ContinueBoot*/
  CHARGERLIB_HANDLE_CONTINUE_BOOT,
  /*Invalid error*/
  CHARGERLIB_HANDLE_INVALID
}ChargerLibHandleType;


EFI_STATUS ChargerLibInit(ChgAppConfig *ChargerConfig);

EFI_STATUS BatteryGetSOC(UINT32 *StateOfCharge);

EFI_STATUS BatteryGetRatedCapacity(UINT32 *RatedCapacity);

EFI_STATUS BatteryGetChargeCurrent(INT32 *ChargeCurrent);

EFI_STATUS BatteryGetChargerErrors(EFI_BATTERY_CHARGING_STATUS *ChargerErrors);

EFI_STATUS ChargerEnable(BOOLEAN Enable);

EFI_STATUS ChargerSetMaxUsbCurrent(UINT32 MaxCurrent);

EFI_STATUS BatteryGetPowerPath(EFI_CHARGER_ATTACHED_CHGR_TYPE *PowerPath);

EFI_STATUS BatteryGetBatteryPresence(BOOLEAN *BatteryPresence);

BOOLEAN EFIAPI ChargerLib_PrintDebugMsg(void);

BOOLEAN EFIAPI ChargerLib_PrintDebugMsgToFile(void);

VOID ULogPrint (IN  UINTN ErrorLevel, IN  CONST CHAR8  *Format,  ...);

EFI_STATUS ChargerLogParams(UINT32 *StateOfCharge, UINT32 *RatedCapacity, INT32 *ChargeCurrent);

EFI_STATUS ChargerGetMaxCurrent(UINT32 *pMaxCurrent);

EFI_STATUS ChargerCheckBatteryStatus
( OUT UINT32                       *StateOfCharge,
  OUT UINT32                       *RatedCapacity,
  OUT INT32                        *ChargeCurrent,
  OUT EFI_BATTERY_CHARGING_STATUS  *ChargerErrors
);


EFI_STATUS ChargerLibEnableAfpMode(void);

/*This API resets the device.*/
EFI_STATUS ChargerLibForceSysShutdown(ChgAppSysShutdownType ShutdownType);

/*This API returns ChargerLibError type*/
EFI_STATUS ChargerLibBatteryErrorHandler(ChargerLibHandleType *ChargerLibError);

/*disable charging and put device in hold off status for specified period*/
EFI_STATUS ChargerLibWaitToCoolOff(void);

/*Get's The status of One time sticky charging */
EFI_STATUS ChargerGetOnetimeStickyChargingStatus(BOOLEAN* Enabled);

/* API to enable and disable USB SUSPEND mode */
EFI_STATUS ChargerLibUsbSuspend(BOOLEAN Enable);

/* API to pet charger watchdog */
EFI_STATUS ChargerLibPetChgWdog( void );

/* API to RTC ALARM expire status */
EFI_STATUS ChargerLibAlarmExpireStatus(BOOLEAN *ExpireStatus);

#endif  /* __CHARGERLIB_H__ */
