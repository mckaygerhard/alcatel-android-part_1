/*! @file PmicSmbchgProtocol.c 

*  PMIC- SMBCHG MODULE RELATED DECLARATION
*  This file contains functions and variable declarations to support 
*  the PMIC SMBCHG (Switch Mode Battery Charger and Boost) module.
*
*  Copyright (c) 2014 -2015 Qualcomm Technologies, Inc.  All Rights Reserved.
*  Qualcomm Technologies Inc Proprietary and Confidential.
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     --------------------------------------------------------------------------
05/12/15   mr      Enabled Charger functionality for 8952 (CR-846387)
05/21/15   sm      Added API to pet charger watchdog
05/08/15   sm      Added changes in SmbchgSetUsbMaxCurrent() to enable HC mode for AICL
                   and disable AICL before setting ICL and re-enable.
04/30/15   al      Make FCC calibration execution configurable
04/30/15   al      Adding USB suspend API
04/20/15   al      FCC Hardlimit check added
04/02/15   al      FCC calibration
04/09/15   va      JEITA algo Fix - Do not set Fcc and Fv Max during Init
02/20/15   sm      Added "ESR pulse missing during CC charge" workaround
02/18/15   al      Fix for api to set USB max current
02/09/15   al      Add API to disable AICL
02/03/15   al      Chaging JEITA struct
01/07/15   va      Set AICL deglitch time
11/17/14   al      Adding i)API to read RID status and enable/disable OTG via command register
                   ii)SW WA while enabling OTG
11/12/14   va      Added SW Jeita feature
11/04/14   sm      Added changes to configure ICL for 500mA for SDP port 
10/21/14   sm      Added API to enable ChargerWatchdog and
                   changes to enable/disable charging by CMD control
09/23/14   al      Adding API for charger path priority  
08/04/14   sm      Added APIs to enable charging and set USB Max Current
08/11/14   al      Adding API for AFP mode
06/09/14   al      Arch update
05/09/14   va      Correcting PmicDeviceIndex Argument
04/29/14   al      Added API to config OTG and set current limit 
04/18/14   al      Updated copyright 
04/07/14   al      New file.
===========================================================================*/

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/

#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>

#include "pm_uefi.h"
#include "pm_version.h"
#include "pm_smbchg_usb_chgpth.h"
#include "pm_smbchg_dc_chgpth.h"
#include "pm_smbchg_misc.h"
#include "pm_smbchg_bat_if.h"
#include "pm_smbchg_chgr.h"
#include "pm_smbchg_otg.h"

#include <Protocol/EFIPmicSmbchg.h>
#include "Fg/PmicFgBattParameter.h"
#include <Library/DebugLib.h>
#include "PmicFileLog.h"
/**
  PMIC Lib interfaces
 */
#include "pm_uefi.h"

/*initalizing charging termination current with 100 milli amp*/
static UINT32 ITERM = 100;

static UINT32 OldFastChgCurrent = 0;
static UINT32 ChargerPmicIndex = 0;
EFI_PM_FG_CFGDATA_TYPE BatteryCfgData;
STATIC UINT32 InitialFccMa = 0;

#define ESRWA_MIN_CURRENT  200 // in mA
#define FCC_CALIBRATION_COFIRMED 2
#define ESR_PULSE_WA_TIMER_DURATION  1000000000
#define FCC_CALIBRATION_TIMER_DURATION 15000000   //1.5sec

EFI_STATUS
EFIAPI
PmicSmbchgEnableWdog
(
  IN UINT32  PmicDeviceIndex
);

EFI_EVENT SmbChgResetFccEvent;
EFI_EVENT SmbChgEsrPulseWAEvent;
EFI_EVENT SmbChgFccCalibrationEvent = NULL;

/*SW WA while enabling OTG with high load capacitance*/
static pm_err_flag_type PmicSmbchgOtgHighCapWorkArnd(IN  UINT32  PmicDeviceIndex);

VOID EFIAPI SmbChgEsrPulseWAEventHandler( IN EFI_EVENT Event, IN VOID *Context );
VOID EFIAPI SmbChgResetFccEventHandler( IN EFI_EVENT Event, IN VOID *Context);
VOID EFIAPI SmbChgFccCalibrationEventHandler( IN EFI_EVENT Event, IN VOID *Context);
EFI_STATUS EFI_PmicSmbchgRunEsrWa( void );

EFI_STATUS PmicSmbchgFccCalibrationWa(UINT32 PmicDeviceIndex);


/*===========================================================================
EXTERNAL FUNCTION DECLARATIONS
===========================================================================*/

/**
EFI_PmicSmbchgInit ()

@brief
Initializes SMBCHG
*/
EFI_STATUS
EFIAPI
EFI_PmicSmbchgInit
(
  IN UINT32 PmicDeviceIndex
)
{
  EFI_STATUS   Status  = EFI_SUCCESS;
  pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;

  SetMem(&BatteryCfgData, sizeof(EFI_PM_FG_CFGDATA_TYPE), 0x00);

  Status = PmicFgBattParam_GetDefaultCfg(&BatteryCfgData );
  if(EFI_SUCCESS == Status)
  {
    ITERM = BatteryCfgData.ChrgingTermCurrent;
  }

  errFlag = pm_smbchg_chgr_set_charge_termination_current((uint32)PmicDeviceIndex, ITERM);

  /* Set AICL deglitch time to 15us  (USBIN Input Collapse Glitch Filter) */
  Status |= pm_smbchg_usb_chgpth_config_aicl(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_AICL_CFG__DEB_5V_ADAPTER_SEL, TRUE);

  ChargerPmicIndex = PmicDeviceIndex;

  (void)errFlag;

  return EFI_SUCCESS;
}

/**
EFI_PmicSmbchgConfigAicl ()

@brief
Configures AICL
*/
EFI_STATUS
EFIAPI
EFI_PmicSmbchgConfigAicl
(
  IN UINT32 PmicDeviceIndex,
  IN EFI_PM_SMBCHG_USB_CHGPTH_AICL_CFG_TYPE AiclCfgType,
  IN BOOLEAN Set
)
{
  EFI_STATUS   Status  = EFI_SUCCESS;
  pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;

  errFlag = pm_smbchg_usb_chgpth_config_aicl(PmicDeviceIndex, (pm_smbchg_usb_chgpth_aicl_cfg_type)AiclCfgType, Set);

  Status = (PM_ERR_FLAG__SUCCESS == errFlag)? PM_ERR_FLAG__SUCCESS : EFI_DEVICE_ERROR;

  return Status;
}

/**
EFI_PmicSmbchgEnableCharger ()

@brief
Enable charging
*/
EFI_STATUS
EFIAPI
EFI_PmicSmbchgEnableCharger
(
  IN UINT32  PmicDeviceIndex,
  IN BOOLEAN ChargerEnable
)
{
  EFI_STATUS             Status  = EFI_SUCCESS;
  pm_err_flag_type       errFlag = PM_ERR_FLAG__SUCCESS;

  PMIC_DEBUG((EFI_D_WARN, "PmicDxe: PmicSmbchgEnableCharger : (%d) \n\r", ChargerEnable));

  if(ChargerEnable)
  {
    if(EFI_SUCCESS != PmicSmbchgEnableWdog(PmicDeviceIndex))
    {
      PMIC_DEBUG(( EFI_D_ERROR, "** EFI_PmicSmbchgEnableCharger error while enabling watchdog ** \n\r"));
    }
  }

  //Enable SW JEITA
  if (TRUE == BatteryCfgData.JeitaCfgData.SWJeitaEnable && TRUE == ChargerEnable)
  {
    errFlag |= pm_smbchg_chgr_config_chgr_temp_cmpn(PmicDeviceIndex, PM_SMBCHG_CHGR_CCMPN__JEITA_TEMP_HARD_LIMIT, TRUE);
    errFlag |= pm_smbchg_chgr_config_chgr_temp_cmpn(PmicDeviceIndex, PM_SMBCHG_CHGR_CCMPN__LOAD_BAT, FALSE);
    errFlag |= pm_smbchg_chgr_config_chgr_temp_cmpn(PmicDeviceIndex, PM_SMBCHG_CHGR_CCMPN__HOT_SL_FV_COMP, FALSE);
    errFlag |= pm_smbchg_chgr_config_chgr_temp_cmpn(PmicDeviceIndex, PM_SMBCHG_CHGR_CCMPN__COLD_SL_FV_COMP, FALSE);
    errFlag |= pm_smbchg_chgr_config_chgr_temp_cmpn(PmicDeviceIndex, PM_SMBCHG_CHGR_CCMPN__HOT_SL_CHG_I_COMP, FALSE);
    errFlag |= pm_smbchg_chgr_config_chgr_temp_cmpn(PmicDeviceIndex, PM_SMBCHG_CHGR_CCMPN__COLD_SL_CHG_I_COMP, FALSE);
    PMIC_DEBUG((EFI_D_WARN, "PmicDxe: SW JEITA is enabled \n\r"));
  }
  else
  {
    /*Enable HW JEITA here but it is not supported by SW */
    PMIC_DEBUG((EFI_D_WARN, "PmicDxe: HW JEITA not supported by SW \n\r"));
  }

  //Change Charge Enable Source to CMD controlled
  errFlag |= pm_smbchg_chgr_enable_src(PmicDeviceIndex, FALSE);
  //Change Charge Enable to Active high
  errFlag |= pm_smbchg_chgr_set_chg_polarity_low(PmicDeviceIndex, FALSE);
  errFlag |= pm_smbchg_bat_if_config_chg_cmd(PmicDeviceIndex, PM_SMBCHG_BAT_IF_CMD__EN_BAT_CHG, ChargerEnable);

  if(ChargerEnable)
  {
    EFI_PmicSmbchgRunEsrWa();
    //Run WA every 100s
    gBS->CreateEvent(  EVT_TIMER | EVT_NOTIFY_SIGNAL,
                       TPL_CALLBACK,
                       SmbChgEsrPulseWAEventHandler,
                       NULL,
                       &SmbChgEsrPulseWAEvent );

    gBS->SetTimer(SmbChgEsrPulseWAEvent, TimerPeriodic, ESR_PULSE_WA_TIMER_DURATION);

    /* FCC calibration workaround*/
    /* read and store fast charge current */

    if(TRUE == BatteryCfgData.RunFccCalibration)
    {
      if (NULL == SmbChgFccCalibrationEvent)
      {
        errFlag |= pm_smbchg_chgr_get_fast_chg_i(PmicDeviceIndex, &InitialFccMa);

        gBS->CreateEvent(  EVT_TIMER | EVT_NOTIFY_SIGNAL,
                             TPL_CALLBACK,
                             SmbChgFccCalibrationEventHandler,
                             NULL,
                             &SmbChgFccCalibrationEvent);

          gBS->SetTimer(SmbChgFccCalibrationEvent, TimerPeriodic, FCC_CALIBRATION_TIMER_DURATION);
      }
    }
  }

  Status = (PM_ERR_FLAG__SUCCESS == errFlag)? PM_ERR_FLAG__SUCCESS : EFI_DEVICE_ERROR;

  return Status;
}

EFI_STATUS
EFIAPI
EFI_PmicSmbchgUsbinValid
(
  IN  UINT32 PmicDeviceIndex,
  OUT BOOLEAN *Valid
)
{
  EFI_STATUS                           Status  = EFI_SUCCESS;
  pm_err_flag_type                     errFlag = PM_ERR_FLAG__SUCCESS;
  boolean                              Present;

  if(NULL == Valid)
  {
    Status = EFI_INVALID_PARAMETER;
  }
  else
  {
    /*Read IRQ real time status*/
    errFlag = pm_smbchg_usb_chgpth_irq_status(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_USBIN_SRC_DET, PM_IRQ_STATUS_RT,(boolean*)&Present);

    Status = (PM_ERR_FLAG__SUCCESS == errFlag)? PM_ERR_FLAG__SUCCESS : EFI_DEVICE_ERROR;

    *Valid = (Present) ? TRUE : FALSE;
  }

  return Status;
}


EFI_STATUS
EFIAPI
EFI_PmicSmbchgGetChargePath
(
  IN  UINT32                 PmicDeviceIndex,
  OUT PM_SMBCHG_PWR_PTH_TYPE *ChargerPath
)
{
  EFI_STATUS                  Status  = EFI_SUCCESS;
  pm_err_flag_type            errFlag = PM_ERR_FLAG__SUCCESS;
  pm_smbchg_usb_chgpth_pwr_pth_type             pwr_path;

  if(NULL == ChargerPath)
  {
    Status = EFI_INVALID_PARAMETER;
  }
  else
  {
    errFlag = pm_smbchg_usb_chgpth_get_pwr_pth(PmicDeviceIndex, &pwr_path);

    *ChargerPath =(PM_SMBCHG_PWR_PTH_TYPE)pwr_path;

    Status = (PM_ERR_FLAG__SUCCESS == errFlag)? EFI_SUCCESS : EFI_DEVICE_ERROR;
  }

  return Status;
}


/**
EFI_PmicSmbchgIsBatteryPresent ()

@brief
Gets battery presence status
*/
EFI_STATUS
EFIAPI
EFI_PmicSmbchgIsBatteryPresent
(
  IN  UINT32 PmicDeviceIndex,
  OUT BOOLEAN *BatteryPresent
)
{
  EFI_STATUS        Status        = EFI_SUCCESS;
  pm_err_flag_type  errFlag       = PM_ERR_FLAG__SUCCESS;
  boolean           BattPres      = FALSE;

  if(NULL == BatteryPresent)
  {
    Status =  EFI_INVALID_PARAMETER;
  }
  else
  {
    errFlag =  pm_smbchg_bat_if_get_bat_pres_status(PmicDeviceIndex, &BattPres);

    *BatteryPresent = (BattPres)? TRUE: FALSE;

    Status = (PM_ERR_FLAG__SUCCESS == errFlag)? EFI_SUCCESS : EFI_DEVICE_ERROR;
  }

  return Status;
}


/**
EFI_PmicChargerPortType ()

@brief
Gets charger port type
*/
EFI_STATUS
EFIAPI
EFI_PmicSmbchgChargerPortType
(
  IN  UINT32 PmicDeviceIndex,
  OUT EFI_PM_SMBCHG_CHGR_PORT_TYPE *PortType
)
{
  EFI_STATUS        Status        = EFI_SUCCESS;
  pm_err_flag_type  errFlag       = PM_ERR_FLAG__SUCCESS;

  pm_smbchg_misc_src_detect_type src_detected;
  pm_usb_chgpth_usb_cfg_type     Usb51Config;

  if(NULL == PortType)
  {
    Status = EFI_INVALID_PARAMETER;
  }
  else
  {
    errFlag =  pm_smbchg_misc_chgr_port_detected(PmicDeviceIndex, &src_detected);
    if(PM_ERR_FLAG__SUCCESS != errFlag)
    {
      Status = EFI_DEVICE_ERROR;
      *PortType = EFI_PM_SMBCHG_CHG_PORT_INVALID;
      return Status;
    }

    *PortType = (EFI_PM_SMBCHG_CHGR_PORT_TYPE)src_detected;
    if(EFI_PM_SMBCHG_CHG_PORT_SDP == (EFI_PM_SMBCHG_CHGR_PORT_TYPE)src_detected)
    {
      errFlag |=  pm_smbchg_usb_chgpth_set_cmd_il((uint32)PmicDeviceIndex, PM_SMBCHG_USBCHGPTH_CMD_IL__USB51_MODE, TRUE);

      Usb51Config.usb51ac_ctrl_type = PM_SMBCHG_USB_CHGPTH_USB51AC_CTRL__PIN;
      Usb51Config.usb51_cmd_pol_type = PM_SMBCHG_USB_CHGPTH_USB51_CMD_POL__CMD1_100;
      errFlag |=  pm_smbchg_usb_chgpth_config_usb_chgpth((uint32)PmicDeviceIndex, &Usb51Config);
    }
  }

  return Status;
}


/**
@brief
Gets HVDCP status
*/
EFI_STATUS
EFIAPI
EFI_PmicSmbchgGetHvdcp
(
  IN  UINT32                 PmicDeviceIndex,
  IN  EFI_PM_HVDCP_STS_TYPE  HvdcpSts,
  OUT BOOLEAN                *Enable
)
{
  EFI_STATUS        Status  = EFI_SUCCESS;
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;
  boolean           en=FALSE;

  if(NULL == Enable)
  {
    Status = EFI_INVALID_PARAMETER;
  }
  else
  {
    errFlag =  pm_smbchg_usb_chgpth_hvdcp_sts(PmicDeviceIndex, (pm_smbchg_usb_chgpth_hvdcp_sts_type)HvdcpSts, &en);
    *Enable = (BOOLEAN)en;

    Status = (PM_ERR_FLAG__SUCCESS == errFlag)? PM_ERR_FLAG__SUCCESS : EFI_DEVICE_ERROR;
  }

  return Status;
}





EFI_STATUS
EFIAPI
EFI_PmicSmbchgConfigOtg
(
   IN  UINT32                      PmicDeviceIndex,
   IN  EFI_PM_SMBCHG_OTG_CFG_TYPE  *OtgCfgType
   )
{
  EFI_STATUS             Status  = EFI_SUCCESS;
  pm_err_flag_type       errFlag = PM_ERR_FLAG__SUCCESS;
  pm_smbchg_otg_cfg_type otg_cfg;

  if(NULL == OtgCfgType)
  {
    Status = EFI_INVALID_PARAMETER;
  }
  else if( EFI_PM_SMBCHG_OTG_CTR__PIN_CTRL_RID_DISABLED == OtgCfgType->OtgCtrl ||
           EFI_PM_SMBCHG_OTG_CTR__CMD_REG_RID_DISABLED == OtgCfgType->OtgCtrl )
  {
    Status = EFI_UNSUPPORTED;
  }
  else
  {
    otg_cfg.hv_otg_protection_en       = (boolean)(OtgCfgType->HvOtgProtection);
    otg_cfg.otg_pin_pol_active_high    = (boolean)(OtgCfgType->OtgPinPol);
    otg_cfg.otg_ctrl_type              = (pm_smbchg_otg_ctrl_type)(OtgCfgType->OtgCtrl);
    otg_cfg.uvlo_sensor_src            = (pm_smbchg_otg_uvlo_sensor_src_type)(OtgCfgType->OtgUvloSensorSrc);

    errFlag = pm_smbchg_otg_config_otg((uint32)PmicDeviceIndex, &otg_cfg);

    if(EFI_PM_SMBCHG_OTG_CTR__CMD_REG_RID_ENABLED == OtgCfgType->OtgCtrl)
    {
        /*If it is command controlled configuration then additionally need to write 1 to CHG_CMD*/
        errFlag |= pm_smbchg_bat_if_config_chg_cmd((uint32)PmicDeviceIndex, PM_SMBCHG_BAT_IF_CMD__OTG_EN, TRUE);
        /*WORK AROUND for high capacitance load*/
        errFlag |= PmicSmbchgOtgHighCapWorkArnd(PmicDeviceIndex);
    }
  }

  if(PM_ERR_FLAG__SUCCESS != errFlag)
  {
    Status = EFI_DEVICE_ERROR;
  }

  return Status;
}

/*this SW WA is for high capacitance load while enabling OTG*/

pm_err_flag_type PmicSmbchgOtgHighCapWorkArnd
(
   IN  UINT32  PmicDeviceIndex
   )
{
  pm_err_flag_type       err_flag = PM_ERR_FLAG__SUCCESS;
  BOOLEAN OverCurrent = FALSE;

  err_flag = pm_smbchg_usb_chgpth_irq_status(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_OTG_OVERCURRENT, PM_IRQ_STATUS_RT, &OverCurrent);

  if(TRUE == OverCurrent)
  {
      /*If it is command controlled configuration then additionally need to write 1 to CHG_CMD*/
      err_flag |= pm_smbchg_bat_if_config_chg_cmd((uint32)PmicDeviceIndex, PM_SMBCHG_BAT_IF_CMD__OTG_EN, FALSE);
      /*Stall for 20ms*/
      gBS->Stall(20000);

      /*If it is command controlled configuration then additionally need to write 1 to CHG_CMD*/
      err_flag |= pm_smbchg_bat_if_config_chg_cmd((uint32)PmicDeviceIndex, PM_SMBCHG_BAT_IF_CMD__OTG_EN, TRUE);
  }

  return err_flag;
}


EFI_STATUS
EFIAPI
EFI_PmicSmbchgSetOtgILimit
(
  IN  UINT32   PmicDeviceIndex,
  IN  UINT32   ImAmp
)
{
  EFI_STATUS        Status  = EFI_SUCCESS;
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

  errFlag = pm_smbchg_otg_set_otg_i_limit((uint32)PmicDeviceIndex,(uint32)ImAmp);
  if(PM_ERR_FLAG__SUCCESS != errFlag)
  {
    Status = EFI_DEVICE_ERROR;
  }

  return Status;
}


/**
  Sets Maximum USB current

  @param MaxCurrent  Current to be set

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_INVALID_PARAMETER: A Parameter was incorrect.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_NOT_READY:         The physical device is busy or not ready to
                         process this request.
*/

EFI_STATUS
EFIAPI
EFI_PmicSmbchgSetUsbMaxCurrent
(
  IN UINT32 PmicDeviceIndex,
  IN UINT32 MaxCurrent
)
{
    EFI_STATUS        Status  = EFI_SUCCESS;
    pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

    //Change USBIN Mode to High current (HC) Mode
    errFlag |= pm_smbchg_usb_chgpth_set_cmd_il(PmicDeviceIndex,  PM_SMBCHG_USBCHGPTH_CMD_IL__USBIN_MODE_CHG, TRUE);

    //run ICL in Override mode (Overrides ICL from APSD)
    errFlag |= pm_smbchg_usb_chgpth_set_cmd_il(PmicDeviceIndex,  PM_SMBCHG_USBCHGPTH_CMD_IL__ICL_OVERRIDE, TRUE);

    /* Disable AICL */
    errFlag |= pm_smbchg_usb_chgpth_config_aicl(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_AICL_CFG__AICL_EN, FALSE);

    /* set desired MAX current */
    errFlag |= pm_smbchg_usb_chgpth_set_usbin_current_limit(PmicDeviceIndex, MaxCurrent);

    /* Enable AICL */
    errFlag |= pm_smbchg_usb_chgpth_config_aicl(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_AICL_CFG__AICL_EN, TRUE);

    if(PM_ERR_FLAG__SUCCESS != errFlag)
    {
      Status = EFI_DEVICE_ERROR;
    }

    return Status;
}



EFI_STATUS
EFIAPI
EFI_PmicSmbchgEnableAfpMode
(
  IN  UINT32   PmicDeviceIndex
)
{
  EFI_STATUS        Status  = EFI_SUCCESS;
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

  errFlag = pm_smbchg_misc_en_afp((uint32)PmicDeviceIndex);

  if(PM_ERR_FLAG__SUCCESS != errFlag)
  {
    Status = EFI_DEVICE_ERROR;
  }
  return Status;
}


EFI_STATUS
EFIAPI
EFI_PmicSmbchgSetChargerPathPriority
(
   IN  UINT32                          PmicDeviceIndex,
   IN  EFI_PM_SMBCHG_CHGR_PATH_TYPE    ChgrPath
   )
{
  EFI_STATUS        Status  = EFI_SUCCESS;
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;
  pm_smbchg_usb_chgpth_apsd_cfg_type apsd_cfg;

  if(ChgrPath < EFI_PM_SMBCHG_CHGR_PATH_INVALID)
  {
      /*read existing configuration*/
      errFlag = pm_smbchg_usb_chgpth_get_apsd_config((uint32)PmicDeviceIndex, &apsd_cfg);
      /*change only charger priority bit*/
      apsd_cfg.is_input_prority_usbin = (ChgrPath == EFI_PM_SMBCHG_CHGR_PATH_USB) ? TRUE : FALSE;
      /*write back updated configuration*/
      errFlag |= pm_smbchg_usb_chgpth_config_apsd((uint32)PmicDeviceIndex, &apsd_cfg);

      Status = (PM_ERR_FLAG__SUCCESS == errFlag)? EFI_SUCCESS : EFI_DEVICE_ERROR;
  }
  else
  {
   Status = EFI_DEVICE_ERROR;
  }

  return Status;
}


EFI_STATUS
EFIAPI
PmicSmbchgEnableWdog
(
  IN UINT32  PmicDeviceIndex
)
{
  EFI_STATUS                   Status  = EFI_SUCCESS;
  pm_err_flag_type             errFlag = PM_ERR_FLAG__SUCCESS;

  pm_smbchg_misc_wdog_cfg_type wd_cfg;

  //Read original watcdog configuration
  errFlag = pm_smbchg_misc_get_wdog_config(PmicDeviceIndex, &wd_cfg);
  Status = (PM_ERR_FLAG__SUCCESS == errFlag)? PM_ERR_FLAG__SUCCESS : EFI_DEVICE_ERROR;
  if (PM_ERR_FLAG__SUCCESS != errFlag)
  {
      Status = EFI_DEVICE_ERROR;
      return Status;
  }

  //Enable watchdog and set timeout
  wd_cfg.wdog_timer_en = TRUE;
  wd_cfg.wdog_option_run_always = TRUE;
  wd_cfg.wdog_irq_sfty_en = TRUE;
  wd_cfg.wdog_timeout = PM_SMBCHG_MISC_WD_TMOUT_72S;

  errFlag = pm_smbchg_misc_config_wdog(PmicDeviceIndex, &wd_cfg);
  if (PM_ERR_FLAG__SUCCESS != errFlag)
  {
      Status = EFI_DEVICE_ERROR;
      return Status;
  }

  return Status;
}

EFI_STATUS
EFIAPI
EFI_PmicSmbchgSetFccMaxCurrent
(
  IN UINT32 PmicDeviceIndex,
  IN UINT32 MaxCurrentInmA
)
{
  EFI_STATUS        Status  = EFI_SUCCESS;
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

  errFlag = pm_smbchg_chgr_set_fast_chg_i(PmicDeviceIndex, MaxCurrentInmA );
  if(PM_ERR_FLAG__SUCCESS != errFlag)
  {
    Status = EFI_DEVICE_ERROR;
  }

  return Status;
}


EFI_STATUS
EFIAPI
  EFI_PmicSmbchgSetFvMaxVoltage

(
  IN UINT32 PmicDeviceIndex,
  IN UINT32 MaxVoltageInMv
)
{
  EFI_STATUS        Status  = EFI_SUCCESS;
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

  errFlag = pm_smbchg_chgr_set_float_volt(PmicDeviceIndex, MaxVoltageInMv);
  if(PM_ERR_FLAG__SUCCESS != errFlag)
  {
    Status = EFI_DEVICE_ERROR;
  }

  return Status;
}


EFI_STATUS
EFIAPI
EFI_PmicSmbchgEnableOtg
(
  IN  UINT32   PmicDeviceIndex,
  IN  BOOLEAN  Enable
)
{
  EFI_STATUS             Status  = EFI_SUCCESS;
  pm_err_flag_type       errFlag = PM_ERR_FLAG__SUCCESS;

  if(TRUE == Enable)
  {
      errFlag = pm_smbchg_bat_if_config_chg_cmd((uint32)PmicDeviceIndex, PM_SMBCHG_BAT_IF_CMD__OTG_EN, TRUE);
      errFlag |= PmicSmbchgOtgHighCapWorkArnd(PmicDeviceIndex);
  }
  else
  {
      errFlag = pm_smbchg_bat_if_config_chg_cmd((uint32)PmicDeviceIndex, PM_SMBCHG_BAT_IF_CMD__OTG_EN, FALSE);
  }

  Status = (PM_ERR_FLAG__SUCCESS == errFlag)? PM_ERR_FLAG__SUCCESS : EFI_DEVICE_ERROR;

  return Status;
}


EFI_STATUS
EFIAPI
EFI_PmicSmbchgOtgStatus
(
   IN  UINT32   PmicDeviceIndex,
   OUT BOOLEAN  *Ok
   )
{
    EFI_STATUS         Status      = EFI_SUCCESS;
    pm_err_flag_type   err_flag    = PM_ERR_FLAG__SUCCESS;
    BOOLEAN            OverCurrent = FALSE;

    err_flag = pm_smbchg_usb_chgpth_irq_status(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_OTG_OVERCURRENT, PM_IRQ_STATUS_RT, &OverCurrent);

    *Ok = (OverCurrent)? FALSE : TRUE ;

    Status = (PM_ERR_FLAG__SUCCESS == err_flag) ? PM_ERR_FLAG__SUCCESS : EFI_DEVICE_ERROR;

    return Status;
}



EFI_STATUS
EFIAPI 
EFI_PmicSmbchgGetRidStatus
(
  IN   UINT32                           PmicDeviceIndex,
  OUT  EFI_PM_SMBCHG_USB_RID_STS_TYPE   *RidStsType
)
{
  EFI_STATUS        Status  = EFI_SUCCESS; 
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

  pm_smbchg_usb_chgpth_rid_sts_type rid_sts_type;

  if(NULL != RidStsType)
  {
     errFlag = pm_smbchg_usb_chgpth_rid_sts(PmicDeviceIndex, &rid_sts_type);

     *RidStsType = (EFI_PM_SMBCHG_USB_RID_STS_TYPE)rid_sts_type;

     Status = (PM_ERR_FLAG__SUCCESS == errFlag)? PM_ERR_FLAG__SUCCESS : EFI_DEVICE_ERROR;
  }
  else
  {
      Status =  EFI_DEVICE_ERROR;
  }

  return Status;
}

VOID EFIAPI SmbChgResetFccEventHandler(
   IN EFI_EVENT Event,
   IN VOID *Context
   )
{
  pm_err_flag_type             errFlag = PM_ERR_FLAG__SUCCESS;

  errFlag |= pm_smbchg_chgr_set_fast_chg_i( ChargerPmicIndex, OldFastChgCurrent );
  if (PM_ERR_FLAG__SUCCESS != errFlag)
  {
    PMIC_DEBUG(( EFI_D_ERROR, "**PmicDxe: ChgWdogEventHandler error while petting watchdog ** \n\r"));
  }

  gBS->CloseEvent (SmbChgResetFccEvent);
}

EFI_STATUS EFI_PmicSmbchgRunEsrWa( void )
{
  EFI_STATUS   Status  = EFI_SUCCESS;
  pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;
  pm_smbchg_chgr_chgr_status_type chgr_sts;
  pm_smbchg_usb_chgpth_input_sts_type input_sts_type_usb;
  pm_smbchg_usb_chgpth_input_sts_type input_sts_type_dc;

  UINT32 TempFastChgCurrent = 0;
  INT32  ShadowCurrent = 0;
  static EFI_QCOM_PMIC_FG_PROTOCOL  *PmicFgProtocol      = NULL;

  errFlag = pm_smbchg_chgr_get_chgr_sts(ChargerPmicIndex, &chgr_sts);
  if(PM_SMBCHG_CHGR_FAST_CHARGING == chgr_sts.charging_type)
  {
    //check if input is connected
    errFlag |= pm_smbchg_usb_chgpth_input_sts(ChargerPmicIndex, PM_SMBCHG_CHAR_TYPE_USB, &input_sts_type_usb);
    errFlag |= pm_smbchg_usb_chgpth_input_sts(ChargerPmicIndex, PM_SMBCHG_CHAR_TYPE_DC, &input_sts_type_dc);
    if((PM_SMBCHG_NO_CHGR_DETECTED != input_sts_type_usb) || (PM_SMBCHG_NO_CHGR_DETECTED != input_sts_type_dc))
    {
      //FG Workaround: 2.5 ESR pulses missing during CC charge
      pm_smbchg_chgr_get_fast_chg_i( ChargerPmicIndex, &OldFastChgCurrent );
      gBS->LocateProtocol( &gQcomPmicFgProtocolGuid, NULL, (VOID **)&PmicFgProtocol );
      PmicFgProtocol->GetShadowCurrent( ChargerPmicIndex, &ShadowCurrent );

      if((ShadowCurrent - ESRWA_MIN_CURRENT) > (BatteryCfgData.ChrgingTermCurrent + ESRWA_MIN_CURRENT) )
      {
        TempFastChgCurrent = (ShadowCurrent - ESRWA_MIN_CURRENT);
      }
      else
      {
        TempFastChgCurrent = BatteryCfgData.ChrgingTermCurrent + ESRWA_MIN_CURRENT;
      }

      pm_smbchg_chgr_set_fast_chg_i( ChargerPmicIndex, TempFastChgCurrent );
      //wait for 1.5 seconds
      gBS->CreateEvent(  EVT_TIMER | EVT_NOTIFY_SIGNAL,
                         TPL_CALLBACK,
                         SmbChgResetFccEventHandler,
                         NULL,
                         &SmbChgResetFccEvent );

      gBS->SetTimer(SmbChgResetFccEvent, TimerPeriodic, 1500000 );
    }
  }
  else
  {
    gBS->CloseEvent (SmbChgEsrPulseWAEvent);
  }

  Status = (PM_ERR_FLAG__SUCCESS == errFlag)? PM_ERR_FLAG__SUCCESS : EFI_DEVICE_ERROR;

  return Status;
}


VOID EFIAPI SmbChgEsrPulseWAEventHandler(
   IN EFI_EVENT Event,
   IN VOID *Context
   )
{
  EFI_PmicSmbchgRunEsrWa();
}


VOID EFIAPI SmbChgFccCalibrationEventHandler(
    IN EFI_EVENT Event,
    IN VOID *Context
    )
{
  PmicSmbchgFccCalibrationWa(PM_DEVICE_1);
}


/*Full charge current calibration for accuracy */
EFI_STATUS
PmicSmbchgFccCalibrationWa(UINT32 PmicDeviceIndex)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  EFI_STATUS   Status  = EFI_SUCCESS;
  pm_smbchg_chgr_chgr_status_type chgr_sts = { 0 };
  pm_smbchg_misc_src_detect_type src_detected;
  EFI_QCOM_PMIC_FG_PROTOCOL            *PmicFgProtocol  = NULL;
  INT32  ibat_trim_decoded = 0;
  INT32  fcc_target = 500;
  UINT32 fv_mv;
  INT32  fcc_measured = 0;
  INT32  fcc_delta;
  BOOLEAN aicl_in_hard_limit = FALSE;
  BOOLEAN algo_ran = FALSE;
  STATIC UINT32 confirm_calibration = 0;
  STATIC BOOLEAN StartCalibration = FALSE;
  STATIC UINT32 FccHardLimitcheck = 0;

  if(FALSE == StartCalibration)
  {
    err_flag |= pm_smbchg_dc_chgpth_get_fcc_calibration_done(PmicDeviceIndex, &algo_ran);

    if(TRUE == algo_ran && err_flag == PM_ERR_FLAG__SUCCESS)
    {
      PMIC_DEBUG((EFI_D_WARN, "**PmicDxe: FCC Calibration Algo Already ran ** \n\r"));
      err_flag |= pm_smbchg_chgr_set_fast_chg_i(PmicDeviceIndex, InitialFccMa);
      gBS->CloseEvent(SmbChgFccCalibrationEvent);
      return Status;
    }

    /* run only for DCP or OCP */
    err_flag |=  pm_smbchg_misc_chgr_port_detected(PmicDeviceIndex, &src_detected);
    if(src_detected != PM_SMBCHG_MISC_SRC_DETECT_OTHER_CHARGING_PORT && src_detected != PM_SMBCHG_MISC_SRC_DETECT_DCP)
    {
      PMIC_DEBUG((EFI_D_WARN, "**PmicDxe: FCC Calibration Port type not DCP or OCP ** \n\r"));
      err_flag |= pm_smbchg_chgr_set_fast_chg_i(PmicDeviceIndex, InitialFccMa);
      gBS->CloseEvent(SmbChgFccCalibrationEvent);
      return Status;
    }

    /* only for fastcharging */
    err_flag |= pm_smbchg_chgr_get_chgr_sts(PmicDeviceIndex, &chgr_sts);
    if(chgr_sts.charging_type != PM_SMBCHG_CHGR_FAST_CHARGING)
    {
      err_flag |= pm_smbchg_chgr_set_fast_chg_i(PmicDeviceIndex, InitialFccMa);
      //if not fast charging then come back to check again if fast charging started
      PMIC_DEBUG((EFI_D_WARN, "**PmicDxe: FCC Calibration Not in Fast Charge. Will re-attempt ** \n\r"));
      return Status;
    }

    /*if hard limit then come back and read again. Alogo requires to wait and read for three times and then come back again.
      So instead of reading three times, this algo will be called again*/
    err_flag |= pm_smbchg_chgr_get_float_volt_sts(PmicDeviceIndex, &aicl_in_hard_limit, &fv_mv);
    if(TRUE == aicl_in_hard_limit)
    {
      err_flag |= pm_smbchg_chgr_get_fast_chg_i(PmicDeviceIndex, &InitialFccMa);
      FccHardLimitcheck++ ;
      PMIC_DEBUG((EFI_D_WARN, "**PmicDxe: FCC Calibration AICL_IN_HARD_LIMIT ** \n\r"));
      /*check upto 5 times if hard limit */
      if(FccHardLimitcheck > 5)
      {
          err_flag |= pm_smbchg_chgr_set_fast_chg_i(PmicDeviceIndex, InitialFccMa);
          gBS->CloseEvent(SmbChgFccCalibrationEvent);
      }
      return Status;
    }
    else
    {
      /*again read the set FCC current in case it has changed */
      err_flag |= pm_smbchg_chgr_get_fast_chg_i(PmicDeviceIndex, &InitialFccMa);
      StartCalibration = TRUE;
    }
  }
  else
  {
    // TRUE == StartCalibration
    /* set fast charge current to 500mA = fcc_target */
    err_flag = pm_smbchg_chgr_set_fast_chg_i(PmicDeviceIndex, fcc_target);
    PMIC_DEBUG((EFI_D_WARN, "**PmicDxe: FCC Calibration initial_fcc_ma = %d ** \n\r", InitialFccMa));

    if(NULL == PmicFgProtocol)
    {
      Status = gBS->LocateProtocol(&gQcomPmicFgProtocolGuid, NULL, (VOID **)&PmicFgProtocol);
    }

    if(NULL == PmicFgProtocol || EFI_SUCCESS != Status)
    {
      err_flag |= pm_smbchg_chgr_set_fast_chg_i(PmicDeviceIndex, InitialFccMa);
      gBS->CloseEvent(SmbChgFccCalibrationEvent);
      return Status;
    }
    else
    {
      Status = PmicFgProtocol->GetShadowCurrent(PmicDeviceIndex, &fcc_measured);
    }

    //Store (fcc_measured � fcc_target) = fcc_delta
    fcc_delta = fcc_measured - fcc_target;
    PMIC_DEBUG((EFI_D_WARN, "**PmicDxe: FCC Calibration fcc_measured = %d ** \n\r", fcc_measured));
    PMIC_DEBUG((EFI_D_WARN, "**PmicDxe: FCC Calibration fcc_delta = %d ** \n\r", fcc_delta));

    err_flag = pm_smbchg_misc_get_ibat_trim(PmicDeviceIndex, &ibat_trim_decoded);

    PMIC_DEBUG((EFI_D_WARN, "**PmicDxe: FCC Calibration for ibat_trim_decoded = %d  ** \n\r", ibat_trim_decoded));

    if(fcc_delta > 70)
    {
      ibat_trim_decoded -= 15;
    }
    else if(fcc_delta > 8)
    {
      ibat_trim_decoded -= 1;
    }
    else if(fcc_delta < -70)
    {
      ibat_trim_decoded += 15;
    }
    else if(fcc_delta < -8)
    {
      ibat_trim_decoded += 1;
    }
    else
    { //FcDelta =+/- 8
      //trimming complete / successful
      confirm_calibration++;
      /* run calibration twice to make sure */
      if(confirm_calibration == FCC_CALIBRATION_COFIRMED)
      {
        /* set the value TRUE so that this algo is not required to be run again */
        err_flag |= pm_smbchg_dc_chgpth_set_fcc_calibration_done(PmicDeviceIndex, TRUE);
        err_flag |= pm_smbchg_chgr_set_fast_chg_i(PmicDeviceIndex, InitialFccMa);
        gBS->CloseEvent(SmbChgFccCalibrationEvent);
        PMIC_DEBUG((EFI_D_WARN, "**PmicDxe: FCC Calibration completed ** \n\r"));
      }
      return Status;
    }

    /*The API will decode the vale and write to trim12 register */
    PMIC_DEBUG((EFI_D_WARN, "**PmicDxe: FCC Calibration Set ibat_trim_decoded = %d  ** \n\r", ibat_trim_decoded));
    err_flag |= pm_smbchg_misc_set_ibat_trim(PmicDeviceIndex, ibat_trim_decoded);
  }
  return (EFI_STATUS)(Status | err_flag);
}


EFI_STATUS
EFIAPI
EFI_PmicSmbchgUsbSuspend
(
  IN UINT32  PmicDeviceIndex,
  IN BOOLEAN Enable
  )
{
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

  errFlag = pm_smbchg_usb_chgpth_set_cmd_il(PmicDeviceIndex, PM_SMBCHG_USBCHGPTH_CMD_IL__USBIN_SUSPEND, Enable);

  if(PM_ERR_FLAG__SUCCESS != errFlag)
  {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}


EFI_STATUS
EFIAPI
EFI_PmicSmbchgPetChgWdog
(
  IN UINT32 PmicDeviceIndex
)
{
  EFI_STATUS        Status  = EFI_SUCCESS;
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

  errFlag = pm_smbchg_misc_wdog_rst(PmicDeviceIndex);
  if (PM_ERR_FLAG__SUCCESS != errFlag)
  {
    PMIC_DEBUG(( EFI_D_ERROR, "**PmicDxe: EFI_PmicSmbchgPetChgWdog error while petting watchdog ** \n\r"));
    return EFI_DEVICE_ERROR;
  }

  return Status;
}

/**
PMIC SMBCHG UEFI Protocol implementation
*/
EFI_QCOM_PMIC_SMBCHG_PROTOCOL PmicSmbchgProtocolImplementation = 
{
    PMIC_SMBCHG_REVISION,     
    EFI_PmicSmbchgInit,
    EFI_PmicSmbchgEnableCharger,
    EFI_PmicSmbchgUsbinValid,    
    EFI_PmicSmbchgGetChargePath, 
    EFI_PmicSmbchgIsBatteryPresent,    
    EFI_PmicSmbchgChargerPortType, 
    EFI_PmicSmbchgGetHvdcp,
    EFI_PmicSmbchgConfigOtg,
    EFI_PmicSmbchgSetOtgILimit,
    EFI_PmicSmbchgSetUsbMaxCurrent,
    EFI_PmicSmbchgEnableAfpMode,
    EFI_PmicSmbchgSetChargerPathPriority,
    EFI_PmicSmbchgSetFccMaxCurrent,
    EFI_PmicSmbchgSetFvMaxVoltage,
    EFI_PmicSmbchgEnableOtg,
    EFI_PmicSmbchgOtgStatus,
    EFI_PmicSmbchgGetRidStatus,
    EFI_PmicSmbchgConfigAicl,
    EFI_PmicSmbchgUsbSuspend,
    EFI_PmicSmbchgPetChgWdog,
};

