#ifndef __PMICFGBATTPROFILE_H__
#define __PMICFGBATTPROFILE_H__

 /*! @file PmicFgBattProfile.h
 *
 * PMIC Battery Profile UEFI definitions.
 *
 * Copyright (c) 2014 - 2015 Qualcomm Technologies, Inc.  All Rights Reserved. 
 * Qualcomm Technologies Proprietary and Confidential.
 */

/*=============================================================================
                              EDIT HISTORY


 when          who     what, where, why
 --------   ---     -----------------------------------------------------------
 04/08/15   va        Rslow WA Changes 
 03/13/15   al        Adding changes for optimization
 01/05/15   va        Added Multiple Battery Profile and Profile Parser Status API
 09/23/14   al        Moving API declaration from source file to header file 
 09/25/14   va        Update for New Battery Profile Format
 05/16/14   va        New file.
=============================================================================*/

/**
  PMIC Platform interface
 */
#include <Protocol/EFIPlatformInfo.h>
#include "DDIPlatformInfo.h"
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/EFIPmicFg.h>

/*===========================================================================
                               MACROS
===========================================================================*/
#define MAX_NUM_OF_BATT_PROFILE         8 // Number profile that can exists in profile file
#define MAX_VARNAME_SIZE                    30
#define BREAK_IF_ERROR(Status) if (EFI_SUCCESS != Status ) { break;} 

/*=========================================================================
                            GLOBAL VARIABLES
===========================================================================*/

/*===========================================================================
                               TYPE DEFINITIONS
===========================================================================*/

/*!
 *  \brief Structure used for storing Battery Profile
*/

/* Battery profile addr data value */
typedef struct _FgBattAddrData{
  UINT32 Addr;
  UINT32 Addr2;
  UINT32 Offset;
  UINT32 Data;
}FgBattAddrData;

typedef struct _FgBattRslowProfileProfileSectionFlag{
  /*used during Rslow Charger Clear fix sequence */
  BOOLEAN RslowLowSocDisCharge;/*0x538,2[4] from battery profile */
  BOOLEAN RslowLowSocDisChargeC2En;/*0x538,2[5] from battery profile */
}FgBattRslowProfileProfileSectionFlag;


/*Rslow WA required Battery Profile Section Data*/
typedef struct _FgBattRslowProfileSectionParams{
  UINT32 RslowCompSocC1ch;/*rslow_comp_along_soc_c1 0x528, 0-1*/
  UINT32 RslowCompSocC2ch;/*rslow_comp_along_soc_c1 0x528, 2-3*/
  UINT32 RsToRslowChargeFix;/*rs_to_rslow_charge 0x514, 2*/
  UINT32 RslowCompChFixTh; /* Soc_rslow_th 0x52C, 0*/
  FgBattRslowProfileProfileSectionFlag RslowProfileFlag;
}FgBattRslowProfileSectionParams;

/*Rslow WA required Battery Profile Config section Data*/
typedef struct _FgBattRslowProfileConfigs{
  UINT32 RslowCompSocC1ch;/*rslow_comp_along_soc_c1 0x528, 0-1*/
  UINT32 RslowCompSocC2ch;/*rslow_comp_along_soc_c1 0x528, 2-3*/
  UINT32 RsToRslowChargeFix;/*rs_to_rslow_charge 0x514, 2*/
  UINT32 RslowCompChFixTh; /* Soc_rslow_th 0x52C, 0*/
}FgBattRslowProfileConfigs;

/*Rslow WA required Battery Profile Section Data*/
typedef struct _FgBattRslowProfileParams{
  FgBattRslowProfileConfigs       RsProfileConfigs;
  FgBattRslowProfileSectionParams RsProfileSectionParams;
  BOOLEAN RslowWorkaround;/*set if RslowWA needs run */
  BOOLEAN RslowFixClrSeq;/*if Charge Fix/Clear seq is needed runtime */
  BOOLEAN RslowDeltaSoc;/*Set if delta soc interrupt is high*/
}FgBattRslowProfileParams;

typedef struct _FgBattThermCoff{
  UINT32 ThermC1Coeff;
  UINT32 ThermC2Coeff;
  UINT32 ThermC3Coeff;
}FgBattThermCoff;

/* Battery profile params*/
typedef struct _FgBattProfileParams{
  UINT32 BatteryId;
  UINT32 FloatVoltage;
  UINT32 NomBattCapacity;
  UINT32 ThermistorValue;
  UINT32 CheckSum;
  UINT8  *BatteryName;
  FgBattThermCoff ThermCoff;
  UINT32 CurrProfileRev;
  FgBattRslowProfileParams RslowPp;

}FgBattProfileParams;

/* Battery profile Data */
typedef struct _FgBattProfileData{
  FgBattAddrData     *addrData;
  FgBattProfileParams Pp; /*ProfileParams*/
}FgBattProfileData;

/* Battery profile Number */
typedef enum _FgBattProfileNum{
 FG_BATTPROFILE_NUMSEL_1,/*Profile Number */
 FG_BATTPROFILE_NUMSEL_2, /*Profile Number */
 FG_BATTPROFILE_NUMSEL_3, /*Profile Number */
 FG_BATTPROFILE_NUMSEL_4, /*Profile Number */
 FG_BATTPROFILE_NUMSEL_5, /*Profile Number */
 FG_BATTPROFILE_NUMSEL_6, /*Profile Number */
 FG_BATTPROFILE_NUMSEL_7, /*Profile Number */
 FG_BATTPROFILE_NUMSEL_8, /*Profile Number */
 FG_BATTPROFILE_NUMSEL_MAX /*MAx Profile Number */
}FgBattProfileNumSel;

/* Battery profile complete Info */
typedef struct _FgBattProfile{
  FgBattProfileData Pd[MAX_NUM_OF_BATT_PROFILE];/* One Profile complete Information */
  UINT32 ProfileCount;/*Total number of profile available */
  UINT32 SelectedProfile;/*Selected profile and flashed in FG SRAM */
}FgBattProfile;

typedef enum 
{
  UEFI_VARIABLE_OP_GET,
  UEFI_VARIABLE_OP_SET,
  UEFI_VARIABLE_OP_INVALID,
}PmUefiVariableOpType;

EFI_STATUS PmicFgBattProfileInit(VOID);
EFI_STATUS PmicFgBattHandleProfile(UINT32 PmicDeviceIndex);

EFI_STATUS PmicFgBattParserStatus(EFI_PM_FG_PROFILE_PARSER_STATUS *parser_status);

VOID EFI_PmicBattProfileStrToUpper(char *Str, UINT32 StrSize);

BOOLEAN EFI_PmicBattProfileCmpArray(UINT32 *ArrAddr);

VOID EFI_PmicBattProfileProcessVariable(char *VarName);

EFI_STATUS EFI_PmicBattProfileStrCat(char *Dest, char *Src);

EFI_STATUS PmicFgBattProfileOpenFile
(
  IN  CHAR16           *Path,
  IN  EFI_GUID         *RootDeviceType,
  IN  EFI_GUID         *PartitionType,
  IN  BOOLEAN          SelectNonRemovable,
  IN  CHAR8            *VolumeName,
  OUT EFI_FILE_HANDLE  *FileHandle 
);

EFI_STATUS PmicFgBattProfileReadFile
(
  IN     CHAR16   *Path,
  IN     EFI_GUID *RootDeviceType,
  IN     EFI_GUID *PartitionType,
  IN     BOOLEAN  SelectNonRemovable,
  IN     CHAR8    *VolumeName,
  IN OUT UINTN    *BytesToRead,
  IN     UINT64   ReadOffset,
  OUT    UINT8    *FileBuffer,
  IN     UINTN   FileBufferSize 
);

EFI_STATUS PmicFgBattProfileGetFileSize
(
  IN  CHAR16   *Path,
  IN  EFI_GUID *RootDeviceType,
  IN  EFI_GUID *PartitionType,
  IN  BOOLEAN  SelectNonRemovable,
  IN  CHAR8    *VolumeName,
  OUT UINTN    *FileSize 
);

EFI_STATUS PmicFgBattProfileReadProfileParams
(
  UINT32 PmicDeviceIndex, BOOLEAN ReadProfile
);

EFI_STATUS PmicUefiVariable(UINT16 *variable, UINTN* Value, PmUefiVariableOpType OpType /*TRUE for Set and FALSE for Get*/);

EFI_STATUS PmicFgBattProfileGetProfileParams(OUT FgBattProfileParams *Pp);
#endif /* __PMICFGBATTPROFILE_H__ */

