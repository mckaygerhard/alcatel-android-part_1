/*! @file PmicFgProtocol.c
 *
 *  PMIC-FG MODULE RELATED DECLARATION
 *  This file contains functions and variable declarations to support
 *  the PMIC FG module.
 *
 *  Copyright (c) 2014 - 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
 *  Qualcomm Technologies Proprietary and Confidential.
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who    what, where, why
--------   ---    ----------------------------------------------------------
05/12/15   mr     Enabled Charger functionality for 8952 (CR-846387)
05/14/15   va     Profile Integrity Fix
04/30/15   va     Enable/Disable Charging before profile reload
04/16/15   al     Added Batteryconfiguration as param
04/09/15   va     API to read Battery temperature validity
04/08/15   va     Rslow WA Changes
03/13/15   al     Adding changes for optimization
02/25/15   sm     Added API to read SRAM shadow register
01/12/15   al     Removing hardcoded dvdd_rb status
01/19/15   al     Adding API to return battery type
01/13/15   va     Logging for ChargerApp states
01/06/15   al     Adding VddMaxAdjustment
01/05/15   va     Added Multiple Battery Profile and Profile Parser Status API
12/17/14   al     Adding SW WA for FG and debug message for 8004
12/02/14   va     Updated DEBUG prints log level
11/12/14   sm     Changes in GetChargerCurrent and GetBatteryVoltage APIs to
                  use driver level functions for calibration
09/25/14   va     Update for New Battery Profile Format
10/08/14   sm     Added sign correction in GetChargerCurrent API
08/21/14   va     Enable File Logging
08/14/14   sm     Added GetBatteryVoltage and GetBatteryTemperature APIs
                  Also updated Chargecurrent and GetSoc APIs
06/20/14   va     New File.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

/**
  EFI interfaces
 */
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Protocol/EFIPmicFg.h>
#include <Protocol/EFIPmicPwrOn.h>
#include <Protocol/EFIPlatformInfo.h>
#include <Protocol/EFIChipInfo.h>

#include "DDIPlatformInfo.h"

/**
  PMIC Lib interfaces
 */
#include "pm_uefi.h"
#include "PmicFileLog.h"

#include "pm_fg_soc.h"
#include "pm_fg_adc_usr.h"
#include "pm_version.h"
#include "pm_smbchg_bat_if.h"
#include "pm_smbchg_chgr.h"
#include "pm_smbchg_misc.h"
#include "pm_smbchg_usb_chgpth.h"
#include "pm_fg_memif.h"
#include "pm_fg_batt.h"

/**
  PMIC Fg interface
 */
#include <PmicFg.h>
#include <PmicFgBattParameter.h>
#include <PmicFgBattProfile.h>
#include <PmicFgSram.h>

#if 0
extern EFI_STATUS PmicGetSoc(UINT32 PmicDeviceIndex, OUT UINT32 *SoC );
extern EFI_STATUS PmicFgGetChargeCurrent(IN UINT32 PmicDeviceIndex, OUT INT32 *ChargeCurrent );
extern EFI_STATUS PmicFgGetChargeCapacity(IN UINT32 PmicDeviceIndex, OUT INT32 *ChargeCapacity );

VOID EFIAPI PmicFgExitBootServicesEvent( IN EFI_EVENT  Event, IN VOID *Context );
#endif

#define INCREMENT_ONE   1
#define DECREMENT_ONE  -1


/*===========================================================================
                  EXTERNAL FUNCTION DECLARATIONS
===========================================================================*/
/**
EFI_PmicFgInit()

@brief
Initializes Fule Gauge Module
*/
EFI_STATUS
EFIAPI
EFI_PmicFgInit
(
  UINT32 PmicDeviceIndex
)
{
  EFI_STATUS  Status = EFI_SUCCESS;
  //EFI_EVENT   EfiExitBatterySocEvent = (EFI_EVENT)NULL;
  EFI_PM_FG_CFGDATA_TYPE BatteryDefaults;
  pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;
  pm_fg_batt_sys_sts batt_sys_sts ={0};

#if 0
  /*Event to be signalled during ExitbootService to store SOC before exiting UEFI*/
  Status = gBS->CreateEvent(EVT_SIGNAL_EXIT_BOOT_SERVICES, TPL_CALLBACK, PmicFgExitBootServicesEvent, NULL, &EfiExitBatterySocEvent);
#endif

  Status |= PmicFgSram_Init(FG_SRAM_STATUS_AVAILABLE);

  Status |= PmicFgBattParam_Init();

  /* Get Battery Defaults */
  Status |= PmicFgBattParam_GetDefaultCfg(&BatteryDefaults);

 /*profile_integrity check to confirm battery profile valid or not*/
  errFlag = pm_fg_batt_get_sys_batt_sts(PmicDeviceIndex,&batt_sys_sts);
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: EFI_PmicFgInit: Profile Integrity Value (%d) \n\r", batt_sys_sts.pm_fg_batt_profile_sts));

  if (PM_FG_BATT_PROFILE_ORIGINAL == batt_sys_sts.pm_fg_batt_profile_sts)
  {
    /* Charging needs to be disabled first and then re-enabled after loading of profile */

    /* Change Charge Enable Source to CMD controlled */
    errFlag |= pm_smbchg_chgr_enable_src(PmicDeviceIndex, FALSE);

    /* Change Charge Enable to Active high */
    errFlag |= pm_smbchg_chgr_set_chg_polarity_low(PmicDeviceIndex, FALSE);
    errFlag |= pm_smbchg_bat_if_config_chg_cmd(PmicDeviceIndex, PM_SMBCHG_BAT_IF_CMD__EN_BAT_CHG, FALSE);

    Status |= PmicFgBattHandleProfile(PmicDeviceIndex);

    /* Enable Charging */
    errFlag |= pm_smbchg_bat_if_config_chg_cmd(PmicDeviceIndex, PM_SMBCHG_BAT_IF_CMD__EN_BAT_CHG, TRUE);
  }
  else    /* PM_FG_BATT_PROFILE_CHANGED */
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: EFI_PmicFgInit: Battery profile loading skipped. Status (%d) Profile Integrity Value (%d) \n\r", Status, batt_sys_sts.pm_fg_batt_profile_sts));

    /* Assign Profile Params for Cache */
    Status |= PmicFgBattProfileReadProfileParams(PmicDeviceIndex, TRUE);

    /* As we do not need to load profile, do Conditional Restart here */
    if( TRUE == BatteryDefaults.FgCondRestart )
    {
      pm_fg_soc_irq_set_trigger(PmicDeviceIndex, PM_FG_SOC_EMPTY_SOC_INT, PM_IRQ_TRIGGER_RISING_EDGE);
      pm_fg_soc_irq_enable(PmicDeviceIndex, PM_FG_SOC_DELTA_SOC_INT, TRUE);
      Status = PmicFgSram_CondRestart(PmicDeviceIndex, BatteryDefaults);
      PMIC_DEBUG(( EFI_D_INFO, "PmicDxe: EFI_PmicFgInit: FG Cond Restart Sequence if VBattEstDiffThreshold = (%d) \n\r", BatteryDefaults.VBattEstDiffThreshold));
    }
    else
    {
      PMIC_DEBUG(( EFI_D_WARN, "PmicDxe: EFI_PmicFgInit: Skipping FG Cond Restart Sequence \n\r", Status));
    }
  }

  /* Fork Cache update and dump feature if available  */
  Status |= PmicFgBattParam_StartCacheUpdate();

  return Status|errFlag;
}

/**
EFI_PmicFgGetChargeCurrent()

@brief
Returns Charge Current (IBAT)
*/
EFI_STATUS
EFIAPI
EFI_PmicFgGetChargeCurrent
(
  IN UINT32 PmicDeviceIndex,
  OUT INT32 *ChargeCurrent
)
{
  INT32 raw_ibat = 0;

  if (NULL == ChargeCurrent)
  {
    return EFI_DEVICE_ERROR;
  }

  if(PM_ERR_FLAG__SUCCESS != (pm_fg_adc_usr_get_calibrated_ibat(PmicDeviceIndex, &raw_ibat)))
  {
    return EFI_DEVICE_ERROR;
  }

  /*Change the sign for charge current according to user perspective*/
  *ChargeCurrent = ((-1) * raw_ibat);
  // PMIC_DEBUG(( EFI_D_ERROR, "ChargerDxe: ChargeCurrent = (%d), raw_ibat (%d)\n\r", *ChargeCurrent, raw_ibat));

  return EFI_SUCCESS;
}

BOOLEAN IsVbattAdjustThreReached(UINT32 PmicDeviceIndex)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  INT32 TerminationCurrent = 0;
  INT32 Ibat = 0;
  BOOLEAN RunVddMaxAdjustment = FALSE;

  err_flag = pm_smbchg_chgr_get_charge_termination_current(PmicDeviceIndex, (UINT32 *)&TerminationCurrent);
  err_flag |= pm_fg_adc_usr_get_calibrated_ibat(PmicDeviceIndex, &Ibat);

  //DEBUG(( EFI_D_ERROR, "IsVbattAdjustThreReached Ibat:%d\n\r", Ibat));

  if (PM_ERR_FLAG__SUCCESS == err_flag)
  {
    /*raw_ibat will be negative if charging. Current going into battery is less than termination current then TRUE*/
    /*In UEFI we don't register for interrupt and so not checking for ICL, delta SoC and charging status change*/
    RunVddMaxAdjustment = (((-1) * Ibat) > TerminationCurrent) ? TRUE : FALSE;
  }
  else
  {
    PMIC_DEBUG((EFI_D_ERROR, "RunVddMaxAdjustment Failed Status = (%d), raw_ibat (%d)\n\r", err_flag));
  }

  return RunVddMaxAdjustment;
}


/*Vdd Max adjustment*/
pm_err_flag_type PmicFgRunVddMaxAdjustment(UINT32 PmicDeviceIndex)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  static int32 max_vbat_mv = 0;
  static uint32 count = 0;
  int32 target_vbat_mv = 0;
  int32 batt_volt_mv = 0;

  PmicFgBattParam_GetCachedBattVolt(&batt_volt_mv);
  //DEBUG((EFI_D_ERROR, "PmicFgRunVddMaxAdjustment batt_volt_mv:%d\n\r", batt_volt_mv));

  err_flag = pm_smbchg_chgr_get_float_volt_cfg(PmicDeviceIndex, (uint32 *)&target_vbat_mv);
  /*for last 40mv*/
  if (target_vbat_mv - batt_volt_mv <= 40)
  {
    count++;
    /*update maximum volt reading*/
    if (batt_volt_mv > max_vbat_mv)
    {
      max_vbat_mv = batt_volt_mv;
    }

    /*if it is within -5/+7 mv range then adjust trim and reset count*/
    /* +1 when max_vbat_mv < target_vbat_mv-5mv  and  -1 when max_vbat_mv > target_vbat_mv+7mv */
    if (max_vbat_mv < (target_vbat_mv - 5))
    {
      pm_smbchg_misc_adjust_float_volt_trim(PmicDeviceIndex, INCREMENT_ONE);
      count = 0;
      max_vbat_mv = 0;
    }
    else if (max_vbat_mv > (target_vbat_mv + 7))
    {
      pm_smbchg_misc_adjust_float_volt_trim(PmicDeviceIndex, DECREMENT_ONE);
      count = 0;
      max_vbat_mv = 0;
    }
  }

  return err_flag;
}

/**
EFI_PmicFgGetSoc()

@brief
Returns Battery State of Charge (SOC)
*/
EFI_STATUS
EFIAPI
EFI_PmicFgGetSoc
(
   IN UINT32 PmicDeviceIndex,
   OUT UINT32 *StateOfCharge
)
{
  EFI_STATUS       Status  = EFI_SUCCESS;
  pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;
  UINT32           TempSoc = 0;

  if (NULL == StateOfCharge)
  {
    return EFI_DEVICE_ERROR;
  }

  errFlag =  pm_fg_soc_get_monotonic_soc(PmicDeviceIndex, (UINT32 *)&TempSoc);
  Status = (PM_ERR_FLAG__SUCCESS == errFlag)? EFI_SUCCESS : EFI_DEVICE_ERROR;

  *StateOfCharge = TempSoc;

  //PMIC_DEBUG(( EFI_D_ERROR, "EFI_PmicFgGetSoc StateOfCharge:%d\n\r", *StateOfCharge));

  if ((EFI_SUCCESS == Status) && (TRUE == IsVbattAdjustThreReached(PmicDeviceIndex)))
  {
    PmicFgRunVddMaxAdjustment(PmicDeviceIndex);
  }

  return Status;
}

/**
EFI_PmicFgGetRatedCapacity()

@brief
Returns Battery Capacity
*/
EFI_STATUS
EFIAPI
EFI_PmicFgGetRatedCapacity
(
  IN UINT32 PmicDeviceIndex,
  OUT UINT32 *RatedCapacity
)
{
  EFI_STATUS   Status  = EFI_SUCCESS;

  //Hard coding the value for now till the data is not available in battery profile
  *RatedCapacity = 3020;

  return Status;
}

/**
EFI_PmicFgGetBatteryVoltage()

@brief
Gets battery voltage
*/
EFI_STATUS
EFIAPI
EFI_PmicFgGetBatteryVoltage
(
   IN  UINT32  PmicDeviceIndex,
   OUT UINT32  *BatteryVoltage
)
{
  UINT32 TempVbat = 0;

  if (NULL == BatteryVoltage)
  {
    return EFI_DEVICE_ERROR;
  }

  if (PM_ERR_FLAG__SUCCESS != pm_fg_adc_usr_get_calibrated_vbat(PmicDeviceIndex, (uint32*)&TempVbat))
  {
    return EFI_DEVICE_ERROR;
  }

  *BatteryVoltage = TempVbat;
  PMIC_DEBUG(( EFI_D_ERROR, "ChargerDxe: BatteryVoltage = (%d) CalVbat (%d)\n\r", *BatteryVoltage, TempVbat));

  return EFI_SUCCESS;
}

/**
EFI_PmicFgGetBatteryConfigData()

@brief
Returns default Config Data
*/
EFI_STATUS
EFIAPI
EFI_PmicFgGetBatteryConfigData
(
  OUT EFI_PM_FG_CFGDATA_TYPE *BatteryCfgData
)
{
  EFI_STATUS   Status  = EFI_SUCCESS;

  Status = PmicFgBattParam_GetDefaultCfg( BatteryCfgData );

  return Status;
}


/**
PmicFgGetBatteryTemperature()

@brief
Cached Battery Temperature of  FG Sram
*/
EFI_STATUS
EFIAPI
EFI_PmicFgGetBatteryTemperature
(
  OUT INT32 *pBattTemperature
)
{
  EFI_STATUS   Status  = EFI_SUCCESS;

  Status = PmicFgBattParam_GetCachedBattTemp(pBattTemperature);

  return Status;
}

/**
EFI_PmicFgIsBattTemperatureValid()

@brief
 Battery Temperature Valid Status
 */
EFI_STATUS
EFIAPI
EFI_PmicFgIsBattTemperatureValid
(
  OUT BOOLEAN *bValid
)
{
  EFI_STATUS   Status  = EFI_SUCCESS;

  if(!bValid)
    return EFI_INVALID_PARAMETER;

  Status = PmicFgBattParam_IsBattTempValid(bValid);

  return Status;
}



/**
EFI_PmicFgGetShadowCurrent()

@brief
Cached shadow current of  FG SRAM
*/
EFI_STATUS
EFIAPI
EFI_PmicFgGetShadowCurrent
(
  IN  UINT32  PmicDeviceIndex,
  OUT INT32 *pShadowCurrent
)
{
  EFI_STATUS   Status  = EFI_SUCCESS;

  Status = PmicFgBattParam_GetCachedShadowCurrent(pShadowCurrent);

  return Status;
}

VOID EFIAPI PmicFgPostExit(VOID)
{
  EFI_CHIPINFO_PROTOCOL     *ChipInfoProtocol = NULL;
  EFIChipInfoIdType         eId = (EFIChipInfoIdType)0;
  gBS->LocateProtocol (&gEfiChipInfoProtocolGuid, NULL, (VOID **) &ChipInfoProtocol);
  if (NULL != ChipInfoProtocol)
  {
    ChipInfoProtocol->GetChipId(ChipInfoProtocol, &eId);
  }

  if ((eId == EFICHIPINFO_ID_MSM8994) && (PMIC_IS_PM8004 == pm_get_pmic_model(PM_DEVICE_2)))
  {
    DEBUG(( EFI_D_ERROR, "******** UEFI IS NOT SUPPORTED ON DEVICE WITH PM8004 ******* \n\r"));
  }

}

/**
PmicFgGetProfileStatus()

@brief
Battery ID Or Profile Parser Status
*/
EFI_STATUS
EFIAPI
EFI_PmicFgGetProfileStatus
(
  OUT EFI_PM_FG_PROFILE_STATUS *ProfileStatus
)
{
  EFI_STATUS   Status  = EFI_SUCCESS;
  EFI_PM_FG_PROFILE_PARSER_STATUS parser_status = PM_PARSER_STATUS_PARSE_ERROR;

  /* Validate Input Param */
  if(!ProfileStatus)
  {
    return EFI_INVALID_PARAMETER;
  }

  /* Get Parser Status */
  Status = PmicFgBattParserStatus(&parser_status);
  if (EFI_SUCCESS == Status)
    ProfileStatus->ParserStatus = parser_status;

  return Status;
}


EFI_STATUS
EFIAPI
EFI_PmicFgGetBatteryType
(
  OUT EFI_PM_FG_BATT_TYPE* BattType
)
{
  EFI_STATUS Status = EFI_SUCCESS;

  if(NULL == BattType)
  {
    Status = EFI_DEVICE_ERROR;
  }
  else
  {
    Status = PmicFgGetBatteryType(BattType);
  }

  return Status;
}

#if 0
/**
PmicFgExitBootServicesEvent()

@brief
UEFI Exit Callback to set proper Exit of Module
*/
VOID EFIAPI PmicFgExitBootServicesEvent( IN EFI_EVENT  Event,
                                             IN VOID       *Context )
{
  UINT32 Soc = 0;

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: ** PmicFgExitBootServicesEvent ** \n\r"));
  //Cancel if any existing timers
  PmicFgBattParam_CancelTimers();
  EFI_PmicFgGetSoc(PM_DEVICE_1, &Soc);
  PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: ** PmicFgExitBootServicesEvent StateOfCharge: (%d) **\r\n", Soc));
  PmicFgPostExit();
}

#endif


/**
EFI_PmicFgExit()

@brief
Exit Fule Gauge Module
*/
EFI_STATUS
EFIAPI
EFI_PmicFgExit
(
  UINT32 PmicDeviceIndex
)
{
  UINT32 Soc = 0;
  FgBattProfileParams Pp = {0};
  BOOLEAN Enable = FALSE;
  pm_fg_batt_sys_sts batt_sys_sts ={0};
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: ** PmicFgExitBootServicesEvent ** \n\r"));

  /* Cancel if any existing timers */
  PmicFgBattParam_CancelTimers();

  EFI_PmicFgGetSoc(PmicDeviceIndex, &Soc);
  PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: ** PmicFgExitBootServicesEvent StateOfCharge: (%d) **\r\n", Soc));

  PmicFgBattProfileGetProfileParams(&Pp);

  /*Rslow WA clear sequence */
  if(TRUE == Pp.RslowPp.RslowWorkaround)
  {
    /*Execute Rslow Clear sequence */
    Pp.RslowPp.RslowFixClrSeq = FALSE;
    PmicFgSram_RunRslowSramRequest(PmicDeviceIndex, &Pp, Soc);
  }

  /* Disable Empty Soc Interrupt*/
  pm_fg_soc_irq_enable(PmicDeviceIndex, PM_FG_SOC_EMPTY_SOC_INT, FALSE);
  pm_fg_soc_irq_clear(PmicDeviceIndex, PM_FG_SOC_EMPTY_SOC_INT);

  /* Clear USB_ID latched interrupt */
  pm_smbchg_usb_chgpth_irq_enable(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_USBID_CHANGE_INTR, FALSE);
  pm_smbchg_usb_chgpth_irq_clear(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_USBID_CHANGE_INTR);

  /* Clear Delta SOC latched interrupt */
  pm_fg_soc_irq_enable(PmicDeviceIndex, PM_FG_SOC_DELTA_SOC_INT, FALSE);
  pm_fg_soc_irq_clear(PmicDeviceIndex, PM_FG_SOC_DELTA_SOC_INT);

  /* Make sure to release SRAM memory access since this is UEFI exit process */
   PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

   pm_fg_memif_get_mem_intf_cfg(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, &Enable);
   if(Enable)
   {
     PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: - MEM_IF_STATUS: (%d)\r\n", Enable));
   }

  PmicFgPostExit();

  /* Profile_integrity check to confirm battery profile valid or not*/
  pm_fg_batt_get_sys_batt_sts(PmicDeviceIndex,&batt_sys_sts);
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgExitBootServicesEvent: After loading Profile: Integrity Value (%d) \n\r", batt_sys_sts.pm_fg_batt_profile_sts));

  return EFI_SUCCESS;
}

/**
  PMIC FG UEFI Protocol implementation
 */
EFI_QCOM_PMIC_FG_PROTOCOL PmicFgProtocolImplementation =
{
  PMIC_FG_REVISION,
  EFI_PmicFgInit,
  EFI_PmicFgGetSoc,
  EFI_PmicFgGetChargeCurrent,
  EFI_PmicFgGetRatedCapacity,
  EFI_PmicFgGetBatteryVoltage,
  EFI_PmicFgGetBatteryConfigData,
  EFI_PmicFgGetBatteryTemperature,
  EFI_PmicFgGetProfileStatus,
  EFI_PmicFgGetBatteryType,
  EFI_PmicFgExit,
  EFI_PmicFgGetShadowCurrent,
  EFI_PmicFgIsBattTemperatureValid,
};

