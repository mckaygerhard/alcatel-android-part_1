#ifndef __PMICFGSRAM_H__
#define __PMICFGSRAM_H__

 /*! @file PmicFgSram.h
 *
 * PMIC Battery Profile SRAM/OTP access functionalities 
 * FG OTP Starts at address 0x0.
 * 256x24 (3 banks of 256x8) byte addressable within a 32 bit word.
 * MSByte of each 32 bit word is invalid and will readback 0x00 and will not be programmed.
 * 
 * FG RAM Starts at address 0x400.
 * 128x32 byte addressable.
 *
 * Copyright (c) 2014 - 2015 Qualcomm Technologies, Inc.  All Rights Reserved. 
 * Qualcomm Technologies Proprietary and Confidential.
 */

/*=============================================================================
                              EDIT HISTORY


 when         who       what, where, why
 --------------------------------------------------------------------------------------
 05/28/15     va        Adding Fule Gauge initial register settting suggested by PSV team
 04/16/15     al        Added Batteryconfiguration as param
 03/31/15     va        Rslow workaround Changes
 02/20/15     al        Adding API to read battery id
 01/26/15     SM        Added TERM_CURRENT and TMP_FRAC_SHIFT register address MACROs 
 01/19/15     al        Adding helper function for battery id
 01/13/15     va        Correcting FG SRAM system termination address
 01/05/15     va        Added Multiple Battery Profile and Profile Parser Status API
 12/26/14     al        Adding SRAM volt reading
 09/25/14     va        Update for New Battery Profile Format
 10/06/14     va        Release FG Sram access during UEFI exit
 06/06/14     va        New file.
=============================================================================*/

/**
  PMIC Platform interface
 */

#include <Protocol/EFIPlatformInfo.h>
#include "DDIPlatformInfo.h"
#include "PmicFgBattProfile.h"
#include "PmicFgBattParameter.h"
#include "pm_fg_batt.h"


/*===========================================================================
                               MACROS
===========================================================================*/
#define BATTARY_PROFILE_MAX_SIZE 0x80 /* 128 bytes */

// Refer to scratch pad register table start from 0x540 - 0x5FF
#define FG_SRAM_SYS_TERM_CURRENT_REG_ADDR 0x40C
#define FG_SRAM_TMP_FRAC_SHIFT_REG_ADDR   0x4A4
#define FG_SRAM_TERM_CURRENT_REG_ADDR     0x4F8
#define FG_SRAM_KI_COEFF_PRED_FULL_REG_ADDR 0x408 /*Ki coefficient when predicting the empty SoC value*/
#define FG_SRAM_KI_COEFF_PRED_FULL_VALUE    4 * 1000 /*Ki coefficient when predicting the empty SoC value*/
#define FG_SRAM_BAT_FULL_RS_COMP_90PCT_REG_ADDR         0x530 //Offset 1,2
#define FG_SRAM_BAT_FULL_RS_COMP_90PCT_REG_DATA         0x3373
#define FG_SRAM_SYS_AUTORCHG_MASKING_DISABLED_REG_ADDR  0x4B0 //offset 1
#define FG_SRAM_SYS_AUTORCHG_MASKING_DISABLED_REG_DATA  0x79
#define FG_SRAM_THERM_C1_COFF_REG_ADDR       0x444 /*2,3*/
#define FG_SRAM_THERM_C2_C3_COFF_REG_ADDR    0x448/*C2 = 0,1 C3= 2,3*/

#define FG_SRAM_VOLT_REG_ADDR             0x5CC
#define FG_SRAM_TEMP_REG_ADDR             0x5D0
#define FG_SRAM_SYS_IRQ_VOLT_REG_ADDR     0x458

#define FG_SRAM_PROFILE_START_ADDR     0x4C0
#define FG_SRAM_PROFILE_END_ADDR       0x53F
#define FG_SRAM_PROFILE_ABS_START_ADDR 0x0C0
#define FG_SRAM_PROFILE_ABS_END_ADDR   0x13F

#define NUM_BITS_IN_BYTE sizeof(UINT8) * 8
#define NUM_BYTE_IN_WORD sizeof(UINT32)
#define FG_SRAM_RD_WR_BUS_WIDTH         4
#define FG_SRAM_RD_WR_OFFSET_WIDTH      3

#define FG_SRAM_PROFILE_FLASHED L"FgProfileFlashed"

#define FG_SRAM_PROFILE_REV_ADDR 31
#define FG_SRAM_PROFILE_REV_APPLY_RSLOW_WA 12 /*Profile Rev greater than to appply Rslow Workaround*/
#define FG_SRAM_RSLOWLOWSOC_TH_COMPADDR    0x488 
#define FG_SRAM_RSLOWCOMPSOCC1C2_ADDR      0x490

/*
SRAM Block      Offset            SRAM Address
System            0x00-0xBF     0x400 - 0x4BF
Profile              0xC0-0x13F    0x4C0 - 0x53F
Scratchpad       0x140-0x1FF  0x540 -  0x5FF
*/
#define FG_SRAM_MAX_SIZE 0x1FF /* Absoulte Sram Max mMemory Size */


/*=========================================================================
                            GLOBAL VARIABLES
===========================================================================*/
/**
  Sram State
*/
typedef enum _FgSramState{
  FG_SRAM_STATUS_INIT,
  FG_SRAM_STATUS_LOAD_PROFILE,
  FG_SRAM_STATUS_IN_USE,
  FG_SRAM_STATUS_POLLING,
  FG_SRAM_STATUS_AVAILABLE,
  FG_SRAM_STATUS_INVALID
}FgSramState;


/**
  BID Detect State
*/
typedef enum{
  //REG[1:0]
  BID_DETECT_BATTERY_SMALL_CURRENT = 0x0,
  BID_DETECT_BATTERY_MEDIUM_CURRENT,
  BID_DETECT_BATTERY_HIGH_CURRENT,
  //REG[3:2]
  BID_DETECT_BATTERY_ID_4P,
  //REG[4]
  BID_DETECT_BATTERY_SMART ,
  BID_DETECT_BATTERY_UNKNOWN,
  BID_DETECT_BATTERY_SHORT,
  BID_DETECT_BATTERY_ERROR,
  BID_DETECT_BATTERY_INVALID = 0x7FFFFFFF
}BidDetectId;


/**
  Battery Profile Reload Params Type
*/
typedef enum {

  PROFILERELOADTYPE_SRAM,
  PROFILERELOADTYPE_OTP,
  PROFILERELOADTYPE_INVALID = 0x7FFFFFFF
}ProfileReloadType;

/**
  Sram Address Data Pair
  Structure to hold address, data which would be updated in single Sram access
*/
typedef struct {
  UINT32 SramAddr;
  UINT32 SramData;
  //UINT32 SramAddrDataCount;//Reserved
}FgSramAddrData;

/**
  Sram Address Data Pair with Data Offset and Size
  Structure to hold address, data which would be updated in single Sram access
*/
typedef struct {
  UINT32 SramAddr;
  UINT32 SramData;
  UINT8  DataOffset; //Offset from Sram Address given valid values 0 -3
  UINT8  DataSize;   //Number of bytes to write (0 - 4): Skip configuring if DataSize=0
}FgSramAddrDataEx;

/**
  Sram Address Data Pair with UINT8 data and offset 
  Structure to hold Address, Data (UINT8), Offset of provide data which would be updated in single Sram access
*/

typedef struct {
  UINT32 SramAddr;//Sram Start Address for the given offset 
  UINT8  SramData;//UINT8 Data
  UINT8  DataOffset;//Offset from Sram Address given valid values 0 -3
}FgSramAddrDataOffset;

/**
  Battery ID and Battery Detect Value read from FG Sram, Tolernace limit would be used for exact read battery id from Sram
*/
typedef struct {
  UINT32      Bid;//Battery ID
  BidDetectId DetId;// Battery ID detected enum value
  UINT32      BatteryIdTolerance;
}FgBattId;


/*===========================================================================
                               FUNCTION DEFINITIONS
===========================================================================*/

EFI_STATUS PmicFgSram_Init(FgSramState FgSramState);

EFI_STATUS PmicFgSram_GetState(FgSramState *FgSramSt);

//Single Check on SRAM memory access
EFI_STATUS PmicFgSram_IsFgMemAvailable(UINT32  PmicDeviceIndex, boolean * SRAMAccessStatus,
                                                 FgSramState *SramState);

EFI_STATUS PmicFgSram_Dump(UINT32  PmicDeviceIndex, UINT32 DumpSramStartAddr,
                                   UINT32 DumpSramEndAddr);

EFI_STATUS PmicFgSram_LoadBattProfile(UINT32  PmicDeviceIndex,
                                               IN FgBattProfile *ProfileData,
                                               IN FgSramAddrDataEx * AddrDataPairEx, IN UINT32 AddrDataCount,
                                               OUT FgBattId *Batt_Id );

EFI_STATUS PmicFgSram_ReadSingleAccess(UINT32 PmicDeviceIndex, OUT FgBattParamCache* BattParamCache,
                                                  IN FgSramState* SramState);

EFI_STATUS PmicFgSram_ProgBurstAccess(UINT32 PmicDeviceIndex, FgSramAddrData * AddrDataPair,
                                                 UINT32 Count);

EFI_STATUS PmicFgSram_ProgBurstAccessEx(UINT32 PmicDeviceIndex, FgSramAddrDataEx * AddrDataPairEx,
                                                   UINT32 Count);

EFI_STATUS PmicFgSram_ProgSingleAccess(UINT32 PmicDeviceIndex, FgSramAddrDataOffset * AddrDataPair,
                                                 UINT32 AddrDataCount);

EFI_STATUS PmicFgSram_ReleaseFgSramAccess(UINT32  PmicDeviceIndex);
EFI_STATUS PmicFgSram_CondRestart(UINT32  PmicDeviceIndex, EFI_PM_FG_CFGDATA_TYPE CfgDataType);

EFI_STATUS PmicFgSramValidateCheckSum( FgBattProfile *profileData, BOOLEAN *bValid);

EFI_STATUS PmicFgSram_ForceOtpProfileReload( UINT32 PmicDeviceIndex,  UINT32 ProfileNum);

EFI_STATUS PmicFgSram_ReadBattID(UINT32 PmicDeviceIndex, FgBattId *Batt_Id);

EFI_STATUS PmicFgSram_PickProfile(FgBattId *Batt_Id, IN FgBattProfile *Profile, UINT32 *ProfileCount);

BOOLEAN    PmicFgSram_BatteryIdInToleranceLimit(FgBattId *Batt_Id, UINT32 Profile_Batt_Id);

EFI_STATUS PmicFgSram_GetStoredBidDetValue(BidDetectId *BatteryIdDet);

EFI_STATUS PmicFgSram_ReadBattIdEx( UINT32 PmicDeviceIndex, FgBattId *Batt_Id);

EFI_STATUS PmicFgSram_GetHALFEncode( INT32 value, UINT16* pEncodedValue);

EFI_STATUS PmicFgSram_RslowChargeClearSeq( UINT32 PmicDeviceIndex, FgBattProfileParams *Pp);

EFI_STATUS PmicFgSram_RslowChargeFixSeq( UINT32 PmicDeviceIndex, FgBattProfileParams *Pp, UINT32 StateOfCharge);

EFI_STATUS PmicFgSram_RunRslowSramRequest( UINT32 PmicDeviceIndex, FgBattProfileParams *Pp, UINT32 StateOfCharge);

EFI_STATUS PmicFgSram_RslowSaveProfileParams( UINT32 PmicDeviceIndex, FgBattProfileParams *Pp);



#endif //__PMICFGSRAM_H__

