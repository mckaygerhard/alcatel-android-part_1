#ifndef __PMICFG__
#define __PMICFG__

/*! @file EFIPmicFg.h
 *
 * PMIC FG UEFI Protocol definitions
 *
 * Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved. 
 * Qualcomm Technologies Proprietary and Confidential.
 */

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 08/04/14   sm      Removed GetVbatt and GetChargeCurrent APIs
 07/08/14   va      New File.
=============================================================================*/

typedef enum
{
  //
  // Nothing
  //
  Pmic_FgStatusNone = 0,
  
  // 
  // The operation completed successfully
  //  
  Pmic_FgStatusSuccess,
  
  // 
  // The battery is getting too hot to charge 
  // 
  Pmic_FgStatusOverheat,
  
  // 
  // Charging logic detected the voltage to be out of the operational range 
  // 
  Pmic_FgStatusVoltageOutOfRange,
  
  // 
  // Charging logic detected the current to be out of the operational range 
  // 
  Pmic_FgStatusCurrentOutOfRange,
  
  // 
  // Charging logic detected that the battery is not getting charged within 
  // a reasonable time 
  //
  Pmic_FgStatusTimeout,
  
  // 
  // The operation was aborted 
  // 
  Pmic_FgStatusAborted,
  
  // 
  // The physical device reported an error 
  // 
  Pmic_FgStatusDeviceError,
  
  // 
  // Battery too cold
  //
  Pmic_FgStatusTooCold,
  
  // 
  // Invalid Battery
  //
  Pmic_FgInvalidBattery
  
} PMIC_FG_STATUS;


/**
  PmicFGGetBatteryTemperature ()

  @brief
  Get Battery Temperature
 */
EFI_STATUS 
PmicFGGetBatteryTemperature
(
  OUT INT32 *pBattTemperature
);



/**
  PmicFgSocInit ()

  @brief
  Initializes event for ExitBootServices
  and other SOC related parameters
 */
EFI_STATUS 
PmicFgSocInit
(
  VOID 
);

#endif  /* __PMICFG__ */

