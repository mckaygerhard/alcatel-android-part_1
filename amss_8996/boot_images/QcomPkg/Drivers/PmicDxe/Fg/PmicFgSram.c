 /*! @file PmicFgSram.c
 *
 * PMIC Battery Profile SRAM/OTP access functionalities 
 * FG OTP Starts at address 0x0.
 * 256x24 (3 banks of 256x8) byte addressable within a 32 bit word.
 * MSByte of each 32 bit word is invalid and will readback 0x00 and will not be programmed.
 * 
 * FG RAM Starts at address 0x400.
 * 128x32 byte addressable.
 *
 * Copyright (c) 2014 - 2015 Qualcomm Technologies, Inc.  All Rights Reserved. 
 * Qualcomm Technologies Proprietary and Confidential.
 */

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 06/04/15   va        Make sure to set Low latency bit false after releasing memory access 
 05/29/15   va        Read Battery Temperature in Fg Init
 05/19/15   va        Fuel Gauge HW workaround for loading battery profile correctly
 05/14/15   al        Replace saved RTC with current time with every boot
 04/14/15   al        Adding change to restart FG is shutdown for more than 24hrs
 04/14/15   va        Disabling Rslow Charg Sequnce for SOC not incresing issue
 04/14/15   va        Restart FG when VBatShadow > VEmpty and Empty SOC INTR is fired
 03/31/15   va        Rslow workaround Changes
 03/13/15   al        Adding changes for optimization
 02/20/15   al        Adding API to read battery ID
 01/26/15   sm        Replaced ADDRESS with ADDR and REGISTER by REG
 01/13/15   va        Correcting VBAT scaling factor
 01/05/15   va        Added Multiple Battery Profile and Profile Parser Status API, Conditional restart workaround encoding fix
 12/17/14   al        Adding SW WA for negative current patching
 12/02/14   va        Conditional restart workaround Changes, SRAM dump log print fix
 10/27/14   va        Update for New Battery Profile Format
 10/06/14   va        Release FG Sram access during UEFI exit
 09/02/14   va        Enable File Logging 
 08/27/14   va        KW Erros Fix.
 05/19/14   va        New file.
 01/19/15   al        Moving helper function to header file
 01/13/15   va        Correcting VBAT scaling factor
 01/05/15   va        Added Multiple Battery Profile and Profile Parser Status API, Conditional restart workaround encoding fix
 12/17/14   al        Adding SW WA for negative current patching
 12/02/14   va        Conditional restart workaround Changes, SRAM dump log print fix
 10/27/14   va        Update for New Battery Profile Format
 10/06/14   va        Release FG Sram access during UEFI exit
 09/02/14   va        Enable File Logging 
 08/27/14   va        KW Erros Fix.
 05/19/14   va        New file.
=============================================================================*/

/**
  PMIC Platform interface
 */

#include <Protocol/EFIPlatformInfo.h>
#include <Library/UefiBootServicesTableLib.h>
#include "DDIPlatformInfo.h"

#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>
#include <Library/QcomLib.h>
#include "string.h"

/** 
File Log Dependencies 
*/ 
#include "PmicFileLog.h"

/**
  FG interface
 */
#include "PmicFgSram.h"
#include "PmicFgBattParameter.h"
#include "pm_fg_soc.h"
#include "pm_fg_batt.h"
#include "pm_fg_memif.h"
#include "pm_smbchg_chgr.h"
#include "pm_smbchg_usb_chgpth.h"

#include "pm_rtc.h"
#include "pm_uefi.h"


/*===========================================================================
                               MACROS
===========================================================================*/
  
#define FG_SRAM_START_ADDRESS 0x400
#define FG_SRAM_END_ADDRESS   0x5FF
#define FG_SRAM_ADDR_INCREMENT 4
#define FG_MEM_AVAILABLE_RT_STS_POLL_MIN_TIME 150 //in ms
#define FG_MEM_AVAILABLE_RT_STS_POLL_MAX_TIME 1470 // in ms
#define FG_FIRST_ITER_DONE_INTR_POLL_MIN_TIME 100 //in ms
#define FG_FIRST_ITER_DONE_INTR_POLL_MAX_TIME 6000 // in ms
#define FG_SRAM_PROFILE_RANGE 0x7F
#define FG_SRAM_PROFILE_CHK_ADDR    0x053C //refer MDOS for address, data values
#define FG_SRAM_PROFILE_CHK_OFFSET  0x0
#define FG_SRAM_PROFILE_CHK_ENABLE  0x01

#define FG_SRAM_SYS_CFG_ADDR  0x4AC

//FG Conditional restart register Address
#define FG_SRAM_V_CURR_PREDICTED 0x540 //Offset is 0 and 2 byte size
#define FG_SRAM_VOLTAGE_SHADOW   0x5CC //Offset is 1 and 2 byte size
//Encoding for voltage 5V - 1 LSB (LSB = 5 / 0x7FFF)
#define FG_SRAM_VOLTAGE_ENCODING 152588 // 5 div 2 to powr 15 in mv
#define VOLTAGE_ENCODING_SCALE_MV 1000000

#define FG_SRAM_VOLTAGE_MAX 5 * 1000         //5 mV
#define FG_SRAM_VOLTAGE_MAX_ENCODING_VALUE 0x7FFF

// BID DETECT Value at start address 0x594 Byte Offset is 1 
// Followed by BID DETECT ADDITIONAL Byte offset 3

#define FG_SRAM_BID_DETECT_ADDR 0x594 
#define FG_SRAM_BID_SMALL_CURRENT_RANGE 5 // uA
#define FG_SRAM_BID_MEDIUM_CURRENT_RANGE 15 // uA
#define FG_SRAM_BID_HIGH_CURRENT_RANGE 150 // uA

#define FG_SRAM_SCRATCH_REG_RTC   0x484 
#define SECS_IN_HOUR              (60*60)
#define MAX_FG_RESTART_24HOUR     24

#define FG_SRAM_BID_ENCODING_CONST 9765 //(5/ 2^9 * 1000000)  is in uA

//Offset uesd for Burst Sram write
#define OFFSET_ZERO 0
#define OFFSET_ONE 1
#define OFFSET_TWO 2
#define OFFSET_THREE 3
#define DATASIZE_ONE 1
#define DATASIZE_TWO 2
#define DATASIZE_THREE 3
#define DATASIZE_FOUR 4
#define DATASIZE_ZERO 0



/*=========================================================================
                            GLOBAL VARIABLES
===========================================================================*/
STATIC FgSramState SramState;
STATIC BOOLEAN     Rslow_Charge_Fix;

/*===========================================================================
                               TYPE DEFINITIONS
===========================================================================*/



/*==========================================================================
                        LOCAL API PROTOTYPES
===========================================================================*/

EFI_STATUS PmicFgSram_SetState(FgSramState FgSramSt);

EFI_STATUS PmicFgSram_RequestFgSramAccess(UINT32  PmicDeviceIndex, 
                                                       pm_fg_memif_mem_intf_cfg  mem_intf_cfg,
                                                       BOOLEAN Fast_Memory_Access);
//Continuous Poll on Sram memory access 
EFI_STATUS PmicFgSram_PollFgSramAccess(UINT32 PmicDeviceIndex, boolean *SramAccessStatus);
pm_err_flag_type PmicFgSram_ReadData(UINT32 PmicDeviceIndex, UINT16 ReadAddress, UINT32 *fg_memif_data);

pm_err_flag_type PmicFgSram_WriteData(UINT32 PmicDeviceIndex, UINT16 WriteAddress, UINT32 fg_memif_data);

EFI_STATUS PmicFgSram_WriteOffsetData(UINT32 PmicDeviceIndex, UINT16 WriteAddress, 
                                               UINT8 Data, UINT8 Offset);

EFI_STATUS PmicFgSram_WriteProfileToSram(UINT32  PmicDeviceIndex, 
                                                    FgBattAddrData profileArray[BATTARY_PROFILE_MAX_SIZE]);
EFI_STATUS PmicFgSram_ValidateProfileWrite(UINT32 PmicDeviceIndex,
                                                   FgBattAddrData profileArray[BATTARY_PROFILE_MAX_SIZE]);
EFI_STATUS PmicFgSram_FgAlgoRestartDoneChk(UINT32 PmicDeviceIndex, pm_fg_soc_alg_sts RestartChkFalg);


void       PmicFgSram_GetBidDetValue(UINT32 BidDetAddValue, BidDetectId *BidDetID);
EFI_STATUS PmicFgSram_RedoBattId(UINT32 PmicDeviceIndex);
EFI_STATUS PmicFgSram_SetFgPreProfileLoad(UINT32 PmicDeviceIndex);

EFI_STATUS PmicFgSram_WriteDataEx(UINT32 PmicDeviceIndex, FgSramAddrDataEx * AddrDataPairEx, UINT32 AddrDataCount);

void       PmicFgSram_PrintState(FgSramState FgSramSt);

EFI_STATUS PmicFgSram_SetProfileReloadParams( UINT32  PmicDeviceIndex, ProfileReloadType ReloadType);

EFI_STATUS PmicFgSram_OverwriteSavedRtcSoC(UINT32 PmicDeviceIndex, UINT32 *FgRestartHrs, BOOLEAN *RtcExceedRestart);

EFI_STATUS PmicFgSram_ReStartFgToReflectProfile(UINT32 PmicDeviceIndex);

EFI_STATUS PmicFgSram_FgAlgoFirstIterDoneChk(UINT32 PmicDeviceIndex);

/*EFI_STATUS PmicFgSram_VerifyRegs(UINT32 PmicDeviceIndex);*/

EFI_STATUS PmicFgSram_ReadBattParamCache(UINT32 PmicDeviceIndex, FgBattParamCache *fgBattParamCache);

/*==========================================================================
                        GLOBAL API DEFINITION
===========================================================================*/
/**
PmicFgSram_Init()

@brief
Initializes Sram State
*/
EFI_STATUS PmicFgSram_Init(FgSramState FgSramState)
{
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_Init \n\r"));
  PmicFgSram_PrintState(FgSramState);
  if (FG_SRAM_STATUS_INIT == FgSramState || FG_SRAM_STATUS_AVAILABLE == FgSramState ){
    SramState = FgSramState;
  }else{
    SramState = FG_SRAM_STATUS_INIT;
  }
  /* Init Rslow workaround Flag */
  Rslow_Charge_Fix = FALSE;
  return EFI_SUCCESS;
}

/**
PmicFgSram_GetState()

@brief
Returns Current Sram State
*/
EFI_STATUS PmicFgSram_GetState(FgSramState *FgSramSt)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  if ( FG_SRAM_STATUS_INVALID == SramState)
  {
    Status = EFI_DEVICE_ERROR;
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_SetState: Error SramState = (%d) \n\r", SramState));
  }
  else{
    *FgSramSt = SramState;
  }

  return Status;
}

/*This API overwrites the FG-SRAM scratch register with current RTC HRs if device has been shutdown for more than FgRestartHrs */
/*If FgRestartHrs and RtcExceedRestart are NULL then it will simply overwrite scratch register with current RTC HRs */

EFI_STATUS PmicFgSram_OverwriteRtcAndDeltaSoC(UINT32 PmicDeviceIndex, UINT32 *FgRestartHrs, BOOLEAN *RtcExceedRestart)
{
  pm_err_flag_type  err_flag   = PM_ERR_FLAG__SUCCESS;
  
  pm_hal_rtc_time_type  hal_time;
  UINT32 CurrentRtcHrs = 0;
  UINT32 SavedRtcDeltaSoC =0;
  UINT32 SavedRtcHrs = 0;
  UINT32 SavedDeltaSoc = 0;

  //read and store current RTC
  err_flag |= pm_rtc_hal_rtc_get_time(PM_DEVICE_0, &hal_time);
  CurrentRtcHrs = hal_time.sec / SECS_IN_HOUR;

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: CurrentRtcHrs = %d \n\r", CurrentRtcHrs));

  //check for fuel gauge restart flag
  if (NULL != RtcExceedRestart && NULL != FgRestartHrs)
  {
    //read saved RTC from scratch register. Compare with with current RTC for restart condition
    err_flag |=  PmicFgSram_ReadData(PmicDeviceIndex, FG_SRAM_SCRATCH_REG_RTC, &SavedRtcDeltaSoC);
    SavedRtcHrs = SavedRtcDeltaSoC >> 16;
    SavedDeltaSoc = SavedRtcDeltaSoC & 0xFF;

    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: SavedRtcDeltaSoC = %d and SavedRtcHrs = %d \n\r", SavedRtcDeltaSoC, SavedRtcHrs));
    /*saved RTC should always be less than current RTC */
    SavedRtcHrs = (SavedRtcHrs > CurrentRtcHrs) ? CurrentRtcHrs : SavedRtcHrs;
    *RtcExceedRestart = ((CurrentRtcHrs - SavedRtcHrs) > *FgRestartHrs) ? TRUE : FALSE;
    /*If FG doesn't need to be re-started then updated only current RTC */
    SavedRtcDeltaSoC = (TRUE == *RtcExceedRestart) ? (CurrentRtcHrs << 16) : ((CurrentRtcHrs << 16) | SavedDeltaSoc);
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: RtcExceedRestart = %d \n\r", *RtcExceedRestart));
    /*overwrite stored RTC and SoC scratch register with current RTC Hrs*/
    err_flag |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, TRUE);
    err_flag |=  PmicFgSram_WriteData(PmicDeviceIndex, FG_SRAM_SCRATCH_REG_RTC, SavedRtcDeltaSoC);
    err_flag |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);
  }
  else
  {
    /*overwrite stored RTC and SoC scratch register with current RTC Hrs*/
    SavedRtcDeltaSoC = CurrentRtcHrs << 16;
    err_flag |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, TRUE);
    err_flag |=  PmicFgSram_WriteData(PmicDeviceIndex, FG_SRAM_SCRATCH_REG_RTC, SavedRtcDeltaSoC);
    err_flag |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);
  }

  return (EFI_STATUS)err_flag;
}

/**
PmicFgSram_CondRestart()

@brief
Conditional Restart for Fuel Gauge Algorithm 
*/
EFI_STATUS PmicFgSram_CondRestart
(
  UINT32  PmicDeviceIndex, EFI_PM_FG_CFGDATA_TYPE CfgDataType
)
{
  EFI_STATUS        Status    = EFI_SUCCESS;
  pm_err_flag_type  err_flg   = PM_ERR_FLAG__SUCCESS;
  BOOLEAN           SramAccessStatus = FALSE;
  UINT32            VCurrPred = 0, VoltageShadow = 0, CurrVoltShadow = 0, VEmpty = 0; 
  INT32             VBattEstDiff = 0;
  BOOLEAN           EmptySocIrqstatus = FALSE;
  UINT32 VBattEstDiffThreshold = CfgDataType.VBattEstDiffThreshold;
  BOOLEAN  RtcExceedRestart = FALSE;
  UINT32   FgRestartHrs =CfgDataType.RestartFgInactiveHrs;
  FgBattParamCache BattParamCache = {0};

  /*PmicFgSram_VerifyRegs(PmicDeviceIndex);*/

  /* Refer to 5.7 section of MDOS section */

  //Request the memory access
  Status  =  PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, TRUE);

  //Request/Poll the memory access
  Status |= PmicFgSram_PollFgSramAccess(PmicDeviceIndex, &SramAccessStatus);

  if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
  {
    //Read few values to check if we need to restart FG algo

    //Requet Read Access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);

    /* Read Batt Param Cache for temperature */
    Status |= PmicFgSram_ReadBattParamCache(PmicDeviceIndex, &BattParamCache);

    /*Update FG batt Cache for Temperature */
    Status |= PmicFgBattParam_UpdateCache(&BattParamCache);

    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, FG_SRAM_V_CURR_PREDICTED, &VCurrPred);

    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, FG_SRAM_VOLT_REG_ADDR, &CurrVoltShadow);

    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, FG_SRAM_SYS_IRQ_VOLT_REG_ADDR, &VEmpty);

    Status  |=  PmicFgBattParamConvertVolt(&CurrVoltShadow);

    Status  |=  PmicFgBattParamConvertVEmptyIrq(&VEmpty);

    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_CondRestart: CurrVoltShadow = (%d) VEmpty =(%d) \n\r", CurrVoltShadow, VEmpty));

    err_flg |=  pm_fg_soc_irq_status(PmicDeviceIndex, PM_FG_SOC_EMPTY_SOC_INT, PM_IRQ_STATUS_LATCHED, &EmptySocIrqstatus);
    if((PM_ERR_FLAG__SUCCESS == err_flg) && (TRUE == EmptySocIrqstatus))
    {
      PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: Empty SOC interrrupt HIGH (%d) \n\r", EmptySocIrqstatus));
      pm_fg_soc_irq_clear(PmicDeviceIndex, PM_FG_SOC_EMPTY_SOC_INT);
    }

    VCurrPred = (VCurrPred & 0xFFFF); //Get 0, 1 byte 
    if (VCurrPred > FG_SRAM_VOLTAGE_MAX_ENCODING_VALUE){
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_CondRestart: Error VCurrPred = (%d) Out of Range \n\r", VCurrPred));
    }

    //Convert values to voltage Encoding according to MDOS
    VCurrPred = (FG_SRAM_VOLTAGE_ENCODING * VCurrPred)/VOLTAGE_ENCODING_SCALE_MV;
    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, FG_SRAM_VOLTAGE_SHADOW, &VoltageShadow);

    VoltageShadow = ((VoltageShadow >> 8 )& 0xFFFF); //Get 1, 2 byte 
    if (VoltageShadow > FG_SRAM_VOLTAGE_MAX_ENCODING_VALUE){
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_CondRestart: Error VoltageShadow = (0x%x) Out of Range \n\r", VoltageShadow));
    }

    //Convert values to voltage Encoding according to MDOS
    VoltageShadow = (FG_SRAM_VOLTAGE_ENCODING * VoltageShadow)/VOLTAGE_ENCODING_SCALE_MV;

    //Get respective 2 bytes and Convert to number format VCurrPred, VoltageShadow 
    VBattEstDiff = (VCurrPred - VoltageShadow);
    
    /*check if device was shutdown for more than 24hrs*/
    Status |= PmicFgSram_OverwriteRtcAndDeltaSoC(PmicDeviceIndex, &FgRestartHrs, &RtcExceedRestart);

    if ((ABS(VBattEstDiff) > VBattEstDiffThreshold ) || ((TRUE == EmptySocIrqstatus ) && (CurrVoltShadow > VEmpty)) || (TRUE == RtcExceedRestart))
    {
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_CondRestart: Restarting Fg \n\r"));

      //Set FG_SOC_PREVENT_OTP_PROFILE_RELOAD to 0
      pm_fg_soc_set_fg_soc_boot_mod(PmicDeviceIndex, PM_FG_SOC_PREVENT_OTP_PROFILE_RELOAD, TRUE);

      //When the memory access is granted set RERUN_FIRST_EST = 1 and GO = 1
      err_flg |=  pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RERUN_FIRST_EST, TRUE);

      //GO = 1
      pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_GO, TRUE);

      //Release the memory access
      Status |= PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);
      //Wait for Restart Done Status. Skipping it for boot time optimization
      //Status |= PmicFgSram_FgAlgoRestartDoneChk(PmicDeviceIndex);

      //Clear status 
      err_flg |=  pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RERUN_FIRST_EST, FALSE);
      pm_fg_soc_set_fg_soc_boot_mod(PmicDeviceIndex, PM_FG_SOC_PREVENT_OTP_PROFILE_RELOAD, FALSE);
      //GO = 0
      err_flg |=  pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_GO, FALSE);
    }
    else
    {
      PMIC_DEBUG(( EFI_D_INFO, "PmicDxe: PmicFgSram_CondRestart: Skipped  VBattEstDiffThreshold = (0x%x), VBattEstDiff = (0x%x) VCurrPred = (0x%x), VoltageShadow = (0x%x)\n\r", VBattEstDiffThreshold, VBattEstDiff, VCurrPred, VoltageShadow));
    }
  }
  else
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_CondRestart: PmicFgSram_PollFgSramAccess Failed : Status = (%d) SRAMAccessStatus = (%d) \n\r", Status, SramAccessStatus));
  }

  //By Default Release SRAM Memory access
  Status |= PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  //make sure to not have low latency access 
  err_flg = pm_fg_memif_set_mem_intf_cfg(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_LOW_LATENCY_ACS_EN, FALSE);

  //Set Sram module internal state to available
  Status  |= PmicFgSram_SetState(FG_SRAM_STATUS_AVAILABLE);
  /*PmicFgSram_VerifyRegs(PmicDeviceIndex);*/
  return (Status | err_flg);
}


/*This SW WA will let the FG read negative current reading only when the charger is charging the battery. 
  This is needed to make sure that in low current situation we don�t get stuck to an SoC due to the internal adc 
  giving negative for the offset and the monotonic filter preventing the soc from decreasing. */
EFI_STATUS PmicFgSram_NegativeCurrentPatching(UINT32 PmicDeviceIndex)
{
  EFI_STATUS  Status   = EFI_SUCCESS;
  pm_err_flag_type  err_flag = PM_ERR_FLAG__SUCCESS;
  UINT32 CurrentPatching = 0;
  err_flag |= pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);
  err_flag |= PmicFgSram_ReadData(PmicDeviceIndex, FG_SRAM_SYS_CFG_ADDR, &CurrentPatching);
  err_flag |= pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, TRUE);
  err_flag |= PmicFgSram_WriteData(PmicDeviceIndex,FG_SRAM_SYS_CFG_ADDR, (CurrentPatching| (0x1<<19)));

  Status = (PM_ERR_FLAG__SUCCESS == err_flag)? PM_ERR_FLAG__SUCCESS : EFI_DEVICE_ERROR;

  return Status;
}


/**
PmicFgSram_ForceOtpProfileReload()

@brief
Load Selected Battery Profile from OTP 
*/
EFI_STATUS PmicFgSram_ForceOtpProfileReload
(
  UINT32  PmicDeviceIndex,
  UINT32  ProfileNum
)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  BOOLEAN     SramAccessStatus = FALSE;

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ForceOtpProfileReload\n\r"));

  //Request the memory access with burst
  PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, TRUE);

  //Request/Poll the memory access
  Status |= PmicFgSram_PollFgSramAccess(PmicDeviceIndex, &SramAccessStatus);
  if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
  {
    PmicFgSram_SetProfileReloadParams(PmicDeviceIndex, PROFILERELOADTYPE_OTP);
    /* For SW profile to be used On OTP Profile Reload */
    pm_fg_batt_force_sw_batt_id(PmicDeviceIndex, PM_FG_BATT_SW_BATT_ID_FORCE_BATID_TRUE);
    pm_fg_batt_set_batt_profile_id(PmicDeviceIndex, (pm_fg_batt_profile_id)ProfileNum);

    //Set Restart Reload profile to 1
    pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RELOAD_PROFILE, TRUE);
    //GO = 1
    pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_GO, TRUE);
  }

  //Release the memory access 
  Status |= PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  //make sure to not have low latency access 
  pm_fg_memif_set_mem_intf_cfg(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_LOW_LATENCY_ACS_EN, FALSE);

  //removing this check to optimize boot time
  //if (Status == EFI_SUCCESS)
  //{
  //  Status |= PmicFgSram_FgAlgoRestartDoneChk(PmicDeviceIndex);
  //}
  return Status;
}

/**
PmicFgSram_LoadBattProfile()

@brief
Load Battery Profile in Sram
*/
EFI_STATUS PmicFgSram_LoadBattProfile
(
  UINT32  PmicDeviceIndex,
  IN FgBattProfile *Profile,
  IN FgSramAddrDataEx * AddrDataPairEx, IN UINT32 AddrDataCount,
  OUT FgBattId *Batt_Id
)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  BOOLEAN     SramAccessStatus = FALSE;
  BOOLEAN     bValidProfile = FALSE;
  UINT32      ProfileIndex = 0;
  EFI_PM_FG_CFGDATA_TYPE BatteryDefaults;
  BOOLEAN     bBatIdInToleranceLimit = FALSE;
  UINT32            AddrDataThermExCnt = 0;
  FgSramAddrDataEx  AddrDataThermEx[2]; // = {0};
  FgBattParamCache  fgBattParamCache = {0};
  /* Debug SRAM dump before loading profile */
  /*PmicFgSram_VerifyRegs(PmicDeviceIndex);*/

  //Get Battery Defaults
  Status |= PmicFgBattParam_GetDefaultCfg(&BatteryDefaults);

  //validate profile for check sum, if check sum validates then only load profile other wise just retrieve Batt ID
  Status |= PmicFgSramValidateCheckSum(Profile, &bValidProfile);
  if (TRUE == bValidProfile && Status == EFI_SUCCESS)
  {
    /*  1. make sure we are not in memory access
     2. set NO_OTP_RELOAD to 0
     3. set RESTART_GO and REDO_FIRST_ESTIMATE to 0
     4. set low latency to 1
     5. get memory access, do a read or whatever, then release memory access
     6. wait 3 seconds
     7. make sure we are not in memory access again
     8. set low latency to 0
     9. write the battery profile into SRAM
     10. release access
     11. set NO_OTP_RELOAD to 1
     12. set REDO_FIRST_ESTIMATE and RESTART_GO to 1
     13. wait up to 6 second for a first_estimate done interrupt
     14. set NO_OTP_RELOAD to 0
     15. set REDO_FIRST_ESTIMATE and RESTART_GO to */
    /*steps 1 -6 */
    Status |= PmicFgSram_SetFgPreProfileLoad(PmicDeviceIndex);

    /*Steps 7 - 10 */
    //Release the memory access 
    Status |= PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

    pm_fg_memif_set_mem_intf_cfg(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_LOW_LATENCY_ACS_EN, FALSE);
    PMIC_DEBUG(( EFI_D_ERROR, "PmicFgSram_ReleaseFgSramAccess: Clearing LOW_LATENCY_ACS_EN \n\r"));

    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_LoadBattProfile Start \n\r"));

    //Request the memory access 
    PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, FALSE);

    //Request/Poll the memory access
    Status |= PmicFgSram_PollFgSramAccess(PmicDeviceIndex, &SramAccessStatus);
    if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
    {
      //Read Battery ID from SRAM and compare it with 
      //Check for Battery ID here
      Status |= PmicFgSram_ReadBattID(PmicDeviceIndex, Batt_Id);
      /*Read Battery Param Cache */
      Status |= PmicFgSram_ReadBattParamCache(PmicDeviceIndex, &fgBattParamCache);

      Status |= PmicFgSram_PickProfile(Batt_Id, Profile, &ProfileIndex);
      bBatIdInToleranceLimit = PmicFgSram_BatteryIdInToleranceLimit(Batt_Id, Profile->Pd[ProfileIndex].Pp.BatteryId);
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_LoadBattProfile: ProfileIndex = (%d) FgForceOtpProfile = (%d) bBatIdInToleranceLimit = (%d)\n\r",
                   ProfileIndex, BatteryDefaults.FgForceOtpProfile, bBatIdInToleranceLimit));
      if ((TRUE == bBatIdInToleranceLimit ) || (BatteryDefaults.FgForceOtpProfile == FALSE))
      {
        //Set Sram module internal state 
        PmicFgSram_SetState(FG_SRAM_STATUS_LOAD_PROFILE);

        //Write the profile in the volatile memory
        Status |= PmicFgSram_WriteProfileToSram(PmicDeviceIndex, Profile->Pd[ProfileIndex].addrData);

        //Make sure that the register Profile_check has the Integrity Profile set to 1
        Status |= PmicFgSram_WriteOffsetData(PmicDeviceIndex, (UINT16)FG_SRAM_PROFILE_CHK_ADDR, 
                                             (UINT8) FG_SRAM_PROFILE_CHK_ENABLE, (UINT8)FG_SRAM_PROFILE_CHK_OFFSET);
        /*Save Profile Params one time only */
        if( (((Profile->Pd[ProfileIndex].addrData[FG_SRAM_PROFILE_REV_ADDR].Data) >> 24) & 0xFF ) >= FG_SRAM_PROFILE_REV_APPLY_RSLOW_WA)
        {
          Status |= PmicFgSram_RslowSaveProfileParams(PmicDeviceIndex, &(Profile->Pd[ProfileIndex].Pp));
          //Status != PmicFgSram_RslowChargeFixSeq(PmicDeviceIndex, &(Profile->Pd[ProfileIndex].Pp), Soc);
        }
      }
      else
      {
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_LoadBattProfile: Force OTP profile InValid Bat ID Batt_Id.Bid = (0x%x), Profile->BatteryId = (0x%x)\n\r",
                     Batt_Id->Bid, Profile->Pd[ProfileIndex].Pp.BatteryId));
        PmicFgSram_SetProfileReloadParams(PmicDeviceIndex, PROFILERELOADTYPE_OTP);
        /* For SW profile to be used On OTP Profile Reload */
        pm_fg_batt_force_sw_batt_id(PmicDeviceIndex, PM_FG_BATT_SW_BATT_ID_FORCE_BATID_TRUE);
        pm_fg_batt_set_batt_profile_id(PmicDeviceIndex, (pm_fg_batt_profile_id)BatteryDefaults.FgOtpProfileNum);
        //GO = 1
        pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_GO, TRUE);
      }
    }
    else
    {
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_PollFgSramAccess Failed : Status = (%d) SRAMAccessStatus = (%d) \n\r", Status, SramAccessStatus));
    }
  }
  else
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_LoadBattProfile: InValid Profile/CheckSum \n\r"));
  }

  //Update Sram system register if any provided..Best effort call 
  if ((NULL != AddrDataPairEx) && (0 != AddrDataCount)){
    Status |= PmicFgSram_WriteDataEx(PmicDeviceIndex, AddrDataPairEx, AddrDataCount);
  }
  else
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_LoadBattProfile: Write System Register skipped AddrDataCount = (%d)\n\r", AddrDataCount));

  if(0 != Profile->Pd[ProfileIndex].Pp.ThermCoff.ThermC1Coeff || 0 != Profile->Pd[ProfileIndex].Pp.ThermCoff.ThermC2Coeff || 0 != Profile->Pd[ProfileIndex].Pp.ThermCoff.ThermC3Coeff )
  {
    AddrDataThermEx[0].SramAddr = FG_SRAM_THERM_C1_COFF_REG_ADDR; AddrDataThermEx[0].SramData = Profile->Pd[ProfileIndex].Pp.ThermCoff.ThermC1Coeff;
    AddrDataThermEx[0].DataSize = 2; AddrDataThermEx[0].DataOffset = 2;
    AddrDataThermEx[1].SramAddr = FG_SRAM_THERM_C2_C3_COFF_REG_ADDR;
    AddrDataThermEx[1].SramData = (Profile->Pd[ProfileIndex].Pp.ThermCoff.ThermC2Coeff & 0xFFFF) | (Profile->Pd[ProfileIndex].Pp.ThermCoff.ThermC3Coeff << 16);
    AddrDataThermEx[1].DataSize = 4; AddrDataThermEx[1].DataOffset = 0;
    AddrDataThermExCnt = 2;
    /* Update Therm cofficicent */
    Status |= PmicFgSram_WriteDataEx(PmicDeviceIndex, AddrDataThermEx, AddrDataThermExCnt);
  }else{
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_LoadBattProfile: Write Cofficients skipped \n\r"));
  }

  /*work around for negative current patching*/
  Status |= PmicFgSram_NegativeCurrentPatching(PmicDeviceIndex);

  /*Clear scratch register for delta SoC and saved time*/
  Status |= PmicFgSram_OverwriteRtcAndDeltaSoC(PmicDeviceIndex, NULL, NULL);

  ////Release the memory access 
  Status |= PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  /*Update FG batt Cache for Temperature */
  Status |= PmicFgBattParam_UpdateCache(&fgBattParamCache);

  //Print profile name which is valid and getting loaded
  PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: Battery Profile Name flashed = (%s) \n\r", Profile->Pd[ProfileIndex].Pp.BatteryName));
  PMIC_UART_DEBUG(( EFI_D_ERROR, "PmicDxe: Battery Profile Name flashed = (%a) \n\r", Profile->Pd[ProfileIndex].Pp.BatteryName));

  //Set Sram module internal state to available
  PmicFgSram_SetState(FG_SRAM_STATUS_AVAILABLE);

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_LoadBattProfile End \n\r"));

  /*Steps 11 -15 */
  Status |= PmicFgSram_ReStartFgToReflectProfile(PmicDeviceIndex);

  /* Debug SRAM dump After loading profile */
  /*Status |= PmicFgSram_VerifyRegs(PmicDeviceIndex);*/

  return Status;
}

EFI_STATUS PmicFgSram_SetProfileReloadParams( UINT32  PmicDeviceIndex, ProfileReloadType ReloadType)
{
  EFI_STATUS Status = EFI_SUCCESS;

  /* Reset Registers */
  pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_GO, FALSE);
  pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RELOAD_SYS, FALSE);
  pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RELOAD_PROFILE, FALSE);
  pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RERUN_FIRST_EST, FALSE);
  pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_REDO_BATID_DURING_FIRST_EST, FALSE);

  switch(ReloadType)
  {
    case PROFILERELOADTYPE_SRAM:
      //Set FG_SOC_PREVENT_OTP_PROFILE_RELOAD to 0
      pm_fg_soc_set_fg_soc_boot_mod(PmicDeviceIndex, PM_FG_SOC_PREVENT_OTP_PROFILE_RELOAD, FALSE);

      //When the memory access is granted set RERUN_FIRST_EST = 1 and GO = 1
      pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RERUN_FIRST_EST, FALSE);
    break;

    case PROFILERELOADTYPE_OTP:
    default:
      //Set FG_SOC_PREVENT_OTP_PROFILE_RELOAD to 0
      pm_fg_soc_set_fg_soc_boot_mod(PmicDeviceIndex, PM_FG_SOC_PREVENT_OTP_PROFILE_RELOAD, FALSE);

      //When the memory access is granted set RERUN_FIRST_EST = 1 and GO = 1
      pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RERUN_FIRST_EST, TRUE);
    break;
  }

  return Status;
}


EFI_STATUS PmicFgSram_PickProfile
(
  FgBattId *Batt_Id,
  FgBattProfile *Profile,
  UINT32 *ProfileCount
)
{
  UINT32 count = 0;
  BOOLEAN ProfileValid = FALSE;

  for(count = 0; (count < MAX_NUM_OF_BATT_PROFILE ) && (count < Profile->ProfileCount )
              && (NULL != Profile->Pd[count].addrData); count++)
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_PickProfile: Count = (0x%x) Batt_Id.Bid= (0x%x) Profile[%d].BatteryId = (0x%x)\n\r",
                        count, Batt_Id->Bid, count, Profile->Pd[count].Pp.BatteryId));
    ProfileValid = PmicFgSram_BatteryIdInToleranceLimit(Batt_Id, Profile->Pd[count].Pp.BatteryId);
    if(ProfileValid == TRUE)
    {
      *ProfileCount = count;
      break;
    }
  }

  if(ProfileValid == FALSE || count == MAX_NUM_OF_BATT_PROFILE){
    /* Make default profile as first */
  *ProfileCount = 0;
  }
  //Assign for which profile is selected
  Profile->SelectedProfile = *ProfileCount;

  return EFI_SUCCESS;

}


EFI_STATUS PmicFgSram_ReadBattID(UINT32 PmicDeviceIndex, FgBattId *Batt_Id)
{
  EFI_STATUS Status = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  UINT32           ReadData = 0;
  UINT32  BidDetValue = 0, BidDetAddValue = 0;
  BidDetectId BidDetID = BID_DETECT_BATTERY_INVALID;

  //Request single access 
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_BURST, FALSE);

  //Request Read Access
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);

  Status |= PmicFgSram_ReadData(PmicDeviceIndex, FG_SRAM_BID_DETECT_ADDR, &ReadData);

  BidDetValue = (ReadData >> 8 )& 0xFF;//Get byte offset 1
  
  BidDetAddValue = (ReadData >> 24 )& 0xFF;//Get byte offset 3

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadBattID: ReadData = (0x%x) BidDetValue = (0x%x) BidDetAddValue = (0x%x)\n\r", ReadData, BidDetValue, BidDetAddValue));

  BidDetValue = BidDetValue * (FG_SRAM_BID_ENCODING_CONST);

  PmicFgSram_GetBidDetValue(BidDetAddValue, &BidDetID);
  switch(BidDetID)
  {
    case BID_DETECT_BATTERY_MEDIUM_CURRENT:
    {
      Batt_Id->Bid = BidDetValue / (FG_SRAM_BID_MEDIUM_CURRENT_RANGE );
      Batt_Id->DetId = BID_DETECT_BATTERY_MEDIUM_CURRENT;
    }
    break;
    case BID_DETECT_BATTERY_SMALL_CURRENT:
    {
      Batt_Id->Bid = BidDetValue / (FG_SRAM_BID_SMALL_CURRENT_RANGE );
      Batt_Id->DetId = BID_DETECT_BATTERY_SMALL_CURRENT;
    }
    break;
    case BID_DETECT_BATTERY_HIGH_CURRENT:
    {
      Batt_Id->Bid = BidDetValue / (FG_SRAM_BID_HIGH_CURRENT_RANGE );
      Batt_Id->DetId = BID_DETECT_BATTERY_HIGH_CURRENT;
    }

    break;
    case BID_DETECT_BATTERY_SMART:
      //Return type of smart battery here to the caller 
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadBattID: BID_DETECT_BATTERY_SMART = (%d)\n\r", BidDetID));
      Batt_Id->DetId = BID_DETECT_BATTERY_SMART;
      Batt_Id->Bid = BidDetValue / (FG_SRAM_BID_SMALL_CURRENT_RANGE );
    break;

    case BID_DETECT_BATTERY_UNKNOWN:
    case BID_DETECT_BATTERY_ERROR:
    case BID_DETECT_BATTERY_SHORT:
       Batt_Id->DetId = BidDetID;
      //Error Out
    break;

    default:
      Batt_Id->DetId = BID_DETECT_BATTERY_INVALID;
    break;
  }
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadBattID: Batt_Id->Bid = (%d)\n\r", Batt_Id->Bid));
  
  return Status;
}


/**
PmicFgSram_ReadBattParamCache()

@brief
Read Battery Parameter Cache data from FG SRAM 
*/
EFI_STATUS PmicFgSram_ReadBattParamCache(UINT32 PmicDeviceIndex, FgBattParamCache *fgBattParamCache)
{
  EFI_STATUS   Status   = EFI_SUCCESS;
  UINT32       ReadData = 0;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  if(!fgBattParamCache)
    return EFI_INVALID_PARAMETER;

  //Requet read access 
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);

  /*Read voltage and temperature*/
  err_flg |= PmicFgSram_ReadData(PmicDeviceIndex, FG_SRAM_TEMP_REG_ADDR, &ReadData);
  fgBattParamCache->FgBattParam_CachedTemperature = ReadData;

  err_flg |= PmicFgSram_ReadData(PmicDeviceIndex, FG_SRAM_VOLT_REG_ADDR, &ReadData);
  fgBattParamCache->FgBattParam_CachedBattVoltage = ReadData;

  /* Update Temperature Read Value Flag */
  fgBattParamCache->FgBattParam_TemperatureValid = TRUE;

  return Status;
}


/**
PmicFgSram_BatteryIDInToleranceLimit()

@brief
Check Battery id tolerance limit against battery id from sram and profile 
*/
BOOLEAN PmicFgSram_BatteryIdInToleranceLimit(FgBattId *Batt_Id, UINT32 Profile_Batt_Id)
{
  UINT32 lower_limit = 0, higher_limit = 0;

  if(!Batt_Id)
    return FALSE;

  lower_limit  = Profile_Batt_Id - ((Profile_Batt_Id * Batt_Id->BatteryIdTolerance)/ 100);
  higher_limit = Profile_Batt_Id + ((Profile_Batt_Id * Batt_Id->BatteryIdTolerance)/ 100);

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_BatteryIdInToleranceLimit: lower_limit = (%d), higher_limit = (%d)\n\r", lower_limit, higher_limit));

  if ((Batt_Id->Bid >= lower_limit) && (Batt_Id->Bid <= higher_limit))
    return TRUE;
  else
    return FALSE;
}


/**
PmicFgSram_GetBidDetValue()

@brief
Retrieve battery id value based on BID additional value for current
*/
void PmicFgSram_GetBidDetValue(UINT32 BidDetAddValue, BidDetectId *BidDetID)
{
  UINT32 Reg1Mask = 0x03, iValue = 0;

  if ((BidDetAddValue >> BID_DETECT_BATTERY_SMART ) == TRUE )
      *BidDetID = BID_DETECT_BATTERY_SMART;
/* Do not need as of now
  else if ((BidDetAddValue >> BID_DETECT_BATTERY_ERROR ) == TRUE )
    *BidDetID = BID_DETECT_BATTERY_ERROR;
  else if ((BidDetAddValue >> BID_DETECT_BATTERY_SHORT ) == TRUE )
    *BidDetID = BID_DETECT_BATTERY_SHORT;
  else if ((BidDetAddValue >> BID_DETECT_BATTERY_UNKNOWN ) == TRUE )
    *BidDetID = BID_DETECT_BATTERY_UNKNOWN;
  else if ((BidDetAddValue >> BID_DETECT_BATTERY_SHORT )== TRUE )
    *BidDetID = BID_DETECT_BATTERY_SHORT;
*/
  else{

    iValue = BidDetAddValue & Reg1Mask;
    switch(iValue)
    {
      case 1:
        *BidDetID = BID_DETECT_BATTERY_SMALL_CURRENT;
      break;
      case 2:
        *BidDetID = BID_DETECT_BATTERY_MEDIUM_CURRENT;
      break;
      case 3:
        *BidDetID = BID_DETECT_BATTERY_HIGH_CURRENT;
      break;
      case 0:
        *BidDetID = BID_DETECT_BATTERY_ERROR;
      break;

      default:
      break;
    }
  }

  return;
}


/**
PmicFgSram_ReadBattIdEx()
 
@brief
Read Battery Id from SRAM explicit call
Redo Batt Id workaround for Batter Id accuracy
*/
EFI_STATUS PmicFgSram_ReadBattIdEx
(
  UINT32 PmicDeviceIndex, FgBattId *Batt_Id)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  BOOLEAN     SramAccessStatus = FALSE;
 
  //Redo Batt ID estimation Here for HW workaround of battery ID read
  Status |= PmicFgSram_RedoBattId(PmicDeviceIndex);
 
  //Request the memory access with low latency
  PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, TRUE);
 
  //Request/Poll the memory access
  Status |= PmicFgSram_PollFgSramAccess(PmicDeviceIndex, &SramAccessStatus);
 if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
  {
    //Read Battery ID from SRAM
    Status |= PmicFgSram_ReadBattID(PmicDeviceIndex, Batt_Id);
  }
  else
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_PollFgSramAccess Failed : Status = (%d) SRAMAccessStatus = (%d) \n\r", Status, SramAccessStatus));
  }
 
  ////Release the memory access
  Status |= PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  //make sure to not have low latency access 
  pm_fg_memif_set_mem_intf_cfg(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_LOW_LATENCY_ACS_EN, FALSE);

  //Set Sram module internal state to available
  PmicFgSram_SetState(FG_SRAM_STATUS_AVAILABLE);
 
  return Status;
}

/**
PmicFgSram_RedoBattId()

@brief
Redo Batt ID estimation Here for HW workaround of battery ID read
*/
EFI_STATUS PmicFgSram_RedoBattId(UINT32 PmicDeviceIndex)
{
  EFI_STATUS Status = EFI_SUCCESS;
  BOOLEAN     SramAccessStatus = FALSE;

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_RedoBattId \n\r"));

  //Request the memory access with burst
  PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, FALSE);
  
  //Request/Poll the memory access
  Status |= PmicFgSram_PollFgSramAccess(PmicDeviceIndex, &SramAccessStatus);
  if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
  {    

    //Set Restart Reload profile to 0
    pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RELOAD_PROFILE, FALSE);

    //Set Restart Reload System setting to 0
    pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RELOAD_SYS, FALSE);

    //When the memory access is granted set RERUN_FIRST_EST = 1 and GO = 1
    //Skip SoC first est since not required for re do batt id calculation
    pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RERUN_FIRST_EST, FALSE);
    
    //Perform a Battery ID detection when First SoC is requested
    pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_REDO_BATID_DURING_FIRST_EST, TRUE);

    //GO = TRUE, perform above requested sequence
    pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_GO, TRUE);

  }
  else
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_PollFgSramAccess Failed : Status = (%d) SRAMAccessStatus = (%d) \n\r", Status, SramAccessStatus));
  }
  //Release the memory access
  Status |= PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  //Wait for Restart Done Status 
  Status |= PmicFgSram_FgAlgoRestartDoneChk(PmicDeviceIndex, PM_FG_SOC_ALG_RESTART_DONE);

  //Set Sram module internal state to available
  PmicFgSram_SetState(FG_SRAM_STATUS_AVAILABLE);

  return Status;
}


/**
PmicFgSram_SetFgPreProfileLoad()

@brief
Redo Batt ID estimation Here for HW workaround of battery ID read
*/
EFI_STATUS PmicFgSram_SetFgPreProfileLoad(UINT32 PmicDeviceIndex)
{
  EFI_STATUS Status = EFI_SUCCESS;
  BOOLEAN     SramAccessStatus = FALSE;
  UINT32       ReadData = 0;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_SetFgPreProfileLoad Start \n\r"));
  //Make sure we do not have access hence Release memory access at first 
  Status |= PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  //Set FG_SOC_PREVENT_OTP_PROFILE_RELOAD to 0
  pm_fg_soc_set_fg_soc_boot_mod(PmicDeviceIndex, PM_FG_SOC_PREVENT_OTP_PROFILE_RELOAD, FALSE);

    //When the memory access is granted set RERUN_FIRST_EST = 1 and GO = 1
  //Skip SoC first est since not required for re do batt id calculation
  pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RERUN_FIRST_EST, FALSE);

    //GO = TRUE, perform above requested sequence
  pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_GO, FALSE);

  //Request memory access with low latency
  PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, TRUE);

  //Request/Poll the memory access
  Status |= PmicFgSram_PollFgSramAccess(PmicDeviceIndex, &SramAccessStatus);
  if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
  {
    //Requet read access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);

    /*Read Any Data Voltage and Temperature*/
    err_flg |= PmicFgSram_ReadData(PmicDeviceIndex, FG_SRAM_TEMP_REG_ADDR, &ReadData);

    //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_RedoBattId Just read : ReadData = 0x(%x) \n\r", ReadData));
  }
  else
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_PollFgSramAccess Failed : Status = (%d) SRAMAccessStatus = (%d) \n\r", Status, SramAccessStatus));
  }
  //Release the memory access
  Status |= PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  //Set Sram module internal state to available
  PmicFgSram_SetState(FG_SRAM_STATUS_AVAILABLE);

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_SetFgPreProfileLoad Waiting for 3 seconds \n\r"));

  //Wait for 3 seconds-hard wait required by FG HW
  gBS->Stall(3000000);

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_SetFgPreProfileLoad End \n\r"));

  return Status;
}


/**
PmicFgSram_ReStartFgToReflectProfile()

@brief
Redo Batt ID estimation Here for HW workaround of battery ID read
*/
EFI_STATUS PmicFgSram_ReStartFgToReflectProfile(UINT32 PmicDeviceIndex)
{
  EFI_STATUS Status = EFI_SUCCESS;

  pm_err_flag_type err_flag    = PM_ERR_FLAG__SUCCESS;
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReStartFgToReflectProfile  \n\r" ));

  /* Debug print */
  /*PmicFgSram_VerifyRegs(PmicDeviceIndex);*/

  /* Set First estimation INTR */
  err_flag |= pm_fg_soc_irq_enable(PmicDeviceIndex, PM_FG_SOC_FIRST_EST_DONE_INT, TRUE);
  err_flag |= pm_fg_soc_irq_set_trigger(PmicDeviceIndex, PM_FG_SOC_FIRST_EST_DONE_INT, PM_IRQ_TRIGGER_RISING_EDGE/*PM_IRQ_TRIGGER_FALLING_EDGE*/);

  //Set FG_SOC_PREVENT_OTP_PROFILE_RELOAD to 0
  err_flag |= pm_fg_soc_set_fg_soc_boot_mod(PmicDeviceIndex, PM_FG_SOC_PREVENT_OTP_PROFILE_RELOAD, TRUE);

  //When the memory access is granted set RERUN_FIRST_EST = 1 and GO = 1
  //Skip SoC first est since not required for re do batt id calculation
  err_flag |= pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RERUN_FIRST_EST, TRUE);

    //GO = TRUE, perform above requested sequence
  err_flag |= pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_GO, TRUE);

  /*FG is rebooting */
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe:PmicFgSram_ReStartFgToReflectProfile ** NOT **Waiting for 3 seconds while profile being reloaded by HW\n\r"));

  //Wait for Restart Done Status 
  Status |= PmicFgSram_FgAlgoFirstIterDoneChk(PmicDeviceIndex);

  /*Disbale INT*/
  err_flag |= pm_fg_soc_irq_enable(PmicDeviceIndex, PM_FG_SOC_FIRST_EST_DONE_INT, FALSE);
  err_flag |= pm_fg_soc_irq_clear(PmicDeviceIndex, PM_FG_SOC_FIRST_EST_DONE_INT);

  //Set Sram module internal state to available
  PmicFgSram_SetState(FG_SRAM_STATUS_AVAILABLE);

  //Set FG_SOC_PREVENT_OTP_PROFILE_RELOAD to 0
  err_flag |= pm_fg_soc_set_fg_soc_boot_mod(PmicDeviceIndex, PM_FG_SOC_PREVENT_OTP_PROFILE_RELOAD, FALSE);

  err_flag |= pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RERUN_FIRST_EST, FALSE);

  //GO = FALSE, perform above requested sequence
  err_flag |= pm_fg_soc_set_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_GO, FALSE);

  return Status|err_flag;
}



/**
PmicFgSram_Dump()

@brief
Dump Sram contents for given range
*/
EFI_STATUS PmicFgSram_Dump(UINT32 PmicDeviceIndex, UINT32 DumpSramStartAddr, UINT32 DumpSramEndAddr)
{
  EFI_STATUS       Status   = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  UINT32           ReadData = 0;
  UINT16           startSramAddress;
  UINT16           endSramAddress;
  UINT16           sramAddressIncrement = FG_SRAM_START_ADDRESS;
  BOOLEAN          SramAccessStatus     = FALSE;
  UINT32           RdDumpArray[BATTARY_PROFILE_MAX_SIZE];
  UINT32           ReadCount = 0;

  SetMem32(RdDumpArray, BATTARY_PROFILE_MAX_SIZE, 0x00);
  //Validate start and end address if they are not correct dump all SRAM 
  if ((DumpSramStartAddr < FG_SRAM_START_ADDRESS) || (DumpSramEndAddr > FG_SRAM_END_ADDRESS)){
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_Dump: Over Riding given SRAM address DumpSramStartAddr = (0x%x), DumpSramEndAddr = (0x%x)\n\r", DumpSramStartAddr, DumpSramEndAddr));
    startSramAddress = FG_SRAM_START_ADDRESS;
    endSramAddress   = FG_SRAM_END_ADDRESS;
  }
  else{
    //PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_Dump: SRAM dump Start = (0x%x) End = (0x%x)address \n\r", DumpSramStartAddr, DumpSramEndAddr));
    startSramAddress = DumpSramStartAddr;
    endSramAddress   = DumpSramEndAddr;
    sramAddressIncrement = DumpSramStartAddr;
  }
  //Request the memory access
  PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, FALSE);

  /*Poll for Sram memory access */
  Status = PmicFgSram_PollFgSramAccess(PmicDeviceIndex, &SramAccessStatus);

  if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
  {
    /*Enables RIF memory interface and the RIF Memory Access Mode.  1 */
    err_flg |=  pm_fg_memif_set_mem_intf_cfg(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, TRUE);
    //Request Burst access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_BURST, TRUE);

    //Requet read access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);

    //Start loop to read and print all Sram contents this could be a 900ms operation 

    //Updated Read address only once.. in burst access FG internally increment address 
    err_flg |=  pm_fg_memif_write_addr(PmicDeviceIndex, startSramAddress);

    do{
      //Read Data
      ReadData = 0;
      err_flg |=  pm_fg_memif_read_data_reg(PmicDeviceIndex, &ReadData);
      //Update Read Profile array 
      RdDumpArray[ReadCount++] = ReadData;

      sramAddressIncrement += FG_SRAM_ADDR_INCREMENT;

      //Write incremented sream register address 
      //err_flg |=  pm_fg_memif_write_addr(PmicDeviceIndex, sramAddressIncrement);

    }while((sramAddressIncrement <= endSramAddress )
           && (ReadCount < BATTARY_PROFILE_MAX_SIZE) 
           && (PM_ERR_FLAG__SUCCESS == err_flg));
  }
  else{
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_PollFgSramAccess Failed : Status = (%d) SramAccessStatus = (%d) \n\r", Status, SramAccessStatus));
  }

  // After read completion clear access bit request i.e. RIF_MEM_ACCESS_REQ = 0 ..
  // Release the memory access
  Status |= PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  if (TRUE == SramAccessStatus)
  {
    /* Dump only to File */
    //PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: SRAM Dump Start * \n\r"));
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: SRAM Dump Start * \n\r"));
    //Print the Dump now
    sramAddressIncrement = DumpSramStartAddr;
    for(ReadCount = 0; (ReadCount < (BATTARY_PROFILE_MAX_SIZE)) && (sramAddressIncrement <= endSramAddress); ReadCount++)
    {
      //Print proper address value pair below print would do 4 byte print
      /*
      PMIC_FILE_DEBUG(( EFI_D_ERROR, "%03X, %02X %02X %02X %02X \n\r", sramAddressIncrement, 
      (RdDumpArray[ReadCount] & 0xFF),  (RdDumpArray[ReadCount] >> 8)& 0xFF, (RdDumpArray[ReadCount] >> 16)& 0xFF, 
      (RdDumpArray[ReadCount] >> 24) & 0xFF));*/
     PMIC_DEBUG(( EFI_D_ERROR, "%03X, %02X %02X %02X %02X \n\r", sramAddressIncrement, 
      (RdDumpArray[ReadCount] & 0xFF),  (RdDumpArray[ReadCount] >> 8)& 0xFF, (RdDumpArray[ReadCount] >> 16)& 0xFF, 
      (RdDumpArray[ReadCount] >> 24) & 0xFF));
      //Increment for printing 
       sramAddressIncrement += FG_SRAM_ADDR_INCREMENT;
    }

    //PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: SRAM Dump End * \n\r"));
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: SRAM Dump End * \n\r"));
  }

  return (Status = err_flg | Status);
}


/**
PmicFgSram_ReadSingleAccess()

@brief
Reads 4 bytes (UINT32) in given Sram address, does asynchronous read, caller need to call this API untill Sram status is available
*/
EFI_STATUS PmicFgSram_ReadSingleAccess(UINT32 PmicDeviceIndex, FgBattParamCache* BattParamCache,
                                                 FgSramState* SramState)
{
  EFI_STATUS   Status   = EFI_SUCCESS;
  BOOLEAN      SramAccessStatus     = FALSE;
  FgSramState  FgSramSt = FG_SRAM_STATUS_INVALID;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  BOOLEAN  UsbIdChangeIntr = FALSE;
  BOOLEAN  UsbInSrcDetSts = FALSE;
  pm_smbchg_chgr_chgr_status_type chgr_sts = {0};
  pm_err_flag_type       errFlag = PM_ERR_FLAG__SUCCESS;
  BOOLEAN  DeltaSocIrqstatus = FALSE;
  FgBattProfileParams ProfileParams = {0};
  UINT32 Soc = 0;

  //Request the memory access if only Sram state is available
  Status |= PmicFgSram_GetState(&FgSramSt);
  if (FG_SRAM_STATUS_LOAD_PROFILE == FgSramSt){
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadSingleAccess FG_SRAM_STATUS_LOAD_PROFILE .. Skip requesting SRAM access : %d \n\r", Status));
    return EFI_NOT_READY;
  }
  else if(FG_SRAM_STATUS_AVAILABLE == FgSramSt)
  {
    //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadSingleAccess Requestng SRAM access : %d \n\r", Status));
    Status = PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, FALSE);
  }else {
    //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadSingleAccess Skip requesting SRAM access : %d \n\r", Status));
  }

  /*Check for SRAM memory access */
  Status |= PmicFgSram_IsFgMemAvailable(PmicDeviceIndex, &SramAccessStatus, SramState);
  if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
  {

    /*Read Battery Param Cache */
    Status |= PmicFgSram_ReadBattParamCache(PmicDeviceIndex, BattParamCache);

    /*Update FG batt Cache for Temperature */
    Status |= PmicFgBattParam_UpdateCache(BattParamCache);

    PmicFgBattProfileGetProfileParams(&ProfileParams);

    /* Disabling Rslow Charge Sequnce for SOC not incresing issue*/
    ProfileParams.RslowPp.RslowWorkaround = FALSE;

    if(TRUE == ProfileParams.RslowPp.RslowWorkaround) /* Profile revision is >= 12 */
    {
      /*Run Rslow WA if Profile rev is greater than 12 this flag is set during initilization*/
      /* Determine Delta Soc */
      errFlag |= pm_fg_soc_irq_status(PmicDeviceIndex, PM_FG_SOC_DELTA_SOC_INT, PM_IRQ_STATUS_LATCHED, &DeltaSocIrqstatus);
      if((PM_ERR_FLAG__SUCCESS == errFlag) && (TRUE == DeltaSocIrqstatus))
      {
        PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: ** Delta SOC interrrupt HIGH ** (%d) \n\r", DeltaSocIrqstatus));
        pm_fg_soc_irq_clear(PmicDeviceIndex, PM_FG_SOC_DELTA_SOC_INT);
        ProfileParams.RslowPp.RslowDeltaSoc = TRUE;
        /*Read Currnent Soc*/
        errFlag |=  pm_fg_soc_get_monotonic_soc(PmicDeviceIndex, (UINT32 *)&Soc);
        Status  = (PM_ERR_FLAG__SUCCESS == errFlag)? EFI_SUCCESS : EFI_DEVICE_ERROR;
        PMIC_DEBUG(( EFI_D_ERROR, "StateOfCharge: (%d) \n\r", Soc));

        /*Is Charging */
        errFlag = pm_smbchg_chgr_get_chgr_sts(PmicDeviceIndex, &chgr_sts);
        if(PM_SMBCHG_CHGR_PRE_CHARGING == chgr_sts.charging_type || PM_SMBCHG_CHGR_FAST_CHARGING == chgr_sts.charging_type ||
           PM_SMBCHG_CHGR_TAPER_CHARGING ==   chgr_sts.charging_type)
        {
          PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: Rslow Battery Charging (%d) \n\r", chgr_sts.charging_type));
          ProfileParams.RslowPp.RslowFixClrSeq = TRUE;
        }
        else
        {
          ProfileParams.RslowPp.RslowFixClrSeq = FALSE;
        }
      }

      err_flg |= pm_smbchg_usb_chgpth_irq_status(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_USBID_CHANGE_INTR, PM_IRQ_STATUS_LATCHED, &UsbIdChangeIntr);
      if(UsbIdChangeIntr == TRUE)
      {
        err_flg |= pm_smbchg_usb_chgpth_irq_status(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_USBIN_SRC_DET, PM_IRQ_STATUS_RT, &UsbInSrcDetSts);
        err_flg |= pm_smbchg_usb_chgpth_irq_clear(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_USBID_CHANGE_INTR);
        if((TRUE == UsbInSrcDetSts) && (TRUE == ProfileParams.RslowPp.RslowFixClrSeq))
        {
          PmicFgSram_RslowChargeFixSeq(PmicDeviceIndex, &ProfileParams, Soc);
        }
        else
        {
          PmicFgSram_RslowChargeClearSeq(PmicDeviceIndex, &ProfileParams);
        }
      }
      else if(TRUE == ProfileParams.RslowPp.RslowDeltaSoc ) /*Delta soc interrrupt*/
      {
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram Delta Soc  Executing Rslow Sequence : Soc = (%d) \n\r", Soc));
        if(TRUE == ProfileParams.RslowPp.RslowFixClrSeq ) /*If Charging*/
        {
          PmicFgSram_RslowChargeFixSeq(PmicDeviceIndex, &ProfileParams, Soc);
        }
        else
        {
          /* if charging and delta soc interrrupt and usb input is not removed */
          PmicFgSram_RslowChargeClearSeq(PmicDeviceIndex, &ProfileParams);
        }
        ProfileParams.RslowPp.RslowDeltaSoc = FALSE;
      }
      else
      {
        //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram No Delta Soc  skipping Rslow Charge Seq : Soc = (%d) \n\r", Soc));
      }

      /* Reset Only when Rslow is applied */
      ProfileParams.RslowPp.RslowDeltaSoc = FALSE;
    }

    //After read completion clear access bit request i.e. RIF_MEM_ACCESS_REQ = 0
    Status |= PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

    Status |= PmicFgSram_GetState(SramState);

    if(EFI_SUCCESS != Status)
     {
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadSingleAccess Status : %d \n\r", Status));
     }
  }
  else
  {
    //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadSingleAccess Check Back Again : Status = (%d) SramAccessStatus = (%d) \n\r", Status, SramAccessStatus));
  }

  return (Status |= err_flg);
}


/**
PmicFgSram_ProgBurstAccess()

@brief
Write Bytes (UINT32) Array in given Address pair
*/
EFI_STATUS PmicFgSram_ProgBurstAccess(UINT32 PmicDeviceIndex, FgSramAddrData * AddrDataPair, UINT32 AddrDataCount)
{
  EFI_STATUS       Status   = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  BOOLEAN          SramAccessStatus     = FALSE;
  UINT32           CurrCount = 0;

  //Validate input params
  if ((!AddrDataPair) || (0 == AddrDataCount) || (AddrDataCount >= FG_SRAM_MAX_SIZE))
    return EFI_INVALID_PARAMETER;

  //Request the memory access
  Status = PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, TRUE);

  /*Poll for Sram memory access */
  Status |= PmicFgSram_PollFgSramAccess(PmicDeviceIndex, &SramAccessStatus);

  if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
  {
    //Request Burst access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_BURST, TRUE);

    //Requet write access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, TRUE);

    //Write address 
    err_flg |=  pm_fg_memif_write_addr(PmicDeviceIndex, AddrDataPair[CurrCount].SramAddr);

    //write in loop
    do{
      //Read Data
      err_flg |=  pm_fg_memif_write_data(PmicDeviceIndex, AddrDataPair[CurrCount].SramData);
    }while(++CurrCount < AddrDataCount);

  }else{
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ProgBurstAccess Failed : Status = (%d) SramAccessStatus = (%d) \n\r", Status, SramAccessStatus));
    err_flg |= Status;
  }

  //After read completion clear access bit request i.e. RIF_MEM_ACCESS_REQ = 0
  PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  //make sure to not have low latency access 
  err_flg = pm_fg_memif_set_mem_intf_cfg(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_LOW_LATENCY_ACS_EN, FALSE);

  if(PM_ERR_FLAG__SUCCESS == err_flg)
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ProgBurstAccess err_flg Code: (%d) AddrDataCount = (0x%x)\n\r", err_flg, AddrDataCount));
  else{
    Status |= err_flg ;
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ProgBurstAccess err_flg Code: %d \n\r", err_flg));
  }

  return Status;
}


/**
PmicFgSram_ProgBurstAccessEx()

@brief
Write Bytes Based on given dataoffert and size Array in given Address pair
*/
EFI_STATUS PmicFgSram_ProgBurstAccessEx(UINT32 PmicDeviceIndex, FgSramAddrDataEx * AddrDataPairEx, UINT32 AddrDataCount)
{
  EFI_STATUS       Status   = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  BOOLEAN          SramAccessStatus     = FALSE;

  //Validate input params
  if ((!AddrDataPairEx) || (0 == AddrDataCount) || (AddrDataCount >= FG_SRAM_MAX_SIZE))
    return EFI_INVALID_PARAMETER;

  //Request the memory access
  Status = PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, FALSE);

  /*Poll for Sram memory access */
  Status |= PmicFgSram_PollFgSramAccess(PmicDeviceIndex, &SramAccessStatus);

  if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
  {
    Status |= PmicFgSram_WriteDataEx(PmicDeviceIndex, AddrDataPairEx, AddrDataCount);
  }
  else{
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ProgBurstAccess Failed : Status = (%d) SramAccessStatus = (%d) \n\r", Status, SramAccessStatus));
    err_flg |= Status;
  }

  //After read completion clear access bit request i.e. RIF_MEM_ACCESS_REQ = 0
  PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  if(PM_ERR_FLAG__SUCCESS == err_flg)
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ProgBurstAccess err_flg Code: (%d) AddrDataCount = (0x%x)\n\r", err_flg, AddrDataCount));
  else{
    Status |= err_flg ;
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ProgBurstAccess err_flg Code: %d \n\r", err_flg));
  }

  return Status;
}

EFI_STATUS PmicFgSram_WriteDataEx(UINT32 PmicDeviceIndex, FgSramAddrDataEx * AddrDataPairEx, UINT32 AddrDataCount)
{
  EFI_STATUS       Status   = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  UINT32           CurrCount = 0;
  UINT32           Mask      = 0;
  UINT32           ReadData  = 0;
  UINT32           WriteData = 0;

  //Validate input params
  if ((!AddrDataPairEx) || (0 == AddrDataCount) || (AddrDataCount >= FG_SRAM_MAX_SIZE))
    return EFI_INVALID_PARAMETER;

  //write in loop
  do{
    if ((AddrDataPairEx[CurrCount].DataSize   > FG_SRAM_RD_WR_BUS_WIDTH )|| 
        (AddrDataPairEx[CurrCount].DataOffset > FG_SRAM_RD_WR_OFFSET_WIDTH )){
      Status = EFI_INVALID_PARAMETER;
      break;
    }
    //Request Burst access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_BURST, FALSE);
  
    switch(AddrDataPairEx[CurrCount].DataSize)
    {
      case DATASIZE_FOUR:
      {
        if (AddrDataPairEx[CurrCount].DataOffset != OFFSET_ZERO){
          Status = EFI_INVALID_PARAMETER;
          break;
        }
        WriteData  = AddrDataPairEx[CurrCount].SramData;
      }
      break;
      case DATASIZE_THREE:
      {
        if (AddrDataPairEx[CurrCount].DataOffset > OFFSET_ONE){
          Status = EFI_INVALID_PARAMETER;
          break;
        }
        Mask = ~(0xFFFFFF << (AddrDataPairEx[CurrCount].DataOffset  * NUM_BITS_IN_BYTE /*8*/));
      }
      break;
  
      case DATASIZE_TWO:
      {
        if (AddrDataPairEx[CurrCount].DataOffset > OFFSET_TWO){
          Status = EFI_INVALID_PARAMETER;
          break;
        }
          Mask = ~(0xFFFF << (AddrDataPairEx[CurrCount].DataOffset  * NUM_BITS_IN_BYTE /*8*/));
      }
      break;
  
      case DATASIZE_ONE:
      {
        Mask = ~(0xFF << (AddrDataPairEx[CurrCount].DataOffset  * NUM_BITS_IN_BYTE /*8*/));
      }
      break;
  
      case DATASIZE_ZERO: //Skip current Entry
      Status = EFI_SUCCESS;
      continue;
  
      default:
      break;
    }
    if(EFI_SUCCESS == Status)
    {
      if (AddrDataPairEx[CurrCount].DataSize != DATASIZE_FOUR)
      {
        //Read Data from SRAM to avoid overwrite on other bytes 
        //Request Read Access
        err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);
        
        //Write address 
        err_flg |=  pm_fg_memif_write_addr(PmicDeviceIndex, AddrDataPairEx[CurrCount].SramAddr);
        
        //Read Data
        err_flg |=  pm_fg_memif_read_data_reg(PmicDeviceIndex, &ReadData);
      
        WriteData = ((ReadData & Mask) | ( AddrDataPairEx[CurrCount].SramData << (AddrDataPairEx[CurrCount].DataOffset * NUM_BITS_IN_BYTE)));
      }
      //Requet write access 
      err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, TRUE);
      
      //Write address 
      err_flg |=  pm_fg_memif_write_addr(PmicDeviceIndex, AddrDataPairEx[CurrCount].SramAddr);
      //Read Data
      err_flg |=  pm_fg_memif_write_data(PmicDeviceIndex, WriteData);
    }
  }while(++CurrCount < AddrDataCount);
  return Status;
}


/**
PmicFgSram_ProgSingleAccess()

@brief
Write Bytes (UINT8) Array in given Address pair
*/
EFI_STATUS PmicFgSram_ProgSingleAccess(UINT32 PmicDeviceIndex, FgSramAddrDataOffset * AddrDataPair,UINT32 AddrDataCount)
{
  EFI_STATUS       Status    = EFI_SUCCESS;
  pm_err_flag_type err_flg   = PM_ERR_FLAG__SUCCESS;
  BOOLEAN          SramAccessStatus     = FALSE;
  UINT8            Offset    = 0;
  UINT32           CurrCount = 0;

  //Validate input params
  if ((!AddrDataPair) || (0 == AddrDataCount) || (AddrDataCount >= FG_SRAM_MAX_SIZE))
    return EFI_INVALID_PARAMETER;
  
  //Request the memory access
  Status = PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, FALSE);

  /*Poll for Sram memory access */
  Status |= PmicFgSram_PollFgSramAccess(PmicDeviceIndex, &SramAccessStatus);

  if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
  {

    //Request single access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_BURST, FALSE);

    //write in loop
    do{
      //Know and validate the offset otherwise break the loop and return invalid 
      if(3 >= AddrDataPair[CurrCount].DataOffset){
        Offset = AddrDataPair[CurrCount].DataOffset;
      }else{
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ProgSingleAccess InValid Offset: Offset passed = (%d) \n\r", AddrDataPair[CurrCount].DataOffset));
        Status = EFI_INVALID_PARAMETER;
        err_flg = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
        break;
      }
      Status |= PmicFgSram_WriteOffsetData(PmicDeviceIndex, AddrDataPair[CurrCount].SramAddr,
                                                 AddrDataPair[CurrCount].SramData, Offset);
    }while(++CurrCount < AddrDataCount && EFI_SUCCESS == Status);
  }
  else{
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ProgSingleAccess Failed : Status = (%d) SramAccessStatus = (%d) \n\r", Status, SramAccessStatus));
    err_flg |= Status;
  }

  //After read completion clear access bit request i.e. RIF_MEM_ACCESS_REQ = 0
  PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  if(PM_ERR_FLAG__SUCCESS == err_flg || EFI_SUCCESS != Status)
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ProgSingleAccess err_flg Code: (%d) Status = (%d) AddrDataCount = (0x%x)\n\r", err_flg, Status, AddrDataCount));
  else{
    Status |= err_flg ;
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ProgSingleAccess err_flg Code: %d \n\r", err_flg));
  }

  return Status;
}


/*==========================================================================
                        LOCAL  API DEFINITION
===========================================================================*/
/**
PmicFgSram_SetState()

@brief
Set Sram State
*/
EFI_STATUS PmicFgSram_SetState(FgSramState FgSramSt)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  if ( FG_SRAM_STATUS_INVALID == SramState)
  {
    Status = EFI_DEVICE_ERROR;
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_SetState: Error SramState = (%d) \n\r", SramState));
  }
  else{
    SramState = FgSramSt;
    //PmicFgSram_PrintState(FgSramSt);
    //PmicFgSram_PrintState(SramState);
  }

  return Status;
}

/**
PmicFgSram_PrintState()

@brief
Debug Print Sram State
*/
void PmicFgSram_PrintState(FgSramState FgSramSt)
{
  switch(FgSramSt){
    case FG_SRAM_STATUS_INIT:
      PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: FG_SRAM_STATUS_INIT SramState: = (%d) \n\r", SramState));
    break;
    case FG_SRAM_STATUS_LOAD_PROFILE:
      PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: FG_SRAM_STATUS_LOAD_PROFILE SramState: = (%d) \n\r", SramState));
    break;
    case FG_SRAM_STATUS_IN_USE:
      PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: FG_SRAM_STATUS_IN_USE SramState: = (%d) \n\r", SramState));
    break;
    case FG_SRAM_STATUS_POLLING:
      PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: FG_SRAM_STATUS_POLLING SramState: = (%d) \n\r", SramState));
    break;
    case FG_SRAM_STATUS_AVAILABLE:
      PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: FG_SRAM_STATUS_AVAILABLE SramState: = (%d) \n\r", SramState));
    break;
    case FG_SRAM_STATUS_INVALID:
      PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: FG_SRAM_STATUS_INVALID SramState: = (%d) \n\r", SramState));
    break;
    default:
      PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: FG_SRAM_STATUS_DEFAULT SramState: = (%d) \n\r", SramState));
      break;
  }
  return;
}

/**
PmicFgSram_WriteData()

@brief
Write 4 bytes (UINT32) in given Sram address
*/
pm_err_flag_type PmicFgSram_WriteData(UINT32 PmicDeviceIndex, UINT16 WriteAddress, UINT32 fg_memif_data)
{
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;

  //Write  Address 
  err_flg |=  pm_fg_memif_write_addr(PmicDeviceIndex, WriteAddress);
  
  //Write Data
  err_flg |=  pm_fg_memif_write_data(PmicDeviceIndex, fg_memif_data);

  return err_flg;

}

/**
PmicFgSram_WriteOffsetData()

@brief
Write 1 Offset byte from given Sram Address
*/
EFI_STATUS PmicFgSram_WriteOffsetData(UINT32 PmicDeviceIndex, UINT16 WriteAddress, 
                                               UINT8 Data, UINT8 Offset)
{
  EFI_STATUS       Status    = EFI_SUCCESS;
  pm_err_flag_type err_flg   = PM_ERR_FLAG__SUCCESS;
  UINT32           ReadData  = 0;
  UINT32           WriteData = 0;
  UINT32           Mask      = 0;

  //Request Read Access
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);

  err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, WriteAddress, &ReadData);

  PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_WriteOffsetData: ReadData = (0x%x) \n\r", ReadData));
  
  //Generate Mask 
  Mask = ~(0xFF << (Offset  * NUM_BITS_IN_BYTE /*8*/));
  PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_WriteOffsetData: Mask = (0x%x) = (0x%x) Offset = (0x%x)\n\r", Mask, (ReadData & Mask), Offset));
  //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_WriteOffsetData: = (0x%x) = (0x%x) \n\r",  (Data << ( Offset) * NUM_BITS_IN_BYTE)), (((Offset) * NUM_BITS_IN_BYTE)));

  WriteData = (ReadData & Mask ) | (Data << (Offset  * NUM_BITS_IN_BYTE ));

  PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_WriteOffsetData WriteData = (0x%x) \n\r", WriteData));

  //Request Write Access
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, TRUE);

  PmicFgSram_WriteData(PmicDeviceIndex, WriteAddress, WriteData);

  //Debug Read for what we have written is correct 
  //Request Read Access
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);

  ReadData = 0;
  err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, WriteAddress, &ReadData);
  PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_WriteOffsetData: ReadData = (0x%x) \n\r", ReadData));

  return (Status |= err_flg);

}


/**
PmicFgSram_ReadData()

@brief
Reads 4 bytes (UINT32) in given Sram address
*/
pm_err_flag_type PmicFgSram_ReadData(UINT32 PmicDeviceIndex, UINT16 ReadAddress, UINT32 *fg_memif_data)
{
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;

  //Write address 
  err_flg |=  pm_fg_memif_write_addr(PmicDeviceIndex, ReadAddress);
  
  //Read Data
  err_flg |=  pm_fg_memif_read_data_reg(PmicDeviceIndex, fg_memif_data);

  return err_flg;
}

/**
PmicFgSram_ReleaseFgSramAccess()

@brief
Release Sram access, Clears memory access bit 
*/
EFI_STATUS PmicFgSram_ReleaseFgSramAccess
(
  UINT32  PmicDeviceIndex
)
{
  EFI_STATUS       Status   = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  boolean          CycleStrteched = FALSE;

  //After write completion clear access bit request i.e. RIF_MEM_ACCESS_REQ = 0
  err_flg |= pm_fg_memif_set_mem_intf_cfg(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, FALSE);

  //Set Sram module internal state 
  Status  = PmicFgSram_SetState(FG_SRAM_STATUS_AVAILABLE);  

  //Read Cycle Strech Bit and print if cycle is stretch 
  err_flg = pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_CYCLE_STRETCHED, &CycleStrteched);
  if(PM_ERR_FLAG__SUCCESS == err_flg)
  {
    if (TRUE == CycleStrteched){
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Cycle Stretched in Last SRAM transaction \n\r"));
      err_flg = pm_fg_soc_set_fg_soc_sts_clr(PmicDeviceIndex);
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Cycle Stretched Cleared (%d)\n\r", err_flg));
    }
  }else{
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Error in reading Cycle Stretched status \n\r"));
  }

  return (Status |= err_flg );
}

/**
PmicFgSram_RequestFgSramAccess()

@brief
Request Sram access, Sets memory access bit 
*/
EFI_STATUS PmicFgSram_RequestFgSramAccess
(
  UINT32  PmicDeviceIndex, pm_fg_memif_mem_intf_cfg  mem_intf_cfg, BOOLEAN Fast_Memory_Access
)
{
  EFI_STATUS       Status   = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;

  //1. Requet access to FG i.e. RIF_MEM_ACCESS_REQ = 1

  if (TRUE == Fast_Memory_Access){
    err_flg = pm_fg_memif_set_mem_intf_cfg(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_LOW_LATENCY_ACS_EN, TRUE);
    PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_RequestFgSramAccess: Requesting LOW_LATENCY_ACS_EN \n\r"));
  }else{
    //make sure to not have low latency access 
    err_flg = pm_fg_memif_set_mem_intf_cfg(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_LOW_LATENCY_ACS_EN, FALSE);
  }
  /*Enables RIF memory interface and the RIF Memory Access Mode.  1 */
  err_flg |= pm_fg_memif_set_mem_intf_cfg(PmicDeviceIndex, mem_intf_cfg/*PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ */, TRUE);

  //Set Sram module internal state 
  Status = PmicFgSram_SetState(FG_SRAM_STATUS_IN_USE);
  

  return (Status |= err_flg);
}

/**
PmicFgSram_PollFgSramAccess()

@brief
Poll Sram memory access untill timeout or returns when memory access is permitted 
*/
EFI_STATUS PmicFgSram_PollFgSramAccess
(
  UINT32  PmicDeviceIndex, boolean * SramAccessStatus
)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  boolean Mem_Available_status = FALSE;
  pm_err_flag_type err_flg = PM_ERR_FLAG__SUCCESS;
  uint16 Ttl_spent_time_in_polling = 0;

  *SramAccessStatus = FALSE;

  //Set Sram module internal state 
  PmicFgSram_SetState(FG_SRAM_STATUS_POLLING);

  //Poll FG_MEM_AVAIL_RT_STS = 1 
  do{
      // mem available best time is 150 so first time this call is supposed to fail and successive call may return mem available status as TRUE
      err_flg |=  pm_fg_memif_irq_status(PmicDeviceIndex, PM_FG_MEMIF_FG_MEM_AVAIL_RT_STS, PM_IRQ_STATUS_RT, &Mem_Available_status);
      if ( TRUE == Mem_Available_status )
      {
        *SramAccessStatus = TRUE;
        //PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_PollFgSramAccess PM_FG_MEMIF_FG_MEM_AVAIL_RT_STS : = (%d) \n\r", Mem_Available_status));
        break;
      }//check for error condition as we do not want to loop forever
      else if (Ttl_spent_time_in_polling >= FG_MEM_AVAILABLE_RT_STS_POLL_MAX_TIME)
      {
        *SramAccessStatus = FALSE;
        Status = EFI_DEVICE_ERROR;
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_PollFgSramAccess TimeOut : Ttl_spent_time_in_polling = (%d) \n\r", Ttl_spent_time_in_polling));
        break;
      }

      /*wait for 150 ms before querying mem available status again */
      gBS->Stall(FG_MEM_AVAILABLE_RT_STS_POLL_MIN_TIME * 1000);

      Ttl_spent_time_in_polling += FG_MEM_AVAILABLE_RT_STS_POLL_MIN_TIME;
  }while(TRUE);

  //Set Sram module internal state 
  if (FALSE == *SramAccessStatus){
    //Clear memory access bit request i.e. RIF_MEM_ACCESS_REQ = 0
    PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

    //might need to read cycle streach bit and clear it here
    PmicFgSram_SetState(FG_SRAM_STATUS_AVAILABLE); //when time out for polling access request 
  }
  else 
    PmicFgSram_SetState(FG_SRAM_STATUS_IN_USE);

  return (Status |= err_flg);
}



/**
PmicFgSram_IsFgMemAvailable()

@brief
Single Check on Sram memory access permission, Asynchronous memory access polling
*/
EFI_STATUS PmicFgSram_IsFgMemAvailable(
  UINT32  PmicDeviceIndex, boolean * SramAccessStatus, FgSramState *SramState
)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  boolean Mem_Available_status = FALSE;
  pm_err_flag_type err_flg = PM_ERR_FLAG__SUCCESS;

  *SramAccessStatus = FALSE;

  // mem available best time is 150 so first time this call is supposed to fail and successive call may return mem available status as TRUE
  err_flg |=  pm_fg_memif_irq_status(PmicDeviceIndex, PM_FG_MEMIF_FG_MEM_AVAIL_RT_STS, PM_IRQ_STATUS_RT, &Mem_Available_status);
  
  if ( (PM_ERR_FLAG__SUCCESS == err_flg) && (TRUE == Mem_Available_status ))
  {
    *SramAccessStatus = Mem_Available_status;
    //PMIC_UART_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_IsFgemAvailable PM_FG_MEMIF_FG_MEM_AVAIL_RT_STS : = (%d) \n\r", Mem_Available_status));
  }else
  {
    //Set Sram module internal state to Polling
    PmicFgSram_SetState(FG_SRAM_STATUS_POLLING);
    //pass polling status
    PmicFgSram_GetState(SramState);
    //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_IsFgemAvailable PM_FG_MEMIF_FG_MEM_AVAIL_RT_STS : = (%d) \n\r", Mem_Available_status));
  }

  return (Status |= err_flg);
}

/**
PmicFgSram_WriteProfileToSram()

@brief
Writes Profile Array to Sram
*/
EFI_STATUS PmicFgSram_WriteProfileToSram
(
  UINT32 PmicDeviceIndex,
  FgBattAddrData profileArray[BATTARY_PROFILE_MAX_SIZE]
)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  UINT16           CurrSramProfileAddress = FG_SRAM_PROFILE_START_ADDR;
  UINT16           EndSramProfileAddress   = FG_SRAM_PROFILE_END_ADDR;
  UINT16 WriteCounter = 0;

  //Set Sram module internal state 
  PmicFgSram_SetState(FG_SRAM_STATUS_LOAD_PROFILE);

  /*Enables RIF memory interface and the RIF Memory Access Mode.  1 */

  //Request Burst access 
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_BURST, TRUE);

  //Requet Write access 
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, TRUE);

  //Start loop to read and print all SRAM contents this could be a 900ms operation 

  //Updated Read address only once.. in burst access FG internally increment address 
  err_flg |=  pm_fg_memif_write_addr(PmicDeviceIndex, CurrSramProfileAddress); //check with system team

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Loading Profile to FG SRAM \n\r"));

  do{

    //Write Data
    err_flg |=  pm_fg_memif_write_data(PmicDeviceIndex, profileArray[WriteCounter].Data);

    //Print proper address value pair below print would do 4 byte print
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: ByteAddr: 0x%x = 0x%x \n\r", CurrSramProfileAddress , profileArray[WriteCounter].Data));

    //Upkeeping Address
    CurrSramProfileAddress += FG_SRAM_ADDR_INCREMENT;

  }while((++WriteCounter <= (FG_SRAM_PROFILE_RANGE/FG_SRAM_ADDR_INCREMENT)) && (CurrSramProfileAddress <= EndSramProfileAddress));

  Status = PmicFgSram_ValidateProfileWrite(PmicDeviceIndex, profileArray);
  //Set Sram module internal state 
  PmicFgSram_SetState(FG_SRAM_STATUS_IN_USE);

  return (Status |= err_flg);

}


/**
PmicFgSram_ValidateProfileWrite()

@brief
Validates Profile Array flashed to Sram
*/
EFI_STATUS PmicFgSram_ValidateProfileWrite(UINT32 PmicDeviceIndex,
                           FgBattAddrData profileArray[BATTARY_PROFILE_MAX_SIZE])
{
  EFI_STATUS       Status  = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  UINT16           CurrSramProfileAddress = FG_SRAM_PROFILE_START_ADDR;
  UINT16           EndSramProfileAddress   = FG_SRAM_PROFILE_END_ADDR;
  UINT16           WriteCounter = 0;
  UINT32           ChkCounter = 0;
  UINT32           fg_memif_data = 0x00;
  BOOLEAN          ProfileValidWrite = FALSE;
  UINT32           RdprofileArray[BATTARY_PROFILE_MAX_SIZE/FG_SRAM_ADDR_INCREMENT];

  //memset Read Array to zero
   SetMem(RdprofileArray, sizeof(UINT32) *(BATTARY_PROFILE_MAX_SIZE/FG_SRAM_ADDR_INCREMENT), 0x00);

  //Request Burst access
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_BURST, TRUE);

  //Requet read access
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);

  //Start loop to read all battery profile contents We just write into

  //Updated Read address only once.. in burst access FG internally increment address 
  err_flg |=  pm_fg_memif_write_addr(PmicDeviceIndex, CurrSramProfileAddress);

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Validating Profile Writes \n\r"));

  do{

      err_flg |=  pm_fg_memif_read_data_reg(PmicDeviceIndex, &fg_memif_data);
      RdprofileArray[WriteCounter] = fg_memif_data;


      //Print Proper Address value pair below print would do 4 byte print
      PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: ByteAddr (0x%x) Read = (0x%x) \n\r", CurrSramProfileAddress, RdprofileArray[WriteCounter]));

      //Upkeeping Address
      CurrSramProfileAddress += FG_SRAM_ADDR_INCREMENT;

  }while((++WriteCounter <= (FG_SRAM_PROFILE_RANGE/FG_SRAM_ADDR_INCREMENT)) && (CurrSramProfileAddress <= EndSramProfileAddress));

  for(ChkCounter = 0;
      (((ChkCounter <= (FG_SRAM_PROFILE_RANGE/FG_SRAM_ADDR_INCREMENT) ) && (ChkCounter < (BATTARY_PROFILE_MAX_SIZE/FG_SRAM_ADDR_INCREMENT) )));
      ChkCounter++){
    if (0 == (RdprofileArray[ChkCounter] ^  profileArray[ChkCounter].Data) )
      ProfileValidWrite = TRUE;
    else{
      ProfileValidWrite = FALSE;
      break;
    }
  }
  if (TRUE == ProfileValidWrite){
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ValidateProfileWrite: Success count = (%d) \n\r", ChkCounter));
  }
  else{
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ValidateProfileWrite: Failed at count = (%d)\n\r", ChkCounter));
  }

  return (Status |= err_flg);
}


/**
PmicFgSram_FgAlgoRestartDoneChk

@brief 
Wait for Restart done once profile is loaded or if Fg is restarted
**/
EFI_STATUS PmicFgSram_FgAlgoRestartDoneChk(UINT32 PmicDeviceIndex, pm_fg_soc_alg_sts RestartChkFalg)
{

  EFI_STATUS        Status  = EFI_SUCCESS;
  pm_err_flag_type  err_flg = PM_ERR_FLAG__SUCCESS;
  BOOLEAN           bWDog = FALSE, bRstProgres = FALSE, bBootDone = FALSE, bRstDone = FALSE, bFstItDone = FALSE, bRestartDone = FALSE;
  pm_fg_soc_alg_sts soc_alg_sts = RestartChkFalg;
  UINT32            Ttl_spent_time_in_polling = 0;

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoRestartDoneChk:  Start RestartChkFalg: (%d) \n\r", RestartChkFalg));

  /* Debug Query to know all Restart Status */
  err_flg = pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_RESTART_IN_PROGRESS, &bRstProgres);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_BOOT_DONE, &bBootDone);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_RESTART_DONE, &bRstDone);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_FIRST_ITER_DONE, &bFstItDone);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_WDOG_EXP, &bWDog);

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoRestartDoneChk:  bRstProgres= (%d), bBootDone= (%d), bRstDone= (%d), bFstItDone = (%d) bWDog = (%d) bRestartDone = (%d)\n\r", 
                  bRstProgres, bBootDone, bRstDone, bFstItDone, bWDog, bRestartDone));
  do{

    err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, soc_alg_sts, &bRestartDone);
    if (Ttl_spent_time_in_polling >= FG_FIRST_ITER_DONE_INTR_POLL_MAX_TIME)
    {
      Status = EFI_DEVICE_ERROR;
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoRestartDoneChk TimeOut : Ttl_spent_time_in_polling = (%d) \n\r", Ttl_spent_time_in_polling));
      break;
    }
    if(FALSE == bRestartDone)
    {
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoRestartDoneChk:  waiting for 10ms: bRestartDone = (%d) \n\r", bRestartDone));
      gBS->Stall(10000);
    }

    Ttl_spent_time_in_polling +=FG_FIRST_ITER_DONE_INTR_POLL_MIN_TIME;

  }while( FALSE == bRestartDone);

  /* Debug Query to know all Restart Status */
  err_flg = pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_RESTART_IN_PROGRESS, &bRstProgres);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_BOOT_DONE, &bBootDone);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_RESTART_DONE, &bRstDone);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_FIRST_ITER_DONE, &bFstItDone);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_WDOG_EXP, &bWDog);

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoRestartDoneChk:  bRstProgres= (%d), bBootDone= (%d), bRstDone= (%d), bFstItDone = (%d) bWDog = (%d) bRestartDone = (%d)\n\r", 
                  bRstProgres, bBootDone, bRstDone, bFstItDone, bWDog, bRestartDone));

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoRestartDoneChk:  End \n\r"));

  return (Status | err_flg);
}


/**
PmicFgSram_FgAlgoFirstIterDoneChk

@brief 
Wait for Restart done once profile is loaded or if Fg SOC first SOC is completed 
**/
EFI_STATUS PmicFgSram_FgAlgoFirstIterDoneChk(UINT32 PmicDeviceIndex)
{

  EFI_STATUS       Status  = EFI_SUCCESS;
  pm_err_flag_type err_flg = PM_ERR_FLAG__SUCCESS;
  BOOLEAN FirstIterSocIrqstatus = FALSE;
  UINT32 Ttl_spent_time_in_polling = 0;

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoFirstIterDoneChk:  Start \n\r"));

  do{
    err_flg |=  pm_fg_soc_irq_status(PmicDeviceIndex, PM_FG_SOC_FIRST_EST_DONE_INT, PM_IRQ_STATUS_LATCHED, &FirstIterSocIrqstatus);
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoFirstIterDoneChk:FirstIterSocIrqstatus = (%d) \n\r", FirstIterSocIrqstatus));
    if (Ttl_spent_time_in_polling >= FG_FIRST_ITER_DONE_INTR_POLL_MAX_TIME)
    {
      Status = EFI_DEVICE_ERROR;
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoFirstIterDoneChk TimeOut : Ttl_spent_time_in_polling = (%d) \n\r", Ttl_spent_time_in_polling));
      break;
    }

    Ttl_spent_time_in_polling +=FG_FIRST_ITER_DONE_INTR_POLL_MIN_TIME;

    if(FALSE == FirstIterSocIrqstatus)
    {
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoFirstIterDoneChk:  waiting for 100ms: \n\r"));
      gBS->Stall(100000);
    }
  }while( FALSE == FirstIterSocIrqstatus);

  if(TRUE == FirstIterSocIrqstatus)
  {
    err_flg |= pm_fg_soc_irq_clear(PmicDeviceIndex, PM_FG_SOC_FIRST_EST_DONE_INT);
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoFirstIterDoneChk:  Clearing PM_FG_SOC_FIRST_EST_DONE_INT for Falling Edge \n\r"));
  }

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_FgAlgoFirstIterDoneChk:  End \n\r"));

  return (Status | err_flg);
}


/**
PmicFgSramValidateCheckSum

@brief 
Verifies Battery Profile Provided Checksum with battery profile data 
**/
EFI_STATUS PmicFgSramValidateCheckSum(FgBattProfile *Profile, BOOLEAN *bValid)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 chksum = 0, count = 0, byteCount = 0, ProfCnt = 0;
  int    cross = 0;
  long   lngDiv = 32768;

  if (NULL == bValid)
    return EFI_INVALID_PARAMETER;

  do
  {
    chksum = 0;
    //Validate check sum here for 128 bytes 32 * 4 
    for(count = 0; count < 32; count++)
    {
      PMIC_DEBUG(( EFI_D_INFO, "PmicDxe: ReadData = (0x%x) \r\n", Profile->Pd[ProfCnt].addrData[count].Data));
      for(byteCount = 0; byteCount < 4; byteCount++)
      {
        if (chksum/lngDiv)
          cross = 1;
        else
          cross = 0;
        chksum = ((((chksum * 2) & 0xFFFF) + cross) ^ ((Profile->Pd[ProfCnt].addrData[count].Data >> (byteCount * NUM_BITS_IN_BYTE)) & 0xFF));
        //PMIC_DEBUG(( EFI_D_INFO, "PmicDxe: sourceCount: (0x%x) = (0x%x) chksum = (0x%x)\r\n",(Profile->addrData[count].Data >> (byteCount * NUM_BITS_IN_BYTE)) & 0xFF,  chksum));
      }
    }

    if (chksum == Profile->Pd[ProfCnt].Pp.CheckSum)
    {
      *bValid = TRUE;
    }
    else
    {
      *bValid = FALSE;
      Status  = EFI_LOAD_ERROR;
      PMIC_DEBUG(( EFI_D_WARN, "PmicDxe: PmicFgSramValidateCheckSum:ProfCnt = (%d) chksum = (0x%x) is incorrect \r\n",Profile->Pd[ProfCnt].Pp.CheckSum, ProfCnt));
      break;
    }
  }while((++ProfCnt < Profile->ProfileCount ) && (ProfCnt < MAX_NUM_OF_BATT_PROFILE));

  return Status;

}

/**
PmicFgSram_RslowChargeFixeSeq()

@brief
Rslow workaround - Charger Fix Sequnce
*/
EFI_STATUS PmicFgSram_RslowChargeFixSeq
(
  UINT32 PmicDeviceIndex,
  FgBattProfileParams *Pp,
  UINT32 StateOfCharge
)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  UINT16 RslowLowSoc_ChAddr = 0x538, RslowLowSoc_ThAddr = 0x52C, RslowToCharge_Addr = 0x514, RslowCompSocC1C2_Addr = 0x528;
  UINT32 RslowLowSoc_ChData = 0, RslowLowSoc_ThData = 0, RslowToCharger_Data = 0, RslowCompSocC1C2_Data = 0;

  PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: Charge Fix Sequnce Soc = (%d), RsToRslowChargeFix = (%d), Rslow_Charge_Fix flag = (%d) back (%d)\n\r",
              StateOfCharge, Pp->RslowPp.RsProfileConfigs.RslowCompChFixTh, Rslow_Charge_Fix , (Pp->RslowPp.RsProfileConfigs.RslowCompChFixTh * 100 / 0xFF)));

  if((StateOfCharge > (Pp->RslowPp.RsProfileConfigs.RslowCompChFixTh * 100 / 0xFF)) && (FALSE == Rslow_Charge_Fix ))
  {
    PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: Executing Charge Fix Sequnce \n\r"));
    /*Enables RIF memory interface and the RIF Memory Access Mode.  1 */
    //Request single access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_BURST, FALSE);

    //Requet Read Access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);

    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowLowSoc_ChAddr, &RslowLowSoc_ChData);
    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowLowSoc_ThAddr, &RslowLowSoc_ThData);
    /* Debug Read */
    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowToCharge_Addr, &RslowToCharger_Data);
    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowCompSocC1C2_Addr, &RslowCompSocC1C2_Data);

    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadBeforeWriteRslow: 0x%x = 0x%x, 0x%x = 0x%x, 0x%x = 0x%x, 0x%x = 0x%x \n\r",
                RslowLowSoc_ChAddr, RslowLowSoc_ChData, RslowLowSoc_ThAddr, RslowLowSoc_ThData, RslowToCharge_Addr, RslowToCharger_Data, RslowCompSocC1C2_Addr,RslowCompSocC1C2_Data));

    /* Set Respective bits as per Rslow workaround Charge Fix Sequnce */
    RslowLowSoc_ChData = (RslowLowSoc_ChData | 0xC0000 ) & (0xFFCFFFFF);/* Set 2[2], 2[3] and ReSet 2[4], 2[5]*/

    RslowLowSoc_ThData = (RslowLowSoc_ThData | 0xFF); /* Set Rslow Soc threshold to 100%*/

    RslowCompSocC1C2_Data = ((Pp->RslowPp.RsProfileConfigs.RslowCompSocC2ch << 16) & 0xFFFF0000) 
                            | (Pp->RslowPp.RsProfileConfigs.RslowCompSocC1ch & 0xFFFF) ;/* Merge two profile params for single write */

    RslowToCharger_Data = (RslowToCharger_Data & 0x0000FFFF) | ((Pp->RslowPp.RsProfileConfigs.RsToRslowChargeFix & 0xFFFF ) << 16);

    /*Start Updating SRAM contents */

   //Requet Write Access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, TRUE);

    err_flg |=  PmicFgSram_WriteData(PmicDeviceIndex, RslowLowSoc_ChAddr, RslowLowSoc_ChData);

    err_flg |=  PmicFgSram_WriteData(PmicDeviceIndex, RslowLowSoc_ThAddr, RslowLowSoc_ThData);

    err_flg |=  PmicFgSram_WriteData(PmicDeviceIndex, RslowCompSocC1C2_Addr, RslowCompSocC1C2_Data);

    /* Set rs_to_slow_charge_fix from profile */
    err_flg |=  PmicFgSram_WriteData(PmicDeviceIndex, RslowToCharge_Addr, RslowToCharger_Data);

    /* Debug Read */
    //Requet Read Access 
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);
    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowLowSoc_ChAddr, &RslowLowSoc_ChData);
    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowLowSoc_ThAddr, &RslowLowSoc_ThData);
    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowToCharge_Addr, &RslowToCharger_Data);
    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowCompSocC1C2_Addr, &RslowCompSocC1C2_Data);

    //Print proper address value pair below print would do 4 byte print
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadAfterWriteRslow: 0x%x = 0x%x, 0x%x = 0x%x, 0x%x = 0x%x, 0x%x = 0x%x \n\r",
                RslowLowSoc_ChAddr, RslowLowSoc_ChData, RslowLowSoc_ThAddr, RslowLowSoc_ThData, RslowToCharge_Addr, RslowToCharger_Data, RslowCompSocC1C2_Addr,RslowCompSocC1C2_Data));

    Rslow_Charge_Fix = TRUE;
  }
  else if((StateOfCharge < Pp->RslowPp.RsProfileConfigs.RsToRslowChargeFix ) && (TRUE == Rslow_Charge_Fix ))
  {
    /*Execute Clear Sequnce */
    PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: Battery SOC < than rslow_comp_th_fix_th SOC = (%d), RsToRslowChargeFix = 0x(%x) Run Clear \n\r",
                  StateOfCharge, Pp->RslowPp.RsProfileConfigs.RsToRslowChargeFix));
    PmicFgSram_RslowChargeClearSeq(PmicDeviceIndex, Pp);
  }
  else
  {
    PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: Not valid use case \n\r"));
  }
  return (Status |= err_flg);

}


/**
PmicFgSram_RslowChargeClearSeq()

@brief
Rslow workaround - Charger Clera Sequnce
*/
EFI_STATUS PmicFgSram_RslowChargeClearSeq
(
  UINT32 PmicDeviceIndex,
  FgBattProfileParams *Pp
)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  UINT16 RslowLowSoc_ChAddr = 0x538, RslowLowSoc_ThAddr = 0x52C, RslowToCharge_Addr = 0x514, RslowCompSocC1C2_Addr = 0x528;
  UINT32 RslowLowSoc_ChData = 0, RslowLowSoc_ThData = 0, RslowToCharger_Data = 0, RslowCompSocC1C2_Data = 0;

  PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: Executing Rslow Charge Clear Sequnce \n\r"));
  /*Enables RIF memory interface and the RIF Memory Access Mode.  1 */
  //Request single access 
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_BURST, FALSE);

  //Requet Read Access 
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);

  err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowLowSoc_ChAddr, &RslowLowSoc_ChData);
  err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowLowSoc_ThAddr, &RslowLowSoc_ThData);
  /* Debug Read */
  err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowToCharge_Addr, &RslowToCharger_Data);
  err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowCompSocC1C2_Addr, &RslowCompSocC1C2_Data);

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadBeforeWriteRslow: 0x%x = 0x%x, 0x%x = 0x%x, 0x%x = 0x%x, 0x%x = 0x%x \n\r",
              RslowLowSoc_ChAddr, RslowLowSoc_ChData, RslowLowSoc_ThAddr, RslowLowSoc_ThData, RslowToCharge_Addr, RslowToCharger_Data, RslowCompSocC1C2_Addr,RslowCompSocC1C2_Data));

  /* Set Respective bits as per Rslow workaround Charge Clear  Sequnce */
  /* Set 0x538 2[2], 2[3] to FALSE and Set 2[4], 2[5] from Profile Value */
  RslowLowSoc_ChData = (RslowLowSoc_ChData & 0xFFF3FFFF ) 
                       | ((Pp->RslowPp.RsProfileSectionParams.RslowProfileFlag.RslowLowSocDisCharge << 20) 
                       | (Pp->RslowPp.RsProfileSectionParams.RslowProfileFlag.RslowLowSocDisChargeC2En << 21)) ;

  RslowLowSoc_ThData = (RslowLowSoc_ThData & 0xFFFFFF00 ) 
                        | (0xFF & Pp->RslowPp.RsProfileSectionParams.RslowCompChFixTh) ; /* Set Rslow Soc threshold to profile Value*/

  RslowCompSocC1C2_Data = (Pp->RslowPp.RsProfileSectionParams.RslowCompSocC2ch << 16 ) 
                          | (Pp->RslowPp.RsProfileSectionParams.RslowCompSocC1ch & 0xFFFF);/* Merge two profile params for single write */
  PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: RsToRslowChargeFix (0x%x)\n\r", Pp->RslowPp.RsProfileSectionParams.RsToRslowChargeFix));

  RslowToCharger_Data = (RslowToCharger_Data & 0x0000FFFF) | (Pp->RslowPp.RsProfileSectionParams.RsToRslowChargeFix  << 16);

  /*Start Updating SRAM contents */

 //Requet Write Access 
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, TRUE);

  err_flg |=  PmicFgSram_WriteData(PmicDeviceIndex, RslowLowSoc_ChAddr, RslowLowSoc_ChData);

  err_flg |=  PmicFgSram_WriteData(PmicDeviceIndex, RslowLowSoc_ThAddr, RslowLowSoc_ThData);

  err_flg |=  PmicFgSram_WriteData(PmicDeviceIndex, RslowCompSocC1C2_Addr, RslowCompSocC1C2_Data);

  /* Set rs_to_slow_charge_fix from profile */
  err_flg |=  PmicFgSram_WriteData(PmicDeviceIndex, RslowToCharge_Addr, RslowToCharger_Data);

  /* Debug Read */
  //Requet Read Access 
  err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);
  err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowLowSoc_ChAddr, &RslowLowSoc_ChData);
  err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowLowSoc_ThAddr, &RslowLowSoc_ThData);
  err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowToCharge_Addr, &RslowToCharger_Data);
  err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, RslowCompSocC1C2_Addr, &RslowCompSocC1C2_Data);

  //Print proper address value pair below print would do 4 byte print
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_ReadAfterWriteRslow: 0x%x = 0x%x, 0x%x = 0x%x, 0x%x = 0x%x, 0x%x = 0x%x \n\r",
              RslowLowSoc_ChAddr, RslowLowSoc_ChData, RslowLowSoc_ThAddr, RslowLowSoc_ThData, RslowToCharge_Addr, RslowToCharger_Data, RslowCompSocC1C2_Addr,RslowCompSocC1C2_Data));

  Rslow_Charge_Fix = FALSE;

  return (Status |= err_flg);

}

/**
PmicFgSram_RunRslowSramRequest()

@brief
Run Rslow work around with FG sram request access 
*/
EFI_STATUS PmicFgSram_RunRslowSramRequest(UINT32 PmicDeviceIndex, FgBattProfileParams *Pp, UINT32 StateOfCharge)
{
  EFI_STATUS       Status   = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  BOOLEAN          SramAccessStatus     = FALSE;
  UINT32           ProfilRev = 0;

  //Validate input params
  if (!Pp )
    return EFI_INVALID_PARAMETER;

  //Request the memory access
  Status = PmicFgSram_RequestFgSramAccess(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, FALSE);

  /*Poll for Sram memory access */
  Status |= PmicFgSram_PollFgSramAccess(PmicDeviceIndex, &SramAccessStatus);

  if (EFI_SUCCESS == Status && TRUE == SramAccessStatus)
  {
    if ( TRUE == Pp->RslowPp.RslowFixClrSeq )
    {
      Status |= PmicFgSram_RslowChargeFixSeq(PmicDeviceIndex, Pp, StateOfCharge);
    }
    else
    {
      Status |= PmicFgSram_RslowChargeClearSeq(PmicDeviceIndex, Pp);
    }

    /*Debug Read for Profile Revision */
    err_flg |=  pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);
    err_flg |=  PmicFgSram_ReadData(PmicDeviceIndex, FG_SRAM_PROFILE_CHK_ADDR, &ProfilRev);
  }
  else{
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_RunRslowSramRequest Failed : Status = (%d) SramAccessStatus = (%d) \n\r", Status, SramAccessStatus));
    err_flg |= Status;
  }

  //After read completion clear access bit request i.e. RIF_MEM_ACCESS_REQ = 0
  PmicFgSram_ReleaseFgSramAccess(PmicDeviceIndex);

  if(PM_ERR_FLAG__SUCCESS == err_flg)
    PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_RunRslowSramRequest err_flg Code: (%d) \n\r", err_flg));
  else{
    Status |= err_flg ;
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_RunRslowSramRequest err_flg Code: %d \n\r", err_flg));
  }

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_RunRslowSramRequest Profile Rev : (0x%x)\n\r", ProfilRev));

  return Status;
}

/**
PmicFgSram_RslowSaveProfileParams()

@brief
Rslow saving profile Params to FG SRAM for HLOS 
*/
EFI_STATUS PmicFgSram_RslowSaveProfileParams
(
  UINT32 PmicDeviceIndex,
  FgBattProfileParams *Pp
)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  pm_err_flag_type err_flg  = PM_ERR_FLAG__SUCCESS;
  UINT16 RslowLowSoc_Th_CompAddr = FG_SRAM_RSLOWLOWSOC_TH_COMPADDR /*0th Byte for soc th*/, RslowCompSocC1C2_Addr = FG_SRAM_RSLOWCOMPSOCC1C2_ADDR;
  UINT32 RslowLowSoc_Th_CompData = 0, RslowCompSocC1C2_Data = 0;

  //PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: PmicFgSram_RslowSaveProfileParams \n\r"));

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: RslowSaveProfileParams: 0x%x, 0x%x, 0x%x, 0x%x \n\r",
              Pp->RslowPp.RsProfileConfigs.RslowCompChFixTh, Pp->RslowPp.RsProfileConfigs.RsToRslowChargeFix, Pp->RslowPp.RsProfileConfigs.RslowCompSocC1ch, Pp->RslowPp.RsProfileConfigs.RslowCompSocC2ch));

  /*save Updating SRAM contents */

 //Requet Write Access 
  err_flg |= pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, TRUE);

  /* Set rs_to_slow_charge_fix from profile */
  RslowLowSoc_Th_CompData = (Pp->RslowPp.RsProfileConfigs.RsToRslowChargeFix << 16 ) | Pp->RslowPp.RsProfileConfigs.RslowCompChFixTh;

  err_flg |= PmicFgSram_WriteData(PmicDeviceIndex, RslowLowSoc_Th_CompAddr, RslowLowSoc_Th_CompData );

  RslowCompSocC1C2_Data  = ((Pp->RslowPp.RsProfileConfigs.RslowCompSocC2ch << 16) & 0xFFFF0000) | (Pp->RslowPp.RsProfileConfigs.RslowCompSocC1ch & 0xFFFF);/* Merge two profile params for single write */

  err_flg |= PmicFgSram_WriteData(PmicDeviceIndex, RslowCompSocC1C2_Addr, RslowCompSocC1C2_Data);

  /* Debug Read */
  RslowLowSoc_Th_CompData = 0; RslowCompSocC1C2_Data = 0;
  //Requet Read Access 
  err_flg |= pm_fg_memif_set_mem_intf_ctl(PmicDeviceIndex, PM_FG_MEMIF_MEM_INTF_CTL_WR_EN, FALSE);
  err_flg |= PmicFgSram_ReadData(PmicDeviceIndex, RslowLowSoc_Th_CompAddr, &RslowLowSoc_Th_CompData);
  err_flg |= PmicFgSram_ReadData(PmicDeviceIndex, RslowCompSocC1C2_Addr, &RslowCompSocC1C2_Data);

  //Print proper address value pair below print would do 4 byte print
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_RslowSaveProfileParams: 0x%x = 0x%x, 0x%x = 0x%x \n\r",
              RslowLowSoc_Th_CompAddr, RslowLowSoc_Th_CompData, RslowCompSocC1C2_Addr,RslowCompSocC1C2_Data));

  return (Status |= err_flg);

}


/**
PmicFgSram_GetHALFEncode

@brief 
This function takes a fixed point value which is a floating poing value * 1000, e.g.
input value of 125 means 0.125 and 134045 means 134.045 and convert it to a 16 bit half
float encoded integer value.

**/
EFI_STATUS PmicFgSram_GetHALFEncode(INT32 value, UINT16* pEncodedValue)
{
  //in the fixedPointDecimalTextBox it is the floating point value * 1000
  INT32 integerValue = 0;
  INT32 exponent = 0;
  INT32 temp_value = 0;
  INT32 mantissa = 0;
  INT32 i = 0;
  EFI_STATUS status = EFI_SUCCESS;
  INT32 signBit = 0;

  if (pEncodedValue == NULL)
  {
    status = EFI_INVALID_PARAMETER;
    goto ExitFunction;
  }
  if (value < 0)
  {
    signBit = 1;
    value = (-value);
  }
  integerValue = value / 1000;
  if (integerValue > 0)
  {
    //5 bit for exponent, so bigest is 31.
    //if integerValue > 0, then n >= 0, so exponent start from 15
    for (i = 15; i <= 31; i++)
    {
      INT32 n = i - 15;
      temp_value = 1 << n;
      if (temp_value >= integerValue)
      {
        exponent = i;
        break;
      }
    }
    if (temp_value > integerValue)
    {
      //find the exponent that will make the 
      //2^(exp-15) one step smaller than intergerValue.
      exponent = i - 1;
    }
    mantissa = value - (1 << (exponent - 15)) * 1000;
    if (mantissa < 0)
    {
      status = EFI_INVALID_PARAMETER;
      goto ExitFunction;
    }
    //findout mantissa hex value
    //mantissa = remainder * 2^(10 - (expn - 15));
    if (exponent <= 25)
    {
      mantissa = mantissa * (1 << (25 - exponent));
    }
    else
    {
      mantissa = mantissa * 1000 / (1 << (exponent - 25));
      mantissa /= 1000;
    }
    mantissa = (mantissa + 1000 / 2) / 1000;
    
  }
  else
  {
    //it is less or =1
    for (i = 0; i < 15; i++)
    {
      temp_value = 1000 / (1 << i);
      if (temp_value <= value)
      {
        //we found the exponent and it is actually -exponent
        exponent = i;
        break;
      }
    }
    //findout the mantisa since exponent should be a negative value, so
    //since value is all less than 1000 here so no worry of overflowing.
    mantissa = value * 1000 - (1000 * 1000 / (1 << exponent));
    mantissa = mantissa  * (1 << (10 + exponent));

    mantissa /= 1000000;
    exponent = 15 - exponent;
  }

  *pEncodedValue = (UINT16)((exponent << 11) | (signBit << 10) | mantissa);
ExitFunction:
  return status;
}

#if 0
EFI_STATUS PmicFgSram_VerifyRegs(UINT32 PmicDeviceIndex)
{
  //EFI_STATUS status = EFI_SUCCESS;
  pm_fg_batt_sys_sts batt_sys_sts = {0};
  pm_err_flag_type err_flg = PM_ERR_FLAG__SUCCESS;
  //pm_fg_batt_latch batt_latch = PM_FG_BATT_REMOVED_LATCH_CLEAR_TRUE;
  BOOLEAN bEnable = FALSE;
  BOOLEAN          bWDog = FALSE, bRstProgres = FALSE, bBootDone = FALSE, bRstDone = FALSE, bFstItDone = FALSE, bRestartDone = FALSE;

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgSram_VerifyRegs: pm_fg_batt_rem_latched_sts = (%d), pm_fg_batt_profile_sts = (%d), pm_fg_batt_rem_latched_sts = (%d) pm_fg_batt_fg_sys_sts = (%d)\n\r",
              batt_sys_sts.pm_fg_batt_rem_latched_sts, batt_sys_sts.pm_fg_batt_profile_sts, batt_sys_sts.pm_fg_batt_rem_latched_sts, batt_sys_sts.pm_fg_batt_fg_sys_sts));

  if(TRUE == batt_sys_sts.pm_fg_batt_profile_sts){
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: BATT_PROFILE_CHANGED \n\r"));
  }else{
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: BATT_PROFILE_ORIGINAL \n\r"));
  }

  if(TRUE == batt_sys_sts.pm_fg_batt_rem_latched_sts)
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: ** BATT WAS REMOVED Clearing latched status **\n\r"));
    err_flg |= pm_fg_batt_clr_batt_latched(PmicDeviceIndex, PM_FG_BATT_REMOVED_LATCH_CLEAR_TRUE);
    gBS->Stall(1000000);
    err_flg = pm_fg_batt_get_sys_batt_sts(PmicDeviceIndex, &batt_sys_sts);
    if(TRUE == batt_sys_sts.pm_fg_batt_rem_latched_sts)
    {
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: BATT WAS REMOVED bit set after Clearing latched status \n\r"));
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: After clearing latch: pm_fg_batt_rem_latched_sts = (%d), pm_fg_batt_profile_sts = (%d), pm_fg_batt_rem_latched_sts = (%d) \n\r",
                  batt_sys_sts.pm_fg_batt_rem_latched_sts, batt_sys_sts.pm_fg_batt_profile_sts, batt_sys_sts.pm_fg_batt_rem_latched_sts));
    }
  }
  else
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: BATT WAS NOT ** REMOVED \n\r"));
  }
  err_flg |= pm_fg_batt_irq_status(PmicDeviceIndex, PM_FG_BATT_BATT_MISSING_INT_SET_TYPE, PM_IRQ_STATUS_RT, &bEnable);
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PM_FG_BATT_BATT_MISSING_INT_SET_TYPE Status (%d)\n\r", bEnable));


  pm_fg_soc_get_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_GO, &bEnable);
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PM_FG_SOC_FG_SOC_RESTART_GO (%d)\n\r", bEnable));
  pm_fg_soc_get_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RELOAD_SYS, &bEnable);
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PM_FG_SOC_FG_SOC_RESTART_RELOAD_SYS (%d)\n\r", bEnable));
  pm_fg_soc_get_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RELOAD_PROFILE, &bEnable);
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PM_FG_SOC_FG_SOC_RESTART_RELOAD_PROFILE (%d)\n\r", bEnable));
  pm_fg_soc_get_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_RERUN_FIRST_EST, &bEnable);
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PM_FG_SOC_FG_SOC_RESTART_RERUN_FIRST_EST (%d)\n\r", bEnable));
  pm_fg_soc_get_fg_soc_restart(PmicDeviceIndex, PM_FG_SOC_FG_SOC_RESTART_REDO_BATID_DURING_FIRST_EST, &bEnable);
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PM_FG_SOC_FG_SOC_RESTART_REDO_BATID_DURING_FIRST_EST (%d)\n\r", bEnable));

  /* Debug Query to know all Restart Status */
  err_flg = pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_RESTART_IN_PROGRESS, &bRstProgres);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_BOOT_DONE, &bBootDone);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_RESTART_DONE, &bRstDone);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_FIRST_ITER_DONE, &bFstItDone);
  err_flg |= pm_fg_soc_get_fg_alg_sts(PmicDeviceIndex, PM_FG_SOC_ALG_WDOG_EXP, &bWDog);

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: bRstProgres= (%d), bBootDone= (%d), bRstDone= (%d), bFstItDone = (%d) bWDog = (%d) bRestartDone = (%d)\n\r", 
                  bRstProgres, bBootDone, bRstDone, bFstItDone, bWDog, bRestartDone));
  return EFI_SUCCESS;

}
#endif

