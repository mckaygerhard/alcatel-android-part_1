/*! @file PmicFgBattProfile.c
 *
 *  PMIC-Fuel Gauge Module Related Definations
 *  This file contains functions and variable declarations to support
 *  the PMIC Fuel Gauge module - battery profile APIs.
 *
 *  Copyright (c) 2014 - 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
 *  Qualcomm Technologies Proprietary and Confidential.
 */

 /*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/28/15   va      Adding Fuel Gauge initial register settting suggested by PSV team
06/03/15   al      If battery profile doesn't match then compare range  
05/12/15   mr      Enabled Charger functionality for 8952 (CR-846387)
05/14/15   va      Rslow Clear Seq Threshol value fix
05/14/15   va      Profile Integrity Fix
05/12/15   al      Remove overwriting profile battery id with SRAM battery ID
04/08/15   va      Fixed Convert API for Therm cofficients
04/15/15   al      Making data type changes to make it compatible with 64bit
04/16/15   mic     Use updated DppProtocol
04/08/15   va      Rslow WA Changes
03/13/15   al      Adding changes for optimization
02/20/15   al      Adding reading battery id for case battery profile not parsed
01/26/15   sm      Writing to TERM_CURRENT and TMP_FRAC_SHIFT SRAM registers in PmicFgBattHandleProfile
01/13/15   va      Correcting FG SRAM system termination address
01/13/15   va      Logging for ChargerApp states
01/05/15   va      Added Multiple Battery Profile and Profile Parser Status API, Init FG Termination current
12/26/14   al      Adding header file and CHI support
11/17/14   al      Replacing UEFI banned function
10/28/14   va      Added valid profile check before parsing battery profile
09/25/14   va      Update for New Battery Profile Format
09/23/14   al      Moving API declaration from source file to header file
09/15/14   va      Enable File Logging
09/04/14   al      KW Errors Fix
08/27/14   va      KW Errors Fix.
05/16/14   va      New file.
--------   ---     ----------------------------------------------------------
01/19/15   al         Adding API to return battery type
01/13/15   va         Correcting FG SRAM system termination address
01/13/15   va         Logging for ChargerApp states
01/05/15   va         Added Multiple Battery Profile and Profile Parser Status API, Init FG Termination current
12/26/14   al         Adding header file and CHI support
11/17/14   al         Replacing UEFI banned function
10/28/14   va         Added valid profile check before parsing battery profile
09/25/14   va         Update for New Battery Profile Format
09/23/14   al         Moving API declaration from source file to header file
09/15/14   va         Enable File Logging
09/04/14   al         KW Errors Fix
08/27/14   va         KW Errors Fix.
05/16/14   va         New file.
===========================================================================*/

 /*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
/**
  EFI interfaces
 */
#include <Library/QcomBaseLib.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/QcomLib.h>
#include "PmicFileLog.h"
#include <Protocol/EFIVariableServices.h>

/**
  PMIC Lib interfaces
 */
#include "pm_uefi.h"
#include "pm_fg_soc.h"

/**
  DPP Dependencies
 */
#include <Protocol/EFIDpp.h>

/**
  PMIC Platform interface
 */
#include <Protocol/EFIPlatformInfo.h>
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/EFIPmicFg.h>
#include "string.h"
#include "ParserLib.h"
#include "DDIPlatformInfo.h"
#include <Guid/FileSystemInfo.h>
#include <Guid/FileInfo.h>
#include <Guid/Gpt.h>
#include <Library/QcomLib.h>

/**
  FG Dependencies
 */

#include "PmicFgBattProfile.h"
#include <Library/MemoryAllocationLib.h>
#include "PmicFgBattParameter.h"
#include "pm_fg_batt.h"
#include "PmicFgSram.h"
#include "pm_smbchg_usb_chgpth.h"

/*===========================================================================
                            MACRO
===========================================================================*/
#define NULL_CHAR              '\0'
#define FILE_INFO_SIZE (SIZE_OF_EFI_FILE_INFO + 256)

/* Batter Profile Strings */
#define BATT_PROFILE_LEN_IN_4_BYTE      0x1F //128/4 bytes
#define BATT_PROFILE_DATA_4_OFFSET      0x03
#define BATT_PROFILE_DATA_TKN_IN_LINE   4

#define TAG_BATTERY_RPOFILE             "BATTERY_PROFILE"
#define TAG_GUI_VERSION                 "GUIVersion"
#define TAG_BATTERY_ID                  "Batt_id"
#define TAG_PROFILE_CHKSUM              "Checksum"
#define TAG_FLOAT_VOLTAGE               "Float_Voltage"
#define TAG_NOMINAL_BATTERY_CAPACITY    "Nom_Batt_Capacity"
#define TAG_THERMISTOR_B                "Therm_B"
#define TAG_BATTERY_NAME                "Battery_name"
#define TAG_RSLOW_COMP_ALONG_SOC_C1_CH  "rslow_comp_along_soc_C1_ch" // 2788
#define TAG_RSLOW_COMP_ALONG_SOC_C2_CH  "rslow_comp_along_soc_C2_ch" // 3246
#define TAG_RS_TO_RSLOW_CHARGE_FIX      "rs_to_rslow_charge_fix" // 1083
#define TAG_RSLOW_COMP_CH_FIX_TH        "rslow_comp_ch_fix_th" // 600
#define TAG_THERM_C1_COEFF              "Therm_C1_Coeff" // 85EC
#define TAG_THERM_C2_COEFF              "Therm_C2_Coeff" // 4A76
#define TAG_THERM_C3_COEFF              "Therm_C3_Coeff" // 35FC

#define EFIESP_BATTERY_PROVISION_PATH  L"\\BATTERY.PROVISION"
#define PLAT_BATTERY_PROVISION_PATH    L"\\PROVISION\\BATTERY.PROVISION"

#define FG_SRAM_TERM_CURRENT_ENCODING 152588 // 5 div 2 to powr 15 in nA
#define FG_SRAM_TERM_ENCODING_SCALE_NA 1000000 // in nA
#define FG_SRAM_RSLOW_SOC_DISCHARGE_ADRR_CNT 30
#define FG_SRAM_RSLOW_SOC_C1_C2_ADRR_CNT  26
#define FG_SRAM_RSLOW_TH_ADRR_CNT         27
#define FG_SRAM_RSLOW_CHARGE_FIX_ADRR_CNT 21
#define FG_SRAM_ROUNDING_COSNT     5

STATIC HandleInfo BattProfileHandleInfoList[2];
STATIC PartiSelectFilter BattProfileHandleFilter;

STATIC UINT32 DesiredSignature[] = {0x8183040A, 0x45928B44, 0XD9C6F792, 0XF76005EE};

STATIC  FgBattProfile FgProfile;
STATIC  FgBattProfileParams ProfileCachedParams;
STATIC  BOOLEAN profileValid = FALSE;
STATIC  UINT32 ProfileAddrCount  = 0;
STATIC  EFI_PM_FG_BATT_TYPE FgBattType = EFI_PM_FG_BATT_TYPE_UNKNOWN;
STATIC BOOLEAN BatteryDetected = FALSE;

#define BATTERY_TYPE                 L"FgBatteryType"

/*==========================================================================
                        MACRO APIs
===========================================================================*/


/*==========================================================================
                        LOCAL VARIABLES
===========================================================================*/

//Root Partition for PLAT and EFIESP Partition
extern EFI_GUID gEfiEmmcGppPartition1Guid;
//Partition Type for PLAT
extern EFI_GUID gEfiPlatPartitionTypeGuid;

extern EFI_GUID gEfiEmmcUserPartitionGuid;
extern EFI_GUID gEfiBDPPartitionGuid;
extern EFI_GUID gEfiFileInfoGuid;

STATIC EFI_PLATFORMINFO_PLATFORM_TYPE  PlatformType = (EFI_PLATFORMINFO_PLATFORM_TYPE)0;
STATIC EFI_PM_FG_CFGDATA_TYPE BattCfgDefaults; //how do we take care for battery defaults or the parameter to be overriden by text config file which is currently supported

STATIC EFI_PM_FG_PROFILE_PARSER_STATUS BatteryType_Parserstatus = PM_PARSER_STATUS_PARSE_ERROR;

/*==========================================================================
                        LOCAL API PROTOTYPES
===========================================================================*/
static EFI_PM_FG_PROFILE_PARSER_STATUS PmicBattProfileParseDataBuffer
(
  IN  char *pBufferData,
  IN  UINTN DataBufferSize
);

EFI_PM_FG_PROFILE_PARSER_STATUS PmicFgBattGetBattProfile(VOID);

EFI_STATUS PmicFgBattReadBatteryProfile(UINT32 PmicDeviceIndex,
                                                  EFI_PM_FG_PROFILE_PARSER_STATUS *Parserstatus);

//Read from EMMC EFIESP or PLAT Parition
EFI_STATUS PmicFgBattProfileEmmcReadFile(char **pData, UINTN *DataSize);

//Read from DPP Parition
EFI_STATUS PmicFgBattProfileDppReadFile(char **pData, UINTN *DataSize);

//Read From Given Partition
EFI_STATUS PmicFgBattProfileRead(IN  CHAR16    *Path,
                                  IN  EFI_GUID  *RootDeviceType,
                                  IN  EFI_GUID  *PartitionType,
                                  IN  BOOLEAN   SelectNonRemovable,
                                  IN  CHAR8     *VolumeName,
                                  OUT CHAR8     **pData,
                                  OUT UINTN     *DataSize );

VOID PmicFgBattProfileTokenPtrArrCallBack(UINT8* Section, UINTN SectionOffset, UINT8* TokenPtrArray[], UINTN MaxTokens);
EFI_STATUS PmicFgBattProfile_ProcessTokenArray(UINT8* Section, UINTN SectionOffset, UINT8* TokenPtrArray[], UINTN MaxTokens);

INT32 PmicFgBattProfileAsciiToInt( CHAR8 *Str);
UINT32 PmicFgBattProfileAsciiStrToHex (CHAR8* Str, UINT32 StrSize);

EFI_STATUS PmicFgBatteryType(EFI_PM_FG_CFGDATA_TYPE *BatteryDefaults, FgBattId *SramBatt_Id, FgBattProfile *FgProfile);
static CHAR8 * AsciiStrNDup(CONST CHAR8 *Source, UINTN  Length);


/*==========================================================================
                        GLOBAL API DEFINITION
===========================================================================*/

/**
PmicFgBattProfileInit

@brief
Initializes parameters for Battery Profile
**/
EFI_STATUS PmicFgBattProfileInit(VOID)
{
  UINT32 Count = 0;
  FgProfile.ProfileCount    = 0;
  FgProfile.SelectedProfile = 0;
  //Set Elements to zero
  for(Count = 0; Count < MAX_NUM_OF_BATT_PROFILE; Count++)
  {
    FgProfile.Pd[Count].addrData = NULL;
    SetMem(&FgProfile.Pd[Count].Pp, sizeof(FgBattProfileParams), 0x00);
  }
  /* Initialize Profile params cache */
  SetMem(&ProfileCachedParams, sizeof(FgBattProfileParams), 0x00);

  return EFI_SUCCESS;
}


EFI_STATUS PmicFgGetBatteryType(EFI_PM_FG_BATT_TYPE* BattType)
{
  EFI_STATUS Status = EFI_SUCCESS;
  FgBattId   SramBatt_Id     = {0};
  EFI_PM_FG_CFGDATA_TYPE BatteryDefaults;
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_fg_batt_sys_sts batt_sys_sts ={0};
  UINTN BattTypeTemp = 0;

  if(NULL == BattType)
  {
    Status = EFI_DEVICE_ERROR;
  }
  else if (TRUE == BatteryDetected)
  {
    *BattType = FgBattType;
  }
  else
  {
     /*read battery profile integrity bit to determine reload of profile needed or not */
    err_flag = pm_fg_batt_get_sys_batt_sts(PM_DEVICE_1, &batt_sys_sts);
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgGetBatteryType: Integrity Status (%d) \n\r", batt_sys_sts.pm_fg_batt_profile_sts));
    if ( PM_FG_BATT_PROFILE_CHANGED == batt_sys_sts.pm_fg_batt_profile_sts)
      {
        PmicUefiVariable(BATTERY_TYPE, &BattTypeTemp,  UEFI_VARIABLE_OP_GET);
        *BattType = (EFI_PM_FG_BATT_TYPE)BattTypeTemp;
      }
      else
      {
        /*read battery type*/
        PmicFgSram_ReadBattIdEx(PM_DEVICE_1, &SramBatt_Id);
        Status = PmicFgBattParam_GetDefaultCfg(&BatteryDefaults);
        PmicFgBatteryType(&BatteryDefaults, &SramBatt_Id, NULL);
        *BattType = FgBattType;
      }
    }

  return Status|err_flag;
}


EFI_STATUS PmicFgBatteryType(EFI_PM_FG_CFGDATA_TYPE *BatteryDefaults, FgBattId *SramBatt_Id, FgBattProfile *FgProfile)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 ProfileCount;
  BOOLEAN BatIdInToleranceLimit = FALSE;
  UINTN TempBattType = 0;

  /*only if battery profile was found*/
  if (NULL!= FgProfile)
  {
    Status = PmicFgSram_PickProfile(SramBatt_Id, FgProfile, &ProfileCount);
    BatIdInToleranceLimit = PmicFgSram_BatteryIdInToleranceLimit(SramBatt_Id, FgProfile->Pd[ProfileCount].Pp.BatteryId);
  }
  else if ((SramBatt_Id->Bid >= BatteryDefaults->BattIdCfgData.AnalogBattIdMin) && (SramBatt_Id->Bid <= BatteryDefaults->BattIdCfgData.AnalogBattIdMax))
  {
    FgBattType = EFI_PM_FG_BATT_TYPE_NORMAL;
    BatteryDetected = TRUE;
    return Status;
  }

  if (TRUE == BatIdInToleranceLimit)
  {
    //SMART or REAL BATTERY: KNOWN battery
    FgBattType = (SramBatt_Id->DetId == BID_DETECT_BATTERY_SMART) ? EFI_PM_FG_BATT_TYPE_SMART : EFI_PM_FG_BATT_TYPE_NORMAL;
  }
  else if ((SramBatt_Id->Bid >= BatteryDefaults->BattIdCfgData.AnalogBattIdMin) && (SramBatt_Id->Bid <= BatteryDefaults->BattIdCfgData.AnalogBattIdMax))
  { /*if battery profile was not found then we still need to make sure if it is analog battery, which is regular battery and smart battery*/
     /*have range from 15k to 450K for these target*/
    FgBattType = EFI_PM_FG_BATT_TYPE_NORMAL;
  }
  else if ((SramBatt_Id->Bid >= BatteryDefaults->BattIdCfgData.DebugBoardBattIdMin) && (SramBatt_Id->Bid <= BatteryDefaults->BattIdCfgData.DebugBoardBattIdMax))
  {
    FgBattType = EFI_PM_FG_BATT_TYPE_DEBUG_BOARD;
  }
  else if ((SramBatt_Id->Bid >= BatteryDefaults->BattIdCfgData.DummyBattIdMin1 && SramBatt_Id->Bid <= BatteryDefaults->BattIdCfgData.DummyBattIdMax1)
           || (SramBatt_Id->Bid >= BatteryDefaults->BattIdCfgData.DummyBattIdMin2 && SramBatt_Id->Bid <= BatteryDefaults->BattIdCfgData.DummyBattIdMax2))
  {
    //dummy battery
    FgBattType = EFI_PM_FG_BATT_BATT_EMULATOR;
  }
  else
  {
    //unknown battery
    FgBattType = EFI_PM_FG_BATT_TYPE_UNKNOWN;
  }

  BatteryDetected = TRUE;

  TempBattType = (UINTN)FgBattType;

  /*storing battery type as UEFI variable*/
  PmicUefiVariable(BATTERY_TYPE, &TempBattType, UEFI_VARIABLE_OP_SET);

  return Status;
}


#define SRAM_TMP_TRAC_SHIFT 0x12A2

/**
PmicFgBattHandleProfile

@brief
Read battery profile from EFIESP/PLAT parition and Set Battery Profile respectivly
**/
EFI_STATUS PmicFgBattHandleProfile(UINT32 PmicDeviceIndex)
{
  EFI_STATUS             Status          = EFI_SUCCESS;
  FgBattId               SramBatt_Id     = {0};
  EFI_PM_FG_CFGDATA_TYPE BatteryDefaults;
  UINT32                 AddrDataCount   = 0;
  FgSramAddrDataEx       AddrDataPairEx[6]; // = 0;
  EFI_PM_FG_PROFILE_PARSER_STATUS Parserstatus = PM_PARSER_STATUS_GOOD_MATCH;
  UINT32                 Count           = 0;
  UINT32                 SysTermCurrent  = 0;
  UINT32                 TermCurrent     = 0;
  UINT16                 KiCoeffPred     = 0;
  UINTN                  SelectedProfile = 0;

  //Get Battery Defaults
  Status |= PmicFgBattParam_GetDefaultCfg(&BatteryDefaults);

  TermCurrent = BatteryDefaults.ChrgingTermCurrent;
  SysTermCurrent = (TermCurrent + 50) ;
  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgBattHandleProfile: ChrgingTermCurrent = (0x%x)\n\r", BatteryDefaults.ChrgingTermCurrent));

  //Convert term current to current encoding
  TermCurrent = (BatteryDefaults.ChrgingTermCurrent * FG_SRAM_TERM_ENCODING_SCALE_NA)/ FG_SRAM_TERM_CURRENT_ENCODING;
  SysTermCurrent = (SysTermCurrent * FG_SRAM_TERM_ENCODING_SCALE_NA)/ FG_SRAM_TERM_CURRENT_ENCODING;

  /* Get 2's compliment */
  TermCurrent = (~TermCurrent) + 1;
  SysTermCurrent = (~SysTermCurrent) + 1;

  //Force sign i.e. 15th bit as charger current encoding is negative
  TermCurrent = TermCurrent | (0x8000);
  SysTermCurrent = SysTermCurrent | (0x8000);

  //Get FG Sram initial Setting to be updated after profile reload (JEITA/ other initial setting could be updated here)
  AddrDataPairEx[0].SramAddr = FG_SRAM_SYS_TERM_CURRENT_REG_ADDR; AddrDataPairEx[0].SramData = SysTermCurrent;  AddrDataPairEx[0].DataSize = 2;  AddrDataPairEx[0].DataOffset = 2;
  AddrDataPairEx[1].SramAddr = FG_SRAM_TERM_CURRENT_REG_ADDR; AddrDataPairEx[1].SramData = TermCurrent;  AddrDataPairEx[1].DataSize = 2;  AddrDataPairEx[1].DataOffset = 2;
  AddrDataPairEx[2].SramAddr = FG_SRAM_TMP_FRAC_SHIFT_REG_ADDR; AddrDataPairEx[2].SramData = SRAM_TMP_TRAC_SHIFT;  AddrDataPairEx[2].DataSize = 2;  AddrDataPairEx[2].DataOffset = 2;
  AddrDataPairEx[3].SramAddr = FG_SRAM_KI_COEFF_PRED_FULL_REG_ADDR; AddrDataPairEx[3].DataSize = 2;  AddrDataPairEx[3].DataOffset = 2;
  Status |= PmicFgSram_GetHALFEncode(FG_SRAM_KI_COEFF_PRED_FULL_VALUE, &KiCoeffPred);
  AddrDataPairEx[3].SramData = (UINT32 )KiCoeffPred;

  AddrDataPairEx[4].SramAddr = FG_SRAM_BAT_FULL_RS_COMP_90PCT_REG_ADDR; AddrDataPairEx[4].SramData = FG_SRAM_BAT_FULL_RS_COMP_90PCT_REG_DATA;
  AddrDataPairEx[4].DataSize = 2;  AddrDataPairEx[4].DataOffset = 1;
  AddrDataPairEx[5].SramAddr = FG_SRAM_SYS_AUTORCHG_MASKING_DISABLED_REG_ADDR; AddrDataPairEx[5].SramData = FG_SRAM_SYS_AUTORCHG_MASKING_DISABLED_REG_DATA;
  AddrDataPairEx[5].DataSize = 1;  AddrDataPairEx[5].DataOffset = 1;

  PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgBattHandleProfile: KiCoeffPred = (0x%x)\n\r", AddrDataPairEx[3].SramData));

  //If data count is zero, updating register would be skipped
  AddrDataCount = 6;

  /* Read Battery Profile and if available Try loading battery profile to SRAM  */
  Status |= PmicFgBattReadBatteryProfile(PmicDeviceIndex, &Parserstatus);

  // Load profile here and retrieve the sram battery ID so that check for dummy battery id can be done here
  if ((EFI_SUCCESS == Status) && (NULL != FgProfile.Pd[0].addrData) && (PM_PARSER_STATUS_PARSE_ERROR != Parserstatus))
  {
    //Set and Pass Tolerance level
    SramBatt_Id.BatteryIdTolerance = BatteryDefaults.BattIdCfgData.BatteryIdTolerance;

    Status |= PmicFgSram_LoadBattProfile(PmicDeviceIndex, &FgProfile, AddrDataPairEx,  AddrDataCount, &SramBatt_Id);
    SelectedProfile = (UINTN )FgProfile.SelectedProfile;
    Status |= PmicUefiVariable(BATTERY_TYPE, &SelectedProfile, UEFI_VARIABLE_OP_SET);

    /*Assign Profile Params for Cache */
    Status |= PmicFgBattProfileReadProfileParams(PmicDeviceIndex, FALSE);

    /*update battery type*/
    PmicFgBatteryType(&BatteryDefaults, &SramBatt_Id, &FgProfile);

    if (EFI_SUCCESS == Status)
    {
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgBattHandleProfile: Batt_ID = (0x%x)\n\r", SramBatt_Id.Bid));
      if (TRUE == BatteryDefaults.BattIdCfgData.EnableDummyBattId)
      {
        if (((SramBatt_Id.Bid >= BatteryDefaults.BattIdCfgData.DummyBattIdMin1) && (SramBatt_Id.Bid <= BatteryDefaults.BattIdCfgData.DummyBattIdMax1 ))
          || ((SramBatt_Id.Bid >= BatteryDefaults.BattIdCfgData.DummyBattIdMin2) && (SramBatt_Id.Bid <= BatteryDefaults.BattIdCfgData.DummyBattIdMax2 ))){
          //Detected dummy battery
          Parserstatus = PM_PARSER_STATUS_DUMMY_BATTERY;
          PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Detected Dummy Battery: Batt_ID = (0x%x)\n\r", SramBatt_Id.Bid));
        }
      }
    }
    else{
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgBattHandleProfile: Sram : Batt_ID = (0x%x) DetId (0x%x)\n\r", SramBatt_Id.Bid, SramBatt_Id.DetId));
    }
  }
  else
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgBattReadBatteryProfile: Can not load Battery profile Parserstatus : (%d)\n\r", Parserstatus));
    Status |= PmicFgSram_Init(FG_SRAM_STATUS_AVAILABLE);
  }

  if ((PM_PARSER_STATUS_PARSE_ERROR == Parserstatus )|| (PM_PARSER_STATUS_CONFIG_ERROR == Parserstatus)
      || (PM_PARSER_STATUS_NO_ID_MATCH == Parserstatus))
  {
    /* Select OTP profile if the forecd OTP profile flag is set Check Config flag  */
    if(TRUE == BatteryDefaults.FgForceOtpProfile)
    {
      //Set battery OTP profile based on config set
      Status |= PmicFgSram_ForceOtpProfileReload(PmicDeviceIndex, BatteryDefaults.FgOtpProfileNum);
    }
  }
  /* Release not required data/memory */
  if(NULL != FgProfile.Pd[0].addrData)
  {
    /* Free allocated profile */
    for(Count = 0; (Count < MAX_NUM_OF_BATT_PROFILE) && (NULL != FgProfile.Pd[Count].addrData); Count++)
    {
      Status |= gBS->FreePool(FgProfile.Pd[Count].addrData);
      FgProfile.Pd[Count].addrData = NULL;
    }
    for(Count = 0; (Count < MAX_NUM_OF_BATT_PROFILE) && (NULL != FgProfile.Pd[Count].Pp.BatteryName); Count++)
    {
      Status |= gBS->FreePool(FgProfile.Pd[Count].Pp.BatteryName);
      FgProfile.Pd[Count].Pp.BatteryName = NULL;
    }
  }

  //Assign Parser Status for battery /debug board detection
  BatteryType_Parserstatus = Parserstatus;

  return Status;
}


/**
PmicFgBattReadBatteryProfile

@brief
Read battery profile from EFIESP/PLAT parition and Set Battery Profile respectivly
**/
EFI_STATUS PmicFgBattReadBatteryProfile
(
  UINT32 PmicDeviceIndex,
  EFI_PM_FG_PROFILE_PARSER_STATUS *Parserstatus
)
{
  EFI_STATUS               Status  = EFI_SUCCESS;
  PmicFgBattParam_GetDefaultCfg(&BattCfgDefaults);

  // Battery ID to be selected and range needs to configured or selected HW would pick up the respective matching battery profile
  //May need to define APIs to configure or force battery profile to use. Also smart battery detection needs to be enabled.
  if (EFI_ERROR(Status))
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Invalid battery ID! Error Code: 0x%08X\n\r", Status));
    goto end;
  }

  Status = GetPlatformType(&PlatformType);
  if (EFI_ERROR(Status))
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgBattReadBatteryProfile: Invalid platform type! Error Code: 0x%08X\n\r", Status));
    goto end;
  }
  switch(PlatformType)
  {
    case EFI_PLATFORMINFO_TYPE_CDP:
    case EFI_PLATFORMINFO_TYPE_FLUID:
    case EFI_PLATFORMINFO_TYPE_MTP_MSM:
    //case EFI_PLATFORMINFO_TYPE_CHI:
      if(BattCfgDefaults.ReadBattProfileFromPartition)
      {
        *Parserstatus = PmicFgBattGetBattProfile();
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgBattGetBattProfile: : Parserstatus = (%d) \n\r", *Parserstatus));
      }
      else
      {
        *Parserstatus = PM_PARSER_STATUS_CONFIG_ERROR;
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Battery Profile Load from Partition Disabled: Parserstatus = (%d) \n\r", *Parserstatus));
      }

      break;

    case EFI_PLATFORMINFO_TYPE_QT:
    case  EFI_PLATFORMINFO_TYPE_LIQUID:

      if(BattCfgDefaults.ReadBattProfileFromPartition)
      {
        *Parserstatus = PmicFgBattGetBattProfile();
      }
      else
      {
        *Parserstatus = PM_PARSER_STATUS_CONFIG_ERROR;
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Battery Profile Load from Partition Disabled: Parserstatus = (%d) \n\r", *Parserstatus));
      }
      break;

    case EFI_PLATFORMINFO_TYPE_QRD:

      if(BattCfgDefaults.ReadBattProfileFromPartition)
      {
        *Parserstatus = PmicFgBattGetBattProfile();
      }
      else
      {
        *Parserstatus = PM_PARSER_STATUS_CONFIG_ERROR;
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Battery Profile Load from Partition Disabled: Parserstatus = (%d) \n\r", *Parserstatus));
      }
      break;

     default:
       PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Platform not detected as known configuration: Default case Error Code: 0x%08X\n\r", Status));
      break;
  }

end:
  return Status;
}

/*==========================================================================
                        LOCAL API DEFINITION
===========================================================================*/
/**
PmicFgBattGetBattProfile

@brief
Reads Battery Profile based on platform type
**/
EFI_PM_FG_PROFILE_PARSER_STATUS PmicFgBattGetBattProfile(VOID)
{
  EFI_PM_FG_PROFILE_PARSER_STATUS ParserStatus  = PM_PARSER_STATUS_GOOD_MATCH;
  EFI_STATUS               Status        = EFI_SUCCESS;
  char                     *pData = NULL;
  UINTN                    DataSize      = 0;

  Status = GetPlatformType(&PlatformType);

  switch(PlatformType)
  {
    case EFI_PLATFORMINFO_TYPE_CDP:
    case EFI_PLATFORMINFO_TYPE_FLUID:
    case EFI_PLATFORMINFO_TYPE_MTP_MSM:
    case EFI_PLATFORMINFO_TYPE_QRD:
    //case EFI_PLATFORMINFO_TYPE_CHI:

      Status = PmicFgBattProfileEmmcReadFile(&pData, &DataSize);
      if((EFI_SUCCESS != Status) || (NULL == pData))
      {
        return PM_PARSER_STATUS_PARSE_ERROR;
      }
      break;

    case EFI_PLATFORMINFO_TYPE_QT:
    case EFI_PLATFORMINFO_TYPE_LIQUID:

      Status = PmicFgBattProfileDppReadFile(&pData, &DataSize);
      if((EFI_SUCCESS != Status) || (NULL == pData))
      {
        return PM_PARSER_STATUS_NO_DPP;
      }
    break;

    default:
      pData = NULL;
    break;
  }

  if(EFI_SUCCESS == Status && NULL != pData)
  {
    ParserStatus = PmicBattProfileParseDataBuffer(pData, DataSize);
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgBattGetBattProfile Error Code: %d \n\r", Status));
  }
  else
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgBattGetBattProfile Error Code: %d \n\r", Status));
  }
  return ParserStatus;
}

#define SECTION_START_CHAR    '['
/**
PmicBattProfileParseDataBuffer

@brief
Parse battery profile Data on given Buffer
**/
 EFI_PM_FG_PROFILE_PARSER_STATUS PmicBattProfileParseDataBuffer
(
 IN  char       *pBufferData,
 IN  UINTN       DataBufferSize
)
{
  INTN  Pd = 0;
  EFI_PM_FG_PROFILE_PARSER_STATUS ParserStatus = PM_PARSER_STATUS_GOOD_MATCH;

  Pd = OpenParser ((UINT8 *)pBufferData, DataBufferSize, NULL);

  if (Pd < 0)
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Pmic Battery Profile- Parser open failed on read batter profile data \n\r"));
    ParserStatus = PM_PARSER_STATUS_PARSE_ERROR;
  }
  else
  {
    /* BackWard Compatibility for battery profile */
    if (pBufferData[0] == SECTION_START_CHAR)
    {
      /* Kick off Pmic CFG file interpreter */
      EnumCommaDelimSectionItems (Pd, (UINT8 *)"BATTERY_PROFILE", PmicFgBattProfileTokenPtrArrCallBack);
    }
    else
    {
      EnumCommaDelimSectionItems (Pd, NULL, PmicFgBattProfileTokenPtrArrCallBack);
    }
    /* Clean up resources */
    CloseParser(Pd);
  }
  if (TRUE == profileValid){
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicBattProfileParseDataBuffer Battery Profile- Success \n\r"));
  }else
  {
    ParserStatus = PM_PARSER_STATUS_PARSE_ERROR;
    profileValid = FALSE;
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Pmic Battery Profile- Parser failed (%d)\n\r", ParserStatus));
  }

  return ParserStatus;

}


/**
PmicFgBattProfileTokenPtrArrCallBack

@brief
Token ptr array call back for battey profile data parsing
**/
VOID
PmicFgBattProfileTokenPtrArrCallBack(UINT8* Section, UINTN SectionOffset, UINT8* TokenPtrArray[], UINTN MaxTokens)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 tokenCount = 0;
  UINT32 absAddr    = 0;
  UINT32 strLen     = 0;
  UINT16 pEncodedValue = 0;
  STATIC UINTN SectionCnt = 0;//SectionOffset start with 1

  /*PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Section = (%a) , SectionOffset = (%d), TokenPtrArray = (%a), MaxTokens = (%d)\n\r",
              Section, SectionOffset, TokenPtrArray[0], MaxTokens)); */
  /* BackWard Compatibility for battery profile */
  if(0 == SectionOffset)
    SectionOffset =1;
/* Enable below when backward support for battery profile is not needed */
#if 0
  if(AsciiStriCmp((CHAR8 *)Section, (CHAR8*)TAG_BATTERY_RPOFILE) != 0)
  {
    PMIC_DEBUG(( EFI_D_WARN, "PmicDxe: Profile Tag Mis Match (%a) (%a)\n\r", TAG_BATTERY_RPOFILE, Section));
    profileValid = FALSE;
    return;
  }
#endif

  if(MAX_NUM_OF_BATT_PROFILE < SectionOffset)
  {
    PMIC_DEBUG(( EFI_D_WARN, "PmicDxe: Profile support limit reached (%d) (%d)\n\r", MAX_NUM_OF_BATT_PROFILE, SectionOffset));
    //profileValid = FALSE;
    return;
  }

  if((SectionCnt != SectionOffset ) && (NULL == FgProfile.Pd[SectionOffset - 1].addrData))
  {
    /* Increment section count */
    ++SectionCnt;
    if(AsciiStrCmp((CHAR8 *)TokenPtrArray[SectionOffset - SectionCnt], TAG_GUI_VERSION) == 0)
    {
      PMIC_UART_DEBUG(( EFI_D_ERROR, "PmicDxe: Valid Profile GUI Version = (%a)\n\r", TokenPtrArray[(SectionOffset - SectionCnt) + 1]));
      PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: Valid Profile GUI Version = (%s)\n\r", TokenPtrArray[(SectionOffset - SectionCnt) + 1]));
      profileValid = TRUE;
    }
    else
    {
      PMIC_DEBUG(( EFI_D_WARN, "PmicDxe: Profile (%d) Invalid \n\r", SectionOffset));
      profileValid = FALSE;
    }
    /* Allocate Memory for one profile */
    Status = gBS->AllocatePool( EfiBootServicesData,
                            sizeof(FgBattAddrData) * BATTARY_PROFILE_MAX_SIZE,
                            (VOID**)&FgProfile.Pd[SectionOffset - 1].addrData);
    (void)FgProfile.Pd[SectionCnt];
    if(EFI_SUCCESS != Status || NULL == FgProfile.Pd[SectionOffset - 1].addrData)
    {
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicBattProfileParseDataBuffer: Unable to allocate pool! Error Code: 0x%08X \n\r", Status));
      return ;
    }
    SetMem(FgProfile.Pd[SectionOffset - 1].addrData, sizeof(FgBattAddrData) * BATTARY_PROFILE_MAX_SIZE, 0x00);
    /* Keep Parsed profile counter updated */
    FgProfile.ProfileCount = (UINT32 )SectionOffset;
    /* Reset Profile Address Count */
    ProfileAddrCount = 0;
  }

  if (TRUE == profileValid)
  {
    for (tokenCount = 0; ((tokenCount <= MaxTokens) && (NULL != TokenPtrArray[tokenCount])); tokenCount++, absAddr = 0)
    {
      //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Token(%d) = (%a) \n\r", tokenCount, TokenPtrArray[tokenCount]));

      if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_BATTERY_ID) == 0)
      {
         FgProfile.Pd[SectionOffset - 1].Pp.BatteryId = PmicFgBattProfileAsciiToInt((CHAR8 *)TokenPtrArray[tokenCount + 1]);
        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_PROFILE_CHKSUM) == 0)
      {
        FgProfile.Pd[SectionOffset - 1].Pp.CheckSum = AsciiStrToHex((CHAR8 *)TokenPtrArray[tokenCount + 1], (UINT32)AsciiStrLen((CHAR8 *)TokenPtrArray[tokenCount + 1]) + 1 );
        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_FLOAT_VOLTAGE) == 0)
      {
        FgProfile.Pd[SectionOffset - 1].Pp.FloatVoltage = PmicFgBattProfileAsciiToInt((CHAR8 *)TokenPtrArray[tokenCount + 1]);
        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_NOMINAL_BATTERY_CAPACITY) == 0)
      {
        FgProfile.Pd[SectionOffset - 1].Pp.NomBattCapacity = PmicFgBattProfileAsciiToInt((CHAR8 *)TokenPtrArray[tokenCount + 1]);
        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_BATTERY_NAME) == 0)
      {
        //Ignore battery name just log the name
        PMIC_UART_DEBUG(( EFI_D_ERROR,  "PmicDxe: Battery Name = (%a) \n\r", TokenPtrArray[tokenCount + 1]));
        PMIC_FILE_DEBUG(( EFI_D_ERROR, "PmicDxe: Battery Name = (%s) \n\r", TokenPtrArray[tokenCount + 1]));
        FgProfile.Pd[SectionOffset - 1].Pp.BatteryName = (UINT8 *) AsciiStrNDup((CHAR8 *)TokenPtrArray[tokenCount + 1], (UINT32)AsciiStrLen((CHAR8 *)TokenPtrArray[tokenCount + 1]) + 1);
        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_THERMISTOR_B) == 0)
      {
        FgProfile.Pd[SectionOffset - 1].Pp.ThermistorValue = PmicFgBattProfileAsciiToInt((CHAR8 *)TokenPtrArray[tokenCount + 1]);
        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_RSLOW_COMP_ALONG_SOC_C1_CH) == 0)
      {
        FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC1ch = PmicFgBattProfileAsciiToInt((CHAR8 *)TokenPtrArray[tokenCount + 1]);
        Status = PmicFgSram_GetHALFEncode(FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC1ch, &pEncodedValue);
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: RslowCompSocC1ch Encoded HALF = (%d) (0x%x) \n\r", FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC1ch, pEncodedValue));
        FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC1ch = pEncodedValue;
        //FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC1ch = 0x8194;
        //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: RslowCompSocC1ch Over Writing = (0x%x) \n\r", FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC1ch));
        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_RSLOW_COMP_ALONG_SOC_C2_CH) == 0)
      {
        FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC2ch = PmicFgBattProfileAsciiToInt((CHAR8 *)TokenPtrArray[tokenCount + 1]);
        Status = PmicFgSram_GetHALFEncode(FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC2ch, &pEncodedValue);
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: RslowCompSocC2ch Encoded HALF = (%d) (0x%x) \n\r", FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC2ch, pEncodedValue));
        FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC2ch = pEncodedValue;
        //FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC2ch = 0x827E;
        //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: RslowCompSocC2ch Over Writing = (0x%x) \n\r", FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompSocC2ch));

        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_RS_TO_RSLOW_CHARGE_FIX) == 0)
      {
        FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RsToRslowChargeFix = PmicFgBattProfileAsciiToInt((CHAR8 *)TokenPtrArray[tokenCount + 1]);
        Status = PmicFgSram_GetHALFEncode(FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RsToRslowChargeFix, &pEncodedValue);
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: RsToRslowChargeFix Encoded HALF = (%d) (0x%x) \n\r", FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RsToRslowChargeFix, pEncodedValue));
        FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RsToRslowChargeFix = pEncodedValue;
        //FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RsToRslowChargeFix = 0x7855;
        //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: RsToRslowChargeFix Over Writing = (0x%x) \n\r", FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RsToRslowChargeFix));

        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_RSLOW_COMP_CH_FIX_TH) == 0)
      {
        FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompChFixTh = PmicFgBattProfileAsciiToInt((CHAR8 *)TokenPtrArray[tokenCount + 1]);
        pEncodedValue = ((((FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompChFixTh * 0xFF)/0x64 )
                            + FG_SRAM_ROUNDING_COSNT )/0xA);
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: RslowCompChFixTh = (%d) (0x%x) \n\r", FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompChFixTh, pEncodedValue));
        FgProfile.Pd[SectionOffset - 1].Pp.RslowPp.RsProfileConfigs.RslowCompChFixTh = pEncodedValue;
        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_THERM_C1_COEFF) == 0)
      {
        strLen = AsciiStrLen((CHAR8 *)TokenPtrArray[tokenCount + 1]);
        FgProfile.Pd[SectionOffset - 1].Pp.ThermCoff.ThermC1Coeff = PmicFgBattProfileAsciiStrToHex((CHAR8 *)TokenPtrArray[tokenCount + 1], strLen );
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: ThermC1Coeff = (0x%x) \n\r", FgProfile.Pd[SectionOffset - 1].Pp.ThermCoff.ThermC1Coeff));
        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_THERM_C2_COEFF) == 0)
      {
        strLen = AsciiStrLen((CHAR8 *)TokenPtrArray[tokenCount + 1]);
        FgProfile.Pd[SectionOffset - 1].Pp.ThermCoff.ThermC2Coeff = PmicFgBattProfileAsciiStrToHex((CHAR8 *)TokenPtrArray[tokenCount + 1], strLen );
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: ThermC2Coeff = (0x%x) \n\r", FgProfile.Pd[SectionOffset - 1].Pp.ThermCoff.ThermC2Coeff));
        break;
      }
      else if(AsciiStriCmp((CHAR8 *)TokenPtrArray[tokenCount], TAG_THERM_C3_COEFF) == 0)
      {
        strLen = AsciiStrLen((CHAR8 *)TokenPtrArray[tokenCount + 1]);
        FgProfile.Pd[SectionOffset - 1].Pp.ThermCoff.ThermC3Coeff = PmicFgBattProfileAsciiStrToHex((CHAR8 *)TokenPtrArray[tokenCount + 1], strLen );
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: ThermC3Coeff = (0x%x) \n\r", FgProfile.Pd[SectionOffset - 1].Pp.ThermCoff.ThermC3Coeff));
        break;
      }else{
        strLen = AsciiStrLen((CHAR8 *)TokenPtrArray[tokenCount]);
        absAddr = PmicFgBattProfileAsciiStrToHex((CHAR8 *)TokenPtrArray[tokenCount], strLen );
        //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: absAddr(%d) \n\r", absAddr));
        if ((absAddr >= FG_SRAM_PROFILE_ABS_START_ADDR ) && (absAddr <= FG_SRAM_PROFILE_ABS_END_ADDR )){
          PmicFgBattProfile_ProcessTokenArray(Section, SectionOffset, TokenPtrArray, MaxTokens);
          break;//break for loop as we read entire line here
        }
      }
    }
  }
  else
  {
    PMIC_DEBUG(( EFI_D_WARN, "PmicDxe: Profile Invalid \n\r"));
  }
}


EFI_STATUS PmicFgBattProfile_ProcessTokenArray(UINT8* Section, UINTN SectionOffset, UINT8* TokenPtrArray[], UINTN MaxTokens)
{
  STATIC UINT32 PartialData = 0;
  UINT32 Addr = 0, Addr2 = 0, Offset = 0, Data = 0;

  /* BackWard Compatibility for battery profile */
  if(SectionOffset == 0)
    SectionOffset =1;

  //Validate input params
  if ((MaxTokens < BATT_PROFILE_DATA_TKN_IN_LINE)&&
    ((NULL != TokenPtrArray[0]) && (NULL != TokenPtrArray[1]) && (NULL != TokenPtrArray[2]) && (NULL != TokenPtrArray[3])))
    return EFI_INVALID_PARAMETER;
  Addr   = AsciiStrToHex((CHAR8 *)TokenPtrArray[0], StrLen ((UINT16 *)TokenPtrArray[0]) + 2);
  Addr2  = AsciiStrToHex((CHAR8 *)TokenPtrArray[1], StrLen ((UINT16 *)TokenPtrArray[1]) + 2);
  Offset = AsciiStrToHex((CHAR8 *)TokenPtrArray[2], StrLen ((UINT16 *)TokenPtrArray[2]) + 2);
  Data   = AsciiStrToHex((CHAR8 *)TokenPtrArray[3], StrLen ((UINT16 *)TokenPtrArray[3]) + 2);

  if ((Addr2 >= FG_SRAM_PROFILE_START_ADDR ) && (Addr2 <= FG_SRAM_PROFILE_END_ADDR )&& (ProfileAddrCount <= BATT_PROFILE_LEN_IN_4_BYTE ))
  {
    PartialData = PartialData | (Data << (Offset * NUM_BITS_IN_BYTE));
    //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PartialData = (0x%x)\"\n",PartialData));
    if (Offset == BATT_PROFILE_DATA_4_OFFSET){
      FgProfile.Pd[SectionOffset - 1].addrData[ProfileAddrCount].Data = PartialData;
      //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: BatteryProfileArray[(%d)] = (0x%x)\"\n", ProfileAddrCount, FgProfile.addrData[ProfileAddrCount]));
      //Assign other elements for book keeping
      FgProfile.Pd[SectionOffset - 1].addrData[ProfileAddrCount].Addr  = Addr;
      FgProfile.Pd[SectionOffset - 1].addrData[ProfileAddrCount].Addr2 = Addr2;
      FgProfile.Pd[SectionOffset - 1].addrData[ProfileAddrCount].Offset = Offset;

      PartialData = 0;//reset 4 byte append count
      ProfileAddrCount++;//upkeep address count
    }else{
      //PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Profile Addr = (0x%x), Addr2 = (0x%x), Offset = (0x%x), Data = (0x%x)\r\n",Addr, Addr2, Offset, Data));
    }
  }
  else{
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: ** Profile Addr Not in Range ** \r\n"));
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Profile Addr = (0x%x), Addr2 = (0x%x), Offset = (0x%x), Data = (0x%x)\r\n",Addr, Addr2, Offset, Data));
  }
  return EFI_SUCCESS;
}

/**
PmicFgBattProfileGetFileSize

@brief
Returns File Size
**/
EFI_STATUS PmicFgBattProfileGetFileSize(
  IN  CHAR16           *Path,
  IN  EFI_GUID         *RootDeviceType,
  IN  EFI_GUID         *PartitionType,
  IN  BOOLEAN          SelectNonRemovable,
  IN  CHAR8            *VolumeName,
  OUT UINTN           *FileSize
)
{
  EFI_STATUS        Status;
  EFI_FILE_HANDLE   FileHandle;
  EFI_FILE_INFO    *Info    = NULL;
  UINTN             InfoSize = FILE_INFO_SIZE;

  // Open File
  Status = PmicFgBattProfileOpenFile( Path,
                                       RootDeviceType,
                                       PartitionType,
                                       SelectNonRemovable,
                                       VolumeName,
                                       &FileHandle );

  // Get the File info
  if(!EFI_ERROR(Status))
  {
    Info = AllocatePool (InfoSize);

    if(!Info){
      Status =  EFI_OUT_OF_RESOURCES;
    }
    else{
      Status = FileHandle->GetInfo(FileHandle,
                                   &gEfiFileInfoGuid,
                                   &InfoSize,
                                   Info
                                   );

      if(Status == EFI_BUFFER_TOO_SMALL){
        // If Status == EFI_BUFFER_TOO_SMALL, InfoSize is too small to read file-info
        // InfoSize is updated with the actual size that needs to be allocated for Info
        // So, re-allocate with InfoSize, and call GetInfo()
        FreePool (Info);
        Info   = AllocatePool (InfoSize);
        Status = FileHandle->GetInfo(FileHandle,
                                     &gEfiFileInfoGuid,
                                     &InfoSize,
                                     Info
                                     );
      }

      if(!EFI_ERROR(Status) && NULL != Info){
        // If GetInfo() is successful, return the size of file
        *FileSize = Info->FileSize;
      }

      FreePool (Info);
      Info = NULL;
    }

    // Close file-handle
    if(FileHandle != NULL){
      Status = FileHandle->Close(FileHandle);
    }
  }

  return Status;
}


/**
  PmicFgBattProfileEmmcReadFile

  @brief
  Read battery profile from Emmc/PLAT patition
**/
EFI_STATUS PmicFgBattProfileEmmcReadFile(char **pData, UINTN *DataSize)
{
  EFI_STATUS Status          = EFI_SUCCESS;
  char       *tpData         = NULL;
  BOOLEAN     partitionPath  = FALSE;
  EFI_GUID   *RootDeviceType = NULL ;
  EFI_GUID   *PartitionType  = NULL;
  EFI_PM_FG_CFGDATA_TYPE BatteryDefaults;

  do{

    //Read  flag from Battery config defaluts
    Status = PmicFgBattParam_GetDefaultCfg(&BatteryDefaults);
    BREAK_IF_ERROR(Status);

    RootDeviceType = &gEfiEmmcUserPartitionGuid;

    switch(BatteryDefaults.ChkBattProfileFirstFromEfiEspOrPlat)
    {

      case TRUE:
        /* Look EFIESP Partition first */
        PartitionType  = &gEfiBDPPartitionGuid;

        /* Attempt to load Battery Provision file from EFI system partition */
        Status = PmicFgBattProfileGetFileSize( EFIESP_BATTERY_PROVISION_PATH,
                                                 RootDeviceType,
                                                 PartitionType,
                                                 TRUE,
                                                 NULL,
                                                 DataSize );
        if( EFI_SUCCESS != Status && 0 == *DataSize)
        {
          /* Set it to PLAT partition and look for it */
          PartitionType  = &gEfiPlatPartitionTypeGuid;
          partitionPath  = FALSE;
          PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Battery Provision File not Found in EFIESP - Status = (%d) DataSize = (%d)\r\n", Status, *DataSize));
        }
        else
        {
          PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Battery Provision File found in EFIESP - File DataSize = (%d)\r\n", *DataSize));
          partitionPath  = TRUE;
        }
      break;

      case FALSE:
        /* Look PLAT Partition First */
        PartitionType  = &gEfiPlatPartitionTypeGuid;

        /* Attempt to load Battery Provision file from Plat parition in eMMC GPP */
        Status = PmicFgBattProfileGetFileSize( PLAT_BATTERY_PROVISION_PATH,
                                                 RootDeviceType,
                                                 PartitionType,
                                                 TRUE,
                                                 NULL,
                                                 DataSize);
        if( 0 == *DataSize || Status != EFI_SUCCESS )
        {
          /* Set it to EFIESP partition and look for it */
          PartitionType  = &gEfiBDPPartitionGuid;
          partitionPath  = TRUE;
          PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Battery Provision File not Found in PLAT - Status = (%d) DataSize = (%d).\r\n", Status, *DataSize));
        }
        else
        {
          PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Battery Provision File found in PLAT Partition - File DataSize = (%d)\r\n", *DataSize));
          partitionPath  = FALSE;
        }
      break;

      default:
      break;
    }

    *DataSize = 0;
    //Search now in respective paritition
    if(TRUE  == partitionPath)
    {
      Status = PmicFgBattProfileRead(EFIESP_BATTERY_PROVISION_PATH, RootDeviceType, PartitionType, TRUE , NULL , &tpData, DataSize);
      BREAK_IF_ERROR(Status);
    }
    else
    {
      Status = PmicFgBattProfileRead(PLAT_BATTERY_PROVISION_PATH, RootDeviceType, PartitionType, TRUE , NULL , &tpData, DataSize);
      BREAK_IF_ERROR(Status);
    }
  }while(FALSE);


  //Check if any error otherwise process with success
  if(EFI_SUCCESS != Status || NULL == tpData)
  {
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: PmicFgBattProfileEmmcReadFile: Error Status(%d)\r\n", Status));
  }
  else
  {
    *pData = tpData;
  }

  return Status;
}


/**
  PmicFgBattProfileRead
  @brief
  Read battery profile from given path **/
EFI_STATUS PmicFgBattProfileRead(
  IN  CHAR16           *Path,
  IN  EFI_GUID         *RootDeviceType,
  IN  EFI_GUID         *PartitionType,
  IN  BOOLEAN          SelectNonRemovable,
  IN  CHAR8            *VolumeName,
  OUT CHAR8            **pData,
  OUT UINTN           *DataSize
)
{
  EFI_STATUS  Status  = EFI_SUCCESS;
  CHAR8       *tpData = NULL;

  //Attempt to load Battery Provision file from Plat parition in eMMC GPP
  do{
    Status = PmicFgBattProfileGetFileSize( Path,
                                           RootDeviceType,
                                           PartitionType,
                                           TRUE,
                                           NULL,
                                           DataSize);
    if(EFI_SUCCESS == Status && 0 != *DataSize)
    {
      Status = gBS->AllocatePool( EfiBootServicesData,
                                *DataSize,
                                (VOID**)&tpData);
      BREAK_IF_ERROR(Status);
      Status = PmicFgBattProfileReadFile( Path,
                                          RootDeviceType,
                                          PartitionType,
                                          TRUE,
                                          NULL,
                                          DataSize,
                                          0,
                                          (UINT8 *)tpData,
                                          *DataSize );
      BREAK_IF_ERROR(Status);
    }
  }while(FALSE);

  //Check if any error
  if((0 != *DataSize ) && (EFI_SUCCESS == Status) && NULL != tpData)
    *pData = tpData;
  else
  {
    *pData = NULL;
  }

  return Status;

}


/**
  PmicFgBattProfileDppReadFile

  @brief
  Read DPP battery profile from Dpp patition
**/
EFI_STATUS PmicFgBattProfileDppReadFile(char **pData, UINTN *DataSize)
{
  EFI_STATUS        Status        = EFI_SUCCESS;
  EFI_DPP_PROTOCOL  *pDppProtocol = NULL;
  char              *tpData       = NULL;
  UINTN TempDataSize = 0;
  Status = gBS->LocateProtocol( &gEfiDppProtocolGuid,
                                NULL,
                                (VOID**)&pDppProtocol);

  if(EFI_SUCCESS != Status)
  {
      return PM_PARSER_STATUS_PARSE_ERROR;
  }

  Status = pDppProtocol->GetDPP( pDppProtocol,
                                  L"QCOM",
                                  L"BATTERY.PROVISION",
                                  NULL,
                                  (UINT32*)&TempDataSize);

  if(EFI_SUCCESS != Status)
  {
      return PM_PARSER_STATUS_PARSE_ERROR;
  }

  Status = gBS->AllocatePool( EfiBootServicesData,
                              TempDataSize,
                              (VOID**)&tpData);

  if(EFI_SUCCESS != Status)
  {
      return PM_PARSER_STATUS_PARSE_ERROR;
  }

  Status = pDppProtocol->GetDPP( pDppProtocol,
                                  L"QCOM",
                                  L"BATTERY.PROVISION",
                                  tpData,
                                  (UINT32*)&TempDataSize);
    //Check if any error otherwise proceed with success
  if(EFI_SUCCESS != Status || NULL == tpData )
  {
    *pData = NULL;
  }
  else
  {
    *pData = tpData;
    *DataSize = TempDataSize;
  }
  return Status;
}


/**
PmicFgBattProfileReadFile

@brief
Read battery profile
**/
EFI_STATUS PmicFgBattProfileReadFile(
  IN     CHAR16           *Path,
  IN     EFI_GUID         *RootDeviceType,
  IN     EFI_GUID         *PartitionType,
  IN     BOOLEAN          SelectNonRemovable,
  IN     CHAR8            *VolumeName,
  IN OUT UINTN            *BytesToRead,
  IN     UINT64           ReadOffset,
  OUT    UINT8            *FileBuffer,
  IN     UINTN           FileBufferSize
)
{
  EFI_STATUS      Status = EFI_SUCCESS;
  EFI_FILE_HANDLE FileHandle;

  if((BytesToRead == NULL) || (*BytesToRead == 0) || ((*BytesToRead + ReadOffset) > FileBufferSize)){
    return EFI_INVALID_PARAMETER;
  }


  // Open File
  Status = PmicFgBattProfileOpenFile( Path,
                                       RootDeviceType,
                                       PartitionType,
                                       SelectNonRemovable,
                                       VolumeName,
                                       &FileHandle );
  if(!EFI_ERROR(Status)){
          if (FileHandle)
          {
              // Set read-offset
              Status = FileHandle->SetPosition( FileHandle,
                                                ReadOffset );

              if(EFI_ERROR(Status)){
                PMIC_DEBUG ((DEBUG_LOAD, "PmicDxe: ReadFile(): Unable to set offset for file-read\r\n" ));
              }
              else{
                // Read file content
                Status = FileHandle->Read( FileHandle,
                                           BytesToRead,
                                           FileBuffer );

                if ((EFI_ERROR (Status)) || (*BytesToRead == 0)) {
                  PMIC_DEBUG ((DEBUG_LOAD, "PmicDxe: ReadFile() Unable to read from file \r\n"));
                }
              }

              // Close the file handle
              Status = FileHandle->Close(FileHandle);
          }
    }


  return Status;
}

/**
PmicFgBattProfileOpenFile

@brief
Open battery profile File
**/
EFI_STATUS PmicFgBattProfileOpenFile(
  IN  CHAR16           *Path,
  IN  EFI_GUID         *RootDeviceType,
  IN  EFI_GUID         *PartitionType,
  IN  BOOLEAN          SelectNonRemovable,
  IN  CHAR8            *VolumeName,
  OUT EFI_FILE_HANDLE  *FileHandle
)
{
  EFI_STATUS                         Status;
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL   *Volume;
  EFI_FILE_HANDLE                   RootFileHandle;
  UINT32                            MaxHandles;
  UINT32                            Attrib = 0;
  UINT32                            i      = 0;

  MaxHandles = sizeof(BattProfileHandleInfoList)/sizeof(*BattProfileHandleInfoList);

  if( (RootDeviceType == NULL) && (PartitionType == NULL) )
    return EFI_INVALID_PARAMETER;

  Attrib |= BLK_IO_SEL_PARTITIONED_GPT;

  if (SelectNonRemovable == TRUE)
    Attrib |= BLK_IO_SEL_MEDIA_TYPE_NON_REMOVABLE;

  if (RootDeviceType != NULL)
    Attrib |= BLK_IO_SEL_MATCH_ROOT_DEVICE;

  if (PartitionType != NULL)
    Attrib |= BLK_IO_SEL_MATCH_PARTITION_TYPE_GUID;

  Attrib |= BLK_IO_SEL_SELECT_MOUNTED_FILESYSTEM;

  BattProfileHandleFilter.PartitionType = PartitionType;
  BattProfileHandleFilter.RootDeviceType = RootDeviceType;

  if(VolumeName != NULL)
      BattProfileHandleFilter.VolumeName = VolumeName;
    else
      BattProfileHandleFilter.VolumeName = 0;

  // Returns the IO Block handle
  Status = GetBlkIOHandles( Attrib,
                            &BattProfileHandleFilter,
                            BattProfileHandleInfoList,
                            &MaxHandles );

  for(i = 0; i < MaxHandles; i++)
  {
      if(EFI_PmicBattProfileCmpArray((UINT32 *)&(BattProfileHandleInfoList[i].PartitionInfo->Signature[0])))
      {
        MaxHandles = 1;
        BattProfileHandleInfoList[0] = BattProfileHandleInfoList[i];

        break;
      }
  }

  if( !EFI_ERROR(Status) ) {
    if(MaxHandles == 0)
      return EFI_NO_MEDIA;

    if(MaxHandles != 1) {
      // Unable to deterministically load from single partition
      PMIC_DEBUG(( EFI_D_INFO, "PmicDxe: OpenFile(): multiple partitions found \r\n"));
      return EFI_LOAD_ERROR;
    }
  }

  // Find the file system interface to the device
  Status = gBS->HandleProtocol( BattProfileHandleInfoList[0].Handle,
                                &gEfiSimpleFileSystemProtocolGuid,
                                (VOID *) &Volume );

  if(EFI_ERROR(Status))
    return Status;

  // Open the root directory of the volume
  Status = Volume->OpenVolume( Volume,
                               &RootFileHandle );
  if(EFI_ERROR(Status))
    return Status;

  if (RootFileHandle != NULL)
  {
  Status = RootFileHandle->Open( RootFileHandle,
                                 FileHandle,
                                 Path,
                                 EFI_FILE_MODE_READ,
                                 0 );
  if(EFI_ERROR(Status))
    *FileHandle = NULL;

    // Close root-dir handle

    RootFileHandle->Close(RootFileHandle);
  }

  return Status;
}

EFI_STATUS PmicFgBattParserStatus(EFI_PM_FG_PROFILE_PARSER_STATUS *parser_status)
{
  if(!parser_status)
    return EFI_INVALID_PARAMETER;

  *parser_status = BatteryType_Parserstatus;

  return EFI_SUCCESS;
}

/*
Read  Profile Params for flashed profile
  */
EFI_STATUS PmicFgBattProfileReadProfileParams(UINT32 PmicDeviceIndex, BOOLEAN ReadProfile)
{
  EFI_STATUS Status = EFI_SUCCESS;
  EFI_PM_FG_PROFILE_PARSER_STATUS Parserstatus = PM_PARSER_STATUS_OTHER_ERROR;
  UINTN FgProfileSelected = 0;
  UINT32  Count = 0;

  if(TRUE == ReadProfile)
  {
    Status |= PmicUefiVariable(BATTERY_TYPE, &FgProfileSelected, UEFI_VARIABLE_OP_GET);

    if (Status != EFI_SUCCESS) {
      // Variable was not found, default to disabled
      FgProfileSelected = 0;
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Error in Reading Flashed Profile Index \r\n"));
      //return EFI_NOT_FOUND;
    }
    /* Read Battery Profile and if available Try loading battery profile to SRAM  */
    Status |= PmicFgBattReadBatteryProfile(PmicDeviceIndex, &Parserstatus);
  }
  else
  {
    Parserstatus = PM_PARSER_STATUS_GOOD_MATCH;
  }
  // Load profile here and retrieve the sram battery ID so that check for dummy battery id can be done here
  if ((EFI_SUCCESS == Status) && (NULL != FgProfile.Pd[0].addrData) && (PM_PARSER_STATUS_PARSE_ERROR != Parserstatus))
  {
    /*Retrieve loaded battery profile index to UEFI variable */
    if(NULL != FgProfile.Pd[FgProfileSelected].addrData)
    {
      /* Cache common parameters */
      ProfileCachedParams.CurrProfileRev                            = (UINT32 ) (((FgProfile.Pd[FgProfileSelected].addrData[FG_SRAM_PROFILE_REV_ADDR].Data) >> 24) & 0xFF);
      /*BatteryId and Battery Name is not cached during warm reset */
      ProfileCachedParams.FloatVoltage                = FgProfile.Pd[FgProfileSelected].Pp.FloatVoltage;
      ProfileCachedParams.NomBattCapacity             = FgProfile.Pd[FgProfileSelected].Pp.NomBattCapacity;
      ProfileCachedParams.ThermistorValue             = FgProfile.Pd[FgProfileSelected].Pp.ThermistorValue;
      ProfileCachedParams.CheckSum                    = FgProfile.Pd[FgProfileSelected].Pp.CheckSum;
      ProfileCachedParams.BatteryName                 = NULL;
      ProfileCachedParams.ThermCoff.ThermC1Coeff      = FgProfile.Pd[FgProfileSelected].Pp.ThermCoff.ThermC1Coeff;
      ProfileCachedParams.ThermCoff.ThermC2Coeff      = FgProfile.Pd[FgProfileSelected].Pp.ThermCoff.ThermC2Coeff;
      ProfileCachedParams.ThermCoff.ThermC3Coeff      = FgProfile.Pd[FgProfileSelected].Pp.ThermCoff.ThermC3Coeff;

      /* First save Battery Profile Params which will be used to write on Charge Fix Sequence */
     /*Get 2[4], 2[5] bit and Profile Revision */
      ProfileCachedParams.RslowPp.RsProfileSectionParams.RslowProfileFlag.RslowLowSocDisCharge     = (FgProfile.Pd[FgProfileSelected].addrData[FG_SRAM_RSLOW_SOC_DISCHARGE_ADRR_CNT].Data & 0x00100000) ? TRUE : FALSE;
      ProfileCachedParams.RslowPp.RsProfileSectionParams.RslowProfileFlag.RslowLowSocDisChargeC2En = (FgProfile.Pd[FgProfileSelected].addrData[FG_SRAM_RSLOW_SOC_DISCHARGE_ADRR_CNT].Data & 0x00200000) ? TRUE : FALSE;
      ProfileCachedParams.RslowPp.RsProfileSectionParams.RslowCompSocC1ch    = (UINT32 ) ((FgProfile.Pd[FgProfileSelected].addrData[FG_SRAM_RSLOW_SOC_C1_C2_ADRR_CNT].Data ) & 0xFFFF);
      ProfileCachedParams.RslowPp.RsProfileSectionParams.RslowCompSocC2ch    = (UINT32 ) ((FgProfile.Pd[FgProfileSelected].addrData[FG_SRAM_RSLOW_SOC_C1_C2_ADRR_CNT].Data ) >> 16);
      ProfileCachedParams.RslowPp.RsProfileSectionParams.RsToRslowChargeFix  = (UINT32 ) ((FgProfile.Pd[FgProfileSelected].addrData[FG_SRAM_RSLOW_CHARGE_FIX_ADRR_CNT].Data ) >> 16);
      ProfileCachedParams.RslowPp.RsProfileSectionParams.RslowCompChFixTh    = (UINT32 ) ((FgProfile.Pd[FgProfileSelected].addrData[FG_SRAM_RSLOW_TH_ADRR_CNT].Data ) & 0xFF);

      /*Cache Battery Profile Configs */
      ProfileCachedParams.RslowPp.RsProfileConfigs.RslowCompSocC1ch    = FgProfile.Pd[FgProfileSelected].Pp.RslowPp.RsProfileConfigs.RslowCompSocC1ch ;
      ProfileCachedParams.RslowPp.RsProfileConfigs.RslowCompSocC2ch    = FgProfile.Pd[FgProfileSelected].Pp.RslowPp.RsProfileConfigs.RslowCompSocC2ch;
      ProfileCachedParams.RslowPp.RsProfileConfigs.RsToRslowChargeFix  = FgProfile.Pd[FgProfileSelected].Pp.RslowPp.RsProfileConfigs.RsToRslowChargeFix;
      ProfileCachedParams.RslowPp.RsProfileConfigs.RslowCompChFixTh    = FgProfile.Pd[FgProfileSelected].Pp.RslowPp.RsProfileConfigs.RslowCompChFixTh;
      if(ProfileCachedParams.CurrProfileRev >= FG_SRAM_PROFILE_REV_APPLY_RSLOW_WA)
      {
        PMIC_DEBUG((EFI_D_ERROR, "Profile Rev = (%d) Rslow WorkAround will be applied \r\n", ProfileCachedParams.CurrProfileRev));
        ProfileCachedParams.RslowPp.RslowWorkaround = TRUE;
        pm_smbchg_usb_chgpth_irq_set_trigger(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_USBID_CHANGE_INTR, PM_IRQ_TRIGGER_RISING_EDGE);
        pm_smbchg_usb_chgpth_irq_enable(PmicDeviceIndex, PM_SMBCHG_USB_CHGPTH_USBID_CHANGE_INTR, TRUE);
        pm_fg_soc_irq_set_trigger(PmicDeviceIndex, PM_FG_SOC_DELTA_SOC_INT, PM_IRQ_TRIGGER_RISING_EDGE);
        pm_fg_soc_irq_enable(PmicDeviceIndex, PM_FG_SOC_DELTA_SOC_INT, TRUE);
      }
      else
      {
        PMIC_DEBUG((EFI_D_ERROR, "Profile Rev = (%d) Rslow WA NOT be applied (0x%x) \r\n", ProfileCachedParams.CurrProfileRev, FgProfile.Pd[FgProfileSelected].addrData[FG_SRAM_PROFILE_REV_ADDR].Data));
        ProfileCachedParams.RslowPp.RslowWorkaround = FALSE;
      }
    }
    else
    {
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: Error in Battery Profile File Read \r\n"));
    }

  }

  /* Release not required data/memory */
  if(NULL != FgProfile.Pd[0].addrData)
  {
    /* Free allocated profile */
    for(Count = 0; (Count < MAX_NUM_OF_BATT_PROFILE) && (NULL != FgProfile.Pd[Count].addrData); Count++)
    {
      Status |= gBS->FreePool(FgProfile.Pd[Count].addrData);
      FgProfile.Pd[Count].addrData = NULL;
    }
    for(Count = 0; (Count < MAX_NUM_OF_BATT_PROFILE) && (NULL != FgProfile.Pd[Count].Pp.BatteryName); Count++)
    {
      Status |= gBS->FreePool(FgProfile.Pd[Count].Pp.BatteryName);
      FgProfile.Pd[Count].Pp.BatteryName = NULL;
    }
  }

  return Status;
}


/*
Get  Profile Params for flashed profile
  */
EFI_STATUS PmicFgBattProfileGetProfileParams(OUT FgBattProfileParams *Pp)
{
  if (!Pp)
  return EFI_INVALID_PARAMETER;

  *Pp = ProfileCachedParams;

return EFI_SUCCESS;
}

/* Utils functions can be made common part of QCOM LIB of seperate file in pmic uefi */

/**
  PmicFgBattProfileAsciiToInt

  @brief
  Converts ASCII string to integer
**/
INT32 PmicFgBattProfileAsciiToInt
(
  CHAR8 *Str
)
{
    INT32 Value = 0;
    int   Sign  = 1;

    if(Str == NULL)
        return 0;

    if(*Str == '-')
    {
        Sign = -1;
        Str++;
    }

    while(*Str != NULL_CHAR)
    {
        if(*Str >= '0' && *Str <= '9')
        {
            Value = ((Value * 10) + (*Str - '0'));
        }
        Str++;
    }

    return (Value * Sign);
}

/**
  EFI_PmicBattProfileStrCat

  @brief
  Concatenates strings
**/
EFI_STATUS EFI_PmicBattProfileStrCat
(
  char *Dest,
  char *Src
)
{
    EFI_STATUS Status = EFI_SUCCESS;

    if((Src == NULL)||(Dest == NULL))
    {
        Status = EFI_INVALID_PARAMETER;
    }

    if (Status == EFI_SUCCESS)
    {
       while(*Dest != NULL_CHAR)
       {
           Dest++;
       }

       while(*Src != NULL_CHAR)
       {
           *Dest = *Src;
           Src++;
           Dest++;
       }
       *Dest = NULL_CHAR;
    }

    return Status;
}

/**
  EFI_PmicBattProfileStrToUpper

  @brief
  Converts string to Uppercase
**/
VOID EFI_PmicBattProfileStrToUpper
(
  char *Str,
  UINT32 StrSize
)
{

    while((*Str != NULL_CHAR) && (StrSize))
    {
        if(*Str >= 'a' && *Str <= 'z')
        {
            *Str = *Str - ('a' - 'A');
        }

        Str++;
        StrSize--;
    }

}

/**
  EFI_PmicBattProfileCmpArray

  @brief
  Compares received signature with desired one
**/
BOOLEAN EFI_PmicBattProfileCmpArray(UINT32 *ArrAddr)
{
    UINT32 *StartAddr = DesiredSignature;
    int count = 0;

    while(count < 4)
    {
        if(*StartAddr == *ArrAddr)
        {
            count++;
            StartAddr++;
            ArrAddr++;

            continue;
        }
        else
        {
            return FALSE;
        }
    }

    return TRUE;
}

UINT32 PmicFgBattProfileAsciiStrToHex (CHAR8* Str, UINT32 StrSize)
{
  UINT32 result = 0;
  if(Str == NULL)
    return 0;

  while( (*Str!=0) && StrSize )
  {
    // Break as soon as non-hex char encountered.
    if (!((*Str >= '0' && *Str <= '9') ||
          (*Str >= 'a' && *Str <= 'f') ||
          (*Str >= 'A' && *Str <= 'F')))
      {
        result = 0;
        break;
      }
    result = result << 4;
    if(*Str >= '0' && *Str <= '9')
      result = result + (*Str - '0');
    if(*Str >= 'a' && *Str <= 'f')
      result = result + (*Str - 'a') + 10;
    if(*Str >= 'A' && *Str <= 'F')
      result = result + (*Str - 'A') + 10;

    StrSize--;
    Str++;
  }

  return result;
}


EFI_STATUS PmicUefiVariable(UINT16 *variable, UINTN* Value, PmUefiVariableOpType OpType)
{
  EFI_VARIABLESERVICES_PROTOCOL *VariableServicesProtocol = NULL;
  EFI_STATUS Status = EFI_SUCCESS;
  UINTN   DataSize = sizeof(*Value);
  UINT32  Attributes = (EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS);

  if (NULL == variable || NULL == Value)
  {
    return EFI_INVALID_PARAMETER;
  }

  if (UEFI_VARIABLE_OP_SET == OpType)
  {
    Status = gRT->SetVariable(variable, &gQcomTokenSpaceGuid, Attributes, DataSize, Value);

    gBS->LocateProtocol(&gEfiVariableServicesProtocolGuid, NULL, (VOID **)&VariableServicesProtocol);

    if (VariableServicesProtocol != NULL)
    {
      VariableServicesProtocol->FlushVariableNV(VariableServicesProtocol);
    }
  }
  else
  {
    Status = gRT->GetVariable(variable, &gQcomTokenSpaceGuid, NULL, &DataSize, Value);
  }

  return Status;
}

/**
  Duplicates ASCII string

  @param  Source  Null Terminated string of characters
  @param  Length  source String length
  @return  Duplicated String  if successful otherwise return NULL

  note: Caller responsibility to Free returned String
**/
static CHAR8 * AsciiStrNDup(
  CONST CHAR8               *Source,
  UINTN                     Length
)
{
  CHAR8 *Dest = NULL;
  if(NULL == Source || 0 == Length)
    return NULL;

  Dest = AllocatePool (Length + 1);
  if (NULL == Dest)
  {
    return NULL;
  }

  AsciiStrnCpy(Dest, Source, Length + 1);

  return Dest;
}
/* End of Util function */


