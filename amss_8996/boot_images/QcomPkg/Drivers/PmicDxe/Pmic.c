/* @file Pmic.c

  Implements the Pmic protocol
  
  Copyright (c) 2011-2013 Qualcomm Technologies, Inc.  All Rights Reserved. 
  Qualcomm Technologies Proprietary and Confidential.
*/


/*=============================================================================
                              EDIT HISTORY


 when         who     what, where, why
 --------     ---     -----------------------------------------------------------
 11/12/14     al      Adding core specific DAL initialization
 10/07/13     al      Moving platform config to target
 05/06/13     al      Storing pmic info and FabId
 04/01/13     sm      Added check for BattChgApp UEFI Variable
 03/07/13     dy      Add shutdown path for Liquid if pon trigger is charger
 11/06/12     dy      Add SMB350 Workaround
 10/25/12     al      Moved protocol installation to targets
 09/07/12     al      Adding PMIC SMBB Protocol
 09/07/12     al      Adding PMIC CLK BUFF Protocol
 08/23/12     al      Removing Charger and ChargerTask related
 07/27/12     al      Removed device index from pm_init
 07/27/12     al      Commented out not regquired for 8974 and compiling for 8960
 05/18/12     al      Added BMS Protocol
 05/14/12     al      Added Low Current Led Protocol
 05/10/12     dy      Configure TCXO settings only for V3 Liquid (not V3.2.1)
 04/11/12     dy      Shutdown device if USB was PWRON trigger for Liquid
 03/26/12     dy      Add PMIC PWRON Protocol
 02/22/12     dy      Added check for V3 Liquid to configure TCXO
 02/15/12     sm      Added PMIC WLED Protocol
 02/13/12     sm      Added PMIC Vibrator Protocol
 01/31/12     sm      Removed PMIC BMS Protocol
 01/14/12     sm      Added PMIC BMS Protocol
 12/19/11     sm      Added PMIC Mega XO Protocol
 11/29/11     sm      Added PMIC Interrupt Protocol
 06/28/11     dy      Add Pmic Charger Protocol
 05/19/11     dy      Add Keypad, MPP, GPIO, VREG, LPG Protcol
 04/28/11     dy      Add ADC Protocol
 03/25/11     dy      Add RTC Protocol
 03/03/11     dy      New File

=============================================================================*/


#include <Uefi.h>
#include <Library/UefiLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/QcomLib.h>

#include "DALDeviceId.h"
#include "DALFramework.h"


#include "pm_uefi.h"

#define ARRAY_LENGTH(x)  (sizeof(x) / sizeof(x[0]))
/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/
extern DALREG_DriverInfo DALSystem_DriverInfo;

static StringDevice gaDALModDriverList[] =
{
   {"/pmic/target", 0, 0, &DALSystem_DriverInfo, 0, NULL},
   {"/pmic/common", 0, 0, &DALSystem_DriverInfo, 0, NULL},
};


static DALSYSConfig gPmicDALSYSModConfig =
{
   // string device info
   {ARRAY_LENGTH(gaDALModDriverList), gaDALModDriverList},
   //numeric device info
   {0,NULL}
};



/**
  PmicInitialize ()

  @brief
  Init implementation of EFI_QCOM_PMIC_CLIENT_DEVICE_PROTOCOL
 */
EFI_STATUS
PmicInitialize (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable)
{
  EFI_STATUS Status = EFI_SUCCESS;
  
  /*initialize DAL*/
  DALSYS_InitMod(&gPmicDALSYSModConfig);

  // Initialize the PMIC Library  
  if (pm_init() == PM_ERR_FLAG__SUCCESS)
  {
    Status = pm_install_uefi_protocol(ImageHandle,SystemTable);
  }
  else
  {
    Status = EFI_DEVICE_ERROR;
  }

  return Status;
}

