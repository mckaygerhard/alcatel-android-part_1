/** @file
*  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
*  Portions Copyright (c) 2011-2013, ARM Limited. All rights  Reserved.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution.  The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
*
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 04/23/15   bh      Fix KW errors
 03/05/15   bh      Set ValidOption back to FALSE if a handle is not found
 01/04/15   bh      Fix BootOrder issue
 12/11/14   bh      Fix KW errors
 12/09/14   bh      Add support for USB boot through signaling for USB host mode toggle
 11/21/14   bh      Enumerate all media after BootOrder fail (MSFT hack)
 11/12/14   bh      Fix FreeListEntries function
 11/03/14   bh      Added POST Time output, authentication checks
 10/20/14   bh      Fix PcdFirmwareVendor to be widechar when adding to ST
 10/24/14   bh      Initial branch from ArmPkg with edits

=============================================================================*/

#include "QcomBdsInternal.h"
#include <Library/PlatformBdsLib.h>
#include <Library/GenericBdsLib.h>
#include <Library/PcdLib.h>
#include <Library/PerformanceLib.h>
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/BlockIo.h>
#include <Protocol/Bds.h>
#include <Library/QcomLib.h>
#include <Library/QcomBaseLib.h>
#include <Protocol/EFIUsbConfig.h>
#include <Protocol/PciIo.h>
#include <Protocol/UsbIo.h>

#define BOOT_REMOVABLE_FIRST      TRUE
#define BOOT_NON_REMOVABLE_FIRST  FALSE
#define ENUMERATE_AND_BOOT        TRUE
#define ONLY_ENUMERATE            FALSE

EFI_HANDLE  mImageHandle;
UINT8       ConnectControllersDone;
extern VOID DisplayPOSTTime( VOID );

VOID FreeListEntries(LIST_ENTRY * );

typedef enum {
  RemovableMediaOnly = 0,
  NonRemovableMediaOnly,
  AllMediaTypes
} ENUMERATION_FILTER;

#ifdef SIGNAL_TO_ENABLE_HOST_MODE
extern EFI_GUID gEfiEventToggleUsbModeGuid;

/* Dummy function needed for event notification callback */
VOID DummyNotify (IN EFI_EVENT Event,IN VOID *Context) 
{ 
  ; 
}

/* Enables USB host mode by signaling the corresponding event */
VOID
EnableUsbHostMode(VOID) 
{
  EFI_STATUS Status; 
  QCOM_USB_CONFIG_PROTOCOL *QcUsbConfig = NULL;
  EFI_EVENT ToggleUsbModeEvt;

  //Locate the configuration protocol, only needed to confirm driver is present
  Status = gBS->LocateProtocol(
      &gQcomUsbConfigProtocolGuid,
      NULL,
      (VOID **) &QcUsbConfig
      );
  if (EFI_ERROR(Status)) 
    DEBUG((EFI_D_ERROR, "Failed to locate USB config protocol: %r", Status));

  Status = gBS->CreateEventEx(
      EVT_NOTIFY_SIGNAL,
      TPL_CALLBACK,
      DummyNotify,
      NULL,
      &gEfiEventToggleUsbModeGuid,
      &ToggleUsbModeEvt
      );

  if (EFI_ERROR(Status)) 
    DEBUG((EFI_D_ERROR, "Event not signalled: %r", Status));
  else {
    gBS->SignalEvent(ToggleUsbModeEvt);
    gBS->CloseEvent(ToggleUsbModeEvt);
  }
}
#endif

/* Check if a device path corresponds to a USB Class or WWID device path 
   
   @param    DevicePath     Device path that is checked for USB dependency
   @return   TRUE 			If the DevicePathType and DevicePathSubType matches
             FALSE			Otherwise
*/
BOOLEAN
BdsIsUsbDevicePath (
  IN  EFI_DEVICE_PATH*  DevicePath
  )
{
  return ((DevicePathType (DevicePath) == MESSAGING_DEVICE_PATH) &&
          ((DevicePathSubType (DevicePath) == MSG_USB_CLASS_DP) ||
           (DevicePathSubType (DevicePath) == MSG_USB_WWID_DP) ||
           (DevicePathSubType (DevicePath) == MSG_USB_DP)));
}

/**
  Removes a Boot Option from the BootOptionsList, 
  and deletes from BootOrder if the Option is invalid

  @param     BootOptionsList     Linked list of boot options
**/
VOID
RemoveOptionFromBootOrder (LIST_ENTRY * BootOptionsList, LIST_ENTRY * Entry)
{
  DEBUG((EFI_D_WARN, "Removing from BootOrder\n"));
  BDS_LOAD_OPTION_ENTRY *BootOptionEntry;

  BootOptionEntry = LOAD_OPTION_ENTRY_FROM_LINK(Entry);

  if (!IsListEmpty(&BootOptionEntry->Link)) {
    RemoveEntryList(&BootOptionEntry->Link);
  }
  //Following line for persistent boot variables only 
  BootOptionDelete(BootOptionEntry->BdsLoadOption);
}

/* Puts the actual device path into the BootOption instead of stock USB class device path 
   
   @param     BootOption     The BootOption that has a Usb Class or WWID generic device path
   @return    EFI_SUCCESS    A valid USB device path is found and put into BootOption
              EFI_NOT_FOUND  No valid USB device is connected
 * */
EFI_STATUS
GetValidUsbDevicePath(BDS_LOAD_OPTION * BootOption){
  EFI_STATUS                 Status;
  UINTN                      HandleCount;
  EFI_HANDLE                *HandleBuffer;
  UINTN                      Index;
  EFI_DEVICE_PATH_PROTOCOL  *DevicePath;
  UINT8                      DevicePathSet;
  EFI_HANDLE                 Handle;

  DevicePath = NULL;
  DevicePathSet = FALSE;
#ifdef SIGNAL_TO_ENABLE_HOST_MODE
  EnableUsbHostMode();
#endif
  
  if (!ConnectControllersDone) {
    BdsConnectAllDrivers();
    ConnectControllersDone = TRUE;
#ifdef SIGNAL_TO_ENABLE_HOST_MODE 
    WaitForTimeout(5000, 0);
#else
    WaitForTimeout(2000, 0);
#endif 
  }

  Status = gBS->LocateHandleBuffer (ByProtocol, &gEfiUsbIoProtocolGuid, NULL, &HandleCount, &HandleBuffer);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  for (Index = 0; Index < HandleCount; Index++) {
    DevicePath = DevicePathFromHandle(HandleBuffer[Index]);
    if (DevicePath == NULL) {
      DEBUG((EFI_D_WARN, "Could not get device path \n"));
      continue;
    }
    DEBUG((EFI_D_INFO, "Handle DevPath: %s\n", DevicePathToStr(DevicePath)));
    Handle = BdsLibGetBootableHandle(DevicePath);
    if (Handle == NULL){
      DEBUG((EFI_D_ERROR, "Could not find bootable handle\n"));
      continue;
    }
    DEBUG((EFI_D_ERROR, "USB path found\n"));
    BootOption->FilePathList = DevicePath;
    BootOption->FilePathListLength = GetUnalignedDevicePathSize (DevicePath);
    DevicePathSet = TRUE;
    break;
  }
  DEBUG((EFI_D_INFO, "BootOption DevPath: %s\n", DevicePathToStr(BootOption->FilePathList)));
  if (DevicePathSet)
    return EFI_SUCCESS;
  else 
    return EFI_NOT_FOUND;
}

/*
  This function goes through the BootOrderList and
  tries to boot each option. 

  @param    BootOrderList    The populated list that contains boot options
  @return   EFI_NOT_FOUND    When the BootOrderList does not have any valid options
            EFI_ABORTED      All of the options in BootOrderList returned
*/
EFI_STATUS
BootFromBootOptionList (LIST_ENTRY * BootOrderList)
{
  BDS_LOAD_OPTION*                  BootOption;
  LIST_ENTRY*                       Entry;
  BOOLEAN                           ValidOption;
  UINTN                             BootOrderIndex;
  EFI_STATUS                        Status;

  Status = EFI_NOT_FOUND;
  ValidOption = FALSE;
  BootOrderIndex = 0;
 
  for (Entry = GetFirstNode(BootOrderList);
       !IsNull (BootOrderList, Entry);
       Entry = GetNextNode(BootOrderList, Entry) )
  {
    BootOption = LOAD_OPTION_FROM_LINK(Entry);
   
    if (!BdsLibIsValidEFIBootOptDevicePath(BootOption->FilePathList, TRUE)){
      DEBUG ((EFI_D_ERROR, "Invalid device path\n"));
      RemoveOptionFromBootOrder(BootOrderList, Entry);
      continue;
    }
    DEBUG ((EFI_D_WARN, "Booting option %d:(Boot%04X) \"%s\"\n", BootOrderIndex, BootOption->LoadOptionIndex, BootOption->Description));
    ValidOption = TRUE;
    EfiSignalEventReadyToBoot();
    DisplayPOSTTime();
    if (BdsIsUsbDevicePath(BootOption->FilePathList)){
      Status = GetValidUsbDevicePath(BootOption);
      if (EFI_ERROR(Status)) {
        DEBUG((EFI_D_WARN, "USB device not found\n"));
        continue;
      }
    }
    Status = BootOptionStart(BootOption);
    DEBUG((EFI_D_INFO, "Boot option returned: %x\n", Status));
    if (Status == EFI_SECURITY_VIOLATION)
      DEBUG((EFI_D_ERROR, "Authentication failed\n"));
    if (Status == EFI_NOT_FOUND)
      ValidOption = FALSE;

    BootOrderIndex++;
  }

  if (!ValidOption)
  {
    DEBUG((EFI_D_WARN, "No valid options found in list\n"));
    return EFI_NOT_FOUND;
  }
  return EFI_ABORTED;
}

/*
  This function enumerates boot options based on parameters
  and adds valid ones to BootOptionsList 

  @param   BootOptionsList    Head of boot option linked list to populate
  @param   Filter             Filters type of Block IO handles to consider
  @param   AddRemovableFirst  Specifies the order in which we attempt to boot options

  @return  EFI_SUCCESS        Enumeration was successful, at least one option was found
  @return  EFI_NOT_FOUND      No bootable options were found during enumeration
*/
EFI_STATUS
EnumerateOptionsAndAdd (
   LIST_ENTRY* BootOptionsList, 
   ENUMERATION_FILTER Filter, 
   BOOLEAN AddRemovableFirst
   )
{
  EFI_STATUS                        Status;
  UINTN                             HandleCount;
  EFI_HANDLE                        *HandleBuffer;
  UINTN                             Index;
  EFI_DEVICE_PATH_PROTOCOL          *DevicePath;
  EFI_BLOCK_IO_PROTOCOL             *BlkIo;
  CHAR16                            BootDescription[BOOT_DEVICE_DESCRIPTION_MAX];
  UINT32                            Attributes;
  BDS_LOAD_OPTION_ENTRY             *BdsLoadOptionEntry;
  ARM_BDS_LOADER_TYPE               BootType;
  BOOLEAN                           LoadedOption;
  
  if (!ConnectControllersDone)
    BdsConnectAllDrivers(); 
 
  if (Filter == AllMediaTypes)
    DEBUG((EFI_D_WARN, "Enumerating all boot options\n"));
  else if (Filter == RemovableMediaOnly)
    DEBUG((EFI_D_WARN, "Enumerating removable boot options\n"));
  else if (Filter == NonRemovableMediaOnly)
    DEBUG((EFI_D_WARN, "Enumerating non-removable boot options\n"));
  
  Attributes = 0;
  BootType = BDS_LOADER_EFI_APPLICATION;
  Status = EFI_NOT_FOUND;
  LoadedOption = FALSE;

  // List all the Simple File System Protocols
  Status = gBS->LocateHandleBuffer (ByProtocol, &gEfiSimpleFileSystemProtocolGuid, NULL, &HandleCount, &HandleBuffer);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  for (Index = 0; Index < HandleCount; Index++) {
    Status = gBS->HandleProtocol (
                      HandleBuffer[Index],
                      &gEfiBlockIoProtocolGuid,
                      (VOID **) &BlkIo
                      );
    if ((Filter == RemovableMediaOnly) && (!BlkIo->Media->RemovableMedia))
    {
      //only want to re-enumerate removable media 
      continue;
    }
    else if ((Filter == NonRemovableMediaOnly) && BlkIo->Media->RemovableMedia) 
    {
      //only want to re-enumerate non-removable media
      continue;
    }
    DevicePath = DevicePathFromHandle(HandleBuffer[Index]);
    if (DevicePath == NULL) {
      DEBUG((EFI_D_ERROR, "Could not get device path\n"));
      continue;
    }
    if (BdsLibIsValidEFIBootOptDevicePath(DevicePath, TRUE))
    {
      if(BlkIo->Media->RemovableMedia)
      {
        UnicodeSPrint(BootDescription, BOOT_DEVICE_DESCRIPTION_MAX, L"Removable Media");
      }
      else {
        UnicodeSPrint(BootDescription, BOOT_DEVICE_DESCRIPTION_MAX, L"Non-removable Media");
      }

      BdsLoadOptionEntry = (BDS_LOAD_OPTION_ENTRY*) AllocatePool(sizeof(BDS_LOAD_OPTION_ENTRY));
      if (BdsLoadOptionEntry == NULL) {
        DEBUG((EFI_D_ERROR, "Could not allocate resource for boot option\n"));
        continue;
      }
      Status = BootOptionCreate(Attributes, BootDescription, DevicePath, BootType, NULL, &BdsLoadOptionEntry->BdsLoadOption);
      if (!EFI_ERROR(Status))
      {
        DEBUG((EFI_D_WARN, "Adding new boot/driver option Boot%04X, Description: %s\n", BdsLoadOptionEntry->BdsLoadOption->LoadOptionIndex ,BdsLoadOptionEntry->BdsLoadOption->Description));
        LoadedOption = TRUE;
        if (BlkIo->Media->RemovableMedia == AddRemovableFirst)
        {
          InsertHeadList(BootOptionsList, &BdsLoadOptionEntry->Link);
          //only for persistent boot variables 
#ifdef ENABLE_ENUMERATED_BOOTORDER_SAVING
          BdsLibUpdateBootOrder(HandleBuffer[Index]);
#endif
        }
        else {
          InsertTailList(BootOptionsList, &BdsLoadOptionEntry->Link);
        }
      }
    }
  }

  if (LoadedOption){
    return EFI_SUCCESS;
  }
  else {
    return EFI_NOT_FOUND;
  }
}

/* 
  Updates the Firmware Vendor in 
  the System Table and fixes CRC
*/
STATIC
EFI_STATUS
UpdateFWVendorInST(VOID)
{
  UINTN       Size;
  EFI_STATUS  Status;
  //
  // Declare the Firmware Vendor
  //
  if (FixedPcdGetPtr(PcdFirmwareVendor) != NULL) {
    Size = 0x100;
    gST->FirmwareVendor = AllocateRuntimePool (Size);
    ASSERT (gST->FirmwareVendor != NULL);
    if (gST->FirmwareVendor == NULL) {
      DEBUG((EFI_D_ERROR, "Could not update Firmware Vendor in System Table\n"));
      return EFI_OUT_OF_RESOURCES;
    }
    UnicodeSPrint (gST->FirmwareVendor, Size, L"%S", PcdGetPtr(PcdFirmwareVendor));
  }

  //
  // Fixup Table CRC after we updated Firmware Vendor
  //
  gST->Hdr.CRC32 = 0;
  Status = gBS->CalculateCrc32 ((VOID*)gST, gST->Hdr.HeaderSize, &gST->Hdr.CRC32);
  return Status;
}


/*Enumerates only removable media and attempts to boot */
STATIC
VOID AttemptBootFromRemovable(VOID)
{  
  LIST_ENTRY BootKeyOptionList; 
  DEBUG((EFI_D_WARN, "Removable boot key detected\n"));
#ifdef SIGNAL_TO_ENABLE_HOST_MODE
  EnableUsbHostMode();
#endif

  BdsConnectAllDrivers();
  ConnectControllersDone = TRUE;

#ifdef SIGNAL_TO_ENABLE_HOST_MODE
  WaitForTimeout(5000, 0);
#else
  WaitForTimeout(2000, 0);
#endif  
  
  InitializeListHead (&BootKeyOptionList);
  EnumerateOptionsAndAdd(&BootKeyOptionList, RemovableMediaOnly, BOOT_REMOVABLE_FIRST);
  BootFromBootOptionList(&BootKeyOptionList);
  FreeListEntries(&BootKeyOptionList);
}

/* Checks the BootNext variable and tries to boot said option */
STATIC
VOID  AttemptBootNext(VOID)
{
  UINT16    *BootNext;
  UINTN      BootNextSize;
  CHAR16     BootVariableName[9];
  EFI_STATUS Status;
  
  // If BootNext environment variable is defined then we just load it !
  BootNextSize = sizeof(UINT16);
  Status = GetGlobalEnvironmentVariable (L"BootNext", NULL, &BootNextSize, (VOID**)&BootNext);
  if (!EFI_ERROR(Status) && (BootNext != NULL)) {
    ASSERT(BootNextSize == sizeof(UINT16));

    // Generate the requested Boot Entry variable name
    UnicodeSPrint (BootVariableName, 9 * sizeof(CHAR16), L"Boot%04X", *BootNext);

    // Set BootCurrent variable
    gRT->SetVariable (L"BootCurrent", &gEfiGlobalVariableGuid,
              EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
              BootNextSize, BootNext);

    FreePool (BootNext);

    // Start the requested Boot Entry
    Status = BdsLaunchBootOption (BootVariableName);
    if (Status != EFI_NOT_FOUND) {
      // BootNext has not been succeeded launched
      if (EFI_ERROR(Status)) {
        DEBUG((EFI_D_WARN, "Fail to start BootNext.\n"));
      }

      // Delete the BootNext environment variable
      gRT->SetVariable (L"BootNext", &gEfiGlobalVariableGuid,
          EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
          0, NULL);
    }

    // Clear BootCurrent variable
    gRT->SetVariable (L"BootCurrent", &gEfiGlobalVariableGuid,
        EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
        0, NULL);
  }
}

/**
  Frees the memory used by the list after it is no longer needed

  @param BootList   The list that needs to be cleared. 

**/
VOID FreeListEntries(LIST_ENTRY * BootList){
  LIST_ENTRY *Entry;
  LIST_ENTRY *TempEntry;
  
  if (BootList == NULL)
    return;
  
  Entry = GetFirstNode(BootList);
  while (!IsListEmpty(BootList) && !IsNull (BootList, Entry)) 
  {
    TempEntry = Entry;
    Entry = RemoveEntryList(Entry);
    FreePool(TempEntry);
  }  
}

/**
  This function goes through the following boot rules to choose an option
  to load. 
 
  RULES FOR BOOTING IN BDS
  Volume Down hotkey pressed:
   - Enumerate and boot from removable media
  BootOrder variable is not set:
    - Enumerate all boot options
    - Boot removable media first, then non-removable
  BootOrder variable is set 
    Has valid options:
     - Try booting those options
    NO valid options: 
     - Enumerate all boot options
     - Boot removable media first, then non-removable
    Applications fail authentication or otherwise return
     - Enumerate only removable media options and boot if possible
  
  Enumeration finds no options
   - Drop into Shell in Non-prod mode, then ask user for input to shut down

  BootOrder variable is not set, only deleted when invalid

  @param  This             The EFI_BDS_ARCH_PROTOCOL instance.

  @return None.

**/
VOID
EFIAPI
BdsEntry (
  IN EFI_BDS_ARCH_PROTOCOL  *This
  )
{
  EFI_STATUS          Status;
  LIST_ENTRY          BootOptionsList, RemovableOptionsList;
  BDS_INIT_OPTION     InitOption;
  
  ConnectControllersDone = FALSE;

  PERF_END   (NULL, "DXE", NULL, 0);

  Status = UpdateFWVendorInST();
  ASSERT_EFI_ERROR(Status);

  PlatformBdsInitEx(&InitOption);

  if (InitOption == BootFromRemovableMedia)
    AttemptBootFromRemovable();

  AttemptBootNext();

  /* This just blindly copies the bootorder variables into the List structure */
  Status = BuildBootOptionListFromBootOrder(&BootOptionsList);

  if (EFI_ERROR(Status))
  {
    DEBUG((EFI_D_WARN, "No bootable option found\n"));

    /* Enumerate the possible boot options that can be added to bootorder, but don't store them */
    Status = EnumerateOptionsAndAdd(&BootOptionsList, AllMediaTypes, BOOT_REMOVABLE_FIRST);
    if (EFI_ERROR(Status))
    {
      DEBUG((EFI_D_WARN, "Enumeration failed\n"));
      PlatformBdsBootHalt();
    }
  }
  
  /* 
     First attempt to boot from BootOptionsList
     Either populated by BootOrder, or by enumeration
  */
  Status = BootFromBootOptionList(&BootOptionsList);
   
  //BootOptionsList has no valid options
  if (Status == EFI_NOT_FOUND)
  {
    /* This case is valid only when we attempt to boot using BootOrder variables only
     * When enumerated this case shouldn't exist, since that option will not event make it */
    DEBUG((EFI_D_WARN, "No valid bootable option found\n"));

    /* Enumerate the possible boot options that can be added to bootorder, but don't store them */
    Status = EnumerateOptionsAndAdd(&BootOptionsList, AllMediaTypes, BOOT_REMOVABLE_FIRST);
    if (EFI_ERROR(Status))
    {
      DEBUG((EFI_D_WARN, "Enumeration failed\n"));
      PlatformBdsBootHalt();
    }
    Status = BootFromBootOptionList(&BootOptionsList);

    if (EFI_ERROR(Status))
      DEBUG((EFI_D_WARN, "Failed booting enumerated option\n"));
  }
  else if (Status == EFI_ABORTED)
  {
    //Application failed authentication or crashed
    //only look for options on removable media
    DEBUG((EFI_D_WARN, "All boot options returned\n"));
    InitializeListHead(&RemovableOptionsList);
    Status = EnumerateOptionsAndAdd(&RemovableOptionsList, RemovableMediaOnly, BOOT_REMOVABLE_FIRST);

    Status = BootFromBootOptionList(&RemovableOptionsList);
    if (EFI_ERROR(Status))
      DEBUG((EFI_D_WARN, "Failed to start removable options\n"));
  }
  FreeListEntries(&BootOptionsList);
  FreeListEntries(&RemovableOptionsList);
  /* Nothing worked above, halt the device */
  PlatformBdsBootHalt();

  ASSERT_EFI_ERROR (Status);
}

EFI_BDS_ARCH_PROTOCOL  gBdsProtocol = {
  BdsEntry,
};

/* Initializes BDS driver */
EFI_STATUS
EFIAPI
BdsInitialize (
  IN EFI_HANDLE                            ImageHandle,
  IN EFI_SYSTEM_TABLE                      *SystemTable
  )
{
  EFI_STATUS  Status;

  mImageHandle = ImageHandle;

  Status = gBS->InstallMultipleProtocolInterfaces (
                  &ImageHandle,
                  &gEfiBdsArchProtocolGuid, &gBdsProtocol,
                  NULL
                  );
  ASSERT_EFI_ERROR (Status);

  return Status;
}
