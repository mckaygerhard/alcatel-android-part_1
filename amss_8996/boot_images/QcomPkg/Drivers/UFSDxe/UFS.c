/**
* @file     UFS.c
* @brief   Universal Flash Storage (UFS) Dxe Driver 
*
*  Copyright (c) 2013 - 2015, Qualcomm Technologies, Inc. All rights reserved.
*/


/*=============================================================================
                              EDIT HISTORY


when            who   what, where, why
----------      ---   -----------------------------------------------------------
2015-05-13      jb    Change device path type name to not conflict with standard one
2015-04-03      rm    Add check for boot device
2015-03-26      rm    Add RPMB protocols  
10-09-2014      rh    Adding RPMB APIs
09-23-2014      rh    Use unique GUID for LUs, different from eMMC
05-05-2013      ai    Initial version

=============================================================================*/

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UncachedMemoryAllocationLib.h>
#include <Library/ArmLib.h>
#include <Library/SerialPortShLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/BlockIo.h>
#include <Protocol/DevicePath.h>
#include <Protocol/EFICardInfo.h>
#include <Protocol/EFIClock.h>
#include <Protocol/EFIHWIO.h>

#include <Library/GPTListener.h>
#include <Library/RpmbListener.h>
#include <Library/RpmbLib.h>
#include <Protocol/EFIRpmb.h>

// TODO: Remove me
#include "TimetickUEFI.h"

#include "UFS.h"
#include <api/storage/ufs_api.h>

#include <BootConfig.h>

/* RPMB transfer limit */
#define RPMB_MAX_BLOCK_TRANSFER_SIZE 0xffff

/* UFS Device Path */
typedef struct {
   VENDOR_DEVICE_PATH  Ufs;
   EFI_DEVICE_PATH     End;
} QCUFS_DEVICE_PATH;

static QCUFS_DEVICE_PATH gUfsDevicePath [UFS_UEFI_MAX_LUN_ACCESS]; 

static VENDOR_DEVICE_PATH UfsPath = 
{ 
   HARDWARE_DEVICE_PATH,
   HW_VENDOR_DP,
   (UINT8)(sizeof(VENDOR_DEVICE_PATH)),
   (UINT8)((sizeof(VENDOR_DEVICE_PATH)) >> 8),
   0 
};

static EFI_DEVICE_PATH UfsEndPath = 
{ 
   END_DEVICE_PATH_TYPE,
   END_ENTIRE_DEVICE_PATH_SUBTYPE,
   sizeof (EFI_DEVICE_PATH_PROTOCOL),
   0
}; 

#define _UFS_DEV_PROTOTYPE    { UFS_DEV_SIGNATURE, NULL, NULL, 0 }
static UFS_DEV UfsDevice[UFS_UEFI_MAX_LUN_ACCESS] = {
   _UFS_DEV_PROTOTYPE,
   _UFS_DEV_PROTOTYPE,
   _UFS_DEV_PROTOTYPE,
   _UFS_DEV_PROTOTYPE,
   _UFS_DEV_PROTOTYPE,
   _UFS_DEV_PROTOTYPE,
   _UFS_DEV_PROTOTYPE,
   _UFS_DEV_PROTOTYPE
}; 

/* UFS Block IO's Media */
static EFI_BLOCK_IO_MEDIA UFSMedia = {
   SIGNATURE_32('u','f','s',' '),            /* MediaId */
   FALSE,                                    /* RemovableMedia */
   FALSE,                                    /* MediaPresent */
   FALSE,                                    /* LogicalPartition */
   FALSE,                                    /* ReadOnly */
   FALSE,                                    /* WriteCaching */
   4096,                                     /* BlockSize */
   8,                                        /* IoAlign */
   0,                                        /* LastBlock */
   0,                                        /* LowestAlignedLba */
   0,                                        /* LogicalBlocksPerPhysicalBlock */
   0                                         /* OptimalTransferLengthGranularity */  
}; 

static EFI_BLOCK_IO_MEDIA gMediaUfs[UFS_UEFI_MAX_LUN_ACCESS]; 

/* UFS Block IO Protocol */
EFI_BLOCK_IO_PROTOCOL gBlockIoUfs = {
   EFI_BLOCK_IO_PROTOCOL_REVISION3,          /* Revision */
   0,                                        /* Media */
   UFSReset,                                 /* Reset */
   UFSReadBlocks,                            /* ReadBlocks */
   UFSWriteBlocks,                           /* WriteBlocks */
   UFSFlushBlocks                            /* FlushBlocks */
};

/* Device Paths for all the UFS LUNs */
static EFI_GUID UfsDevicePathGuids[UFS_UEFI_MAX_LUN_ACCESS] = {
   EFI_UFS_LUN_0_GUID,
   EFI_UFS_LUN_1_GUID,
   EFI_UFS_LUN_2_GUID,
   EFI_UFS_LUN_3_GUID,
   EFI_UFS_LUN_4_GUID,
   EFI_UFS_LUN_5_GUID,
   EFI_UFS_LUN_6_GUID,
   EFI_UFS_LUN_7_GUID
}; 

/* RPMB Protocol */
static const EFI_SDCC_RPMB_PROTOCOL gUfsRpmbProtocol = {
   EFI_RPMB_PROTOCOL_REVISION,               /* Revision */
   0,                                        /* Reliable Write Count */
   0,                                        /* RPMB Partition Size */
   RPMBReadCounterPkt,                       /* Read Counter*/ 
   RPMBProgProvisionKey,                     /* Program Provision Key */
   RPMBReadBlocks,                           /* Read Sectors */
   RPMBWriteBlocks,                          /* Write Sectors */
   0                                         /* Max RMPB Read/Write Transfer Size */
};

static UFS_DEV UfsRpmbDevice = _UFS_DEV_PROTOTYPE;

static QCUFS_DEVICE_PATH gUfsRpmbDevicePath;

/* SCM registration event */
static EFI_EVENT ScmInitialized = NULL; 
static VOID *ScmInitializedToken = NULL;
extern EFI_GUID gQcomScmProtocolGuid;

/**
   Reset the Block Device.

**/
EFI_STATUS
EFIAPI
UFSReset (
   IN EFI_BLOCK_IO_PROTOCOL   *This,
   IN BOOLEAN                 ExtendedVerification
   )
{
   return EFI_SUCCESS; 
}

/**
   Read BufferSize bytes from Lba into Buffer. 

**/
EFI_STATUS
EFIAPI
UFSReadBlocks (
  IN EFI_BLOCK_IO_PROTOCOL   *This,
  IN UINT32                  MediaId,
  IN EFI_LBA                 Lba,
  IN UINTN                   BufferSize,
  OUT VOID                   *Buffer
  )
{
   UINT32  rc; 
   UFS_DEV    *UfsDevice;
   struct ufs_handle *hUFS;
   UINT32 transfer_size = 0, block_count = 0, block_size = 0;
   UINT8 *temp_buffer;

   /* Validate passed-in pointers are not NULL */
   if ((NULL == Buffer) || (NULL == This) || (NULL == This->Media))
   {
      return EFI_INVALID_PARAMETER;
   }

   /* Validate if buffer is multiple of block size */
   if ((BufferSize%(This->Media->BlockSize)) != 0 )
   {
      return EFI_BAD_BUFFER_SIZE;
   }

   /* Validate block size and access range */
   if ((Lba > This->Media->LastBlock) || (This->Media->BlockSize == 0)) 
   {
      return EFI_INVALID_PARAMETER;
   }
   
   if (This->Media->MediaId != MediaId)
   {
      return EFI_MEDIA_CHANGED;
   }

   UfsDevice = UFS_DEV_FROM_BLOCKIO (This);
   hUFS = (struct ufs_handle *) UfsDevice->DeviceHandle;       
   
   /* If BufferSize is zero, the host is pinging the card for its presence */
   if (0 == BufferSize)
   {
      if (!hUFS)
      {
         (void) ufs_close(hUFS);       
         UfsDevice->DeviceHandle = NULL;   
         UfsDevice->BlkIo.Media->MediaPresent = FALSE;        
         return EFI_NO_MEDIA;
      }
      else
      {
         return EFI_SUCCESS;
      }
   }

   block_size = UfsDevice->BlkIo.Media->BlockSize;

   block_count = (BufferSize/block_size);

   /* Check if LBA plus the total sectors trying to access would exceed the */
   /* total size of the partition */
   if ((Lba + (BufferSize/block_size)) > (UfsDevice->BlkIo.Media->LastBlock + 1))
   {
      return EFI_INVALID_PARAMETER;
   }

   /* Break transfer into smaller transfers to UFS_MAX_BLOCK_TRANSFER_SIZE transfer size */
   temp_buffer = (UINT8 *)Buffer;

   while (block_count > 0)
   {
      transfer_size = (block_count > UFS_MAX_BLOCK_TRANSFERS) ? 
         UFS_MAX_BLOCK_TRANSFERS : block_count;

      rc = ufs_read( hUFS, temp_buffer, Lba, transfer_size);

      Lba = Lba + transfer_size;
      block_count = block_count - transfer_size;
      temp_buffer = temp_buffer + (transfer_size * block_size);
   
      if (UFS_EOK != rc)
      {
         /* There's an error */
         DEBUG ((EFI_D_ERROR, "UFS READ with return value of %d\n", rc));
         //DEBUG_DEAD_LOOP("UFSDXE_Read: Attach JTAG Now!\n");

         return EFI_DEVICE_ERROR;
         
      }
   }
   
   return EFI_SUCCESS;
}

/**
   Write BufferSize bytes from Lba into Buffer. 

**/
EFI_STATUS
EFIAPI
UFSWriteBlocks (
   IN EFI_BLOCK_IO_PROTOCOL   *This,
   IN UINT32                  MediaId,
   IN EFI_LBA                 Lba,
   IN UINTN                   BufferSize,
   IN VOID                    *Buffer
   )
{
   UINT32  rc;   
   UFS_DEV    *UfsDevice;
   struct ufs_handle *hUFS;
   UINT32 transfer_size = 0, block_count = 0, block_size = 0;
   UINT8 *temp_buffer;

   /* Validate passed-in pointers are not NULL */
   if ((NULL == Buffer) || (NULL == This) || (NULL == This->Media))
   {
      return EFI_INVALID_PARAMETER;
   }

   /* Validate if buffer is multiple of block size */   
   if (((BufferSize%(This->Media->BlockSize)) != 0))
   {
      return EFI_BAD_BUFFER_SIZE;
   }

   /* Validate block size and access range */
   if ((Lba > This->Media->LastBlock) || (This->Media->BlockSize == 0)) 
   {
      return EFI_INVALID_PARAMETER;
   }
   
   /* Validate if LBA plus the total sectors trying to access would exceed the */
   /* total size of the partition */
   if ((Lba + (BufferSize/This->Media->BlockSize)) > (This->Media->LastBlock + 1))
   {
      return EFI_INVALID_PARAMETER;
   }
     
   if (This->Media->MediaId != MediaId)
   {
      return EFI_MEDIA_CHANGED;
   }

   UfsDevice = UFS_DEV_FROM_BLOCKIO (This);

   hUFS = UfsDevice->DeviceHandle;
   
   if (NULL == hUFS)
   {
      return EFI_INVALID_PARAMETER;
   }
  
   /* Break transfer into smaller transfers to UFS_MAX_BLOCK_TRANSFER_SIZE transfer size */
   temp_buffer = (UINT8 *)Buffer;

   block_size = UfsDevice->BlkIo.Media->BlockSize;

   block_count = (BufferSize/block_size);

   while (block_count > 0)
   {
     transfer_size = (block_count > UFS_MAX_BLOCK_TRANSFERS) ? 
                         UFS_MAX_BLOCK_TRANSFERS : block_count;

     rc = ufs_write( hUFS, temp_buffer, Lba, transfer_size);

     Lba = Lba + transfer_size;
     block_count = block_count - transfer_size;
     temp_buffer = temp_buffer + (transfer_size * block_size);
   
     if (UFS_EOK != rc)
     {
        /* There's an error */
        DEBUG ((EFI_D_ERROR, "UFS WRITE with return value of %d\n", rc));
        //DEBUG_DEAD_LOOP("UFSDXE_Write: Attach JTAG Now!\n");
     
        return EFI_DEVICE_ERROR;
     }
   }
   
   return EFI_SUCCESS;
}


/**
   Flush the Block Device.   

**/
EFI_STATUS
EFIAPI
UFSFlushBlocks (
   IN EFI_BLOCK_IO_PROTOCOL  *This
   )
{
   return EFI_SUCCESS;
}

VOID
EFIAPI
RegisterListener (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
   EFI_STATUS EfiStatus = EFI_UNSUPPORTED; 

   EfiStatus = RPMBListenerInit((UINT32) Context); 
   if (EFI_SUCCESS != EfiStatus)
   {
      DEBUG ((EFI_D_ERROR, "Failed to initialize RPMB Listener, Status 0x%08x\n", EfiStatus));
   }

   EfiStatus = GPTListenerInit(); 
   if (EFI_SUCCESS != EfiStatus)
   {
      DEBUG ((EFI_D_ERROR, "Failed to initialize GPT Listener, Status 0x%08x\n", EfiStatus));
   }

   EfiStatus = InitPartitionConfig();
   if (EFI_SUCCESS != EfiStatus)
   {
      DEBUG ((EFI_D_ERROR, "Failed to initialize Partition config, Status 0x%08x\n", EfiStatus));
   }  
}

/**
   Driver initializatoin entry point.

**/
EFI_STATUS
EFIAPI
UFSDxeInitialize (
   IN EFI_HANDLE         ImageHandle,
   IN EFI_SYSTEM_TABLE   *SystemTable
   )
{
   EFI_STATUS Status;
   UINT32 lun = 0;
   UINT32 lun_enabled_mask = 0;
   UINT8  total_luns_enabled = 0;
   INT32 rc = 0;
   struct ufs_info_type ufs_info;
   struct ufs_handle *ufs_device;

   /* Don't do anything if it doesn't boot from UFS */   
   if (!boot_from_ufs ())
   {
     return EFI_SUCCESS;
   }
   
// TODO: Remove Timer Profiling Code
#if 0
   {
      Timetick_timer_Type Timer;
      UINT32 call_count = 0;
      UINT64 starting_tick = 0, ending_tick = 0, total_ticks = 0;       
      UINT32 TimetickFreq = 0;
      UINT64 uSeconds = 0;
 
      if(PcdGet32(PcdQTimerEnabled))
      {
        Timer = TIMETICK_QTIMER;
      }
      else
      {
        Timer = TIMETICK_TIMER_ADGT;
      }
 
      /* Enable TimeTick for collecting performance data */
      Timetick_Init();
      Timetick_Enable(Timer, TRUE);
      
      Timetick_GetCount(Timer, &starting_tick);
      //Delay of 5s
      for (call_count=0; call_count<5000; call_count++)
      {
        ufs_osal_stall_us(1);
      }       
      Timetick_GetCount(Timer, &ending_tick);
      total_ticks = ending_tick - starting_tick;

      /* Convert ticks to micro seconds */
      Timetick_GetFreq(Timer, &TimetickFreq);
      uSeconds = MultU64x32(total_ticks, 1000000);
      uSeconds = DivU64x32(uSeconds, TimetickFreq);  
      DEBUG ((EFI_D_ERROR, "uSeconds = %ld\n", uSeconds));
   }
#endif
   
   /* Create the RegisterListener callback */
   Status = gBS->CreateEvent(EVT_NOTIFY_SIGNAL,
                             TPL_CALLBACK,
                             RegisterListener,
                             (VOID *)SIGNATURE_32('u','f','s',' '),
                             &ScmInitialized
                             );
   ASSERT_EFI_ERROR (Status); 
   
   Status = gBS->RegisterProtocolNotify(&gQcomScmProtocolGuid,
                                        ScmInitialized, 
                                        (VOID *)&ScmInitializedToken); 

   ASSERT_EFI_ERROR(Status);
   
   /* Initialize UFS */
   ufs_device = ufs_open (0, UFS_WLUN_DEVICE);
   if (!ufs_device)
      ASSERT_EFI_ERROR(EFI_DEVICE_ERROR);

   /* Get valid LUNs */
   rc = ufs_get_device_info (ufs_device, &ufs_info); 
   if (UFS_EOK != rc)
      ASSERT_EFI_ERROR(EFI_DEVICE_ERROR);
   lun_enabled_mask = ufs_info.dLunEnabled;
   total_luns_enabled = ufs_info.bNumberLu;
  
   for (lun = 0; lun < total_luns_enabled; lun++)
   { 
      // Check if LUN is enabled
      if (!(lun_enabled_mask & ((UINT32) 1) << lun))
         continue;

      gUfsDevicePath[lun].Ufs = UfsPath; 
      gUfsDevicePath[lun].Ufs.Guid = UfsDevicePathGuids[lun]; 
      gUfsDevicePath[lun].End = UfsEndPath;
      
      UfsDevice[lun].Signature    = UFS_DEV_SIGNATURE;
      UfsDevice[lun].BlkIo       = gBlockIoUfs;
      UfsDevice[lun].BlkIo.Media = &gMediaUfs[lun];
      CopyMem(UfsDevice[lun].BlkIo.Media, 
                 &UFSMedia, sizeof(EFI_BLOCK_IO_MEDIA));  

      /* Initialize LUN */
      UfsDevice[lun].DeviceHandle = ufs_open (0, lun);
      if (!UfsDevice[lun].DeviceHandle) {
         UfsDevice[lun].BlkIo.Media->MediaPresent = FALSE;
         continue;
      }
      UfsDevice[lun].BlkIo.Media->MediaPresent = TRUE;
      rc = ufs_get_device_info ((struct ufs_handle *)UfsDevice[lun].DeviceHandle, &ufs_info); 
      if (UFS_EOK != rc)
         ASSERT_EFI_ERROR(EFI_DEVICE_ERROR);
      UfsDevice[lun].BlkIo.Media->LastBlock = ufs_info.dLuTotalBlocks - 1;

      /* Assume LUN0 and install the following protocols: */ 
      /* BlkIO */
      Status = gBS->InstallMultipleProtocolInterfaces (
         &UfsDevice[lun].ClientHandle,  
         &gEfiBlockIoProtocolGuid, &UfsDevice[lun].BlkIo, 
         &gEfiDevicePathProtocolGuid, &gUfsDevicePath[lun],
         NULL
      );
      ASSERT_EFI_ERROR(Status);
   }
   
   gUfsRpmbDevicePath.Ufs = UfsPath; 
   gUfsRpmbDevicePath.Ufs.Guid = (EFI_GUID)EFI_UFS_RPMB_LUN_GUID;
   gUfsRpmbDevicePath.End = UfsEndPath;
   
   UfsRpmbDevice.RPMB = gUfsRpmbProtocol;
  
   UfsRpmbDevice.DeviceHandle = ufs_open (0, UFS_WLUN_RPMB);
   if (!UfsRpmbDevice.DeviceHandle) {
      DEBUG ((EFI_D_ERROR, "Cannot open UFS RPMB!\n"));
   }   
   rc = ufs_get_device_info ((struct ufs_handle *)UfsRpmbDevice.DeviceHandle, &ufs_info); 
   if (UFS_EOK != rc)  ASSERT_EFI_ERROR(EFI_DEVICE_ERROR);
   
   UfsRpmbDevice.RPMB.RPMBMaxTransferSize = ufs_info.qLogicalBlockCount * 256;
   UfsRpmbDevice.RPMB.RPMBPartitionSizeInBytes = ufs_info.qLogicalBlockCount * 256;
   UfsRpmbDevice.RPMB.ReliableWriteCount = ufs_info.bRPMB_ReadWriteSize;
   
   Status = gBS->InstallMultipleProtocolInterfaces (
      &UfsRpmbDevice.ClientHandle,  
      &gEfiSdccRpmbProtocolGuid, &UfsRpmbDevice.RPMB,
      &gEfiDevicePathProtocolGuid, &gUfsRpmbDevicePath,
      NULL
   );  

   /* Allocate memory for RPMB listener */
   Status = RPMBListenerAllocMem();
   if(Status != EFI_SUCCESS)
   {
      DEBUG ((EFI_D_ERROR, "Failed to allocate memory for RPMB listener\n"));
      return Status; 
   }

   /* Allocate memory for GPT listener */
   Status = GPTListenerAllocMem();
   if(Status != EFI_SUCCESS)
   {
      DEBUG ((EFI_D_ERROR, "Failed to allocate memory for GPT listener\n"));
      return Status; 
   }   
   
   return EFI_SUCCESS;
}



/**
   Function: RPMBReadCounterPkt
 
   This function retrieves Read Counter Response packet from flash device.
   The Response packet includes the Write Counter as well as the MAC which
   is used later to validate the Response packet
  
   @param  RespPktBuffer [OUT]    Pointer to the response from the Read
                                  Counter command
    
   @retval  RPMB_NO_ERROR           Successfully read the Counter packet.
   @retval  RPMB_ERR_INVALID_PARAM  NULL pointer is passed in to the function.
   @retval  RPMB_ERR_READ           Failed to read the Counter packet.
**/
INT32 
EFIAPI
RPMBReadCounterPkt (
   EFI_SDCC_RPMB_PROTOCOL       *This,
   UINT32                       *RespPktBuffer
   )
{
   UFS_DEV    *UfsDevice;
   struct ufs_handle *hUFS;
   INT32  rc = 0;

   if ((NULL == This) || (NULL == RespPktBuffer))
   {
      return RPMB_ERR_INVALID_PARAM;
   }
   
   UfsDevice = UFS_DEV_FROM_RPMB (This);
   hUFS = UfsDevice->DeviceHandle;
  
   if (NULL == hUFS) 
   {
      return RPMB_ERR_INVALID_PARAM;
   }
 
   rc = ufs_rpmb_read_cnt_pkt(hUFS, (UINT8 *)RespPktBuffer);

   if (UFS_EOK != rc)
   {
      return RPMB_ERR_READ;
   }

   return RPMB_NO_ERROR;
}

/**
 Function: RPMBProgProvisionKey
 
 This function programs the eMMC's key using the provided packet
 formatted as ProvisionKey command. 

 @param CmdPktBuffer  [IN] : Pointer to a ProvisionKey command packet
                             to be sent to eMMC to program its key
 @param RespPktBuffer [OUT]: Pointer to the response packet for the
                             ProvisionKey command
 
 @retval  RPMB_NO_ERROR            Successfully provision the eMMC's Key.
 @retval  RPMB_ERR_INVALID_PARAM   NULL pointer is passed in to the function.
 @retval  RPMB_ERR_GENERAL         Failed to provision the key (Key has been
                                   provisioned).
  
**/
INT32 
EFIAPI
RPMBProgProvisionKey (
   EFI_SDCC_RPMB_PROTOCOL       *This,
   UINT32                       *CmdPktBuffer,
   UINT32                       *RespPktBuffer 
   )
{
   UFS_DEV    *UfsDevice;
   struct ufs_handle *hUFS = NULL;
   INT32  rc = UFS_EOK; 

   if ((NULL == CmdPktBuffer) || (NULL == RespPktBuffer) || (NULL == This)) 
   {
      return RPMB_ERR_INVALID_PARAM;
   }

   UfsDevice = UFS_DEV_FROM_RPMB (This);
   hUFS = (struct ufs_handle *) UfsDevice->DeviceHandle;       
   
   if (NULL == hUFS)
   {
      return RPMB_ERR_INVALID_PARAM;
   }

   rc = ufs_rpmb_write_blocks(hUFS, (UINT8 *)CmdPktBuffer, 
                              1, (UINT8 *)RespPktBuffer);

   if (UFS_EOK != rc)
   {
      return RPMB_ERR_GENERAL;
   }

   return RPMB_NO_ERROR;
}

/**
  Function: RPMBReadBlocks
 
  This function reads the sectors from RPMB partition using the
  cmd packet buffers provided.
 
  @param   CmdPktBuffer  [IN] : Pointer to a formatted packet for Read Request 
  @param   PktCount      [IN] : How many half sectors to read   
  @param   DataBuffer    [OUT]: Pointer to data read from eMMC
 
  @retval  RPMB_NO_ERROR            Successfully read the Counter packet
  @retval  RPMB_ERR_INVALID_PARAM   NULL pointer is passed in to the function
  @retval  RPMB_ERR_READ            Failed to read the sectors
     
**/ 
INT32 
EFIAPI
RPMBReadBlocks (
   EFI_SDCC_RPMB_PROTOCOL       *This,
   UINT32                       *CmdPktBuffer,
   UINT32                       PktCount,
   UINT32                       *DataBuffer
   )
{
   UFS_DEV    *UfsDevice;
   struct ufs_handle *hUFS;
   INT32 rc = UFS_EOK;

   if ((NULL == CmdPktBuffer) || (NULL == DataBuffer) || 
       (NULL == This) || (PktCount == 0))
   {
      return RPMB_ERR_INVALID_PARAM;
   }
   
   if (PktCount > RPMB_MAX_BLOCK_TRANSFER_SIZE)
   {
      return RPMB_ERR_INVALID_PARAM;
   }
   
   UfsDevice = UFS_DEV_FROM_RPMB (This);
   hUFS = (struct ufs_handle *) UfsDevice->DeviceHandle;       
   
   if (NULL == hUFS)
   {
      return RPMB_ERR_INVALID_PARAM;
   }

   rc = ufs_rpmb_read_blocks(hUFS, (UINT8 *)CmdPktBuffer, 
                             PktCount, (UINT8 *)DataBuffer); 
   if (UFS_EOK != rc)
   {
      return RPMB_ERR_READ;
   }
     
   return RPMB_NO_ERROR;

}

/**
  Function: RPMBWriteBlocks
 
  This function writes the sectors to the RPMB partition using
  the provided cmd packet buffers
                       
  @param  CmdPktBuffer  [IN] :  Pointer to a formatted packets to be sent
                                to eMMC to write to RPMB partition
  @param  PktCount      [IN] :  Number of half sectors to write
  @param  RespPktBuffer [OUT]:  Pointer to the response packet from eMMC
 
  @retval  RPMB_NO_ERROR            Successfully Write the RPMB sectors.
                                    Caller should authenticate the Response
                                    packet and validate the Write Counter
  @retval  RPMB_ERR_INVALID_PARAM   NULL pointer is passed in to the function
  @retval  RPMB_ERR_WRITE           Failed to write the sectors
  
**/
INT32 
EFIAPI
RPMBWriteBlocks (
   EFI_SDCC_RPMB_PROTOCOL       *This,
   UINT32                       *CmdPktBuffer,
   UINT32                       PktCount,
   UINT32                       *RespPktBuffer 
   )
{
   UFS_DEV    *UfsDevice;
   struct ufs_handle *hUFS = NULL;
   INT32  rc = UFS_EOK;

   if ((NULL == CmdPktBuffer) || (NULL == RespPktBuffer) || 
      (NULL == This) || (PktCount == 0)) 
   {
      return RPMB_ERR_INVALID_PARAM;
   }

   if (PktCount > RPMB_MAX_BLOCK_TRANSFER_SIZE)
   {
      return RPMB_ERR_INVALID_PARAM;
   }

   UfsDevice = UFS_DEV_FROM_RPMB (This);
   hUFS = (struct ufs_handle *) UfsDevice->DeviceHandle;       
   
   if (NULL == hUFS)
   {
      return RPMB_ERR_INVALID_PARAM;
   }

   rc = ufs_rpmb_write_blocks(hUFS, (UINT8 *)CmdPktBuffer, 
                              PktCount, (UINT8 *)RespPktBuffer);

   if (UFS_EOK != rc)
   {
      return RPMB_ERR_WRITE;
   }

   return RPMB_NO_ERROR;

}

