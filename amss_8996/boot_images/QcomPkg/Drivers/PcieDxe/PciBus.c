/** @file PciBus.c
   
  Contains PCIe bus entry point.

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
   
**/
/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 05/13/15   tselvam  Unified code for multi platforms
 01/26/15   tselvam  Initial checkin for 8996
 06/16/14   hozefak  Add PcieInit protocol
 05/01/14   hozefak  Add code for ExitBootServiceEvent
 04/18/14   hozefak  Initial checkin for 8994  

=============================================================================*/
#include <Library/PcieConfigLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include "Protocol/EFIPCIeInit.h"



//
// PCI Bus Driver Global Variables
//
/*===========================================================================*/
/*                  FUNCTIONS PROTOTYPES                                     */
/*===========================================================================*/
EFI_STATUS PCIeInitHardware(IN QCOM_PCIE_PROTOCOL *This);
UINT32 EFIAPI PCIe_Config_Read_32(IN PCIe_RP_IDX rpIndex, IN UINT32 offset);
EFI_STATUS PCIeDeInitHardware(void);

// Pcie protocol implementation
QCOM_PCIE_PROTOCOL QcomPcieInit = 
{
  PCIeInitHardware,
  PCIe_Config_Read_32,
  PCIeDeInitHardware
};

/**
  The Entry Point for PCI Bus module. The user code starts with this function.

  Calls into PCIe_Enable

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.
  @param[in] SystemTable    A pointer to the EFI System Table.

  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurred when executing this entry point.

**/
EFI_STATUS
EFIAPI
PciBusEntryPoint (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS  Status;

  Status = gBS->InstallProtocolInterface (
                &ImageHandle,
                &gQcomPcieInitProtocolGuid,
                EFI_NATIVE_INTERFACE,
                &QcomPcieInit
                );

  Status = PCIe_Read_Config();
  return Status;
}

EFI_STATUS PCIeInitHardware(IN QCOM_PCIE_PROTOCOL *This)
{
  EFI_STATUS  Status;
  Status = PCIe_Enable();
  if EFI_ERROR (Status) 
  {
    PCIe_Deinitialize();
  }
  return Status;
}

UINT32
EFIAPI
PCIe_Config_Read_32(
		IN PCIe_RP_IDX rpIndex,
		IN UINT32 offset)
{
  return PCIeLib_Config_Read_32 (rpIndex, offset);
}

EFI_STATUS PCIeDeInitHardware(void)
{
  EFI_STATUS  Status = EFI_SUCCESS;

  Status = PCIe_Deinitialize();

  return (Status);
}
