/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_closed_loop.h"
#include "CoreVerify.h"
#include <string.h>
#include "cpr_image.h"
#include "cpr_enablement.h"
#include "cpr_voltage_ranges.h"
#include <stdlib.h>
#include "cpr_smem.h"
#include "stringl/stringl.h"

cpr_closed_loop_rail_t cpr_closed_loop_rail_root = NULL;


#define CPR_DEFAULT_GCNT 19     //We always use 19 since we always run the CPR Ref Clk at 19.2MHz

static const cpr_closed_loop_rail_config_t* cpr_closed_loop_rail_config(cpr_rail_id_t rail_id)
{
    for(int i=0; i<cpr_bsp_closed_loop_config.closed_loop_config_count; i++)
    {
        const cpr_closed_loop_rail_config_t* rail_config = cpr_bsp_closed_loop_config.closed_loop_config[i];
        if(rail_id == rail_config->rail_id)
        {
            return rail_config;
        }
    }
    CORE_VERIFY(0); //Couldn't find the necessary config.
    return NULL;
}

const cpr_corner_params_t* cpr_find_corner_params(cpr_closed_loop_rail_t rail, cpr_voltage_mode_t voltage_mode)
{
    for(int i=0; i<rail->target_params_count; i++)
    {
        const cpr_corner_params_t* corner_params = &rail->target_params[i];
        if(voltage_mode == corner_params->voltage_mode)
        {
            return corner_params;
        }
    }
    return NULL;
}

static cpr_closed_loop_rail_t cpr_closed_loop_alloc_node(cpr_rail_id_t rail_id, uint32 voltage_level_count)
{
    cpr_closed_loop_rail_t closed_loop_rail = (cpr_closed_loop_rail_t)cpr_image_malloc(sizeof(cpr_closed_loop_rail_s));
    closed_loop_rail->next_link = cpr_closed_loop_rail_root;
    cpr_closed_loop_rail_root = closed_loop_rail;
    
    closed_loop_rail->rail_id = rail_id;
    closed_loop_rail->rail_config = cpr_closed_loop_rail_config(rail_id);
    closed_loop_rail->voltage_control_handle = cpr_image_alloc_voltage_control_handle(rail_id);
    closed_loop_rail->image_rail_state = cpr_image_alloc_image_rail_state();
    
    closed_loop_rail->target_params_count = voltage_level_count;
    closed_loop_rail->target_params = (cpr_corner_params_t*)cpr_image_malloc(sizeof(cpr_corner_params_t)*closed_loop_rail->target_params_count);
    CORE_VERIFY_PTR(closed_loop_rail->target_params);
    
    const cpr_config_versioned_voltage_ranges_t* voltage_ranges = cpr_config_find_versioned_voltage_ranges(rail_id);
    CORE_VERIFY_PTR(voltage_ranges);
    closed_loop_rail->pmic_step_size = voltage_ranges->pmic_step_size;

    return closed_loop_rail;
}

static cpr_closed_loop_rail_t cpr_closed_loop_find_node(cpr_rail_id_t rail_id)
{
    cpr_closed_loop_rail_t closed_loop_rail = cpr_closed_loop_rail_root;
    while(closed_loop_rail)
    {
        if(closed_loop_rail->rail_id == rail_id)
        {
            break;
        }
        closed_loop_rail = closed_loop_rail->next_link;
    }
    return closed_loop_rail;
}

void cpr_closed_loop_init_default_voltage_levels(cpr_closed_loop_rail_t closed_loop_rail, const cpr_enablement_versioned_rail_config_t* rail_enablement_config)
{
    const cpr_rail_id_t rail_id = closed_loop_rail->rail_id;
    for(int i=0; i<rail_enablement_config->supported_level_count; i++)
    {
        const cpr_enablement_supported_level_t* level_info = &rail_enablement_config->supported_level[i];
        cpr_voltage_mode_t voltage_mode = level_info->voltage_mode;

        uint32 open_loop_voltage = cpr_config_open_loop_voltage(rail_id, voltage_mode);
        
        //Always start from the Open-Loop voltage.
        cpr_image_set_rail_mode_voltage(closed_loop_rail->voltage_control_handle, voltage_mode, open_loop_voltage);
    }
}
/*===========================================================================
FUNCTION: cpr_calc_aging

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
int32 cpr_calc_aging(cpr_closed_loop_rail_t rail, uint32 aging_scaling_factor)
{
    CORE_VERIFY_PTR(rail->rail_config);
    int32 aging_margin_limit = rail->rail_config->aging_margin_limit;
    int32 aging_ro_kv_x100 = rail->rail_config->aging_ro_kv_x100;
    
    // check aging_ro_kv_x100 is not 0
    CORE_VERIFY(aging_ro_kv_x100);
    
    // ( max_age_delta * 100 / aging_kv_ro_x100 )
    int32 aging_margin_added = ( rail->max_aging_delta * 100) / aging_ro_kv_x100;
    aging_margin_added = aging_margin_added * aging_scaling_factor;
    
    // limit to aging_margin_limit
    aging_margin_added = MIN(aging_margin_limit, aging_margin_added);
    
    return aging_margin_added;
}


void cpr_enable_init_fn_closed_loop(cpr_rail_id_t rail_id, const cpr_enablement_versioned_rail_config_t* rail_enablement_config, cpr_enablement_stage_t enablement_stage)
{
    cpr_closed_loop_rail_t closed_loop_rail = cpr_closed_loop_find_node(rail_id);

    if(enablement_stage == CPR_ENABLEMENT_STAGE_INIT_INITAL_VOLTAGE_CONFIG)
    {
        //Sanity check that we've not already alloced a node.
        CORE_VERIFY(!closed_loop_rail);
        closed_loop_rail = cpr_closed_loop_alloc_node(rail_id, rail_enablement_config->supported_level_count);

        cpr_closed_loop_init_default_voltage_levels(closed_loop_rail, rail_enablement_config);
        return;
    }
    
    //Sanity check that we could find the rail node.
    CORE_VERIFY_PTR(closed_loop_rail);

  //run CPR aging calculations
  cpr_aging_poll(closed_loop_rail);
    
  //Init target quotients, floor and ceiling voltages.
  for(int i=0; i<rail_enablement_config->supported_level_count; i++)
  {
    const cpr_enablement_supported_level_t* level_info = &rail_enablement_config->supported_level[i];
    cpr_voltage_mode_t voltage_mode = level_info->voltage_mode;
    closed_loop_rail->target_params[i].voltage_mode = voltage_mode;
    cpr_corner_params_t* corner_target_params = &closed_loop_rail->target_params[i];
    
    // calc aging margin
    int32 aging_margin_mv = 0;
    if(closed_loop_rail->rail_config->aging_sensor_count != 0)
    {
      aging_margin_mv = cpr_calc_aging(closed_loop_rail, level_info->aging_scaling_factor);
    }
        
    cpr_target_quotient_set_t* ro_targets = &corner_target_params->rosc_targets;

    int32 static_margin_mv = level_info->static_margin_mv;
    if (level_info->custom_static_margin_fn)
    {
      static_margin_mv = level_info->custom_static_margin_fn (rail_id, voltage_mode, static_margin_mv);
    }
    
    int32 effective_margin_mv = aging_margin_mv + static_margin_mv + cpr_offset_fuse_val(rail_id, voltage_mode);
    cpr_target_quotient_calculate_quotients(rail_id, voltage_mode, effective_margin_mv, ro_targets);

    if(rail_enablement_config->custom_voltage_fn)
    {
      corner_target_params->voltage_ceil  = rail_enablement_config->custom_voltage_fn(CPR_OPEN_LOOP_VOLTAGE, voltage_mode);
      corner_target_params->voltage_floor = rail_enablement_config->custom_voltage_fn(CPR_CLOSED_LOOP_FLOOR_VOLTAGE, voltage_mode);
    }
    else
    {
      corner_target_params->voltage_ceil = cpr_config_open_loop_voltage(rail_id, voltage_mode);
      corner_target_params->voltage_floor = cpr_config_closed_loop_floor_voltage(rail_id, voltage_mode);
    }
  }
    
    //Now do what's remaining with the HW init.
#ifdef CPR_CONFIG_NO_FEEDBACK_LOOP

    if(rail_enablement_config->mode_to_settle_count>0)
    {
        cpr_init_rail_hw(closed_loop_rail);

        cpr_settle_voltage(closed_loop_rail, rail_enablement_config);
    }

#else

    cpr_init_rail_hw(closed_loop_rail);
    if(rail_enablement_config->mode_to_settle_count>0)
    {
        cpr_settle_voltage(closed_loop_rail, rail_enablement_config);
    }

    cpr_enable_closed_loop_feedback(closed_loop_rail);

#endif
}

const cpr_enablement_init_params_t CPR_ENABLE_CLOSED_LOOP =
{
    .enablement_type        = CPR_ENABLEMENT_CLOSED_LOOP_ID,
    .enablement_fn          = cpr_enable_init_fn_closed_loop,
};

void cpr_closed_loop_init_default_voltages_from_smem(cpr_closed_loop_rail_t closed_loop_rail, const cpr_externalized_state_closed_loop_level_t* closed_loop_levels, uint32 closed_loop_level_count)
{
    for(int i=0; i<closed_loop_level_count; i++)
    {
        const cpr_externalized_state_closed_loop_level_t* closed_loop_level = &closed_loop_levels[i];
        
        // Set settled voltage from XBL.
        cpr_image_set_rail_mode_voltage(closed_loop_rail->voltage_control_handle, 
                closed_loop_level->corner_params.voltage_mode, closed_loop_level->voltage);
    }
}

void cpr_enable_init_fn_closed_loop_from_smem(cpr_rail_id_t rail_id, cpr_aging_info_t aging_info, const cpr_externalized_state_closed_loop_level_t* closed_loop_levels, 
        uint32 closed_loop_level_count, cpr_enablement_stage_t enablement_stage)
{
    cpr_closed_loop_rail_t closed_loop_rail = cpr_closed_loop_find_node(rail_id);
        
    if(enablement_stage == CPR_ENABLEMENT_STAGE_INIT_INITAL_VOLTAGE_CONFIG)
    {
        //Sanity check that we've not already alloced a node.
        CORE_VERIFY(!closed_loop_rail);
    
        closed_loop_rail = cpr_closed_loop_alloc_node(rail_id, closed_loop_level_count);

        cpr_closed_loop_init_default_voltages_from_smem(closed_loop_rail, closed_loop_levels, closed_loop_level_count);
        return;
    }

    //Sanity check that we could find the rail node.
    CORE_VERIFY_PTR(closed_loop_rail);
    
    //add aging info from boot into cpr_closed_loop_rail_root
    closed_loop_rail->max_aging_delta = aging_info.max_aging_delta;
    closed_loop_rail->age_max = aging_info.age_max;
    closed_loop_rail->age_min = aging_info.age_min;

    //Pull in target quotients, floors and ceilings from SMem.
    for(int i=0; i<closed_loop_level_count; i++)
    {
        const cpr_externalized_state_closed_loop_level_t* closed_loop_level = &closed_loop_levels[i];
        cpr_corner_params_t* corner_target_params = &closed_loop_rail->target_params[i];

        cpr_image_memscpy(corner_target_params, sizeof(cpr_corner_params_t), &closed_loop_level->corner_params, sizeof(cpr_corner_params_t));
    }
        
    cpr_init_rail_hw(closed_loop_rail);

    cpr_enable_closed_loop_feedback(closed_loop_rail);
}

/*===========================================================================
FUNCTION: rbcpr_prepare_for_sleep

DESCRIPTION: disables RBCPR clocks.

    FOR each rail that is enabled
        Disable CPR
        Turn off clocks

RETURN VALUE:
===========================================================================*/
void rbcpr_prepare_for_sleep(void)
{
    cpr_closed_loop_rail_t node = cpr_closed_loop_rail_root;
    
    while(node)
    {
        if(node->block_enabled)
        {
            cpr_pause_cpr(node);
        }
        node = node->next_link;
    }
}

/*===========================================================================
FUNCTION: rbcpr_exit_sleep

DESCRIPTION: enables RBCPR clocks.

    FOR each rail that was enabled
        Turn on clocks
        Enable CPR

RETURN VALUE:
===========================================================================*/
void rbcpr_exit_sleep(void)
{
    cpr_closed_loop_rail_t node = cpr_closed_loop_rail_root;
    
    while(node)
    {
        if(node->block_enabled)
        {
            uint32 current_voltage = cpr_image_get_rail_mode_voltage(node->voltage_control_handle, node->current_target_params->voltage_mode);
            cpr_prepare_cpr(node, node->current_target_params, current_voltage);
            cpr_restart_cpr(node);
        }
        node = node->next_link;
    }
}
