/**
 * @file comdef.h
 *
 * Common definition file defining standard types for SMEM, SMSM, and SMD
 * drivers on Windows on ARM.
 */

/*==============================================================================
     Copyright (c) 2011,2014 Qualcomm Technologies Incorporated. 
     All rights reserved.
     Qualcomm Confidential and Proprietary
==============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE


when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/03/14   vk      Remove stdlib
01/19/11   tl      Initial release of SMEM for WoA
===========================================================================*/

#ifndef COMDEF_H
#define COMDEF_H

/*===========================================================================
                                INCLUDE FILES
===========================================================================*/


/*=========================================================================== 
                              MACRO DEFINITIONS
===========================================================================*/
/** TODO : Ask UEFI team for a flag to export to differentiate UEFI builds */
#define FEATURE_UEFI

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/*=========================================================================== 
                               TYPE DEFINITIONS
===========================================================================*/

#include "com_dtypes.h"

#endif /* COMDEF_H */
