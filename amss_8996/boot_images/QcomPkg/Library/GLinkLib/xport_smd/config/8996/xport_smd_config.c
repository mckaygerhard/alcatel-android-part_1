/*===========================================================================

            GLink SMD transport 8996 UEFI Configuration Structures

=============================================================================

  @file
    xport_smd_config.c

    Contains structures to be used in Glink SMD trasnport configuration.

  Copyright (c) 2014 Qualcomm Technologies, Inc. 
  All rights reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE


when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/06/14   bm      Initial version for 8996 XBL
===========================================================================*/

/*===========================================================================
                        INCLUDE FILES
===========================================================================*/
#include "xport_smd_config.h"

/*===========================================================================
                        DATA DECLARATIONS
===========================================================================*/
const xport_smd_config_type xport_smd_config[] =
{
  /* Apps->rpm */
  {
    "rpm",                 /* Remote host name        */
    SMD_APPS_RPM           /* SMD compatible edge definition */
  }
};

const uint32 xport_smd_config_num = sizeof(xport_smd_config)/sizeof(xport_smd_config[0]);
const smem_host_type xport_smd_this_host = SMEM_APPS;


const xport_smd_config_type* xport_smd_get_config(uint32 ind)
{
  if (ind >= xport_smd_config_num)
  {
    return NULL;
  }

  return &xport_smd_config[ind];
}
