/*===========================================================================

              SMD transitional transport for Glink Driver Source File


  Copyright (c) 2014 by Qualcomm Technologies Incorporated.  
  All rights reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE


when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/26/14   bc      Channel migration update
12/09/14   db      Added channel migration specific code
11/20/14   an      Cleanup redundant core interface verification.
08/06/14   bm      Initial version.
===========================================================================*/


/*===========================================================================
                        INCLUDE FILES
===========================================================================*/

#include "smem_list.h"
#include "glink.h"
#include "glink_core_if.h"
#include "glink_transport_if.h"
#include "glink_os_utils.h"
#include "glink_internal.h"
#include "glink_vector.h"
#include "xport_smd_config.h"
#include "smd_lite.h"
#include "smd.h" /* for enable/disable smd interrupts */


/*===========================================================================
                           MACRO DEFINITIONS
===========================================================================*/
#define XPORT_SMD_NAME "SMD"

#define SMD_XPORT_VERSION  1
#define SMD_XPORT_FEATURES 0

#define XPORT_SMD_LOG(msg, remote_host, param) \
          GLINK_LOG_EVENT(GLINK_EVENT_XPORT_INTERNAL, msg, XPORT_SMD_NAME, \
            remote_host, (uint32)param)

#define ROUNDUP64(d) ((d + 7) & (~7))

#define XPORT_SMD_NUM_EVENTS 100

/* macros for extracing information from channel
 * migration command */
#define XPORT_SMD_GET_CMD(buffer) (*(uint32 *)buffer)
/*===========================================================================
                              GLOBAL DATA DECLARATIONS
===========================================================================*/
extern const uint32 xport_smd_config_num;
extern const smem_host_type xport_smd_this_host;

xport_smd_config_type* xport_smd_get_config(uint32 ind);
#ifdef FEATURE_SMD_XPORT_TASK
typedef struct _xport_smd_thread_ctx_type {
  os_event_type   event;   /* os event which gets signalled */
  smem_list_type        event_q; /* list of queued events */
  smem_list_type        event_free_q; /* list of free events */
  os_cs_type      event_q_cs; /* lock to prevent access to event list */
} xport_smd_thread_ctx_type;

xport_smd_thread_ctx_type xport_smd_thread_ctx;
#endif //FEATURE_SMD_XPORT_TASK

typedef enum _xport_smd_cmd
{
  XPORT_SMD_CH_MIGRATION_CMD,
  XPORT_SMD_CH_MIGRATION_DONE_CMD,
  XPORT_SMD_CH_CLOSE_CMD,
  XPORT_SMD_CH_CLOSE_CMD_ACK
} xport_smd_cmd;

/* SMD transport context */
typedef struct _xport_smd_ctx_type
{
  /* context structure should start from trasport interface */
  glink_transport_if_type         xport_if;
  
  /* Target specific configuration */
  const xport_smd_config_type     *pcfg;
  
  /* List of open SMD channels */
  smem_list_type                  smd_open_channel_q;

  /* Lock for the SMD open channel list */
  os_cs_type                      smd_open_channel_q_cs;

  /* Intent ID's for faking remote side's intents */
  uint32                          iid;

  /* rcid to be used when remote side is not xport smd */
  uint32                          max_rcid;

  /* check if control channel for migration is opened */  
  boolean                         xport_smd_cntl_ch_open;

  /* smd_lite handle for control channel */
  smdl_handle_type                xport_smd_cntl_ch_handle;

  /* Store channel migration commands for transmitting later */
  smdl_iovec_type                 *cmd_iovec;

  /* Store received channel migration command */
  smdl_iovec_type                 *cmd_iovec_rx;  
} xport_smd_ctx_type;

typedef struct _xport_smd_intent_type
{
  /* Link needed for use with list APIs.  Must be at the head of the struct */
  smem_list_link_type           link;
  
  /* stored intent id */
  uint32                        iid;

  /* Intent's size */
  size_t                        size;
}xport_smd_intent_type;

typedef struct _xport_smd_ch_ctx_type
{
  /* Link needed for use with list APIs.  Must be at the head of the struct */
  smem_list_link_type           link;
  
  /* SMDL handle for this channel */
  smdl_handle_type              smd_handle;
  
  /* Local intent ID associated with this channel */
  uint32                        lcid;

  /* Remote intent ID associated with this channel */
  uint32                        rcid;

  /* Store channel name */
  char                          name[32];

  /* xport capabilities */
  uint32                        xport_capabilities;

  /* Current local signal state */
  uint32                        curr_local_sigs;
  
  /* previous remote signal state */
  uint32                        prev_remote_sigs;

  /* List of rx intents associated with this channel */
  smem_list_type                rx_intent_q;
  smem_list_type                rx_intent_in_use_q;
  os_cs_type                    rx_intent_q_cs;

  /* Lock to synchonize read operations happening in SMD thread / client thread
   * when it re-queues the rx_intent */
  os_cs_type                    rx_read_cs;

  /* Save context for tx long packets */
  smdl_iovec_type               tx_iovec;
  glink_core_tx_pkt_type        tx_pkt;
  os_cs_type                    tx_pending_pkt_cs;

  /* Save context for tx long packets */
  smdl_iovec_type               rx_iovec;
  xport_smd_intent_type        *rx_smd_intent;

  /* This is to identify if we should try to scan rx FIFO for
     more packets ??*/
  boolean                       rx_scan_fifo;

  /* whether we already got a remote close event for this channel */
  boolean                       remote_close;

  /* Pointer to the owning xport context */
  xport_smd_ctx_type           *smd_xport_ctx;

  /* flag to identify if we got remote open through GLINK_CNTL channel */
  boolean                       xport_smd_is_cntl_remote_open;

  /* flag to identify if we got remote close through GLINK_CNTL channel */
  boolean                       xport_smd_is_cntl_remote_close;
  
  /* event to wait until we get open_ack */
  os_event_type                 event_open_ack;
  
  /* event to wait until we get close_ack */
  os_event_type                 event_close_ack;

  /* Reference counting before close/free channel context */
  uint32                        ref_count;
 
}xport_smd_ch_ctx_type;

#ifdef FEATURE_XPORT_SMD_TASK
typedef struct _xport_smd_thread_data_type {
  /* Link needed for use with list APIs.  Must be at the head of the struct */
  smem_list_link_type           link;

  /* SMD channel ctx on which event occured */
  xport_smd_ch_ctx_type         *smd_ch_ctx;

  /* which event occured */
  smdl_event_type               smdl_event;
}xport_smd_thread_data_type;

xport_smd_thread_data_type xport_smd_thread_data[XPORT_SMD_NUM_EVENTS];
#endif //FEATURE_XPORT_SMD_TASK

typedef enum xport_smd_read_t {
  XPORT_SMD_READ,
  XPORT_SMD_READ_PARTIAL,
  XPORT_SMD_RX_INTENT
}xport_smd_read_type;

typedef enum xport_smd_event
{
  XPORT_SMD_OPEN,
  XPORT_SMD_OPEN_ACK,
  XPORT_SMD_CLOSE,
  XPORT_SMD_CLOSE_ACK,
  XPORT_SMD_SIG_BIT_CHANGE,
  XPORT_SMD_INVALID_EVENT
}xport_smd_event_type;

typedef struct xport_smd_event_info
{
  struct xport_smd_event_info *next;
  glink_xport_priority         requested_priority;
  xport_smd_ch_ctx_type       *smd_ch_ctx;
  xport_smd_event_type         event;
  
}xport_smd_event_info_type;

/* context of smd event. This only includes:
 * OPEN/OPEN ACK/CLOSE/CLOSE ACK */
typedef struct xport_smd_event_ctx
{
  /* List of SMD/Control channel event */ 
  smem_list_type                  smd_event_q;
  
  /* Lock for smd_event_q */
  os_cs_type                      smd_event_q_cs;

  /* Free list of smd event queue */
  smem_list_type                  smd_event_free_q;
  
  /* Lock for smd_event_q */
  os_cs_type                      smd_event_free_q_cs;

  os_event_type                   smd_worker_thread_event;
  
}xport_smd_event_ctx_type;

static xport_smd_event_ctx_type   xport_smd_event_ctx;

xport_smd_ctx_type        *xport_smd_ctx = NULL;
glink_core_version_type    xport_smd_version;

/* forward declaration */
void xport_smd_event_cb(smdl_handle_type  port,
                        smdl_event_type   event,
                        void             *data);

/* forward declaration for SMD cntl channel callback */
void xport_smdi_cntl_ch_cb(smdl_handle_type  port,
                           smdl_event_type   event,
                           void             *data);

glink_err_type xport_smd_allocate_rx_intent(
  glink_transport_if_type *if_ptr,
  size_t                  size,
  glink_rx_intent_type    *intent_ptr
);

glink_err_type xport_smd_deallocate_rx_intent(
  glink_transport_if_type *if_ptr,
  glink_rx_intent_type    *intent_ptr
);

void xport_smd_event_cb_thread(smdl_handle_type  port,
                               smdl_event_type   event,
                               void             *data);
/*===========================================================================
                    LOCAL FUNCTION DEFINITIONS
===========================================================================*/
/*===========================================================================
FUNCTION      xport_smd_remove_ch_ctx
===========================================================================*/
/**

  Remove channel context from open channel queue and free the context
  
  @param[in]  smd_ch_ctx   xport smd channel context

  @return       None

  @sideeffects  None.
*/
/*=========================================================================*/
static void xport_smd_remove_ch_ctx
(
  xport_smd_ch_ctx_type **smd_ch_ctx
)
{
  glink_os_cs_acquire( &(*smd_ch_ctx)->smd_xport_ctx->smd_open_channel_q_cs );
  smem_list_delete( &(*smd_ch_ctx)->smd_xport_ctx->smd_open_channel_q,
                    *smd_ch_ctx );
  glink_os_cs_release( &(*smd_ch_ctx)->smd_xport_ctx->smd_open_channel_q_cs );


  glink_os_free( *smd_ch_ctx );
  *smd_ch_ctx = NULL;
}

/*===========================================================================
FUNCTION      xport_smd_event_worker_thread
===========================================================================*/
/**

  This is worker thread to execute smdl/control channel event:
  
  OPEN/OPEN ACK/CLOSE/CLOSE ACK 

  @return     Returns error code.

  @sideeffects  None.
*/
/*=========================================================================*/
static void xport_smd_event_worker_thread
(
  void *param
)
{
  xport_smd_event_ctx_type  *smd_event_ctx = &xport_smd_event_ctx;
  xport_smd_event_info_type *smd_event_info;
  xport_smd_ch_ctx_type     *smd_ch_ctx;
  glink_transport_if_type   *if_ptr;
  glink_core_if_type        *glink_if;
  uint32                     lcid;
  uint32                     rcid;

#ifndef FEATURE_THREADLESS_ENV  
  while( TRUE )
#endif    
  {
    glink_os_cs_acquire( &smd_event_ctx->smd_event_q_cs );
    smd_event_info = smem_list_first( &smd_event_ctx->smd_event_q );
    
    if( !smd_event_info )
    {
      glink_os_cs_release( &smd_event_ctx->smd_event_q_cs );
#ifndef FEATURE_THREADLESS_ENV      
      glink_os_event_clear( &smd_event_ctx->smd_worker_thread_event );
      glink_os_event_wait( &smd_event_ctx->smd_worker_thread_event );
#endif      
      glink_os_cs_acquire( &smd_event_ctx->smd_event_q_cs );
      smd_event_info = smem_list_first( &smd_event_ctx->smd_event_q );
    }
    
    smem_list_delete( &smd_event_ctx->smd_event_q, smd_event_info );
    glink_os_cs_release( &smd_event_ctx->smd_event_q_cs );

    if( !smd_event_info->smd_ch_ctx )
    {
      OS_LOG_MSG(2, "XPORT_SMD_EVENT_WORKER_THREAD: SMD_CH_CTX NULL"
                    "[event: %d]", smd_event_info->event );
    }

    smd_ch_ctx = smd_event_info->smd_ch_ctx;
    if_ptr     = (glink_transport_if_type *)smd_ch_ctx->smd_xport_ctx;
    glink_if   = if_ptr->glink_core_if_ptr;

    switch( smd_event_info->event )
    {
      case XPORT_SMD_OPEN:
        ++smd_ch_ctx->ref_count;
        glink_if->rx_cmd_ch_remote_open( if_ptr, 
                                         smd_ch_ctx->rcid,
                                         smd_ch_ctx->name,
                                         smd_event_info->requested_priority );
        break;
    
      case XPORT_SMD_OPEN_ACK:
        ++smd_ch_ctx->ref_count;
        glink_if->rx_cmd_ch_open_ack( if_ptr,
                                      smd_ch_ctx->lcid,
                                      smd_event_info->requested_priority );
        break;
    
      case XPORT_SMD_CLOSE:
        rcid = smd_ch_ctx->rcid;
        --smd_ch_ctx->ref_count;
        if( !smd_ch_ctx->ref_count )
        {
          xport_smd_remove_ch_ctx( &smd_ch_ctx );
        }
        
        glink_if->rx_cmd_ch_remote_close( if_ptr, rcid );
        
        break;
    
      case XPORT_SMD_CLOSE_ACK:
        lcid = smd_ch_ctx->lcid;
        --smd_ch_ctx->ref_count;
        if( !smd_ch_ctx->ref_count )
        {
          xport_smd_remove_ch_ctx( &smd_ch_ctx );
        }
        
        glink_if->rx_cmd_ch_close_ack( if_ptr, lcid );
        
        break;

      case XPORT_SMD_SIG_BIT_CHANGE:
        /* smd_ch_ctx->prev_remote_sig is already updated at this point */
        glink_if->rx_cmd_remote_sigs( if_ptr,
                                      smd_ch_ctx->rcid,
                                      smd_ch_ctx->prev_remote_sigs );
        break;
        
      default:
        ASSERT(0);
    }
    
    smd_event_info->event = XPORT_SMD_INVALID_EVENT;
    smd_event_info->smd_ch_ctx = NULL;
    
    glink_os_cs_acquire( &smd_event_ctx->smd_event_free_q_cs );
    smem_list_append( &smd_event_ctx->smd_event_free_q, smd_event_info );
    glink_os_cs_release( &smd_event_ctx->smd_event_free_q_cs );
  }
}

/*===========================================================================
FUNCTION      xport_smd_get_free_smd_event_info
===========================================================================*/
/**

  Get a smd event info struct if exist in free queue
  If not allocate new one 

  @param[in]  smd_ctx   xport smd context

  @return     pointer to smd event info type

  @sideeffects  None.
*/
/*=========================================================================*/
static xport_smd_event_info_type *xport_smd_get_free_smd_event_info( void )
{
  xport_smd_event_ctx_type  *smd_event_ctx = &xport_smd_event_ctx;
  xport_smd_event_info_type *smd_event_info;
  glink_os_cs_acquire( &smd_event_ctx->smd_event_free_q_cs );
  smd_event_info = smem_list_first( &smd_event_ctx->smd_event_free_q );
  
  if( smd_event_info )
  {
    smem_list_delete( &smd_event_ctx->smd_event_free_q, smd_event_info );
    glink_os_cs_release( &smd_event_ctx->smd_event_free_q_cs );
    
    return smd_event_info;
  }
  
  glink_os_cs_release( &smd_event_ctx->smd_event_free_q_cs );
  
  smd_event_info = glink_os_calloc( sizeof( xport_smd_event_info_type ) );
  if( !smd_event_info )
  {
    ASSERT(0);
  }
  
  return smd_event_info;
}

/*===========================================================================
FUNCTION      xport_smd_notify_event
===========================================================================*/
/**

  Queue smd event and signal worker thread 

  @param[in]  smd_ch_ctx          xport smd channel context to queue
  @param[in]  event               xport smd internal event 
  @param[in]  requested_priority  requested priority from remote side

  @return     pointer to smd event info type

  @sideeffects  None.
*/
/*=========================================================================*/
static void xport_smd_notify_event
(
  xport_smd_ch_ctx_type *smd_ch_ctx,
  xport_smd_event_type   event,
  glink_xport_priority   requested_priority
)
{
  xport_smd_event_ctx_type  *smd_event_ctx = &xport_smd_event_ctx;
  xport_smd_event_info_type *smd_event_info
    = xport_smd_get_free_smd_event_info();
  
  smd_event_info->smd_ch_ctx         = smd_ch_ctx;
  smd_event_info->event              = event; 
  smd_event_info->requested_priority = requested_priority;
  
  glink_os_cs_acquire( &smd_event_ctx->smd_event_q_cs );
  smem_list_append( &smd_event_ctx->smd_event_q, smd_event_info );
  glink_os_cs_release( &smd_event_ctx->smd_event_q_cs );


#ifndef FEATURE_THREADLESS_ENV  
  glink_os_event_signal( &smd_event_ctx->smd_worker_thread_event );
#else 
  /* for threadless environments, invoke the thread function directly.
   * It would not loop waiting for the event indefinately */
  xport_smd_event_worker_thread(NULL);
#endif  
}

static void xport_smdi_queue_iovec
(
  smdl_iovec_type **iovec,
  void *buffer,
  uint32 size 
)
{
  smdl_iovec_type *temp_iovec, *cmd_iovec;	
  temp_iovec = cmd_iovec = *iovec;

  while(cmd_iovec != NULL)
  {
    temp_iovec = cmd_iovec;
    cmd_iovec = cmd_iovec->next;	
  }	      

  cmd_iovec = glink_os_calloc(sizeof(smdl_iovec_type));

  ASSERT(cmd_iovec != NULL);

  cmd_iovec->next = NULL;
  cmd_iovec->length = size;
  cmd_iovec->buffer = buffer;

  if(*iovec == NULL)
  {
    *iovec = cmd_iovec;	      
  }	      
  else
  {
    temp_iovec->next = cmd_iovec;	      
  }	
}

static uint32 xport_smdi_get_rcid
(
  glink_transport_if_type *if_ptr,
  uint32                  lcid 
)
{
  xport_smd_ctx_type *ctx_ptr = (xport_smd_ctx_type *)if_ptr;

  glink_os_cs_acquire(&ctx_ptr->smd_open_channel_q_cs);
  xport_smd_ch_ctx_type *smd_ch_ctx = smem_list_first(&ctx_ptr->smd_open_channel_q);  

  while(smd_ch_ctx!=NULL)
  {
    if(smd_ch_ctx->lcid == lcid)
    {
	  glink_os_cs_release(&ctx_ptr->smd_open_channel_q_cs);
      return smd_ch_ctx->rcid;	    
    }	    
    smd_ch_ctx = smem_list_next(smd_ch_ctx);
  }	  
  glink_os_cs_release(&ctx_ptr->smd_open_channel_q_cs);

  /* we reach here if lcid is not found */
  ASSERT(0);
  return 0;
}

static void *xport_smdi_generate_cmd
(
  xport_smd_cmd        cmd,
  uint32               cid,
  glink_xport_priority prio,
  const char           *name, 
  uint32               *len
)
{
  void *temp_buffer = NULL;
  uint32 *temp_var;

  *len = sizeof(uint32)*3;
  switch(cmd)
  {
    case XPORT_SMD_CH_MIGRATION_CMD:
	
      *len = *len + strlen(name)+1 /* +1 for null terminator */;
      temp_buffer = glink_os_calloc(*len);
      ASSERT(temp_buffer);

      glink_os_copy_mem((char *)((uint32 *)temp_buffer+3), name, strlen(name));
      break;
  
    case XPORT_SMD_CH_MIGRATION_DONE_CMD:
    case XPORT_SMD_CH_CLOSE_CMD:
    case XPORT_SMD_CH_CLOSE_CMD_ACK:
      temp_buffer = glink_os_calloc(*len);
      ASSERT(temp_buffer);
      break;

    default:
      break;      
  }

  temp_var = (uint32 *)temp_buffer;
  *temp_var = (uint32)cmd;

  temp_var = (uint32 *)temp_buffer+1;
  *temp_var = cid;

  temp_var = (uint32 *)temp_buffer+2;
  *temp_var = (uint32)prio;

  return temp_buffer;
}

/* Find channel by name, lcid, rcid */
static xport_smd_ch_ctx_type *xport_smdi_find_channel_by_name
(
  xport_smd_ctx_type *ctx_ptr,
  const char         *ch_name
)
{
  xport_smd_ch_ctx_type *smd_ch_ctx;
  
  glink_os_cs_acquire(&ctx_ptr->smd_open_channel_q_cs);
  smd_ch_ctx = smem_list_first(&ctx_ptr->smd_open_channel_q);  

  while( smd_ch_ctx )
  {
    if( glink_os_string_compare(smd_ch_ctx->name, ch_name) == 0)
    {
      break;
    }
    smd_ch_ctx = smem_list_next(smd_ch_ctx);
  }
  glink_os_cs_release(&ctx_ptr->smd_open_channel_q_cs);
  
  return smd_ch_ctx;
}

static xport_smd_ch_ctx_type *xport_smdi_find_channel_by_lcid
(
  xport_smd_ctx_type *ctx_ptr,
  uint32              lcid
)
{
  xport_smd_ch_ctx_type *smd_ch_ctx;
  
  glink_os_cs_acquire(&ctx_ptr->smd_open_channel_q_cs);
  smd_ch_ctx = smem_list_first(&ctx_ptr->smd_open_channel_q);  

  while( smd_ch_ctx )
  {
    if( lcid == smd_ch_ctx->lcid )
    {
      break;
    }
    smd_ch_ctx = smem_list_next(smd_ch_ctx);
  }
  glink_os_cs_release( &ctx_ptr->smd_open_channel_q_cs );
  
  /* Channel Context is expected to exist for lcid */
  ASSERT( smd_ch_ctx );
  
  return smd_ch_ctx;
}

static xport_smd_ch_ctx_type *xport_smdi_find_channel_by_rcid
(
  xport_smd_ctx_type *ctx_ptr,
  uint32              rcid
)
{
  xport_smd_ch_ctx_type *smd_ch_ctx;
  
  glink_os_cs_acquire( &ctx_ptr->smd_open_channel_q_cs );
  smd_ch_ctx = smem_list_first( &ctx_ptr->smd_open_channel_q );  

  while( smd_ch_ctx )
  {
    if( rcid == smd_ch_ctx->rcid )
    {
      break;
    }
    smd_ch_ctx = smem_list_next( smd_ch_ctx );
  }
  glink_os_cs_release( &ctx_ptr->smd_open_channel_q_cs );

  /* Channel Context is expected to exist for rcid */
  ASSERT( smd_ch_ctx );
  
  return smd_ch_ctx;
}

/*===========================================================================
FUNCTION      xport_smdi_process_cmd

DESCRIPTION   This function processed command for channel migration and 
              takes necessary action

ARGUMENTS     buffer      :    pointer to buffer containing command
RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
static void xport_smdi_process_cmd
(
  xport_smd_ctx_type   *ctx_ptr, 
  xport_smd_cmd         cmd,
  void                  *buffer,    
  uint32                buff_len  
)
{
  uint32 lcid, rcid;
  glink_xport_priority prio;
  const char *name;
  glink_transport_if_type *if_ptr = (glink_transport_if_type *)ctx_ptr;
  xport_smd_ch_ctx_type *smd_ch_ctx;

  switch(cmd)
  {
    case XPORT_SMD_CH_MIGRATION_CMD:
      rcid = *((uint32 *)buffer+1);
      prio = (glink_xport_priority)*((uint32 *)buffer+2);
      name = (char *)((uint32 *)buffer+3);

      smd_ch_ctx = xport_smdi_find_channel_by_name( ctx_ptr, name );
      
      if( smd_ch_ctx )
      {
        smd_ch_ctx->rcid = rcid;
      }
      else
      {
        /* Create channel context for this channel */
        smd_ch_ctx = glink_os_calloc(sizeof(xport_smd_ch_ctx_type));
        if(smd_ch_ctx == NULL) {
         ASSERT(0);
        }
 
        smd_ch_ctx->smd_xport_ctx = ctx_ptr;
        smd_ch_ctx->rcid = rcid;
        smd_ch_ctx->ref_count = 0;
        glink_os_copy_mem(smd_ch_ctx->name, name, strlen(name)); 

        glink_os_cs_acquire(&ctx_ptr->smd_open_channel_q_cs);
        smem_list_append(&ctx_ptr->smd_open_channel_q, smd_ch_ctx);
        glink_os_cs_release(&ctx_ptr->smd_open_channel_q_cs);
      }
  

      if( !smd_ch_ctx->xport_smd_is_cntl_remote_open )
      {
        smd_ch_ctx->xport_smd_is_cntl_remote_open = TRUE;
        xport_smd_notify_event( smd_ch_ctx, XPORT_SMD_OPEN, prio );
      }
      break;
    
    case XPORT_SMD_CH_MIGRATION_DONE_CMD:
      lcid = *((uint32 *)buffer+1);
      prio = (glink_xport_priority)*((uint32 *)buffer+2);

      smd_ch_ctx = xport_smdi_find_channel_by_lcid( (xport_smd_ctx_type*)if_ptr,
                                                    lcid );
      
      xport_smd_notify_event( smd_ch_ctx, XPORT_SMD_OPEN_ACK, prio );
      glink_os_event_signal( &smd_ch_ctx->event_open_ack );

      break;

    case XPORT_SMD_CH_CLOSE_CMD:
      rcid = *((uint32 *)buffer+1);

      smd_ch_ctx = xport_smdi_find_channel_by_rcid( ctx_ptr, rcid );

      if( !smd_ch_ctx->remote_close )
      {
        smd_ch_ctx->remote_close                   = TRUE;
        smd_ch_ctx->xport_smd_is_cntl_remote_close = TRUE;
        xport_smd_notify_event( smd_ch_ctx, 
                                XPORT_SMD_CLOSE,
                                GLINK_INVALID_PRIORITY );
      }

      break;
    
    case XPORT_SMD_CH_CLOSE_CMD_ACK:
      lcid = *((uint32 *)buffer+1);
      
      smd_ch_ctx = xport_smdi_find_channel_by_lcid( ctx_ptr, lcid );
      
      glink_os_event_signal( &smd_ch_ctx->event_close_ack );
      break;
 
    default:
      break;	
  }	    
}	

/*===========================================================================
FUNCTION      xport_smdi_notify_core_read_pkt_done

DESCRIPTION   This function notifies glink core that full packet was
              read from smd FIFO and updates glink intent parameters.

ARGUMENTS     glink_core_if_type    : glink core interface pointer
              xport_smd_ch_ctx_type : pointer to smd xport channel context
              glink_rx_intent_type  : glink intent	      
              pkt_len               : packet size
RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
static void xport_smdi_notify_core_read_pkt_done
(
  xport_smd_ch_ctx_type *smd_ch_ctx,
  uint32                pkt_len

)
{
  xport_smd_ctx_type *ctx_ptr = smd_ch_ctx->smd_xport_ctx;
  glink_transport_if_type *if_ptr = (glink_transport_if_type*)ctx_ptr;
  glink_core_if_type *glink_if = if_ptr->glink_core_if_ptr;
  glink_rx_intent_type *glink_intent;  

  glink_intent = glink_if->rx_get_pkt_ctx(if_ptr, 
                                              smd_ch_ctx->rcid,
                                              smd_ch_ctx->rx_smd_intent->iid);

  /* Full packet was available for reading in FIFO
  * update glink intent ptr and notify core that reading is done */	      
  glink_intent->pkt_sz = pkt_len;
  glink_intent->used   = pkt_len;

  /* Move smd_intent to rx_intent_in_use_q */
  glink_os_cs_acquire(&smd_ch_ctx->rx_intent_q_cs);
  smem_list_delete(&smd_ch_ctx->rx_intent_q, smd_ch_ctx->rx_smd_intent);
  smem_list_append(&smd_ch_ctx->rx_intent_in_use_q, smd_ch_ctx->rx_smd_intent);
  glink_os_cs_release(&smd_ch_ctx->rx_intent_q_cs);

  /* Clear rx pkt contexts */
  memset(&smd_ch_ctx->rx_iovec, 0, sizeof(smd_ch_ctx->rx_iovec));
  smd_ch_ctx->rx_smd_intent = 0;

  /* notify glink core that reading is complete */	
  glink_if->rx_put_pkt_ctx(if_ptr,
                           smd_ch_ctx->rcid,
                           glink_intent,
                           TRUE /* complete */);	
}


static void xport_smdi_read_pkt(xport_smd_read_type read_type, xport_smd_ch_ctx_type *smd_ch_ctx)
{
  int read;
  boolean req_intent = FALSE;
  xport_smd_intent_type *smd_intent;
  glink_rx_intent_type *glink_intent, glink_intent_intentless;
  xport_smd_ctx_type *ctx_ptr = smd_ch_ctx->smd_xport_ctx;
  glink_transport_if_type *if_ptr = (glink_transport_if_type*)ctx_ptr;
  glink_core_if_type *glink_if = if_ptr->glink_core_if_ptr;
  uint32 pkt_len=0, byte_avail=0;
  smdl_iovec_type smdl_io;
  smdl_io.next = NULL;

  /* Lock read operations */
  glink_os_cs_acquire(&smd_ch_ctx->rx_read_cs);

  if(read_type == XPORT_SMD_READ || read_type == XPORT_SMD_READ_PARTIAL) {
    /* if this function was called frmo smdl callback, request a new
       rx_intent from client */
    req_intent = TRUE;
  }

  /* poke smd FIFO to check if data is available in it for reading */
  ASSERT(SMD_STATUS_SUCCESS == 
       smdl_rx_peek_ext(smd_ch_ctx->smd_handle, &pkt_len, &byte_avail));

  /* Short circuit for intentless mode handling */
  if(smd_ch_ctx->xport_capabilities & GLINK_CAPABILITY_INTENTLESS) {

    do {
      ASSERT( GLINK_STATUS_SUCCESS == 
              xport_smd_allocate_rx_intent( if_ptr,
                                            pkt_len,
                                            &glink_intent_intentless ) );
      do {
        /* read data from FIFO. Data read can be partial OR full packet will be read
         * based on data available in FIFO */
        smdl_io.buffer = ((glink_iovector_type*)glink_intent_intentless.iovec)->vlist->data;
        smdl_io.length = pkt_len;
        read = smdl_readv(smd_ch_ctx->smd_handle,
                          &smdl_io,
                          SMDL_READ_FLAG_LONG);
      } while (read != pkt_len );
      /* Full packet was available for reading in FIFO
       * update glink intent ptr and notify core that reading is done */
      glink_intent_intentless.pkt_sz = pkt_len;
      glink_intent_intentless.used   = pkt_len;
      glink_intent_intentless.size   = pkt_len;

      /* notify glink core that reading is complete */	
      glink_if->rx_put_pkt_ctx(if_ptr,
                               smd_ch_ctx->rcid,
                               &glink_intent_intentless,
                               TRUE /* complete */);

      /* poke FIFO to see if another packet is available for reading */
      ASSERT(SMD_STATUS_SUCCESS == 
           smdl_rx_peek_ext(smd_ch_ctx->smd_handle, &pkt_len, &byte_avail));
      
      xport_smd_deallocate_rx_intent(if_ptr, &glink_intent_intentless);
    }while(byte_avail > 0);

    glink_os_cs_release(&smd_ch_ctx->rx_read_cs);
    return;
  }

  while(byte_avail > 0) 
  {
    /* ========================== CONTINUATION OF OLD PKT =================== */
    
    /* Check to see if we were in the middle of a read, if we were, process it */
    if(smd_ch_ctx->rx_iovec.buffer != NULL) {
      /* we have just received full pkt */
      read = smdl_readv(smd_ch_ctx->smd_handle,
                        &smd_ch_ctx->rx_iovec,
                        SMDL_READ_FLAG_LONG);

      if(read == pkt_len) {
        /* notify glink core that packet is done */
        xport_smdi_notify_core_read_pkt_done(smd_ch_ctx, pkt_len);
      }
      /* poke FIFO to see if another packet is available for reading */
      ASSERT(SMD_STATUS_SUCCESS == 
           smdl_rx_peek_ext(smd_ch_ctx->smd_handle, &pkt_len, &byte_avail));
      if(pkt_len == 0)
      {
        break;
      }
      continue;
    }

    /* ================================== NEW PKT ========================== */
    /* Try to get a matching intent, otherwise skip reading */
    smd_intent = smem_list_first(&smd_ch_ctx->rx_intent_q);
    while(smd_intent && (smd_intent->size < (size_t)pkt_len)) {
      smd_intent = smem_list_next(smd_intent);
    }

    if(smd_intent == NULL && req_intent) {
      /* Fake remote rx_intent request. Client should queue more rx_intents
         to unblock the read operation */
      smd_ch_ctx->rx_scan_fifo = FALSE;
      glink_if->rx_cmd_remote_rx_intent_req(if_ptr,
                                            smd_ch_ctx->rcid, 
                                            pkt_len);
      smd_ch_ctx->rx_scan_fifo = TRUE;

      /* Don't request rx intents again */
      req_intent = FALSE;
    } 
    else if(smd_intent) 
    {
      /* Got the intent, read data */
      glink_intent = glink_if->rx_get_pkt_ctx(if_ptr, 
                                              smd_ch_ctx->rcid,
                                              smd_intent->iid);

      smdl_io.buffer = ((glink_iovector_type*)glink_intent->iovec)->vlist->data;
      smdl_io.length = pkt_len;

      /* read data from FIFO. Data read can be partial OR full packet will be read
       * based on data available in FIFO */
      read = smdl_readv(smd_ch_ctx->smd_handle,
                        &smdl_io,
                        SMDL_READ_FLAG_LONG);

      smd_ch_ctx->rx_iovec      = smdl_io;
      smd_ch_ctx->rx_smd_intent = smd_intent;

      /* Data available in FIFO was partial.
       * update glink intent ptr and exit the loop */
      if(read == SMDL_CONTINUE)
      {
        /* Update GLink rx intent ptr */
        break;
      }
      else if(read == pkt_len)
      {
        /* notify glink core that pkt reading is done */
        xport_smdi_notify_core_read_pkt_done(smd_ch_ctx, pkt_len);

        /* request intent again in case another packet is 
         * available for reading and smd_intent is NULL */	
        req_intent = TRUE;
      }
      else
      {
        /* we reach here only if read in smdl_readv() was
         * neither partial nor full packet read */
        ASSERT(0);
      }

      /* poke FIFO to see if another packet is available for reading */
      ASSERT(SMD_STATUS_SUCCESS == 
           smdl_rx_peek_ext(smd_ch_ctx->smd_handle, &pkt_len, &byte_avail));
      if(pkt_len == 0)
      {
        break;
      }
    } else {
      /* We requested intent but client did not queue it. Break
         out of the loop for now as we can't read */
      break;
    }
  }
  /* Unlock read operations */
  glink_os_cs_release(&smd_ch_ctx->rx_read_cs);
}

/*===========================================================================
FUNCTION      xport_smd_send_cntl_ch_cmd
===========================================================================*/
/**

  Send control channel command 

  @param[in]  smd_ctx     transport context
  @param[in]  cmd         control channel command 
  @param[in]  cid         Channel ID
  @param[in]  name        name of channel if necessary

  @return       None

  @sideeffects  None.
*/
/*=========================================================================*/
static void xport_smd_send_cntl_ch_cmd
( 
  xport_smd_ctx_type   *smd_ctx,
  xport_smd_cmd         cmd,
  uint32                cid,
  const char           *name,
  glink_xport_priority  priority
)
{
  uint32 len;
  uint32 ret;
  void   *buffer;
  
  buffer = xport_smdi_generate_cmd(cmd, cid, priority, name, &len);

  
  if( smd_ctx->xport_smd_cntl_ch_open )
  {
    /* write this command to control channel */   
    ret = smdl_write( smd_ctx->xport_smd_cntl_ch_handle,
                      len,
                      buffer,
                      SMDL_WRITE_FLAG_LONG );

    if(ret == 0 && len != 0)
    {
      xport_smdi_queue_iovec(&smd_ctx->cmd_iovec, buffer, len); 
    }   
    else
    {
      glink_os_free(buffer);
    }
  }
  else
  {
    xport_smdi_queue_iovec(&smd_ctx->cmd_iovec, buffer, len);
  }
}


/*===========================================================================
                    EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/

/*===========================================================================
FUNCTION      xport_smd_tx_cmd_version
===========================================================================*/
/**

  Transmit a version command for local negotiation - 
  expected response is to be delivered via glink_rx_cmd_version_ack().

  @param[in]  if_ptr    Pointer to transport interface instance.
  @param[in]  version   Version to be sent.
  @param[in]  feature   Features to be sent.

  @return     None.

  @sideeffects  None.
*/
/*=========================================================================*/
void xport_smd_tx_cmd_version
(
  glink_transport_if_type *if_ptr,
  uint32 version,
  uint32 features
)
{
  glink_core_if_type *glink_if = if_ptr->glink_core_if_ptr;
  
  /* Nothing sent to remote side */
  /* send glink the same version/features we initiated the negotiation with */
  glink_if->rx_cmd_version_ack(if_ptr, SMD_XPORT_VERSION, SMD_XPORT_FEATURES);
  /* Fake remote side�s cmd_version */
  glink_if->rx_cmd_version(if_ptr, SMD_XPORT_VERSION, SMD_XPORT_FEATURES);
}

/*===========================================================================
FUNCTION      xport_smd_tx_cmd_version_ack
===========================================================================*/
/**

  Transmit a version ack for remote negotiation.

  @param[in]  if_ptr    Pointer to transport interface instance.
  @param[in]  version   Version to be sent.
  @param[in]  feature   Features to be sent.

  @return     None.

  @sideeffects  None.
*/
/*=========================================================================*/
void xport_smd_tx_cmd_version_ack
(
  glink_transport_if_type *if_ptr,
  uint32 version,
  uint32 features
)
{
  /* No need to transmit an actual ACK to remote side */
  /* Do nothing */
}

/*===========================================================================
FUNCTION      xport_smd_set_version
===========================================================================*/
/**

  Signals that negotiation is complete and the transport can now 
  do version-specific initialization.

  @param[in]  if_ptr    Pointer to transport interface instance.
  @param[in]  version   Version to be used.
  @param[in]  feature   Features to be used.

  @return     Transport specific capabilities flag.

  @sideeffects  None.
*/
/*=========================================================================*/
uint32 xport_smd_set_version
(
  glink_transport_if_type *if_ptr,
  uint32 version,
  uint32 features
)
{
  uint32 capabilities = GLINK_CAPABILITY_SIG_SUPPORT;
  glink_core_if_type *glink_if = if_ptr->glink_core_if_ptr;
  
  /* Call the glink core set_core_version function. No negotiation logic */
  if(version != SMD_XPORT_VERSION || features != SMD_XPORT_FEATURES)
  {
    ASSERT(0); /* Core doing something funny */
  } else {
    glink_if->set_core_version(if_ptr, version);
  }

  /* return intentless mode of operation if we are connecting to RPM */
  if(0 == strcmp(if_ptr->glink_core_priv->remote_ss, "rpm")) {
    capabilities |= GLINK_CAPABILITY_INTENTLESS;
    /* Clear out the functions for handling intent implemented in SMD
       core if */
   if_ptr->allocate_rx_intent = NULL;
   if_ptr->deallocate_rx_intent = NULL;
   if_ptr->tx_cmd_local_rx_intent = NULL;
   if_ptr->tx_cmd_local_rx_done = NULL;
  }

  return capabilities;
}

/*===========================================================================
FUNCTION      xport_smd_tx_cmd_ch_open
===========================================================================*/
/**

  Sends the open command - expected response is to be delivered 
  via glink_rx_cmd_ch_open_ack().

  @param[in]  if_ptr   Pointer to transport interface instance.
  @param[in]  lcid     Local channel ID.
  @param[in]  name     Channel name.
  @param[in]  prio     xport priority requested by channel.

  @return     Returns error code.

  @sideeffects  None.
*/
/*=========================================================================*/
glink_err_type xport_smd_tx_cmd_ch_open
(
  glink_transport_if_type *if_ptr,
  uint32 lcid,
  const char *name,
  glink_xport_priority prio
)
{
  xport_smd_ch_ctx_type *smd_ch_ctx;
  uint32 fifo_size = 8192;
  xport_smd_ctx_type *ctx_ptr = (xport_smd_ctx_type *)if_ptr;
  
  
  
  smd_ch_ctx = xport_smdi_find_channel_by_name( ctx_ptr, name );
  if( smd_ch_ctx )
  {
    smd_ch_ctx->lcid = lcid;
  }
  else
  {
    /* Create channel context for this channel */
    smd_ch_ctx = glink_os_calloc(sizeof(xport_smd_ch_ctx_type));
    if(smd_ch_ctx == NULL) {
      return GLINK_STATUS_OUT_OF_RESOURCES;
    }
  /* set essential smd_ctx params before open call as we may get a 
     SMDL_EVENT_OPEN before exiting from smdl_open() */ 
    smd_ch_ctx->smd_xport_ctx = ctx_ptr;
    smd_ch_ctx->lcid = lcid;
    smd_ch_ctx->ref_count = 0;
    glink_os_string_copy(smd_ch_ctx->name, name, sizeof(smd_ch_ctx->name)); 

    /* Add the channel context to the list of open channels */
    glink_os_cs_acquire(&ctx_ptr->smd_open_channel_q_cs);
    smem_list_append(&ctx_ptr->smd_open_channel_q, smd_ch_ctx);
    glink_os_cs_release(&ctx_ptr->smd_open_channel_q_cs);	
  }

  smd_ch_ctx->xport_capabilities = GLINK_CAPABILITY_SIG_SUPPORT;

  if(0 == strcmp(ctx_ptr->pcfg->remote_ss, "rpm")) {
    smd_ch_ctx->xport_capabilities |= GLINK_CAPABILITY_INTENTLESS;
    fifo_size = 1024;
  }

  /* check if control channel for migration 
   * is fully opened */
  xport_smd_send_cntl_ch_cmd( ctx_ptr, 
                              XPORT_SMD_CH_MIGRATION_CMD, 
                              lcid,
                              name,
                              prio );
  

  if( ctx_ptr->xport_smd_cntl_ch_open )
  {	
    /* initialize event and wait until we get open_ack from remote */
    glink_os_event_init(&smd_ch_ctx->event_open_ack);
    glink_os_event_clear(&smd_ch_ctx->event_open_ack);
    glink_os_event_wait(&smd_ch_ctx->event_open_ack);
  }

  /* Open the SMD Channel. For implementation not supporting channel creation, save the   
     open request if channel not already open and open it later when remote side
     opens the channel */
  smd_ch_ctx->smd_handle = smdl_open(name, 
                                     ctx_ptr->pcfg->edge,
                                     SMDL_OPEN_FLAGS_PRIORITY_LOW,
                                     fifo_size,
#ifdef FEATURE_XPORT_SMD_TASK
                                     xport_smd_event_cb,
#else
                                     xport_smd_event_cb_thread,
#endif
                                     smd_ch_ctx);
  /* Fake-ACK the open request. Note that Glink core might get a remote_open before
     this ACK if the channel was already open */
  if(smd_ch_ctx->smd_handle) {
    //glink_if->rx_cmd_ch_open_ack(if_ptr, lcid);
    /* Set channel context fields */
    smem_list_init(&smd_ch_ctx->rx_intent_q);
    smem_list_init(&smd_ch_ctx->rx_intent_in_use_q);
    glink_os_cs_init(&smd_ch_ctx->rx_intent_q_cs);
    glink_os_cs_init(&smd_ch_ctx->rx_read_cs);
    glink_os_cs_init(&smd_ch_ctx->tx_pending_pkt_cs);
  } else {
    smem_list_delete(&ctx_ptr->smd_open_channel_q, smd_ch_ctx);
    glink_os_free(smd_ch_ctx);
    return GLINK_STATUS_FAILURE;
  }

  return GLINK_STATUS_SUCCESS;
}

/*===========================================================================
FUNCTION      xport_smd_tx_cmd_ch_close
===========================================================================*/
/**

  Sends the close command - expected response is to be delivered 
  via glink_rx_cmd_ch_close_ack().

  @param[in]  if_ptr   Pointer to transport interface instance.
  @param[in]  lcid     Local channel ID.

  @return     Returns error code.

  @sideeffects  None.
*/
/*=========================================================================*/
glink_err_type xport_smd_tx_cmd_ch_close
(
  glink_transport_if_type *if_ptr,
  uint32 lcid
)
{
  xport_smd_ch_ctx_type *smd_ch_ctx;
  xport_smd_ctx_type *ctx_ptr = (xport_smd_ctx_type *)if_ptr;
  
  smd_ch_ctx = xport_smdi_find_channel_by_lcid( (xport_smd_ctx_type*)if_ptr,
                                                lcid );
  
  XPORT_SMD_LOG("Channel migration command generated", "adsp", 0);
  
  /* check if control channel for migration 
   * is fully opened */
  xport_smd_send_cntl_ch_cmd( ctx_ptr,
                              XPORT_SMD_CH_CLOSE_CMD,
                              lcid, 
                              NULL, 
                              (glink_xport_priority)0 );


  if( ctx_ptr->xport_smd_cntl_ch_open )
  { 
    /* initialize event and wait until we get open_ack from remote */
    glink_os_event_init(&smd_ch_ctx->event_close_ack);
    glink_os_event_clear(&smd_ch_ctx->event_close_ack);
    glink_os_event_wait(&smd_ch_ctx->event_close_ack);
  }
  
  /* initiate SMD close */
  ASSERT(smdl_close(smd_ch_ctx->smd_handle) == 0);

  return GLINK_STATUS_SUCCESS;
}

/*===========================================================================
FUNCTION      xport_smd_tx_cmd_ch_remote_open_ack
===========================================================================*/
/**

  Sends the remote open ACK command.

  @param[in]  if_ptr   Pointer to transport interface instance.
  @param[in]  rcid     Remote channel ID.
  @param[in]  prio     negotiated xport priority to be sent to remote side

  @return     None.

  @sideeffects  None.
*/
/*=========================================================================*/
void xport_smd_tx_cmd_ch_remote_open_ack
(
  glink_transport_if_type *if_ptr,
  uint32 rcid,
  glink_xport_priority prio 
)
{
  xport_smd_ctx_type    *ctx_ptr = (xport_smd_ctx_type *)if_ptr;
  xport_smd_ch_ctx_type *smd_ch_ctx;
  
  smd_ch_ctx = xport_smdi_find_channel_by_rcid( ctx_ptr, rcid );
  
  if( smd_ch_ctx->xport_smd_is_cntl_remote_open == TRUE )
  {
    xport_smd_send_cntl_ch_cmd( ctx_ptr, 
                                XPORT_SMD_CH_MIGRATION_DONE_CMD, 
                                rcid,
                                NULL,
                                prio );
  }
}

/*===========================================================================
FUNCTION      xport_smd_tx_cmd_ch_remote_close_ack
===========================================================================*/
/**

  Sends the remote close ACK command.

  @param[in]  if_ptr   Pointer to transport interface instance.
  @param[in]  rcid     Remote channel ID.

  @return     None.

  @sideeffects  None.
*/
/*=========================================================================*/
void xport_smd_tx_cmd_ch_remote_close_ack
(
  glink_transport_if_type *if_ptr,
  uint32 rcid
)
{
  xport_smd_ctx_type    *ctx_ptr = (xport_smd_ctx_type *)if_ptr;
  xport_smd_ch_ctx_type *smd_ch_ctx;
  
 
  smd_ch_ctx = xport_smdi_find_channel_by_rcid( ctx_ptr, rcid );
  
  if( smd_ch_ctx->xport_smd_is_cntl_remote_close )
  {
    xport_smd_send_cntl_ch_cmd( ctx_ptr,
                                XPORT_SMD_CH_CLOSE_CMD_ACK, 
                                rcid,
                                NULL,
                                (glink_xport_priority)0 );
  }
}

/*===========================================================================
FUNCTION      xport_smd_tx_cmd_rx_intent_req
===========================================================================*/
/**

  Sends the intent request command.

  @param[in]  if_ptr   Pointer to transport interface instance.
  @param[in]  lcid     Local channel ID.
  @param[in]  size     size of requested intent.

  @return     Returns error code.

  @sideeffects  None.
*/
/*=========================================================================*/
glink_err_type xport_smd_tx_cmd_rx_intent_req
(
  glink_transport_if_type *if_ptr,
  uint32 lcid,
  size_t size
)
{
  glink_core_if_type *glink_if = if_ptr->glink_core_if_ptr;
  xport_smd_ctx_type *ctx_ptr = (xport_smd_ctx_type *)if_ptr;
  uint32 rcid;
  /* Fulfill the client request by faking an ACK followed by a new remote rx 
     intent with requested size */

  rcid = xport_smdi_get_rcid(if_ptr, lcid);
  glink_if->rx_cmd_rx_intent_req_ack(if_ptr, rcid /* = rcid */, TRUE);
  glink_if->rx_cmd_remote_rx_intent_put(if_ptr,
                                        rcid /* = rcid */, 
                                        ctx_ptr->iid++,
                                        size /* = request size */);
  return GLINK_STATUS_SUCCESS;
}

/*===========================================================================
FUNCTION      xport_smd_tx_cmd_remote_rx_intent_req_ack
===========================================================================*/
/**

  Sends the ack for remote intent request.

  @param[in]  if_ptr    Pointer to transport interface instance.
  @param[in]  lcid      Local channel ID.
  @param[in]  granted   Whether the intent request iss granted. 

  @return     Returns error code.

  @sideeffects  None.
*/
/*=========================================================================*/
glink_err_type xport_smd_tx_cmd_remote_rx_intent_req_ack
(
  glink_transport_if_type *if_ptr,
  uint32 lcid,
  boolean granted
)
{
  /* At some point, transitional XAL got a READ event on SMD port but could not
     find a matching rx_intent, so it faked a rx_intent request. Do nothing for
     the ACK */

  return GLINK_STATUS_SUCCESS;
}

/*===========================================================================
FUNCTION      xport_smd_tx_cmd_local_rx_intent
===========================================================================*/
/**

  Send receive intent command.

  @param[in]  if_ptr   Pointer to transport interface instance.
  @param[in]  lcid     Local channel ID.
  @param[in]  size     Size of receive intent.
  @param[in]  liid     Local intent ID.

  @return     Returns error code.

  @sideeffects  None.
*/
/*=========================================================================*/
glink_err_type xport_smd_tx_cmd_local_rx_intent
(
  glink_transport_if_type *if_ptr,
  uint32 lcid,
  size_t size,
  uint32 liid
)
{
  xport_smd_ch_ctx_type *smd_ch_ctx;
  xport_smd_intent_type *smd_intent;
  xport_smd_ctx_type *ctx_ptr = (xport_smd_ctx_type *)if_ptr;
  
  /* Search for the given SMD channel in the list of open channels */
  smd_ch_ctx = smem_list_first(&ctx_ptr->smd_open_channel_q);
  while((smd_ch_ctx != NULL) && (smd_ch_ctx->lcid != lcid)) {
    smd_ch_ctx = smem_list_next(smd_ch_ctx);
  }
  ASSERT(smd_ch_ctx);

  smd_intent = glink_os_malloc(sizeof(xport_smd_intent_type));
  if(smd_intent == NULL) {
    return GLINK_STATUS_OUT_OF_RESOURCES;
  }
  XPORT_SMD_LOG("Local client queued rx intent", "apps", size);

  smd_intent->iid = liid;
  smd_intent->size = size;
  
  /* Keep a record of this intent in the channel context structure. It 
   * is to be used when we get a READ EVENT from SMD to ask Glink
   * for this intent back */
  glink_os_cs_acquire(&smd_ch_ctx->rx_intent_q_cs);
  smem_list_append(&smd_ch_ctx->rx_intent_q, smd_intent);
  glink_os_cs_release(&smd_ch_ctx->rx_intent_q_cs);

  /* See if we can use this intent to read new data from SMD port */
  if(smd_ch_ctx->rx_scan_fifo == TRUE) {
    smd_ch_ctx->rx_scan_fifo = FALSE;
    xport_smdi_read_pkt(XPORT_SMD_RX_INTENT, smd_ch_ctx);
    smd_ch_ctx->rx_scan_fifo = TRUE;
  }

  return GLINK_STATUS_SUCCESS;
}

/*===========================================================================
FUNCTION      xport_smd_tx_cmd_local_rx_done
===========================================================================*/
/**

  Send receive-done command.

  @param[in]  if_ptr   Pointer to transport interface instance.
  @param[in]  lcid     Local channel ID.
  @param[in]  liid     Local intent ID.
  @param[in]  data     Pointer to the allocated buffer.

  @return     None.

  @sideeffects  None.
*/
/*=========================================================================*/
void xport_smd_tx_cmd_local_rx_done
(
  glink_transport_if_type *if_ptr,
  uint32 lcid,
  uint32 liid,
  boolean  reuse
)
{
  /* Re-use intent */
  xport_smd_ch_ctx_type *smd_ch_ctx;
  xport_smd_intent_type *smd_intent;
  xport_smd_ctx_type *ctx_ptr = (xport_smd_ctx_type *)if_ptr;
  
  /* Search for the given SMD channel in the list of open channels */
  smd_ch_ctx = smem_list_first(&ctx_ptr->smd_open_channel_q);
  while((smd_ch_ctx != NULL) && (smd_ch_ctx->lcid != lcid)) {
    smd_ch_ctx = smem_list_next(smd_ch_ctx);
  }
  ASSERT(smd_ch_ctx);

  smd_intent = smem_list_first(&smd_ch_ctx->rx_intent_in_use_q);
  while(smd_intent) {
    /* try to match the intent */
    if(smd_intent->iid == liid) {
      break;
    }
    smd_intent = smem_list_next(smd_intent);
  }

  ASSERT(smd_intent);

  /* Delete the intent from  rx_intent_in_use_q . Requeue it in
     rx_intent_q if reuse option is set */
  glink_os_cs_acquire(&smd_ch_ctx->rx_intent_q_cs);
  smem_list_delete(&smd_ch_ctx->rx_intent_in_use_q, smd_intent);
  if(reuse) {
    smem_list_append(&smd_ch_ctx->rx_intent_q, smd_intent);
  } else {
    glink_os_free(smd_intent);
  }
  glink_os_cs_release(&smd_ch_ctx->rx_intent_q_cs);
}

/*===========================================================================
FUNCTION      xport_smd_tx
===========================================================================*/
/**

  Send a data packet or a fragment of it.

  @param[in]  if_ptr   Pointer to transport interface instance.
  @param[in]  lcid     Local channel ID.
  @param[in]  pctx     Packet TX context.

  @return     Returns error code.

  @sideeffects  None.
*/
/*=========================================================================*/
glink_err_type xport_smd_tx
(
  glink_transport_if_type *if_ptr,
  uint32 lcid,
  glink_core_tx_pkt_type *pctx
)
{
  xport_smd_ch_ctx_type *smd_ch_ctx;
  smdl_iovec_type smdl_io, *smdl_io_temp, *smdl_io_cur;
  xport_smd_ctx_type *ctx_ptr = (xport_smd_ctx_type *)if_ptr;
  glink_core_if_type *glink_if = if_ptr->glink_core_if_ptr;
  int written = 0;
  uint32 temp_iid;

  size_t frag_size;
  void* frag_data;

  if (pctx->vprovider != NULL)
  {
    frag_data = pctx->vprovider(pctx->iovec, 
                  pctx->size - pctx->size_remaining, &frag_size);
  }
  else
  {
    // map from prhysical provider
    ASSERT(0); //not implemented
    frag_data = NULL;
    frag_size = 0;
  }

  ASSERT(frag_data != NULL && frag_size <= pctx->size_remaining);
  
  smdl_io.next = NULL;
  smdl_io.length = frag_size;
  smdl_io.buffer = frag_data;
  smdl_io_cur = &smdl_io;

  /* Let the client know that SMD XAL will take care of transmisison
     from here on */
  pctx->size_remaining -= frag_size;

  /* Compose SMDL IO vector */
  while(pctx->size_remaining) {
    smdl_io_temp = glink_os_calloc(sizeof(smdl_iovec_type));
    ASSERT(smdl_io_temp);
    smdl_io_cur->next = smdl_io_temp;
    frag_data = pctx->vprovider(pctx->iovec, 
                  pctx->size - pctx->size_remaining, &frag_size);
    smdl_io_temp->length = frag_size;
    smdl_io_temp->buffer = frag_data;
    smdl_io_cur = smdl_io_temp;
    pctx->size_remaining -= frag_size;
  }

  /* Search for the given SMD channel in the list of open channels */
  smd_ch_ctx = smem_list_first(&ctx_ptr->smd_open_channel_q);
  while((smd_ch_ctx != NULL) && (smd_ch_ctx->lcid != lcid)) {
    smd_ch_ctx = smem_list_next(smd_ch_ctx);
  }
  ASSERT(smd_ch_ctx);

  /* Short circuit for intentless mode handling */
  if(smd_ch_ctx->xport_capabilities & GLINK_CAPABILITY_INTENTLESS) {
    do {
      written = smdl_writev(smd_ch_ctx->smd_handle, &smdl_io, SMDL_WRITE_FLAG_LONG);
      if(written != SMDL_CONTINUE && written != pctx->size && written != 0) {
        ASSERT(0); /* SMDL writev should not fail */
      }
    } while (written != pctx->size );

    /* free the smdl_iovec chain */
    smdl_io_cur = smdl_io.next;
    while(smdl_io_cur) {
      smdl_io_temp = smdl_io_cur->next;
      glink_os_free(smdl_io_cur);
      smdl_io_cur = smdl_io_temp;
    }
    return GLINK_STATUS_SUCCESS;
  }

  /* Check to see if we already had a pkt queued for transmission */
  glink_os_cs_acquire(&smd_ch_ctx->tx_pending_pkt_cs);
  if(smd_ch_ctx->tx_iovec.buffer) {
    /* Keep retrying to send prev pkt */
    do {
      written = smdl_writev(smd_ch_ctx->smd_handle, &smd_ch_ctx->tx_iovec, SMDL_WRITE_FLAG_LONG);
      if(written != SMDL_CONTINUE && written != smd_ch_ctx->tx_pkt.size && written != 0) {
        ASSERT(0); /* SMDL writev should not fail */
      } else if (written == smd_ch_ctx->tx_pkt.size) {

        /* free the smdl_iovec chain */
        smdl_io_cur = smd_ch_ctx->tx_iovec.next;
        while(smdl_io_cur) {
          smdl_io_temp = smdl_io_cur->next;
          glink_os_free(smdl_io_cur);
          smdl_io_cur = smdl_io_temp;
        }
        /* Clear tx pkt structures */
        temp_iid = smd_ch_ctx->tx_pkt.iid;
        memset(&smd_ch_ctx->tx_pkt, 0, sizeof(smd_ch_ctx->tx_pkt));
        memset(&smd_ch_ctx->tx_iovec, 0, sizeof(smd_ch_ctx->tx_iovec));

        /* Inform GLink core about tx_done */
        glink_if->rx_cmd_tx_done(if_ptr, smd_ch_ctx->rcid, temp_iid, FALSE);

      }
    } while(written == SMDL_CONTINUE || written == 0);
  }

  /* Try to send this pkt if possible, otherwise queue it */
  written = smdl_writev(smd_ch_ctx->smd_handle, &smdl_io, SMDL_WRITE_FLAG_LONG);
  if(written != SMDL_CONTINUE && written != pctx->size && written != 0) {
    ASSERT(0); /* SMDL writev should not fail */
  } else if (written == pctx->size) {

    /* free the smdl_iovec chain */
    smdl_io_cur = smdl_io.next;
    while(smdl_io_cur) {
      smdl_io_temp = smdl_io_cur->next;
      glink_os_free(smdl_io_cur);
      smdl_io_cur = smdl_io_temp;
    }

    /* Inform GLink core about tx_done */
    glink_if->rx_cmd_tx_done(if_ptr, smd_ch_ctx->rcid, pctx->iid, FALSE);
  } else {
    /* SMDL_CONTINUE: save tx pkt contexts */
    smd_ch_ctx->tx_iovec = smdl_io;
    smd_ch_ctx->tx_pkt = *pctx;
  }
  glink_os_cs_release(&smd_ch_ctx->tx_pending_pkt_cs);

  return GLINK_STATUS_SUCCESS;
}

/*===========================================================================
FUNCTION      xport_smd_negotiate_features
===========================================================================*/
/**

  Callback to verify feature set.

  @param[in]  if_ptr        Pointer to transport interface.
  @param[in]  version_ptr   Pointer to version descriptor.
  @param[in]  features      Proposed feature set.

  @return     0.

  @sideeffects  None.

  @dependencies None.
*/
/*=========================================================================*/
uint32 xport_smd_negotiate_features(
  glink_transport_if_type *if_ptr,
  const glink_core_version_type *version_ptr,
  uint32 features)
{
  return 0;
}

/*===========================================================================
FUNCTION      xport_smd_allocate_rx_intent
===========================================================================*/
/**

  Allocates a receive buffer for the local rx intent.

  @param[in]  if_ptr        Pointer to transport interface.
  @param[in]  size          Size of Rx intent.
  @param[in]  intent_ptr    intent pointer to be filled with allocated vector.

  @return     Returns error code.

  @sideeffects  None.

  @dependencies None.
*/
/*=========================================================================*/
glink_err_type xport_smd_allocate_rx_intent(
  glink_transport_if_type *if_ptr,    
  size_t                  size,       
  glink_rx_intent_type    *intent_ptr
)
{
  glink_iovector_type* iovec;

  iovec = (glink_iovector_type*)glink_os_malloc(sizeof(glink_iovector_type) + 
                                  sizeof(glink_iovector_element_type) + size);

  if (iovec == NULL)
  {
    return GLINK_STATUS_OUT_OF_RESOURCES;
  }

  iovec->vlist = (glink_iovector_element_type*)(iovec + 1);
  iovec->vlist->next = NULL;
  iovec->vlast = NULL;
  iovec->vlist->data = (void*)(iovec->vlist + 1);
  iovec->vlist->start_offset = 0;
  iovec->vlist->size = size;
  iovec->plist = NULL;
  iovec->plast = NULL;

  intent_ptr->iovec = iovec;
  intent_ptr->vprovider = &glink_iovec_vprovider;
  intent_ptr->pprovider = &glink_iovec_pprovider;

  return GLINK_STATUS_SUCCESS;
}

/*===========================================================================
FUNCTION      xport_smd_deallocate_rx_intent
===========================================================================*/
/**

  Deallocates a receive buffer for the local rx intent.

  @param[in]  if_ptr        Pointer to transport interface.
  @param[in]  intent_ptr    intent pointer with allocated vector to be freed.

  @return     Returns error code.

  @sideeffects  None.

  @dependencies None.
*/
/*=========================================================================*/
glink_err_type xport_smd_deallocate_rx_intent(
  glink_transport_if_type *if_ptr,    
  glink_rx_intent_type    *intent_ptr
)
{
  glink_os_free(intent_ptr->iovec);

  return GLINK_STATUS_SUCCESS;
}

/*===========================================================================
FUNCTION      xport_smd_tx_cmd_set_sigs
===========================================================================*/
/**

  Sets the local channel signals as per the specified 32-bit mask. SMD XAL
  only takes into account the most significant four bits of this mask and
  set SMD DTR/CTS/CD/RI as per the corresponding bit values of bit# 31, 30
  29 and 28.

  @param[in]  if_ptr        Pointer to transport interface.
  @param[in]  rcid          Local channel ID
  @param[in]  sigs          32 bit signal value

  @return     Returns error code.

  @sideeffects  None.

  @dependencies None.
*/
/*=========================================================================*/
glink_err_type xport_smd_tx_cmd_set_sigs(
  glink_transport_if_type *if_ptr,
  uint32                   lcid,
  uint32                   sigs
)
{
  xport_smd_ch_ctx_type *smd_ch_ctx;
  xport_smd_ctx_type *ctx_ptr = (xport_smd_ctx_type *)if_ptr;

  /* Search for the given SMD channel in the list of open channels */
  smd_ch_ctx = smem_list_first(&ctx_ptr->smd_open_channel_q);
  while((smd_ch_ctx != NULL) && (smd_ch_ctx->lcid != lcid)) {
    smd_ch_ctx = smem_list_next(smd_ch_ctx);
  }
  ASSERT(smd_ch_ctx);

  if( sigs & (1UL << SMD_DTR_SIG_SHFT)) {
    smdl_sig_set(smd_ch_ctx->smd_handle, SMDL_SIG_DTR_DSR, 1);
  } else {
    smdl_sig_set(smd_ch_ctx->smd_handle, SMDL_SIG_DTR_DSR, 0);
  }

    if( sigs & (1 << SMD_CTS_SIG_SHFT)) {
    smdl_sig_set(smd_ch_ctx->smd_handle, SMDL_SIG_RTS_CTS, 1);
  } else {
    smdl_sig_set(smd_ch_ctx->smd_handle, SMDL_SIG_RTS_CTS, 0);
  }

  if( sigs & (1 << SMD_CD_SIG_SHFT)) {
    smdl_sig_set(smd_ch_ctx->smd_handle, SMDL_SIG_CD, 1);
  } else {
    smdl_sig_set(smd_ch_ctx->smd_handle, SMDL_SIG_CD, 0);
  }

  if( sigs & (1 << SMD_RI_SIG_SHFT)) {
    smdl_sig_set(smd_ch_ctx->smd_handle, SMDL_SIG_RI, 1);
  } else {
    smdl_sig_set(smd_ch_ctx->smd_handle, SMDL_SIG_RI, 0);
  }

  return GLINK_STATUS_SUCCESS;
}

/*===========================================================================
FUNCTION      xport_smdi_cntl_ch_cb
===========================================================================*/
/**

  Event callback for SMD cntrol port used for channel migration

  @param[in]  port    smd handle
  @param[in]  event   smd event generated on port
  @param[in]  data    private callback data set by client

  @return     None.

  @sideeffects  None.

  @dependencies None.
*/
/*=========================================================================*/
void xport_smdi_cntl_ch_cb(smdl_handle_type  port,
                           smdl_event_type   event,
                           void             *data)
{
  xport_smd_ctx_type *ctx_ptr = (xport_smd_ctx_type *)data; 
  uint32 pkt_sz, ret=0;
  void *buffer;
  int32 len;
  smdl_iovec_type *temp_iovec;

  switch(event) {
    case SMDL_EVENT_OPEN:
      /* set channel state to open */
      ctx_ptr->xport_smd_cntl_ch_open = TRUE;

      while(ctx_ptr->cmd_iovec != NULL)
	  {	
        ret = smdl_write
              (
                ctx_ptr->xport_smd_cntl_ch_handle,
                ctx_ptr->cmd_iovec->length,
                ctx_ptr->cmd_iovec->buffer,            
                SMDL_WRITE_FLAG_LONG                  
              );

        if(ret!=0)
        {

          temp_iovec = ctx_ptr->cmd_iovec;
          ctx_ptr->cmd_iovec = ctx_ptr->cmd_iovec->next;
          glink_os_free(temp_iovec->buffer);	      
          glink_os_free(temp_iovec);	      
        }
        else
        {
          break;
        }
      }
      break;

    case SMDL_EVENT_READ_PARTIAL:
      break;

    case SMDL_EVENT_READ:
      
      /* poke FIFO to check if packet is available */
      pkt_sz = smdl_rx_peek(ctx_ptr->xport_smd_cntl_ch_handle);

      /* read packets from smd FIFO */
      while(pkt_sz > 0)
      {
      
        /* buffer to get data from FIFO */	
        buffer = glink_os_calloc(pkt_sz);
        ASSERT(buffer);
        
        len = smdl_read( ctx_ptr->xport_smd_cntl_ch_handle,
                         pkt_sz,
                         buffer,
                         0
                        );

        if(len == 0)
        {
          /* Full packet is not available */
          glink_os_free(buffer);		
          break;
        }
        else if(len < 0)
        {
          /* smdl_read will return -ve error code
           * if it fails */
          ASSERT(0);
        }

        /* process the command received from remote side */
        xport_smdi_process_cmd(ctx_ptr, (xport_smd_cmd)XPORT_SMD_GET_CMD(buffer), (uint32 *)buffer, len);
        /* command has been processed. free the buffer */
        glink_os_free(buffer);

        /* check if more packets are avilable in FIFO */
        pkt_sz = smdl_rx_peek(ctx_ptr->xport_smd_cntl_ch_handle);

      }
      break;

    case SMDL_EVENT_WRITE:

      while(ctx_ptr->cmd_iovec != NULL)
      {
        ret = smdl_write 
                (
                  ctx_ptr->xport_smd_cntl_ch_handle,
                  ctx_ptr->cmd_iovec->length,
                  ctx_ptr->cmd_iovec->buffer,
                  SMDL_WRITE_FLAG_LONG
                );
            
        if(ret!=0)
        {
          temp_iovec = ctx_ptr->cmd_iovec;
          ctx_ptr->cmd_iovec = ctx_ptr->cmd_iovec->next;
          glink_os_free(temp_iovec->buffer);	      
          glink_os_free(temp_iovec);	      
        }
        else
        {
          break;
        }
      }
      
      break;

    case SMDL_EVENT_CLOSE_COMPLETE:
      XPORT_SMD_LOG("contorol channel closed", "adsp",0);	
      break;

    case SMDL_EVENT_REMOTE_CLOSE:
      XPORT_SMD_LOG("contorol channel remote close", "adsp",0);	
      break;

    case SMDL_EVENT_DSR:
      break;

    case SMDL_EVENT_CTS:
      break;

    case SMDL_EVENT_CD:
      break;

    case SMDL_EVENT_RI:
      break;

    default:
      /* what to do ?? */
      break;
  }
}

/*===========================================================================
FUNCTION      xport_smd_ssr
===========================================================================*/
/**

  Process SSR event on the given interface

  @param[in]  if_ptr        Pointer to transport interface.

  @return     Returns error code.

  @sideeffects  None.

  @dependencies None.
*/
/*=========================================================================*/
glink_err_type xport_smd_ssr(
  glink_transport_if_type *if_ptr
)
{
#if 0 /* clients should call glink_close and this should happen automatically */
  xport_smd_ch_ctx_type *smd_ch_ctx, *smd_ch_ctx_temp;
  xport_smd_ctx_type *ctx_ptr = (xport_smd_ctx_type *)if_ptr;

  /* Search for the given SMD channel in the list of open channels */
  smd_ch_ctx = smem_list_first(&ctx_ptr->smd_open_channel_q);
  while((smd_ch_ctx != NULL)) {
    smd_ch_ctx_temp = smem_list_next(smd_ch_ctx);
    smem_list_delete(&ctx_ptr->smd_open_channel_q, smd_ch_ctx);
    glink_os_free(smd_ch_ctx);
    smd_ch_ctx = smd_ch_ctx_temp;
  }
#endif
  return GLINK_STATUS_SUCCESS;
}

/*===========================================================================
FUNCTION      xport_smd_poll
===========================================================================*/
/**

  Trigger a poll for any rx data / signals on transport

  @param[in]  if_ptr        Pointer to transport interface.

  @return     Returns error code.

  @sideeffects  None.

  @dependencies None.
*/
/*=========================================================================*/
extern void smd_isr( void *data );

glink_err_type xport_smd_poll(
  glink_transport_if_type *if_ptr
)
{
  /* Fake trigger smd interrupt handler */
  smd_isr((void*)SMEM_RPM);
  return GLINK_STATUS_SUCCESS;
}

/*===========================================================================
FUNCTION      xport_smd_mask_rx_irq
===========================================================================*/
/**

  Mask/Unmask rx interrupt associated with transport

  @param[in]  if_ptr        Pointer to transport interface.

  @return     Returns error code.

  @sideeffects  None.

  @dependencies None.
*/
/*=========================================================================*/
glink_err_type xport_smd_mask_rx_irq(
  glink_transport_if_type *if_ptr,
  boolean                 mask
)
{
  if(mask) {
    smd_disable_intr(SMEM_RPM);
  } else {
    smd_enable_intr(SMEM_RPM);
  }
  return GLINK_STATUS_SUCCESS;
}


#ifdef FEATURE_XPORT_SMD_TASK
/*===========================================================================
FUNCTION      xport_smd_event_cb
===========================================================================*/
/**

  Event callback for SMD transitional transport.

  @param[in]  smd_ch_ctx   Pointer to SMD channel context.

  @return     None.

  @sideeffects  None.

  @dependencies None.
*/
/*=========================================================================*/
void xport_smd_event_cb(smdl_handle_type  port,
                        smdl_event_type   event,
                        void             *data)
{
  xport_smd_thread_data_type *thread_data = NULL;

  /* dequeue a free event */
  glink_os_cs_acquire(&xport_smd_thread_ctx.event_q_cs);

  thread_data = smem_list_first(&xport_smd_thread_ctx.event_free_q);
  ASSERT(thread_data);
  smem_list_delete(&xport_smd_thread_ctx.event_free_q, thread_data);
  glink_os_cs_release(&xport_smd_thread_ctx.event_q_cs);

  thread_data->smdl_event = event;
  thread_data->smd_ch_ctx = (xport_smd_ch_ctx_type*)data;
  /* queue the event to the event processing list */
  glink_os_cs_acquire(&xport_smd_thread_ctx.event_q_cs);
  smem_list_append(&xport_smd_thread_ctx.event_q, thread_data);
  glink_os_cs_release(&xport_smd_thread_ctx.event_q_cs);

  /* signal the thread */
  glink_os_event_signal(&xport_smd_thread_ctx.event);
}
#endif //FEATURE_XPORT_SMD_TASK

/*===========================================================================
FUNCTION      xport_smd_event_cb_thread
===========================================================================*/
/**

  Thread to process event callback for SMD transitional transport.

  @param[in]  smd_ch_ctx   Pointer to SMD channel context.

  @return     None.

  @sideeffects  None.

  @dependencies None.
*/
/*=========================================================================*/
void xport_smd_event_cb_thread(smdl_handle_type  port,
                               smdl_event_type   event,
                               void             *data)
{
  int written;
  uint32 temp_iid;
  xport_smd_ch_ctx_type   *smd_ch_ctx = (xport_smd_ch_ctx_type*)data;
  xport_smd_ctx_type      *ctx_ptr    = smd_ch_ctx->smd_xport_ctx;
  glink_transport_if_type *if_ptr     = (glink_transport_if_type*)ctx_ptr;
  glink_core_if_type      *glink_if   = if_ptr->glink_core_if_ptr;
  
  smdl_iovec_type *smdl_io_cur, *smdl_io_temp;

  switch(event) {
    case SMDL_EVENT_OPEN:
      
      if(smd_ch_ctx->xport_smd_is_cntl_remote_open == FALSE)
      {
      
        smd_ch_ctx->rcid = ctx_ptr->max_rcid;
        ctx_ptr->max_rcid--;

        /* Trigger a remote_open call into glink core */ 
        smd_ch_ctx->remote_close = FALSE;
        xport_smd_notify_event( smd_ch_ctx,
                                XPORT_SMD_OPEN,
                                if_ptr->glink_priority );
      }
      break;

    case SMDL_EVENT_READ_PARTIAL:
      /* Check to see if we were already reading a pkt */
      xport_smdi_read_pkt(XPORT_SMD_READ_PARTIAL, smd_ch_ctx);
      break;

    case SMDL_EVENT_READ:
      xport_smdi_read_pkt(XPORT_SMD_READ, smd_ch_ctx);
      break;

    case SMDL_EVENT_WRITE:
      if(smd_ch_ctx->xport_capabilities & GLINK_CAPABILITY_INTENTLESS) {
        return;
      }
      /* Try to write pending pkt */
      glink_os_cs_acquire(&smd_ch_ctx->tx_pending_pkt_cs);
      if(smd_ch_ctx->tx_iovec.buffer == NULL) {
        /* It is possible that the pending tx pkt got transmitted by
           xport_smd_tx() if client tried to send a new pkt */
        glink_os_cs_release(&smd_ch_ctx->tx_pending_pkt_cs);
        break;
      }
      written = smdl_writev(smd_ch_ctx->smd_handle, &smd_ch_ctx->tx_iovec, SMDL_WRITE_FLAG_LONG);
      if(written != SMDL_CONTINUE && written != smd_ch_ctx->tx_pkt.size && written != 0) {
        ASSERT(0); /* SMDL writev should not fail */
      } else if (written == smd_ch_ctx->tx_pkt.size) {

        /* free the smdl_iovec chain */
        smdl_io_cur = smd_ch_ctx->tx_iovec.next;
        while(smdl_io_cur) {
          smdl_io_temp = smdl_io_cur->next;
          glink_os_free(smdl_io_cur);
          smdl_io_cur = smdl_io_temp;
        }
        /* Clear tx pkt structures */
        temp_iid = smd_ch_ctx->tx_pkt.iid;
        memset(&smd_ch_ctx->tx_pkt, 0, sizeof(smd_ch_ctx->tx_pkt));
        memset(&smd_ch_ctx->tx_iovec, 0, sizeof(smd_ch_ctx->tx_iovec));

        /* Inform GLink core about tx_done */
        glink_if->rx_cmd_tx_done(if_ptr, smd_ch_ctx->rcid, temp_iid, FALSE);

      }
      glink_os_cs_release(&smd_ch_ctx->tx_pending_pkt_cs);
      break;

    case SMDL_EVENT_CLOSE_COMPLETE:
      /* Fake-ACK the close request. Note that Glink core might get a 
       * remote_close before this ACK if this thread gets descheduled */
      xport_smd_notify_event( smd_ch_ctx,
                              XPORT_SMD_CLOSE_ACK,
                              GLINK_INVALID_PRIORITY );
      break;

    case SMDL_EVENT_REMOTE_CLOSE:
      /* Channel close was initiated by remote side, fake generate
       * remote_close to glink core */
      /* HACK: SMDLite has some race because of which it may deliever
         REMOTE_CLOSE event twice */
      if(!smd_ch_ctx->remote_close) 
      {
        smd_ch_ctx->remote_close                   = TRUE;
        smd_ch_ctx->xport_smd_is_cntl_remote_close = FALSE;
        xport_smd_notify_event( smd_ch_ctx, 
                                XPORT_SMD_CLOSE,
                                GLINK_INVALID_PRIORITY );
      }
      break;

    case SMDL_EVENT_DSR:
      {
        uint32 curr_ctrl_sigs;
        int sig = smdl_sig_get(smd_ch_ctx->smd_handle, SMDL_SIG_DTR_DSR);
        ASSERT(sig >= 0);
        if(sig) {
          curr_ctrl_sigs = smd_ch_ctx->prev_remote_sigs | (sig << SMD_DTR_SIG_SHFT);
        } else {
          curr_ctrl_sigs = smd_ch_ctx->prev_remote_sigs & (~(sig << SMD_DTR_SIG_SHFT));
        }
        smd_ch_ctx->prev_remote_sigs = curr_ctrl_sigs;
        /* send event to glink */
        xport_smd_notify_event( smd_ch_ctx,
                                XPORT_SMD_SIG_BIT_CHANGE,
                                GLINK_INVALID_PRIORITY );
      }
      break;
    case SMDL_EVENT_CTS:
      {
        uint32 curr_ctrl_sigs;
        int sig = smdl_sig_get(smd_ch_ctx->smd_handle, SMDL_SIG_RTS_CTS);
        ASSERT(sig >= 0);
        if(sig) {
          curr_ctrl_sigs = smd_ch_ctx->prev_remote_sigs | (sig << SMD_CTS_SIG_SHFT);
        } else {
          curr_ctrl_sigs = smd_ch_ctx->prev_remote_sigs & (~(sig << SMD_CTS_SIG_SHFT));
        }
        smd_ch_ctx->prev_remote_sigs = curr_ctrl_sigs;
        /* send event to glink */
        xport_smd_notify_event( smd_ch_ctx,
                                XPORT_SMD_SIG_BIT_CHANGE,
                                GLINK_INVALID_PRIORITY );
      }
      break;
    case SMDL_EVENT_CD:
      {
        uint32 curr_ctrl_sigs;
        int sig = smdl_sig_get(smd_ch_ctx->smd_handle, SMDL_SIG_CD);
        ASSERT(sig >= 0);
        if(sig) {
          curr_ctrl_sigs = smd_ch_ctx->prev_remote_sigs | (sig << SMD_CD_SIG_SHFT);
        } else {
          curr_ctrl_sigs = smd_ch_ctx->prev_remote_sigs & (~(sig << SMD_CD_SIG_SHFT));
        }
        smd_ch_ctx->prev_remote_sigs = curr_ctrl_sigs;
        /* send event to glink */
        xport_smd_notify_event( smd_ch_ctx,
                                XPORT_SMD_SIG_BIT_CHANGE,
                                GLINK_INVALID_PRIORITY );
      }
      break;
    case SMDL_EVENT_RI:
      {
        uint32 curr_ctrl_sigs;
        int sig = smdl_sig_get(smd_ch_ctx->smd_handle, SMDL_SIG_RI);
        ASSERT(sig >= 0);
        if(sig) {
          curr_ctrl_sigs = smd_ch_ctx->prev_remote_sigs | (sig << SMD_RI_SIG_SHFT);
        } else {
          curr_ctrl_sigs = smd_ch_ctx->prev_remote_sigs & (~(sig << SMD_RI_SIG_SHFT));
        }
        smd_ch_ctx->prev_remote_sigs = curr_ctrl_sigs;
        /* send event to glink */
        xport_smd_notify_event( smd_ch_ctx,
                                XPORT_SMD_SIG_BIT_CHANGE,
                                GLINK_INVALID_PRIORITY );
      }
      break;
    default:
      /* what to do ?? */
      break;
  }
}
#ifdef FEATURE_XPORT_SMD_TASK
void xport_smd_thread(void *param)
{
  xport_smd_thread_data_type *thread_data = NULL;
  while(1) {
    glink_os_event_wait(&xport_smd_thread_ctx.event);
    glink_os_event_clear(&xport_smd_thread_ctx.event);

    thread_data = smem_list_first(&xport_smd_thread_ctx.event_q);
    while(thread_data != NULL) {
      /* dequeue commands from the cmd queue and process them */
      glink_os_cs_acquire(&xport_smd_thread_ctx.event_q_cs);
      smem_list_delete(&xport_smd_thread_ctx.event_q, thread_data);
      glink_os_cs_release(&xport_smd_thread_ctx.event_q_cs);

      /* Call the smdl_callback to process data */
      xport_smd_event_cb_thread(thread_data->smd_ch_ctx->smd_handle, 
                                thread_data->smdl_event, 
                                thread_data->smd_ch_ctx);
      /* queue event back to free queue */
      glink_os_cs_acquire(&xport_smd_thread_ctx.event_q_cs);
      smem_list_append(&xport_smd_thread_ctx.event_free_q, thread_data);
      glink_os_cs_release(&xport_smd_thread_ctx.event_q_cs);

      thread_data = smem_list_first(&xport_smd_thread_ctx.event_q);
    }
  }
}
#endif //FEATURE_XPORT_SMD_TASK

/*===========================================================================
FUNCTION      xport_smd_init
===========================================================================*/
/**

  Initializes SMD transitional transport.
  Must be called before any other operations are done.

  @param[in]  arg   The argument.

  @return     Returns error code.

  @sideeffects  None.
*/
/*=========================================================================*/
glink_err_type xport_smd_init(void *arg)
{
  uint32 ind;
  glink_core_transport_cfg_type xport_smd_cfg = { 0 };
  smdl_handle_type smdl_handle = NULL;
  if (xport_smd_config_num == 0)
  {
    return GLINK_STATUS_SUCCESS;
  }

  xport_smd_ctx = glink_os_calloc(sizeof(xport_smd_ctx_type)*xport_smd_config_num);

  ASSERT(xport_smd_ctx != NULL);

  /* Initialize supported version and features */
  xport_smd_version.version = SMD_XPORT_VERSION;
  xport_smd_version.features = SMD_XPORT_FEATURES;
  xport_smd_version.negotiate_features = &xport_smd_negotiate_features;

  /* Initialize event context */
  smem_list_init( &xport_smd_event_ctx.smd_event_q );
  smem_list_init( &xport_smd_event_ctx.smd_event_free_q );
  
  glink_os_cs_init( &xport_smd_event_ctx.smd_event_q_cs );
  glink_os_cs_init( &xport_smd_event_ctx.smd_event_free_q_cs );

#ifndef FEATURE_THREADLESS_ENV  
  glink_os_thread_start( xport_smd_event_worker_thread, 
                         NULL,
                         "XPORT_SMD_EVENT_WORKER" );
  glink_os_event_init( &xport_smd_event_ctx.smd_worker_thread_event );
#endif  
  /* Initialize edges */

  for (ind = 0; ind < xport_smd_config_num; ind++)
  {
    xport_smd_ctx[ind].pcfg = xport_smd_get_config(ind);//&xport_smem_config[ind];
#ifndef FEATURE_CH_MIGRATION_FREE   
    smdl_handle = smdl_open( "GLINK_CTRL",
                              xport_smd_ctx[ind].pcfg->edge,
                              SMDL_OPEN_FLAGS_PRIORITY_HIGH,
                              1024,
                              xport_smdi_cntl_ch_cb,
                              &xport_smd_ctx[ind]
	                          );
     
    if(smdl_handle == NULL)
    {
      XPORT_SMD_LOG("GLINK_CTRL open failed", "apss", NULL);	    
    }
#endif
    /* assign control channel handle to xport context */
    xport_smd_ctx[ind].xport_smd_cntl_ch_handle = smdl_handle;

    /* Initialize context */
    smem_list_init(&xport_smd_ctx[ind].smd_open_channel_q);
    glink_os_cs_init(&xport_smd_ctx[ind].smd_open_channel_q_cs);

    /* Initialize GLink transport interface */
    xport_smd_ctx[ind].xport_if.tx_cmd_version = &xport_smd_tx_cmd_version;
    xport_smd_ctx[ind].xport_if.tx_cmd_version_ack = &xport_smd_tx_cmd_version_ack;
    xport_smd_ctx[ind].xport_if.set_version = &xport_smd_set_version;
    xport_smd_ctx[ind].xport_if.tx_cmd_ch_open = &xport_smd_tx_cmd_ch_open;
    xport_smd_ctx[ind].xport_if.tx_cmd_ch_close = &xport_smd_tx_cmd_ch_close;
    xport_smd_ctx[ind].xport_if.tx_cmd_ch_remote_open_ack = &xport_smd_tx_cmd_ch_remote_open_ack;
    xport_smd_ctx[ind].xport_if.tx_cmd_ch_remote_close_ack = &xport_smd_tx_cmd_ch_remote_close_ack;
    xport_smd_ctx[ind].xport_if.tx_cmd_local_rx_intent = &xport_smd_tx_cmd_local_rx_intent;
    xport_smd_ctx[ind].xport_if.tx_cmd_local_rx_done = &xport_smd_tx_cmd_local_rx_done;
    xport_smd_ctx[ind].xport_if.tx_cmd_rx_intent_req = &xport_smd_tx_cmd_rx_intent_req;
    xport_smd_ctx[ind].xport_if.tx_cmd_remote_rx_intent_req_ack = &xport_smd_tx_cmd_remote_rx_intent_req_ack;
    xport_smd_ctx[ind].xport_if.tx = &xport_smd_tx;
    xport_smd_ctx[ind].xport_if.allocate_rx_intent = &xport_smd_allocate_rx_intent;
    xport_smd_ctx[ind].xport_if.deallocate_rx_intent = &xport_smd_deallocate_rx_intent;
    xport_smd_ctx[ind].xport_if.tx_cmd_set_sigs = &xport_smd_tx_cmd_set_sigs;
    xport_smd_ctx[ind].xport_if.ssr = &xport_smd_ssr;
    xport_smd_ctx[ind].xport_if.poll = &xport_smd_poll;
    xport_smd_ctx[ind].xport_if.mask_rx_irq = &xport_smd_mask_rx_irq;

    /* initialize max rcid for xport smd */
    xport_smd_ctx[ind].max_rcid = 0x10000;

    /* TODO: glink transport priority */
    xport_smd_ctx[ind].xport_if.glink_priority = GLINK_XPORT_SMD;

    /* Setup GLink configuration */
    xport_smd_cfg.name = XPORT_SMD_NAME;
    xport_smd_cfg.remote_ss = xport_smd_ctx[ind].pcfg->remote_ss;
    xport_smd_cfg.version = &xport_smd_version;
    xport_smd_cfg.version_count = 1;
    xport_smd_cfg.max_cid = 0x10000;
    xport_smd_cfg.max_iid = 0x10000;

    if (glink_core_register_transport(&xport_smd_ctx[ind].xport_if, &xport_smd_cfg) !=
        GLINK_STATUS_SUCCESS)
    {
      /* Registration failed, set index to invalid. */
      xport_smd_ctx[ind].pcfg = NULL;
      continue;
    }
    /* invoke link-up notification */
    xport_smd_ctx[ind].xport_if.glink_core_if_ptr->link_up(&xport_smd_ctx[ind].xport_if);
  }
#ifdef FEATURE_XPORT_SMD_TASK
  glink_os_event_init(&xport_smd_thread_ctx.event);
  smem_list_init(&xport_smd_thread_ctx.event_q);
  smem_list_init(&xport_smd_thread_ctx.event_free_q);
  glink_os_cs_init(&xport_smd_thread_ctx.event_q_cs);
  for(ind = 0; ind < XPORT_SMD_NUM_EVENTS; ind++) {
    smem_list_append(&xport_smd_thread_ctx.event_free_q, &xport_smd_thread_data[ind]);
  }

  /* Start a thread to process SMDL callabck for clients */
  /* OS has full freedom as to what to do in this function to 
     start a thread, it should just end up calling the thread
     implementation provided as the parameter */
  glink_os_thread_start(xport_smd_thread, NULL, "xport_smd_thread");
#endif //FEATURE_XPORT_SMD_TASK
  return GLINK_STATUS_SUCCESS;
}
