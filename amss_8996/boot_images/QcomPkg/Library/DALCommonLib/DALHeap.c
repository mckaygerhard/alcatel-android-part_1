/** @file DALHeap.c

  DAL Heap

  Copyright (c) 2015, Qualcomm Technologies Inc. All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 09/17/15   aa      Ported memheap_lite from IMC to XBL 
 03/27/15   unr     64 bit fixes
 08/11/14   sho     Creation

=============================================================================*/
#include "DALStdDef.h"
#include "DALHeap.h"
#include "DALSys.h" 
#include "memheap_lite.h"

/*
* Heap is initialized in two stages. At Initial stage dal heap is set
* to immem heap and latter on switched to DDR when available.
*/

typedef struct 
{
    mem_heap_type heap_struct;
    uint8_t initialized;
}DALSYSMemState;

static DALSYSMemState memState[DALHEAP_NUM] = 
   {{{0}, FALSE /*heap initialized flag */}, 
    {{0}, FALSE /*heap initialized flag */}}; 

VOID DALSYS_HeapInit(VOID *pHeapBase, UINT32 dwHeapSz, 
      DALHEAP_MEM_TYPE memType ) 
{
    if (DALHEAP_NUM <= memType || memState[memType].initialized)
    {
       // Invalid parameters, or heap is already initialized
       return;
    } 

    // Set flag to indicate that we have initialized this heap
    memState[memType].initialized = 0x1;
    // Initialize heap structure with all zeros
    memset(&memState[memType].heap_struct, 0, sizeof(mem_heap_type));
    // Initialize the heap
    mem_init_heap(&memState[memType].heap_struct, pHeapBase, dwHeapSz);
}

UINT32 DALSYS_HeapExternalMemIsReady(VOID)
{
   return memState[DALHEAP_EXT_MEM].initialized;
}

DALResult
DALSYS_Malloc(UINT32 dwSize, VOID **ppMem)
{
   DALHEAP_MEM_TYPE memType = (DALSYS_HeapExternalMemIsReady()) ? DALHEAP_EXT_MEM: DALHEAP_IMEM;
    
   // If the pointer to the pointer is null, skip it
   if(ppMem != NULL)
   {
       // Call into mem_malloc
       *ppMem = mem_malloc(&memState[memType].heap_struct, dwSize);
       if(*ppMem != NULL)
       {
           return DAL_SUCCESS;
       }
   }

   return DAL_ERROR;
}

DALResult
DALSYS_Free(VOID *pmem)
{
   // Free the memory

   // Check to see which heap this pointer came from (and if that heap was initialized)
   if(memState[DALHEAP_IMEM].initialized && \
      ((pmem >= (void*) memState[DALHEAP_IMEM].heap_struct.first_block) && \
       (pmem < (((void*) memState[DALHEAP_IMEM].heap_struct.first_block) + memState[DALHEAP_IMEM].heap_struct.total_bytes))))
   {
        mem_free(&memState[DALHEAP_IMEM].heap_struct, pmem);
   }
   else if(memState[DALHEAP_EXT_MEM].initialized && \
           ((pmem >= (void*) memState[DALHEAP_EXT_MEM].heap_struct.first_block) && 
            (pmem < (((void*) memState[DALHEAP_EXT_MEM].heap_struct.first_block) + memState[DALHEAP_EXT_MEM].heap_struct.total_bytes))))
   {
        mem_free(&memState[DALHEAP_EXT_MEM].heap_struct, pmem);
   }
   // No failures returned from mem_free
   return DAL_SUCCESS;
}
