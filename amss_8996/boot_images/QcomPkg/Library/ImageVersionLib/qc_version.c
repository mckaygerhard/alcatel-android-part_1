/*
#============================================================================
#  Name:                                                                     
#    qc_version.c 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2016 by QUALCOMM, Incorporated.  All Rights Reserved.        
#============================================================================
#                                                                            
# *** AUTO GENERATED FILE - DO NOT EDIT                                      
#                                                                            
# GENERATED: Fri Aug 19 02:35:49 2016 
#============================================================================
*/
const char QC_IMAGE_VERSION_STRING_AUTO_UPDATED[]="QC_IMAGE_VERSION_STRING=BOOT.XF.1.0-00306";
