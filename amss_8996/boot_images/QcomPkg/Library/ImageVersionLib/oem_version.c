/*
#============================================================================
#  Name:                                                                     
#    oem_version.c 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2016 by QUALCOMM, Incorporated.  All Rights Reserved.        
#============================================================================
#                                                                            
# *** AUTO GENERATED FILE - DO NOT EDIT                                      
#                                                                            
# GENERATED: Fri Aug 19 02:35:49 2016 
#============================================================================
*/
const char IMAGE_VARIANT_STRING_AUTO_UPDATED[]="IMAGE_VARIANT_STRING=M8996LAB";
const char OEM_IMAGE_VERSION_STRING_AUTO_UPDATED[]="OEM_IMAGE_VERSION_STRING=crm-ubuntu39";
