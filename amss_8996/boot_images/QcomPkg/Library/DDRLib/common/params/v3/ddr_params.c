/*
===========================================================================

FILE:         ddr_params.c

DESCRIPTION:
  Information of DDR devices supported by target.

===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------
03/12/14   sr      Initial revision.

===========================================================================
                   Copyright 2014 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
===========================================================================
*/

/*==============================================================================
                                    INCLUDES
==============================================================================*/
#include "ddr_params.h"
#include "crc.h"
#include "ddr_log.h"
#include "dev_flash_params.h"
#define USE_LOADER_UTILS
#include "LoaderUtils.h"

/*==============================================================================
                                      DATA
==============================================================================*/
/* Pointer to DDR CDT header */
static struct ddr_device_params_header_v2 *ddr_device_header = NULL;

/* pointer to extended cdt in memory, updated by ddr_set_params() */
static EXTENDED_CDT_STRUCT *ddr_ecdt_ptr = NULL;

/* Pointer to DDR device table, updated by ddr_set_params() */
static ddr_device_params *ddr_device_table = NULL;

/* Pointer to shared imem DDR device table, this table is valid after ddr initialization is done */
static struct ddr_device_params_header_v2 *shared_ddr_device_table_header =
                              (struct ddr_device_params_header_v2 *)SHARED_IMEM_DDR_PARAM_BASE;

static ddr_device_params *shared_ddr_device_table =
   (ddr_device_params *)(SHARED_IMEM_DDR_PARAM_BASE + sizeof(struct ddr_device_params_header_v2));

/* Size of DDR device table entry, updated by ddr_set_params() */
static uint32 ddr_device_table_header_size = 0;

/* Size of extended cdt table entry, updated by ddr_set_params() */
static uint32 ddr_ecdt_size = 0;

/* Number of entries in DDR device table, updated by ddr_set_params() */
static uint32 ddr_device_table_entry_num = 0;

/* Size of DDR device table entry, updated by ddr_set_params() */
static uint32 ddr_device_table_entry_size = 0;

/* DDR device parameters of CH0 and CH1 */
static ddr_device_params *ddr_device_params_ch0 = NULL;
static ddr_device_params *ddr_device_params_ch1 = NULL;

/* This addr is defined in target build files */
//DDR_STRUCT local_alloc = {0};
//DDR_STRUCT *ddrsns_share_data = &local_alloc;
DDR_STRUCT *ddrsns_share_data = (DDR_STRUCT *)DDR_GLOBAL_STRUCT_DATARAM_ADDR;


/*==============================================================================
                                   FUNCTIONS
==============================================================================*/


/* ============================================================================
**  Function : ddr_set_params
** ============================================================================
*/
/**
*   @brief
*   This function sets the DDR driver's device table to an external device table.
*   It will also update the interleaving mode information according to the parameter
*   header that's passed in.
*   User should append ddr_device_params_header_v1 header in front of their own DDR device 
*   table and pass  a pointer points to the beginning of this header via the this API to 
*   force DDR driver to utilize the user defined parameters.
*   The user defined ddr device table must contain a minimal number of entries to 
*   match the system's ddr devices.
*   For example if a system has two ddr devices attached then the external table must 
*   define at least two entries to match those two devices.
*   However, more entries can be added to the end of table to support other ddr devices
*   with different parameters.
*
*   @param[in]  ddr_params_ptr   Pointer to the external ddr parameters. It should
*                                point to the user defined ddr device table with ddr
*                                parameters header appended in front. This pointer must
*                                points to a 4 bytes aligned address.
*                         
*
*   @return
*   TRUE if DDR device table is updated, FALSE if an error happens and this operation
*   is aborted
*
*   @dependencies
*   Must be called prior to ddr_initialize_device. 
*   This API will take no effect after ddr_initialize_device
*
*   @sa
*   None
*
*   @sa
*   None
*/
boolean ddr_set_params(void *ddr_params_ptr, uint32 ddr_params_size)
{
  boolean status = FALSE;
  uint32 *ddr_params_header_version = NULL;
  size_t temp;

  do
  {
    /*First check to see if external ddr parameters exists*/
    if(ddr_params_ptr == NULL || ddr_params_size == 0)
    {
      break;
    }

    /*We know the first byte in ddr parameters data is its version number*/
    ddr_params_header_version = (uint32 *)ddr_params_ptr;

    if(*ddr_params_header_version == 2)
    {
      /*The beginning of external DDR parameters is a header which tells the
        number of interfaces and size of ddr device table entry*/
      ddr_device_header = (struct ddr_device_params_header_v2 *)
                          ddr_params_ptr;
      ddr_device_table_header_size = DDR_PARAMS_HEADER_V2_SIZE;

      /*Check if the magic number in the header is correct*/
      if(ddr_device_header->magic_number != DDR_PARAMS_MAGIC_NUM)
      {
        break;
      }

      /* parse for extended CDT params */
      ddr_ecdt_size = sizeof(EXTENDED_CDT_STRUCT);
      ddr_ecdt_ptr = (EXTENDED_CDT_STRUCT *) ((size_t)ddr_params_ptr + ddr_device_table_header_size);

      /*Update the device table pointer*/
      /*The actual device table is right after the ddr parameter header*/
      ddr_device_table = (ddr_device_params *)
                         ((size_t)ddr_params_ptr + ddr_device_table_header_size + ddr_ecdt_size);

      /* Update number of device table entries*/
      ddr_device_table_entry_num = ddr_device_header->num_of_device;

      /* Update size of device table entry */
      switch (ddr_device_table[0].common.device_type)
      {
        case DDR_TYPE_LPDDR2:
        case DDR_TYPE_LPDDR3:
      	case DDR_TYPE_LPDDR4:
      	      	ddr_device_table_entry_size = sizeof(struct ddr_device_params_lpddr);
                      if(ddr_device_table[0].common.device_type == DDR_TYPE_LPDDR4) {
      				
      		ddr_device_params_ch0 = (ddr_device_params *)
                                      ((size_t)ddr_device_table );
      		temp = ((size_t)ddr_device_table + ddr_device_table_entry_size);
                      ddr_device_params_ch1 = (ddr_device_params *)temp;
      		
            if (PcdGet32 (PcdBuildType) != 5)
            {
                /* memset the 5KB dataram pointer to zero */	
                memset(ddrsns_share_data, 0x0, sizeof(DDR_STRUCT));
              
            }
            else
            {
                if (ddrsns_share_data->ddi_buf[0] != 0xE12A3456)// DDI came out of training
                {
                    /* memset the 5KB dataram pointer to zero */	
                    memset(ddrsns_share_data, 0x0, (sizeof(DDR_STRUCT)-sizeof(ddrsns_share_data->ddi_buf)));
                    
                }
                


            }
      		ddrsns_share_data->cdt_params[0] = *ddr_device_params_ch0; 
            ddrsns_share_data->cdt_params[1] = *ddr_device_params_ch1;
      }
       break;

        case DDR_TYPE_PCDDR3:
          ddr_device_table_entry_size = sizeof(struct ddr_device_params_pcddr3);
          break;

        default:
          ddr_device_table_entry_size = 0;
          break;
      }

      /* Check if size of device table entry matches size written in header */
      if (ddr_device_table_entry_size == 0 ||
          ddr_device_table_entry_size != ddr_device_header->size_of_param)
      {
        break;
      }
    }
    else
    {
      /* Currently only Version 2 is supported */
      break;
    }

    status = TRUE;

  } while(0);

  return status;
}

/* ============================================================================
**  Function : ddr_get_header
** ============================================================================
*/
/**
*   @brief
*   Get DDR device parameters header. This contains useful information such as
*   interleave mode, optional features, etc that applies to all ddr devices
*
*   @param[in]  None
*
*   @return
*   DDR device header
*
*   @dependencies
*   None
*
*   @sa
*   None
*
*   @sa
*   None
*/
struct ddr_device_params_header_v2 *ddr_get_header(void)
{
#ifdef BUILD_BOOT_CHAIN
    /* Use external table for Boot */
    return ddr_device_header;
#else
    /* Use shared IMEM table for RPM */
    return shared_ddr_device_table_header;
#endif
} /* ddr_get_header */

/* ============================================================================
**  Function : ddr_get_params
** ============================================================================
*/
/**
*   @brief
*   Get DDR device parameters.
*
*   @param[in]  interface_name  Interface to get DDR device parameters for
*
*   @return
*   DDR device parameters
*
*   @dependencies
*   None
*
*   @sa
*   None
*
*   @sa
*   None
*/
ddr_device_params *ddr_get_params(DDR_CHANNEL interface_name)
{
  ddr_device_params *device_params;

  device_params = (interface_name == DDR_CH0) ? &(ddrsns_share_data->cdt_params[0])
                                                        : &(ddrsns_share_data->cdt_params[1]);

   return device_params;
} /* ddr_get_params */

/* ============================================================================
**  Function : ddr_get_extended_cdt_params
** ============================================================================
*/
/**
*   @brief
*   Get DDR device parameters.
*
*   @param[in]  interface_name  Interface to get DDR device parameters for
*
*   @return
*   DDR device parameters
*
*   @dependencies
*   None
*
*   @sa
*   None
*
*   @sa
*   None
*/
void *ddr_get_extended_cdt_params(void)
{
  if(ddr_ecdt_size != 0)
  {
    return (void *)ddr_ecdt_ptr;
  }
  else
  {
    return NULL;
  }
} /* ddr_get_params */

/* =============================================================================
**  Function : ddr_params_detection
** =============================================================================
*/
/**
*   @brief
*   Detect correct DDR device parameters based on manufacture and dimension
*   information.
*
*   @param[in]  interface_name  Interface to detect for
*
*   @retval  TRUE   Parameters detected
*   @retval  FALSE  Parameters not dectected
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_detection(DDR_CHANNEL interface_name)
{
  struct ddr_device_params_common *target_params, *source_params;
  uint32 entry, entry_offset;

  /* Get current device parameters to be matched */
  target_params = &(ddr_get_params(interface_name)->common);

  /* Skip first two entries of device table */
  entry = 2;
  entry_offset = 2 * ddr_device_table_entry_size;

  while (entry < ddr_device_table_entry_num)
  {
    source_params = &(( (ddr_device_params *)
                        ((size_t)ddr_device_table + entry_offset) )->common); // uint32 srini

    if(source_params->manufacture_name == target_params->manufacture_name &&
       source_params->num_rows_cs0 == target_params->num_rows_cs0 &&
       source_params->num_cols_cs0 == target_params->num_cols_cs0 &&
       source_params->num_banks_cs0 == target_params->num_banks_cs0 &&
       source_params->num_rows_cs1== target_params->num_rows_cs1 &&
       source_params->num_cols_cs1 == target_params->num_cols_cs1 &&
       source_params->num_banks_cs1 == target_params->num_banks_cs1 &&
       source_params->interface_width_cs0 == target_params->interface_width_cs0 &&
       source_params->interface_width_cs1 == target_params->interface_width_cs1)
    {
      if (interface_name == DDR_CH0)
      {
        ddr_device_params_ch0 = (ddr_device_params *)
                                ((size_t)ddr_device_table + entry_offset);
      }
      else
      {
        ddr_device_params_ch1 = (ddr_device_params *)
                                ((size_t)ddr_device_table + entry_offset);
      }
      break;
    }

    entry++;
    entry_offset += ddr_device_table_entry_size;
  }

  return (entry < ddr_device_table_entry_num);

} /* ddr_params_detection */

/* ============================================================================
**  Function : ddr_params_set_shared_memory
** ============================================================================
*/
/**
*   @brief
*   Copies the detected ddr parameter to shared memory region
*
*   @return
*   None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_params_set_shared_memory( void )
{
  /* We need to populate the updated parameter to correct location in shared device table so rpm
   will get the correct entry */

  /* Use memsmove to ensure the correctness of ram copy if ddr parameters buffer region
     overlaps with shared imem DDR region*/

  /* Move header to the base address of IMEM */

  memsmove((void *)shared_ddr_device_table_header,
            sizeof(struct ddr_device_params_header_v2),
            (void *)ddr_get_header(),
            ddr_device_table_header_size);

  memsmove((void *)shared_ddr_device_table,
          sizeof(ddr_device_params),
          (void *)ddr_get_params(DDR_CH0),
          ddr_device_table_entry_size);

  memsmove((void *)((size_t)shared_ddr_device_table + ddr_device_table_entry_size),
          sizeof(ddr_device_params),
          (void *)ddr_get_params(DDR_CH1),
          ddr_device_table_entry_size);


  /* Update DDR device parameters with new locations */
  ddr_device_params_ch0 = shared_ddr_device_table;
  ddr_device_params_ch1 = (ddr_device_params *)
                          ((size_t)shared_ddr_device_table + ddr_device_table_entry_size);

  ddrsns_share_data->cdt_params[0] = *ddr_device_params_ch0;
  ddrsns_share_data->cdt_params[1] = *ddr_device_params_ch1;

} /* ddr_param_set_shared_memory */

/* ============================================================================
**  Function : ddr_param_interleaved
** ============================================================================
*/
/**
*   @brief
*   Return the interleave setting based on ddr parameters
*
*   @return
*   return a bitmask of interleave status
*
*   @dependencies
*   Must be called after ddr_set_params. Calling it before ddr_set_params will
*   return the default setting which is TRUE.
*
*   @sa
*   None
*
*   @sa
*   None
*/
uint32 ddr_param_interleaved(void)
{
  return ddrsns_share_data->cdt_params[0].common.interleave_en;
}

/* =============================================================================
**  Function : ddr_params_get_training_data
** =============================================================================
*/
/**
*   @brief
*   Get DDR parameter partition: partition base is returned, and partition size
*   is updated through pointer.
*
*   @param[out]  size  DDR parameter partition size
*
*   @return
*   DDR parameter partition base
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void *ddr_params_get_training_data(uint32 *size)
{
  *size = sizeof(struct ddr_params_partition);

  return (void *)&(ddrsns_share_data->flash_params);

} /* ddr_params_get_partition */

/* =============================================================================
**  Function : ddr_params_set_training_data
** =============================================================================
*/
/**
*   @brief
*   Set DDR parameter partition.
*
*   @param[in]  base  DDR parameter partition base
*   @param[in]  size  DDR parameter partition size
*
*   @retval  TRUE   Partition set successfully
*   @retval  FALSE  Partition set unsuccessfully
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_set_training_data(void *base, uint32 size)
{
  boolean success = FALSE;

  if ( base != NULL && size == sizeof(struct ddr_params_partition) )
  {
    ddrsns_share_data->flash_params = *(struct ddr_params_partition *)base;
    success = TRUE;
  }
  return success;
}


/* =============================================================================
**  Function : ddr_get_training_checksum
** =============================================================================
*/
/**
*   @brief
*   Get DDR training checksum.
*
*   @param
*   None
*
*   @return
*   DDR training data checksum, 0 if no valid training partition is found
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
uint32 ddr_get_training_checksum(uint32 serial_number)
{
  uint32 tr_data_size_in_bits;
  uint32 crc32 = 0x0UL;
  
  if (&(ddrsns_share_data->flash_params) != NULL)
  {
    /* Calculate the training data size in bits */
    tr_data_size_in_bits = sizeof (uint8) * TRAINING_DATA_SIZE_IN_BYTES * 8; /* TRAINING_DATA_SIZE_IN_BYTES is the training size for LP4 , allocated in ddr_params_partition */ 

    crc32 = crc_32_calc((uint8 *)(ddrsns_share_data->flash_params.training_data), (uint16)tr_data_size_in_bits, serial_number);
	ddr_printf (DDR_NORMAL, "DDR: Computed CRC checksum retrieved from flash is %d", crc32);
    return crc32;
  }
  else
  {
    return 0;
  }
}
/* =============================================================================
**  Function : ddr_set_training_checksum
** =============================================================================
*/
/**
*   @brief
*   Compute the checksum over entire training data
*
*   @param
*   Serial number
*
*   @return
*   CRC checksum
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_set_training_checksum(uint32 serial_number)
{
  uint32 tr_data_size_in_bits;
  uint32 crc32 = 0x0UL;
	
  if (&(ddrsns_share_data->flash_params) != NULL)
  {
      /* Calculate the training data size in bits */
      tr_data_size_in_bits = sizeof (uint8) * TRAINING_DATA_SIZE_IN_BYTES * 8; /* TRAINING_DATA_SIZE_IN_BYTES is the training size for LP4 , allocated in ddr_params_partition */ 

      crc32 = crc_32_calc((uint8 *)(ddrsns_share_data->flash_params.training_data), (uint16)tr_data_size_in_bits, serial_number);
      ddrsns_share_data->flash_params.checksum = crc32;
	  ddr_printf (DDR_NORMAL, "DDR: Checksum to be stored on flash is %d", crc32);
	  return TRUE;
  }
  else
  {
    return FALSE;
  }
}
