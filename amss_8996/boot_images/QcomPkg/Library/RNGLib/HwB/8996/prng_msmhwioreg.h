#ifndef __PRNG_MSMHWIOREG_H__
#define __PRNG_MSMHWIOREG_H__
/*
===========================================================================
*/
/**
  @file prng_msmhwioreg.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8996 (Istari) v2 [istari_v2.1_p3q2r16.11]
 
  This file contains HWIO register definitions for the following modules:
    PRNG_CFG_EE3_EE3_PRNG_SUB

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Library/RNGLib/HwB/8996/prng_msmhwioreg.h#2 $
  $DateTime: 2016/07/07 23:06:19 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: PRNG_CFG_EE3_EE3_PRNG_SUB
 *--------------------------------------------------------------------------*/

#define PRNG_CFG_EE3_EE3_PRNG_SUB_REG_BASE                               (PRNG_CFG_PRNG_TOP_BASE      + 0x00004000)
#define PRNG_CFG_EE3_EE3_PRNG_SUB_REG_BASE_PHYS                          (PRNG_CFG_PRNG_TOP_BASE_PHYS + 0x00004000)
#define PRNG_CFG_EE3_EE3_PRNG_SUB_REG_BASE_OFFS                          0x00004000

#define HWIO_PRNG_CFG_EE3_PRNG_DATA_OUT_ADDR                             (PRNG_CFG_EE3_EE3_PRNG_SUB_REG_BASE      + 0x00000000)
#define HWIO_PRNG_CFG_EE3_PRNG_DATA_OUT_PHYS                             (PRNG_CFG_EE3_EE3_PRNG_SUB_REG_BASE_PHYS + 0x00000000)
#define HWIO_PRNG_CFG_EE3_PRNG_DATA_OUT_OFFS                             (PRNG_CFG_EE3_EE3_PRNG_SUB_REG_BASE_OFFS + 0x00000000)
#define HWIO_PRNG_CFG_EE3_PRNG_DATA_OUT_RMSK                             0xffffffff
#define HWIO_PRNG_CFG_EE3_PRNG_DATA_OUT_IN          \
        in_dword_masked(HWIO_PRNG_CFG_EE3_PRNG_DATA_OUT_ADDR, HWIO_PRNG_CFG_EE3_PRNG_DATA_OUT_RMSK)
#define HWIO_PRNG_CFG_EE3_PRNG_DATA_OUT_INM(m)      \
        in_dword_masked(HWIO_PRNG_CFG_EE3_PRNG_DATA_OUT_ADDR, m)
#define HWIO_PRNG_CFG_EE3_PRNG_DATA_OUT_PRNG_DATA_OUT_BMSK               0xffffffff
#define HWIO_PRNG_CFG_EE3_PRNG_DATA_OUT_PRNG_DATA_OUT_SHFT                      0x0

#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_ADDR                               (PRNG_CFG_EE3_EE3_PRNG_SUB_REG_BASE      + 0x00000004)
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_PHYS                               (PRNG_CFG_EE3_EE3_PRNG_SUB_REG_BASE_PHYS + 0x00000004)
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_OFFS                               (PRNG_CFG_EE3_EE3_PRNG_SUB_REG_BASE_OFFS + 0x00000004)
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_RMSK                                    0x3ff
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_IN          \
        in_dword_masked(HWIO_PRNG_CFG_EE3_PRNG_STATUS_ADDR, HWIO_PRNG_CFG_EE3_PRNG_STATUS_RMSK)
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_INM(m)      \
        in_dword_masked(HWIO_PRNG_CFG_EE3_PRNG_STATUS_ADDR, m)
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_CURRENT_OPERATION_BMSK                  0x300
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_CURRENT_OPERATION_SHFT                    0x8
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_CURRENT_OPERATION_IDLE_FVAL               0x0
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_CURRENT_OPERATION_GENERATE_FVAL           0x1
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_CURRENT_OPERATION_INSTANTIATE_FVAL        0x2
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_CURRENT_OPERATION_RESEED_FVAL             0x3
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_GENERATE_REQUIRED_BMSK                   0x80
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_GENERATE_REQUIRED_SHFT                    0x7
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_RESEED_REQUIRED_BMSK                     0x40
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_RESEED_REQUIRED_SHFT                      0x6
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_INSTANTIATE_REQUIRED_BMSK                0x20
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_INSTANTIATE_REQUIRED_SHFT                 0x5
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_RING_OSC3_HEALTHY_BMSK                   0x10
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_RING_OSC3_HEALTHY_SHFT                    0x4
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_RING_OSC2_HEALTHY_BMSK                    0x8
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_RING_OSC2_HEALTHY_SHFT                    0x3
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_RING_OSC1_HEALTHY_BMSK                    0x4
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_RING_OSC1_HEALTHY_SHFT                    0x2
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_RING_OSC0_HEALTHY_BMSK                    0x2
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_RING_OSC0_HEALTHY_SHFT                    0x1
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_DATA_AVAIL_BMSK                           0x1
#define HWIO_PRNG_CFG_EE3_PRNG_STATUS_DATA_AVAIL_SHFT                           0x0


#define PRNG_CFG_TZ_TZ_PRNG_TZ_REG_BASE                                                       (PRNG_CFG_PRNG_TOP_BASE      + 0x00001000)
#define PRNG_CFG_TZ_TZ_PRNG_TZ_REG_BASE_PHYS                                                  (PRNG_CFG_PRNG_TOP_BASE_PHYS + 0x00001000)

#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_ADDR                                                   (PRNG_CFG_TZ_TZ_PRNG_TZ_REG_BASE      + 0x00000100)
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_PHYS                                                   (PRNG_CFG_TZ_TZ_PRNG_TZ_REG_BASE_PHYS + 0x00000100)
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_OFFS                                                   (PRNG_CFG_TZ_TZ_PRNG_TZ_REG_BASE_OFFS + 0x00000100)
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RMSK                                                      0x1ffff
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_IN          \
        in_dword_masked(HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_ADDR, HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RMSK)
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_INM(m)      \
        in_dword_masked(HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_ADDR, m)
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_OUT(v)      \
        out_dword(HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_ADDR,v)
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_ADDR,m,v,HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_IN)
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_LFSR_OVRID_ON_BMSK                                        0x10000
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_LFSR_OVRID_ON_SHFT                                           0x10
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_LFSR3_EN_BMSK                                              0x8000
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_LFSR3_EN_SHFT                                                 0xf
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC3_CFG_BMSK                                         0x7000
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC3_CFG_SHFT                                            0xc
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC3_CFG_FEEDBACK_POINT_0_FVAL                           0x4
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC3_CFG_FEEDBACK_POINT_1_FVAL                           0x5
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC3_CFG_FEEDBACK_POINT_2_FVAL                           0x6
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC3_CFG_FEEDBACK_POINT_3_FVAL                           0x7
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_LFSR2_EN_BMSK                                               0x800
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_LFSR2_EN_SHFT                                                 0xb
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC2_CFG_BMSK                                          0x700
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC2_CFG_SHFT                                            0x8
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC2_CFG_FEEDBACK_POINT_0_FVAL                           0x4
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC2_CFG_FEEDBACK_POINT_1_FVAL                           0x5
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC2_CFG_FEEDBACK_POINT_2_FVAL                           0x6
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC2_CFG_FEEDBACK_POINT_3_FVAL                           0x7
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_LFSR1_EN_BMSK                                                0x80
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_LFSR1_EN_SHFT                                                 0x7
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC1_CFG_BMSK                                           0x70
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC1_CFG_SHFT                                            0x4
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC1_CFG_FEEDBACK_POINT_0_FVAL                           0x4
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC1_CFG_FEEDBACK_POINT_1_FVAL                           0x5
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC1_CFG_FEEDBACK_POINT_2_FVAL                           0x6
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC1_CFG_FEEDBACK_POINT_3_FVAL                           0x7
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_LFSR0_EN_BMSK                                                 0x8
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_LFSR0_EN_SHFT                                                 0x3
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC0_CFG_BMSK                                            0x7
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC0_CFG_SHFT                                            0x0
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC0_CFG_FEEDBACK_POINT_0_FVAL                           0x4
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC0_CFG_FEEDBACK_POINT_1_FVAL                           0x5
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC0_CFG_FEEDBACK_POINT_2_FVAL                           0x6
#define HWIO_PRNG_CFG_TZ_PRNG_LFSR_CFG_RING_OSC0_CFG_FEEDBACK_POINT_3_FVAL                           0x7

#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_ADDR                                                     (PRNG_CFG_TZ_TZ_PRNG_TZ_REG_BASE      + 0x00000104)
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_PHYS                                                     (PRNG_CFG_TZ_TZ_PRNG_TZ_REG_BASE_PHYS + 0x00000104)
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_RMSK                                                           0x3f
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_IN          \
        in_dword_masked(HWIO_PRNG_CFG_TZ_PRNG_CONFIG_ADDR, HWIO_PRNG_CFG_TZ_PRNG_CONFIG_RMSK)
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_INM(m)      \
        in_dword_masked(HWIO_PRNG_CFG_TZ_PRNG_CONFIG_ADDR, m)
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_OUT(v)      \
        out_dword(HWIO_PRNG_CFG_TZ_PRNG_CONFIG_ADDR,v)
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PRNG_CFG_TZ_PRNG_CONFIG_ADDR,m,v,HWIO_PRNG_CFG_TZ_PRNG_CONFIG_IN)
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_TEST_OUT_SEL_BMSK                                              0x3c
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_TEST_OUT_SEL_SHFT                                               0x2
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_PRNG_EN_BMSK                                                    0x2
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_PRNG_EN_SHFT                                                    0x1
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_SW_RESET_BMSK                                                   0x1
#define HWIO_PRNG_CFG_TZ_PRNG_CONFIG_SW_RESET_SHFT                                                   0x0

#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_ADDR                                                 (PRNG_CFG_TZ_TZ_PRNG_TZ_REG_BASE      + 0x00000140)
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_PHYS                                                 (PRNG_CFG_TZ_TZ_PRNG_TZ_REG_BASE_PHYS + 0x00000140)
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_RMSK                                                 0x1fffffff
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_IN          \
        in_dword_masked(HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_ADDR, HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_RMSK)
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_INM(m)      \
        in_dword_masked(HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_ADDR, m)
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_CRC32_CONTINUOUS_TEST_FAILURE_BMSK                   0x10000000
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_CRC32_CONTINUOUS_TEST_FAILURE_SHFT                         0x1c
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_DRBG_CONTINUOUS_TEST_FAILURE_BMSK                     0x8000000
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_DRBG_CONTINUOUS_TEST_FAILURE_SHFT                          0x1b
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_KAT_FAILURE_BMSK                                      0x4000000
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_KAT_FAILURE_SHFT                                           0x1a
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_KAT_DONE_BMSK                                         0x2000000
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_KAT_DONE_SHFT                                              0x19
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_VC_RD_READY_BMSK                                0x1000000
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_VC_RD_READY_SHFT                                     0x18
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_ENTROPY_RD_READY_BMSK                            0x800000
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_ENTROPY_RD_READY_SHFT                                0x17
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_VC_WR_READY_BMSK                                 0x400000
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_VC_WR_READY_SHFT                                     0x16
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_ENTROPY_WR_READY_BMSK                            0x200000
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_ENTROPY_WR_READY_SHFT                                0x15
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_VC_CNT_BMSK                                      0x1e0000
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_VC_CNT_SHFT                                          0x11
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_ENTROPY_CNT_BMSK                                  0x1fffe
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_ENTROPY_CNT_SHFT                                      0x1
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_DONE_BMSK                                             0x1
#define HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_SWKAT_DONE_SHFT                                             0x0

#endif /* __PRNG_MSMHWIOREG_H__ */
