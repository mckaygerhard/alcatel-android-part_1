#ifndef _PRNGCL_TARGET_H
#define _PRNGCL_TARGET_H
/*===========================================================================

                       P R N G E n g i n e D r i v e r

                       H e a d e r  F i l e (e x t e r n a l)

DESCRIPTION
  This header file contains HW Crypto specific declarations.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2010-2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
2014-05-06   sp     Changes to make RNG driver target independent
2012-08-21   shl    Made UEFI change 
2010-06-25   yk     Initial version
============================================================================*/


/*===========================================================================

                           INCLUDE FILES


===========================================================================*/
#include "prng_msmhwioreg.h"

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/
#define SEC_RETAILMSG(a, b, c, d)  MSG_ERROR(a, b, c, d)

#define SEC_PRNG_STATUS PRNG_CFG_EE3_PRNG_STATUS
#define SEC_PRNG_DATA_OUT PRNG_CFG_EE3_PRNG_DATA_OUT
#define SEC_PRNG_CONFIG PRNG_CFG_TZ_PRNG_CONFIG
#define SEC_PRNG_EN PRNG_EN
#define SEC_PRNG_LFSR_CFG PRNG_CFG_TZ_PRNG_LFSR_CFG

#define SEC_PRNG_KAT_STATUS PRNG_CFG_TZ_PRNG_KAT_STATUS
#define SEC_HWIO_PRNG_KAT_STATUS_KAT_FAILURE_BMSK HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_KAT_FAILURE_BMSK
#define SEC_HWIO_PRNG_KAT_STATUS_KAT_FAILURE_SHFT HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_KAT_FAILURE_SHFT
#define SEC_HWIO_PRNG_KAT_STATUS_KAT_DONE_BMSK HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_KAT_DONE_BMSK
#define SEC_HWIO_PRNG_KAT_STATUS_KAT_DONE_SHFT HWIO_PRNG_CFG_TZ_PRNG_KAT_STATUS_KAT_DONE_SHFT

#define SEC_LFSR0_EN LFSR0_EN
#define SEC_LFSR1_EN LFSR1_EN
#define SEC_LFSR2_EN LFSR2_EN
#define SEC_LFSR3_EN LFSR3_EN

#define SEC_HWIO_PRNG_STATUS_DATA_AVAIL_BMSK HWIO_PRNG_CFG_EE3_PRNG_STATUS_DATA_AVAIL_BMSK

#endif /* _PRNGCL_TARGET_H */
