/**
@file PrngCL.c 
@brief PRNG Engine source file 
*/

/*===========================================================================

                     P R N G E n g i n e D r i v e r

DESCRIPTION
  This file contains declarations and definitions for the
  interface between PRNG engine api and the PRNG hardware

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2009 - 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when         who     what, where, why
--------     ---     ----------------------------------------------------------
2014-05-13   sk      Modified PrngML getdata name
2014-03-16   sk      New crypto code and 64 compatibility
2013-04-03   shl     Reorg the code so the common file is shared
2013-03-06   shl     Revome some registers configuration to avoid XPU violation
                     TZ configurates these registers now. 
2012-01-28   shl     Warning fix 
2012-08-21   shl     Made UEFI change 
2011-09-08   nk      Added the implementation of de-init routine. 
2010-06-25   yk      Initial version
============================================================================*/
#include <com_dtypes.h>
#include "PrngCL.h"
#include "PrngEL.h"
#include <HALhwio.h>
#include <msmhwiobase.h>
#include "PrngCL_target.h"
#include <Library/BaseMemoryLib.h>

#define SEC_PRNG_KAT_DONE (HWIO_IN(SEC_PRNG_KAT_STATUS) & (1<<SEC_HWIO_PRNG_KAT_STATUS_KAT_DONE_SHFT))
#define SEC_PRNG_KAT_FAILURE (HWIO_IN(SEC_PRNG_KAT_STATUS) & (1<<SEC_HWIO_PRNG_KAT_STATUS_KAT_FAILURE_SHFT))

/**
 * @brief This function initializes PRNG Engine.
 *
 * @param None
 * @param None
 *
 * @return None
 *
 * @see PrngCL_getdata
 *
 */
PrngCL_Result_Type PrngCL_lite_init(void)
{
	PrngCL_Result_Type stat = PRNGCL_ERROR_NONE;

   /* PRNG_KAT:
   * Call_ahb_read (�TZ_PRNG_KAT_STATUS[KAT_DONE] )
   * proceed to next step if read = 1�b1.
   * Call_ahb_read(�TZ_PRNG_KAT_STATUS[KAT_FAILURE])
   * proceed to next step if read = 1�b0, else jump to error routine.*/

   while ( !SEC_PRNG_KAT_DONE )
   ;

         if (SEC_PRNG_KAT_FAILURE){
          stat = PRNGCL_ERROR_BAD_STATE;
          goto EXIT;
		 }

   //PrngELMemoryBarrier();

   HWIO_OUT(SEC_PRNG_LFSR_CFG,
        (0x5                                                                                 | //Set RING_OSC0_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR0_EN)) | //Enable LFSR0_EN
        0x50                                                                                 | //Set RING_OSC1_CFG to 101, which is feedback point 1 
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR1_EN)) | //Enable LFSR1_EN
        0x500                                                                               | //Set RING_OSC2_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR2_EN)) | //Enable LFSR2_EN
        0x5000                                                                             | //Set RING_OSC3_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR3_EN))    //Enable LFSR3_EN
        ));

   //PrngELMemoryBarrier();

   HWIO_OUT(SEC_PRNG_CONFIG,
        ((1 << HWIO_SHFT(SEC_PRNG_CONFIG, SEC_PRNG_EN)) ));

   //PrngELMemoryBarrier();
EXIT:
   return stat;
}

/**
 * @brief  This function returns the contents of the PRNG_DATA_OUT register.
 *
 * @param random_ptr [in]pointer to random number
 * @param random_len [in]length of random number
 *
 * @return PrngCL_Resut_Type
 *
 * @see PrngCL_init
 *
 */
PrngCL_Result_Type PrngCL_getdata(uint8*  random_ptr,  uint16  random_len)
{
  PrngCL_Result_Type ret_val = PRNGCL_ERROR_NONE;
  uint32 tmp_iv;
  uint32 i;
  uint32 tmp_random_len = random_len;
  const uint16 unit_random_len = 4;
   
  if(!random_ptr || !random_len){
    return PRNGCL_ERROR_INVALID_PARAM; 
  }

  if(random_len%unit_random_len !=0)
  {
  	tmp_random_len += (unit_random_len - random_len%unit_random_len);
  }
  
  /* Generate random numbers. Unit length of PRNG is 4 Bytes. 
  *  Hence, multiple PRNG random numbers are generated if random_len > 4 */
  for (i=0; i<(tmp_random_len/unit_random_len); i++)
  {
    /* check PRNG_STATUS */
    while(1)
      {
        uint32 prng_status = HWIO_IN(SEC_PRNG_STATUS);

        if(prng_status &  SEC_HWIO_PRNG_STATUS_DATA_AVAIL_BMSK)	
        {
          break;
        }
      }

      /* Get the randum number from PRNG_DATA_OUT */
        tmp_iv = HWIO_IN(SEC_PRNG_DATA_OUT);

       /* Generate mutiple of 4 bytes random numbers. Then, if necessary, adjust total random
        *  number to random_len size */
       if( i < (tmp_random_len/unit_random_len -1) )
       {
         CopyMem(random_ptr, &tmp_iv, unit_random_len);
         random_ptr += unit_random_len;
       }
       else 
       {
         if ( (random_len%unit_random_len) !=0)
         {
	       CopyMem(random_ptr, &tmp_iv, random_len%unit_random_len);
         }
         else
         {
           CopyMem(random_ptr, &tmp_iv, unit_random_len);
         }
      }
   }

  return ret_val;
}

/**
 * @brief This function de-initializes PRNG Engine.
 *
 * @param None
 * @param None
 *
 * @return None
 *
 * @see PrngCL_init
 *
 */
PrngCL_Result_Type PrngCL_lite_deinit(void)
{
	PrngCL_Result_Type stat = PRNGCL_ERROR_NONE;
   /* TODO: Test the following regressively
      for side effects before finalising the
      implementation. To be used in PrngML_getdata_lite()
      after the buffer gets a valid number.
   */

  //PrngELMemoryBarrier();
#if 0
  /* Set Reset State: 0x00000000 to PRNG_CONFIG Register */
   HWIO_OUT(SEC_PRNG_CONFIG, 0x00000000);

   /* Set Reset State: 0x00000000 to PRNG_LFSR_CFG Register */
   HWIO_OUT(SEC_PRNG_LFSR_CFG, 0x00000000);
#endif
   //PrngELMemoryBarrier();

   /* Disable clock for random number generator */
   //stat = (PrngCL_Result_Type) PrngEL_ClkDisable();
   return stat;
}

