/*! \file pm_smps_driver.c
*  \n
*  \brief This file contains SMPS peripheral driver initialization during which the driver
*         driver data is stored. 
*  \n   
*  \n &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/06/14   al      Porting optimization changes
07/16/14   akm     Comm change Updates
03/31/14   akm     Updated to the latest PMIC driver architecture   
12/06/12   hw      Rearchitecturing module driver to peripheral driver
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_smps_driver.h"
#include "CoreVerify.h"


/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the SMPS driver data */
static pm_smps_data_type *pm_smps_data_arr[PM_MAX_NUM_PMICS];

/*===========================================================================

                     FUNCTION DECLARATION 

===========================================================================*/

static pm_pwr_volt_info_type* pm_smps_get_volt_setting_info(peripheral_info_type *peripheral_info);
static pm_pwr_range_info_type* pm_smps_get_range_setting_info(peripheral_info_type *peripheral_info);

/*===========================================================================

                     INTERNAL DRIVER FUNCTIONS 

===========================================================================*/


void pm_smps_driver_init(pm_comm_info_type *comm_ptr, peripheral_info_type *peripheral_info, uint8 pmic_index)
{
    pm_smps_data_type *smps_ptr = NULL;
    uint16 smps_index = 0;
    pm_register_address_type base_address = 0;
    pm_register_address_type periph_offset = 0;

    if ((peripheral_info->peripheral_type == PM_HW_MODULE_FTS) && 
        (peripheral_info->peripheral_subtype != PM_HW_MODULE_FTS__FTS_CTL) &&
        (peripheral_info->peripheral_subtype != PM_HW_MODULE_FTS__FTS2p5_CTL))
    {
        return;
    }

    if ((peripheral_info->peripheral_type == PM_HW_MODULE_HF_BUCK) && 
        (peripheral_info->peripheral_subtype != PM_HW_MODULE_HF_BUCK_GP_CTL))
    {
        return;
    }

    smps_ptr = pm_smps_data_arr[pmic_index];

    if (smps_ptr == NULL)
    {
        pm_malloc( sizeof(pm_smps_data_type), (void**)&smps_ptr);
                                                    
        /* Assign Comm ptr */
        smps_ptr->comm_ptr = comm_ptr;

        /* SMPS Register Info - Obtaining Data through dal config */
        smps_ptr->pm_pwr_data.pwr_reg_table = (pm_pwr_register_info_type*)pm_target_information_get_common_info(PM_PROP_SMPS_REG);

        CORE_VERIFY_PTR(smps_ptr->pm_pwr_data.pwr_reg_table);

        smps_ptr->pm_pwr_data.num_of_peripherals = pm_target_information_get_periph_count_info(PM_PROP_SMPS_NUM, pmic_index);

        /* Num of peripherals cannot be 0 if this driver init gets called */
        CORE_VERIFY(smps_ptr->pm_pwr_data.num_of_peripherals != 0);

        /* SMPS pwr rail specific info pointer malloc to save all the peripheral's base address, Type, Range and Vset */
        pm_malloc(sizeof(pm_pwr_specific_info_type)*(smps_ptr->pm_pwr_data.num_of_peripherals), (void**)&(smps_ptr->pm_pwr_data.pwr_specific_info));

        CORE_VERIFY_PTR(smps_ptr->pm_pwr_data.pwr_specific_info);

        /* Save first SMPS peripheral's base address */
        smps_ptr->pm_pwr_data.pwr_specific_info[0].periph_base_address = peripheral_info->base_address;

        pm_smps_data_arr[pmic_index] = smps_ptr;
    }
    
    if (smps_ptr != NULL)
    {
        base_address = smps_ptr->pm_pwr_data.pwr_specific_info[0].periph_base_address;
        periph_offset = smps_ptr->pm_pwr_data.pwr_reg_table->peripheral_offset;

        /* Peripheral Baseaddress should be >= first peripheral's base addr */
        CORE_VERIFY(peripheral_info->base_address >= base_address);

        /* Calculate SMPS peripheral index */
        smps_index = ((peripheral_info->base_address - base_address)/periph_offset);


        /* Save SMPS's Peripheral Type value */
        smps_ptr->pm_pwr_data.pwr_specific_info[smps_index].periph_type = peripheral_info->peripheral_type;

        /* Save each SMPS peripheral's base address */
        smps_ptr->pm_pwr_data.pwr_specific_info[smps_index].periph_base_address = peripheral_info->base_address;

        /* SMPS Vset Info - Obtaining Data through dal config */
        smps_ptr->pm_pwr_data.pwr_specific_info[smps_index].pwr_vset = pm_smps_get_volt_setting_info(peripheral_info);

        CORE_VERIFY_PTR(smps_ptr->pm_pwr_data.pwr_specific_info[smps_index].pwr_vset);

        /* SMPS Range Info - Obtaining Data through dal config */
        smps_ptr->pm_pwr_data.pwr_specific_info[smps_index].pwr_range = pm_smps_get_range_setting_info(peripheral_info);

        CORE_VERIFY_PTR(smps_ptr->pm_pwr_data.pwr_specific_info[smps_index].pwr_range);
    }
}

pm_pwr_volt_info_type* pm_smps_get_volt_setting_info(peripheral_info_type *peripheral_info)
{
  pm_pwr_volt_info_type *volt_info = NULL;
  static pm_pwr_volt_info_type *fts_volt_info = NULL;
  static pm_pwr_volt_info_type *fts2p5_volt_info = NULL;
  static pm_pwr_volt_info_type *hfs_volt_info = NULL;

  switch ((pm_hw_module_type)peripheral_info->peripheral_type)
  {
     case PM_HW_MODULE_FTS:
       switch(peripheral_info->peripheral_subtype)
       {
          case PM_HW_MODULE_FTS__FTS_CTL:
            if (NULL==fts_volt_info)
            {
              fts_volt_info = (pm_pwr_volt_info_type *)pm_target_information_get_common_info(PM_PROP_FTS_VOLT);
            }
            volt_info = fts_volt_info;
            break;
          case PM_HW_MODULE_FTS__FTS2p5_CTL:
            if (NULL==fts2p5_volt_info)
            {
              fts2p5_volt_info = (pm_pwr_volt_info_type *)pm_target_information_get_common_info(PM_PROP_FTS2p5_VOLT);
            }
            volt_info = fts2p5_volt_info;
            break;
          default:
            volt_info = NULL;
            break;
       }
       break;
     case PM_HW_MODULE_HF_BUCK:
       if (NULL==hfs_volt_info)
       {
         hfs_volt_info = (pm_pwr_volt_info_type *)pm_target_information_get_common_info(PM_PROP_HFS_VOLT);
       }
       volt_info = hfs_volt_info;
       break;
     default:
       volt_info = NULL;
       break;
  }

  return volt_info;
}

pm_pwr_range_info_type* pm_smps_get_range_setting_info(peripheral_info_type *peripheral_info)
{
  pm_pwr_range_info_type *range_info = NULL;

  static pm_pwr_range_info_type *fts_range_info = NULL;
  static pm_pwr_range_info_type *fts2p5_range_info = NULL;
  static pm_pwr_range_info_type *hfs_range_info = NULL;


  switch ((pm_hw_module_type)peripheral_info->peripheral_type)
  {
     case PM_HW_MODULE_FTS:
       switch (peripheral_info->peripheral_subtype)
       {
          case PM_HW_MODULE_FTS__FTS_CTL:
            if (NULL==fts_range_info)
            {
              fts_range_info = (pm_pwr_range_info_type *)pm_target_information_get_common_info(PM_PROP_FTS_RANGE);
            }
            range_info = fts_range_info;
            break;
          case PM_HW_MODULE_FTS__FTS2p5_CTL:
            if (NULL==fts2p5_range_info)
            {
              fts2p5_range_info = (pm_pwr_range_info_type *)pm_target_information_get_common_info(PM_PROP_FTS2p5_RANGE);
            }
            range_info = fts2p5_range_info;
            break;
          default:
            range_info = NULL;
            break;
       }
       break;
     case PM_HW_MODULE_HF_BUCK:
       if (NULL==hfs_range_info)
       {
         hfs_range_info = (pm_pwr_range_info_type *)pm_target_information_get_common_info(PM_PROP_HFS_RANGE);
       }
       range_info = hfs_range_info;
       break;     
     default:
       range_info = NULL;
       break;
  }

  return range_info;
}

pm_smps_data_type* pm_smps_get_data(uint8 pmic_index)
{
  if(pmic_index < PM_MAX_NUM_PMICS) 
  {
      return pm_smps_data_arr[pmic_index];
  }

    return NULL;
}

uint8 pm_smps_get_num_peripherals(uint8 pmic_index)
{
  if((pmic_index < PM_MAX_NUM_PMICS) && 
  	  (pm_smps_data_arr[pmic_index] != NULL))
  {
      return pm_smps_data_arr[pmic_index]->pm_pwr_data.num_of_peripherals;
  }

  return 0;
}

