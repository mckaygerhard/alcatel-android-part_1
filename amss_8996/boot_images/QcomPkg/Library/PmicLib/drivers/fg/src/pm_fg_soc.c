/*! \file
*  
*  \brief  pm_fg_soc.c --- FG Driver SOC Implementation.
*  \details  Fuel Gauge SOC driver implementation.
*  &copy; 
*  Copyright (c) 2014-2016 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  Qualcomm Technologies Proprietary and Confidential.
*/

/*===========================================================================

EDIT HISTORY FOR MODULE


when        who     what, where, why
--------    ---     ---------------------------------------------------------- 
04/11/16   va       Added pm_fg_soc_set_fg_reset_cfg()  
08/20/14    al      Updating comm lib 
08/27/14    va      Driver cleanup update
08/13/14    sm      Made changes to read soc in single API
                    Also corrected SOC calculation in get_monotonic_soc API 
06/25/14    va      Driver update, added protected call
04/07/14    va      New file
========================================================================== */

/*===========================================================================

INCLUDE FILES 

===========================================================================*/
#include "pm_fg_soc.h"
#include "pm_fg_driver.h"
#include "pm_resource_manager.h"

/*===========================================================================
                       MACRO DECLARATIONS
===========================================================================*/
#define SOC_100 0x64 //SOC 100
#define SOC_FF  0xFF

/*===========================================================================

FUNCTION DEFINITIONS 

===========================================================================*/

/**
* @brief This function grant access to FG SOC access *
* @details
* This function grant access to FG SOC access 
* 
* @param[in] pm_fg_data_type Self
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_grant_sec_access(pm_fg_data_type *fg_soc_ptr)
{

   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

   if (NULL == fg_soc_ptr)
   {
      err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else
   {
      pm_register_address_type sec_access = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_sec_access;
      err_flag = pm_comm_write_byte(fg_soc_ptr->comm_ptr->slave_id, sec_access, 0xA5, 0);
   }

   return err_flag;
}

/**
* @brief This function returns  Fuel Gauge Algorithm status * 
* @details
*  This function returns  Fuel Gauge Algorithm status
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in] pm_fg_soc_alg_sts. alg status
* @param[out] enable. true/false status
*
* @return  pm_err_flag_type
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC
*          PM_ERR_FLAG__SUCCESS               = SUCCESS
*
*/
pm_err_flag_type pm_fg_soc_get_fg_alg_sts(uint32 pmic_device, pm_fg_soc_alg_sts alg_sts, boolean *enable)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_data_type data;
   pm_register_address_type fg_soc_alg_sts;
   uint8 mask = 1 << alg_sts;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (alg_sts >= PM_FG_SOC_ALG_RDY_INVALID)
   {
      err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      fg_soc_alg_sts = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_alg_sts;
      err_flag |= pm_comm_read_byte_mask(fg_soc_ptr->comm_ptr->slave_id, fg_soc_alg_sts, mask, &data, 0);
      *enable = (data & mask) ? TRUE : FALSE;
   }

   return err_flag;

}


/**
* @brief This function returns battery state of charge  * 
* @details
*  This function returns battery state of charge, 0xFF indicates 100%
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out]mono_soc. % of state of charge (SOC) 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_get_monotonic_soc(uint32 pmic_device, uint32 *mono_soc)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type fg_soc_monotonic_soc;
  pm_register_address_type fg_soc_monotonic_soc_cp;
  pm_register_data_type    data1 = 0;
  pm_register_data_type    data  = 0;

  int8   count = 0;
  //Compare data thrice in copy register and original register to make sure we have right data
  int8   total_count = 3;

  pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

  if (NULL == fg_soc_ptr)
  {
     return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == mono_soc)
  {
     err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_soc_monotonic_soc = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_monotonic_soc;
    fg_soc_monotonic_soc_cp = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_monotonic_soc_cp;

    do {
      err_flag = pm_comm_read_byte(fg_soc_ptr->comm_ptr->slave_id, fg_soc_monotonic_soc, &data1, 0);
      err_flag = pm_comm_read_byte(fg_soc_ptr->comm_ptr->slave_id, fg_soc_monotonic_soc_cp, &data, 0);

      if(data == data1)
      {
        break;
      }

      if((total_count - 1) == count)
      {
        data = 0;
        err_flag = PM_ERR_FLAG__INVALID;
      }
      count ++;
    } while ( count < total_count);

    /*Register value 0xFF corresponds to SOC 100*/
    *mono_soc = (uint32)(((uint32)data * SOC_100) / SOC_FF);
  }
  return err_flag;
}

/**
* @brief This function returns  Fuel Gauge sleep shut down status * 
* @details
*  This function returns  Fuel Gauge sleep  shut down status
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out]pm_fg_soc_sleep_shutdown_sts. sleep shutdown status in define enum 
* @param[out] Enable. true/false status
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_get_sleep_shutdown_sts(uint32 pmic_device, pm_fg_soc_sleep_shutdown_sts fg_soc_shutdown_sts, boolean *enable)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_data_type data;
   pm_register_address_type fg_shutdown_sts;
   uint8 mask = 1 << fg_soc_shutdown_sts;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if ((fg_soc_shutdown_sts >= PM_FG_SOC_SLEEP_SHUTDOWN_STS_INVALID) || (fg_soc_shutdown_sts < PM_FG_SOC_SLEEP_SHUTDOWN_STS_FG_SHUTDOWN_ALLOWED))
   {
      err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      fg_shutdown_sts = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_sleep_shutdown_sts;
      err_flag = pm_comm_read_byte_mask(fg_soc_ptr->comm_ptr->slave_id, fg_shutdown_sts, mask, &data, 0);
      *enable = (data & (0x1 << fg_soc_shutdown_sts)) ? TRUE : FALSE;
   }

   return err_flag;
}


pm_err_flag_type pm_fg_soc_irq_enable(uint32 pmic_device, pm_fg_soc_irq_type irq, boolean enable)
{
   pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
   pm_register_address_type irq_reg;
   pm_register_data_type data = 1 << irq;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (irq >= PM_FG_SOC_IRQ_INVALID)
   {
      err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      if (enable)
      {
         irq_reg = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->int_en_set;
      }
      else
      {
         irq_reg = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->int_en_clr;
      }

      err_flag = pm_comm_write_byte(fg_soc_ptr->comm_ptr->slave_id, irq_reg, data, 0);
   }
   return err_flag;
}

pm_err_flag_type pm_fg_soc_irq_clear(uint32  pmic_device, pm_fg_soc_irq_type irq)
{
   pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
   pm_register_data_type data = 1 << irq;
   pm_register_address_type int_latched_clr;
   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (irq >= PM_FG_SOC_IRQ_INVALID)
   {
      err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else
   {
      int_latched_clr = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->int_latched_clr;
      err_flag = pm_comm_write_byte(fg_soc_ptr->comm_ptr->slave_id, int_latched_clr, data, 0);
   }

   return err_flag;
}


pm_err_flag_type pm_fg_soc_irq_set_trigger(uint32 pmic_device, pm_fg_soc_irq_type irq, pm_irq_trigger_type trigger)
{
   pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
   uint8 mask = 1 << irq;
   pm_register_data_type set_type, polarity_high, polarity_low;
   pm_register_address_type int_set_type, int_polarity_high, int_polarity_low;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (irq >= PM_FG_SOC_IRQ_INVALID)
   {
      err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else
   {
      int_set_type = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->int_set_type;
      int_polarity_high = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->int_polarity_high;
      int_polarity_low = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->int_polarity_low;

      switch (trigger)
      {
      case PM_IRQ_TRIGGER_ACTIVE_LOW:
         set_type = 0x00;
         polarity_high = 0x00;
         polarity_low = 0xFF;
         break;
      case PM_IRQ_TRIGGER_ACTIVE_HIGH:
         set_type = 0x00;
         polarity_high = 0xFF;
         polarity_low = 0x00;
         break;
      case PM_IRQ_TRIGGER_RISING_EDGE:
         set_type = 0xFF;
         polarity_high = 0xFF;
         polarity_low = 0x00;
         break;
      case PM_IRQ_TRIGGER_FALLING_EDGE:
         set_type = 0xFF;
         polarity_high = 0x00;
         polarity_low = 0xFF;
         break;
      case PM_IRQ_TRIGGER_DUAL_EDGE:
         set_type = 0xFF;
         polarity_high = 0xFF;
         polarity_low = 0xFF;
         break;
      default:
         return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
      }
      err_flag = pm_comm_write_byte_mask(fg_soc_ptr->comm_ptr->slave_id, int_set_type, mask, set_type, 0);
      err_flag |= pm_comm_write_byte_mask(fg_soc_ptr->comm_ptr->slave_id, int_polarity_high, mask, polarity_high, 0);
      err_flag |= pm_comm_write_byte_mask(fg_soc_ptr->comm_ptr->slave_id, int_polarity_low, mask, polarity_low, 0);
   }

   return err_flag;
}


pm_err_flag_type pm_fg_soc_irq_status(uint32 pmic_device, pm_fg_soc_irq_type irq, pm_irq_status_type type, boolean *status)
{
   pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
   pm_register_data_type data;
   uint8 mask = 1 << irq;
   pm_register_address_type int_sts;
   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (irq >= PM_FG_SOC_IRQ_INVALID)
   {
      err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      switch (type)
      {
      case PM_IRQ_STATUS_RT:
         int_sts = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->int_rt_sts;
         break;
      case PM_IRQ_STATUS_LATCHED:
         int_sts = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->int_latched_sts;
         break;
      case PM_IRQ_STATUS_PENDING:
         int_sts = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->int_pending_sts;
         break;
      default:
         return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
      }

      err_flag = pm_comm_read_byte_mask(fg_soc_ptr->comm_ptr->slave_id, int_sts, mask, &data, 0);
      *status = data ? TRUE : FALSE;
   }

   return err_flag;
}


/**
* @brief This function returns  fuel Gauge Boot Mode mofication setting *
* @details
*  This function returns  fuel Gauge Boot Mode mofication setting  
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in] pm_fg_soc_boot_mod. status in define enum 
* @param[in] enable. true/false value 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_get_fg_soc_boot_mod(uint32 pmic_device, pm_fg_soc_boot_mod fg_soc_boot_mod, boolean *enable)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_data_type data;
   pm_register_address_type boot_mod;
   uint8 mask = 1 << fg_soc_boot_mod;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if ((fg_soc_boot_mod >= PM_FG_SOC_BOOT_MOD_INVALID) || (fg_soc_boot_mod < PM_FG_SOC_PREVENT_OTP_PROFILE_RELOAD))
   {
      err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      boot_mod = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_boot_mode;
      //secure access control 
      err_flag  = pm_fg_soc_grant_sec_access(fg_soc_ptr);
      err_flag |= pm_comm_read_byte_mask(fg_soc_ptr->comm_ptr->slave_id, boot_mod, mask, &data, 0);
      *enable = (data & (0x1 << fg_soc_boot_mod)) ? TRUE : FALSE;
   }

   return err_flag;

}


/**
* @brief This function sets  fuel Gauge Boot Mode *
* @details
*  This function sets  fuel Gauge Boot Mode 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out]pm_fg_soc_boot_mod. sets defined mode in enum 
* @param[in]enable. true/false 

* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_set_fg_soc_boot_mod(uint32 pmic_device, pm_fg_soc_boot_mod fg_soc_boot_mod, boolean enable)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_address_type boot_mod;
   uint8 mask = 1 << fg_soc_boot_mod;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (fg_soc_boot_mod >= PM_FG_SOC_BOOT_MOD_INVALID)
   {
      err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      boot_mod = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_boot_mode;
      //secure access control 
      err_flag  = pm_fg_soc_grant_sec_access(fg_soc_ptr);
      err_flag |= pm_comm_write_byte_mask(fg_soc_ptr->comm_ptr->slave_id, boot_mod, mask,(pm_register_data_type)(enable << fg_soc_boot_mod), 0);
   }

   return err_flag;
}


/**
* @brief This function returns  Fuel Gauge restart status *
* @details
*  This function returns  fuel Gauge restart status  
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in] pm_fg_soc_fg_soc_restart. status in defined enum 
* @param[out enable. true/false 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_get_fg_soc_restart(uint32 pmic_device, pm_fg_soc_fg_soc_restart fg_soc_restart, boolean *enable)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_data_type data;
   pm_register_address_type fg_restart;
   uint8 mask = 1 << fg_soc_restart;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (fg_soc_restart >= PM_FG_SOC_FG_SOC_RESTART_INVALID)
   {
      err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      fg_restart = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_restart;
      //secure access control 
      err_flag  =  pm_fg_soc_grant_sec_access(fg_soc_ptr);
      err_flag |= pm_comm_read_byte_mask(fg_soc_ptr->comm_ptr->slave_id, fg_restart, mask, &data, 0);
      *enable = (data & (0x1 << fg_soc_restart)) ? TRUE : FALSE;
   }

   return err_flag;
}


/**
* @brief This function sets  Fule Gauge restart configurations *
* @details
*  This function sets  Fule Gauge restart configurations 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out]pm_fg_soc_fg_soc_restart. Sets defined mode in enum 
* @param[in] enable true/false configration to enable
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_set_fg_soc_restart(uint32 pmic_device, pm_fg_soc_fg_soc_restart fg_soc_restart, boolean enable)
{

   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_address_type fg_restart;
   uint8 mask = 1 << fg_soc_restart;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (fg_soc_restart >= PM_FG_SOC_FG_SOC_RESTART_INVALID)
   {
      err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      fg_restart = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_restart;
      //secure access control 
      err_flag  =  pm_fg_soc_grant_sec_access(fg_soc_ptr);
      err_flag |= pm_comm_write_byte_mask(fg_soc_ptr->comm_ptr->slave_id, fg_restart, mask, (pm_register_data_type)(enable << fg_soc_restart), 0);
   }

   return err_flag;


}

/**
* @brief This function clears WDOG_EXP bit in FG_ALG_STS register *
* @details
* This function clears WDOG_EXP bit in FG_ALG_STS register
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in] fg_wdog_exp. Any value in range 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_set_fg_soc_wdog_exp(uint32 pmic_device, uint8 fg_wdog_exp)
{

   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_address_type wdog_exp;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else
   {
      wdog_exp = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_wdog_exp;
      err_flag = pm_comm_write_byte(fg_soc_ptr->comm_ptr->slave_id, wdog_exp, fg_wdog_exp, 0);
   }

   return err_flag;

}

/**
* @brief This function clears FG_ALG_STS__CYCLE_STRETCHED Status register *
* @details
* This function clears FG_ALG_STS__CYCLE_STRETCHED Status register
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_set_fg_soc_sts_clr(uint32 pmic_device)
{

   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_address_type fg_sts_clr;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else
   {
      fg_sts_clr = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_sts_clr;
      err_flag = pm_comm_write_byte_mask(fg_soc_ptr->comm_ptr->slave_id, fg_sts_clr, 0x80, 0x80, 0);
   }

   return err_flag;

}

/**
* @brief This function returns  Fuel Gauge reset Configuration *
* @details
* This function returns  Fuel Gauge reset Configuration 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out] pm_fg_soc_reset. returns current reset configuration 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_get_reset_sts(uint32 pmic_device, pm_fg_soc_reset_type *fg_soc_reset_type)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_data_type data;
   pm_register_address_type fg_reset_type;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (NULL == fg_soc_reset_type)
   {
      return PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      fg_reset_type = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_fg_reset;
      //secure access control 
      err_flag  = pm_fg_soc_grant_sec_access(fg_soc_ptr);
      err_flag |= pm_comm_read_byte(fg_soc_ptr->comm_ptr->slave_id, fg_reset_type, &data, 0);
      fg_soc_reset_type->pm_fg_soc_reset_cmd = data >> 0x07;
      fg_soc_reset_type->pm_fg_soc_reset_type_sel = data >> 0x06;
   }

   return err_flag;

}


/**
* @brief This function resets   Fuel Gauge *
* @details
* This function resets   Fuel Gauge 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in] pm_fg_soc_reset_type. as defined in enum
* @param[in] enable. TRUE/FASLE value
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_set_fg_reset(uint32 pmic_device, pm_fg_soc_reset fg_soc_reset, boolean enable)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_address_type fg_reset;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (fg_soc_reset >= PM_FG_SOC_RESET_INVLIAD)
   {
      err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      fg_reset = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_fg_reset;
      //secure access control 
      err_flag  = pm_fg_soc_grant_sec_access(fg_soc_ptr);
      err_flag |= pm_comm_write_byte_mask(fg_soc_ptr->comm_ptr->slave_id, fg_reset, 0x03, (pm_register_data_type)(enable << fg_soc_reset), 0);
   }

   return err_flag;

}


/**
* @brief This function returns  Fuel Gauge low power configuration *
* @details
* This function returns  Fuel Gauge low power configuration 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out] pm_fg_soc_low_pwr_cfg_type. returns current low pwr cfg 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_get_low_pwr_cfg(uint32 pmic_device, pm_fg_soc_low_pwr_cfg_type *fg_soc_low_pwr_cfg_type)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_data_type data;
   pm_register_address_type fg_low_pwr_cfg;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (NULL == fg_soc_low_pwr_cfg_type)
   {
      return PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      fg_low_pwr_cfg = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_low_power_cfg;
      //secure access control 
      err_flag  = pm_fg_soc_grant_sec_access(fg_soc_ptr);
      err_flag |=  pm_comm_read_byte(fg_soc_ptr->comm_ptr->slave_id, fg_low_pwr_cfg, &data, 0);
      fg_soc_low_pwr_cfg_type->pm_fg_soc_low_pwr_cfg_sleep_en = data >> 0x07;
      fg_soc_low_pwr_cfg_type->pm_fg_soc_low_pwr_cfg_lq_frq_clkswitch_en = data >> 0x06;
   }

   return err_flag;
}

/**
* @brief This function sets  Fuel Gauge low power configuration *
* @details
* This function sets  Fuel Gauge low power configuration 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in] pm_fg_soc_low_pwr_cfg_type. low pwr cfg 
* @param[out] enable. value to be set for low pwr cfg passed enum
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_set_low_pwr_cfg(uint32 pmic_device, pm_fg_soc_low_pwr_cfg fg_soc_low_pwr_cfg, boolean enable)
{

   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_address_type fg_low_pwr;
   uint8 mask = 1 << fg_soc_low_pwr_cfg;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (fg_soc_low_pwr_cfg >= PM_FG_SOC_LOW_PWR_CFG_INVALID)
   {
      err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      fg_low_pwr = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_low_power_cfg;
      //secure access control 
      err_flag  = pm_fg_soc_grant_sec_access(fg_soc_ptr);
      err_flag |= pm_comm_write_byte_mask(fg_soc_ptr->comm_ptr->slave_id, fg_low_pwr, mask, (pm_register_data_type)(enable << fg_soc_low_pwr_cfg), 0);
   }

   return err_flag;
}


/**
* @brief This function resets Fuel Gauge locally *
* @details
* This function reset Fuel Gauge locally
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in] fg_reset_cfg. refer pm_fg_soc_fg_reset_cfg
* @param[in] enable. value to be set for local fg reset 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_soc_set_fg_reset_cfg(uint32 pmic_device, pm_fg_soc_fg_reset_cfg fg_reset_cfg, boolean enable)
{

   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_register_address_type fg_reset_reg = 0;
   uint8 mask = 1 << fg_reset_cfg;

   pm_fg_data_type *fg_soc_ptr  = pm_fg_get_data(pmic_device);

   if (NULL == fg_soc_ptr)
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }
   else if (fg_reset_cfg >= PM_FG_SOC_FG_RESET_INVALID)
   {
      err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
   }
   else
   {
      fg_reset_reg = fg_soc_ptr->fg_register->soc_register->base_address + fg_soc_ptr->fg_register->soc_register->fg_soc_fg_reset;
      //secure access control 
      err_flag  = pm_fg_soc_grant_sec_access(fg_soc_ptr);
      err_flag |= pm_comm_write_byte_mask(fg_soc_ptr->comm_ptr->slave_id, fg_reset_reg, mask, (pm_register_data_type)(enable << fg_reset_cfg), 0);
   }

   return err_flag;
}


