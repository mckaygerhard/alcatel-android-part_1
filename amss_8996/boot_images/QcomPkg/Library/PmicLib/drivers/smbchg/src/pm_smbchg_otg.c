/*! \file
*  
*  \brief  pm_smbchg_otg.c driver implementation.
*  \details charger driver implementation.
*  &copy;
*  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  Qualcomm Technologies Proprietary and Confidential.
*/

/*===========================================================================

EDIT HISTORY FOR MODULE


when        who          what, where, why
--------    ---          ----------------------------------------------------------
09/08/14    al           Comm lib update 
05/20/14    al           Architecture update
05/16/14    al           Added config 
04/18/14    al           Updated copyright 
04/18/14    al           Initial revision
========================================================================== */

/*===========================================================================

                    INCLUDE FILES

===========================================================================*/

#include "pm_smbchg_otg.h"
#include "pm_smbchg_driver.h"
#include "pm_resource_manager.h"

static inline pm_err_flag_type pm_smbchg_otg_unlock_perph_write(pm_smbchg_data_type *smbchg_ptr);

pm_err_flag_type pm_smbchg_otg_config_otg(uint32 pmic_device, pm_smbchg_otg_cfg_type* otg_cfg)
{
  pm_err_flag_type      err_flag    = PM_ERR_FLAG__SUCCESS;
  pm_register_data_type ctrl_type, sensor_src, data;
  pm_smbchg_data_type *smbchg_ptr  = pm_smbchg_get_data(pmic_device);

  if(NULL == smbchg_ptr) 
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if(otg_cfg == NULL)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else if(otg_cfg->otg_ctrl_type >= PM_SMBCHG_OTG_CTR__INVALID || otg_cfg->uvlo_sensor_src >= PM_SMBCHG_OTG_UVLO_SENSOR__INVALID )
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    pm_register_address_type otg_cfg_reg   = smbchg_ptr->smbchg_register->otg_register->base_address + smbchg_ptr->smbchg_register->otg_register->otg_cfg;

    switch(otg_cfg->otg_ctrl_type)
    {
       case PM_SMBCHG_OTG_CTR__CMD_REG_RID_DISABLED : ctrl_type =  0x0;  break;  
       case PM_SMBCHG_OTG_CTR__PIN_CTRL_RID_DISABLED: ctrl_type =  0x1;  break;
       case PM_SMBCHG_OTG_CTR__CMD_REG_RID_ENABLED  : ctrl_type =  0x2;  break;
       case PM_SMBCHG_OTG_CTR__AUTO_OTG_RID_ENABLED : ctrl_type =  0x3;  break;
       default: ctrl_type = 0x3; //Auto OTG default
    }

    switch(otg_cfg->uvlo_sensor_src)
    {
       case PM_SMBCHG_OTG_UVLO_SENSOR__ANA  : sensor_src = 0x00; break;
       case PM_SMBCHG_OTG_UVLO_SENSOR__FG   : sensor_src = 0x2;  break;
       case PM_SMBCHG_OTG_UVLO_SENSOR__BOTH : sensor_src = 0x3;  break;
       default: sensor_src = 0x3; //default both
    }

    data = sensor_src | ctrl_type<<2 | (otg_cfg->otg_pin_pol_active_high <<4) | (otg_cfg->hv_otg_protection_en << 5);   

    err_flag = pm_smbchg_otg_unlock_perph_write(smbchg_ptr);
    err_flag |= pm_comm_write_byte_mask(smbchg_ptr->comm_ptr->slave_id, otg_cfg_reg, 0x3F, data, 0);
  }

  return err_flag;
}


pm_err_flag_type pm_smbchg_otg_get_otg_config(uint32 pmic_device, pm_smbchg_otg_cfg_type* otg_cfg)
{
  pm_err_flag_type      err_flag    = PM_ERR_FLAG__SUCCESS;
  pm_register_data_type data;
  pm_smbchg_data_type *smbchg_ptr  = pm_smbchg_get_data(pmic_device);

  if(NULL == smbchg_ptr) 
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if(otg_cfg == NULL)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    pm_register_address_type otg_cfg_reg   = smbchg_ptr->smbchg_register->otg_register->base_address + smbchg_ptr->smbchg_register->otg_register->otg_cfg;
    err_flag = pm_comm_read_byte(smbchg_ptr->comm_ptr->slave_id, otg_cfg_reg, &data, 0);

    if(PM_ERR_FLAG__SUCCESS == err_flag)
    {
      otg_cfg->hv_otg_protection_en = (data & 0x20)? TRUE : FALSE;
      otg_cfg->otg_pin_pol_active_high = (data & 0x10)? TRUE : FALSE;
      otg_cfg->otg_ctrl_type = (pm_smbchg_otg_ctrl_type)((data &0xc)>>2);
      otg_cfg->uvlo_sensor_src = (data&0x2)? (pm_smbchg_otg_uvlo_sensor_src_type )(data&0x3) : PM_SMBCHG_OTG_UVLO_SENSOR__ANA;
    }
  }

  return err_flag;
}

/*This API sets the limit for battery under voltage lockout*/
pm_err_flag_type pm_smbchg_otg_set_otg_batt_uvlo(uint32 pmic_device, uint32 milli_volt)
{
  pm_err_flag_type      err_flag    = PM_ERR_FLAG__SUCCESS;
  pm_register_data_type data;
  pm_smbchg_data_type *smbchg_ptr  = pm_smbchg_get_data(pmic_device);

  if(NULL == smbchg_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    pm_register_address_type cfg_battuv   = smbchg_ptr->smbchg_register->otg_register->base_address + smbchg_ptr->smbchg_register->otg_register->cfg_battuv;

    if(milli_volt < 2900)
    {
      data = 0x00;
    }
    else if(milli_volt < 3100)
    {
      data = 0x01;
    }
    else if(milli_volt < 3300)
    {
      data = 0x02;
    }
    else
    {
      data = 0x03;
    }

    err_flag = pm_smbchg_otg_unlock_perph_write(smbchg_ptr);
    err_flag |= pm_comm_write_byte_mask(smbchg_ptr->comm_ptr->slave_id, cfg_battuv, 0x3, data, 0);
  }

  return err_flag;

}

/*This API reads the set limit for battery under voltage lockout*/
pm_err_flag_type pm_smbchg_otg_get_otg_batt_uvlo(uint32 pmic_device, uint32 *milli_volt)
{
  pm_err_flag_type      err_flag    = PM_ERR_FLAG__SUCCESS;
  pm_register_data_type data;
  pm_smbchg_data_type *smbchg_ptr  = pm_smbchg_get_data(pmic_device);

  if(NULL == smbchg_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    pm_register_address_type cfg_battuv   = smbchg_ptr->smbchg_register->otg_register->base_address + smbchg_ptr->smbchg_register->otg_register->cfg_battuv;

    err_flag = pm_comm_read_byte_mask(smbchg_ptr->comm_ptr->slave_id, cfg_battuv, 0x3, &data, 0);

    if(PM_ERR_FLAG__SUCCESS == err_flag)
    {
      switch(data)
      {
         case 0x00:
           *milli_volt = 2700;  break;
         case 0x01:
           *milli_volt = 2900;  break;
         case 0x02:
           *milli_volt = 3100;  break;
         case 0x03:
           *milli_volt = 3300; break;
         default:
           err_flag = PM_ERR_FLAG__SPMI_OPT2_ERR;
      }
    }
  }
  return err_flag;
}


pm_err_flag_type pm_smbchg_otg_set_otg_i_limit(uint32 pmic_device, uint32 otg_limit_ma)
{
  pm_err_flag_type      err_flag    = PM_ERR_FLAG__SUCCESS;
  pm_register_data_type data;
  pm_smbchg_data_type *smbchg_ptr  = pm_smbchg_get_data(pmic_device);

  if(NULL == smbchg_ptr) 
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    pm_register_address_type otg_icfg   = smbchg_ptr->smbchg_register->otg_register->base_address + smbchg_ptr->smbchg_register->otg_register->otg_icfg;

    if(otg_limit_ma < 600)
    {
      data = 0x00;
    }
    else if(otg_limit_ma < 750)
    {
      data = 0x01;
    }
    else if(otg_limit_ma < 1000)
    {
      data = 0x02;
    }
    else
    {
      data = 0x03;
    }

    err_flag = pm_smbchg_otg_unlock_perph_write(smbchg_ptr);
    err_flag |= pm_comm_write_byte_mask(smbchg_ptr->comm_ptr->slave_id, otg_icfg, 0x3, data, 0);
  }

  return err_flag;
}

pm_err_flag_type pm_smbchg_otg_get_otg_i_limit(uint32 pmic_device, uint32 *otg_limit_ma)
{
  pm_err_flag_type      err_flag    = PM_ERR_FLAG__SUCCESS;
  pm_register_data_type data;
  pm_smbchg_data_type *smbchg_ptr  = pm_smbchg_get_data(pmic_device);

  if(NULL == smbchg_ptr) 
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    pm_register_address_type otg_icfg   = smbchg_ptr->smbchg_register->otg_register->base_address + smbchg_ptr->smbchg_register->otg_register->otg_icfg;

    err_flag = pm_comm_read_byte_mask(smbchg_ptr->comm_ptr->slave_id, otg_icfg, 0x3, &data, 0);

    if(PM_ERR_FLAG__SUCCESS == err_flag)
    {
      switch(data)
      {
         case 0x00:
           *otg_limit_ma = 250;  break;
         case 0x01:
           *otg_limit_ma = 600;  break;
         case 0x02:
           *otg_limit_ma = 750;  break;
         case 0x03:
           *otg_limit_ma = 1000; break;
         default:
           return PM_ERR_FLAG__SPMI_OPT2_ERR;
      }
    }
  }
  return err_flag;
}
//cfg_tloop
//pbs_trig_control
//low_pwr_options


static inline pm_err_flag_type pm_smbchg_otg_unlock_perph_write(pm_smbchg_data_type *smbchg_ptr)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (NULL == smbchg_ptr) 
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {                                     
        pm_register_address_type sec_access = smbchg_ptr->smbchg_register->otg_register->base_address + smbchg_ptr->smbchg_register->otg_register->sec_access;
        err_flag = pm_comm_write_byte(smbchg_ptr->comm_ptr->slave_id, sec_access, 0xA5, 0);
    }

    return err_flag;
}
