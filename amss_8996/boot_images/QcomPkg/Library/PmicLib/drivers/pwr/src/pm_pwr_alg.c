/*! \file pm_pwr_alg.c 
*  \n
*  \brief  
*  \n  
*  \n &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when        who     what, where, why
--------    ---     ----------------------------------------------------------
07/16/14   akm     Comm change Updates
03/31/14    akm     Updated to the latest PMIC driver architecture   
12/06/12    hw      Rearchitecturing module driver to peripheral driver
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_pwr_alg.h"
#include "pm_comm.h"

/*===========================================================================

                     FUNCTION IMPLEMENTATION 

===========================================================================*/

pm_err_flag_type pm_pwr_pull_down_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index, pm_on_off_type on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    
    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (on_off >= PM_INVALID)
    {
        err_flag = PM_ERR_FLAG__MACRO_NOT_RECOGNIZED;
    }
    else
    {
        pm_register_address_type          reg = 0xFF;
        pm_register_data_type             data = 0xFF;
    
        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->PD_CTL);
    
        data = (PM_OFF == on_off) ? 0 : 0x80;

        err_flag = pm_comm_write_byte_mask(comm_ptr->slave_id, reg, 0x80, data, 0);
    }

    return err_flag;
}


pm_err_flag_type pm_pwr_pull_down_status_alg(pm_pwr_data_type *pwr_data,  pm_comm_info_type *comm_ptr, uint8 peripheral_index, pm_on_off_type *on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if ((pwr_data == NULL) || (comm_ptr == NULL) || (on_off == NULL))
    {
        return PM_ERR_FLAG__INVALID_POINTER;
    }

    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
        pm_register_data_type              data = 0;


        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->PD_CTL);

        err_flag = pm_comm_read_byte(comm_ptr->slave_id, reg, &data, 0);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            *on_off = PM_INVALID;
            return err_flag;
        }

        *on_off = (data & 0x80) ? PM_ON : PM_OFF;
    }

    return err_flag;
}



pm_err_flag_type pm_pwr_sw_mode_status_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index, pm_sw_mode_type *sw_mode)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
                                                          
    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (sw_mode == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
        pm_register_data_type              data = 0xFF;
            
        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->MODE_CTL);

        err_flag = pm_comm_read_byte(comm_ptr->slave_id, reg, &data, 0);
        
        if (err_flag == PM_ERR_FLAG__SUCCESS)
        {
            /* BYPASS supercedes NPM & PFM */
            /* Only valid for LDO's */
            if (data & 0x20)
            {
                *sw_mode = PM_SW_MODE_BYPASS;
            }
            /* NPM supercedes AUTO & PFM */
            else if (data & 0x80)
            {
                *sw_mode = PM_SW_MODE_NPM;
            }
            /* AUTO supercedes PFM */
            /* only valid for SMPS */
            else if (data & 0x40)
            {
                *sw_mode = PM_SW_MODE_AUTO;
            }
            else /* IT MIGHT BE PFM DEPENDING ON pc_mode bits */
            {
                *sw_mode = PM_SW_MODE_LPM;
            }
        }
        else
        {
            *sw_mode = PM_SW_MODE_INVALID;
        }
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_pin_ctrl_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index, uint8 select_pin)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index < pwr_data->num_of_peripherals)
    {
        pm_register_address_type           reg = 0x0;

        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->EN_CTL);

        err_flag = pm_comm_write_byte_mask(comm_ptr->slave_id, reg, 0x0F, (pm_register_data_type)select_pin, 1); // bit<0:3>
    }
    else
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_pin_ctrl_status_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index, pm_on_off_type *on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if ((pwr_data == NULL) || (comm_ptr == NULL) || (on_off == NULL))
    {
        return PM_ERR_FLAG__INVALID_POINTER;
    }

    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
        pm_register_data_type              data = 0;

        if (pwr_data->pwr_reg_table == NULL)
        {
            return PM_ERR_FLAG__INVALID_POINTER;
        }

        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->EN_CTL);

        err_flag = pm_comm_read_byte(comm_ptr->slave_id, reg, &data, 0);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            *on_off = PM_INVALID;
            return err_flag;
        }

        if (data & 0x0F)
        {
            *on_off = PM_ON;
        }
        else
        {
            *on_off = PM_OFF;
        }
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_sw_enable_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index, pm_on_off_type on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (on_off >= PM_INVALID)
    {
        err_flag = PM_ERR_FLAG__MACRO_NOT_RECOGNIZED;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
            
        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->EN_CTL);

        if (PM_OFF == on_off)
        {
            err_flag = pm_comm_write_byte_mask(comm_ptr->slave_id, reg, 0x80, 0, 0);
        }
        else
        {
            err_flag = pm_comm_write_byte_mask(comm_ptr->slave_id, reg, 0x80, 0x80, 0);
        }
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_sw_enable_status_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index, pm_on_off_type *on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (on_off == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
        pm_register_data_type              data = 0;

        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->EN_CTL);

        err_flag = pm_comm_read_byte(comm_ptr->slave_id, reg, &data, 0);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            *on_off = PM_INVALID;
            return err_flag;
        }

        if (data & 0x80)
        {
            *on_off = PM_ON;
        }
        else
        {
            *on_off = PM_OFF;
        }
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_volt_level_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index, pm_volt_level_type  volt_level)
{
    pm_err_flag_type                  err_flag = PM_ERR_FLAG__SUCCESS;
    uint32                            vmin = 0;  // microvolt
    uint32                            vStep = 0; // microvolt
    uint32                            range = 0; // 0 to 4
    pm_register_data_type             vset = 0;
    pm_register_data_type             data = 0;
    pm_register_data_type             curr_range = 0;
    pm_register_address_type          status_reg = 0;
    pm_register_address_type          reg = 0;
    

    if (peripheral_index >= (pwr_data->num_of_peripherals))
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED ;
    }
    else
    {
        // Set Range
        reg = pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->VOLTAGE_CTRL1;

        //Read the current range
        err_flag = pm_comm_read_byte(comm_ptr->slave_id, reg, &curr_range, 0);

        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            return err_flag;
        }

        if( (volt_level <= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[curr_range].RangeMax) &&
            (volt_level >= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[curr_range].RangeMin) )
        {
            vmin = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[curr_range].RangeMin;
            vStep = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[curr_range].VStep;
            range = curr_range;
        }
        else
        {
        for(range = pwr_data->pwr_specific_info[peripheral_index].pwr_range->start; range <= pwr_data->pwr_specific_info[peripheral_index].pwr_range->end; range++)
        {
            if( (volt_level <= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMax) &&
                (volt_level >= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin) )
            {
                vmin = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin;
                vStep = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].VStep;
                break;
            }
        }
        }
       
        if (vStep > 0)
        {
            //calculate VSET
            vset = (pm_register_data_type)((volt_level - vmin)/vStep);

            // Set the range first, only if the current range is not equal to requested range
            if(curr_range != ((pm_register_data_type)range))
            {
                status_reg = pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->STATUS;

                //Read the Vreg OK status
                err_flag = pm_comm_read_byte(comm_ptr->slave_id, status_reg, &data, 0);

                if(PM_ERR_FLAG__SUCCESS != err_flag)
                {
                    return err_flag;
                }

                //New range cannot be set if Power rail is ON
                if(data & 0x80)
                {
                    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
                }

                //Set the new range
                err_flag = pm_comm_write_byte_mask(comm_ptr->slave_id, reg, 0x07, (pm_register_data_type)range , 0) ; // 0:2

                if(PM_ERR_FLAG__SUCCESS != err_flag)
                {
                    return err_flag;
                }
            }

             // Set VSET
             reg =  pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->VOLTAGE_CTRL2;
             // Set the vset then
             err_flag = pm_comm_write_byte_mask(comm_ptr->slave_id, reg, 0xFF, vset , 0) ; // 0:7
        }
        else
        {
            err_flag = PM_ERR_FLAG__VLEVEL_OUT_OF_RANGE;
        }
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_volt_level_status_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8  peripheral_index, pm_volt_level_type *volt_level)
{
    pm_err_flag_type                 err_flag = PM_ERR_FLAG__SUCCESS;
    uint32                           vmin = 0; // microvolt
    uint32                           vStep = 0; //microvolt
    uint32                           range = 0; // 0 to 4
    pm_register_data_type            vset = 0;
    pm_register_address_type         reg = 0;
    pm_register_data_type            reg_data[2];

    if (peripheral_index >= (pwr_data->num_of_peripherals))
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (volt_level == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        // get the voltage level LUT
        reg =  (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->VOLTAGE_CTRL1);

        err_flag = pm_comm_read_byte_array(comm_ptr->slave_id, reg, 2, reg_data, 0);

        if ( err_flag == PM_ERR_FLAG__SUCCESS )
        {
            range = reg_data[0]; // 0 to 4
            vset = reg_data[1];
            vmin = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin; // microvolt
            vStep = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].VStep; //microvolt

            *volt_level = vmin + vset * vStep;
        }
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_soft_reset_status_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index, boolean *status)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if ((pwr_data == NULL) || (comm_ptr == NULL) || (status == NULL))
    {
        return PM_ERR_FLAG__INVALID_POINTER;
    }

    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
        pm_register_data_type              data = 0;

        if (pwr_data->pwr_reg_table == NULL)
        {
            return PM_ERR_FLAG__INVALID_POINTER;
        }

        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->PERPH_RESET_CTL4);

        err_flag = pm_comm_read_byte(comm_ptr->slave_id, reg, &data, 0);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            return err_flag;
        }

        if (data & 0x01)
        {
            *status = TRUE;
        }
        else
        {
            *status = FALSE;
        }
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_soft_reset_exit_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if ((pwr_data == NULL) || (comm_ptr == NULL))
    {
        return PM_ERR_FLAG__INVALID_POINTER;
    }

    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
        pm_register_address_type           sec_reg = 0x0;
        pm_register_address_type           perph_reg = 0x0;
        pm_register_data_type              sec_data = 0xA5;
        pm_register_data_type              perph_data = 0;

        if (pwr_data->pwr_reg_table == NULL)
        {
            return PM_ERR_FLAG__INVALID_POINTER;
        }

        sec_reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->SEC_ACCESS);
        perph_reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->PERPH_RESET_CTL4);

        /* Write 0xA5 to sec_access register to get write access to protected registers */
        err_flag = pm_comm_write_byte(comm_ptr->slave_id, sec_reg, sec_data, 0);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            return err_flag;
        }

        /* Write 0 to perph_ctl4 register to get power rail out of soft reset */
        err_flag = pm_comm_write_byte(comm_ptr->slave_id, perph_reg, perph_data, 0);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            return err_flag;
        }
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_volt_level_stepper_done_status_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8  peripheral_index, boolean *stepper_done)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index >= (pwr_data->num_of_peripherals))
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (stepper_done == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        pm_register_address_type          reg = 0;
        pm_register_data_type             data = 0;

        // get the voltage level LUT
        reg =  (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->STATUS);

        err_flag = pm_comm_read_byte(comm_ptr->slave_id, reg, &data, 0);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            *stepper_done = FALSE;
            return err_flag;
        }

        if (data & 0x01)
        {
            *stepper_done = TRUE;
        }
        else
        {
            *stepper_done = FALSE;
        }
    }

    return err_flag;
}

pm_err_flag_type
pm_pwr_vreg_ok_status_alg(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index, boolean *status)
{
    pm_err_flag_type                 err_flag = PM_ERR_FLAG__SUCCESS;
	pm_register_address_type		 status_reg = 0;
	pm_register_address_type		 stepper_done_data = 0;
	pm_register_address_type		 vreg_done_data = 0;
	pm_register_data_type            data = 0;

	if (peripheral_index >= (pwr_data->num_of_peripherals))
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED ;
    }

	status_reg = pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->STATUS;

	//Read the Vreg OK status
    err_flag = pm_comm_read_byte(comm_ptr->slave_id, status_reg, &data, 0);

    vreg_done_data = data & 0x80;

	// LDOs with stepper done 
    if ((0x07 == pwr_data->pwr_reg_table->PERIPH_SUBTYPE) && (0x04 == pwr_data->pwr_reg_table->PERIPH_TYPE))
    {
        stepper_done_data = data & 0x01;
    	if ((TRUE == vreg_done_data) && (TRUE == stepper_done_data))
	    {
	       *status = TRUE;
	    }
    }
	else
	{
	    if (TRUE == vreg_done_data)
	    {
	        *status = TRUE;
	    }
		else
		{
		    *status = FALSE;
		}
	}

return err_flag;
}

