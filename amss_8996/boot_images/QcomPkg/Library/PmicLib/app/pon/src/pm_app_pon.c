/*! \file pm_app_pon.c
*  
*  \brief Implementation file for PON APP level APIs.
*    
*  &copy; Copyright 2014-2015 QUALCOMM Technologies Incorporated, All Rights Reserved
*/
/*===================================================================
EDIT HISTORY FOR MODULE
 This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Date: 2015/11/17 $ 
$Change: 9435347 $
when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
11/11/15   aab     Added back pm_check_pbs_ram_warm_reset_seq_presence() 
10/05/15   aab     Removed Updated pmapp_ps_hold_cfg(). Added pm_app_ps_hold_cfg(), pm_app_pon_reset_cfg() and pm_app_pon_s3_reset_timer_cfg()
09/25/15   aab     Added pm_check_pbs_ram_warm_reset_seq_presence()
08/08/15   aab     Updated pmapp_ps_hold_cfg() to support PON APIs with out doing pmic init
05/31/15   aab     Adding pm_app_log_pon_reasons() and pm_app_log_verbose_pon_reason_status()
04/14/15   al      Adding Hawker support
04/02/15   aab     Updated pmapp_ps_hold_cfg(), 
                    - configure PM8994/PMI8994 individualy instead of using broadcasting command to all 3 PMICs 
01/06/15   kt      Added pmapp_ps_hold_cfg API for multi-pmic config
07/10/14   akm     Creation
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_pon.h"
#include "pmio_pon.h"
#include "pm_err_flags.h"
#include "device_info.h"
#include "pm_config_target.h"
#include "pm_sbl_boot_target.h"
#include "pm_sbl_pon_target.h"
#include "boot_logger.h"
#include <stdio.h>
#include <string.h>


#define PM_MIN_SEC_PMIC_S3_TIMER 64
#define PON_SPARE_BIT_7 0x80
#define PM_SBL_PON_STATUS_MSG_LEN 300

static char pon_status_message[PM_SBL_PON_STATUS_MSG_LEN];
static pm_err_flag_type pm_app_log_verbose_pon_reason_status(uint64 pon_reasons);

typedef struct
{
    uint8 pon_reason1;
    uint8 pon_reason2;
    uint8 poff_reason1;
    uint8 poff_reason2;
    uint8 warm_reset_reason1;
    uint8 warm_reset_reason2;
    uint8 soft_reset_reason1;
    uint8 soft_reset_reason2;
}pm_reason_status_type;



pm_err_flag_type
pm_app_pon_pshold_cfg(pm_app_pon_reset_cfg_type app_pshold_cfg) 
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_model_type pmic_model  = PMIC_IS_INVALID;
   boolean skip_reset_config = FALSE;
   uint32  pmic_index        = 0;

   pm_app_pon_reset_cfg_type   pm_app_pshold_cfg  = PM_APP_PON_CFG_MAX;
   pm_pon_reset_cfg_type       pshold_cfg         = PM_PON_RESET_CFG_INVALID;


   if (app_pshold_cfg >= PM_APP_PON_CFG_MAX) 
   {
      return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
   }

   if ((app_pshold_cfg == PM_APP_PON_CFG_DVDD_HARD_RESET) || (app_pshold_cfg == PM_APP_PON_CFG_DVDD_SHUTDOWN)) 
   {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
   }

   err_flag |= pm_device_setup();
   if (err_flag != PM_ERR_FLAG__SUCCESS) 
   {
      return PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
   }


   while (pmic_index < PM_MAX_NUM_PMICS) 
   {
      pmic_model = pm_get_pmic_model(pmic_index);
      skip_reset_config = FALSE;

      if ((pmic_model != PMIC_IS_INVALID) && (pmic_model != PMIC_IS_UNKNOWN)) 
      {
         err_flag |= pm_tgt_get_pshold_cfg(pmic_index, app_pshold_cfg, &pm_app_pshold_cfg); //Get PMIC specific configuration

         if (err_flag != PM_ERR_FLAG__SUCCESS) 
         {
            break;
         }

         //Map App level allowed config enums to PON config enums
         switch (pm_app_pshold_cfg) {
         case PM_APP_PON_CFG_NO_CONFIG_NEEDED:
            skip_reset_config = TRUE;
            break;
         case PM_APP_PON_CFG_WARM_RESET:
            pshold_cfg = PM_PON_RESET_CFG_WARM_RESET;
            err_flag |= pm_check_pbs_ram_warm_reset_seq_presence(pmic_index);
            break;
         case PM_APP_PON_CFG_HARD_RESET:
            pshold_cfg = PM_PON_RESET_CFG_HARD_RESET;
            break;
         case PM_APP_PON_CFG_NORMAL_SHUTDOWN:
            pshold_cfg = PM_PON_RESET_CFG_NORMAL_SHUTDOWN;
            break;
         case PM_APP_PON_CFG_DVDD_HARD_RESET:
            pshold_cfg = PM_PON_RESET_CFG_D_VDD_BATT_REMOVE_HARD_RESET;
            break;
         case PM_APP_PON_CFG_DVDD_SHUTDOWN:
            pshold_cfg = PM_PON_RESET_CFG_D_VDD_BATT_REMOVE_SHUTDOWN;
            break;
         default:
            return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
         }

         if (skip_reset_config == FALSE) 
         { //Check if NO configuration is needed for this PMIC
            err_flag |= pm_pon_ps_hold_cfg(pmic_index, pshold_cfg);
         }
      }
      pmic_index++;
   }

   return err_flag;
} 




pm_err_flag_type 
pm_app_pon_reset_cfg( pm_app_pon_reset_source_type  app_pon_reset_source, 
                      pm_app_pon_reset_cfg_type     app_pon_reset_cfg, 
                      uint32 s1_timer, 
                      uint32 s2_timer )
{
    pm_err_flag_type err_flag  = PM_ERR_FLAG__SUCCESS;
    boolean skip_reset_config  = FALSE;
    pm_model_type pmic_model   = PMIC_IS_INVALID;
    uint32 pmic_index          = 0;

    pm_pon_reset_source_type    pon_reset_source          = PM_PON_RESET_SOURCE_INVALID;
    pm_app_pon_reset_cfg_type   pm_app_pon_reset_cfg      = PM_APP_PON_CFG_MAX;
    pm_pon_reset_cfg_type       pon_reset_cfg             = PM_PON_RESET_CFG_INVALID;
     
    if(app_pon_reset_source >= PM_APP_PON_RESET_SOURCE_MAX) 
    {
       return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }
    else if (app_pon_reset_source == PM_APP_PON_RESET_SOURCE_RESIN_OR_KPDPWR) 
    {
       return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }


    if(app_pon_reset_cfg >= PM_APP_PON_CFG_MAX)
    {
       return PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
    } 
    else if( (app_pon_reset_cfg != PM_APP_PON_CFG_WARM_RESET) &&
             (app_pon_reset_cfg != PM_APP_PON_CFG_DVDD_HARD_RESET) )
    {
       return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }


    err_flag |= pm_device_setup();
    if (err_flag != PM_ERR_FLAG__SUCCESS) 
    {
       return PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }

    //Map App level allowed PON reset source enum to PON source enum
    switch(app_pon_reset_source)
    {
       case PM_APP_PON_RESET_SOURCE_KPDPWR:
       {
          pon_reset_source = PM_PON_RESET_SOURCE_KPDPWR;
       }
       break;
       case PM_APP_PON_RESET_SOURCE_RESIN:
       {
          pon_reset_source = PM_PON_RESET_SOURCE_RESIN;
       }
       break;
       case PM_APP_PON_RESET_SOURCE_RESIN_AND_KPDPWR:
       {
          pon_reset_source = PM_PON_RESET_SOURCE_RESIN_AND_KPDPWR;
       }
       break;
       default:
          return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    while (pmic_index < PM_MAX_NUM_PMICS)
    {
       pmic_model = pm_get_pmic_model(pmic_index);
       skip_reset_config = FALSE;

       if ((pmic_model != PMIC_IS_INVALID) && (pmic_model != PMIC_IS_UNKNOWN))
       {
          //Get individual PMIC specific configuration
          err_flag |= pm_tgt_get_pon_reset_cfg(pmic_index, app_pon_reset_source, app_pon_reset_cfg, &pm_app_pon_reset_cfg);
          if (err_flag != PM_ERR_FLAG__SUCCESS) 
          {
             break;
          }

          //Map App level allowed config enums to the PON config enums 
          switch(pm_app_pon_reset_cfg)
          {
             case PM_APP_PON_CFG_NO_CONFIG_NEEDED:
                skip_reset_config = TRUE;
                break;
             case PM_APP_PON_CFG_DVDD_HARD_RESET:
                pon_reset_cfg = PM_PON_RESET_CFG_D_VDD_BATT_REMOVE_HARD_RESET; 
                break;
             case PM_APP_PON_CFG_WARM_RESET:
                pon_reset_cfg = PM_PON_RESET_CFG_WARM_RESET;
                break;
             default:
                return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
          }

          if (skip_reset_config == FALSE) //Check if NO configuration is needed for this PMIC
          {
             err_flag |= pm_pon_reset_source_cfg(  pmic_index, 
                                                   pon_reset_source,
                                                   s1_timer,   
                                                   s2_timer,   
                                                   pon_reset_cfg   
                                                 );
          }
       }
       pmic_index++;
    }

    return err_flag;
} 





pm_err_flag_type 
pm_app_pon_s3_reset_timer_cfg( pm_app_pon_reset_source_type  app_pon_s3_reset_source, uint32 s3_timer )
{
    pm_err_flag_type err_flag  = PM_ERR_FLAG__SUCCESS;
    boolean skip_reset_config  = FALSE;
    pm_model_type pmic_model   = PMIC_IS_INVALID;
    uint32 pmic_index          = 0;
    pm_pon_reset_source_type    pon_s3_reset_source = PM_PON_RESET_SOURCE_INVALID;
    uint32                      pm_s3_reset_timer_multiplier;
    uint32                      pm_s3_timer;

    if(app_pon_s3_reset_source >= PM_APP_PON_RESET_SOURCE_MAX)
    {
       return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }

    err_flag |= pm_device_setup();
    if (err_flag != PM_ERR_FLAG__SUCCESS) 
    {
       return PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }

    //Map App level allowed PON reset source enum
    switch(app_pon_s3_reset_source)
    {
       case PM_APP_PON_RESET_SOURCE_KPDPWR:
       {
          pon_s3_reset_source = PM_PON_RESET_SOURCE_KPDPWR;
       }
       break;
       case PM_APP_PON_RESET_SOURCE_RESIN:
       {
          pon_s3_reset_source = PM_PON_RESET_SOURCE_RESIN;
       }
       break;
       case PM_APP_PON_RESET_SOURCE_RESIN_AND_KPDPWR:
       {
          pon_s3_reset_source = PM_PON_RESET_SOURCE_RESIN_AND_KPDPWR;
       }
       break;
       case PM_APP_PON_RESET_SOURCE_RESIN_OR_KPDPWR:
       {
          pon_s3_reset_source = PM_PON_RESET_SOURCE_RESIN_OR_KPDPWR;
       }
       break;
       default:
          return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }


    while (pmic_index < PM_MAX_NUM_PMICS)
    {
       pmic_model = pm_get_pmic_model(pmic_index);
       skip_reset_config = FALSE;

       if ((pmic_model != PMIC_IS_INVALID) && (pmic_model != PMIC_IS_UNKNOWN))
       {
          //Get individual PMIC specific configuration
          err_flag |= pm_tgt_get_pon_s3_reset_timer_multiplier( pmic_index, app_pon_s3_reset_source, &pm_s3_reset_timer_multiplier);
          if (err_flag != PM_ERR_FLAG__SUCCESS) 
          {
             break;
          }

          if (pm_s3_reset_timer_multiplier != 0)
          {//Configure S3 reset timer 
             pm_s3_timer = s3_timer * pm_s3_reset_timer_multiplier;

              //S3 timer value for all Secondary PMICs should be at least 64 sec
              if( (pmic_index > 0) && (pm_s3_timer < PM_MIN_SEC_PMIC_S3_TIMER))
              {
                  pm_s3_timer = PM_MIN_SEC_PMIC_S3_TIMER;
              }
              err_flag |= pm_pon_stage3_reset_source_cfg(pmic_index, pon_s3_reset_source, pm_s3_timer);
          }
       }
       pmic_index++;
    }

    return err_flag;
}






pm_err_flag_type 
pm_pon_uvlo_reset_status (uint32 pmic_device_index,boolean * status)
{
    pm_err_flag_type err_flag   = PM_ERR_FLAG__SUCCESS;
    uint8 pmic_spare_bit        = 0;
    uint8 warm_reset_reason_bit = 0;
    pm_pon_poff_reason_type poff_reason;
    pm_pon_warm_reset_reason_type warm_reset_reason;
    uint8 reg_data;
	
    pm_pon_get_poff_reason(pmic_device_index, &poff_reason);
    pm_pon_get_warm_reset_reason(pmic_device_index,&warm_reset_reason);
    pm_pon_get_spare_reg_data( pmic_device_index,PM_PON_XVDD_RB_SPARE ,&reg_data); 


    /*'pmic_spare_bit' bit<7> of XVDD_RB_SPARE reg is used as a flag for UVLO reset*/
	
    if( PON_SPARE_BIT_7 == reg_data)
    {
       pmic_spare_bit = 1;
    }

    /* Check if there is any warm reset reason bit is set*/
    warm_reset_reason_bit = warm_reset_reason.gp1 || warm_reset_reason.gp2 || 
                            warm_reset_reason.kpdpwr || warm_reset_reason.kpdpwr_and_resin || 
                            warm_reset_reason.pmic_wd || warm_reset_reason.ps_hold || 
                            warm_reset_reason.reserved || warm_reset_reason.reserved1 || 
                            warm_reset_reason.resin || warm_reset_reason.soft || 
                            warm_reset_reason.tft ;  

    /* If poff reason is UVLO and no warm_reset_reason and XVDD_SPARE_REG bit<7> is set, then return 
           UVLO reset status as TRUE */
    if (((poff_reason.gp1 ==1 ) || (poff_reason.uvlo == 1)) && (warm_reset_reason_bit == 0) && (pmic_spare_bit == 1))
    {
     *status = TRUE;
    }
    else
    {
     *status = FALSE;
    }

    return err_flag;
}



pm_err_flag_type 
pm_log_pon_reasons(boolean pon_verbose_log_mode)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_model_type pmic_model = PMIC_IS_INVALID;
   uint8 pmic_index = 0;
   uint64 pon_reasons;
   char temp_str[PM_SBL_PON_STATUS_MSG_LEN];
   char pon_status_message[PM_SBL_PON_STATUS_MSG_LEN];


   if(pon_verbose_log_mode == TRUE)
   {
      boot_log_message("BEGIN PON REASENS LOG -------------------------");
   }
  
   //Get and log all the PON reasons for all PMICs
   for (pmic_index = 0; pmic_index < PM_MAX_NUM_PMICS; pmic_index++)
   {
      pmic_model = pm_get_pmic_model(pmic_index);
      if ((pmic_model != PMIC_IS_INVALID) && (pmic_model != PMIC_IS_UNKNOWN) )
      {
         err_flag = pm_pon_get_all_pon_reasons(pmic_index, &pon_reasons);

         if (pon_verbose_log_mode == FALSE)
         {
            snprintf(pon_status_message, PM_SBL_PON_STATUS_MSG_LEN, "PM%d:0x%llx ",(uint8)pmic_index, (uint64)pon_reasons);
            strlcat(temp_str,pon_status_message, sizeof(temp_str));
         }
         else
         { //If verbose PON logging is enabled
            snprintf(pon_status_message, PM_SBL_PON_STATUS_MSG_LEN, "PON REASON for PMIC# %d: 0x%llx ",(uint8)pmic_index, (uint64)pon_reasons);
            boot_log_message(pon_status_message);
            err_flag |= pm_app_log_verbose_pon_reason_status(pon_reasons);
         }
      }
   }

   if(pon_verbose_log_mode == FALSE)
   {
      //Log REASON STATUS for each PMICPON1-PON2-WRM1-WRM2-OFF1-OFF2-SFT1-SFT2 
      snprintf(pon_status_message, PM_SBL_PON_STATUS_MSG_LEN, "PON REASON:%s",temp_str);
      boot_log_message(pon_status_message);
   }
   else
   {
      boot_log_message("END PON REASENS LOG -------------------------");
   }

   return err_flag;
}




pm_err_flag_type 
pm_app_log_verbose_pon_reason_status(uint64 pon_reasons)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_reason_status_type *reasons_ptr = (pm_reason_status_type*)(&pon_reasons);

   //PON RESEN1
   snprintf(pon_status_message, PM_SBL_PON_STATUS_MSG_LEN, "PON RESEN1 STATUS: 0x%lx", (uintnt)reasons_ptr->pon_reason1);
   boot_log_message(pon_status_message);

   if ((uintnt)(reasons_ptr->pon_reason1) != 0x00) 
   {
       if ((uintnt)(reasons_ptr->pon_reason1) & PMIO_PON_PON_REASON1_KPDPWR_N_BMSK) 
       {
           boot_log_message("    KPDPWR_N");
       }
       if((uintnt)(reasons_ptr->pon_reason1) & PMIO_PON_PON_REASON1_CBLPWR_N_BMSK)
       {
           boot_log_message("    CBLPWR_N");
       }
       if((uintnt)(reasons_ptr->pon_reason1) & PMIO_PON_PON_REASON1_PON1_BMSK)
       {
           boot_log_message("    PON1");
       }
       if((uintnt)(reasons_ptr->pon_reason1) & PMIO_PON_PON_REASON1_USB_CHG_BMSK)
       {
           boot_log_message("    USB_CHG");
       }
       if((uintnt)(reasons_ptr->pon_reason1) & PMIO_PON_PON_REASON1_DC_CHG_BMSK)
       {
           boot_log_message("    DC_CHG");
       }
       if((uintnt)(reasons_ptr->pon_reason1) & PMIO_PON_PON_REASON1_RTC_BMSK)
       {
           boot_log_message("    RTC");
       }
       if((uintnt)(reasons_ptr->pon_reason1) & PMIO_PON_PON_REASON1_SMPL_BMSK)
       {
           boot_log_message("    SMPL");
       }
       if((uintnt)(reasons_ptr->pon_reason1) & PMIO_PON_PON_REASON1_HARD_RESET_BMSK)
       {
           boot_log_message("    HARD_RESET");
       }
   }


   //PON RESEN2 
   snprintf(pon_status_message, PM_SBL_PON_STATUS_MSG_LEN, "PON RESEN2 STATUS: 0x%lx", (uintnt)reasons_ptr->pon_reason2);
   boot_log_message(pon_status_message);


   //POFF RESEN1
   snprintf(pon_status_message, PM_SBL_PON_STATUS_MSG_LEN, "POFF RESEN1 STATUS: 0x%lx", (uintnt)reasons_ptr->poff_reason1);
   boot_log_message(pon_status_message);

   if ((uintnt)(reasons_ptr->poff_reason1) != 0x00) 
   {
       if ((uintnt)(reasons_ptr->poff_reason1) & PMIO_PON_POFF_REASON1_KPDPWR_N_BMSK) 
       {
           boot_log_message("    KPDPWR_N");
       }
       if((uintnt)(reasons_ptr->poff_reason1) & PMIO_PON_POFF_REASON1_RESIN_N_BMSK)
       {
           boot_log_message("    RESIN_N");
       }
       if((uintnt)(reasons_ptr->poff_reason1) & PMIO_PON_POFF_REASON1_KPDPWR_AND_RESIN_BMSK)
       {
           boot_log_message("    KPDPWR_AND_RESIN");
       }
       if((uintnt)(reasons_ptr->poff_reason1) & PMIO_PON_POFF_REASON1_GP2_BMSK)
       {
           boot_log_message("    GP2");
       }
       if((uintnt)(reasons_ptr->poff_reason1) & PMIO_PON_POFF_REASON1_GP1_BMSK)
       {
           boot_log_message("    GP1");
       }
       if((uintnt)(reasons_ptr->poff_reason1) & PMIO_PON_POFF_REASON1_PMIC_WD_BMSK)
       {
           boot_log_message("    PMIC_WD");
       }
       if((uintnt)(reasons_ptr->poff_reason1) & PMIO_PON_POFF_REASON1_PS_HOLD_BMSK)
       {
           boot_log_message("    PS_HOLD");
       }
       if((uintnt)(reasons_ptr->poff_reason1) & PMIO_PON_POFF_REASON1_SOFT_BMSK)
       {
           boot_log_message("    SOFT");
       }
   }


   //POFF RESEN2
   snprintf(pon_status_message, PM_SBL_PON_STATUS_MSG_LEN, "POFF RESEN2 STATUS: 0x%lx", (uintnt)reasons_ptr->poff_reason2);
   boot_log_message(pon_status_message);

   if ((uintnt)(reasons_ptr->poff_reason2) != 0x00) 
   {
       if ((uintnt)(reasons_ptr->poff_reason2) & PMIO_PON_POFF_REASON2_STAGE3_BMSK) 
       {
           boot_log_message("    STAGE3");
       }
       if((uintnt)(reasons_ptr->poff_reason2) & PMIO_PON_POFF_REASON2_OTST3_BMSK)
       {
           boot_log_message("    OTST3");
       }
       if((uintnt)(reasons_ptr->poff_reason2) & PMIO_PON_POFF_REASON2_STAGE3_BMSK)
       {
           boot_log_message("    STAGE3");
       }
       if((uintnt)(reasons_ptr->poff_reason2) & PMIO_PON_POFF_REASON2_UVLO_BMSK)
       {
           boot_log_message("    UVLO");
       }
       if((uintnt)(reasons_ptr->poff_reason2) & PMIO_PON_POFF_REASON2_TFT_BMSK)
       {
           boot_log_message("    TFT");
       }
       if((uintnt)(reasons_ptr->poff_reason2) & PMIO_PON_POFF_REASON2_CHARGER_BMSK)
       {
           boot_log_message("    CHARGER");
       }
   }


   //WARM RESET RESEN1
   snprintf(pon_status_message, PM_SBL_PON_STATUS_MSG_LEN, "WARM RESET RESEN1 STATUS: 0x%lx", (uintnt)reasons_ptr->warm_reset_reason1);
   boot_log_message(pon_status_message);

   if ((uintnt)(reasons_ptr->warm_reset_reason1) != 0x00) 
   {
       if ((uintnt)(reasons_ptr->warm_reset_reason1) & PMIO_PON_WARM_RESET_REASON1_KPDPWR_N_BMSK) 
       {
           boot_log_message("    KPDPWR_N");
       }
       if((uintnt)(reasons_ptr->warm_reset_reason1) & PMIO_PON_WARM_RESET_REASON1_KPDPWR_N_BMSK)
       {
           boot_log_message("    KPDPWR_N");
       }
       if((uintnt)(reasons_ptr->warm_reset_reason1) & PMIO_PON_WARM_RESET_REASON1_RESIN_N_BMSK)
       {
           boot_log_message("    RESIN_N");
       }
       if((uintnt)(reasons_ptr->warm_reset_reason1) & PMIO_PON_WARM_RESET_REASON1_KPDPWR_AND_RESIN_BMSK)
       {
           boot_log_message("    KPDPWR_AND_RESIN");
       }
       if((uintnt)(reasons_ptr->warm_reset_reason1) & PMIO_PON_WARM_RESET_REASON1_GP2_BMSK)
       {
           boot_log_message("    GP2");
       }
       if((uintnt)(reasons_ptr->warm_reset_reason1) & PMIO_PON_WARM_RESET_REASON1_GP1_BMSK)
       {
           boot_log_message("    GP1");
       }
       if((uintnt)(reasons_ptr->warm_reset_reason1) & PMIO_PON_WARM_RESET_REASON1_PMIC_WD_BMSK)
       {
           boot_log_message("    PMIC_WD");
       }
       if((uintnt)(reasons_ptr->warm_reset_reason1) & PMIO_PON_WARM_RESET_REASON1_PS_HOLD_BMSK)
       {
           boot_log_message("    PS_HOLD");
       }
       if((uintnt)(reasons_ptr->warm_reset_reason1) & PMIO_PON_WARM_RESET_REASON1_SOFT_BMSK)
       {
           boot_log_message("    SOFT");
       }
   }


   //WARM RESET RESEN2
   snprintf(pon_status_message, PM_SBL_PON_STATUS_MSG_LEN, "WARM RESET RESEN2 STATUS: 0x%lx", (uintnt)reasons_ptr->warm_reset_reason2);
   boot_log_message(pon_status_message);
   if((uintnt)(reasons_ptr->warm_reset_reason2) & PMIO_PON_WARM_RESET_REASON2_TFT_BMSK)
   {
       boot_log_message("    TFT");
   }


   //SOFT RESET RESEN1
   snprintf(pon_status_message, PM_SBL_PON_STATUS_MSG_LEN, "SOFT RESET RESEN1 STATUS: 0x%lx", (uintnt)reasons_ptr->soft_reset_reason1);
   boot_log_message(pon_status_message);

   if ((uintnt)(reasons_ptr->soft_reset_reason1) != 0x00) 
   {
       if ((uintnt)(reasons_ptr->soft_reset_reason1) & PMIO_PON_SOFT_RESET_REASON1_KPDPWR_N_BMSK) 
       {
           boot_log_message("    KPDPWR_N");
       }
       if((uintnt)(reasons_ptr->soft_reset_reason1) & PMIO_PON_SOFT_RESET_REASON1_RESIN_N_BMSK)
       {
           boot_log_message("    RESIN_N");
       }
       if((uintnt)(reasons_ptr->soft_reset_reason1) & PMIO_PON_SOFT_RESET_REASON1_KPDPWR_AND_RESIN_BMSK)
       {
           boot_log_message("    KPDPWR_AND_RESIN");
       }
       if((uintnt)(reasons_ptr->soft_reset_reason1) & PMIO_PON_SOFT_RESET_REASON1_GP2_BMSK)
       {
           boot_log_message("    GP2");
       }
       if((uintnt)(reasons_ptr->soft_reset_reason1) & PMIO_PON_SOFT_RESET_REASON1_GP1_BMSK)
       {
           boot_log_message("    GP1");
       }
       if((uintnt)(reasons_ptr->soft_reset_reason1) & PMIO_PON_SOFT_RESET_REASON1_PMIC_WD_BMSK)
       {
           boot_log_message("    PMIC_WD");
       }
       if((uintnt)(reasons_ptr->soft_reset_reason1) & PMIO_PON_SOFT_RESET_REASON1_PS_HOLD_BMSK)
       {
           boot_log_message("    PS_HOLD");
       }
       if((uintnt)(reasons_ptr->soft_reset_reason1) & PMIO_PON_SOFT_RESET_REASON1_SOFT_BMSK)
       {
           boot_log_message("    SOFT");
       }
   }


   //SOFT RESET RESEN2
   snprintf(pon_status_message, PM_SBL_PON_STATUS_MSG_LEN, "SOFT RESET RESEN2 STATUS: 0x%lx", (uintnt)reasons_ptr->soft_reset_reason1);
   boot_log_message(pon_status_message);
   if((uintnt)(reasons_ptr->soft_reset_reason1) & PMIO_PON_SOFT_RESET_REASON2_TFT_BMSK)
   {
       boot_log_message("    TFT");
   }

   return err_flag;

}

