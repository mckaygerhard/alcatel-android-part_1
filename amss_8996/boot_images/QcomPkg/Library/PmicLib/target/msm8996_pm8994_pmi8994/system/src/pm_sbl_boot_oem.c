/*! \file pm_sbl_boot_oem.c
*  \n
*  \brief This file contains PMIC device initialization function where initial PMIC
*  \n SBL settings are configured through the PDM auto-generated code.
*  \n
*  \n &copy; Copyright 2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/03/16   aab     Added Config to set EN_IIN_OPT (EN_INN_BUF_SYSON_LDO)
01/29/16   aab     Configure PMI8996 FG clock to 200KHz 
01/19/15   aab     Removed unused code that supports v1
11/12/15   aab     Updated pm_device_post_init()
10/07/15   aab     Used App level API instead of driver level API: pm_app_pon_pshold_cfg() and pm_app_pon_reset_cfg()
08/08/15   aab     Updated pm_driver_post_init() to ensure that Rails for download mode configured every bootup
05/21/14   aab     Creation
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_sbl_boot_target.h"
#include "DDIPlatformInfo.h"
#include "tct.h"


/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/



/*===========================================================================

                        FUNCTION DEFINITIONS 

===========================================================================*/

pm_err_flag_type
pm_device_pre_init(void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  return err_flag;
}




pm_err_flag_type
pm_device_post_init(void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_model_type pmic_model = PMIC_IS_UNKNOWN;
  uint32 pmi_device_index = 1; //PMI8994/PMI8996
  
  //These configurations is only used for development phones and should be commented out for production phones
  err_flag |= pm_app_pon_pshold_cfg(PM_APP_PON_CFG_WARM_RESET);
  err_flag |= pm_app_pon_reset_cfg( PM_APP_PON_RESET_SOURCE_KPDPWR, PM_APP_PON_CFG_WARM_RESET, 10256, 2000); //PON KPDPWR PON Reset configuration
  err_flag |= pm_app_pon_reset_cfg( PM_APP_PON_RESET_SOURCE_RESIN_AND_KPDPWR, PM_APP_PON_CFG_DVDD_HARD_RESET, 10256, 2000); //PON RESIN_AND_KPDPWR PON Reset configuration
#ifdef FEATURE_TCTNB_IDOL4S_CN /* [BUFFIX]-Add- by TCTSH.XQJ, PR-982570, 2016/01/13,use s3 timer for reset,8s*/
     pm_pon_stage3_reset_source_cfg(0, PM_PON_RESET_SOURCE_KPDPWR, 8);
#endif
  err_flag |= pm_comm_write_byte(1, 0x3241, 0xA5, 1); //Set VDD_APCC Rail level

  /* Enable SMPS-12A to PWM mode for DDR PX-1 on V2, should be ok for V1 as well*/
  err_flag |= pm_comm_write_byte(1, 0x3545, 0x80, 1);


  if (DalPlatformInfo_IsFusion() == TRUE)
  {
     /* Configure PM MPP2 to not follow warm_rb for download mode
        to work on fusion platforms */
     err_flag |= pm_comm_write_byte(0, 0xA1D0, 0xA5, 1);
     err_flag |= pm_comm_write_byte(0, 0xA1DA, 0x0B, 1);
  }

   pmic_model = pm_get_pmic_model(pmi_device_index);
   if(pmic_model == PMIC_IS_PMI8996)
   {
      //Configure FG clock to 200KHz
      err_flag |= pm_comm_write_byte(2, 0x40D0, 0xA5, 1);
      err_flag |= pm_comm_write_byte_mask(2, 0x40F5, 0x01, 0x01, 1);
   }


   if(pmic_model == PMIC_IS_PMI8994)
   {
      //Set USB_CHGPTH_NEAL6 Bit[2] EN_IIN_OPT; 
      err_flag |= pm_comm_write_byte(2, 0x13D0, 0xA5, 1);
      err_flag |= pm_comm_write_byte_mask(2, 0x13FC, 0x04, 0x04, 1);
   }

  return err_flag;
}




pm_err_flag_type
pm_driver_pre_init (void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  return err_flag;
}




pm_err_flag_type
pm_driver_post_init (void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  //Ensure that Rails for download mode configured to default level(2.95V) and turned ON
  err_flag |= pm_ldo_volt_level(0, PM_LDO_13, 2950000);
  err_flag |= pm_ldo_sw_enable (0, PM_LDO_13, PM_ON);
  err_flag |= pm_ldo_sw_enable (0, PM_LDO_21, PM_OFF);	//Add by TCT.NB-qw Turn off L21 for sdvdd

  return err_flag;
}



pm_err_flag_type
pm_sbl_chg_pre_init (void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  
  return err_flag;
}


pm_err_flag_type
pm_sbl_chg_post_init (void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  return err_flag;
}




pm_err_flag_type
pm_app_config_download_mode(void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  if (DalPlatformInfo_IsFusion() == TRUE)
  {
     /* Configure PM MPP2 to not follow warm_rb, shutdownx_rb for download mode
        to work on fusion platforms */
     err_flag |= pm_comm_write_byte(0, 0xA1D0, 0xA5, 1);
     err_flag |= pm_comm_write_byte(0, 0xA1DA, 0x08, 1);
  }

  return err_flag;
}

