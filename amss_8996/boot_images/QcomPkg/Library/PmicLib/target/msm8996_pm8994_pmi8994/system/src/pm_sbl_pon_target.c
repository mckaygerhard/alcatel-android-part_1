/*! \file pm_sbl_pon_target.c
*  \n
*  \brief This file contains PMIC device initialization function where initial PMIC
*  \n SBL settings are configured through the PDM auto-generated code.
*  \n
*  \n &copy; Copyright 2015 Qualcomm Technologies Inc, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/08/15   aab     Created
========================================================================== */
/*===========================================================================

                     INCLUDE FILES

===========================================================================*/
#include "pm_sbl_pon_target.h"


/*===========================================================================

                     Global Variables

===========================================================================*/

//PSHOlD config for each PMIC in target
static const pm_app_pon_reset_cfg_type
pm_app_pshold_reset_cfg_arr[PM_APP_PON_CFG_MAX][PM_MAX_NUM_PMICS] =
{  //       PM8994                              PMI8994                       PM8004                             PMK8001
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED, PM_APP_PON_CFG_NO_CONFIG_NEEDED, PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,  0,0,0}, //No config needed
   {PM_APP_PON_CFG_WARM_RESET,       PM_APP_PON_CFG_WARM_RESET,       PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_WARM_RESET,        0,0,0}, //Warm Reset
   {PM_APP_PON_CFG_HARD_RESET,       PM_APP_PON_CFG_NORMAL_SHUTDOWN,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NORMAL_SHUTDOWN,   0,0,0}, //Hard Reset
   {PM_APP_PON_CFG_NORMAL_SHUTDOWN,  PM_APP_PON_CFG_NORMAL_SHUTDOWN,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NORMAL_SHUTDOWN,   0,0,0}, //Shutdown
   {PM_APP_PON_CFG_DVDD_HARD_RESET,  PM_APP_PON_CFG_DVDD_SHUTDOWN,    PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_DVDD_SHUTDOWN,     0,0,0}, //Dvdd hard reset
   {PM_APP_PON_CFG_DVDD_SHUTDOWN,    PM_APP_PON_CFG_DVDD_SHUTDOWN,    PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_DVDD_SHUTDOWN,     0,0,0} //DVDD Shutdown
};




//Kpdpwr:  PON Reset Config
static const pm_app_pon_reset_cfg_type
pm_app_pon_kpdpwr_reset_cfg_arr[PM_APP_PON_CFG_MAX][PM_MAX_NUM_PMICS] =
{  //       PM8994                              PMI8994                       PM8004                             PMK8001
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //No config needed
   {PM_APP_PON_CFG_WARM_RESET,        PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_WARM_RESET,       0,0,0}, //Warm Reset
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //Hard Reset
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //Shutdown
   {PM_APP_PON_CFG_DVDD_HARD_RESET,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //DVDD Hard Reset
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}  //DVDD Shutdown
};

//Resin:  PON Reset Config
static const pm_app_pon_reset_cfg_type
pm_app_pon_resin_reset_cfg_arr[PM_APP_PON_CFG_MAX][PM_MAX_NUM_PMICS] =
{  //       PM8994                              PMI8994                       PM8004                             PMK8001
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //No config needed
   {PM_APP_PON_CFG_WARM_RESET,        PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_WARM_RESET,       0,0,0}, //Warm Reset
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //Hard Reset
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //Shutdown
   {PM_APP_PON_CFG_DVDD_HARD_RESET,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //DVDD Hard Reset
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}  //DVDD Shutdown
};

//Resin_and_Kpdpwr(key_combo): PON Reset Config
static const pm_app_pon_reset_cfg_type
pm_app_pon_keycombo_reset_cfg_arr[PM_APP_PON_CFG_MAX][PM_MAX_NUM_PMICS] =
{  //       PM8994                              PMI8994                       PM8004                             PMK8001
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //No config needed
   {PM_APP_PON_CFG_WARM_RESET,        PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_WARM_RESET,       0,0,0},  //Warm Reset
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //Hard Reset
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //Shutdown
   {PM_APP_PON_CFG_DVDD_HARD_RESET,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}, //DVDD Hard Reset
   {PM_APP_PON_CFG_NO_CONFIG_NEEDED,  PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED,   PM_APP_PON_CFG_NO_CONFIG_NEEDED, 0,0,0}  //DVDD Shutdown
};



//S3 reset Timer Config
static const uint8
pm_app_pon_s3_reset_timer_multiplier_arr[PM_APP_PON_RESET_SOURCE_MAX][PM_MAX_NUM_PMICS] =
{  //PM8994, PMI8994, PM8004, PMK8001, ...
   { 1,  3,   0,   3,  0,  0,  0},    //KPDPWR_N
   { 1,  3,   0,   3,  0,  0,  0},    //RESIN_N
   { 1,  3,   0,   3,  0,  0,  0},    //KPDPWR_AND_RESIN
   { 1,  3,   0,   3,  0,  0,  0}     //KPDPWR_OR_RESIN
};



/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
pm_err_flag_type pm_tgt_get_pshold_cfg( uint32                     pmic_index,
                                        pm_app_pon_reset_cfg_type  app_pshold_cfg,
                                        pm_app_pon_reset_cfg_type* pm_app_pshold_cfg )
{
  if( (pmic_index        >= PM_MAX_NUM_PMICS)   ||  
      (app_pshold_cfg    >= PM_APP_PON_CFG_MAX) ||
      (pm_app_pshold_cfg == NULL) )
  {
    return PM_ERR_FLAG__PAR_OUT_OF_RANGE;
  }

  *pm_app_pshold_cfg = pm_app_pshold_reset_cfg_arr[app_pshold_cfg][pmic_index];

  return PM_ERR_FLAG__SUCCESS;
}




pm_err_flag_type
pm_tgt_get_pon_reset_cfg( uint32                        pmic_index,
                          pm_app_pon_reset_source_type  app_pon_reset_source,
                          pm_app_pon_reset_cfg_type     app_pon_reset_cfg,
                          pm_app_pon_reset_cfg_type*    pm_app_pon_reset_cfg //pmic specific config
                        )
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;


    if( (pmic_index           >= PM_MAX_NUM_PMICS)           || 
        (app_pon_reset_source >= PM_APP_PON_RESET_SOURCE_MAX)||
        (app_pon_reset_cfg    >= PM_APP_PON_CFG_MAX)         ||
        (pm_app_pon_reset_cfg == NULL) )
    {
       return PM_ERR_FLAG__PAR_OUT_OF_RANGE;
    }


    switch(app_pon_reset_source)
    {
       case PM_APP_PON_RESET_SOURCE_KPDPWR:
       {
          *pm_app_pon_reset_cfg = pm_app_pon_kpdpwr_reset_cfg_arr[app_pon_reset_cfg][pmic_index];
       }
       break;
       case PM_APP_PON_RESET_SOURCE_RESIN:
       {
          *pm_app_pon_reset_cfg = pm_app_pon_resin_reset_cfg_arr[app_pon_reset_cfg][pmic_index];
       }
       break;
       case PM_APP_PON_RESET_SOURCE_RESIN_AND_KPDPWR:
       {
          *pm_app_pon_reset_cfg = pm_app_pon_keycombo_reset_cfg_arr[app_pon_reset_cfg][pmic_index];
       }
       break;
       default:
          return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    return err_flag;
}





pm_err_flag_type
pm_tgt_get_pon_s3_reset_timer_multiplier( uint32                        pmic_index,
                                          pm_app_pon_reset_source_type  app_pon_s3_reset_source,
                                          uint32*                       pm_s3_reset_timer_multiplier )
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if( (pmic_index                   >= PM_MAX_NUM_PMICS)           || 
        (app_pon_s3_reset_source      >= PM_APP_PON_RESET_SOURCE_MAX)||
        (pm_s3_reset_timer_multiplier == NULL) )
    {
       return PM_ERR_FLAG__PAR_OUT_OF_RANGE;
    }

    *pm_s3_reset_timer_multiplier = pm_app_pon_s3_reset_timer_multiplier_arr[app_pon_s3_reset_source][pmic_index];

    return err_flag;
}


