/*! \file pm_sbl_boot.c
*  \n
*  \brief This file contains PMIC device initialization function where initial PMIC
*  \n SBL settings are configured through the PDM auto-generated code.
*  \n
*  \n &copy; Copyright 2013-2016 Qualcomm Technologies Inc, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/29/16   aab     Added pm_pbs_ram_image_verify_minimum() and pm_pbs_ram_image_verify_minimum()
01/19/15   aab     Removed unused code that supports v1
12/22/15   aab     Updated pm_check_pbs_ram_warm_reset_seq_presence() to insure that it is applicable only for primary pmic
10/16/15   aab     Added support for V4 power grid variant
09/01/15   aab     Updated driver to support SBL Conditional configuration feature 
10/05/15   aab     Added pm_sbl_get_pon_reset_cfg() and pm_sbl_get_ps_hold_cfg()
09/25/15   aab     Added pm_check_pbs_ram_warm_reset_seq_presence() 
09/02/15   aab     Enable SBL charging for Liquid and Fluid targets
08/26/15   aab     Added PMI8996 support
08/08/15   aab     Moved out pm_device_setup() to pm_loader_init.c
07/13/15   pb      Moved Settings validation test function call from pm_device_init() to pm_sbl_load_config()
06/23/15   aab     Added support for MSM8996 V2/V3. Dropped support for V1
05/31/15   aab     Added pm_sbl_chg_init(); Moved out  pm_log_all_pon_reasons() to APP level API
                   Updated pm_device_init() and moved out pre/post init functions to a separate file
04/29/15   aab     Added support for PMK8001
05/10/15   aab     Enable SBL charging and charging related parameter conditional configuration
04/28/15   aab     Updated pm_device_programmer_init(); make workaround only applicable for V1 target
04/17/15   aab     Added pm_sbl_config_v1_specific_powergrid() to support msm8996 V2/V1 compatability  
04/01/15   aab     Added change to Read the SSC CX and MX voltages based on fuse and program in the MISC register for pin ctrl
04/01/15   aab     Added pm_device_programmer_init()
01/29/15   aab     Added pm_sbl_set_ext_buck_to_pwm_mode() to set external Buck to PWM mode
10/10/14   aab     Branch from 8994 target
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_sbl_boot_target.h"

static pm_err_flag_type pm_program_ssc_retention_uv(void);
static pm_err_flag_type pm_sbl_pre_config(void);
static pm_err_flag_type pm_sbl_target_detect(void);

static boolean pm_ram_image_loaded_flag = FALSE;

static pm_pbs_ram_data_type
pm_pbs_ram_wr_seq[3] =
{
   //data, offset, base_addr, sid/OpCode
   {0x54, 0x03, 0x01, 0xC0}, // W#0	-	Delay for 26ms
   {0x40, 0x91, 0x08, 0x00}, // W#1	-	Acknowledge warm reset done.
   {0xFF, 0xFF,	0xFF, 0xFC}, // W#2	-	EOS
};


/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

static uint32 read_ssc_mx_fuse_setting(void)
{
    unsigned offset = 2;
    /* 0x0x00070134[31:29] */
    return (HWIO_INM(QFPROM_RAW_PTE_ROW3_LSB, (0x7 << offset)) >> offset);
}

static uint32 read_ssc_cx_fuse_setting(void)
{
    unsigned offset = 29;
    /* 0x00070148[4:2] */
    return (HWIO_INM(QFPROM_RAW_PTE_ROW0_MSB, (0x7 << offset)) >> offset);
}


/*===========================================================================

                        FUNCTION DEFINITIONS 

===========================================================================*/
pm_err_flag_type 
pm_device_init ( void )
{
    static pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    err_flag |= pm_device_setup();
    pm_target_information_init();
    pm_comm_info_init();

    err_flag |= pm_device_pre_init(); 

    err_flag |= pm_pon_init();      

    err_flag |= pm_pbs_info_rom_init();  /* Read PBS INFO for the pmic rom devices */

    err_flag |= pm_sbl_pre_config(); /* SBL Pre Configuration */       

    err_flag |= pm_sbl_config(); /* SBL Configuration */
    if (err_flag == PM_ERR_FLAG__SUCCESS)
    {
       pm_ram_image_loaded_flag = TRUE;
    }     

    err_flag |= pm_sbl_config_test(); /* SBL Configuration validation, only executes complete code if spare reg 0x88F bit 0 is set*/        

    err_flag |= pm_pbs_info_ram_init();  /* Read PBS INFO for the pmic ram devices */

    err_flag |= pm_pbs_ram_version_validation_test(); /* PBS RAM Version validation, only executes complete code if spare reg 0x88F bit 0 is set*/        
    
    err_flag |= pm_device_post_init(); /* Initialize PMIC with the ones PDM can not perform */

    err_flag |= pm_program_ssc_retention_uv(); /* set SSC retention voltages*/

    return err_flag; /* NON ZERO return means an ERROR */
}




pm_err_flag_type
pm_sbl_chg_init (void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  uint32 device_index = 1; //PMI8994
  DalPlatformInfoPlatformType platform_type = DALPLATFORMINFO_TYPE_UNKNOWN;
  pm_model_type pmic_model = PMIC_IS_UNKNOWN;

  err_flag |= pm_sbl_chg_pre_init();

  err_flag |= pm_sbl_config_chg_parameters(device_index);
   
  //Configure FG parameters to SRAM
  err_flag |= pm_sbl_config_fg_sram(device_index);

  platform_type = DalPlatformInfo_Platform();
  pmic_model = pm_get_pmic_model(device_index);

  if( ((platform_type == DALPLATFORMINFO_TYPE_MTP)
    || (platform_type == DALPLATFORMINFO_TYPE_FLUID)
    || (platform_type == DALPLATFORMINFO_TYPE_LIQUID)
    || (platform_type == DALPLATFORMINFO_TYPE_QRD)) &&
       ( (pmic_model  == PMIC_IS_PMI8994) || (pmic_model == PMIC_IS_PMI8996)) )
    {
        err_flag |= pm_sbl_chg_check_weak_battery_status(device_index);
    }
  

  err_flag |= pm_sbl_chg_post_init();  

   return err_flag;
}




pm_err_flag_type
pm_device_programmer_init (void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  
  err_flag |= pm_device_setup();
  pm_target_information_init();
  pm_comm_info_init();

  err_flag |= pm_pon_init();
    
  /* Enable SMPS-12A to PWM mode for DDR PX-1 on V2*/
  err_flag |= pm_comm_write_byte(1, 0x3545, 0x80, 1);


  return err_flag;
}




static pm_err_flag_type
pm_sbl_pre_config(void)
        {
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_sbl_specific_data_type *sbl_param_ptr = NULL;

  sbl_param_ptr = (pm_sbl_specific_data_type*)pm_target_information_get_specific_info(PM_PROP_SBL_SPECIFIC_DATA);
  CORE_VERIFY_PTR(sbl_param_ptr);

  err_flag |= pm_log_pon_reasons(sbl_param_ptr->verbose_uart_logging);

  err_flag |= pm_sbl_target_detect();

    return err_flag;
}




pm_err_flag_type
pm_sbl_target_detect(void) /* SBL Target detect */
{
    static pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS; 

    boolean v3_target_detected = FALSE;
    uint8 pmic_device_index_pmk8001 = 3;
    uint8 pmic_device_index_pm8004 = 2;

    pm_pbs_info_type pbs_info_ptr;
    uint8 pmic_device_index = 0;
    uint8 pbs_rom_rev	    = 0x00;
    uint8 pbs_rom_branch    = 0x00;

    err_flag |= pm_get_pbs_info(pmic_device_index, &pbs_info_ptr); //Check if target is V3 : Read PM8994 PBS ROM version
    pbs_rom_rev    = (uint8)((pbs_info_ptr.rom_version & 0xFF00)>> 8);
    pbs_rom_branch = (uint8)(pbs_info_ptr.rom_version &  0xFF);

    //Configure Clocks (Target Variant with/without PMK)
    if (pm_get_pmic_model(pmic_device_index_pmk8001) == PMIC_IS_PMK8001) //Detect if PMK8001 exists
    { 
      //PMK is detected
      err_flag |= pm_comm_write_byte(0, PM_CLOCK_SOURCE_DETECT_SPARE_REG, 0x01, 1);
    } 
    else
    {
       //NO PMK is detected
       err_flag |= pm_comm_write_byte(0, PM_CLOCK_SOURCE_DETECT_SPARE_REG, 0x00, 1);
    }


    //Check for MSM Rev V3:   target has PBS ROM :  Rev >= 4 and branch >= 6
    if( (pbs_rom_rev >= 0x04) && (pbs_rom_branch >= 0x06) )
    {
       v3_target_detected = TRUE;  
    }


    if( (v3_target_detected == TRUE) && (pm_get_pmic_model(pmic_device_index_pm8004) == PMIC_IS_PM8004))
    {
       // V4 target detected   
       err_flag |= pm_comm_write_byte(0, PM_POWER_GRID_VARIANT_DETECT_SPARE_REG, 0x04, 1);
    }
    else if( v3_target_detected == TRUE)
    {
       //V3 Target Detected
       err_flag |= pm_comm_write_byte(0, PM_POWER_GRID_VARIANT_DETECT_SPARE_REG, 0x03, 1);
    }
    else
    {  
        // V2 target detected
        err_flag |= pm_comm_write_byte(0, PM_POWER_GRID_VARIANT_DETECT_SPARE_REG, 0x02, 1);
    }

    return err_flag;
}





pm_err_flag_type 
pm_program_ssc_retention_uv(void)
{
    uint8 ssc_rail_retention_index = 0;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    ssc_rail_retention_index = read_ssc_cx_fuse_setting();
	CORE_VERIFY(ssc_rail_retention_index < 8);
    err_flag |= pm_comm_write_byte( MISC_SLAVE_ADDR,MISC_NSLDO_L26_RET_ADDR,  
                     ssc_vddcx_pvs_retention_data[ssc_rail_retention_index] , 0); //SSC CX retention
    
    ssc_rail_retention_index = read_ssc_mx_fuse_setting();
	CORE_VERIFY(ssc_rail_retention_index < 8);
    err_flag |= pm_comm_write_byte( MISC_SLAVE_ADDR,MISC_NSLDO_L31_RET_ADDR,  
                     ssc_vddmx_pvs_retention_data[ssc_rail_retention_index] , 0); //SSC MX retention

    return err_flag;
}


static pm_err_flag_type
pm_pbs_ram_image_verify_minimum(uint32 slave_id, pm_pbs_ram_data_type *data, unsigned size, uint16 start_addr)
{
  int i;
  pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
  uint32 read_val, data_val;

  /* Configuring read access in burst mode to PBS RAM */
  err = pm_pbs_config_access(slave_id, start_addr, PM_PBS_ACCESS_READ_BURST);
  if(err != PM_ERR_FLAG__SUCCESS) return err; //Stop on first error

  /* Burst Read PBS sequence and verify */
  for (i = 0; i < size; i++)
  {
    err |= pm_comm_read_byte_array(slave_id, PMIO_PBS_CORE_MEM_INTF_RD_DATA0_ADDR, 4, (uint8*) &read_val, 1);
    if(err != PM_ERR_FLAG__SUCCESS) return err; //Stop on first error
    pm_clk_busy_wait(10);

    data_val = *((uint32 *)&data[i]);
    if (data_val != read_val)
    {
      return PM_ERR_FLAG__DATA_VERIFY_FAILURE;
    }
  }

  return err;
}

static pm_err_flag_type
pm_pbs_ram_image_load_minimum(uint32 slave_id, pm_pbs_ram_data_type *data, unsigned size, uint16 start_addr)
{
  int i;
  pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;

  /* Burst Write PBS sequence */
  for (i = 0; i < size; i++)
  {
    err = pm_comm_write_byte_array(slave_id, PMIO_PBS_CORE_MEM_INTF_WR_DATA0_ADDR, 4, (uint8 *)&data[i], 1);
    if(err != PM_ERR_FLAG__SUCCESS) return err; //Stop on first error
    pm_clk_busy_wait(10);
  }

  //Verify
  err = pm_pbs_ram_image_verify_minimum(slave_id, data, size, start_addr);
  if(err != PM_ERR_FLAG__SUCCESS) return err; //Stop on first error

  return err;
}

pm_err_flag_type 
pm_check_pbs_ram_warm_reset_seq_presence(uint32 device_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint8  slave_id             = device_index * 2;
    uint16 pbs_ram_ver_addr     = 0x07FC; 
    uint16 pbs_ram_temp_wr_addr = 0x07F0;
    uint32 pbs_ram_ver_read     = 0x00;
    uint32 pbs_ram_opcode       = 0x00;

    if ((pm_ram_image_loaded_flag == FALSE) && (device_index == 0))
    {
       //Check to see if PBS RAM is programmed:
       err_flag |= pm_pbs_enable_access(slave_id) ;
       err_flag |= pm_pbs_config_access(slave_id, pbs_ram_ver_addr, PM_PBS_ACCESS_READ_BURST);
       err_flag |= pm_comm_read_byte_array(slave_id, PMIO_PBS_CORE_MEM_INTF_RD_DATA0_ADDR, 4, (uint8*)&pbs_ram_ver_read, 1);
       err_flag |= pm_pbs_disable_access(slave_id);
       pbs_ram_opcode = (pbs_ram_ver_read & 0xFFF00000);

       if (pbs_ram_opcode != 0xF8F00000)
       { //If NO PBS RAM programmed, Program Warm Reset sequence

          err_flag |= pm_pbs_enable_access(slave_id) ;
          err_flag |= pm_pbs_config_access(slave_id, pbs_ram_temp_wr_addr, PM_PBS_ACCESS_WRITE_BURST);
          err_flag |= pm_pbs_ram_image_load_minimum(slave_id, &pm_pbs_ram_wr_seq[0], 3, pbs_ram_temp_wr_addr);
          err_flag |= pm_pbs_disable_access(slave_id);

          //Configure PBS RAM warm reset trigger address at the 
          //end of PBS ram word starting from word # 252
          err_flag |= pm_comm_write_byte(slave_id, 0x7059, 0x07, 1);  
          err_flag |= pm_comm_write_byte(slave_id, 0x7058, 0xF0, 1);
       }
       else
       {
          //Configure PBS RAM warm reset trigger address
          err_flag |= pm_comm_write_byte(slave_id, 0x7059, 0x04, 1);  
          err_flag |= pm_comm_write_byte(slave_id, 0x7058, 0x04, 1);
       }

       //Enable Client2 PBS trigger
       err_flag |= pm_comm_write_byte(slave_id, 0x7346, 0x80, 1);  
    }

    return err_flag;
}

