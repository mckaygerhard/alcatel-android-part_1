#ifndef __PMIC_SBL_BOOT_TARGET_H__
#define __PMIC_SBL_BOOT_TARGET_H__

/*! \file pm_sbl_boot_target.h
*  \n
*  \brief This file contains PMIC device initialization function and globals declaration.
*  \n
*  \n &copy; Copyright 2013-2015 Qualcomm Technologies Inc, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/01/15   aab     Updated driver to support SBL Conditional configuration feature 
10/05/15   aab     Added pm_sbl_get_pon_reset_cfg() and pm_sbl_get_ps_hold_cfg()
09/25/15   aab     Added prototype pm_check_pbs_ram_warm_reset_seq_presence() 
06/23/15   pb      Added pm_config_sbl_test.h needed for Settings validation test
05/21/15   aab     Creation
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "com_dtypes.h"
#include "pm_boot.h"
#include "pm_sbl_boot.h"
#include "pm_fg_sram.h"
#include "pm_config_sbl.h"
#include "pm_config_sbl_test.h"
#include "pm_pbs_info.h"
#include "device_info.h"
#include "pm_target_information.h"
#include "pm_app_smbchg.h"
#include "pm_utils.h"
#include "pm_comm.h"
#include "boot_api.h"
#include "boothw_target.h"
#include "boot_logger.h"
#include "CoreVerify.h"
#include "SpmiCfg.h"
#include "DALDeviceId.h"
#include "DDIPlatformInfo.h"
#include "DDIChipInfo.h"

#include "pm_pon.h"
#include "security_control_core_hwio.h"


/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

#define PM_CLOCK_SOURCE_DETECT_SPARE_REG         0x5953   // CLK_DIST_SPARE3: 0x00005953
#define PM_POWER_GRID_VARIANT_DETECT_SPARE_REG   0x088C   // PON_PERPH_RB_SPARE: 0x0000088C


/*----------------------------------------------------------------------------
 * MODULE: TCSR_TCSR_REGS
 *--------------------------------------------------------------------------*/
#include <HALhwio.h>
#include <msmhwiobase.h>

#define TCSR_TCSR_REGS_REG_BASE                               (CORE_TOP_CSR_BASE      + 0x000A0000)
#define HWIO_TCSR_SOC_HW_VERSION_ADDR                         (TCSR_TCSR_REGS_REG_BASE      + 0x00008000)
#define HWIO_TCSR_SOC_HW_VERSION_OFFS                         (TCSR_TCSR_REGS_REG_BASE_OFFS + 0x00008000)
#define HWIO_TCSR_SOC_HW_VERSION_RMSK                         0xffffffff

#define HWIO_TCSR_SOC_HW_VERSION_MAJOR_VERSION_BMSK           0xff00
#define HWIO_TCSR_SOC_HW_VERSION_MAJOR_VERSION_SHFT                                                                              0x8
#define HWIO_TCSR_SOC_HW_VERSION_MINOR_VERSION_BMSK                                                                             0xff
#define HWIO_TCSR_SOC_HW_VERSION_MINOR_VERSION_SHFT                                                                              0x0

#define HWIO_TCSR_SOC_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_TCSR_SOC_HW_VERSION_ADDR, m)


/*----------------------------------------------------------------------------
 * MODULE: TLMM_CSR
 *--------------------------------------------------------------------------*/
#define TLMM_CSR_REG_BASE                                     (TLMM_BASE      + 0x00010000)
#define TLMM_CSR_REG_BASE_OFFS                                0x00010000


#define HWIO_TLMM_GPIO_CFGn_ADDR(n)                                        (TLMM_CSR_REG_BASE      + 0x00000000 + 0x1000 * (n))
#define HWIO_TLMM_GPIO_CFGn_OFFS(n)                                        (TLMM_CSR_REG_BASE_OFFS + 0x00000000 + 0x1000 * (n))
#define HWIO_TLMM_GPIO_CFGn_RMSK                                                0x7ff
#define HWIO_TLMM_GPIO_CFGn_MAXn                                                  149

#define HWIO_TLMM_GPIO_IN_OUTn_ADDR(n)                                     (TLMM_CSR_REG_BASE      + 0x00000004 + 0x1000 * (n))
#define HWIO_TLMM_GPIO_IN_OUTn_OFFS(n)                                     (TLMM_CSR_REG_BASE_OFFS + 0x00000004 + 0x1000 * (n))
#define HWIO_TLMM_GPIO_IN_OUTn_RMSK                                               0x3
#define HWIO_TLMM_GPIO_IN_OUTn_MAXn                                               149

#define HWIO_TLMM_GPIO_IN_OUTn_GPIO_OUT_BMSK                                      0x2
#define HWIO_TLMM_GPIO_IN_OUTn_GPIO_OUT_SHFT                                      0x1

#define HWIO_TLMM_GPIO_CFGn_OUTI(n,val)    \
        out_dword(HWIO_TLMM_GPIO_CFGn_ADDR(n),val)

#define HWIO_TLMM_GPIO_IN_OUTn_INI(n)        \
        in_dword_masked(HWIO_TLMM_GPIO_IN_OUTn_ADDR(n), HWIO_TLMM_GPIO_IN_OUTn_RMSK)
#define HWIO_TLMM_GPIO_IN_OUTn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TLMM_GPIO_IN_OUTn_ADDR(n),mask,val,HWIO_TLMM_GPIO_IN_OUTn_INI(n))

#define MISC_SLAVE_ADDR                              0x0
#define MISC_BASE_ADDRESS                            0x900
#define MISC_NSLDO_L26_RET_ADDR                     (MISC_BASE_ADDRESS + 0x50)
#define MISC_NSLDO_L26_NOM_ADDR                     (MISC_BASE_ADDRESS + 0x51) 
#define MISC_NSLDO_L31_RET_ADDR                     (MISC_BASE_ADDRESS + 0x52)
#define MISC_NSLDO_L31_NOM_ADDR                     (MISC_BASE_ADDRESS + 0x53)
#define MISC_NSLDO11_ADDR                           (MISC_BASE_ADDRESS + 0x54)

// retention programmed in uV ( 600000uV = 0.6V )
static const uint8 ssc_vddcx_pvs_retention_data[8] =
{
  /* 000  600000*/  0x12 , /* TODO based on fuses */
  /* 001  550000*/  0xE ,
  /* 010  500000*/  0xA ,
  /* 011  450000*/  0x6 ,
  /* 100  400000*/  0x2 ,
  /* 101  400000*/  0x2 ,
  /* 110  400000*/  0x2 ,
  /* 111  600000*/  0x12 
};


// retention programmed in uV ( 700000uV = 0.7V )
static const uint8 ssc_vddmx_pvs_retention_data[8] =
{
  /* 000 700000*/ 0x1A,
  /* 001 650000*/ 0x16,
  /* 010 587500*/ 0x11,
  /* 011 550000*/ 0x0E,
  /* 100 487500*/ 0x09,
  /* 101 487500*/ 0x09,
  /* 110 487500*/ 0x09,
  /* 111 487500*/ 0x09 
}; 


pm_err_flag_type 
pm_check_pbs_ram_warm_reset_seq_presence(uint32 device_index);



#endif //__PMIC_SBL_BOOT_TARGET_H__
