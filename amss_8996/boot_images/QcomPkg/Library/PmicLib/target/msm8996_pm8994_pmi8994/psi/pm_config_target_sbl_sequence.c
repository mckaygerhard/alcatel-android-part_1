/*! \file  pm_config_target_sbl_sequence.c
 *  
 *  \brief  File Contains the PMIC Set Mode Driver Implementation
 *  \details Set Mode Driver implementation is responsible for setting and getting 
 *  all mode settings such as Register values, memory values, etc.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Locked Version: MSM8996_COMMERCIAL_1p0-05272016v1 - Approved
 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2016 Qualcomm Technologies, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.xf/1.0/QcomPkg/Library/PmicLib/target/msm8996_pm8994_pmi8994/psi/pm_config_target_sbl_sequence.c#35 $ 
$DateTime: 2016/08/04 23:14:09 $  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "pm_target_information.h"
#include "pm_config_sbl.h"

/*========================== SBL Sequence LUT =============================*/

//NOTES ON CREATING SBL SEQUENCE:

//1. When creating SBL sequence(Table A), if configuration do not need conditional check, set cond start/end index to 0
//2. If Reg configuration require conditional check, indicate the range of conditional check using cond start/end index
//3. For Reg operation PM_SBL_DELAY case, address field contains delay amount in us
//4. For Reg Operation PM_SBL_PBS_RAM case, data field contains index to access in PBS RAM Table
//5. For Reg Operation PM_SBL_PBS_RAM case, address field contains size of PBS RAM

//Table B (Conditional configuration check Table):
//1. DO NOT use the first entry. Index 0 is used when no conditional configuration is needed (This is just to avoid confusion)
//2. Single or multiple configuration can be added in Table B
//3. If multiple configuration is needed, it should be entered in a consecutive order, so that it can be indexed in one range(from Table A)

pm_sbl_config_info_type
pm_sbl_config_info = 
{
   PSI_SIGNATURE,  // PSI_SIGNATURE
   0x01,           // PSI Major Version
   0x01,           // PSI Minor Version
   0x01,           // Number of target configurations
   0xE6            // Total number of conditional entries on pm_sbl_cond_seq_type table (Example: 0x11=17)
};


pm_sbl_cond_seq_type
pm_sbl_cond_seq [ ] =
{
   //sid  data  register  operator        mask
   // PBS_CONFIG Mode 3  SPMI Seq 16  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 1
   
   // PBS_CONFIG Mode 3  SPMI Seq 17  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 2
   
   // PBS_CONFIG Mode 3  SPMI Seq 22  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 3
   
   // PBS_CONFIG Mode 3  SPMI Seq 23  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 4
   
   // PBS_CONFIG Mode 3  SPMI Seq 24  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 5
   
   // PBS_CONFIG Mode 3  SPMI Seq 6  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 6
   
   // PBS_CONFIG Mode 3  SPMI Seq 7  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 7
   
   // CLOCKS Mode 6  SPMI Seq 2  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 8
   
   // CLOCKS Mode 6  SPMI Seq 3  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 9
   
   // CLOCKS Mode 6  SPMI Seq 4  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 10
   
   // CLOCKS Mode 6  SPMI Seq 5  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 11
   
   // CLOCKS Mode 6  SPMI Seq 6  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 12
   
   // CLOCKS Mode 6  SPMI Seq 7  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 13
   
   // CLOCKS Mode 6  SPMI Seq 8  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 14
   
   // CLOCKS Mode 6  SPMI Seq 9  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 15
   
   // CLOCKS Mode 6  SPMI Seq 10  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 16
   
   // CLOCKS Mode 6  SPMI Seq 11  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 17
   
   // CLOCKS Mode 6  SPMI Seq 12  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 18
   
   // CLOCKS Mode 6  SPMI Seq 13  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 19
   
   // CLOCKS Mode 6  SPMI Seq 14  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 20
   
   // CLOCKS Mode 6  SPMI Seq 15  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 21
   
   // CLOCKS Mode 6  SPMI Seq 16  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 22
   
   // CLOCKS Mode 6  SPMI Seq 17  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 23
   
   // CLOCKS Mode 6  SPMI Seq 18  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 24
   
   // CLOCKS Mode 6  SPMI Seq 19  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 25
   
   // CLOCKS Mode 6  SPMI Seq 20  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 26
   
   // CLOCKS Mode 6  SPMI Seq 21  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 27
   
   // CLOCKS Mode 6  SPMI Seq 22  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 28
   
   // CLOCKS Mode 6  SPMI Seq 23  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 29
   
   // CLOCKS Mode 6  SPMI Seq 24  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 30
   
   // CLOCKS Mode 6  SPMI Seq 25  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 31
   
   // CLOCKS Mode 6  SPMI Seq 26  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 32
   
   // CLOCKS Mode 6  SPMI Seq 29  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 33
   
   // CLOCKS Mode 6  SPMI Seq 30  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 34
   
   // CLOCKS Mode 6  SPMI Seq 31  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 35
   
   // CLOCKS Mode 6  SPMI Seq 32  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 36
   
   // CLOCKS Mode 6  SPMI Seq 33  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 37
   
   // CLOCKS Mode 6  SPMI Seq 34  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 38
   
   // CLOCKS Mode 6  SPMI Seq 35  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 39
   
   // CLOCKS Mode 6  SPMI Seq 36  Contains Conditions for SPMI Sequence
   { 0,	0x00,	0x5953,	EQUAL,	0xFF},	// 40
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 1  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 41
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 2  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 42
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 3  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 43
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 4  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 44
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 6  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 45
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 7  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 46
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 8  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 47
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 9  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 48
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 10  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 49
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 11  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 50
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 12  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 51
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 13  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 52
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 14  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 53
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 15  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 54
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 16  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 55
   
   // PBS_CONFIG_PMK Mode 7  SPMI Seq 5  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 56
   
   // CLOCKS_PMK Mode 8  SPMI Seq 1  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 57
   
   // CLOCKS_PMK Mode 8  SPMI Seq 2  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 58
   
   // CLOCKS_PMK Mode 8  SPMI Seq 3  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 59
   
   // CLOCKS_PMK Mode 8  SPMI Seq 4  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 60
   
   // CLOCKS_PMK Mode 8  SPMI Seq 5  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 61
   
   // CLOCKS_PMK Mode 8  SPMI Seq 6  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 62
   
   // CLOCKS_PMK Mode 8  SPMI Seq 7  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 63
   
   // CLOCKS_PMK Mode 8  SPMI Seq 8  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 64
   
   // CLOCKS_PMK Mode 8  SPMI Seq 9  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 65
   
   // CLOCKS_PMK Mode 8  SPMI Seq 11  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 66
   
   // CLOCKS_PMK Mode 8  SPMI Seq 12  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 67
   
   // CLOCKS_PMK Mode 8  SPMI Seq 13  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 68
   
   // CLOCKS_PMK Mode 8  SPMI Seq 14  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 69
   
   // CLOCKS_PMK Mode 8  SPMI Seq 15  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 70
   
   // CLOCKS_PMK Mode 8  SPMI Seq 16  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 71
   
   // CLOCKS_PMK Mode 8  SPMI Seq 17  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 72
   
   // CLOCKS_PMK Mode 8  SPMI Seq 18  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 73
   
   // CLOCKS_PMK Mode 8  SPMI Seq 19  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 74
   
   // CLOCKS_PMK Mode 8  SPMI Seq 20  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 75
   
   // CLOCKS_PMK Mode 8  SPMI Seq 21  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 76
   
   // CLOCKS_PMK Mode 8  SPMI Seq 22  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 77
   
   // CLOCKS_PMK Mode 8  SPMI Seq 23  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 78
   
   // CLOCKS_PMK Mode 8  SPMI Seq 24  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 79
   
   // CLOCKS_PMK Mode 8  SPMI Seq 25  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 80
   
   // CLOCKS_PMK Mode 8  SPMI Seq 26  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 81
   
   // CLOCKS_PMK Mode 8  SPMI Seq 27  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 82
   
   // CLOCKS_PMK Mode 8  SPMI Seq 28  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 83
   
   // CLOCKS_PMK Mode 8  SPMI Seq 29  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 84
   
   // CLOCKS_PMK Mode 8  SPMI Seq 30  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 85
   
   // CLOCKS_PMK Mode 8  SPMI Seq 31  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 86
   
   // CLOCKS_PMK Mode 8  SPMI Seq 32  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 87
   
   // CLOCKS_PMK Mode 8  SPMI Seq 33  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 88
   
   // CLOCKS_PMK Mode 8  SPMI Seq 34  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 89
   
   // CLOCKS_PMK Mode 8  SPMI Seq 35  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 90
   
   // CLOCKS_PMK Mode 8  SPMI Seq 36  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 91
   
   // CLOCKS_PMK Mode 8  SPMI Seq 37  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 92
   
   // CLOCKS_PMK Mode 8  SPMI Seq 38  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 93
   
   // CLOCKS_PMK Mode 8  SPMI Seq 39  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 94
   
   // PON_INIT_PMK Mode 9  SPMI Seq 1  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 95
   
   // PON_INIT_PMK Mode 9  SPMI Seq 2  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 96
   
   // PON_INIT_PMK Mode 9  SPMI Seq 3  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 97
   
   // PON_INIT_PMK Mode 9  SPMI Seq 4  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 98
   
   // PON_INIT_PMK Mode 9  SPMI Seq 5  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 99
   
   // PON_INIT_PMK Mode 9  SPMI Seq 6  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 100
   
   // PON_INIT_PMK Mode 9  SPMI Seq 7  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 101
   
   // PON_INIT_PMK Mode 9  SPMI Seq 8  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 102
   
   // PON_INIT_PMK Mode 9  SPMI Seq 9  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 103
   
   // GPIO_CONFIG_PMK Mode 10  SPMI Seq 1  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 104
   
   // GPIO_CONFIG_PMK Mode 10  SPMI Seq 2  Contains Conditions for SPMI Sequence
   { 0,	0x01,	0x5953,	EQUAL,	0xFF},	// 105
   
   // FTS_1_6_CONFIG Mode 15  SPMI Seq 69  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 106
   
   // FTS_1_6_CONFIG Mode 15  SPMI Seq 70  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 107
   
   // FTS_1_6_CONFIG Mode 15  SPMI Seq 71  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 108
   
   // FTS_12a_CONFIG Mode 17  SPMI Seq 54  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 109
   
   // FTS_12a_CONFIG Mode 17  SPMI Seq 55  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 110
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 2  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 111
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 3  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 112
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 4  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 113
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 5  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 114
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 6  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 115
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 7  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 116
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 8  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 117
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 9  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 118
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 10  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 119
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 11  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 120
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 12  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 121
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 13  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 122
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 14  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 123
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 15  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 124
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 16  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 125
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 17  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 126
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 18  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 127
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 19  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 128
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 20  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 129
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 21  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 130
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 22  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 131
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 23  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 132
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 24  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 133
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 25  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 134
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 26  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 135
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 27  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 136
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 28  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 137
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 29  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 138
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 30  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 139
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 31  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 140
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 32  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 141
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 33  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 142
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 34  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 143
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 35  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 144
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 36  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 145
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 37  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 146
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 38  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 147
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 39  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 148
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 40  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 149
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 41  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 150
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 42  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 151
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 43  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 152
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 44  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 153
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 45  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 154
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 46  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 155
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 47  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 156
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 48  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 157
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 49  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 158
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 50  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 159
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 51  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 160
   
   // FTS_8_CONFIG Mode 18  SPMI Seq 52  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 161
   
   // S9_FTS_LUT Mode 20  SPMI Seq 2  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 162
   
   // S9_FTS_LUT Mode 20  SPMI Seq 3  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 163
   
   // S9_FTS_LUT Mode 20  SPMI Seq 4  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 164
   
   // S9_FTS_LUT Mode 20  SPMI Seq 5  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 165
   
   // S9_FTS_LUT Mode 20  SPMI Seq 6  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 166
   
   // S9_FTS_LUT Mode 20  SPMI Seq 7  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 167
   
   // S9_FTS_LUT Mode 20  SPMI Seq 9  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 168
   
   // S9_FTS_LUT Mode 20  SPMI Seq 10  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 169
   
   // S9_FTS_LUT Mode 20  SPMI Seq 11  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 170
   
   // S9_FTS_LUT Mode 20  SPMI Seq 12  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 171
   
   // S9_FTS_LUT Mode 20  SPMI Seq 15  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 172
   
   // S9_FTS_LUT Mode 20  SPMI Seq 16  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 173
   
   // S9_FTS_LUT Mode 20  SPMI Seq 17  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 174
   
   // S9_FTS_LUT Mode 20  SPMI Seq 18  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 175
   
   // S10_FTS_LUT Mode 21  SPMI Seq 3  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 176
   
   // S10_FTS_LUT Mode 21  SPMI Seq 5  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 177
   
   // S10_FTS_LUT Mode 21  SPMI Seq 6  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	EQUAL,	0xFF},	// 178
   
   // S10_FTS_LUT Mode 21  SPMI Seq 7  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 179
   
   // S10_FTS_LUT Mode 21  SPMI Seq 9  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 180
   
   // S10_FTS_LUT Mode 21  SPMI Seq 10  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 181
   
   // S10_FTS_LUT Mode 21  SPMI Seq 11  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 182
   
   // S10_FTS_LUT Mode 21  SPMI Seq 12  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	EQUAL,	0xFF},	// 183
   
   // S10_FTS_LUT Mode 21  SPMI Seq 13  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 184
   
   // S10_FTS_LUT Mode 21  SPMI Seq 16  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 185
   
   // S10_FTS_LUT Mode 21  SPMI Seq 17  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	EQUAL,	0xFF},	// 186
   
   // S10_FTS_LUT Mode 21  SPMI Seq 18  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 187
   
   // S10_FTS_LUT Mode 21  SPMI Seq 19  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 188
   
   // S10_FTS_LUT Mode 21  SPMI Seq 20  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	EQUAL,	0xFF},	// 189
   
   // S10_FTS_LUT Mode 21  SPMI Seq 21  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 190
   
   // S10_FTS_LUT Mode 21  SPMI Seq 22  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 191
   
   // S10_FTS_LUT Mode 21  SPMI Seq 23  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	EQUAL,	0xFF},	// 192
   
   // S10_FTS_LUT Mode 21  SPMI Seq 24  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 193
   
   // FTS_11_10_9 Mode 22  SPMI Seq 27  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 194
   
   // FTS_11_10_9 Mode 22  SPMI Seq 29  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 195
   
   // FTS_11_10_9 Mode 22  SPMI Seq 31  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 196
   
   // FTS_11_10_9 Mode 22  SPMI Seq 33  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 197
   
   // FTS_11_10_9 Mode 22  SPMI Seq 47  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 198
   
   // FTS_11_10_9 Mode 22  SPMI Seq 49  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 199
   
   // FTS_11_10_9 Mode 22  SPMI Seq 50  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 200
   
   // FTS_11_10_9 Mode 22  SPMI Seq 51  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 201
   
   // FTS_11_10_9 Mode 22  SPMI Seq 68  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 202
   
   // FTS_11_10_9 Mode 22  SPMI Seq 70  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 203
   
   // FTS_11_10_9 Mode 22  SPMI Seq 75  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 204
   
   // FTS_11_10_9 Mode 22  SPMI Seq 87  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 205
   
   // FTS_11_10_9 Mode 22  SPMI Seq 88  Contains Conditions for SPMI Sequence
   { 0,	0x03,	0x088C,	GREATER_OR_EQUAL,	0xFF},	// 206
   
   // FTS_11_10_9 Mode 22  SPMI Seq 100  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 207
   
   // FTS_11_10_9 Mode 22  SPMI Seq 102  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 208
   
   // FTS_11_10_9 Mode 22  SPMI Seq 113  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 209
   
   // FTS_11_10_9 Mode 22  SPMI Seq 114  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 210
   
   // FTS_11_10_9 Mode 22  SPMI Seq 115  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 211
   
   // BUA_OTP Mode 28  SPMI Seq 2  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 212
   
   // BUA_OTP Mode 28  SPMI Seq 3  Contains Conditions for SPMI Sequence
   { 0,	0x02,	0x088C,	EQUAL,	0xFF},	// 213
   
   // SMBCHG Mode 32  SPMI Seq 4  Contains Conditions for SPMI Sequence
   { 2,	0x0a,	0x0105,	EQUAL,	0xFF},	// 214
   
   // SMBCHG Mode 32  SPMI Seq 9  Contains Conditions for SPMI Sequence
   { 2,	0x13,	0x0105,	EQUAL,	0xFF},	// 215
   
   // SMBCHG Mode 32  SPMI Seq 10  Contains Conditions for SPMI Sequence
   { 2,	0x13,	0x0105,	EQUAL,	0xFF},	// 216
   
   // PBS_CONFIG_PMI Mode 33  SPMI Seq 19  Contains Conditions for SPMI Sequence
   { 2,	0x0a,	0x0105,	EQUAL,	0xFF},	// 217
   
   // PBS_CONFIG_PMI Mode 33  SPMI Seq 20  Contains Conditions for SPMI Sequence
   { 2,	0x0a,	0x0105,	EQUAL,	0xFF},	// 218
   
   // PBS_CONFIG_PMI Mode 33  SPMI Seq 21  Contains Conditions for SPMI Sequence
   { 2,	0x0a,	0x0105,	EQUAL,	0xFF},	// 219
   
   // PBS_CONFIG_PMI Mode 33  SPMI Seq 29  Contains Conditions for SPMI Sequence
   { 2,	0x0a,	0x0105,	EQUAL,	0xFF},	// 220
   
   // PBS_CONFIG_PMI Mode 33  SPMI Seq 30  Contains Conditions for SPMI Sequence
   { 2,	0x0a,	0x0105,	EQUAL,	0xFF},	// 221
   
   // PBS_CONFIG_PMI Mode 33  SPMI Seq 31  Contains Conditions for SPMI Sequence
   { 2,	0x0a,	0x0105,	EQUAL,	0xFF},	// 222
   
   // PBS_CONFIG_PMI Mode 33  SPMI Seq 32  Contains Conditions for SPMI Sequence
   { 2,	0x0a,	0x0105,	EQUAL,	0xFF},	// 223
   
   // PBS_CONFIG_PMI Mode 33  SPMI Seq 7  Contains Conditions for SPMI Sequence
   { 2,	0x0a,	0x0105,	EQUAL,	0xFF},	// 224
   
   // PBS_CONFIG_PMI Mode 33  SPMI Seq 8  Contains Conditions for SPMI Sequence
   { 2,	0x13,	0x0105,	EQUAL,	0xFF},	// 225
   
   // FTS_2B_3B_CONFIG Mode 38  SPMI Seq 67  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 226
   
   // FTS_2B_3B_CONFIG Mode 38  SPMI Seq 68  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 227
   
   // FTS_2C_4C CONFIG Mode 52  SPMI Seq 58  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 228
   
   // FTS_2C_4C CONFIG Mode 52  SPMI Seq 59  Contains Conditions for SPMI Sequence
   { 0,	0x04,	0x088C,	EQUAL,	0xFF},	// 229
   
   // PMi8996_SBL_ID Mode 56  SPMI Seq 2  Contains Conditions for SPMI Sequence
   { 2,	0x0a,	0x0105,	EQUAL,	0xFF},	// 230
   
};



pm_sbl_seq_type
pm_sbl_seq [ ] =
{
   
   // MODE - WATCHDOG_EN: 1
   //sid  data    register  reg op      cond start    cond end
   { 0,		0x01,	0x0858,	PM_SBL_WRITE,	0,		0},	// 1	   
   { 0,		0x00,	0x0857,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 0,		0x99,	0x01F4,	PM_SBL_DELAY,	0,		0},	// 3	   Offset: 500ms. Address Offset represent delay time in this case.
   { 0,		0x0F,	0x0854,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 0,		0x03,	0x0855,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 0,		0x08,	0x0856,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 0,		0x80,	0x0857,	PM_SBL_WRITE,	0,		0},	// 8	   
   
   // MODE - PON_INIT_PM8994: 2
   //sid  data    register  reg op      cond start    cond end
   { 0,		0xA5,	0x08D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 0,		0xBC,	0x08F2,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 0,		0x80,	0x2C44,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 0,		0x20,	0x088A,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 0,		0x2C,	0x0882,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 0,		0x40,	0x0947,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 0,		0x00,	0x0850,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 0,		0x01,	0x0851,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 0,		0x99,	0x00A0,	PM_SBL_DELAY,	0,		0},	// 10	   Offset: 160ms. Address Offset represent delay time in this case.
   { 0,		0x80,	0x0853,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 0,		0x99,	0x00A0,	PM_SBL_DELAY,	0,		0},	// 12	   Offset: 160ms. Address Offset represent delay time in this case.
   { 0,		0x00,	0x0945,	PM_SBL_WRITE,	0,		0},	// 13	   
   
   // MODE - PBS_CONFIG: 3
   //sid  data    register  reg op      cond start    cond end
   { 0,		0x80,	0x7040,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 0,		0xC0,	0x7041,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 0,		0x00,	0x7042,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 0,		0x04,	0x7043,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 0,		0,	0x0100,	PM_SBL_PBS_RAM,	6,		6},	// 6	   PBS Version: PBS_RAM_MSM8996.PMIC.HW.PM8994_2p0_1_3_4  PBS RAM Size = 256
   { 0,		1,	0x0100,	PM_SBL_PBS_RAM,	7,		7},	// 7	   PBS Version: PBS_RAM_MSM8996.PMIC.HW.PM8994_2p0_1_4_7  PBS RAM Size = 256
   { 0,		0x00,	0x7040,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 0,		0x00,	0x7041,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 0,		0x00,	0x7042,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 0,		0x04,	0x7043,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 0,		0x04,	0x7059,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 0,		0x04,	0x7058,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 0,		0x80,	0x7346,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 0,		0x04,	0x707D,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 0,		0x10,	0x707C,	PM_SBL_WRITE,	1,		1},	// 16	   
   { 0,		0x0C,	0x707C,	PM_SBL_WRITE,	2,		2},	// 17	   
   { 0,		0x80,	0x7C46,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 0,		0x04,	0x7065,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 0,		0x00,	0x7064,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 0,		0x80,	0x7646,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 0,		0x04,	0x7075,	PM_SBL_WRITE,	3,		3},	// 22	   
   { 0,		0x10,	0x7074,	PM_SBL_WRITE,	4,		4},	// 23	   
   { 0,		0x80,	0x7A46,	PM_SBL_WRITE,	5,		5},	// 24	   
   
   // MODE - RESET: 4
   //sid  data    register  reg op      cond start    cond end
   { 0,		0x00,	0x085B,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 0,		0x00,	0x0843,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 0,		0x00,	0x084B,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 0,		0x00,	0x0863,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 0,		0x00,	0x0867,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 0,		0x99,	0x01F4,	PM_SBL_DELAY,	0,		0},	// 7	   Offset: 500ms. Address Offset represent delay time in this case.
   { 0,		0x08,	0x085A,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 0,		0x80,	0x085B,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 0,		0x0E,	0x0840,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 0,		0x07,	0x0841,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 0,		0x08,	0x0842,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 0,		0x80,	0x0843,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 0,		0x0C,	0x0848,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 0,		0x07,	0x0849,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 0,		0x08,	0x084A,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 0,		0x80,	0x084B,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 0,		0x04,	0x0862,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 0,		0x80,	0x0863,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 0,		0x07,	0x0866,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 0,		0x80,	0x0867,	PM_SBL_WRITE,	0,		0},	// 21	   
   
   // MODE - WATCHDOG_DIS: 5
   //sid  data    register  reg op      cond start    cond end
   { 0,		0x01,	0x0858,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 0,		0x00,	0x0857,	PM_SBL_WRITE,	0,		0},	// 3	   
   
   // MODE - CLOCKS: 6
   //sid  data    register  reg op      cond start    cond end
   { 0,		0x80,	0x5945,	PM_SBL_WRITE,	8,		8},	// 2	   
   { 0,		0x01,	0x5246,	PM_SBL_WRITE,	9,		9},	// 3	   
   { 0,		0x20,	0x505C,	PM_SBL_WRITE,	10,		10},	// 4	   
   { 0,		0x10,	0x504C,	PM_SBL_WRITE,	11,		11},	// 5	   
   { 0,		0x00,	0x504D,	PM_SBL_WRITE,	12,		12},	// 6	   
   { 0,		0xA5,	0x59D0,	PM_SBL_WRITE,	13,		13},	// 7	   
   { 0,		0x3C,	0x5940,	PM_SBL_WRITE,	14,		14},	// 8	   
   { 0,		0xA5,	0x59D0,	PM_SBL_WRITE,	15,		15},	// 9	   
   { 0,		0x01,	0x5941,	PM_SBL_WRITE,	16,		16},	// 10	   
   { 0,		0xA5,	0x59D0,	PM_SBL_WRITE,	17,		17},	// 11	   
   { 0,		0x00,	0x5942,	PM_SBL_WRITE,	18,		18},	// 12	   
   { 0,		0xA5,	0x59D0,	PM_SBL_WRITE,	19,		19},	// 13	   
   { 0,		0x00,	0x5943,	PM_SBL_WRITE,	20,		20},	// 14	   
   { 0,		0xA0,	0x5A58,	PM_SBL_WRITE,	21,		21},	// 15	   
   { 0,		0x0F,	0x5A59,	PM_SBL_WRITE,	22,		22},	// 16	   
   { 0,		0x01,	0x5A5A,	PM_SBL_WRITE,	23,		23},	// 17	   
   { 0,		0x80,	0x5A5B,	PM_SBL_WRITE,	24,		24},	// 18	   
   { 0,		0xFF,	0x5A5C,	PM_SBL_WRITE,	25,		25},	// 19	   
   { 0,		0x01,	0x2845,	PM_SBL_WRITE,	26,		26},	// 20	   
   { 0,		0x80,	0x2846,	PM_SBL_WRITE,	27,		27},	// 21	   
   { 0,		0x45,	0x6044,	PM_SBL_WRITE,	28,		28},	// 22	   
   { 0,		0x80,	0x6046,	PM_SBL_WRITE,	29,		29},	// 23	   
   { 0,		0x80,	0x5A46,	PM_SBL_WRITE,	30,		30},	// 24	   
   { 0,		0x80,	0x5A48,	PM_SBL_WRITE,	31,		31},	// 25	   
   { 0,		0xFA,	0x505E,	PM_SBL_WRITE,	32,		32},	// 26	   
   { 0,		0x02,	0x5B43,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 0,		0xA5,	0x50D0,	PM_SBL_WRITE,	33,		33},	// 29	   
   { 0,		0x02,	0x50E2,	PM_SBL_WRITE,	34,		34},	// 30	   
   { 0,		0x00,	0x5951,	PM_SBL_WRITE,	35,		35},	// 31	   
   { 0,		0x00,	0x5952,	PM_SBL_WRITE,	36,		36},	// 32	   
   { 0,		0xA5,	0x51D0,	PM_SBL_WRITE,	37,		37},	// 33	   
   { 0,		0x01,	0x51DA,	PM_SBL_WRITE,	38,		38},	// 34	   
   { 0,		0xA5,	0x51D0,	PM_SBL_WRITE,	39,		39},	// 35	   
   { 0,		0x00,	0x51D9,	PM_SBL_WRITE,	40,		40},	// 36	   
   
   // MODE - PBS_CONFIG_PMK: 7
   //sid  data    register  reg op      cond start    cond end
   { 6,		0x80,	0x7040,	PM_SBL_WRITE,	41,		41},	// 1	   
   { 6,		0xC0,	0x7041,	PM_SBL_WRITE,	42,		42},	// 2	   
   { 6,		0x00,	0x7042,	PM_SBL_WRITE,	43,		43},	// 3	   
   { 6,		0x04,	0x7043,	PM_SBL_WRITE,	44,		44},	// 4	   
   { 6,		2,	0x0020,	PM_SBL_PBS_RAM,	56,		56},	// 5	   PBS Version: PBS_RAM_MSM8994_2.PMIC.HW.PMK8001_1p0_4_0_3  PBS RAM Size = 32
   { 6,		0x00,	0x7040,	PM_SBL_WRITE,	45,		45},	// 6	   
   { 6,		0x00,	0x7041,	PM_SBL_WRITE,	46,		46},	// 7	   
   { 6,		0x00,	0x7042,	PM_SBL_WRITE,	47,		47},	// 8	   
   { 6,		0x00,	0x7043,	PM_SBL_WRITE,	48,		48},	// 9	   
   { 6,		0x04,	0x7059,	PM_SBL_WRITE,	49,		49},	// 10	   
   { 6,		0x6C,	0x7058,	PM_SBL_WRITE,	50,		50},	// 11	   
   { 6,		0x80,	0x7346,	PM_SBL_WRITE,	51,		51},	// 12	   
   { 6,		0x04,	0x705D,	PM_SBL_WRITE,	52,		52},	// 13	   
   { 6,		0x00,	0x705C,	PM_SBL_WRITE,	53,		53},	// 14	   
   { 6,		0x00,	0x7440,	PM_SBL_WRITE,	54,		54},	// 15	   
   { 6,		0x80,	0x7446,	PM_SBL_WRITE,	55,		55},	// 16	   
   
   // MODE - CLOCKS_PMK: 8
   //sid  data    register  reg op      cond start    cond end
   { 6,		0x01,	0x5846,	PM_SBL_WRITE,	57,		57},	// 1	   
   { 6,		0x80,	0x5A46,	PM_SBL_WRITE,	58,		58},	// 2	   
   { 6,		0x0F,	0x5843,	PM_SBL_WRITE,	59,		59},	// 3	   
   { 6,		0x03,	0x5844,	PM_SBL_WRITE,	60,		60},	// 4	   
   { 6,		0xA5,	0x59D0,	PM_SBL_WRITE,	61,		61},	// 5	   
   { 6,		0x01,	0x5941,	PM_SBL_WRITE,	62,		62},	// 6	   
   { 6,		0x80,	0x5A5B,	PM_SBL_WRITE,	63,		63},	// 7	   
   { 6,		0xA5,	0x59D0,	PM_SBL_WRITE,	64,		64},	// 8	   
   { 6,		0x00,	0x5944,	PM_SBL_WRITE,	65,		65},	// 9	   
   { 6,		0x00,	0x504C,	PM_SBL_WRITE,	66,		66},	// 11	   
   { 6,		0x20,	0x505A,	PM_SBL_WRITE,	67,		67},	// 12	   
   { 6,		0x45,	0x6044,	PM_SBL_WRITE,	68,		68},	// 13	   
   { 6,		0x20,	0x505A,	PM_SBL_WRITE,	69,		69},	// 14	   
   { 6,		0x80,	0x6046,	PM_SBL_WRITE,	70,		70},	// 15	   
   { 6,		0x17,	0x5058,	PM_SBL_WRITE,	71,		71},	// 16	   
   { 6,		0x00,	0x5059,	PM_SBL_WRITE,	72,		72},	// 17	   
   { 0,		0x80,	0x5945,	PM_SBL_WRITE,	73,		73},	// 18	   
   { 0,		0x01,	0x5246,	PM_SBL_WRITE,	74,		74},	// 19	   
   { 0,		0x1C,	0x504C,	PM_SBL_WRITE,	75,		75},	// 20	   
   { 0,		0x00,	0x504D,	PM_SBL_WRITE,	76,		76},	// 21	   
   { 1,		0x80,	0x4648,	PM_SBL_WRITE,	77,		77},	// 22	   
   { 0,		0xA5,	0x59D0,	PM_SBL_WRITE,	78,		78},	// 23	   
   { 0,		0x02,	0xD541,	PM_SBL_WRITE,	79,		79},	// 24	   
   { 0,		0xA5,	0x59D0,	PM_SBL_WRITE,	80,		80},	// 25	   
   { 0,		0x14,	0x5940,	PM_SBL_WRITE,	81,		81},	// 26	   
   { 0,		0xA5,	0x59D0,	PM_SBL_WRITE,	82,		82},	// 27	   
   { 0,		0x00,	0x5941,	PM_SBL_WRITE,	83,		83},	// 28	   
   { 0,		0xA5,	0x59D0,	PM_SBL_WRITE,	84,		84},	// 29	   
   { 0,		0x01,	0x5943,	PM_SBL_WRITE,	85,		85},	// 30	   
   { 0,		0x01,	0x2845,	PM_SBL_WRITE,	86,		86},	// 31	   
   { 0,		0x80,	0x2846,	PM_SBL_WRITE,	87,		87},	// 32	   
   { 0,		0x80,	0x5A46,	PM_SBL_WRITE,	88,		88},	// 33	   
   { 0,		0x80,	0x5A48,	PM_SBL_WRITE,	89,		89},	// 34	   
   { 0,		0x00,	0x505C,	PM_SBL_WRITE,	90,		90},	// 35	   
   { 7,		0xA5,	0x41D0,	PM_SBL_WRITE,	91,		91},	// 36	   
   { 7,		0x0B,	0x41DA,	PM_SBL_WRITE,	92,		92},	// 37	   
   { 6,		0xA5,	0x5AD0,	PM_SBL_WRITE,	93,		93},	// 38	   
   { 6,		0x01,	0x5AD8,	PM_SBL_WRITE,	94,		94},	// 39	   
   
   // MODE - PON_INIT_PMK: 9
   //sid  data    register  reg op      cond start    cond end
   { 6,		0x10,	0x2C44,	PM_SBL_WRITE,	95,		95},	// 1	   
   { 6,		0x7E,	0x0880,	PM_SBL_WRITE,	96,		96},	// 2	   
   { 6,		0x24,	0x088B,	PM_SBL_WRITE,	97,		97},	// 3	   
   { 6,		0xA5,	0x08D0,	PM_SBL_WRITE,	98,		98},	// 4	   
   { 6,		0x06,	0x08F2,	PM_SBL_WRITE,	99,		99},	// 5	   
   { 6,		0x05,	0x085A,	PM_SBL_WRITE,	101,		101},	// 7	   
   { 6,		0xA5,	0x08D0,	PM_SBL_WRITE,	102,		102},	// 8	   
   { 6,		0x06,	0x0875,	PM_SBL_WRITE,	103,		103},	// 9	   
   
   // MODE - GPIO_CONFIG_PMK: 10
   //sid  data    register  reg op      cond start    cond end
   { 6,		0x00,	0xC340,	PM_SBL_WRITE,	104,		104},	// 1	   
   { 6,		0x01,	0xC345,	PM_SBL_WRITE,	105,		105},	// 2	   
   
   // MODE - GPIO_CONFIG: 11
   //sid  data    register  reg op      cond start    cond end
   { 0,		0x02,	0xCA41,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 0,		0x02,	0xCB41,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 0,		0xA5,	0xC8D0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 0,		0x01,	0xC8DA,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 0,		0xA5,	0xC8D0,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 0,		0x00,	0xC8D9,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 0,		0xA5,	0xD0D0,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 0,		0x01,	0xD0DA,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 0,		0xA5,	0xD0D0,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 0,		0x00,	0xD0D9,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 0,		0xA5,	0xD1D0,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 0,		0x01,	0xD1DA,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 0,		0xA5,	0xD1D0,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 0,		0x00,	0xD1D9,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 0,		0xA5,	0xD2D0,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 0,		0x01,	0xD2DA,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 0,		0xA5,	0xD2D0,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 0,		0x00,	0xD2D9,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 0,		0x00,	0xD340,	PM_SBL_WRITE,	0,		0},	// 20	   
   
   // MODE - INTERRUPT: 12
   //sid  data    register  reg op      cond start    cond end
   { 0,		0x00,	0x0546,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 0,		0x80,	0x0546,	PM_SBL_WRITE,	0,		0},	// 3	   
   
   // MODE - LDOs_CONFIG: 13
   //sid  data    register  reg op      cond start    cond end
   { 1,		0xD0,	0x4052,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0xD0,	0x4252,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0xD0,	0x5E52,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0xD0,	0x5952,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0x80,	0x5E48,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0x01,	0x4240,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0x2E,	0x4241,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0xA5,	0x42D0,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0x01,	0x42DA,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0x01,	0x5940,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0x31,	0x5941,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0x03,	0x5140,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0x34,	0x5141,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 1,		0x00,	0x5145,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 1,		0x0C,	0x5146,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 1,		0x82,	0x4261,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 1,		0x82,	0x5961,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 1,		0x82,	0x5E61,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 1,		0x04,	0x5240,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 1,		0x19,	0x5241,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 1,		0x04,	0x5540,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 1,		0x19,	0x5541,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 1,		0x82,	0x4845,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 1,		0x02,	0x4540,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 1,		0x24,	0x4541,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 1,		0x3A,	0x4C41,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 1,		0x3A,	0x5441,	PM_SBL_WRITE,	0,		0},	// 30	   
   
   // MODE - S6_FTS_LUT: 14
   //sid  data    register  reg op      cond start    cond end
   { 1,		0x0C,	0x2480,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x09,	0x2481,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0x06,	0x2482,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0x3F,	0x2483,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0x05,	0x2487,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0x06,	0x2488,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0x3F,	0x2489,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0x40,	0x248C,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0x26,	0x248D,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0x43,	0x248E,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0x00,	0x248F,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0x01,	0x2490,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0x00,	0x2491,	PM_SBL_WRITE,	0,		0},	// 14	   
   
   // MODE - FTS_1_6_CONFIG: 15
   //sid  data    register  reg op      cond start    cond end
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x05,	0x1450,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0xF0,	0x1469,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0x3F,	0x146B,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0xC0,	0x1468,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0x07,	0x14AC,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0x41,	0x14AD,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 1,		0x0F,	0x1651,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 1,		0x80,	0x14A6,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 1,		0x00,	0x14D9,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 1,		0x00,	0x15D9,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 1,		0x00,	0x23D9,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 1,		0x00,	0x24D9,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 1,		0x80,	0x1451,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 1,		0x80,	0x1551,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 1,		0xC0,	0x2351,	PM_SBL_WRITE,	0,		0},	// 30	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 31	   
   { 1,		0xC0,	0x2451,	PM_SBL_WRITE,	0,		0},	// 32	   
   { 1,		0x01,	0x1454,	PM_SBL_WRITE,	0,		0},	// 33	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 34	   
   { 1,		0x12,	0x1464,	PM_SBL_WRITE,	0,		0},	// 35	   
   { 1,		0x82,	0x1461,	PM_SBL_WRITE,	0,		0},	// 36	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 37	   
   { 1,		0x01,	0x14DA,	PM_SBL_WRITE,	0,		0},	// 38	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 39	   
   { 1,		0x01,	0x15DA,	PM_SBL_WRITE,	0,		0},	// 40	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 41	   
   { 1,		0x01,	0x23DA,	PM_SBL_WRITE,	0,		0},	// 42	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 43	   
   { 1,		0x01,	0x24DA,	PM_SBL_WRITE,	0,		0},	// 44	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 45	   
   { 1,		0x08,	0x1471,	PM_SBL_WRITE,	0,		0},	// 46	   
   { 1,		0x04,	0x2344,	PM_SBL_WRITE,	0,		0},	// 47	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 48	   
   { 1,		0x08,	0x1568,	PM_SBL_WRITE,	0,		0},	// 49	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 50	   
   { 1,		0x03,	0x1561,	PM_SBL_WRITE,	0,		0},	// 51	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 52	   
   { 1,		0x08,	0x1563,	PM_SBL_WRITE,	0,		0},	// 53	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 54	   
   { 1,		0x03,	0x146F,	PM_SBL_WRITE,	0,		0},	// 55	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 56	   
   { 1,		0x40,	0x1462,	PM_SBL_WRITE,	0,		0},	// 57	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 58	   
   { 1,		0x1C,	0x1463,	PM_SBL_WRITE,	0,		0},	// 59	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 60	   
   { 1,		0x99,	0x1596,	PM_SBL_WRITE,	0,		0},	// 61	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 62	   
   { 1,		0x93,	0x1597,	PM_SBL_WRITE,	0,		0},	// 63	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 64	   
   { 1,		0x99,	0x2496,	PM_SBL_WRITE,	0,		0},	// 65	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 66	   
   { 1,		0x93,	0x2497,	PM_SBL_WRITE,	0,		0},	// 67	   
   { 1,		0x80,	0x1445,	PM_SBL_WRITE,	0,		0},	// 68	   
   { 1,		0xA5,	0x14D0,	PM_SBL_WRITE,	106,		106},	// 69	   
   { 1,		0xDD,	0x2460,	PM_SBL_WRITE,	107,		107},	// 70	   
   { 1,		0xDD,	0x1560,	PM_SBL_WRITE,	108,		108},	// 71	   
   { 1,		0x00,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 72	   
   
   // MODE - FTS_2a_CONFIG: 16
   //sid  data    register  reg op      cond start    cond end
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x05,	0x1750,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0x1C,	0x1763,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0xBC,	0x1769,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0x4A,	0x176B,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0xC0,	0x1768,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0x80,	0x17A6,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 1,		0x00,	0x17D9,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 1,		0x00,	0x18D9,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 1,		0x00,	0x1751,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 1,		0x00,	0x1851,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 1,		0x00,	0x1754,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 1,		0x9B,	0x1764,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 1,		0x82,	0x1761,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 1,		0x01,	0x17DA,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 1,		0x01,	0x18DA,	PM_SBL_WRITE,	0,		0},	// 30	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 31	   
   { 1,		0x08,	0x1771,	PM_SBL_WRITE,	0,		0},	// 32	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 33	   
   { 1,		0x08,	0x1868,	PM_SBL_WRITE,	0,		0},	// 34	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 35	   
   { 1,		0x03,	0x1861,	PM_SBL_WRITE,	0,		0},	// 36	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 37	   
   { 1,		0x08,	0x1863,	PM_SBL_WRITE,	0,		0},	// 38	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 39	   
   { 1,		0x40,	0x1762,	PM_SBL_WRITE,	0,		0},	// 40	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 41	   
   { 1,		0x0F,	0x176F,	PM_SBL_WRITE,	0,		0},	// 42	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 43	   
   { 1,		0x07,	0x17AC,	PM_SBL_WRITE,	0,		0},	// 44	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 45	   
   { 1,		0x41,	0x17AD,	PM_SBL_WRITE,	0,		0},	// 46	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 47	   
   { 1,		0x99,	0x1896,	PM_SBL_WRITE,	0,		0},	// 48	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 49	   
   { 1,		0x99,	0x1896,	PM_SBL_WRITE,	0,		0},	// 50	   
   { 1,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 51	   
   { 1,		0x93,	0x1897,	PM_SBL_WRITE,	0,		0},	// 52	   
   { 1,		0xBF,	0x1860,	PM_SBL_WRITE,	0,		0},	// 53	   
   { 1,		0x40,	0x1745,	PM_SBL_WRITE,	0,		0},	// 54	   
   { 1,		0x00,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 55	   
   
   // MODE - FTS_12a_CONFIG: 17
   //sid  data    register  reg op      cond start    cond end
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x05,	0x3550,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0x1C,	0x3563,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0xD6,	0x3569,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0xB8,	0x356B,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0x80,	0x35A6,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0x00,	0x35D9,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 1,		0x00,	0x36D9,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 1,		0x9B,	0x3564,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 1,		0x08,	0x3663,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 1,		0x82,	0x3561,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 1,		0x01,	0x35DA,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 1,		0x01,	0x36DA,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 1,		0x01,	0x37DA,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 1,		0x08,	0x3571,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 1,		0x08,	0x3668,	PM_SBL_WRITE,	0,		0},	// 30	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 31	   
   { 1,		0x03,	0x3661,	PM_SBL_WRITE,	0,		0},	// 32	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 33	   
   { 1,		0x08,	0x3668,	PM_SBL_WRITE,	0,		0},	// 34	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 35	   
   { 1,		0x40,	0x3562,	PM_SBL_WRITE,	0,		0},	// 36	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 37	   
   { 1,		0x0F,	0x356F,	PM_SBL_WRITE,	0,		0},	// 38	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 39	   
   { 1,		0x07,	0x35AC,	PM_SBL_WRITE,	0,		0},	// 40	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 41	   
   { 1,		0x41,	0x35AD,	PM_SBL_WRITE,	0,		0},	// 42	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 43	   
   { 1,		0x99,	0x3696,	PM_SBL_WRITE,	0,		0},	// 44	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 45	   
   { 1,		0x93,	0x3697,	PM_SBL_WRITE,	0,		0},	// 46	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 47	   
   { 1,		0x00,	0x3553,	PM_SBL_WRITE,	0,		0},	// 48	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 49	   
   { 1,		0x35,	0x35C0,	PM_SBL_WRITE,	0,		0},	// 50	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 51	   
   { 1,		0x00,	0x3653,	PM_SBL_WRITE,	0,		0},	// 52	   
   { 1,		0x88,	0x3548,	PM_SBL_WRITE,	0,		0},	// 53	   
   { 1,		0xA5,	0x35D0,	PM_SBL_WRITE,	109,		109},	// 54	   
   { 1,		0xBB,	0x3660,	PM_SBL_WRITE,	110,		110},	// 55	   
   { 1,		0x40,	0x3545,	PM_SBL_WRITE,	0,		0},	// 56	   
   { 1,		0x00,	0x35D0,	PM_SBL_WRITE,	0,		0},	// 57	   
   
   // MODE - FTS_8_CONFIG: 18
   //sid  data    register  reg op      cond start    cond end
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	111,		111},	// 2	   
   { 1,		0x05,	0x2950,	PM_SBL_WRITE,	112,		112},	// 3	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	113,		113},	// 4	   
   { 1,		0xF0,	0x2969,	PM_SBL_WRITE,	114,		114},	// 5	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	115,		115},	// 6	   
   { 1,		0x4F,	0x296B,	PM_SBL_WRITE,	116,		116},	// 7	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	117,		117},	// 8	   
   { 1,		0xC0,	0x2968,	PM_SBL_WRITE,	118,		118},	// 9	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	119,		119},	// 10	   
   { 1,		0xE7,	0x29AC,	PM_SBL_WRITE,	120,		120},	// 11	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	121,		121},	// 12	   
   { 1,		0x41,	0x29AD,	PM_SBL_WRITE,	122,		122},	// 13	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	123,		123},	// 14	   
   { 1,		0x80,	0x29A6,	PM_SBL_WRITE,	124,		124},	// 15	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	125,		125},	// 16	   
   { 1,		0x00,	0x29D9,	PM_SBL_WRITE,	126,		126},	// 17	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	127,		127},	// 18	   
   { 1,		0x00,	0x2AD9,	PM_SBL_WRITE,	128,		128},	// 19	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	129,		129},	// 20	   
   { 1,		0x00,	0x2954,	PM_SBL_WRITE,	130,		130},	// 21	   
   { 1,		0x40,	0x2945,	PM_SBL_WRITE,	131,		131},	// 22	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	132,		132},	// 23	   
   { 1,		0x02,	0x2960,	PM_SBL_WRITE,	133,		133},	// 24	   
   { 1,		0x82,	0x2961,	PM_SBL_WRITE,	134,		134},	// 25	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	135,		135},	// 26	   
   { 1,		0x00,	0x2953,	PM_SBL_WRITE,	136,		136},	// 27	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	137,		137},	// 28	   
   { 1,		0x00,	0x2A53,	PM_SBL_WRITE,	138,		138},	// 29	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	139,		139},	// 30	   
   { 1,		0x01,	0x29DA,	PM_SBL_WRITE,	140,		140},	// 31	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	141,		141},	// 32	   
   { 1,		0x01,	0x2ADA,	PM_SBL_WRITE,	142,		142},	// 33	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	143,		143},	// 34	   
   { 1,		0x08,	0x2971,	PM_SBL_WRITE,	144,		144},	// 35	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	145,		145},	// 36	   
   { 1,		0x08,	0x2A68,	PM_SBL_WRITE,	146,		146},	// 37	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	147,		147},	// 38	   
   { 1,		0x03,	0x2A61,	PM_SBL_WRITE,	148,		148},	// 39	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	149,		149},	// 40	   
   { 1,		0x17,	0x296F,	PM_SBL_WRITE,	150,		150},	// 41	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	151,		151},	// 42	   
   { 1,		0x08,	0x2A63,	PM_SBL_WRITE,	152,		152},	// 43	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	153,		153},	// 44	   
   { 1,		0x40,	0x2962,	PM_SBL_WRITE,	154,		154},	// 45	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	155,		155},	// 46	   
   { 1,		0x1C,	0x2963,	PM_SBL_WRITE,	156,		156},	// 47	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	157,		157},	// 48	   
   { 1,		0x9B,	0x2964,	PM_SBL_WRITE,	158,		158},	// 49	   
   { 1,		0x80,	0x2948,	PM_SBL_WRITE,	159,		159},	// 50	   
   { 1,		0xA5,	0x29D0,	PM_SBL_WRITE,	160,		160},	// 51	   
   { 1,		0x77,	0x2A60,	PM_SBL_WRITE,	161,		161},	// 52	   
   { 1,		0x00,	0x29D0,	PM_SBL_WRITE,	0,		0},	// 53	   
   
   // MODE - S8_FTS_LUT: 19
   //sid  data    register  reg op      cond start    cond end
   { 1,		0x0F,	0x2A80,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x0D,	0x2A81,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0x07,	0x2A82,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0x3F,	0x2A83,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0x05,	0x2A87,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0x07,	0x2A88,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0x3F,	0x2A89,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0x40,	0x2A8C,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0x1A,	0x2A8D,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0x43,	0x2A8E,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0x00,	0x2A8F,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0x01,	0x2A90,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0x00,	0x2A91,	PM_SBL_WRITE,	0,		0},	// 14	   
   
   // MODE - S9_FTS_LUT: 20
   //sid  data    register  reg op      cond start    cond end
   { 1,		0x0E,	0x2D80,	PM_SBL_WRITE,	162,		162},	// 2	   
   { 1,		0x11,	0x2D80,	PM_SBL_WRITE,	163,		163},	// 3	   
   { 1,		0x0C,	0x2D81,	PM_SBL_WRITE,	164,		164},	// 4	   
   { 1,		0x10,	0x2D81,	PM_SBL_WRITE,	165,		165},	// 5	   
   { 1,		0x06,	0x2D82,	PM_SBL_WRITE,	166,		166},	// 6	   
   { 1,		0x07,	0x2D82,	PM_SBL_WRITE,	167,		167},	// 7	   
   { 1,		0x3F,	0x2D83,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0x04,	0x2D87,	PM_SBL_WRITE,	168,		168},	// 9	   
   { 1,		0x06,	0x2D87,	PM_SBL_WRITE,	169,		169},	// 10	   
   { 1,		0x06,	0x2D88,	PM_SBL_WRITE,	170,		170},	// 11	   
   { 1,		0x07,	0x2D88,	PM_SBL_WRITE,	171,		171},	// 12	   
   { 1,		0x3F,	0x2D89,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0x40,	0x2D8C,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 1,		0x20,	0x2D8D,	PM_SBL_WRITE,	172,		172},	// 15	   
   { 1,		0x0B,	0x2D8D,	PM_SBL_WRITE,	173,		173},	// 16	   
   { 1,		0x43,	0x2D8E,	PM_SBL_WRITE,	174,		174},	// 17	   
   { 1,		0x49,	0x2D8E,	PM_SBL_WRITE,	175,		175},	// 18	   
   { 1,		0x00,	0x2D8F,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 1,		0x01,	0x2D90,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 1,		0x00,	0x2D91,	PM_SBL_WRITE,	0,		0},	// 21	   
   
   // MODE - S10_FTS_LUT: 21
   //sid  data    register  reg op      cond start    cond end
   { 1,		0x0D,	0x3080,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x0C,	0x3080,	PM_SBL_WRITE,	176,		176},	// 3	   
   { 1,		0x0B,	0x3081,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0x05,	0x3082,	PM_SBL_WRITE,	177,		177},	// 5	   
   { 1,		0x06,	0x3082,	PM_SBL_WRITE,	178,		178},	// 6	   
   { 1,		0x05,	0x3082,	PM_SBL_WRITE,	179,		179},	// 7	   
   { 1,		0x3F,	0x3083,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0x03,	0x3087,	PM_SBL_WRITE,	180,		180},	// 9	   
   { 1,		0x04,	0x3087,	PM_SBL_WRITE,	181,		181},	// 10	   
   { 1,		0x05,	0x3088,	PM_SBL_WRITE,	182,		182},	// 11	   
   { 1,		0x06,	0x3088,	PM_SBL_WRITE,	183,		183},	// 12	   
   { 1,		0x05,	0x3088,	PM_SBL_WRITE,	184,		184},	// 13	   
   { 1,		0x3F,	0x3089,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 1,		0x40,	0x308C,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 1,		0x2B,	0x308D,	PM_SBL_WRITE,	185,		185},	// 16	   
   { 1,		0x20,	0x308D,	PM_SBL_WRITE,	186,		186},	// 17	   
   { 1,		0x10,	0x308D,	PM_SBL_WRITE,	187,		187},	// 18	   
   { 1,		0x43,	0x308E,	PM_SBL_WRITE,	188,		188},	// 19	   
   { 1,		0x42,	0x308E,	PM_SBL_WRITE,	189,		189},	// 20	   
   { 1,		0x46,	0x308E,	PM_SBL_WRITE,	190,		190},	// 21	   
   { 1,		0x00,	0x308F,	PM_SBL_WRITE,	191,		191},	// 22	   
   { 1,		0x20,	0x308F,	PM_SBL_WRITE,	192,		192},	// 23	   
   { 1,		0x00,	0x308F,	PM_SBL_WRITE,	193,		193},	// 24	   
   { 1,		0x01,	0x3090,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 1,		0x00,	0x3091,	PM_SBL_WRITE,	0,		0},	// 26	   
   
   // MODE - FTS_11_10_9: 22
   //sid  data    register  reg op      cond start    cond end
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x05,	0x3250,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0xF0,	0x3269,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0x4F,	0x326B,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0xC0,	0x3268,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0x41,	0x32AD,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0x80,	0x32A6,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 1,		0x00,	0x32D9,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 1,		0x00,	0x33D9,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 1,		0x00,	0x2FD9,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 1,		0x00,	0x30D9,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 1,		0x00,	0x2CD9,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 1,		0x00,	0x2DD9,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 1,		0x00,	0x29D9,	PM_SBL_WRITE,	194,		194},	// 27	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 1,		0x00,	0x2AD9,	PM_SBL_WRITE,	195,		195},	// 29	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 30	   
   { 1,		0x03,	0x2953,	PM_SBL_WRITE,	196,		196},	// 31	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 32	   
   { 1,		0x03,	0x2A53,	PM_SBL_WRITE,	197,		197},	// 33	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 34	   
   { 1,		0x80,	0x3251,	PM_SBL_WRITE,	0,		0},	// 35	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 36	   
   { 1,		0x80,	0x3351,	PM_SBL_WRITE,	0,		0},	// 37	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 38	   
   { 1,		0xC0,	0x2F51,	PM_SBL_WRITE,	0,		0},	// 39	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 40	   
   { 1,		0xC0,	0x3051,	PM_SBL_WRITE,	0,		0},	// 41	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 42	   
   { 1,		0xC0,	0x2C51,	PM_SBL_WRITE,	0,		0},	// 43	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 44	   
   { 1,		0xC0,	0x2D51,	PM_SBL_WRITE,	0,		0},	// 45	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 46	   
   { 1,		0xC0,	0x2951,	PM_SBL_WRITE,	198,		198},	// 47	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 48	   
   { 1,		0xC0,	0x2A51,	PM_SBL_WRITE,	199,		199},	// 49	   
   { 1,		0x03,	0x3254,	PM_SBL_WRITE,	200,		200},	// 50	   
   { 1,		0x02,	0x3254,	PM_SBL_WRITE,	201,		201},	// 51	   
   { 1,		0x02,	0x3260,	PM_SBL_WRITE,	0,		0},	// 52	   
   { 1,		0x82,	0x3261,	PM_SBL_WRITE,	0,		0},	// 53	   
   { 1,		0x08,	0x3248,	PM_SBL_WRITE,	0,		0},	// 54	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 55	   
   { 1,		0x01,	0x32DA,	PM_SBL_WRITE,	0,		0},	// 56	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 57	   
   { 1,		0x01,	0x33DA,	PM_SBL_WRITE,	0,		0},	// 58	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 59	   
   { 1,		0x01,	0x2FDA,	PM_SBL_WRITE,	0,		0},	// 60	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 61	   
   { 1,		0x01,	0x30DA,	PM_SBL_WRITE,	0,		0},	// 62	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 63	   
   { 1,		0x01,	0x2CDA,	PM_SBL_WRITE,	0,		0},	// 64	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 65	   
   { 1,		0x01,	0x2DDA,	PM_SBL_WRITE,	0,		0},	// 66	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 67	   
   { 1,		0x01,	0x29DA,	PM_SBL_WRITE,	202,		202},	// 68	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 69	   
   { 1,		0x01,	0x2ADA,	PM_SBL_WRITE,	203,		203},	// 70	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 71	   
   { 1,		0x08,	0x3271,	PM_SBL_WRITE,	0,		0},	// 72	   
   { 1,		0x04,	0x2F44,	PM_SBL_WRITE,	0,		0},	// 73	   
   { 1,		0x04,	0x2C44,	PM_SBL_WRITE,	0,		0},	// 74	   
   { 1,		0x04,	0x2944,	PM_SBL_WRITE,	204,		204},	// 75	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 76	   
   { 1,		0x08,	0x3368,	PM_SBL_WRITE,	0,		0},	// 77	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 78	   
   { 1,		0x03,	0x3361,	PM_SBL_WRITE,	0,		0},	// 79	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 80	   
   { 1,		0x0F,	0x326F,	PM_SBL_WRITE,	0,		0},	// 81	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 82	   
   { 1,		0x08,	0x3363,	PM_SBL_WRITE,	0,		0},	// 83	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 84	   
   { 1,		0x70,	0x3262,	PM_SBL_WRITE,	0,		0},	// 85	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 86	   
   { 1,		0x9B,	0x3264,	PM_SBL_WRITE,	205,		205},	// 87	   
   { 1,		0x89,	0x3264,	PM_SBL_WRITE,	206,		206},	// 88	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 89	   
   { 1,		0xE7,	0x32AC,	PM_SBL_WRITE,	0,		0},	// 90	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 91	   
   { 1,		0x99,	0x3096,	PM_SBL_WRITE,	0,		0},	// 92	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 93	   
   { 1,		0x93,	0x3097,	PM_SBL_WRITE,	0,		0},	// 94	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 95	   
   { 1,		0x99,	0x2D96,	PM_SBL_WRITE,	0,		0},	// 96	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 97	   
   { 1,		0x93,	0x2D97,	PM_SBL_WRITE,	0,		0},	// 98	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 99	   
   { 1,		0x99,	0x2A96,	PM_SBL_WRITE,	207,		207},	// 100	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 101	   
   { 1,		0x93,	0x2A97,	PM_SBL_WRITE,	208,		208},	// 102	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 103	   
   { 1,		0x99,	0x3396,	PM_SBL_WRITE,	0,		0},	// 104	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 105	   
   { 1,		0x93,	0x3397,	PM_SBL_WRITE,	0,		0},	// 106	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 107	   
   { 1,		0x60,	0x3262,	PM_SBL_WRITE,	0,		0},	// 108	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 109	   
   { 1,		0x1D,	0x3263,	PM_SBL_WRITE,	0,		0},	// 110	   
   { 1,		0xA5,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 111	   
   { 1,		0xCA,	0x3362,	PM_SBL_WRITE,	0,		0},	// 112	   
   { 1,		0xDD,	0x2D60,	PM_SBL_WRITE,	209,		209},	// 113	   
   { 1,		0xDD,	0x3060,	PM_SBL_WRITE,	210,		210},	// 114	   
   { 1,		0xDD,	0x3360,	PM_SBL_WRITE,	211,		211},	// 115	   
   { 1,		0x80,	0x3245,	PM_SBL_WRITE,	0,		0},	// 116	   
   { 1,		0x40,	0x3245,	PM_SBL_WRITE,	0,		0},	// 117	   
   { 1,		0x00,	0x32D0,	PM_SBL_WRITE,	0,		0},	// 118	   
   
   // MODE - BUCK_UL_LL_AND_PD_CONFIG: 23
   //sid  data    register  reg op      cond start    cond end
   { 1,		0xA5,	0x1AD0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x5A,	0x1A69,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0xA5,	0x1AD0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0x42,	0x1A6B,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0xA5,	0x1AD0,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0xC0,	0x1A68,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0xA5,	0x1DD0,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0x1C,	0x1D69,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0xA5,	0x1DD0,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0x0A,	0x1D6B,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0xA5,	0x1DD0,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0xC0,	0x1D68,	PM_SBL_WRITE,	0,		0},	// 13	   
   
   // MODE - S3_HFS_CONFIG: 24
   //sid  data    register  reg op      cond start    cond end
   { 1,		0xBC,	0x1B4A,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x84,	0x1B4B,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0x03,	0x1A51,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0xA5,	0x1CD0,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0x08,	0x1C50,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0xA5,	0x1CD0,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0x02,	0x1C51,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0x04,	0x1A52,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0x02,	0x1A53,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0x0B,	0x1A56,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0x15,	0x1A44,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0x0E,	0x1A5B,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0x4F,	0x1A5A,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 1,		0xA8,	0x1A5D,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 1,		0x0B,	0x1A5E,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 1,		0x30,	0x1B72,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 1,		0x01,	0x1A55,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 1,		0x0F,	0x1B75,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 1,		0xD0,	0x1A62,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 1,		0x88,	0x1A59,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 1,		0x00,	0x1B79,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 1,		0xA5,	0x1AD0,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 1,		0x01,	0x1ADA,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 1,		0xA5,	0x1BD0,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 1,		0x01,	0x1BDA,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 1,		0xA5,	0x1CD0,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 1,		0x01,	0x1CDA,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 1,		0xF9,	0x1B70,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 1,		0x40,	0x1A45,	PM_SBL_WRITE,	0,		0},	// 30	   
   
   // MODE - S4_HFS_CONFIG: 25
   //sid  data    register  reg op      cond start    cond end
   { 1,		0xA5,	0x1FD0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x08,	0x1F50,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0xB1,	0x1E4A,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0x83,	0x1E4B,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0x04,	0x1D51,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0x07,	0x1D52,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0x08,	0x1D53,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0x03,	0x1D56,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0x85,	0x1D44,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0x0D,	0x1D5B,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0x57,	0x1D5A,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0xBC,	0x1D5D,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0x06,	0x1D5E,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 1,		0x30,	0x1E72,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 1,		0x01,	0x1D55,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 1,		0x0F,	0x1E75,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 1,		0xD0,	0x1D62,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 1,		0x88,	0x1D59,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 1,		0x00,	0x1E79,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 1,		0x10,	0x1D63,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 1,		0xA5,	0x1DD0,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 1,		0x01,	0x1DDA,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 1,		0xA5,	0x1ED0,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 1,		0x01,	0x1EDA,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 1,		0xA5,	0x1FD0,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 1,		0x01,	0x1FDA,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 1,		0xF0,	0x1E70,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 1,		0x80,	0x1D45,	PM_SBL_WRITE,	0,		0},	// 29	   
   
   // MODE - S5_HFS_CONFIG: 26
   //sid  data    register  reg op      cond start    cond end
   { 1,		0xA5,	0x22D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x0B,	0x2250,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0xB3,	0x214A,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0x83,	0x214B,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0x03,	0x2051,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0x07,	0x2052,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0x03,	0x2053,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0x03,	0x2056,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0x44,	0x2044,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0x0C,	0x205B,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0x67,	0x205A,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0xB2,	0x205D,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0x0F,	0x205E,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 1,		0x30,	0x2172,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 1,		0x01,	0x2055,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 1,		0x0F,	0x2175,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 1,		0x01,	0x2040,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 1,		0x18,	0x2041,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 1,		0xC8,	0x2062,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 1,		0x88,	0x2059,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 1,		0x00,	0x2179,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 1,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 1,		0x01,	0x20DA,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 1,		0xA5,	0x21D0,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 1,		0x01,	0x21DA,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 1,		0xA5,	0x22D0,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 1,		0x01,	0x22DA,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 1,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 1,		0x88,	0x206C,	PM_SBL_WRITE,	0,		0},	// 30	   
   { 1,		0xF0,	0x2170,	PM_SBL_WRITE,	0,		0},	// 31	   
   { 1,		0x40,	0x2045,	PM_SBL_WRITE,	0,		0},	// 32	   
   
   // MODE - S7_HFS_CONFIG: 27
   //sid  data    register  reg op      cond start    cond end
   { 1,		0xBB,	0x274A,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x32,	0x2641,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0x84,	0x274B,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0x03,	0x2651,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 1,		0xA5,	0x28D0,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 1,		0x08,	0x2850,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 1,		0xA5,	0x28D0,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 1,		0x0F,	0x2851,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 1,		0x04,	0x2652,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 1,		0x02,	0x2653,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 1,		0x0B,	0x2656,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 1,		0x15,	0x2644,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 1,		0x0E,	0x265B,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 1,		0x4F,	0x265A,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 1,		0xA8,	0x265D,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 1,		0x06,	0x265E,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 1,		0x30,	0x2772,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 1,		0x01,	0x2655,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 1,		0x0F,	0x2775,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 1,		0xD0,	0x2662,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 1,		0x88,	0x2659,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 1,		0x00,	0x2779,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 1,		0x40,	0x2645,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 1,		0xA5,	0x26D0,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 1,		0x01,	0x26DA,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 1,		0xA5,	0x27D0,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 1,		0x01,	0x27DA,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 1,		0xA5,	0x28D0,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 1,		0x01,	0x28DA,	PM_SBL_WRITE,	0,		0},	// 30	   
   { 1,		0x83,	0x2660,	PM_SBL_WRITE,	0,		0},	// 31	   
   { 1,		0x8A,	0x2661,	PM_SBL_WRITE,	0,		0},	// 32	   
   { 1,		0xF9,	0x2770,	PM_SBL_WRITE,	0,		0},	// 33	   
   { 1,		0x40,	0x2645,	PM_SBL_WRITE,	0,		0},	// 34	   
   
   // MODE - BUA_OTP: 28
   //sid  data    register  reg op      cond start    cond end
   { 0,		0x04,	0x7069,	PM_SBL_WRITE,	212,		212},	// 2	   
   { 0,		0x14,	0x7068,	PM_SBL_WRITE,	213,		213},	// 3	   
   { 0,		0x03,	0x7711,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 0,		0x03,	0x7712,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 0,		0x03,	0x7715,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 0,		0x80,	0x7740,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 0,		0x80,	0x7746,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 0,		0x96,	0x1C40,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 0,		0x80,	0x1C46,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 2,		0x80,	0x1C46,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 0,		0x24,	0xD440,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 0,		0x02,	0xD441,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 0,		0x04,	0xD442,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 0,		0x22,	0xD445,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 0,		0x80,	0xD446,	PM_SBL_WRITE,	0,		0},	// 16	   
   
   // MODE - SSC_PIN_CTRL: 29
   //sid  data    register  reg op      cond start    cond end
   { 1,		0x01,	0x5940,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 1,		0x15,	0x5941,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 1,		0x01,	0x5E40,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 1,		0x2A,	0x5E41,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 0,		0x12,	0x0950,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 0,		0x15,	0x0951,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 0,		0x1A,	0x0952,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 0,		0x26,	0x0953,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 0,		0x00,	0x0948,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 0,		0x04,	0x708D,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 0,		0x08,	0x708C,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 0,		0xC0,	0x8040,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 0,		0x03,	0x8012,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 0,		0x03,	0x8013,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 0,		0x03,	0x8015,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 0,		0x80,	0x8046,	PM_SBL_WRITE,	0,		0},	// 19	   
   
   // MODE - MISC_CFG_PM8994: 30
   //sid  data    register  reg op      cond start    cond end
   { 0,		0xA5,	0x09D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 0,		0x00,	0x09D9,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 0,		0xA5,	0x09D0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 0,		0x01,	0x09DA,	PM_SBL_WRITE,	0,		0},	// 5	   
   
   // MODE - PON_INIT_PMI8994: 31
   //sid  data    register  reg op      cond start    cond end
   { 2,		0xA5,	0x08D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 2,		0xA4,	0x08F2,	PM_SBL_WRITE,	0,		0},	// 3	   
   
   // MODE - SMBCHG: 32
   //sid  data    register  reg op      cond start    cond end
   { 2,		0xA5,	0x16D0,	PM_SBL_WRITE,	0,		0},	// 1	   
   { 2,		0x18,	0x16F6,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 2,		0xA5,	0x12D0,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 2,		0x00,	0x12F4,	PM_SBL_WRITE,	214,		214},	// 4	   
   { 2,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 2,		0x08,	0x14FB,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 2,		0xA5,	0x10D0,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 2,		0xC7,	0x10FC,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 2,		0xA5,	0x13D0,	PM_SBL_WRITE,	215,		215},	// 9	   
   { 2,		0x83,	0x13FC,	PM_SBL_WRITE,	216,		216},	// 10	   
   
   // MODE - PBS_CONFIG_PMI: 33
   //sid  data    register  reg op      cond start    cond end
   { 2,		0xA5,	0x70D0,	PM_SBL_WRITE,	0,		0},	// 1	   
   { 2,		0x07,	0x70DA,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 2,		0x80,	0x7040,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 2,		0xC0,	0x7041,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 2,		0x00,	0x7042,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 2,		0x04,	0x7043,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 2,		3,	0x0080,	PM_SBL_PBS_RAM,	224,		224},	// 7	   PBS Version: PBS_RAM_MSM8994_2.PMIC.HW.PMi8994_2p0_2_0_14  PBS RAM Size = 128
   { 2,		4,	0x0080,	PM_SBL_PBS_RAM,	225,		225},	// 8	   PBS Version: PBS_RAM_MSM8994_2.PMIC.HW.PMi8996_1p0_2_0_7  PBS RAM Size = 128
   { 2,		0x00,	0x7040,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 2,		0x00,	0x7041,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 2,		0x00,	0x7042,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 2,		0x00,	0x7043,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 2,		0x04,	0x706D,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 2,		0x04,	0x706C,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 2,		0x80,	0x7846,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 2,		0x04,	0x7065,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 2,		0x00,	0x7064,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 2,		0x80,	0x7646,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 2,		0x04,	0x7061,	PM_SBL_WRITE,	217,		217},	// 19	   
   { 2,		0x0C,	0x7060,	PM_SBL_WRITE,	218,		218},	// 20	   
   { 2,		0x80,	0x7546,	PM_SBL_WRITE,	219,		219},	// 21	   
   { 2,		0x10,	0x7058,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 2,		0x04,	0x7059,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 2,		0x80,	0x7346,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 2,		0x04,	0x7071,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 2,		0x14,	0x7070,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 2,		0xC0,	0x7940,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 2,		0x80,	0x7946,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 2,		0xA5,	0x40D0,	PM_SBL_WRITE,	220,		220},	// 29	   
   { 2,		0x44,	0x40F2,	PM_SBL_WRITE,	221,		221},	// 30	   
   { 2,		0xA5,	0x40D0,	PM_SBL_WRITE,	222,		222},	// 31	   
   { 2,		0x80,	0x40F1,	PM_SBL_WRITE,	223,		223},	// 32	   
   
   // MODE - RESET_PMI8994: 34
   //sid  data    register  reg op      cond start    cond end
   { 2,		0x00,	0x085B,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 0,		0x99,	0x01F4,	PM_SBL_DELAY,	0,		0},	// 3	   Offset: 500ms. Address Offset represent delay time in this case.
   { 2,		0x06,	0x085A,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 2,		0x80,	0x085B,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 2,		0x00,	0x084B,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 2,		0x00,	0x0843,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 2,		0x00,	0x0847,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 2,		0xA5,	0x08D0,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 2,		0x06,	0x0875,	PM_SBL_WRITE,	0,		0},	// 10	   
   
   // MODE - INTERRUPT_PMI8994: 35
   //sid  data    register  reg op      cond start    cond end
   { 2,		0x00,	0x0546,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 2,		0x80,	0x0546,	PM_SBL_WRITE,	0,		0},	// 3	   
   
   // MODE - S1B_HFS_CONFIG: 36
   //sid  data    register  reg op      cond start    cond end
   { 3,		0xA5,	0x16D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 3,		0x0B,	0x1650,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 3,		0x81,	0x1460,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 3,		0xB5,	0x154A,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 3,		0x83,	0x154B,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 3,		0x03,	0x1451,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 3,		0x07,	0x1452,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 3,		0x05,	0x1453,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 3,		0x01,	0x1456,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 3,		0x15,	0x1444,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 3,		0x0D,	0x145B,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 3,		0x5F,	0x145A,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 3,		0xBC,	0x145D,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 3,		0x0C,	0x145E,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 3,		0x30,	0x1572,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 3,		0x01,	0x1455,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 3,		0x0F,	0x1575,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 3,		0x00,	0x1440,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 3,		0x34,	0x1441,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 3,		0xD0,	0x1462,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 3,		0x88,	0x1459,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 3,		0x00,	0x1579,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 3,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 3,		0x01,	0x14DA,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 3,		0xA5,	0x15D0,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 3,		0x01,	0x15DA,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 3,		0xA5,	0x16D0,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 3,		0x01,	0x16DA,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 3,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 30	   
   { 3,		0x88,	0x146C,	PM_SBL_WRITE,	0,		0},	// 31	   
   { 3,		0xF0,	0x1570,	PM_SBL_WRITE,	0,		0},	// 32	   
   { 3,		0x40,	0x1445,	PM_SBL_WRITE,	0,		0},	// 33	   
   
   // MODE - S3B_FTS_LUT: 37
   //sid  data    register  reg op      cond start    cond end
   { 3,		0x0C,	0x1B80,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 3,		0x09,	0x1B81,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 3,		0x06,	0x1B82,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 3,		0x3F,	0x1B83,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 3,		0x05,	0x1B87,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 3,		0x06,	0x1B88,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 3,		0x3F,	0x1B89,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 3,		0x40,	0x1B8C,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 3,		0x26,	0x1B8D,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 3,		0x43,	0x1B8E,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 3,		0x00,	0x1B8F,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 3,		0x01,	0x1B90,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 3,		0x00,	0x1B91,	PM_SBL_WRITE,	0,		0},	// 14	   
   
   // MODE - FTS_2B_3B_CONFIG: 38
   //sid  data    register  reg op      cond start    cond end
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 3,		0x05,	0x1750,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 3,		0x1A,	0x1763,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 3,		0xF0,	0x1769,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 3,		0x4F,	0x176B,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 3,		0xC0,	0x1768,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 3,		0x07,	0x17AC,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 3,		0x41,	0x17AD,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 3,		0x80,	0x17A6,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 3,		0x00,	0x17D9,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 3,		0x00,	0x18D9,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 3,		0x00,	0x1AD9,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 3,		0x00,	0x1BD9,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 3,		0x08,	0x1748,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 3,		0x80,	0x1751,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 3,		0x80,	0x1851,	PM_SBL_WRITE,	0,		0},	// 30	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 31	   
   { 3,		0xC0,	0x1A51,	PM_SBL_WRITE,	0,		0},	// 32	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 33	   
   { 3,		0xC0,	0x1B51,	PM_SBL_WRITE,	0,		0},	// 34	   
   { 3,		0x01,	0x1754,	PM_SBL_WRITE,	0,		0},	// 35	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 36	   
   { 3,		0x01,	0x1863,	PM_SBL_WRITE,	0,		0},	// 37	   
   { 3,		0x02,	0x1760,	PM_SBL_WRITE,	0,		0},	// 38	   
   { 3,		0x82,	0x1761,	PM_SBL_WRITE,	0,		0},	// 39	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 40	   
   { 3,		0x01,	0x17DA,	PM_SBL_WRITE,	0,		0},	// 41	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 42	   
   { 3,		0x01,	0x18DA,	PM_SBL_WRITE,	0,		0},	// 43	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 44	   
   { 3,		0x01,	0x1ADA,	PM_SBL_WRITE,	0,		0},	// 45	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 46	   
   { 3,		0x01,	0x1BDA,	PM_SBL_WRITE,	0,		0},	// 47	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 48	   
   { 3,		0x04,	0x1771,	PM_SBL_WRITE,	0,		0},	// 49	   
   { 3,		0x04,	0x1A44,	PM_SBL_WRITE,	0,		0},	// 50	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 51	   
   { 3,		0x08,	0x1868,	PM_SBL_WRITE,	0,		0},	// 52	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 53	   
   { 3,		0x03,	0x1861,	PM_SBL_WRITE,	0,		0},	// 54	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 55	   
   { 3,		0x08,	0x1863,	PM_SBL_WRITE,	0,		0},	// 56	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 57	   
   { 3,		0x03,	0x176F,	PM_SBL_WRITE,	0,		0},	// 58	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 59	   
   { 3,		0x40,	0x1762,	PM_SBL_WRITE,	0,		0},	// 60	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 61	   
   { 3,		0x1C,	0x1763,	PM_SBL_WRITE,	0,		0},	// 62	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 63	   
   { 3,		0xD2,	0x1764,	PM_SBL_WRITE,	0,		0},	// 64	   
   { 3,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 65	   
   { 3,		0xCA,	0x1862,	PM_SBL_WRITE,	0,		0},	// 66	   
   { 3,		0xDD,	0x1860,	PM_SBL_WRITE,	226,		226},	// 67	   
   { 3,		0xDD,	0x1860,	PM_SBL_WRITE,	227,		227},	// 68	   
   { 3,		0x40,	0x1745,	PM_SBL_WRITE,	0,		0},	// 69	   
   { 3,		0x00,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 70	   
   
   // MODE - FLASH_CONFIG: 39
   //sid  data    register  reg op      cond start    cond end
   { 3,		0x81,	0xD35C,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 3,		0x03,	0xD34A,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 3,		0x40,	0xD34F,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 3,		0xA1,	0xD352,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 3,		0xAD,	0xD354,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 3,		0x80,	0xD351,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 3,		0xE1,	0xD35A,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 3,		0x4F,	0xD341,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 3,		0x81,	0xD35D,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 2,		0xA5,	0x16D0,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 2,		0x25,	0x16F4,	PM_SBL_WRITE,	0,		0},	// 12	   
   
   // MODE - HAPTICS_PMI8994: 40
   //sid  data    register  reg op      cond start    cond end
   { 3,		0x00,	0xC04F,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 3,		0x01,	0xC050,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 3,		0x01,	0xC052,	PM_SBL_WRITE,	0,		0},	// 4	   
   
   // MODE - WLED_CONFIG: 41
   //sid  data    register  reg op      cond start    cond end
   { 3,		0x09,	0xD8E2,	PM_SBL_WRITE,	0,		0},	// 9
   { 3,         0x80,   0xD85A, PM_SBL_WRITE,   0,              0},	// ADD by TCTNB.LCY, 2016-11-10,defect-3396859
   // MODE - AUDIO_BOOST: 42
   //sid  data    register  reg op      cond start    cond end
   { 3,		0x80,	0xA056,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 3,		0xA5,	0xA0D0,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 3,		0x80,	0xA0E6,	PM_SBL_WRITE,	0,		0},	// 4	   
   
   // MODE - BOOST_BYPASS: 43
   //sid  data    register  reg op      cond start    cond end
   { 3,		0xE0,	0xA245,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 3,		0x0E,	0xA250,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 3,		0x95,	0xA24B,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 3,		0x01,	0xA251,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 3,		0x15,	0xA257,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 3,		0x6C,	0xA25C,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 3,		0x02,	0xA24C,	PM_SBL_WRITE,	0,		0},	// 8	   
   
   // MODE - LCD_AMOLED_PMI: 44
   //sid  data    register  reg op      cond start    cond end
   { 3,		0x80,	0xDE49,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 3,		0xA5,	0xDCD0,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 3,		0xBF,	0xDC4B,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 3,		0x85,	0xDC50,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 3,		0xA5,	0xDCD0,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 3,		0xF7,	0xDC58,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 3,		0x00,	0xDC61,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 2,		0x04,	0x7069,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 2,		0x08,	0x7068,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 2,		0x80,	0x7740,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 2,		0x80,	0x7746,	PM_SBL_WRITE,	0,		0},	// 12	   
   
   // MODE - MPP_CONFIG_PMi8994: 45
   //sid  data    register  reg op      cond start    cond end
   { 2,		0xA5,	0xA2D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 2,		0x00,	0xA2D9,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 2,		0xA5,	0xA2D0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 2,		0x01,	0xA2DA,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 2,		0x00,	0xA240,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 2,		0x00,	0xA246,	PM_SBL_WRITE,	0,		0},	// 7	   
   
   // MODE - PPON_PM8004: 46
   //sid  data    register  reg op      cond start    cond end
   { 5,		0xA5,	0x18D0,	PM_SBL_WRITE,	0,		0},	// 1	   
   { 5,		0x80,	0x18C1,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 5,		0xA5,	0x1ED0,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 5,		0x80,	0x1EC1,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 5,		0xA5,	0x1DD0,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 5,		0x80,	0x1DC1,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 5,		0xA5,	0x21D0,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 5,		0x80,	0x21C1,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 5,		0x80,	0x17E2,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 5,		0x80,	0x20E2,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 0,		0x99,	0x07D0,	PM_SBL_DELAY,	0,		0},	// 13	   Offset: 2000ms. Address Offset represent delay time in this case.
   { 5,		0x00,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 14	   
   
   // MODE - PON_INIT_PM8004: 47
   //sid  data    register  reg op      cond start    cond end
   { 4,		0x01,	0x0874,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 4,		0xA5,	0x08D0,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 4,		0x06,	0x0875,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 4,		0x06,	0x0870,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 4,		0x81,	0x594A,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 4,		0x84,	0x2C44,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 4,		0xA5,	0x08D0,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 4,		0xA4,	0x08F2,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 4,		0x01,	0x0942,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 4,		0x00,	0x085A,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 4,		0x00,	0x085B,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 4,		0x00,	0x0846,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 4,		0x00,	0x0847,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 4,		0x00,	0x0844,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 4,		0x00,	0x0845,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 4,		0x00,	0x0871,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 0,		0x99,	0x014A,	PM_SBL_DELAY,	0,		0},	// 19	   Offset: 330ms. Address Offset represent delay time in this case.
   { 4,		0x06,	0x085A,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 4,		0x80,	0x085B,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 4,		0x01,	0x0846,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 4,		0x80,	0x0847,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 4,		0x20,	0x088A,	PM_SBL_WRITE,	0,		0},	// 24	   
   
   // MODE - PBS_CONFIG_8004: 48
   //sid  data    register  reg op      cond start    cond end
   { 4,		0x80,	0x7040,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 4,		0xC0,	0x7041,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 4,		0x00,	0x7042,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 4,		0x04,	0x7043,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 4,		5,	0x0100,	PM_SBL_PBS_RAM,	0,		0},	// 6	   PBS Version: PBS_RAM_MSM8994_2.PMIC.HW.PM8004_1p0_3_0_5  PBS RAM Size = 256
   { 4,		0x00,	0x7040,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 4,		0x00,	0x7041,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 4,		0x00,	0x7042,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 4,		0x00,	0x7043,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 4,		0x04,	0x7065,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 4,		0x00,	0x7064,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 4,		0x80,	0x7646,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 4,		0x04,	0x7059,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 4,		0x04,	0x7058,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 4,		0x80,	0x7346,	PM_SBL_WRITE,	0,		0},	// 16	   
   
   // MODE - MPP_CONFIG_PM8004: 49
   //sid  data    register  reg op      cond start    cond end
   { 4,		0xA5,	0xA0D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 4,		0x00,	0xA0D9,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 4,		0xA5,	0xA0D0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 4,		0x01,	0xA0DA,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 4,		0x00,	0xA040,	PM_SBL_WRITE,	0,		0},	// 6	   
   
   // MODE - S4C_FTS_LUT: 50
   //sid  data    register  reg op      cond start    cond end
   { 5,		0x0C,	0x1E80,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 5,		0x09,	0x1E81,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 5,		0x06,	0x1E82,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 5,		0x3F,	0x1E83,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 5,		0x05,	0x1E87,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 5,		0x06,	0x1E88,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 5,		0x3F,	0x1E89,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 5,		0x40,	0x1E8C,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 5,		0x26,	0x1E8D,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 5,		0x43,	0x1E8E,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 5,		0x00,	0x1E8F,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 5,		0x01,	0x1E90,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 5,		0x00,	0x1E91,	PM_SBL_WRITE,	0,		0},	// 14	   
   
   // MODE - S1C_HFS_CONFIG: 51
   //sid  data    register  reg op      cond start    cond end
   { 5,		0xA5,	0x16D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 5,		0x0B,	0x1650,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 5,		0x81,	0x1460,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 5,		0xB5,	0x154A,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 5,		0x83,	0x154B,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 5,		0x03,	0x1451,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 5,		0x07,	0x1452,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 5,		0x05,	0x1453,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 5,		0x01,	0x1456,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 5,		0x15,	0x1444,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 5,		0x0D,	0x145B,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 5,		0x5F,	0x145A,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 5,		0xBC,	0x145D,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 5,		0x0C,	0x145E,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 5,		0x30,	0x1572,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 5,		0x01,	0x1455,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 5,		0x0F,	0x1575,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 5,		0x00,	0x1440,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 5,		0x34,	0x1441,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 5,		0xD0,	0x1462,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 5,		0x88,	0x1459,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 5,		0x00,	0x1579,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 5,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 5,		0x01,	0x14DA,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 5,		0xA5,	0x15D0,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 5,		0x01,	0x15DA,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 5,		0xA5,	0x16D0,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 5,		0x01,	0x16DA,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 5,		0xA5,	0x14D0,	PM_SBL_WRITE,	0,		0},	// 30	   
   { 5,		0x88,	0x146C,	PM_SBL_WRITE,	0,		0},	// 31	   
   { 5,		0xF0,	0x1570,	PM_SBL_WRITE,	0,		0},	// 32	   
   { 5,		0x40,	0x1445,	PM_SBL_WRITE,	0,		0},	// 33	   
   
   // MODE - FTS_2C_4C CONFIG: 52
   //sid  data    register  reg op      cond start    cond end
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 5,		0x1A,	0x1763,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 5,		0xF0,	0x1769,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 5,		0x4F,	0x176B,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 5,		0xC0,	0x1768,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 5,		0x07,	0x17AC,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 5,		0x41,	0x17AD,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 5,		0x80,	0x17A6,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 5,		0x00,	0x17D9,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 5,		0x00,	0x18D9,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 5,		0x00,	0x1DD9,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 5,		0x00,	0x1ED9,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 5,		0x01,	0x1754,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 5,		0x40,	0x1745,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 5,		0x82,	0x1761,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 5,		0x01,	0x17DA,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 5,		0x01,	0x18DA,	PM_SBL_WRITE,	0,		0},	// 30	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 31	   
   { 5,		0x01,	0x1DDA,	PM_SBL_WRITE,	0,		0},	// 32	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 33	   
   { 5,		0x01,	0x1EDA,	PM_SBL_WRITE,	0,		0},	// 34	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 35	   
   { 5,		0x04,	0x1771,	PM_SBL_WRITE,	0,		0},	// 36	   
   { 5,		0x04,	0x1D44,	PM_SBL_WRITE,	0,		0},	// 37	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 38	   
   { 5,		0x08,	0x1868,	PM_SBL_WRITE,	0,		0},	// 39	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 40	   
   { 5,		0x03,	0x1861,	PM_SBL_WRITE,	0,		0},	// 41	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 42	   
   { 5,		0x08,	0x1863,	PM_SBL_WRITE,	0,		0},	// 43	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 44	   
   { 5,		0x03,	0x176F,	PM_SBL_WRITE,	0,		0},	// 45	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 46	   
   { 5,		0xAA,	0x1862,	PM_SBL_WRITE,	0,		0},	// 47	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 48	   
   { 5,		0x12,	0x1764,	PM_SBL_WRITE,	0,		0},	// 49	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 50	   
   { 5,		0x99,	0x1896,	PM_SBL_WRITE,	0,		0},	// 51	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 52	   
   { 5,		0x93,	0x1897,	PM_SBL_WRITE,	0,		0},	// 53	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 54	   
   { 5,		0x99,	0x1E96,	PM_SBL_WRITE,	0,		0},	// 55	   
   { 5,		0xA5,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 56	   
   { 5,		0x93,	0x1E97,	PM_SBL_WRITE,	0,		0},	// 57	   
   { 5,		0xDD,	0x1E60,	PM_SBL_WRITE,	228,		228},	// 58	   
   { 5,		0xDD,	0x1860,	PM_SBL_WRITE,	229,		229},	// 59	   
   { 5,		0x00,	0x17D0,	PM_SBL_WRITE,	0,		0},	// 60	   
   
   // MODE - FTS_5C_CONFIG: 53
   //sid  data    register  reg op      cond start    cond end
   { 5,		0x00,	0x2048,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 5,		0x00,	0x2046,	PM_SBL_WRITE,	0,		0},	// 3	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 4	   
   { 5,		0xD1,	0x2041,	PM_SBL_WRITE,	0,		0},	// 5	   
   { 5,		0x1A,	0x2063,	PM_SBL_WRITE,	0,		0},	// 6	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 7	   
   { 5,		0xF0,	0x2069,	PM_SBL_WRITE,	0,		0},	// 8	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 9	   
   { 5,		0x4F,	0x206B,	PM_SBL_WRITE,	0,		0},	// 10	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 11	   
   { 5,		0xC0,	0x2068,	PM_SBL_WRITE,	0,		0},	// 12	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 13	   
   { 5,		0x07,	0x20AC,	PM_SBL_WRITE,	0,		0},	// 14	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 15	   
   { 5,		0x41,	0x20AD,	PM_SBL_WRITE,	0,		0},	// 16	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 17	   
   { 5,		0x80,	0x20A6,	PM_SBL_WRITE,	0,		0},	// 18	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 19	   
   { 5,		0x00,	0x20D9,	PM_SBL_WRITE,	0,		0},	// 20	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 21	   
   { 5,		0x00,	0x21D9,	PM_SBL_WRITE,	0,		0},	// 22	   
   { 5,		0x00,	0x2054,	PM_SBL_WRITE,	0,		0},	// 23	   
   { 5,		0x82,	0x2061,	PM_SBL_WRITE,	0,		0},	// 24	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 25	   
   { 5,		0x01,	0x20DA,	PM_SBL_WRITE,	0,		0},	// 26	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 27	   
   { 5,		0x01,	0x21DA,	PM_SBL_WRITE,	0,		0},	// 28	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 29	   
   { 5,		0x08,	0x2168,	PM_SBL_WRITE,	0,		0},	// 30	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 31	   
   { 5,		0x03,	0x2161,	PM_SBL_WRITE,	0,		0},	// 32	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 33	   
   { 5,		0x08,	0x2163,	PM_SBL_WRITE,	0,		0},	// 34	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 35	   
   { 5,		0x03,	0x206F,	PM_SBL_WRITE,	0,		0},	// 36	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 38	   
   { 5,		0x04,	0x2071,	PM_SBL_WRITE,	0,		0},	// 39	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 40	   
   { 5,		0xAA,	0x2162,	PM_SBL_WRITE,	0,		0},	// 41	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 42	   
   { 5,		0x12,	0x2064,	PM_SBL_WRITE,	0,		0},	// 43	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 44	   
   { 5,		0x99,	0x2196,	PM_SBL_WRITE,	0,		0},	// 45	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 46	   
   { 5,		0x99,	0x2196,	PM_SBL_WRITE,	0,		0},	// 47	   
   { 5,		0xA5,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 48	   
   { 5,		0x93,	0x2197,	PM_SBL_WRITE,	0,		0},	// 49	   
   { 5,		0x00,	0x20D0,	PM_SBL_WRITE,	0,		0},	// 50	   
   
   // MODE - INTERRUPT_PM8004: 54
   //sid  data    register  reg op      cond start    cond end
   { 4,		0x00,	0x0546,	PM_SBL_WRITE,	0,		0},	// 2	   
   { 4,		0x80,	0x0546,	PM_SBL_WRITE,	0,		0},	// 3	   
   
   // MODE - SBL_ID: 55
   //sid  data    register  reg op      cond start    cond end
   { 0,		0x38,	0x09D0,	PM_SBL_WRITE,	0,		0},	// 1	   
   
   // MODE - PMi8996_SBL_ID: 56
   //sid  data    register  reg op      cond start    cond end
   { 2,		0x89,	0x09D0,	PM_SBL_WRITE,	0,		0},	// 1	   
   { 2,		0x2F,	0x09D0,	PM_SBL_WRITE,	230,		230},	// 2	   
   
   	// This line of data is created by PDM per request from Embedded SW Driver. It is not part of database.
   { 0,	0x00,	0x0000,	PM_SBL_OPERATION_INVALID,	0,	0}
};
