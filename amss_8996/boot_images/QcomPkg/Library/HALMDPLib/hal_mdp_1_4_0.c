/*! \file */

/*
===========================================================================

FILE:         hal_mdp_1_4_0.c

DESCRIPTION:  MDP Initialization
  
===========================================================================
===========================================================================
Copyright (c) 2012 - 2014 Qualcomm Technologies, Inc.
All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

/*------------------------------------------------------------------------------
 * Include Files
 *----------------------------------------------------------------------------*/
#include "hal_mdp_i.h"
#include "mdssreg_1.4.0.h"

#ifdef __cplusplus
extern "C" {
#endif


/*------------------------------------------------------------------------------
 * Defines
 *----------------------------------------------------------------------------*/
#define  HAL_MDP_1_4_0_NUM_OF_RGB_LAYERS                       2     /** RGB Source Surface Pixel Processors      */
#define  HAL_MDP_1_4_0_NUM_OF_VIG_LAYERS                       2     /** VIG Source Surface Pixel Processors      */
#define  HAL_MDP_1_4_0_NUM_OF_DMA_LAYERS                       1     /** DMA Source Surface Pixel Processors      */
#define  HAL_MDP_1_4_0_NUM_OF_CURSOR_LAYERS                    2     /** CURSOR Source Surface Pixel Processors  */
#define  HAL_MDP_1_4_0_NUM_OF_LAYER_MIXERS                     3     /** Layer Mixers */
#define  HAL_MDP_1_4_0_NUM_OF_DSPP                             2     /** Destination Surface Pixel Processor           */
#define  HAL_MDP_1_4_0_NUM_OF_PHYSICAL_INTERFACES              1     /** INTF_0                                                    */
#define  HAL_MDP_1_4_0_NUM_OF_WRITEBACK_INTERFACES             2     /** WB_0, WB_2                      */
#define  HAL_MDP_1_4_0_NUM_OF_CONTROL_PATHS                    3     /** MDP_CTL_x (x = 0, 1, 2)        */
#define  HAL_MDP_1_4_0_NUM_OF_DATA_PATHS                       3     /** MDP_WB_x (x = 0, 1, 2)         */
#define  HAL_MDP_1_4_0_NUM_OF_PINGPONGS                        2     /** Pingpong blocks                          */
#define  HAL_MDP_1_4_0_NUM_OF_WATCHDOGS                        2     /** Watch Dogs                               */

/** LAYER MIXER */
#define  HAL_MDP_1_4_0_LAYER_MIXER_MAX_BLEND_STAGES            4     /** Blend Stage #0 ,1 ,2,3             */

/** VBIF max burst length */
#define HAL_MDP_1_4_0_VBIF_ROUND_ROBIN_QOS_ARB                 (HWIO_MMSS_VBIF_VBIF_ROUND_ROBIN_QOS_ARB_RR_QOS_EN_BMSK |\
                                                                HWIO_MMSS_VBIF_VBIF_ROUND_ROBIN_QOS_ARB_MMU_RR_QOS_EN_BMSK)

/* QoS priority re-mapping for real time read clients; real time clients are ViG, RGB, and DMA in line mode;*/
#define HAL_MDP_1_4_0_QOS_REMAPPER_REALTIME_CLIENTS            HAL_MDP_QOS_REMAPPER_INFO(                \
                                                               HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,     \
                                                               HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,         \
                                                               HAL_MDP_TRFCTRL_LATENCY_REALTIME,         \
                                                               HAL_MDP_TRFCTRL_LATENCY_REALTIME )

/* QoS priority re-mapping for non-real time read clients; non-real time clients are DMA in block mode;*/
#define HAL_MDP_1_4_0_QOS_REMAPPER_NONREALTIME_CLIENTS         HAL_MDP_QOS_REMAPPER_INFO(                \
                                                               HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,     \
                                                               HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,     \
                                                               HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,     \
                                                               HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME )
/**QSEED Scale Filter caps */
#define  HAL_MDP_1_4_0_QSEED_FILTER_CAPS                       (HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_NEAREST_NEIGHBOR_FILTER) | \
                                                                HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_BILINEAR_FILTER)         | \
                                                                HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_PCMN_FILTER)             | \
                                                                HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_CAF_FILTER))

/*------------------------------------------------------------------------
 * Global Data Definitions
 *------------------------------------------------------------------------ */
/* SMP client to h/w client id mapping */
const uint32 gMDP_SMPClientMap_1_4_0[HAL_MDP_SMP_MMB_CLIENT_MAX] = 
{
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_NONE  
  0x1,     //HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_Y,
  0x2,     //HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_CR,
  0x3,     //HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_CB,
  0x4,     //HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_Y,
  0x5,     //HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_CR,
  0x6,     //HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_CB,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_Y,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_CR,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_CB,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_Y,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_CR,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_CB,
  0x7,     //HAL_MDP_SMP_MMB_CLIENT_DMA0_FETCH_Y,
  0x8,     //HAL_MDP_SMP_MMB_CLIENT_DMA0_FETCH_CR,
  0x9,     //HAL_MDP_SMP_MMB_CLIENT_DMA0_FETCH_CB,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_DMA1_FETCH_Y,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_DMA1_FETCH_CR,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_DMA1_FETCH_CB,
  0xA,    //HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH,
  0xB,    //HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH,
  0x0,    //HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH,
  0x0,    //HAL_MDP_SMP_MMB_CLIENT_RGB3_FETCH,
};

/****************************************************************************
*
** FUNCTION: HAL_MDP_SetHWBlockRegOffsets_1_4_0()
*/
/*!
* \brief
*     set MDP HW block register offsets
*
*
****************************************************************************/
static void HAL_MDP_SetHWBlockRegOffsets_1_4_0(void)
{
  /* Control path HW block register offset */
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_NONE]           = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_0]              = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_1]              = (MMSS_MDP_CTL_1_REG_BASE_OFFS - MMSS_MDP_CTL_0_REG_BASE_OFFS);
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_2]              = (MMSS_MDP_CTL_2_REG_BASE_OFFS - MMSS_MDP_CTL_0_REG_BASE_OFFS);
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_3]              = (MMSS_MDP_CTL_3_REG_BASE_OFFS - MMSS_MDP_CTL_0_REG_BASE_OFFS);
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_4]              = (MMSS_MDP_CTL_4_REG_BASE_OFFS - MMSS_MDP_CTL_0_REG_BASE_OFFS);
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_5]              = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_6]              = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_7]              = 0x00000000;

  /* Data path HW block register offset */
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_NONE]                 = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_0]                    = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_1]                    = (MMSS_MDP_WB_1_REG_BASE_OFFS - MMSS_MDP_WB_0_REG_BASE_OFFS);
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_2]                    = (MMSS_MDP_WB_2_REG_BASE_OFFS - MMSS_MDP_WB_0_REG_BASE_OFFS);
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_3]                    = (MMSS_MDP_WB_3_REG_BASE_OFFS - MMSS_MDP_WB_0_REG_BASE_OFFS);
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_4]                    = (MMSS_MDP_WB_4_REG_BASE_OFFS - MMSS_MDP_WB_0_REG_BASE_OFFS);
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_5]                    = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_6]                    = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_7]                    = 0x00000000;

  /* Destination (DSPP) HW block register offset */
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_NONE]              = 0x00000000;
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_0]                 = 0x00000000;
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_1]                 = (MMSS_MDP_VP_0_DSPP_1_REG_BASE_OFFS - MMSS_MDP_VP_0_DSPP_0_REG_BASE_OFFS);
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_2]                 = (MMSS_MDP_VP_0_DSPP_2_REG_BASE_OFFS - MMSS_MDP_VP_0_DSPP_0_REG_BASE_OFFS);
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_3]                 = 0x00000000;
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_4]                 = 0x00000000;

  /* Physical interface HW block register offset */
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_ID_NONE] = 0x00000000;
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_0]       = 0x00000000;
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_1]       = (MMSS_MDP_INTF_1_REG_BASE_OFFS - MMSS_MDP_INTF_0_REG_BASE_OFFS);
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_2]       = (MMSS_MDP_INTF_2_REG_BASE_OFFS - MMSS_MDP_INTF_0_REG_BASE_OFFS);
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_3]       = (MMSS_MDP_INTF_3_REG_BASE_OFFS - MMSS_MDP_INTF_0_REG_BASE_OFFS);
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_4]       = 0x00000000;
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_5]       = 0x00000000;

  /* Layer mixer HW block register offset */
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_NONE]             = 0x00000000;
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_0]                = 0x00000000;
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_1]                = (MMSS_MDP_VP_0_LAYER_1_REG_BASE_OFFS - MMSS_MDP_VP_0_LAYER_0_REG_BASE_OFFS);
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_2]                = (MMSS_MDP_VP_0_LAYER_2_REG_BASE_OFFS - MMSS_MDP_VP_0_LAYER_0_REG_BASE_OFFS);
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_3]                = (MMSS_MDP_VP_0_LAYER_3_REG_BASE_OFFS - MMSS_MDP_VP_0_LAYER_0_REG_BASE_OFFS);
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_4]                = (MMSS_MDP_VP_0_LAYER_4_REG_BASE_OFFS - MMSS_MDP_VP_0_LAYER_0_REG_BASE_OFFS);
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_5]                = 0x00000000;
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_6]                = 0x00000000;

  /* Layer mixer blending stage register offset */
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_0]      = 0x00000000;
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_1]      = (HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND1_OP_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND0_OP_OFFS);
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_2]      = (HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND2_OP_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND0_OP_OFFS);
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_3]      = (HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND3_OP_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND0_OP_OFFS);
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_4]      = 0x00000000;
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_5]      = 0x00000000;
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_CURSOR] = 0x00000000;

  /* PingPong HW block register offset */
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_NONE]                  = 0x00000000;
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_0]                     = 0x00000000;
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_1]                     = (MMSS_MDP_PP_1_REG_BASE_OFFS - MMSS_MDP_PP_0_REG_BASE_OFFS);
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_2]                     = (MMSS_MDP_PP_2_REG_BASE_OFFS - MMSS_MDP_PP_0_REG_BASE_OFFS);
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_3]                     = 0x00000000;
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_4]                     = 0x00000000;

  /* Source (SSPP) HW block register offset */
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_NONE]                   = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_0]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_1]                  = (MMSS_MDP_VP_0_VIG_1_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_2]                  = (MMSS_MDP_VP_0_VIG_2_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_3]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_4]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_0]                  = (MMSS_MDP_VP_0_RGB_0_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_1]                  = (MMSS_MDP_VP_0_RGB_1_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_2]                  = (MMSS_MDP_VP_0_RGB_2_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_3]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_4]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_0]                  = (MMSS_MDP_VP_0_DMA_0_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_1]                  = (MMSS_MDP_VP_0_DMA_1_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_2]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_3]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_4]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_0]               = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_1]               = (HWIO_MMSS_MDP_VP_0_LAYER_1_CURSOR_IMG_SIZE_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_CURSOR_IMG_SIZE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_2]               = (HWIO_MMSS_MDP_VP_0_LAYER_2_CURSOR_IMG_SIZE_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_CURSOR_IMG_SIZE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_3]               = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_4]               = 0x00000000;

  /* Source (SSPP) Layer (Scalar) HW block register offset */
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_NONE]                   = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_0]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_1]                  = (HWIO_MMSS_MDP_VP_0_VIG_1_OP_MODE_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_OP_MODE_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_2]                  = (HWIO_MMSS_MDP_VP_0_VIG_2_OP_MODE_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_OP_MODE_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_3]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_4]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_0]                  = (HWIO_MMSS_MDP_VP_0_RGB_0_SCALE_CONFIG_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_QSEED2_CONFIG_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_1]                  = (HWIO_MMSS_MDP_VP_0_RGB_1_SCALE_CONFIG_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_QSEED2_CONFIG_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_2]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_3]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_4]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_0]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_1]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_2]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_3]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_4]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_0]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_1]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_2]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_3]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_4]               = 0x00000000;
}

/****************************************************************************
*
** FUNCTION: HAL_MDP_ReadHardwareInfo_1_4_0()
*/
/*!
* \brief
*     Reads the hardware capabilities for the given MDP Version(1.4.0)
*
* \param [out]  psHwInfo            - Hardware information
* \param [out]  psMdpHwInfo         - MDP hardware information 
*
* \retval NONE
*
****************************************************************************/
void HAL_MDP_ReadHardwareInfo_1_4_0(HAL_MDP_HwInfo             *psHwInfo,
                                    HAL_MDP_Hw_Private_Info    *psMdpHwInfo)
{
   /*
    * Notes: This file only contains differences between MDP_1.0.0 and MDP_1.4.0
    */
   MMSS_MDP_WB_1_REG_BASE            = (MMSS_MDSS_REG_BASE + 0x00011500);
   MMSS_MDP_WB_2_REG_BASE            = (MMSS_MDSS_REG_BASE + 0x00011900);
   MMSS_MDP_WB_3_REG_BASE            = (MMSS_MDSS_REG_BASE + 0x00011d00);
   MMSS_MDP_WB_4_REG_BASE            = (MMSS_MDSS_REG_BASE + 0x00012100);   
   MMSS_MDP_INTF_0_REG_BASE          = (MMSS_MDSS_REG_BASE + 0x00012500);
   MMSS_MDP_INTF_1_REG_BASE          = (MMSS_MDSS_REG_BASE + 0x00012700);
   MMSS_MDP_INTF_2_REG_BASE          = (MMSS_MDSS_REG_BASE + 0x00012900);
   MMSS_MDP_INTF_3_REG_BASE          = (MMSS_MDSS_REG_BASE + 0x00012B00);   
   MMSS_MDP_PP_0_REG_BASE            = (MMSS_MDSS_REG_BASE + 0x00012D00);
   MMSS_MDP_PP_1_REG_BASE            = (MMSS_MDSS_REG_BASE + 0x00012E00);
   MMSS_MDP_PP_2_REG_BASE            = (MMSS_MDSS_REG_BASE + 0x00012F00);
   MMSS_MDP_AADC_0_REG_BASE          = (MMSS_MDSS_REG_BASE + 0x00013100);
   MMSS_MDP_AADC_1_REG_BASE          = (MMSS_MDSS_REG_BASE + 0x00013300);
   MMSS_MDP_ENCR_NS_0_REG_BASE       = (MMSS_MDSS_REG_BASE + 0x00013000);
   MMSS_MDP_ENCR_S_0_REG_BASE        = (MMSS_MDSS_REG_BASE + 0x00013010);

   HAL_MDP_SetHWBlockRegOffsets_1_4_0();

   if(NULL != psHwInfo)
   {
      psHwInfo->uNumOfRGBLayers              = HAL_MDP_1_4_0_NUM_OF_RGB_LAYERS;
      psHwInfo->uNumOfVIGLayers              = HAL_MDP_1_4_0_NUM_OF_VIG_LAYERS;
      psHwInfo->uNumOfDMALayers              = HAL_MDP_1_4_0_NUM_OF_DMA_LAYERS;
      psHwInfo->uNumOfCursorLayers           = HAL_MDP_1_4_0_NUM_OF_CURSOR_LAYERS;
      psHwInfo->uNumOfLayerMixers            = HAL_MDP_1_4_0_NUM_OF_LAYER_MIXERS;
      psHwInfo->uNumOfDSPPs                  = HAL_MDP_1_4_0_NUM_OF_DSPP;      
      psHwInfo->uNumOfBltEngines             = HAL_MDP_1_4_0_NUM_OF_WRITEBACK_INTERFACES;     
      psHwInfo->uNumOfControlPaths           = HAL_MDP_1_4_0_NUM_OF_CONTROL_PATHS;
      psHwInfo->uNumOfDataPaths              = HAL_MDP_1_4_0_NUM_OF_DATA_PATHS;
      psHwInfo->uNumOfPhyInterfaces          = HAL_MDP_1_4_0_NUM_OF_PHYSICAL_INTERFACES;
      psHwInfo->uNumOfPingPongs              = HAL_MDP_1_4_0_NUM_OF_PINGPONGS;
      psHwInfo->uNumOfWatchDogs              = HAL_MDP_1_4_0_NUM_OF_WATCHDOGS;      
   }

   if(NULL != psMdpHwInfo)
   {
      //VBIF
      psMdpHwInfo->uVBIFRoundRobinQosArb              = HAL_MDP_1_4_0_VBIF_ROUND_ROBIN_QOS_ARB;

      //QoS
      psMdpHwInfo->uQosRemapperRealTimeClients        = HAL_MDP_1_4_0_QOS_REMAPPER_REALTIME_CLIENTS;
      psMdpHwInfo->uQosRemapperNonRealTimeClients     = HAL_MDP_1_4_0_QOS_REMAPPER_NONREALTIME_CLIENTS;

      //DST PACK PATTERN
      psMdpHwInfo->pDstPackPatternInfo                = (uint32 *)&guSrcUnpackInfo;
      
      // SMP client ID mapping
      psMdpHwInfo->pClientToHWClientMap               = (uint32 *)&gMDP_SMPClientMap_1_4_0;

      // Histogram config function
      psMdpHwInfo->sIpFxnTbl.HistogramConfig          = HAL_MDP_DSPP_HistogramV2_Config;

      // Histogram lock function
      psMdpHwInfo->sIpFxnTbl.HistogramLock            = HAL_MDP_DSPP_HistogramV2_Lock;
      
      // QSEED
      psMdpHwInfo->uQSEEDSupportedScaleFilters        = HAL_MDP_1_4_0_QSEED_FILTER_CAPS;      
   }
}


#ifdef __cplusplus
}
#endif
