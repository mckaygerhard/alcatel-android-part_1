/*! \file */

/*
===========================================================================

FILE:         mdssreg_1100d.h

===========================================================================
Copyright (c) 2012-2014 QUALCOMM Technologies Incorporated. 
All rights reserved. Licensed Material - Restricted Use.
Qualcomm Confidential and Proprietary.
===========================================================================
*/

#ifndef _MDSSREG_1100D_H_
#define _MDSSREG_1100D_H_

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_PPB0_CNTL_1100_ADDR                                                         (MMSS_MDP_REG_BASE      + 0x00000334)
#define HWIO_MMSS_MDP_PPB0_CNTL_1100_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PPB0_CNTL_1100_ADDR, HWIO_MMSS_MDP_PPB0_CNTL_RMSK)
#define HWIO_MMSS_MDP_PPB0_CNTL_1100_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PPB0_CNTL_1100_ADDR, m)
#define HWIO_MMSS_MDP_PPB0_CNTL_1100_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PPB0_CNTL_1100_ADDR,v)
#define HWIO_MMSS_MDP_PPB0_CNTL_1100_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PPB0_CNTL_1100_ADDR,m,v,HWIO_MMSS_MDP_PPB0_CNTL_1100_IN)

#define HWIO_MMSS_MDP_PPB0_CONFIG_1100_ADDR                                                       (MMSS_MDP_REG_BASE      + 0x00000338)
#define HWIO_MMSS_MDP_PPB0_CONFIG_1100_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PPB0_CONFIG_1100_ADDR, HWIO_MMSS_MDP_PPB0_CONFIG_RMSK)
#define HWIO_MMSS_MDP_PPB0_CONFIG_1100_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PPB0_CONFIG_1100_ADDR, m)
#define HWIO_MMSS_MDP_PPB0_CONFIG_1100_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PPB0_CONFIG_1100_ADDR,v)
#define HWIO_MMSS_MDP_PPB0_CONFIG_1100_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PPB0_CONFIG_1100_ADDR,m,v,HWIO_MMSS_MDP_PPB0_CONFIG_1100_IN)

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_PP_0
 *--------------------------------------------------------------------------*/


#define HWIO_MMSS_MDP_PP_0_SCM_REG1_ADDR                                                (MMSS_MDP_PP_0_REG_BASE      + 0x000000b4)
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_RMSK                                                0xffffffff
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_SCM_REG1_ADDR, HWIO_MMSS_MDP_PP_0_SCM_REG1_RMSK)
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_SCM_REG1_ADDR, m)
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PP_0_SCM_REG1_ADDR,v)
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PP_0_SCM_REG1_ADDR,m,v,HWIO_MMSS_MDP_PP_0_SCM_REG1_IN)
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_MIN_HOR_BLANK_HIGH_BMSK                             0xe0000000
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_MIN_HOR_BLANK_HIGH_SHFT                                   0x1d
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_TEST_EN_BMSK                                        0x10000000
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_TEST_EN_SHFT                                              0x1c
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_TEST_CH_BMSK                                         0xe000000
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_TEST_CH_SHFT                                              0x19
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_BP_BMSK                                              0x1000000
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_BP_SHFT                                                   0x18
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_MIN_HOR_BLANK_LOW_BMSK                                0xf00000
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_MIN_HOR_BLANK_LOW_SHFT                                    0x14
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_HDATA_BMSK                                               0xfff
#define HWIO_MMSS_MDP_PP_0_SCM_REG1_HDATA_SHFT                                                 0x0

#define HWIO_MMSS_MDP_PP_0_SCM_REG2_ADDR                                                (MMSS_MDP_PP_0_REG_BASE      + 0x000000b8)
#define HWIO_MMSS_MDP_PP_0_SCM_REG2_RMSK                                                0xffffffff
#define HWIO_MMSS_MDP_PP_0_SCM_REG2_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_SCM_REG2_ADDR, HWIO_MMSS_MDP_PP_0_SCM_REG2_RMSK)
#define HWIO_MMSS_MDP_PP_0_SCM_REG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_SCM_REG2_ADDR, m)
#define HWIO_MMSS_MDP_PP_0_SCM_REG2_SCM_REG2_BMSK                                       0xffffffff
#define HWIO_MMSS_MDP_PP_0_SCM_REG2_SCM_REG2_SHFT                                              0x0

#define HWIO_MMSS_MDP_PP_0_SCM_REVID_ADDR                                               (MMSS_MDP_PP_0_REG_BASE      + 0x000000bc)
#define HWIO_MMSS_MDP_PP_0_SCM_REVID_RMSK                                                     0xff
#define HWIO_MMSS_MDP_PP_0_SCM_REVID_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_SCM_REVID_ADDR, HWIO_MMSS_MDP_PP_0_SCM_REVID_RMSK)
#define HWIO_MMSS_MDP_PP_0_SCM_REVID_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_SCM_REVID_ADDR, m)
#define HWIO_MMSS_MDP_PP_0_SCM_REVID_SCM_REVID_BMSK                                           0xff
#define HWIO_MMSS_MDP_PP_0_SCM_REVID_SCM_REVID_SHFT                                            0x0

#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_ADDR                                   (MMSS_MDP_PP_0_REG_BASE      + 0x000000c0)
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_RMSK                                         0xff
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_ADDR, HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_RMSK)
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_ADDR, m)
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_ADDR,v)
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_ADDR,m,v,HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_IN)
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_HYSTERISIS_CTRL_BMSK                0xc0
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_HYSTERISIS_CTRL_SHFT                 0x6
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_FORCE_OFF_BMSK                      0x20
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_FORCE_OFF_SHFT                       0x5
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_FORCE_ON_BMSK                       0x10
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_FORCE_ON_SHFT                        0x4
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CLK_HYSTERISIS_CTRL_BMSK                      0xc
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CLK_HYSTERISIS_CTRL_SHFT                      0x2
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CLK_FORCE_OFF_BMSK                            0x2
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CLK_FORCE_OFF_SHFT                            0x1
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CLK_FORCE_ON_BMSK                             0x1
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_CTRL_1100_CLK_FORCE_ON_SHFT                             0x0

#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_ADDR                                 (MMSS_MDP_PP_0_REG_BASE      + 0x000000c4)
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_RMSK                                       0x1f
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_ADDR, HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_RMSK)
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_ADDR, m)
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_DIV2_CLK_ACTIVE_BMSK                       0x10
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_DIV2_CLK_ACTIVE_SHFT                        0x4
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_FBC_DIV2_CLK_ACTIVE_BMSK                    0x8
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_FBC_DIV2_CLK_ACTIVE_SHFT                    0x3
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_SCM_CLK_ACTIVE_BMSK                         0x4
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_SCM_CLK_ACTIVE_SHFT                         0x2
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_FBC_CLK_ACTIVE_BMSK                         0x2
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_FBC_CLK_ACTIVE_SHFT                         0x1
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_ENC_CLK_ACTIVE_BMSK                         0x1
#define HWIO_MMSS_MDP_PP_0_MDP_DISP_ENC_CLK_STATUS_1100_ENC_CLK_ACTIVE_SHFT                         0x0

#define HWIO_MMSS_MDP_PP_0_DCE_DATA_IN_SWAP_1100_ADDR                                        (MMSS_MDP_PP_0_REG_BASE      + 0x000000c8)
#define HWIO_MMSS_MDP_PP_0_DCE_DATA_IN_SWAP_1100_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_DCE_DATA_IN_SWAP_1100_ADDR, HWIO_MMSS_MDP_PP_0_DCE_DATA_IN_SWAP_RMSK)
#define HWIO_MMSS_MDP_PP_0_DCE_DATA_IN_SWAP_1100_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_DCE_DATA_IN_SWAP_1100_ADDR, m)
#define HWIO_MMSS_MDP_PP_0_DCE_DATA_IN_SWAP_1100_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PP_0_DCE_DATA_IN_SWAP_1100_ADDR,v)
#define HWIO_MMSS_MDP_PP_0_DCE_DATA_IN_SWAP_1100_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PP_0_DCE_DATA_IN_SWAP_1100_ADDR,m,v,HWIO_MMSS_MDP_PP_0_DCE_DATA_IN_SWAP_1100_IN)

#define HWIO_MMSS_MDP_PP_0_DCE_DATA_OUT_SWAP_1100_ADDR                                       (MMSS_MDP_PP_0_REG_BASE      + 0x000000cc)
#define HWIO_MMSS_MDP_PP_0_DCE_DATA_OUT_SWAP_1100_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_DCE_DATA_OUT_SWAP_1100_ADDR, HWIO_MMSS_MDP_PP_0_DCE_DATA_OUT_SWAP_RMSK)
#define HWIO_MMSS_MDP_PP_0_DCE_DATA_OUT_SWAP_1100_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_0_DCE_DATA_OUT_SWAP_1100_ADDR, m)
#define HWIO_MMSS_MDP_PP_0_DCE_DATA_OUT_SWAP_1100_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PP_0_DCE_DATA_OUT_SWAP_1100_ADDR,v)
#define HWIO_MMSS_MDP_PP_0_DCE_DATA_OUT_SWAP_1100_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PP_0_DCE_DATA_OUT_SWAP_1100_ADDR,m,v,HWIO_MMSS_MDP_PP_0_DCE_DATA_OUT_SWAP_1100_IN)

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_PP_1
 *--------------------------------------------------------------------------*/


#define HWIO_MMSS_MDP_PP_1_SCM_REG1_ADDR                                                (MMSS_MDP_PP_1_REG_BASE      + 0x000000b4)
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_RMSK                                                0xffffffff
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_SCM_REG1_ADDR, HWIO_MMSS_MDP_PP_1_SCM_REG1_RMSK)
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_SCM_REG1_ADDR, m)
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PP_1_SCM_REG1_ADDR,v)
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PP_1_SCM_REG1_ADDR,m,v,HWIO_MMSS_MDP_PP_1_SCM_REG1_IN)
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_MIN_HOR_BLANK_HIGH_BMSK                             0xe0000000
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_MIN_HOR_BLANK_HIGH_SHFT                                   0x1d
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_TEST_EN_BMSK                                        0x10000000
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_TEST_EN_SHFT                                              0x1c
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_TEST_CH_BMSK                                         0xe000000
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_TEST_CH_SHFT                                              0x19
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_BP_BMSK                                              0x1000000
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_BP_SHFT                                                   0x18
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_MIN_HOR_BLANK_LOW_BMSK                                0xf00000
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_MIN_HOR_BLANK_LOW_SHFT                                    0x14
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_HDATA_BMSK                                               0xfff
#define HWIO_MMSS_MDP_PP_1_SCM_REG1_HDATA_SHFT                                                 0x0

#define HWIO_MMSS_MDP_PP_1_SCM_REG2_ADDR                                                (MMSS_MDP_PP_1_REG_BASE      + 0x000000b8)
#define HWIO_MMSS_MDP_PP_1_SCM_REG2_RMSK                                                0xffffffff
#define HWIO_MMSS_MDP_PP_1_SCM_REG2_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_SCM_REG2_ADDR, HWIO_MMSS_MDP_PP_1_SCM_REG2_RMSK)
#define HWIO_MMSS_MDP_PP_1_SCM_REG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_SCM_REG2_ADDR, m)
#define HWIO_MMSS_MDP_PP_1_SCM_REG2_SCM_REG2_BMSK                                       0xffffffff
#define HWIO_MMSS_MDP_PP_1_SCM_REG2_SCM_REG2_SHFT                                              0x0

#define HWIO_MMSS_MDP_PP_1_SCM_REVID_ADDR                                               (MMSS_MDP_PP_1_REG_BASE      + 0x000000bc)
#define HWIO_MMSS_MDP_PP_1_SCM_REVID_RMSK                                                     0xff
#define HWIO_MMSS_MDP_PP_1_SCM_REVID_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_SCM_REVID_ADDR, HWIO_MMSS_MDP_PP_1_SCM_REVID_RMSK)
#define HWIO_MMSS_MDP_PP_1_SCM_REVID_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_SCM_REVID_ADDR, m)
#define HWIO_MMSS_MDP_PP_1_SCM_REVID_SCM_REVID_BMSK                                           0xff
#define HWIO_MMSS_MDP_PP_1_SCM_REVID_SCM_REVID_SHFT                                            0x0

#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_ADDR                                   (MMSS_MDP_PP_1_REG_BASE      + 0x000000c0)
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_RMSK                                         0xff
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_ADDR, HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_RMSK)
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_ADDR, m)
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_ADDR,v)
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_ADDR,m,v,HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_IN)
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_HYSTERISIS_CTRL_BMSK                0xc0
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_HYSTERISIS_CTRL_SHFT                 0x6
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_FORCE_OFF_BMSK                      0x20
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_FORCE_OFF_SHFT                       0x5
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_FORCE_ON_BMSK                       0x10
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CORE_CLK_FORCE_ON_SHFT                        0x4
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CLK_HYSTERISIS_CTRL_BMSK                      0xc
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CLK_HYSTERISIS_CTRL_SHFT                      0x2
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CLK_FORCE_OFF_BMSK                            0x2
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CLK_FORCE_OFF_SHFT                            0x1
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CLK_FORCE_ON_BMSK                             0x1
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_CTRL_1100_CLK_FORCE_ON_SHFT                             0x0

#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_ADDR                                 (MMSS_MDP_PP_1_REG_BASE      + 0x000000c4)
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_RMSK                                       0x1f
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_ADDR, HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_RMSK)
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_ADDR, m)
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_DIV2_CLK_ACTIVE_BMSK                       0x10
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_DIV2_CLK_ACTIVE_SHFT                        0x4
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_FBC_DIV2_CLK_ACTIVE_BMSK                    0x8
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_FBC_DIV2_CLK_ACTIVE_SHFT                    0x3
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_SCM_CLK_ACTIVE_BMSK                         0x4
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_SCM_CLK_ACTIVE_SHFT                         0x2
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_FBC_CLK_ACTIVE_BMSK                         0x2
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_FBC_CLK_ACTIVE_SHFT                         0x1
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_ENC_CLK_ACTIVE_BMSK                         0x1
#define HWIO_MMSS_MDP_PP_1_MDP_DISP_ENC_CLK_STATUS_1100_ENC_CLK_ACTIVE_SHFT                         0x0

#define HWIO_MMSS_MDP_PP_1_DCE_DATA_IN_SWAP_1100_ADDR                                        (MMSS_MDP_PP_1_REG_BASE      + 0x000000c8)
#define HWIO_MMSS_MDP_PP_1_DCE_DATA_IN_SWAP_1100_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_DCE_DATA_IN_SWAP_1100_ADDR, HWIO_MMSS_MDP_PP_1_DCE_DATA_IN_SWAP_RMSK)
#define HWIO_MMSS_MDP_PP_1_DCE_DATA_IN_SWAP_1100_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_DCE_DATA_IN_SWAP_1100_ADDR, m)
#define HWIO_MMSS_MDP_PP_1_DCE_DATA_IN_SWAP_1100_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PP_1_DCE_DATA_IN_SWAP_1100_ADDR,v)
#define HWIO_MMSS_MDP_PP_1_DCE_DATA_IN_SWAP_1100_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PP_1_DCE_DATA_IN_SWAP_1100_ADDR,m,v,HWIO_MMSS_MDP_PP_1_DCE_DATA_IN_SWAP_1100_IN)

#define HWIO_MMSS_MDP_PP_1_DCE_DATA_OUT_SWAP_1100_ADDR                                       (MMSS_MDP_PP_1_REG_BASE      + 0x000000cc)
#define HWIO_MMSS_MDP_PP_1_DCE_DATA_OUT_SWAP_1100_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_DCE_DATA_OUT_SWAP_1100_ADDR, HWIO_MMSS_MDP_PP_1_DCE_DATA_OUT_SWAP_RMSK)
#define HWIO_MMSS_MDP_PP_1_DCE_DATA_OUT_SWAP_1100_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PP_1_DCE_DATA_OUT_SWAP_1100_ADDR, m)
#define HWIO_MMSS_MDP_PP_1_DCE_DATA_OUT_SWAP_1100_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PP_1_DCE_DATA_OUT_SWAP_1100_ADDR,v)
#define HWIO_MMSS_MDP_PP_1_DCE_DATA_OUT_SWAP_1100_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PP_1_DCE_DATA_OUT_SWAP_1100_ADDR,m,v,HWIO_MMSS_MDP_PP_1_DCE_DATA_OUT_SWAP_1100_IN)

#endif /* _MDSSREG_1100D_H_ */
