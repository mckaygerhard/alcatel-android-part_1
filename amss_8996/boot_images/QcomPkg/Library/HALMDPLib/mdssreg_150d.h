/*! \file */

/*
===========================================================================

FILE:         mdssreg_150d.h

===========================================================================
Copyright (c) 2012-2013 QUALCOMM Technologies Incorporated. 
All rights reserved. Licensed Material - Restricted Use.
Qualcomm Confidential and Proprietary.
===========================================================================
*/

#ifndef _MDSSREG_150D_H_
#define _MDSSREG_150D_H_

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_ADDR                                                 (MMSS_MDP_REG_BASE      + 0x00000178)
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_RMSK                                                   0xffffff
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_ADDR, HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_RMSK)
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_ADDR, m)
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_ADDR,v)
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_ADDR,m,v,HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_IN)
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_HYSTERESIS_BMSK                                        0xff0000
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_HYSTERESIS_SHFT                                            0x10
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_UNUSED_ENABLE_BMSK                                       0xfc00
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_UNUSED_ENABLE_SHFT                                          0xa
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_DMA1_ENABLE_BMSK                                          0x200
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_DMA1_ENABLE_SHFT                                            0x9
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_DMA0_ENABLE_BMSK                                          0x100
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_DMA0_ENABLE_SHFT                                            0x8
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_RGB3_ENABLE_BMSK                                           0x80
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_RGB3_ENABLE_SHFT                                            0x7
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_RGB2_ENABLE_BMSK                                           0x40
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_RGB2_ENABLE_SHFT                                            0x6
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_RGB1_ENABLE_BMSK                                           0x20
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_RGB1_ENABLE_SHFT                                            0x5
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_RGB0_ENABLE_BMSK                                           0x10
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_RGB0_ENABLE_SHFT                                            0x4
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_VIG3_ENABLE_BMSK                                            0x8
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_VIG3_ENABLE_SHFT                                            0x3
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_VIG2_ENABLE_BMSK                                            0x4
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_VIG2_ENABLE_SHFT                                            0x2
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_VIG1_ENABLE_BMSK                                            0x2
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_VIG1_ENABLE_SHFT                                            0x1
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_VIG0_ENABLE_BMSK                                            0x1
#define HWIO_MMSS_MDP_PANIC_ROBUST_CTRL_VIG0_ENABLE_SHFT                                            0x0

#define HWIO_MMSS_MDP_PANIC_LUT0_ADDR                                                        (MMSS_MDP_REG_BASE      + 0x0000017c)
#define HWIO_MMSS_MDP_PANIC_LUT0_RMSK                                                          0xffffff
#define HWIO_MMSS_MDP_PANIC_LUT0_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PANIC_LUT0_ADDR, HWIO_MMSS_MDP_PANIC_LUT0_RMSK)
#define HWIO_MMSS_MDP_PANIC_LUT0_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PANIC_LUT0_ADDR, m)
#define HWIO_MMSS_MDP_PANIC_LUT0_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PANIC_LUT0_ADDR,v)
#define HWIO_MMSS_MDP_PANIC_LUT0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PANIC_LUT0_ADDR,m,v,HWIO_MMSS_MDP_PANIC_LUT0_IN)
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_UNUSED_BMSK                                                0xff0000
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_UNUSED_SHFT                                                    0x10
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_7_BMSK                                                       0xc000
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_7_SHFT                                                          0xe
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_6_BMSK                                                       0x3000
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_6_SHFT                                                          0xc
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_5_BMSK                                                        0xc00
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_5_SHFT                                                          0xa
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_4_BMSK                                                        0x300
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_4_SHFT                                                          0x8
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_3_BMSK                                                         0xc0
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_3_SHFT                                                          0x6
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_2_BMSK                                                         0x30
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_2_SHFT                                                          0x4
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_1_BMSK                                                          0xc
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_1_SHFT                                                          0x2
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_0_BMSK                                                          0x3
#define HWIO_MMSS_MDP_PANIC_LUT0_FL_0_SHFT                                                          0x0

#define HWIO_MMSS_MDP_PANIC_LUT1_ADDR                                                        (MMSS_MDP_REG_BASE      + 0x00000180)
#define HWIO_MMSS_MDP_PANIC_LUT1_RMSK                                                          0xffffff
#define HWIO_MMSS_MDP_PANIC_LUT1_IN          \
        in_dword_masked(HWIO_MMSS_MDP_PANIC_LUT1_ADDR, HWIO_MMSS_MDP_PANIC_LUT1_RMSK)
#define HWIO_MMSS_MDP_PANIC_LUT1_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_PANIC_LUT1_ADDR, m)
#define HWIO_MMSS_MDP_PANIC_LUT1_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_PANIC_LUT1_ADDR,v)
#define HWIO_MMSS_MDP_PANIC_LUT1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_PANIC_LUT1_ADDR,m,v,HWIO_MMSS_MDP_PANIC_LUT1_IN)
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_UNUSED_BMSK                                                0xff0000
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_UNUSED_SHFT                                                    0x10
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_15_BMSK                                                      0xc000
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_15_SHFT                                                         0xe
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_14_BMSK                                                      0x3000
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_14_SHFT                                                         0xc
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_13_BMSK                                                       0xc00
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_13_SHFT                                                         0xa
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_12_BMSK                                                       0x300
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_12_SHFT                                                         0x8
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_11_BMSK                                                        0xc0
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_11_SHFT                                                         0x6
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_10_BMSK                                                        0x30
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_10_SHFT                                                         0x4
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_9_BMSK                                                          0xc
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_9_SHFT                                                          0x2
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_8_BMSK                                                          0x3
#define HWIO_MMSS_MDP_PANIC_LUT1_FL_8_SHFT                                                          0x0

#define HWIO_MMSS_MDP_ROBUST_LUT_ADDR                                                        (MMSS_MDP_REG_BASE      + 0x00000184)
#define HWIO_MMSS_MDP_ROBUST_LUT_RMSK                                                          0xffffff
#define HWIO_MMSS_MDP_ROBUST_LUT_IN          \
        in_dword_masked(HWIO_MMSS_MDP_ROBUST_LUT_ADDR, HWIO_MMSS_MDP_ROBUST_LUT_RMSK)
#define HWIO_MMSS_MDP_ROBUST_LUT_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_ROBUST_LUT_ADDR, m)
#define HWIO_MMSS_MDP_ROBUST_LUT_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_ROBUST_LUT_ADDR,v)
#define HWIO_MMSS_MDP_ROBUST_LUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_ROBUST_LUT_ADDR,m,v,HWIO_MMSS_MDP_ROBUST_LUT_IN)
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_UNUSED_BMSK                                                0xff0000
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_UNUSED_SHFT                                                    0x10
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_15_BMSK                                                      0x8000
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_15_SHFT                                                         0xf
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_14_BMSK                                                      0x4000
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_14_SHFT                                                         0xe
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_13_BMSK                                                      0x2000
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_13_SHFT                                                         0xd
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_12_BMSK                                                      0x1000
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_12_SHFT                                                         0xc
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_11_BMSK                                                       0x800
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_11_SHFT                                                         0xb
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_10_BMSK                                                       0x400
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_10_SHFT                                                         0xa
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_9_BMSK                                                        0x200
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_9_SHFT                                                          0x9
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_8_BMSK                                                        0x100
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_8_SHFT                                                          0x8
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_7_BMSK                                                         0x80
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_7_SHFT                                                          0x7
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_6_BMSK                                                         0x40
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_6_SHFT                                                          0x6
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_5_BMSK                                                         0x20
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_5_SHFT                                                          0x5
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_4_BMSK                                                         0x10
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_4_SHFT                                                          0x4
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_3_BMSK                                                          0x8
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_3_SHFT                                                          0x3
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_2_BMSK                                                          0x4
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_2_SHFT                                                          0x2
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_1_BMSK                                                          0x2
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_1_SHFT                                                          0x1
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_0_BMSK                                                          0x1
#define HWIO_MMSS_MDP_ROBUST_LUT_FL_0_SHFT                                                          0x0


#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_ADDR                                           (MMSS_MDP_REG_BASE      + 0x00000200)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_RMSK                                           0xf3ff0fff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_ADDR, HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_RMSK)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_ADDR,v)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_ADDR,m,v,HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_IN)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_VIG_MASK_BMSK                                  0xf0000000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_VIG_MASK_SHFT                                        0x1c
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_INDEX_UPDATE_BMSK                               0x2000000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_INDEX_UPDATE_SHFT                                    0x19
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_VALUE_UPDATEN_BMSK                              0x1000000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_VALUE_UPDATEN_SHFT                                   0x18
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_INDEX_BMSK                                       0xff0000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_INDEX_SHFT                                           0x10
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_VALUE_BMSK                                          0xfff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_RAM_LUTN_VALUE_SHFT                                            0x0

#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_ADDR                                           (MMSS_MDP_REG_BASE      + 0x00000204)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_RMSK                                           0xf3ff0fff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_ADDR, HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_RMSK)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_ADDR,v)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_ADDR,m,v,HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_IN)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_VIG_MASK_BMSK                                  0xf0000000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_VIG_MASK_SHFT                                        0x1c
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_INDEX_UPDATE_BMSK                               0x2000000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_INDEX_UPDATE_SHFT                                    0x19
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_VALUE_UPDATEN_BMSK                              0x1000000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_VALUE_UPDATEN_SHFT                                   0x18
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_INDEX_BMSK                                       0xff0000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_INDEX_SHFT                                           0x10
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_VALUE_BMSK                                          0xfff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_RAM_LUTN_VALUE_SHFT                                            0x0

#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_ADDR                                           (MMSS_MDP_REG_BASE      + 0x00000208)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_RMSK                                           0xf3ff0fff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_ADDR, HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_RMSK)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_ADDR,v)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_ADDR,m,v,HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_IN)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_VIG_MASK_BMSK                                  0xf0000000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_VIG_MASK_SHFT                                        0x1c
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_INDEX_UPDATE_BMSK                               0x2000000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_INDEX_UPDATE_SHFT                                    0x19
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_VALUE_UPDATEN_BMSK                              0x1000000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_VALUE_UPDATEN_SHFT                                   0x18
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_INDEX_BMSK                                       0xff0000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_INDEX_SHFT                                           0x10
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_VALUE_BMSK                                          0xfff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_RAM_LUTN_VALUE_SHFT                                            0x0

#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_ADDR                                           (MMSS_MDP_REG_BASE      + 0x00000210)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_RMSK                                           0xf3ff0fff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_ADDR, HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_RMSK)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_ADDR,v)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_ADDR,m,v,HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_IN)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_RGB_MASK_BMSK                                  0xf0000000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_RGB_MASK_SHFT                                        0x1c
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_INDEX_UPDATE_BMSK                               0x2000000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_INDEX_UPDATE_SHFT                                    0x19
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_VALUE_UPDATEN_BMSK                              0x1000000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_VALUE_UPDATEN_SHFT                                   0x18
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_INDEX_BMSK                                       0xff0000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_INDEX_SHFT                                           0x10
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_VALUE_BMSK                                          0xfff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_RAM_LUTN_VALUE_SHFT                                            0x0

#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_ADDR                                           (MMSS_MDP_REG_BASE      + 0x00000214)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_RMSK                                           0xf3ff0fff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_ADDR, HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_RMSK)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_ADDR,v)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_ADDR,m,v,HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_IN)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_RGB_MASK_BMSK                                  0xf0000000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_RGB_MASK_SHFT                                        0x1c
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_INDEX_UPDATE_BMSK                               0x2000000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_INDEX_UPDATE_SHFT                                    0x19
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_VALUE_UPDATEN_BMSK                              0x1000000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_VALUE_UPDATEN_SHFT                                   0x18
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_INDEX_BMSK                                       0xff0000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_INDEX_SHFT                                           0x10
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_VALUE_BMSK                                          0xfff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_RAM_LUTN_VALUE_SHFT                                            0x0

#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_ADDR                                           (MMSS_MDP_REG_BASE      + 0x00000218)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_RMSK                                           0xf3ff0fff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_ADDR, HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_RMSK)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_ADDR,v)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_ADDR,m,v,HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_IN)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_RGB_MASK_BMSK                                  0xf0000000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_RGB_MASK_SHFT                                        0x1c
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_INDEX_UPDATE_BMSK                               0x2000000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_INDEX_UPDATE_SHFT                                    0x19
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_VALUE_UPDATEN_BMSK                              0x1000000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_VALUE_UPDATEN_SHFT                                   0x18
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_INDEX_BMSK                                       0xff0000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_INDEX_SHFT                                           0x10
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_VALUE_BMSK                                          0xfff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_RAM_LUTN_VALUE_SHFT                                            0x0

#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_ADDR                                           (MMSS_MDP_REG_BASE      + 0x00000220)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_RMSK                                           0x33ff0fff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_ADDR, HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_RMSK)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_ADDR,v)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_ADDR,m,v,HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_IN)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_DMA_MASK_BMSK                                  0x30000000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_DMA_MASK_SHFT                                        0x1c
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_INDEX_UPDATE_BMSK                               0x2000000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_INDEX_UPDATE_SHFT                                    0x19
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_VALUE_UPDATEN_BMSK                              0x1000000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_VALUE_UPDATEN_SHFT                                   0x18
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_INDEX_BMSK                                       0xff0000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_INDEX_SHFT                                           0x10
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_VALUE_BMSK                                          0xfff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_RAM_LUTN_VALUE_SHFT                                            0x0

#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_ADDR                                           (MMSS_MDP_REG_BASE      + 0x00000224)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_RMSK                                           0x33ff0fff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_ADDR, HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_RMSK)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_ADDR,v)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_ADDR,m,v,HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_IN)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_DMA_MASK_BMSK                                  0x30000000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_DMA_MASK_SHFT                                        0x1c
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_INDEX_UPDATE_BMSK                               0x2000000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_INDEX_UPDATE_SHFT                                    0x19
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_VALUE_UPDATEN_BMSK                              0x1000000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_VALUE_UPDATEN_SHFT                                   0x18
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_INDEX_BMSK                                       0xff0000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_INDEX_SHFT                                           0x10
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_VALUE_BMSK                                          0xfff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_RAM_LUTN_VALUE_SHFT                                            0x0

#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_ADDR                                           (MMSS_MDP_REG_BASE      + 0x00000228)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_RMSK                                           0x33ff0fff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_ADDR, HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_RMSK)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_ADDR,v)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_ADDR,m,v,HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_IN)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_DMA_MASK_BMSK                                  0x30000000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_DMA_MASK_SHFT                                        0x1c
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_INDEX_UPDATE_BMSK                               0x2000000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_INDEX_UPDATE_SHFT                                    0x19
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_VALUE_UPDATEN_BMSK                              0x1000000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_VALUE_UPDATEN_SHFT                                   0x18
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_INDEX_BMSK                                       0xff0000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_INDEX_SHFT                                           0x10
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_VALUE_BMSK                                          0xfff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_RAM_LUTN_VALUE_SHFT                                            0x0

#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM0_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000230)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM0_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM0_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM0_LUTN_ADDR, HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM0_LUTN_RMSK)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM0_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM0_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM0_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM0_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM0_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM0_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM0_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000234)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM0_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM0_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM0_LUTN_ADDR, HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM0_LUTN_RMSK)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM0_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM0_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM0_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM0_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM0_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM0_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM0_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000238)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM0_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM0_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM0_LUTN_ADDR, HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM0_LUTN_RMSK)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM0_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM0_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM0_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM0_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM0_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM0_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM0_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000240)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM0_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM0_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM0_LUTN_ADDR, HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM0_LUTN_RMSK)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM0_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM0_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM0_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM0_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM0_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM0_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM0_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000244)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM0_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM0_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM0_LUTN_ADDR, HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM0_LUTN_RMSK)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM0_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM0_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM0_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM0_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM0_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM0_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM0_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000248)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM0_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM0_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM0_LUTN_ADDR, HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM0_LUTN_RMSK)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM0_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM0_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM0_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM0_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM0_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM0_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM0_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000250)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM0_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM0_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM0_LUTN_ADDR, HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM0_LUTN_RMSK)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM0_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM0_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM0_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM0_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM0_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM0_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM0_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000254)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM0_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM0_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM0_LUTN_ADDR, HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM0_LUTN_RMSK)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM0_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM0_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM0_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM0_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM0_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM0_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM0_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000258)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM0_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM0_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM0_LUTN_ADDR, HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM0_LUTN_RMSK)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM0_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM0_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM0_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM0_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM0_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM0_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM1_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000260)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM1_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM1_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM1_LUTN_ADDR, HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM1_LUTN_RMSK)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM1_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM1_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM1_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM1_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM1_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR0_ROM1_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM1_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000264)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM1_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM1_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM1_LUTN_ADDR, HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM1_LUTN_RMSK)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM1_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM1_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM1_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM1_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM1_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR1_ROM1_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM1_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000268)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM1_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM1_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM1_LUTN_ADDR, HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM1_LUTN_RMSK)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM1_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM1_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM1_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM1_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM1_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_VIG_IGC_COLOR2_ROM1_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM1_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000270)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM1_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM1_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM1_LUTN_ADDR, HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM1_LUTN_RMSK)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM1_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM1_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM1_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM1_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM1_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR0_ROM1_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM1_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000274)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM1_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM1_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM1_LUTN_ADDR, HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM1_LUTN_RMSK)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM1_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM1_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM1_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM1_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM1_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR1_ROM1_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM1_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000278)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM1_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM1_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM1_LUTN_ADDR, HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM1_LUTN_RMSK)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM1_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM1_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM1_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM1_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM1_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_RGB_IGC_COLOR2_ROM1_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM1_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000280)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM1_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM1_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM1_LUTN_ADDR, HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM1_LUTN_RMSK)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM1_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM1_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM1_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM1_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM1_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR0_ROM1_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM1_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000284)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM1_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM1_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM1_LUTN_ADDR, HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM1_LUTN_RMSK)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM1_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM1_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM1_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM1_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM1_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR1_ROM1_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM1_LUTN_ADDR                                          (MMSS_MDP_REG_BASE      + 0x00000288)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM1_LUTN_RMSK                                            0xff0fff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM1_LUTN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM1_LUTN_ADDR, HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM1_LUTN_RMSK)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM1_LUTN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM1_LUTN_ADDR, m)
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM1_LUTN_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM1_LUTN_INDEX_SHFT                                          0x10
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM1_LUTN_VALUE_BMSK                                         0xfff
#define HWIO_MMSS_MDP_DMA_IGC_COLOR2_ROM1_LUTN_VALUE_SHFT                                           0x0

#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_ADDR                                                (MMSS_MDP_REG_BASE      + 0x00000330)
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_RMSK                                                       0xfffff0
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_IN          \
        in_dword_masked(HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_ADDR, HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_RMSK)
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_ADDR, m)
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_ADDR,v)
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_ADDR,m,v,HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_IN)
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_VSYNC_INVERT_3_BMSK                                        0x800000
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_VSYNC_INVERT_3_SHFT                                            0x17
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_PING_PONG_3_BMSK                                           0x780000
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_PING_PONG_3_SHFT                                               0x13
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_VSYNC_INVERT_0_BMSK                                         0x40000
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_VSYNC_INVERT_0_SHFT                                            0x12
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_VSYNC_INVERT_1_BMSK                                         0x20000
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_VSYNC_INVERT_1_SHFT                                            0x11
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_VSYNC_INVERT_2_BMSK                                         0x10000
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_VSYNC_INVERT_2_SHFT                                            0x10
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_PING_PONG_0_BMSK                                             0xf000
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_PING_PONG_0_SHFT                                                0xc
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_PING_PONG_1_BMSK                                              0xf00
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_PING_PONG_1_SHFT                                                0x8
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_PING_PONG_2_BMSK                                               0xf0
#define HWIO_MMSS_MDP_MDP_VSYNC_SEL_1_5_0_PING_PONG_2_SHFT                                                0x4

#define HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_ADDR                                                  (MMSS_MDP_REG_BASE      + 0x00000404)
#define HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_RMSK                                                  0xffffffff
#define HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_IN          \
        in_dword_masked(HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_ADDR, HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_RMSK)
#define HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_ADDR, m)
#define HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_ADDR,v)
#define HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_ADDR,m,v,HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_IN)
#define HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_MDP_DSPP_SPARE0_BMSK                                  0xffffffff
#define HWIO_MMSS_MDP_MDP_DSPP_SPARE_0_1_5_0_MDP_DSPP_SPARE0_SHFT                                         0x0



/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_VIG_0
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_ADDR                                      (MMSS_MDP_VP_0_VIG_0_REG_BASE      + 0x000002e0)
#define HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_ADDR, HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_RMSK)
#define HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_ADDR,m,v,HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_IN)
#define HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_INDEX_BMSK                                0xff000000
#define HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_INDEX_SHFT                                      0x18
#define HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_VALUE_BMSK                                  0xffffff
#define HWIO_MMSS_MDP_VP_0_VIG_0_HIST_V_DATAN_VALUE_SHFT                                       0x0

#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_ADDR                                           (MMSS_MDP_VP_0_VIG_0_REG_BASE      + 0x000002f0)
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_RMSK                                              0x3ffff
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_ADDR, HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_RMSK)
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_ADDR,m,v,HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_IN)
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_VALUE_UPDATEN_BMSK                                0x20000
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_VALUE_UPDATEN_SHFT                                   0x11
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_INDEX_UPDATE_BMSK                                 0x10000
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_INDEX_UPDATE_SHFT                                    0x10
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_INDEX_BMSK                                         0xff00
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_INDEX_SHFT                                            0x8
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_VALUE_BMSK                                           0xff
#define HWIO_MMSS_MDP_VP_0_VIG_0_PA_LUTV_VALUE_SHFT                                            0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_VIG_1
 *--------------------------------------------------------------------------*/


#define HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_ADDR                                      (MMSS_MDP_VP_0_VIG_1_REG_BASE      + 0x000002e0)
#define HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_ADDR, HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_RMSK)
#define HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_ADDR,m,v,HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_IN)
#define HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_INDEX_BMSK                                0xff000000
#define HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_INDEX_SHFT                                      0x18
#define HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_VALUE_BMSK                                  0xffffff
#define HWIO_MMSS_MDP_VP_0_VIG_1_HIST_V_DATAN_VALUE_SHFT                                       0x0

#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_ADDR                                           (MMSS_MDP_VP_0_VIG_1_REG_BASE      + 0x000002f0)
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_RMSK                                              0x3ffff
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_ADDR, HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_RMSK)
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_ADDR,m,v,HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_IN)
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_VALUE_UPDATEN_BMSK                                0x20000
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_VALUE_UPDATEN_SHFT                                   0x11
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_INDEX_UPDATE_BMSK                                 0x10000
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_INDEX_UPDATE_SHFT                                    0x10
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_INDEX_BMSK                                         0xff00
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_INDEX_SHFT                                            0x8
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_VALUE_BMSK                                           0xff
#define HWIO_MMSS_MDP_VP_0_VIG_1_PA_LUTV_VALUE_SHFT                                            0x0


/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_VIG_2
 *--------------------------------------------------------------------------*/


#define HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_ADDR                                      (MMSS_MDP_VP_0_VIG_2_REG_BASE      + 0x000002e0)
#define HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_ADDR, HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_RMSK)
#define HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_ADDR,m,v,HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_IN)
#define HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_INDEX_BMSK                                0xff000000
#define HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_INDEX_SHFT                                      0x18
#define HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_VALUE_BMSK                                  0xffffff
#define HWIO_MMSS_MDP_VP_0_VIG_2_HIST_V_DATAN_VALUE_SHFT                                       0x0

#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_ADDR                                           (MMSS_MDP_VP_0_VIG_2_REG_BASE      + 0x000002f0)
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_RMSK                                              0x3ffff
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_ADDR, HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_RMSK)
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_ADDR,m,v,HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_IN)
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_VALUE_UPDATEN_BMSK                                0x20000
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_VALUE_UPDATEN_SHFT                                   0x11
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_INDEX_UPDATE_BMSK                                 0x10000
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_INDEX_UPDATE_SHFT                                    0x10
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_INDEX_BMSK                                         0xff00
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_INDEX_SHFT                                            0x8
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_VALUE_BMSK                                           0xff
#define HWIO_MMSS_MDP_VP_0_VIG_2_PA_LUTV_VALUE_SHFT                                            0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_VIG_3
 *--------------------------------------------------------------------------*/


#define HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_ADDR                                      (MMSS_MDP_VP_0_VIG_3_REG_BASE      + 0x000002e0)
#define HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_ADDR, HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_RMSK)
#define HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_ADDR,m,v,HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_IN)
#define HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_INDEX_BMSK                                0xff000000
#define HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_INDEX_SHFT                                      0x18
#define HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_VALUE_BMSK                                  0xffffff
#define HWIO_MMSS_MDP_VP_0_VIG_3_HIST_V_DATAN_VALUE_SHFT                                       0x0

#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_ADDR                                           (MMSS_MDP_VP_0_VIG_3_REG_BASE      + 0x000002f0)
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_RMSK                                              0x3ffff
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_ADDR, HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_RMSK)
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_ADDR,m,v,HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_IN)
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_VALUE_UPDATEN_BMSK                                0x20000
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_VALUE_UPDATEN_SHFT                                   0x11
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_INDEX_UPDATE_BMSK                                 0x10000
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_INDEX_UPDATE_SHFT                                    0x10
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_INDEX_BMSK                                         0xff00
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_INDEX_SHFT                                            0x8
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_VALUE_BMSK                                           0xff
#define HWIO_MMSS_MDP_VP_0_VIG_3_PA_LUTV_VALUE_SHFT                                            0x0


/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_LAYER_0
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_LAYER_0_OP_MODE_GC_LUT_EN_BMSK                                             0x1
#define HWIO_MMSS_MDP_VP_0_LAYER_0_OP_MODE_GC_LUT_EN_SHFT                                             0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_0_REG_BASE      + 0x00000110)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_RMSK                                    0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_STARTX_BMSK                                0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_STARTX_N_STARTX_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_ADDR                                   (MMSS_MDP_VP_0_LAYER_0_REG_BASE      + 0x00000114)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_RMSK                                     0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_BMSK                       0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_SHFT                           0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_BMSK                      0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_SHFT                          0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_SEGMENT_BMSK                              0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_SEGMENT_SHFT                                 0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_SLOPE_BMSK                                 0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_SLOPE_N_SLOPE_SHFT                                    0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_0_REG_BASE      + 0x00000118)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_OFFSET_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_RED_OFFSET_N_OFFSET_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_ADDR                                (MMSS_MDP_VP_0_LAYER_0_REG_BASE      + 0x00000120)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_RMSK                                  0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_STARTX_BMSK                              0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_STARTX_N_STARTX_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_0_REG_BASE      + 0x00000124)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_SLOPE_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_SLOPE_N_SLOPE_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_ADDR                                (MMSS_MDP_VP_0_LAYER_0_REG_BASE      + 0x00000128)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_RMSK                                  0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_OFFSET_BMSK                             0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_GREEN_OFFSET_N_OFFSET_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_0_REG_BASE      + 0x00000130)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_RMSK                                   0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_STARTX_BMSK                               0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_STARTX_N_STARTX_SHFT                                 0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_0_REG_BASE      + 0x00000134)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_SLOPE_BMSK                                0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_SLOPE_N_SLOPE_SHFT                                   0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_0_REG_BASE      + 0x00000138)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_OFFSET_BMSK                              0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_0_GC_ARLUT_BLUE_OFFSET_N_OFFSET_SHFT                                 0x

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_LAYER_1
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_LAYER_1_OP_MODE_GC_LUT_EN_BMSK                                             0x1
#define HWIO_MMSS_MDP_VP_0_LAYER_1_OP_MODE_GC_LUT_EN_SHFT                                             0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_1_REG_BASE      + 0x00000110)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_RMSK                                    0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_STARTX_BMSK                                0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_STARTX_N_STARTX_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_ADDR                                   (MMSS_MDP_VP_0_LAYER_1_REG_BASE      + 0x00000114)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_RMSK                                     0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_BMSK                       0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_SHFT                           0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_BMSK                      0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_SHFT                          0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_SEGMENT_BMSK                              0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_SEGMENT_SHFT                                 0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_SLOPE_BMSK                                 0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_SLOPE_N_SLOPE_SHFT                                    0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_1_REG_BASE      + 0x00000118)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_OFFSET_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_RED_OFFSET_N_OFFSET_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_ADDR                                (MMSS_MDP_VP_0_LAYER_1_REG_BASE      + 0x00000120)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_RMSK                                  0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_STARTX_BMSK                              0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_STARTX_N_STARTX_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_1_REG_BASE      + 0x00000124)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_SLOPE_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_SLOPE_N_SLOPE_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_ADDR                                (MMSS_MDP_VP_0_LAYER_1_REG_BASE      + 0x00000128)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_RMSK                                  0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_OFFSET_BMSK                             0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_GREEN_OFFSET_N_OFFSET_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_1_REG_BASE      + 0x00000130)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_RMSK                                   0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_STARTX_BMSK                               0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_STARTX_N_STARTX_SHFT                                 0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_1_REG_BASE      + 0x00000134)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_SLOPE_BMSK                                0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_SLOPE_N_SLOPE_SHFT                                   0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_1_REG_BASE      + 0x00000138)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_OFFSET_BMSK                              0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_1_GC_ARLUT_BLUE_OFFSET_N_OFFSET_SHFT                                 0x0


/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_LAYER_2
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_LAYER_2_OP_MODE_GC_LUT_EN_BMSK                                             0x1
#define HWIO_MMSS_MDP_VP_0_LAYER_2_OP_MODE_GC_LUT_EN_SHFT                                             0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_2_REG_BASE      + 0x00000110)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_RMSK                                    0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_STARTX_BMSK                                0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_STARTX_N_STARTX_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_ADDR                                   (MMSS_MDP_VP_0_LAYER_2_REG_BASE      + 0x00000114)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_RMSK                                     0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_BMSK                       0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_SHFT                           0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_BMSK                      0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_SHFT                          0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_SEGMENT_BMSK                              0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_SEGMENT_SHFT                                 0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_SLOPE_BMSK                                 0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_SLOPE_N_SLOPE_SHFT                                    0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_2_REG_BASE      + 0x00000118)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_OFFSET_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_RED_OFFSET_N_OFFSET_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_ADDR                                (MMSS_MDP_VP_0_LAYER_2_REG_BASE      + 0x00000120)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_RMSK                                  0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_STARTX_BMSK                              0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_STARTX_N_STARTX_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_2_REG_BASE      + 0x00000124)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_SLOPE_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_SLOPE_N_SLOPE_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_ADDR                                (MMSS_MDP_VP_0_LAYER_2_REG_BASE      + 0x00000128)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_RMSK                                  0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_OFFSET_BMSK                             0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_GREEN_OFFSET_N_OFFSET_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_2_REG_BASE      + 0x00000130)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_RMSK                                   0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_STARTX_BMSK                               0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_STARTX_N_STARTX_SHFT                                 0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_2_REG_BASE      + 0x00000134)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_SLOPE_BMSK                                0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_SLOPE_N_SLOPE_SHFT                                   0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_2_REG_BASE      + 0x00000138)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_OFFSET_BMSK                              0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_2_GC_ARLUT_BLUE_OFFSET_N_OFFSET_SHFT                                 0x



/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_LAYER_3
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_LAYER_3_OP_MODE_GC_LUT_EN_BMSK                                             0x1
#define HWIO_MMSS_MDP_VP_0_LAYER_3_OP_MODE_GC_LUT_EN_SHFT                                             0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_3_REG_BASE      + 0x00000110)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_RMSK                                    0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_STARTX_BMSK                                0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_STARTX_N_STARTX_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_ADDR                                   (MMSS_MDP_VP_0_LAYER_3_REG_BASE      + 0x00000114)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_RMSK                                     0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_BMSK                       0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_SHFT                           0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_BMSK                      0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_SHFT                          0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_SEGMENT_BMSK                              0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_SEGMENT_SHFT                                 0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_SLOPE_BMSK                                 0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_SLOPE_N_SLOPE_SHFT                                    0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_3_REG_BASE      + 0x00000118)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_OFFSET_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_RED_OFFSET_N_OFFSET_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_ADDR                                (MMSS_MDP_VP_0_LAYER_3_REG_BASE      + 0x00000120)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_RMSK                                  0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_STARTX_BMSK                              0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_STARTX_N_STARTX_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_3_REG_BASE      + 0x00000124)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_SLOPE_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_SLOPE_N_SLOPE_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_ADDR                                (MMSS_MDP_VP_0_LAYER_3_REG_BASE      + 0x00000128)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_RMSK                                  0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_OFFSET_BMSK                             0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_GREEN_OFFSET_N_OFFSET_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_3_REG_BASE      + 0x00000130)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_RMSK                                   0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_STARTX_BMSK                               0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_STARTX_N_STARTX_SHFT                                 0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_3_REG_BASE      + 0x00000134)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_SLOPE_BMSK                                0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_SLOPE_N_SLOPE_SHFT                                   0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_3_REG_BASE      + 0x00000138)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_OFFSET_BMSK                              0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_3_GC_ARLUT_BLUE_OFFSET_N_OFFSET_SHFT                                 0x


/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_LAYER_4
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_LAYER_4_OP_MODE_GC_LUT_EN_BMSK                                             0x1
#define HWIO_MMSS_MDP_VP_0_LAYER_4_OP_MODE_GC_LUT_EN_SHFT                                             0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_4_REG_BASE      + 0x00000110)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_RMSK                                    0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_STARTX_BMSK                                0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_STARTX_N_STARTX_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_ADDR                                   (MMSS_MDP_VP_0_LAYER_4_REG_BASE      + 0x00000114)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_RMSK                                     0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_BMSK                       0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_SHFT                           0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_BMSK                      0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_SHFT                          0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_SEGMENT_BMSK                              0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_SEGMENT_SHFT                                 0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_SLOPE_BMSK                                 0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_SLOPE_N_SLOPE_SHFT                                    0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_4_REG_BASE      + 0x00000118)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_OFFSET_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_RED_OFFSET_N_OFFSET_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_ADDR                                (MMSS_MDP_VP_0_LAYER_4_REG_BASE      + 0x00000120)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_RMSK                                  0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_STARTX_BMSK                              0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_STARTX_N_STARTX_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_4_REG_BASE      + 0x00000124)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_SLOPE_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_SLOPE_N_SLOPE_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_ADDR                                (MMSS_MDP_VP_0_LAYER_4_REG_BASE      + 0x00000128)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_RMSK                                  0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_OFFSET_BMSK                             0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_GREEN_OFFSET_N_OFFSET_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_4_REG_BASE      + 0x00000130)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_RMSK                                   0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_STARTX_BMSK                               0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_STARTX_N_STARTX_SHFT                                 0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_4_REG_BASE      + 0x00000134)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_SLOPE_BMSK                                0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_SLOPE_N_SLOPE_SHFT                                   0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_4_REG_BASE      + 0x00000138)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_OFFSET_BMSK                              0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_4_GC_ARLUT_BLUE_OFFSET_N_OFFSET_SHFT                                 0x



/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_LAYER_5
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_LAYER_5_OP_MODE_GC_LUT_EN_BMSK                                             0x1
#define HWIO_MMSS_MDP_VP_0_LAYER_5_OP_MODE_GC_LUT_EN_SHFT                                             0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_5_REG_BASE      + 0x00000110)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_RMSK                                    0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_STARTX_BMSK                                0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_STARTX_N_STARTX_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_ADDR                                   (MMSS_MDP_VP_0_LAYER_5_REG_BASE      + 0x00000114)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_RMSK                                     0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_BMSK                       0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_SHFT                           0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_BMSK                      0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_SHFT                          0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_SEGMENT_BMSK                              0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_SEGMENT_SHFT                                 0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_SLOPE_BMSK                                 0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_SLOPE_N_SLOPE_SHFT                                    0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_5_REG_BASE      + 0x00000118)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_OFFSET_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_RED_OFFSET_N_OFFSET_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_ADDR                                (MMSS_MDP_VP_0_LAYER_5_REG_BASE      + 0x00000120)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_RMSK                                  0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_STARTX_BMSK                              0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_STARTX_N_STARTX_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_5_REG_BASE      + 0x00000124)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_SLOPE_BMSK                               0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_SLOPE_N_SLOPE_SHFT                                  0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_ADDR                                (MMSS_MDP_VP_0_LAYER_5_REG_BASE      + 0x00000128)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_RMSK                                  0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_BMSK                    0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_SHFT                        0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_BMSK                   0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_SHFT                       0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_BMSK                           0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_SHFT                              0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_OFFSET_BMSK                             0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_GREEN_OFFSET_N_OFFSET_SHFT                                0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_5_REG_BASE      + 0x00000130)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_RMSK                                   0x3f0fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_STARTX_BMSK                               0xfff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_STARTX_N_STARTX_SHFT                                 0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_ADDR                                  (MMSS_MDP_VP_0_LAYER_5_REG_BASE      + 0x00000134)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_RMSK                                    0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_BMSK                      0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_SHFT                          0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_BMSK                     0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_SHFT                         0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_BMSK                             0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_SLOPE_BMSK                                0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_SLOPE_N_SLOPE_SHFT                                   0x0

#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_ADDR                                 (MMSS_MDP_VP_0_LAYER_5_REG_BASE      + 0x00000138)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_RMSK                                   0x3f7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_BMSK                     0x200000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_SHFT                         0x15
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_BMSK                    0x100000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_SHFT                        0x14
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_BMSK                            0xf0000
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_SHFT                               0x10
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_OFFSET_BMSK                              0x7fff
#define HWIO_MMSS_MDP_VP_0_LAYER_5_GC_ARLUT_BLUE_OFFSET_N_OFFSET_SHFT                                 0x

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_DSPP_0
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_GAMUT_PCC_ORDER_BMSK                                                   0x1000000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_GAMUT_PCC_ORDER_SHFT                                                        0x18
#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_GAMUT_EN_BMSK                                                           0x800000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_GAMUT_EN_SHFT                                                               0x17
#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_GC_LUT_EN_BMSK                                                          0x400000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_GC_LUT_EN_SHFT                                                              0x16
#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_PCC_EN_BMSK                                                                 0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_PCC_EN_SHFT                                                                  0x4

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_ADDR(c)                    (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000030 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_RMSK                           0xffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_MAXc                                2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_COEFF_BMSK                     0xffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_CONSTANT_COEFF_COEFF_SHFT                        0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_ADDR(c)                           (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000040 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_RMSK                                 0x3ffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_MAXc                                       2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_COEFF_BMSK                           0x3ffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_R_COEFF_COEFF_SHFT                               0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_ADDR(c)                           (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000050 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_RMSK                                 0x3ffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_MAXc                                       2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_COEFF_BMSK                           0x3ffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_G_COEFF_COEFF_SHFT                               0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_ADDR(c)                           (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000060 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_RMSK                                 0x3ffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_MAXc                                       2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_COEFF_BMSK                           0x3ffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_B_COEFF_COEFF_SHFT                               0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000070 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RR_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000080 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RG_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000090 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RB_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000000a0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_GG_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000000b0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BG_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000000c0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_BB_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_ADDR(c)                       (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000000d0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_RMSK                                0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_MAXc                                   2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_COEFF_BMSK                          0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_0_COEFF_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_ADDR(c)                       (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000000e0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_RMSK                          0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_MAXc                                   2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_COEFF_BMSK                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PCC_PLANEc_RGB_COEFF_1_COEFF_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_ADDR                                    (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x0000022c)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_RMSK                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_INDEX_BMSK                              0xff000000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_INDEX_SHFT                                    0x18
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_VALUE_BMSK                                0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_VALUE_SHFT                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_ADDR                                         (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000230)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_RMSK                                            0x3ffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_VALUE_UPDATEN_BMSK                              0x20000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_VALUE_UPDATEN_SHFT                                 0x11
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_INDEX_UPDATE_BMSK                               0x10000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_INDEX_UPDATE_SHFT                                  0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_INDEX_BMSK                                       0xff00
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_INDEX_SHFT                                          0x8
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_VALUE_BMSK                                         0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_PA_LUTV_VALUE_SHFT                                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_ADDR                           (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002b0)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_RMSK                             0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_STARTX_BMSK                         0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_STARTX_N_STARTX_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_ADDR                            (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002b4)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_RMSK                              0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_BMSK                0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_SHFT                    0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_BMSK               0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_SHFT                   0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_SEGMENT_BMSK                       0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_SEGMENT_SHFT                          0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_SLOPE_BMSK                          0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_SLOPE_N_SLOPE_SHFT                             0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_ADDR                           (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002b8)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_RMSK                             0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_OFFSET_BMSK                        0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_RED_OFFSET_N_OFFSET_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_ADDR                         (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002c0)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_RMSK                           0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_BMSK             0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_SHFT                 0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_BMSK            0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_SHFT                0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_SEGMENT_BMSK                    0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_SEGMENT_SHFT                       0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_STARTX_BMSK                       0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_STARTX_N_STARTX_SHFT                         0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_ADDR                          (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002c4)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_RMSK                            0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_SLOPE_BMSK                        0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_SLOPE_N_SLOPE_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_ADDR                         (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002c8)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_RMSK                           0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_BMSK             0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_SHFT                 0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_BMSK            0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_SHFT                0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_BMSK                    0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_SHFT                       0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_OFFSET_BMSK                      0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_GREEN_OFFSET_N_OFFSET_SHFT                         0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_ADDR                          (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002d0)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_RMSK                            0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_STARTX_BMSK                        0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_STARTX_N_STARTX_SHFT                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_ADDR                           (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002d4)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_RMSK                             0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_SLOPE_BMSK                         0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_SLOPE_N_SLOPE_SHFT                            0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_ADDR                          (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002d8)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_RMSK                            0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_OFFSET_BMSK                       0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GC_ARLUT_BLUE_OFFSET_N_OFFSET_SHFT                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_BIN_i_ADDR(i)                           (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00001000 + 0x4 * (i))
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_BIN_i_RMSK                                0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_BIN_i_MAXi                                     255
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_BIN_i_INI(i)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_BIN_i_ADDR(i), HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_BIN_i_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_BIN_i_INMI(i,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_BIN_i_ADDR(i), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_BIN_i_VALUE_BMSK                          0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_V_DATAN_BIN_i_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002dc)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_0_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002e0)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_1_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002e4)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_2_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002e8)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_3_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002ec)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_4_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002f0)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_5_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002f4)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_6_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002f8)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_RED_TABLE_7_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x000002fc)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_0_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000300)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_1_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000304)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_2_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000308)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_3_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x0000030c)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_4_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000310)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_5_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000314)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_6_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000318)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_GREEN_TABLE_7_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x0000031c)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_0_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000320)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_1_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000324)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_2_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000328)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_3_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x0000032c)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_4_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000330)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_5_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000334)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_6_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000338)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_GAMUT_BLUE_TABLE_7_N_ENTRY_SHFT                                                      0x0


/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_DSPP_1
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_DSPP_1_OP_MODE_GAMUT_PCC_ORDER_BMSK                                                   0x1000000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_OP_MODE_GAMUT_PCC_ORDER_SHFT                                                        0x18
#define HWIO_MMSS_MDP_VP_0_DSPP_1_OP_MODE_GAMUT_EN_BMSK                                                           0x800000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_OP_MODE_GAMUT_EN_SHFT                                                               0x17
#define HWIO_MMSS_MDP_VP_0_DSPP_1_OP_MODE_GC_LUT_EN_BMSK                                                          0x400000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_OP_MODE_GC_LUT_EN_SHFT                                                              0x16
#define HWIO_MMSS_MDP_VP_0_DSPP_1_OP_MODE_PCC_EN_BMSK                                                                 0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_1_OP_MODE_PCC_EN_SHFT                                                                  0x4

#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000070 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RR_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000000a0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_GG_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000000c0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_BB_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_ADDR(c)                       (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000000d0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_RMSK                                0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_MAXc                                   2
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_COEFF_BMSK                          0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_0_COEFF_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_ADDR(c)                       (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000000e0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_RMSK                          0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_MAXc                                   2
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_COEFF_BMSK                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PCC_PLANEc_RGB_COEFF_1_COEFF_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_ADDR                                    (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x0000022c)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_RMSK                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_INDEX_BMSK                              0xff000000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_INDEX_SHFT                                    0x18
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_VALUE_BMSK                                0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_VALUE_SHFT                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_ADDR                                         (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000230)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_RMSK                                            0x3ffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_VALUE_UPDATEN_BMSK                              0x20000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_VALUE_UPDATEN_SHFT                                 0x11
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_INDEX_UPDATE_BMSK                               0x10000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_INDEX_UPDATE_SHFT                                  0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_INDEX_BMSK                                       0xff00
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_INDEX_SHFT                                          0x8
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_VALUE_BMSK                                         0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_PA_LUTV_VALUE_SHFT                                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_ADDR                           (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002b0)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_RMSK                             0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_STARTX_BMSK                         0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_STARTX_N_STARTX_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_ADDR                            (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002b4)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_RMSK                              0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_BMSK                0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_SHFT                    0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_BMSK               0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_SHFT                   0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_SEGMENT_BMSK                       0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_SEGMENT_SHFT                          0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_SLOPE_BMSK                          0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_SLOPE_N_SLOPE_SHFT                             0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_ADDR                           (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002b8)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_RMSK                             0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_OFFSET_BMSK                        0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_RED_OFFSET_N_OFFSET_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_ADDR                         (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002c0)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_RMSK                           0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_BMSK             0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_SHFT                 0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_BMSK            0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_SHFT                0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_SEGMENT_BMSK                    0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_SEGMENT_SHFT                       0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_STARTX_BMSK                       0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_STARTX_N_STARTX_SHFT                         0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_ADDR                          (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002c4)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_RMSK                            0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_SLOPE_BMSK                        0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_SLOPE_N_SLOPE_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_ADDR                         (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002c8)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_RMSK                           0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_BMSK             0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_SHFT                 0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_BMSK            0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_SHFT                0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_BMSK                    0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_SHFT                       0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_OFFSET_BMSK                      0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_GREEN_OFFSET_N_OFFSET_SHFT                         0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_ADDR                          (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002d0)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_RMSK                            0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_STARTX_BMSK                        0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_STARTX_N_STARTX_SHFT                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_ADDR                           (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002d4)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_RMSK                             0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_SLOPE_BMSK                         0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_SLOPE_N_SLOPE_SHFT                            0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_ADDR                          (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002d8)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_RMSK                            0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_OFFSET_BMSK                       0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GC_ARLUT_BLUE_OFFSET_N_OFFSET_SHFT                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_BIN_i_ADDR(i)                           (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00001000 + 0x4 * (i))
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_BIN_i_RMSK                                0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_BIN_i_MAXi                                     255
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_BIN_i_INI(i)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_BIN_i_ADDR(i), HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_BIN_i_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_BIN_i_INMI(i,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_BIN_i_ADDR(i), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_BIN_i_VALUE_BMSK                          0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_HIST_V_DATAN_BIN_i_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002dc)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_0_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002e0)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_1_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002e4)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_2_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002e8)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_3_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002ec)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_4_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002f0)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_5_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002f4)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_6_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002f8)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_RED_TABLE_7_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x000002fc)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_0_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000300)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_1_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000304)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_2_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000308)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_3_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x0000030c)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_4_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000310)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_5_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000314)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_6_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000318)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_GREEN_TABLE_7_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x0000031c)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_0_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000320)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_1_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000324)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_2_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000328)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_3_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x0000032c)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_4_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000330)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_5_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000334)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_6_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_1_REG_BASE      + 0x00000338)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_1_GAMUT_BLUE_TABLE_7_N_ENTRY_SHFT                                                      0x0


/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_DSPP_2
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_DSPP_2_OP_MODE_GAMUT_PCC_ORDER_BMSK                                                   0x1000000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_OP_MODE_GAMUT_PCC_ORDER_SHFT                                                        0x18
#define HWIO_MMSS_MDP_VP_0_DSPP_2_OP_MODE_GAMUT_EN_BMSK                                                           0x800000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_OP_MODE_GAMUT_EN_SHFT                                                               0x17
#define HWIO_MMSS_MDP_VP_0_DSPP_2_OP_MODE_GC_LUT_EN_BMSK                                                          0x400000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_OP_MODE_GC_LUT_EN_SHFT                                                              0x16
#define HWIO_MMSS_MDP_VP_0_DSPP_2_OP_MODE_PCC_EN_BMSK                                                                 0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_2_OP_MODE_PCC_EN_SHFT                                                                  0x4

#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000070 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RR_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000000a0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_GG_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000000c0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_BB_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_ADDR(c)                       (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000000d0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_RMSK                                0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_MAXc                                   2
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_COEFF_BMSK                          0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_0_COEFF_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_ADDR(c)                       (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000000e0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_RMSK                          0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_MAXc                                   2
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_COEFF_BMSK                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PCC_PLANEc_RGB_COEFF_1_COEFF_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_ADDR                                    (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x0000022c)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_RMSK                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_INDEX_BMSK                              0xff000000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_INDEX_SHFT                                    0x18
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_VALUE_BMSK                                0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_VALUE_SHFT                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_ADDR                                         (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000230)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_RMSK                                            0x3ffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_VALUE_UPDATEN_BMSK                              0x20000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_VALUE_UPDATEN_SHFT                                 0x11
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_INDEX_UPDATE_BMSK                               0x10000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_INDEX_UPDATE_SHFT                                  0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_INDEX_BMSK                                       0xff00
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_INDEX_SHFT                                          0x8
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_VALUE_BMSK                                         0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_PA_LUTV_VALUE_SHFT                                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_ADDR                           (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002b0)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_RMSK                             0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_STARTX_BMSK                         0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_STARTX_N_STARTX_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_ADDR                            (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002b4)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_RMSK                              0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_BMSK                0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_SHFT                    0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_BMSK               0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_SHFT                   0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_SEGMENT_BMSK                       0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_SEGMENT_SHFT                          0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_SLOPE_BMSK                          0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_SLOPE_N_SLOPE_SHFT                             0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_ADDR                           (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002b8)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_RMSK                             0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_OFFSET_BMSK                        0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_RED_OFFSET_N_OFFSET_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_ADDR                         (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002c0)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_RMSK                           0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_BMSK             0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_SHFT                 0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_BMSK            0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_SHFT                0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_SEGMENT_BMSK                    0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_SEGMENT_SHFT                       0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_STARTX_BMSK                       0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_STARTX_N_STARTX_SHFT                         0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_ADDR                          (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002c4)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_RMSK                            0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_SLOPE_BMSK                        0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_SLOPE_N_SLOPE_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_ADDR                         (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002c8)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_RMSK                           0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_BMSK             0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_SHFT                 0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_BMSK            0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_SHFT                0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_BMSK                    0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_SHFT                       0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_OFFSET_BMSK                      0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_GREEN_OFFSET_N_OFFSET_SHFT                         0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_ADDR                          (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002d0)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_RMSK                            0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_STARTX_BMSK                        0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_STARTX_N_STARTX_SHFT                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_ADDR                           (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002d4)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_RMSK                             0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_SLOPE_BMSK                         0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_SLOPE_N_SLOPE_SHFT                            0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_ADDR                          (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002d8)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_RMSK                            0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_OFFSET_BMSK                       0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GC_ARLUT_BLUE_OFFSET_N_OFFSET_SHFT                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_BIN_i_ADDR(i)                           (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00001000 + 0x4 * (i))
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_BIN_i_RMSK                                0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_BIN_i_MAXi                                     255
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_BIN_i_INI(i)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_BIN_i_ADDR(i), HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_BIN_i_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_BIN_i_INMI(i,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_BIN_i_ADDR(i), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_BIN_i_VALUE_BMSK                          0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_HIST_V_DATAN_BIN_i_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002dc)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_0_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002e0)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_1_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002e4)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_2_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002e8)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_3_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002ec)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_4_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002f0)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_5_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002f4)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_6_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002f8)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_RED_TABLE_7_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x000002fc)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_0_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000300)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_1_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000304)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_2_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000308)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_3_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x0000030c)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_4_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000310)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_5_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000314)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_6_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000318)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_GREEN_TABLE_7_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x0000031c)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_0_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000320)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_1_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000324)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_2_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000328)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_3_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x0000032c)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_4_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000330)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_5_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000334)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_6_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_2_REG_BASE      + 0x00000338)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_2_GAMUT_BLUE_TABLE_7_N_ENTRY_SHFT                                                      0x0


/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_DSPP_3
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_DSPP_3_OP_MODE_GAMUT_PCC_ORDER_BMSK                                                   0x1000000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_OP_MODE_GAMUT_PCC_ORDER_SHFT                                                        0x18
#define HWIO_MMSS_MDP_VP_0_DSPP_3_OP_MODE_GAMUT_EN_BMSK                                                           0x800000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_OP_MODE_GAMUT_EN_SHFT                                                               0x17
#define HWIO_MMSS_MDP_VP_0_DSPP_3_OP_MODE_GC_LUT_EN_BMSK                                                          0x400000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_OP_MODE_GC_LUT_EN_SHFT                                                              0x16
#define HWIO_MMSS_MDP_VP_0_DSPP_3_OP_MODE_PCC_EN_BMSK                                                                 0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_3_OP_MODE_PCC_EN_SHFT                                                                  0x4

#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000070 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RR_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000000a0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_GG_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_ADDR(c)                          (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000000c0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_RMSK                              0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_MAXc                                      2
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_COEFF_BMSK                        0xfffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_BB_COEFF_COEFF_SHFT                              0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_ADDR(c)                       (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000000d0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_RMSK                                0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_MAXc                                   2
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_COEFF_BMSK                          0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_0_COEFF_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_ADDR(c)                       (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000000e0 + 0x4 * (c))
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_RMSK                          0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_MAXc                                   2
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_INI(c)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_ADDR(c), HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_INMI(c,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_ADDR(c), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_OUTI(c,val)    \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_ADDR(c),val)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_OUTMI(c,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_ADDR(c),mask,val,HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_INI(c))
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_COEFF_BMSK                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PCC_PLANEc_RGB_COEFF_1_COEFF_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_ADDR                                    (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x0000022c)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_RMSK                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_INDEX_BMSK                              0xff000000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_INDEX_SHFT                                    0x18
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_VALUE_BMSK                                0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_VALUE_SHFT                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_ADDR                                         (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000230)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_RMSK                                            0x3ffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_VALUE_UPDATEN_BMSK                              0x20000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_VALUE_UPDATEN_SHFT                                 0x11
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_INDEX_UPDATE_BMSK                               0x10000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_INDEX_UPDATE_SHFT                                  0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_INDEX_BMSK                                       0xff00
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_INDEX_SHFT                                          0x8
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_VALUE_BMSK                                         0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_PA_LUTV_VALUE_SHFT                                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_ADDR                           (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002b0)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_RMSK                             0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_STARTX_BMSK                         0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_STARTX_N_STARTX_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_ADDR                            (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002b4)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_RMSK                              0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_BMSK                0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_VALUE_UPDATEN_SHFT                    0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_BMSK               0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_SEGMENT_UPDATE_SHFT                   0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_SEGMENT_BMSK                       0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_SEGMENT_SHFT                          0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_SLOPE_BMSK                          0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_SLOPE_N_SLOPE_SHFT                             0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_ADDR                           (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002b8)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_RMSK                             0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_OFFSET_BMSK                        0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_RED_OFFSET_N_OFFSET_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_ADDR                         (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002c0)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_RMSK                           0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_BMSK             0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_VALUE_UPDATEN_SHFT                 0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_BMSK            0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_SEGMENT_UPDATE_SHFT                0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_SEGMENT_BMSK                    0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_SEGMENT_SHFT                       0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_STARTX_BMSK                       0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_STARTX_N_STARTX_SHFT                         0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_ADDR                          (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002c4)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_RMSK                            0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_SLOPE_BMSK                        0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_SLOPE_N_SLOPE_SHFT                           0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_ADDR                         (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002c8)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_RMSK                           0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_BMSK             0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_VALUE_UPDATEN_SHFT                 0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_BMSK            0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_UPDATE_SHFT                0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_BMSK                    0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_SEGMENT_SHFT                       0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_OFFSET_BMSK                      0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_GREEN_OFFSET_N_OFFSET_SHFT                         0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_ADDR                          (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002d0)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_RMSK                            0x3f0fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_STARTX_BMSK                        0xfff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_STARTX_N_STARTX_SHFT                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_ADDR                           (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002d4)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_RMSK                             0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_BMSK               0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_VALUE_UPDATEN_SHFT                   0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_BMSK              0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_UPDATE_SHFT                  0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_BMSK                      0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_SEGMENT_SHFT                         0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_SLOPE_BMSK                         0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_SLOPE_N_SLOPE_SHFT                            0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_ADDR                          (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002d8)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_RMSK                            0x3f7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_BMSK              0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_VALUE_UPDATEN_SHFT                  0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_BMSK             0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_UPDATE_SHFT                 0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_BMSK                     0xf0000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_SEGMENT_SHFT                        0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_OFFSET_BMSK                       0x7fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GC_ARLUT_BLUE_OFFSET_N_OFFSET_SHFT                          0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_BIN_i_ADDR(i)                           (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00001000 + 0x4 * (i))
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_BIN_i_RMSK                                0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_BIN_i_MAXi                                     255
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_BIN_i_INI(i)        \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_BIN_i_ADDR(i), HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_BIN_i_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_BIN_i_INMI(i,mask)    \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_BIN_i_ADDR(i), mask)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_BIN_i_VALUE_BMSK                          0xffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_HIST_V_DATAN_BIN_i_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002dc)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_0_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002e0)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_1_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002e4)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_2_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002e8)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_3_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002ec)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_4_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002f0)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_5_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002f4)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_6_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_ADDR                                                      (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002f8)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_RMSK                                                      0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_ENTRY_UPDATEN_BMSK                                          0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_ENTRY_UPDATEN_SHFT                                              0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_INDEX_UPDATE_BMSK                                           0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_INDEX_UPDATE_SHFT                                               0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_INDEX_BMSK                                                   0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_INDEX_SHFT                                                       0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_ENTRY_BMSK                                                    0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_RED_TABLE_7_N_ENTRY_SHFT                                                       0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x000002fc)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_0_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000300)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_1_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000304)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_2_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000308)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_3_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x0000030c)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_4_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000310)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_5_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000314)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_6_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_ADDR                                                    (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000318)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_RMSK                                                    0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_ENTRY_UPDATEN_BMSK                                        0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_ENTRY_UPDATEN_SHFT                                            0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_INDEX_UPDATE_BMSK                                         0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_INDEX_UPDATE_SHFT                                             0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_INDEX_BMSK                                                 0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_INDEX_SHFT                                                     0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_ENTRY_BMSK                                                  0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_GREEN_TABLE_7_N_ENTRY_SHFT                                                     0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x0000031c)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_0_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000320)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_1_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000324)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_2_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000328)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_3_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x0000032c)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_4_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000330)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_5_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000334)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_6_N_ENTRY_SHFT                                                      0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_ADDR                                                     (MMSS_MDP_VP_0_DSPP_3_REG_BASE      + 0x00000338)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_RMSK                                                     0xffffffff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_ENTRY_UPDATEN_BMSK                                         0x200000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_ENTRY_UPDATEN_SHFT                                             0x15
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_INDEX_UPDATE_BMSK                                          0x100000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_INDEX_UPDATE_SHFT                                              0x14
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_INDEX_BMSK                                                  0xfe000
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_INDEX_SHFT                                                      0xd
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_ENTRY_BMSK                                                   0x1fff
#define HWIO_MMSS_MDP_VP_0_DSPP_3_GAMUT_BLUE_TABLE_7_N_ENTRY_SHFT                                                      0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_WB_0
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_WB_0_DST_FORMAT_DST_CHROMA_SITE_BMSK                     0x3c000000
#define HWIO_MMSS_MDP_WB_0_DST_FORMAT_DST_CHROMA_SITE_SHFT                           0x1a

#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_ALPHA_VERT_FILTER_MODE_BMSK               0x400000
#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_ALPHA_VERT_FILTER_MODE_SHFT                   0x16
#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_ALPHA_HORZ_FILTER_MODE_BMSK               0x200000
#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_ALPHA_HORZ_FILTER_MODE_SHFT                   0x15
#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_DNSCL_RATIO_BMSK                          0x1e0000
#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_DNSCL_RATIO_SHFT                              0x11
#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_DNSCL_EN_BMSK                              0x10000
#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_DNSCL_EN_SHFT                                 0x10
#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_BWC_ENC_QP_BMSK                                0x6
#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_BWC_ENC_QP_SHFT                                0x1
#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_BWC_ENC_EN_BMSK                                0x1
#define HWIO_MMSS_MDP_WB_0_DST_OP_MODE_BWC_ENC_EN_SHFT                                0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_WB_1
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_WB_1_DST_FORMAT_DST_CHROMA_SITE_BMSK                     0x3c000000
#define HWIO_MMSS_MDP_WB_1_DST_FORMAT_DST_CHROMA_SITE_SHFT                           0x1a

#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_ALPHA_VERT_FILTER_MODE_BMSK               0x400000
#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_ALPHA_VERT_FILTER_MODE_SHFT                   0x16
#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_ALPHA_HORZ_FILTER_MODE_BMSK               0x200000
#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_ALPHA_HORZ_FILTER_MODE_SHFT                   0x15
#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_DNSCL_RATIO_BMSK                          0x1e0000
#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_DNSCL_RATIO_SHFT                              0x11
#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_DNSCL_EN_BMSK                              0x10000
#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_DNSCL_EN_SHFT                                 0x10
#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_BWC_ENC_QP_BMSK                                0x6
#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_BWC_ENC_QP_SHFT                                0x1
#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_BWC_ENC_EN_BMSK                                0x1
#define HWIO_MMSS_MDP_WB_1_DST_OP_MODE_BWC_ENC_EN_SHFT                                0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_WB_2
 *--------------------------------------------------------------------------*/
#define HWIO_MMSS_MDP_WB_2_DST_FORMAT_DST_CHROMA_SITE_BMSK                     0x3c000000
#define HWIO_MMSS_MDP_WB_2_DST_FORMAT_DST_CHROMA_SITE_SHFT                           0x1a

#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_ALPHA_VERT_FILTER_MODE_BMSK               0x400000
#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_ALPHA_VERT_FILTER_MODE_SHFT                   0x16
#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_ALPHA_HORZ_FILTER_MODE_BMSK               0x200000
#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_ALPHA_HORZ_FILTER_MODE_SHFT                   0x15
#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_DNSCL_RATIO_BMSK                          0x1e0000
#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_DNSCL_RATIO_SHFT                              0x11
#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_DNSCL_EN_BMSK                              0x10000
#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_DNSCL_EN_SHFT                                 0x10
#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_BWC_ENC_QP_BMSK                                0x6
#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_BWC_ENC_QP_SHFT                                0x1
#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_BWC_ENC_EN_BMSK                                0x1
#define HWIO_MMSS_MDP_WB_2_DST_OP_MODE_BWC_ENC_EN_SHFT                                0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_WB_3
 *--------------------------------------------------------------------------*/



#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_ADDR                                     (MMSS_MDP_WB_3_REG_BASE      + 0x00000000)
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_RMSK                                     0xffde77ff
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_FORMAT_ADDR, HWIO_MMSS_MDP_WB_3_DST_FORMAT_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_FORMAT_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST_FORMAT_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST_FORMAT_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST_FORMAT_IN)
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_FRAME_FORMAT_BMSK                        0xc0000000
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_FRAME_FORMAT_SHFT                              0x1e
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DST_CHROMA_SITE_BMSK                     0x3c000000
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DST_CHROMA_SITE_SHFT                           0x1a
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DST_CHROMA_SAMP_BMSK                      0x3800000
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DST_CHROMA_SAMP_SHFT                           0x17
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DST_DITHER_EN_BMSK                         0x400000
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DST_DITHER_EN_SHFT                             0x16
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_WRITE_PLANES_BMSK                          0x180000
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_WRITE_PLANES_SHFT                              0x13
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_PACK_ALIGN_BMSK                             0x40000
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_PACK_ALIGN_SHFT                                0x12
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_PACK_TIGHT_BMSK                             0x20000
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_PACK_TIGHT_SHFT                                0x11
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DST_ALPHA_X_BMSK                             0x4000
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DST_ALPHA_X_SHFT                                0xe
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_PACK_COUNT_BMSK                              0x3000
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_PACK_COUNT_SHFT                                 0xc
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DST_BPP_BMSK                                  0x600
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DST_BPP_SHFT                                    0x9
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DSTC3_EN_BMSK                                 0x100
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DSTC3_EN_SHFT                                   0x8
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DSTC3_OUT_BMSK                                 0xc0
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DSTC3_OUT_SHFT                                  0x6
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DSTC2_OUT_BMSK                                 0x30
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DSTC2_OUT_SHFT                                  0x4
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DSTC1_OUT_BMSK                                  0xc
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DSTC1_OUT_SHFT                                  0x2
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DSTC0_OUT_BMSK                                  0x3
#define HWIO_MMSS_MDP_WB_3_DST_FORMAT_DSTC0_OUT_SHFT                                  0x0

#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ADDR                                    (MMSS_MDP_WB_3_REG_BASE      + 0x00000004)
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_RMSK                                      0x7f7f77
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ADDR, HWIO_MMSS_MDP_WB_3_DST_OP_MODE_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST_OP_MODE_IN)
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ALPHA_VERT_FILTER_MODE_BMSK               0x400000
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ALPHA_VERT_FILTER_MODE_SHFT                   0x16
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ALPHA_HORZ_FILTER_MODE_BMSK               0x200000
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ALPHA_HORZ_FILTER_MODE_SHFT                   0x15
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_DNSCL_RATIO_BMSK                          0x1e0000
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_DNSCL_RATIO_SHFT                              0x11
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_DNSCL_EN_BMSK                              0x10000
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_DNSCL_EN_SHFT                                 0x10
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CHROMA_DWN_SAMPLE_V_MTHD_BMSK               0x4000
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CHROMA_DWN_SAMPLE_V_MTHD_SHFT                  0xe
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CHROMA_DWN_SAMPLE_H_MTHD_BMSK               0x2000
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CHROMA_DWN_SAMPLE_H_MTHD_SHFT                  0xd
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CHROMA_DWN_SAMPLE_FORMAT_BMSK               0x1000
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CHROMA_DWN_SAMPLE_FORMAT_SHFT                  0xc
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CHROMA_DWN_SAMPLE_EN_BMSK                    0x800
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CHROMA_DWN_SAMPLE_EN_SHFT                      0xb
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CSC_DST_DATA_FORMAT_BMSK                     0x400
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CSC_DST_DATA_FORMAT_SHFT                       0xa
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CSC_SRC_DATA_FORMAT_BMSK                     0x200
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CSC_SRC_DATA_FORMAT_SHFT                       0x9
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CSC_EN_BMSK                                  0x100
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_CSC_EN_SHFT                                    0x8
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ROT_EN_BMSK                                   0x40
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ROT_EN_SHFT                                    0x6
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ROT_MODE_BMSK                                 0x20
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_ROT_MODE_SHFT                                  0x5
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_BLOCK_SIZE_BMSK                               0x10
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_BLOCK_SIZE_SHFT                                0x4
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_BWC_ENC_QP_BMSK                                0x6
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_BWC_ENC_QP_SHFT                                0x1
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_BWC_ENC_EN_BMSK                                0x1
#define HWIO_MMSS_MDP_WB_3_DST_OP_MODE_BWC_ENC_EN_SHFT                                0x0

#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ADDR                               (MMSS_MDP_WB_3_REG_BASE      + 0x00000008)
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_RMSK                                0x3030303
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ADDR, HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_IN)
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ELEMENT3_BMSK                       0x3000000
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ELEMENT3_SHFT                            0x18
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ELEMENT2_BMSK                         0x30000
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ELEMENT2_SHFT                            0x10
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ELEMENT1_BMSK                           0x300
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ELEMENT1_SHFT                             0x8
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ELEMENT0_BMSK                             0x3
#define HWIO_MMSS_MDP_WB_3_DST_PACK_PATTERN_ELEMENT0_SHFT                             0x0

#define HWIO_MMSS_MDP_WB_3_DST0_ADDR_ADDR                                      (MMSS_MDP_WB_3_REG_BASE      + 0x0000000c)
#define HWIO_MMSS_MDP_WB_3_DST0_ADDR_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_WB_3_DST0_ADDR_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST0_ADDR_ADDR, HWIO_MMSS_MDP_WB_3_DST0_ADDR_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST0_ADDR_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST0_ADDR_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST0_ADDR_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST0_ADDR_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST0_ADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST0_ADDR_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST0_ADDR_IN)
#define HWIO_MMSS_MDP_WB_3_DST0_ADDR_ADDR_BMSK                                 0xffffffff
#define HWIO_MMSS_MDP_WB_3_DST0_ADDR_ADDR_SHFT                                        0x0

#define HWIO_MMSS_MDP_WB_3_DST1_ADDR_ADDR                                      (MMSS_MDP_WB_3_REG_BASE      + 0x00000010)
#define HWIO_MMSS_MDP_WB_3_DST1_ADDR_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_WB_3_DST1_ADDR_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST1_ADDR_ADDR, HWIO_MMSS_MDP_WB_3_DST1_ADDR_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST1_ADDR_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST1_ADDR_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST1_ADDR_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST1_ADDR_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST1_ADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST1_ADDR_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST1_ADDR_IN)
#define HWIO_MMSS_MDP_WB_3_DST1_ADDR_ADDR_BMSK                                 0xffffffff
#define HWIO_MMSS_MDP_WB_3_DST1_ADDR_ADDR_SHFT                                        0x0

#define HWIO_MMSS_MDP_WB_3_DST2_ADDR_ADDR                                      (MMSS_MDP_WB_3_REG_BASE      + 0x00000014)
#define HWIO_MMSS_MDP_WB_3_DST2_ADDR_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_WB_3_DST2_ADDR_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST2_ADDR_ADDR, HWIO_MMSS_MDP_WB_3_DST2_ADDR_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST2_ADDR_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST2_ADDR_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST2_ADDR_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST2_ADDR_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST2_ADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST2_ADDR_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST2_ADDR_IN)
#define HWIO_MMSS_MDP_WB_3_DST2_ADDR_ADDR_BMSK                                 0xffffffff
#define HWIO_MMSS_MDP_WB_3_DST2_ADDR_ADDR_SHFT                                        0x0

#define HWIO_MMSS_MDP_WB_3_DST3_ADDR_ADDR                                      (MMSS_MDP_WB_3_REG_BASE      + 0x00000018)
#define HWIO_MMSS_MDP_WB_3_DST3_ADDR_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_WB_3_DST3_ADDR_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST3_ADDR_ADDR, HWIO_MMSS_MDP_WB_3_DST3_ADDR_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST3_ADDR_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST3_ADDR_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST3_ADDR_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST3_ADDR_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST3_ADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST3_ADDR_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST3_ADDR_IN)
#define HWIO_MMSS_MDP_WB_3_DST3_ADDR_ADDR_BMSK                                 0xffffffff
#define HWIO_MMSS_MDP_WB_3_DST3_ADDR_ADDR_SHFT                                        0x0

#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_ADDR                                   (MMSS_MDP_WB_3_REG_BASE      + 0x0000001c)
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_RMSK                                   0xffffffff
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_ADDR, HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_IN)
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_DST1_YSTRIDE_BMSK                      0xffff0000
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_DST1_YSTRIDE_SHFT                            0x10
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_DST0_YSTRIDE_BMSK                          0xffff
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE0_DST0_YSTRIDE_SHFT                             0x0

#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_ADDR                                   (MMSS_MDP_WB_3_REG_BASE      + 0x00000020)
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_RMSK                                   0xffffffff
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_ADDR, HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_IN)
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_DST3_YSTRIDE_BMSK                      0xffff0000
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_DST3_YSTRIDE_SHFT                            0x10
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_DST2_YSTRIDE_BMSK                          0xffff
#define HWIO_MMSS_MDP_WB_3_DST_YSTRIDE1_DST2_YSTRIDE_SHFT                             0x0

#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_ADDR                            (MMSS_MDP_WB_3_REG_BASE      + 0x00000024)
#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_RMSK                                  0x3f
#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_ADDR, HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_IN)
#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_C2_BITS_BMSK                          0x30
#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_C2_BITS_SHFT                           0x4
#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_C1_BITS_BMSK                           0xc
#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_C1_BITS_SHFT                           0x2
#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_C0_BITS_BMSK                           0x3
#define HWIO_MMSS_MDP_WB_3_DST_DITHER_BITDEPTH_C0_BITS_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x00000030)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ADDR, HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_RMSK)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_IN)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ENTRY_3_BMSK                         0xf000
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ENTRY_3_SHFT                            0xc
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ENTRY_2_BMSK                          0xf00
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ENTRY_2_SHFT                            0x8
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ENTRY_1_BMSK                           0xf0
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ENTRY_1_SHFT                            0x4
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ENTRY_0_BMSK                            0xf
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW0_ENTRY_0_SHFT                            0x0

#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x00000034)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ADDR, HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_RMSK)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_IN)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ENTRY_7_BMSK                         0xf000
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ENTRY_7_SHFT                            0xc
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ENTRY_6_BMSK                          0xf00
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ENTRY_6_SHFT                            0x8
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ENTRY_5_BMSK                           0xf0
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ENTRY_5_SHFT                            0x4
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ENTRY_4_BMSK                            0xf
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW1_ENTRY_4_SHFT                            0x0

#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x00000038)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ADDR, HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_RMSK)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_IN)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ENTRY_11_BMSK                        0xf000
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ENTRY_11_SHFT                           0xc
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ENTRY_10_BMSK                         0xf00
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ENTRY_10_SHFT                           0x8
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ENTRY_9_BMSK                           0xf0
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ENTRY_9_SHFT                            0x4
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ENTRY_8_BMSK                            0xf
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW2_ENTRY_8_SHFT                            0x0

#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x0000003c)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ADDR, HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_RMSK)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_IN)
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ENTRY_15_BMSK                        0xf000
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ENTRY_15_SHFT                           0xc
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ENTRY_14_BMSK                         0xf00
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ENTRY_14_SHFT                           0x8
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ENTRY_13_BMSK                          0xf0
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ENTRY_13_SHFT                           0x4
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ENTRY_12_BMSK                           0xf
#define HWIO_MMSS_MDP_WB_3_DITHER_MATRIX_ROW3_ENTRY_12_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_ADDR                               (MMSS_MDP_WB_3_REG_BASE      + 0x00000048)
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_RMSK                                    0x7ff
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_ADDR, HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_IN)
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_ADDR_GEN_MODE_BMSK                      0x400
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_ADDR_GEN_MODE_SHFT                        0xa
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_HIGHEST_BANK_BIT_BMSK                   0x300
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_HIGHEST_BANK_BIT_SHFT                     0x8
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_PRIORITY_LEVEL_BMSK                      0xc0
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_PRIORITY_LEVEL_SHFT                       0x6
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_REQ_PRIORITY_BMSK                        0x30
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_REQ_PRIORITY_SHFT                         0x4
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_MAX_BURST_SIZE_BMSK                       0xf
#define HWIO_MMSS_MDP_WB_3_DST_WRITE_CONFIG_MAX_BURST_SIZE_SHFT                       0x0

#define HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_ADDR                        (MMSS_MDP_WB_3_REG_BASE      + 0x00000054)
#define HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_RMSK                          0xff00ff
#define HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_ADDR, HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_RMSK)
#define HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_ADDR,m,v,HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_IN)
#define HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_DNSC_FACTOR_V_BMSK            0xff0000
#define HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_DNSC_FACTOR_V_SHFT                0x10
#define HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_DNSC_FACTOR_H_BMSK                0xff
#define HWIO_MMSS_MDP_WB_3_ROTATOR_PIPE_DOWNSCALER_DNSC_FACTOR_H_SHFT                 0x0

#define HWIO_MMSS_MDP_WB_3_OUT_SIZE_ADDR                                       (MMSS_MDP_WB_3_REG_BASE      + 0x00000074)
#define HWIO_MMSS_MDP_WB_3_OUT_SIZE_RMSK                                       0xffffffff
#define HWIO_MMSS_MDP_WB_3_OUT_SIZE_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_OUT_SIZE_ADDR, HWIO_MMSS_MDP_WB_3_OUT_SIZE_RMSK)
#define HWIO_MMSS_MDP_WB_3_OUT_SIZE_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_OUT_SIZE_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_OUT_SIZE_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_OUT_SIZE_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_OUT_SIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_OUT_SIZE_ADDR,m,v,HWIO_MMSS_MDP_WB_3_OUT_SIZE_IN)
#define HWIO_MMSS_MDP_WB_3_OUT_SIZE_DST_H_BMSK                                 0xffff0000
#define HWIO_MMSS_MDP_WB_3_OUT_SIZE_DST_H_SHFT                                       0x10
#define HWIO_MMSS_MDP_WB_3_OUT_SIZE_DST_W_BMSK                                     0xffff
#define HWIO_MMSS_MDP_WB_3_OUT_SIZE_DST_W_SHFT                                        0x0

#define HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_ADDR                              (MMSS_MDP_WB_3_REG_BASE      + 0x00000078)
#define HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_RMSK                                    0xff
#define HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_ADDR, HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_IN)
#define HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_VALUE_BMSK                              0xff
#define HWIO_MMSS_MDP_WB_3_DST_ALPHA_X_VALUE_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x00000260)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_RMSK                             0x1fff1fff
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_ADDR, HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_COEFF_12_BMSK                    0x1fff0000
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_COEFF_12_SHFT                          0x10
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_COEFF_11_BMSK                        0x1fff
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_0_COEFF_11_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x00000264)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_RMSK                             0x1fff1fff
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_ADDR, HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_COEFF_21_BMSK                    0x1fff0000
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_COEFF_21_SHFT                          0x10
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_COEFF_13_BMSK                        0x1fff
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_1_COEFF_13_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x00000268)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_RMSK                             0x1fff1fff
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_ADDR, HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_COEFF_23_BMSK                    0x1fff0000
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_COEFF_23_SHFT                          0x10
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_COEFF_22_BMSK                        0x1fff
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_2_COEFF_22_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x0000026c)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_RMSK                             0x1fff1fff
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_ADDR, HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_COEFF_32_BMSK                    0x1fff0000
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_COEFF_32_SHFT                          0x10
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_COEFF_31_BMSK                        0x1fff
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_3_COEFF_31_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x00000270)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_RMSK                                 0x1fff
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_ADDR, HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_COEFF_33_BMSK                        0x1fff
#define HWIO_MMSS_MDP_WB_3_CSC_MATRIX_COEFF_4_COEFF_33_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x00000274)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_LOW_BMSK                             0xff00
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_LOW_SHFT                                0x8
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_HIGH_BMSK                              0xff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PRECLAMP_HIGH_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x00000278)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_LOW_BMSK                             0xff00
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_LOW_SHFT                                0x8
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_HIGH_BMSK                              0xff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PRECLAMP_HIGH_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x0000027c)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_LOW_BMSK                             0xff00
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_LOW_SHFT                                0x8
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_HIGH_BMSK                              0xff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PRECLAMP_HIGH_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_ADDR                            (MMSS_MDP_WB_3_REG_BASE      + 0x00000280)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_RMSK                                0xffff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_LOW_BMSK                            0xff00
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_LOW_SHFT                               0x8
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_HIGH_BMSK                             0xff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTCLAMP_HIGH_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_ADDR                            (MMSS_MDP_WB_3_REG_BASE      + 0x00000284)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_RMSK                                0xffff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_LOW_BMSK                            0xff00
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_LOW_SHFT                               0x8
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_HIGH_BMSK                             0xff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTCLAMP_HIGH_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_ADDR                            (MMSS_MDP_WB_3_REG_BASE      + 0x00000288)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_RMSK                                0xffff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_LOW_BMSK                            0xff00
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_LOW_SHFT                               0x8
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_HIGH_BMSK                             0xff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTCLAMP_HIGH_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_ADDR                              (MMSS_MDP_WB_3_REG_BASE      + 0x0000028c)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_RMSK                                   0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_VALUE_BMSK                             0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_PREBIAS_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_ADDR                              (MMSS_MDP_WB_3_REG_BASE      + 0x00000290)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_RMSK                                   0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_VALUE_BMSK                             0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_PREBIAS_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_ADDR                              (MMSS_MDP_WB_3_REG_BASE      + 0x00000294)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_RMSK                                   0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_VALUE_BMSK                             0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_PREBIAS_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x00000298)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_RMSK                                  0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_VALUE_BMSK                            0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP0_POSTBIAS_VALUE_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x0000029c)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_RMSK                                  0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_VALUE_BMSK                            0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP1_POSTBIAS_VALUE_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x000002a0)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_RMSK                                  0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_ADDR, HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_IN)
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_VALUE_BMSK                            0x1ff
#define HWIO_MMSS_MDP_WB_3_CSC_COMP2_POSTBIAS_VALUE_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_ADDR                             (MMSS_MDP_WB_3_REG_BASE      + 0x000002b0)
#define HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_RMSK                                    0x1
#define HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_ADDR, HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_RMSK)
#define HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_ADDR, m)
#define HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_ADDR,v)
#define HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_ADDR,m,v,HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_IN)
#define HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_DST_ADDR_CP_BMSK                        0x1
#define HWIO_MMSS_MDP_WB_3_DST_ADDR_SW_STATUS_DST_ADDR_CP_SHFT                        0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_WB_4
 *--------------------------------------------------------------------------*/



#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_ADDR                                     (MMSS_MDP_WB_4_REG_BASE      + 0x00000000)
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_RMSK                                     0xffde77ff
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_FORMAT_ADDR, HWIO_MMSS_MDP_WB_4_DST_FORMAT_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_FORMAT_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST_FORMAT_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST_FORMAT_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST_FORMAT_IN)
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_FRAME_FORMAT_BMSK                        0xc0000000
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_FRAME_FORMAT_SHFT                              0x1e
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DST_CHROMA_SITE_BMSK                     0x3c000000
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DST_CHROMA_SITE_SHFT                           0x1a
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DST_CHROMA_SAMP_BMSK                      0x3800000
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DST_CHROMA_SAMP_SHFT                           0x17
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DST_DITHER_EN_BMSK                         0x400000
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DST_DITHER_EN_SHFT                             0x16
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_WRITE_PLANES_BMSK                          0x180000
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_WRITE_PLANES_SHFT                              0x13
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_PACK_ALIGN_BMSK                             0x40000
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_PACK_ALIGN_SHFT                                0x12
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_PACK_TIGHT_BMSK                             0x20000
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_PACK_TIGHT_SHFT                                0x11
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DST_ALPHA_X_BMSK                             0x4000
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DST_ALPHA_X_SHFT                                0xe
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_PACK_COUNT_BMSK                              0x3000
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_PACK_COUNT_SHFT                                 0xc
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DST_BPP_BMSK                                  0x600
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DST_BPP_SHFT                                    0x9
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DSTC3_EN_BMSK                                 0x100
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DSTC3_EN_SHFT                                   0x8
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DSTC3_OUT_BMSK                                 0xc0
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DSTC3_OUT_SHFT                                  0x6
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DSTC2_OUT_BMSK                                 0x30
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DSTC2_OUT_SHFT                                  0x4
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DSTC1_OUT_BMSK                                  0xc
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DSTC1_OUT_SHFT                                  0x2
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DSTC0_OUT_BMSK                                  0x3
#define HWIO_MMSS_MDP_WB_4_DST_FORMAT_DSTC0_OUT_SHFT                                  0x0

#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ADDR                                    (MMSS_MDP_WB_4_REG_BASE      + 0x00000004)
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_RMSK                                      0x7f7f77
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ADDR, HWIO_MMSS_MDP_WB_4_DST_OP_MODE_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST_OP_MODE_IN)
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ALPHA_VERT_FILTER_MODE_BMSK               0x400000
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ALPHA_VERT_FILTER_MODE_SHFT                   0x16
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ALPHA_HORZ_FILTER_MODE_BMSK               0x200000
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ALPHA_HORZ_FILTER_MODE_SHFT                   0x15
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_DNSCL_RATIO_BMSK                          0x1e0000
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_DNSCL_RATIO_SHFT                              0x11
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_DNSCL_EN_BMSK                              0x10000
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_DNSCL_EN_SHFT                                 0x10
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CHROMA_DWN_SAMPLE_V_MTHD_BMSK               0x4000
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CHROMA_DWN_SAMPLE_V_MTHD_SHFT                  0xe
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CHROMA_DWN_SAMPLE_H_MTHD_BMSK               0x2000
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CHROMA_DWN_SAMPLE_H_MTHD_SHFT                  0xd
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CHROMA_DWN_SAMPLE_FORMAT_BMSK               0x1000
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CHROMA_DWN_SAMPLE_FORMAT_SHFT                  0xc
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CHROMA_DWN_SAMPLE_EN_BMSK                    0x800
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CHROMA_DWN_SAMPLE_EN_SHFT                      0xb
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CSC_DST_DATA_FORMAT_BMSK                     0x400
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CSC_DST_DATA_FORMAT_SHFT                       0xa
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CSC_SRC_DATA_FORMAT_BMSK                     0x200
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CSC_SRC_DATA_FORMAT_SHFT                       0x9
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CSC_EN_BMSK                                  0x100
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_CSC_EN_SHFT                                    0x8
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ROT_EN_BMSK                                   0x40
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ROT_EN_SHFT                                    0x6
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ROT_MODE_BMSK                                 0x20
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_ROT_MODE_SHFT                                  0x5
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_BLOCK_SIZE_BMSK                               0x10
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_BLOCK_SIZE_SHFT                                0x4
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_BWC_ENC_QP_BMSK                                0x6
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_BWC_ENC_QP_SHFT                                0x1
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_BWC_ENC_EN_BMSK                                0x1
#define HWIO_MMSS_MDP_WB_4_DST_OP_MODE_BWC_ENC_EN_SHFT                                0x0

#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ADDR                               (MMSS_MDP_WB_4_REG_BASE      + 0x00000008)
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_RMSK                                0x3030303
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ADDR, HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_IN)
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ELEMENT3_BMSK                       0x3000000
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ELEMENT3_SHFT                            0x18
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ELEMENT2_BMSK                         0x30000
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ELEMENT2_SHFT                            0x10
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ELEMENT1_BMSK                           0x300
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ELEMENT1_SHFT                             0x8
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ELEMENT0_BMSK                             0x3
#define HWIO_MMSS_MDP_WB_4_DST_PACK_PATTERN_ELEMENT0_SHFT                             0x0

#define HWIO_MMSS_MDP_WB_4_DST0_ADDR_ADDR                                      (MMSS_MDP_WB_4_REG_BASE      + 0x0000000c)
#define HWIO_MMSS_MDP_WB_4_DST0_ADDR_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_WB_4_DST0_ADDR_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST0_ADDR_ADDR, HWIO_MMSS_MDP_WB_4_DST0_ADDR_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST0_ADDR_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST0_ADDR_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST0_ADDR_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST0_ADDR_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST0_ADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST0_ADDR_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST0_ADDR_IN)
#define HWIO_MMSS_MDP_WB_4_DST0_ADDR_ADDR_BMSK                                 0xffffffff
#define HWIO_MMSS_MDP_WB_4_DST0_ADDR_ADDR_SHFT                                        0x0

#define HWIO_MMSS_MDP_WB_4_DST1_ADDR_ADDR                                      (MMSS_MDP_WB_4_REG_BASE      + 0x00000010)
#define HWIO_MMSS_MDP_WB_4_DST1_ADDR_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_WB_4_DST1_ADDR_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST1_ADDR_ADDR, HWIO_MMSS_MDP_WB_4_DST1_ADDR_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST1_ADDR_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST1_ADDR_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST1_ADDR_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST1_ADDR_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST1_ADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST1_ADDR_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST1_ADDR_IN)
#define HWIO_MMSS_MDP_WB_4_DST1_ADDR_ADDR_BMSK                                 0xffffffff
#define HWIO_MMSS_MDP_WB_4_DST1_ADDR_ADDR_SHFT                                        0x0

#define HWIO_MMSS_MDP_WB_4_DST2_ADDR_ADDR                                      (MMSS_MDP_WB_4_REG_BASE      + 0x00000014)
#define HWIO_MMSS_MDP_WB_4_DST2_ADDR_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_WB_4_DST2_ADDR_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST2_ADDR_ADDR, HWIO_MMSS_MDP_WB_4_DST2_ADDR_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST2_ADDR_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST2_ADDR_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST2_ADDR_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST2_ADDR_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST2_ADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST2_ADDR_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST2_ADDR_IN)
#define HWIO_MMSS_MDP_WB_4_DST2_ADDR_ADDR_BMSK                                 0xffffffff
#define HWIO_MMSS_MDP_WB_4_DST2_ADDR_ADDR_SHFT                                        0x0

#define HWIO_MMSS_MDP_WB_4_DST3_ADDR_ADDR                                      (MMSS_MDP_WB_4_REG_BASE      + 0x00000018)
#define HWIO_MMSS_MDP_WB_4_DST3_ADDR_RMSK                                      0xffffffff
#define HWIO_MMSS_MDP_WB_4_DST3_ADDR_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST3_ADDR_ADDR, HWIO_MMSS_MDP_WB_4_DST3_ADDR_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST3_ADDR_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST3_ADDR_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST3_ADDR_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST3_ADDR_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST3_ADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST3_ADDR_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST3_ADDR_IN)
#define HWIO_MMSS_MDP_WB_4_DST3_ADDR_ADDR_BMSK                                 0xffffffff
#define HWIO_MMSS_MDP_WB_4_DST3_ADDR_ADDR_SHFT                                        0x0

#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_ADDR                                   (MMSS_MDP_WB_4_REG_BASE      + 0x0000001c)
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_RMSK                                   0xffffffff
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_ADDR, HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_IN)
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_DST1_YSTRIDE_BMSK                      0xffff0000
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_DST1_YSTRIDE_SHFT                            0x10
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_DST0_YSTRIDE_BMSK                          0xffff
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE0_DST0_YSTRIDE_SHFT                             0x0

#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_ADDR                                   (MMSS_MDP_WB_4_REG_BASE      + 0x00000020)
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_RMSK                                   0xffffffff
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_ADDR, HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_IN)
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_DST3_YSTRIDE_BMSK                      0xffff0000
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_DST3_YSTRIDE_SHFT                            0x10
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_DST2_YSTRIDE_BMSK                          0xffff
#define HWIO_MMSS_MDP_WB_4_DST_YSTRIDE1_DST2_YSTRIDE_SHFT                             0x0

#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_ADDR                            (MMSS_MDP_WB_4_REG_BASE      + 0x00000024)
#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_RMSK                                  0x3f
#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_ADDR, HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_IN)
#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_C2_BITS_BMSK                          0x30
#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_C2_BITS_SHFT                           0x4
#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_C1_BITS_BMSK                           0xc
#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_C1_BITS_SHFT                           0x2
#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_C0_BITS_BMSK                           0x3
#define HWIO_MMSS_MDP_WB_4_DST_DITHER_BITDEPTH_C0_BITS_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x00000030)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ADDR, HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_RMSK)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_IN)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ENTRY_3_BMSK                         0xf000
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ENTRY_3_SHFT                            0xc
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ENTRY_2_BMSK                          0xf00
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ENTRY_2_SHFT                            0x8
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ENTRY_1_BMSK                           0xf0
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ENTRY_1_SHFT                            0x4
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ENTRY_0_BMSK                            0xf
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW0_ENTRY_0_SHFT                            0x0

#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x00000034)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ADDR, HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_RMSK)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_IN)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ENTRY_7_BMSK                         0xf000
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ENTRY_7_SHFT                            0xc
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ENTRY_6_BMSK                          0xf00
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ENTRY_6_SHFT                            0x8
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ENTRY_5_BMSK                           0xf0
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ENTRY_5_SHFT                            0x4
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ENTRY_4_BMSK                            0xf
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW1_ENTRY_4_SHFT                            0x0

#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x00000038)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ADDR, HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_RMSK)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_IN)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ENTRY_11_BMSK                        0xf000
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ENTRY_11_SHFT                           0xc
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ENTRY_10_BMSK                         0xf00
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ENTRY_10_SHFT                           0x8
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ENTRY_9_BMSK                           0xf0
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ENTRY_9_SHFT                            0x4
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ENTRY_8_BMSK                            0xf
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW2_ENTRY_8_SHFT                            0x0

#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x0000003c)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ADDR, HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_RMSK)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_IN)
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ENTRY_15_BMSK                        0xf000
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ENTRY_15_SHFT                           0xc
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ENTRY_14_BMSK                         0xf00
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ENTRY_14_SHFT                           0x8
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ENTRY_13_BMSK                          0xf0
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ENTRY_13_SHFT                           0x4
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ENTRY_12_BMSK                           0xf
#define HWIO_MMSS_MDP_WB_4_DITHER_MATRIX_ROW3_ENTRY_12_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_ADDR                               (MMSS_MDP_WB_4_REG_BASE      + 0x00000048)
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_RMSK                                    0x7ff
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_ADDR, HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_IN)
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_ADDR_GEN_MODE_BMSK                      0x400
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_ADDR_GEN_MODE_SHFT                        0xa
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_HIGHEST_BANK_BIT_BMSK                   0x300
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_HIGHEST_BANK_BIT_SHFT                     0x8
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_PRIORITY_LEVEL_BMSK                      0xc0
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_PRIORITY_LEVEL_SHFT                       0x6
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_REQ_PRIORITY_BMSK                        0x30
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_REQ_PRIORITY_SHFT                         0x4
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_MAX_BURST_SIZE_BMSK                       0xf
#define HWIO_MMSS_MDP_WB_4_DST_WRITE_CONFIG_MAX_BURST_SIZE_SHFT                       0x0

#define HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_ADDR                        (MMSS_MDP_WB_4_REG_BASE      + 0x00000054)
#define HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_RMSK                          0xff00ff
#define HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_ADDR, HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_RMSK)
#define HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_ADDR,m,v,HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_IN)
#define HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_DNSC_FACTOR_V_BMSK            0xff0000
#define HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_DNSC_FACTOR_V_SHFT                0x10
#define HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_DNSC_FACTOR_H_BMSK                0xff
#define HWIO_MMSS_MDP_WB_4_ROTATOR_PIPE_DOWNSCALER_DNSC_FACTOR_H_SHFT                 0x0

#define HWIO_MMSS_MDP_WB_4_OUT_SIZE_ADDR                                       (MMSS_MDP_WB_4_REG_BASE      + 0x00000074)
#define HWIO_MMSS_MDP_WB_4_OUT_SIZE_RMSK                                       0xffffffff
#define HWIO_MMSS_MDP_WB_4_OUT_SIZE_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_OUT_SIZE_ADDR, HWIO_MMSS_MDP_WB_4_OUT_SIZE_RMSK)
#define HWIO_MMSS_MDP_WB_4_OUT_SIZE_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_OUT_SIZE_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_OUT_SIZE_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_OUT_SIZE_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_OUT_SIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_OUT_SIZE_ADDR,m,v,HWIO_MMSS_MDP_WB_4_OUT_SIZE_IN)
#define HWIO_MMSS_MDP_WB_4_OUT_SIZE_DST_H_BMSK                                 0xffff0000
#define HWIO_MMSS_MDP_WB_4_OUT_SIZE_DST_H_SHFT                                       0x10
#define HWIO_MMSS_MDP_WB_4_OUT_SIZE_DST_W_BMSK                                     0xffff
#define HWIO_MMSS_MDP_WB_4_OUT_SIZE_DST_W_SHFT                                        0x0

#define HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_ADDR                              (MMSS_MDP_WB_4_REG_BASE      + 0x00000078)
#define HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_RMSK                                    0xff
#define HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_ADDR, HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_IN)
#define HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_VALUE_BMSK                              0xff
#define HWIO_MMSS_MDP_WB_4_DST_ALPHA_X_VALUE_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x00000260)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_RMSK                             0x1fff1fff
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_ADDR, HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_COEFF_12_BMSK                    0x1fff0000
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_COEFF_12_SHFT                          0x10
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_COEFF_11_BMSK                        0x1fff
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_0_COEFF_11_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x00000264)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_RMSK                             0x1fff1fff
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_ADDR, HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_COEFF_21_BMSK                    0x1fff0000
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_COEFF_21_SHFT                          0x10
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_COEFF_13_BMSK                        0x1fff
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_1_COEFF_13_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x00000268)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_RMSK                             0x1fff1fff
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_ADDR, HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_COEFF_23_BMSK                    0x1fff0000
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_COEFF_23_SHFT                          0x10
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_COEFF_22_BMSK                        0x1fff
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_2_COEFF_22_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x0000026c)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_RMSK                             0x1fff1fff
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_ADDR, HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_COEFF_32_BMSK                    0x1fff0000
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_COEFF_32_SHFT                          0x10
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_COEFF_31_BMSK                        0x1fff
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_3_COEFF_31_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x00000270)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_RMSK                                 0x1fff
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_ADDR, HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_COEFF_33_BMSK                        0x1fff
#define HWIO_MMSS_MDP_WB_4_CSC_MATRIX_COEFF_4_COEFF_33_SHFT                           0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x00000274)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_LOW_BMSK                             0xff00
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_LOW_SHFT                                0x8
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_HIGH_BMSK                              0xff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PRECLAMP_HIGH_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x00000278)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_LOW_BMSK                             0xff00
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_LOW_SHFT                                0x8
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_HIGH_BMSK                              0xff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PRECLAMP_HIGH_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x0000027c)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_RMSK                                 0xffff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_LOW_BMSK                             0xff00
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_LOW_SHFT                                0x8
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_HIGH_BMSK                              0xff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PRECLAMP_HIGH_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_ADDR                            (MMSS_MDP_WB_4_REG_BASE      + 0x00000280)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_RMSK                                0xffff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_LOW_BMSK                            0xff00
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_LOW_SHFT                               0x8
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_HIGH_BMSK                             0xff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTCLAMP_HIGH_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_ADDR                            (MMSS_MDP_WB_4_REG_BASE      + 0x00000284)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_RMSK                                0xffff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_LOW_BMSK                            0xff00
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_LOW_SHFT                               0x8
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_HIGH_BMSK                             0xff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTCLAMP_HIGH_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_ADDR                            (MMSS_MDP_WB_4_REG_BASE      + 0x00000288)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_RMSK                                0xffff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_LOW_BMSK                            0xff00
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_LOW_SHFT                               0x8
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_HIGH_BMSK                             0xff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTCLAMP_HIGH_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_ADDR                              (MMSS_MDP_WB_4_REG_BASE      + 0x0000028c)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_RMSK                                   0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_VALUE_BMSK                             0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_PREBIAS_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_ADDR                              (MMSS_MDP_WB_4_REG_BASE      + 0x00000290)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_RMSK                                   0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_VALUE_BMSK                             0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_PREBIAS_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_ADDR                              (MMSS_MDP_WB_4_REG_BASE      + 0x00000294)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_RMSK                                   0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_VALUE_BMSK                             0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_PREBIAS_VALUE_SHFT                               0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x00000298)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_RMSK                                  0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_VALUE_BMSK                            0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP0_POSTBIAS_VALUE_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x0000029c)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_RMSK                                  0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_VALUE_BMSK                            0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP1_POSTBIAS_VALUE_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x000002a0)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_RMSK                                  0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_ADDR, HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_RMSK)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_ADDR,m,v,HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_IN)
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_VALUE_BMSK                            0x1ff
#define HWIO_MMSS_MDP_WB_4_CSC_COMP2_POSTBIAS_VALUE_SHFT                              0x0

#define HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_ADDR                             (MMSS_MDP_WB_4_REG_BASE      + 0x000002b0)
#define HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_RMSK                                    0x1
#define HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_ADDR, HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_RMSK)
#define HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_ADDR, m)
#define HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_ADDR,v)
#define HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_ADDR,m,v,HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_IN)
#define HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_DST_ADDR_CP_BMSK                        0x1
#define HWIO_MMSS_MDP_WB_4_DST_ADDR_SW_STATUS_DST_ADDR_CP_SHFT                        0x0


/*----------------------------------------------------------------------------
 * MODULE: MMSS_VBIF_VBIF_MDP
 *--------------------------------------------------------------------------*/
#define HWIO_MMSS_VBIF_VBIF_VA_ARANGE1_BASE_150_ADDR                                              (MMSS_VBIF_VBIF_MDP_REG_BASE      + 0x00000084)

#define HWIO_MMSS_VBIF_VBIF_VA_ARANGE1_HIGH_150_ADDR                                              (MMSS_VBIF_VBIF_MDP_REG_BASE      + 0x00000088)

#define HWIO_MMSS_VBIF_VBIF_VA_ARANGE2_SORT_150_ADDR                                              (MMSS_VBIF_VBIF_MDP_REG_BASE      + 0x00000090)

#define HWIO_MMSS_VBIF_VBIF_VA_ARANGE2_BASE_150_ADDR                                              (MMSS_VBIF_VBIF_MDP_REG_BASE      + 0x00000094)

#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_ADDR                                              (MMSS_VBIF_VBIF_MDP_REG_BASE      + 0x000000ac)
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_RMSK                                                  0x3fff
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_IN          \
        in_dword_masked(HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_ADDR, HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_RMSK)
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_ADDR, m)
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_OUT(v)      \
        out_dword(HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_ADDR,v)
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_ADDR,m,v,HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_IN)
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C13_BMSK                         0x2000
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C13_SHFT                            0xd
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C12_BMSK                         0x1000
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C12_SHFT                            0xc
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C11_BMSK                          0x800
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C11_SHFT                            0xb
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C10_BMSK                          0x400
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C10_SHFT                            0xa
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C9_BMSK                           0x200
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C9_SHFT                             0x9
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C8_BMSK                           0x100
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C8_SHFT                             0x8
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C7_BMSK                            0x80
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C7_SHFT                             0x7
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C6_BMSK                            0x40
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C6_SHFT                             0x6
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C5_BMSK                            0x20
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C5_SHFT                             0x5
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C4_BMSK                            0x10
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C4_SHFT                             0x4
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C3_BMSK                             0x8
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C3_SHFT                             0x3
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C2_BMSK                             0x4
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C2_SHFT                             0x2
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C1_BMSK                             0x2
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C1_SHFT                             0x1
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C0_BMSK                             0x1
#define HWIO_MMSS_VBIF_VBIF_WRITE_GATHER_EN_VBIF_WRITE_GATHER_EN_C0_SHFT                             0x0

#define HWIO_MMSS_VBIF_VBIF_CCPBE_EN_ADDR                                                     (MMSS_VBIF_VBIF_MDP_REG_BASE      + 0x000000e4)
#define HWIO_MMSS_VBIF_VBIF_CCPBE_EN_RMSK                                                            0x1
#define HWIO_MMSS_VBIF_VBIF_CCPBE_EN_IN          \
        in_dword_masked(HWIO_MMSS_VBIF_VBIF_CCPBE_EN_ADDR, HWIO_MMSS_VBIF_VBIF_CCPBE_EN_RMSK)
#define HWIO_MMSS_VBIF_VBIF_CCPBE_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_VBIF_VBIF_CCPBE_EN_ADDR, m)
#define HWIO_MMSS_VBIF_VBIF_CCPBE_EN_OUT(v)      \
        out_dword(HWIO_MMSS_VBIF_VBIF_CCPBE_EN_ADDR,v)
#define HWIO_MMSS_VBIF_VBIF_CCPBE_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_VBIF_VBIF_CCPBE_EN_ADDR,m,v,HWIO_MMSS_VBIF_VBIF_CCPBE_EN_IN)
#define HWIO_MMSS_VBIF_VBIF_CCPBE_EN_CCPBE_EN_BMSK                                                   0x1
#define HWIO_MMSS_VBIF_VBIF_CCPBE_EN_CCPBE_EN_SHFT                                                   0x0


/*----------------------------------------------------------------------------
 * MODULE: MMSS_HDMI
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_HDMI_CTRL_ENC_REQUIRED_BMSK                                               0x4
#define HWIO_MMSS_HDMI_CTRL_ENC_REQUIRED_SHFT                                               0x2

#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_ADDR                                          (MMSS_HDMI_REG_BASE      + 0x00000044)
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_RMSK                                               0xf13
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_IN          \
        in_dword_masked(HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_ADDR, HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_RMSK)
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_ADDR, m)
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_ADDR,v)
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_ADDR,m,v,HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_IN)
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_CHANNEL_ENABLE_BMSK                                0xf00
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_CHANNEL_ENABLE_SHFT                                  0x8
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_CS_60958_SOURCE_BMSK                                0x10
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_CS_60958_SOURCE_SHFT                                 0x4
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_LAYOUT_SEL_BMSK                                      0x2
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_LAYOUT_SEL_SHFT                                      0x1
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_LAYOUT_OVRD_BMSK                                     0x1
#define HWIO_MMSS_HDMI_AUDIO_PKT_CTRL2_LAYOUT_OVRD_SHFT                                     0x0

#define HWIO_MMSS_HDMI_HDCP_CTRL2_ADDR                                               (MMSS_HDMI_REG_BASE      + 0x0000010c)
#define HWIO_MMSS_HDMI_HDCP_CTRL2_RMSK                                                     0x11
#define HWIO_MMSS_HDMI_HDCP_CTRL2_IN          \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_CTRL2_ADDR, HWIO_MMSS_HDMI_HDCP_CTRL2_RMSK)
#define HWIO_MMSS_HDMI_HDCP_CTRL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_CTRL2_ADDR, m)
#define HWIO_MMSS_HDMI_HDCP_CTRL2_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_CTRL2_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_CTRL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_HDMI_HDCP_CTRL2_ADDR,m,v,HWIO_MMSS_HDMI_HDCP_CTRL2_IN)
#define HWIO_MMSS_HDMI_HDCP_CTRL2_ENC_REQUIRED_BMSK                                        0x10
#define HWIO_MMSS_HDMI_HDCP_CTRL2_ENC_REQUIRED_SHFT                                         0x4
#define HWIO_MMSS_HDMI_HDCP_CTRL2_OVERRIDE_BMSK                                             0x1
#define HWIO_MMSS_HDMI_HDCP_CTRL2_OVERRIDE_SHFT                                             0x0

#define HWIO_MMSS_HDMI_HDCP_CTRL_ENCRYPTION_ENABLE_BMSK                                   0x100
#define HWIO_MMSS_HDMI_HDCP_CTRL_ENCRYPTION_ENABLE_SHFT                                     0x8

#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_ADDR                                          (MMSS_HDMI_REG_BASE      + 0x00000114)
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_RMSK                                          0xfffffbff
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_IN          \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_ADDR, HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_RMSK)
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_ADDR, m)
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_ADDR,m,v,HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_IN)
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_FRAME_CNT_START_VAL_BMSK                      0xff000000
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_FRAME_CNT_START_VAL_SHFT                            0x18
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_IGNORED_PJ_CHK_TO_BMSK                          0x800000
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_IGNORED_PJ_CHK_TO_SHFT                              0x17
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_IGNORED_RI_CHK_TO_BMSK                          0x400000
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_IGNORED_RI_CHK_TO_SHFT                              0x16
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_EESS_WHEN_UNAUTH_BMSK                           0x200000
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_EESS_WHEN_UNAUTH_SHFT                               0x15
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_SWDDC_XFERREQ_TO_DIS_BMSK                       0x100000
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_SWDDC_XFERREQ_TO_DIS_SHFT                           0x14
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_EESS_WHEN_AVMUTE_BMSK                            0x80000
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_EESS_WHEN_AVMUTE_SHFT                               0x13
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_ADV_CIPHER_ON_AVMUTE_BMSK                        0x40000
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_ADV_CIPHER_ON_AVMUTE_SHFT                           0x12
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_FRAMES_TO_PJ_CHK_BMSK                            0x30000
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_FRAMES_TO_PJ_CHK_SHFT                               0x10
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_SW_ENABLE_AN_BMSK                                 0x8000
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_SW_ENABLE_AN_SHFT                                    0xf
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_NO_DEFERRED_ENC_DIS_BMSK                          0x4000
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_NO_DEFERRED_ENC_DIS_SHFT                             0xe
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_FRAMES_TO_RI_CHK_BMSK                             0x3000
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_FRAMES_TO_RI_CHK_SHFT                                0xc
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_MAX_PJ_MISM_CNT_RST_BMSK                           0x800
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_MAX_PJ_MISM_CNT_RST_SHFT                             0xb
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_MAX_PJ_MISM_CNT_BMSK                               0x300
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_MAX_PJ_MISM_CNT_SHFT                                 0x8
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_BLNK_ALL_PKTS_BMSK                                  0x80
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_BLNK_ALL_PKTS_SHFT                                   0x7
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_RAISE_URG_2FRM_EARLY_BMSK                           0x40
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_RAISE_URG_2FRM_EARLY_SHFT                            0x6
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_DIS_RI_CHKIN_ADV_CIP_BMSK                           0x20
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_DIS_RI_CHKIN_ADV_CIP_SHFT                            0x5
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_IGNORE_HPD_UNPLUG_BMSK                              0x10
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_IGNORE_HPD_UNPLUG_SHFT                               0x4
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_SYNC_KEY_READ_BMSK                                   0x8
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_SYNC_KEY_READ_SHFT                                   0x3
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_DEBUG_RNG_CIPHER_BMSK                                0x4
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_DEBUG_RNG_CIPHER_SHFT                                0x2
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_FRAME_CNT_SELECT_BMSK                                0x2
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_FRAME_CNT_SELECT_SHFT                                0x1
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_USER_DEFINED_AN_BMSK                                 0x1
#define HWIO_MMSS_HDMI_HDCP_DEBUG_CTRL_USER_DEFINED_AN_SHFT                                 0x0

#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA0_ADDR                                       (MMSS_HDMI_REG_BASE      + 0x00000134)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA0_RMSK                                       0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA0_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA0_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA0_LINK0_BKSV_0_BMSK                          0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA0_LINK0_BKSV_0_SHFT                                 0x0

#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA1_ADDR                                       (MMSS_HDMI_REG_BASE      + 0x00000138)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA1_RMSK                                             0xff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA1_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA1_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA1_LINK0_BKSV_1_BMSK                                0xff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA1_LINK0_BKSV_1_SHFT                                 0x0

#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_ADDR                                       (MMSS_HDMI_REG_BASE      + 0x00000154)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_RMSK                                       0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_IN          \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_ADDR, HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_RMSK)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_INM(m)      \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_ADDR, m)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_ADDR,m,v,HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_IN)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_V_H0_BMSK                                  0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA7_V_H0_SHFT                                         0x0

#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_ADDR                                       (MMSS_HDMI_REG_BASE      + 0x00000158)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_RMSK                                       0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_IN          \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_ADDR, HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_RMSK)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_INM(m)      \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_ADDR, m)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_ADDR,m,v,HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_IN)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_V_H1_BMSK                                  0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA8_V_H1_SHFT                                         0x0

#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_ADDR                                       (MMSS_HDMI_REG_BASE      + 0x0000015c)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_RMSK                                       0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_IN          \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_ADDR, HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_RMSK)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_INM(m)      \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_ADDR, m)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_ADDR,m,v,HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_IN)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_V_H2_BMSK                                  0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA9_V_H2_SHFT                                         0x0

#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_ADDR                                      (MMSS_HDMI_REG_BASE      + 0x00000160)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_RMSK                                      0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_IN          \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_ADDR, HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_RMSK)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_INM(m)      \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_ADDR, m)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_ADDR,m,v,HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_IN)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_V_H3_BMSK                                 0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA10_V_H3_SHFT                                        0x0

#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_ADDR                                      (MMSS_HDMI_REG_BASE      + 0x00000164)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_RMSK                                      0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_IN          \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_ADDR, HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_RMSK)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_INM(m)      \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_ADDR, m)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_ADDR,m,v,HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_IN)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_V_H4_BMSK                                 0xffffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA11_V_H4_SHFT                                        0x0

#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_ADDR                                      (MMSS_HDMI_REG_BASE      + 0x00000168)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_RMSK                                        0xffffff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_IN          \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_ADDR, HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_RMSK)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_INM(m)      \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_ADDR, m)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_ADDR,m,v,HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_IN)
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_BSTATUS_BMSK                                0xffff00
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_BSTATUS_SHFT                                     0x8
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_BCAPS_BMSK                                      0xff
#define HWIO_MMSS_HDMI_HDCP_RCVPORT_DATA12_BCAPS_SHFT                                       0x0

#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_ADDR                                            (MMSS_HDMI_REG_BASE      + 0x0000023c)
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_RMSK                                            0x80001111
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_IN          \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_SHA_CTRL_ADDR, HWIO_MMSS_HDMI_HDCP_SHA_CTRL_RMSK)
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_HDMI_HDCP_SHA_CTRL_ADDR, m)
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_SHA_CTRL_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_HDMI_HDCP_SHA_CTRL_ADDR,m,v,HWIO_MMSS_HDMI_HDCP_SHA_CTRL_IN)
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_DEBUG_EN_BMSK                                   0x80000000
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_DEBUG_EN_SHFT                                         0x1f
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_DBG_NO_APND_BYTES_BMSK                              0x1000
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_DBG_NO_APND_BYTES_SHFT                                 0xc
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_DBG_EN_BMSK                                          0x100
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_DBG_EN_SHFT                                            0x8
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_SELECT_BMSK                                           0x10
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_SELECT_SHFT                                            0x4
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_RESET_BMSK                                             0x1
#define HWIO_MMSS_HDMI_HDCP_SHA_CTRL_RESET_SHFT                                             0x0

#define HWIO_MMSS_HDMI_HDCP_SHA_DATA_ADDR                                            (MMSS_HDMI_REG_BASE      + 0x00000244)
#define HWIO_MMSS_HDMI_HDCP_SHA_DATA_RMSK                                              0xff0001
#define HWIO_MMSS_HDMI_HDCP_SHA_DATA_OUT(v)      \
        out_dword(HWIO_MMSS_HDMI_HDCP_SHA_DATA_ADDR,v)
#define HWIO_MMSS_HDMI_HDCP_SHA_DATA_DATA_BMSK                                         0xff0000
#define HWIO_MMSS_HDMI_HDCP_SHA_DATA_DATA_SHFT                                             0x10
#define HWIO_MMSS_HDMI_HDCP_SHA_DATA_DATA_DONE_BMSK                                         0x1
#define HWIO_MMSS_HDMI_HDCP_SHA_DATA_DATA_DONE_SHFT                                         0x0




#endif /* _MDSSREG_150D_H_ */
