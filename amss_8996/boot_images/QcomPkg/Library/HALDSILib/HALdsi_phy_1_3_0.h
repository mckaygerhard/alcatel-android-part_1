#ifndef HALDSI_PHY_1_3_0_H
#define HALDSI_PHY_1_3_0_H
/*=============================================================================

  File: HALdsi_phy_1_3_0.h
  
  This file contains DSI PHY/PLL registers for following chips:
      - MSM8992 Estel
	  - MSM8994 Elessar
	  - MSM8994 Elessar (V2)


  Copyright (c) 2010-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/

/*----------------------------------------------------------------------------
 * MODULE: MMSS_DSI_0_PHY
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_ADDR(n)                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x00000000 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_RMSK                                                           0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_MAXn                                                              3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_ADDR(n),val)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_ADDR(n),mask,val,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_INI(n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_RSVD_7_4_BMSK                                                  0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_RSVD_7_4_SHFT                                                   0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_DSIPHY_HSTX_PGM_DLY_BMSK                                        0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_DSIPHY_HSTX_PGM_DLY_SHFT                                        0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_ADDR(n)                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x00000004 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_RMSK                                                           0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_MAXn                                                              3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_ADDR(n),val)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_ADDR(n),mask,val,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_INI(n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_DSIPHY_HSTX_SLEW_BOT_BMSK                                      0xc0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_DSIPHY_HSTX_SLEW_BOT_SHFT                                       0x6
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_DSIPHY_HSTX_SLEW_TOP_BMSK                                      0x30
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_DSIPHY_HSTX_SLEW_TOP_SHFT                                       0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_RSVD_3_2_BMSK                                                   0xc
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_RSVD_3_2_SHFT                                                   0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_DSIPHY_PEMPH_DLY_SEL_BMSK                                       0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_DSIPHY_PEMPH_DLY_SEL_SHFT                                       0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_ADDR(n)                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x00000008 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_RMSK                                                           0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_MAXn                                                              3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_ADDR(n),val)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_ADDR(n),mask,val,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_INI(n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_STRBOT_BMSK                                       0xe0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_STRBOT_SHFT                                        0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_RSVD_4_BMSK                                                    0x10
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_RSVD_4_SHFT                                                     0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_STRTOP_BMSK                                        0xe
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_STRTOP_SHFT                                        0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_EN_BMSK                                            0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_EN_SHFT                                            0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_ADDR(n)                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x0000000c + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_RMSK                                                           0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_MAXn                                                              3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_ADDR(n),val)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_ADDR(n),mask,val,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_INI(n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_IS_CKLANE_BMSK                                                 0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_IS_CKLANE_SHFT                                                  0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_IS_LANE_R_BMSK                                                 0x40
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_IS_LANE_R_SHFT                                                  0x6
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_DSIPHY_TEST_MODE_LPTX_EN_SEL_BMSK                              0x30
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_DSIPHY_TEST_MODE_LPTX_EN_SEL_SHFT                               0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_DSIPHY_LPRX_DLY_BMSK                                            0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_DSIPHY_LPRX_DLY_SHFT                                            0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_ADDR(n)                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x00000010 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_RMSK                                                           0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_MAXn                                                              3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_ADDR(n),val)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_ADDR(n),mask,val,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_INI(n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_RSVD_7_BMSK                                                    0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_RSVD_7_SHFT                                                     0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_IS_LANE_NUM_BMSK                                               0x60
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_IS_LANE_NUM_SHFT                                                0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_LANE_T_HS_ZERO_ADJ_BMSK                                        0x1f
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_LANE_T_HS_ZERO_ADJ_SHFT                                         0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_ADDR(n)                                         (MMSS_DSI_0_PHY_REG_BASE      + 0x00000014 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_RMSK                                                  0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_MAXn                                                     3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_ADDR(n),val)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_ADDR(n),mask,val,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_INI(n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_ANE_BYPASS_EN_BMSK                                    0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_ANE_BYPASS_EN_SHFT                                     0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_DMUX_LP_SEL_BMSK                                      0x70
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_DMUX_LP_SEL_SHFT                                       0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_DMUX_HS_SEL_BMSK                                       0xc
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_DMUX_HS_SEL_SHFT                                       0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_TEST_MODE_HSTX_EN_BMSK                                 0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_TEST_MODE_HSTX_EN_SHFT                                 0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_FORCE_TEST_MODE_BMSK                                   0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_DATAPATH_FORCE_TEST_MODE_SHFT                                   0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_ADDR(n)                                             (MMSS_DSI_0_PHY_REG_BASE      + 0x00000018 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_MAXn                                                         3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_ADDR(n),val)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_ADDR(n),mask,val,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_INI(n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_DSIPHY_DEBUG_SEL_BMSK                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_DEBUG_SEL_DSIPHY_DEBUG_SEL_SHFT                                      0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_ADDR(n)                                             (MMSS_DSI_0_PHY_REG_BASE      + 0x0000001c + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_RMSK                                                      0xf1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_MAXn                                                         3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_ADDR(n),val)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_ADDR(n),mask,val,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_INI(n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_RSVD_7_BMSK                                               0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_RSVD_7_SHFT                                                0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_PLL_TEST_MODE_SEL_BMSK                                    0x60
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_PLL_TEST_MODE_SEL_SHFT                                     0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_DSIPHY_PLL_TESTMODE_BMSK                                  0x10
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_DSIPHY_PLL_TESTMODE_SHFT                                   0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_DSIPHY_STR_VALUE_OVERRIDE_BMSK                             0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR0_DSIPHY_STR_VALUE_OVERRIDE_SHFT                             0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n)                                             (MMSS_DSI_0_PHY_REG_BASE      + 0x00000020 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_MAXn                                                         3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n),val)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n),mask,val,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_INI(n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_DSIPHY_HSTX_STR_HSTOP_BMSK                                0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_DSIPHY_HSTX_STR_HSTOP_SHFT                                 0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_DSIPHY_HSTX_STR_HSBOT_BMSK                                 0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_DSIPHY_HSTX_STR_HSBOT_SHFT                                 0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_ADDR(n)                                             (MMSS_DSI_0_PHY_REG_BASE      + 0x00000024 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_MAXn                                                         3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_ADDR(n),val)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_ADDR(n),mask,val,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_INI(n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_RSVD_7_BMSK                                               0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_RSVD_7_SHFT                                                0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_MISR_SIG_READ_BMSK                                        0x60
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_MISR_SIG_READ_SHFT                                         0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_MISR_SIG_CLEAR_BMSK                                       0x10
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_MISR_SIG_CLEAR_SHFT                                        0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_RSVD_3_BMSK                                                0x8
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_RSVD_3_SHFT                                                0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_BIST_RX_PRBS_ERROR_INJECT_BMSK                             0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_BIST_RX_PRBS_ERROR_INJECT_SHFT                             0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_BIST_RX_PRBS_GEN_SHORT_BMSK                                0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_BIST_RX_PRBS_GEN_SHORT_SHFT                                0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_DSIPHY_ERROR_CLR_BMSK                                      0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL0_DSIPHY_ERROR_CLR_SHFT                                      0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_ADDR(n)                                             (MMSS_DSI_0_PHY_REG_BASE      + 0x00000028 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_MAXn                                                         3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_ADDR(n),val)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_ADDR(n),mask,val,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_INI(n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_RSVD_7_2_BMSK                                             0xfc
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_RSVD_7_2_SHFT                                              0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_DSIPHY_HSRX_EN_BMSK                                        0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_DSIPHY_HSRX_EN_SHFT                                        0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_DSIPHY_HSRX_TERM_EN_BMSK                                   0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_CTL1_DSIPHY_HSRX_TERM_EN_SHFT                                   0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_ADDR(n)                                     (MMSS_DSI_0_PHY_REG_BASE      + 0x0000002c + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RMSK                                              0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_MAXn                                                 3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RSVD_7_3_BMSK                                     0xf8
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RSVD_7_3_SHFT                                      0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_CHECK_DONE_BMSK                                    0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_CHECK_DONE_SHFT                                    0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RXCHK_HEADER_SEL_BMSK                              0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RXCHK_HEADER_SEL_SHFT                              0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RXCHK_PRBS_START_BMSK                              0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RXCHK_PRBS_START_SHFT                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_ADDR(n)                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x00000030 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_RMSK                                                0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_MAXn                                                   3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSTOP_STATUS_BMSK                   0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSTOP_STATUS_SHFT                    0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSBOT_STATUS_BMSK                    0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSBOT_STATUS_SHFT                    0x0

#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNn_ADDR(n)                                                   (MMSS_DSI_0_PHY_REG_BASE      + 0x00000034 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNn_RMSK                                                            0xff
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNn_MAXn                                                               3
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNn_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNn_ADDR(n), HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNn_RMSK)
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNn_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNn_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNn_RSVD_7_0_BMSK                                                   0xff
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNn_RSVD_7_0_SHFT                                                    0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT0_ADDR(n)                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x00000038 + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT0_RMSK                                                0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT0_MAXn                                                   3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT0_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT0_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT0_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT0_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT0_DSIPHY_BISTCHKERR_MISRSIG_7_0_BMSK                  0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT0_DSIPHY_BISTCHKERR_MISRSIG_7_0_SHFT                   0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT1_ADDR(n)                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x0000003c + 0x40 * (n))
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT1_RMSK                                                0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT1_MAXn                                                   3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT1_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT1_ADDR(n), HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT1_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT1_ADDR(n), mask)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT1_DSIPHY_BISTCHKERR_MISRSIG_15_8_BMSK                 0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_BIST_MISR_STAT1_DSIPHY_BISTCHKERR_MISRSIG_15_8_SHFT                  0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_ADDR                                                    (MMSS_DSI_0_PHY_REG_BASE      + 0x00000100)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_RMSK                                                          0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_RSVD_7_4_BMSK                                                 0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_RSVD_7_4_SHFT                                                  0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_DSIPHY_HSTX_PGM_DLY_BMSK                                       0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_DSIPHY_HSTX_PGM_DLY_SHFT                                       0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_ADDR                                                    (MMSS_DSI_0_PHY_REG_BASE      + 0x00000104)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_RMSK                                                          0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_DSIPHY_HSTX_SLEW_BOT_BMSK                                     0xc0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_DSIPHY_HSTX_SLEW_BOT_SHFT                                      0x6
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_DSIPHY_HSTX_SLEW_TOP_BMSK                                     0x30
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_DSIPHY_HSTX_SLEW_TOP_SHFT                                      0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_RSVD_3_2_BMSK                                                  0xc
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_RSVD_3_2_SHFT                                                  0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_DSIPHY_PEMPH_DLY_SEL_BMSK                                      0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_DSIPHY_PEMPH_DLY_SEL_SHFT                                      0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_ADDR                                                    (MMSS_DSI_0_PHY_REG_BASE      + 0x00000108)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_RMSK                                                          0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_STRBOT_BMSK                                      0xe0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_STRBOT_SHFT                                       0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_RSVD_4_BMSK                                                   0x10
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_RSVD_4_SHFT                                                    0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_STRTOP_BMSK                                       0xe
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_STRTOP_SHFT                                       0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_EN_BMSK                                           0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_EN_SHFT                                           0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_ADDR                                                    (MMSS_DSI_0_PHY_REG_BASE      + 0x0000010c)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_RMSK                                                          0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_IS_CKLANE_BMSK                                                0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_IS_CKLANE_SHFT                                                 0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_IS_LANE_R_BMSK                                                0x40
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_IS_LANE_R_SHFT                                                 0x6
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_DSIPHY_TEST_MODE_LPTX_EN_SEL_BMSK                             0x30
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_DSIPHY_TEST_MODE_LPTX_EN_SEL_SHFT                              0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_DSIPHY_LPRX_DLY_BMSK                                           0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_DSIPHY_LPRX_DLY_SHFT                                           0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_ADDR                                                    (MMSS_DSI_0_PHY_REG_BASE      + 0x00000110)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_RMSK                                                          0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_CLKLANE_HSTX_INV_BMSK                                         0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_CLKLANE_HSTX_INV_SHFT                                          0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_IS_LANE_NUM_BMSK                                              0x60
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_IS_LANE_NUM_SHFT                                               0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_LANE_T_HS_ZERO_ADJ_BMSK                                       0x1f
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_LANE_T_HS_ZERO_ADJ_SHFT                                        0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_ADDR                                           (MMSS_DSI_0_PHY_REG_BASE      + 0x00000114)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_ANE_BYPASS_EN_BMSK                                   0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_ANE_BYPASS_EN_SHFT                                    0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_DMUX_LP_SEL_BMSK                                     0x70
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_DMUX_LP_SEL_SHFT                                      0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_DMUX_HS_SEL_BMSK                                      0xc
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_DMUX_HS_SEL_SHFT                                      0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_TEST_MODE_HSTX_EN_BMSK                                0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_TEST_MODE_HSTX_EN_SHFT                                0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_FORCE_TEST_MODE_BMSK                                  0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_DATAPATH_FORCE_TEST_MODE_SHFT                                  0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_ADDR                                               (MMSS_DSI_0_PHY_REG_BASE      + 0x00000118)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_DSIPHY_DEBUG_SEL_BMSK                                    0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_DEBUG_SEL_DSIPHY_DEBUG_SEL_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_ADDR                                               (MMSS_DSI_0_PHY_REG_BASE      + 0x0000011c)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_RMSK                                                     0xf1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_RSVD_7_BMSK                                              0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_RSVD_7_SHFT                                               0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_PLL_TEST_MODE_SEL_BMSK                                   0x60
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_PLL_TEST_MODE_SEL_SHFT                                    0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_DSIPHY_PLL_TESTMODE_BMSK                                 0x10
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_DSIPHY_PLL_TESTMODE_SHFT                                  0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_DSIPHY_STR_VALUE_OVERRIDE_BMSK                            0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR0_DSIPHY_STR_VALUE_OVERRIDE_SHFT                            0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_ADDR                                               (MMSS_DSI_0_PHY_REG_BASE      + 0x00000120)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_DSIPHY_HSTX_STR_HSTOP_BMSK                               0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_DSIPHY_HSTX_STR_HSTOP_SHFT                                0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_DSIPHY_HSTX_STR_HSBOT_BMSK                                0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_DSIPHY_HSTX_STR_HSBOT_SHFT                                0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_ADDR                                               (MMSS_DSI_0_PHY_REG_BASE      + 0x00000124)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_RSVD_7_BMSK                                              0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_RSVD_7_SHFT                                               0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_MISR_SIG_READ_BMSK                                       0x60
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_MISR_SIG_READ_SHFT                                        0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_MISR_SIG_CLEAR_BMSK                                      0x10
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_MISR_SIG_CLEAR_SHFT                                       0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_RSVD_3_BMSK                                               0x8
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_RSVD_3_SHFT                                               0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_BIST_RX_PRBS_ERROR_INJECT_BMSK                            0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_BIST_RX_PRBS_ERROR_INJECT_SHFT                            0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_BIST_RX_PRBS_GEN_SHORT_BMSK                               0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_BIST_RX_PRBS_GEN_SHORT_SHFT                               0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_DSIPHY_ERROR_CLR_BMSK                                     0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL0_DSIPHY_ERROR_CLR_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_ADDR                                               (MMSS_DSI_0_PHY_REG_BASE      + 0x00000128)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_RSVD_7_2_BMSK                                            0xfc
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_RSVD_7_2_SHFT                                             0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_DSIPHY_HSRX_EN_BMSK                                       0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_DSIPHY_HSRX_EN_SHFT                                       0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_DSIPHY_HSRX_TERM_EN_BMSK                                  0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_CTL1_DSIPHY_HSRX_TERM_EN_SHFT                                  0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_ADDR                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x0000012c)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RMSK                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RSVD_7_3_BMSK                                    0xf8
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RSVD_7_3_SHFT                                     0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_CHECK_DONE_BMSK                                   0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_CHECK_DONE_SHFT                                   0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RXCHK_HEADER_SEL_BMSK                             0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RXCHK_HEADER_SEL_SHFT                             0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RXCHK_PRBS_START_BMSK                             0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RXCHK_PRBS_START_SHFT                             0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_ADDR                                         (MMSS_DSI_0_PHY_REG_BASE      + 0x00000130)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_RMSK                                               0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSTOP_STATUS_BMSK                  0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSTOP_STATUS_SHFT                   0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSBOT_STATUS_BMSK                   0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSBOT_STATUS_SHFT                   0x0

#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNCK_ADDR                                                     (MMSS_DSI_0_PHY_REG_BASE      + 0x00000134)
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNCK_RMSK                                                           0xff
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNCK_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNCK_ADDR, HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNCK_RMSK)
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNCK_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNCK_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNCK_RSVD_7_0_BMSK                                                  0xff
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_LNCK_RSVD_7_0_SHFT                                                   0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_ADDR                                         (MMSS_DSI_0_PHY_REG_BASE      + 0x00000138)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_RMSK                                               0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_DSIPHY_BISTCHKERR_MISRSIG_7_0_BMSK                 0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_DSIPHY_BISTCHKERR_MISRSIG_7_0_SHFT                  0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_ADDR                                         (MMSS_DSI_0_PHY_REG_BASE      + 0x0000013c)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_RMSK                                               0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_DSIPHY_BISTCHKERR_MISRSIG_15_8_BMSK                0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_DSIPHY_BISTCHKERR_MISRSIG_15_8_SHFT                 0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_ADDR                                                (MMSS_DSI_0_PHY_REG_BASE      + 0x00000140)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_DSIPHY_T_CLK_ZERO_BMSK                                    0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_DSIPHY_T_CLK_ZERO_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_ADDR                                                (MMSS_DSI_0_PHY_REG_BASE      + 0x00000144)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_DSIPHY_T_CLK_TRAIL_BMSK                                   0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_DSIPHY_T_CLK_TRAIL_SHFT                                    0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_ADDR                                                (MMSS_DSI_0_PHY_REG_BASE      + 0x00000148)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_DSIPHY_T_CLK_PREPARE_BMSK                                 0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_DSIPHY_T_CLK_PREPARE_SHFT                                  0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_ADDR                                                (MMSS_DSI_0_PHY_REG_BASE      + 0x0000014c)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_RSVD_7_1_BMSK                                             0xfe
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_RSVD_7_1_SHFT                                              0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_DSIPHY_T_CLK_ZERO_8_BMSK                                   0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_DSIPHY_T_CLK_ZERO_8_SHFT                                   0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_ADDR                                                (MMSS_DSI_0_PHY_REG_BASE      + 0x00000150)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_DSIPHY_T_HS_EXIT_BMSK                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_DSIPHY_T_HS_EXIT_SHFT                                      0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_ADDR                                                (MMSS_DSI_0_PHY_REG_BASE      + 0x00000154)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_DSIPHY_T_HS_ZERO_BMSK                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_DSIPHY_T_HS_ZERO_SHFT                                      0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_ADDR                                                (MMSS_DSI_0_PHY_REG_BASE      + 0x00000158)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_DSIPHY_T_HS_PREPARE_BMSK                                  0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_DSIPHY_T_HS_PREPARE_SHFT                                   0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_ADDR                                                (MMSS_DSI_0_PHY_REG_BASE      + 0x0000015c)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_DSIPHY_T_HS_TRAIL_BMSK                                    0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_DSIPHY_T_HS_TRAIL_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_ADDR                                                (MMSS_DSI_0_PHY_REG_BASE      + 0x00000160)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_DSIPHY_T_HS_RQST_BMSK                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_DSIPHY_T_HS_RQST_SHFT                                      0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_ADDR                                                (MMSS_DSI_0_PHY_REG_BASE      + 0x00000164)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_RSVD_7_BMSK                                               0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_RSVD_7_SHFT                                                0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_DSIPHY_T_TA_SURE_BMSK                                     0x70
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_DSIPHY_T_TA_SURE_SHFT                                      0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_RSVD_3_BMSK                                                0x8
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_RSVD_3_SHFT                                                0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_DSIPHY_T_TA_GO_BMSK                                        0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_DSIPHY_T_TA_GO_SHFT                                        0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_ADDR                                               (MMSS_DSI_0_PHY_REG_BASE      + 0x00000168)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_RSVD_7_3_BMSK                                            0xf8
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_RSVD_7_3_SHFT                                             0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_DSIPHY_T_TA_GET_BMSK                                      0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_DSIPHY_T_TA_GET_SHFT                                      0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_ADDR                                               (MMSS_DSI_0_PHY_REG_BASE      + 0x0000016c)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_DSIPHY_TRIG3_CMD_BMSK                                    0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_11_DSIPHY_TRIG3_CMD_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR                                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x00000170)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_RMSK                                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_RSVD_7_BMSK                                                      0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_RSVD_7_SHFT                                                       0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_DIGTOP_PWRDN_B_BMSK                                       0x40
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_DIGTOP_PWRDN_B_SHFT                                        0x6
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_CAL_PWRDN_B_BMSK                                          0x20
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_CAL_PWRDN_B_SHFT                                           0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_DLN3_SHUTDOWNB_BMSK                                       0x10
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_DLN3_SHUTDOWNB_SHFT                                        0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_DLN2_SHUTDOWNB_BMSK                                        0x8
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_DLN2_SHUTDOWNB_SHFT                                        0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_CLK_SHUTDOWNB_BMSK                                         0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_CLK_SHUTDOWNB_SHFT                                         0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_DLN1_SHUTDOWNB_BMSK                                        0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_DLN1_SHUTDOWNB_SHFT                                        0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_DLN0_SHUTDOWNB_BMSK                                        0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_DSIPHY_DLN0_SHUTDOWNB_SHFT                                        0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_ADDR                                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x00000174)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_RMSK                                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_DSIPHY_SW_RESET_BMSK                                             0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_DSIPHY_SW_RESET_SHFT                                              0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_RSVD_6_0_BMSK                                                    0x7f
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_RSVD_6_0_SHFT                                                     0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_ADDR                                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x00000178)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_RMSK                                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_DCC_COMP_WAIT_TIME_BMSK                                          0xc0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_DCC_COMP_WAIT_TIME_SHFT                                           0x6
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_DCC_WAKEUP_TIME_BMSK                                             0x30
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_DCC_WAKEUP_TIME_SHFT                                              0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_RSVD_3_BMSK                                                       0x8
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_RSVD_3_SHFT                                                       0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_DCC_SPEEDUP_EN_BMSK                                               0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_DCC_SPEEDUP_EN_SHFT                                               0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_DCC_SEL_BMSK                                                      0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_DCC_SEL_SHFT                                                      0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_DCC_EN_BMSK                                                       0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_DCC_EN_SHFT                                                       0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_ADDR                                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x0000017c)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_RMSK                                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_RSVD_7_BMSK                                                      0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_RSVD_7_SHFT                                                       0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_DCC_SEL_P_PGM_BMSK                                               0x7f
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_3_DCC_SEL_P_PGM_SHFT                                                0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_ADDR                                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x00000180)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_RMSK                                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_RSVD_7_BMSK                                                      0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_RSVD_7_SHFT                                                       0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_DCC_SEL_N_PGM_BMSK                                               0x7f
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_4_DCC_SEL_N_PGM_SHFT                                                0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR                                              (MMSS_DSI_0_PHY_REG_BASE      + 0x00000184)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_RMSK                                                    0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_N_BMSK                                    0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_N_SHFT                                     0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_P_BMSK                                     0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_P_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_ADDR                                              (MMSS_DSI_0_PHY_REG_BASE      + 0x00000188)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_RMSK                                                    0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_RSVD_7_4_BMSK                                           0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_RSVD_7_4_SHFT                                            0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_3_BMSK                                    0x8
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_3_SHFT                                    0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_2_BMSK                                    0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_2_SHFT                                    0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_1_BMSK                                    0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_1_SHFT                                    0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_RSVD_0_BMSK                                              0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_RSVD_0_SHFT                                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_ADDR                                               (MMSS_DSI_0_PHY_REG_BASE      + 0x0000018c)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_RSVD_7_1_BMSK                                            0xfe
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_RSVD_7_1_SHFT                                             0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_TRIGGER_BMSK                                              0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_TRIGGER_TRIGGER_SHFT                                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_ADDR                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x00000190)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_RMSK                                                        0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_RSVD_7_5_BMSK                                               0xe0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_RSVD_7_5_SHFT                                                0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_SW_CAL_TX_BOT_EN_BMSK                                       0x10
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_SW_CAL_TX_BOT_EN_SHFT                                        0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_RSVD_3_1_BMSK                                                0xe
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_RSVD_3_1_SHFT                                                0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_SW_CAL_TX_TOP_EN_BMSK                                        0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG0_SW_CAL_TX_TOP_EN_SHFT                                        0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_ADDR                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x00000194)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_RMSK                                                        0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_SW_CAL_TX_BOT_STR_BMSK                                      0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_SW_CAL_TX_BOT_STR_SHFT                                       0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_SW_CAL_TX_TOP_STR_BMSK                                       0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG1_SW_CAL_TX_TOP_STR_SHFT                                       0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_ADDR                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x00000198)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_RMSK                                                        0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_RSVD_7_1_BMSK                                               0xfe
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_RSVD_7_1_SHFT                                                0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_CALIBRATION_SEL_BMSK                                         0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_SW_CFG2_CALIBRATION_SEL_SHFT                                         0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_ADDR                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x0000019c)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_RMSK                                                        0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_RSVD_7_5_BMSK                                               0xe0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_RSVD_7_5_SHFT                                                0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_CAL_POLYR_MODE_BMSK                                         0x10
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_CAL_POLYR_MODE_SHFT                                          0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_RSVD_3_1_BMSK                                                0xe
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_RSVD_3_1_SHFT                                                0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_HW_CAL_EN_BMSK                                               0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG0_HW_CAL_EN_SHFT                                               0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_ADDR                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x000001a0)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_RMSK                                                        0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_LOWER_THRESHOLD_BMSK                                        0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_LOWER_THRESHOLD_SHFT                                         0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_UPPER_THRESHOLD_BMSK                                         0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG1_UPPER_THRESHOLD_SHFT                                         0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_ADDR                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x000001a4)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_RMSK                                                        0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_RSVD_7_5_BMSK                                               0xe0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_RSVD_7_5_SHFT                                                0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_PC_STR_TOO_WEAK_COUNT_BMSK                                  0x1f
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG2_PC_STR_TOO_WEAK_COUNT_SHFT                                   0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_ADDR                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x000001a8)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_RMSK                                                        0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_COMPARATOR_SETTLE_TIME_7_0_BMSK                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG3_COMPARATOR_SETTLE_TIME_7_0_SHFT                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_ADDR                                                  (MMSS_DSI_0_PHY_REG_BASE      + 0x000001ac)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_RMSK                                                        0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_RSVD_7_2_BMSK                                               0xfc
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_RSVD_7_2_SHFT                                                0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_COMPARATOR_SETTLE_TIME_9_8_BMSK                              0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_HW_CFG4_COMPARATOR_SETTLE_TIME_9_8_SHFT                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_ADDR                                               (MMSS_DSI_0_PHY_REG_BASE      + 0x000001b0)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_RSVD_7_0_BMSK                                            0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_CAL_TEST_EFUSE_RSVD_7_0_SHFT                                             0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_ADDR                                                   (MMSS_DSI_0_PHY_REG_BASE      + 0x000001b4)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_RMSK                                                         0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_RSVD_7_6_BMSK                                                0xc0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_RSVD_7_6_SHFT                                                 0x6
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_BIST_LP_EN_FIXPAT_BMSK                                       0x20
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_BIST_LP_EN_FIXPAT_SHFT                                        0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_RSVD_4_3_BMSK                                                0x18
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_RSVD_4_3_SHFT                                                 0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_BIST_TX_PATSEL_BMSK                                           0x6
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_BIST_TX_PATSEL_SHFT                                           0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_BIST_EN_TX_PRBS_BMSK                                          0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_BIST_EN_TX_PRBS_SHFT                                          0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_ADDR                                                   (MMSS_DSI_0_PHY_REG_BASE      + 0x000001b8)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_RMSK                                                         0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_RSVD_7_BMSK                                                  0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_RSVD_7_SHFT                                                   0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_BIST_TX_TEST_PAT_SEL_BMSK                                    0x60
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_BIST_TX_TEST_PAT_SEL_SHFT                                     0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_BIST_TX_FIXPAT_SEL_BMSK                                      0x1f
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_BIST_TX_FIXPAT_SEL_SHFT                                       0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_ADDR                                                   (MMSS_DSI_0_PHY_REG_BASE      + 0x000001bc)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_RMSK                                                         0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_BIST_PRBS_POLY_BMSK                                          0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_BIST_PRBS_POLY_SHFT                                           0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_ADDR                                                   (MMSS_DSI_0_PHY_REG_BASE      + 0x000001c0)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_RMSK                                                         0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_BIST_PRBS_SEED_BMSK                                          0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_BIST_PRBS_SEED_SHFT                                           0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_ADDR                                                   (MMSS_DSI_0_PHY_REG_BASE      + 0x000001c4)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_RMSK                                                         0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_RSVD_7_5_BMSK                                                0xe0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_RSVD_7_5_SHFT                                                 0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_DSIPHY_BIST_LNCK_HS_REQUEST_BMSK                             0x10
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_DSIPHY_BIST_LNCK_HS_REQUEST_SHFT                              0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_DSIPHY_BIST_SEL_BMSK                                          0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_DSIPHY_BIST_SEL_SHFT                                          0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_ADDR                                                   (MMSS_DSI_0_PHY_REG_BASE      + 0x000001c8)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_RMSK                                                         0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_RSVD_7_4_BMSK                                                0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_RSVD_7_4_SHFT                                                 0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_BIST_RX_PRBS_CHK_EN_BMSK                                      0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_BIST_RX_PRBS_CHK_EN_SHFT                                      0x0

#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_ADDR                                                   (MMSS_DSI_0_PHY_REG_BASE      + 0x000001cc)
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_RMSK                                                         0xff
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_ADDR, HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_RMSK)
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_IN)
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_RSVD_7_0_BMSK                                                0xff
#define HWIO_MMSS_DSI_0_PHY_RSVD_1_PHY_DIGTOP_RSVD_7_0_SHFT                                                 0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_ADDR                                               (MMSS_DSI_0_PHY_REG_BASE      + 0x000001d0)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_RSVD_7_4_BMSK                                            0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_RSVD_7_4_SHFT                                             0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_DSIPHY_MISR_EN_BMSK                                       0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_MISR_CTRL_DSIPHY_MISR_EN_SHFT                                       0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR                                               (MMSS_DSI_0_PHY_REG_BASE      + 0x000001d4)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_RSVD_7_BMSK                                              0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_RSVD_7_SHFT                                               0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_TEST_ESC_CLK_SEL_BMSK                                    0x40
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_TEST_ESC_CLK_SEL_SHFT                                     0x6
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_TEST_BYTECLK_SEL_BMSK                                    0x30
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_TEST_BYTECLK_SEL_SHFT                                     0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_RSVD_3_1_BMSK                                             0xe
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_RSVD_3_1_SHFT                                             0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_DSIPHY_BITCLK_HS_SEL_BMSK                                 0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_DSIPHY_BITCLK_HS_SEL_SHFT                                 0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_ADDR                                        (MMSS_DSI_0_PHY_REG_BASE      + 0x000001d8)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_RMSK                                              0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_DIGTOP_AMUX_SEL_BMSK                              0xc0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_DIGTOP_AMUX_SEL_SHFT                               0x6
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_DIGTOP_DEBUG_SEL_BMSK                             0x3f
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_DIGTOP_DEBUG_SEL_SHFT                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_ADDR                                                    (MMSS_DSI_0_PHY_REG_BASE      + 0x000001dc)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_RMSK                                                          0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_IN)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_RSVD_7_6_BMSK                                                 0xc0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_RSVD_7_6_SHFT                                                  0x6
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_SEL_BMSK                                             0x38
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_SEL_SHFT                                              0x3
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_DIV_EN_BMSK                                           0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_DIV_EN_SHFT                                           0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_CGM_EN_BMSK                                           0x2
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_CGM_EN_SHFT                                           0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_LDO_MODE_0P4_EN_BMSK                                           0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_LDO_MODE_0P4_EN_SHFT                                           0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_ADDR                                        (MMSS_DSI_0_PHY_REG_BASE      + 0x000001e0)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RMSK                                              0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RSVD_7_5_BMSK                                     0xe0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RSVD_7_5_SHFT                                      0x5
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_HW_CALIBRATOR_BUSY_BMSK                           0x10
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_HW_CALIBRATOR_BUSY_SHFT                            0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RSVD_3_1_BMSK                                      0xe
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RSVD_3_1_SHFT                                      0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_PC_STR_TOO_WEAK_BMSK                               0x1
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS0_PC_STR_TOO_WEAK_SHFT                               0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS1_ADDR                                        (MMSS_DSI_0_PHY_REG_BASE      + 0x000001e4)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS1_RMSK                                              0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS1_HW_CAL_BOT_STR_BMSK                               0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS1_HW_CAL_BOT_STR_SHFT                                0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS1_HW_CAL_TOP_STR_BMSK                                0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REGULATOR_CAL_STATUS1_HW_CAL_TOP_STR_SHFT                                0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_ADDR                                        (MMSS_DSI_0_PHY_REG_BASE      + 0x000001e8)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_RMSK                                              0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_DCC_DONE_SEL_P_BMSK                               0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_DCC_DONE_SEL_P_SHFT                                0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_DCC_SEL_P_STATUS_BMSK                             0x7f
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_DCC_SEL_P_STATUS_SHFT                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_ADDR                                        (MMSS_DSI_0_PHY_REG_BASE      + 0x000001ec)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_RMSK                                              0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_DCC_DONE_SEL_N_BMSK                               0x80
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_DCC_DONE_SEL_N_SHFT                                0x7
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_DCC_SEL_N_STATUS_BMSK                             0x7f
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_DCC_SEL_N_STATUS_SHFT                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_ADDR                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x000001f0)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_RMSK                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_DEBUG_BUS_7_0_BMSK                               0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_DEBUG_BUS_7_0_SHFT                                0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_ADDR                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x000001f4)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_RMSK                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_DEBUG_BUS_15_8_BMSK                              0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_DEBUG_BUS_15_8_SHFT                               0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_ADDR                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x000001f8)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_RMSK                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_DEBUG_BUS_23_16_BMSK                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_DEBUG_BUS_23_16_SHFT                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_ADDR                                       (MMSS_DSI_0_PHY_REG_BASE      + 0x000001fc)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_RMSK                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_DEBUG_BUS_31_24_BMSK                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_DEBUG_BUS_31_24_SHFT                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID0_ADDR                                                 (MMSS_DSI_0_PHY_REG_BASE      + 0x00000200)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID0_RMSK                                                       0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID0_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID0_STEP_7_0_BMSK                                              0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID0_STEP_7_0_SHFT                                               0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID1_ADDR                                                 (MMSS_DSI_0_PHY_REG_BASE      + 0x00000204)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID1_RMSK                                                       0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID1_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID1_STEP_15_8_BMSK                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID1_STEP_15_8_SHFT                                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID2_ADDR                                                 (MMSS_DSI_0_PHY_REG_BASE      + 0x00000208)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID2_RMSK                                                       0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID2_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID2_MINOR_7_0_BMSK                                             0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID2_MINOR_7_0_SHFT                                              0x0

#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID3_ADDR                                                 (MMSS_DSI_0_PHY_REG_BASE      + 0x0000020c)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID3_RMSK                                                       0xff
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID3_ADDR, HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID3_MAJOR_BMSK                                                 0xf0
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID3_MAJOR_SHFT                                                  0x4
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID3_MINOR_11_8_BMSK                                             0xf
#define HWIO_MMSS_DSI_0_PHY_DSIPHY_REVISION_ID3_MINOR_11_8_SHFT                                             0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_DSI_0_PHY_PLL
 *--------------------------------------------------------------------------*/


#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_ADDR                                             (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000000)
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_RMSK                                                   0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_PLACEHOLDER_CHAIN_INPUT_BMSK                           0x80
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_PLACEHOLDER_CHAIN_INPUT_SHFT                            0x7
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_RBLDO_EN_MUX_BMSK                               0x40
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_RBLDO_EN_MUX_SHFT                                0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_RBLDO_EN_BMSK                                   0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_RBLDO_EN_SHFT                                    0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_CM_BMSK                                         0x10
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_CM_SHFT                                          0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_AC_COUPLE_BMSK                                   0x8
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_AC_COUPLE_SHFT                                   0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_GNDTERM_BMSK                                     0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_GNDTERM_SHFT                                     0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_TERM_SEL_BMSK                                    0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_TERM_SEL_SHFT                                    0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_BUF_EN_BMSK                                      0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_SYSCLK_BUF_EN_SHFT                                      0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_ADDR                                           (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000004)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_PLL_VCO_TAIL_MUX_BMSK                                0x80
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_PLL_VCO_TAIL_MUX_SHFT                                 0x7
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_PLL_VCO_TAIL_BMSK                                    0x7c
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_PLL_VCO_TAIL_SHFT                                     0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_PLL_EN_MUX_BMSK                                       0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_PLL_EN_MUX_SHFT                                       0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_PLL_EN_BMSK                                           0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_PLL_EN_SHFT                                           0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_ADDR                                                 (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000008)
#define HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_RMSK                                                        0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_CMN_MODE_BMSK                                               0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_CMN_MODE_SHFT                                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_ADDR                                                  (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000000c)
#define HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_RMSK                                                        0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_IETRIM_BMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_IETRIM_SHFT                                                  0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_ADDR                                                  (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000010)
#define HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_RMSK                                                        0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_IPTRIM_BMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_IPTRIM_SHFT                                                  0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_ADDR                                                (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000014)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_CP_PROP_INSEL_BMSK                                    0x80
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_CP_PROP_INSEL_SHFT                                     0x7
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_IBOOST_BMSK                                           0x40
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_IBOOST_SHFT                                            0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_OCP_EN_BMSK                                               0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_OCP_EN_SHFT                                                0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_SYSCLKD2_BMSK                                         0x10
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_SYSCLKD2_SHFT                                          0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_CPLIN_BMSK                                             0x8
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_CPLIN_SHFT                                             0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_DIV_FFEN_BMSK                                          0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_DIV_FFEN_SHFT                                          0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_DIV_ORD_BMSK                                           0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_PLL_DIV_ORD_SHFT                                           0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_VCO_SEL_BMSK                                               0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_VCO_SEL_SHFT                                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_ADDR                                        (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000018)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_RMSK                                              0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_PLL_CLKOUT_SEL_BMSK                               0x3c
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_PLL_CLKOUT_SEL_SHFT                                0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_PLL_FLIP_DIR_BMSK                                  0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_PLL_FLIP_DIR_SHFT                                  0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_PLL_PHSEL_EN_BMSK                                  0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_PLL_PHSEL_EN_SHFT                                  0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_ADDR                                   (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000001c)
#define HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_RMSK                                         0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_VCCA_TX_SEL_BMSK                             0xe0
#define HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_VCCA_TX_SEL_SHFT                              0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_IPTAT_TRIM_BMSK                              0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_IPTAT_TRIM_SHFT                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_ADDR                                             (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000020)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_RMSK                                                   0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_PLL_PHSEL_DC_BMSK                                      0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_PLL_PHSEL_DC_SHFT                                       0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_ADDR                                              (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000024)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_RMSK                                                    0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_PLL_IPSETI_BMSK                                         0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_PLL_IPSETI_SHFT                                          0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_ADDR                                     (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000028)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_RMSK                                            0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_CORECLK_IN_SYNC_SEL_BMSK                        0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_CORECLK_IN_SYNC_SEL_SHFT                        0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR                                      (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000002c)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_RMSK                                            0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_INVERT_PLL_CAL_TUNE_CODE_BMSK                   0x40
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_INVERT_PLL_CAL_TUNE_CODE_SHFT                    0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_KVCO_CAL_EN_MUX_BMSK                            0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_KVCO_CAL_EN_MUX_SHFT                             0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_KVCO_CAL_EN_BMSK                                0x10
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_KVCO_CAL_EN_SHFT                                 0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_BKG_CAL_EN_MUX_BMSK                              0x8
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_BKG_CAL_EN_MUX_SHFT                              0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_BKG_CAL_EN_BMSK                                  0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_BKG_CAL_EN_SHFT                                  0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_PLL_CAL_EN_MUX_BMSK                              0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_PLL_CAL_EN_MUX_SHFT                              0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_PLL_CAL_EN_BMSK                                  0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_PLL_CAL_EN_SHFT                                  0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR                                      (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000030)
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_RMSK                                            0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_L_EN_MUX_BMSK                            0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_L_EN_MUX_SHFT                             0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_L_EN_BMSK                                0x10
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_L_EN_SHFT                                 0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_R_EN_MUX_BMSK                             0x8
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_R_EN_MUX_SHFT                             0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_R_EN_BMSK                                 0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_R_EN_SHFT                                 0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_BIAS_EN_MUX_BMSK                                 0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_BIAS_EN_MUX_SHFT                                 0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_BIAS_EN_BMSK                                     0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_BIAS_EN_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_ADDR                                              (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000034)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_RMSK                                                    0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_PLL_CPSETI_BMSK                                         0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_PLL_CPSETI_SHFT                                          0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_ADDR                                              (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000038)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_RMSK                                                    0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_PLL_IPSETP_BMSK                                         0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_PLL_IPSETP_SHFT                                          0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_ADDR                                              (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000003c)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_RMSK                                                    0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_PLL_CPSETP_BMSK                                         0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_PLL_CPSETP_SHFT                                          0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_ADDR                                                 (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000040)
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_RMSK                                                       0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_ATB_SEL1_BMSK                                              0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_ATB_SEL1_SHFT                                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_ADDR                                                 (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000044)
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_RMSK                                                       0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_DEBUGBUS_SEL_BMSK                                          0xf0
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_DEBUGBUS_SEL_SHFT                                           0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_ATB_SEL2_BMSK                                               0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_ATB_SEL2_SHFT                                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_ADDR                                     (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000048)
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_RMSK                                           0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_TX_BAND_MUX_BMSK                               0x40
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_TX_BAND_MUX_SHFT                                0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_TX_BAND_BMSK                                   0x30
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_TX_BAND_SHFT                                    0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_SEL_BMSK                                 0xc
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_SEL_SHFT                                 0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_EN_MUX_BMSK                              0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_EN_MUX_SHFT                              0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_EN_BMSK                                  0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_EN_SHFT                                  0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_ADDR                                            (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000004c)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_RMSK                                                  0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_BYPASS_KVCO_BMSK                                      0x80
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_BYPASS_KVCO_SHFT                                       0x7
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_DISABLE_CLKGATE_BMSK                                  0x40
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_DISABLE_CLKGATE_SHFT                                   0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_PLL_UNLOCK_DET_BMSK                                   0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_PLL_UNLOCK_DET_SHFT                                    0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_FRQ_TUNE_MODE_BMSK                                    0x10
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_FRQ_TUNE_MODE_SHFT                                     0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_BYPASS_VCOCAL_BMSK                                     0x8
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_BYPASS_VCOCAL_SHFT                                     0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_BYPASS_RESTRIM_BMSK                                    0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_BYPASS_RESTRIM_SHFT                                    0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_BYPASS_PLLLOCK_BMSK                                    0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_BYPASS_PLLLOCK_SHFT                                    0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_FORCE_PLLLOCK_BMSK                                     0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_FORCE_PLLLOCK_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_ADDR                                           (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000050)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_RMSK                                                  0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_BYPASS_VCOCAL2_BMSK                                   0x8
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_BYPASS_VCOCAL2_SHFT                                   0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_BYPASS_RESTRIM2_BMSK                                  0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_BYPASS_RESTRIM2_SHFT                                  0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_BYPASS_AMP_CAL_BMSK                                   0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_BYPASS_AMP_CAL_SHFT                                   0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_BYPASS_BKG_CAL_BMSK                                   0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_BYPASS_BKG_CAL_SHFT                                   0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_ADDR                                           (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000054)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_RMSK                                                  0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_RESETSM_RESET_BMSK                                    0x8
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_RESETSM_RESET_SHFT                                    0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_CORE_RESET_CSR_BMSK                                   0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_CORE_RESET_CSR_SHFT                                   0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_START_MUX_BMSK                                        0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_START_MUX_SHFT                                        0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_START_BMSK                                            0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_START_SHFT                                            0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_ADDR                                   (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000058)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_RMSK                                         0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_PLL_FAST_TUNE_COUNT_LOW_BMSK                 0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT1_PLL_FAST_TUNE_COUNT_LOW_SHFT                  0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_ADDR                                   (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000005c)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_RMSK                                         0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_PLL_FAST_TUNE_COUNT_HIGH_BMSK                0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_PLL_CAL_COUNT2_PLL_FAST_TUNE_COUNT_HIGH_SHFT                 0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_ADDR                                                 (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000060)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_RMSK                                                       0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_DIV_REF1_BMSK                                              0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_DIV_REF1_SHFT                                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_ADDR                                                 (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000064)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_RMSK                                                        0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_DIV_REF2_BMSK                                               0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_DIV_REF2_SHFT                                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_ADDR                                              (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000068)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_RMSK                                                    0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_KVCO_COUNT1_BMSK                                        0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_KVCO_COUNT1_SHFT                                         0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_ADDR                                              (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000006c)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_RMSK                                                     0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_KVCO_COUNT2_BMSK                                         0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT2_KVCO_COUNT2_SHFT                                         0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_ADDR                                           (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000070)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_RMSK                                                 0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_OPEN_LOOP_MUX_BMSK                                   0x40
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_OPEN_LOOP_MUX_SHFT                                    0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_OPEN_LOOP_BMSK                                       0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_OPEN_LOOP_SHFT                                        0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_POS_KVSLP_BMSK                                       0x10
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_POS_KVSLP_SHFT                                        0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_STARTUP_TIME_BMSK                                     0xc
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_STARTUP_TIME_SHFT                                     0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_WAITTIME_BMSK                                         0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_WAITTIME_SHFT                                         0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_ADDR                                                (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000074)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_RMSK                                                      0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_KVCO_CODE_MUX_BMSK                                        0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_KVCO_CODE_MUX_SHFT                                         0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_KVCO_CODE_BMSK                                            0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_KVCO_CODE_SHFT                                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_ADDR                                                (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000078)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_VREF_CFG1_BMSK                                            0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_VREF_CFG1_SHFT                                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_ADDR                                                (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000007c)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_RMSK                                                       0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_VREF_CFG_MUX_BMSK                                          0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_VREF_CFG_MUX_SHFT                                          0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_VREF_CFG2_BMSK                                             0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_VREF_CFG2_SHFT                                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_ADDR                                                (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000080)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_VREF_CFG3_BMSK                                            0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_VREF_CFG3_SHFT                                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_ADDR                                                (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000084)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_RMSK                                                       0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_VREF_CFG4_BMSK                                             0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_VREF_CFG4_SHFT                                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_ADDR                                                (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000088)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_RMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_VREF_CFG5_BMSK                                            0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG5_VREF_CFG5_SHFT                                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_ADDR                                                (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000008c)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_RMSK                                                       0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_VREF_CFG6_BMSK                                             0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG6_VREF_CFG6_SHFT                                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_ADDR                                             (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000090)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_RMSK                                                   0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_PLLLOCK_CMP1_BMSK                                      0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_PLLLOCK_CMP1_SHFT                                       0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_ADDR                                             (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000094)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_RMSK                                                   0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_PLLLOCK_CMP2_BMSK                                      0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_PLLLOCK_CMP2_SHFT                                       0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_ADDR                                             (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000098)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_RMSK                                                   0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_PLLLOCK_CMP3_BMSK                                      0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_PLLLOCK_CMP3_SHFT                                       0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_ADDR                                           (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000009c)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_RMSK                                                 0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_EN_MUX_BMSK                                  0x40
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_EN_MUX_SHFT                                   0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_EN_BMSK                                      0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_EN_SHFT                                       0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_RNG_BMSK                                     0x18
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_RNG_SHFT                                      0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_CNT_BMSK                                      0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_CNT_SHFT                                      0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_CMP_EN_BMSK                                   0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_CMP_EN_SHFT                                   0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_BGTC_ADDR                                                     (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000a0)
#define HWIO_MMSS_DSI_0_PHY_PLL_BGTC_RMSK                                                           0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_BGTC_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_BGTC_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_BGTC_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_BGTC_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_BGTC_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_BGTC_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_BGTC_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_BGTC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_BGTC_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_BGTC_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_BGTC_BGTC_BMSK                                                      0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_BGTC_BGTC_SHFT                                                       0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_ADDR                                            (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000a4)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_RMSK                                                   0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_PLL_TESTDN_BMSK                                        0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_PLL_TESTDN_SHFT                                        0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_PLL_TESTUP_BMSK                                        0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_PLL_TESTUP_SHFT                                        0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_ADDR                                             (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000a8)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_RMSK                                                   0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_PLL_VCO_TUNE_MUX_BMSK                                  0x80
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_PLL_VCO_TUNE_MUX_SHFT                                   0x7
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_PLL_VCO_TUNE_BMSK                                      0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_PLL_VCO_TUNE_SHFT                                       0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_ADDR                                               (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000ac)
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_DEC_START1_MUX_BMSK                                      0x80
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_DEC_START1_MUX_SHFT                                       0x7
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_DEC_START1_BMSK                                          0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_DEC_START1_SHFT                                           0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_ADDR                                               (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000b0)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_RMSK                                                     0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_AMPSTART_MUX_BMSK                                        0x40
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_AMPSTART_MUX_SHFT                                         0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_AMPSTART_BMSK                                            0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_AMPSTART_SHFT                                             0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_PLL_AMP_OS_BMSK                                          0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_PLL_AMP_OS_SHFT                                           0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_ADDR                                            (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000b4)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_RMSK                                                   0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_SSCCENTER_BMSK                                         0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_SSCCENTER_SHFT                                         0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_SSCEN_BMSK                                             0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_SSCEN_SHFT                                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_ADDR                                             (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000b8)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_RMSK                                                   0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_SSC_ADJPER1_BMSK                                       0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER1_SSC_ADJPER1_SHFT                                        0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_ADDR                                             (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000bc)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_RMSK                                                    0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_SSC_ADJPER2_BMSK                                        0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_ADJ_PER2_SSC_ADJPER2_SHFT                                        0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_ADDR                                                 (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000c0)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_RMSK                                                       0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_SSC_PER1_BMSK                                              0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER1_SSC_PER1_SHFT                                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_ADDR                                                 (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000c4)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_RMSK                                                       0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_SSC_PER2_BMSK                                              0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_PER2_SSC_PER2_SHFT                                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_ADDR                                           (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000c8)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_SSC_STEPSIZE1_BMSK                                   0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE1_SSC_STEPSIZE1_SHFT                                    0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_ADDR                                           (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000cc)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_SSC_STEPSIZE2_BMSK                                   0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_SSC_STEP_SIZE2_SSC_STEPSIZE2_SHFT                                    0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_ADDR                                              (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000d0)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_RMSK                                                    0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_RESCODEUP_MUX_BMSK                                      0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_RESCODEUP_MUX_SHFT                                       0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_RESCODE_UP_BMSK                                         0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_RESCODE_UP_SHFT                                          0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_ADDR                                              (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000d4)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_RMSK                                                    0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_RESCODEDN_MUX_BMSK                                      0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_RESCODEDN_MUX_SHFT                                       0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_RESCODE_DN_BMSK                                         0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_RESCODE_DN_SHFT                                          0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_ADDR                                       (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000d8)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_RMSK                                             0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_RESCODEUP_OFFSET_BMSK                            0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_OFFSET_RESCODEUP_OFFSET_SHFT                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_ADDR                                       (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000dc)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_RMSK                                             0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_RESCODEDN_OFFSET_BMSK                            0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_OFFSET_RESCODEDN_OFFSET_SHFT                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_ADDR                                      (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000e0)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_RMSK                                            0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_RESTRIM_SENSESTATE_SEG1_BMSK                    0x40
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_RESTRIM_SENSESTATE_SEG1_SHFT                     0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_RESTRIM_DIRECTION_SEG1_BMSK                     0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_RESTRIM_DIRECTION_SEG1_SHFT                      0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_RESCODE_START_SEG1_BMSK                         0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_RESCODE_START_SEG1_SHFT                          0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_ADDR                                      (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000e4)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_RMSK                                            0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_RESTRIM_SENSESTATE_SEG2_BMSK                    0x40
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_RESTRIM_SENSESTATE_SEG2_SHFT                     0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_RESTRIM_DIRECTION_SEG2_BMSK                     0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_RESTRIM_DIRECTION_SEG2_SHFT                      0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_RESCODE_START_SEG2_BMSK                         0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_RESCODE_START_SEG2_SHFT                          0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_ADDR                                         (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000e8)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_RMSK                                               0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_RESCODE_CSR_BMSK                                   0xf0
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_RESCODE_CSR_SHFT                                    0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_RESCODE_CAL_BMSK                                    0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_RESCODE_CAL_SHFT                                    0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_ADDR                                                 (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000ec)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_RMSK                                                       0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_RESCODE_MUX_BMSK                                           0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_RESCODE_MUX_SHFT                                            0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_RESCODE_BMSK                                               0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_RESCODE_SHFT                                                0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_ADDR                                         (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000f0)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_RMSK                                               0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEGMENT2_BMSK                              0x30
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEGMENT2_SHFT                               0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEGMENT_BMSK                                0xc
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEGMENT_SHFT                                0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_FREEZE_BMSK                                 0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_FREEZE_SHFT                                 0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEARCH_BMSK                                 0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEARCH_SHFT                                 0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_ADDR                                        (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000f4)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_RMSK                                               0x7
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_STEP_BMSK                                  0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_STEP_SHFT                                  0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_START_MUX_BMSK                             0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_START_MUX_SHFT                             0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_START_BMSK                                 0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_START_SHFT                                 0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_ADDR                                   (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000f8)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RMSK                                         0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_SEEK_MUX_BMSK                                0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_SEEK_MUX_SHFT                                 0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_SEEK_BMSK                                    0x10
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_SEEK_SHFT                                     0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_VCO_CAL_DONE_MUX_BMSK                         0x8
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_VCO_CAL_DONE_MUX_SHFT                         0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_VCO_CAL_DONE_BMSK                             0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_VCO_CAL_DONE_SHFT                             0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RESTRIM_EN_MUX_BMSK                           0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RESTRIM_EN_MUX_SHFT                           0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RESTRIM_EN_BMSK                               0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RESTRIM_EN_SHFT                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_ADDR                                                  (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x000000fc)
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_RMSK                                                        0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_TEST_CLK_EN_BMSK                                            0x10
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_TEST_CLK_EN_SHFT                                             0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_HR_OCLK3_EN_BMSK                                             0x8
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_HR_OCLK3_EN_SHFT                                             0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_HR_OCLK2_EN_BMSK                                             0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_HR_OCLK2_EN_SHFT                                             0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_FAUX_SEL_BMSK                                                0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_FAUX_SEL_SHFT                                                0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_FAUX_EN_BMSK                                                 0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_FAUX_EN_SHFT                                                 0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_ADDR                                          (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000100)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_RMSK                                                0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_DIV_FRAC_START1_MUX_BMSK                            0x80
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_DIV_FRAC_START1_MUX_SHFT                             0x7
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_DIV_FRAC_START1_BMSK                                0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_DIV_FRAC_START1_SHFT                                 0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_ADDR                                          (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000104)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_RMSK                                                0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_DIV_FRAC_START2_MUX_BMSK                            0x80
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_DIV_FRAC_START2_MUX_SHFT                             0x7
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_DIV_FRAC_START2_BMSK                                0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_DIV_FRAC_START2_SHFT                                 0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_ADDR                                          (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000108)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_RMSK                                                0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_DIV_FRAC_START3_MUX_BMSK                            0x40
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_DIV_FRAC_START3_MUX_SHFT                             0x6
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_DIV_FRAC_START3_BMSK                                0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_DIV_FRAC_START3_SHFT                                 0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_ADDR                                               (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000010c)
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_RMSK                                                      0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_DEC_START2_MUX_BMSK                                       0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_DEC_START2_MUX_SHFT                                       0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_DEC_START2_BMSK                                           0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_DEC_START2_SHFT                                           0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_ADDR                                         (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000110)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_RMSK                                               0x3f
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_CLK_EP_EN_FORCE_BMSK                               0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_CLK_EP_EN_FORCE_SHFT                                0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_CLK_EP_EN_BMSK                                     0x10
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_CLK_EP_EN_SHFT                                      0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_TXCLK_EN_MUX_BMSK                               0x8
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_TXCLK_EN_MUX_SHFT                               0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_TXCLK_EN_BMSK                                   0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_TXCLK_EN_SHFT                                   0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_RXCLK_EN_MUX_BMSK                               0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_RXCLK_EN_MUX_SHFT                               0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_RXCLK_EN_BMSK                                   0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_RXCLK_EN_SHFT                                   0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_ADDR                                               (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000114)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_RMSK                                                     0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_PLL_RCTRL_BMSK                                           0xf0
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_PLL_RCTRL_SHFT                                            0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_PLL_CCTRL_BMSK                                            0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_PLL_CCTRL_SHFT                                            0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_ADDR                                             (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000118)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_RMSK                                                   0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_PLL_CLK_EP_DIV_BMSK                                    0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_CLKEPDIV_PLL_CLK_EP_DIV_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_ADDR                                           (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000011c)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_RMSK                                                  0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_PLL_FREQUPDATE_BMSK                                   0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_FREQUPDATE_PLL_FREQUPDATE_SHFT                                   0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_ADDR                                       (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000120)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_RMSK                                             0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_BKGCAL_TRIM_UP_BMSK                              0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_UP_BKGCAL_TRIM_UP_SHFT                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_ADDR                                       (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000124)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_RMSK                                             0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_BKGCAL_TRIM_DN_BMSK                              0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_DN_BKGCAL_TRIM_DN_SHFT                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_ADDR                                      (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000128)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_RMSK                                             0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_BKGCAL_TRIM_UP_MUX_BMSK                          0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_BKGCAL_TRIM_UP_MUX_SHFT                          0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_BKGCAL_TRIM_DN_MUX_BMSK                          0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_TRIM_MUX_BKGCAL_TRIM_DN_MUX_SHFT                          0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_ADDR                                      (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000012c)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_RMSK                                             0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_BKGCAL_VREF_CFG_BMSK                             0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_VREF_CFG_BKGCAL_VREF_CFG_SHFT                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_ADDR                                      (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000130)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_RMSK                                            0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_BKGCAL_DIV_REF1_BMSK                            0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF1_BKGCAL_DIV_REF1_SHFT                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_ADDR                                      (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000134)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_RMSK                                             0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_BKGCAL_DIV_REF2_BMSK                             0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKGCAL_DIV_REF2_BKGCAL_DIV_REF2_SHFT                             0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_ADDR                                                  (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000138)
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_RMSK                                                        0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_MUXADDR_BMSK                                                0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXADDR_MUXADDR_SHFT                                                 0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_ADDR                                     (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000013c)
#define HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_RMSK                                            0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_LP_VCO_CTRL_BMSK                                0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_LP_VCO_CTRL_SHFT                                0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_ADDR                                     (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000140)
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_RMSK                                           0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_LP_CLK_CTRL_BMSK                               0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_LP_CLK_CTRL_SHFT                                0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_ADDR                                         (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000144)
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_RMSK                                               0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_HR_OCLK2_DIV_BMSK                                  0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_HR_OCLK2_DIV_SHFT                                   0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_ADDR                                         (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000148)
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_RMSK                                               0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_IN)
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_HR_OCLK3_DIV_BMSK                                  0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_HR_OCLK3_DIV_SHFT                                   0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_ADDR                                             (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000014c)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_RMSK                                                    0xf
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_PLACEHOLDER_CHAIN_OUTPUT_BMSK                           0x8
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_PLACEHOLDER_CHAIN_OUTPUT_SHFT                           0x3
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_PLL_FREQ_DONE_BMSK                                      0x4
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_PLL_FREQ_DONE_SHFT                                      0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_AMP_DONE_BMSK                                           0x2
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_AMP_DONE_SHFT                                           0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_PLL_VCOHIGH_BMSK                                        0x1
#define HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_HIGH_PLL_VCOHIGH_SHFT                                        0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_RESET_SM_ADDR                                                 (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000150)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESET_SM_RMSK                                                       0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_RESET_SM_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESET_SM_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_RESET_SM_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESET_SM_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_RESET_SM_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_RESET_SM_RESETSM_BMSK                                               0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_RESET_SM_RESETSM_SHFT                                                0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_MUXVAL_ADDR                                                   (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000154)
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXVAL_RMSK                                                         0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXVAL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_MUXVAL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_MUXVAL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXVAL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_MUXVAL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXVAL_MUXVAL_BMSK                                                  0xff
#define HWIO_MMSS_DSI_0_PHY_PLL_MUXVAL_MUXVAL_SHFT                                                   0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_DN_ADDR                                         (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000158)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_DN_RMSK                                               0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_DN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_DN_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_DN_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_DN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_DN_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_DN_CORE_RES_CODE_DN_BMSK                              0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_DN_CORE_RES_CODE_DN_SHFT                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_UP_ADDR                                         (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x0000015c)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_UP_RMSK                                               0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_UP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_UP_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_UP_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_UP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_UP_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_UP_CORE_RES_CODE_UP_BMSK                              0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_RES_CODE_UP_CORE_RES_CODE_UP_SHFT                               0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TUNE_ADDR                                            (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000160)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TUNE_RMSK                                                  0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TUNE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TUNE_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TUNE_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TUNE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TUNE_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TUNE_CORE_VCO_TUNE_BMSK                                    0x7f
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TUNE_CORE_VCO_TUNE_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TAIL_ADDR                                            (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000164)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TAIL_RMSK                                                  0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TAIL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TAIL_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TAIL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TAIL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TAIL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TAIL_CORE_VCO_TAIL_BMSK                                    0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_VCO_TAIL_CORE_VCO_TAIL_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_KVCO_CODE_ADDR                                           (MMSS_DSI_0_PHY_PLL_REG_BASE      + 0x00000168)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_KVCO_CODE_RMSK                                                 0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_KVCO_CODE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_KVCO_CODE_ADDR, HWIO_MMSS_DSI_0_PHY_PLL_CORE_KVCO_CODE_RMSK)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_KVCO_CODE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_PLL_CORE_KVCO_CODE_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_KVCO_CODE_CORE_KVCO_CODE_BMSK                                  0x1f
#define HWIO_MMSS_DSI_0_PHY_PLL_CORE_KVCO_CODE_CORE_KVCO_CODE_SHFT                                   0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_DSI_0_PHY_REG
 *--------------------------------------------------------------------------*/


#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR                                           (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x00000000)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_IN)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_RSVD_7_4_BMSK                                        0xf0
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_RSVD_7_4_SHFT                                         0x4
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_DSIPHY_REG_REF_SEL_BMSK                               0xe
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_DSIPHY_REG_REF_SEL_SHFT                               0x1
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_DSIPHY_REG_EN_BMSK                                    0x1
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_DSIPHY_REG_EN_SHFT                                    0x0

#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR                                           (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x00000004)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_IN)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_DSIPHY_REG_CONT_DISCHARGE_CYCLES_BMSK                0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_DSIPHY_REG_CONT_DISCHARGE_CYCLES_SHFT                 0x0

#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR                                           (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x00000008)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_IN)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_DSIPHY_REG_CONT_CHARGE_CYCLES_BMSK                   0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_DSIPHY_REG_CONT_CHARGE_CYCLES_SHFT                    0x0

#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR                                           (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x0000000c)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_IN)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_RSVD_7_4_BMSK                                        0xf0
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_RSVD_7_4_SHFT                                         0x4
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_DSIPHY_REG_CLK_DIV_RATIO_BMSK                         0xf
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_DSIPHY_REG_CLK_DIV_RATIO_SHFT                         0x0

#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR                                           (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x00000010)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_IN)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RSVD_7_6_BMSK                                        0xc0
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RSVD_7_6_SHFT                                         0x6
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_DIV_EN_BMSK                                          0x20
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_DIV_EN_SHFT                                           0x5
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_NP_CGM_ENABLE_BMSK                                   0x10
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_NP_CGM_ENABLE_SHFT                                    0x4
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RSVD_3_0_BMSK                                         0xf
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RSVD_3_0_SHFT                                         0x0

#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_ADDR                                           (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x00000014)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_IN)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_RSVD_7_5_BMSK                                        0xe0
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_RSVD_7_5_SHFT                                         0x5
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWCTRL_DISCHARGE_EN_BMSK                  0x10
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWCTRL_DISCHARGE_EN_SHFT                   0x4
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_FORCE_PWR_SWITCH_BMSK                  0x8
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_FORCE_PWR_SWITCH_SHFT                  0x3
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_PSW_CTRL_BMSK                          0x4
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_PSW_CTRL_SHFT                          0x2
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_NSW_CTRL_BMSK                          0x2
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_NSW_CTRL_SHFT                          0x1
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_EMI_CTRL_BMSK                          0x1
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_EMI_CTRL_SHFT                          0x0

#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR                                      (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x00000018)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RMSK                                            0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_IN)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RSVD_7_5_BMSK                                   0xe0
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RSVD_7_5_SHFT                                    0x5
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_SW_RESET_BMSK                                   0x10
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_SW_RESET_SHFT                                    0x4
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RSVD_3_1_BMSK                                    0xe
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RSVD_3_1_SHFT                                    0x1
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_DSIPHY_REG_PWRDN_B_BMSK                          0x1
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_DSIPHY_REG_PWRDN_B_SHFT                          0x0

#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_ADDR                                           (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x0000001c)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_IN)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_INIT_DISCHARGE_CYCLES_BMSK                           0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_6_INIT_DISCHARGE_CYCLES_SHFT                            0x0

#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_ADDR                                           (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x00000020)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_RMSK                                                 0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_IN)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_RSVD_7_0_BMSK                                        0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_RSVD_1_RSVD_7_0_SHFT                                         0x0

#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_ADDR                                            (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x00000024)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_RMSK                                                  0xf7
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_ADDR,v)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_ADDR,m,v,HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_IN)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_RSVD_7_3_BMSK                                         0xf0
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_RSVD_7_3_SHFT                                          0x4
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_REG_DEBUG_SEL_BMSK                                     0x7
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_REG_DEBUG_SEL_SHFT                                     0x0

#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT0_ADDR                                              (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x00000028)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT0_RMSK                                                    0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT0_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT0_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT0_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT0_DEBUG_BUS_7_0_BMSK                                      0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT0_DEBUG_BUS_7_0_SHFT                                       0x0

#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT1_ADDR                                              (MMSS_DSI_0_PHY_REG_REG_BASE      + 0x0000002c)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT1_RMSK                                                    0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT1_ADDR, HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT1_RMSK)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT1_ADDR, m)
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT1_DEBUG_BUS_15_8_BMSK                                     0xff
#define HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REG_DBG_STAT1_DEBUG_BUS_15_8_SHFT                                      0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_DSI_1_PHY
 *--------------------------------------------------------------------------*/


#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_ADDR(n)                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x00000000 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_RMSK                                                           0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_MAXn                                                              3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_ADDR(n),val)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_ADDR(n),mask,val,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_INI(n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_RSVD_7_4_BMSK                                                  0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_RSVD_7_4_SHFT                                                   0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_DSIPHY_HSTX_PGM_DLY_BMSK                                        0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG0_DSIPHY_HSTX_PGM_DLY_SHFT                                        0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_ADDR(n)                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x00000004 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_RMSK                                                           0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_MAXn                                                              3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_ADDR(n),val)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_ADDR(n),mask,val,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_INI(n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_DSIPHY_HSTX_SLEW_BOT_BMSK                                      0xc0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_DSIPHY_HSTX_SLEW_BOT_SHFT                                       0x6
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_DSIPHY_HSTX_SLEW_TOP_BMSK                                      0x30
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_DSIPHY_HSTX_SLEW_TOP_SHFT                                       0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_RSVD_3_2_BMSK                                                   0xc
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_RSVD_3_2_SHFT                                                   0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_DSIPHY_PEMPH_DLY_SEL_BMSK                                       0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG1_DSIPHY_PEMPH_DLY_SEL_SHFT                                       0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_ADDR(n)                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x00000008 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_RMSK                                                           0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_MAXn                                                              3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_ADDR(n),val)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_ADDR(n),mask,val,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_INI(n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_STRBOT_BMSK                                       0xe0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_STRBOT_SHFT                                        0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_RSVD_4_BMSK                                                    0x10
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_RSVD_4_SHFT                                                     0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_STRTOP_BMSK                                        0xe
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_STRTOP_SHFT                                        0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_EN_BMSK                                            0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG2_DSIPHY_PEMPH_EN_SHFT                                            0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_ADDR(n)                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x0000000c + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_RMSK                                                           0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_MAXn                                                              3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_ADDR(n),val)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_ADDR(n),mask,val,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_INI(n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_IS_CKLANE_BMSK                                                 0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_IS_CKLANE_SHFT                                                  0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_IS_LANE_R_BMSK                                                 0x40
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_IS_LANE_R_SHFT                                                  0x6
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_DSIPHY_TEST_MODE_LPTX_EN_SEL_BMSK                              0x30
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_DSIPHY_TEST_MODE_LPTX_EN_SEL_SHFT                               0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_DSIPHY_LPRX_DLY_BMSK                                            0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG3_DSIPHY_LPRX_DLY_SHFT                                            0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_ADDR(n)                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x00000010 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_RMSK                                                           0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_MAXn                                                              3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_ADDR(n),val)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_ADDR(n),mask,val,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_INI(n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_RSVD_7_BMSK                                                    0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_RSVD_7_SHFT                                                     0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_IS_LANE_NUM_BMSK                                               0x60
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_IS_LANE_NUM_SHFT                                                0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_LANE_T_HS_ZERO_ADJ_BMSK                                        0x1f
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_CFG4_LANE_T_HS_ZERO_ADJ_SHFT                                         0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_ADDR(n)                                         (MMSS_DSI_1_PHY_REG_BASE      + 0x00000014 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_RMSK                                                  0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_MAXn                                                     3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_ADDR(n),val)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_ADDR(n),mask,val,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_INI(n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_ANE_BYPASS_EN_BMSK                                    0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_ANE_BYPASS_EN_SHFT                                     0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_DMUX_LP_SEL_BMSK                                      0x70
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_DMUX_LP_SEL_SHFT                                       0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_DMUX_HS_SEL_BMSK                                       0xc
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_DMUX_HS_SEL_SHFT                                       0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_TEST_MODE_HSTX_EN_BMSK                                 0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_TEST_MODE_HSTX_EN_SHFT                                 0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_FORCE_TEST_MODE_BMSK                                   0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_DATAPATH_FORCE_TEST_MODE_SHFT                                   0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_ADDR(n)                                             (MMSS_DSI_1_PHY_REG_BASE      + 0x00000018 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_MAXn                                                         3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_ADDR(n),val)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_ADDR(n),mask,val,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_INI(n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_DSIPHY_DEBUG_SEL_BMSK                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_DEBUG_SEL_DSIPHY_DEBUG_SEL_SHFT                                      0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_ADDR(n)                                             (MMSS_DSI_1_PHY_REG_BASE      + 0x0000001c + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_RMSK                                                      0xf1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_MAXn                                                         3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_ADDR(n),val)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_ADDR(n),mask,val,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_INI(n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_RSVD_7_BMSK                                               0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_RSVD_7_SHFT                                                0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_PLL_TEST_MODE_SEL_BMSK                                    0x60
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_PLL_TEST_MODE_SEL_SHFT                                     0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_DSIPHY_PLL_TESTMODE_BMSK                                  0x10
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_DSIPHY_PLL_TESTMODE_SHFT                                   0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_DSIPHY_STR_VALUE_OVERRIDE_BMSK                             0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_DSIPHY_STR_VALUE_OVERRIDE_SHFT                             0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n)                                             (MMSS_DSI_1_PHY_REG_BASE      + 0x00000020 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_MAXn                                                         3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n),val)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n),mask,val,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_INI(n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_DSIPHY_HSTX_STR_HSTOP_BMSK                                0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_DSIPHY_HSTX_STR_HSTOP_SHFT                                 0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_DSIPHY_HSTX_STR_HSBOT_BMSK                                 0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_DSIPHY_HSTX_STR_HSBOT_SHFT                                 0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_ADDR(n)                                             (MMSS_DSI_1_PHY_REG_BASE      + 0x00000024 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_MAXn                                                         3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_ADDR(n),val)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_ADDR(n),mask,val,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_INI(n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_RSVD_7_BMSK                                               0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_RSVD_7_SHFT                                                0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_MISR_SIG_READ_BMSK                                        0x60
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_MISR_SIG_READ_SHFT                                         0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_MISR_SIG_CLEAR_BMSK                                       0x10
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_MISR_SIG_CLEAR_SHFT                                        0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_RSVD_3_BMSK                                                0x8
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_RSVD_3_SHFT                                                0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_BIST_RX_PRBS_ERROR_INJECT_BMSK                             0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_BIST_RX_PRBS_ERROR_INJECT_SHFT                             0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_BIST_RX_PRBS_GEN_SHORT_BMSK                                0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_BIST_RX_PRBS_GEN_SHORT_SHFT                                0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_DSIPHY_ERROR_CLR_BMSK                                      0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL0_DSIPHY_ERROR_CLR_SHFT                                      0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_ADDR(n)                                             (MMSS_DSI_1_PHY_REG_BASE      + 0x00000028 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_MAXn                                                         3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_OUTI(n,val)    \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_ADDR(n),val)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_ADDR(n),mask,val,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_INI(n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_RSVD_7_2_BMSK                                             0xfc
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_RSVD_7_2_SHFT                                              0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_DSIPHY_HSRX_EN_BMSK                                        0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_DSIPHY_HSRX_EN_SHFT                                        0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_DSIPHY_HSRX_TERM_EN_BMSK                                   0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_CTL1_DSIPHY_HSRX_TERM_EN_SHFT                                   0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_ADDR(n)                                     (MMSS_DSI_1_PHY_REG_BASE      + 0x0000002c + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RMSK                                              0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_MAXn                                                 3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RSVD_7_3_BMSK                                     0xf8
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RSVD_7_3_SHFT                                      0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_CHECK_DONE_BMSK                                    0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_CHECK_DONE_SHFT                                    0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RXCHK_HEADER_SEL_BMSK                              0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RXCHK_HEADER_SEL_SHFT                              0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RXCHK_PRBS_START_BMSK                              0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_RXCHK_STATUS_RXCHK_PRBS_START_SHFT                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_ADDR(n)                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x00000030 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_RMSK                                                0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_MAXn                                                   3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSTOP_STATUS_BMSK                   0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSTOP_STATUS_SHFT                    0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSBOT_STATUS_BMSK                    0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSBOT_STATUS_SHFT                    0x0

#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNn_ADDR(n)                                                   (MMSS_DSI_1_PHY_REG_BASE      + 0x00000034 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNn_RMSK                                                            0xff
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNn_MAXn                                                               3
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNn_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNn_ADDR(n), HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNn_RMSK)
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNn_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNn_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNn_RSVD_7_0_BMSK                                                   0xff
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNn_RSVD_7_0_SHFT                                                    0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT0_ADDR(n)                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x00000038 + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT0_RMSK                                                0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT0_MAXn                                                   3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT0_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT0_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT0_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT0_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT0_DSIPHY_BISTCHKERR_MISRSIG_7_0_BMSK                  0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT0_DSIPHY_BISTCHKERR_MISRSIG_7_0_SHFT                   0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT1_ADDR(n)                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x0000003c + 0x40 * (n))
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT1_RMSK                                                0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT1_MAXn                                                   3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT1_INI(n)        \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT1_ADDR(n), HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT1_INMI(n,mask)    \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT1_ADDR(n), mask)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT1_DSIPHY_BISTCHKERR_MISRSIG_15_8_BMSK                 0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_BIST_MISR_STAT1_DSIPHY_BISTCHKERR_MISRSIG_15_8_SHFT                  0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_ADDR                                                    (MMSS_DSI_1_PHY_REG_BASE      + 0x00000100)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_RMSK                                                          0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_RSVD_7_4_BMSK                                                 0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_RSVD_7_4_SHFT                                                  0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_DSIPHY_HSTX_PGM_DLY_BMSK                                       0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG0_DSIPHY_HSTX_PGM_DLY_SHFT                                       0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_ADDR                                                    (MMSS_DSI_1_PHY_REG_BASE      + 0x00000104)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_RMSK                                                          0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_DSIPHY_HSTX_SLEW_BOT_BMSK                                     0xc0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_DSIPHY_HSTX_SLEW_BOT_SHFT                                      0x6
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_DSIPHY_HSTX_SLEW_TOP_BMSK                                     0x30
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_DSIPHY_HSTX_SLEW_TOP_SHFT                                      0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_RSVD_3_2_BMSK                                                  0xc
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_RSVD_3_2_SHFT                                                  0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_DSIPHY_PEMPH_DLY_SEL_BMSK                                      0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG1_DSIPHY_PEMPH_DLY_SEL_SHFT                                      0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_ADDR                                                    (MMSS_DSI_1_PHY_REG_BASE      + 0x00000108)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_RMSK                                                          0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_STRBOT_BMSK                                      0xe0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_STRBOT_SHFT                                       0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_RSVD_4_BMSK                                                   0x10
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_RSVD_4_SHFT                                                    0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_STRTOP_BMSK                                       0xe
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_STRTOP_SHFT                                       0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_EN_BMSK                                           0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG2_DSIPHY_PEMPH_EN_SHFT                                           0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_ADDR                                                    (MMSS_DSI_1_PHY_REG_BASE      + 0x0000010c)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_RMSK                                                          0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_IS_CKLANE_BMSK                                                0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_IS_CKLANE_SHFT                                                 0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_IS_LANE_R_BMSK                                                0x40
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_IS_LANE_R_SHFT                                                 0x6
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_DSIPHY_TEST_MODE_LPTX_EN_SEL_BMSK                             0x30
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_DSIPHY_TEST_MODE_LPTX_EN_SEL_SHFT                              0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_DSIPHY_LPRX_DLY_BMSK                                           0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG3_DSIPHY_LPRX_DLY_SHFT                                           0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_ADDR                                                    (MMSS_DSI_1_PHY_REG_BASE      + 0x00000110)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_RMSK                                                          0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_CLKLANE_HSTX_INV_BMSK                                         0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_CLKLANE_HSTX_INV_SHFT                                          0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_IS_LANE_NUM_BMSK                                              0x60
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_IS_LANE_NUM_SHFT                                               0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_LANE_T_HS_ZERO_ADJ_BMSK                                       0x1f
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_CFG4_LANE_T_HS_ZERO_ADJ_SHFT                                        0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_ADDR                                           (MMSS_DSI_1_PHY_REG_BASE      + 0x00000114)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_ANE_BYPASS_EN_BMSK                                   0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_ANE_BYPASS_EN_SHFT                                    0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_DMUX_LP_SEL_BMSK                                     0x70
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_DMUX_LP_SEL_SHFT                                      0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_DMUX_HS_SEL_BMSK                                      0xc
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_DMUX_HS_SEL_SHFT                                      0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_TEST_MODE_HSTX_EN_BMSK                                0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_TEST_MODE_HSTX_EN_SHFT                                0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_FORCE_TEST_MODE_BMSK                                  0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_DATAPATH_FORCE_TEST_MODE_SHFT                                  0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_ADDR                                               (MMSS_DSI_1_PHY_REG_BASE      + 0x00000118)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_DSIPHY_DEBUG_SEL_BMSK                                    0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_DEBUG_SEL_DSIPHY_DEBUG_SEL_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_ADDR                                               (MMSS_DSI_1_PHY_REG_BASE      + 0x0000011c)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_RMSK                                                     0xf1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_RSVD_7_BMSK                                              0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_RSVD_7_SHFT                                               0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_PLL_TEST_MODE_SEL_BMSK                                   0x60
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_PLL_TEST_MODE_SEL_SHFT                                    0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_DSIPHY_PLL_TESTMODE_BMSK                                 0x10
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_DSIPHY_PLL_TESTMODE_SHFT                                  0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_DSIPHY_STR_VALUE_OVERRIDE_BMSK                            0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_DSIPHY_STR_VALUE_OVERRIDE_SHFT                            0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_ADDR                                               (MMSS_DSI_1_PHY_REG_BASE      + 0x00000120)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_DSIPHY_HSTX_STR_HSTOP_BMSK                               0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_DSIPHY_HSTX_STR_HSTOP_SHFT                                0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_DSIPHY_HSTX_STR_HSBOT_BMSK                                0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_DSIPHY_HSTX_STR_HSBOT_SHFT                                0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_ADDR                                               (MMSS_DSI_1_PHY_REG_BASE      + 0x00000124)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_RSVD_7_BMSK                                              0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_RSVD_7_SHFT                                               0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_MISR_SIG_READ_BMSK                                       0x60
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_MISR_SIG_READ_SHFT                                        0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_MISR_SIG_CLEAR_BMSK                                      0x10
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_MISR_SIG_CLEAR_SHFT                                       0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_RSVD_3_BMSK                                               0x8
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_RSVD_3_SHFT                                               0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_BIST_RX_PRBS_ERROR_INJECT_BMSK                            0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_BIST_RX_PRBS_ERROR_INJECT_SHFT                            0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_BIST_RX_PRBS_GEN_SHORT_BMSK                               0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_BIST_RX_PRBS_GEN_SHORT_SHFT                               0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_DSIPHY_ERROR_CLR_BMSK                                     0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL0_DSIPHY_ERROR_CLR_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_ADDR                                               (MMSS_DSI_1_PHY_REG_BASE      + 0x00000128)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_RSVD_7_2_BMSK                                            0xfc
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_RSVD_7_2_SHFT                                             0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_DSIPHY_HSRX_EN_BMSK                                       0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_DSIPHY_HSRX_EN_SHFT                                       0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_DSIPHY_HSRX_TERM_EN_BMSK                                  0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_CTL1_DSIPHY_HSRX_TERM_EN_SHFT                                  0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_ADDR                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x0000012c)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RMSK                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RSVD_7_3_BMSK                                    0xf8
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RSVD_7_3_SHFT                                     0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_CHECK_DONE_BMSK                                   0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_CHECK_DONE_SHFT                                   0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RXCHK_HEADER_SEL_BMSK                             0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RXCHK_HEADER_SEL_SHFT                             0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RXCHK_PRBS_START_BMSK                             0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_RXCHK_STATUS_RXCHK_PRBS_START_SHFT                             0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_ADDR                                         (MMSS_DSI_1_PHY_REG_BASE      + 0x00000130)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_RMSK                                               0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSTOP_STATUS_BMSK                  0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSTOP_STATUS_SHFT                   0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSBOT_STATUS_BMSK                   0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_HSTX_STR_STATUS_DSIPHY_HSTX_STR_HSBOT_STATUS_SHFT                   0x0

#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNCK_ADDR                                                     (MMSS_DSI_1_PHY_REG_BASE      + 0x00000134)
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNCK_RMSK                                                           0xff
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNCK_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNCK_ADDR, HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNCK_RMSK)
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNCK_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNCK_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNCK_RSVD_7_0_BMSK                                                  0xff
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_LNCK_RSVD_7_0_SHFT                                                   0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_ADDR                                         (MMSS_DSI_1_PHY_REG_BASE      + 0x00000138)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_RMSK                                               0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_DSIPHY_BISTCHKERR_MISRSIG_7_0_BMSK                 0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT0_DSIPHY_BISTCHKERR_MISRSIG_7_0_SHFT                  0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_ADDR                                         (MMSS_DSI_1_PHY_REG_BASE      + 0x0000013c)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_RMSK                                               0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_DSIPHY_BISTCHKERR_MISRSIG_15_8_BMSK                0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_BIST_MISR_STAT1_DSIPHY_BISTCHKERR_MISRSIG_15_8_SHFT                 0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_ADDR                                                (MMSS_DSI_1_PHY_REG_BASE      + 0x00000140)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_DSIPHY_T_CLK_ZERO_BMSK                                    0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_0_DSIPHY_T_CLK_ZERO_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_ADDR                                                (MMSS_DSI_1_PHY_REG_BASE      + 0x00000144)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_DSIPHY_T_CLK_TRAIL_BMSK                                   0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_1_DSIPHY_T_CLK_TRAIL_SHFT                                    0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_ADDR                                                (MMSS_DSI_1_PHY_REG_BASE      + 0x00000148)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_DSIPHY_T_CLK_PREPARE_BMSK                                 0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_2_DSIPHY_T_CLK_PREPARE_SHFT                                  0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_ADDR                                                (MMSS_DSI_1_PHY_REG_BASE      + 0x0000014c)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_RSVD_7_1_BMSK                                             0xfe
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_RSVD_7_1_SHFT                                              0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_DSIPHY_T_CLK_ZERO_8_BMSK                                   0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_3_DSIPHY_T_CLK_ZERO_8_SHFT                                   0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_ADDR                                                (MMSS_DSI_1_PHY_REG_BASE      + 0x00000150)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_DSIPHY_T_HS_EXIT_BMSK                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_4_DSIPHY_T_HS_EXIT_SHFT                                      0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_ADDR                                                (MMSS_DSI_1_PHY_REG_BASE      + 0x00000154)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_DSIPHY_T_HS_ZERO_BMSK                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_5_DSIPHY_T_HS_ZERO_SHFT                                      0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_ADDR                                                (MMSS_DSI_1_PHY_REG_BASE      + 0x00000158)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_DSIPHY_T_HS_PREPARE_BMSK                                  0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_6_DSIPHY_T_HS_PREPARE_SHFT                                   0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_ADDR                                                (MMSS_DSI_1_PHY_REG_BASE      + 0x0000015c)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_DSIPHY_T_HS_TRAIL_BMSK                                    0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_7_DSIPHY_T_HS_TRAIL_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_ADDR                                                (MMSS_DSI_1_PHY_REG_BASE      + 0x00000160)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_DSIPHY_T_HS_RQST_BMSK                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_8_DSIPHY_T_HS_RQST_SHFT                                      0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_ADDR                                                (MMSS_DSI_1_PHY_REG_BASE      + 0x00000164)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_RSVD_7_BMSK                                               0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_RSVD_7_SHFT                                                0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_DSIPHY_T_TA_SURE_BMSK                                     0x70
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_DSIPHY_T_TA_SURE_SHFT                                      0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_RSVD_3_BMSK                                                0x8
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_RSVD_3_SHFT                                                0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_DSIPHY_T_TA_GO_BMSK                                        0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_9_DSIPHY_T_TA_GO_SHFT                                        0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_ADDR                                               (MMSS_DSI_1_PHY_REG_BASE      + 0x00000168)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_RSVD_7_3_BMSK                                            0xf8
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_RSVD_7_3_SHFT                                             0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_DSIPHY_T_TA_GET_BMSK                                      0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_10_DSIPHY_T_TA_GET_SHFT                                      0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_ADDR                                               (MMSS_DSI_1_PHY_REG_BASE      + 0x0000016c)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_DSIPHY_TRIG3_CMD_BMSK                                    0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_TIMING_CTRL_11_DSIPHY_TRIG3_CMD_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_ADDR                                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x00000170)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_RMSK                                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_RSVD_7_BMSK                                                      0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_RSVD_7_SHFT                                                       0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_DIGTOP_PWRDN_B_BMSK                                       0x40
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_DIGTOP_PWRDN_B_SHFT                                        0x6
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_CAL_PWRDN_B_BMSK                                          0x20
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_CAL_PWRDN_B_SHFT                                           0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_DLN3_SHUTDOWNB_BMSK                                       0x10
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_DLN3_SHUTDOWNB_SHFT                                        0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_DLN2_SHUTDOWNB_BMSK                                        0x8
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_DLN2_SHUTDOWNB_SHFT                                        0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_CLK_SHUTDOWNB_BMSK                                         0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_CLK_SHUTDOWNB_SHFT                                         0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_DLN1_SHUTDOWNB_BMSK                                        0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_DLN1_SHUTDOWNB_SHFT                                        0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_DLN0_SHUTDOWNB_BMSK                                        0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_0_DSIPHY_DLN0_SHUTDOWNB_SHFT                                        0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_ADDR                                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x00000174)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_RMSK                                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_DSIPHY_SW_RESET_BMSK                                             0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_DSIPHY_SW_RESET_SHFT                                              0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_RSVD_6_0_BMSK                                                    0x7f
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_1_RSVD_6_0_SHFT                                                     0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_ADDR                                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x00000178)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_RMSK                                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_DCC_COMP_WAIT_TIME_BMSK                                          0xc0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_DCC_COMP_WAIT_TIME_SHFT                                           0x6
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_DCC_WAKEUP_TIME_BMSK                                             0x30
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_DCC_WAKEUP_TIME_SHFT                                              0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_RSVD_3_BMSK                                                       0x8
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_RSVD_3_SHFT                                                       0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_DCC_SPEEDUP_EN_BMSK                                               0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_DCC_SPEEDUP_EN_SHFT                                               0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_DCC_SEL_BMSK                                                      0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_DCC_SEL_SHFT                                                      0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_DCC_EN_BMSK                                                       0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_2_DCC_EN_SHFT                                                       0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_ADDR                                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x0000017c)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_RMSK                                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_RSVD_7_BMSK                                                      0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_RSVD_7_SHFT                                                       0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_DCC_SEL_P_PGM_BMSK                                               0x7f
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_3_DCC_SEL_P_PGM_SHFT                                                0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_ADDR                                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x00000180)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_RMSK                                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_RSVD_7_BMSK                                                      0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_RSVD_7_SHFT                                                       0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_DCC_SEL_N_PGM_BMSK                                               0x7f
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CTRL_4_DCC_SEL_N_PGM_SHFT                                                0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR                                              (MMSS_DSI_1_PHY_REG_BASE      + 0x00000184)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_RMSK                                                    0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_N_BMSK                                    0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_N_SHFT                                     0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_P_BMSK                                     0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_P_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_ADDR                                              (MMSS_DSI_1_PHY_REG_BASE      + 0x00000188)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_RMSK                                                    0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_RSVD_7_4_BMSK                                           0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_RSVD_7_4_SHFT                                            0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_3_BMSK                                    0x8
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_3_SHFT                                    0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_2_BMSK                                    0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_2_SHFT                                    0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_1_BMSK                                    0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_DSIPHY_LN_CTRL_1_SHFT                                    0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_RSVD_0_BMSK                                              0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_STRENGTH_CTRL_1_RSVD_0_SHFT                                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_ADDR                                               (MMSS_DSI_1_PHY_REG_BASE      + 0x0000018c)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_RSVD_7_1_BMSK                                            0xfe
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_RSVD_7_1_SHFT                                             0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_TRIGGER_BMSK                                              0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_TRIGGER_TRIGGER_SHFT                                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_ADDR                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x00000190)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_RMSK                                                        0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_RSVD_7_5_BMSK                                               0xe0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_RSVD_7_5_SHFT                                                0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_SW_CAL_TX_BOT_EN_BMSK                                       0x10
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_SW_CAL_TX_BOT_EN_SHFT                                        0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_RSVD_3_1_BMSK                                                0xe
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_RSVD_3_1_SHFT                                                0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_SW_CAL_TX_TOP_EN_BMSK                                        0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG0_SW_CAL_TX_TOP_EN_SHFT                                        0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_ADDR                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x00000194)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_RMSK                                                        0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_SW_CAL_TX_BOT_STR_BMSK                                      0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_SW_CAL_TX_BOT_STR_SHFT                                       0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_SW_CAL_TX_TOP_STR_BMSK                                       0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG1_SW_CAL_TX_TOP_STR_SHFT                                       0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_ADDR                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x00000198)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_RMSK                                                        0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_RSVD_7_1_BMSK                                               0xfe
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_RSVD_7_1_SHFT                                                0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_CALIBRATION_SEL_BMSK                                         0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_SW_CFG2_CALIBRATION_SEL_SHFT                                         0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_ADDR                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x0000019c)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_RMSK                                                        0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_RSVD_7_5_BMSK                                               0xe0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_RSVD_7_5_SHFT                                                0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_CAL_POLYR_MODE_BMSK                                         0x10
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_CAL_POLYR_MODE_SHFT                                          0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_RSVD_3_1_BMSK                                                0xe
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_RSVD_3_1_SHFT                                                0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_HW_CAL_EN_BMSK                                               0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG0_HW_CAL_EN_SHFT                                               0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_ADDR                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x000001a0)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_RMSK                                                        0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_LOWER_THRESHOLD_BMSK                                        0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_LOWER_THRESHOLD_SHFT                                         0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_UPPER_THRESHOLD_BMSK                                         0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG1_UPPER_THRESHOLD_SHFT                                         0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_ADDR                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x000001a4)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_RMSK                                                        0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_RSVD_7_5_BMSK                                               0xe0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_RSVD_7_5_SHFT                                                0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_PC_STR_TOO_WEAK_COUNT_BMSK                                  0x1f
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG2_PC_STR_TOO_WEAK_COUNT_SHFT                                   0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_ADDR                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x000001a8)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_RMSK                                                        0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_COMPARATOR_SETTLE_TIME_7_0_BMSK                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG3_COMPARATOR_SETTLE_TIME_7_0_SHFT                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_ADDR                                                  (MMSS_DSI_1_PHY_REG_BASE      + 0x000001ac)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_RMSK                                                        0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_RSVD_7_2_BMSK                                               0xfc
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_RSVD_7_2_SHFT                                                0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_COMPARATOR_SETTLE_TIME_9_8_BMSK                              0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_HW_CFG4_COMPARATOR_SETTLE_TIME_9_8_SHFT                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_ADDR                                               (MMSS_DSI_1_PHY_REG_BASE      + 0x000001b0)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_RSVD_7_0_BMSK                                            0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_CAL_TEST_EFUSE_RSVD_7_0_SHFT                                             0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_ADDR                                                   (MMSS_DSI_1_PHY_REG_BASE      + 0x000001b4)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_RMSK                                                         0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_RSVD_7_6_BMSK                                                0xc0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_RSVD_7_6_SHFT                                                 0x6
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_BIST_LP_EN_FIXPAT_BMSK                                       0x20
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_BIST_LP_EN_FIXPAT_SHFT                                        0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_RSVD_4_3_BMSK                                                0x18
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_RSVD_4_3_SHFT                                                 0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_BIST_TX_PATSEL_BMSK                                           0x6
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_BIST_TX_PATSEL_SHFT                                           0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_BIST_EN_TX_PRBS_BMSK                                          0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL0_BIST_EN_TX_PRBS_SHFT                                          0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_ADDR                                                   (MMSS_DSI_1_PHY_REG_BASE      + 0x000001b8)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_RMSK                                                         0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_RSVD_7_BMSK                                                  0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_RSVD_7_SHFT                                                   0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_BIST_TX_TEST_PAT_SEL_BMSK                                    0x60
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_BIST_TX_TEST_PAT_SEL_SHFT                                     0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_BIST_TX_FIXPAT_SEL_BMSK                                      0x1f
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL1_BIST_TX_FIXPAT_SEL_SHFT                                       0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_ADDR                                                   (MMSS_DSI_1_PHY_REG_BASE      + 0x000001bc)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_RMSK                                                         0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_BIST_PRBS_POLY_BMSK                                          0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL2_BIST_PRBS_POLY_SHFT                                           0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_ADDR                                                   (MMSS_DSI_1_PHY_REG_BASE      + 0x000001c0)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_RMSK                                                         0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_BIST_PRBS_SEED_BMSK                                          0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL3_BIST_PRBS_SEED_SHFT                                           0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_ADDR                                                   (MMSS_DSI_1_PHY_REG_BASE      + 0x000001c4)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_RMSK                                                         0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_RSVD_7_5_BMSK                                                0xe0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_RSVD_7_5_SHFT                                                 0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_DSIPHY_BIST_LNCK_HS_REQUEST_BMSK                             0x10
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_DSIPHY_BIST_LNCK_HS_REQUEST_SHFT                              0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_DSIPHY_BIST_SEL_BMSK                                          0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL4_DSIPHY_BIST_SEL_SHFT                                          0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_ADDR                                                   (MMSS_DSI_1_PHY_REG_BASE      + 0x000001c8)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_RMSK                                                         0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_RSVD_7_4_BMSK                                                0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_RSVD_7_4_SHFT                                                 0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_BIST_RX_PRBS_CHK_EN_BMSK                                      0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_BIST_CTRL5_BIST_RX_PRBS_CHK_EN_SHFT                                      0x0

#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_ADDR                                                   (MMSS_DSI_1_PHY_REG_BASE      + 0x000001cc)
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_RMSK                                                         0xff
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_ADDR, HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_RMSK)
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_IN)
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_RSVD_7_0_BMSK                                                0xff
#define HWIO_MMSS_DSI_1_PHY_RSVD_1_PHY_DIGTOP_RSVD_7_0_SHFT                                                 0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_ADDR                                               (MMSS_DSI_1_PHY_REG_BASE      + 0x000001d0)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_RSVD_7_4_BMSK                                            0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_RSVD_7_4_SHFT                                             0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_DSIPHY_MISR_EN_BMSK                                       0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_MISR_CTRL_DSIPHY_MISR_EN_SHFT                                       0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR                                               (MMSS_DSI_1_PHY_REG_BASE      + 0x000001d4)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_RSVD_7_BMSK                                              0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_RSVD_7_SHFT                                               0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_TEST_ESC_CLK_SEL_BMSK                                    0x40
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_TEST_ESC_CLK_SEL_SHFT                                     0x6
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_TEST_BYTECLK_SEL_BMSK                                    0x30
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_TEST_BYTECLK_SEL_SHFT                                     0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_RSVD_3_1_BMSK                                             0xe
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_RSVD_3_1_SHFT                                             0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_DSIPHY_BITCLK_HS_SEL_BMSK                                 0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_TEST_CTRL_DSIPHY_BITCLK_HS_SEL_SHFT                                 0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_ADDR                                        (MMSS_DSI_1_PHY_REG_BASE      + 0x000001d8)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_RMSK                                              0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_DIGTOP_AMUX_SEL_BMSK                              0xc0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_DIGTOP_AMUX_SEL_SHFT                               0x6
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_DIGTOP_DEBUG_SEL_BMSK                             0x3f
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_DIGTOP_DEBUG_SEL_DIGTOP_DEBUG_SEL_SHFT                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_ADDR                                                    (MMSS_DSI_1_PHY_REG_BASE      + 0x000001dc)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_RMSK                                                          0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_IN)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_RSVD_7_6_BMSK                                                 0xc0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_RSVD_7_6_SHFT                                                  0x6
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_SEL_BMSK                                             0x38
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_SEL_SHFT                                              0x3
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_DIV_EN_BMSK                                           0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_DIV_EN_SHFT                                           0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_CGM_EN_BMSK                                           0x2
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_LDO_VREF_CGM_EN_SHFT                                           0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_LDO_MODE_0P4_EN_BMSK                                           0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_LDO_CNTRL_LDO_MODE_0P4_EN_SHFT                                           0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_ADDR                                        (MMSS_DSI_1_PHY_REG_BASE      + 0x000001e0)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RMSK                                              0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RSVD_7_5_BMSK                                     0xe0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RSVD_7_5_SHFT                                      0x5
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_HW_CALIBRATOR_BUSY_BMSK                           0x10
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_HW_CALIBRATOR_BUSY_SHFT                            0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RSVD_3_1_BMSK                                      0xe
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_RSVD_3_1_SHFT                                      0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_PC_STR_TOO_WEAK_BMSK                               0x1
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS0_PC_STR_TOO_WEAK_SHFT                               0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS1_ADDR                                        (MMSS_DSI_1_PHY_REG_BASE      + 0x000001e4)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS1_RMSK                                              0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS1_HW_CAL_BOT_STR_BMSK                               0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS1_HW_CAL_BOT_STR_SHFT                                0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS1_HW_CAL_TOP_STR_BMSK                                0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REGULATOR_CAL_STATUS1_HW_CAL_TOP_STR_SHFT                                0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_ADDR                                        (MMSS_DSI_1_PHY_REG_BASE      + 0x000001e8)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_RMSK                                              0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_DCC_DONE_SEL_P_BMSK                               0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_DCC_DONE_SEL_P_SHFT                                0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_DCC_SEL_P_STATUS_BMSK                             0x7f
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_P_DCC_SEL_P_STATUS_SHFT                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_ADDR                                        (MMSS_DSI_1_PHY_REG_BASE      + 0x000001ec)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_RMSK                                              0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_DCC_DONE_SEL_N_BMSK                               0x80
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_DCC_DONE_SEL_N_SHFT                                0x7
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_DCC_SEL_N_STATUS_BMSK                             0x7f
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DCC_SEL_N_DCC_SEL_N_STATUS_SHFT                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_ADDR                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x000001f0)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_RMSK                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_DEBUG_BUS_7_0_BMSK                               0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS0_DEBUG_BUS_7_0_SHFT                                0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_ADDR                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x000001f4)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_RMSK                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_DEBUG_BUS_15_8_BMSK                              0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS1_DEBUG_BUS_15_8_SHFT                               0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_ADDR                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x000001f8)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_RMSK                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_DEBUG_BUS_23_16_BMSK                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS2_DEBUG_BUS_23_16_SHFT                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_ADDR                                       (MMSS_DSI_1_PHY_REG_BASE      + 0x000001fc)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_RMSK                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_DEBUG_BUS_31_24_BMSK                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_GLBL_STATUS_DEBUG_BUS3_DEBUG_BUS_31_24_SHFT                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID0_ADDR                                                 (MMSS_DSI_1_PHY_REG_BASE      + 0x00000200)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID0_RMSK                                                       0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID0_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID0_STEP_7_0_BMSK                                              0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID0_STEP_7_0_SHFT                                               0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID1_ADDR                                                 (MMSS_DSI_1_PHY_REG_BASE      + 0x00000204)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID1_RMSK                                                       0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID1_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID1_STEP_15_8_BMSK                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID1_STEP_15_8_SHFT                                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID2_ADDR                                                 (MMSS_DSI_1_PHY_REG_BASE      + 0x00000208)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID2_RMSK                                                       0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID2_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID2_MINOR_7_0_BMSK                                             0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID2_MINOR_7_0_SHFT                                              0x0

#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID3_ADDR                                                 (MMSS_DSI_1_PHY_REG_BASE      + 0x0000020c)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID3_RMSK                                                       0xff
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID3_ADDR, HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID3_MAJOR_BMSK                                                 0xf0
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID3_MAJOR_SHFT                                                  0x4
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID3_MINOR_11_8_BMSK                                             0xf
#define HWIO_MMSS_DSI_1_PHY_DSIPHY_REVISION_ID3_MINOR_11_8_SHFT                                             0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_DSI_1_PHY_PLL
 *--------------------------------------------------------------------------*/


#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_ADDR                                             (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000000)
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_RMSK                                                   0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_PLACEHOLDER_CHAIN_INPUT_BMSK                           0x80
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_PLACEHOLDER_CHAIN_INPUT_SHFT                            0x7
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_RBLDO_EN_MUX_BMSK                               0x40
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_RBLDO_EN_MUX_SHFT                                0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_RBLDO_EN_BMSK                                   0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_RBLDO_EN_SHFT                                    0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_CM_BMSK                                         0x10
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_CM_SHFT                                          0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_AC_COUPLE_BMSK                                   0x8
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_AC_COUPLE_SHFT                                   0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_GNDTERM_BMSK                                     0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_GNDTERM_SHFT                                     0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_TERM_SEL_BMSK                                    0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_TERM_SEL_SHFT                                    0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_BUF_EN_BMSK                                      0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_SYS_CLK_CTRL_SYSCLK_BUF_EN_SHFT                                      0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_ADDR                                           (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000004)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_PLL_VCO_TAIL_MUX_BMSK                                0x80
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_PLL_VCO_TAIL_MUX_SHFT                                 0x7
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_PLL_VCO_TAIL_BMSK                                    0x7c
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_PLL_VCO_TAIL_SHFT                                     0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_PLL_EN_MUX_BMSK                                       0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_PLL_EN_MUX_SHFT                                       0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_PLL_EN_BMSK                                           0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_PLL_EN_SHFT                                           0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_ADDR                                                 (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000008)
#define HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_RMSK                                                        0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_CMN_MODE_BMSK                                               0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_CMN_MODE_CMN_MODE_SHFT                                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_ADDR                                                  (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000000c)
#define HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_RMSK                                                        0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_IETRIM_BMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_IE_TRIM_IETRIM_SHFT                                                  0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_ADDR                                                  (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000010)
#define HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_RMSK                                                        0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_IPTRIM_BMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_IP_TRIM_IPTRIM_SHFT                                                  0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_ADDR                                                (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000014)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_CP_PROP_INSEL_BMSK                                    0x80
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_CP_PROP_INSEL_SHFT                                     0x7
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_IBOOST_BMSK                                           0x40
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_IBOOST_SHFT                                            0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_OCP_EN_BMSK                                               0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_OCP_EN_SHFT                                                0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_SYSCLKD2_BMSK                                         0x10
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_SYSCLKD2_SHFT                                          0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_CPLIN_BMSK                                             0x8
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_CPLIN_SHFT                                             0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_DIV_FFEN_BMSK                                          0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_DIV_FFEN_SHFT                                          0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_DIV_ORD_BMSK                                           0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_PLL_DIV_ORD_SHFT                                           0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_VCO_SEL_BMSK                                               0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CNTRL_VCO_SEL_SHFT                                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_ADDR                                        (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000018)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_RMSK                                              0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_PLL_CLKOUT_SEL_BMSK                               0x3c
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_PLL_CLKOUT_SEL_SHFT                                0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_PLL_FLIP_DIR_BMSK                                  0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_PLL_FLIP_DIR_SHFT                                  0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_PLL_PHSEL_EN_BMSK                                  0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_CONTROL_PLL_PHSEL_EN_SHFT                                  0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_ADDR                                   (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000001c)
#define HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_RMSK                                         0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_VCCA_TX_SEL_BMSK                             0xe0
#define HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_VCCA_TX_SEL_SHFT                              0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_IPTAT_TRIM_BMSK                              0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_IPTAT_TRIM_SHFT                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_ADDR                                             (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000020)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_RMSK                                                   0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_PLL_PHSEL_DC_BMSK                                      0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_PHSEL_DC_PLL_PHSEL_DC_SHFT                                       0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_ADDR                                              (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000024)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_RMSK                                                    0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_PLL_IPSETI_BMSK                                         0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETI_PLL_IPSETI_SHFT                                          0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_ADDR                                     (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000028)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_RMSK                                            0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_CORECLK_IN_SYNC_SEL_BMSK                        0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_CLK_IN_SYNC_SEL_CORECLK_IN_SYNC_SEL_SHFT                        0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR                                      (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000002c)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_RMSK                                            0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_INVERT_PLL_CAL_TUNE_CODE_BMSK                   0x40
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_INVERT_PLL_CAL_TUNE_CODE_SHFT                    0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_KVCO_CAL_EN_MUX_BMSK                            0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_KVCO_CAL_EN_MUX_SHFT                             0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_KVCO_CAL_EN_BMSK                                0x10
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_KVCO_CAL_EN_SHFT                                 0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_BKG_CAL_EN_MUX_BMSK                              0x8
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_BKG_CAL_EN_MUX_SHFT                              0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_BKG_CAL_EN_BMSK                                  0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_BKG_CAL_EN_SHFT                                  0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_PLL_CAL_EN_MUX_BMSK                              0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_PLL_CAL_EN_MUX_SHFT                              0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_PLL_CAL_EN_BMSK                                  0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKG_KVCO_CAL_EN_PLL_CAL_EN_SHFT                                  0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR                                      (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000030)
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_RMSK                                            0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_L_EN_MUX_BMSK                            0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_L_EN_MUX_SHFT                             0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_L_EN_BMSK                                0x10
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_L_EN_SHFT                                 0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_R_EN_MUX_BMSK                             0x8
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_R_EN_MUX_SHFT                             0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_R_EN_BMSK                                 0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_CLKBUF_R_EN_SHFT                                 0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_BIAS_EN_MUX_BMSK                                 0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_BIAS_EN_MUX_SHFT                                 0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_BIAS_EN_BMSK                                     0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_BIAS_EN_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_ADDR                                              (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000034)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_RMSK                                                    0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_PLL_CPSETI_BMSK                                         0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETI_PLL_CPSETI_SHFT                                          0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_ADDR                                              (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000038)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_RMSK                                                    0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_PLL_IPSETP_BMSK                                         0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_IP_SETP_PLL_IPSETP_SHFT                                          0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_ADDR                                              (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000003c)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_RMSK                                                    0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_PLL_CPSETP_BMSK                                         0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CP_SETP_PLL_CPSETP_SHFT                                          0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_ADDR                                                 (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000040)
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_RMSK                                                       0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_ATB_SEL1_BMSK                                              0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL1_ATB_SEL1_SHFT                                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_ADDR                                                 (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000044)
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_RMSK                                                       0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_DEBUGBUS_SEL_BMSK                                          0xf0
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_DEBUGBUS_SEL_SHFT                                           0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_ATB_SEL2_BMSK                                               0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_ATB_SEL2_ATB_SEL2_SHFT                                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_ADDR                                     (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000048)
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_RMSK                                           0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_TX_BAND_MUX_BMSK                               0x40
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_TX_BAND_MUX_SHFT                                0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_TX_BAND_BMSK                                   0x30
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_TX_BAND_SHFT                                    0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_SEL_BMSK                                 0xc
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_SEL_SHFT                                 0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_EN_MUX_BMSK                              0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_EN_MUX_SHFT                              0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_EN_BMSK                                  0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_SYSCLK_EN_SEL_TXBAND_SYSCLK_EN_SHFT                                  0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_ADDR                                            (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000004c)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_RMSK                                                  0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_BYPASS_KVCO_BMSK                                      0x80
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_BYPASS_KVCO_SHFT                                       0x7
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_DISABLE_CLKGATE_BMSK                                  0x40
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_DISABLE_CLKGATE_SHFT                                   0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_PLL_UNLOCK_DET_BMSK                                   0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_PLL_UNLOCK_DET_SHFT                                    0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_FRQ_TUNE_MODE_BMSK                                    0x10
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_FRQ_TUNE_MODE_SHFT                                     0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_BYPASS_VCOCAL_BMSK                                     0x8
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_BYPASS_VCOCAL_SHFT                                     0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_BYPASS_RESTRIM_BMSK                                    0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_BYPASS_RESTRIM_SHFT                                    0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_BYPASS_PLLLOCK_BMSK                                    0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_BYPASS_PLLLOCK_SHFT                                    0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_FORCE_PLLLOCK_BMSK                                     0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL_FORCE_PLLLOCK_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_ADDR                                           (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000050)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_RMSK                                                  0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_BYPASS_VCOCAL2_BMSK                                   0x8
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_BYPASS_VCOCAL2_SHFT                                   0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_BYPASS_RESTRIM2_BMSK                                  0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_BYPASS_RESTRIM2_SHFT                                  0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_BYPASS_AMP_CAL_BMSK                                   0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_BYPASS_AMP_CAL_SHFT                                   0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_BYPASS_BKG_CAL_BMSK                                   0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL2_BYPASS_BKG_CAL_SHFT                                   0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_ADDR                                           (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000054)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_RMSK                                                  0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_RESETSM_RESET_BMSK                                    0x8
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_RESETSM_RESET_SHFT                                    0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_CORE_RESET_CSR_BMSK                                   0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_CORE_RESET_CSR_SHFT                                   0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_START_MUX_BMSK                                        0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_START_MUX_SHFT                                        0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_START_BMSK                                            0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_START_SHFT                                            0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_ADDR                                   (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000058)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_RMSK                                         0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_PLL_FAST_TUNE_COUNT_LOW_BMSK                 0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT1_PLL_FAST_TUNE_COUNT_LOW_SHFT                  0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_ADDR                                   (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000005c)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_RMSK                                         0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_PLL_FAST_TUNE_COUNT_HIGH_BMSK                0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_PLL_CAL_COUNT2_PLL_FAST_TUNE_COUNT_HIGH_SHFT                 0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_ADDR                                                 (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000060)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_RMSK                                                       0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_DIV_REF1_BMSK                                              0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF1_DIV_REF1_SHFT                                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_ADDR                                                 (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000064)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_RMSK                                                        0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_DIV_REF2_BMSK                                               0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_REF2_DIV_REF2_SHFT                                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_ADDR                                              (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000068)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_RMSK                                                    0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_KVCO_COUNT1_BMSK                                        0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT1_KVCO_COUNT1_SHFT                                         0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_ADDR                                              (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000006c)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_RMSK                                                     0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_KVCO_COUNT2_BMSK                                         0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_COUNT2_KVCO_COUNT2_SHFT                                         0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_ADDR                                           (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000070)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_RMSK                                                 0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_OPEN_LOOP_MUX_BMSK                                   0x40
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_OPEN_LOOP_MUX_SHFT                                    0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_OPEN_LOOP_BMSK                                       0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_OPEN_LOOP_SHFT                                        0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_POS_KVSLP_BMSK                                       0x10
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_POS_KVSLP_SHFT                                        0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_STARTUP_TIME_BMSK                                     0xc
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_STARTUP_TIME_SHFT                                     0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_WAITTIME_BMSK                                         0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CAL_CNTRL_WAITTIME_SHFT                                         0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_ADDR                                                (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000074)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_RMSK                                                      0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_KVCO_CODE_MUX_BMSK                                        0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_KVCO_CODE_MUX_SHFT                                         0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_KVCO_CODE_BMSK                                            0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_KVCO_CODE_KVCO_CODE_SHFT                                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_ADDR                                                (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000078)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_VREF_CFG1_BMSK                                            0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG1_VREF_CFG1_SHFT                                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_ADDR                                                (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000007c)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_RMSK                                                       0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_VREF_CFG_MUX_BMSK                                          0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_VREF_CFG_MUX_SHFT                                          0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_VREF_CFG2_BMSK                                             0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG2_VREF_CFG2_SHFT                                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_ADDR                                                (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000080)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_VREF_CFG3_BMSK                                            0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG3_VREF_CFG3_SHFT                                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_ADDR                                                (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000084)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_RMSK                                                       0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_VREF_CFG4_BMSK                                             0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG4_VREF_CFG4_SHFT                                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_ADDR                                                (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000088)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_RMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_VREF_CFG5_BMSK                                            0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG5_VREF_CFG5_SHFT                                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_ADDR                                                (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000008c)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_RMSK                                                       0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_VREF_CFG6_BMSK                                             0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_VREF_CFG6_VREF_CFG6_SHFT                                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_ADDR                                             (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000090)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_RMSK                                                   0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_PLLLOCK_CMP1_BMSK                                      0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP1_PLLLOCK_CMP1_SHFT                                       0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_ADDR                                             (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000094)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_RMSK                                                   0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_PLLLOCK_CMP2_BMSK                                      0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP2_PLLLOCK_CMP2_SHFT                                       0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_ADDR                                             (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000098)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_RMSK                                                   0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_PLLLOCK_CMP3_BMSK                                      0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP3_PLLLOCK_CMP3_SHFT                                       0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_ADDR                                           (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000009c)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_RMSK                                                 0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_EN_MUX_BMSK                                  0x40
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_EN_MUX_SHFT                                   0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_EN_BMSK                                      0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_EN_SHFT                                       0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_RNG_BMSK                                     0x18
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_RNG_SHFT                                      0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_CNT_BMSK                                      0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_CNT_SHFT                                      0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_CMP_EN_BMSK                                   0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLLLOCK_CMP_EN_PLLLOCK_CMP_EN_SHFT                                   0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_BGTC_ADDR                                                     (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000a0)
#define HWIO_MMSS_DSI_1_PHY_PLL_BGTC_RMSK                                                           0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_BGTC_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_BGTC_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_BGTC_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_BGTC_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_BGTC_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_BGTC_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_BGTC_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_BGTC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_BGTC_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_BGTC_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_BGTC_BGTC_BMSK                                                      0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_BGTC_BGTC_SHFT                                                       0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_ADDR                                            (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000a4)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_RMSK                                                   0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_PLL_TESTDN_BMSK                                        0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_PLL_TESTDN_SHFT                                        0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_PLL_TESTUP_BMSK                                        0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_TEST_UPDN_PLL_TESTUP_SHFT                                        0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_ADDR                                             (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000a8)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_RMSK                                                   0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_PLL_VCO_TUNE_MUX_BMSK                                  0x80
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_PLL_VCO_TUNE_MUX_SHFT                                   0x7
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_PLL_VCO_TUNE_BMSK                                      0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_TUNE_PLL_VCO_TUNE_SHFT                                       0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_ADDR                                               (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000ac)
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_DEC_START1_MUX_BMSK                                      0x80
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_DEC_START1_MUX_SHFT                                       0x7
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_DEC_START1_BMSK                                          0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START1_DEC_START1_SHFT                                           0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_ADDR                                               (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000b0)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_RMSK                                                     0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_AMPSTART_MUX_BMSK                                        0x40
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_AMPSTART_MUX_SHFT                                         0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_AMPSTART_BMSK                                            0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_AMPSTART_SHFT                                             0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_PLL_AMP_OS_BMSK                                          0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_AMP_OS_PLL_AMP_OS_SHFT                                           0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_ADDR                                            (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000b4)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_RMSK                                                   0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_SSCCENTER_BMSK                                         0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_SSCCENTER_SHFT                                         0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_SSCEN_BMSK                                             0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_EN_CENTER_SSCEN_SHFT                                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_ADDR                                             (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000b8)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_RMSK                                                   0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_SSC_ADJPER1_BMSK                                       0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER1_SSC_ADJPER1_SHFT                                        0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_ADDR                                             (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000bc)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_RMSK                                                    0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_SSC_ADJPER2_BMSK                                        0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_ADJ_PER2_SSC_ADJPER2_SHFT                                        0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_ADDR                                                 (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000c0)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_RMSK                                                       0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_SSC_PER1_BMSK                                              0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER1_SSC_PER1_SHFT                                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_ADDR                                                 (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000c4)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_RMSK                                                       0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_SSC_PER2_BMSK                                              0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_PER2_SSC_PER2_SHFT                                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_ADDR                                           (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000c8)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_SSC_STEPSIZE1_BMSK                                   0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE1_SSC_STEPSIZE1_SHFT                                    0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_ADDR                                           (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000cc)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_SSC_STEPSIZE2_BMSK                                   0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_SSC_STEP_SIZE2_SSC_STEPSIZE2_SHFT                                    0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_ADDR                                              (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000d0)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_RMSK                                                    0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_RESCODEUP_MUX_BMSK                                      0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_RESCODEUP_MUX_SHFT                                       0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_RESCODE_UP_BMSK                                         0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_RESCODE_UP_SHFT                                          0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_ADDR                                              (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000d4)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_RMSK                                                    0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_RESCODEDN_MUX_BMSK                                      0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_RESCODEDN_MUX_SHFT                                       0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_RESCODE_DN_BMSK                                         0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_RESCODE_DN_SHFT                                          0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_ADDR                                       (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000d8)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_RMSK                                             0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_RESCODEUP_OFFSET_BMSK                            0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_UP_OFFSET_RESCODEUP_OFFSET_SHFT                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_ADDR                                       (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000dc)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_RMSK                                             0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_RESCODEDN_OFFSET_BMSK                            0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_DN_OFFSET_RESCODEDN_OFFSET_SHFT                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_ADDR                                      (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000e0)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_RMSK                                            0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_RESTRIM_SENSESTATE_SEG1_BMSK                    0x40
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_RESTRIM_SENSESTATE_SEG1_SHFT                     0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_RESTRIM_DIRECTION_SEG1_BMSK                     0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_RESTRIM_DIRECTION_SEG1_SHFT                      0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_RESCODE_START_SEG1_BMSK                         0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG1_RESCODE_START_SEG1_SHFT                          0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_ADDR                                      (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000e4)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_RMSK                                            0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_RESTRIM_SENSESTATE_SEG2_BMSK                    0x40
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_RESTRIM_SENSESTATE_SEG2_SHFT                     0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_RESTRIM_DIRECTION_SEG2_BMSK                     0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_RESTRIM_DIRECTION_SEG2_SHFT                      0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_RESCODE_START_SEG2_BMSK                         0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_START_SEG2_RESCODE_START_SEG2_SHFT                          0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_ADDR                                         (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000e8)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_RMSK                                               0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_RESCODE_CSR_BMSK                                   0xf0
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_RESCODE_CSR_SHFT                                    0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_RESCODE_CAL_BMSK                                    0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_CAL_CSR_RESCODE_CAL_SHFT                                    0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_ADDR                                                 (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000ec)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_RMSK                                                       0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_RESCODE_MUX_BMSK                                           0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_RESCODE_MUX_SHFT                                            0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_RESCODE_BMSK                                               0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_CODE_RESCODE_SHFT                                                0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_ADDR                                         (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000f0)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_RMSK                                               0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEGMENT2_BMSK                              0x30
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEGMENT2_SHFT                               0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEGMENT_BMSK                                0xc
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEGMENT_SHFT                                0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_FREEZE_BMSK                                 0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_FREEZE_SHFT                                 0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEARCH_BMSK                                 0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL_RESTRIM_SEARCH_SHFT                                 0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_ADDR                                        (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000f4)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_RMSK                                               0x7
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_STEP_BMSK                                  0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_STEP_SHFT                                  0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_START_MUX_BMSK                             0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_START_MUX_SHFT                             0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_START_BMSK                                 0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_CONTROL2_RESTRIM_START_SHFT                                 0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_ADDR                                   (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000f8)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RMSK                                         0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_SEEK_MUX_BMSK                                0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_SEEK_MUX_SHFT                                 0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_SEEK_BMSK                                    0x10
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_SEEK_SHFT                                     0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_VCO_CAL_DONE_MUX_BMSK                         0x8
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_VCO_CAL_DONE_MUX_SHFT                         0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_VCO_CAL_DONE_BMSK                             0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_VCO_CAL_DONE_SHFT                             0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RESTRIM_EN_MUX_BMSK                           0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RESTRIM_EN_MUX_SHFT                           0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RESTRIM_EN_BMSK                               0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_RES_TRIM_EN_VCOCALDONE_RESTRIM_EN_SHFT                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_ADDR                                                  (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x000000fc)
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_RMSK                                                        0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_TEST_CLK_EN_BMSK                                            0x10
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_TEST_CLK_EN_SHFT                                             0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_HR_OCLK3_EN_BMSK                                             0x8
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_HR_OCLK3_EN_SHFT                                             0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_HR_OCLK2_EN_BMSK                                             0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_HR_OCLK2_EN_SHFT                                             0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_FAUX_SEL_BMSK                                                0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_FAUX_SEL_SHFT                                                0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_FAUX_EN_BMSK                                                 0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_FAUX_EN_FAUX_EN_SHFT                                                 0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_ADDR                                          (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000100)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_RMSK                                                0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_DIV_FRAC_START1_MUX_BMSK                            0x80
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_DIV_FRAC_START1_MUX_SHFT                             0x7
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_DIV_FRAC_START1_BMSK                                0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START1_DIV_FRAC_START1_SHFT                                 0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_ADDR                                          (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000104)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_RMSK                                                0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_DIV_FRAC_START2_MUX_BMSK                            0x80
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_DIV_FRAC_START2_MUX_SHFT                             0x7
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_DIV_FRAC_START2_BMSK                                0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START2_DIV_FRAC_START2_SHFT                                 0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_ADDR                                          (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000108)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_RMSK                                                0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_DIV_FRAC_START3_MUX_BMSK                            0x40
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_DIV_FRAC_START3_MUX_SHFT                             0x6
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_DIV_FRAC_START3_BMSK                                0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_DIV_FRAC_START3_DIV_FRAC_START3_SHFT                                 0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_ADDR                                               (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000010c)
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_RMSK                                                      0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_DEC_START2_MUX_BMSK                                       0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_DEC_START2_MUX_SHFT                                       0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_DEC_START2_BMSK                                           0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_DEC_START2_DEC_START2_SHFT                                           0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_ADDR                                         (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000110)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_RMSK                                               0x3f
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_CLK_EP_EN_FORCE_BMSK                               0x20
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_CLK_EP_EN_FORCE_SHFT                                0x5
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_CLK_EP_EN_BMSK                                     0x10
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_CLK_EP_EN_SHFT                                      0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_TXCLK_EN_MUX_BMSK                               0x8
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_TXCLK_EN_MUX_SHFT                               0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_TXCLK_EN_BMSK                                   0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_TXCLK_EN_SHFT                                   0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_RXCLK_EN_MUX_BMSK                               0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_RXCLK_EN_MUX_SHFT                               0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_RXCLK_EN_BMSK                                   0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_RXTXEPCLK_EN_PLL_RXCLK_EN_SHFT                                   0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_ADDR                                               (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000114)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_RMSK                                                     0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_PLL_RCTRL_BMSK                                           0xf0
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_PLL_RCTRL_SHFT                                            0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_PLL_CCTRL_BMSK                                            0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CRCTRL_PLL_CCTRL_SHFT                                            0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_ADDR                                             (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000118)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_RMSK                                                   0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_PLL_CLK_EP_DIV_BMSK                                    0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_CLKEPDIV_PLL_CLK_EP_DIV_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_ADDR                                           (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000011c)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_RMSK                                                  0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_PLL_FREQUPDATE_BMSK                                   0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_FREQUPDATE_PLL_FREQUPDATE_SHFT                                   0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_ADDR                                       (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000120)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_RMSK                                             0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_BKGCAL_TRIM_UP_BMSK                              0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_UP_BKGCAL_TRIM_UP_SHFT                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_ADDR                                       (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000124)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_RMSK                                             0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_BKGCAL_TRIM_DN_BMSK                              0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_DN_BKGCAL_TRIM_DN_SHFT                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_ADDR                                      (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000128)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_RMSK                                             0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_BKGCAL_TRIM_UP_MUX_BMSK                          0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_BKGCAL_TRIM_UP_MUX_SHFT                          0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_BKGCAL_TRIM_DN_MUX_BMSK                          0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_TRIM_MUX_BKGCAL_TRIM_DN_MUX_SHFT                          0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_ADDR                                      (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000012c)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_RMSK                                             0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_BKGCAL_VREF_CFG_BMSK                             0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_VREF_CFG_BKGCAL_VREF_CFG_SHFT                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_ADDR                                      (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000130)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_RMSK                                            0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_BKGCAL_DIV_REF1_BMSK                            0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF1_BKGCAL_DIV_REF1_SHFT                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_ADDR                                      (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000134)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_RMSK                                             0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_BKGCAL_DIV_REF2_BMSK                             0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_BKGCAL_DIV_REF2_BKGCAL_DIV_REF2_SHFT                             0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_ADDR                                                  (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000138)
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_RMSK                                                        0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_MUXADDR_BMSK                                                0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXADDR_MUXADDR_SHFT                                                 0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_ADDR                                     (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000013c)
#define HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_RMSK                                            0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_LP_VCO_CTRL_BMSK                                0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_LOW_POWER_RO_CONTROL_LP_VCO_CTRL_SHFT                                0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_ADDR                                     (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000140)
#define HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_RMSK                                           0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_LP_CLK_CTRL_BMSK                               0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_POST_DIVIDER_CONTROL_LP_CLK_CTRL_SHFT                                0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_ADDR                                         (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000144)
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_RMSK                                               0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_HR_OCLK2_DIV_BMSK                                  0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK2_DIVIDER_HR_OCLK2_DIV_SHFT                                   0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_ADDR                                         (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000148)
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_RMSK                                               0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_IN)
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_HR_OCLK3_DIV_BMSK                                  0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_HR_OCLK3_DIVIDER_HR_OCLK3_DIV_SHFT                                   0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_ADDR                                             (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000014c)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_RMSK                                                    0xf
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_PLACEHOLDER_CHAIN_OUTPUT_BMSK                           0x8
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_PLACEHOLDER_CHAIN_OUTPUT_SHFT                           0x3
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_PLL_FREQ_DONE_BMSK                                      0x4
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_PLL_FREQ_DONE_SHFT                                      0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_AMP_DONE_BMSK                                           0x2
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_AMP_DONE_SHFT                                           0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_PLL_VCOHIGH_BMSK                                        0x1
#define HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCO_HIGH_PLL_VCOHIGH_SHFT                                        0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_RESET_SM_ADDR                                                 (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000150)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESET_SM_RMSK                                                       0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_RESET_SM_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESET_SM_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_RESET_SM_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESET_SM_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_RESET_SM_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_RESET_SM_RESETSM_BMSK                                               0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_RESET_SM_RESETSM_SHFT                                                0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_MUXVAL_ADDR                                                   (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000154)
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXVAL_RMSK                                                         0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXVAL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_MUXVAL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_MUXVAL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXVAL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_MUXVAL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXVAL_MUXVAL_BMSK                                                  0xff
#define HWIO_MMSS_DSI_1_PHY_PLL_MUXVAL_MUXVAL_SHFT                                                   0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_DN_ADDR                                         (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000158)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_DN_RMSK                                               0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_DN_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_DN_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_DN_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_DN_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_DN_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_DN_CORE_RES_CODE_DN_BMSK                              0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_DN_CORE_RES_CODE_DN_SHFT                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_UP_ADDR                                         (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x0000015c)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_UP_RMSK                                               0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_UP_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_UP_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_UP_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_UP_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_UP_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_UP_CORE_RES_CODE_UP_BMSK                              0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_RES_CODE_UP_CORE_RES_CODE_UP_SHFT                               0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TUNE_ADDR                                            (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000160)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TUNE_RMSK                                                  0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TUNE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TUNE_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TUNE_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TUNE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TUNE_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TUNE_CORE_VCO_TUNE_BMSK                                    0x7f
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TUNE_CORE_VCO_TUNE_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TAIL_ADDR                                            (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000164)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TAIL_RMSK                                                  0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TAIL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TAIL_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TAIL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TAIL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TAIL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TAIL_CORE_VCO_TAIL_BMSK                                    0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_VCO_TAIL_CORE_VCO_TAIL_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_KVCO_CODE_ADDR                                           (MMSS_DSI_1_PHY_PLL_REG_BASE      + 0x00000168)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_KVCO_CODE_RMSK                                                 0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_KVCO_CODE_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_KVCO_CODE_ADDR, HWIO_MMSS_DSI_1_PHY_PLL_CORE_KVCO_CODE_RMSK)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_KVCO_CODE_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_PLL_CORE_KVCO_CODE_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_KVCO_CODE_CORE_KVCO_CODE_BMSK                                  0x1f
#define HWIO_MMSS_DSI_1_PHY_PLL_CORE_KVCO_CODE_CORE_KVCO_CODE_SHFT                                   0x0

/*----------------------------------------------------------------------------
 * MODULE: MMSS_DSI_1_PHY_REG
 *--------------------------------------------------------------------------*/


#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR                                           (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x00000000)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_IN)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_RSVD_7_4_BMSK                                        0xf0
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_RSVD_7_4_SHFT                                         0x4
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_DSIPHY_REG_REF_SEL_BMSK                               0xe
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_DSIPHY_REG_REF_SEL_SHFT                               0x1
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_DSIPHY_REG_EN_BMSK                                    0x1
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_0_DSIPHY_REG_EN_SHFT                                    0x0

#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR                                           (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x00000004)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_IN)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_DSIPHY_REG_CONT_DISCHARGE_CYCLES_BMSK                0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_1_DSIPHY_REG_CONT_DISCHARGE_CYCLES_SHFT                 0x0

#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR                                           (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x00000008)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_IN)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_DSIPHY_REG_CONT_CHARGE_CYCLES_BMSK                   0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_2_DSIPHY_REG_CONT_CHARGE_CYCLES_SHFT                    0x0

#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR                                           (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x0000000c)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_IN)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_RSVD_7_4_BMSK                                        0xf0
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_RSVD_7_4_SHFT                                         0x4
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_DSIPHY_REG_CLK_DIV_RATIO_BMSK                         0xf
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_3_DSIPHY_REG_CLK_DIV_RATIO_SHFT                         0x0

#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR                                           (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x00000010)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_IN)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RSVD_7_6_BMSK                                        0xc0
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RSVD_7_6_SHFT                                         0x6
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_DIV_EN_BMSK                                          0x20
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_DIV_EN_SHFT                                           0x5
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_NP_CGM_ENABLE_BMSK                                   0x10
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_NP_CGM_ENABLE_SHFT                                    0x4
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RSVD_3_0_BMSK                                         0xf
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_4_RSVD_3_0_SHFT                                         0x0

#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_ADDR                                           (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x00000014)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_IN)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_RSVD_7_5_BMSK                                        0xe0
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_RSVD_7_5_SHFT                                         0x5
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWCTRL_DISCHARGE_EN_BMSK                  0x10
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWCTRL_DISCHARGE_EN_SHFT                   0x4
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_FORCE_PWR_SWITCH_BMSK                  0x8
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_FORCE_PWR_SWITCH_SHFT                  0x3
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_PSW_CTRL_BMSK                          0x4
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_PSW_CTRL_SHFT                          0x2
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_NSW_CTRL_BMSK                          0x2
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_NSW_CTRL_SHFT                          0x1
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_EMI_CTRL_BMSK                          0x1
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_5_DSIPHY_REG_SWI_EMI_CTRL_SHFT                          0x0

#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR                                      (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x00000018)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RMSK                                            0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_IN)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RSVD_7_5_BMSK                                   0xe0
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RSVD_7_5_SHFT                                    0x5
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_SW_RESET_BMSK                                   0x10
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_SW_RESET_SHFT                                    0x4
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RSVD_3_1_BMSK                                    0xe
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_RSVD_3_1_SHFT                                    0x1
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_DSIPHY_REG_PWRDN_B_BMSK                          0x1
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_DSIPHY_REG_PWRDN_B_SHFT                          0x0

#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_ADDR                                           (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x0000001c)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_IN)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_INIT_DISCHARGE_CYCLES_BMSK                           0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_CTRL_6_INIT_DISCHARGE_CYCLES_SHFT                            0x0

#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_ADDR                                           (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x00000020)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_RMSK                                                 0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_IN)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_RSVD_7_0_BMSK                                        0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REGULATOR_RSVD_1_RSVD_7_0_SHFT                                         0x0

#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_ADDR                                            (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x00000024)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_RMSK                                                  0xf7
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_OUT(v)      \
        out_dword(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_ADDR,v)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_ADDR,m,v,HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_IN)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_RSVD_7_3_BMSK                                         0xf0
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_RSVD_7_3_SHFT                                          0x4
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_REG_DEBUG_SEL_BMSK                                     0x7
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_CAL_DBG_SEL_REG_DEBUG_SEL_SHFT                                     0x0

#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT0_ADDR                                              (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x00000028)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT0_RMSK                                                    0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT0_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT0_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT0_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT0_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT0_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT0_DEBUG_BUS_7_0_BMSK                                      0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT0_DEBUG_BUS_7_0_SHFT                                       0x0

#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT1_ADDR                                              (MMSS_DSI_1_PHY_REG_REG_BASE      + 0x0000002c)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT1_RMSK                                                    0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT1_IN          \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT1_ADDR, HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT1_RMSK)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT1_INM(m)      \
        in_dword_masked(HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT1_ADDR, m)
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT1_DEBUG_BUS_15_8_BMSK                                     0xff
#define HWIO_MMSS_DSI_1_PHY_REG_DSIPHY_REG_DBG_STAT1_DEBUG_BUS_15_8_SHFT                                      0x0

#endif  /* #define HALDSI_PHY_1_3_0_H */
