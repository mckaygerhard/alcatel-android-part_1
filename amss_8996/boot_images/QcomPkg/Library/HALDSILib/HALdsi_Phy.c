/*=============================================================================

  File: HALdsi_Phy.c
  

     Copyright (c) 2010-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
     Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/

/*============================================================================
*                         INCLUDE FILES
============================================================================*/
#ifdef __cplusplus
extern "C" {
#endif 

#include "HALdsi.h"
#include "HALdsi_Phy.h"
#include "dsiHostSystem.h"

HAL_DSI_PhyFunctionTable    gDsiPhyFxnTbl;

/* -----------------------------------------------------------------------
** Local Data Types
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Local functions
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Public functions
** ----------------------------------------------------------------------- */

/****************************************************************************
 *
 ** FUNCTION: HAL_DSI_PhyFxnsInit()
 */
/*!
 * \brief
 *     Initialize DSI function table.
 *
 * \retval None
 *
 ****************************************************************************/
void HAL_DSI_PhyFxnsInit(void)
{
    HAL_DSI_VersionType           sDSIVersionInfo;

    HAL_DSI_ReadCoreVersionInfo(&sDSIVersionInfo);

    /* Cheel, Bagheera, Kichi, Shere, Sahi */
    if ((0x01 == sDSIVersionInfo.uMajorVersion) &&
        (0x03 == sDSIVersionInfo.uMinorVersion) &&
        (0x01 == sDSIVersionInfo.uReleaseVersion))
    {
        gDsiPhyFxnTbl.HAL_DSI_PhyPllInitialize     = HAL_DSI_1_3_1_PhyPllInitialize;
        gDsiPhyFxnTbl.HAL_DSI_PhyDisable           = HAL_DSI_1_3_1_PhyDisable;
        gDsiPhyFxnTbl.HAL_DSI_PhyPllPowerCtrl      = HAL_DSI_1_3_1_PhyPllPowerCtrl;
        gDsiPhyFxnTbl.HAL_DSI_PhyPllSetup          = HAL_DSI_1_3_1_PhyPllSetup;
        gDsiPhyFxnTbl.HAL_DSI_PhyReConfigure       = HAL_DSI_1_3_1_PhyReConfigure;
        gDsiPhyFxnTbl.HAL_DSI_PhySetup             = HAL_DSI_1_3_1_PhySetup;
        gDsiPhyFxnTbl.HAL_DSI_PhySetupTimingParams = HAL_DSI_1_3_1_PhySetupTimingParams;
        
    }
    /* Eldarion, Estel, Elessar */
    else if ((0x01 == sDSIVersionInfo.uMajorVersion) &&
             (0x03 == sDSIVersionInfo.uMinorVersion) &&
             (0x00 == sDSIVersionInfo.uReleaseVersion))
    {
        gDsiPhyFxnTbl.HAL_DSI_PhyPllInitialize     = HAL_DSI_1_3_0_PhyPllInitialize;
        gDsiPhyFxnTbl.HAL_DSI_PhyDisable           = HAL_DSI_1_3_0_PhyDisable;
        gDsiPhyFxnTbl.HAL_DSI_PhyPllPowerCtrl      = HAL_DSI_1_3_0_PhyPllPowerCtrl;
        gDsiPhyFxnTbl.HAL_DSI_PhyPllSetup          = HAL_DSI_1_3_0_PhyPllSetup;
        gDsiPhyFxnTbl.HAL_DSI_PhyReConfigure       = HAL_DSI_1_3_0_PhyReConfigure;
        gDsiPhyFxnTbl.HAL_DSI_PhySetup             = HAL_DSI_1_3_0_PhySetup;
        gDsiPhyFxnTbl.HAL_DSI_PhySetupTimingParams = HAL_DSI_1_3_0_PhySetupTimingParams;
    }
    /* Elfstone, Istari */
    else if ((0x01 == sDSIVersionInfo.uMajorVersion) &&
             (0x04 == sDSIVersionInfo.uMinorVersion))
    {
        gDsiPhyFxnTbl.HAL_DSI_PhyPllInitialize     = HAL_DSI_1_4_0_PhyPllInitialize;
        gDsiPhyFxnTbl.HAL_DSI_PhyDisable           = HAL_DSI_1_4_0_PhyDisable;
        gDsiPhyFxnTbl.HAL_DSI_PhyPllPowerCtrl      = HAL_DSI_1_4_0_PhyPllPowerCtrl;
        gDsiPhyFxnTbl.HAL_DSI_PhyPllSetup          = HAL_DSI_1_4_0_PhyPllSetup;
        gDsiPhyFxnTbl.HAL_DSI_PhyReConfigure       = HAL_DSI_1_4_0_PhyReConfigure;
        gDsiPhyFxnTbl.HAL_DSI_PhySetup             = HAL_DSI_1_4_0_PhySetup;
        gDsiPhyFxnTbl.HAL_DSI_PhySetupTimingParams = HAL_DSI_1_4_0_PhySetupTimingParams;
    }
    else
    {
    }
}


/****************************************************************************
*
** FUNCTION: HAL_DSI_PhyDisable()
*/
/*!
* \brief
*     Disables DSI Phy.
*
* \param [in]   eDeviceId   - DSI core ID
*
* \retval None
*
****************************************************************************/
void HAL_DSI_PhyDisable( DSI_Device_IDType   eDeviceId )
{
    if (NULL != gDsiPhyFxnTbl.HAL_DSI_PhyDisable)
    {
        gDsiPhyFxnTbl.HAL_DSI_PhyDisable(eDeviceId);
    }
}

/****************************************************************************
*
** FUNCTION: HAL_DSI_PhyPllPowerCtrl()
*/
/*!
* \brief
*     Power up/down PLL, LDO and powergen.
*
*
* \param [in]  eDeviceId    - DSI core ID
* \param [in]  bPllPowerUp  - TRUE: power up, FALSE: power down;
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_DSI_PhyPllPowerCtrl( DSI_Device_IDType   eDeviceId,
                                            bool32              bPllPowerUp )
{
   HAL_MDSS_ErrorType   eStatus = HAL_MDSS_STATUS_SUCCESS;

   if (NULL != gDsiPhyFxnTbl.HAL_DSI_PhyPllPowerCtrl)
   {
       eStatus = gDsiPhyFxnTbl.HAL_DSI_PhyPllPowerCtrl(eDeviceId, bPllPowerUp);
   }
   return eStatus;
}

/****************************************************************************
*
** FUNCTION: HAL_DSI_PhySetup()
*/
/*!
* \brief
*     Set up DSI Phy alone.
*
* \param [in]  eDeviceId       - DSI core ID
* \param [IN]  psDsiPhyConfig     - Phy configuration
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_DSI_PhySetup(DSI_Device_IDType          eDeviceId, 
                                    HAL_DSI_PhyConfigType     *psDsiPhyConfig)
{
    HAL_MDSS_ErrorType        eStatus = HAL_MDSS_STATUS_SUCCESS;

    if (NULL != gDsiPhyFxnTbl.HAL_DSI_PhySetup)
    {
        eStatus = gDsiPhyFxnTbl.HAL_DSI_PhySetup(eDeviceId, psDsiPhyConfig);
    }  
    return eStatus;
}

/****************************************************************************
*
** FUNCTION: HAL_DSI_PhyPllSetup()
*/
/*!
* \brief
*     Set up DSI PLL, pass back some config parameters, such as VCO output frequency, 
*     PCLK divider ratio for CC in the form of numerator and denominator, etc.
*
* \param [in]   psDsiPhyConfig     - Phy config info
* \param [out]  psDsiPhyConfigInfo - Phy & PLL config pass back info
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_DSI_PhyPllSetup(HAL_DSI_PhyConfigType       *psDsiPhyConfig,
                                       HAL_DSI_PhyConfigInfoType   *psDsiPhyConfigInfo)
{
    HAL_MDSS_ErrorType        eStatus = HAL_MDSS_STATUS_SUCCESS;

    if (NULL != gDsiPhyFxnTbl.HAL_DSI_PhyPllSetup)
    {
        eStatus = gDsiPhyFxnTbl.HAL_DSI_PhyPllSetup(psDsiPhyConfig, psDsiPhyConfigInfo);
    }
    return eStatus;
}



/****************************************************************************
*
** FUNCTION: HAL_DSI_PhySetupTimingParams()
*/
/*!
* \brief
*     Calculate PHY timing parameters.
*
* \param [in]  pTimingParameters - DSI core ID
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_DSI_PhySetupTimingParams(HAL_DSI_TimingSettingType        *pTimingParameters)
{
    HAL_MDSS_ErrorType eStatus = HAL_MDSS_STATUS_SUCCESS;

    if (NULL != gDsiPhyFxnTbl.HAL_DSI_PhySetupTimingParams)
    {
        eStatus = gDsiPhyFxnTbl.HAL_DSI_PhySetupTimingParams(pTimingParameters);
    }

    return eStatus;
}

/****************************************************************************

HAL_DSI_PhyPllInitalize()

Description:
This function initializes the entire DSI core (Phy/PLL) to a default state.
This function should be applied only once after the core has come out of reset.

****************************************************************************/
void HAL_DSI_PhyPllInitialize(void)
{
    if (NULL != gDsiPhyFxnTbl.HAL_DSI_PhyPllInitialize)
    {
        gDsiPhyFxnTbl.HAL_DSI_PhyPllInitialize();
    }
}


#ifdef __cplusplus
}
#endif
