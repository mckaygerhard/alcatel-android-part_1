/** @file FwESRT.h
   
   Header for functions to maintain the ESRT , including ESRT
   header and ESRT entry .
  
  Copyright (c) 2012 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

**/
/*=============================================================================
                              EDIT HISTORY
  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.



 when           who    what, where, why
 --------       ---    --------------------------------------------------
 2013/04/11     rsb    Added 8084 platform types
 2012/11/30     mic    Added ESRT entry print function
 2012/11/09     jmb    Adding CBH platform
 2012/10/21     mic    Code review
 2012/08/14     ash    Added 8064 WB292 and V1 variants
 2012/06/18     jd     rename OEM_UPDATE_EVENT_GUID 
 2012/06/05     rs     Added UXCapsule OEM Update Event GUID
 2012/05/09     rsb    Platform run-time type detection for FW Resource GUID
 2012/05/07     jd     Migrated to DPP Protocol.
 2012/02/01     mic    Initial version

=============================================================================*/
#ifndef __QCOM_FW_ESRT_H_
#define __QCOM_FW_ESRT_H_

#define QCOM_FW_UPDATE_PRODUCT_VERSION_10 0x00010000
#define QCOM_FW_UPDATE_PRODUCT_VERSION_11 0x00010001
#define QCOM_FW_UPDATE_PRODUCT_VERSION_12 0x00010002

/* System Firmware Resource GUID for 8960 CDP: {4BF085D1-B56C-4F24-8B3D-82F9CEE16EA7} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8960_CDP_GUID \
{ \
  0x4BF085D1, 0xB56C, 0x4F24, { 0x8B, 0x3D, 0x82, 0xF9, 0xCE, 0xE1, 0x6E, 0xA7 } \
}

/* System Firmware Resource GUID for 8960 LIQUID: {28d578e6-a736-4a8a-88a8-94c47e8f8047} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8960_LIQUID_GUID \
{ \
  0x28D578E6, 0xA736, 0x4A8A, { 0x88, 0xA8, 0x94, 0xC4, 0x7E, 0x8F, 0x80, 0x47 } \
}

/* System Firmware Resource GUID for 8960 LIQUIDv3.2.1: {0D8725C2-3369-4065-99FA-2B9C786FE3ED} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8960_LIQUIDV321_GUID \
{ \
  0x0D8725C2, 0x3369, 0x4065, { 0x99, 0xFA, 0x2B, 0x9C, 0x78, 0x6F, 0xE3, 0xED } \
}

/* System Firmware Resource GUID for 8064 CDP: {EE11C7CC-0735-4945-8B01-E17533958601} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8064_CDP_GUID \
{ \
  0xEE11C7CC, 0x0735, 0x4945, { 0x8B, 0x01, 0xE1, 0x75, 0x33, 0x95, 0x86, 0x01 } \
}

/* System Firmware Resource GUID for 8064 CDP WB292: {140E5810-CDC6-4083-8118-23D8026AF976} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8064_CDP_WB292_GUID \
{ \
  0x140E5810, 0xCDC6, 0x4083, { 0x81, 0x18, 0x23, 0xD8, 0x02, 0x6A, 0xF9, 0x76 } \
}

/* System Firmware Resource GUID for 8064 Fusion3 CDP: {CF00E9F2-1829-4D65-9781-BF4A32D1B7A4} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8064_F3CDP_GUID \
{ \
  0xCF00E9F2, 0x1829, 0x4D65, { 0x97, 0x81, 0xBF, 0x4A, 0x32, 0xD1, 0xB7, 0xA4 } \
}

/* System Firmware Resource GUID for 8064 Fusion 3 LIQUID: {679B5E88-1EED-414E-9B34-A824B76FA298} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8064_F3LIQUID_GUID \
{ \
  0x679B5E88, 0x1EED, 0x414E, { 0x9B, 0x34, 0xA8, 0x24, 0xB7, 0x6F, 0xA2, 0x98 } \
}

/* System Firmware Resource GUID for 8064 Fusion 3 LIQUID V1.0: {7112167E-9AAA-4F01-847F-C06CEED3046C} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8064_F3LIQUID_V10_GUID \
{ \
  0x7112167E, 0x9AAA, 0x4F01, { 0x84, 0x7F, 0xC0, 0x6C, 0xEE, 0xD3, 0x04, 0x6C } \
}

/* System Firmware Resource GUID for 8064 Fusion 3 LIQUID WB292: {9FCA683C-960E-474A-8941-968554E5A8B4} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8064_F3LIQUID_WB292_GUID \
{ \
  0x9FCA683C, 0x960E, 0x474A, { 0x89, 0x41, 0x96, 0x85, 0x54, 0xE5, 0xA8, 0xB4 } \
}

/* System Firmware Resource GUID for 8064 LIQUID: {89663A60-5A85-4D20-B7A1-591C31978972} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8064_LIQUID_GUID \
{ \
  0x89663A60, 0x5A85, 0x4D20, { 0xB7, 0xA1, 0x59, 0x1C, 0x31, 0x97, 0x89, 0x72 } \
}

/* System Firmware Resource GUID for 8064 LIQUID V1.0: {2184964F-9404-4088-819C-9C686E711EF4} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8064_LIQUID_V10_GUID \
{ \
  0x2184964F, 0x9404, 0x4088, { 0x81, 0x9C, 0x9C, 0x68, 0x6E, 0x71, 0x1E, 0xF4 } \
}

/* System Firmware Resource GUID for 8064 LIQUID WB292: {B4EDAC98-9F20-48AB-BE1C-B1B352507521} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8064_LIQUID_WB292_GUID \
{ \
  0xB4EDAC98, 0x9F20, 0x48AB, { 0xBE, 0x1C, 0xB1, 0xB3, 0x52, 0x50, 0x75, 0x21 } \
}

/* System Firmware Resource GUID for 8974 CDP: {6E1CD379-577A-45C7-B793-
5F18CA8D91EF} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8974_CDP_GUID \
{ \
  0x6E1CD379, 0x577A, 0x45C7, { 0xB7, 0x93, 0x5F, 0x18, 0xCA, 0x8D, 0x91, 0xEF } \
}

/* System Firmware Resource GUID for 8974 LIQUID: {90A994F7-D72D-4FB3-8F51-
582511B51CB1} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8974_LIQUID_GUID \
{ \
  0x90A994F7, 0xD72D, 0x4FB3, { 0x8F, 0x51, 0x58, 0x25, 0x11, 0xB5, 0x1C, 0xB1 } \
}

/* System Firmware Resource GUID for 8974 MTP: {B9A7EC50-8827-4111-9C50-
4BD341AF0745} */

#define QCOM_FW_UPDATE_SYSTEM_FW_8974_MTP_GUID \
{ \
  0xB9A7EC50, 0x8827, 0x4111, { 0x9C, 0x50, 0x4B, 0xD3, 0x41, 0xAF, 0x07, 0x45 } \
}

/* System Firmware Resource GUID for 8974 CBH: {30B6B82D-2AE3-4CC1-A905-C1FFF408A184} */
#define QCOM_FW_UPDATE_SYSTEM_FW_8974_CBH_GUID \
{ \
  0x30b6b82d, 0x2ae3, 0x4cc1, { 0xa9, 0x5, 0xc1, 0xff, 0xf4, 0x8, 0xa1, 0x84 } \
}

/* System Firmware Resource GUID for 8084 CDP: {6F0B570C-4AF0-403A-8664-1A403A0C6F0B} */
#define QCOM_FW_UPDATE_SYSTEM_FW_8084_CDP_GUID \
{ \
  0x6F0B570C, 0x4AF0, 0x403A, { 0x86, 0x64, 0x1A, 0x40, 0x3A, 0x0C, 0x6F, 0x0B } \
}

/* System Firmware Resource GUID for 8084 LIQUID: {09095700-FC47-495A-BB50-9F0B2BAFE8C5} */
#define QCOM_FW_UPDATE_SYSTEM_FW_8084_LIQUID_GUID \
{ \
  0x09095700, 0xFC47, 0x495A, { 0xBB, 0x50, 0x9F, 0x0B, 0x2B, 0xAF, 0xE8, 0xC5 } \
}

/* System Firmware Resource GUID for 8084 CBH: {A5207871-6E07-45F4-86AF-F6CD41CD12BA} */
#define QCOM_FW_UPDATE_SYSTEM_FW_8084_CBH_GUID \
{ \
  0xA5207871, 0x6E07, 0x45F4, { 0x86, 0xAF, 0xF6, 0xCD, 0x41, 0xCD, 0x12, 0xBA } \
}

/* System Firmware Resource GUID for UNKNOWN platform type: {06CE6651-B594-4AC3-B57A-D9637CC335A8} */

#define QCOM_FW_UPDATE_SYSTEM_FW_UNKNOWN_GUID \
{ \
  0x06CE6651, 0xB594, 0x4AC3, { 0xB5, 0x7A, 0xD9, 0x63, 0x7C, 0xC3, 0x35, 0xA8 } \
}

/* UX Capsule GUID : {3b8c8162-188c-46a4-aec9-be43f1d65697}  */

#define QCOM_FW_UPDATE_UX_CAPSULE_GUID \
{ \
  0x3B8C8162, 0x188C, 0x46A4, { 0xAE, 0xC9, 0xBE, 0x43, 0xF1, 0xD6, 0x56, 0x97 } \
}

#define QCOM_FW_UPDATE_ESRT_GUID \
{ \
  0xB122A263, 0x3661, 0x4F68, { 0x99, 0x29, 0x78, 0xf8, 0xb0, 0xd6, 0x21, 0x80 } \
}

/*{592515FE-F062-4AD0-89C8-9118F0F2D6BF}*/
#define QCOM_FW_UPDATE_OEM_UPDATE_EVENT_GUID \
{ \
  0x592515FE, 0xF062, 0x4AD0, { 0x89, 0xC8, 0x91, 0x18, 0xF0, 0xF2, 0xD6, 0xBF } \
}

typedef enum {
  FW_TYPE_UNKNOWN,
  FW_TYPE_SYSTEM,
  FW_TYPE_DEVICE,
  FW_TYPE_DRIVER
} ESRT_FWTYPE;

typedef enum {
  FW_LAST_ATMPT_STATUS_SUCCESS,
  FW_LAST_ATMPT_STATUS_UNSUCCESSFUL,
  FW_LAST_ATMPT_STATUS_INSUFF_RESOURCE,
  FW_LAST_ATMPT_STATUS_INCORRECT_VERSION,
  FW_LAST_ATMPT_STATUS_INVALID_IMG_FMT,
  FW_LAST_ATMPT_STATUS_AUTHEN_ERROR,
  FW_LAST_ATMPT_STATUS_POWER_AC_NOT_CONNECTED,
  FW_LAST_ATMPT_STATUS_POWER_INSUFFICIENT_BATTERY

} ESRT_LAST_ATTEMPT_STATUS;

/* The list of System Firmware Resource types for each platform, this enumeration must match 
   one to one with qcomFwResourceGUIDs. */
typedef enum _QCOM_FW_RESOURCE_TYPE
{
  QCOM_FW_UPDATE_SYSTEM_FW_8960_CDP_TYPE = 0,
  QCOM_FW_UPDATE_SYSTEM_FW_8960_LIQUID_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8960_LIQUIDV321_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8064_CDP_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8064_CDP_WB292_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8064_F3CDP_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8064_F3LIQUID_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8064_F3LIQUID_V10_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8064_F3LIQUID_WB292_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8064_LIQUID_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8064_LIQUID_V10_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8064_LIQUID_WB292_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8974_CDP_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8974_LIQUID_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8974_MTP_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8974_CBH_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8084_CDP_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8084_LIQUID_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_8084_CBH_TYPE,
  QCOM_FW_UPDATE_SYSTEM_FW_UNKNOWN_TYPE

} QCOM_FW_RESOURCE_TYPE;

typedef struct
{
  EFI_GUID FWClass ;
  UINT32 FWType;
  UINT32 FWVer;
  UINT32 FwLowestSupportedVer;
  UINT32 CapsuleFlag;
  UINT32 LastAttemptVer;
  UINT32 LastAttemptStatus;

}ESRT_ENTRY;

typedef struct
{
  UINT32 FWResrcCnt;
  UINT32 FWResrcMax;
  UINT64 FWResrcVer;

}ESRT_TABLE_HEADER;

/**
 * Function to print ESRT entry 
 * 
 * @param ent  - ESRT entry
 * @DebugLevel - Debug level 
 *
 * @return : EFI_STATUS
 */
EFI_STATUS
QcomPrintEsrtEntry(
  IN ESRT_ENTRY *ent, 
  IN UINTN      DebugLevel
  );

/**
* Get the ESRT entry info 
*
* @param  ent         - the address of the pointer which points to ESRT entry
* @return EFI_STATUS
**/
EFI_STATUS
QcomGetEsrtEntry(
  IN OUT ESRT_ENTRY  **ent
  );

/**
 * This Function try to update ESRT in both DPP and system table
 * 
 * @param  SystemTable   - Passed from calling application
 * @param  attemptStatus - Passed from the calling application
 *
 * @return EFI_STATUS 
 **/
EFI_STATUS
QcomUpdateESRT(
  IN EFI_SYSTEM_TABLE         *SystemTable,
  IN ESRT_LAST_ATTEMPT_STATUS attemptStatus
  );

/**
 * This Function kicks off the ESRT  Phase of 
 * the Firmware update process. 
 * 
 * @param  SystemTable - Passed from calling application
 * 
 * @return EFI_STATUS 
 **/
EFI_STATUS
QcomESRTPhaseMain(
  IN OUT EFI_SYSTEM_TABLE  *SystemTable
  );

#endif
