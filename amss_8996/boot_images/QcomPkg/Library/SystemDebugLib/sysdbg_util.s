/*============================================================================

             SYSDBG ASSEMBLY ROUTINES

 Copyright 2014 - 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved
============================================================================

============================================================================

                       EDIT HISTORY FOR MODULE

 $Header: //components/rel/boot.xf/1.0/QcomPkg/Library/SystemDebugLib/sysdbg_util.s#1 $
 $DateTime: 2015/06/01 23:27:13 $
 $Author: pwbldsvc $

 This section contains comments describing changes made to the module.
 Notice that changes are listed in reverse chronological order.


 when      who     what, where, why
 --------  ---     ---------------------------------------------------------
 04/12/15  AJC     Initial Version
============================================================================

============================================================================

                        MODULE MACROS AND FUNCTIONS

============================================================================
    .section .code.SYSDB_UTIL_ASM, "ax", %progbits
    .p2align 3
=============================================================================*/

/* @Name: set_global_lock(global_lock_type* lock, uint64 value)
 Set the lock variable to a particular value atomically
 Arg0 : global lock address, value to set the lock
*/
.global set_global_lock
.type set_global_lock,%function
set_global_lock:
    sub sp, sp, 0x10
    stp x2, x3, [SP]
    ldaxr x3, [x0]
51:
    stlxr w2, x1, [x0]
    cbnz w2, 51b
    mov x0, xzr
    mov w0, w2
    ldp x2, x3, [SP]
    add sp, sp, 0x10
    ret


/* @Name: get_global_lock(global_lock_type* lock)
 Get the lock value atomically
 Arg0 : global lock address, 
 Return Val : Lock value
*/

.global get_global_lock
.type get_global_lock,%function
get_global_lock:
    sub sp, sp, 0x10
    stp x2, x3, [SP]
    ldaxr x3, [x0]
52:
    stlxr w2, x3, [x0]
    cbnz w2, 52b
    mov x0, x3
    ldp x2, x3, [SP]
    add sp, sp, 0x10
    ret
