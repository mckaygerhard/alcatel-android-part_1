#ifndef _HALHDMIPHYSETTING_3_0_1_H_
#define _HALHDMIPHYSETTING_3_0_1_H_

/*=============================================================================
 
  File: HALHdmiPhySetting_3_0_1.h
  
 
  Copyright (c) 2010-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/

/* 
 * Note: The Phy/Pll settings provided in this file are for 28nm HDMI Phy specifically.
 */

#define TO_MHZ(x)         (uint32)(x*1000000)


/* HDMI TMDS frequencies supported */
static uint32 uHdmiPllFreqTable[] =
{
  TO_MHZ(25.175), TO_MHZ(25.20), TO_MHZ(27.00), TO_MHZ(27.027), TO_MHZ(31.500), TO_MHZ(33.750),
  TO_MHZ(35.500), TO_MHZ(36.00), TO_MHZ(40.00), TO_MHZ(49.50),  TO_MHZ(50.00),  TO_MHZ(54.054),
  TO_MHZ(56.25),  TO_MHZ(65.00), TO_MHZ(68.25), TO_MHZ(71.00),  TO_MHZ(72.00),  TO_MHZ(73.25),
  TO_MHZ(74.25),  TO_MHZ(75.00), TO_MHZ(78.75), TO_MHZ(79.50),  TO_MHZ(83.50),  TO_MHZ(85.50),
  TO_MHZ(88.75),  TO_MHZ(94.50), TO_MHZ(101.00),TO_MHZ(102.25), TO_MHZ(106.50), TO_MHZ(108.00),
  TO_MHZ(108.108),TO_MHZ(115.50),TO_MHZ(117.50),TO_MHZ(119.00), TO_MHZ(121.75), TO_MHZ(122.50),
  TO_MHZ(135.00), TO_MHZ(136.75),TO_MHZ(140.25),TO_MHZ(146.25), TO_MHZ(148.25), TO_MHZ(148.50),
  TO_MHZ(268.50), TO_MHZ(297.00),
};

#define NUMBER_OF_HDMI_PLL_FREQ_SUPPORTED      sizeof(uHdmiPllFreqTable)/sizeof(uint32)
#define NUMBER_OF_HDMI_PHYPLL_REGS_TO_PROGRAM  46

/* HDMI Phy/PLL register settings for supported TMDS frequencies */
static uint8 uHdmiPhyPllRegSetting[NUMBER_OF_HDMI_PLL_FREQ_SUPPORTED][NUMBER_OF_HDMI_PHYPLL_REGS_TO_PROGRAM] =
{
  {  /* TMDS  25.175 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x52,     0x00,     0xAB,     0x00,     0x10,     0x1A,     0x05,     0x03, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xF3,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  25.200 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x52,     0x00,     0xB0,     0x00,     0x10,     0x1A,     0x05,     0x03, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xF4,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  27.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x54,     0x00,     0x18,     0x00,     0x10,     0x1A,     0x05,     0x03, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x2A,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  27.027 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x54,     0x66,     0x1D,     0x00,     0x10,     0x1A,     0x05,     0x03, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x2A,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  31.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x57,     0x00,     0x9C,     0x00,     0x10,     0x1A,     0x05,     0x03, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xB1,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  33.750 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x50,     0x00,     0x94,     0x00,     0x10,     0x1A,     0x05,     0x02, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xA3,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  35.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x51,     0x55,     0x7D,     0x00,     0x10,     0x1A,     0x05,     0x02, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xC6,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  36.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x51,     0x00,     0xC0,     0x00,     0x10,     0x1A,     0x05,     0x02, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xD0,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  40.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x53,     0x55,     0xD5,     0x00,     0x10,     0x1A,     0x05,     0x02, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x20,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  49.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x58,     0x00,     0xC8,     0x00,     0x10,     0x1A,     0x05,     0x02, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xDE,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  50.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x59,     0xAB,     0x0A,     0x00,     0x10,     0x1A,     0x05,     0x02, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xE8,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  54.054 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x5B,     0x33,     0x27,     0x00,     0x10,     0x1A,     0x05,     0x02, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x39,     0x04,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  56.250 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x5C,     0x00,     0x4C,     0x00,     0x10,     0x1A,     0x05,     0x02, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x65,     0x04,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  65.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x4F,     0x55,     0xED,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x8A,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  68.250 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x50,     0x00,     0xC6,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xAA,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  71.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x51,     0x55,     0x7D,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xC6,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x02, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  72.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x51,     0x00,     0xC0,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xD0,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x02, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  73.250 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x52,     0x55,     0x13,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xDC,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  74.250 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x52,     0x00,     0x56,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xE6,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  75.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x52,     0x00,     0x88,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xEE,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  78.750 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x53,     0x00,     0x82,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x13,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  79.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x53,     0x00,     0xB4,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x1B,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  83.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x54,     0xAB,     0xBE,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x43,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  85.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x55,     0x00,     0x44,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x57,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  88.750 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x56,     0xAB,     0x1C,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x77,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS  94.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x57,     0x00,     0x9C,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xB1,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 101.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x59,     0x55,     0x4D,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xF2,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 102.250 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x59,     0xAB,     0xA0,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xFE,     0x03,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 106.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x5A,     0x00,     0xBC,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x29,     0x04,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 108.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x5B,     0x00,     0x20,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x38,     0x04,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 108.108 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x5B,     0x33,     0x27,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x39,     0x04,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 115.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x5D,     0x00,     0x14,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x83,     0x04,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 117.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x5D,     0x55,     0x99,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x97,     0x04,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 119.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x5D,     0x55,     0xFD,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xA6,     0x04,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 121.750 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x5E,     0xAB,     0xB4,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xC1,     0x04,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 122.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x5E,     0xAB,     0xE6,     0x00,     0x10,     0x1A,     0x05,     0x01, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xC9,     0x04,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 135.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x50,     0x00,     0x94,     0x00,     0x10,     0x1A,     0x05,     0x00, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xA3,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 136.750 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x50,     0x55,     0xCE,     0x00,     0x10,     0x1A,     0x05,     0x00, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xAB,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 140.250 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x51,     0x00,     0x43,     0x00,     0x10,     0x1A,     0x05,     0x00, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xBD,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 146.250 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x52,     0x00,     0x0B,     0x00,     0x10,     0x1A,     0x05,     0x00, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xDB,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 148.250 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x52,     0xAB,     0x4D,     0x00,     0x10,     0x1A,     0x05,     0x00, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xE5,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 148.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x52,     0x00,     0x56,     0x00,     0x10,     0x1A,     0x05,     0x00, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xE6,     0x02,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x01, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x02,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 268.500 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x61,     0x00,     0xF6,     0x00,     0x10,     0x1A,     0x05,     0x00, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0x3E,     0x05,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x05, 
    0x00,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x11,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

  {  /* TMDS 297.000 MHz */ 
    0x81,     0x01,     0x01,     0x19,     0x0E,     0x20,     0x0D,     0x00, 
    0x65,     0x00,     0xAC,     0x00,     0x10,     0x1A,     0x05,     0x00, 
    0x00,     0x00,     0x01,     0x60,     0x00,     0xCD,     0x05,     0x1F, 
    0x00,     0x03,     0x0F,     0x00,     0xDB,     0x43,     0x06, 
    0x03,     0x04,     0xD0,     0x1A,     0x00,     0x00,     0x00,     0x05, 
    0x62,     0x03,     0x69,     0x02,     0x00,     0x00, 
  },

};


#endif /* _HALHDMIPHYSETTING_3_0_1_H_ */

