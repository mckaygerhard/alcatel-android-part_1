#ifndef HDMIDDC_H
#define HDMIDDC_H
/*
===========================================================================

FILE:         HALHdmiDDC.h

DESCRIPTION: 
  This is the interface to be used to access Qualcomm's HDMI TX core DDC interface
  The intended audience for this source file is HDMI Host layer
  which incorporates this hardware block with other similiar hardware blocks.  
  This interface is not intended or recommended for direct usage by any application.


===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------


===========================================================================
Copyright (c) 2008-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "HALHdmiCommon.h"
/* -----------------------------------------------------------------------
** Defines
** ----------------------------------------------------------------------- */
#define HAL_HDMI_HDCP_PRIMARY_LINK_DEVICE_ADDR                (0x74)  //Device address for HDCP link using DDC protocol
#define HAL_HDMI_DDC_CHECKSUM_MODULUS                         (0x100) //Checksum modulus for calculating DDC read checksums
#define HAL_HDMI_DETAIL_TIMING_DESC_BLOCK_SIZE                (0x12)  //Detailed timing descriptor block size

/* -----------------------------------------------------------------------
** Macros
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Types
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Static Variables
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Prototypes
** ----------------------------------------------------------------------- */
/****************************************************************************
*
** FUNCTION: HAL_HDMI_DDC_Init()
*/
/*!
* \brief
*   The \b HAL_HDMI_DDC_Init function configures the DDC setup registers such as
*   speed and drive strength.
*
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_DDC_Init(void);

/****************************************************************************
*
** FUNCTION: HAL_HDMI_DDC_Read()
*/
/*!
* \brief
*   The \b HAL_HDMI_DDC_Read function read the DDC data from HDMI Sink device
*
* \param [in]      uDevAddr    - I2C Device Address
* \param [in]      uOffset     - The offset into the device to read
* \param [in/out]  pDataBuf    - Buffer to populate the read data
* \param [in]      uDataLength - Read length
* 
* \retval HAL_HDMI_StatusType
*
****************************************************************************/
HAL_HDMI_StatusType HAL_HDMI_DDC_Read(uint32 uSegmentAddr, uint32 uSegmentNum, uint32 uDevAddr, uint32 uOffset, uint8* pDataBuf, uint32 uDataLength,uint32 uTimeOut);


/****************************************************************************
*
** FUNCTION: HAL_HDMI_DDC_Write()
*/
/*!
* \brief
*   The \b HAL_HDMI_DDC_Write function read the DDC data from HDMI Sink device
*
* \param [in]      uDevAddr    - I2C Device Address
* \param [in]      uOffset     - The offset into the device to write
* \param [in/out]  pDataBuf    - Buffer to populate the write data
* \param [in]      uDataLength - Write length
* 
* \retval HAL_HDMI_StatusType
*
****************************************************************************/
HAL_HDMI_StatusType HAL_HDMI_DDC_Write(uint32 uSegmentAddr, uint32 uSegmentNum, uint32 uDevAddr, uint32 uOffset, uint8* pDataBuf, uint32 uDataLength, uint32 uTimeOut);
/****************************************************************************
*
** FUNCTION: HAL_HDMI_DDC_Reset()
*/
/*!
* \brief
*   The \b HAL_HDMI_DDC_Reset function resets the DDC controller 
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_DDC_Reset(void);

#endif /* HDMIDDC_H */


