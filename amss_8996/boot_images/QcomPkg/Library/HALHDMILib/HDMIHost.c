/*=============================================================================
 
  File: HDMIHost.c
 
  HDMI Host layer
  
 
 Copyright (c) 2011-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential.
 =============================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "HDMIHost.h"
#include "HDMI_EDID.h"
#include "HALHdmiPhy.h"
#include "HALHdmiHPD.h"
#include "HDMI_Util.h"

#include <Base.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/EFIClock.h>
#include <Library/PcdLib.h>


/* -----------------------------------------------------------------------
** Global Variable
** ----------------------------------------------------------------------- */
HDMI_DeviceDataType gHdmiSharedData;

static const HAL_HDMI_AVI_PixelRepeatFactor ValidPixelRepeatValues[]  =
{
  /* From CEA-861-D table 13 Valid Pixel Repeat Values for Each Video Format Timing */
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //0 = Place Holder since it starts from 1;
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //1 = HDMI_VIDEO_FORMAT_640x480p60_4_3   = 0,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //2 = HDMI_VIDEO_FORMAT_720x480p60_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //3 = HDMI_VIDEO_FORMAT_720x480p60_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //4 = HDMI_VIDEO_FORMAT_1280x720p60_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //5 = HDMI_VIDEO_FORMAT_1920x1080i60_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //6 = HDMI_VIDEO_FORMAT_720x480i60_4_3,   HDMI_VIDEO_FORMAT_1440x480i60_4_3
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //7 = HDMI_VIDEO_FORMAT_720x480i60_16_9,  HDMI_VIDEO_FORMAT_1440x480i60_16_9
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //8 = HDMI_VIDEO_FORMAT_720x240p60_4_3,   HDMI_VIDEO_FORMAT_1440x240p60_4_3
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //9 = HDMI_VIDEO_FORMAT_720x240p60_16_9,  HDMI_VIDEO_FORMAT_1440x240p60_16_9
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //10= HDMI_VIDEO_FORMAT_2880x480i60_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //11= HDMI_VIDEO_FORMAT_2880x480i60_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //12= HDMI_VIDEO_FORMAT_2880x240p60_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //13= HDMI_VIDEO_FORMAT_2880x240p60_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //14= HDMI_VIDEO_FORMAT_1440x480p60_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //15= HDMI_VIDEO_FORMAT_1440x480p60_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //16 =HDMI_VIDEO_FORMAT_1920x1080p60_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //17= HDMI_VIDEO_FORMAT_720x576p50_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //18= HDMI_VIDEO_FORMAT_720x576p50_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //19= HDMI_VIDEO_FORMAT_1280x720p50_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //20= HDMI_VIDEO_FORMAT_1920x1080i50_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //21= HDMI_VIDEO_FORMAT_720x576i50_4_3,  HDMI_VIDEO_FORMAT_1440x576i50_4_3
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //22= HDMI_VIDEO_FORMAT_720x576i50_16_9, HDMI_VIDEO_FORMAT_1440x576i50_16_9
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //23= HDMI_VIDEO_FORMAT_720x288p50_4_3,  HDMI_VIDEO_FORMAT_1440x288p50_4_3
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //24= HDMI_VIDEO_FORMAT_720x288p50_16_9, HDMI_VIDEO_FORMAT_1440x288p50_16_9
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //25= HDMI_VIDEO_FORMAT_2880x576i50_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //26= HDMI_VIDEO_FORMAT_2880x576i50_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //27= HDMI_VIDEO_FORMAT_2880x288p50_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //28= HDMI_VIDEO_FORMAT_2880x288p50_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //29= HDMI_VIDEO_FORMAT_1440x576p50_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //30= HDMI_VIDEO_FORMAT_1440x576p50_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //31= HDMI_VIDEO_FORMAT_1920x1080p50_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //32= HDMI_VIDEO_FORMAT_1920x1080p24_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //33= HDMI_VIDEO_FORMAT_1920x1080p25_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //34= HDMI_VIDEO_FORMAT_1920x1080p30_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //35= HDMI_VIDEO_FORMAT_2880x480p60_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //36= HDMI_VIDEO_FORMAT_2880x480p60_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //37= HDMI_VIDEO_FORMAT_2880x576p50_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,   //38= HDMI_VIDEO_FORMAT_2880x576p50_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //39= HDMI_VIDEO_FORMAT_1920x1080i50_16_9_special, HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //	Format 39 is 1080i50 mode with total 1250 vertical lines.
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //40= HDMI_VIDEO_FORMAT_1920x1080i100_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //41= HDMI_VIDEO_FORMAT_1280x720p100_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //42= HDMI_VIDEO_FORMAT_720x576p100_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //43= HDMI_VIDEO_FORMAT_720x576p100_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //44= HDMI_VIDEO_FORMAT_720x576i100_4_3,  HDMI_VIDEO_FORMAT_1440x576i100_4_3
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //45= HDMI_VIDEO_FORMAT_720x576i100_16_9, HDMI_VIDEO_FORMAT_1440x576i100_16_9
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //46= HDMI_VIDEO_FORMAT_1920x1080i120_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //47= HDMI_VIDEO_FORMAT_1280x720p120_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //48= HDMI_VIDEO_FORMAT_720x480p120_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //49= HDMI_VIDEO_FORMAT_720x480p120_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //50= HDMI_VIDEO_FORMAT_720x480i120_4_3,  HDMI_VIDEO_FORMAT_1440x480i120_4_3
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //51= HDMI_VIDEO_FORMAT_1440x480i120_16_9,HDMI_VIDEO_FORMAT_720x480i120_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //52= HDMI_VIDEO_FORMAT_720x576p200_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //53= HDMI_VIDEO_FORMAT_720x576p200_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //54= HDMI_VIDEO_FORMAT_720x576i200_4_3,  HDMI_VIDEO_FORMAT_1440x576i200_4_3
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //55= HDMI_VIDEO_FORMAT_720x576i200_16_9, HDMI_VIDEO_FORMAT_1440x576i200_16_9
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //56= HDMI_VIDEO_FORMAT_720x480p240_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //57= HDMI_VIDEO_FORMAT_720x480p240_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //58= HDMI_VIDEO_FORMAT_720x480i240_4_3, HDMI_VIDEO_FORMAT_1440x480i240_4_3
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,     //59= HDMI_VIDEO_FORMAT_720x480i240_16_9,HDMI_VIDEO_FORMAT_1440x480i240_16_9
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //60= HDMI_VIDEO_FORMAT_1024x768p60_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //61= HDMI_VIDEO_FORMAT_1024x768p70_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //62= HDMI_VIDEO_FORMAT_1024x768p75_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //63= HDMI_VIDEO_FORMAT_1280x768p60_5_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //64= HDMI_VIDEO_FORMAT_1280x800p60_16_10,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //65= HDMI_VIDEO_FORMAT_1280x960p60_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //66= HDMI_VIDEO_FORMAT_1280x1024p60_5_4,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //67= HDMI_VIDEO_FORMAT_1360x768p60_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //68= HDMI_VIDEO_FORMAT_1366x768p60_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //69= HDMI_VIDEO_FORMAT_1400x1050p60_4_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //70= HDMI_VIDEO_FORMAT_1440x900p60_16_10,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //71= HDMI_VIDEO_FORMAT_1600x900p60_16_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE,  //72= HDMI_VIDEO_FORMAT_1680x1050p60_16_10,

};

extern HAL_HDMI_DispModeTimingType         gSupportedVideoModeLUT[];
extern HAL_HDMI_DispModeTimingType         gSupported3DFramePackingModeLUT[];

#define HDMI_CLOCK_1080p60            148500000
#define HDMI_CLOCK_720p60              74250000

/* -----------------------------------------------------------------------
** Static Variable
** ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
** Static Functions
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Public Functions
** ----------------------------------------------------------------------- */
/****************************************************************************
*
** FUNCTION: HDMI_Init()
*/
/*!
* \brief
*   The \b HDMI_Init function initializes a device before it can be used.  
*   It includes initialization of any state variables and OS specific services.
**
* \retval MDP_Status
*
****************************************************************************/
  MDP_Status HDMI_Init()
  {
    HDMI_DeviceDataType*  pSharedData  = &gHdmiSharedData;

    if (FALSE  == pSharedData->bInitialized)
    {
      pSharedData->bInitialized                = TRUE;
      pSharedData->bPhyInitialized             = FALSE;
      pSharedData->bLastConnectionStatus       = FALSE;
      pSharedData->bContentProtectionEnable    = FALSE;
      pSharedData->bLastAuthenticationStatus   = TRUE; /*Initial state must be true to trigger authentication to occur */
      pSharedData->bEDIDParserComplete         = FALSE;
    }

    return MDP_STATUS_OK;
  }
/****************************************************************************
*
** FUNCTION: HDMI_DeInit()
*/
/*!
* \brief
*   The \b HDMI_DeInit function de-initializes the HDMI TX core driver.  
*   It closes all the resources allocated by the driver and reset to back to default
*   value for the state variables.
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_DeInit()
{
  MDP_Status            eStatus      = MDP_STATUS_FAILED;
  HDMI_DeviceDataType*  pSharedData  = &gHdmiSharedData;
  
  if (TRUE == pSharedData->bInitialized)
  {
    MDP_OSAL_MEMZERO(pSharedData, sizeof(HDMI_DeviceDataType));
    pSharedData->bLastAuthenticationStatus   = TRUE; /* Initial state must be true to trigger authentication to occur */
  
    eStatus = MDP_STATUS_OK;
  }
  return eStatus;
}

/****************************************************************************
*
** FUNCTION: HDMI_Device_Open()
*/
/*!
* \brief
*   The \b HDMI_Device_Open function initializes a device before it can be used.  
*   An error will be returned if a new handle can not be obtained.
*
* \param [out] phDevice            - Pointer to handle for initialized device.
* \param [in]  pDeviceOpenParams   - Parameters list for initialization.
* \param [in]  flags               - Option to be requested ( Set to 0 for default call ).
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_Device_Open(void **phDevice, HDMI_DeviceOpenParam *pDeviceOpenParams, uint32 flags)
{
  MDP_Status            eStatus      = MDP_STATUS_OK; 
  HDMI_DeviceDataType*  pSharedData  = HDMI_GETCTX();

  if (NULL != phDevice)
  {
	/* Initialize HDMI PHY/PLL function table */
    HAL_HDMI_PhyFxnsInit();

    // Generate a device handle
    *phDevice = (void*) ((UINTN)pSharedData->uNumDeviceUserCtx | DEVICE_USER_CONTEXT_HANDLE_TYPE);

    pSharedData->sDeviceUserCtx[pSharedData->uNumDeviceUserCtx].eDeviceID = pDeviceOpenParams->eDeviceID;
    pSharedData->uNumDeviceUserCtx++;
        
    /* Enable the HDMI block at DVI mode as some DVI monitor does not work in HDMI mode. */
    /* HDMI monitor will work at DVI mode.  For windows, driver will reset HDMI phy. */
    HAL_HDMI_EngineCtrl(HAL_HDMI_VIDEO_DVI_MODE, HAL_HDMI_POWER_ON);

    // Enable HPD detection hardware
    HAL_HDMI_HPD_Engine_Enable();

    // Enable HPD/DDC pins
    if (MDP_STATUS_OK != (eStatus = HDMI_EnableTLMM_DDC_HPD()))
    {
      DEBUG ((EFI_D_WARN, "DisplayDxe: Enable HDMI_EnableTLMM_DDC_HPD()  failed\n"));
    }
  }
    
  return eStatus;
}


/****************************************************************************
*
** FUNCTION: HDMI_Device_Close()
*/
/*!
* \brief
*   The \b HDMI_Device_Close function is to be called upon deinitialization.
*   A flag can be passed as a parameter to specify an option to enable.
*
* \param [in]  hDevice         - Handle for device to terminated
* \param [in]  flags           - Option to be requested ( Set to 0 for default call )   
*
* \retval MDP_Status
*
****************************************************************************/
void HDMI_Device_Close(void* hDevice, uint32 flags)
{
  HDMI_DeviceDataType*  pSharedData  = HDMI_GETCTX();
  uint32                index        = (uint32)((UINTN)hDevice);

  (void) flags;

  index &= ~(DEVICE_USER_CONTEXT_HANDLE_TYPE);

      pSharedData->sDeviceUserCtx[index].eDeviceID = HDMI_DEVICE_ID0;
      pSharedData->uNumDeviceUserCtx--;

    if (MDP_STATUS_OK != HDMI_DisableTLMM_DDC_HPD())
    {
         DEBUG ((EFI_D_INFO, "DisplayDxe: Enable HDMI_DisableTLMM_DDC_HPD()  failed\n"));
    }  
    //Disable Tx Core
    HAL_HDMI_EngineCtrl(HAL_HDMI_OperationModeGet(), HAL_HDMI_POWER_OFF);
}


/****************************************************************************
*
** FUNCTION: HDMI_Device_GetProperty()
*/
/*!
* \brief
*   The \b HDMI_Device_GetProperty function gets one property for the device 
*   specified by hDevice
*
* \param [in]    hDevice           Handle for device whose properties are being set.
* \param [in]    ePropertyType     Property that will be applied to the device.  
* \param [in]    pPropertyData     Contains the new parameters of the property that will be applied.
*
* \retval MDP_Status
*
****************************************************************************/
  MDP_Status HDMI_Device_GetProperty(void *hDevice, HDMI_Device_PropertyType eProperty, HDMI_Device_PropertyParamsType *pPropertyData)
  {
    HDMI_DeviceDataType  *pSharedData  = HDMI_GETCTX();
    uint32                index        = (uint32)((UINTN)hDevice);
    MDP_Status            eStatus      = MDP_STATUS_OK;
    HDMI_UserCtxType     *pUserCtx     = NULL;

    index   &= ~(DEVICE_USER_CONTEXT_HANDLE_TYPE);

  pUserCtx = &pSharedData->sDeviceUserCtx[index];

    switch (eProperty)
    {
    case HDMI_DEVICE_PROPERTY_POWER_MODE:
      {
        pPropertyData->ePowerMode = pUserCtx->ePowerMode;
        break;
      }
    case HDMI_DEVICE_PROPERTY_VIDEO_MODE:
      {
        pPropertyData->eMode = pUserCtx->eMode;
        break;
      }
    case HDMI_DEVICE_PROPERTY_AUDIO_MODE_INFO:
      {
        if (!pSharedData->bEDIDParserComplete)
          eStatus = HDMI_EDID_Parser_Open();

        if (MDP_STATUS_OK == eStatus)
          eStatus = HDMI_EDID_Parser_GetAudioModeInfo(&pPropertyData->sAudioModeInfo);
        break;
      }
    case HDMI_DEVICE_PROPERTY_AUDIO_PARAM:
      {
        pPropertyData->sAudioParams.bAudioInfoPacket     = pUserCtx->sAudioParams.bAudioInfoPacket;
        pPropertyData->sAudioParams.bInhibitDownMix      = pUserCtx->sAudioParams.bInhibitDownMix;
        pPropertyData->sAudioParams.eAudioNumOfChannel   = pUserCtx->sAudioParams.eAudioNumOfChannel;
        pPropertyData->sAudioParams.eAudioSampleRate     = pUserCtx->sAudioParams.eAudioSampleRate;
        pPropertyData->sAudioParams.eAudioFormat         = pUserCtx->sAudioParams.eAudioFormat;
        pPropertyData->sAudioParams.eAudioSampleBitDepth = pUserCtx->sAudioParams.eAudioSampleBitDepth;
        pPropertyData->sAudioParams.eChannelAllocCode    = pUserCtx->sAudioParams.eChannelAllocCode;
        pPropertyData->sAudioParams.eLevelShiftValue     = pUserCtx->sAudioParams.eLevelShiftValue;
        break;
      }
    case HDMI_DEVICE_PROPERTY_VENDOR_SPECIFIC_INFO:
      {
        if (!pSharedData->bEDIDParserComplete)
          eStatus = HDMI_EDID_Parser_Open();

        if (MDP_STATUS_OK == eStatus)
          eStatus = HDMI_EDID_Parser_GetVendorInfo(&pPropertyData->sVendorInfo);
        break;
      }
    case HDMI_DEVICE_PROPERTY_DISP_MODE:
      {
        if (!pSharedData->bEDIDParserComplete)
          eStatus = HDMI_EDID_Parser_Open();

        if (MDP_STATUS_OK == eStatus)
          eStatus = HDMI_EDID_Parser_GetDisplayModeInfo(&pPropertyData->sDispModeInfo);

        if (MDP_STATUS_OK == eStatus)
          eStatus = HDMI_EDID_Parser_GetColorBitDepth(&pPropertyData->sDispModeInfo.ePixelFormat);

        if (MDP_STATUS_OK == eStatus)
          eStatus = HDMI_EDID_Parser_GetOrientation(&pPropertyData->sDispModeInfo.eRotation);

        break;
      }
    case HDMI_DEVICE_PROPERTY_CONTENT_PROTECTION_ENABLE:
      {
        pPropertyData->bContentProtectionEnable = pUserCtx->bContentProtectionEnable;
        break;
      }
    case HDMI_DEVICE_PROPERTY_EDID_INFO:
      {
        if (!pSharedData->bEDIDParserComplete)
          eStatus = HDMI_EDID_Parser_Open();

        if (MDP_STATUS_OK == eStatus)
          eStatus = HDMI_EDID_Parser_GetRawEdidInfo(&pPropertyData->sEdidInfo);
        break;
      }
    case HDMI_DEVICE_PROPERTY_CONNECTION_STATUS:
      {
         pPropertyData->bConnected = FALSE;

         // Check for HPD connection 
         if(TRUE==HAL_HDMI_HPD_CheckConnection())
         {
            if(MDP_STATUS_OK ==  HDMI_EDID_Parser_Open())
            {
                 pPropertyData->bConnected = TRUE;
            }
         }

        break;
      }
    default:
      break;
    }

    return eStatus;
}

/****************************************************************************
*
** FUNCTION: HDMI_Device_SetProperty()
*/
/*!
* \brief
*   The \b HDMI_Device_SetProperty function sets one property for the device specified by hDevice
*   The newly changed property will be effective only if HDMI_Device_Commit
*   is invoked.
*
* \param [in]    hDevice           Handle for device whose properties are being set.
* \param [in]    ePropertyType     Property that will be applied to the device.  
* \param [in]    pPropertyData     Contains the new parameters of the property that will be applied.
*
* \retval MDP_Status
*
****************************************************************************/
  MDP_Status HDMI_Device_SetProperty(void *hDevice, HDMI_Device_PropertyType eProperty, HDMI_Device_PropertyParamsType *pPropertyData)
  {
    HDMI_DeviceDataType  *pSharedData  = HDMI_GETCTX();
    uint32                uIndex       = (uint32)((UINTN)hDevice);
    MDP_Status            eStatus      = MDP_STATUS_OK;
    HDMI_UserCtxType     *pUserCtx     = NULL;

    uIndex   &= ~(DEVICE_USER_CONTEXT_HANDLE_TYPE);

    pUserCtx  = &pSharedData->sDeviceUserCtx[uIndex];

    switch (eProperty)
    {
    case HDMI_DEVICE_PROPERTY_POWER_MODE:
      {
        pUserCtx->ePowerMode    = pPropertyData->ePowerMode;
        pUserCtx->uDirtyBits   |= HDMI_DEVICE_DIRTYBIT_POWER_MODE;
        break;
      }
    case HDMI_DEVICE_PROPERTY_VIDEO_MODE:
      {
        pUserCtx->eMode       = pPropertyData->eMode;
        pUserCtx->uDirtyBits |= HDMI_DEVICE_DIRTYBIT_OPERATION_MODE;
        break;
      }       
    case HDMI_DEVICE_PROPERTY_AUDIO_PARAM:
      {
        pUserCtx->sAudioParams.bAudioInfoPacket     =  pPropertyData->sAudioParams.bAudioInfoPacket;
        pUserCtx->sAudioParams.bInhibitDownMix      =  pPropertyData->sAudioParams.bInhibitDownMix;
        pUserCtx->sAudioParams.eAudioNumOfChannel   =  pPropertyData->sAudioParams.eAudioNumOfChannel;
        pUserCtx->sAudioParams.eAudioSampleRate     =  pPropertyData->sAudioParams.eAudioSampleRate;
        pUserCtx->sAudioParams.eAudioFormat         =  pPropertyData->sAudioParams.eAudioFormat;
        pUserCtx->sAudioParams.eAudioSampleBitDepth =  pPropertyData->sAudioParams.eAudioSampleBitDepth;
        pUserCtx->sAudioParams.eChannelAllocCode    =  pPropertyData->sAudioParams.eChannelAllocCode;
        pUserCtx->sAudioParams.eLevelShiftValue     =  pPropertyData->sAudioParams.eLevelShiftValue;
        pUserCtx->uDirtyBits                       |=  HDMI_DEVICE_DIRTYBIT_AUDIO_PARAM;
        break;
      }
    case HDMI_DEVICE_PROPERTY_DISP_MODE:
      {
          if(HDMI_VIDEO_FORMAT_MAX > pPropertyData->sDispModeInfo.uModeIndex)
          {
            pUserCtx->uDispModeIndex    = pPropertyData->sDispModeInfo.uModeIndex; 
            pUserCtx->uDirtyBits        |= HDMI_DEVICE_DIRTYBIT_DISP_MODE_INDEX;
          }
          else
          {
            eStatus = MDP_STATUS_FAILED;
          }
        break;
      }
    case HDMI_DEVICE_PROPERTY_AVI_PACKET_TYPE:
      {
        pUserCtx->sAVIPacketParams   = pPropertyData->sAVIPacketParams;
        pUserCtx->uDirtyBits        |= HDMI_DEVICE_DIRTYBIT_AVI_PACKET_TYPE;
        break;
      }
    case HDMI_DEVICE_PROPERTY_CONTENT_PROTECTION_ENABLE:
      {
        pUserCtx->bContentProtectionEnable    = pPropertyData->bContentProtectionEnable;
        pUserCtx->uDirtyBits                 |= HDMI_DEVICE_DIRTYBIT_CONTENT_PROTECTION;
        break;
      }
    default:
      break;
    }

    return eStatus;
  }
/****************************************************************************
*
** FUNCTION: HDMI_Device_Commit()
*/
/*!
* \brief
*   The \b HDMI_Device_Commit function enables the changes made by xxx_SetProperty functions
*
* \param [in]   hDevice       - Device entity where the "commit" will occur.
* \param [in]   flags         - Option to be requested ( Set to 0 for default call )   
*
* \retval MDP_Status
*
****************************************************************************/
  MDP_Status HDMI_Device_Commit(void *phDevice, uint32 flags)
  {
    HDMI_DeviceDataType           *pSharedData  = HDMI_GETCTX();
    uint32                         uIndex       = (uint32)((UINTN)phDevice);
    MDP_Status                     eStatus      = MDP_STATUS_OK;
    HDMI_UserCtxType              *pUserCtx     = NULL;
    uint32                         uDirtyBits   = 0;
    HAL_HDMI_VideoAudioParamType   sParamSetup;

   (void) flags;

    uIndex &= ~(DEVICE_USER_CONTEXT_HANDLE_TYPE);

    MDP_OSAL_MEMZERO(&sParamSetup, sizeof(HAL_HDMI_VideoAudioParamType));
    pUserCtx    = &pSharedData->sDeviceUserCtx[uIndex];
    uDirtyBits  = pUserCtx->uDirtyBits;

    if (uDirtyBits != 0)
    {
      sParamSetup.eDeviceID = (HAL_HDMI_DeviceIDType)pUserCtx->eDeviceID;

      if (uDirtyBits & HDMI_DEVICE_DIRTYBIT_POWER_MODE)
      {
        sParamSetup.uFeatureOp |= HAL_HDMI_DIRTYBIT_POWER_MODE;
        sParamSetup.ePowerMode  = (HAL_HDMI_PowerModeType)pUserCtx->ePowerMode;
      }
      if (uDirtyBits & HDMI_DEVICE_DIRTYBIT_OPERATION_MODE)
      {
        sParamSetup.uFeatureOp |= HAL_HDMI_DIRTYBIT_OPERATION_MODE;
        sParamSetup.eMode       = (HAL_HDMI_OperationModeType)pUserCtx->eMode;
      }
      if (uDirtyBits & HDMI_DEVICE_DIRTYBIT_DISP_MODE_INDEX)
      {
        sParamSetup.uFeatureOp        |= HAL_HDMI_DIRTYBIT_VIDEO_RESOLUTION;
        sParamSetup.eVideoResolution   = pSharedData->sDispModeList.eDispModeList[pUserCtx->uDispModeIndex].eVideoFormat;

      if(pSharedData->sDispModeList.eDispModeList[pUserCtx->uDispModeIndex].e3DFrameFormat)
        {
          sParamSetup.uFeatureOp                                  |= HAL_HDMI_DIRTYBIT_VS_PACKET_TYPES;
          sParamSetup.sVSPacketParams.bVideoFormat3D               = TRUE;
          sParamSetup.sVSPacketParams.videoFormat.eVideoFormat3D   = pSharedData->sDispModeList.eDispModeList[pUserCtx->uDispModeIndex].e3DFrameFormat;
          sParamSetup.sVSPacketParams.eExtData3D                   = pSharedData->sDispModeList.eDispModeList[pUserCtx->uDispModeIndex].e3DExtendedFormat;
        }

        /* Reprogram the ACR packets when resolution changes while audio is active*/
        if (HAL_HDMI_AUDIO_CHANNEL_NONE != pUserCtx->sAudioParams.eAudioNumOfChannel)
          uDirtyBits |= HDMI_DEVICE_DIRTYBIT_AUDIO_PARAM;
      }

      if (uDirtyBits & HDMI_DEVICE_DIRTYBIT_AUDIO_PARAM)
      {
        sParamSetup.uFeatureOp                     |= HAL_HDMI_DIRTYBIT_AUDIO_PARAM;
        sParamSetup.sAudioParams.bAudioInfoPacket   = pUserCtx->sAudioParams.bAudioInfoPacket;
        sParamSetup.sAudioParams.eAudioSampleRate   = pUserCtx->sAudioParams.eAudioSampleRate;
        sParamSetup.sAudioParams.eAudioNumOfChannel = pUserCtx->sAudioParams.eAudioNumOfChannel;
        sParamSetup.sAudioParams.bInhibitDownMix        = pUserCtx->sAudioParams.bInhibitDownMix;
        sParamSetup.sAudioParams.eAudioFormat           = pUserCtx->sAudioParams.eAudioFormat;
        sParamSetup.sAudioParams.eAudioSampleBitDepth   = pUserCtx->sAudioParams.eAudioSampleBitDepth;
        sParamSetup.sAudioParams.eChannelAllocCode      = pUserCtx->sAudioParams.eChannelAllocCode;
        sParamSetup.sAudioParams.eLevelShiftValue       = pUserCtx->sAudioParams.eLevelShiftValue;

        /* Audio param changes require the video resolution */
        sParamSetup.eVideoResolution                = pSharedData->sDispModeList.eDispModeList[pUserCtx->uDispModeIndex].eVideoFormat;
      }

      if (uDirtyBits & HDMI_DEVICE_DIRTYBIT_AVI_PACKET_TYPE)
      {
        sParamSetup.uFeatureOp |= HAL_HDMI_DIRTYBIT_AVI_PACKET_TYPES;

        if (pUserCtx->sAVIPacketParams.sLetterBoxInfo.uEndOfTopBarLine  || pUserCtx->sAVIPacketParams.sLetterBoxInfo.uStartOfBottomBarLine ||
            pUserCtx->sAVIPacketParams.sLetterBoxInfo.uEndOfLeftBarLine || pUserCtx->sAVIPacketParams.sLetterBoxInfo.uStartOfRightBarLine)
        {
          sParamSetup.sAVIPacketParams.sLetterBoxInfo.bBarInfoValid         = 1;
          sParamSetup.sAVIPacketParams.sLetterBoxInfo.uEndOfTopBarLine      = pUserCtx->sAVIPacketParams.sLetterBoxInfo.uEndOfTopBarLine;
          sParamSetup.sAVIPacketParams.sLetterBoxInfo.uStartOfBottomBarLine = pUserCtx->sAVIPacketParams.sLetterBoxInfo.uStartOfBottomBarLine;
          sParamSetup.sAVIPacketParams.sLetterBoxInfo.uEndOfLeftBarLine     = pUserCtx->sAVIPacketParams.sLetterBoxInfo.uEndOfLeftBarLine;
          sParamSetup.sAVIPacketParams.sLetterBoxInfo.uStartOfRightBarLine  = pUserCtx->sAVIPacketParams.sLetterBoxInfo.uStartOfRightBarLine;
        }
        else
          MDP_OSAL_MEMZERO(&sParamSetup.sAVIPacketParams.sLetterBoxInfo, sizeof(HAL_HDMI_AVI_LetterBoxInfoType));

        sParamSetup.sAVIPacketParams.eActiveFormat      = pUserCtx->sAVIPacketParams.eActiveFormat;
        sParamSetup.sAVIPacketParams.eColorFormat       = pUserCtx->sAVIPacketParams.eColorFormat;
        sParamSetup.sAVIPacketParams.eVideoIDCode       = pUserCtx->sAVIPacketParams.eVideoIDCode;
        sParamSetup.sAVIPacketParams.bITContent         = pUserCtx->sAVIPacketParams.bITContent;
        sParamSetup.sAVIPacketParams.eQuantization      = (HAL_HDMI_AVI_Quantization)pUserCtx->sAVIPacketParams.eQuantization;
        sParamSetup.sAVIPacketParams.eColorimetry       = pUserCtx->sAVIPacketParams.eColorimetry;
        sParamSetup.sAVIPacketParams.eScaleInfo         = (HAL_HDMI_AVI_ScaleInfo)pUserCtx->sAVIPacketParams.eScaleInfo;

        if(HDMI_VIDEO_3D_FORMAT_FRAME_PACKING == pSharedData->sDispModeList.eDispModeList[pUserCtx->uDispModeIndex].e3DFrameFormat)
        {
          sParamSetup.sAVIPacketParams.eAspectRatio       = 
          gSupported3DFramePackingModeLUT[pSharedData->sDispModeList.eDispModeList[pUserCtx->uDispModeIndex].eVideoFormat].eAspectRatio;
        }
        else
        {
          sParamSetup.sAVIPacketParams.eAspectRatio       = 
          gSupportedVideoModeLUT[pSharedData->sDispModeList.eDispModeList[pUserCtx->uDispModeIndex].eVideoFormat].eAspectRatio;
        }

        /* ValidPixelRepeatValues array index (for valid video formats) starts from 1*/
        sParamSetup.sAVIPacketParams.ePixelRepeatFactor = ValidPixelRepeatValues[sParamSetup.sAVIPacketParams.eVideoIDCode + 1];
      }

      HAL_HDMI_VideoAudioSetup(&sParamSetup);

      if (uDirtyBits & HDMI_DEVICE_DIRTYBIT_CONTENT_PROTECTION)
      {
        /* Check All Users to ensure that if no one needs HDCP enabled 
           then disable it, otherwise if at least 1 is required, then 
           enable it */
        if (pSharedData->sDeviceUserCtx[uIndex].bContentProtectionEnable)
        {
          pSharedData->bContentProtectionEnable = TRUE;
        }
        else
        {
          pSharedData->bContentProtectionEnable = FALSE;
        }
      }
       
    }
    //Clear dirty bits
    pUserCtx->uDirtyBits = 0;

    return eStatus;
}


#ifdef __cplusplus
}
#endif

