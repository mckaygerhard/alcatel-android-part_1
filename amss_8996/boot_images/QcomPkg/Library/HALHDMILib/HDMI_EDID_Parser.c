
/*
===========================================================================

FILE:         HDMI_EDID_Parser.c

DESCRIPTION:  
  This is the interface to parse the important parameters from the EDID structure.
  The intended audience for this source file is HDMI Host driver.
  This interface is not intended or recommended for direct usage by any application.

===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------
10/12/11 kabhijit removed the pcdimageconfig and hardcoding of resolution
===========================================================================
     Copyright (c) 2011-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
     Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "HALHdmiCommon.h"
#include "HDMI_EDID.h"
#include "HDMIHost.h"
#include "HALHdmiDDC.h"
#include "HDMI_Util.h"
#include <Library/PcdLib.h>
#include <Library/UefiCfgLib.h>

/* -----------------------------------------------------------------------
** Globals
** ----------------------------------------------------------------------- */
extern HAL_HDMI_DispModeTimingType         gSupportedVideoModeLUT[];
extern HAL_HDMI_DispModeTimingType         gSupported3DFramePackingModeLUT[];
extern HDMI_DeviceDataType                 gHdmiSharedData;

/* -----------------------------------------------------------------------
** Enum 
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
 ** Macro 
 ** ----------------------------------------------------------------------- */
#define HDMI_VIDEO_FORMAT_NONE             HDMI_VIDEO_FORMAT_MAX

#define HDMI_HDCP_EDID_READ_MAX_RETRIES    3        // Maixmum number of EDID read if fail to read EDID 
/* -----------------------------------------------------------------------
** Structures 
** ----------------------------------------------------------------------- */
/*!
 * \b HDMI_DeviceIDType
 *
 * The Logic ID for HDMI TX Core. Currently only support 1 HDMI TX Core.
 */
typedef struct
{
  HDMI_VideoFormatType  eVideoCode;
  uint32                uActiveH;
  uint32                uActiveV;
  bool32                bInterlaced;
  uint32                uTotalH;
  uint32                uTotalBlankH;
  uint32                uTotalV;
  uint32                uTotalBlankV;
  uint32                fFreqH;             //Must divide by 1000 to get the freq
  uint32                fFreqV;             //Must divide by 1000 to get the freq
  uint32                fPixelFreq;         //Must divide by 1000 to get the freq
  uint32                fRefreshRate;       //Must divide by 1000 to get the freq
  bool32                bAspectRatio4_3;
}HDMI_EDID_VideoModePropertyType;

/* -----------------------------------------------------------------------
** Static Variable
** ----------------------------------------------------------------------- */

//LUT is sorted from lowest Active H to highest Active H - ease searching
static const HDMI_EDID_VideoModePropertyType asEDIDDispModeLUT[] = 
{
  /* All 640 H Active */
  {HDMI_VIDEO_FORMAT_640x480p60_4_3, 640, 480, FALSE, 800, 160, 525, 45, 31465, 59940, 25175, 59940, TRUE},
  {HDMI_VIDEO_FORMAT_640x480p60_4_3, 640, 480, FALSE, 800, 160, 525, 45, 31500, 60000, 25200, 60000, TRUE},
  /* All 720 H Active */
  {HDMI_VIDEO_FORMAT_720x576p50_4_3,  720, 576, FALSE, 864, 144, 625, 49, 31250, 50000, 27000, 50000, TRUE},
  {HDMI_VIDEO_FORMAT_720x480p60_4_3,  720, 480, FALSE, 858, 138, 525, 45, 31469, 59940, 27000, 59940, TRUE},
  {HDMI_VIDEO_FORMAT_720x480p60_4_3,  720, 480, FALSE, 858, 138, 525, 45, 31500, 60000, 27027, 60000, TRUE},
  {HDMI_VIDEO_FORMAT_720x576p100_4_3, 720, 576, FALSE, 864, 144, 625, 49, 62500, 100000, 54000, 100000, TRUE},
  {HDMI_VIDEO_FORMAT_720x480p120_4_3, 720, 480, FALSE, 858, 138, 525, 45, 62937, 119880, 54000, 119880, TRUE},
  {HDMI_VIDEO_FORMAT_720x480p120_4_3, 720, 480, FALSE, 858, 138, 525, 45, 63000, 120000, 54054, 120000, TRUE},
  {HDMI_VIDEO_FORMAT_720x576p200_4_3, 720, 576, FALSE, 864, 144, 625, 49, 125000, 200000, 108000, 200000, TRUE},
  {HDMI_VIDEO_FORMAT_720x480p240_4_3, 720, 480, FALSE, 858, 138, 525, 45, 125874, 239760, 108000, 239000, TRUE},
  {HDMI_VIDEO_FORMAT_720x480p240_4_3, 720, 480, FALSE, 858, 138, 525, 45, 126000, 240000, 108108, 240000, TRUE},
  /* All 1024 H Active */
  {HDMI_VIDEO_FORMAT_1024x768p60_4_3, 1024, 768, FALSE, 1344, 320, 806, 38, 48363, 60004, 65000, 60000, TRUE},
  {HDMI_VIDEO_FORMAT_1024x768p70_4_3, 1024, 768, FALSE, 1328, 304, 806, 38, 56476, 70069, 75000, 70000, TRUE},
  {HDMI_VIDEO_FORMAT_1024x768p75_4_3, 1024, 768, FALSE, 1312, 288, 800, 32, 60023, 75029, 78750, 75000, TRUE}, 
  /* All 1280 H Active */
  {HDMI_VIDEO_FORMAT_1280x720p50_16_9,  1280, 720, FALSE, 1980, 700, 750, 30, 37500, 50000, 74250, 50000, FALSE},
  {HDMI_VIDEO_FORMAT_1280x720p60_16_9,  1280, 720, FALSE, 1650, 370, 750, 30, 44955, 59940, 74176, 59940, FALSE},
  {HDMI_VIDEO_FORMAT_1280x720p60_16_9,  1280, 720, FALSE, 1650, 370, 750, 30, 45000, 60000, 74250, 60000, FALSE},
  {HDMI_VIDEO_FORMAT_1280x720p100_16_9, 1280, 720, FALSE, 1980, 700, 750, 30, 75000, 100000, 148500, 100000, FALSE},
  {HDMI_VIDEO_FORMAT_1280x720p120_16_9, 1280, 720, FALSE, 1650, 370, 750, 30, 89909, 119880, 148352, 119880, FALSE},
  {HDMI_VIDEO_FORMAT_1280x720p120_16_9, 1280, 720, FALSE, 1650, 370, 750, 30, 90000, 120000, 148500, 120000, FALSE},
  {HDMI_VIDEO_FORMAT_1280x768p60_5_3,    1280, 768, FALSE, 1644, 384, 798, 30, 47776, 59870, 79500, 60000, FALSE},
  {HDMI_VIDEO_FORMAT_1280x800p60_16_10,  1280, 800, FALSE, 1680, 400, 831, 31, 49702, 59810, 83500, 60000, FALSE},
  {HDMI_VIDEO_FORMAT_1280x960p60_4_3,    1280, 960, FALSE, 1800, 520, 1000,40, 60000, 60000, 108000,60000, TRUE},
  {HDMI_VIDEO_FORMAT_1280x1024p60_5_4,   1280,1024, FALSE, 1688, 408, 1066,42, 63981, 60020, 108000,60000, FALSE},
  /* All 1360 H Active */
  {HDMI_VIDEO_FORMAT_1360x768p60_16_9,   1360, 768, FALSE, 1792, 432, 795, 27, 47712, 60015, 85500, 60000, FALSE},
  /* All 1366 H Active */
  {HDMI_VIDEO_FORMAT_1366x768p60_16_9,   1366, 768, FALSE, 1792, 426, 798, 30, 47712, 59790, 85500, 60000, FALSE},
  /* All 1400 H Active */
  {HDMI_VIDEO_FORMAT_1400x1050p60_4_3,   1400, 1050,FALSE, 1864, 464, 1089, 39, 65317,59978, 121750, 60000, TRUE},
  /* All 1440 H Active */
  {HDMI_VIDEO_FORMAT_1440x576i50_4_3, 1440, 576, TRUE,  1728, 288, 625, 24, 15625, 50000, 27000, 50000, TRUE},
  {HDMI_VIDEO_FORMAT_720x288p50_4_3,  1440, 288, FALSE, 1728, 288, 312, 24, 15625, 50080, 27000, 50000, TRUE},
  {HDMI_VIDEO_FORMAT_720x288p50_4_3,  1440, 288, FALSE, 1728, 288, 313, 25, 15625, 49920, 27000, 50000, TRUE},
  {HDMI_VIDEO_FORMAT_720x288p50_4_3,  1440, 288, FALSE, 1728, 288, 314, 26, 15625, 49761, 27000, 50000, TRUE},
  {HDMI_VIDEO_FORMAT_1440x576p50_4_3, 1440, 576, FALSE, 1728, 288, 625, 49, 31250, 50000, 54000, 50000, TRUE},
  {HDMI_VIDEO_FORMAT_1440x480i60_4_3, 1440, 480, TRUE,  1716, 276, 525, 22, 15734, 59940, 27000, 59940, TRUE},
  {HDMI_VIDEO_FORMAT_1440x240p60_4_3, 1440, 240, FALSE, 1716, 276, 262, 22, 15734, 60054, 27000, 59940, TRUE},
  {HDMI_VIDEO_FORMAT_1440x240p60_4_3, 1440, 240, FALSE, 1716, 276, 263, 23, 15734, 59826, 27000, 59940, TRUE},
  {HDMI_VIDEO_FORMAT_1440x480p60_4_3, 1440, 480, FALSE, 1716, 276, 525, 45, 31469, 59940, 54000, 59940, TRUE},
  {HDMI_VIDEO_FORMAT_1440x480i60_4_3, 1440, 480, TRUE,  1716, 276, 525, 22, 15750, 60000, 27027, 60000, TRUE},
  {HDMI_VIDEO_FORMAT_1440x240p60_4_3, 1440, 240, FALSE, 1716, 276, 262, 22, 15750, 60115, 27027, 60000, TRUE},
  {HDMI_VIDEO_FORMAT_1440x240p60_4_3, 1440, 240, FALSE, 1716, 276, 263, 23, 15750, 59886, 27027, 60000, TRUE},
  {HDMI_VIDEO_FORMAT_1440x480p60_4_3, 1440, 480, FALSE, 1716, 276, 525, 45, 31500, 60000, 54054, 60000, TRUE},
  {HDMI_VIDEO_FORMAT_1440x576i100_4_3,1440, 576, TRUE,  1728, 288, 625, 24, 31250, 100000, 54000, 100000, TRUE},
  {HDMI_VIDEO_FORMAT_1440x480i120_4_3,1440, 480, TRUE,  1716, 276, 525, 22, 31469, 119880, 54000, 119880, TRUE},
  {HDMI_VIDEO_FORMAT_1440x480i120_4_3,1440, 480, TRUE,  1716, 276, 525, 22, 31500, 120000, 54054, 120000, TRUE},
  {HDMI_VIDEO_FORMAT_1440x576i200_4_3,1440, 576, TRUE,  1728, 288, 625, 24, 62500, 200000, 108000, 200000, TRUE},
  {HDMI_VIDEO_FORMAT_1440x480i240_4_3,1440, 480, TRUE,  1716, 276, 525, 22, 62937, 239760, 108000, 239000, TRUE},
  {HDMI_VIDEO_FORMAT_1440x480i240_4_3,1440, 480, TRUE,  1716, 276, 525, 22, 63000, 240000, 108108,240000, TRUE},
  {HDMI_VIDEO_FORMAT_1440x900p60_16_10,  1440, 900, FALSE, 1904, 464, 934, 34, 55935,	59887, 106500, 60000, FALSE},
  /* All 1600 H Active */
  {HDMI_VIDEO_FORMAT_1600x900p60_16_9,   1600, 900, FALSE, 1800, 200, 1000, 100,60000,  60000,  108000,  60000, FALSE},
  /* All 1680 H Active */  
  {HDMI_VIDEO_FORMAT_1680x1050p60_16_10, 1680, 1050,FALSE, 2240, 560, 1089, 39, 65290,  59954,  146250, 60000, FALSE},
  /* All 1920 H Active */
  {HDMI_VIDEO_FORMAT_1920x1080p24_16_9, 1920, 1080, FALSE, 2750, 830, 1125, 45, 26973, 23976, 74176, 23976, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080p24_16_9, 1920, 1080, FALSE, 2750, 830, 1125, 45, 27000, 24000, 74250, 24000, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080p25_16_9, 1920, 1080, FALSE, 2640, 720, 1125, 45, 28125, 25000, 74250, 25000, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080p30_16_9, 1920, 1080, FALSE, 2200, 280, 1125, 45, 33716, 29970, 74176, 29970, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080p30_16_9, 1920, 1080, FALSE, 2200, 280, 1125, 45, 33750, 30000, 74250, 30000, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080p50_16_9, 1920, 1080, FALSE, 2640, 720, 1125, 45, 56250, 50000, 148500,50000, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080i50_16_9, 1920, 1080, TRUE,  2640, 720, 1125, 22, 28125, 50000, 74250, 50000, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080i60_16_9, 1920, 1080, TRUE,  2200, 280, 1125, 22, 33716, 59940, 74176, 59940, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080p60_16_9, 1920, 1080, FALSE, 2200, 280, 1125, 45, 67433, 59940, 148352,59940, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080i60_16_9, 1920, 1080, TRUE,  2200, 280, 1125, 22, 33750, 60000, 74250, 60000, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080p60_16_9, 1920, 1080, FALSE,  2200, 280, 1125, 45, 67500, 60000, 148500,60000, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080i100_16_9,1920, 1080, TRUE,  2640, 720, 1125, 22, 56250, 100000, 148500,100000, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080i120_16_9,1920, 1080, TRUE,  2200, 280, 1125, 22, 67432, 119880, 148352,119980, FALSE},
  {HDMI_VIDEO_FORMAT_1920x1080i120_16_9,1920, 1080, TRUE,  2200, 280, 1125, 22, 67500, 120000, 148500,120000, FALSE},
  /* All 2560 H Active*/
  {HDMI_VIDEO_FORMAT_2560x1600p60_16_10,2560, 1600, FALSE,  2720, 160, 1646, 46, 98713, 59972, 268500, 60000, FALSE},
  /* All 2880 H Active */
  {HDMI_VIDEO_FORMAT_2880x576i50_4_3,2880, 576, TRUE,  3456, 576, 625, 24, 15625, 50000, 54000, 50000, TRUE},
  {HDMI_VIDEO_FORMAT_2880x288p50_4_3,2880, 576, FALSE, 3456, 576, 312, 24, 15625, 50080, 54000, 50000, TRUE},
  {HDMI_VIDEO_FORMAT_2880x288p50_4_3,2880, 576, FALSE, 3456, 576, 313, 25, 15625, 49920, 54000, 50000, TRUE},
  {HDMI_VIDEO_FORMAT_2880x288p50_4_3,2880, 576, FALSE, 3456, 576, 314, 26, 15625, 49761, 54000, 50000, TRUE},
  {HDMI_VIDEO_FORMAT_2880x576p50_4_3,2880, 576, FALSE, 3456, 576, 625, 49, 31250, 50000, 108000,50000, TRUE},
  {HDMI_VIDEO_FORMAT_2880x480i60_4_3,2880, 480, TRUE,  3432, 552, 525, 22, 15734, 59940, 54000, 59940, TRUE},
  {HDMI_VIDEO_FORMAT_2880x240p60_4_3,2880, 480, FALSE, 3432, 552, 262, 22, 15734, 60054, 54000, 59940, TRUE},
  {HDMI_VIDEO_FORMAT_2880x240p60_4_3,2880, 480, FALSE, 3432, 552, 263, 23, 15734, 59940, 54000, 59940, TRUE},
  {HDMI_VIDEO_FORMAT_2880x480p60_4_3,2880, 480, FALSE, 3432, 552, 525, 45, 31469, 59940, 108000,59940, TRUE},
  {HDMI_VIDEO_FORMAT_2880x480i60_4_3,2880, 480, TRUE,  3432, 552, 525, 22, 15750, 60000, 54054, 60000, TRUE},
  {HDMI_VIDEO_FORMAT_2880x240p60_4_3,2880, 240, FALSE, 3432, 552, 262, 22, 15750, 60115, 54054, 60000, TRUE},
  {HDMI_VIDEO_FORMAT_2880x240p60_4_3,2880, 240, FALSE, 3432, 552, 262, 23, 15750, 59886, 54054, 60000, TRUE},
  {HDMI_VIDEO_FORMAT_2880x480p60_4_3,2880, 480, FALSE, 3432, 552, 525, 45, 31500, 60000, 108108,60000, TRUE},
};

// Hardcoded EDID data, this EDID is used when we fail to read the EDID from the sink
static uint8 asDefaultEDIDData[] =
{
  0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00,                   // Block 0 Header
  0x0C, 0xA1, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0x10, 0x0C,       // Vendor/Product Information
  0x01, 0x03,                                                       // EDID Version 1.3
  0x80, 0x50, 0x2D, 0x78, 0x0A,                                     // Basic display paramters and features  (16:9, PTM)
  0x0D, 0xC9, 0xA0, 0x57, 0x47, 0x98, 0x27, 0x12, 0x48, 0x4C,       // Color Characteristics
  0x20, 0x00, 0x00,                                                  // Established Timings 640*480 60Hz
  0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,                   // Standard timing  0x1 = padding
  0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
  0x01, 0x1D, 0x00, 0x72, 0x51, 0xD0, 0x1E, 0x20, 0x6E, 0x28,       // Preferred timing Mode 1280*720p ( CEA Format 4)
  0x55, 0x00, 0x20, 0xC2, 0x31, 0x00, 0x00, 0x30,
  0x01, 0x1D, 0x00, 0x72, 0x51, 0xD0, 0x1E, 0x20, 0x6E, 0x28,       // Detailed timing descriptor 1280*720p ( CEA Format 4)
  0x55, 0x00, 0x20, 0xC2, 0x31, 0x00, 0x00, 0x30,
  0x00, 0x00, 0x00, 0xFC, 0x00, 0x4D, 0x59,                         // First Monitor Descriptor
  0x20, 0x48, 0x44, 0x54, 0x56, 0x0A, 0x20, 0x20, 0x20, 0x20, 0x20, 
  0x00, 0x00, 0x00, 0xFD, 0x00, 0x3B, 0x3D,                         // Second Monitor Descriptor
  0x0F, 0x2E, 0x08, 0x00, 0x0A, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 
  0x00,                                                             // Extension Flag
  0xC3,                                                              // Checksum
};    

// Detailed Timing descriptor for HDMI_VIDEO_FORMAT_1920x1080p60_16_9
static uint8 as1080pDetailedTiming[] =
{
  
  0x02, 0x3A, 0x80, 0x18, 0x71, 0x38, 0x2D, 0x40, 0x58, 0x2C,       // Detailed timing descriptor 1920*1080p ( CEA Format 16)
  0x04, 0x05, 0x0F, 0x48, 0x42, 0x00, 0x00, 0x1E,
};



/* -----------------------------------------------------------------------
** Static Functions
** ----------------------------------------------------------------------- */
/*
 * HDMI_EDID_DataBlockOffsetSetup()
 */
static void HDMI_EDID_DataBlockOffsetSetup(uint8* pDataBuf, uint32 uNumOfCEABlocks);

/*
 * HDMI_EDID_Parser_VendorBlockInfo()
 */
static MDP_Status HDMI_EDID_Parser_VendorBlockInfo(uint8* pDataBuf, HDMI_DispModeListType* pDispModeList, uint32 uNumOfCEABlocks);

/*
 * HDMI_EDID_Parser_GetDispMode()
 */
static MDP_Status HDMI_EDID_Parser_GetDispMode(uint8* pDataBuf, HDMI_DispModeListType* pDispModeList, uint32 uNumOfCEABlocks, bool32 bShortDesc);

/*
 * HDMI_EDID_Parser_Detail_Desc()
 */
static MDP_Status HDMI_EDID_Parser_Detail_Desc(uint8* pDataBuf, HDMI_VideoFormatType* pDispMode);

/*
 * HDMI_EDID_Append3DDispModes()
 */
static void HDMI_EDID_Append3DDispModes(uint8* pDataBuf, uint32 uStartingOffset, HDMI_DispModeListType* pDispModeList, uint32 uCEABlockIndex);

/*
 * HDMI_EDID_InsertDisplayMode()
 */
static void HDMI_EDID_InsertDisplayMode(uint8* pDataBuf, HDMI_DispModeListType* pDispModeList, uint32 uNumOfDispMode, uint32 uBufferIndex, HDMI_VideoFormatType eVideoFormat, bool32 bAudioSupported);

/*
 * HDMI_EDID_Parser_DetailTiming()
 */
static MDP_Status HDMI_EDID_Parser_DetailTiming(uint8* pDataBuf, HDMI_DispModeListType* pDispModeList);

/*
 * HDMI_EDID_Parser_InsertDispFormat()
 */
static void HDMI_EDID_Parser_InsertDispFormat(HDMI_DispModeListType* pDispModeList, HDMI_VideoFormatType eVideoFormat, bool32 bAudioSupported);

/*
 * HDMI_EDID_IsDispModeSupportAudio()
 */
static bool32 HDMI_EDID_IsDispModeSupportAudio(void);

/*
 * HDMI_EDID_Parser_EstablishTimingIII()
 */
static void HDMI_EDID_Parser_EstablishTimingIII(uint8* pDataBuf, uint32 uEstablishTimingOffset, HDMI_DispModeListType* pDispModeList);

/*
 * HDMI_EDID_Parser_DisplayDescriptor()
 */
static void HDMI_EDID_Parser_DisplayDescriptor(uint8* pDataBuf, uint32 uDescOffset, HDMI_DispModeListType* pDispModeList);

/*
 * HDMI_EDID_Parser_DisplayDescriptor()
 */
static uint32 HDMI_EDID_GetNumOfCEAExtBlocks(void);

/*
 * HDMI_EDID_ReadEDID
 */
static MDP_Status HDMI_EDID_ReadEDID(HDMI_DeviceDataType* pSharedData, uint32* puNumOfCEABlocks);

/*
 * HDMI_EDID_LoadStaticEDID
 */

static MDP_Status HDMI_EDID_LoadStaticEDID(HDMI_DeviceDataType* pSharedData, uint32* puNumOfCEABlocks);



/****************************************************************************
*
** FUNCTION: HDMI_EDID_DataBlockOffsetSetup()
*/
/*!
* \brief
*   The \b HDMI_EDID_DataBlockOffsetSetup initializes the various data block offsets
*
* \param [in]      pDataBuf         - Buffer that contains EDID data
* \param [out]     uNumOfCEABlocks  - Number of CEA blocks in this EDID
**
* \retval void
*
****************************************************************************/
static void HDMI_EDID_DataBlockOffsetSetup(uint8* pDataBuf, uint32 uNumOfCEABlocks)
{
  
  HDMI_DeviceDataType *pSharedData = HDMI_GETCTX();
  uint32 uWorkingOffset   = 0;
  uint32 uStartingOffset  = 0;
  uint32 uDataTagCode     = 0;
  uint32 uNumOfBytes      = 0;
  uint32 uIndex           = 0;
  uint32 uI               = 0;
  uint32 uNumOfBlocks     = uNumOfCEABlocks;
  uint32 uVideoBlockCount = 0;

  /* Initialize global variable for all the data block offsets to be 0 */
  MDP_OSAL_MEMZERO(&pSharedData->sDataBlockDesc, sizeof(HDMI_EDID_DataBlockType));

  /* Check if block 2 is a block map or not, all relative working offset needs to shift by 1 block size if block 2 is a block map */
  if(HDMI_EDID_EXTENSION_BLOCK_MAP_TAG_CODE == pDataBuf[HDMI_EDID_CEA_EXTENSION_VERSION_OFFSET - 1])
  {
    uStartingOffset += HDMI_EDID_BLOCK_SIZE;
    if(uNumOfBlocks)
      uNumOfBlocks   -= 1;
  }

  /* First data block offset always start from block 2 or greater */
  uStartingOffset += HDMI_EDID_BLOCK_SIZE;

  for(uI = 0; uI < uNumOfBlocks; uI++)
  {
    /* Beginning of each extension block */
    uStartingOffset += (uI * HDMI_EDID_BLOCK_SIZE);

    /* Starting offset of the detail timing descriptors, it also indicates the end of all data block within a CEA extension block */
    pSharedData->sDataBlockDesc.sDetailTimingBlockDesc[uI].uDetailTimingBlockOffset = pDataBuf[uStartingOffset + HDMI_EDID_DETAIL_TIMING_RELATIVE_OFFSET] + uStartingOffset;

    /* Offset to the first tag code in an CEA block */
    uWorkingOffset = uStartingOffset + HDMI_EDID_DATA_BLOCK_RELATIVE_OFFSET;

    /* First check if there is any data block */
    if(uWorkingOffset != pSharedData->sDataBlockDesc.sDetailTimingBlockDesc[uI].uDetailTimingBlockOffset)
    {
      /* Reset the video block count for each CEA extension block */
      uVideoBlockCount = 0;

      for(uIndex = 0; uIndex < HDMI_EDID_MAX_NUM_OF_TAG_CODES; uIndex++)
      {
        /* Extract the tag code */
        uDataTagCode = (pDataBuf[uWorkingOffset] & HDMI_EDID_DATA_BLOCK_TAG_MASK) >> HDMI_EDID_DATA_BLOCK_TAG_SHIFT;
        /* Find out the length in the data block */
        uNumOfBytes = pDataBuf[uWorkingOffset] & HDMI_EDID_DATA_BLOCK_LENGTH_MASK;

        switch(uDataTagCode)
        {
          case HDMI_EDID_VIDEO_DATA_BLOCK_TAG_CODE:
          {
            /* Video data block offset */
            pSharedData->sDataBlockDesc.sVideoDataBlockDesc[uI].uVideoDataBlockOffset[uVideoBlockCount] = uWorkingOffset;
            pSharedData->sDataBlockDesc.sVideoDataBlockDesc[uI].uDataBlockLength[uVideoBlockCount]      = uNumOfBytes;
            uVideoBlockCount++;

            break;
          }
          case HDMI_EDID_AUDIO_DATA_BLOCK_TAG_CODE:
          {
            /* Audio data block offset is relevant to the video data block size */
            pSharedData->sDataBlockDesc.sAudioDataBlockDesc[uI].uAudioDataBlockOffset    = uWorkingOffset;
            pSharedData->sDataBlockDesc.sAudioDataBlockDesc[uI].uDataBlockLength         = uNumOfBytes;
            break;
          }
          case HDMI_EDID_SPEAKER_ALLOC_DATA_BLOCK_TAG_CODE:
          {
            /* Speaker allocation data block offset is relevant to the video data block size */
            pSharedData->sDataBlockDesc.sSpeakerDataBlockDesc[uI].uSpeakerAllocBlockOffset = uWorkingOffset;
            pSharedData->sDataBlockDesc.sSpeakerDataBlockDesc[uI].uDataBlockLength         = uNumOfBytes;
            break;
          }
          case HDMI_EDID_VENDOR_SPECIFIC_DATA_BLOCK_TAG_CODE:
          {
            /* Speaker allocation data block offset is relevant to the video data block size */
            pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uI].uVendorSpecificBlockOffset = uWorkingOffset;
            pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uI].uDataBlockLength           = uNumOfBytes;
            /* Extension field exists */
            if(uNumOfBytes > HDMI_EDID_VENDOR_SPECIFIC_BLOCK_IEEE_ID_LENGTH + HDMI_EDID_VENDOR_SPECIFIC_PHYSICAL_ADDR_LENGTH)
            {
              pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uI].uVendorSpecificBlockExtOffset = uWorkingOffset + HDMI_EDID_VENDOR_SPECIFIC_BLOCK_IEEE_ID_LENGTH + 
                                                                                               HDMI_EDID_VENDOR_SPECIFIC_PHYSICAL_ADDR_LENGTH + 1;
            }
            break;
          }
          case HDMI_EDID_EXTENDED_DATA_BLOCK_TAG_CODE:
          {
            /* Move to next byte to find out the extended tag code */
            switch(pDataBuf[uWorkingOffset + 1])
            {
              case HDMI_EDID_EXTENDED_COLORIMETRY_TAG_CODE:
              {
                pSharedData->sDataBlockDesc.sColorimetryDataBlockDesc[uI].uColorimetryDataBlockOffset = uWorkingOffset;
                pSharedData->sDataBlockDesc.sColorimetryDataBlockDesc[uI].uDataBlockLength            = uNumOfBytes;
                break;
              }
              case HDMI_EDID_EXTENDED_VENDOR_SPECIFIC_VIDEO_TAG_CODE:
              {
                /* Video data block offset */
                pSharedData->sDataBlockDesc.sExtVideoDataBlockDesc[uI].uVideoDataBlockOffset = uWorkingOffset;
                pSharedData->sDataBlockDesc.sExtVideoDataBlockDesc[uI].uDataBlockLength      = uNumOfBytes;
                break;
              }
              case HDMI_EDID_EXTENDED_VENDOR_SPECIFIC_AUDIO_TAG_CODE:
              {
                /* Audio data block offset is relevant to the video data block size */
                pSharedData->sDataBlockDesc.sExtAudioDataBlockDesc[uI].uAudioDataBlockOffset = uWorkingOffset;
                pSharedData->sDataBlockDesc.sExtAudioDataBlockDesc[uI].uDataBlockLength      = uNumOfBytes;
                break;
              }
              case HDMI_EDID_EXTENDED_VIDEO_CAP_TAG_CODE:
              {
                pSharedData->sDataBlockDesc.sVideoCapDataBlockDesc[uI].uVideoCapDataBlockOffset = uWorkingOffset;
                pSharedData->sDataBlockDesc.sVideoCapDataBlockDesc[uI].uDataBlockLength         = uNumOfBytes;
                break;
              }
              default:
                uIndex = HDMI_EDID_MAX_NUM_OF_TAG_CODES; /* Exit For Loop */
                break;
            }
            break;
          }
          default:
          {
            uIndex = HDMI_EDID_MAX_NUM_OF_TAG_CODES; /* Exit For Loop */
            break;
          }
        }
        uWorkingOffset += uNumOfBytes;
        /* Next byte will be the next tag code */
        uWorkingOffset++;

        /* Check whether we completed iterating all data blocks within this CEA block */
        if(uWorkingOffset == pSharedData->sDataBlockDesc.sDetailTimingBlockDesc[uI].uDetailTimingBlockOffset)
        {
          /* Move to the next CEA extension block */
          break;
        }
      }
    }
  }
}
/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_Detail_Desc()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_Detail_Desc parses the EDID structure and find out
*   all the video mode supported using detailed timing descriptors.
*
* \param [in]      pDataBuf        - Buffer that contains EDID data
* \param [out]     pDispMode       - Matching display mode 
*
* \retval MDP_Status
*
****************************************************************************/
static MDP_Status HDMI_EDID_Parser_Detail_Desc(uint8* pDataBuf, HDMI_VideoFormatType* pDispMode)
{
  MDP_Status  eStatus          = MDP_STATUS_FAILED;
  bool32      bAspectRatio4_3  = FALSE;
  bool32      bInterlaced      = FALSE;
  uint32      uResult          = 0;
  uint32      uResult1         = 0;
  uint32      uActiveH         = 0;
  uint32      uActiveV         = 0;
  uint32      uBlankH          = 0;
  uint32      uBlankV          = 0;
  uint32      uIndex           = 0;
  uint32      uMaxNumOfElement = 0;
  uint32      uImageSizeH      = 0;
  uint32      uImageSizeV      = 0;
  uint32      uPixelClock      = 0;

  if(pDataBuf)
  {
    uPixelClock   = pDataBuf[HDMI_EDID_TIMING_PIXEL_CLOCK_UPPER_BYTE_OFFSET];
    uPixelClock <<= 8;
    uPixelClock  |= pDataBuf[HDMI_EDID_TIMING_PIXEL_CLOCK_LOWER_BYTE_OFFSET];
    uPixelClock   = uPixelClock * 10; // Convert to KHZ for LUT search

    /* See VESA Spec */
    uActiveH   =((pDataBuf[HDMI_EDID_TIMING_DESC_UPPER_H_NIBBLE_OFFSET] >> HDMI_EDID_TIMING_H_ACTIVE_UPPER_NIBBLE_SHIFT) & 
                  HDMI_EDID_TIMING_H_ACTIVE_UPPER_NIBBLE_MASK);
    uActiveH <<= 8; //Shift 8 bits 
    uActiveH  |= pDataBuf[HDMI_EDID_TIMING_DESC_H_ACTIVE_OFFSET]; //Shift 8 bits 

    uBlankH   =(pDataBuf[HDMI_EDID_TIMING_DESC_UPPER_H_NIBBLE_OFFSET] & HDMI_EDID_TIMING_H_BLANK_UPPER_NIBBLE_MASK);
    uBlankH <<= 8; //Shift 8 bits 
    uBlankH  |= pDataBuf[HDMI_EDID_TIMING_DESC_H_BLANK_OFFSET]; //Shift 8 bits 

    uActiveV   =((pDataBuf[HDMI_EDID_TIMING_DESC_UPPER_V_NIBBLE_OFFSET] >> HDMI_EDID_TIMING_V_ACTIVE_UPPER_NIBBLE_SHIFT) & 
                  HDMI_EDID_TIMING_V_ACTIVE_UPPER_NIBBLE_MASK);
    uActiveV <<= 8; //Shift 8 bits 
    uActiveV  |= pDataBuf[HDMI_EDID_TIMING_DESC_V_ACTIVE_OFFSET]; //Shift 8 bits 

    uBlankV   =(pDataBuf[HDMI_EDID_TIMING_DESC_UPPER_V_NIBBLE_OFFSET] & HDMI_EDID_TIMING_V_BLANK_UPPER_NIBBLE_MASK);
    uBlankV <<= sizeof(uint8); //Shift 8 bits 
    uBlankV  |= pDataBuf[HDMI_EDID_TIMING_DESC_V_BLANK_OFFSET]; //Shift 8 bits 

    uImageSizeH   = (pDataBuf[HDMI_EDID_TIMING_DESC_IMAGE_SIZE_UPPER_NIBBLE_OFFSET] >> HDMI_EDID_TIMING_H_IMAGE_SIZE_UPPER_NIBBLE_SHIFT) &
                     HDMI_EDID_TIMING_H_IMAGE_SIZE_UPPER_NIBBLE_MASK;
    uImageSizeH <<= 8;
    uImageSizeH  |= pDataBuf[HDMI_EDID_TIMING_DESC_H_IMAGE_SIZE_OFFSET];

    uImageSizeV   = (pDataBuf[HDMI_EDID_TIMING_DESC_IMAGE_SIZE_UPPER_NIBBLE_OFFSET]& HDMI_EDID_TIMING_V_IMAGE_SIZE_UPPER_NIBBLE_MASK);
    uImageSizeV <<= 8;
    uImageSizeV  |= pDataBuf[HDMI_EDID_TIMING_DESC_V_IMAGE_SIZE_OFFSET];

    /* Calculating aspect ratio 4:3 */
    uResult  = uImageSizeH * 3;
    uResult1 = uImageSizeV * 4;

    /* Due to rounding of the image size in the EDID info, we need to provide a range to check against */
    if(uResult == uResult1)
    {
      bAspectRatio4_3 = TRUE;
    }
    else if(uResult1 > uResult)
    {
      if(HDMI_EDID_TIMING_ASPECT_RATIO_CHECK_RANGE > (uResult1 - uResult))
      {
        bAspectRatio4_3 = TRUE;
      }
    }
    else
    {
      if(HDMI_EDID_TIMING_ASPECT_RATIO_CHECK_RANGE > (uResult - uResult1))
      {
        bAspectRatio4_3 = TRUE;
      }
    }

    uMaxNumOfElement  = (sizeof(asEDIDDispModeLUT)/ sizeof(HDMI_EDID_VideoModePropertyType));
    uIndex            = uMaxNumOfElement >> 2;

    //Break table in half and search using H Active
    if(uActiveH <= asEDIDDispModeLUT[uIndex].uActiveH)
      uIndex = 0;

    bInterlaced = (pDataBuf[HDMI_EDID_TIMING_DESC_INTERLACE_OFFSET] & HDMI_EDID_TIMING_INTERLACE_MASK) >> HDMI_EDID_TIMING_INTERLACE_SHIFT;

    /* Interlace mode the active V should multiply by 2 because EDID reports field height only */
    if(bInterlaced)
      uActiveV = uActiveV << 1;

    while(uIndex < uMaxNumOfElement)
    {
      if(bInterlaced == asEDIDDispModeLUT[uIndex].bInterlaced  &&
         uActiveH    == asEDIDDispModeLUT[uIndex].uActiveH     &&
         uBlankH     == asEDIDDispModeLUT[uIndex].uTotalBlankH &&
         uBlankV     == asEDIDDispModeLUT[uIndex].uTotalBlankV && 
         uActiveV    == asEDIDDispModeLUT[uIndex].uActiveV)
      {
        /* Pixel clock needs to check against a range to include non integer value of the refresh rates */
        if(((asEDIDDispModeLUT[uIndex].fPixelFreq - HDMI_EDID_TIMING_PIXEL_CLOCK_RANGE)  < uPixelClock) &&  (uPixelClock < asEDIDDispModeLUT[uIndex].fPixelFreq + HDMI_EDID_TIMING_PIXEL_CLOCK_RANGE))
        {
          /* 16:9 VIC code of the same video format is 1 greater than the 4:3 aspect ratio VIC */
          if(asEDIDDispModeLUT[uIndex].bAspectRatio4_3 && (!bAspectRatio4_3))
            *pDispMode = (HDMI_VideoFormatType)(asEDIDDispModeLUT[uIndex].eVideoCode + 1);
          else
            *pDispMode = (HDMI_VideoFormatType)asEDIDDispModeLUT[uIndex].eVideoCode;

          //Check whether HDMI TX core supports this mode
          if(gSupportedVideoModeLUT[*pDispMode].bSupported)
            eStatus = MDP_STATUS_OK;
          break;
        }
      }
      uIndex++;
    }
  }

  return eStatus;
}


/****************************************************************************
*
** FUNCTION: HDMI_EDID_ModeSupported()
*/
/*!
* \brief
*   The \b HDMI_EDID_ModeSupported query the list of supported 
*   all the established video mode supported in standard timing section.
*
* \param [in]      eVesaMode       - Buffer that contains EDID data
*
* \retval  None
*
****************************************************************************/
static bool32 HDMI_EDID_ModeSupported(HDMI_VideoFormatType eVideoMode)
{
  bool32 bResult = FALSE;
  uint32 uIndex;

  for (uIndex=0;uIndex<HDMI_VIDEO_FORMAT_MAX;uIndex++)
  {
    if ((eVideoMode == gSupportedVideoModeLUT[uIndex].eVideoFormat) &&
        (TRUE == gSupportedVideoModeLUT[uIndex].bSupported))
    {
      bResult = TRUE;
    }
  }

  return bResult;
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_EstablishedTimings()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_EstablishedTimings parses the EDID established timing section and find out
*   all the established video mode supported in standard timing section.
*
* \param [in]      pDataBuf        - Buffer that contains EDID data
* \param [in/out]  pDispModeList   - Array that stores all the display mode supported
*
* \retval  None
*
****************************************************************************/
static void HDMI_EDID_Parser_EstablishedTimings(uint8* pDataBuf, HDMI_DispModeListType* pDispModeList )
{

  /* Parse the established timing section 2 to find out which modes are supported*/
  uint8 uEstablishedTimingII = pDataBuf[HDMI_EDID_ESTABLISHED_TIMINGII_START_OFFSET];

  if ((HDMI_EDID_ESTABLISHED_TIMINGII_1024x768p60 & uEstablishedTimingII) &&
      (TRUE == HDMI_EDID_ModeSupported(HAL_HDMI_VIDEO_FORMAT_1024x768p60_4_3)))
  {   

    /* The resolution read from the establish section corresponds to VESA ,  hence no audio support flag is set*/ 
    HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, HDMI_VIDEO_FORMAT_1024x768p60_4_3, FALSE);    
  }
  
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_StandardTimings()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_StandardTimings parses the EDID standard timing section and find out
*   all the standard video mode supported in standard timing section.
*
* \param [in]      pDataBuf                          - Buffer that contains EDID data
* \param [in]      uStandardTimingStartOffset        - Starting offset of the standard timing section
* \param [in]      uStandardTimingEndOffset          - End offset of the standard timing section
* \param [in/out]  pDispModeList                     - Array that stores all the display mode supported
*
* \retval  None
*
****************************************************************************/
static void HDMI_EDID_Parser_StandardTimings(uint8* pDataBuf, uint32 uStandardTimingStartOffset, uint32 uStandardTimingEndOffset, HDMI_DispModeListType* pDispModeList )
{
  HDMI_VideoFormatType      eVideoFormat     = HDMI_VIDEO_FORMAT_640x480p60_4_3;
  uint32                    uMaxNumOfElement = (sizeof(asEDIDDispModeLUT)/ sizeof(HDMI_EDID_VideoModePropertyType));
  uint32                    uI               = 0;
  uint32                    uIndex           = 0;
  uint32                    uActiveH         = 0;
  uint32                    uActiveV         = 0;
  uint32                    uRefreshRate     = 0;   
  uint32                    uEdidRevision    = 0;
  uint32                    uAspectRatio     = 0;

  // As some mode is only reported in standard timing section, so need add standard timing parser.
  for ( uI = uStandardTimingStartOffset; uI < uStandardTimingEndOffset; uI += 2)
  {
    //First byte: Horizontal resolution. Multiply by 8, then add 248 for actual value.
    uActiveH = (pDataBuf[uI] << HDMI_EDID_STANDARD_TIMING_HRESOLUTION_SHIFT) + HDMI_EDID_STANDARD_TIMING_HRESOLUTION_BASE;
    
    if (uActiveH > HDMI_EDID_STANDARD_TIMING_HRESOLUTION_BASE + 8) // pDataBuf[uI] could be 0 or 1 if there is no stand time.
    {
      // Bits 6 & 7 : Aspect ratio. Actual vertical resolution depends on horizontal resolution.
      // 00=16:10, 01=4:3, 10=5:4, 11=16:9 
      uAspectRatio = (pDataBuf[uI + 1] & HDMI_EDID_STANDARD_TIMING_ASPECTRATIO_MASK) >> HDMI_EDID_STANDARD_TIMING_ASPECTRATIO_SHIFT;

      // Standard timing support modes with fixed aspect ratios.
      switch (uAspectRatio)
      {
        case 0:
           uEdidRevision = pDataBuf[HDMI_EDID_REVISION_OFFSET];
        
           //EDID structures prior to version 1 revision 3 defined the bit combination of 0 0 to indicate a 1:1 aspect ratio 
           if(0x03 > uEdidRevision)
           {  
              uActiveV = uActiveH;
           }
           else
           {          
             uActiveV = uActiveH * 10 / 16;
           }
           break;
        
        case 1:
           uActiveV = uActiveH * 3 / 4;
           break;
        
        case 2:
           uActiveV = uActiveH * 4 / 5;
           break;
        
        case 3:
           uActiveV = uActiveH * 9 / 16;
           break;
        default:
           //Not support standard timing identification
           uActiveV = 0;
          break;
      }      
      if (uActiveV > 0)
      {
        //bits 0-5 are vertical frequency. Add 60 to get actual value. Multiply 1000 to get khz
        uRefreshRate = ((pDataBuf[uI + 1] & HDMI_EDID_STANDARD_TIMING_VFREQUENCY_BITS_MASK) + HDMI_EDID_STANDARD_TIMING_MIN_VFREQUENCY) * 1000;
        uIndex = 0;
        while(uIndex < uMaxNumOfElement)
        {
          if((FALSE        == asEDIDDispModeLUT[uIndex].bInterlaced)  &&
             (uActiveH     == asEDIDDispModeLUT[uIndex].uActiveH)     &&
             (uActiveV     == asEDIDDispModeLUT[uIndex].uActiveV)     && 
             (uRefreshRate == asEDIDDispModeLUT[uIndex].fRefreshRate))
          {            
            eVideoFormat = asEDIDDispModeLUT[uIndex].eVideoCode;

            /* The resolution read from the standard timing corresponds to VESA , hence no audio support flag is set*/               
            HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, eVideoFormat, FALSE);   

            break;
          }
          uIndex++;
        }
      }
    }
  }
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_IsAudioSupport()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_IsAudioSupport parses the information if monitor support audio.
*
* \param [in]  None
*
* \retval  bool
*
****************************************************************************/
static bool32 HDMI_EDID_Parser_IsAudioSupport()
{
  HDMI_DeviceDataType* pSharedData      = HDMI_GETCTX();
  uint32               uNumOfCEABlocks  = 0;
  uint32               uStartingOffset  = 0;

  pSharedData->bAudioSupport = FALSE;

  uNumOfCEABlocks = HDMI_EDID_GetNumOfCEAExtBlocks();
  
  /* Skip Block 2 if block 2 is a BLOCK MAP */
  if(HDMI_EDID_EXTENSION_BLOCK_MAP_TAG_CODE == pSharedData->auEDIDCache[HDMI_EDID_CEA_EXTENSION_VERSION_OFFSET - 1])
  {
    uStartingOffset += HDMI_EDID_BLOCK_SIZE;
    if(uNumOfCEABlocks)
        uNumOfCEABlocks -= 1;
  }

  uStartingOffset += HDMI_EDID_BLOCK_SIZE;

  if (uNumOfCEABlocks)
  {
    if (pSharedData->auEDIDCache[uStartingOffset + HDMI_EDID_CEA_EXTENSION_CABS_RELATIVE_OFFSET] & HDMI_EDID_BASIC_AUDIO_SUPPORT_MASK)
    {
      pSharedData->bAudioSupport = TRUE;
    }
  }
  return pSharedData->bAudioSupport;
  
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetDispMode()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetDispMode parses the EDID structure and find out
*   all the supported video mode.
*
* \param [in]      pDataBuf        - Buffer that contains EDID data
* \param [in/out]  pDispMode       - Array that stores all the display mode supported
* \param [in]      uNumOfCEABlocks - Number of CEA blocks proceeding Block 0
* \param [in]      bShortDesc      - Short Video descriptor or detailed timing descriptors
*
* \retval MDP_Status
*
****************************************************************************/
static MDP_Status HDMI_EDID_Parser_GetDispMode(uint8* pDataBuf, HDMI_DispModeListType* pDispModeList, uint32 uNumOfCEABlocks, bool32 bShortDesc)
{
  MDP_Status            eStatus        = MDP_STATUS_OK;
  HDMI_DeviceDataType *pSharedData     = HDMI_GETCTX();
  uint32                uNumOfBlocks   = uNumOfCEABlocks;

  if(NULL == pDataBuf)
  {
     eStatus = MDP_STATUS_BAD_PARAM;
  }
  else 
  {
    if(uNumOfBlocks)
    {
      uint32                uNumOfDispMode   = 0;
      uint32                uVideoDataTag    = 0;
      uint32                uWorkingOffset   = 0;
      uint32                uCEABlockIndex   = 0;
      uint32                uCEAExtensionVer = 0;
      bool32                bSkipBlockMap    = FALSE;
      uint32                uDescOffset      = 0;
      
      uint32                uVideoBlockCount = 0;
      // Update and cache the the audio support information read from audio bcaps 
      bool32               bAudioSupported   = HDMI_EDID_Parser_IsAudioSupport();
        
      // Move to the next block if block 2 is a BLOCK MAP 
      if(HDMI_EDID_EXTENSION_BLOCK_MAP_TAG_CODE == pDataBuf[HDMI_EDID_CEA_EXTENSION_VERSION_OFFSET - 1])
      {
        bSkipBlockMap = TRUE;

        if(uNumOfBlocks)
        {
          uNumOfBlocks -= 1;
        }  
      }

      // Iterate through all the CEA extension blocks and extract all applicable display modes
      for(uCEABlockIndex = 0; uCEABlockIndex < uNumOfBlocks; uCEABlockIndex++)
      {
        if(bSkipBlockMap)
        {
          uCEAExtensionVer = pDataBuf[HDMI_EDID_CEA_EXTENSION_VERSION_OFFSET + ((uCEABlockIndex + 1) * HDMI_EDID_BLOCK_SIZE)];
          // Block 2 is a block map, hence break out of loop when index reaches 1 less the total number of extension blocks 
        }
        else
        {
          uCEAExtensionVer = pDataBuf[HDMI_EDID_CEA_EXTENSION_VERSION_OFFSET + (uCEABlockIndex * HDMI_EDID_BLOCK_SIZE)];
        }  

        switch(uCEAExtensionVer)
        {
          case 1:
            break;
          //Obsolete from CEA_861D spec
          case 2:  
            break;
          case 3:
          {
            bShortDesc = TRUE;
            break;
          }
          default:
            break;
        }

        //
        // when usingVersion 3 CEA Extension: All CEA video formats shall be advertised using Short Video Descriptors,  
        // even if they are also advertised using the Detailed Timing Descriptors.
        // Hence parsing of detailed timing descriptor is not required for the extension block 
        //
        if(bShortDesc)
        {
          
          for(uVideoBlockCount = 0; uVideoBlockCount < HDMI_EDID_MAX_NUM_OF_VIDEO_DATA_BLOCKS; uVideoBlockCount++)
          {
            uWorkingOffset = pSharedData->sDataBlockDesc.sVideoDataBlockDesc[uCEABlockIndex].uVideoDataBlockOffset[uVideoBlockCount];

            if(uWorkingOffset && (uWorkingOffset < pSharedData->uEDIDDataLength))
            {
              
              // Extract the video data tag code 
              uVideoDataTag = (pDataBuf[uWorkingOffset] & HDMI_EDID_DATA_BLOCK_TAG_MASK) >> HDMI_EDID_DATA_BLOCK_TAG_SHIFT;

              // Check whether this block has video data block
              if(HDMI_EDID_VIDEO_DATA_BLOCK_TAG_CODE == uVideoDataTag)
              {
                uNumOfDispMode = pDataBuf[uWorkingOffset] & HDMI_EDID_DATA_BLOCK_LENGTH_MASK;
              }  
              else
              {
                uNumOfDispMode = 0;
              }  

              // Determine whether the detected mode can be inserted to the display mode list 
              if(uNumOfDispMode)
              {
                // Addding short video descriptor to the modes list, audio support based on  audio bcpas read from the extension block 
                HDMI_EDID_InsertDisplayMode(pDataBuf, pDispModeList, uNumOfDispMode, uWorkingOffset,HDMI_VIDEO_FORMAT_NONE, bAudioSupported);
              }  
              
            }
            else
            {
              // Exit loop
              break;
            }
          }

          uWorkingOffset = pSharedData->sDataBlockDesc.sExtVideoDataBlockDesc[uCEABlockIndex].uVideoDataBlockOffset + 1;

          // Handle extended video data block 
          if(pSharedData->sDataBlockDesc.sExtVideoDataBlockDesc[uCEABlockIndex].uVideoDataBlockOffset  && (uWorkingOffset < pSharedData->uEDIDDataLength))
          {
            // Extract the video data tag code 
            uVideoDataTag = (pDataBuf[uWorkingOffset] & HDMI_EDID_DATA_BLOCK_TAG_MASK) >> HDMI_EDID_DATA_BLOCK_TAG_SHIFT;

            if(HDMI_EDID_EXTENDED_VENDOR_SPECIFIC_VIDEO_TAG_CODE == uVideoDataTag)
            {
              uNumOfDispMode = pDataBuf[uWorkingOffset] & HDMI_EDID_DATA_BLOCK_LENGTH_MASK;
            }  
            else
            {
              uNumOfDispMode = 0;
            }  

            // Determine whether the detected mode can be inserted to the display mode list 
            if(uNumOfDispMode)
            {
               // Addding short video descriptor to the modes list, 
               //audio support based on  audio bcpas read from the extension block 
               HDMI_EDID_InsertDisplayMode(pDataBuf, pDispModeList, uNumOfDispMode, uWorkingOffset, HDMI_VIDEO_FORMAT_NONE, bAudioSupported);
              
            }
          }
        }
        //
        // When using CEA Extension Version 1, all of the CEA video formats listed in E-EDID are described using
        // Detailed Timing Descriptors 
        //
        else
        {
          // Parse the CEA blocks accordingly for the detail timing descriptors 
          if(pSharedData->sDataBlockDesc.sDetailTimingBlockDesc[uCEABlockIndex].uDetailTimingBlockOffset)
          {
            // Reset counting index
            uint32   uI                            = 0;
            bool32   bExitLoop                     = FALSE;
            HDMI_VideoFormatType  eVideoFormat     = HAL_HDMI_VIDEO_FORMAT_640x480p60_4_3; 

            while(!bExitLoop)
            {
              uDescOffset = pSharedData->sDataBlockDesc.sDetailTimingBlockDesc[uCEABlockIndex].uDetailTimingBlockOffset + (uI * HDMI_EDID_DETAIL_TIMING_DESC_BLOCK_SIZE);

              if((uDescOffset < pSharedData->uEDIDDataLength) && pDataBuf[uDescOffset])
              {
                if (MDP_STATUS_OK == (eStatus = HDMI_EDID_Parser_Detail_Desc(&pDataBuf[uDescOffset], &eVideoFormat)))
                {                  
                  // Detailed timing read from extension block should support audio 
                  // But update it based on audio bcpas read from the extension block 
                  (void) HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, eVideoFormat, bAudioSupported);
                  
                }

                // Increment to the next detail timing descriptor 
                uI++;
              }
              else
              {
                bExitLoop = TRUE;
              }  
            }
          }
        }
      }
    }    

    //Parse the detail timing present in the EDID block
    HDMI_EDID_Parser_DetailTiming(pDataBuf, pDispModeList);

    // Handle modes in the established timing section
    HDMI_EDID_Parser_EstablishedTimings(pDataBuf, pDispModeList);

    // Handle modes in the standard timing section
    HDMI_EDID_Parser_StandardTimings(pDataBuf, HDMI_EDID_STANDARD_TIMING_START_OFFSET, HDMI_EDID_STANDARD_TIMING_END_OFFSET, pDispModeList);

    //Need to add default 640 by 480 timings, in case not described in the EDID structure or CEA extension block.
    //All DTV sink devices should support this mode
    HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, HDMI_VIDEO_FORMAT_640x480p60_4_3, FALSE);
  
  }


  return eStatus;
}


/****************************************************************************
*
** FUNCTION: HDMI_EDID_Append3DDispModes()
*/
/*!
* \brief
*   The \b HDMI_EDID_Append3DDispModes parses the EDID structure and find out
*   the 3D capabilities from vendor specific data block.
*
* \param [in]      pDataBuf        - Buffer that contains EDID data
* \param [in]      uWorkingOffset  - Starting offset to pDatBuf containing the 3D descriptions
* \param [in/out]  pDispMode       - Array that stores all the display mode supported
* \param [in]      uCEABlockIndex  - Index to which CEA extension block we wanted to parse
*
* \retval MDP_Status
*
****************************************************************************/
static void HDMI_EDID_Append3DDispModes(uint8* pDataBuf, uint32 uStartingOffset, HDMI_DispModeListType* pDispModeList, uint32 uCEABlockIndex)
{
  HDMI_DeviceDataType *pSharedData = HDMI_GETCTX();
  uint32      uIndex                = 0;
  uint32      uInitialElementCount  = 0;
  uint32      uWorkingOffset        = 0;
  uint32      uTempVal              = 0;
  uint16      uSupported3DStructure = 0;
  uint32      uRemainingLength      = 0;
  
  /* Starting offset should be pointing to the byte after HDMI_3D_LEN field */
  uWorkingOffset       = uStartingOffset;

  /* Handle 3D present bit - if TRUE need to append the following 3D formats 
     1080p 24Hz frame packing       - HW limitation Not OK
     720p  60Hz frame packing       - OK
     1080i 60Hz side by side (HALF) - OK
     1080p 24Hz Top and Bottom      - OK
     720p  60Hz Top and Bottom      - OK
     720p  50Hz Frame packing       - OK
     1080i 50Hz side by side (HALF) - OK
     720p  50Hz Top and Bottom      - OK */

  /* HDMI spec dictates maximum only the first 16 elements allow to support 3D mode */
  if(pDispModeList->uNumOfElements > 16)
  {
    uInitialElementCount = 16;
  }  
  else
  {
    uInitialElementCount = pDispModeList->uNumOfElements;
  }  

  if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_3D)
  {
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat     = HDMI_VIDEO_3D_FORMAT_FRAME_PACKING;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat     = HDMI_VIDEO_FORMAT_1280x720p60_16_9;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat     = HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF;
    /* If no indication which sub sampling method, default should go to sub sampling odd left odd right */
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DExtendedFormat  = HDMI_3D_EXT_DATA_HORI_SUBSAMPLE_ODD_L_ODD_R;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat     = HDMI_VIDEO_FORMAT_1920x1080i60_16_9;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat     = HDMI_VIDEO_3D_FORMAT_TOP_BOTTOM;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat     = HDMI_VIDEO_FORMAT_1920x1080p24_16_9;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat     = HDMI_VIDEO_3D_FORMAT_TOP_BOTTOM;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat     = HDMI_VIDEO_FORMAT_1280x720p60_16_9;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat     = HDMI_VIDEO_3D_FORMAT_FRAME_PACKING;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat     = HDMI_VIDEO_FORMAT_1280x720p50_16_9;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat     = HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF;
    /* If no indication which sub sampling method, default should go to sub sampling odd left odd right */
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DExtendedFormat  = HDMI_3D_EXT_DATA_HORI_SUBSAMPLE_ODD_L_ODD_R;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat     = HDMI_VIDEO_FORMAT_1920x1080i50_16_9;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat     = HDMI_VIDEO_3D_FORMAT_TOP_BOTTOM;
    pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat     = HDMI_VIDEO_FORMAT_1280x720p50_16_9;
  }

  if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVideo2DDescLen)
  {
    uWorkingOffset += pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVideo2DDescLen;
    uStartingOffset = uWorkingOffset; /* Starting Offset of HDMI_3D_LEN block */
  }
  /* Ensure multi-present field is not zero prior to extracting the multi-present 3D structures */
  if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].u3DMultiPresentFieldVal)
  {
    /* Find out the 3D structures that are supported by the sink device */
    uSupported3DStructure  = pDataBuf[uWorkingOffset++] << 8;
    uSupported3DStructure |= pDataBuf[uWorkingOffset];
  }

  if(HDMI_EDID_VENDOR_BLOCK_3D_STRUCTURE_ALL == pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].u3DMultiPresentFieldVal)
  {
    /* Start appending all the 2D video formats supported by the sink device 
      (First 16 entries only) and add 3D capability as part of the display mode list */
    for(uIndex = 0; uIndex < uInitialElementCount; uIndex++)
    {
      if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_FRAME_PACKING & uSupported3DStructure)
      {
        /* 8660 HW can't support frame packing >= 1080p */
        uTempVal = gSupportedVideoModeLUT[pDispModeList->eDispModeList[uIndex].eVideoFormat].uActiveH;
        if(uTempVal < 1920)
        {
          /* Filter out display modes already included by the mandatory 3D modes */
          if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_3D)
          {
            switch(pDispModeList->eDispModeList[uIndex].eVideoFormat)
            {
              case HDMI_VIDEO_FORMAT_1280x720p60_16_9:
              case HDMI_VIDEO_FORMAT_1280x720p50_16_9:
                break;
              default:
              {
                pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_FRAME_PACKING;
                pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
                break;
              }
            }
          }
          else
          {
            pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_FRAME_PACKING;
            pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
          }
        }
      }
      if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_FIELD_ALTERNATIVE & uSupported3DStructure)
      {
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_FIELD_ALTERNATIVE;
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
      }

      if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_LINE_ALTERNATIVE & uSupported3DStructure)
      {
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_LINE_ALTERNATIVE;
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
      }

      if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_SIDE_BY_SIDE_FULL & uSupported3DStructure)
      {
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_FULL;
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
      }
      if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_L_PLUS_DEPTH & uSupported3DStructure)
      {
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_L_PLUS_DEPTH;
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
      }
      if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_L_PLUS_DEPTH_PLUS_GRAPHIC_DEPTH & uSupported3DStructure)
      {
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_L_PLUS_DEPTH_PLUS_GRAPHIC_DEPTH;
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
      }
      if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_TOP_BOTTOM & uSupported3DStructure)
      {
        /* Filter out display modes already included by the mandatory 3D modes */
        if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_3D)
        {
          switch(pDispModeList->eDispModeList[uIndex].eVideoFormat)
          {
            case HDMI_VIDEO_FORMAT_1920x1080p24_16_9:
            case HDMI_VIDEO_FORMAT_1280x720p60_16_9:
            case HDMI_VIDEO_FORMAT_1280x720p50_16_9:
              break;
            default:
            {
              pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_TOP_BOTTOM;
              pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
              break;
            }
          }
        }
        else
        {
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_TOP_BOTTOM;
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
        }
      }
      if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_SIDE_BY_SIDE_HALF_HORI_SUB_SAMPLE & uSupported3DStructure)
      {
        /* Filter out display modes already included by the mandatory 3D modes */
        if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_3D)
        {
          switch(pDispModeList->eDispModeList[uIndex].eVideoFormat)
          {
            case HDMI_VIDEO_FORMAT_1920x1080i60_16_9:
            case HDMI_VIDEO_FORMAT_1920x1080i50_16_9:
              break;
            default:
            {
              pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DExtendedFormat = HDMI_3D_EXT_DATA_HORI_SUBSAMPLE_ODD_L_ODD_R;
              pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat    = HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF;
              pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat    = pDispModeList->eDispModeList[uIndex].eVideoFormat;
              break;
            }
          }
        }
        else
        {
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DExtendedFormat = HDMI_3D_EXT_DATA_HORI_SUBSAMPLE_ODD_L_ODD_R;
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat    = HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF;
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat    = pDispModeList->eDispModeList[uIndex].eVideoFormat;
        }
      }
      if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_SIDE_BY_SIDE_HALF_QUINCUNX_SUB_SAMPLE & uSupported3DStructure)
      {
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DExtendedFormat = HDMI_3D_EXT_DATA_QUINCUIX_MATRIX_ODD_L_ODD_R;
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat    = HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF;
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat    = pDispModeList->eDispModeList[uIndex].eVideoFormat;
      }
    }
  }
  else if(HDMI_EDID_VENDOR_BLOCK_3D_STRUCTURE_MASK == pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].u3DMultiPresentFieldVal)
  {
    /* Move 1 byte to 3D frame mask field */
    uWorkingOffset++;
    /* Find out the 3D structure mask and see which 2D resolution is capable of doing 3D */
    uTempVal   = pDataBuf[uWorkingOffset++] << 8;
    uTempVal  |= pDataBuf[uWorkingOffset];
    
    for(uIndex = 0; uIndex < uInitialElementCount; uIndex++)
    {
      /* Check the mask bit */
      if((uTempVal >> uIndex) & 0x1)
      {
        if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_FRAME_PACKING & uSupported3DStructure)
        {
          /* 8660 HW can't support frame packing >= 1080p */
          if(gSupportedVideoModeLUT[pDispModeList->eDispModeList[uIndex].eVideoFormat].uActiveH < 1920)
          {
            /* Filter out display modes already included by the mandatory 3D modes */
            if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_3D)
            {
              switch(pDispModeList->eDispModeList[uIndex].eVideoFormat)
              {
                case HDMI_VIDEO_FORMAT_1280x720p60_16_9:
                case HDMI_VIDEO_FORMAT_1280x720p50_16_9:
                  break;
                default:
                {
                  pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_FRAME_PACKING;
                  pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
                  break;
                }
              }
            }
            else
            {
              pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_FRAME_PACKING;
              pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
            }
          }
        }
        if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_FIELD_ALTERNATIVE & uSupported3DStructure)
        {
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_FIELD_ALTERNATIVE;
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
        }

        if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_LINE_ALTERNATIVE & uSupported3DStructure)
        {
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_LINE_ALTERNATIVE;
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
        }

        if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_SIDE_BY_SIDE_FULL & uSupported3DStructure)
        {
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_FULL;
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
        }
        if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_L_PLUS_DEPTH & uSupported3DStructure)
        {
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_L_PLUS_DEPTH;
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
        }
        if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_L_PLUS_DEPTH_PLUS_GRAPHIC_DEPTH & uSupported3DStructure)
        {
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_L_PLUS_DEPTH_PLUS_GRAPHIC_DEPTH;
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
        }
        if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_TOP_BOTTOM & uSupported3DStructure)
        {
          /* Filter out display modes already included by the mandatory 3D modes */
          if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_3D)
          {
            switch(pDispModeList->eDispModeList[uIndex].eVideoFormat)
            {
              case HDMI_VIDEO_FORMAT_1920x1080p24_16_9:
              case HDMI_VIDEO_FORMAT_1280x720p60_16_9:
              case HDMI_VIDEO_FORMAT_1280x720p50_16_9:
                break;
              default:
              {
                pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_TOP_BOTTOM;
                pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
                break;
              }
            }
          }
          else
          {
            pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_TOP_BOTTOM;
            pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat = pDispModeList->eDispModeList[uIndex].eVideoFormat;
          }
        }
        if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_SIDE_BY_SIDE_HALF_HORI_SUB_SAMPLE & uSupported3DStructure)
        {
          /* Filter out display modes already included by the mandatory 3D modes */
          if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_3D)
          {
            switch(pDispModeList->eDispModeList[uIndex].eVideoFormat)
            {
              case HDMI_VIDEO_FORMAT_1920x1080i60_16_9:
              case HDMI_VIDEO_FORMAT_1920x1080i50_16_9:
                break;
              default:
              {
                pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DExtendedFormat = HDMI_3D_EXT_DATA_HORI_SUBSAMPLE_ODD_L_ODD_R;
                pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat    = HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF;
                pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat    = pDispModeList->eDispModeList[uIndex].eVideoFormat;
                break;
              }
            }
          }
          else
          {
            pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DExtendedFormat = HDMI_3D_EXT_DATA_HORI_SUBSAMPLE_ODD_L_ODD_R;
            pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat    = HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF;
            pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat    = pDispModeList->eDispModeList[uIndex].eVideoFormat;
          }
        }
        if(HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_SIDE_BY_SIDE_HALF_QUINCUNX_SUB_SAMPLE & uSupported3DStructure)
        {
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DExtendedFormat = HDMI_3D_EXT_DATA_QUINCUIX_MATRIX_ODD_L_ODD_R;
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DFrameFormat    = HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF;
          pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].eVideoFormat    = pDispModeList->eDispModeList[uIndex].eVideoFormat;
        }
      }
    }
  }
  /* Move to next byte and extract additional 3D_structure if they exist */
  uWorkingOffset++;
  /* Find out any left over 3D formats to add */
  uRemainingLength = uWorkingOffset - uStartingOffset;
  if(uRemainingLength < pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVideo3DDescLen)
  {
    /* Iterate through each remaining 3D descrptors */
    while(uIndex < uRemainingLength)
    {
      /* Only side by side half resolution has extended 3D info */
      uTempVal = ((pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_HDMI_3D_STRUCTURE_X_SHIFT) & HDMI_EDID_VENDOR_BLOCK_HDMI_3D_STRUCTURE_X_MASK);
      if(HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF == uTempVal)
      {
        uTempVal = ((pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_HDMI_2D_VIC_X_SHIFT) & HDMI_EDID_VENDOR_BLOCK_HDMI_2D_VIC_X_MASK);
        /* Zero based for eVideoFormat, hence the minus 1 */
        if(uTempVal)
        {
          uTempVal--;
        }
        
        /* Assign the video format */
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].eVideoFormat = (HDMI_VideoFormatType)uTempVal;

        /* Move to next byte to get the 3D extended info */
        uWorkingOffset++;

        /* Find out the side by side extended info */
        uTempVal = ((pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_HDMI_3D_DETAIL_X_SHIFT) & HDMI_EDID_VENDOR_BLOCK_HDMI_3D_DETAIL_X_MASK);

        /* Add the sub sampling scheme - Add 1 because e3DExtendedFormat is not zero based, HDMI spec is */
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].e3DExtendedFormat = (HDMI_3D_EXT_DataType)(uTempVal + 1);

        /* Assign 3D frame format */
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].e3DFrameFormat = HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF;
        /* Increment by 2 bytes - 1 for the side by side half format and 2 for the extended sub sample info */
        uIndex += 2;
      }
      else
      {
        /* Zero based for eVideoFormat, hence the minus 1 */
        uTempVal = ((pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_HDMI_2D_VIC_X_SHIFT) & HDMI_EDID_VENDOR_BLOCK_HDMI_2D_VIC_X_MASK);
        /* Zero based for eVideoFormat, hence the minus 1 */
        if(uTempVal)
        {
          uTempVal--;
        }  

        /* Assign the video format */
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].eVideoFormat = (HDMI_VideoFormatType)uTempVal;
        uTempVal = (pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_HDMI_3D_STRUCTURE_X_SHIFT) & HDMI_EDID_VENDOR_BLOCK_HDMI_3D_STRUCTURE_X_MASK;

        /* Assign 3D frame format - Add 1 because it is not zero based for e3DFrameFormat whereas HDMI spec is */
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements++].e3DFrameFormat = (HDMI_VideoFormat3D_type)(uTempVal + 1);

        /* Increment by 1 byte */
        uIndex++;
      }
      /* Move to next 3D descriptor */
      uWorkingOffset++;
    }
  }
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_VendorBlockInfo()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_VendorBlockInfo parses the EDID structure and find out
*   the vendor capabilities from vendor data block.
*
* \param [in]      pDataBuf        - Buffer that contains EDID data
* \param [in/out]  pDispMode       - Array that stores all the display mode supported
* \param [in]      uNumOfCEABlocks - Number of CEA extension blocks
*
* \retval MDP_Status
*
****************************************************************************/
static MDP_Status HDMI_EDID_Parser_VendorBlockInfo(uint8* pDataBuf, HDMI_DispModeListType* pDispModeList, uint32 uNumOfCEABlocks)
{
  MDP_Status  eStatus              = MDP_STATUS_OK;
  HDMI_DeviceDataType *pSharedData = HDMI_GETCTX();

  
  if((NULL == pDataBuf) || (NULL == pDispModeList))
  {
    eStatus = MDP_STATUS_FAILED;
  }
  else
  {
    uint32      uWorkingOffset = 0;
    uint32      uDataTagCode   = 0;
    uint32      uCounter       = 0;
    uint32      uNumOfBlocks   = uNumOfCEABlocks;
    uint32      uCEABlockIndex = 0;

    /* Skip Block 2 if block 2 is a BLOCK MAP */
    if(HDMI_EDID_EXTENSION_BLOCK_MAP_TAG_CODE == pDataBuf[HDMI_EDID_CEA_EXTENSION_VERSION_OFFSET - 1])
    {
      if(uNumOfBlocks)
      {
        uNumOfBlocks -= 1;
      }  
    }

    for(uCEABlockIndex = 0; uCEABlockIndex < uNumOfBlocks; uCEABlockIndex++)
    {
      uWorkingOffset = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVendorSpecificBlockOffset;
      if(uWorkingOffset)
      {
        // Extract the vendor specific data tag code 
        uDataTagCode = (pDataBuf[uWorkingOffset] & HDMI_EDID_DATA_BLOCK_TAG_MASK) >> HDMI_EDID_DATA_BLOCK_TAG_SHIFT;

        if(HDMI_EDID_VENDOR_SPECIFIC_DATA_BLOCK_TAG_CODE == uDataTagCode)
        {
          /* Find out the number of bytes within the vendor data block - Minus one for the extended tag code byte */
          //uNumOfBytes = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uDataBlockLength;
          
          // Move 1 byte to access the IEEE registration ID 
          uWorkingOffset++;

          /* Extract IEEE ID */
          pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uIEEERegistrationID = pDataBuf[uWorkingOffset];
          uWorkingOffset++;
          pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uIEEERegistrationID |= pDataBuf[uWorkingOffset] << 8;
          uWorkingOffset++;
          pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uIEEERegistrationID |= pDataBuf[uWorkingOffset] << 16;

          // Move 1 byte to access the 16 bit physical address of the sink device 
          uWorkingOffset++;

          pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uPhysicalAddress = (pDataBuf[uWorkingOffset+1] << 8)| pDataBuf[uWorkingOffset];
        }
        else
          eStatus = MDP_STATUS_FAILED;

        if(MDP_STATUS_OK == eStatus)
        {
          // Check whether extension fields exists 
          if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVendorSpecificBlockExtOffset)
          {
            uWorkingOffset = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVendorSpecificBlockExtOffset;
            while((uWorkingOffset - pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVendorSpecificBlockOffset) < pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uDataBlockLength)
            {
              switch(uCounter)
              {
                case 0:
                {
                  pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uSupportedDeepColorModeMask = (pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_DEEP_COLOR_SHIFT ) & HDMI_EDID_VENDOR_BLOCK_DEEP_COLOR_MASK;
                  pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].bAISupport                  = (pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_AI_SUPPORT_SHIFT ) & HDMI_EDID_VENDOR_BLOCK_AI_SUPPORT_MASK;
                  pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].bDVIDualSupport             = (pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_DVI_DUAL_SUPPORT_SHIFT ) & HDMI_EDID_VENDOR_BLOCK_DVI_DUAL_SUPPORT_MASK;
                  break;
                }
                case 1:
                {
                  pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uMaxTMDSClock = pDataBuf[uWorkingOffset] * 5; /* HDMI Spec requires this clock rate to multiply by 5MHZ */
                  break;
                }
                case 2:
                {
                  if((pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_LATENCY_PRESENT_SHIFT ) & HDMI_EDID_VENDOR_BLOCK_LATENCY_PRESENT_MASK)
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags |=  HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_LATENCY;

                  if((pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_I_LATENCY_PRESENT_SHIFT ) & HDMI_EDID_VENDOR_BLOCK_I_LATENCY_PRESENT_MASK)
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags |=  HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_I_LATENCY;

                  if((pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_VIDEO_PRESENT_SHIFT ) & HDMI_EDID_VENDOR_BLOCK_VIDEO_PRESENT_MASK)
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags |=  HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_HDMI_VIDEO;

                  // Parse video content Type 
                  pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uSupportedContentFilterMask = ((pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_CONTENT_TYPE_SHIFT ) & HDMI_EDID_VENDOR_BLOCK_CONTENT_TYPE_MASK);

                  break;
                }
                case 3:
                {
                  // Check for Progressive latency present bit 
                  if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_LATENCY)
                  {
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVideoLatency           = pDataBuf[uWorkingOffset];
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uInterlacedVideoLatency = pDataBuf[uWorkingOffset];
                  }
                  else
                  {
                    //  Check 3D present bit 
                    if((pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_3D_PRESENT_SHIFT ) & HDMI_EDID_VENDOR_BLOCK_3D_PRESENT_MASK)
                    {
                      pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags |=  HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_3D;
                    }
                    
                    //  Parse Image size 
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].eImageSizeInfo = 
                      (HDMI_EDID_VendorBlockImageSizeInfoType)((pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_IMAGE_INFO_SHIFT ) & HDMI_EDID_VENDOR_BLOCK_IMAGE_INFO_MASK);

                    //  Parse Multi present field value 
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].u3DMultiPresentFieldVal = 
                      ((pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_3D_MULTI_PRESENT_SHIFT ) & HDMI_EDID_VENDOR_BLOCK_3D_MULTI_PRESENT_MASK);
                    
                  }
                  break;
                }
                case 4:
                {
                  // Check for Progressive latency present bit 
                  if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_LATENCY)
                  {
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uAudioLatency           = pDataBuf[uWorkingOffset];
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uInterlacedAudioLatency = pDataBuf[uWorkingOffset];
                  }
                  else
                  {
                    // Extract the HDMI_VIC_LEN and the HDMI 3D LEN fields 
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVideo2DDescLen = 
                      (pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_HDMI_VIC_LEN_SHIFT) & HDMI_EDID_VENDOR_BLOCK_HDMI_VIC_LEN_MASK;

                    // Extract the HDMI_VIC_LEN and the HDMI 3D LEN fields 
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVideo3DDescLen = 
                      (pDataBuf[uWorkingOffset] >> HDMI_EDID_VENDOR_BLOCK_HDMI_3D_LEN_SHIFT) & HDMI_EDID_VENDOR_BLOCK_HDMI_3D_LEN_MASK;
                  }
                  break;
                }
                case 5:
                {
                  // Check for Interlace latency present bit 
                  if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_I_LATENCY)
                  {
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uInterlacedVideoLatency = pDataBuf[uWorkingOffset];
                  }
                  else
                  {
                    if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVideo2DDescLen)
                    {
                      // Do nothing as these extended VIC code cannot be supported by 8660 HDMI core 
                    }
                    if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVideo3DDescLen ||
                       pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].u3DMultiPresentFieldVal ||
                      (pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_3D))
                    {
                      // Append any 3D formats as the supported display modes 
                      (void) HDMI_EDID_Append3DDispModes(pDataBuf, uWorkingOffset, pDispModeList, uCEABlockIndex);
                    }
                  }
                  break;
                }
                case 6:
                {
                  /* Check for Interlace latency present bit */
                  if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].ePresetBitFlags & HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_I_LATENCY)
                  {
                    pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uInterlacedAudioLatency = pDataBuf[uWorkingOffset];
                  }
                  // All subsequent 3D information will be handled through HDMI_EDID_Append3DDispModes()
                  // At this point we should exit the loop as all vendor specific information is parsed 
                  uWorkingOffset = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uDataBlockLength;
                  break;
                }
              }
              uWorkingOffset++;
              uCounter++;
            }
          }
        }
      }
    }
  }

  return eStatus;
}


/****************************************************************************
*
** FUNCTION: HDMI_EDID_InsertDisplayMode()
*/
/*!
* \brief
*   The \b HDMI_EDID_InsertDisplayMode populates the global display mode list
*
* \param [in] pDataBuf             - Cache buffer that stores the RAW EDID info of short video descriptor
* \param [in] uNumOfDispMode - Total number of modes to iterate
* \param [in] pDispModeList  - Stores all the detected and supported display modes
* \param [in] uBufferIndex   - Working Offset of the cache buffer
* \param [in] eVideoFormat       - Video format in case there is no need to pase the Raw edid
* \param [in] baudioSupported  -  Whether audio supported by the mode
* \retval void
*
****************************************************************************/
static void HDMI_EDID_InsertDisplayMode(uint8* pDataBuf, HDMI_DispModeListType* pDispModeList, uint32 uNumOfDispMode, uint32 uBufferIndex, HDMI_VideoFormatType eVideoFormat, bool32 bAudioSupported)
{
  uint32                uI             = 0;
  uint32                uWorkingOffset = uBufferIndex;

  if(pDataBuf)
  {
    /* Move to the first short video descriptor */
  uWorkingOffset++;
  for(uI = 0; uI < uNumOfDispMode; uI++)
  {
    //Subtract 1 because it is zero based in the driver, while the Video identification code is 1 based in the CEA_861D spec
    eVideoFormat = (HDMI_VideoFormatType)((pDataBuf[uWorkingOffset + uI] & HDMI_EDID_SHORT_VIDEO_DESC_VID_MASK) - 1); 
      HDMI_EDID_Parser_InsertDispFormat(pDispModeList, eVideoFormat,bAudioSupported);
    }
  }
  else
  {
    /* Insert the video format to the mode list.*/
    if(0x01 == uNumOfDispMode)
    {
      HDMI_EDID_Parser_InsertDispFormat(pDispModeList, eVideoFormat,bAudioSupported);    
    }
  }
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_DisplayDescriptor()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_DisplayDescriptor parses the display descriptor block of the EDID
*
* \param [in]      pDataBuf        - Buffer that contains EDID data
* \param [in]      uDescOffset     - Starting offset of display descriptor
* \param [in]      pDispModeList   - Mode list

*
* \retval void
*
****************************************************************************/

static void HDMI_EDID_Parser_DisplayDescriptor(uint8* pDataBuf, uint32 uDescOffset, HDMI_DispModeListType* pDispModeList)
{

  /* Additional standard timing*/
  if(HDMI_EDID_DISPLAY_DESC_STANDARD_TIMING_TAG == pDataBuf[uDescOffset + HDMI_EDID_DISPLAY_DESC_TAG_OFFSET])
  {
    uint32 uStandardTimingStartOffset = uDescOffset + HDMI_EDID_DISPLAY_DESC_STANDARD_TIMING_START_OFFSET;
    uint32 uStandardTimingEndOffset = uStandardTimingStartOffset + 2 * HDMI_EDID_DISPLAY_DESC_STANDARD_TIMING_DEFINITION_COUNT  - 1;
    
    HDMI_EDID_Parser_StandardTimings(pDataBuf, uStandardTimingStartOffset, uStandardTimingEndOffset, pDispModeList); 
  }

  /*Establish timing III */
  if(HDMI_EDID_DISPLAY_DESC_ESTABLISH_TIMINGIII_TAG == pDataBuf[uDescOffset + HDMI_EDID_DISPLAY_DESC_TAG_OFFSET])
  {
    HDMI_EDID_Parser_EstablishTimingIII(pDataBuf, uDescOffset + HDMI_EDID_DISPLAY_DESC_ESTABLISH_TIMINGIII_START_OFFSET, pDispModeList); 
  }
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_ReadEDID()
*/
/*!
* \brief
*   The \b HDMI_EDID_ReadEDID  Reads EDID 
*
* \param [in] pSharedData             - Cache buffer that stores the RAW EDID info of short video descriptor
* \param [out] puNumOfCEABlocks - Total number of modes to iterate
* \retval MDP_Status
*
****************************************************************************/

static MDP_Status HDMI_EDID_ReadEDID(HDMI_DeviceDataType* pSharedData,
                                            uint32*              puNumOfCEABlocks)
{
  MDP_Status           eStatus          = MDP_STATUS_OK;
  uint8                uOffset          = 0;
  uint32               uNumOfCEABlocks  = 0;
  uint32               uSegmentNum      = 0;
  
  // Initialize DDC 
  (void) HAL_HDMI_DDC_Init();

  //Read Block 0
  if (HAL_HDMI_SUCCESS != HAL_HDMI_DDC_Read(HDMI_EDID_BLOCK_SEGMENT_ADDR, 
                                                         uSegmentNum,
                                                         HDMI_EDID_BLOCK_ZERO_DEVICE_ADDR, 
                                                         uOffset, 
                                                         pSharedData->auEDIDCache, 
                                                         HDMI_EDID_BLOCK_SIZE, 
                                                         HDMI_EDID_READ_TIMEOUT))
  {
    eStatus = MDP_STATUS_FAILED;
  }
  //EDID version must be 1 otherwise failed to continue parsing
  else if (1 != pSharedData->auEDIDCache[HDMI_EDID_VERSION_OFFSET])
  {
    eStatus = MDP_STATUS_NOT_SUPPORTED;
  }
  else
  {
    uNumOfCEABlocks  = HDMI_EDID_GetNumOfCEAExtBlocks();
  
    //Find out any CEA extension blocks following block 0
    switch(uNumOfCEABlocks)
    {
      case 0: //No CEA extension
        pSharedData->uEDIDDataLength = HDMI_EDID_BLOCK_SIZE;
      break;
      case 1: //Block 1 is read below
      default:
      {
        uOffset                       = HDMI_EDID_BLOCK_SIZE;
        pSharedData->uEDIDDataLength  = HDMI_EDID_BLOCK_SIZE;
  
        //Read block 1
        if (HAL_HDMI_SUCCESS != HAL_HDMI_DDC_Read(HDMI_EDID_BLOCK_SEGMENT_ADDR, 
                                                                uSegmentNum,
                                                                HDMI_EDID_BLOCK_ZERO_DEVICE_ADDR, 
                                                                uOffset, 
                                                                &pSharedData->auEDIDCache[pSharedData->uEDIDDataLength], 
                                                                HDMI_EDID_BLOCK_SIZE, 
                                                                HDMI_EDID_READ_TIMEOUT))
        {
          eStatus = MDP_STATUS_FAILED;
        }
        else
        {
          uint32 uIndex = 0;

          /* Increment the RAW EDID size after each block read */
          pSharedData->uEDIDDataLength += HDMI_EDID_BLOCK_SIZE;
            
          /* Read additional EDID blocks require new segment number for every two blocks */
          for (uIndex = 0; uIndex < uNumOfCEABlocks - 1; uIndex++ )
          {
            /* Every two blocks it is a new segment page number */
            if (uIndex % 2 == 0)
            {
              uOffset = 0;
              uSegmentNum++;
            }
            else
            {
              uOffset = HDMI_EDID_BLOCK_SIZE;
            }
            
            // Read Additional blocks
            if (HAL_HDMI_SUCCESS != HAL_HDMI_DDC_Read(HDMI_EDID_BLOCK_SEGMENT_ADDR, 
                                                                   uSegmentNum, 
                                                                   HDMI_EDID_BLOCK_ZERO_DEVICE_ADDR, 
                                                                   uOffset, 
                                                                   &pSharedData->auEDIDCache[pSharedData->uEDIDDataLength],
                                                                   HDMI_EDID_BLOCK_SIZE, 
                                                                   HDMI_EDID_READ_TIMEOUT))
            {
              eStatus = MDP_STATUS_FAILED;
            }
            else
            {
              /* Increment the RAW EDID size after each block read */
              pSharedData->uEDIDDataLength += HDMI_EDID_BLOCK_SIZE;
            }
          }
        }
        break;
      }
    }

    // Return count of CEA blocks read
    *puNumOfCEABlocks = uNumOfCEABlocks;
  }

  // Reset DDC
  HAL_HDMI_DDC_Reset();

  return eStatus;
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_ReadStaticEDID()
*/
/*!
* \brief
*   The \b HDMI_EDID_ReadStaticEDID  Reads EDID stored in the device memory
*
* \param [in] pSharedData             - Cache buffer that stores the RAW EDID info of short video descriptor
* \param [out] puNumOfCEABlocks - Total number of modes to iterate
* \retval MDP_Status
*
****************************************************************************/

static MDP_Status HDMI_EDID_LoadStaticEDID(HDMI_DeviceDataType* pSharedData,
                                            uint32*              puNumOfCEABlocks)
{
  bool32        bForce1080pMode   = FALSE;
  UINT32        uConfig; 
  
  // Read the config item to find about the preference of mode
  // If the config item does not exist, default mode is HDMI_VIDEO_FORMAT_1280x720p60_16_9 mode 
  if (EFI_SUCCESS == GetConfigValue("DisplayForceHDMI1080pMode", &uConfig))
  {
    bForce1080pMode = (0 == uConfig)?FALSE:TRUE;
  }

  // Read Stored EDID into EDID Cache
  MDP_OSAL_MEMCPY(&pSharedData->auEDIDCache,&asDefaultEDIDData,HDMI_EDID_BLOCK_SIZE);

  // Check if we want to force the monitor to 1080p mode
  if (TRUE == bForce1080pMode)
  {
    // Stored edid supports HDMI_VIDEO_FORMAT_640x480p60_4_3 and HDMI_VIDEO_FORMAT_1280x720p60_16_9 modes
    // To force HDMI_VIDEO_FORMAT_1920x1080p60_16_9, we would over ride the preferred timing mode with the 
    // Detailed Timing descriptor for 1080p mode

    MDP_OSAL_MEMCPY(&pSharedData->auEDIDCache[HDMI_EDID_FIRST_TIMING_DESC_OFFSET],
                    &as1080pDetailedTiming,
                    HDMI_EDID_DETAIL_TIMING_DESC_BLOCK_SIZE);
  }

  *puNumOfCEABlocks = HDMI_EDID_GetNumOfCEAExtBlocks();

  return MDP_STATUS_OK;
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_GetNumOfCEAExtBlocks()
*/
/*!
* \brief
*   The \b HDMI_EDID_GetNumOfCEAExtBlocks parses the edid to find the number of CEA extension blocks
*
* \param [in]  void
*
* \retval  uint32
*
****************************************************************************/
static uint32 HDMI_EDID_GetNumOfCEAExtBlocks(void)
{
  HDMI_DeviceDataType* pSharedData         = HDMI_GETCTX();
  uint32               uNumOfCEABlocks     = 0;
  
  uNumOfCEABlocks  = pSharedData->auEDIDCache[HDMI_EDID_CEA_EXTENSION_FLAG_OFFSET];

  // Make sure that the number of CEA blocks number is not larger than maximum number of CEA blocks.
  if ( uNumOfCEABlocks > HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS)
  {
    DEBUG ((EFI_D_WARN, "Number of CEA extension blocks is %d, greater than HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS", uNumOfCEABlocks));
    
    uNumOfCEABlocks = HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS;
  }

  return uNumOfCEABlocks;
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_IsDispModeSupportAudio
*/
/*!
* \brief
*   The \b HDMI_EDID_IsDispModeSupportAudio - It check whether the current active display mode supports audio.
* 
*
* \param [in]  none
*
* \retval  TRUE/FALSE
*
****************************************************************************/
static bool32 HDMI_EDID_IsDispModeSupportAudio(void)
{
  bool32                  bResult                 = FALSE;
  HDMI_DeviceDataType     *pSharedData            = HDMI_GETCTX();
  uint32 uModeIndex                               = 0;

  /* Get the current acitvie mode index */
  uModeIndex = pSharedData->uActiveModeIndex;
    
  if(HDMI_VIDEO_FORMAT_MAX > uModeIndex)
  {
    bResult = !pSharedData->sDispModeList.eDispModeList[uModeIndex].bVideoOnly;
  }

  return bResult;
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_InsertDispFormat
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_InsertDispFormat - If a video modes is not present in the mode list, inserts it to the mode list, else update the audio support information
* 
*
* \param [in] pDispModeList, pDispModeList, bAudioSupported
*
* \retval  void
*
****************************************************************************/
static void HDMI_EDID_Parser_InsertDispFormat(HDMI_DispModeListType* pDispModeList, HDMI_VideoFormatType eVideoFormat, bool32 bAudioSupported)
{
  if(pDispModeList)
  {
    if((HAL_HDMI_VIDEO_FORMAT_MAX > eVideoFormat) && (gSupportedVideoModeLUT[eVideoFormat].bSupported))
    {    
      uint32 uNumOfModes = pDispModeList->uNumOfElements;
      uint32 uIndex = 0;
    
      //Search for duplicate entry in the mode list */
      for(uIndex = 0; (uIndex < uNumOfModes) && (pDispModeList->eDispModeList[uIndex].eVideoFormat != eVideoFormat); uIndex++);

      if(uIndex == uNumOfModes)
      {
        //There is no duplicate entry , add to the mode list */
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].bVideoOnly = !bAudioSupported;                            
        pDispModeList->eDispModeList[pDispModeList->uNumOfElements].eVideoFormat = eVideoFormat;
        pDispModeList->uNumOfElements++;
      }
      else
      {
        //The request to add a diplay format already added mode received. update the bVideoOnly only if the current reqeust to enable audio support. 
        if((TRUE == pDispModeList->eDispModeList[uIndex].bVideoOnly) && (TRUE == bAudioSupported))
        {
          pDispModeList->eDispModeList[uIndex].bVideoOnly = FALSE;
        }       
      }
    }    
  }
}


/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_DetailTiming()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_Detail_Desc parses the EDID structure and find out
*   all the video mode supported using detailed timing descriptors of the first block i.e EDID block
*
* \param [in]      pDataBuf        - Buffer that contains EDID data
* \param [out]     pDispMode       - Matching display mode 
*
* \retval MDP_Status
*
****************************************************************************/

static MDP_Status HDMI_EDID_Parser_DetailTiming(uint8* pDataBuf, HDMI_DispModeListType* pDispModeList)
{
  MDP_Status            eStatus            = MDP_STATUS_OK;
  HDMI_VideoFormatType  eVideoFormat       = HDMI_VIDEO_FORMAT_640x480p60_4_3;
  uint32                uDescOffset        = 0;
  uint32                uI                 = 0;
  
  
 /* Maximum 4 timing descriptor in block 0 */
 for(uI = 0; uI < HDMI_EDID_TIMING_BLOCK_0_MAX_DESC; uI++)
 {
   uDescOffset = HDMI_EDID_FIRST_TIMING_DESC_OFFSET + (uI * HDMI_EDID_DETAIL_TIMING_DESC_BLOCK_SIZE);
   
   if(pDataBuf[uDescOffset])
   {
     eStatus = HDMI_EDID_Parser_Detail_Desc(&pDataBuf[uDescOffset], &eVideoFormat);
     
     if(MDP_STATUS_OK == eStatus)
     {
       /* The resolution read from the EDID block won't support audio , hence no audio support flag is set*/ 
       HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, eVideoFormat, FALSE); 
     }
   }
   else
   {
     /* The first 3 bytes should be 0 if it is used as display descriptor */
     if((!pDataBuf[uDescOffset]) && (!pDataBuf[uDescOffset + 1]) && (!pDataBuf[uDescOffset + 2]))
     {
       HDMI_EDID_Parser_DisplayDescriptor(pDataBuf, uDescOffset, pDispModeList);
     }
   }
  }
  return eStatus;
}


/* -----------------------------------------------------------------------
** Public Functions
** ----------------------------------------------------------------------- */

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_Open()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_Open initializes and caches
*   the EDID structure extracted from the sink device.
**
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_Open(void)
{
  MDP_Status           eStatus          = MDP_STATUS_OK;
  HDMI_DeviceDataType* pSharedData      = HDMI_GETCTX();
  uint32               uRetry           = 0;
  uint32               uNumOfCEABlocks  = 0;
  bool32               bShortDesc       = FALSE;

  // Turn on HDMI engine if not already on, keep it powered up
  if(HAL_HDMI_POWER_ON != HAL_HDMI_PowerModeGet())
  {
    (void) HAL_HDMI_PowerModeSet(HAL_HDMI_POWER_ON);
  }

  do 
  {
    // Initialize EDID Cache
    MDP_OSAL_MEMZERO(pSharedData->auEDIDCache, (sizeof(uint8) * HDMI_EDID_MAX_BUF_SIZE));
    pSharedData->uEDIDDataLength  = 0;
  
    // Initialize Supported Modes list
     MDP_OSAL_MEMZERO(&pSharedData->sDispModeList, sizeof(HDMI_DispModeListType));

    // Read EDID
    if (MDP_STATUS_OK ==(eStatus = HDMI_EDID_ReadEDID(pSharedData,&uNumOfCEABlocks)))
    {
      /* Initialize the data block offsets */
      (void) HDMI_EDID_DataBlockOffsetSetup(pSharedData->auEDIDCache, uNumOfCEABlocks);
      
      /* Parse all the supported 2D display modes */
      if (MDP_STATUS_OK  != (eStatus = HDMI_EDID_Parser_GetDispMode(pSharedData->auEDIDCache, &pSharedData->sDispModeList, uNumOfCEABlocks, bShortDesc)))
      {
        // Do nothing
      }
      /* Parse all the parameters inside a vendor specific data block and extract all the supported 3D display modes*/
      else if (MDP_STATUS_OK != (eStatus = HDMI_EDID_Parser_VendorBlockInfo(pSharedData->auEDIDCache, &pSharedData->sDispModeList, uNumOfCEABlocks)))
      {
        // Do nothing
      }
      else
      {
        pSharedData->bEDIDParserComplete = TRUE;
      }
    }
    uRetry++;
  } while ((uRetry < HDMI_HDCP_EDID_READ_MAX_RETRIES) && (!pSharedData->bEDIDParserComplete));

  // Load the Stored EDID to allow HDMI mode initialization (either 1080p or 720p mode)
  if ((uRetry == HDMI_HDCP_EDID_READ_MAX_RETRIES) && (FALSE == pSharedData->bEDIDParserComplete))
  {
    DEBUG(( EFI_D_INFO, "Driver failed to read EDID %d times, Falling back to stored EDID\n ", uRetry));

    // Read Stored EDID
    if (MDP_STATUS_OK ==(eStatus = HDMI_EDID_LoadStaticEDID(pSharedData,&uNumOfCEABlocks)))
    {
      /* Initialize the data block offsets */
      (void) HDMI_EDID_DataBlockOffsetSetup(pSharedData->auEDIDCache, uNumOfCEABlocks);
      
      /* Parse all the supported 2D display modes */
      if (MDP_STATUS_OK  != (eStatus = HDMI_EDID_Parser_GetDispMode(pSharedData->auEDIDCache, &pSharedData->sDispModeList, uNumOfCEABlocks, bShortDesc)))
      {
        // Do nothing
      }
      /* Parse all the parameters inside a vendor specific data block and extract all the supported 3D display modes*/
      else if (MDP_STATUS_OK != (eStatus = HDMI_EDID_Parser_VendorBlockInfo(pSharedData->auEDIDCache, &pSharedData->sDispModeList, uNumOfCEABlocks)))
      {
        // Do nothing
      }
      else
      {
        pSharedData->bEDIDParserComplete = TRUE;
      }
    }
    else
    {
      // We should never reach here, log the error and return
      DEBUG(( EFI_D_ERROR, "Driver failed to read stored EDID\n."));
    }
  }
    
  return eStatus;
}
/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_Close()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_Close resets the DDC engine and close the DDC 
*   gpio lines. Reset all global variables back to zero.
**
* \retval void
*
****************************************************************************/
void HDMI_EDID_Parser_Close(void)
{

}


/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetAudioModeInfo()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetAudioModeInfo parses the EDID structure and find out
*   the audio capabilities from each audio short descriptors.
*
* \param [in] pAudioModeInfo - Pointer to a structure containing the properties of a short audio descriptor.
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetAudioModeInfo(HDMI_AudioModeInfoType* pAudioModeInfo)
{
  MDP_Status            eStatus          = MDP_STATUS_OK;
  HDMI_DeviceDataType*  pSharedData      = HDMI_GETCTX();

  uint8*                pDataBuf         = pSharedData->auEDIDCache;  
  uint32                uDataTagCode     = 0;
  uint32                uNumOfBytes      = 0;
  uint32                uWorkingOffset   = 0;
  uint32                uI               = 0;
  bool32                bAudioSupport    = 0;
  uint32                uNumOfCEABlocks  = 0;
  uint32                uCEABlockIndex   = 0;
  uint32                uStartingOffset  = 0;
  bool32                uDispModeSupportAudio = FALSE;


  uDispModeSupportAudio = HDMI_EDID_IsDispModeSupportAudio();

  /* Send audio mode info only when the the current display mode supports audio*/
  if (uDispModeSupportAudio)
  {
    uNumOfCEABlocks  = HDMI_EDID_GetNumOfCEAExtBlocks();

    /* Skip Block 2 if block 2 is a BLOCK MAP */
    if(HDMI_EDID_EXTENSION_BLOCK_MAP_TAG_CODE == pDataBuf[HDMI_EDID_CEA_EXTENSION_VERSION_OFFSET - 1])
    {
      uStartingOffset += HDMI_EDID_BLOCK_SIZE;
      if(uNumOfCEABlocks)
        uNumOfCEABlocks -= 1;
    }

    uStartingOffset += HDMI_EDID_BLOCK_SIZE;

    for(uCEABlockIndex = 0; uCEABlockIndex < uNumOfCEABlocks; uCEABlockIndex++)
    {
      /* Whether any audio descriptors exists */
      if(pSharedData->sDataBlockDesc.sAudioDataBlockDesc[uCEABlockIndex].uAudioDataBlockOffset || pSharedData->sDataBlockDesc.sExtAudioDataBlockDesc[uCEABlockIndex].uAudioDataBlockOffset)        
      {
        if(pAudioModeInfo)
        {
          bAudioSupport = (pDataBuf[uStartingOffset + HDMI_EDID_CEA_EXTENSION_CABS_RELATIVE_OFFSET] & HDMI_EDID_BASIC_AUDIO_SUPPORT_MASK) >> HDMI_EDID_BASIC_AUDIO_SUPPORT_SHIFT;

          /*If vesa resolution is selected then disable audio, no need to send audio mode information*/
          
          if(bAudioSupport)
          {
            /* In the event that video/audio data block doesn't exists - basic audio capabilities should still be returned */
            pAudioModeInfo->eAudioFormat                             = HDMI_AUDIO_FORMAT_LPCM ;
            pAudioModeInfo->uAudioChannelMask                        = HDMI_AUDIO_CHANNEL_2;
            pAudioModeInfo->uAudioSampleRateMask                     = HDMI_AUDIO_SAMPLE_RATE_32KHZ;
            pAudioModeInfo->audioStreamInfo.uAudioSampleBitDepthMask = HDMI_AUDIO_BIT_DEPTH_16_BIT;
            pAudioModeInfo->uAudioSpeakerLocationMask                = (HDMI_AUDIO_SPEAKER_LOCATION_FL | HDMI_AUDIO_SPEAKER_LOCATION_FR);

            if(pSharedData->sDataBlockDesc.sAudioDataBlockDesc[uCEABlockIndex].uAudioDataBlockOffset)
            {
              uWorkingOffset = pSharedData->sDataBlockDesc.sAudioDataBlockDesc[uCEABlockIndex].uAudioDataBlockOffset;

              /* Extract the video data tag code */
              uDataTagCode = (pDataBuf[uWorkingOffset] & HDMI_EDID_DATA_BLOCK_TAG_MASK) >> HDMI_EDID_DATA_BLOCK_TAG_SHIFT;

              /* Check whether the audio tag is correct */
              if(HDMI_EDID_AUDIO_DATA_BLOCK_TAG_CODE == uDataTagCode)
              {
                /* Find out the amount of descriptors within the audio data block */
                uNumOfBytes = pSharedData->sDataBlockDesc.sAudioDataBlockDesc[uCEABlockIndex].uDataBlockLength;
              }
              else
              {
                eStatus = MDP_STATUS_FAILED;
              }  
            }      
            
            /* TODO: what happens for extended audio modes */
            if(pSharedData->sDataBlockDesc.sExtAudioDataBlockDesc[uCEABlockIndex].uAudioDataBlockOffset)
            {
              uWorkingOffset = pSharedData->sDataBlockDesc.sAudioDataBlockDesc[uCEABlockIndex].uAudioDataBlockOffset + 1;

              /* Extract the audio data tag code */
              uDataTagCode = (pDataBuf[uWorkingOffset] & HDMI_EDID_DATA_BLOCK_TAG_MASK) >> HDMI_EDID_DATA_BLOCK_TAG_SHIFT;

              if(HDMI_EDID_EXTENDED_VENDOR_SPECIFIC_AUDIO_TAG_CODE == uDataTagCode)
              {
                /* Find out the amount of descriptors within the audio data block - Minus one for the extended tag code byte */
                uNumOfBytes = pSharedData->sDataBlockDesc.sAudioDataBlockDesc[uCEABlockIndex].uDataBlockLength - 1;
              }
              else
              {
                eStatus = MDP_STATUS_FAILED;
              }  
            }

            if(MDP_STATUS_OK == eStatus)
            {
              /* Move to next byte - 1st byte of the 1st audio descriptor */
              uWorkingOffset++;

              /* Move to the corresponding audio data descriptor */
              uWorkingOffset += pAudioModeInfo->uAudioModeIndex * HDMI_EDID_AUDIO_SHORT_DESC_SIZE;

              /* Ensure the modeIndex is within the audio data block size */
              if((pAudioModeInfo->uAudioModeIndex * HDMI_EDID_AUDIO_SHORT_DESC_SIZE) < uNumOfBytes)
              {
                pAudioModeInfo->eAudioFormat       = (HDMI_AudioFormatType)((pDataBuf[uWorkingOffset] & HDMI_EDID_AUDIO_DESC_AUDIO_FORMAT_MASK) >> HDMI_EDID_AUDIO_DESC_AUDIO_FORMAT_SHIFT);
                pAudioModeInfo->uAudioChannelMask  = (pDataBuf[uWorkingOffset] & HDMI_EDID_AUDIO_DESC_AUDIO_MAX_CHANNEL_MASK) + 1;
                switch(pAudioModeInfo->uAudioChannelMask)
                {
                  case 2:
                    pAudioModeInfo->uAudioChannelMask = (HDMI_AUDIO_CHANNEL_2);
                    break;
                  case 4:
                    pAudioModeInfo->uAudioChannelMask = (HDMI_AUDIO_CHANNEL_4 | HDMI_AUDIO_CHANNEL_2);
                    break;
                  case 6:
                    pAudioModeInfo->uAudioChannelMask = (HDMI_AUDIO_CHANNEL_6 | HDMI_AUDIO_CHANNEL_4 | HDMI_AUDIO_CHANNEL_2);
                    break;
                  case 8:
                    pAudioModeInfo->uAudioChannelMask = (HDMI_AUDIO_CHANNEL_8 | HDMI_AUDIO_CHANNEL_6 | HDMI_AUDIO_CHANNEL_4 | HDMI_AUDIO_CHANNEL_2);
                    break;
                  default:
                    pAudioModeInfo->uAudioChannelMask = (HDMI_AUDIO_CHANNEL_NONE);
                    break;
                }
                pAudioModeInfo->uAudioSampleRateMask      = 0;
                /* Move to byte 2 of audio short descriptor */
                uWorkingOffset++; 
                for(uI = 0; uI < 7; uI++)
                {
                  if((pDataBuf[uWorkingOffset] >> uI) & 0x1)
                  {
                    switch(uI)
                    {
                      case 0:
                        pAudioModeInfo->uAudioSampleRateMask |= HDMI_AUDIO_SAMPLE_RATE_32KHZ;
                        break;
                      case 1:
                        pAudioModeInfo->uAudioSampleRateMask |= HDMI_AUDIO_SAMPLE_RATE_44_1KHZ;
                        break;
                      case 2:
                        pAudioModeInfo->uAudioSampleRateMask |= HDMI_AUDIO_SAMPLE_RATE_48KHZ;
                        break;
                      case 3:
                        pAudioModeInfo->uAudioSampleRateMask |= HDMI_AUDIO_SAMPLE_RATE_88_2KHZ;
                        break;
                      case 4:
                        pAudioModeInfo->uAudioSampleRateMask |= HDMI_AUDIO_SAMPLE_RATE_96KHZ;
                        break;
                      case 5:
                        pAudioModeInfo->uAudioSampleRateMask |= HDMI_AUDIO_SAMPLE_RATE_176_4KHZ;
                        break;
                      case 6:
                        pAudioModeInfo->uAudioSampleRateMask |= HDMI_AUDIO_SAMPLE_RATE_192KHZ;
                        break;
                      default:
                        pAudioModeInfo->uAudioSampleRateMask |= HDMI_AUDIO_SAMPLE_RATE_32KHZ;
                        break;
                    }
                  }
                }
                pAudioModeInfo->audioStreamInfo.uAudioSampleBitDepthMask = 0;
                /* Move to byte 3 of audio short descriptor */
                uWorkingOffset++; 
                if(HDMI_AUDIO_FORMAT_LPCM == pAudioModeInfo->eAudioFormat)
                {
                  for(uI = 0; uI < 3; uI++)
                  {
                    if((pDataBuf[uWorkingOffset] >> uI) & 0x1)
                    {
                      switch(uI)
                      {
                        case 0:
                          pAudioModeInfo->audioStreamInfo.uAudioSampleBitDepthMask |= HDMI_AUDIO_BIT_DEPTH_16_BIT;
                          break;
                        case 1:
                          pAudioModeInfo->audioStreamInfo.uAudioSampleBitDepthMask |= HDMI_AUDIO_BIT_DEPTH_20_BIT;
                          break;
                        case 2:
                          pAudioModeInfo->audioStreamInfo.uAudioSampleBitDepthMask |= HDMI_AUDIO_BIT_DEPTH_24_BIT;
                          break;
                        default:
                          pAudioModeInfo->audioStreamInfo.uAudioSampleBitDepthMask |= HDMI_AUDIO_BIT_DEPTH_16_BIT;
                          break;
                      }
                    }
                  }
                }
                else
                {
                  pAudioModeInfo->audioStreamInfo.uAudioMaxBitRate = pDataBuf[uWorkingOffset] * HDMI_EDID_AUDIO_BIT_RATE_8KBPS;  /* Need to multiply 8Kbps to get maximum bit rate */
                }
              }
              else
              {
                eStatus = MDP_STATUS_FAILED;
              }
            }

            if(eStatus == MDP_STATUS_OK)
            {
              if(pSharedData->sDataBlockDesc.sSpeakerDataBlockDesc[uCEABlockIndex].uSpeakerAllocBlockOffset)
              {
                /* Speaker allocation block offset */
                uWorkingOffset = pSharedData->sDataBlockDesc.sSpeakerDataBlockDesc[uCEABlockIndex].uSpeakerAllocBlockOffset;

                /* Extract the speaker allocation data tag code if it exists */
                uDataTagCode  = (pDataBuf[uWorkingOffset] & HDMI_EDID_DATA_BLOCK_TAG_MASK) >> HDMI_EDID_DATA_BLOCK_TAG_SHIFT;

                /* Check whether the speaker allocation tag exists */
                if(HDMI_EDID_SPEAKER_ALLOC_DATA_BLOCK_TAG_CODE == uDataTagCode)
                {
                  /* Move to next byte */
                  uWorkingOffset++;
                  for(uI = 0; uI < 6; uI++)
                  {
                    if((pDataBuf[uWorkingOffset] >> uI) & 0x1)
                    {
                      switch(uI)
                      {
                        case 0:
                          pAudioModeInfo->uAudioSpeakerLocationMask |= (HDMI_AUDIO_SPEAKER_LOCATION_FL | HDMI_AUDIO_SPEAKER_LOCATION_FR);
                          break;
                        case 1:
                          pAudioModeInfo->uAudioSpeakerLocationMask |=  HDMI_AUDIO_SPEAKER_LOCATION_LFE;
                          break;
                        case 2:
                          pAudioModeInfo->uAudioSpeakerLocationMask |=  HDMI_AUDIO_SPEAKER_LOCATION_FC;
                          break;
                        case 3:
                          pAudioModeInfo->uAudioSpeakerLocationMask |= (HDMI_AUDIO_SPEAKER_LOCATION_RL | HDMI_AUDIO_SPEAKER_LOCATION_RR);
                          break;
                        case 4:
                          pAudioModeInfo->uAudioSpeakerLocationMask |=  HDMI_AUDIO_SPEAKER_LOCATION_RC;
                          break;
                        case 5:
                          pAudioModeInfo->uAudioSpeakerLocationMask |= (HDMI_AUDIO_SPEAKER_LOCATION_FLC | HDMI_AUDIO_SPEAKER_LOCATION_FRC);
                          break;
                        case 6:
                          pAudioModeInfo->uAudioSpeakerLocationMask |= (HDMI_AUDIO_SPEAKER_LOCATION_RLC | HDMI_AUDIO_SPEAKER_LOCATION_RRC);
                          break;
                        default:
                          pAudioModeInfo->uAudioSpeakerLocationMask |= (HDMI_AUDIO_SPEAKER_LOCATION_FL | HDMI_AUDIO_SPEAKER_LOCATION_FR);
                          break;
                      }
                    }
                  }
                }//if(HDMI_EDID_SPEAKER_ALLOC_DATA_BLOCK_TAG_CODE == uDataTagCode)
              }
            }//if(eStatus == MDP_STATUS_OK)
          }//if(bAudioSupport)
        }
      }
    }
  }
  return eStatus;
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetVendorInfo()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetVendorInfo parses the EDID structure and find out
*   the vendor specific information.
*
* \param [in] pVendorInfo  - pointer to the vendor information
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetVendorInfo(HDMI_VendorInfoType* pVendorInfo)
{
  MDP_Status           eStatus               = MDP_STATUS_OK;
  HDMI_DeviceDataType* pSharedData           = HDMI_GETCTX();
  bool32               bAudioSupport         = FALSE;
  bool32               bDispModeSupportAudio = FALSE;
  uint32               uNumOfCEABlocks       = 0;
  uint32               uCEABlockIndex        = 0;
  uint32               uStartingOffset       = 0;
  uint32               uDescOffset           = 0;
  uint8                uI                    = 0;
  uint8                uJ                    = 0;

  uNumOfCEABlocks  = HDMI_EDID_GetNumOfCEAExtBlocks();

  /* Skip Block 2 if block 2 is a BLOCK MAP */
  if(HDMI_EDID_EXTENSION_BLOCK_MAP_TAG_CODE == pSharedData->auEDIDCache[HDMI_EDID_CEA_EXTENSION_VERSION_OFFSET - 1])
  {
    uStartingOffset += HDMI_EDID_BLOCK_SIZE;
    if(uNumOfCEABlocks)
      uNumOfCEABlocks -= 1;
  }

  uStartingOffset += HDMI_EDID_BLOCK_SIZE;
  bDispModeSupportAudio = HDMI_EDID_IsDispModeSupportAudio();

  for(uCEABlockIndex = 0; uCEABlockIndex < uNumOfCEABlocks; uCEABlockIndex++)
  {
    bAudioSupport = (pSharedData->auEDIDCache[uStartingOffset + HDMI_EDID_CEA_EXTENSION_CABS_RELATIVE_OFFSET] & HDMI_EDID_BASIC_AUDIO_SUPPORT_MASK) >> HDMI_EDID_BASIC_AUDIO_SUPPORT_SHIFT;

    /* No Audio support - DVI or the selected display resolution does not supports audio */
    if(!bAudioSupport || !bDispModeSupportAudio)
    {
      pVendorInfo->bVideoOnly = TRUE;
    }  

    if(pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uPhysicalAddress)
    {
      pVendorInfo->sLipSyncInfo.uVideoLatency          = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uVideoLatency;

      /* Selected display resolution does not support audio no need to report the audio latency */
      if(bDispModeSupportAudio)
      {        
        pVendorInfo->sLipSyncInfo.uAudioLatency        = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uAudioLatency;
        pVendorInfo->sLipSyncInfo.uInterlaceAudioLatency = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uInterlacedAudioLatency;
      }
      pVendorInfo->sLipSyncInfo.uInterlaceVideoLatency = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uInterlacedVideoLatency;
      pVendorInfo->uSupportedDeepColorModeMask         = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uSupportedDeepColorModeMask;
      pVendorInfo->uPhysicalAddr                       = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uPhysicalAddress;
      pVendorInfo->bAISupport                          = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].bAISupport;
      pVendorInfo->bDVIDualSupport                     = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].bDVIDualSupport;
      pVendorInfo->uSupportedContentFilterMask         = pSharedData->sDataBlockDesc.sVendorDataBlockDesc[uCEABlockIndex].uSupportedContentFilterMask;
    }
    else
    {
      eStatus = MDP_STATUS_FAILED;
    }  
  }  

  pVendorInfo->uMaxScreenWidthInMillimeter    = (pSharedData->auEDIDCache[HDMI_EDID_HORIZONTAL_SCREEN_SIZE_OFFSET]  * 10);  //Change to mm from cm by multipling 10. 
  pVendorInfo->uMaxScreenHeightInMillimeter   = (pSharedData->auEDIDCache[HDMI_EDID_VERTICAL_SCREEN_SIZE_OFFSET] * 10);     //Change to mm from cm by multipling 10.
  pVendorInfo->uManufacturerId[0]             = (pSharedData->auEDIDCache[HDMI_EDID_MANUFACTURER_ID_OFFSET]);
  pVendorInfo->uManufacturerId[1]             = (pSharedData->auEDIDCache[HDMI_EDID_MANUFACTURER_ID_OFFSET+1]);
  pVendorInfo->uProductId                     = (pSharedData->auEDIDCache[HDMI_EDID_MANUFACTURER_ID_OFFSET+3] << 8 ) | pSharedData->auEDIDCache[HDMI_EDID_MANUFACTURER_ID_OFFSET+2];

  for(uI = 0; uI < HDMI_EDID_TIMING_BLOCK_0_MAX_DESC; uI++)
  {
    uDescOffset = HDMI_EDID_FIRST_TIMING_DESC_OFFSET + (uI * HDMI_EDID_DETAIL_TIMING_DESC_BLOCK_SIZE);

    //Monitor Name descriptor.        
    if((pSharedData->auEDIDCache[uDescOffset] == 0) && (pSharedData->auEDIDCache[uDescOffset + 3] == HDMI_EDID_MONITOR_NAME_TAG))
    { 
      // find name length 
      for ( uJ = 0; uJ <HDMI_EDID_MONITOR_NAME_MAX_LENGTH; uJ++)
      {             
        if (pSharedData->auEDIDCache[uDescOffset + 5 + uJ] == HDMI_EDID_MONITOR_NAME_TERMINATION_CHAR)
        {
          break;
        }
      } 
      pVendorInfo->uNameDescriptionLength = uJ;        
      MDP_OSAL_MEMCPY(&pVendorInfo->uNameDescription[0], 
                     &pSharedData->auEDIDCache[uDescOffset + 5], 
                     pVendorInfo->uNameDescriptionLength);      
      break;
    }
  }
    
  return eStatus;
}
/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetColorBitDepth()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetColorBitDepth parses the EDID structure and find out
*   the color bit depth.
*
* \param [in]      pPixelFormat  - Structure that contains the pixel format
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetColorBitDepth(HDMI_PixelFormatType* pPixelFormat)
{
  MDP_Status           eStatus       = MDP_STATUS_OK;
  HDMI_DeviceDataType* pSharedData   = HDMI_GETCTX();
  uint32             uColorBitCode = 0;

  // Enable a default pixel format, handle cases where EDID is corrupt or invalid.
  *pPixelFormat = MDP_PIXEL_FORMAT_NONE;
  
  //Check if digital interface 
  if(pSharedData->auEDIDCache[HDMI_EDID_VIDEO_INPUT_DEFINITION_OFFSET] & HDMI_EDID_VIDEO_SIGNAL_INTERFACE_MASK)
  {
    uColorBitCode = (pSharedData->auEDIDCache[HDMI_EDID_VIDEO_INPUT_DEFINITION_OFFSET] & HDMI_EDID_COLOR_BIT_DEPTH_MASK) >> HDMI_EDID_COLOR_BIT_DEPTH_SHIFT;

    switch(uColorBitCode)
    {
      case 0:
        *pPixelFormat = MDP_PIXEL_FORMAT_RGB_888_24BPP;
        break;
      case 1:
        *pPixelFormat = MDP_PIXEL_FORMAT_RGB_666_18BPP;
        break;
      case 2:
        *pPixelFormat = MDP_PIXEL_FORMAT_RGB_888_24BPP;
        break;
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      default:
        *pPixelFormat = MDP_PIXEL_FORMAT_RGB_888_24BPP;
        break;
    }
  }
    
  return eStatus;
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetOrientation()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetOrientation parses the EDID structure and find out
*   whether it is portrait or landscape mode.
*
* \param [in]      pRotation   - Structure that contains display orientation
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetOrientation(HDMI_RotateFlipType* pRotation)
{
  MDP_Status           eStatus       = MDP_STATUS_OK;

  *pRotation = MDP_ROTATE_NONE;
    
  return eStatus;
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetDisplayModeInfo()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetDisplayModeInfo parses display mode list and provide
*   display timing generations to the caller. 

* \param [in] pDispModeInfo - Contains all the attributes of a particular display mode
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetDisplayModeInfo(HDMI_DispModeInfoType* pDispModeInfo)
{
  MDP_Status               eStatus       = MDP_STATUS_OK;
  HDMI_DeviceDataType*     pSharedData   = HDMI_GETCTX();
  uint32                   uModeIndex    = 0;
  uint32                   uLUTIndex     = 0;
  HDMI_DispModeTimingType *pLUT          = NULL;


  uModeIndex = pDispModeInfo->uModeIndex;

  if((0 == pSharedData->sDispModeList.uNumOfElements) || (uModeIndex > pSharedData->sDispModeList.uNumOfElements))
  {
    // Invalid Mode index
    eStatus = MDP_STATUS_FAILED;
  }
  else
  {
    if(HDMI_VIDEO_3D_FORMAT_FRAME_PACKING == pSharedData->sDispModeList.eDispModeList[uModeIndex].e3DFrameFormat)
    {
      pLUT = gSupported3DFramePackingModeLUT;

      /* Search this table to find the right timing parameters - This table should be fairly small for the search */
      while((pSharedData->sDispModeList.eDispModeList[uModeIndex].eVideoFormat != gSupported3DFramePackingModeLUT[uLUTIndex].eVideoFormat) && 
            (uLUTIndex < HAL_HDMI_VIDEO_FORMAT_3D_FRAME_PACKING_MAX))
        uLUTIndex++;
    }
    else
    {
      pLUT = gSupportedVideoModeLUT;
      uLUTIndex  = pSharedData->sDispModeList.eDispModeList[uModeIndex].eVideoFormat;
    }
    
    pDispModeInfo->sDispModeTiming.eVideoFormat   = pLUT[uLUTIndex].eVideoFormat;
    pDispModeInfo->sDispModeTiming.uActiveH       = pLUT[uLUTIndex].uActiveH;
    pDispModeInfo->sDispModeTiming.uFrontPorchH   = pLUT[uLUTIndex].uFrontPorchH;
    pDispModeInfo->sDispModeTiming.uPulseWidthH   = pLUT[uLUTIndex].uPulseWidthH;
    pDispModeInfo->sDispModeTiming.uBackPorchH    = pLUT[uLUTIndex].uBackPorchH;
    pDispModeInfo->sDispModeTiming.bActiveLowH    = pLUT[uLUTIndex].bActiveLowH;
    pDispModeInfo->sDispModeTiming.uActiveV       = pLUT[uLUTIndex].uActiveV;
    pDispModeInfo->sDispModeTiming.uFrontPorchV   = pLUT[uLUTIndex].uFrontPorchV;
    pDispModeInfo->sDispModeTiming.uPulseWidthV   = pLUT[uLUTIndex].uPulseWidthV;
    pDispModeInfo->sDispModeTiming.uBackPorchV    = pLUT[uLUTIndex].uBackPorchV;
    pDispModeInfo->sDispModeTiming.bActiveLowV    = pLUT[uLUTIndex].bActiveLowV;
    pDispModeInfo->sDispModeTiming.uRefreshRate   = pLUT[uLUTIndex].uRefreshRate;
    pDispModeInfo->sDispModeTiming.bInterlaced    = pLUT[uLUTIndex].bInterlaced;
    pDispModeInfo->sDispModeTiming.uPixelFreq     = pLUT[uLUTIndex].uPixelFreq;
    pDispModeInfo->sDispModeTiming.bSupported     = pLUT[uLUTIndex].bSupported;
    pDispModeInfo->sDispModeTiming.uPixelRepeatFactor   = pLUT[uLUTIndex].uPixelRepeatFactor;
    pDispModeInfo->sDispModeTiming.eAspectRatio         = pLUT[uLUTIndex].eAspectRatio;
    pDispModeInfo->e3DFrameFormat                       = pSharedData->sDispModeList.eDispModeList[uModeIndex].e3DFrameFormat;
  }

  return eStatus;
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetRawEdidInfo()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetRawEdidInfo extracts the entire RAW EDID information
*
* \param [in] pEdidInfo - Contains the length and data of the display EDID block.
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetRawEdidInfo(HDMI_EdidInfoType* pEdidInfo)
{
  MDP_Status               eStatus       = MDP_STATUS_OK;
  HDMI_DeviceDataType*     pSharedData   = HDMI_GETCTX();
  uint32                   uCopyLength   = pSharedData->uEDIDDataLength;

  if (pEdidInfo->uDataLength)
  {
    if(pEdidInfo->uDataLength <= pSharedData->uEDIDDataLength)
      uCopyLength = pEdidInfo->uDataLength;
  }

  if(pEdidInfo->pDataBuf)
  {
      MDP_OSAL_MEMCPY(pEdidInfo->pDataBuf, pSharedData->auEDIDCache, uCopyLength);
  }

  pEdidInfo->uDataLength = uCopyLength;

  return eStatus;
}

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_EstablishTimingIII()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_EstablishTimingIII parses the establish timing III section of the display descriptor
*
* \param [in]         pDataBuf                       - Buffer that contains EDID data
* \param [in]         uEstablishTimingOffset         - Starting offset of establish timing III section
* \param [in/out]   pDispModeList                    - Array that stores all the display mode supported
*
* \retval void
*
****************************************************************************/
void HDMI_EDID_Parser_EstablishTimingIII(uint8* pDataBuf, uint32 uEstablishTimingOffset, HDMI_DispModeListType* pDispModeList)
{
  /* Skip the first byte of the establish timing section*/
  uint8 uEstablishedTimingIII = pDataBuf[uEstablishTimingOffset + 1];

  if(uEstablishedTimingIII & HDMI_EDID_ESTABLISHED_TIMINGIII_1280x768p60)
  {         
    HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, HAL_HDMI_VIDEO_FORMAT_1280x768p60_5_3, FALSE);    
  }

  /* Move to the next byte of establish timing */	
  uEstablishedTimingIII = pDataBuf[uEstablishTimingOffset + 2];
  
  if(uEstablishedTimingIII & HDMI_EDID_ESTABLISHED_TIMINGIII_1280x960p60)
  {         
    HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, HAL_HDMI_VIDEO_FORMAT_1280x960p60_4_3, FALSE);    
  }

  if(uEstablishedTimingIII & HDMI_EDID_ESTABLISHED_TIMINGIII_1280x1024p60)
  {         
    HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, HAL_HDMI_VIDEO_FORMAT_1280x1024p60_5_4, FALSE);    
  }

  /* Move to the next byte of establish timing */
  uEstablishedTimingIII = pDataBuf[uEstablishTimingOffset + 3];

  if(uEstablishedTimingIII & HDMI_EDID_ESTABLISHED_TIMINGIII_1360x768p60)
  {         
    HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, HAL_HDMI_VIDEO_FORMAT_1360x768p60_16_9, FALSE);    
  }

  if(uEstablishedTimingIII & HDMI_EDID_ESTABLISHED_TIMINGIII_1440x900p60)
  {         
    HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, HAL_HDMI_VIDEO_FORMAT_1440x900p60_16_10, FALSE);    
  }
  
  if(uEstablishedTimingIII & HDMI_EDID_ESTABLISHED_TIMINGIII_1400x1050p60)
  {         
    HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, HAL_HDMI_VIDEO_FORMAT_1400x1050p60_4_3, FALSE);    
  }

  /* Move to the next byte of establish timing */
  uEstablishedTimingIII = pDataBuf[uEstablishTimingOffset + 4];

  if(uEstablishedTimingIII & HDMI_EDID_ESTABLISHED_TIMINGIII_1680x1050p60)
  {         
    HDMI_EDID_InsertDisplayMode(NULL, pDispModeList, 1, 0, HAL_HDMI_VIDEO_FORMAT_1680x1050p60_16_10, FALSE);    
  }    
}


#ifdef __cplusplus
}
#endif

