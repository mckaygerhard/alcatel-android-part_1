#ifndef HDMIAUDIO_H
#define HDMIAUDIO_H
/*
===========================================================================

FILE:         HALHdmiAudio.h

DESCRIPTION:  


===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------


===========================================================================
Copyright (c) 2008-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "HDMIHost.h"


/* -----------------------------------------------------------------------
** Defines 
** ----------------------------------------------------------------------- */
/* Missing from SWI */
#define      HWIO_HDMI_AUDIO_PKT_CTRL1_ADDR      (HDMI_TX_REG_BASE + 0x00000020)

/*!
 * \b HAL_HDMI_AudioParamType
 */
#define HAL_HDMI_AudioParamType                  HDMI_AudioParamType  /* Audio parameters */

/* -----------------------------------------------------------------------
** Enum Types 
** ----------------------------------------------------------------------- */
/*!
 * \b HAL_HDMI_AudioChannelType
 *
 * The Audio channel types supported by the HDMI display
 */
#define   HAL_HDMI_AUDIO_CHANNEL_NONE  HDMI_AUDIO_CHANNEL_NONE
#define   HAL_HDMI_AUDIO_CHANNEL_2     HDMI_AUDIO_CHANNEL_2
#define   HAL_HDMI_AUDIO_CHANNEL_4     HDMI_AUDIO_CHANNEL_4
#define   HAL_HDMI_AUDIO_CHANNEL_6     HDMI_AUDIO_CHANNEL_6
#define   HAL_HDMI_AUDIO_CHANNEL_8     HDMI_AUDIO_CHANNEL_8
#define   HAL_HDMI_AUDIO_CHANNEL_MAX   HDMI_AUDIO_CHANNEL_MAX
#define   HAL_HDMI_AudioChannelType    HDMI_AudioChannelType

/*!
 * \b HAL_HDMI_AudioSampleRateType
 *
 * The various audio sample rate supported by the HDMI display
 */
#define HAL_HDMI_AUDIO_SAMPLE_RATE_NONE     HDMI_AUDIO_SAMPLE_RATE_NONE
#define HAL_HDMI_AUDIO_SAMPLE_RATE_32KHZ    HDMI_AUDIO_SAMPLE_RATE_32KHZ
#define HAL_HDMI_AUDIO_SAMPLE_RATE_44_1KHZ  HDMI_AUDIO_SAMPLE_RATE_44_1KHZ
#define HAL_HDMI_AUDIO_SAMPLE_RATE_48KHZ    HDMI_AUDIO_SAMPLE_RATE_48KHZ
#define HAL_HDMI_AUDIO_SAMPLE_RATE_88_2KHZ  HDMI_AUDIO_SAMPLE_RATE_88_2KHZ
#define HAL_HDMI_AUDIO_SAMPLE_RATE_96KHZ    HDMI_AUDIO_SAMPLE_RATE_96KHZ
#define HAL_HDMI_AUDIO_SAMPLE_RATE_176_4KHZ HDMI_AUDIO_SAMPLE_RATE_176_4KHZ
#define HAL_HDMI_AUDIO_SAMPLE_RATE_192KHZ   HDMI_AUDIO_SAMPLE_RATE_192KHZ
#define HAL_HDMI_AUDIO_SAMPLE_RATE_MAX      HDMI_AUDIO_SAMPLE_RATE_MAX
#define HAL_HDMI_AudioSampleRateType        HDMI_AudioSampleRateType

/*!
 * \b HAL_HDMI_AudioSampleBitDepthType
 *
 * The various audio sample bit depth supported by the HDMI Display
 */
#define HAL_HDMI_AUDIO_BIT_DEPTH_NONE         HDMI_AUDIO_BIT_DEPTH_NONE
#define HAL_HDMI_AUDIO_BIT_DEPTH_16_BIT       HDMI_AUDIO_BIT_DEPTH_16_BIT
#define HAL_HDMI_AUDIO_BIT_DEPTH_20_BIT       HDMI_AUDIO_BIT_DEPTH_20_BIT
#define HAL_HDMI_AUDIO_BIT_DEPTH_24_BIT       HDMI_AUDIO_BIT_DEPTH_24_BIT
#define HAL_HDMI_AUDIO_BIT_DEPTH_MAX          HDMI_AUDIO_BIT_DEPTH_MAX
#define HAL_HDMI_AUDIO_BIT_DEPTH_FORCE_32BIT  HDMI_AUDIO_BIT_DEPTH_FORCE_32BIT
#define HAL_HDMI_AudioSampleBitDepthType      HDMI_AudioSampleBitDepthType

/*!
 * \b HAL_HDMI_AudioInfoLevelShiftType
 *
 * The various audio Level shift 
 */
#define HAL_HDMI_AUDIO_LEVELSHIFT_0DB          HDMI_AUDIO_LEVELSHIFT_0DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_1DB          HDMI_AUDIO_LEVELSHIFT_1DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_2DB          HDMI_AUDIO_LEVELSHIFT_2DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_3DB          HDMI_AUDIO_LEVELSHIFT_3DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_4DB          HDMI_AUDIO_LEVELSHIFT_4DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_5DB          HDMI_AUDIO_LEVELSHIFT_5DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_6DB          HDMI_AUDIO_LEVELSHIFT_6DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_7DB          HDMI_AUDIO_LEVELSHIFT_7DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_8DB          HDMI_AUDIO_LEVELSHIFT_8DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_9DB          HDMI_AUDIO_LEVELSHIFT_9DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_10DB         HDMI_AUDIO_LEVELSHIFT_10DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_11DB         HDMI_AUDIO_LEVELSHIFT_11DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_12DB         HDMI_AUDIO_LEVELSHIFT_12DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_13DB         HDMI_AUDIO_LEVELSHIFT_13DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_14DB         HDMI_AUDIO_LEVELSHIFT_14DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_15DB         HDMI_AUDIO_LEVELSHIFT_15DB
#define HAL_HDMI_AUDIO_LEVELSHIFT_MAX          HDMI_AUDIO_LEVELSHIFT_MAX
#define HAL_HDMI_AUDIO_LEVELSHIFT_FORCE_32BIT  HDMI_AUDIO_LEVELSHIFT_FORCE_32BIT 
#define HAL_HDMI_AudioLevelShiftType           HDMI_AudioLevelShiftType

/*!
 * \b HAL_HDMI_AudioChannelAllocType
 *
 *  Defines the mapping between the audio channel and sink device speakers
 *
 *  FL  - Front Left
 *  FC  - Front Center
 *  FR  - Front Right
 *  FLC - Front Left Center
 *  FRC - Front Right Center
 *  RL  - Rear Left
 *  RC  - Rear Center
 *  RR  - Rear Right
 *  RLC - Rear Left Center
 *  RRC - Rear Right Center
 *  LFE - Low Frequencey Effect
 */
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_0               HDMI_AUDIO_CHANNEL_ALLOC_CODE_0
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_1               HDMI_AUDIO_CHANNEL_ALLOC_CODE_1
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_2               HDMI_AUDIO_CHANNEL_ALLOC_CODE_2
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_3               HDMI_AUDIO_CHANNEL_ALLOC_CODE_3
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_4               HDMI_AUDIO_CHANNEL_ALLOC_CODE_4
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_5               HDMI_AUDIO_CHANNEL_ALLOC_CODE_5
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_6               HDMI_AUDIO_CHANNEL_ALLOC_CODE_6
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_7               HDMI_AUDIO_CHANNEL_ALLOC_CODE_7
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_8               HDMI_AUDIO_CHANNEL_ALLOC_CODE_8
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_9               HDMI_AUDIO_CHANNEL_ALLOC_CODE_9
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_10              HDMI_AUDIO_CHANNEL_ALLOC_CODE_10
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_11              HDMI_AUDIO_CHANNEL_ALLOC_CODE_11 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_12              HDMI_AUDIO_CHANNEL_ALLOC_CODE_12 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_13              HDMI_AUDIO_CHANNEL_ALLOC_CODE_13 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_14              HDMI_AUDIO_CHANNEL_ALLOC_CODE_14 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_15              HDMI_AUDIO_CHANNEL_ALLOC_CODE_15 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_16              HDMI_AUDIO_CHANNEL_ALLOC_CODE_16 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_17              HDMI_AUDIO_CHANNEL_ALLOC_CODE_17 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_18              HDMI_AUDIO_CHANNEL_ALLOC_CODE_18 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_19              HDMI_AUDIO_CHANNEL_ALLOC_CODE_19 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_20              HDMI_AUDIO_CHANNEL_ALLOC_CODE_20 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_21              HDMI_AUDIO_CHANNEL_ALLOC_CODE_21 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_22              HDMI_AUDIO_CHANNEL_ALLOC_CODE_22 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_23              HDMI_AUDIO_CHANNEL_ALLOC_CODE_23 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_24              HDMI_AUDIO_CHANNEL_ALLOC_CODE_24 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_25              HDMI_AUDIO_CHANNEL_ALLOC_CODE_25 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_26              HDMI_AUDIO_CHANNEL_ALLOC_CODE_26 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_27              HDMI_AUDIO_CHANNEL_ALLOC_CODE_27 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_28              HDMI_AUDIO_CHANNEL_ALLOC_CODE_28 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_29              HDMI_AUDIO_CHANNEL_ALLOC_CODE_29 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_30              HDMI_AUDIO_CHANNEL_ALLOC_CODE_30 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_31              HDMI_AUDIO_CHANNEL_ALLOC_CODE_31 
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_MAX             HDMI_AUDIO_CHANNEL_ALLOC_CODE_MAX
#define HAL_HDMI_AUDIO_CHANNEL_ALLOC_CODE_FORCE_32BIT     HDMI_AUDIO_CHANNEL_ALLOC_CODE_FORCE_32BIT
#define HAL_HDMI_AudioChannelAllocType                    HDMI_AudioChannelAllocType

/*!
 * \b HAL_HDMI_AudioFormatType
 *
 *  Defines the audio formats supported by the HDMI display
 */
#define HAL_HDMI_AUDIO_FORMAT_RESERVED            HDMI_AUDIO_FORMAT_RESERVED
#define HAL_HDMI_AUDIO_FORMAT_LPCM                HDMI_AUDIO_FORMAT_LPCM
#define HAL_HDMI_AUDIO_FORMAT_AC3                 HDMI_AUDIO_FORMAT_AC3
#define HAL_HDMI_AUDIO_FORMAT_MPEG1_LAYER1_AND_2  HDMI_AUDIO_FORMAT_MPEG1_LAYER1_AND_2
#define HAL_HDMI_AUDIO_FORMAT_MP3                 HDMI_AUDIO_FORMAT_MP3
#define HAL_HDMI_AUDIO_FORMAT_MPEG2               HDMI_AUDIO_FORMAT_MPEG2
#define HAL_HDMI_AUDIO_FORMAT_AAC                 HDMI_AUDIO_FORMAT_AAC
#define HAL_HDMI_AUDIO_FORMAT_DTS                 HDMI_AUDIO_FORMAT_DTS
#define HAL_HDMI_AUDIO_FORMAT_ATRAC               HDMI_AUDIO_FORMAT_ATRAC
#define HAL_HDMI_AUDIO_FORMAT_ONE_BIT_AUDIO       HDMI_AUDIO_FORMAT_ONE_BIT_AUDIO
#define HAL_HDMI_AUDIO_FORMAT_DOLBY_DIGITAL_PLUS  HDMI_AUDIO_FORMAT_DOLBY_DIGITAL_PLUS
#define HAL_HDMI_AUDIO_FORMAT_DTS_HD              HDMI_AUDIO_FORMAT_DTS_HD
#define HAL_HDMI_AUDIO_FORMAT_MAT                 HDMI_AUDIO_FORMAT_MAT
#define HAL_HDMI_AUDIO_FORMAT_DST                 HDMI_AUDIO_FORMAT_DST
#define HAL_HDMI_AUDIO_FORMAT_WMA_PRO             HDMI_AUDIO_FORMAT_WMA_PRO
#define HAL_HDMI_AUDIO_FORMAT_RESERVED1           HDMI_AUDIO_FORMAT_RESERVED1
#define HAL_HDMI_AUDIO_FORMAT_MAX                 HDMI_AUDIO_FORMAT_MAX
#define HAL_HDMI_AUDIO_FORMAT_FORCE_32BIT         HDMI_AUDIO_FORMAT_FORCE_32BIT 
#define HAL_HDMI_AudioFormatType                  HDMI_AudioFormatType

/*!
 * \b HAL_HDMI_AudioEngineWatermarkType
 *
 * The various audio watermark 
 */
typedef enum
{
  HAL_HDMI_AUDIO_WATERMARK_0 = 0, 
  HAL_HDMI_AUDIO_WATERMARK_1,
  HAL_HDMI_AUDIO_WATERMARK_2,
  HAL_HDMI_AUDIO_WATERMARK_3,
  HAL_HDMI_AUDIO_WATERMARK_4,
  HAL_HDMI_AUDIO_WATERMARK_5,
  HAL_HDMI_AUDIO_WATERMARK_6,
  HAL_HDMI_AUDIO_WATERMARK_7,
  HAL_HDMI_AUDIO_WATERMARK_8,  /* Request when FIFO is fully empty */
  HAL_HDMI_AUDIO_WATERMARK_MAX,
  HAL_HDMI_AUDIO_WATERMARK_FORCE_32BIT = 0x7FFFFFFF
}HAL_HDMI_AudioEngineWatermarkType;

/*!
 * \b HAL_HDMI_AudioSampleRateIndexType
 *
 * Index to the HAL_HDMI_AudioACRParamLUT for calculating the N and CTS values
 */
typedef enum
{
  HAL_HDMI_AUDIO_SAMPLE_RATE_32KHZ_INDEX = 0, 
  HAL_HDMI_AUDIO_SAMPLE_RATE_44_1KHZ_INDEX,
  HAL_HDMI_AUDIO_SAMPLE_RATE_48KHZ_INDEX,
  HAL_HDMI_AUDIO_SAMPLE_RATE_88_2KHZ_INDEX,
  HAL_HDMI_AUDIO_SAMPLE_RATE_96KHZ_INDEX,
  HAL_HDMI_AUDIO_SAMPLE_RATE_176_4KHZ_INDEX,
  HAL_HDMI_AUDIO_SAMPLE_RATE_192KHZ_INDEX,
  HAL_HDMI_AUDIO_SAMPLE_RATE_INDEX_MAX,
  HAL_HDMI_AUDIO_SAMPLE_RATE_INDEX_32BIT = 0x7FFFFFFF
}HAL_HDMI_AudioSampleRateIndexType;
/* -----------------------------------------------------------------------
** Structure Types 
** ----------------------------------------------------------------------- */
/*!
 * \b HAL_HDMI_AudioClkRegenParamType
 *
 * The N and CTS value used to regenerate the audio sampling clock
 */
typedef struct
{
  uint32 uN;     /* N parameter for clock regeneration */
  uint32 uCTS;   /* CTS parameter for clock regeneration */
} HAL_HDMI_AudioACRParamType;

/*!
 * \b HAL_HDMI_AudioACRPacketParamType
 *
 * The contains the parameters needed to configure the ACR packet.
 */
typedef struct
{
  bool32                         bEnable;
  HAL_HDMI_VideoFormatType       eVideoResolution;
  HAL_HDMI_AudioSampleRateType   eAudioSampleRate; 
  HAL_HDMI_AudioChannelType      eAudioNumOfChannel;
} HAL_HDMI_AudioACRPacketParamType;

/*!
 * \b HAL_HDMI_AudioInfoPacketParamType
 *
 * The contains the parameters needed to configure the audio info packet.
 */
typedef struct
{
  bool32                             bEnable;
  bool32                             bDownMix;
  HAL_HDMI_AudioLevelShiftType       eLevelShift;
  HAL_HDMI_AudioChannelType          eAudioNumOfChannel;
  HAL_HDMI_AudioChannelAllocType     eChannelAllocCode;
} HAL_HDMI_AudioInfoPacketParamType;

/*!
 * \b HAL_HDMI_AudioPacketCtrlType
 *
 * The contains the parameters needed to configure the audio packet control.
 */
typedef struct
{
  bool32                             bEnable;
  uint32                             uDelay;  /* ms */
} HAL_HDMI_AudioPacketCtrlType;

/*!
 * \b HAL_HDMI_AudioConfigParamType
 *
 * The contains the parameters needed to configure the audio engine.
 */
typedef struct
{
  bool32                             bEnable;
  HAL_HDMI_AudioEngineWatermarkType  eWatermark;
  HAL_HDMI_AudioChannelType          eAudioNumOfChannel;
} HAL_HDMI_AudioEngineCtrlType;


/* -----------------------------------------------------------------------
** Prototypes
** ----------------------------------------------------------------------- */
void HAL_HDMI_ACRPacketSetup(HAL_HDMI_AudioACRPacketParamType* sACRPacketParam);
void HAL_HDMI_AudioInfoPacketSetup(HAL_HDMI_AudioInfoPacketParamType *sAudioInfoPacketParam);
void HAL_HDMI_AudioPacketCtrl(HAL_HDMI_AudioPacketCtrlType  *psConfig);
void HAL_HDMI_AudioEngineCtrl(HAL_HDMI_AudioEngineCtrlType  *psConfig);
#endif /* HDMIAUDIO_H */


