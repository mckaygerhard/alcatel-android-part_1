#ifndef HDMIVIDEO_H
#define HDMIVIDEO_H
/*
===========================================================================

FILE:         HALHdmiVideo.h

DESCRIPTION: 
  This is the interface to be used to access the SWI of HDMI TX Core.
  The intended audience for this source file is HDMI HOST driver.
  It abtracts the functionalities that the HDMI Core can provide.
  This interface is not intended or recommended for direct usage by 
  any application.


===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------


===========================================================================
Copyright (c) 2008-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "HDMIHost.h"

/* -----------------------------------------------------------------------
** Macros
** ----------------------------------------------------------------------- */
#define HAL_HDMI_VIDEO_FORMAT_3D_FRAME_PACKING_MAX     3
/* -----------------------------------------------------------------------
** Enums
** ----------------------------------------------------------------------- */
/*!
 * \b HAL_HDMI_OperationModeType
 */
typedef enum 
{
  HAL_HDMI_VIDEO_HDMI_MODE            = 0,
  HAL_HDMI_VIDEO_DVI_MODE,
  HAL_HDMI_VIDEO_MODE_MAX,
  HAL_HDMI_VIDEO_MODE_FORCE_32BIT = 0x7FFFFFFF
} HAL_HDMI_OperationModeType;

/*!
 * \b HAL_HDMI_AVI_Colorimetry
 *
* Defines the color encoding standard for the input video data
 */
typedef enum
{
  HAL_HDMI_AVI_COLORIMETRY_NONE       = 0,
  HAL_HDMI_AVI_COLORIMETRY_ITU601,
  HAL_HDMI_AVI_COLORIMETRY_ITU709,
  HAL_HDMI_AVI_COLORIMETRY_XVYCC601,
  HAL_HDMI_AVI_COLORIMETRY_XVYCC709,
  HAL_HDMI_AVI_COLORIMETRY_MAX,
  HAL_HDMI_AVI_COLORIMETRY_FORCE_32BIT = 0x7FFFFFFF
} HAL_HDMI_AVI_Colorimetry;

/*!
 * \b HAL_HDMI_AVI_Quantization
 *
* Defines the color encoding standard for the input video data
 */
typedef enum
{
  HAL_HDMI_AVI_QUANTIZATION_NONE          = 0,
  HAL_HDMI_AVI_QUANTIZATION_LIMITED_RANGE,
  HAL_HDMI_AVI_QUANTIZATION_FULL_RANGE,
  HAL_HDMI_AVI_QUANTIZATION_MAX,
  HAL_HDMI_AVI_QUANTIZATION_FORCE_32BIT = 0x7FFFFFFF
} HAL_HDMI_AVI_Quantization;
/*!
 * \b HAL_HDMI_AVI_ScaleInfo
 *
* Defines whether source data has been scaled in x and y direction
 */
typedef enum
{
  HAL_HDMI_AVI_SCALE_NONE          = 0,
  HAL_HDMI_AVI_SCALE_HORIZONTAL,
  HAL_HDMI_AVI_SCALE_VERTICAL,
  HAL_HDMI_AVI_SCALE_BOTH,
  HAL_HDMI_AVI_SCALE_MAX,
  HAL_HDMI_AVI_SCALE_FORCE_32BIT,
}HAL_HDMI_AVI_ScaleInfo;

/*!
 * \b HAL_HDMI_Video_AspectRatio
 *
* Defines the aspect ratio of the input video format 
 */

#define HAL_HDMI_Video_AspectRatio    HDMI_Video_AspectRatio
/*!
 * \b HAL_HDMI_AVI_PixelRepeatFactor
 *
* Defines the pixel repeated factor
 */
typedef enum
{
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE          = 0,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_2,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_4,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_5,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_6,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_7,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_8,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_9,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_10,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_RESERVED,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_RESERVED1,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_RESERVED2,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_RESERVED3,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_RESERVED4,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX,
  HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_FORCE_32BIT,
}HAL_HDMI_AVI_PixelRepeatFactor;

/* -----------------------------------------------------------------------
** Types
** ----------------------------------------------------------------------- */
/*
 * HAL_HDMI_VideoFormatType
 */
#define HAL_HDMI_VIDEO_FORMAT_640x480p60_4_3     HDMI_VIDEO_FORMAT_640x480p60_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x480p60_4_3     HDMI_VIDEO_FORMAT_720x480p60_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x480p60_16_9    HDMI_VIDEO_FORMAT_720x480p60_16_9
#define HAL_HDMI_VIDEO_FORMAT_1280x720p60_16_9   HDMI_VIDEO_FORMAT_1280x720p60_16_9
#define HAL_HDMI_VIDEO_FORMAT_1920x1080i60_16_9  HDMI_VIDEO_FORMAT_1920x1080i60_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x480i60_4_3     HDMI_VIDEO_FORMAT_720x480i60_4_3
#define HAL_HDMI_VIDEO_FORMAT_1440x480i60_4_3    HDMI_VIDEO_FORMAT_1440x480i60_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x480i60_16_9    HDMI_VIDEO_FORMAT_720x480i60_16_9
#define HAL_HDMI_VIDEO_FORMAT_1440x480i60_16_9   HDMI_VIDEO_FORMAT_1440x480i60_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x240p60_4_3     HDMI_VIDEO_FORMAT_720x240p60_4_3
#define HAL_HDMI_VIDEO_FORMAT_1440x240p60_4_3    HDMI_VIDEO_FORMAT_1440x240p60_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x240p60_16_9    HDMI_VIDEO_FORMAT_720x240p60_16_9
#define HAL_HDMI_VIDEO_FORMAT_1440x240p60_16_9   HDMI_VIDEO_FORMAT_1440x240p60_16_9
#define HAL_HDMI_VIDEO_FORMAT_2880x480i60_4_3    HDMI_VIDEO_FORMAT_2880x480i60_4_3
#define HAL_HDMI_VIDEO_FORMAT_2880x480i60_16_9   HDMI_VIDEO_FORMAT_2880x480i60_16_9
#define HAL_HDMI_VIDEO_FORMAT_2880x240p60_4_3    HDMI_VIDEO_FORMAT_2880x240p60_4_3
#define HAL_HDMI_VIDEO_FORMAT_2880x240p60_16_9   HDMI_VIDEO_FORMAT_2880x240p60_16_9
#define HAL_HDMI_VIDEO_FORMAT_1440x480p60_4_3    HDMI_VIDEO_FORMAT_1440x480p60_4_3
#define HAL_HDMI_VIDEO_FORMAT_1440x480p60_16_9   HDMI_VIDEO_FORMAT_1440x480p60_16_9
#define HAL_HDMI_VIDEO_FORMAT_1920x1080p60_16_9  HDMI_VIDEO_FORMAT_1920x1080p60_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x576p50_4_3     HDMI_VIDEO_FORMAT_720x576p50_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x576p50_16_9    HDMI_VIDEO_FORMAT_720x576p50_16_9
#define HAL_HDMI_VIDEO_FORMAT_1280x720p50_16_9   HDMI_VIDEO_FORMAT_1280x720p50_16_9
#define HAL_HDMI_VIDEO_FORMAT_1920x1080i50_16_9_special HDMI_VIDEO_FORMAT_1920x1080i50_16_9_special
#define HAL_HDMI_VIDEO_FORMAT_720x576i50_4_3     HDMI_VIDEO_FORMAT_720x576i50_4_3
#define HAL_HDMI_VIDEO_FORMAT_1440x576i50_4_3    HDMI_VIDEO_FORMAT_1440x576i50_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x576i50_16_9    HDMI_VIDEO_FORMAT_720x576i50_16_9
#define HAL_HDMI_VIDEO_FORMAT_1440x576i50_16_9   HDMI_VIDEO_FORMAT_1440x576i50_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x288p50_4_3     HDMI_VIDEO_FORMAT_720x288p50_4_3
#define HAL_HDMI_VIDEO_FORMAT_1440x288p50_4_3    HDMI_VIDEO_FORMAT_1440x288p50_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x288p50_16_9    HDMI_VIDEO_FORMAT_720x288p50_16_9
#define HAL_HDMI_VIDEO_FORMAT_1440x288p50_16_9   HDMI_VIDEO_FORMAT_1440x288p50_16_9
#define HAL_HDMI_VIDEO_FORMAT_2880x576i50_4_3    HDMI_VIDEO_FORMAT_2880x576i50_4_3
#define HAL_HDMI_VIDEO_FORMAT_2880x576i50_16_9   HDMI_VIDEO_FORMAT_2880x576i50_16_9
#define HAL_HDMI_VIDEO_FORMAT_2880x288p50_4_3    HDMI_VIDEO_FORMAT_2880x288p50_4_3
#define HAL_HDMI_VIDEO_FORMAT_2880x288p50_16_9   HDMI_VIDEO_FORMAT_2880x288p50_16_9
#define HAL_HDMI_VIDEO_FORMAT_1440x576p50_4_3    HDMI_VIDEO_FORMAT_1440x576p50_4_3
#define HAL_HDMI_VIDEO_FORMAT_1440x576p50_16_9   HDMI_VIDEO_FORMAT_1440x576p50_16_9
#define HAL_HDMI_VIDEO_FORMAT_1920x1080p50_16_9  HDMI_VIDEO_FORMAT_1920x1080p50_16_9
#define HAL_HDMI_VIDEO_FORMAT_1920x1080p24_16_9  HDMI_VIDEO_FORMAT_1920x1080p24_16_9
#define HAL_HDMI_VIDEO_FORMAT_1920x1080p25_16_9  HDMI_VIDEO_FORMAT_1920x1080p25_16_9
#define HAL_HDMI_VIDEO_FORMAT_1920x1080p30_16_9  HDMI_VIDEO_FORMAT_1920x1080p30_16_9
#define HAL_HDMI_VIDEO_FORMAT_2880x480p60_4_3    HDMI_VIDEO_FORMAT_2880x480p60_4_3
#define HAL_HDMI_VIDEO_FORMAT_2880x480p60_16_9   HDMI_VIDEO_FORMAT_2880x480p60_16_9
#define HAL_HDMI_VIDEO_FORMAT_2880x576p50_4_3    HDMI_VIDEO_FORMAT_2880x576p50_4_3
#define HAL_HDMI_VIDEO_FORMAT_2880x576p50_16_9   HDMI_VIDEO_FORMAT_2880x576p50_16_9
#define HAL_HDMI_VIDEO_FORMAT_1920x1080i50_16_9  HDMI_VIDEO_FORMAT_1920x1080i50_16_9
#define HAL_HDMI_VIDEO_FORMAT_1920x1080i100_16_9 HDMI_VIDEO_FORMAT_1920x1080i100_16_9
#define HAL_HDMI_VIDEO_FORMAT_1280x720p100_16_9  HDMI_VIDEO_FORMAT_1280x720p100_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x576p100_4_3    HDMI_VIDEO_FORMAT_720x576p100_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x576p100_16_9   HDMI_VIDEO_FORMAT_720x576p100_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x576i100_4_3    HDMI_VIDEO_FORMAT_720x576i100_4_3
#define HAL_HDMI_VIDEO_FORMAT_1440x576i100_4_3   HDMI_VIDEO_FORMAT_1440x576i100_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x576i100_16_9   HDMI_VIDEO_FORMAT_720x576i100_16_9
#define HAL_HDMI_VIDEO_FORMAT_1440x576i100_16_9  HDMI_VIDEO_FORMAT_1440x576i100_16_9
#define HAL_HDMI_VIDEO_FORMAT_1920x1080i120_16_9 HDMI_VIDEO_FORMAT_1920x1080i120_16_9
#define HAL_HDMI_VIDEO_FORMAT_1280x720p120_16_9  HDMI_VIDEO_FORMAT_1280x720p120_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x480p120_4_3    HDMI_VIDEO_FORMAT_720x480p120_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x480p120_16_9   HDMI_VIDEO_FORMAT_720x480p120_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x480i120_4_3    HDMI_VIDEO_FORMAT_720x480i120_4_3
#define HAL_HDMI_VIDEO_FORMAT_1440x480i120_4_3   HDMI_VIDEO_FORMAT_1440x480i120_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x480i120_16_9   HDMI_VIDEO_FORMAT_720x480i120_16_9
#define HAL_HDMI_VIDEO_FORMAT_1440x480i120_16_9  HDMI_VIDEO_FORMAT_1440x480i120_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x576p200_4_3    HDMI_VIDEO_FORMAT_720x576p200_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x576p200_16_9   HDMI_VIDEO_FORMAT_720x576p200_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x576i200_4_3    HDMI_VIDEO_FORMAT_720x576i200_4_3
#define HAL_HDMI_VIDEO_FORMAT_1440x576i200_4_3   HDMI_VIDEO_FORMAT_1440x576i200_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x576i200_16_9   HDMI_VIDEO_FORMAT_720x576i200_16_9
#define HAL_HDMI_VIDEO_FORMAT_1440x576i200_16_9  HDMI_VIDEO_FORMAT_1440x576i200_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x480p240_4_3    HDMI_VIDEO_FORMAT_720x480p240_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x480p240_16_9   HDMI_VIDEO_FORMAT_720x480p240_16_9
#define HAL_HDMI_VIDEO_FORMAT_720x480i240_4_3    HDMI_VIDEO_FORMAT_720x480i240_4_3
#define HAL_HDMI_VIDEO_FORMAT_1440x480i240_4_3   HDMI_VIDEO_FORMAT_1440x480i240_4_3
#define HAL_HDMI_VIDEO_FORMAT_720x480i240_16_9   HDMI_VIDEO_FORMAT_720x480i240_16_9
#define HAL_HDMI_VIDEO_FORMAT_1440x480i240_16_9  HDMI_VIDEO_FORMAT_1440x480i240_16_9
#define HAL_HDMI_VIDEO_FORMAT_1024x768p60_4_3    HDMI_VIDEO_FORMAT_1024x768p60_4_3
#define HAL_HDMI_VIDEO_FORMAT_1024x768p70_4_3    HDMI_VIDEO_FORMAT_1024x768p70_4_3
#define HAL_HDMI_VIDEO_FORMAT_1024x768p75_4_3    HDMI_VIDEO_FORMAT_1024x768p75_4_3
#define HAL_HDMI_VIDEO_FORMAT_1280x768p60_5_3    HDMI_VIDEO_FORMAT_1280x768p60_5_3
#define HAL_HDMI_VIDEO_FORMAT_1280x800p60_16_10  HDMI_VIDEO_FORMAT_1280x800p60_16_10
#define HAL_HDMI_VIDEO_FORMAT_1280x960p60_4_3    HDMI_VIDEO_FORMAT_1280x960p60_4_3
#define HAL_HDMI_VIDEO_FORMAT_1280x1024p60_5_4   HDMI_VIDEO_FORMAT_1280x1024p60_5_4
#define HAL_HDMI_VIDEO_FORMAT_1360x768p60_16_9   HDMI_VIDEO_FORMAT_1360x768p60_16_9
#define HAL_HDMI_VIDEO_FORMAT_1366x768p60_16_9   HDMI_VIDEO_FORMAT_1366x768p60_16_9
#define HAL_HDMI_VIDEO_FORMAT_1440x900p60_16_10  HDMI_VIDEO_FORMAT_1440x900p60_16_10
#define HAL_HDMI_VIDEO_FORMAT_1400x1050p60_4_3   HDMI_VIDEO_FORMAT_1400x1050p60_4_3 
#define HAL_HDMI_VIDEO_FORMAT_1600x900p60_16_9   HDMI_VIDEO_FORMAT_1600x900p60_16_9 
#define HAL_HDMI_VIDEO_FORMAT_1680x1050p60_16_10 HDMI_VIDEO_FORMAT_1680x1050p60_16_10
#define HAL_HDMI_VIDEO_FORMAT_2560x1600p60_16_10 HDMI_VIDEO_FORMAT_2560x1600p60_16_10


#define HAL_HDMI_VIDEO_FORMAT_MAX                HDMI_VIDEO_FORMAT_MAX
/*!
 * \b HAL_HDMI_VideoFormatType
 */
#define HAL_HDMI_VideoFormatType                 HDMI_VideoFormatType

/*!
 * \b HAL_HDMI_VideoFormat3DType
 */
#define  HAL_HDMI_VIDEO_3D_FORMAT_FRAME_PACKING                     HDMI_VIDEO_3D_FORMAT_FRAME_PACKING
#define  HAL_HDMI_VIDEO_3D_FORMAT_FIELD_ALTERNATIVE                 HDMI_VIDEO_3D_FORMAT_FIELD_ALTERNATIVE
#define  HAL_HDMI_VIDEO_3D_FORMAT_LINE_ALTERNATIVE                  HDMI_VIDEO_3D_FORMAT_LINE_ALTERNATIVE
#define  HAL_HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_FULL                 HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_FULL
#define  HAL_HDMI_VIDEO_3D_FORMAT_L_PLUS_DEPTH                      HDMI_VIDEO_3D_FORMAT_L_PLUS_DEPTH
#define  HAL_HDMI_VIDEO_3D_FORMAT_L_PLUS_DEPTH_PLUS_GRAPHIC_DEPTH   HDMI_VIDEO_3D_FORMAT_L_PLUS_DEPTH_PLUS_GRAPHIC_DEPTH
#define  HAL_HDMI_VIDEO_3D_FORMAT_TOP_BOTTOM                        HDMI_VIDEO_3D_FORMAT_TOP_BOTTOM
#define  HAL_HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF                 HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF

#define  HAL_HDMI_VideoFormat3DType                                 HDMI_VideoFormat3D_type
/*!
 * \b HAL_HDMI_DispModeTimingType
 *
* Table entry for the LUT to check whether the videoFormat is supported by HDMI TX Core v1.0 and the corresponding property
 */
#define HAL_HDMI_DispModeTimingType                             HDMI_DispModeTimingType

/*!
 * \b HAL_HDMI_AVI_Colorimetry
 *
* Defines the color encoding standard for the input video data
 */
#define HAL_HDMI_AVI_COLORIMETRY_NONE     HDMI_AVI_COLORIMETRY_NONE
#define HAL_HDMI_AVI_COLORIMETRY_ITU601   HDMI_AVI_COLORIMETRY_ITU601
#define HAL_HDMI_AVI_COLORIMETRY_ITU709   HDMI_AVI_COLORIMETRY_ITU709
#define HAL_HDMI_AVI_COLORIMETRY_XVYCC601 HDMI_AVI_COLORIMETRY_XVYCC601
#define HAL_HDMI_AVI_COLORIMETRY_XVYCC709 HDMI_AVI_COLORIMETRY_XVYCC709
#define HAL_HDMI_AVI_COLORIMETRY_MAX      HDMI_AVI_COLORIMETRY_MAX
#define HAL_HDMI_AVI_Colorimetry          HDMI_AVI_Colorimetry

/*!
 * \b HAL_HDMI_AVI_ContentFilterType
 */
#define  HAL_HDMI_CONTENT_TYPE_NONE 	HDMI_CONTENT_TYPE_NONE
#define  HAL_HDMI_CONTENT_TYPE_GRAPHICS HDMI_CONTENT_TYPE_GRAPHICS		 
#define  HAL_HDMI_CONTENT_TYPE_PHOTO    HDMI_CONTENT_TYPE_PHOTO		 
#define  HAL_HDMI_CONTENT_TYPE_CINEMA   HDMI_CONTENT_TYPE_CINEMA	
#define  HAL_HDMI_CONTENT_TYPE_GAME     HDMI_CONTENT_TYPE_GAME
#define  HAL_HDMI_CONTENT_TYPE_MAX      HDMI_CONTENT_TYPE_MAX,
#define  HAl_HDMI_AVI_ContentFilterType	HDMI_AVI_ContentFilterType

/*!
 * \b HAL_HDMI_AVI_ScanType
 */
#define HAL_HDMI_AVI_SCAN_NONE      HDMI_AVI_SCAN_NONE
#define HAL_HDMI_AVI_SCAN_OVERSCAN  HDMI_AVI_SCAN_OVERSCAN
#define HAL_HDMI_AVI_SCAN_UNDERSCAN HDMI_AVI_SCAN_UNDERSCAN
#define HAL_HDMI_AVI_SCAN_MAX       HDMI_AVI_SCAN_MAX
#define HAL_HDMI_AVI_ScanType       HDMI_AVI_ScanType

/*!
 * \b HAL_HDMI_AVI_ActiveFormatType
 */
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_NONE                      HDMI_AVI_ACTIVE_FORMAT_NONE
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_BOX_16_9_TOP              HDMI_AVI_ACTIVE_FORMAT_BOX_16_9_TOP
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_BOX_14_9_TOP              HDMI_AVI_ACTIVE_FORMAT_BOX_14_9_TOP
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_BOX_16_9_CENTER           HDMI_AVI_ACTIVE_FORMAT_BOX_16_9_CENTER
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_CODED_FRAME               HDMI_AVI_ACTIVE_FORMAT_CODED_FRAME
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_4_3_CENTER                HDMI_AVI_ACTIVE_FORMAT_4_3_CENTER
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_16_9_CENTER               HDMI_AVI_ACTIVE_FORMAT_16_9_CENTER
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_14_9_CENTER               HDMI_AVI_ACTIVE_FORMAT_14_9_CENTER
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_4_3_PROTECT_14_9_CENTER   HDMI_AVI_ACTIVE_FORMAT_4_3_PROTECT_14_9_CENTER
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_16_9_PROTECT_14_9_CENTER  HDMI_AVI_ACTIVE_FORMAT_16_9_PROTECT_14_9_CENTER
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_16_9_PROTECT_4_3_CENTER   HDMI_AVI_ACTIVE_FORMAT_16_9_PROTECT_4_3_CENTER
#define  HAL_HDMI_AVI_ACTIVE_FORMAT_MAX                       HDMI_AVI_ACTIVE_FORMAT_MAX
#define  HAL_HDMI_AVI_ActiveFormatType                        HDMI_AVI_ActiveFormatType

/*!
 * \b HAL_HDMI_AVI_ColorFormatType
 */
#define  HAL_HDMI_AVI_COLOR_FORMAT_RGB           HDMI_AVI_COLOR_FORMAT_RGB
#define  HAL_HDMI_AVI_COLOR_FORMAT_YUV422        HDMI_AVI_COLOR_FORMAT_YUV422
#define  HAL_HDMI_AVI_COLOR_FORMAT_YUV444        HDMI_AVI_COLOR_FORMAT_YUV444
#define  HAL_HDMI_AVI_COLOR_FORMAT_MAX           HDMI_AVI_COLOR_FORMAT_MAX
#define  HAL_HDMI_AVI_ColorFormatType            HDMI_AVI_ColorFormatType
/*!
 * \b HAL_HDMI_AVI_LetterBoxInfoType
 *
* Defines all the parametes required to configure the letter box width and height
 */
typedef struct
{
  bool32 bBarInfoValid;
  uint32 uEndOfTopBarLine;
  uint32 uStartOfBottomBarLine;
  uint32 uEndOfLeftBarLine;
  uint32 uStartOfRightBarLine;
} HAL_HDMI_AVI_LetterBoxInfoType;

/*!
 * \b HAL_HDMI_AVIPacketParamType
 *
* Defines all the parametes required to configure the AVI packet
 */
typedef struct 
{
  bool32                            bITContent;
  HAL_HDMI_AVI_ScanType             eScanInfo;
  HAL_HDMI_AVI_LetterBoxInfoType    sLetterBoxInfo;
  HAL_HDMI_AVI_ActiveFormatType     eActiveFormat;
  HAL_HDMI_AVI_ColorFormatType      eColorFormat;
  HAL_HDMI_VideoFormatType          eVideoIDCode;
  HAL_HDMI_AVI_Quantization         eQuantization;
  HAL_HDMI_AVI_Colorimetry          eColorimetry;
  HAL_HDMI_AVI_ScaleInfo            eScaleInfo;
  HAL_HDMI_Video_AspectRatio        eAspectRatio;
  HAL_HDMI_AVI_PixelRepeatFactor    ePixelRepeatFactor;
  HAl_HDMI_AVI_ContentFilterType    eContentFilter;
} HAL_HDMI_AVIPacketParamType;

/* -----------------------------------------------------------------------
** Static Variables
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Prototypes
** ----------------------------------------------------------------------- */
/****************************************************************************
*
** FUNCTION: HAL_HDMI_VideoAudioSetup()
*/
/*!
* \brief
*   The \b HAL_HDMI_VideoAudioSetup function configures all the video/audio 
*   attributes that the HDMI TX Core requires to be functional.
*
* \param [in]  sParam   - contains the values of the attributes for the corresponding
                          property that needs configuration.
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_VideoInterfaceSetup(HAL_HDMI_VideoFormatType eVideoFormat, HAL_HDMI_VideoFormat3DType e3DFrameFormat);
/****************************************************************************
*
** FUNCTION: HAL_HDMI_AVIInfoFramePacketSetup()
*/
/*!
* \brief
*   The \b HAL_HDMI_AVIInfoFramePacketSetup function configures all the 
*   auxiliary video info packet parameters. (Refer to HDMI 1.3 spec and above) 
*   These parameters are derived based on the recommended values provided in the HDMI
*   specification. (Spec 1.4 specifically for 3D inclusion)
*
* \param [in]  psAVIInfoPacketParam   - it contains video resolution
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_AVIInfoFramePacketSetup(HAL_HDMI_AVIPacketParamType* psAVIInfoPacketParam);
#endif /* HDMIVIDEO_H */


