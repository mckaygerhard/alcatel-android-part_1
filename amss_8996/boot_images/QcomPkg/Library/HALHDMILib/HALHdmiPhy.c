
/*
===========================================================================

FILE:         HALHdmiPhy.c


DESCRIPTION:  
  This is the interface to be used to access Qualcomm's HDMI TX Core 
  PHY interface. The intended audience for this source file HDMI 
  host driver layer which incorporates this hardware interface with 
  HDMI TX Core. This interface is not intended or recommended for direct 
  usage by any application.

===========================================================================

                             Edit History

  $$

when       who     what, where, why
--------   ---     --------------------------------------------------------


===========================================================================
Copyright (c) 2008-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "HALHdmiPhy.h"
#include "MDPSystem.h"
#include <Base.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/UefiBootServicesTableLib.h>

HAL_HDMI_PhyPllFunctionTable gHdmiPhyPllFxnTable;

/* -----------------------------------------------------------------------
** Macros
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Static Variable
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Private Functions
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Public Functions
** ----------------------------------------------------------------------- */

/****************************************************************************
*
** FUNCTION: HAL_HDMI_PhyFxnsInit()
*/
/*!
* \DESCRIPTION
*   Initialize HDMI PHY/PLL function table
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_PhyFxnsInit(void)
{

    uint32                uRegValue = 0;
    HAL_HW_VersionType    sCoreVersion;

    /*
    * Note: we are using HDMI Tx core version number here because the PHY version registers
    *       have different offset and we have no way to access different regisgters here
    */
    uRegValue = in_dword(HWIO_MMSS_HDMI_VERSION_ADDR);
    sCoreVersion.uMajorVersion = HWIO_GETVAL(HDMI_VERSION, uRegValue, MAJOR_VERSION);
    sCoreVersion.uMinorVersion = HWIO_GETVAL(HDMI_VERSION, uRegValue, MINOR_VERSION);
    sCoreVersion.uReleaseVersion = HWIO_GETVAL(HDMI_VERSION, uRegValue, STEP);

    /* Aragorn */
    if ((3 == sCoreVersion.uMajorVersion) &&
        (0 == sCoreVersion.uMinorVersion) &&
        (1 == sCoreVersion.uReleaseVersion))
    {
        gHdmiPhyPllFxnTable.HAL_HDMI_PHY_Config = HAL_HDMI_3_0_1_PHY_Config;
    }
    /* Elessar, Estel, Eldarion */
    else if ((3 == sCoreVersion.uMajorVersion) &&
             (3 == sCoreVersion.uMinorVersion))
    {
        gHdmiPhyPllFxnTable.HAL_HDMI_PHY_Config = HAL_HDMI_3_3_0_PHY_Config;
    }
    /* Istari, Elfstone */
    else if ((4 == sCoreVersion.uMajorVersion) &&
             (0 == sCoreVersion.uMinorVersion))
    {
        gHdmiPhyPllFxnTable.HAL_HDMI_PHY_Config = HAL_HDMI_4_0_0_PHY_Config;
    }
}


/****************************************************************************
*
** FUNCTION: HAL_HDMI_PHY_Config()
*/
/*!
* \DESCRIPTION
*   The HAL_HDMI_PHY_Config function to config Hdmi phy and pll freg
*
* \param [in]   pHdmiPllConfigInfo  - Hdmi Phy Pll config info
*
* \retval boolean
*
****************************************************************************/
bool32 HAL_HDMI_PHY_Config(HdmiPhyConfigType  *pHdmiPllConfigInfo)
{
    bool32      bStatus = FALSE;

    if (NULL != gHdmiPhyPllFxnTable.HAL_HDMI_PHY_Config)
    {
        bStatus = gHdmiPhyPllFxnTable.HAL_HDMI_PHY_Config(pHdmiPllConfigInfo);
    }
    return bStatus;
}


#ifdef __cplusplus
}
#endif

