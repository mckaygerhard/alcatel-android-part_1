#ifndef HDMIUTIL_H
#define HDMIUTIL_H
/*
===========================================================================

FILE:         HDMI_Util.h

DESCRIPTION:  
  This is the interface that provides helper functions to handle non HDMI specific
  configurations.

===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------


===========================================================================
Copyright (c) 2008-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "MDPTypes.h"
/* -----------------------------------------------------------------------
** Macros
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Types
** ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
** Static Variables
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Prototypes
** ----------------------------------------------------------------------- */
/****************************************************************************
*
** FUNCTION: HDMI_EnableTLMM_DDC_HPD()
*/
/*!
* \brief
*   The \b HDMI_EnableTLMM_DDC_HPD Initializes the GPIO configuration for the 
*   DDC lines and enables the HPD GPIO configurations.
**
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EnableTLMM_DDC_HPD(void);

/****************************************************************************
*
** FUNCTION: HDMI_DisableTLMM_DDC()
*/
/*!
* \brief
*   The \b HDMI_DisableTLMM_DDC disable the GPIO configuration for the 
*   DDC lines.
**
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_DisableTLMM_DDC_HPD(void);



#endif /* HDMIUTIL_H */


