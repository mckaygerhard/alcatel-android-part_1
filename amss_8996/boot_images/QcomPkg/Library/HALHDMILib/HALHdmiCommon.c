
/*
===========================================================================

FILE:         HalHdmiCommon.c

DESCRIPTION:  
  This is the common interface to the HDMI TX Core. It abstracts the system
  functionalities of the Core. The main user to these interfaces is the HDMI
  Host Driver and these interfaces are not expected to be called by any 
  application directly.


  Copyright (c) 2011-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ===========================================================================
*/

#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "HALHdmiCommon.h"

#define HWIO_MMSS_HDMI_AVI_INFO1_CONTENT_TYPE_BMSK                                          0x3000
#define HWIO_MMSS_HDMI_AVI_INFO1_CONTENT_TYPE_SHFT                                          0xc

/* -----------------------------------------------------------------------
** Static Variable
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Private Functions
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Public Functions
** ----------------------------------------------------------------------- */
/****************************************************************************
*
** FUNCTION: HAL_HDMI_PowerModeSet()
*/
/*!
* \brief
*   The \b HAL_HDMI_PowerModeSet function set the HDMI TX core to 
*   POWERED ON or OFF
*
* \param [in]  None
*
* \retval HAL_HDMI_PowerModeType
*
****************************************************************************/
void HAL_HDMI_PowerModeSet(HAL_HDMI_PowerModeType ePowerMode)
{
    uint32 uRegVal  = HWIO_MMSS_HDMI_CTRL_IN;

    if(HAL_HDMI_POWER_OFF == ePowerMode)
      uRegVal &= ~(HWIO_MMSS_HDMI_CTRL_ENABLE_BMSK);
    else
      uRegVal |= (1 << HWIO_MMSS_HDMI_CTRL_ENABLE_SHFT) & HWIO_MMSS_HDMI_CTRL_ENABLE_BMSK;

    HWIO_MMSS_HDMI_CTRL_OUT(uRegVal);
}
/****************************************************************************
*
** FUNCTION: HAL_HDMI_PowerModeGet()
*/
/*!
* \brief
*   The \b HAL_HDMI_PowerModeGet function retreives whether the HDMI TX core is 
*   POWERED ON or OFF
*
* \param [in]  None
*
* \retval HAL_HDMI_PowerModeType
*
****************************************************************************/
HAL_HDMI_PowerModeType HAL_HDMI_PowerModeGet(void)
{
    HAL_HDMI_PowerModeType eMode    = HAL_HDMI_POWER_OFF;
    uint32                 uRegVal  = HWIO_MMSS_HDMI_CTRL_IN;

    if(uRegVal & (1 << HWIO_MMSS_HDMI_CTRL_ENABLE_SHFT))
    {
      eMode = HAL_HDMI_POWER_ON;
    }
    return eMode;
}

/****************************************************************************
*
** FUNCTION: HAL_HDMI_OperationModeGet()
*/
/*!
* \brief
*   The \b HAL_HDMI_OperationModeGet function retreives whether the HDMI TX core is 
*   FULL HDMI MODE or DVI MODE
*
* \param [in]  None
*
* \retval HAL_HDMI_OperationModeType
*
****************************************************************************/
HAL_HDMI_OperationModeType HAL_HDMI_OperationModeGet(void)
{
    HAL_HDMI_OperationModeType eMode    = HAL_HDMI_VIDEO_DVI_MODE;
    uint32                     uRegVal  = HWIO_MMSS_HDMI_CTRL_IN;

    if(uRegVal & (1 << HWIO_MMSS_HDMI_CTRL_HDMI_DVI_SEL_SHFT))
    {
        eMode = HAL_HDMI_VIDEO_HDMI_MODE;
    }
    return eMode;
}

/****************************************************************************
*
** FUNCTION: HAL_HDMI_EngineCtrl()
*/
/*!
* \brief
*   The \b HAL_HDMI_EngineCtrl function configures the power mode and the video mode 
*   of the HDMI TX core.
*
* \param [in]  mode   - either DVI mode or FULL HDMI Mode
* \param [in]  ePower - either POWER_ON or POWER_OFF
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_EngineCtrl(HAL_HDMI_OperationModeType eMode, HAL_HDMI_PowerModeType ePower)
{
    uint32 uRegVal = HWIO_MMSS_HDMI_CTRL_IN;
    
    if(HAL_HDMI_POWER_ON == ePower)
    {
      //Enable the block
      uRegVal |= (1 << HWIO_MMSS_HDMI_CTRL_ENABLE_SHFT);
      //HDMI or DVI mode
      if(HAL_HDMI_VIDEO_HDMI_MODE == eMode)
      {
        //Toggle the DVI bit to reset the Audio counters everytime we start the HDMI Engine.
        uRegVal &= ~(1 << HWIO_MMSS_HDMI_CTRL_HDMI_DVI_SEL_SHFT);
        HWIO_MMSS_HDMI_CTRL_OUT(uRegVal); 
    
        uRegVal |= (1 << HWIO_MMSS_HDMI_CTRL_HDMI_DVI_SEL_SHFT);
      }
      else
      {
        /* DVI only */
        uRegVal &= ~(1 << HWIO_MMSS_HDMI_CTRL_HDMI_DVI_SEL_SHFT);
      }
    }
    else if (HAL_HDMI_POWER_OFF == ePower)
    {
      //Disable the block
      uRegVal &= ~HWIO_MMSS_HDMI_CTRL_ENABLE_BMSK;
    }

    HWIO_MMSS_HDMI_CTRL_OUT(uRegVal); // swap R&B, and enable HDMI, HDMI mode (0x1 for DVI)
}

/****************************************************************************
*
** FUNCTION: HAL_HDMI_VendorInfoFramePacketSetup()
*/
/*!
* \brief
*   The \b HAL_HDMI_VendorInfoFramePacketSetup function configures all the vendor specific 
*   info packet parameters. (Refer to HDMI spec 1.4 and above) 
*   These parameters are derived based on the recommended values provided in the HDMI
*   specification. (Spec 1.4 specifically for 3D inclusion)
*
* \param [in]  psVendorInfoPacketParam   - it contains video resolution
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_VendorInfoFramePacketSetup(HAL_HDMI_VendorInfoFramePacketParamType* psVendorInfoPacketParam)
{
  uint32 uPacketHeader      = 0;
  uint32 uCheckSum          = 0;
  uint32 uPacketPayload     = 0;
  uint32 uGenPckCtrlReg     = 0;
  uint32 uG0packetLineStart = 1;

  /* Setup Packet header and payload */
  uPacketHeader  = (HAL_HDMI_VS_INFO_FRAME_ID & HWIO_MMSS_HDMI_GENERIC0_HDR_HB0_BMSK);
  uPacketHeader |= (HAL_HDMI_VS_INFO_FRAME_VERSION << HWIO_MMSS_HDMI_GENERIC0_HDR_HB1_SHFT) & HWIO_MMSS_HDMI_GENERIC0_HDR_HB1_BMSK;
  uPacketHeader |= (HAL_HDMI_VS_INFO_FRAME_PAYLOAD_LENGTH << HWIO_MMSS_HDMI_GENERIC0_HDR_HB2_SHFT) & HWIO_MMSS_HDMI_GENERIC0_HDR_HB2_BMSK;

  HWIO_MMSS_HDMI_GENERIC0_HDR_OUT(uPacketHeader);

  /* Checksum = (256 - (256 % sum of all bytes)) % 256 */
  uCheckSum  =  (uPacketHeader & 0xff);
  uCheckSum +=  ((uPacketHeader >> 8) & 0xff);
  uCheckSum +=  ((uPacketHeader >> 16) & 0xff);

  if(psVendorInfoPacketParam->bVideoFormat3D)
  {
    uPacketPayload  = ((HAL_HDMI_VS_INFO_FRAME_3D_PRESENT & HWIO_MMSS_HDMI_GENERIC0_1_BYTE4_BMSK) << HAL_HDMI_VS_INFO_FRAME_3D_PRESENT_FIELD_SHIFT);
    /* eVideoFormat3D - minus 1 because HDMI spec is zero based */
    uPacketPayload |= ((((psVendorInfoPacketParam->videoFormat.eVideoFormat3D - 1 )<< HWIO_MMSS_HDMI_GENERIC0_1_BYTE5_SHFT) & HWIO_MMSS_HDMI_GENERIC0_1_BYTE5_BMSK) 
                                                                           << HAL_HDMI_VS_INFO_FRAME_3D_STRUCTURE_FIELD_SHIFT);

    if(HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF == psVendorInfoPacketParam->videoFormat.eVideoFormat3D)
    {
      /* eExtData3D - minus 1 because HDMI spec is zero based */
      uPacketPayload |= ((((psVendorInfoPacketParam->eExtData3D  - 1 ) << HWIO_MMSS_HDMI_GENERIC0_1_BYTE6_SHFT) & HWIO_MMSS_HDMI_GENERIC0_1_BYTE6_BMSK) 
                                                                             << HAL_HDMI_VS_INFO_FRAME_3D_STRUCTURE_FIELD_SHIFT);
    }
  }
  else
    uPacketPayload = (((psVendorInfoPacketParam->videoFormat.eVideoFormat2D << HWIO_MMSS_HDMI_GENERIC0_1_BYTE5_SHFT) & HWIO_MMSS_HDMI_GENERIC0_1_BYTE5_BMSK)
                                                                           << HAL_HDMI_VS_INFO_FRAME_3D_STRUCTURE_FIELD_SHIFT);
  HWIO_MMSS_HDMI_GENERIC0_1_OUT(uPacketPayload);

  uCheckSum +=  (uPacketPayload & 0xff);
  uCheckSum +=  ((uPacketPayload >> 8) & 0xff);

  /* Reset for next packet composition */
  uPacketPayload = 0;

  /* Next 3 bytes are IEEE Registration Identifcation */
  uPacketPayload |= (HAL_HDMI_VS_INFO_FRAME_IEEE_REGISTRATION_ID  << HWIO_MMSS_HDMI_GENERIC0_0_BYTE1_SHFT) & HWIO_MMSS_HDMI_GENERIC0_0_BYTE1_BMSK;
  uPacketPayload |= ((HAL_HDMI_VS_INFO_FRAME_IEEE_REGISTRATION_ID >> 8) << HWIO_MMSS_HDMI_GENERIC0_0_BYTE2_SHFT) & HWIO_MMSS_HDMI_GENERIC0_0_BYTE2_BMSK;
  uPacketPayload |= ((HAL_HDMI_VS_INFO_FRAME_IEEE_REGISTRATION_ID >> 16)<< HWIO_MMSS_HDMI_GENERIC0_0_BYTE3_SHFT) & HWIO_MMSS_HDMI_GENERIC0_0_BYTE3_BMSK;

  uCheckSum +=  (HAL_HDMI_VS_INFO_FRAME_IEEE_REGISTRATION_ID & 0xff);
  uCheckSum +=  ((HAL_HDMI_VS_INFO_FRAME_IEEE_REGISTRATION_ID >> 8) & 0xff);
  uCheckSum +=  ((HAL_HDMI_VS_INFO_FRAME_IEEE_REGISTRATION_ID >> 16) & 0xff);

  uCheckSum = (HAL_HDMI_INFO_FRAME_CHECKSUM_MODULUS - (uCheckSum % HAL_HDMI_INFO_FRAME_CHECKSUM_MODULUS)) % HAL_HDMI_INFO_FRAME_CHECKSUM_MODULUS;
  uPacketPayload |= (uCheckSum & HWIO_MMSS_HDMI_GENERIC0_0_BYTE0_BMSK);

  HWIO_MMSS_HDMI_GENERIC0_0_OUT(uPacketPayload);

  /* Setup HDMI TX generic packet control */
  uGenPckCtrlReg  = (uG0packetLineStart << HWIO_MMSS_HDMI_GEN_PKT_CTRL_GENERIC0_LINE_SHFT) & HWIO_MMSS_HDMI_GEN_PKT_CTRL_GENERIC0_LINE_BMSK;
  /* Enable this packet to transmit every frame */
  uGenPckCtrlReg |= (1 << HWIO_MMSS_HDMI_GEN_PKT_CTRL_GENERIC0_UPDATE_SHFT) & HWIO_MMSS_HDMI_GEN_PKT_CTRL_GENERIC0_UPDATE_BMSK;
  /* Enable this packet to transmit every frame */
  uGenPckCtrlReg |= (1 << HWIO_MMSS_HDMI_GEN_PKT_CTRL_GENERIC0_CONT_SHFT) & HWIO_MMSS_HDMI_GEN_PKT_CTRL_GENERIC0_CONT_BMSK;
  /* Enable HDMI TX engine to transmit Generic packet 0 */
  uGenPckCtrlReg |= (1 & HWIO_MMSS_HDMI_GEN_PKT_CTRL_GENERIC0_SEND_BMSK);

  HWIO_MMSS_HDMI_GEN_PKT_CTRL_OUT(uGenPckCtrlReg);
}
/****************************************************************************
*
** FUNCTION: HAL_HDMI_AVIInfoFramePacketSetup()
*/
/*!
* \brief
*   The \b HAL_HDMI_AVIInfoFramePacketSetup function configures all the AVI specific 
*   info packet parameters. (Refer to HDMI spec 1.3 and above) 
*   These parameters are derived based on the recommended values provided in the HDMI
*   specification. 
*
* \param [in]  psAVIInfoPacketParam   - it contains AVI info configuration parameters
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_AVIInfoFramePacketSetup(HAL_HDMI_AVIPacketParamType* psAVIInfoPacketParam)
{
  uint32 uAVIInfoFrameCtrl0Reg  =  HWIO_MMSS_HDMI_INFOFRAME_CTRL0_IN;
  uint32 uAVIInfoFrameCtrl1Reg  =  HWIO_MMSS_HDMI_INFOFRAME_CTRL1_IN;
  uint32 uAVIInfoReg            = 0;
  uint32 uExtendedColorimeter   = 0;
  uint32 uColorimeter           = 0;
  uint32 uCheckSum              = 0;
  uint32 uBarInfo               = 0;
  uint32 uByte                  = 0;
  uint32 uPacketLineStart       = 1;
  uint32 uContentType			= 0;

  if(HAL_HDMI_AVI_COLORIMETRY_XVYCC601 == psAVIInfoPacketParam->eColorimetry ||
    (HAL_HDMI_AVI_COLORIMETRY_XVYCC709 == psAVIInfoPacketParam->eColorimetry))
    uColorimeter = 3;
  else
    uColorimeter = psAVIInfoPacketParam->eColorimetry;

  if(HAL_HDMI_AVI_COLORIMETRY_XVYCC709 == psAVIInfoPacketParam->eColorimetry)
    uExtendedColorimeter = 1;
  
  /*Setting up content Type According to the HDMI 1.4a Specs*/
  if(psAVIInfoPacketParam->eContentFilter != HAL_HDMI_CONTENT_TYPE_NONE)
  {
    switch(psAVIInfoPacketParam->eContentFilter)
    {
      case (HAL_HDMI_CONTENT_TYPE_GRAPHICS):
         uContentType = 0;
      break;
      case (HAL_HDMI_CONTENT_TYPE_PHOTO):
         uContentType = 1;
      break;
      case (HAL_HDMI_CONTENT_TYPE_CINEMA):
         uContentType = 2;
      break;
      case (HAL_HDMI_CONTENT_TYPE_GAME):
         uContentType = 3;
      break;
      default:
      break;

   }
  }
  else 
  {
    psAVIInfoPacketParam->bITContent = FALSE;
  }
  
  /* Setup AVI info 1 register */
  if(psAVIInfoPacketParam->sLetterBoxInfo.bBarInfoValid)
    uAVIInfoReg  = (psAVIInfoPacketParam->sLetterBoxInfo.uEndOfTopBarLine << HWIO_MMSS_HDMI_AVI_INFO1_TOP_SHFT) & HWIO_MMSS_HDMI_AVI_INFO1_TOP_BMSK;
  else
    uAVIInfoReg = 0;

  /*Programming Pixel Repeat Factor. For Now Set as None */
  if ( HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_MAX == psAVIInfoPacketParam->ePixelRepeatFactor )
  {
    /*Todo, user may need to specify the repeat factor, default to none */
    psAVIInfoPacketParam->ePixelRepeatFactor = HAL_HDMI_AVI_PIXEL_REPEAT_FACTOR_NONE;
  }
   
  uAVIInfoReg |= (psAVIInfoPacketParam->ePixelRepeatFactor << HWIO_MMSS_HDMI_AVI_INFO1_PR_SHFT) & HWIO_MMSS_HDMI_AVI_INFO1_PR_BMSK;
  
  /*Content type- Game, Cinema etc*/
  uAVIInfoReg |= (uContentType << HWIO_MMSS_HDMI_AVI_INFO1_CONTENT_TYPE_SHFT) & HWIO_MMSS_HDMI_AVI_INFO1_CONTENT_TYPE_BMSK;
  

  /* eVideoIDCode is zero based so need to add 1 to reflect to real VIC value in th spec */
  uAVIInfoReg |= ((psAVIInfoPacketParam->eVideoIDCode + 1) << HWIO_MMSS_HDMI_AVI_INFO1_VIC_SHFT) & HWIO_MMSS_HDMI_AVI_INFO1_VIC_BMSK;
  HWIO_MMSS_HDMI_AVI_INFO1_OUT(uAVIInfoReg);

  /* Setup AVI info 2 register */
  if(psAVIInfoPacketParam->sLetterBoxInfo.bBarInfoValid)
  {
    uAVIInfoReg  = (psAVIInfoPacketParam->sLetterBoxInfo.uStartOfBottomBarLine << HWIO_MMSS_HDMI_AVI_INFO2_BOTTOM_SHFT) & HWIO_MMSS_HDMI_AVI_INFO2_BOTTOM_BMSK;
    uAVIInfoReg |= (psAVIInfoPacketParam->sLetterBoxInfo.uEndOfLeftBarLine << HWIO_MMSS_HDMI_AVI_INFO2_LEFT_SHFT) & HWIO_MMSS_HDMI_AVI_INFO2_LEFT_BMSK;
  }
  else
    uAVIInfoReg = 0;

  HWIO_MMSS_HDMI_AVI_INFO2_OUT(uAVIInfoReg);

  /* Setup AVI info 3 register */
  uAVIInfoReg  = (HAL_HDMI_AVI_INFO_FRAME_VERSION << HWIO_MMSS_HDMI_AVI_INFO3_VERSION_SHFT) & HWIO_MMSS_HDMI_AVI_INFO3_VERSION_BMSK;
  if(psAVIInfoPacketParam->sLetterBoxInfo.bBarInfoValid)
    uAVIInfoReg |= (psAVIInfoPacketParam->sLetterBoxInfo.uStartOfRightBarLine << HWIO_MMSS_HDMI_AVI_INFO3_RIGHT_SHFT) & HWIO_MMSS_HDMI_AVI_INFO3_RIGHT_BMSK;

  HWIO_MMSS_HDMI_AVI_INFO3_OUT(uAVIInfoReg);

  /* Setup AVI info 0 register */
  uAVIInfoReg   = (psAVIInfoPacketParam->bITContent << HWIO_MMSS_HDMI_AVI_INFO0_IT_CONTENT_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_IT_CONTENT_BMSK;
  uAVIInfoReg  |= (uExtendedColorimeter << HWIO_MMSS_HDMI_AVI_INFO0_EXTENDED_COLORIMETRY_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_EXTENDED_COLORIMETRY_BMSK;  //As per the Programming Done at the start of Function 
  uAVIInfoReg  |= (psAVIInfoPacketParam->eQuantization << HWIO_MMSS_HDMI_AVI_INFO0_QUANTIZATION_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_QUANTIZATION_BMSK;
  uAVIInfoReg  |= (psAVIInfoPacketParam->eScaleInfo << HWIO_MMSS_HDMI_AVI_INFO0_NON_UNIFORM_SCALE_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_NON_UNIFORM_SCALE_BMSK;
  uAVIInfoReg  |= (uColorimeter	<< HWIO_MMSS_HDMI_AVI_INFO0_COLORIMETRY_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_COLORIMETRY_BMSK; //As per the Programming Done at the start of Function
  uAVIInfoReg  |= (psAVIInfoPacketParam->eAspectRatio << HWIO_MMSS_HDMI_AVI_INFO0_PICTURE_ASPECT_RATIO_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_PICTURE_ASPECT_RATIO_BMSK;
  uAVIInfoReg  |= (psAVIInfoPacketParam->eActiveFormat << HWIO_MMSS_HDMI_AVI_INFO0_ACTIVE_FORMAT_RATIO_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_ACTIVE_FORMAT_RATIO_BMSK;
  uAVIInfoReg  |= (psAVIInfoPacketParam->eColorFormat << HWIO_MMSS_HDMI_AVI_INFO0_RGB_OR_YCBCR_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_RGB_OR_YCBCR_BMSK;
  if(psAVIInfoPacketParam->eActiveFormat)
  {
    uAVIInfoReg  |= (1 << HWIO_MMSS_HDMI_AVI_INFO0_ACTIVE_INFO_PRES_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_ACTIVE_INFO_PRES_BMSK;
  }  

  if(psAVIInfoPacketParam->sLetterBoxInfo.bBarInfoValid)
  {
    if(psAVIInfoPacketParam->sLetterBoxInfo.uEndOfTopBarLine || psAVIInfoPacketParam->sLetterBoxInfo.uStartOfBottomBarLine)
      uBarInfo = 1;
    if(psAVIInfoPacketParam->sLetterBoxInfo.uEndOfLeftBarLine || psAVIInfoPacketParam->sLetterBoxInfo.uStartOfRightBarLine)
      uBarInfo |= 2;

    uAVIInfoReg |= (uBarInfo << HWIO_MMSS_HDMI_AVI_INFO0_BAR_INFO_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_BAR_INFO_BMSK;
  }
  else
  {
    uAVIInfoReg |= (psAVIInfoPacketParam->sLetterBoxInfo.bBarInfoValid << HWIO_MMSS_HDMI_AVI_INFO0_BAR_INFO_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_BAR_INFO_BMSK;
  }  

  uAVIInfoReg  |= (psAVIInfoPacketParam->eScanInfo << HWIO_MMSS_HDMI_AVI_INFO0_SCAN_INFO_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_SCAN_INFO_BMSK;

  /* Calculate CheckSum */
  /*Frame ID */
  uCheckSum  = HAL_HDMI_AVI_INFO_FRAME_ID;
  /*Version # */
  uCheckSum += HAL_HDMI_AVI_INFO_FRAME_VERSION;
  /*Length of the AVI info packet*/
  uCheckSum += HAL_HDMI_AVI_INFO_FRAME_LENGTH;
  /*Byte 1 =  Bit(7)  - Reserved, 
              Bit(6,5)- ColorFormat,
              Bit(4)  - Active format or not 
              Bit(3,2)- BarInfo
              Bit(1,0)- ScanInfo */
  /*ScanInfo */
  uByte  = psAVIInfoPacketParam->eScanInfo;
  /* BarInfo */
  uByte |= (uBarInfo << 2); 
  /*Active format or not */
  if(psAVIInfoPacketParam->eActiveFormat)
  {
    uByte |= (1 << 4);
  }  
  /*ColorFormat*/
  uByte |= (psAVIInfoPacketParam->eColorFormat << 5);
  /*Byte 1*/
  uCheckSum += uByte;

  /*Byte 2 = Bit(7,6) - Colorimetry
             Bit(5,4) - Picture Aspect Ratio
             Bit(3,0) - ActiveFormat Aspect Ratio */
  /* ActiveFormat Aspect Ratio */
  uByte  = psAVIInfoPacketParam->eActiveFormat;
  /* Picture Aspect Ratio */
  uByte |= (psAVIInfoPacketParam->eAspectRatio << 4);
  /* Colorimetry */
  uByte |= (uColorimeter << 6);
  uCheckSum += uByte;

  /*Byte 3 = Bit(7)     - IT content
             Bit(6,5,4) - Extended Colorimetry
             Bit(3,2)   - RGB Quantization
             Bit(1,0)   - ScaleInfo*/
  /* ScaleInfo */
  uByte = psAVIInfoPacketParam->eScaleInfo;
  /* RGB Quantization */
  uByte |= (psAVIInfoPacketParam->eQuantization << 2);
  /* Extended Colorimetry */
  uByte |= (uExtendedColorimeter << 4);
  /* IT content */
  uByte |= (psAVIInfoPacketParam->bITContent << 7);
  uCheckSum += uByte;

  /*Byte 4 = Bit(7)     - Reserved
             Bit(6,5,4,3,2,1,0) - Video Identification Code (VIC)
    VIC - Add one because it is zero based */
  uByte = (psAVIInfoPacketParam->eVideoIDCode + 1);
  uCheckSum += uByte;
  
  /*Byte 5 = Bit(7,6,5,4) - Reserved
             Bit(3,2,1,0) - Pixel Repetition Factor
    Pixel Repetition Factor */
  uByte = psAVIInfoPacketParam->ePixelRepeatFactor;
  uByte |= (uContentType << 4); 
  uCheckSum += uByte;

  /*Byte 6 = Lower Bit(7-0) - Line # of end of top bar */
  if(psAVIInfoPacketParam->sLetterBoxInfo.bBarInfoValid)
  {
    uByte = (psAVIInfoPacketParam->sLetterBoxInfo.uEndOfTopBarLine & 0xFF);
    uCheckSum += uByte;

    /*Byte 7 = Upper Bit(7-0) - Line # of end of top bar */
    uByte = ((psAVIInfoPacketParam->sLetterBoxInfo.uEndOfTopBarLine >> 8) & 0xFF);
    uCheckSum += uByte;

    /*Byte 8 = Lower Bit(7-0) - Line # of Start of Bottom bar */
    uByte = (psAVIInfoPacketParam->sLetterBoxInfo.uStartOfBottomBarLine & 0xFF);
    uCheckSum += uByte;

    /*Byte 9 = Upper Bit(7-0) - Line # of Start of Bottom bar */
    uByte = ((psAVIInfoPacketParam->sLetterBoxInfo.uStartOfBottomBarLine >> 8) & 0xFF);
    uCheckSum += uByte;

    /*Byte 10 = Lower Bit(7-0) - Line # of end of left bar */
    uByte = (psAVIInfoPacketParam->sLetterBoxInfo.uEndOfLeftBarLine & 0xFF);
    uCheckSum += uByte;

    /*Byte 11 = Upper Bit(7-0) - Line # of end of left bar */
    uByte = ((psAVIInfoPacketParam->sLetterBoxInfo.uEndOfLeftBarLine >> 8) & 0xFF);
    uCheckSum += uByte;

    /*Byte 12 = Lower Bit(7-0) - Line # of start of right bar */
    uByte = (psAVIInfoPacketParam->sLetterBoxInfo.uStartOfRightBarLine & 0xFF);
    uCheckSum += uByte;

    /*Byte 13 = Upper Bit(7-0) - Line # of start of right bar */
    uByte = ((psAVIInfoPacketParam->sLetterBoxInfo.uStartOfRightBarLine >> 8) & 0xFF);
    uCheckSum += uByte;
  }

  uCheckSum = (HAL_HDMI_INFO_FRAME_CHECKSUM_MODULUS - (uCheckSum % HAL_HDMI_INFO_FRAME_CHECKSUM_MODULUS)) % HAL_HDMI_INFO_FRAME_CHECKSUM_MODULUS;

  uAVIInfoReg |= (uCheckSum << HWIO_MMSS_HDMI_AVI_INFO0_CHECKSUM_SHFT) & HWIO_MMSS_HDMI_AVI_INFO0_CHECKSUM_BMSK;;
  HWIO_MMSS_HDMI_AVI_INFO0_OUT(uAVIInfoReg);

  /* Program which line to start AVI info packet transmission */
  uAVIInfoFrameCtrl1Reg |= (uPacketLineStart << HWIO_MMSS_HDMI_INFOFRAME_CTRL1_AVI_INFO_LINE_SHFT) & HWIO_MMSS_HDMI_INFOFRAME_CTRL1_AVI_INFO_LINE_BMSK;
  HWIO_MMSS_HDMI_INFOFRAME_CTRL1_OUT(uAVIInfoFrameCtrl1Reg);

  /*Program the control register for the AVI info packet */
  /* Enable this packet to transmit every frame */
  uAVIInfoFrameCtrl0Reg |= (1 << HWIO_MMSS_HDMI_INFOFRAME_CTRL0_AVI_INFO_CONT_SHFT) & HWIO_MMSS_HDMI_INFOFRAME_CTRL0_AVI_INFO_CONT_BMSK;
  /* Enable HDMI TX engine to transmit AVI info packet  */
  uAVIInfoFrameCtrl0Reg |= (1 & HWIO_MMSS_HDMI_INFOFRAME_CTRL0_AVI_INFO_SEND_BMSK);

  HWIO_MMSS_HDMI_INFOFRAME_CTRL0_OUT(uAVIInfoFrameCtrl0Reg);
}
/****************************************************************************
*
** FUNCTION: HAL_HDMI_VideoAudioSetup()
*/
/*!
* \brief
*   The \b HAL_HDMI_VideoAudioSetup function configures all the video/audio 
*   attributes that the HDMI TX Core requires to be functional.
*
* \param [in]  sParam   - contains the values of the attributes for the corresponding
                          property that needs configuration.
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_VideoAudioSetup(HAL_HDMI_VideoAudioParamType *sParam)
{
  switch(sParam->eDeviceID)
  {
    case HAL_HDMI_DEVICE_ID0:
    {
      if(sParam->uFeatureOp & HAL_HDMI_DIRTYBIT_VIDEO_RESOLUTION)
      {
        bool32 bRestorePower = FALSE;
        /* Power off HDMI engine prior to programming the interface*/
        if(HAL_HDMI_POWER_ON == HAL_HDMI_PowerModeGet())
        {
          HAL_HDMI_PowerModeSet(HAL_HDMI_POWER_OFF);
          bRestorePower = TRUE;
        }
        /* Program HDMI timing interface with new resolution */
        HAL_HDMI_VideoInterfaceSetup(sParam->eVideoResolution, sParam->sVSPacketParams.videoFormat.eVideoFormat3D);
        /* Restore Power */
        if(bRestorePower)
          HAL_HDMI_PowerModeSet(HAL_HDMI_POWER_ON);
      }

      if(sParam->uFeatureOp & HAL_HDMI_DIRTYBIT_AUDIO_PARAM)
      {
        // Audio not supported
      }

      if(sParam->uFeatureOp & HAL_HDMI_DIRTYBIT_AVI_PACKET_TYPES)
      {
        HAL_HDMI_AVIInfoFramePacketSetup(&sParam->sAVIPacketParams);
      }

      if(sParam->uFeatureOp & HAL_HDMI_DIRTYBIT_VS_PACKET_TYPES)
      {
        HAL_HDMI_VendorInfoFramePacketSetup(&sParam->sVSPacketParams);
      }

      if(sParam->uFeatureOp & (HAL_HDMI_DIRTYBIT_POWER_MODE | HAL_HDMI_DIRTYBIT_OPERATION_MODE))
      {
        HAL_HDMI_PowerModeType     ePowerMode = HAL_HDMI_PowerModeGet();
        HAL_HDMI_OperationModeType eMode      = HAL_HDMI_OperationModeGet();

        if(sParam->uFeatureOp & HAL_HDMI_DIRTYBIT_POWER_MODE)
        {
          ePowerMode = sParam->ePowerMode;
        }
        if(sParam->uFeatureOp & HAL_HDMI_DIRTYBIT_OPERATION_MODE)
        {
          eMode = sParam->eMode;
        }
        //Disable and re-enable HDMI engine
        HAL_HDMI_EngineCtrl(eMode, HAL_HDMI_POWER_OFF);
        HAL_HDMI_EngineCtrl(eMode, ePowerMode);
      }
      break;
    }
    default:
      break;
  }
}

#ifdef __cplusplus
}
#endif

