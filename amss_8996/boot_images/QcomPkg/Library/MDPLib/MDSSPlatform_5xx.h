#ifndef __MDSSPlatform_5xx_H__
#define __MDSSPlatform_5xx_H__
/*=============================================================================
 
  File: MDSSPlatform_5xx.h
 
  Internal header file for MDP library
  
 
  Copyright (c) 2011-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "MDPLib_i.h"
#include "MDSSPlatform_510.h"
#include "MDSSPlatform_570.h"
#include "MDSSPlatform_3xx.h"
#include "MDSSPlatform_590.h"

/*===========================================================================
                                Defines 
===========================================================================*/

#define RES_MAX_FAMILY_MAJOR                      6
#define RES_MAX_FAMILY_MINOR                     10

#define XO_DEFAULT_FREQ_IN_HZ                    19200000
 /*=========================================================================
       Public Functions
 ==========================================================================*/

/* MDP resource list for MDP1.x
*/
static const MDP_HwMinorFamilyResourceList asHarwareFamilyMinor1x[RES_MAX_FAMILY_MINOR] = {
    {MDP_DEVICE_VERSION_05_00, (DisplayResourceList*)&sDisplayMDP570Resources},  /* MDP Family 1.0 , Taking default as 8994*/
    {MDP_DEVICE_VERSION_05_01, (DisplayResourceList*)&sDisplayMDP570Resources},  /* MDP Family 1.1 , Taking default as 8994*/
    {MDP_DEVICE_VERSION_05_02, (DisplayResourceList*)&sDisplayMDP570Resources},  /* MDP Family 1.2 , Taking default as 8994*/
    {MDP_DEVICE_VERSION_05_03, (DisplayResourceList*)&sDisplayMDP570Resources},  /* MDP Family 1.3 , Taking default as 8994 */   
    {MDP_DEVICE_VERSION_05_04, (DisplayResourceList*)&sDisplayMDP570Resources},  /* MDP Family 1.4 , Taking default as 8994*/   
    {MDP_DEVICE_VERSION_05_05, (DisplayResourceList*)&sDisplayMDP570Resources},  /* MDP Family 1.5 , MSM8994 v1*/   
    {MDP_DEVICE_VERSION_05_06, (DisplayResourceList*)&sDisplayMDP510Resources},  /* MDP Family 1.6 , MSM8916*/
    {MDP_DEVICE_VERSION_05_07, (DisplayResourceList*)&sDisplayMDP590Resources},  /* MDP Family 1.7 , MSM8996*/
    {MDP_DEVICE_VERSION_MAX, NULL},                                              /* MDP Family 1.8 */    
    {MDP_DEVICE_VERSION_05_05, (DisplayResourceList*)&sDisplayMDP570Resources},  /* MDP Family 1.9 , MSM8994 v2*/
};


/* MDP resource list for MDP3.x;
*/
static const MDP_HwMinorFamilyResourceList asHarwareFamilyMinor3x[RES_MAX_FAMILY_MINOR] = {
    {MDP_DEVICE_VERSION_MAX, NULL},                                              /* MDP Family 3.0 */
    {MDP_DEVICE_VERSION_MAX, NULL},                                              /* MDP Family 3.1 */
    {MDP_DEVICE_VERSION_MAX, NULL},                                              /* MDP Family 3.2 */
    {MDP_DEVICE_VERSION_MAX, NULL},                                              /* MDP Family 3.3 */
    {MDP_DEVICE_VERSION_03_04, (DisplayResourceList*)&sDisplayMDP3xxResources},  /* MDP Family 3.4 */
    {MDP_DEVICE_VERSION_03_05, (DisplayResourceList*)&sDisplayMDP3xxResources},  /* MDP Family 3.5 */
}; 


/* MDP resource list  based on MDP major version.
   MDP3.x does not have a MDP version, use the MDP major version for clock list indexing
*/
static const MDP_HwMajorFamilyResourceList asHarwareFamilyMajor[RES_MAX_FAMILY_MAJOR] = {
    {NULL},                                                    /* MDP Family 0.x   */
    {(MDP_HwMinorFamilyResourceList*)&asHarwareFamilyMinor1x}, /* MDP Fmaily 1.x   */
    {NULL},                                                    /* MDP Family 2.x   */ 
    {(MDP_HwMinorFamilyResourceList*)&asHarwareFamilyMinor3x}, /* MDP Fmaily 3.x   */
    {NULL},                                                    /* MDP Family 4.x   */
    {NULL}                                                     /* MDP Family 5.x   */
}; 
       



#endif // __MDSSPlatform_5xx_H__

