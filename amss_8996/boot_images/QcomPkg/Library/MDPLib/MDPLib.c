/*=============================================================================
 
  File: MDPLIb.c
 
  Source file for MDP functions
  
 
  Copyright (c) 2013-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/SynchronizationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UncachedMemoryAllocationLib.h>
#include <Library/QcomLib.h>
#include <Library/QcomBaseLib.h>
#include <Library/UefiCfgLib.h>
#include <Library/RamPartitionTableLib.h>
#include <Protocol/EFIScm.h>
#include <Include/scm_sip_interface.h>
#include "MDPLib.h"
#include "MDPLib_i.h"
#include "MDPSystem.h"
#include "MDPPlatformLib.h"
#include "HALHDMIDriver.h"

/*=========================================================================
     Default Defines
==========================================================================*/
#define MDSS_BASEADDRESSMAPINGS_MAX               10

/* Default MDPInit Flags */
#define DEAFAULT_MDP_INIT_FLAGS                  (HAL_MDP_INIT_FLAGS_VBIF_CONFIGURATION |\
                                                  HAL_MDP_INIT_FLAGS_DISABLE_INTERRUPTS |\
                                                  HAL_MDP_INIT_FLAGS_CLEAR_INTERRUPTS)
  
              
/*=========================================================================
     Local Static Variables
==========================================================================*/

/* Local context for storing display state 
 */
static MDP_Panel_AttrType  gDisplayInfo[MDP_DISPLAY_MAX];


/*=========================================================================
     Local Static Functions
==========================================================================*/


/* Used to store persistent data to firmware environment variables for consumption by the OS drivers
 */
static MDP_Status MDPPlatformExportEnvVariables(MDPFirmwareEnvType *pFirmwareEnvInfo);


/* Function to detect the presence of HDMI and valid mode supported
 */
static bool32 MDPDetectHDMIPlugin(MDP_Panel_AttrType  *pPanelInfo);

/* Read UEFI variable DISABLEDISPLAY
 */
static bool32 MDPPlatformGetDisableDisplay(void);

/* Set MDSS base address
 */
static MDP_Status MDPPlatformSetMdssBase(EFIChipInfoFamilyType sEFIChipSetFamily);

/*=========================================================================
     Globals
==========================================================================*/
extern EFI_GUID       gMSFTVendorGuid;
extern uint8         *gpDSIInitSequenceBuffer;
extern uint8         *gpDSITermSequenceBuffer;
MDP_HwPrivateInfo     gsMDPHwPrivateInfo;

EFI_PLATFORMINFO_PLATFORM_TYPE     gePlatformType = EFI_PLATFORMINFO_TYPE_UNKNOWN;

/*

Mapping for mdss base adress
*/
static const sMDSS_BaseAddressMappings asMDSS_BaseAddressMappings[MDSS_BASEADDRESSMAPINGS_MAX] = {  
             {EFICHIPINFO_FAMILY_UNKNOWN, 0x00000000},
             {EFICHIPINFO_FAMILY_MSM8994, 0xFD900000},  //for 8994(Elessar) family                                                                   
             {EFICHIPINFO_FAMILY_MSM8916, 0x01A00000},  //for 8916(bagheera) family
             {EFICHIPINFO_FAMILY_APQ8096, 0x00900000},  //for 8096(Istari) family
             {EFICHIPINFO_FAMILY_MSM8996, 0x00900000},  //for 8996(Istari) family
             {EFICHIPINFO_NUM_FAMILIES,   0xFD900000},  //used as default , when no chipfamily is found
};

/* Locks used to prevent re-entrancy */
static MDP_LockInfo sLockInfo;

/*=========================================================================
      Public APIs
==========================================================================*/


/****************************************************************************
*
** FUNCTION: MDPInit()
*/
/*!
* \brief
*   This function will perform the basic initialization and detection of the MDP core
*
* \param [out] pMDPInitParams   - Information regarding the hardware core
*        [in]  uFlags           - Reserved
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPInit(MDP_InitParamsType *pMDPInitParams, uint32 uFlags)
{
    MDP_Status          eStatus             = MDP_STATUS_OK;
    MDP_HwPrivateInfo  *psMDPHwPrivateInfo  = &gsMDPHwPrivateInfo;

    MDP_LOG_FUNC_ENTRY("MDPInit", 0x00);    

    MDP_OSAL_MEMZERO(psMDPHwPrivateInfo, sizeof(MDP_HwPrivateInfo));
    
    psMDPHwPrivateInfo->sEFIChipSetId = EFICHIPINFO_ID_UNKNOWN;

    if ((NULL == gpDSIInitSequenceBuffer ) ||
        (NULL == gpDSITermSequenceBuffer ))
    {
      gpDSIInitSequenceBuffer = (uint8*)UncachedAllocateZeroPool(MDP_DSI_COMMAND_BUFFER_SIZE);
      gpDSITermSequenceBuffer = (uint8*)UncachedAllocateZeroPool(MDP_DSI_COMMAND_BUFFER_SIZE);
    }

    if ((NULL == gpDSIInitSequenceBuffer ) ||
        (NULL == gpDSITermSequenceBuffer ))
    {
      eStatus = MDP_STATUS_NO_RESOURCES;
    }
    else if (NULL == pMDPInitParams)
    {
      eStatus = MDP_STATUS_BAD_PARAM;
    }
    else
    {
        MDPPlatformParams     sPlatformParams;
        
        if (MDP_INIT_FLAG_MINIMAL_INIT & uFlags)
        {
          //For minimal init do not clear the display context as it will be reused later.
        }
        else
        {
          // Default case is complete intialization, clear the display context.
          MDP_OSAL_MEMZERO(&gDisplayInfo, sizeof(gDisplayInfo));
        }

        // Need to know if the platform is hardware accelerated first. If it is not then we have to skip initialization of all HW.
        if (MDP_STATUS_OK != (eStatus = MDPPlatformConfigure(MDP_DISPLAY_PRIMARY, MDPPLATFORM_CONFIG_SW_RENDERER, &sPlatformParams)))
        {
           DEBUG ((EFI_D_WARN, "MDPLib: MDPPlatformConfigure(MDPPLATFORM_CONFIG_SW_RENDERER) failed!\n"));  
           // Force SW rendering path
           sPlatformParams.sPlatformInfo.bSWRender = TRUE;
        }

        // 
        // SW rendering path
        //
        if (TRUE == sPlatformParams.sPlatformInfo.bSWRender)
        {
           // Framebuffer only mode allows DXE to execute, but does not touch the hardware
           //        
           // Setup for frame buffer only mode
           DEBUG ((EFI_D_WARN, "MDPLib: Hardware not detection, entering FrameBuffer mode!\n"));  
           
           // Populate the input parameters
           pMDPInitParams->uMDPVersionMajor    = 0;
           pMDPInitParams->uMDPVersionMinor    = 0;
           pMDPInitParams->uMDPVersionRevision = 0;
           
           // Fix the supported displays to primary & external
           MDP_OSAL_MEMZERO(&pMDPInitParams->aSupportedDisplays, sizeof(pMDPInitParams->aSupportedDisplays));
           pMDPInitParams->aSupportedDisplays[MDP_DISPLAY_PRIMARY]  = TRUE;
           pMDPInitParams->aSupportedDisplays[MDP_DISPLAY_EXTERNAL] = FALSE;        
        }
        else
        {
          //Get the platform Chip ID and catch in gsMDPHwPrivateInfo
          if (MDP_STATUS_OK == (eStatus = MDPPlatformConfigure(MDP_DISPLAY_PRIMARY, MDPPLATFORM_CONFIG_GETPLATFORMINFO, &sPlatformParams)))
          {
            psMDPHwPrivateInfo->sEFIChipSetId     = sPlatformParams.sPlatformInfo.sEFIChipSetId;
            psMDPHwPrivateInfo->sEFIChipSetFamily = sPlatformParams.sPlatformInfo.sEFIChipSetFamily;
            gePlatformType                        = sPlatformParams.sPlatformInfo.sEFIPlatformType.platform;
          }
        
          // Hardware path
          //
          if (MDP_STATUS_OK != (eStatus = MDPSetCoreClock(0x0)))
          {
            DEBUG ((EFI_D_WARN, "MDPLib: Failed to setup MDP core clocks!\n"));
          }
          //Set MDSS base address
          else if (MDP_STATUS_OK != (eStatus = MDPPlatformSetMdssBase(sPlatformParams.sPlatformInfo.sEFIChipSetFamily)))
          {
            DEBUG ((EFI_D_WARN, "MDPLib: MDPPlatformSetMdssBase() failed!\n"));
          }
          else
          {
            if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_Init(NULL, DEAFAULT_MDP_INIT_FLAGS))
            {
              DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_Init() failed!\n"));
              eStatus = MDP_STATUS_HW_ERROR;
            }
            // Initialize the HW private info 
            else if (MDP_STATUS_OK != (eStatus = MDPInitHwPrivateInfo(psMDPHwPrivateInfo)))
            {
              DEBUG ((EFI_D_WARN, "MDPLib: MDPInitHwPrivateInfo() failed!\n"));
            }
            else 
            {
              MDPInitSMPPool();
              
              // Handle various MDP hardware block initialization
              if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_TrafficCtrl_Init(NULL, 0))
              {
                DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_TrafficCtrl_Init() failed!\n"));  
              }
              else
              {
                // Hardware detected
                // - Setup based on the hardware
              
                // Populate the input parameters
                pMDPInitParams->uMDPVersionMajor    = psMDPHwPrivateInfo->sMDPVersionInfo.uMajorVersion;
                pMDPInitParams->uMDPVersionMinor    = psMDPHwPrivateInfo->sMDPVersionInfo.uMinorVersion;
                pMDPInitParams->uMDPVersionRevision = psMDPHwPrivateInfo->sMDPVersionInfo.uReleaseVersion;

                // Find the supported displays 
                MDP_OSAL_MEMZERO(&pMDPInitParams->aSupportedDisplays, sizeof(pMDPInitParams->aSupportedDisplays));
                pMDPInitParams->aSupportedDisplays[MDP_DISPLAY_PRIMARY]  = TRUE;
                pMDPInitParams->aSupportedDisplays[MDP_DISPLAY_EXTERNAL] = TRUE;
                    
                // Check the external display configuration.
                if (1 == PcdGet32(PcdDisableHDMI))
                {
                  pMDPInitParams->aSupportedDisplays[MDP_DISPLAY_EXTERNAL] = FALSE;
                }

                // If DISABLEDISPLAY variable is set, then disable primary display
                // Note: Make sure to check if external display is supported before proceeding
                if(TRUE == pMDPInitParams->aSupportedDisplays[MDP_DISPLAY_EXTERNAL])
                {
                  if(TRUE == MDPPlatformGetDisableDisplay())
                  {
                    // DISABLEDISPLAY variable is set. Disable primary display
                    pMDPInitParams->aSupportedDisplays[MDP_DISPLAY_PRIMARY]  = FALSE;
                  }
                  else
                  {
                    // DIABLEDISPLAY is not set, therefore disable external display
                    pMDPInitParams->aSupportedDisplays[MDP_DISPLAY_EXTERNAL] = FALSE;
                  }
                }
              }
            }
          }
        }

    }

    MDP_LOG_FUNC_EXIT("MDPInit()");   

    return eStatus;
}


/****************************************************************************
*
** FUNCTION: MDPAcquireLockOrFail()
*/
/*!
* \brief
*   This function will try to get the specified lock.
*
* The function will acquire the lock if not already acquired
* It will return FAIL if lock is already acquired 
*
* \param  eLock              - Lock to acquire 
*
* \retval MDP_STATUS_OK      - Lock acquired successfully
* \retval MDP_STATUS_FAILED  - Failed to acquire Lock 
*
****************************************************************************/
MDP_Status  MDPAcquireLockOrFail(MDP_Lock_Type eLock)
{
  MDP_Status eStatus = MDP_STATUS_OK;

  if (MDP_LOCK_TYPE_MAX <= eLock)
  {
    eStatus = MDP_STATUS_BAD_PARAM;
  }
  else
  {
    // uLock = 0 -> free, uLock = 1 -> acquired
    if (InterlockedCompareExchange32(&sLockInfo.uLock[eLock], 0, 1) != 0) 
    {
      // failed to acquire lock
      eStatus = MDP_STATUS_FAILED;
    }
  }

  return eStatus;
}

/****************************************************************************
*
** FUNCTION: MDPReleaseLock()
*/
/*!
* \brief
*   This function will release the specified lock.
*
* \param  eLock          - Lock to release 
*
* \retval MDP_STATUS_OK  - Lock released successfully
*
****************************************************************************/
MDP_Status  MDPReleaseLock(MDP_Lock_Type eLock)
{
  MDP_Status eStatus = MDP_STATUS_OK;

  if (MDP_LOCK_TYPE_MAX <= eLock)
  {
    eStatus = MDP_STATUS_BAD_PARAM;
  }
  else
  {
    // uLock = 0 -> free, uLock = 1 -> acquired
    InterlockedCompareExchange32(&sLockInfo.uLock[eLock], 1, 0);
  }

  return eStatus;
}


/****************************************************************************
*
** FUNCTION: MDPPower()
*/
/*!
* \brief
*   This function will power up and initialize the interface needed for a particular display.
*
* \param [in] eDisplayId       - The display to initialize
*        [in] pMDPPowerParams  - Power configuration
*        [in] uFlags           - Reserved
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPPower(MDP_Display_IDType eDisplayId, MDP_PowerParamsType *pMDPPowerParams, uint32 uFlags)
{
    MDP_Status eStatus = MDP_STATUS_OK;

    MDP_LOG_FUNC_ENTRY("MDPPower()", eDisplayId);    

    if (NULL == pMDPPowerParams)
    {
      eStatus = MDP_STATUS_BAD_PARAM;
    }
    else if (TRUE == pMDPPowerParams->bPowerOn)
    {
        // Power up the respective displays
        if (MDP_STATUS_OK != (eStatus = MDPPlatformConfigure(eDisplayId, MDPPLATFORM_CONFIG_POWERUP, NULL)))
        {
            DEBUG ((EFI_D_WARN, "MDPLib: MDPPlatformConfigure(MDPPLATFORM_CONFIG_POWERUP) failed!\n"));   
        }
    }
    else if (FALSE == pMDPPowerParams->bPowerOn)
    {
        // Power down the respective displays
        if (MDP_STATUS_OK != (eStatus = MDPPlatformConfigure(eDisplayId, MDPPLATFORM_CONFIG_POWERDOWN, NULL)))
        {
            DEBUG ((EFI_D_WARN, "MDPLib: MDPPlatformConfigure(MDPPLATFORM_CONFIG_POWERDOWN) failed!\n"));   
        }
    }

    MDP_LOG_FUNC_EXIT("MDPPower()");    

    return eStatus;
}


/****************************************************************************
*
** FUNCTION: MDPDetect()
*/
/*!
* \brief
*   This function will detect the presense of a display and supported modes.
*
* \param [in] eDisplayId       - The display to initialize
*        [in] MDPDetectParams  - Information regarding the hardware core
*        [in] uFlags           - Reserved
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPDetect(MDP_Display_IDType eDisplayId, MDP_DetectParamType *pMDPDetectParams, uint32 uFlags )
{
    MDP_Status        eStatus = MDP_STATUS_OK;

    MDP_LOG_FUNC_ENTRY("MDPDetect()", eDisplayId);    
    
    if ((NULL == pMDPDetectParams) ||
        (eDisplayId >= MDP_DISPLAY_MAX))
    {
      eStatus = MDP_STATUS_BAD_PARAM;
    }
    else
    {
        MDP_Panel_AttrType  *pDisplayInfo = &gDisplayInfo[eDisplayId];

        // Handle each display
        switch (eDisplayId)
        {
        case MDP_DISPLAY_PRIMARY:
            {
              if (MDP_STATUS_OK != MDPDetectPanel(eDisplayId, pDisplayInfo))
              {
                DEBUG ((EFI_D_WARN, "MDPLib: MDPDetectPanel() failed!\n"));
              }
              else
              {
                 pMDPDetectParams->bDisplayDetected          = pDisplayInfo->bDetected;
                 pMDPDetectParams->uSupportedModes           = 1; // Only 1 mode is supported
                 pMDPDetectParams->aModeList[0].bInterlaced  = FALSE;
                 pMDPDetectParams->aModeList[0].uModeIndex   = 0;
                 pMDPDetectParams->aModeList[0].uWidth       = pDisplayInfo->uDisplayWidth;
                 pMDPDetectParams->aModeList[0].uHeight      = pDisplayInfo->uDisplayHeight;
                 switch (pDisplayInfo->ePhysConnect)
                 {
                 case MDP_DISPLAY_CONNECT_PRIMARY_DSI_VIDEO:
                 case MDP_DISPLAY_CONNECT_PRIMARY_DSI_CMD:
                 case MDP_DISPLAY_CONNECT_SECONDARY_DSI_VIDEO:
                 case MDP_DISPLAY_CONNECT_SECONDARY_DSI_CMD:                
                    pMDPDetectParams->aModeList[0].uRefreshRate = pDisplayInfo->uAttrs.sDsi.uRefreshRate;
                    break;
                 case MDP_DISPLAY_CONNECT_DP:
                    pMDPDetectParams->aModeList[0].uRefreshRate = pDisplayInfo->uAttrs.sEdp.uRefreshRate;
                    break;
                 default:
                    break;
                 }
              }
            }
            break;

        case MDP_DISPLAY_EXTERNAL:
            {
              if (TRUE != MDPDetectHDMIPlugin(pDisplayInfo))
              {
                DEBUG ((EFI_D_INFO, "MDPLib: HDMI monitor not detected!\n"));
              }
              else
              {
                  MDP_Panel_AttrType  sHDMIMode;
                  uint32              i;

                  // HDMI detected 
                  pDisplayInfo->bDetected            = TRUE;
                  pMDPDetectParams->bDisplayDetected = TRUE;
                  pDisplayInfo->uNumInterfaces       = MDP_INTERFACE_SINGLE;

                  // Enumerate all modes
                  for (i=0;i<MDP_DISPLAY_MAX_MODES;i++)
                  {
                    if (MDP_STATUS_OK == HAL_HDMI_GetInfo(i, &sHDMIMode))
                    {
                      pMDPDetectParams->aModeList[i].uModeIndex   = i;
                      pMDPDetectParams->aModeList[i].bInterlaced  = sHDMIMode.uAttrs.sHdmi.bInterlaced;
                      pMDPDetectParams->aModeList[i].uWidth       = sHDMIMode.uDisplayWidth;
                      pMDPDetectParams->aModeList[i].uHeight      = sHDMIMode.uDisplayHeight;
                      pMDPDetectParams->aModeList[i].uRefreshRate = sHDMIMode.uAttrs.sHdmi.uRefreshRate;
                    
                      pMDPDetectParams->uSupportedModes++;
                    }
                    else
                    {
                      break;
                    }
                  }
              }
            }
            break;
        default:
            eStatus = MDP_STATUS_BAD_PARAM;
            break;
        }
        
    }

    MDP_LOG_FUNC_EXIT("MDPDetect()");    

    return eStatus;
}



/****************************************************************************
*
** FUNCTION: MDPSetMode()
*/
/*!
* \brief
*   This function setup the display for a particular mode.
*
* \param [in] eDisplayId       - The display to initialize
*        [in] MDPSetModeParams - Mode setup parameters
*        [in] uFlags           - Reserved
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPSetMode(MDP_Display_IDType eDisplayId, MDP_SetModeParamType *pMDPSetModeParams, uint32 uFlags )
{
  MDP_Status           eStatus      = MDP_STATUS_OK;
  MDP_Panel_AttrType  *pDisplayInfo = NULL;

  MDP_LOG_FUNC_ENTRY("MDPSetMode()", eDisplayId); 

  if ((NULL == pMDPSetModeParams)     ||
      (eDisplayId >= MDP_DISPLAY_MAX))
  {
    eStatus = MDP_STATUS_BAD_PARAM;
  }
  else
  {
    pDisplayInfo = &gDisplayInfo[eDisplayId];

    if (TRUE != pDisplayInfo->bDetected)
    {
      eStatus = MDP_STATUS_NOT_SUPPORTED;
    }
    else if (TRUE == pDisplayInfo->bSWRenderer)
    {
      // Cache the current mode and surface information
      pDisplayInfo->uModeId  = pMDPSetModeParams->uModeIndex;
      MDP_OSAL_MEMCPY(&pDisplayInfo->sFrameBuffer, &pMDPSetModeParams->sSurfaceInfo, sizeof(MDPSurfaceInfo));

      // Display is in software rendering mode, don't initialize hardware and continue
      eStatus = MDP_STATUS_OK;
    }
    else
    {
      // Setup MDP control paths
      if (MDP_STATUS_OK != (eStatus = MDPSetupPipe(pDisplayInfo, &pMDPSetModeParams->sSurfaceInfo)))
      {
        DEBUG ((EFI_D_WARN, "DisplayDxe: MDPSetupPipe Failed!\n"));
      }
      else if (MDP_STATUS_OK != (eStatus = MDPPanelInit(pMDPSetModeParams->uModeIndex, pDisplayInfo)))
      {
        DEBUG ((EFI_D_WARN, "DisplayDxe: MDPPanelInit Failed!\n"));
      }
      else if (MDP_STATUS_OK != (eStatus = MDPStartPipe(pDisplayInfo))) // Kick start the displays
      {
        DEBUG ((EFI_D_WARN, "DisplayDxe: MDPStartPipe Failed!\n"));
      }
      else
      {
        // Cache the current mode and surface information
        pDisplayInfo->uModeId  = pMDPSetModeParams->uModeIndex;
        MDP_OSAL_MEMCPY(&pDisplayInfo->sFrameBuffer, &pMDPSetModeParams->sSurfaceInfo, sizeof(MDPSurfaceInfo));

        // If DSI video transfer should be on during DCS transactions then send command for sending the init sequence.
        if (TRUE == pDisplayInfo->uAttrs.sDsi.bForceCmdInVideoHS)
        {
          if (MDP_STATUS_OK != MDPPanelSendCommandSequence(pDisplayInfo, MDP_PANEL_COMMAND_INIT, NULL, 0))
          {
            DEBUG ((EFI_D_WARN, "DisplayDxe: MDPPanelSendCommandSequence() failed to send INIT sequence!\n"));
          }
        }
      }
    }
  }

  MDP_LOG_FUNC_EXIT("MDPSetMode()"); 

  return eStatus;
}


/****************************************************************************
*
** FUNCTION: MDPSetProperty()
*/
/*!
* \brief
*   This function will configure a specific property of the display
*
* \param [in] eDisplayId       - The display to initialize
*        [in] eProperty        - The particular property to set
*        [in] MDPSetModeParams - Mode setup parameters
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPSetProperty(MDP_Display_IDType eDisplayId, MDP_Display_Property eProperty, MDP_PropertiesParamType *pMDPPropertiesParams)
{
  MDP_Status           eStatus      = MDP_STATUS_OK;
  MDP_Panel_AttrType  *pDisplayInfo = NULL;

  MDP_LOG_FUNC_ENTRY("MDPSetProperty()", eProperty);        
    
  if (eDisplayId >= MDP_DISPLAY_MAX)
  {
    eStatus = MDP_STATUS_BAD_PARAM;
  }
  else
  {
    pDisplayInfo = &gDisplayInfo[eDisplayId];

    switch (eProperty)
    {
    case MDP_DISPLAY_PROPERTY_BACKLIGHT:
      {
        MDPPlatformParams  sPlatformParams;
        // Setup any other display parameters
        
        MDP_OSAL_MEMZERO(&sPlatformParams, sizeof(MDPPlatformParams));

        sPlatformParams.sBacklightConfig.bEnable                        = TRUE;
        sPlatformParams.sBacklightConfig.eBacklightType                 = pDisplayInfo->eBacklightType;
        sPlatformParams.sBacklightConfig.uBacklightCntrl.eBacklightCtrl = pDisplayInfo->uBacklightCntrl.eBacklightCtrl;
        sPlatformParams.sBacklightConfig.uLevel                         = pMDPPropertiesParams->uBacklightLevel;

        if (MDP_STATUS_OK == (eStatus = MDPPlatformConfigure(eDisplayId, MDPPLATFORM_CONFIG_SETBACKLIGHT, &sPlatformParams)))
        {
          // Cache current backlight level
          pDisplayInfo->uBacklightLevel = pMDPPropertiesParams->uBacklightLevel;
        }
      }
      break;

    case MDP_DISPLAY_PROPERTY_FIRMWAREENV:
      {
        MDPPlatformParams   sPlatformParams;

        if (MDP_STATUS_OK == (eStatus = MDPPlatformConfigure(eDisplayId, MDPPLATFORM_CONFIG_GETPLATFORMINFO, &sPlatformParams)))
        {
          UINT32 FrameBufferSize = 0;
          UINT8  uHighestBankBit = 0;           //0 is defined to be error value with GFX
          CHAR8* MemLabel = "Display Reserved";
          MemRegionInfo DisplayMemRegionInfo;
          MDPFirmwareEnvType  sFirmwareEnvInfo;
          EFI_STATUS Status;
          MDP_OSAL_MEMZERO(&sFirmwareEnvInfo, sizeof(MDPFirmwareEnvType));

          // Get the Display region.
          Status = GetMemRegionInfoByName(MemLabel, &DisplayMemRegionInfo);
          if (Status != EFI_SUCCESS)
          {
            DEBUG ((EFI_D_WARN, "MDPLib: Could not find the Display memory region. Defaulting to 8MB.\n"));
            FrameBufferSize = 0x00800000; /* Default to 8MB */
          }
          else
          {
            FrameBufferSize = DisplayMemRegionInfo.MemSize;
          }
          
          if(EFI_SUCCESS != (Status = GetHighestBankBit(&uHighestBankBit)))
          {
            DEBUG ((EFI_D_WARN, "MDPLib: GetHighestBankBit failed.\n"));
          }

          // Populate firmware environment information
          sFirmwareEnvInfo.uVersionInfo                = (MDP_FIRMWARE_ENV_VERSION_MAGIC << 16) | 
                                                         (MDP_FIRMWARE_ENV_VERSION_MAJOR << 8)  |
                                                         (MDP_FIRMWARE_ENV_VERSION_MINOR);
          sFirmwareEnvInfo.uPlatformId                 = sPlatformParams.sPlatformInfo.sEFIPlatformType.platform;
          sFirmwareEnvInfo.uChipsetId                  = sPlatformParams.sPlatformInfo.sEFIChipSetId;
          sFirmwareEnvInfo.uPlatformVersion            = 0; // Reserved
          // Frame buffer information comes from the currently cached frame buffer
          sFirmwareEnvInfo.uFrameBufferAddress         = (UINTN) pDisplayInfo->sFrameBuffer.pPlane0Offset;
          // We report the framebuffersize as the carveout memory size which is sum of PcdFrameBufferSize and PcdHDMISize
          sFirmwareEnvInfo.uFrameBufferSize            = FrameBufferSize; 
          sFirmwareEnvInfo.uFrameBufferPixelDepth      = (8* pDisplayInfo->sFrameBuffer.uPlane0Stride) / pDisplayInfo->sFrameBuffer.uWidth;
          sFirmwareEnvInfo.uFrameBufferImagePxWidth    = pDisplayInfo->sFrameBuffer.uWidth;
          sFirmwareEnvInfo.uFrameBufferImagePxHeight   = pDisplayInfo->sFrameBuffer.uHeight;
          sFirmwareEnvInfo.uFrameBufferImageStride     = pDisplayInfo->sFrameBuffer.uPlane0Stride;
          sFirmwareEnvInfo.uHighestBankBit             = uHighestBankBit;

          // Populate primary panel information
          if(MDP_DISPLAY_PRIMARY == eDisplayId)
          {
            sFirmwareEnvInfo.uPrimaryPanelId             = sPlatformParams.sPlatformInfo.uPrimaryPanelId;
            sFirmwareEnvInfo.uPrimaryPanelFlags          = MDP_FIRMWARE_ENV_FLAG_DISPLAY_INITIALIZED;
            sFirmwareEnvInfo.uPrimaryPanelBacklightLevel = pDisplayInfo->uBacklightLevel;
          }

          eStatus = MDPPlatformExportEnvVariables(&sFirmwareEnvInfo);
        }
      }
      break;  

    case MDP_DISPLAY_PROPERTY_MODE_INFO:
      {
         MDP_HwPrivateInfo  *psMDPHwPrivateInfo  = &gsMDPHwPrivateInfo;
         uint32 ModeIndex =  pMDPPropertiesParams->sModeParams.uModeIndex;

         // HDMI monitor supports more than one mode , store the mode info for the selected mode
         // For primary only one mode is supported and this information is populated at the detect time
         switch (eDisplayId)
         {
            case MDP_DISPLAY_EXTERNAL:
               eStatus = HAL_HDMI_GetInfo(ModeIndex, pDisplayInfo);
            break;
            default:
            break;   
         }

         // Check if we need Dual pipe for this panel
         if ((NULL != psMDPHwPrivateInfo->pDeviceCaps) &&
             (pDisplayInfo->uDisplayWidth > psMDPHwPrivateInfo->pDeviceCaps->pResolutionCaps->uMaxLayerWidthPx))
         {
             pDisplayInfo->uNumMixers = MDP_DUALPIPE_NUM_MIXERS;
         }
      }
      break;

   case MDP_DISPLAY_PROPERTY_POWER_STATE:
     {
        /* Cache the current display power state information */
        pDisplayInfo->bDisplayPwrState  =  pMDPPropertiesParams->bDisplayPwrState;
        break;
     }


    default:
      eStatus = MDP_STATUS_BAD_PARAM;
      break;
    }
  }

  MDP_LOG_FUNC_EXIT("MDPSetProperty()");

  return eStatus;
}


/****************************************************************************
*
** FUNCTION: MDPGetProperty()
*/
/*!
* \brief
*   This function will retrieve a specific property of the display
*
* \param  [in] eDisplayId           - The display to initialize
*         [in] eProperty            - The particular property to set
*         [in] pMDPPropertiesParams - Property parameters
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPGetProperty(MDP_Display_IDType eDisplayId, MDP_Display_Property eProperty, MDP_PropertiesParamType *pMDPPropertiesParams)
{
  MDP_Status           eStatus      = MDP_STATUS_OK;
  MDP_Panel_AttrType  *pDisplayInfo = NULL;

  MDP_LOG_FUNC_ENTRY("MDPGetProperty()", eProperty);

  if (eDisplayId >= MDP_DISPLAY_MAX)
  {
    eStatus = MDP_STATUS_BAD_PARAM;
  }
  else
  {
    pDisplayInfo = &gDisplayInfo[eDisplayId];

    switch (eProperty)
    {
      case MDP_DISPLAY_PROPERTY_BACKLIGHT:
      {
        /* Retrieve the current cached backlight level */
        pMDPPropertiesParams->uBacklightLevel  =  pDisplayInfo->uBacklightLevel;
        break;
      }
    
      case MDP_DISPLAY_PROPERTY_POWER_STATE:
      {
        /* Retrieve the current cached display state information */
        pMDPPropertiesParams->bDisplayPwrState  =  pDisplayInfo->bDisplayPwrState;
        break;
      }

      case MDP_DISPLAY_PROPERTY_MODE_INFO:
      {
        /* Retrieve the current cached display mode information */
        pMDPPropertiesParams->sModeParams.uModeIndex = pDisplayInfo->uModeId;
        MDP_OSAL_MEMCPY(&pMDPPropertiesParams->sModeParams.sSurfaceInfo, &pDisplayInfo->sFrameBuffer, sizeof(MDPSurfaceInfo));
        break;
      }

      case MDP_DISPLAY_PROPERTY_DETECTION_INFO:
      {
        /* Retrieve the current cached display detection information */
        pMDPPropertiesParams->bDisplayDetected = pDisplayInfo->bDetected;      
        break;
      }

      default:
      { 
        eStatus = MDP_STATUS_BAD_PARAM;
        break;
      }
    }
  }

  MDP_LOG_FUNC_EXIT("MDPGetProperty()");

  return eStatus;
}


/****************************************************************************
*
** FUNCTION: MDPDeInit()
*/
/*!
* \brief
*   This function will de-initializes the panel interfaces
*
* \param [in]  eDisplayId       - Display to de-initialize
* \param [in]  uFlags           - Reserved
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPDeInit(MDP_Display_IDType eDisplayId, uint32 uFlags)
{
  MDP_Status           eStatus      = MDP_STATUS_OK;
  MDP_Panel_AttrType  *pDisplayInfo = NULL;

  MDP_LOG_FUNC_ENTRY("MDPDeInit()", eDisplayId);

  if (eDisplayId >= MDP_DISPLAY_MAX)
  {
    eStatus = MDP_STATUS_BAD_PARAM;
  }
  else 
  {
    pDisplayInfo = &gDisplayInfo[eDisplayId];

    if(TRUE == pDisplayInfo->bSWRenderer)
    {
      /* do nothing if display is in SW rendering mode, skip HW configuration */
    }
    else
    {

      /* If DSI video transfer should be on during DCS transactions then send command for sending the termination sequence. */
      if (TRUE == pDisplayInfo->uAttrs.sDsi.bForceCmdInVideoHS)
      {
        if (MDP_STATUS_OK != MDPPanelSendCommandSequence(pDisplayInfo, MDP_PANEL_COMMAND_TERM, NULL, 0))
        {
          DEBUG ((EFI_D_WARN, "DisplayDxe: MDPPanelSendCommandSequence() failed to send TERM sequence!\n"));
        }
      }

      /* Turn off the display timing engines */
      if (MDP_STATUS_OK != (eStatus = MDPStopPipe(pDisplayInfo)))
      {
        DEBUG ((EFI_D_WARN, "MDPLib: MDPPanelDeInit() failed! Status: %d\n", eStatus));      
        // Warn and continue
      }
    
    
      /* Turn off the display controller and panel */
      if (MDP_STATUS_OK != (eStatus = MDPPanelDeInit(pDisplayInfo)))
      {
        DEBUG ((EFI_D_WARN, "MDPLib: MDPPanelDeInit() failed! Status: %d\n", eStatus));  
        // Warn and continue      
      }
    }
  }

  MDP_LOG_FUNC_EXIT("MDPDeInit()");

  return eStatus;
}



/****************************************************************************
*
** FUNCTION: MDPTerm()
*/
/*!
* \brief
*   This function will deinitialize the MDP core and free all resources.
*
* \param [in]  uFlags           - Reserved
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPTerm(uint32 uFlags)
{
  MDP_Status eStatus = MDP_STATUS_OK;

  MDP_LOG_FUNC_ENTRY("MDPTerm()", 0x00);

  /* Turn off the core clock */
  if (MDP_STATUS_OK != (eStatus = MDPDisableClocks(MDP_CLOCKTYPE_CORE)))
  {
    DEBUG ((EFI_D_WARN, "MDPLib: MDPDisableClocks() failed!\n"));    
  }

  MDP_LOG_FUNC_EXIT("MDPTerm()");

  return eStatus;
}

/****************************************************************************
*
** FUNCTION: MDPExitBoot()
*/
/*!
* \brief
*   This function performs house cleaning before UEFI exit
*
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPExitBoot( void )
{
  if (NULL != gpDSIInitSequenceBuffer)
  {
    UncachedSafeFreePool(gpDSIInitSequenceBuffer);
    gpDSIInitSequenceBuffer = NULL;
  }

  if (NULL != gpDSITermSequenceBuffer)
  {
    UncachedSafeFreePool(gpDSITermSequenceBuffer);
    gpDSITermSequenceBuffer = NULL;
  }

  return MDP_STATUS_OK;
}

/****************************************************************************
*
** FUNCTION: MDPSetCoreClock()
*/
/*!
* \brief
*   This function will setup the MDP core clock, enable footswitch, and restore TZ of register access
*
* \param [in]  uFlags           
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPSetCoreClock(uint32 uFlags)
{
  MDP_Status eStatus = MDP_STATUS_OK;

  MDP_LOG_FUNC_ENTRY("MDPSetCoreClock()", 0x00);

  /* Turn on the core clock */
  if (MDP_STATUS_OK != (eStatus = MDPSetupClocks(MDP_CLOCKTYPE_CORE, NULL)))
  {
     DEBUG ((EFI_D_WARN, "MDPLib: MDPSetupClocks() failed!\n"));
  }

  MDP_LOG_FUNC_EXIT("MDPSetCoreClock()");

  return eStatus;
}


#ifdef MDP_ENABLE_PROFILING 
/****************************************************************************
*
** FUNCTION: MDPProfiler()
*/
/*!
* \brief
*   This function performs the profiling of functions 
*
* \param [in]  pFuncName        - Function name
* \param [in]  uParam1          - Display ID, Property or Mode info depending on the function  
* \param [in]  bEntry           - Function entry or exit. True - entry; False - exit;
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPProfiler(uint8 *pFuncName, uint32 uParam1, bool32 bEntry)
{
  MDP_Status      eStatus     =  MDP_STATUS_OK; 
  static uint32   uStartTime;
  static uint32   uTotalDriverTime;

  /* Function entry */
  if (bEntry)
  {
    /* Start time */   
    uStartTime = GetTimerCountus();
    DEBUG((EFI_D_ERROR, "MDPLibProfile: Entry[%a][%d]\n", pFuncName, uParam1));  
  }    
  else 
  {
    /* Function exit */ 
    uint32          uEndTime;
    uint32          uTotalFuncTime = 0;   

    /* End time */ 
    uEndTime = GetTimerCountus();
    /* Function execution time(ms) */ 
    uTotalFuncTime = (uEndTime - uStartTime);   
    /* Total driver execution time(ms) */
    uTotalDriverTime += uTotalFuncTime;

    DEBUG((EFI_D_ERROR, "MDPLibProfile: Exit[%a][%dus function time][%dus total time]\n", pFuncName, uTotalFuncTime, uTotalDriverTime));
  }

  return eStatus;
}
#endif


/*=========================================================================
     Local Static Variables
==========================================================================*/


/*
  Function to export platform and panel information using environment variables for
  the HLOS to use later.
*/
static MDP_Status MDPPlatformExportEnvVariables(MDPFirmwareEnvType *pFirmwareEnvInfo)
{
  MDP_Status    eStatus            = MDP_STATUS_OK;
  EFI_GUID      sOutputGUID        = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;
  const CHAR16  sPlatformEnvName[] = L"UEFIDisplayInfo";

  
  if (EFI_SUCCESS != gRT->SetVariable((CHAR16*)&sPlatformEnvName, &sOutputGUID,
                                      EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                                      sizeof(MDPFirmwareEnvType),
                                      (void*)pFirmwareEnvInfo))
  {
    eStatus = MDP_STATUS_FAILED;
  }

  return eStatus;
}


/****************************************************************************
*
** FUNCTION: MDPDetectHDMIPlugin()
*/
/*!
* \brief
*   This function will detect the presence of the HDMI monitor connected
*
* \param [in]  uFlags           - Reserved
*
* \retval MDP_Status
*
****************************************************************************/
static bool32 MDPDetectHDMIPlugin(MDP_Panel_AttrType  *pPanelInfo)
{
   bool32     bHDMIDetected = FALSE;
   MDP_Status eStatus       = MDP_STATUS_OK;

   if ( NULL == pPanelInfo)
   {
      
      DEBUG ((EFI_D_WARN, "MDPDetectHDMIPlugin: Null pointer passed\n"));  
   }
   else
   {
      if(MDP_STATUS_OK == (eStatus =  HAL_HDMI_Init()))
      {
        // Get Valid Mode information for mode at Index 0
        if (MDP_STATUS_OK == (eStatus = HAL_HDMI_GetInfo(0,pPanelInfo)))
        {
           // Perform a quick sanity check
           if ((0!=pPanelInfo->uDisplayHeight) || (0!=pPanelInfo->uDisplayWidth))
           {
              pPanelInfo->eDisplayId    = MDP_DISPLAY_EXTERNAL;
              pPanelInfo->ePhysConnect  = MDP_DISPLAY_CONNECT_HDMI;
              bHDMIDetected             = TRUE;
           }
        }
      }  
   
      // Valid mode not found, HDMI has not been detected, disable core and clocks
      if ((MDP_STATUS_OK != eStatus) || (FALSE == bHDMIDetected))
      {
         HAL_HDMI_Term();
      }
      
   }

  return bHDMIDetected;
}

/****************************************************************************
*
** FUNCTION: MDPPlatformGetDisableDisplay()
*/
/*!
* \brief
*   Returns TRUE if DISABLEDISPLAY is set and FALSE otherwise
*
*
* \retval bool32
*
****************************************************************************/
static bool32 MDPPlatformGetDisableDisplay(void)
{
  const CHAR16  sDisableDisplay[]  = L"DISABLEDISPLAY";
  UINT32        uAttributes        = 0;
  UINT8         uValue             = 0;
  UINT32        uSize              = sizeof(UINT8);
  bool32        bDisableDisp       = FALSE; 

  if (EFI_SUCCESS == gRT->GetVariable((CHAR16*)&sDisableDisplay, 
                                      &gMSFTVendorGuid,
                                      &uAttributes,
                                      (UINTN*)&uSize,
                                      (void*)&uValue))
  {
    if(1 == uValue)
    {
      // DISABLEDISPLAY is set
      bDisableDisp = TRUE;
    }
  }

  return bDisableDisp;
}

/****************************************************************************
*
** FUNCTION: MDPPlatformSetMdssBase()
*/
/*!
* \brief
*   Set MDSS base address
*
*
* \retval MDP_Status
*
****************************************************************************/
static MDP_Status MDPPlatformSetMdssBase(EFIChipInfoFamilyType  sEFIChipSetFamily)
{
  MDP_Status              Status  = MDP_STATUS_OK;
  uint8                   uCount ;

  //Search for correct chip family
  for(uCount = 0; uCount< MDSS_BASEADDRESSMAPINGS_MAX; uCount++)
  {
    if(asMDSS_BaseAddressMappings[uCount].sEFIChipSetFamily == sEFIChipSetFamily)
      break;   
  }
  //if chip family is not found take a default one , and print warning log.
  if( MDSS_BASEADDRESSMAPINGS_MAX == uCount)
  {
    DEBUG ((EFI_D_WARN, "MDPLib:HAL_MDSS_SetBaseAddress - Chipset Family 0x%x not found , taking default base address !\n", sEFIChipSetFamily));
    Status = MDP_STATUS_BAD_PARAM;
  }
  //Call Hal function to set MDSS base address   
  else if (HAL_MDSS_STATUS_SUCCESS != HAL_MDSS_SetBaseAddress(asMDSS_BaseAddressMappings[uCount].uMDSSBaseAddress))
  {
    DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDSS_SetBaseAddress() failed!\n"));	
    Status = MDP_STATUS_NOT_SUPPORTED;
  }

  return Status;
}

