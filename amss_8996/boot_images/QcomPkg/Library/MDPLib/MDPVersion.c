/*=============================================================================
 
  File: MDPLIb.c
 
  Source file for MDP functions
  
 
  Copyright (c) 2011-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/
#include "MDPLib.h"
#include "MDPLib_i.h"
#include "MDPSystem.h"

/*=========================================================================
     Default Defines
==========================================================================*/

  
              
/*=========================================================================
     Local Static Variables
==========================================================================*/



/*=========================================================================
     Local Static Functions
==========================================================================*/

/* SMP blocks reservation table
 */
static const MDP_SMPReservationType SMPReservations_MDP580[] = 
{
  {0,       HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 0 reserved for RGB0 */
  {1,       HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 1 reserved for RGB0 */
  {2,       HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 2 reserved for RGB1 */
  {3,       HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 3 reserved for RGB1 */
  {4,       HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 4 reserved for RGB2 */
  {5,       HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 5 reserved for RGB2 */
  {6,       HAL_MDP_SMP_MMB_CLIENT_RGB3_FETCH},           /* block 6 reserved for RGB3 */
  {7,       HAL_MDP_SMP_MMB_CLIENT_RGB3_FETCH}            /* block 7 reserved for RGB3 */
};

/* SMP blocks reservation table for MDP565
 */
static const MDP_SMPReservationType SMPReservations_MDP565[] = 
{
  {0,       HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 0 reserved for RGB0 */
  {1,       HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 1 reserved for RGB0 */
  {2,       HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 2 reserved for RGB1 */
  {3,       HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 3 reserved for RGB1 */
  {4,       HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 4 reserved for RGB2 */
  {5,       HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 5 reserved for RGB2 */
  {6,       HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 0 reserved for RGB0 */
  {7,       HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 0 reserved for RGB0 */
  {8,       HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 0 reserved for RGB0 */
  {9,       HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 0 reserved for RGB1 */
  {10,      HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 0 reserved for RGB1 */
  {11,      HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 0 reserved for RGB1 */
  {12,      HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 0 reserved for RGB2 */
  {13,      HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 0 reserved for RGB2 */
  {14,      HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 0 reserved for RGB2 */
  {15,      HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_Y},         /* block 0 reserved for ViG0Y*/
  {16,      HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_Y},         /* block 0 reserved for ViG1Y*/
  {17,      HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_Y},         /* block 0 reserved for ViG2Y*/
};

static const MDP_SMPReservationType SMPReservations_MDP570[] = 
{
  {0,       HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 0 reserved for RGB0 */
  {1,       HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 1 reserved for RGB0 */
  {2,       HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 2 reserved for RGB1 */
  {3,       HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 3 reserved for RGB1 */
  {4,       HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 4 reserved for RGB2 */
  {5,       HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 5 reserved for RGB2 */
  {6,       HAL_MDP_SMP_MMB_CLIENT_RGB3_FETCH},           /* block 6 reserved for RGB3 */
  {7,       HAL_MDP_SMP_MMB_CLIENT_RGB3_FETCH},           /* block 7 reserved for RGB3 */
  {8,       HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 8 reserved for RGB0 */
  {9,       HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 9 reserved for RGB0 */
  {10,      HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH},           /* block 10 reserved for RGB0 */
  {11,      HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 11 reserved for RGB1 */
  {12,      HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 12 reserved for RGB1 */
  {13,      HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH},           /* block 13 reserved for RGB1 */
  {14,      HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 14 reserved for RGB2 */
  {15,      HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 15 reserved for RGB2 */
  {16,      HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH},           /* block 16 reserved for RGB2 */
  {17,      HAL_MDP_SMP_MMB_CLIENT_RGB3_FETCH},           /* block 17 reserved for RGB3 */
  {18,      HAL_MDP_SMP_MMB_CLIENT_RGB3_FETCH},           /* block 18 reserved for RGB3 */
  {19,      HAL_MDP_SMP_MMB_CLIENT_RGB3_FETCH},           /* block 19 reserved for RGB3 */
  {20,      HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_Y},         /* block 20 reserved for HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_Y */
  {21,      HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_Y},         /* block 21 reserved for HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_Y */
  {22,      HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_Y},         /* block 22 reserved for HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_Y */
  {23,      HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_Y},         /* block 23 reserved for HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_Y */
};


/* SMP pool information for MDP 1.0.0, 1.2.0 and 1.4.0
 */
static const MDP_SMPPoolInfoType SMPPoolInfo_MDP5XX = 
{
    22,                             /* Number of SMP blocks available */
    0,                              /* Number of SMP block reservations */
    NULL                            /* SMP reservation table */
};

/* SMP pool information for MDP 1.1.0
 */
static const MDP_SMPPoolInfoType SMPPoolInfo_MDP520 = 
{
    7,                              /* Number of SMP blocks available */
    0,                              /* Number of SMP block reservations */
    NULL                            /* SMP reservation table */
};

/* SMP pool information for MDP 1.4.0
 */
static const MDP_SMPPoolInfoType SMPPoolInfo_MDP540 = 
{
    12,                             /* Number of SMP blocks available */
    0,                              /* Number of SMP block reservations */
    NULL                            /* SMP reservation table */
};

/* SMP pool information for MDP 1.3.0
 */
static const MDP_SMPPoolInfoType SMPPoolInfo_MDP580 = 
{
    44,                             /* Number of SMP blocks available */
    8,                              /* Number of SMP block reservations */
    SMPReservations_MDP580          /* SMP reservation table */
};

/* SMP pool information for MDP 1.5.0 && 1.9.0
 */
static const MDP_SMPPoolInfoType SMPPoolInfo_MDP570 = 
{
  44,                             /* Number of SMP blocks available */
  24,                             /* Number of SMP block reservations */
  SMPReservations_MDP570          /* SMP reservation table */
};


/* SMP pool information for MDP 1.6.0
 */
static const MDP_SMPPoolInfoType SMPPoolInfo_MDP510 = 
{
    8,                             /* Number of SMP blocks available */
    0,                             /* Number of SMP block reservations */
    NULL                           /* SMP reservation table */
};

/* SMP pool information for MDP 1.10.0
 */
static const MDP_SMPPoolInfoType SMPPoolInfo_MDP565 = 
{
  34,                             /* Number of SMP blocks available */
  18,                             /* Number of SMP block reservations */
  SMPReservations_MDP565          /* SMP reservation table */
};


/* SMP pool information for all MDP versions
 */
static const MDP_SMPPoolInfoType *SMPPoolInfo[] = 
{
  &SMPPoolInfo_MDP5XX,           /* MDP 1.0.0 */
  &SMPPoolInfo_MDP520,           /* MDP 1.1.0 */
  &SMPPoolInfo_MDP5XX,           /* MDP 1.2.0 */
  &SMPPoolInfo_MDP580,           /* MDP 1.3.0 */
  &SMPPoolInfo_MDP540,           /* MDP 1.4.0 */
  &SMPPoolInfo_MDP570,           /* MDP 1.5.0 */  
  &SMPPoolInfo_MDP510,           /* MDP 1.6.0 */
  NULL,                          /* MDP 1.7.0 */
  NULL,                          /* MDP 1.8.0 */
  &SMPPoolInfo_MDP570,           /* MDP 1.9.0 */
  &SMPPoolInfo_MDP565            /* MDP 1.10.0*/
};

/* MDP5XX Resolution Capabilities
 */
static const MDP_ResolutionCaps sResolutionCaps_MDP5XX =
{
    2,     /* uMinLayerWidth  */
    2,     /* uMinLayerHeight */
    2048,  /* uMaxLayerWidth  */
    2048,  /* uMaxLayerHeight */
};

/* MDP590 Resolution Capabilities
*/
static const MDP_ResolutionCaps sResolutionCaps_MDP590 =
{
    2,     /* uMinLayerWidth  */
    2,     /* uMinLayerHeight */
    2560,  /* uMaxLayerWidth  */
    2048,  /* uMaxLayerHeight */
};

/**************************************************************
* MDP5XX PP (Ping-Pong) Capabilities
*************************************************************/
static const MDP_PingPongCaps sPPCaps_MDP5XX[HAL_MDP_PINGPONG_MAX] =
{
    /* bSupported, uMaxWidth, uPPFeatureFlags */
    { FALSE,       0,         0 },   // HAL_MDP_PING_PONG_NONE
    { TRUE,        2560,      0 },   // HAL_MDP_PING_PONG_0
    { TRUE,        2560,      0 },   // HAL_MDP_PING_PONG_1
    { TRUE,        2560,      0 },   // HAL_MDP_PING_PONG_2
    { TRUE,        2560,      0 },   // HAL_MDP_PING_PONG_3
    { FALSE,       0,         0 },   // HAL_MDP_PING_PONG_4
};

/**************************************************************
* MDP565 PP (Ping-Pong) Capabilities
*************************************************************/
static const MDP_PingPongCaps sPPCaps_MDP565[HAL_MDP_PINGPONG_MAX] =
{
    /* bSupported, uMaxWidth, uPPFeatureFlags */
    { FALSE,       0,         0 },                                // HAL_MDP_PING_PONG_NONE
    { TRUE,        2560,      MDP_PINGPONG_CAP_PINGPONG_SPLIT },  // HAL_MDP_PING_PONG_0
    { TRUE,        4096,      0 },                                // HAL_MDP_PING_PONG_1
    { FALSE,       0,         0 },                                // HAL_MDP_PING_PONG_2
    { FALSE,       0,         0 },                                // HAL_MDP_PING_PONG_3
    { FALSE,       0,         0 },                                // HAL_MDP_PING_PONG_4
};

/**************************************************************
* MDP590 PP (Ping-Pong) Capabilities
*
* Note: set MDP_PINGPONG_CAP_PINGPONG_SPLIT in uPPFeatureFlags to enable pingpong-split
*************************************************************/
static const MDP_PingPongCaps sPPCaps_MDP590[HAL_MDP_PINGPONG_MAX] =
{
    /* bSupported, uMaxWidth, uPPFeatureFlags */
    { FALSE,       0,         0 },                                // HAL_MDP_PING_PONG_NONE
    { TRUE,        2560,      0 },                                // HAL_MDP_PING_PONG_0
    { TRUE,        2560,      0 },                                // HAL_MDP_PING_PONG_1
    { TRUE,        4096,      0 },                                // HAL_MDP_PING_PONG_2
    { TRUE,        4096,      0 },                                // HAL_MDP_PING_PONG_3
    { FALSE,       0,         0 },                                // HAL_MDP_PING_PONG_4
};

/* Mapping from master ping-pong to slave ping-pong used ONLY when ping-pong split is enabled. */
const HAL_MDP_PingPongId sMDP565_PPSplitSlaveMap[HAL_MDP_PINGPONG_MAX] =
{
    HAL_MDP_PINGPONG_NONE, // HAL_MDP_PINGPONG_NONE,
    HAL_MDP_PINGPONG_2,    // HAL_MDP_PINGPONG_0,
    HAL_MDP_PINGPONG_NONE, // HAL_MDP_PINGPONG_1,
    HAL_MDP_PINGPONG_NONE, // HAL_MDP_PINGPONG_2,
    HAL_MDP_PINGPONG_NONE, // HAL_MDP_PINGPONG_3,
    HAL_MDP_PINGPONG_NONE  // HAL_MDP_PINGPONG_4,
};

/* Mapping from master ping-pong to slave ping-pong used ONLY when ping-pong split is enabled. */
const HAL_MDP_PingPongId sMDP590_PPSplitSlaveMap[HAL_MDP_PINGPONG_MAX] =
{
    HAL_MDP_PINGPONG_NONE, // HAL_MDP_PINGPONG_NONE,
    HAL_MDP_PINGPONG_4,    // HAL_MDP_PINGPONG_0,
    HAL_MDP_PINGPONG_4,    // HAL_MDP_PINGPONG_1,
    HAL_MDP_PINGPONG_NONE, // HAL_MDP_PINGPONG_2,
    HAL_MDP_PINGPONG_NONE, // HAL_MDP_PINGPONG_3,
    HAL_MDP_PINGPONG_NONE  // HAL_MDP_PINGPONG_4,
};

/* Slave PingPong Buffer ID for PP split for all MDP versions
*/
static const HAL_MDP_PingPongId *PingPongSplitSlaveMapInfo[] =
{
    NULL,                          /* MDP 1.0.0 */
    NULL,                          /* MDP 1.1.0 */
    NULL,                          /* MDP 1.2.0 */
    NULL,                          /* MDP 1.3.0 */
    NULL,                          /* MDP 1.4.0 */
    NULL,                          /* MDP 1.5.0 */
    NULL,                          /* MDP 1.6.0 */
    &sMDP590_PPSplitSlaveMap[0],   /* MDP 1.7.0 */
    NULL,                          /* MDP 1.8.0 */
    NULL,                          /* MDP 1.9.0 */
    &sMDP565_PPSplitSlaveMap[0]    /* MDP 1.10.0*/
};

/* MDP5XX Device Capabilities
 */
static const MDP_DeviceCaps sDeviceCaps_MDP5XX =
{
  &sResolutionCaps_MDP5XX,
  &sPPCaps_MDP5XX[0]
};

/* MDP565 Device Capabilities
*/
static const MDP_DeviceCaps sDeviceCaps_MDP565 =
{
    &sResolutionCaps_MDP5XX,
    &sPPCaps_MDP565[0]
};

/* MDP590 Device Capabilities
*/
static const MDP_DeviceCaps sDeviceCaps_MDP590 =
{
    &sResolutionCaps_MDP590,
    &sPPCaps_MDP590[0]
};

/* MDP Device caps  for all MDP versions
 */
static const MDP_DeviceCaps *DeviceCapInfo[] =
{
  &sDeviceCaps_MDP5XX,           /* MDP 1.0.0 */
  &sDeviceCaps_MDP5XX,           /* MDP 1.1.0 */
  &sDeviceCaps_MDP5XX,           /* MDP 1.2.0 */
  &sDeviceCaps_MDP5XX,           /* MDP 1.3.0 */
  &sDeviceCaps_MDP5XX,           /* MDP 1.4.0 */
  &sDeviceCaps_MDP5XX,           /* MDP 1.5.0 */
  &sDeviceCaps_MDP5XX,           /* MDP 1.6.0 */
  &sDeviceCaps_MDP590,           /* MDP 1.7.0 */
  NULL,                          /* MDP 1.8.0 */
  &sDeviceCaps_MDP5XX,           /* MDP 1.9.0 */
  &sDeviceCaps_MDP565            /* MDP 1.10.0*/
};

/*=========================================================================
      Public APIs
==========================================================================*/

MDP_Status MDPInitHwPrivateInfo(MDP_HwPrivateInfo *pHwPrivateInfo)
{
  MDP_Status          eStatus             = MDP_STATUS_OK;
  HAL_MDP_HwInfo      sHwInfo; 

  if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_ReadVersionInfo(&(pHwPrivateInfo->sMDPVersionInfo), &sHwInfo))
  {
     eStatus = MDP_STATUS_HW_ERROR;
  }
  else
  {
    // MDP SMP pool information based on chipset
    pHwPrivateInfo->pSMPPoolInfo = SMPPoolInfo[pHwPrivateInfo->sMDPVersionInfo.uMinorVersion];
    
    // MDP Device Caps
    pHwPrivateInfo->pDeviceCaps = DeviceCapInfo[pHwPrivateInfo->sMDPVersionInfo.uMinorVersion];

    // MDP PingPong buffer split slave ID mapping table
    pHwPrivateInfo->pPPSplitSlaveMap = PingPongSplitSlaveMapInfo[pHwPrivateInfo->sMDPVersionInfo.uMinorVersion];
  }

  return eStatus;
}

