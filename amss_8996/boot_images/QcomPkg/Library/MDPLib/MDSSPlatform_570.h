#ifndef __MDSSPlatform_570_H__
#define __MDSSPlatform_570_H__
/*=============================================================================
 
  File: MDSSPlatform_570.h
 
  Internal header file for MDP library
  
 
  Copyright (c) 2011-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "MDPLib_i.h"

/*===========================================================================
                                Defines 
===========================================================================*/
static MDPExternalClockEntry            sDSI0ExtClocks[] = 
 {
   {"mdss_esc0_clk",     0, 1, 0, 0, 0, 0},  // Index 0 : primary source :XO , Secondary source : dsi pll
   {"mdss_pclk0_clk",    1, 0, 0, 0, 0, 0},  // Index 1 : Source DSI0_PLL
   {"mdss_byte0_clk",    1, 0, 0, 0, 0, 0},  // Index 2 : Source DSI0_PLL
   {"\0",                0, 0, 0, 0, 0, 0},  
 };     
static MDPExternalClockEntry            sDSI1ExtClocks[] = 
 {
   {"mdss_esc1_clk",     0, 1, 0, 0, 0, 0},  // Index 0 : primary source :XO , Secondary source : dsi pll
   {"mdss_pclk1_clk",    2, 0, 0, 0, 0, 0},  // Index 1 : Source DSI1_PLL
   {"mdss_byte1_clk",    2, 0, 0, 0, 0, 0},  // Index 2 : Source DSI1_PLL
   {"\0",                0, 0, 0, 0, 0, 0},  
 };     
 
 /*For Dual DSI Split display, DSI0/DSI1 share same PLL-- DSI0_PLL */
static MDPExternalClockEntry            sDSI1SharedSourceExtClocks[] = 
 {
   {"mdss_esc1_clk",     0, 1, 0, 0, 0, 0},  // Index 0 : primary source :XO , Secondary source : dsi pll
   {"mdss_pclk1_clk",    1, 0, 0, 0, 0, 0},  // Index 1 : Source DSI0_PLL
   {"mdss_byte1_clk",    1, 0, 0, 0, 0, 0},  // Index 2 : Source DSI0_PLL
   {"\0",                0, 0, 0, 0, 0, 0},  
 }; 

 /* MDP Clock list
  * Note: Clocks are ordered in order to ensure Ahb access is available prior to accessing the core.
  */
 static MDPClockEntry MDPClocks[] =
 {
     {"mmss_s0_axi_clk",              0, NULL},
     {"mdss_ahb_clk",                 0, NULL},
     {"mdss_vsync_clk",               0, NULL},
     {"mdss_mdp_clk",         240000000, NULL},
     {"mdss_axi_clk",                 0, NULL},
     {"\0", 0, NULL}
 };
 
 
 /* DSI0 Clock list
  */
 static MDPClockEntry DSI0Clocks[] =
 {
     {"mdss_esc0_clk",   0,         NULL},
     {"mdss_byte0_clk",  0,         NULL},
     {"mdss_pclk0_clk",  0,         NULL},
     {"\0", 0, NULL}
 };
 
 /* DSI1 Clock list
  */
 static MDPClockEntry DSI1Clocks[] =
 {
     {"mdss_esc1_clk",   0,         NULL},
     {"mdss_byte1_clk",  0,         NULL},
     {"mdss_pclk1_clk",  0,         NULL},
     {"\0", 0, NULL}
 };
 
 /* EDP Clock list
  */
 static MDPClockEntry EDPClocks[] =
 {
     {"mdss_edppixel_clk",0,        NULL},
     {"mdss_edplink_clk", 0,        NULL},
     {"mdss_edpaux_clk",  0,        NULL},
     {"\0", 0, NULL}
 
 };
 
 
 
 /* HDMI Clock List
  */
 static MDPClockEntry HDMIClocks[] =
 {
     {"mdss_hdmi_clk",     19200000, NULL},
     {"mdss_hdmi_ahb_clk", 0,        NULL},
     {"mdss_extpclk_clk",  0,        NULL},
     {"\0", 0, NULL}
 };

 
 /* MDP power domain list
  */
 static MDPPowerDomainEntry MDP570PowerDomain[] = 
 {
     {"VDD_MDSS"},
     {"\0"}
 };
 
 /* MDP resource list for MDP570
 */
 static const MDP_ResourceList sMDP570Resources =
 {
     (MDPPowerDomainEntry*)  &MDP570PowerDomain,    /* Power domain    */
     (MDPClockEntry*)        &MDPClocks,            /* MDP clocks      */
     (MDPClockEntry*)        &DSI0Clocks,           /* DSI clocks      */
     (MDPClockEntry*)        &DSI1Clocks,           /* DSI clocks      */
     (MDPClockEntry*)        &EDPClocks,            /* EDP clocks      */
     (MDPClockEntry*)        &HDMIClocks            /* HDMI clocks     */
 };

 
 /* MDP External resource list for MDP570
 */
 static MDP_ExtClockResourceList sMDP570ExtClockResources =
 {
     (MDPExternalClockEntry*)    &sDSI0ExtClocks,               /* DSI0 Ext clocks      */
     (MDPExternalClockEntry*)    &sDSI1ExtClocks,               /* DSI1 Ext clocks      */
     (MDPExternalClockEntry*)    &sDSI1SharedSourceExtClocks,   /* DSI shared clocks for dual DSI */
 };

/* Display resource list 
*/
static DisplayResourceList sDisplayMDP570Resources =
{
   (MDP_ResourceList*)            &sMDP570Resources,               /* MDP Resources           */
   (MDP_ExtClockResourceList*)    &sMDP570ExtClockResources,       /* MDP Ext Resources      */
};
#endif // __MDSSPlatform_570_H__

