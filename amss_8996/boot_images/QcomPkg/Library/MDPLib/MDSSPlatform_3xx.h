#ifndef __MDSSPlatform_3xx_H__
#define __MDSSPlatform_3xx_H__
/*=============================================================================
 
  File: MDSSPlatform_3xx.h
 
  Internal header file for MDP library
  
 
  Copyright (c) 2011-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "MDPLib_i.h"

/*===========================================================================
                                Defines 
===========================================================================*/
 /* Calculate clock configuration */
 static MDPExternalClockEntry   sDSI_2_ExtClocks[] = 
                         {
                          {"dsi_clk",        0, 0, 0, 0, 0,  0},
                          {"dsi_pclk_clk",   0, 0, 0, 0, 0,  0},
                          {"dsi_byte_clk",   0, 0, 0, 0, 0,  0},
                          {"\0",             0, 0, 0, 0, 0,  0},
                         };


 /* MDP Clock list for MDP3.x
  * Note: Clocks are ordered in order to ensure Ahb access is available prior to accessing the core.
  */
 static MDPClockEntry MDPClocks3x[] =
 {
     {"mmss_s0_axi_clk",              0, NULL},
     {"mdp_ahb_clk",                  0, NULL},
     {"mmss_mmssnoc_ahb_clk",         0, NULL},
     {"mmss_mmssnoc_axi_clk",         0, NULL},
     {"mdp_axi_clk",          200000000, NULL},
     {"mdp_dsi_clk",                  0, NULL},
     {"mdp_vsync_clk",                0, NULL},
     {"dsi_ahb_clk",                  0, NULL},
     {"\0", 0, NULL}
 };
 
 /* DSI V2 Clock list
   */
 static MDPClockEntry DSIClocksV2[] =
 {
     {"dsi_ahb_clk",     0,         NULL},
     {"dsi_clk",         0,         NULL},
     {"dsi_esc_clk",     0,         NULL},
     {"dsi_byte_clk",    0,         NULL},
     {"dsi_pclk_clk",    0,         NULL},
     {"\0", 0, NULL}
 };
 
 /* MDP resource list for MDP3.x
 */
 static const MDP_ResourceList sMDP3xResources =
 {
     NULL,                            /* Power domain    */
     (MDPClockEntry*)    &MDPClocks3x,/* MDP clocks      */
     (MDPClockEntry*)    &DSIClocksV2,/* DSI0 clocks     */
     NULL,                            /* DSI1 clocks     */
     NULL,                            /* EDP clocks      */
     NULL                             /* HDMI clocks     */
 };

/* MDP External resource list for MDP3.x
*/
static MDP_ExtClockResourceList sMDP3xExtClockResources =
{
     sDSI_2_ExtClocks,               /* DSI0 Ext clocks      */
     NULL,                           /* DSI1 Ext clocks      */
     NULL,                           /*  DSI shared clocks for dual DSI     */
 };

/* Display resource list 
*/
static DisplayResourceList sDisplayMDP3xxResources =
{
   (MDP_ResourceList*)            &sMDP3xResources,               /* MDP Resources           */
   (MDP_ExtClockResourceList*)    &sMDP3xExtClockResources,       /* MDP Ext Resources      */
};


 
#endif // __MDSSPlatform_3xx_H__

