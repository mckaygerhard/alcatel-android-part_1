#ifndef SMEM_TARGET_H
#define SMEM_TARGET_H

/*===========================================================================

                    Shared Memory Target Specific Header File


 Copyright (c) 2011-2014 by QUALCOMM Technologies Incorporated.  All Rights
 Reserved.
===========================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE


when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/24/14   bm      Update TZ_WONCE address for 8916 and remove RPM msg ram as
                   static define.
12/05/13   bm      Define macros for TX_WONCE register base addr.
10/15/13   sm      Define macros for IMEM phys/virt base addr
03/19/12   bm      Define macros for RPM mgs ram phys/virt base addr and size
03/13/12   bt      Unfeaturize spin_lock_get declaration.
01/20/12   pa      Cleaned up header files for bootloader.
01/18/12   bm      Add SMEM support for RPM processor.
12/19/11   bt      Created target specific file for 8974, based on 8960.
04/08/11   tl      Created UEFI support code for SMEM
===========================================================================*/


/*===========================================================================
                        INCLUDE FILES
===========================================================================*/

extern void*        SMEM_RPM_MSG_RAM_BASE_PHYS;
extern void*        SMEM_RPM_MSG_RAM_BASE;
extern unsigned int SMEM_RPM_MSG_RAM_SIZE;
extern void*        SMEM_TZ_WONCE_REG;
extern void*        SMEM_TARG_INFO_ADDR;

/*===========================================================================
                           MACRO DEFINITIONS
===========================================================================*/

/*-------------------------------------------------------------
   Stub out RPM MSG RAM for Boot
 -------------------------------------------------------------*/

/** Different macro names are being used in msmhwiobase.h on different
 *  procs to export RPM MSG RAM's base/size info */

/** Defines the RPM Msg RAM size */
#define SMEM_RPM_MSG_RAM_BASE_OFFSET 0

/** Defines the IMEM starting virtual address */
#define SMEM_IMEM_BASE        (0xfe802ff0) /* valid/used only for b-family */

/** Defines the IMEM starting physical address */
#define SMEM_IMEM_BASE_PHYS   (0xfe802ff0)

#define SMEM_TARG_INFO_REG_BASE_IDX 0

#define HWIO_OUTI(reg_name, index, value) *((uint32*)SMEM_TZ_WONCE_REG + index) = value /* we only write to TZ Wonce register */

#define SMEM_INVALID_ADDR   ((void*)(-1))
#endif /* SMEM_TARGET_H */
