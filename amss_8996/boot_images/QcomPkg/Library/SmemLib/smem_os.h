/**
 * @file smem_os.h
 *
 * UEFI-specific support for SMEM. This header includes the OS-specific type
 * definitions. Most of the nominally-OS-specific definitions are included in
 * smem_target.h.
 */

/*==============================================================================
     Copyright (c) 2011 - 2012 QUALCOMM Technologies Incorporated. 
     All rights reserved.
     Qualcomm Confidential and Proprietary
==============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE


when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/25/14   bm      Define XBL build environment instead of UEFI
06/19/12   hwu     Moved OS specific macros from target file to os file.
04/08/11   tl      Created UEFI support code for SMEM
===========================================================================*/

#ifndef SMEM_OS_H
#define SMEM_OS_H

/*===========================================================================
                                INCLUDE FILES
===========================================================================*/

#include "com_dtypes.h"
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>

/*===========================================================================
                              MACRO DECLARATIONS
===========================================================================*/

/** This feature allows smem_version.h to uniquely identify UEFI to ensure
 * that all SMEM clients are using the same version of the protocol. */
#define FEATURE_SMEM_XBL

#define SMEM_MEMSET_SMEM( addr, val, size )     SetMem(addr, size, val)

/* Memory Barrier for UEFI */
#define SMEM_MEMORY_BARRIER() smem_generic_memory_barrier() 

/* ERR_FATAL for UEFI */
#define ERR_FATAL(fmt, a, b, c) \
  do { \
    DebugPrint(EFI_D_ERROR, fmt, a, b, c); \
    DebugAssert(__FILE__, __LINE__, fmt); \
    for(;;) {} \
  } while(0)

#endif /* SMEM_OS_H */
