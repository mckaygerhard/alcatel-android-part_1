/**
@file CeEL_Bam.c 
@brief Crypto Engine BAM source file 
*/

/*=========================================================================== 
                     

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

 $Header: //components/rel/boot.xf/1.0/QcomPkg/Library/HashLib/HwB/CeEL_Bam.c#3 $   
 $DateTime: 2015/07/17 20:26:52 $ 
 $Author: pwbldsvc $ 

 when       who   what, where, why
 --------   ---   ---------------------------------------------------------- 
07/31/12    nk    Boot version  
04/18/12   ejt    initial file
============================================================================*/

/*===========================================================================
 
                           INCLUDE FILES

===========================================================================*/

#include "boot_comdef.h"
#include "comdef.h"
#include "CeEL.h"
#include "CeCL_Target.h"
#include "CeEL_Env.h"
#include "CeCL.h"
#include "bam.h"
#include "msmhwiobase.h"
#include "DALSysTypes.h"
#include "DALSys.h"
#include "DDIInterruptController.h"

#include "bam_drv.h"

// block and buffer size
#define MAX_DATA_BUFFER_SIZE        CECL_MAX_BAM_BLOCK_SIZE
#define CE_BLOCK_SIZE               64
#define BAM_DESCRIPTOR_LEN          0x100

// pipe number
#define TX_PIPE_NUM                 0x2
#define RX_PIPE_NUM                 0x3
#define TX_PIPE_FLG                 0x1
#define RX_PIPE_FLG                 0x2
#define CE_BLOCK_SIZE               64  

/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/

/* buffer handles */
//static DALSYSMemInfo       tx_desc_mem_handle_phys_ptr;
//static DALSYSMemInfo       rx_desc_mem_handle_phys_ptr;
static DALSYSMemHandle     bam_data_buff_ptr;
//static uint8 bam_data_buff_ptr[16384] SECTION(".bss.BOOT_DDR_UNCACHED_ZI_ZONE");
static DALSYSMemInfo       bam_data_buff_phys_ptr;
//static DALSYSMemHandle     bam_output_buff_ptr;
//static DALSYSMemInfo       bam_output_buff_phys_ptr;

/* Descriptor list for BAM */
//static void*               tx_desc_mem_handle;
//static void*               rx_desc_mem_handle;

//uint8 ALIGN(32) tx_desc_mem_handle[BAM_DESCRIPTOR_LEN] SECTION(".bss.BOOT_DDR_ZI_DATA_ZONE");
//uint8 ALIGN(32) rx_desc_mem_handle[BAM_DESCRIPTOR_LEN] SECTION(".bss.BOOT_DDR_ZI_DATA_ZONE");

static uint8 ALIGN(8) tx_desc_mem_handle_phys_ptr[BAM_DESCRIPTOR_LEN] SECTION(".bss.BOOT_DDR_UNCACHED_ZI_ZONE");
static uint8 ALIGN(8) rx_desc_mem_handle_phys_ptr[BAM_DESCRIPTOR_LEN] SECTION(".bss.BOOT_DDR_UNCACHED_ZI_ZONE");

//uint8 ALIGN(8) tx_desc_mem_handle[BAM_DESCRIPTOR_LEN] SECTION(".bss.BOOT_DDR_UNCACHED_ZI_ZONE");
//uint8 ALIGN(8) rx_desc_mem_handle[BAM_DESCRIPTOR_LEN] SECTION(".bss.BOOT_DDR_UNCACHED_ZI_ZONE");

static bam_result_type     pipe_result;
static bam_handle          handle, tx_pipe_handle, rx_pipe_handle;

/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/**
 * @brief Set up BAM polling wait loop 
 *
 * @return CE_Result_Type
 *
 * @see 
 *
 */
static CeELErrorType BamPollingWait(bam_handle PipeHandle)
{
  CeELErrorType result = CEEL_ERROR_SUCCESS;
 
  //Sanity check inputs 
  if (!PipeHandle)
  {
    return CEEL_ERROR_INVALID_PARAM;
  }

  CeElMemset(&pipe_result, 0, sizeof(bam_result_type));

  //Set up polling wait loop for BAM operation
  do 
  {
    if (BAM_SUCCESS != bam_pipe_poll(PipeHandle, &pipe_result))
    {
      return CEEL_ERROR_FAILURE;
    }

  } while (pipe_result.event == BAM_EVENT_INVALID);

  return result;
}

/**
 * @brief DeInitialize the BAM 
 *
 * @return none
 *
 * @see 
 *
 */
CeELErrorType CeElBamDeInit(void)
{
  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief Initialize the BAM 
 *
 * @return none
 *
 * @see 
 *
 */
CeELErrorType CeElBamInit(void)
{
  bam_callback_type    bam_isr, tx_pipe_isr, rx_pipe_isr;
  bam_config_type      bam_cfg;
  bam_pipe_config_type bam_tx_pipe_cfg, bam_rx_pipe_cfg;
  uint32               tx_pipe_num = TX_PIPE_NUM;
  uint32               rx_pipe_num = RX_PIPE_NUM;

  (void)bam_drv_init();

  //BAM init
  bam_cfg.bam_va         = CRYPTO0_CRYPTO_TOP_BASE + 0x4000;      // 0xFD404000
  bam_cfg.bam_pa         = CRYPTO0_CRYPTO_TOP_BASE_PHYS + 0x4000; // 0xFD404000
  bam_cfg.bam_irq        = 0;
  bam_cfg.sum_thresh     = 8192;
  bam_cfg.bam_irq_mask   = (BAM_IRQ_HRESP_ERR_EN | BAM_IRQ_ERR_EN);
  bam_cfg.bam_mti_irq_pa = 0;
  bam_cfg.options        = 0;

  //Init BAM
  handle = bam_init(&bam_cfg, &bam_isr);
  if(handle == NULL)
  {
    return CEEL_ERROR_FAILURE;
  }

  // Dal init
  DALSYS_InitMod(NULL);
#if 0  
  //TX Pipe memory region init
  if (DAL_SUCCESS != DALSYS_MemRegionAlloc(DALSYS_MEM_PROPS_UNCACHED | DALSYS_MEM_PROPS_PHYS_CONT,
                                           DALSYS_MEM_ADDR_NOT_SPECIFIED, 
                                           DALSYS_MEM_ADDR_NOT_SPECIFIED,
                                           BAM_DESCRIPTOR_LEN,
                                           &tx_desc_mem_handle,
                                           NULL))
  {
    return CEEL_ERROR_FAILURE;
  }

  if (DAL_SUCCESS != DALSYS_MemInfo(tx_desc_mem_handle, &tx_desc_mem_handle_phys_ptr))
  {
    return CEEL_ERROR_FAILURE;
  }
#endif

  //TX Pipe
  bam_tx_pipe_cfg.options      = BAM_O_DESC_DONE | BAM_O_EOT;
  bam_tx_pipe_cfg.dir          = BAM_DIR_CONSUMER;
  bam_tx_pipe_cfg.mode         = BAM_MODE_SYSTEM;
  bam_tx_pipe_cfg.desc_base_pa = (uint64)tx_desc_mem_handle_phys_ptr;
  bam_tx_pipe_cfg.desc_base_va = (uint64)tx_desc_mem_handle_phys_ptr;
  //bam_tx_pipe_cfg.desc_base_pa = tx_desc_mem_handle_phys_ptr.PhysicalAddr;
  //bam_tx_pipe_cfg.desc_base_va = tx_desc_mem_handle_phys_ptr.VirtualAddr;
  //bam_tx_pipe_cfg.desc_base_pa = (uint64)tx_desc_mem_handle;
  //bam_tx_pipe_cfg.desc_base_va = (uint64)tx_desc_mem_handle;
  bam_tx_pipe_cfg.desc_size    = 0x100;

  bam_tx_pipe_cfg.evt_thresh    = 1;
  bam_tx_pipe_cfg.peer_base_pa  = 0;
  bam_tx_pipe_cfg.peer_pipe_num = 0;
  bam_tx_pipe_cfg.data_base_pa  = 0;
  bam_tx_pipe_cfg.data_size     = 0;

  tx_pipe_handle = bam_pipe_init(handle, tx_pipe_num, &bam_tx_pipe_cfg, &tx_pipe_isr);
  if (tx_pipe_handle == 0)
  {
    return CEEL_ERROR_FAILURE;
  }

#if 0
  //RX Pipe memory region init
  if (DAL_SUCCESS != DALSYS_MemRegionAlloc(DALSYS_MEM_PROPS_UNCACHED | DALSYS_MEM_PROPS_PHYS_CONT,
                                           DALSYS_MEM_ADDR_NOT_SPECIFIED, 
                                           DALSYS_MEM_ADDR_NOT_SPECIFIED,
                                           BAM_DESCRIPTOR_LEN,
                                           &rx_desc_mem_handle,
                                           NULL))
  {
    return CEEL_ERROR_FAILURE;
  }
 
  if (DAL_SUCCESS != DALSYS_MemInfo(rx_desc_mem_handle, &rx_desc_mem_handle_phys_ptr))
  {
    return CEEL_ERROR_FAILURE;
  }
#endif

  //RX Pipe
  bam_rx_pipe_cfg.options       = BAM_O_DESC_DONE | BAM_O_EOT;
  bam_rx_pipe_cfg.dir           = BAM_DIR_PRODUCER;
  bam_rx_pipe_cfg.mode          = BAM_MODE_SYSTEM;
  bam_rx_pipe_cfg.desc_base_pa  = (uint64)rx_desc_mem_handle_phys_ptr;
  bam_rx_pipe_cfg.desc_base_va  = (uint64)rx_desc_mem_handle_phys_ptr;
  //bam_rx_pipe_cfg.desc_base_pa  = rx_desc_mem_handle_phys_ptr.PhysicalAddr;
  //bam_rx_pipe_cfg.desc_base_va  = rx_desc_mem_handle_phys_ptr.VirtualAddr;
  //bam_rx_pipe_cfg.desc_base_pa  = (uint64)rx_desc_mem_handle;
  //bam_rx_pipe_cfg.desc_base_va  = (uint64)rx_desc_mem_handle;
  bam_rx_pipe_cfg.desc_size     = 0x100;

  bam_rx_pipe_cfg.evt_thresh    = 1;
  bam_rx_pipe_cfg.peer_base_pa  = 0;
  bam_rx_pipe_cfg.peer_pipe_num = 0;
  bam_rx_pipe_cfg.data_base_pa  = 0;
  bam_rx_pipe_cfg.data_size     = 0;

  rx_pipe_handle = bam_pipe_init(handle, rx_pipe_num, &bam_rx_pipe_cfg, &rx_pipe_isr);
  if (rx_pipe_handle == 0)
  {
    return CEEL_ERROR_FAILURE;
  }

  // Create input/output data buffers 
  if (DAL_SUCCESS != DALSYS_MemRegionAlloc(DALSYS_MEM_PROPS_UNCACHED | DALSYS_MEM_PROPS_PHYS_CONT,
                                           DALSYS_MEM_ADDR_NOT_SPECIFIED,
                                           DALSYS_MEM_ADDR_NOT_SPECIFIED,
                                           MAX_DATA_BUFFER_SIZE + CE_BLOCK_SIZE, 
                                           &bam_data_buff_ptr, NULL))
  {
    return CEEL_ERROR_FAILURE;
  }
  
  if (DAL_SUCCESS != DALSYS_MemInfo(bam_data_buff_ptr, &bam_data_buff_phys_ptr))
  {
    return CEEL_ERROR_FAILURE;
  }


  //Force the pointers to be 64 byte aligned
  //bam_data_buff_phys_ptr.VirtualAddr  = ((((uint64)bam_data_buff_phys_ptr.VirtualAddr + CE_BLOCK_SIZE - 1) / CE_BLOCK_SIZE) * CE_BLOCK_SIZE);
  //bam_data_buff_phys_ptr.PhysicalAddr = ((((uint64)bam_data_buff_phys_ptr.PhysicalAddr + CE_BLOCK_SIZE - 1) / CE_BLOCK_SIZE) * CE_BLOCK_SIZE); 
  
  bam_data_buff_phys_ptr.VirtualAddr  = ((((uint64)bam_data_buff_phys_ptr.VirtualAddr + 8 - 1) / 8) * 8);
  bam_data_buff_phys_ptr.PhysicalAddr = ((((uint64)bam_data_buff_phys_ptr.PhysicalAddr + 8 - 1) / 8) * 8); 

  //bam_data_buff_phys_ptr.VirtualAddr  = ((((uint64)bam_data_buff_ptr + CE_BLOCK_SIZE - 1) / CE_BLOCK_SIZE) * CE_BLOCK_SIZE);
  //bam_data_buff_phys_ptr.PhysicalAddr = ((((uint64)bam_data_buff_ptr + CE_BLOCK_SIZE - 1) / CE_BLOCK_SIZE) * CE_BLOCK_SIZE); 
  
#if 0
  if (DAL_SUCCESS != DALSYS_MemRegionAlloc(DALSYS_MEM_PROPS_UNCACHED | DALSYS_MEM_PROPS_PHYS_CONT,
                                           DALSYS_MEM_ADDR_NOT_SPECIFIED,
                                           DALSYS_MEM_ADDR_NOT_SPECIFIED,
                                           MAX_DATA_BUFFER_SIZE + CE_BLOCK_SIZE, 
                                           &bam_output_buff_ptr, NULL))
  {
     return CEEL_ERROR_FAILURE;
  }

  if (DAL_SUCCESS != DALSYS_MemInfo(bam_output_buff_ptr, &bam_output_buff_phys_ptr))
  {
    return CEEL_ERROR_FAILURE;
  }

  //Force the pointers to be 64 byte aligned
  bam_output_buff_phys_ptr.VirtualAddr  = ((((uint64)bam_output_buff_phys_ptr.VirtualAddr + CE_BLOCK_SIZE - 1) / CE_BLOCK_SIZE) * CE_BLOCK_SIZE);
  bam_output_buff_phys_ptr.PhysicalAddr = ((((uint64)bam_output_buff_phys_ptr.PhysicalAddr + CE_BLOCK_SIZE - 1) / CE_BLOCK_SIZE) * CE_BLOCK_SIZE);
#endif
  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief  This function prepares the command buffer and then 
 *  
 * @param buff_ptr      [in]  Pointer to input buffer
 * @param buff_len      [in]  Size of the input buffer
 * @param digest_ptr    [out] Pointer to output digest buffer
 * @param digest_len    [in]  Digest length in bytes 
 *  
 * @return CE_Result_Type
 *
 * @see 
 *
 */
CeELErrorType CeELBamHashXfer(uint8 *buff_ptr, 
                              uint32 buff_len,
                              uint8 *digest_ptr,
                              uint32 digest_len,
                              boolean lastBlock,
                              uint8* ctx)
{
  CeELErrorType  result = CEEL_ERROR_SUCCESS;
  uint32         bam_ready_buffer_size = 0;
  CeCLHashAlgoCntxType  *ctx_ptr;


  //Sanity check inputs 
  if (!buff_ptr || !buff_len || !digest_ptr || !digest_len)
  {
    return CEEL_ERROR_INVALID_PARAM;
  }

  //Check data size for max allowed
  if (buff_len > MAX_DATA_BUFFER_SIZE)
  {
    return CEEL_ERROR_FAILURE;
  }

  //Set context pointer
  ctx_ptr = (CeCLHashAlgoCntxType *)ctx;

  //Convert the input length to a multiple of 64 value for V5
  bam_ready_buffer_size = buff_len;
  if (bam_ready_buffer_size % CE_BLOCK_SIZE)
  {
    bam_ready_buffer_size = buff_len + (CE_BLOCK_SIZE - (buff_len % CE_BLOCK_SIZE));
  }
  
  //Copy input data to physical buffer
  //memcpy((void*) bam_input_buff_phys_ptr.VirtualAddr, buff_ptr, buff_len);
  if (CeElMemscpy((void*) bam_data_buff_phys_ptr.VirtualAddr, MAX_DATA_BUFFER_SIZE + CE_BLOCK_SIZE, buff_ptr, buff_len)) return CEEL_ERROR_FAILURE;

  
  /* Invalidate the Cached BAM input Data buffer */
  if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, bam_data_buff_phys_ptr.VirtualAddr, buff_len))
  {
    return CEEL_ERROR_FAILURE;
  }
#if 0
  /* Invalidate the Cached BAM TX pipe descriptor buffer */
  if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, tx_desc_mem_handle_phys_ptr.VirtualAddr, BAM_DESCRIPTOR_LEN))
  //if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, (uint64)tx_desc_mem_handle, BAM_DESCRIPTOR_LEN))
  {
    return CEEL_ERROR_FAILURE;
  }
#endif        
  do 
  {
    if(ctx_ptr->auth_no_context == 1 && ctx_ptr->firstBlock == 0)
    {
      //Set up polling wait loop for write operation 
      if (CEEL_ERROR_SUCCESS != BamPollingWait(tx_pipe_handle))
      {
        result = CEEL_ERROR_FAILURE;
        break;
      }
    }

    //Disable the tx pipe IRQs
    if (BAM_SUCCESS != bam_pipe_setirqmode(tx_pipe_handle, 0, BAM_O_DESC_DONE|BAM_O_EOT))
    {
      result = CEEL_ERROR_FAILURE;
      break;
    }

    //Perform write operation data buffer -> CE using BAM
    if (BAM_SUCCESS != bam_pipe_transfer(tx_pipe_handle, bam_data_buff_phys_ptr.PhysicalAddr, bam_ready_buffer_size,
                                         (BAM_IOVEC_FLAG_INT|BAM_IOVEC_FLAG_EOT), (void *)TX_PIPE_FLG))
    {
      result = CEEL_ERROR_FAILURE;
      break;
    }
  
    //Set up polling wait loop for write operation (data buffer -> CE)
    if(ctx_ptr->auth_no_context == 0 || ctx_ptr->lastBlock == 1)
    {
      //Set up polling wait loop for write operation 
                if (CEEL_ERROR_SUCCESS != BamPollingWait(tx_pipe_handle))
    {
      result = CEEL_ERROR_FAILURE;
                        break;
    }
    }
    
    //If last block get Hash Digest
    if (lastBlock)
    {
      //For crypto V5 with BAM read digest directly from CE CRYPTO_AUTh_IVn registers 
      if (CECL_ERROR_SUCCESS != CeCLIOCtlGetHashDigest((uint32*)digest_ptr, digest_len))
      {
        result = CEEL_ERROR_FAILURE;
        break;
      }
    }
  } while (0);


  ctx_ptr->firstBlock = 0;

  return result;
}

/**
 * @brief  This function prepares the command buffer and then 
 *  
 * @param datain_ptr    [in]  Pointer to input buffer
 * @param nDataLen      [in]  Size of the input buffet
 * @param dataput_ptr   [out] Pointer to output buffer 
   
 * @return CE_Result_Type
 *
 * @see 
 *
 */
CeELErrorType CeELBamCipherXfer(uint8 *datain_ptr, 
                                uint32 nDataLen,
                                uint8 *dataout_ptr,
                                uint8 *ctx)
{
  CeELErrorType  result = CEEL_ERROR_SUCCESS;
  CeCLCipherCntxType *ctx_ptr;
  uint32         bam_ready_buffer_size_tx = 0;
  uint32         bam_ready_buffer_size_rx = 0;

  //Sanity check inputs 
  if (!ctx || !datain_ptr || !nDataLen ||!dataout_ptr)
  {
    return CEEL_ERROR_INVALID_PARAM;
  }
  
  //Check data size for max allowed
  if (nDataLen > MAX_DATA_BUFFER_SIZE)
  {
    return CEEL_ERROR_FAILURE;
  }

  //Set context pointer
  ctx_ptr = (CeCLCipherCntxType *)ctx;

  //Convert the input length to a multiple of 64 value for V5
  bam_ready_buffer_size_tx = nDataLen;
  if (bam_ready_buffer_size_tx % CE_BLOCK_SIZE)
  {
    bam_ready_buffer_size_tx = nDataLen + (CE_BLOCK_SIZE - (nDataLen % CE_BLOCK_SIZE));
  }

  bam_ready_buffer_size_rx = bam_ready_buffer_size_tx;
  if (ctx_ptr->mode == CECL_CIPHER_MODE_CCM  && ctx_ptr->dir == CECL_CIPHER_ENCRYPT)
  {
    //Add 64 then force mult of 64
    bam_ready_buffer_size_rx = nDataLen + CE_BLOCK_SIZE;
    if (bam_ready_buffer_size_rx % CE_BLOCK_SIZE)
    {
      bam_ready_buffer_size_rx = bam_ready_buffer_size_rx + (CE_BLOCK_SIZE - (bam_ready_buffer_size_rx % CE_BLOCK_SIZE));
    }    
  } 

  //Copy input data to physical buffer
  //memcpy((void*) bam_input_buff_phys_ptr.VirtualAddr, datain_ptr, nDataLen); 
  if (CeElMemscpy((void*) bam_data_buff_phys_ptr.VirtualAddr, MAX_DATA_BUFFER_SIZE + CE_BLOCK_SIZE, datain_ptr, nDataLen)) return CEEL_ERROR_FAILURE;
  
  /* Invalidate the Cached BAM input data buffer */
  if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, bam_data_buff_phys_ptr.VirtualAddr, nDataLen))
  {
    return CEEL_ERROR_FAILURE;
  }
#if 0
  /* Invalidate the Cached BAM TX pipe descriptor buffer */
  if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, tx_desc_mem_handle_phys_ptr.VirtualAddr, BAM_DESCRIPTOR_LEN))
  //if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, (uint64)tx_desc_mem_handle, BAM_DESCRIPTOR_LEN))
  {
    return CEEL_ERROR_FAILURE;
  }

  /* Invalidate the Cached BAM RX pipe descriptor buffer */
  if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, rx_desc_mem_handle_phys_ptr.VirtualAddr, BAM_DESCRIPTOR_LEN))
  //if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, (uint64)rx_desc_mem_handle, BAM_DESCRIPTOR_LEN))
  {
    return CEEL_ERROR_FAILURE;
  }
#endif
  do 
  {
    //Disable the tx pipe IRQs
    if (BAM_SUCCESS != bam_pipe_setirqmode(tx_pipe_handle, 0, BAM_O_DESC_DONE|BAM_O_EOT))
    {
      result = CEEL_ERROR_FAILURE;
      break;
    }

    //Disable the rx pipe IRQs
    if (BAM_SUCCESS != bam_pipe_setirqmode(rx_pipe_handle, 0, BAM_O_DESC_DONE|BAM_O_EOT))
    {
      result = CEEL_ERROR_FAILURE;
      break;
    }

    //Perform write operation data buffer -> CE using BAM
    if (BAM_SUCCESS != bam_pipe_transfer(tx_pipe_handle, bam_data_buff_phys_ptr.PhysicalAddr, bam_ready_buffer_size_tx,
                                         (BAM_IOVEC_FLAG_INT|BAM_IOVEC_FLAG_EOT|BAM_IOVEC_FLAG_NWD), (void *)TX_PIPE_FLG))
    {
      result = CEEL_ERROR_FAILURE;
      break;
    }

    //Perform read operation CE -> data buffer using BAM
    if (BAM_SUCCESS != bam_pipe_transfer(rx_pipe_handle, bam_data_buff_phys_ptr.PhysicalAddr, bam_ready_buffer_size_rx,
                                         (BAM_IOVEC_FLAG_INT|BAM_IOVEC_FLAG_EOT), (void *)RX_PIPE_FLG))
    {
      result = CEEL_ERROR_FAILURE;
      break;
    }
                
    //Set up tx pipe polling wait loop for write operation (data buffer -> CE)
                if (CEEL_ERROR_SUCCESS != BamPollingWait(tx_pipe_handle))
    {
      result = CEEL_ERROR_FAILURE;
                        break;
    }

    //Set up rx pipe polling wait loop for read operation (CE -> data buffer)
                if (CEEL_ERROR_SUCCESS != BamPollingWait(rx_pipe_handle))
    {
      result = CEEL_ERROR_FAILURE;
                        break;
    }

  } while(0);

  // Check MAC for error conditions
  if ((HWIO_IN(CECL_CE_STATUS)) & HWIO_CRYPTO0_CRYPTO_STATUS_MAC_FAILED_BMSK) 
  {
    /*
    ** Zero out the entire output buffer
    */
    CeElMemset(dataout_ptr, 0, nDataLen);
    CeElMemoryBarrier();
    (void)DALSYS_CacheCommand(
                              DALSYS_CACHE_CMD_FLUSH,
                              (DALSYSMemAddr)dataout_ptr,
                              nDataLen
                             );
    CeElMemoryBarrier();
    result = CEEL_ERROR_FAILURE;
  }
  else
  {
    CeElMemoryBarrier();

    /* Flush the Cached BAM output data buffer */
    if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, bam_data_buff_phys_ptr.VirtualAddr, nDataLen))
    {
      return CEEL_ERROR_FAILURE;
    }

    CeElMemoryBarrier();

    if (ctx_ptr->mode == CECL_CIPHER_MODE_CCM  && ctx_ptr->dir == CECL_CIPHER_ENCRYPT)
    {
      //Add in Mac length
      nDataLen += ctx_ptr->macLn; 
    }

    //Copy data back to caller
    //memcpy(dataout_ptr, (void*) bam_output_buff_phys_ptr.VirtualAddr, nDataLen);
    if (CeElMemscpy(dataout_ptr, nDataLen, (void*) bam_data_buff_phys_ptr.VirtualAddr, nDataLen)) return CEEL_ERROR_FAILURE;
  }

  return result;
}

/**
 * @brief  This function prepares the command buffer and then 
 *  
 * @param datain_ptr    [in]  Pointer to input buffer
 * @param nDataLen      [in]  Size of the input buffet
 * @param dataput_ptr   [out] Pointer to output buffer 
   
 * @return CE_Result_Type
 *
 * @see 
 *
 */
CeELErrorType CeELBamHashCipherXfer(uint8 *datain_ptr, 
                                uint32 nDataLen,
                                uint8 *dataout_ptr)
{
  CeELErrorType  result = CEEL_ERROR_SUCCESS;
  uint32         bam_ready_buffer_size = 0;

  //Sanity check inputs 
  if (!datain_ptr || !nDataLen ||!dataout_ptr)
  {
    return CEEL_ERROR_INVALID_PARAM;
  }
  
  //Check data size for max allowed
  if (nDataLen > MAX_DATA_BUFFER_SIZE)
  {
    return CEEL_ERROR_FAILURE;
  }

   //Convert the input length to a multiple of 64 value for V5
  bam_ready_buffer_size = nDataLen;
  if (bam_ready_buffer_size % CE_BLOCK_SIZE)
  {
    bam_ready_buffer_size = nDataLen + (CE_BLOCK_SIZE - (nDataLen % CE_BLOCK_SIZE));
  }

  //Copy input data to physical buffer
  //memcpy((void*) bam_input_buff_phys_ptr.VirtualAddr, datain_ptr, nDataLen); 
  if (CeElMemscpy((void*) bam_data_buff_phys_ptr.VirtualAddr, MAX_DATA_BUFFER_SIZE + CE_BLOCK_SIZE, datain_ptr, nDataLen)) return CEEL_ERROR_FAILURE; 

  /* Flush the Cached BAM input data buffer */
  if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, bam_data_buff_phys_ptr.VirtualAddr, nDataLen))
  {
    return CEEL_ERROR_FAILURE;
  }
#if 0
  /* Flush the Cached BAM TX pipe descriptor buffer */
  //if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, tx_desc_mem_handle_phys_ptr.VirtualAddr, BAM_DESCRIPTOR_LEN))
  if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, (uint64)tx_desc_mem_handle, BAM_DESCRIPTOR_LEN))
  {
    return CEEL_ERROR_FAILURE;
  }

  /* Flush the Cached BAM RX pipe descriptor buffer */
  //if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, rx_desc_mem_handle_phys_ptr.VirtualAddr, BAM_DESCRIPTOR_LEN))
  if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, (uint64)rx_desc_mem_handle, BAM_DESCRIPTOR_LEN))
  {
    return CEEL_ERROR_FAILURE;
  }
#endif
  do 
  {
    //Disable the tx pipe IRQs
    if (BAM_SUCCESS != bam_pipe_setirqmode(tx_pipe_handle, 0, BAM_O_DESC_DONE|BAM_O_EOT))
    {
      result = CEEL_ERROR_FAILURE;
      break;
    }

    //Disable the rx pipe IRQs
    if (BAM_SUCCESS != bam_pipe_setirqmode(rx_pipe_handle, 0, BAM_O_DESC_DONE|BAM_O_EOT))
    {
      result = CEEL_ERROR_FAILURE;
      break;
    }
 
    //Perform write operation data buffer -> CE using BAM
    if (BAM_SUCCESS != bam_pipe_transfer(tx_pipe_handle, bam_data_buff_phys_ptr.PhysicalAddr, bam_ready_buffer_size,
                                         (BAM_IOVEC_FLAG_INT|BAM_IOVEC_FLAG_EOT), (void *)TX_PIPE_FLG))
    {
      result = CEEL_ERROR_FAILURE;
      break;
    }

    //Perform read operation CE -> data buffer using BAM
    if (BAM_SUCCESS != bam_pipe_transfer(rx_pipe_handle, bam_data_buff_phys_ptr.PhysicalAddr, bam_ready_buffer_size,
                                         (BAM_IOVEC_FLAG_INT|BAM_IOVEC_FLAG_EOT), (void *)RX_PIPE_FLG))
    {
      result = CEEL_ERROR_FAILURE;
      break;
    }

    //Set up tx pipe polling wait loop for write operation (data buffer -> CE)
    result = BamPollingWait(tx_pipe_handle);
    if (CEEL_ERROR_SUCCESS != result)
    {
      break;
    }

    //Set up rx pipe polling wait loop for read operation (CE -> data buffer)
    result = BamPollingWait(rx_pipe_handle);        
    if (CEEL_ERROR_SUCCESS != result)
    {
      break;
    }
  } while(0);

  CeElMemoryBarrier();

  /* Flush the Cached BAM output data buffer */
  if (DAL_SUCCESS != DALSYS_CacheCommand(DALSYS_CACHE_CMD_FLUSH, bam_data_buff_phys_ptr.VirtualAddr, nDataLen))
  {
    return CEEL_ERROR_FAILURE;
  }

  CeElMemoryBarrier();

  //Copy data back to caller
  //memcpy(dataout_ptr, (void*) bam_output_buff_phys_ptr.VirtualAddr, nDataLen);
  if (CeElMemscpy(dataout_ptr, nDataLen, (void*) bam_data_buff_phys_ptr.VirtualAddr, nDataLen)) return CEEL_ERROR_FAILURE;

  return result;
}




