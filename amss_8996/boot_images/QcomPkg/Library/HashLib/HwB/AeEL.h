/*===========================================================================

                   ArmV8 Crypto Engine Environment API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014-2015 Copyright Qualcomm Technologies, Inc.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/04/14   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "comdef.h"
#include <stringl/stringl.h>
/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

typedef PACKED struct 
{
  void                      *pvBase;
  uint32                     dwLen;
} AEELIovecType;

typedef PACKED struct 
{
  AEELIovecType             *iov;
  uint32                     size;
} AEELIovecListType;

typedef enum
{
  AEEL_DATA_HASH                    = 0x1,
  AEEL_DATA_CIPHER                  = 0x2,
  AEEL_DATA_HASH_CIPHER             = 0x3
}AEELDataType;

typedef enum
{
  AEEL_ERROR_SUCCESS                = 0x0,
  AEEL_ERROR_FAILURE                = 0x1,
  AEEL_ERROR_INVALID_PARAM          = 0x2,
  AEEL_ERROR_NOT_SUPPORTED          = 0x3,
  AEEL_ERROR_NO_MEMORY              = 0x4
} AeELErrorType;

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/

AeELErrorType AeElMemoryBarrier(void);

/*===========================================================================
*
* FUNCTION: AeElInit()
*
* DESCRIPTION:
* This function initializes all the needed dependencies, also making sure
* if they are available. 
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
AeELErrorType AeElInit(void);

/*===========================================================================
*
* FUNCTION: AeElDenit()
*
* DESCRIPTION:
* This function removes any registrations made with the dependencies
* 
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
AeELErrorType AeElDeinit(void);

/*===========================================================================
*
* FUNCTION: AeElEnableClock()
*
* DESCRIPTION:
* This function turns on needed clocks for ce operation in the given environment
* 
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
AeELErrorType AeElEnableClock(void);

/*===========================================================================
*
* FUNCTION: AeElDisableClock()
*
* DESCRIPTION:
* This function turns off needed clocks for ce operation in the given environment
* 
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
AeELErrorType AeElDisableClock(void);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
AeELErrorType AeElmalloc(void** ptr, uint32 preLen);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
AeELErrorType AeElfree(void* ptr);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
AeELErrorType AeElMemcpy(void* dst, void* src, uint32 len);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
AeELErrorType AeElMemscpy(void* dst, uint32 dlen, void* src, uint32 slen);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
AeELErrorType AeElMemset(void* src, uint32 val, uint32 len);



/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
AeELErrorType AeElMutexEnter (void);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
AeELErrorType AeElMutexExit (void);


/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
uint32 AeELBytecmp(const uint8* s1, const uint8* s2, uint32 N);





