/*===========================================================================

                    Crypto Engine Environment API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2010-2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
07/31/12   nk      Boot version
11/02/11   sm      update for crypto5 register hash interface 
04/29/10   bm      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "comdef.h"

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/
typedef enum
{
  CEEL_XFER_MODE_REG   = 0x0,
  CEEL_XFER_MODE_BAM   = 0x1,
  CEEL_XFER_MODE_MAX   = 0x2
}CeElXferModeType;

typedef  struct 
{
  void                      *pvBase;
  uint64                     dwLen;
} CEELIovecType;

typedef  struct 
{
  CEELIovecType             *iov;
  uint64                     size;
} CEELIovecListType;

typedef enum
{
  CEEL_DATA_HASH                    = 0x1,
  CEEL_DATA_CIPHER                  = 0x2,
  CEEL_DATA_HASH_CIPHER             = 0x3
}CEELDataType;

typedef enum
{
  CEEL_ERROR_SUCCESS                = 0x0,
  CEEL_ERROR_FAILURE                = 0x1,
  CEEL_ERROR_INVALID_PARAM          = 0x2,
  CEEL_ERROR_NOT_SUPPORTED          = 0x3,
  CEEL_ERROR_NO_MEMORY              = 0x4
} CeELErrorType;

/* the function pointer representing the data transfer function available in the given
 * environment. "direction" determines if the transfer is into the crypto core or out of 
 * the crypto core. */
typedef CeELErrorType (*CeElXferFunctionType) (CEELIovecListType*, CEELIovecListType*, boolean, uint8*, CEELDataType);

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/

CeELErrorType CeElMemoryBarrier(void);

/*===========================================================================
*
* FUNCTION: CeElInit()
*
* DESCRIPTION:
* This function initializes all the needed dependencies, also making sure
* if they are available. For example, these include DataMover, ClockRegimes,
* Interrupt Handlers, Memory Map
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
CeELErrorType CeElInit(void);

/*===========================================================================
*
* FUNCTION: CeElDenit()
*
* DESCRIPTION:
* This function removes any registrations made with the dependencies
* 
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
CeELErrorType CeElDeinit(void);

/*===========================================================================
*
* FUNCTION: CeElEnableClock()
*
* DESCRIPTION:
* This function turns on needed clocks for ce operation in the given environment
* 
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
CeELErrorType CeElEnableClock(void);

/*===========================================================================
*
* FUNCTION: CeElDisableClock()
*
* DESCRIPTION:
* This function turns off needed clocks for ce operation in the given environment
* 
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
CeELErrorType CeElDisableClock(void);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElmalloc(void** ptr, uint64 preLen);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElfree(void* ptr);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElMemcpy (void* dst, void* src, uint64 len);
  
/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
* @param : dst[out] buffer pointer to which data should be copied, pointer
*                   should be provided by caller
* @param : dstLen[in] : Max length of the dst buffer
* @param : src [in] buffer pointer from where the data will be copied, pointer
*                   should be provided by caller
* @param : srcLen [in] Actual length to be copied.
*  
* DEPENDENCIES:
* dstLen >= srcLen
* =>memcpy(dst,src,srcLen);
* 
* RETURN VALUE: 
*
*
===========================================================================*/
CeELErrorType CeElMemscpy (void* dst, uint64 dstLen, void* src, uint64 srcLen);
  
/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElMemset (void* src, uint64 val, uint64 len);

/*===========================================================================
*
* FUNCTION: CeElGetXferModeList()
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE:
*
===========================================================================*/
CeELErrorType CeElGetXferModeList(CeElXferModeType* xferMode);

/*===========================================================================
*
* FUNCTION: CeElGetXferfunction()
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE:
*
===========================================================================*/
CeELErrorType CeElGetXferfunction(CeElXferModeType, CeElXferFunctionType* );

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElMutexEnter (void);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElMutexExit (void);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
* Provide dividend and divisor(power of 2, eg: if 64, input 
* will be 6 since 2^6 = 64) of which ceiling will be calculated
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/

CeELErrorType CeElCeil(uint32 total, uint32 power, uint32* ceil);

