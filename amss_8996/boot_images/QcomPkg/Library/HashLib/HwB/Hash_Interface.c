/** @file  
                    Hash_Interface.c

  The interface funstions between HashDxe and lower layer hash support lib.

  Copyright (c) 2011 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

**/

/*======================================================================

                        EDIT HISTORY FOR MODULE
 

when         who       what, where, why
--------     ---       -------------------------------------------------
01/28/13     shl       Warning fix 
10/31/12     shl       Force deinit in HashFinalSw
27/09/12     shl       Handled the failed cases 
07/20/12     spathuru  Moved the Hash Interface functions to a new file 
=======================================================================*/

#include "com_dtypes.h"
#include "HashSw.h"
#include "CeML.h"
#include "CeCL.h"
#include <Library/BaseMemoryLib.h>

typedef struct
{
  CeCLHashAlgoCntxType              ctx;
  uint8                             saved_buff[64];
  uint32                            saved_buff_index;
}CeMLHashAlgoCntxType;

CeMLCntxHandle    *cntx;
CEMLIovecListType  CEMLIovecListIn;
CEMLIovecListType  CEMLIovecListOut;
CEMLIovecType      CEMLIovecIn;
CEMLIovecType      CEMLIovecOut;
CeMLHashAlgoType   hashAlgo;
UINT32             hashSize;

/**
 * @brief This is the interface fuction of DMA hash
 *
 * @return EFI_STATUS
 *
 *
 */
EFI_STATUS HashInitSw(SW_SHA_Ctx *pCtx ) 
{
  EFI_STATUS Status = EFI_SUCCESS;

  /* Sanity check inputs */
  if ( pCtx == NULL )
  {
    return EFI_INVALID_PARAMETER;
  }

  // init context
  cntx = NULL;
  // prepare the structures
  CEMLIovecListIn.iov   = &CEMLIovecIn;
  CEMLIovecListIn.size  = 1;
  CEMLIovecListOut.iov  = &CEMLIovecOut;
  CEMLIovecListOut.size = 1;

  switch( pCtx->Algo)
  { 
    case SW_AUTH_ALG_SHA1:
      hashAlgo = CEML_HASH_ALGO_SHA1;
      hashSize = CEML_HASH_DIGEST_SIZE_SHA1;
      CEMLIovecListOut.iov[0].pvBase = pCtx->Digest;
	  CEMLIovecListOut.iov[0].dwLen = CEML_HASH_DIGEST_SIZE_SHA1;
      break;

    case SW_AUTH_ALG_SHA256:
      hashAlgo = CEML_HASH_ALGO_SHA256;
      hashSize = CEML_HASH_DIGEST_SIZE_SHA256;
      CEMLIovecListOut.iov[0].pvBase = pCtx->Digest;
	  CEMLIovecListOut.iov[0].dwLen = CEML_HASH_DIGEST_SIZE_SHA256;
      break;

    default:
      return EFI_INVALID_PARAMETER;
  }  

  if( CeMLInit() != CEML_ERROR_SUCCESS )
   {
     Status = EFI_DEVICE_ERROR;
     goto ErrorExit;
   }

  if( CeMLHashInit( &cntx, hashAlgo ) != CEML_ERROR_SUCCESS )
  {
     Status = EFI_DEVICE_ERROR;
     goto ErrorExit;
  }

  // pass the initial iv back.
  CopyMem( (void*)(pCtx->ctx.iv), (void*)(((CeMLHashAlgoCntxType *)(cntx->pClientCtxt))->ctx.auth_iv), hashSize);

  goto Exit;

ErrorExit:
   CeMLHashDeInit(&cntx);
   CeMLDeInit();

Exit:
  return Status;
}


EFI_STATUS HashUpdateSw(SW_SHA_Ctx *pCtx, UINT8* pData, UINT32 nDataLen)
{
   EFI_STATUS Status = EFI_SUCCESS;

   /* Sanity check inputs */
   if ( pCtx == NULL || nDataLen == 0)
   {
     Status = EFI_INVALID_PARAMETER;
     goto ErrorExit;
   }

   // data to hash
   CEMLIovecListIn.iov[0].pvBase = (void *)pData;
   CEMLIovecListIn.iov[0].dwLen  = nDataLen;     

  CopyMem( (void*)(((CeMLHashAlgoCntxType *)(cntx->pClientCtxt))->ctx.auth_iv), (void*)(pCtx->ctx.iv), hashSize);
  CopyMem( ((CeMLHashAlgoCntxType *)(cntx->pClientCtxt))->saved_buff, pCtx->ctx.leftover, pCtx->ctx.leftover_size );
  ((CeMLHashAlgoCntxType *)(cntx->pClientCtxt))->saved_buff_index = pCtx->ctx.leftover_size;

   if( CeMLHashUpdate( cntx, CEMLIovecListIn ) != CEML_ERROR_SUCCESS ) 
   {
     Status = EFI_DEVICE_ERROR;
     goto ErrorExit;
   }

   CopyMem( (void*)(pCtx->ctx.iv), (void*)(((CeMLHashAlgoCntxType *)(cntx->pClientCtxt))->ctx.auth_iv), hashSize);
   CopyMem( pCtx->ctx.leftover,  ((CeMLHashAlgoCntxType *)(cntx->pClientCtxt))->saved_buff, ((CeMLHashAlgoCntxType *)(cntx->pClientCtxt))->saved_buff_index );
   pCtx->ctx.leftover_size = ((CeMLHashAlgoCntxType *)(cntx->pClientCtxt))->saved_buff_index;

   goto Exit;

ErrorExit:
   CeMLHashDeInit(&cntx);
   CeMLDeInit();

Exit:
   return Status;
}


EFI_STATUS HashFinalSw(SW_SHA_Ctx *pCtx )
{
  EFI_STATUS Status = EFI_SUCCESS;

  /* Sanity check inputs */
  if ( pCtx == NULL )
  {
    Status = EFI_INVALID_PARAMETER;
    goto ErrorExit;
  }

  CopyMem( (void*)(((CeMLHashAlgoCntxType *)(cntx->pClientCtxt))->ctx.auth_iv), (void*)(pCtx->ctx.iv), hashSize);
  CopyMem( ((CeMLHashAlgoCntxType *)(cntx->pClientCtxt))->saved_buff, pCtx->ctx.leftover, pCtx->ctx.leftover_size );
  ((CeMLHashAlgoCntxType *)(cntx->pClientCtxt))->saved_buff_index = pCtx->ctx.leftover_size;

  if( CeMLHashFinal( cntx, &CEMLIovecListOut)!= CEML_ERROR_SUCCESS )
  {
    Status = EFI_DEVICE_ERROR;
    goto ErrorExit;
  }

ErrorExit:
  CeMLHashDeInit(&cntx);
  CeMLDeInit();

  return Status;
}

