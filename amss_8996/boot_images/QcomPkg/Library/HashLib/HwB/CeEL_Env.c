/**
@file CeEL_Env.c 
@brief Crypto Engine Environment Library source file 
*/

/*===========================================================================

                     Crypto Engine Environment Library 

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2010 - 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when         who     what, where, why
--------     ---     ----------------------------------------------------------
2010-05-24   bm      Initial version
============================================================================*/
#include "CeEL_Env.h"
//#include "boot_cache_mmu.h"

typedef struct 
{
  uint64 CeMLIovecTypeBuff[2]; 
  uint64 used;
} CeMLIovecType;

typedef struct 
{
  uint64 CeMLTypeBuff[40]; 
  uint64 used;
} CeMLIovecType2;

CeMLIovecType  CeMLIovecTypeArray[8];

CeMLIovecType2 CeMLHashCipherAlgoCntxTypeBuff[2];

CeEL_EnvErrorType CeElmalloc_Env(void** ptr, uint64 ptrLen)
{  
  uint32 i = 0;
  
  if (ptrLen < 16) 
  {
    for(i=0; i < 8; i++)
    {
      if (CeMLIovecTypeArray[i].used  == 1) continue;
      else
      {
        *ptr = (void*)CeMLIovecTypeArray[i].CeMLIovecTypeBuff;
  		CeMLIovecTypeArray[i].used = 1;
        break;
      }
    }
  }
  else if (ptrLen >= 16 && ptrLen < 320) 
  {
    if (CeMLHashCipherAlgoCntxTypeBuff[0].used == 0)
    {
      *ptr = (void*)CeMLHashCipherAlgoCntxTypeBuff[0].CeMLTypeBuff;
      CeMLHashCipherAlgoCntxTypeBuff[0].used = 1;
    }
    else if (CeMLHashCipherAlgoCntxTypeBuff[1].used == 0)
    {
      *ptr = (void*)CeMLHashCipherAlgoCntxTypeBuff[1].CeMLTypeBuff;
      CeMLHashCipherAlgoCntxTypeBuff[1].used = 1;
    }
    else
    {
      return CEEL_ENV_ERROR_FAILURE;
    }
  }
  else 
  {
    return CEEL_ENV_ERROR_FAILURE;
  }
  	
  if (i > 7) return CEEL_ENV_ERROR_FAILURE;
  
  return CEEL_ENV_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeEL_EnvErrorType CeElfree_Env(void* ptr)
{
  uint32 j;

  for (j=0; j < 8; j++)
  {
    if(ptr == CeMLIovecTypeArray[j].CeMLIovecTypeBuff)
    {
      CeMLIovecTypeArray[j].used = 0;
      break;
    }
    else
    {
      continue;
    }
  }

  if (ptr == CeMLHashCipherAlgoCntxTypeBuff[0].CeMLTypeBuff)
  {
    CeMLHashCipherAlgoCntxTypeBuff[0].used = 0; 
  }

  if (ptr == CeMLHashCipherAlgoCntxTypeBuff[1].CeMLTypeBuff)
  {
    CeMLHashCipherAlgoCntxTypeBuff[1].used = 0; 
  }
    
  return CEEL_ENV_ERROR_SUCCESS;
}

