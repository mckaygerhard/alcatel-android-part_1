;-----------------------------------------------
;  Macro definitions for AArch32 mode
;-----------------------------------------------
    ;PRESERVE8
    ;AREA    BOOT_A53_ASM, CODE, READONLY
    ;CODE64

;--------------------------------
;  GPR Definations
;--------------------------------
 #define _GPR0  x0
 #define _GPR1  x1
 #define _GPR2  x2
 #define _GPR3  x3
 #define _GPR4  x4
 #define _GPR5  x5
 #define _GPR6  x6
 #define _GPR7  x7
 #define _GPR8  x8
 #define _GPR9  x9
 #define _GPR10 x10
 #define _GPR11 x11
 #define _GPR12 x12
 #define _GPR13 x13
 #define _GPR14 x14
 #define _GPR15 x15
 #define _GPR16 x16
 #define _GPR17 x17
 #define _GPR18 x18
 #define _GPR19 x19
 #define _GPR20 x20
 #define _GPR21 x21
 #define _GPR22 x22
 #define _GPR23 x23
 #define _GPR24 x24
 #define _GPR25 x25
 #define _GPR26 x26
 #define _GPR27 x27
 #define _GPR28 x28
 #define _GPR29 x29
 #define _GPR30 x30

#define _QPR0   q0
 #define _QPR1  q1
 #define _QPR2  q2
 #define _QPR3  q3
 #define _QPR4  q4
 #define _QPR5  q5
 #define _QPR6  q6
 #define _QPR7  q7
 #define _QPR8  q8
 #define _QPR9  q9
 #define _QPR10 q10
 #define _QPR11 q11
 #define _QPR12 q12
 #define _QPR13 q13
 #define _QPR14 q14
 #define _QPR15 q15
 #define _QPR16 q16
 #define _QPR17 q17
 #define _QPR18 q18
 #define _QPR19 q19
 #define _QPR20 q20
 

 #define _VPR0  v0
 #define _VPR1  v1
 #define _VPR2  v2
 #define _VPR3  v3
 #define _VPR4  v4
 #define _VPR5  v5
 #define _VPR6  v6
 #define _VPR7  v7
 #define _VPR8  v8
 #define _VPR9  v9
 #define _VPR10 v10
 #define _VPR11 v11
 #define _VPR12 v12
 #define _VPR13 v13
 #define _VPR14 v14
 #define _VPR15 v15
 #define _VPR16 v16
 #define _VPR17 v17
 #define _VPR18 v18
 #define _VPR19 v19
 #define _VPR20 v20
 

    MACRO 
    _VADD $arg1, $arg2, $arg3, $prefix 
        add $arg1$prefix, $arg2$prefix, $arg3$prefix 
    MEND

    MACRO
    _VMOV $arg1, $arg2, $prefix 
        mov $arg1$prefix, $arg2$prefix
    MEND

    MACRO
    _VSMOV $arg1, $arg2, $prefix 
        mov $arg1, {$arg2$prefix}[0]
    MEND

    MACRO
    _VREV32 $arg1, $arg2, $prefix
       rev32 $arg1$prefix, $arg2$prefix
    MEND

    MACRO 
    _LD1_PI16 $arg1, $arg2, $arg3
        ld1 {$arg1}, [$arg2], $arg3
    MEND

    MACRO 
   _ST1_PI16 $arg1, $arg2, $arg3, $prefix
        st1 {$arg1$prefix}, [$arg2], $arg3
    MEND

    MACRO 
    _bgt $arg1
       B.GT $arg1
    MEND

    MACRO 
     _BOF
      stp x19, x20, [sp], #-16
      stp x21, x22, [sp], #-16
      stp x23, x24, [sp], #-16
      stp x25, x26, [sp], #-16
      stp x27, x28, [sp], #-16
      str x29, [sp], #-8
    MEND

    MACRO 
    _EOF
      ldr x29, [sp], #8
      ldp x27, x28, [sp], #16
      ldp x25, x26, [sp], #16
      ldp x23, x24, [sp], #16
      ldp x21, x22, [sp], #16
      ldp x19, x20, [sp], #16
      ret
    MEND
   
;-----------------------------------------------
; SHA-256 Macros
;----------------------------------------------

    MACRO
    _SHA256SU0 $arg1, $arg2, $prefix
      sha256su0 $arg1$prefix, $arg2$prefix
    MEND

    MACRO
    _SHA256SU1 $arg1, $arg2, $arg3, $prefix
      sha256su1 $arg1$prefix, $arg2$prefix, $arg3$prefix
    MEND

    MACRO 
    _SHA256H $arg1, $arg2, $arg3, $prefix
      sha256h $arg1, $arg2, $arg3$prefix
    MEND

    MACRO 
    _SHA256H2 $arg1, $arg2, $arg3, $prefix
      sha256h2 $arg1, $arg2, $arg3$prefix
    MEND

;-----------------------------------------------
; SHA-1 Macros
;-----------------------------------------------
    MACRO
    _SHA1SU0 $arg1, $arg2, $arg3, $prefix
      sha1su0 $arg1$prefix, $arg2$prefix, $arg3$prefix
    MEND

    MACRO 
    _SHA1SU1 $arg1, $arg2, $prefix
      sha1su1 $arg1$prefix, $arg2$prefix
    MEND

    MACRO
    _SHA1C $arg1, $arg2, $arg3, $prefix
      sha1c $arg1, $arg2, $arg3$prefix 
    MEND

    MACRO
    _SHA1H $arg1, $arg2
      sha1h $arg1, $arg2
    MEND

    MACRO
    _SHA1M $arg1, $arg2, $arg3, $prefix
      sha1m $arg1, $arg2, $arg3$prefix
    MEND

    MACRO
    _SHA1P $arg1, $arg2, $arg3, $prefix
      sha1p $arg1, $arg2, $arg3$prefix
    MEND
