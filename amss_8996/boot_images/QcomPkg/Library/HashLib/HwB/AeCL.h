/*===========================================================================

                    ArmV8 Crypto Engine Core API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014-2015 Copyright Qualcomm Technologies, Inc.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE
 

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/04/14   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "comdef.h"

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

typedef enum
{
  AECL_ERROR_SUCCESS                = 0x0,
  AECL_ERROR_FAILURE                = 0x1,
  AECL_ERROR_INVALID_PARAM          = 0x2,
  AECL_ERROR_NOT_SUPPORTED          = 0x3
} AeCLErrorType;

typedef enum
{
  AECL_IOCTL_HASH_VERSION_NUM       = 0x1,
  AECL_IOCTL_SET_HASH_CNTXT         = 0x2,
  AECL_IOCTL_GET_HASH_CNTXT         = 0x3,
  AECL_IOCTL_HASH_XFER              = 0x4
} AeCLIoCtlHashType;

typedef enum
{
  AECL_IOCTL_CIPHER_VERSION_NUM     = 0x1,
  AECL_IOCTL_SET_CIPHER_CNTXT       = 0x2,
  AECL_IOCTL_GET_CIPHER_CNTXT       = 0x3,
  AECL_IOCTL_CIPHER_XFER            = 0x4
} AeCLIoCtlCipherType;

typedef enum
{
  AECL_HASH_ALGO_SHA1               = 0x1,
  AECL_HASH_ALGO_SHA256             = 0x2
} AeCLHashAlgoType;

typedef enum 
{
  AECL_CIPHER_MODE_ECB              = 0x0,
  AECL_CIPHER_MODE_CBC              = 0x1,
  AECL_CIPHER_MODE_CTR              = 0x2,
  AECL_CIPHER_MODE_XTS              = 0x3,
  AECL_CIPHER_MODE_CCM              = 0x4,
  AECL_CIPHER_MODE_CMAC             = 0x5,
  AECL_CIPHER_MODE_CTS              = 0x6
}AeCLCipherModeType;

typedef enum 
{
  AECL_CIPHER_ENCRYPT               = 0x00, 
  AECL_CIPHER_DECRYPT               = 0x01 
} AeCLCipherDir;

typedef enum 
{
  AECL_CIPHER_ALG_AES128            = 0x0,
  AECL_CIPHER_ALG_AES256            = 0x1
}AeCLCipherAlgType;

//enable clock
void ClockEnable_NeonAdSIMD(void);

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/

/*===========================================================================
*
* FUNCTION: AeClClockEnable()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
AeCLErrorType AeClClockEnable(void);

/*===========================================================================
*
* FUNCTION: AeClClockDisable()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
AeCLErrorType AeClClockDisable(void);
/*===========================================================================
*
* FUNCTION: AeCLReverseBytes()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
AeCLErrorType AeCLReverseBytes(uint8*, uint8*, uint32 byte_count);


/*===========================================================================
*
* FUNCTION: AeClInit()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
AeCLErrorType AeClInit(void);

/*===========================================================================
*
* FUNCTION: AeClDeinit()
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
AeCLErrorType AeClDeinit(void);


