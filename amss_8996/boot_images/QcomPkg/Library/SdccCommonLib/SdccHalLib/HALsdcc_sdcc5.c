/** @file HALsdcc_sdcc5.c

  SDCC(Secure Digital Card Controller) driver HAL Platform
  Specific API.
  This file implements the SDCC driver HAL components that are
  common to all implementations of the SDCC5 core

  Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential

**/

/*=============================================================================
                              EDIT HISTORY


when        who     what, where, why
----------  ---     ----------------------------------------------------------- 
2015-02-12  dg      Runtime fixes
2014-06-18  bn      Initial version. Branched from 8994 UEFI

=============================================================================*/

#include "HALhwio.h"
#include "SdccHal.h"
#include "HALsdcc_sdcc5.h"
#include "SdccBsp.h"

#define SDCC_REG_SLOT_1         0
#define SDCC_REG_SLOT_2         1
#define SDCC_REG_SLOT_3         2
#define SDCC_REG_SLOT_4         3

/*
 * Initilize the HWIO address table for access to hwio regs using the
 * drive number as the index:
 */
UINTN sdcc_reg_base[HAL_SDCC_CONTROLLER_COUNT];
UINTN sdcc_reg_hc[HAL_SDCC_CONTROLLER_COUNT];

static boolean hwio_init_done   = FALSE;

/* SDCC base address definitions
 *
 * These variables should initialized with proper base addresses
 * before making any HWIO calls
 *
 */
/* Peripheral Subsystem Base Address */
uint32 nPeriphSSBase;

/* Peripheral Subsystem Base Physical Address */
uint32 nPeriphSSBasePhys;

/* TLMM Base Address */
uint32 nPeriphTlmmBase;

/* TLMM Base Physical Address */
uint32 nPeriphTlmmBasePhys;

/* API to initialize the HWIO base address */
uint32 HAL_sdcc_SetBaseAddress(HAL_sdcc_BaseAddressType *pBaseAddr)
{
  uint32 status = HAL_SDCC_API_STATUS_FAIL;

  /*
   * Assign register base
   */
  if (pBaseAddr != NULL)
  {
    nPeriphSSBase = pBaseAddr->periphSSBase;
    nPeriphSSBasePhys = pBaseAddr->periphSSBasePhys;
    nPeriphTlmmBase = pBaseAddr->periphTlmmBase;
    nPeriphTlmmBasePhys = pBaseAddr->periphTlmmBasePhys;

    status = HAL_SDCC_API_STATUS_SUCCESS;
  }

  return status;
}

void
HAL_sdcc_SetSdCmd (const uint32 nController,
                   const struct HAL_sdcc_SdCmd * hal_cmd)
{
  uint32 command = 0;

  if (!hal_cmd)
  {
    return;
  }

  /* set the command */
  command = hal_cmd->cmd_index;

  /* Construct the command with options */
  if (hal_cmd->enable == TRUE)
  {
    command |= MCI_CMD_ENABLED;
  }

  /* CPSM waits for a response. */
  if (hal_cmd->response == TRUE)
  {
    command |= MCI_CMD_RESPONSE;
  }

  /*If True, receives a 136-bit long response.   */
  if (hal_cmd->longrsp == TRUE)
  {
    command |= MCI_CMD_LONG_RESP;
  }

  /* PROG_DONE status bit will be asserted when busy is
   * de-asserted */
  if (hal_cmd->prog_ena == TRUE)
  {
    command |= MCI_CMD_PROG_ENABLED;
  }

  /* indicates that this is a Command with Data */
  if (hal_cmd->dat_cmd == TRUE)
  {
    command |= MCI_CMD_DAT_CMD;
  }

  HWIO_SDC_REG_OUT (MCI_ARGUMENT, (UINTN)sdcc_reg_base[nController], hal_cmd->cmd_arg);

  HWIO_SDC_REG_OUT (MCI_CMD, (UINTN)sdcc_reg_base[nController], command);
  HAL_sdcc_Mclk_Reg_Wait(nController); 
}

void
HAL_sdcc_DataControl (const uint32 nController,
                             const struct HAL_sdcc_DataControl * dctl)
{
  uint32 HAL_data_ctrl = 0;

  HAL_data_ctrl = (dctl->block_size) << HWIO_MCI_DATA_CTL_BLOCKSIZE_SHFT;

  /* Enable DM if requested */
  if (dctl->dm_enable)
  {
    HAL_data_ctrl |= MCI_DATA_DMA_ENABLED;
  }

  /*
   * Set Read / Write bit
   * 0 : controller to card - Write
   * 1 : card to controller - Read
   */
  if (dctl->direction == HAL_SDCC_DIR_READ)
  {
    HAL_data_ctrl |= MCI_DATA_READ;
  }

  /*
   * Set Data transfer mode:
   * 0 : block data transfer
   * 1 : stream data transfer
   */
  if (dctl->mode != HAL_SDCC_DCTL_BLK_DATA_XFER)
  {
    HAL_data_ctrl |= MCI_DATA_STREAM_MODE;
  }

  /* Enable Data transfer if enabled. */
  if (dctl->data_xfer_enable)
  {
    HAL_data_ctrl |= MCI_DATA_DPSM_ENABLED;
  }

  /* Enable AUTO_PROG_DONE if enabled */
  /* If set (1), automatic detection of PROG_DONE condition is executed */
  /* without sending CMD12, CMD13, CMD52 or any other "dummy" command. */
  if (dctl->auto_prog_done_enable)
  {
    HAL_data_ctrl |= MCI_DATA_AUTO_PROG_DONE_ENABLED;
  }

  /* start the DPSM */
  HWIO_SDC_REG_OUT (MCI_DATA_CTL, (UINTN)sdcc_reg_base[nController], HAL_data_ctrl);
  HAL_sdcc_Mclk_Reg_Wait(nController); 
}

void
HAL_sdcc_FifoBlockRead (const uint32 nController, uint32 *pDataPt)
{
   void * fifoAddr = NULL;

   fifoAddr = (void *) ((UINTN)sdcc_reg_base[nController] + HWIO_SDC_MCI_FIFO_REG_IDX);

  *pDataPt++ = *(uint32 *)fifoAddr;
  *pDataPt++ = *(uint32 *)fifoAddr;
  *pDataPt++ = *(uint32 *)fifoAddr;
  *pDataPt++ = *(uint32 *)fifoAddr;
  *pDataPt++ = *(uint32 *)fifoAddr;
  *pDataPt++ = *(uint32 *)fifoAddr;
  *pDataPt++ = *(uint32 *)fifoAddr;
  *pDataPt++ = *(uint32 *)fifoAddr;
}

void
HAL_sdcc_FifoBlockWrite (const uint32 nController, uint32 *pDataPt)
{
  void * fifoAddr = NULL;
  uint32 *pFifo;

  fifoAddr = (void *) ((UINTN)sdcc_reg_base[nController] + HWIO_SDC_MCI_FIFO_REG_IDX);
  pFifo = (uint32 *)fifoAddr;

  /* NOTE: Can not use memcpy for now because UEFI's implementation
   * of memcpy may not do DWORD write */
  *pFifo = *pDataPt++;
  *pFifo = *pDataPt++;
  *pFifo = *pDataPt++;
  *pFifo = *pDataPt++;

  *pFifo = *pDataPt++;
  *pFifo = *pDataPt++;
  *pFifo = *pDataPt++;
  *pFifo = *pDataPt++;
}

void
HAL_sdcc_InitializeRegisters()
{
  sdcc_base_addr_type sdcc_base_address;

  if (hwio_init_done == TRUE)
  {
     return;
  }
  /* Get the base addresses from the target */
  sdcc_bsp_get_base_addr (&sdcc_base_address);

  /* Start to initialize hwio registers for slot1 */
  sdcc_reg_base[SDCC_REG_SLOT_1] = sdcc_base_address.sdc1_base;
  sdcc_reg_hc[SDCC_REG_SLOT_1]   = sdcc_base_address.sdc1_base_hc;

  /* Start to initialize hwio registers for slot2 */
  sdcc_reg_base[SDCC_REG_SLOT_2] = sdcc_base_address.sdc2_base;
  sdcc_reg_hc[SDCC_REG_SLOT_2]   = sdcc_base_address.sdc2_base_hc;

  /* Start to initialize hwio registers for slot3 */
  sdcc_reg_base[SDCC_REG_SLOT_3] = sdcc_base_address.sdc3_base;
  sdcc_reg_hc[SDCC_REG_SLOT_3]   = sdcc_base_address.sdc3_base_hc;

  /* Start to initialize hwio registers for slot4 */
  sdcc_reg_base[SDCC_REG_SLOT_4] = sdcc_base_address.sdc4_base;
  sdcc_reg_hc[SDCC_REG_SLOT_4]   = sdcc_base_address.sdc4_base_hc;

  hwio_init_done = TRUE;
}

void
HAL_sdcc_SetBusWidth (const uint32 driveno, enum HAL_sdcc_BusWidth width)
{
  uint32 reg_width = 0;

  switch (width)
  {
    case HAL_SDCC_BUS_WIDTH_1_BIT:
      /* 1 bit interface */
      reg_width = MCI_CLK_SET_1BIT_WIDE;
      break;

    case HAL_SDCC_BUS_WIDTH_4_BIT:
      /* 4 bit interface */
      reg_width = MCI_CLK_SET_4BIT_WIDE;
      break;

    case HAL_SDCC_BUS_WIDTH_8_BIT:
      /* 8 bit interface */
      reg_width = MCI_CLK_SET_8BIT_WIDE;
      break;

    default:
      //HAL_MSG_1(HAL_MSG_ERROR, "Bus Width Not Supported", widebus);
      return;
  }

  /* program the bus width */
  HWIO_SDC_REG_OUTMI (MCI_CLK, (UINTN)sdcc_reg_base[driveno], 
                      HWIO_FMSK (MCI_CLK, WIDEBUS), reg_width);
  
  HAL_sdcc_Mclk_Reg_Wait(driveno);
}

/* Remember that just because the controller supports 8-bit does NOT
 * mean the board layout is actually wired for 8bit on all slots. 7K
 * Surf, for example, allows 8-bit use only on Slot 3. */
boolean
HAL_sdcc_Supports8BitBus (const uint32 driveno)
{
  return TRUE; /* Good to go. */
}

void HAL_sdcc_Mclk_Reg_Wait (const uint32 nController)
{
  uint32 cnt = 1000;
  uint32 reg;
  
  while (cnt--)
  {
    reg = HWIO_SDC_REG_IN(MCI_STATUS2, (UINTN)sdcc_reg_base[nController]);
    if ((reg & HWIO_MCI_STATUS2_MCLK_REG_WR_ACTIVE_BMSK) == 0)
    {
      return; 
    }
  }
}

/**
 * Overwrite SDHCi Capabilties 0 Register (supported from Aragorn v2)
 * In SDHCi mode, certain registers may be hard coded at design time
 * The vendor_capabilities registers allows S/W to overwrite the default values
 *
 * @param nControllerId
 *   SD controller number, as described in \ref nControllerId .
 * 
 * @param capabilities
 *   The capabilities 0 to overwrite - see SDHCi spec
 *
 * @param value
 *   New capability value
 */
void
HAL_sdcc_OverwriteCaps0 (const uint32 nController, 
                         HAL_capabilties_type Capabilities, uint32 nValue)
{
  return;
}

/**
 * Overwrite SDHCi Capabilties 1 Register (supported from Aragorn v2)
 * In SDHCi mode, certain registers may be hard coded at design time
 * The vendor_capabilities registers allows S/W to overwrite the default values
 *
 * @param nControllerId
 *   SD controller number, as described in \ref nControllerId .
 * 
 * @param capabilities
 *   The capabilities 1 to overwrite - see SDHCi spec
 *
 * @param value
 *   New capability value
 */
void
HAL_sdcc_OverwriteCaps1 (const uint32 nController, 
                         HAL_capabilties_type Capabilities, uint32 nValue)
{
  return;
}

