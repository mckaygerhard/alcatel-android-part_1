#ifndef __TSENS_BSP_H__
#define __TSENS_BSP_H__
/*============================================================================
  @file TsensBsp.h

  Tsens BSP file.

                Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/
#define TSENS_THRESHOLD_DISABLED 0x7fffffff
#define TSENS_MAX_NUM_CHANNELS_PER_CONTROLLER 16
#define TSENS_MAX_NUM_TBCB_CLIENTS_PER_CONTROLLER 16

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "DALStdDef.h"

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/
typedef enum
{
   TSENS_BSP_SENSOR_CAL_NORMAL = 0,        /* Normal method, i.e. use QFPROM if
                                            * avail else use default char data */
   TSENS_BSP_SENSOR_CAL_IGNORE_DEVICE_CAL  /* Force using default char data */
} TsensBspSensorCalType;

typedef struct
{
   DALBOOL bPSHoldResetEn;     /* Whether to enable PS_HOLD reset */
   uint32 uPeriodActive;       /* Active period */
   uint32 uPeriodSleep;        /* Sleep period */
   DALBOOL bAutoAdjustPeriod;  /* Whether to adjust period on sleep */
   uint32 uTSControl;          /* Analog TS control setting */
   uint32 uTSConfig;           /* Analog TS config setting */
   DALBOOL bStandAlone;        /* Whether this is a stand alone controller */
   uint32 uSidebandEnMask;     /* Mask of sideband sensors to enable */
   DALBOOL bPWMEn;             /* Whether to enable PWM */
   uint8 *pucTsensSROTPhys;    /* SROT region physical base address */
   uint32 uTsensSROTSize;      /* Size of SROT region */
   uint8 *pucTsensTMPhys;      /* TM region physical base address */
   uint32 uTsensTMSize;        /* Size of TM region */
} TsensControllerCfgType;

typedef struct
{
   uint32 uController;                /* Controller index */
   uint32 uChannel;                   /* Channel index */
   uint32 uSensorID;                  /* Sensor ID for TBCB */
   uint32 uFuseIdx;                   /* Which fuse corresponds to this sensor */
   TsensBspSensorCalType eCal;        /* Which cal type to use */
   int32 nCalPoint1CodeDefault;       /* Default TSENS code at calibration point nCalPoint1DeciDegC */
   int32 nCalPoint2CodeDefault;       /* Default TSENS code at calibration point nCalPoint2DeciDegC */
   int32 nThresholdMinDeciDegC;       /* Minimum temperature threshold for critical shutdown
                                       * or use TSENS_THRESHOLD_DISABLED to disable */
   int32 nThresholdLowerDeciDegC;     /* Default lower threshold for software interrupt or
                                       * or use TSENS_THRESHOLD_DISABLED to disable */
   int32 nThresholdUpperDeciDegC;     /* Default upper threshold for software interrupt or
                                       * or use TSENS_THRESHOLD_DISABLED to disable */
   int32 nThresholdCriticalDeciDegC;  /* Default critical threshold for software interrupt or
                                       * use TSENS_THRESHOLD_DISABLED to disable */
   int32 nThresholdMaxDeciDegC;       /* Maximum temperature threshold for critical shutdown
                                       * or use TSENS_THRESHOLD_DISABLED to disable */
} TsensSensorCfgType;

typedef struct
{
   const TsensControllerCfgType *paControllerCfgs;  /* Array of controller configs */
   uint32 uNumControllers;                          /* Number of controllers */
   const TsensSensorCfgType *paSensorCfgs;          /* Array of sensor configs */
   uint32 uNumSensors;                              /* Number of sensors */
   uint32 uSensorConvTime_us;                       /* Sensor conversion time in us */
   int32 nCalPoint1DeciDegC;                        /* Calibration point 1 in deci deg C */
   int32 nCalPoint2DeciDegC;                        /* Calibration point 2 in deci deg C */
   uint32 uShift;                                   /* Shift value */
} TsensBspType;

/*-------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ----------------------------------------------------------------------*/

#endif /* #ifndef __TSENS_BSP_H__ */

