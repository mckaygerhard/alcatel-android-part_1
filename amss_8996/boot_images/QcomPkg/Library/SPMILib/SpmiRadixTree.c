/**
 * @file:  SpmiRadixTree.c
 * 
 * Copyright (c) 2013-2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $Change: 8022311 $ 
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 1/20/15  Multiple bus support
 * 11/3/14  Automatic channel assignment
 * 10/1/13  Initial Version
 */

#include <string.h>
#include "SpmiMaster.h"
#include "SpmiBusCfg.h"
#include "SpmiHal.h"

//******************************************************************************
// Local Helper Functions
//******************************************************************************

static uint32 SpmiMaster_CreateMappingTableEntry(uint8 bid,
                                                 uint32 uBitIndex,
                                                 uint32 uBranchResult0Flag,
                                                 uint32 uBranchResult0,
                                                 uint32 uBranchResult1Flag,
                                                 uint32 uBranchResult1)
{
    return ((uBitIndex          << SWIO_SPMI_MAPPING_TABLE_REGk_BIT_INDEX_SHFT( bid ))            & SWIO_SPMI_MAPPING_TABLE_REGk_BIT_INDEX_BMSK( bid )) |
           ((uBranchResult0Flag << SWIO_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_FLAG_SHFT( bid )) & SWIO_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_FLAG_BMSK( bid )) |
           ((uBranchResult0     << SWIO_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_SHFT( bid ))      & SWIO_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_BMSK( bid )) |
           ((uBranchResult1Flag << SWIO_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_FLAG_SHFT( bid )) & SWIO_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_FLAG_BMSK( bid )) |
           ((uBranchResult1     << SWIO_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_SHFT( bid ))      & SWIO_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_BMSK( bid ));
}

static void SpmiMaster_UpdateMappingImageEntry(uint8 bid,
                                               uint8 ucEntryIndex,
                                               uint8 ucBitIndex,
                                               uint8 ucBitValue,
                                               boolean bIsResult,
                                               uint8 ucTarget)
{
    uint32 uFlag;
    uint32 uNewEntryValue;

    enum {
        RESULT = 0, BRANCH = 1
    };

    uFlag = (TRUE == bIsResult) ? RESULT : BRANCH;
    
    uNewEntryValue = SPMI_HWIO_IN( SWIO_SPMI_MAPPING_TABLE_REGk_ADDR( bid, ucEntryIndex ) );

    if (1 == ucBitValue) {
        uNewEntryValue |= SpmiMaster_CreateMappingTableEntry( bid,
                                                              ucBitIndex,
                                                              0,
                                                              0,
                                                              uFlag,
                                                              ucTarget );
    } else /* 0 == bitValue */ {
        uNewEntryValue |= SpmiMaster_CreateMappingTableEntry( bid,
                                                              ucBitIndex,
                                                              uFlag,
                                                              ucTarget,
                                                              0,
                                                              0 );
    }
    
    SPMI_HWIO_OUT( SWIO_SPMI_MAPPING_TABLE_REGk_ADDR( bid, ucEntryIndex ), uNewEntryValue );
}

static uint8 SpmiMaster_CreateMappingImage(uint8 bid,
                                           SpmiBusCfg_ChannelCfg* entries,
                                           uint32 numEntries,
                                           uint8 ucEntryIndex,
                                           uint8 ucMsbIndex)
{
    uint8 ucOnes = 0;
    uint16 usMsbCompareMask = (1 << ucMsbIndex);
    uint8 ucNewMsbIndex = ucMsbIndex - 1;
    uint8 ucNextEntryIndex = 0;

    /* Count number of values with a '1' at ucMsbIndex  */
    while ((ucOnes < numEntries) && 
           (((entries[ucOnes].slaveId << 8) | entries[ucOnes].periphId) & usMsbCompareMask)) 
    {
        ++ucOnes;
    }

    /* All relevant values match at MSB, continue to next ucMsbIndex */
    if ((ucOnes == numEntries) || (0 == ucOnes)) {
        return SpmiMaster_CreateMappingImage( bid,
                                              entries,
                                              numEntries,
                                              ucEntryIndex,
                                              ucNewMsbIndex );
    }

    /* Split is possible, first handle all values with '1' at ucMsbIndex */
    if (1 == ucOnes) { /* RESULT, target APID must be at (sortedPairs[0].apid) */
        SpmiMaster_UpdateMappingImageEntry( bid,
                                            ucEntryIndex,
                                            ucMsbIndex,
                                            1,
                                            TRUE,
                                            entries[0].channel );

        /* When deploying the subtree for '0' continue from ucEntryIndex+1 */
        ucNextEntryIndex = ucEntryIndex + 1;
    } else { /* BRANCH, target table entry is (ucEntryIndex+1) */
        SpmiMaster_UpdateMappingImageEntry( bid,
                                            ucEntryIndex,
                                            ucMsbIndex,
                                            1,
                                            FALSE,
                                            ucEntryIndex + 1 );

        /* Continue recursively with all values with '1' at ucMsbIndex */
        ucNextEntryIndex = SpmiMaster_CreateMappingImage( bid,
                                                          entries,
                                                          ucOnes,
                                                          ucEntryIndex + 1,
                                                          ucNewMsbIndex );
    }

    /* Now handle all values with '0' at ucMsbIndex */
    if (ucOnes == numEntries - 1) { /* RESULT, target APID must be at (sortedPairs[uEntries-1].apid) */
        SpmiMaster_UpdateMappingImageEntry( bid,
                                            ucEntryIndex,
                                            ucMsbIndex,
                                            0,
                                            TRUE,
                                            entries[numEntries - 1].channel );
    } else {
        /* BRANCH to the entry after the subtree for '1' */
        SpmiMaster_UpdateMappingImageEntry( bid,
                                            ucEntryIndex,
                                            ucMsbIndex,
                                            0,
                                            FALSE,
                                            ucNextEntryIndex );

        /* Continue recursively with all values with '0' at ucMsbIndex */
        ucNextEntryIndex = SpmiMaster_CreateMappingImage( bid,
                                                          (&entries[ucOnes]),
                                                          (numEntries - ucOnes),
                                                          ucNextEntryIndex,
                                                          ucNewMsbIndex );
    }

    /* Return the entry after the last one which was configured */
    return ucNextEntryIndex;
}

//******************************************************************************
// Public API Functions
//******************************************************************************

void SpmiMaster_LoadRadixTree(uint8 bid, SpmiBusCfg_ChannelCfg* entries, uint32 numEntries)
{
    if (1 == numEntries)
    {
        /* Configure the first entry to the APID result */
        SpmiMaster_UpdateMappingImageEntry( bid,
                                            0,
                                            0,
                                            entries[0].periphId & 0x1,
                                            TRUE,
                                            entries[0].channel );
    } 
    else if (2 <= numEntries) {
        /* Configure recursively for any real mapping (2 entries or more) */
        SpmiMaster_CreateMappingImage( bid, entries, numEntries, 0, 11 );
    }
}
