/**
 * @file:  SpmiOs.c
 * @brief: Implements platform specific functionality
 * 
 * Copyright (c) 2013-2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $Change: 8022311 $
 */
#include <Uefi.h>
#include <Library/PcdLib.h>
#include <Library/UefiLib.h>
#include <Library/QcomBaseLib.h>

#include "SpmiOs.h"
#include "SpmiBus_Os.h"
#include "SpmiLogs.h"
#include "SpmiCfgInternal.h"
#include "PmicArb.h"

static UINTN gpuPmicArbSpmiVirtualAddress[SWIO_MAX_BUSES_SUPPORTED] = {0};

/**
 * Static Function Declarations and Definitions
 */
extern UINTN EFIAPI MicroSecondDelay (IN  UINTN MicroSeconds);

static uint16 dbgChannels[] = {SWIO_MAX_CHANNELS_SUPPORTED - 1};
//******************************************************************************
// Required Functionality
//******************************************************************************

boolean SpmiOs_HandleTransactionError(Spmi_Result* rslt, PmicArbCmd cmd, uint8 busId, 
                                      uint8 slaveId, uint16 address, uint8 tries)
{
    boolean rtn = FALSE;

    if(SpmiBusCfg_InDynamicChannelMode( busId ) && (*rslt == SPMI_FAILURE_CHANNEL_NOT_FOUND))
    {
        Spmi_Result cfgRslt;
        SpmiBusCfg_ChannelCfg chanCfg;
        uint8 periph = REG_ADDR_TO_PERIPH_ID( address );
        
        chanCfg.slaveId = slaveId;
        chanCfg.periphId = periph;

        if((cfgRslt = SpmiBusCfg_ConfigureChannel( busId, &chanCfg, TRUE )) != SPMI_SUCCESS) {
            *rslt = cfgRslt;
        }
        else if((cfgRslt = PmicArb_AddCacheEntry( busId, chanCfg.channel, slaveId, periph )) != SPMI_SUCCESS) {
            *rslt = cfgRslt;
        }
        else {
            rtn = TRUE;
        }
    }

    SPMI_LOG_VERBOSE( "OS Error %d (%s)", *rslt, rtn ? "retry" : "done" );
    return rtn;
}

//******************************************************************************
// Public API Functions
//******************************************************************************

SpmiOs_ReservedChannels* SpmiOs_GetReservedChannels(uint8 busId)
{
    uint32 debugChannelEn = 0;
    static SpmiOs_ReservedChannels debugChannels = {
    /* .numReserved = */ sizeof(dbgChannels) / sizeof(dbgChannels[0]),
    /* .reserved =    */ dbgChannels
    };
	
    debugChannelEn = PcdGet32(SPMIDebugChannel);
    if(debugChannelEn) {
        return &debugChannels;
    }
    
    return NULL;
}

/**
 * Gets the owner number of this EE
 */
Spmi_Result SpmiOs_GetOwnerId(uint8* owner)
{
    *owner = PcdGet32(SPMIOwner) & 0xFF;

    return SPMI_SUCCESS;
}

/**
 * Gets the base address of the PMIC Arbiter on this EE (PMIC_ARB_BASE)
 */
Spmi_Result SpmiOs_GetPmicArbBaseAddr(uint8 busId, void** addr)
{
    uintptr_t addrInt;
    
    if (gpuPmicArbSpmiVirtualAddress[busId] == 0)
    {
        SpmiBus_OsGetCoreBaseAddress(busId, &addrInt);
    }
    else
    {
        addrInt = gpuPmicArbSpmiVirtualAddress[busId];
    }

    *addr = (void *) addrInt;
    
    return SPMI_SUCCESS;
}

/**
 * Wait for 'us' microseconds
 */
void SpmiOs_Wait(uint32 us)
{
    MicroSecondDelay (us);
}

//******************************************************************************
// Optional / Debug Functionality
//******************************************************************************

/**
 * Sets the pointer 'buf' to a buffer of size 'size'
 */
Spmi_Result SpmiOs_Malloc(uint32 size, void** buf)
{
    return SPMI_FAILURE_FUNCTIONALITY_NOT_SUPPORTED;
}

/**
 * Frees the buf previously allocated by SpmiOs_Malloc
 */
Spmi_Result SpmiOs_Free(void* buf)
{
    return SPMI_FAILURE_FUNCTIONALITY_NOT_SUPPORTED;
}

/**
 * Registers the given ISR to service all SPMI interrupts
 */
Spmi_Result SpmiOs_RegisterISR(SpmiOs_IsrPtr isr, uint8 busId)
{
    return SPMI_FAILURE_FUNCTIONALITY_NOT_SUPPORTED;
}

/**
 * Return some relative time value
 */
uint64 SpmiOs_GetTimeTick(void)
{
    return (uint64)GetTimerCountus();
}

/**
 * Return the state of the SPMI serial clock
 */
SpmiOs_ClkStatus SpmiOs_GetSerialClkState(uint8 busId)
{
    return SPMI_CLK_STATUS_NOT_SUPPORTED;
}

/**
 * Return the state of the AHB clock
 */
SpmiOs_ClkStatus SpmiOs_GetAhbClkState(uint8 busId)
{
    return SPMI_CLK_STATUS_NOT_SUPPORTED;
}

/**
 * This API will only be called from SPMIRuntimeLib, so we should just return
 * the Physical Address. Note that the call will usually occur in boottime
 */
Spmi_Result SpmiBus_OsGetCoreBaseAddress(
        uint32 uCoreIndex,
        UINTN *puAddress)
{
    if (puAddress == NULL)
    {
        return SPMI_FAILURE_INVALID_PARAMETER;
    }

    switch(uCoreIndex)
    {
        case 0:
            *puAddress = PcdGet32(SPMIBasePhysical0);
            break;
            
        case 1:
            *puAddress = PcdGet32(SPMIBasePhysical1);
            break;
            
        default:
            return SPMI_FAILURE_INVALID_BUS_ID;
    }
    
    return SPMI_BUS_SUCCESS;
}

/**
 * This API will only be called from SPMIRuntimeLib, so we should just set
 * whatever address is sent by the caller. This will always occur in Runtime
 */
Spmi_Result SpmiBus_OsSetCoreBaseAddress(
        uint32 uCoreIndex,
        UINTN uAddress)
{
    /* Use case is only defined in runtime */
    if(uCoreIndex < SWIO_MAX_BUSES_SUPPORTED) {    
        gpuPmicArbSpmiVirtualAddress[uCoreIndex] = uAddress;
        return SPMI_BUS_SUCCESS;
    }
    
    return SPMI_FAILURE_INVALID_BUS_ID;
}

/**
 * This API will return SPMI core memory map size. 
 * This API is called from SPMIRuntimeLib.
 * 
 */
Spmi_Result SpmiBus_OsGetCoreMemoryMapSize(
        uint32 uCoreIndex,
        uint32 *memMapSize)
{
    if (memMapSize == NULL)
    {
        return SPMI_FAILURE_INVALID_PARAMETER;
    } 

    switch(uCoreIndex)
    {
        case 0:
        case 1:
            *memMapSize = PcdGet32(SPMIMemoryMapSize);
            break;
                        
        default:
            return SPMI_FAILURE_INVALID_BUS_ID;
    }
    
    return SPMI_BUS_SUCCESS;
}

