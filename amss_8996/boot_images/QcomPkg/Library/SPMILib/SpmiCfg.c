/**
 * @file:  SpmiCfg.c
 * 
 * Copyright (c) 2013-2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $Change: 8312172 $ 
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 1/20/15  Multiple bus support
 * 11/3/14  Automatic channel assignment
 * 9/2/14   Enabled security features when channel info is set
 * 10/1/13  Initial Version
*/

#include "SpmiCfg.h"
#include "SpmiBusCfg.h"
#include "SpmiSwio.h"

//******************************************************************************
// Macros / Definitions / Constants
//******************************************************************************



//******************************************************************************
// Local Helper Functions
//******************************************************************************



Spmi_Result SpmiCfg_SetDynamicChannelMode(boolean enabled)
{
    uint32 i;

    for(i = 0; i < SWIO_MAX_BUSES_SUPPORTED; i++) 
    {
       SpmiBusCfg_SetDynamicChannelMode(i, enabled);
    }
        
    return SPMI_SUCCESS;
}

