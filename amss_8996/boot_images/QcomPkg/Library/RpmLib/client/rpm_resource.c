/*===========================================================================

  Copyright (c) 2012-2014 QUALCOMM Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#ifdef _MSC_VER
    // enable memory leak detection under msvc
    #define _CRTDBG_MAP_ALLOC
    #include <stdlib.h>
    #include <crtdbg.h>
#endif

#ifdef TARGET_UEFI
    #include <Library/BaseMemoryLib.h>
    #include <Library/DebugLib.h>
    #define abort() ASSERT(FALSE)
    //CopyMem is overlap-safe in UEFI:
    #define memmove(dst, src, size) CopyMem(dst, src, size);
#endif
#include "DALSys.h"
#include "CoreVerify.h"
#include "CoreHeap.h"

#include "inmap.h"
#include "bytearray.h"
#include "rpm_resource.h"
#include "ULogFront.h"
#include "kvp.h"

extern ULogHandle rpm_log;

void rpm_resource_init(rpm_resource_t *self, rpm_resource_type type, unsigned id)
{
    memset(self, 0, sizeof(*self));
    self->resource_identifier = (((uint64_t)type) << 32) | id;
}

static rpm_key_data_t *rpm_resource_find_key(rpm_resource_t *self, unsigned key)
{
    unsigned low = 0, high = self->num_keys - 1;

    if(!self->num_keys)
        return NULL;

    while(high < self->num_keys && high >= low)
    {
        unsigned mid = (low + high) / 2;
        unsigned current_key = self->keys[mid].key;

        if(current_key < key)
            low = mid + 1;
        else if(current_key > key)
            high = mid - 1;
        else
            return &self->keys[mid];
    }

    return NULL;
}

static rpm_key_data_t *rpm_resource_add_key(rpm_resource_t *self, unsigned key)
{
    unsigned i, old_num_keys;

    old_num_keys = self->num_keys;
    self->num_keys++;
    self->keys = Core_Realloc(self->keys, self->num_keys * sizeof(rpm_key_data_t));
    CORE_VERIFY_PTR(self->keys);

    for(i = 0; i < old_num_keys; ++i)
    {
        rpm_key_data_t *key_data = &self->keys[i];

        CORE_VERIFY_PTR(key_data);
        if(key_data->key > key)
        {
            // Found entries that belong after us.  Move them and insert here.
            memmove(&self->keys[i+1], key_data, (old_num_keys - i) * sizeof(rpm_key_data_t));
            memset(key_data, 0, sizeof(rpm_key_data_t));
            key_data->key = key;
            return key_data;
        }
    }

    // Must be largest in the list; insert at the end.
    memset(&self->keys[i], 0, sizeof(rpm_key_data_t));
    self->keys[i].key = key;
    return &self->keys[i];
}
                
static rpm_request_t *rpm_key_get_local_request(rpm_key_data_t *key_data, rpm_set_type set)
{
    switch(set)
    {
        case RPM_ACTIVE_SET:
            return &key_data->active_local;

        case RPM_SLEEP_SET:
            return &key_data->sleep_local;

        case RPM_NEXT_ACTIVE_SET:
            return &key_data->nas_local;

        default:
            abort();
    }
    return NULL;
}

static rpm_request_t *rpm_key_get_cached_request(rpm_key_data_t *key_data, rpm_set_type set)
{
    switch(set)
    {
        case RPM_ACTIVE_SET:
            return &key_data->active_at_rpm;

        case RPM_SLEEP_SET:
            return &key_data->sleep_at_rpm;

        case RPM_NEXT_ACTIVE_SET:
            return &key_data->active_at_rpm;

        default:
            abort();
    }
    return NULL;
}

static void rpm_full_invalidate(rpm_set_type set, rpm_resource_t *self)
{
    unsigned i;
    //set known keys to not dirty and not valid. We are invalidating everything about this set
    for(i = 0; i < self->num_keys; ++i)
    {
        rpm_key_data_t *key_data = &self->keys[i];
        rpm_request_t *local = rpm_key_get_local_request(key_data, set);
        rpm_request_t *rpm   = rpm_key_get_cached_request(key_data, set);

        if ( local && rpm ) //KW fix
        {
            if(!local->data)
                local->data = bytearray_create(0);

            rpm->valid = false;
            local->valid = false;
            key_data->dirtiness_flags &= ~(1 << set);

            bytearray_clear(local->data);
        }
    }

    //set the num_active_dirty for this set to 1, so we know we have to send
    switch(set)
        {
            case RPM_ACTIVE_SET:
                self->num_active_dirty = 1;
                break;

            case RPM_SLEEP_SET:
                self->num_sleep_dirty = 1;
                break;

            case RPM_NEXT_ACTIVE_SET:
                self->num_nas_dirty = 1;
                break;

            default:
                abort();
        }
}


static void rpm_key_invalidate(rpm_key_data_t *key_data, rpm_set_type set)
{
    rpm_request_t *local = rpm_key_get_local_request(key_data, set);
    rpm_request_t *rpm   = rpm_key_get_cached_request(key_data, set);

    if ( local && rpm ) //KW fix
    {
        if(!local->data)
            local->data = bytearray_create(0);

        // Here we flag the resource as invalid, but dirty bit true.  This means
        // that this key has been invalidated since the last time we synchronized
        // with the RPM.
        //
        // Four states possible here:
        //   - valid-clean   -> invalid-dirty
        //   - valid-dirty   -> invalid-dirty
        //   - invalid-dirty -> invalid-dirty
        //
        //   - invalid-clean -> invalid-clean
        if(rpm->valid)
            key_data->dirtiness_flags |= (1 << set);
        local->valid = false;
        bytearray_clear(local->data);
    }
}

static void rpm_key_update(rpm_key_data_t *key_data, rpm_set_type set, unsigned length, void *data)
{
    unsigned dirty_bit = (1 << set), local_size;
    rpm_request_t *local_value = rpm_key_get_local_request(key_data, set);
    rpm_request_t *rpm_value = rpm_key_get_cached_request(key_data, set);
    void *local_data, *rpm_data;

    if ( local_value && rpm_value ) //KW fix
    {
        if(!local_value->data)
            local_value->data = bytearray_create(0);
        if(!rpm_value->data)
            rpm_value->data = bytearray_create(0);

        bytearray_clear(local_value->data);
        bytearray_put(local_value->data, data, length);

        // Four states possible here:
        //   - valid-clean   -> valid, compare for clean
        //   - valid-dirty   -> valid, compare for clean
        //   - invalid-dirty -> valid, compare for clean
        //   - invalid-clean -> valid, always dirty [RPM had no request prior]

        // Handle the invalid-clean case.
        if(!rpm_value->valid)
        {
            local_value->valid = 1;
            key_data->dirtiness_flags |= dirty_bit;
        }
        else
        {
            local_value->valid = 1;

            // Compare local and rpm values to determine dirty status.
            local_data = bytearray_at(local_value->data, 0);
            local_size = bytearray_size(local_value->data);
            rpm_data   = bytearray_at(rpm_value->data, 0);
            if(local_data && rpm_data) //KW fix
            {
                if((local_size != bytearray_size(rpm_value->data)) ||
                   memcmp(local_data, rpm_data, local_size))
                {
                    key_data->dirtiness_flags |= dirty_bit;
                }
                else
                {
                    key_data->dirtiness_flags &= ~dirty_bit;
                }
            } //KW fix
        }
    } //KW fix
}

static void rpm_resource_dirtiness_update(rpm_resource_t *self, unsigned pre_dirty, unsigned post_dirty)
{
    unsigned flipped = pre_dirty ^ post_dirty;
    
    if(ACTIVE_SET_DIRTY & flipped)
        self->num_active_dirty += (ACTIVE_SET_DIRTY & post_dirty) ? 1 : -1;

    if(SLEEP_SET_DIRTY & flipped)
        self->num_sleep_dirty += (SLEEP_SET_DIRTY & post_dirty) ? 1 : -1;

    if(NEXT_ACTIVE_SET_DIRTY & flipped)
        self->num_nas_dirty += (NEXT_ACTIVE_SET_DIRTY & post_dirty) ? 1 : -1;
}

unsigned rpm_resource_get_request_size(rpm_set_type set, rpm_resource_t *self)
{
    unsigned num_bytes = 0, dirty_bit = (1 << set), i;
    for (i = 0; i < self->num_keys; i++)
    {
        if(dirty_bit & self->keys[i].dirtiness_flags)
        {
            rpm_key_data_t *key_data = &self->keys[i];
            switch(set)
            {
                case RPM_ACTIVE_SET:
                    num_bytes += (8 + bytearray_size(key_data->active_local.data));
                    break;

                case RPM_SLEEP_SET:
                    num_bytes += (8 + bytearray_size(key_data->sleep_local.data));
                    break;

                case RPM_NEXT_ACTIVE_SET:
                    num_bytes += (8 + bytearray_size(key_data->nas_local.data));
                    break;

                default:
                    abort();

            }
        }
    }

    if(num_bytes > 0)
    {
        num_bytes += 28;
    }

    return num_bytes; //add the size of the RPM message Header
}

void rpm_resource_update(rpm_resource_t *self, rpm_set_type set, kvp_t *request_data)
{
    unsigned key, length;
    void *data;

    do
    {
        // Check for the invalidate case.
        if(!request_data)
        {
            rpm_full_invalidate(set, self); 
            break;
        }

        // Handle each key one at a time.
        while(kvp_get(request_data, &key, &length, (const char **)&data))
        {
            unsigned pre_dirty, post_dirty;

            /* Don't allow individual KVP's that are too long. */
            CORE_LOG_VERIFY( 
                length < MAX_KVP_LENGTH,
                ULOG_RT_PRINTF_3(rpm_log, "rpm_resource_update (resource: 0x%08x) (data:0x%08x) (length: 0x%08x) KVP too large", self, request_data, length)
            );
                        
            rpm_key_data_t *key_data = rpm_resource_find_key(self, key);
            if(!key_data)
            {
                key_data = rpm_resource_preallocate(self, key, length);
            }

            pre_dirty = key_data->dirtiness_flags;
            if(!length)
                rpm_key_invalidate(key_data, set);
            else
                rpm_key_update(key_data, set, length, data);
            post_dirty = key_data->dirtiness_flags;

            rpm_resource_dirtiness_update(self, pre_dirty, post_dirty);
        }

    } while(false);
}

unsigned rpm_resource_get_dirtiness(rpm_resource_t *self)
{
    unsigned dirtiness = 0;
    if(self->num_active_dirty) dirtiness |= ACTIVE_SET_DIRTY;
    if(self->num_sleep_dirty)  dirtiness |= SLEEP_SET_DIRTY;
    if(self->num_nas_dirty)    dirtiness |= NEXT_ACTIVE_SET_DIRTY;
    return dirtiness;
}

void rpm_key_compose_data(rpm_key_data_t *key_data, rpm_set_type set, kvp_t *message_data)
{
    rpm_request_t *local_value = rpm_key_get_local_request(key_data, set);
    rpm_request_t *rpm_value   = rpm_key_get_cached_request(key_data, set);

    if ( local_value && rpm_value ) //KW fix
    {
        if(!local_value->data)
            local_value->data = bytearray_create(0);
        if(!rpm_value->data)
            rpm_value->data = bytearray_create(0);

        if(0 == ((1 << set) && key_data->dirtiness_flags))
            return;

        if(!local_value->valid)
        {
            kvp_put(message_data, key_data->key, 0, 0);

            ULOG_RT_PRINTF_1(rpm_log, "\trpm_invalidated_key (key: 0x%08x)", key_data->key);
        }
        else
        {
            unsigned length = bytearray_size(local_value->data);
            kvp_put(message_data, key_data->key, length, bytearray_at(local_value->data, 0));

            ULOG_RT_PRINTF_2(rpm_log, "\trpm_dirty_key (key: 0x%08x) (length: 0x%08x)", key_data->key, length);

        }

        rpm_value->valid = local_value->valid;
        bytearray_clear(rpm_value->data);
        if(local_value->valid)
        {
            bytearray_put(rpm_value->data,
                          bytearray_at(local_value->data, 0),
                          bytearray_size(local_value->data));
        }

        key_data->dirtiness_flags &= ~(1 << set);
    }
}

bool rpm_resource_compose_message(rpm_resource_t *self, rpm_set_type set, kvp_t *message_data)
{
    unsigned valid_keys = 0, dirty_bit = (1 << set), i;

    kvp_clear(message_data);

    // If we're dirty, compose the message from each key's data
    if(dirty_bit & rpm_resource_get_dirtiness(self))
    {
        for(i = 0; i < self->num_keys; ++i)
        {
            rpm_key_data_t *key_data = &self->keys[i];
            rpm_request_t *local_value = rpm_key_get_local_request(key_data, set);
            rpm_request_t *rpm_value   = rpm_key_get_cached_request(key_data, set);
            if ( local_value && rpm_value) //KW fix
            {
                if((!(dirty_bit & key_data->dirtiness_flags) && rpm_value->valid) || local_value->valid)
                {
                    valid_keys++;
                }
            }

            // If this key is dirty, grab its data.
            if(dirty_bit & key_data->dirtiness_flags)
                rpm_key_compose_data(key_data, set, message_data);
        }

        // Wrote out all the dirty keys, so "clean up."
        switch(set)
        {
            case RPM_ACTIVE_SET:
                self->num_active_dirty = 0;
                break;

            case RPM_SLEEP_SET:
                self->num_sleep_dirty = 0;
                break;

            case RPM_NEXT_ACTIVE_SET:
                self->num_nas_dirty = 0;
                break;

            default:
                abort();
        }

        if(!valid_keys)
        {
            ULOG_RT_PRINTF_0(rpm_log, "\trpm_fully_invalidate");
            return true;
        }
    }

    return false;
}

#define RESERVE_BUFFER(x) \
    do { \
        if(!key_data->x.data) \
        { \
            key_data->x.data = bytearray_create(expected_length); \
        } \
        else \
        { \
            bytearray_reserve(key_data->x.data, expected_length); \
        } \
    } while(0);

rpm_key_data_t *rpm_resource_preallocate(rpm_resource_t *self, unsigned key, unsigned expected_length)
{
    rpm_key_data_t *key_data = rpm_resource_find_key(self, key);
    if(!key_data)
        key_data = rpm_resource_add_key(self, key);

    // Preallocate all of the possible buffers for this key.
    RESERVE_BUFFER(active_at_rpm);
    RESERVE_BUFFER(active_local);
    RESERVE_BUFFER(sleep_at_rpm);
    RESERVE_BUFFER(sleep_local);
    RESERVE_BUFFER(nas_local);

    return key_data;
}

