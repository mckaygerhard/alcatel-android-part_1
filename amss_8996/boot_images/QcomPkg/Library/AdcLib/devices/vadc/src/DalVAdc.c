/*============================================================================
  FILE:         DalVAdc.c

  OVERVIEW:     Implementation of a Physical ADC device DAL for the
                PMIC VADC peripheral.

  DEPENDENCIES: None

                Copyright (c) 2009-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Library/AdcLib/devices/vadc/src/DalVAdc.c#7 $$DateTime: 2015/06/19 18:23:22 $$Author: pwbldsvc $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-01-12  jjo  Updated scaling methods.
  2014-01-27  jjo  Temperature correction update.
  2013-11-21  jjo  Enable NPA latency request.
  2013-07-17  jjo  Support for multiple VADCs.
  2013-05-22  jjo  Remove work loop.
  2013-02-26  jjo  Added TM APIs.
  2013-02-19  jjo  Added inverse scaling functions.
  2012-10-16  jjo  Updated SPMI APIs.
  2012-10-02  jjo  Added MPP support.
  2012-07-31  jjo  Now checks the ADC type
  2012-07-09  jjo  ADC conversions are now performed entirely in a workloop.
  2012-06-11  jjo  VADC support.
  2012-04-20  jdt  Support for VADC peripheral.
  2012-03-05  gps  Fixed scaling bug in ScaleResult3
  2012-02-09  gps  Added Qxdm logging
  2011-10-26  gps  Requests using sequencer now actually make request
  2010-10-19  jjo  Resolved integral promotion problem by using a cast in
                   VAdc_ScaleResult
  2010-05-25  jdt  Initial revision based on DALVAdc.c#10

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "DalVAdc.h"
#include "AdcInputs.h"
#include <Library/BaseLib.h>

#ifdef VADC_UNIT_TEST
#include "VAdcSim.h"
#endif

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define VADC_BUSYWAIT_INCREMENT_US 50

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/
/*======================================================================

  FUNCTION        VAdc_ReadBytes

  DESCRIPTION     This function reads from the VAdc peripheral

  DEPENDENCIES    None

  PARAMETERS
      pCtxt            [in]  pointer to the HAL interface context
      uOffset          [in]  the starting address
      pucData          [out] the bytes read
      uDataLen         [in]  the number of bytes to read
      puTotalBytesRead [out] the number of bytes read

  RETURN VALUE    VADC_HAL_SUCCESS or an error code

  SIDE EFFECTS    None

======================================================================*/
static VAdcHalResultType
VAdc_ReadBytes(
   void *pCtxt,
   uint32 uOffset,
   unsigned char *pucData,
   uint32 uDataLen,
   uint32 *puTotalBytesRead
   )
{
   VAdcHalInterfaceCtxtType *pVAdcHalInterfaceCtxt;
   VAdcDevCtxt *pDevCtxt;
   uint32 uRegisterAddress;
#ifndef VADC_UNIT_TEST
   SpmiBus_ResultType result;
#endif

   if (pCtxt == NULL)
   {
      return VADC_HAL_ERROR;
   }

   pVAdcHalInterfaceCtxt = (VAdcHalInterfaceCtxtType *)pCtxt;
   pDevCtxt = pVAdcHalInterfaceCtxt->pDevCtxt;

   uRegisterAddress = uOffset & 0xFF;
   uRegisterAddress |= (pDevCtxt->pBsp->uPeripheralID << 8) & 0xFF00;

#ifndef VADC_UNIT_TEST
   result = SpmiBus_ReadLong(pDevCtxt->pBsp->uSlaveId,
                             pDevCtxt->pBsp->eAccessPriority,
                             uRegisterAddress,
                             (uint8 *)pucData,
                             uDataLen,
                             puTotalBytesRead);

   if (result != SPMI_BUS_SUCCESS)
   {
      VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - SPMI read failed", TRUE);
      return VADC_HAL_ERROR;
   }
#else
   (void)VAdcSim_ReadLong(uOffset, pucData, uDataLen, puTotalBytesRead);
#endif

   return VADC_HAL_SUCCESS;
}

/*======================================================================

  FUNCTION        VAdc_WriteBytes

  DESCRIPTION     This function writes to the VAdc peripheral

  DEPENDENCIES    None

  PARAMETERS
      pCtxt            [in] pointer to the HAL interface context
      uOffset          [in] the starting address
      pucData          [in] the bytes to write
      uDataLen         [in] the number of bytes to write

  RETURN VALUE    VADC_HAL_SUCCESS or an error code

  SIDE EFFECTS    None

======================================================================*/
static VAdcHalResultType
VAdc_WriteBytes(
   void *pCtxt,
   uint32 uOffset,
   unsigned char *pucData,
   uint32 uDataLen
   )
{
   VAdcHalInterfaceCtxtType *pVAdcHalInterfaceCtxt;
   VAdcDevCtxt *pDevCtxt;
   uint32 uRegisterAddress;
#ifndef VADC_UNIT_TEST
   SpmiBus_ResultType result;
#endif

   if (pCtxt == NULL)
   {
      return VADC_HAL_ERROR;
   }

   pVAdcHalInterfaceCtxt = (VAdcHalInterfaceCtxtType *)pCtxt;
   pDevCtxt = pVAdcHalInterfaceCtxt->pDevCtxt;

   uRegisterAddress = uOffset & 0xFF;
   uRegisterAddress |= (pDevCtxt->pBsp->uPeripheralID << 8) & 0xFF00;

#ifndef VADC_UNIT_TEST
   result = SpmiBus_WriteLong(pDevCtxt->pBsp->uSlaveId,
                              pDevCtxt->pBsp->eAccessPriority,
                              uRegisterAddress,
                              (uint8 *)pucData,
                              uDataLen);

   if (result != SPMI_BUS_SUCCESS)
   {
      VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - SPMI write failed", TRUE);
      return VADC_HAL_ERROR;
   }
#else
   (void)VAdcSim_WriteLong(uOffset, pucData, uDataLen);
#endif

   return VADC_HAL_SUCCESS;
}

/*======================================================================

  FUNCTION        VAdc_ScaleCodeToPercent

  DESCRIPTION     Scales the ADC result from code to percent (0 to
                  0xffff).

  DEPENDENCIES    None

  PARAMETERS
      pDevCtxt     [in]  device context
      pChannel     [in]  adc channel
      pCalibData   [in]  channel's calib data
      uCode        [in]  the raw ADC code
      puPercent   [out] the value in percent (0 to 0xffff)

  RETURN VALUE    ADC_DEVICE_RESULT_VALID if successful

  SIDE EFFECTS    None

======================================================================*/
static AdcDeviceResultStatusType
VAdc_ScaleCodeToPercent(
   VAdcDevCtxt *pDevCtxt,
   const VAdcChannelConfigType *pChannel,
   const VAdcCalibDataType *pCalibData,
   uint32 uCode,
   uint32 *puPercent
   )
{
   int64 nCode = uCode;
   int64 nPercent;
   int64 nCode1;
   int64 nCode2;
   int64 nPercent1;
   int64 nPercent2;

   switch (pChannel->eCalMethod)
   {
      case VADC_CAL_METHOD_RATIOMETRIC:
         /* Ratiometric: VrefN and VrefP */
         nCode1 = pCalibData->uVrefN;
         nCode2 = pCalibData->uVrefP;
         nPercent1 = pDevCtxt->vAdcScalingData.uPercentVrefN;
         nPercent2 = pDevCtxt->vAdcScalingData.uPercentVrefP;
         break;

      case VADC_CAL_METHOD_ABSOLUTE:
         /* Absolute: Vref1 and Vref2 */
         nCode1 = pCalibData->uVref1;
         nCode2 = pCalibData->uVref2;
         nPercent1 = pDevCtxt->vAdcScalingData.uPercentVref1;
         nPercent2 = pDevCtxt->vAdcScalingData.uPercentVref2;
         break;

      default:
         return ADC_DEVICE_RESULT_INVALID;
   }

   /* Perform the linear interpolation */
   nPercent = ((nPercent2 - nPercent1) * (nCode - nCode1)) / (nCode2 - nCode1) + nPercent1;

   if (nPercent < 0)
   {
      *puPercent = 0;
   }
   else if (nPercent > 0xffff)
   {
      *puPercent = 0xffff;
   }
   else
   {
      *puPercent = (uint32)nPercent;
   }

   return ADC_DEVICE_RESULT_VALID;
}

/*======================================================================

  FUNCTION        VAdc_ScaleCodeToMicrovolts

  DESCRIPTION     Scales the ADC result from code to microvolts.

  DEPENDENCIES    None

  PARAMETERS
      pDevCtxt     [in]  device context
      pChannel     [in]  adc channel
      pCalibData   [in]  channel's calib data
      uCode        [in]  the raw ADC code
      puMicrovolts [out] the value in microvolts

  RETURN VALUE    ADC_DEVICE_RESULT_VALID if successful

  SIDE EFFECTS    None

======================================================================*/
static AdcDeviceResultStatusType
VAdc_ScaleCodeToMicrovolts(
   VAdcDevCtxt *pDevCtxt,
   const VAdcChannelConfigType *pChannel,
   const VAdcCalibDataType *pCalibData,
   uint32 uCode,
   uint32 *puMicrovolts
   )
{
   AdcDeviceResultStatusType eStatus = ADC_DEVICE_RESULT_VALID;
   int64 nMicrovolts;
   int64 nCode = uCode;
   int64 nCode1;
   int64 nCode2;
   int64 nPhysical1;
   int64 nPhysical2;
   int64 nMinPhysical = pDevCtxt->pBsp->uVrefN_mv * 1000;
   int64 nMaxPhysical = pDevCtxt->pBsp->uVrefP_mv * 1000;

   switch (pChannel->eCalMethod)
   {
      case VADC_CAL_METHOD_RATIOMETRIC:
         /* Ratiometric: VrefN and VrefP */
         nCode1 = pCalibData->uVrefN;
         nCode2 = pCalibData->uVrefP;
         nPhysical1 = nMinPhysical;
         nPhysical2 = nMaxPhysical;
         break;

      case VADC_CAL_METHOD_ABSOLUTE:
         /* Absolute: Vref1 and Vref2 */
         nCode1 = pCalibData->uVref1;
         nCode2 = pCalibData->uVref2;
         nPhysical1 = pDevCtxt->pBsp->uVref1_mv * 1000;
         nPhysical2 = pDevCtxt->pBsp->uVref2_mv * 1000;
         break;

      default:
         return ADC_DEVICE_RESULT_INVALID;
   }

   /* Perform the linear interpolation */
   nMicrovolts = ((nPhysical2 - nPhysical1) * (nCode - nCode1)) / (nCode2 - nCode1) + nPhysical1;

   if (nMicrovolts < nMinPhysical)
   {
      *puMicrovolts = (uint32)nMinPhysical;
   }
   else if (nMicrovolts > nMaxPhysical)
   {
      *puMicrovolts = (uint32)nMaxPhysical;
   }
   else
   {
      *puMicrovolts = (uint32)nMicrovolts;
   }

   /*
    * Since PMIC AMUX channels have a prescalar gain applied to the input,
    * we need to scale input by inverse of the channel prescalar gain to
    * obtain the actual input voltage.
    */
   if (pChannel->scalingFactor.uNumerator > 0)
   {
      *puMicrovolts *= pChannel->scalingFactor.uDenominator;
      *puMicrovolts /= pChannel->scalingFactor.uNumerator;
   }
   else
   {
      return ADC_DEVICE_RESULT_INVALID;
   }

   return eStatus;
}

/*======================================================================

  FUNCTION        VAdc_ScaleCodeToMicrovoltsInverse

  DESCRIPTION     Scales the ADC result from microvolts to code.

  DEPENDENCIES    None

  PARAMETERS
      pDevCtxt    [in]  device context
      pChannel    [in]  adc channel
      pCalibData  [in]  channel's calib data
      uMicrovolts [in]  the value in microvolts
      puCode      [out] the value in raw ADC code

  RETURN VALUE    ADC_DEVICE_RESULT_VALID if successful

  SIDE EFFECTS    None

======================================================================*/
static AdcDeviceResultStatusType
VAdc_ScaleCodeToMicrovoltsInverse(
   VAdcDevCtxt *pDevCtxt,
   const VAdcChannelConfigType *pChannel,
   const VAdcCalibDataType *pCalibData,
   uint32 uMicrovolts,
   uint32 *puCode
   )
{
   AdcDeviceResultStatusType eStatus = ADC_DEVICE_RESULT_VALID;
   int64 nMicrovolts;
   int64 nCode;
   int64 nCode1;
   int64 nCode2;
   int64 nPhysical1;
   int64 nPhysical2;

   /* Undo the prescalar gain */
   if (pChannel->scalingFactor.uDenominator > 0)
   {
      uMicrovolts *= pChannel->scalingFactor.uNumerator;
      uMicrovolts /= pChannel->scalingFactor.uDenominator;
   }
   else
   {
      return ADC_DEVICE_RESULT_INVALID;
   }

   nMicrovolts = uMicrovolts;

   /* Undo the calibration method */
   switch (pChannel->eCalMethod)
   {
      case VADC_CAL_METHOD_RATIOMETRIC:
         /* Ratiometric: VrefN and VrefP */
         nCode1 = pCalibData->uVrefN;
         nCode2 = pCalibData->uVrefP;
         nPhysical1 = pDevCtxt->pBsp->uVrefN_mv * 1000;
         nPhysical2 = pDevCtxt->pBsp->uVrefP_mv * 1000;
         break;

      case VADC_CAL_METHOD_ABSOLUTE:
         /* Absolute: Vref1 and Vref2 */
         nCode1 = pCalibData->uVref1;
         nCode2 = pCalibData->uVref2;
         nPhysical1 = pDevCtxt->pBsp->uVref1_mv * 1000;
         nPhysical2 = pDevCtxt->pBsp->uVref2_mv * 1000;
         break;

      default:
         return ADC_DEVICE_RESULT_INVALID;
   }

   nCode = ((nCode2 - nCode1) * (nMicrovolts - nPhysical1)) / (nPhysical2 - nPhysical1) + nCode1;

   *puCode = (uint32)nCode;

   return eStatus;
}

/*======================================================================

  FUNCTION        VAdc_StartConversionInternal

  DESCRIPTION     This function starts the ADC conversion

  DEPENDENCIES    None

  PARAMETERS
      pDevCtxt          [in] pointer to the VADC device context
      pConfig           [in] the VADC configuration to use
      pChConfig         [in] which VADC mux input to use

  RETURN VALUE    DAL_SUCCESS or an error code

  SIDE EFFECTS    DAL_SUCCESS if conversion was started successfully
                  Otherwise, returns a DAL error code.

======================================================================*/
static DALResult
VAdc_StartConversionInternal(
   VAdcDevCtxt *pDevCtxt,
   const VAdcConfigType *pConfig,
   const VAdcChannelConfigType *pChConfig
   )
{
   VAdcHalResultType result;
   VAdcConversionParametersType conversionParams;
   VAdcSequencerParametersType sequencerParams;

   /*
    * Initialize conversion parameter variables.
    */
   conversionParams.uChannel = pChConfig->uAdcHardwareChannel;
   conversionParams.eSettlingDelay = pChConfig->eSettlingDelay;
   conversionParams.eClockSelect = pConfig->eClockSelect;
   conversionParams.eDecimationRatio = pConfig->eDecimationRatio;
   conversionParams.eFastAverageMode = pChConfig->eFastAverageMode;

   /*
    * Configure the conversion parameters.
    */
   result = VAdcHalSetConversionParameters(&pDevCtxt->iVAdcHalInterface, &conversionParams);
   if (result != VADC_HAL_SUCCESS)
   {
      return DAL_ERROR;
   }

   /*
    * Check if this channel will use the conversion sequencer.
    */
   if (pChConfig->bUseSequencer == TRUE)
   {
      sequencerParams = pDevCtxt->pBsp->paSequencerParams[pChConfig->uSequencerIdx];

      result = VAdcHalSetSequencerParameters(&pDevCtxt->iVAdcHalInterface, &sequencerParams);
      if (result != VADC_HAL_SUCCESS)
      {
         return DAL_ERROR;
      }

      result = VAdcHalSetOpMode(&pDevCtxt->iVAdcHalInterface, VADC_OP_MODE_SEQUENCER);
      if (result != VADC_HAL_SUCCESS)
      {
         return DAL_ERROR;
      }
   }
   else
   {
      result = VAdcHalSetOpMode(&pDevCtxt->iVAdcHalInterface, VADC_OP_MODE_NORMAL);
      if (result != VADC_HAL_SUCCESS)
      {
         return DAL_ERROR;
      }
   }

   /*
    * Request an ADC conversion by asserting REQ field of the arbiter control
    * register.
    */
   result = VAdcHalRequestConversion(&pDevCtxt->iVAdcHalInterface);
   if (result != VADC_HAL_SUCCESS)
   {
      return DAL_ERROR;
   }

   return DAL_SUCCESS;
}

/*======================================================================

  FUNCTION        VAdc_WaitForEoc

  DESCRIPTION
      This function waits for the EOC and gets the conversion status.

  DEPENDENCIES
      None

  PARAMETERS
      pDevCtxt    [in]
      pChConfig   [in]
      pConfig     [in]
      pConvStatus [out]

  RETURN VALUE
      DAL_SUCCESS or an error

  SIDE EFFECTS
      Be careful not to make the wait time too small or EOC may
      time out too early.

======================================================================*/
static DALResult
VAdc_WaitForEoc(
   VAdcDevCtxt *pDevCtxt,
   const VAdcChannelConfigType *pChConfig,
   const VAdcConfigType *pConfig,
   VAdcStatusType *pConvStatus
   )
{
   uint32 uReadTimeout_us = pDevCtxt->pBsp->uReadTimeout_us;
   VAdcHalResultType status;
   uint32 uTimeWaited_us = 0;
   uint32 uReadTime_us;
   uint32 uSettlingDelay_us;
   uint32 uNumAverages;

   /* Calculate the read time */
   uSettlingDelay_us = VAdcHalGetSettlingDelayUs(pChConfig->eSettlingDelay) + pDevCtxt->pBsp->uLDOSettlingTime_us;
   uNumAverages = VAdcHalGetNumAverages(pChConfig->eFastAverageMode);
   uReadTime_us = (pConfig->uConversionTime_us * uNumAverages) + uSettlingDelay_us;

   do
   {
#ifndef VADC_UNIT_TEST
      DALSYS_BusyWait(VADC_BUSYWAIT_INCREMENT_US);
#endif

      uTimeWaited_us += VADC_BUSYWAIT_INCREMENT_US;

      if (uTimeWaited_us >= uReadTime_us)
      {
         /* Check for EOC */
         status = VAdcHalGetStatus(&pDevCtxt->iVAdcHalInterface, pConvStatus);
         if (status != VADC_HAL_SUCCESS)
         {
            return DAL_ERROR;
         }

         if (pConvStatus->eConversionStatus == VADC_CONVERSION_STATUS_COMPLETE)
         {
            VAdc_LogEOC(&pDevCtxt->vAdcDebug);
            return DAL_SUCCESS;
         }
      }

   } while (uTimeWaited_us < uReadTimeout_us);

   /* Conversion did not complete */
   VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - conversion timed out", FALSE);
   VAdc_LogDumpRegisters(&pDevCtxt->vAdcDebug, &pDevCtxt->iVAdcHalInterface);

   return DAL_ERROR;
}

/*======================================================================

  FUNCTION        VAdc_ReadAdcCode

  DESCRIPTION
      This function reads the ADC result.

  DEPENDENCIES
      None

  PARAMETERS
      pDevCtxt [in]
      puCode   [out]

  RETURN VALUE
      DAL_SUCCESS or an error

  SIDE EFFECTS
      None

======================================================================*/
static DALResult
VAdc_ReadAdcCode(
   VAdcDevCtxt *pDevCtxt,
   uint32 *puCode
   )
{
   VAdcHalResultType result;
   VAdcConversionCodeType uCode;

   /*
    * Read the data from the VADC
    */
   result = VAdcHalGetConversionCode(&pDevCtxt->iVAdcHalInterface, &uCode);
   if (result != VADC_HAL_SUCCESS)
   {
      return DAL_ERROR;
   }

   *puCode = uCode;

   return DAL_SUCCESS;
}

/*======================================================================

  FUNCTION        VAdc_ReadAdc

  DESCRIPTION
      This function reads an ADC channel.

  DEPENDENCIES
      None

  PARAMETERS
      pDevCtxt    [in]
      pChConfig   [in]
      uConfigIdx  [in]
      pConvStatus [out]
      puCode      [out]

  RETURN VALUE
      DAL_SUCCESS or an error

  SIDE EFFECTS
      None

======================================================================*/
static DALResult
VAdc_ReadAdc(
   VAdcDevCtxt *pDevCtxt,
   const VAdcChannelConfigType *pChConfig,
   uint32 uConfigIdx,
   VAdcStatusType *pConvStatus,
   uint32 *puCode
   )
{
   VAdcHalResultType status;
   DALResult result;
   const VAdcConfigType *pConfig = &pDevCtxt->pBsp->paConfigs[uConfigIdx];

   /* Enable vadc */
   status = VAdcHalSetEnable(&pDevCtxt->iVAdcHalInterface, VADC_ENABLE);
   if (status != VADC_HAL_SUCCESS)
   {
      result = DAL_ERROR;
      goto error;
   }

   /* Start the conversion */
   result = VAdc_StartConversionInternal(pDevCtxt,
                                         pConfig,
                                         pChConfig);
   if (result != DAL_SUCCESS)
   {
      goto error;
   }

   /* Wait for the conversion to finish */
   result = VAdc_WaitForEoc(pDevCtxt, pChConfig, pConfig, pConvStatus);
   if (result != DAL_SUCCESS)
   {
      goto error;
   }

   /* Read the ADC code */
   result = VAdc_ReadAdcCode(pDevCtxt, puCode);
   if (result != DAL_SUCCESS)
   {
      goto error;
   }

#ifdef VADC_DUMP_REGISTERS_AFTER_READS
   VAdc_LogDumpRegisters(&pDevCtxt->vAdcDebug, &pDevCtxt->iVAdcHalInterface);
#endif

   /* Disable vadc */
   status = VAdcHalSetEnable(&pDevCtxt->iVAdcHalInterface, VADC_DISABLE);
   if (status != VADC_HAL_SUCCESS)
   {
      return DAL_ERROR;
   }

   return DAL_SUCCESS;

error:
   VAdc_LogDumpRegisters(&pDevCtxt->vAdcDebug, &pDevCtxt->iVAdcHalInterface);
   (void)VAdcHalSetEnable(&pDevCtxt->iVAdcHalInterface, VADC_DISABLE);
   return result;
}

/*======================================================================

  FUNCTION        VAdc_CalibrateInternal

  DESCRIPTION
      This function calibrates the ADC.

  DEPENDENCIES
      None

  PARAMETERS
      pDevCtxt    [in]

  RETURN VALUE
      DAL_SUCCESS or an error

  SIDE EFFECTS
      None

======================================================================*/
static DALResult
VAdc_CalibrateInternal(VAdcDevCtxt *pDevCtxt)
{
   DALResult result;
   uint32 uConfig;
   VAdcCalibDataType *pCalibData;
   uint32 uCode;
   VAdcStatusType convStatus;
   const VAdcChannelConfigType *pChannel;
   uint32 uNumConfigs = pDevCtxt->pBsp->uNumConfigs;

   for (uConfig = 0; uConfig < uNumConfigs; uConfig++)
   {
      pCalibData = &pDevCtxt->paCalibData[uConfig];

      /* VREFP */
      pChannel = &pDevCtxt->pBsp->paCalChannels[0];
      result = VAdc_ReadAdc(pDevCtxt, pChannel, uConfig, &convStatus, &uCode);
      if (result != DAL_SUCCESS)
      {
         goto error;
      }

      pCalibData->uVrefP = uCode;

      /* VREFN */
      pChannel = &pDevCtxt->pBsp->paCalChannels[1];
      result = VAdc_ReadAdc(pDevCtxt, pChannel, uConfig, &convStatus, &uCode);
      if (result != DAL_SUCCESS)
      {
         goto error;
      }

      pCalibData->uVrefN = uCode;

      /* VREF1 */
      pChannel = &pDevCtxt->pBsp->paCalChannels[2];
      result = VAdc_ReadAdc(pDevCtxt, pChannel, uConfig, &convStatus, &uCode);
      if (result != DAL_SUCCESS)
      {
         goto error;
      }

      pCalibData->uVref1 = uCode;

      /* VREF2 */
      pChannel = &pDevCtxt->pBsp->paCalChannels[3];
      result = VAdc_ReadAdc(pDevCtxt, pChannel, uConfig, &convStatus, &uCode);
      if (result != DAL_SUCCESS)
      {
         goto error;
      }

      pCalibData->uVref2 = uCode;
   }

   pDevCtxt->bCalibrated = TRUE;

   return DAL_SUCCESS;

error:
   return result;
}

/*======================================================================

  FUNCTION        VAdc_ReadAdcChannel

  DESCRIPTION
      This function reads an ADC channel and returns the scaled result
      and status of the read. It is not meant for reading calibration
      channels.

  DEPENDENCIES
      None

  PARAMETERS
      pDevCtxt         [in]
      uChannel         [in]
      pAdcDeviceResult [out]

  RETURN VALUE
      DAL_SUCCESS or an error

  SIDE EFFECTS
      None

======================================================================*/
static DALResult
VAdc_ReadAdcChannel(
   VAdcDevCtxt *pDevCtxt,
   uint32 uChannel,
   AdcDeviceResultType *pAdcDeviceResult
   )
{
   DALResult result;
   VAdcStatusType convStatus;
   uint32 uCode;
   const VAdcChannelConfigType *pChConfig = &pDevCtxt->pBsp->paChannels[uChannel];
   uint32 uConfigIdx = pChConfig->uConfigIdx;
   const VAdcCalibDataType *pCalibData = &pDevCtxt->paCalibData[uConfigIdx];
#ifndef VADC_LOG_DISABLED
   VAdcDebugCalDataType debugCalData;
#endif

   /* Make sure the VAdc has been calibrated */
   if (pDevCtxt->bCalibrated == FALSE)
   {
      result = VAdc_CalibrateInternal(pDevCtxt);
      if (result != DAL_SUCCESS)
      {
         goto error;
      }
   }

   /* Read the ADC */
   result = VAdc_ReadAdc(pDevCtxt,
                         pChConfig,
                         pChConfig->uConfigIdx,
                         &convStatus,
                         &uCode);

   if (result != DAL_SUCCESS)
   {
      goto error;
   }

   /* Scale the result */
   VAdc_ProcessConversionResult(pDevCtxt,
                                uChannel,
                                pCalibData,
                                uCode,
                                pAdcDeviceResult);

   if (pChConfig->bUseSequencer == TRUE && pAdcDeviceResult->eStatus == ADC_DEVICE_RESULT_VALID)
   {
      if (convStatus.bSequencerTimeoutErrorOccurred == TRUE)
      {
         pAdcDeviceResult->eStatus = ADC_DEVICE_RESULT_TIMEOUT;
      }
      else if (convStatus.bSequencerFifoNotEmptyErrorOccurred == TRUE)
      {
         pAdcDeviceResult->eStatus = ADC_DEVICE_RESULT_FIFO_NOT_EMPTY;
      }
   }

#ifndef VADC_LOG_DISABLED
   debugCalData.uVrefP = pCalibData->uVrefP;
   debugCalData.uVrefN = pCalibData->uVrefN;
   debugCalData.uVref1 = pCalibData->uVref1;
   debugCalData.uVref2 = pCalibData->uVref2;

   VAdc_LogConversionResult(&pDevCtxt->vAdcDebug,
                            pDevCtxt->pBsp->paChannels[uChannel].pName,
                            pAdcDeviceResult,
                            &debugCalData);
#endif

   return DAL_SUCCESS;

error:
   pAdcDeviceResult->eStatus = ADC_DEVICE_RESULT_INVALID;

   return DAL_SUCCESS;
}

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/
/*======================================================================

  FUNCTION        VAdc_GetHardwareChannel

  DESCRIPTION     This function gets the hardware channel for a given
                  channel index.

  DEPENDENCIES    None

  PARAMETERS
      pDevCtxt     [in]  pointer to the current device context
      uChannelIdx  [in]  channel index

  RETURN VALUE    HW channel

  SIDE EFFECTS    None

======================================================================*/
uint32
VAdc_GetHardwareChannel(
   VAdcDevCtxt *pDevCtxt,
   uint32 uChannelIdx
   )
{
   return pDevCtxt->pBsp->paChannels[uChannelIdx].uAdcHardwareChannel;
}

/*======================================================================

  FUNCTION        VAdc_GetCalibration

  DESCRIPTION     This function gets the calibration data for a given
                  configuration.

  DEPENDENCIES    None

  PARAMETERS
      pDevCtxt    [in]  pointer to the current device context
      uConfigIdx  [in]  config index
      pCalibData [out] calibration data

  RETURN VALUE    None

  SIDE EFFECTS    None

======================================================================*/
void
VAdc_GetCalibration(
   VAdcDevCtxt *pDevCtxt,
   uint32 uConfigIdx,
   VAdcCalibDataType *pCalibData
   )
{
   *pCalibData = pDevCtxt->paCalibData[uConfigIdx];
}

/*======================================================================

  FUNCTION        VAdc_ProcessConversionResult

  DESCRIPTION     This function is called at the end of conversion to process
                  the result. The raw result is converted into a scaled 16-bit
                  result and then and the scaled result is converted into
                  physical units in millivolts. Further scaling may be
                  performed by the scaling function defined in the BSP.

  DEPENDENCIES    None

  PARAMETERS
      pDevCtxt   [in]  pointer to the current device context
      uChannel   [in]  current channel index
      pCalibData [in]  calibration data
      uCode      [in]  raw ADC code
      pResult    [out] ADC result

  RETURN VALUE    None

  SIDE EFFECTS    None

======================================================================*/
void
VAdc_ProcessConversionResult(
   VAdcDevCtxt *pDevCtxt,
   uint32 uChannel,
   const VAdcCalibDataType *pCalibData,
   uint32 uCode,
   AdcDeviceResultType *pResult
   )
{
   const VAdcChannelConfigType *pChannel = &pDevCtxt->pBsp->paChannels[uChannel];

   pResult->uCode = uCode;

   /* Calculate the value in percent (0 to 0xffff) */
   pResult->eStatus = VAdc_ScaleCodeToPercent(pDevCtxt,
                                              pChannel,
                                              pCalibData,
                                              uCode,
                                              &pResult->uPercent);
   if (pResult->eStatus != ADC_DEVICE_RESULT_VALID)
   {
      return;
   }

   /* Calculate the value in microvolts */
   pResult->eStatus = VAdc_ScaleCodeToMicrovolts(pDevCtxt,
                                                 pChannel,
                                                 pCalibData,
                                                 uCode,
                                                 &pResult->uMicrovolts);
   if (pResult->eStatus != ADC_DEVICE_RESULT_VALID)
   {
      return;
   }

   /* By default, scale the physical result in units of millivolts */
   pResult->nPhysical = AdcDivideWithRounding((int32)pResult->uMicrovolts, 1000);

   /* Call the BSP scaling function (if present) */
   switch (pChannel->eScalingMethod)
   {
      case VADC_SCALE_TO_MILLIVOLTS:
         /* by default, nPhysical is in millivolts */
         break;
      case VADC_SCALE_PMIC_SENSOR_TO_MILLIDEGREES:
         pResult->eStatus = VAdcScalePmicTherm(pResult->uMicrovolts,
                                               &pResult->nPhysical);
         break;
      case VADC_SCALE_INTERPOLATE_FROM_RT_R25:
         pResult->eStatus = VAdcScaleTdkNTCGTherm(pResult->uCode,
                                                  pCalibData->uVrefN,
                                                  pCalibData->uVrefP,
                                                  pChannel->pInterpolationTable,
                                                  pChannel->uInterpolationTableLength,
                                                  &pResult->nPhysical);
         break;
      case VADC_SCALE_THERMISTOR:
         pResult->eStatus = VAdcScaleThermistor(pResult->uCode,
                                                pCalibData->uVrefN,
                                                pCalibData->uVrefP,
                                                pChannel->uPullUp,
                                                pChannel->pInterpolationTable,
                                                pChannel->uInterpolationTableLength,
                                                &pResult->nPhysical);
         break;
      case VADC_SCALE_INTERPOLATE_FROM_MILLIVOLTS:
         pResult->eStatus = AdcMapLinearInt32toInt32(pChannel->pInterpolationTable,
                                                     pChannel->uInterpolationTableLength,
                                                     pResult->nPhysical,
                                                     &pResult->nPhysical);
         break;
      default:
         /* By default, nPhysical is in millivolts */
         break;
   }

   return;
}

/*======================================================================

  FUNCTION        VAdc_ProcessConversionResultInverse

  DESCRIPTION     Inverse of VAdc_ProcessConversionResultInverse.

  DEPENDENCIES    None

  PARAMETERS
      pDevCtxt   [in]  pointer to the current device context
      uChannel   [in]  current channel index
      pCalibData [in]  calibration data
      nPhysical  [in]  physical value
      pResult    [out] ADC result

  RETURN VALUE    None

  SIDE EFFECTS    None

======================================================================*/
void
VAdc_ProcessConversionResultInverse(
   VAdcDevCtxt *pDevCtxt,
   uint32 uChannel,
   const VAdcCalibDataType *pCalibData,
   int32 nPhysical,
   AdcDeviceResultType *pResult
   )
{
   const VAdcChannelConfigType *pChannel = &pDevCtxt->pBsp->paChannels[uChannel];
   int32 nMillivolts;
   DALBOOL bHaveCode = FALSE;

   pResult->nPhysical = nPhysical;

   /* Remaining: eStatus, uMicrovolts, uCode, uPercent */

   /* Reverse the BSP scaling */
   switch (pChannel->eScalingMethod)
   {
      case VADC_SCALE_PMIC_SENSOR_TO_MILLIDEGREES:
         pResult->eStatus = VAdcScalePmicThermInverse(pResult->nPhysical,
                                                      &pResult->uMicrovolts);
         if (pResult->eStatus != ADC_DEVICE_RESULT_VALID)
         {
            return;
         }
         /* Remaining: uCode, uPercent */

         break;
      case VADC_SCALE_INTERPOLATE_FROM_RT_R25:
         pResult->eStatus = VAdcScaleTdkNTCGThermInverse(pResult->nPhysical,
                                                         pCalibData->uVrefN,
                                                         pCalibData->uVrefP,
                                                         pChannel->pInterpolationTable,
                                                         pChannel->uInterpolationTableLength,
                                                         &pResult->uCode);
         if (pResult->eStatus != ADC_DEVICE_RESULT_VALID)
         {
            return;
         }

         pResult->eStatus = VAdc_ScaleCodeToMicrovolts(pDevCtxt,
                                                       pChannel,
                                                       pCalibData,
                                                       pResult->uCode,
                                                       &pResult->uMicrovolts);
         if (pResult->eStatus != ADC_DEVICE_RESULT_VALID)
         {
            return;
         }

         bHaveCode = TRUE;
         /* Remaining: uPercent */

         break;
      case VADC_SCALE_THERMISTOR:
         pResult->eStatus = VAdcScaleThermistorInverse(pResult->nPhysical,
                                                       pCalibData->uVrefN,
                                                       pCalibData->uVrefP,
                                                       pChannel->uPullUp,
                                                       pChannel->pInterpolationTable,
                                                       pChannel->uInterpolationTableLength,
                                                       &pResult->uCode);
         if (pResult->eStatus != ADC_DEVICE_RESULT_VALID)
         {
            return;
         }

         pResult->eStatus = VAdc_ScaleCodeToMicrovolts(pDevCtxt,
                                                       pChannel,
                                                       pCalibData,
                                                       pResult->uCode,
                                                       &pResult->uMicrovolts);
         if (pResult->eStatus != ADC_DEVICE_RESULT_VALID)
         {
            return;
         }

         bHaveCode = TRUE;
         /* Remaining: uPercent */

         break;
      case VADC_SCALE_INTERPOLATE_FROM_MILLIVOLTS:
         /* Gets mV */
         pResult->eStatus = AdcMapLinearInt32toInt32Inverse(pChannel->pInterpolationTable,
                                                            pChannel->uInterpolationTableLength,
                                                            pResult->nPhysical,
                                                            &nMillivolts);
         if (pResult->eStatus != ADC_DEVICE_RESULT_VALID)
         {
            return;
         }

         pResult->uMicrovolts = (uint32)(nMillivolts * 1000);
         /* Remaining: uCode, uPercent */

         break;
      default:
         /* By default, nPhysical is in millivolts */
         pResult->eStatus = ADC_DEVICE_RESULT_VALID;
         pResult->uMicrovolts = (uint32)(nPhysical * 1000);
         /* Remaining: uCode, uPercent */

         break;
   }

   if (bHaveCode == FALSE)
   {
      pResult->eStatus = VAdc_ScaleCodeToMicrovoltsInverse(pDevCtxt,
                                                           pChannel,
                                                           pCalibData,
                                                           pResult->uMicrovolts,
                                                           &pResult->uCode);
      if (pResult->eStatus != ADC_DEVICE_RESULT_VALID)
      {
         return;
      }
      /* Remaining: uPercent */
   }

   /* Calculate the value in percent (0 to 0xffff) */
   pResult->eStatus = VAdc_ScaleCodeToPercent(pDevCtxt,
                                              pChannel,
                                              pCalibData,
                                              pResult->uCode,
                                              &pResult->uPercent);

   return;
}

/*======================================================================

  FUNCTION        VAdc_ProcessConversionResultFromMilliVolts

  DESCRIPTION     This function converts from millivolts to physical
                  units.

                  The mV input is the ADC reading WITHOUT prescalar
                  gain applied, for example:
                   - unity gain channel: 50 mV input --> 50 mV
                   - 1/3 gain channel: 50 mV input --> 150 mV

  DEPENDENCIES    None

  PARAMETERS
      pDevCtxt    [in]  pointer to the current device context
      uChannel    [in]  current channel index
      pCalibData  [in]  calibration data
      nMilliVolts_noPrescalar [in]  unscaled VADC voltage in milliVolts
      pResult     [out] ADC result

  RETURN VALUE    None

  SIDE EFFECTS    None

======================================================================*/
void
VAdc_ProcessConversionResultFromMilliVolts(
   VAdcDevCtxt *pDevCtxt,
   uint32 uChannel,
   const VAdcCalibDataType *pCalibData,
   int32 nMilliVolts_noPrescalar,
   AdcDeviceResultType *pResult
   )
{
   const VAdcChannelConfigType *pChannel = &pDevCtxt->pBsp->paChannels[uChannel];
   int32 nMilliVolts;

   /* Apply the prescalar gain */
   if (pChannel->scalingFactor.uNumerator > 0)
   {
      nMilliVolts = (nMilliVolts_noPrescalar * (int32)pChannel->scalingFactor.uDenominator) / (int32)pChannel->scalingFactor.uNumerator;
   }
   else
   {
      pResult->eStatus = ADC_DEVICE_RESULT_INVALID;
      return;
   }

   /* By default, scale the physical result in units of millivolts */
   pResult->nPhysical = nMilliVolts;

   pResult->uMicrovolts = (uint32)(nMilliVolts * 1000);

   /* Get the code value from microvolts */
   pResult->eStatus = VAdc_ScaleCodeToMicrovoltsInverse(pDevCtxt,
                                                        pChannel,
                                                        pCalibData,
                                                        pResult->uMicrovolts,
                                                        &pResult->uCode);
   if (pResult->eStatus != ADC_DEVICE_RESULT_VALID)
   {
      return;
   }

   /* Calculate the value in percent (0 to 0xffff) */
   pResult->eStatus = VAdc_ScaleCodeToPercent(pDevCtxt,
                                              pChannel,
                                              pCalibData,
                                              pResult->uCode,
                                              &pResult->uPercent);
   if (pResult->eStatus != ADC_DEVICE_RESULT_VALID)
   {
      return;
   }

   /* Call the BSP scaling function (if present) */
   switch (pChannel->eScalingMethod)
   {
      case VADC_SCALE_TO_MILLIVOLTS:
         /* by default, nPhysical is in millivolts */
         break;
      case VADC_SCALE_PMIC_SENSOR_TO_MILLIDEGREES:
         pResult->eStatus = VAdcScalePmicTherm(pResult->uMicrovolts,
                                               &pResult->nPhysical);
         break;
      case VADC_SCALE_INTERPOLATE_FROM_RT_R25:
         pResult->eStatus = VAdcScaleTdkNTCGTherm(pResult->uCode,
                                                  pCalibData->uVrefN,
                                                  pCalibData->uVrefP,
                                                  pChannel->pInterpolationTable,
                                                  pChannel->uInterpolationTableLength,
                                                  &pResult->nPhysical);
         break;
      case VADC_SCALE_THERMISTOR:
         pResult->eStatus = VAdcScaleThermistor(pResult->uCode,
                                                pCalibData->uVrefN,
                                                pCalibData->uVrefP,
                                                pChannel->uPullUp,
                                                pChannel->pInterpolationTable,
                                                pChannel->uInterpolationTableLength,
                                                &pResult->nPhysical);
         break;
      case VADC_SCALE_INTERPOLATE_FROM_MILLIVOLTS:
         pResult->eStatus = AdcMapLinearInt32toInt32(pChannel->pInterpolationTable,
                                                     pChannel->uInterpolationTableLength,
                                                     pResult->nPhysical,
                                                     &pResult->nPhysical);
         break;
      default:
         /* By default, nPhysical is in millivolts */
         break;
   }

   return;
}

/*----------------------------------------------------------------------------
 * The following functions are for DALDriver specific functionality
 * -------------------------------------------------------------------------*/
DALResult
VAdc_DriverInit(VAdcDrvCtxt *pCtxt)
{
   return DAL_SUCCESS;
}

DALResult
VAdc_DriverDeInit(VAdcDrvCtxt *pCtxt)
{
   return DAL_SUCCESS;
}

/*----------------------------------------------------------------------------
 * The following functions are declared in DalDevice Interface.
 * -------------------------------------------------------------------------*/
DALResult
VAdc_DeviceInit(VAdcClientCtxt *pCtxt)
{
   DALResult ret;
   VAdcHalResultType result;
   DALSYSPropertyVar propVar;
   VAdcDevCtxt *pDevCtxt = pCtxt->pVAdcDevCtxt;
   uint32 uChannelIdx;
   const VAdcChannelConfigType *pChConfig;
   pm_err_flag_type pmResult;
#ifndef VADC_UNIT_TEST
   SpmiBus_ResultType spmiRet;
#endif

   /* Initialize VADC context */
   pDevCtxt->bHardwareSupported = TRUE;
   pDevCtxt->paCalibData = NULL;
   pDevCtxt->bCalibrated = FALSE;
   pDevCtxt->pBsp = NULL;

   VAdc_LogInit(&pDevCtxt->vAdcDebug, pDevCtxt->DevId);

   /* Read the DAL properties - hProp populated in device attach */
   ret = DALSYS_GetPropertyValue(pDevCtxt->hProp, "VADC_BSP", 0, &propVar);

   if (ret != DAL_SUCCESS)
   {
      VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - failed to get BSP", TRUE);
      return ret;
   }

   pDevCtxt->pBsp = (VAdcBspType *)propVar.Val.pStruct;

   /* Get the PMIC device info */
   pmResult = pm_get_pmic_info((uint8)pDevCtxt->pBsp->uPmicDevice, &pDevCtxt->pmicDeviceInfo);
   if (pmResult != PM_ERR_FLAG__SUCCESS)
   {
      VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - failed to get PMIC device info", TRUE);
      return DAL_ERROR;
   }

#ifndef VADC_UNIT_TEST
   /* Attach to to the SPMI driver */
   spmiRet = SpmiBus_Init();
   if (spmiRet != SPMI_BUS_SUCCESS)
   {
      VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - failed to attach to SPMI", TRUE);
      return DAL_ERROR;
   }
#endif

   /* Initialize the HAL interface */
   pDevCtxt->vAdcHalInterfaceCtxt.pDevCtxt = pDevCtxt;
   pDevCtxt->iVAdcHalInterface.pCtxt = (void *)&pDevCtxt->vAdcHalInterfaceCtxt;
   pDevCtxt->iVAdcHalInterface.pfnWriteBytes = VAdc_WriteBytes;
   pDevCtxt->iVAdcHalInterface.pfnReadBytes = VAdc_ReadBytes;

   /* Allocate and initialize calibration info */
   ret = DALSYS_Malloc(sizeof(VAdcCalibDataType) * pDevCtxt->pBsp->uNumConfigs,
                       (void **)&pDevCtxt->paCalibData);
   if (ret != DAL_SUCCESS)
   {
      VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - failed to allocate memory for calibration data", TRUE);
      return ret;
   }

   pDevCtxt->vAdcScalingData.uPercentVrefN = 0;
   pDevCtxt->vAdcScalingData.uPercentVrefP = 0xffff;
   pDevCtxt->vAdcScalingData.uPercentVref1 = (0xffff * pDevCtxt->pBsp->uVref1_mv) / pDevCtxt->pBsp->uVrefP_mv;
   pDevCtxt->vAdcScalingData.uPercentVref2 = (0xffff * pDevCtxt->pBsp->uVref2_mv) / pDevCtxt->pBsp->uVrefP_mv;

   // Log the revision & peripheral type
   result = VAdcHalGetRevisionInfo(&pDevCtxt->iVAdcHalInterface, &pDevCtxt->revisionInfo);
   if (result != VADC_HAL_SUCCESS)
   {
      return DAL_ERROR;
   }

   // Sanity check the peripheral type
   if (pDevCtxt->pBsp->uPerphType != pDevCtxt->revisionInfo.uType)
   {
      VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - Invalid VADC peripheral type", TRUE);
      pDevCtxt->bHardwareSupported = FALSE;
   }

   // Check the digital version information
   if (pDevCtxt->revisionInfo.uDigitalMajor < pDevCtxt->pBsp->uMinDigMajor)
   {
      VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - Invalid HW - dig major", TRUE);
      pDevCtxt->bHardwareSupported = FALSE;
   }
   else if (pDevCtxt->revisionInfo.uDigitalMajor == pDevCtxt->pBsp->uMinDigMajor)
   {
      if (pDevCtxt->revisionInfo.uDigitalMinor < pDevCtxt->pBsp->uMinDigMinor)
      {
         VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - Invalid HW - dig minor", TRUE);
         pDevCtxt->bHardwareSupported = FALSE;
      }
   }

   // Check the analog version information
   if (pDevCtxt->revisionInfo.uAnalogMajor < pDevCtxt->pBsp->uMinAnaMajor)
   {
      VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - Invalid HW - ana major", TRUE);
      pDevCtxt->bHardwareSupported = FALSE;
   }
   else if (pDevCtxt->revisionInfo.uAnalogMajor == pDevCtxt->pBsp->uMinAnaMajor)
   {
      if (pDevCtxt->revisionInfo.uAnalogMinor < pDevCtxt->pBsp->uMinAnaMinor)
      {
         VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - Invalid HW - ana minor", TRUE);
         pDevCtxt->bHardwareSupported = FALSE;
      }
   }

   if (pDevCtxt->bHardwareSupported == FALSE)
   {
      return DAL_SUCCESS;
   }

#ifndef VADC_UNIT_TEST
   /* Map static MPPs */
   for (uChannelIdx = 0; uChannelIdx < pDevCtxt->pBsp->uNumChannels; uChannelIdx++)
   {
      pChConfig = &pDevCtxt->pBsp->paChannels[uChannelIdx];

      if (pChConfig->eMppConfig == VADC_CHANNEL_MPP_CONFIG_STATIC)
      {
         if (pChConfig->eMpp == PM_MPP_INVALID ||
             pChConfig->eChSelect == PM_MPP__AIN__CH_INVALID)
         {
            VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - invalid MPP configuration", TRUE);
            return DAL_ERROR;
         }

         pmResult = pm_mpp_config_analog_input((uint8)pDevCtxt->pBsp->uPmicDevice,
                                               pChConfig->eMpp,
                                               pChConfig->eChSelect);
         if (pmResult != PM_ERR_FLAG__SUCCESS)
         {
            VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - failed to configure static MPP", TRUE);
            return DAL_ERROR;
         }
      }
   }
#endif

   /* The device needs to be calibrated prior to TM init */
   ret = VAdc_CalibrateInternal(pDevCtxt);
   if (ret != DAL_SUCCESS)
   {
      VAdc_LogError(&pDevCtxt->vAdcDebug, "VAdc - failed to calibrate", TRUE);
      return ret;
   }

   return DAL_SUCCESS;
}

DALResult
VAdc_DeviceDeInit(VAdcClientCtxt *pCtxt)
{
   VAdcDevCtxt *pDevCtxt = pCtxt->pVAdcDevCtxt;
   DALResult result;
   DALResult retResult = DAL_SUCCESS;

   /* free memory */
   if (pDevCtxt->paCalibData != NULL)
   {
      result = DALSYS_Free(pDevCtxt->paCalibData);
      if (result != DAL_SUCCESS)
      {
         retResult = DAL_ERROR;
      }
      pDevCtxt->paCalibData = NULL;
   }

   return retResult;
}

DALResult
VAdc_Open(
   VAdcClientCtxt *pCtxt,
   uint32 dwaccessMode
   )
{
   return DAL_SUCCESS;
}

DALResult
VAdc_Close(VAdcClientCtxt *pCtxt)
{
   return DAL_SUCCESS;
}

/*----------------------------------------------------------------------------
 * The following functions are extended in DalAdcDevice Interface.
 *
 * These functions are documented in DDIAdcDevice.h
 * -------------------------------------------------------------------------*/
DALResult
VAdc_GetDeviceProperties(
   VAdcClientCtxt *pCtxt,
   AdcDeviceDevicePropertiesType *pAdcDeviceProp
   )
{
   VAdcDevCtxt *pDevCtxt = pCtxt->pVAdcDevCtxt;

   pAdcDeviceProp->uNumChannels = pDevCtxt->pBsp->uNumChannels;

   return DAL_SUCCESS;
}

DALResult
VAdc_GetChannel(
   VAdcClientCtxt *pCtxt,
   const char *pszChannelName,
   uint32 *puChannelIdx
   )
{
   VAdcDevCtxt *pDevCtxt = pCtxt->pVAdcDevCtxt;
   uint32 uNumChannels;
   uint32 uChannel;
   const char *pszCurrentChannelName;

   if (pDevCtxt->bHardwareSupported == FALSE)
   {
      return DAL_ERROR;
   }

   uNumChannels = pDevCtxt->pBsp->uNumChannels;

   for (uChannel = 0; uChannel < uNumChannels; uChannel++)
   {
      pszCurrentChannelName = pDevCtxt->pBsp->paChannels[uChannel].pName;

      if (AsciiStrCmp(pszChannelName, pszCurrentChannelName) == 0)
      {
         *puChannelIdx = uChannel;
         return DAL_SUCCESS;
      }
   }

   return DAL_ERROR;
}

DALResult
VAdc_ReadChannel(
   VAdcClientCtxt *pCtxt,
   uint32 uChannelIdx,
   AdcDeviceResultType *pAdcDeviceResult
   )
{
   DALResult result;
   VAdcDevCtxt *pDevCtxt = pCtxt->pVAdcDevCtxt;
   uint32 uNumChannels;

   pAdcDeviceResult->eStatus = ADC_DEVICE_RESULT_INVALID;

   if (pDevCtxt->bHardwareSupported == FALSE)
   {
      return DAL_ERROR;
   }

   uNumChannels = pDevCtxt->pBsp->uNumChannels;

   if (uChannelIdx >= uNumChannels)
   {
      pAdcDeviceResult->eStatus = ADC_DEVICE_RESULT_INVALID_CHANNEL;

      return DAL_ERROR;
   }

   VAdc_LogConversionRequest(&pDevCtxt->vAdcDebug, uChannelIdx);

   result = VAdc_ReadAdcChannel(pDevCtxt, uChannelIdx, pAdcDeviceResult);
   if (result != DAL_SUCCESS)
   {
      return result;
   }

   return DAL_SUCCESS;
}

DALResult
VAdc_RecalibrateChannel(
   VAdcClientCtxt *pCtxt,
   uint32 uChannelIdx,
   AdcDeviceRecalibrateResultType *pAdcDeviceRecalibrateResult
   )
{
   const VAdcChannelConfigType *pChConfig;
   VAdcDevCtxt *pDevCtxt = pCtxt->pVAdcDevCtxt;
   VAdcCalibDataType *pCalibData;
   VAdcStatusType convStatus;
   uint32 uConfigIdx;
   DALResult result;

   pAdcDeviceRecalibrateResult->eStatus = ADC_DEVICE_RESULT_INVALID;

   if (pDevCtxt->bHardwareSupported == FALSE)
   {
      return DAL_ERROR;
   }

   if (uChannelIdx >= pDevCtxt->pBsp->uNumChannels)
   {
      pAdcDeviceRecalibrateResult->eStatus = ADC_DEVICE_RESULT_INVALID_CHANNEL;

      return DAL_ERROR;
   }

   VAdc_LogRecalibrationRequest(&pDevCtxt->vAdcDebug, uChannelIdx);

   pChConfig = &pDevCtxt->pBsp->paChannels[uChannelIdx];
   uConfigIdx = pChConfig->uConfigIdx;
   pCalibData = &pDevCtxt->paCalibData[uConfigIdx];

   if (pChConfig->eCalMethod == VADC_CAL_METHOD_ABSOLUTE)
   {
      /* VREF1 */
      pChConfig = &pDevCtxt->pBsp->paCalChannels[2];
      result = VAdc_ReadAdc(pDevCtxt, pChConfig, uConfigIdx, &convStatus, &pCalibData->uVref1);
      if (result != DAL_SUCCESS)
      {
         return result;
      }

      /* VREF2 */
      pChConfig = &pDevCtxt->pBsp->paCalChannels[3];
      result = VAdc_ReadAdc(pDevCtxt, pChConfig, uConfigIdx, &convStatus, &pCalibData->uVref2);
      if (result != DAL_SUCCESS)
      {
         return result;
      }

      /* Absolute calibration */
      pAdcDeviceRecalibrateResult->uCode1 = pCalibData->uVref1;
      pAdcDeviceRecalibrateResult->uCode2 = pCalibData->uVref2;
      pAdcDeviceRecalibrateResult->nPhysical1_uV = (int32)(pDevCtxt->pBsp->uVref1_mv * 1000);
      pAdcDeviceRecalibrateResult->nPhysical2_uV = (int32)(pDevCtxt->pBsp->uVref2_mv * 1000);
      pAdcDeviceRecalibrateResult->eStatus = ADC_DEVICE_RESULT_VALID;
   }
   else
   {
      /* VREFP */
      pChConfig = &pDevCtxt->pBsp->paCalChannels[0];
      result = VAdc_ReadAdc(pDevCtxt, pChConfig, uConfigIdx, &convStatus, &pCalibData->uVrefP);
      if (result != DAL_SUCCESS)
      {
         return result;
      }

      /* VREFN */
      pChConfig = &pDevCtxt->pBsp->paCalChannels[1];
      result = VAdc_ReadAdc(pDevCtxt, pChConfig, uConfigIdx, &convStatus, &pCalibData->uVrefN);
      if (result != DAL_SUCCESS)
      {
         return result;
      }

      /* Ratiometric calibration */
      pAdcDeviceRecalibrateResult->uCode1 = pCalibData->uVrefN;
      pAdcDeviceRecalibrateResult->uCode2 = pCalibData->uVrefP;
      pAdcDeviceRecalibrateResult->nPhysical1_uV = (int32)(pDevCtxt->pBsp->uVrefN_mv * 1000);
      pAdcDeviceRecalibrateResult->nPhysical2_uV = (int32)(pDevCtxt->pBsp->uVrefP_mv * 1000);
      pAdcDeviceRecalibrateResult->eStatus = ADC_DEVICE_RESULT_VALID;
   }

   return DAL_SUCCESS;
}

