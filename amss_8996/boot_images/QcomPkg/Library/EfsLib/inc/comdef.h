/*==============================================================================
  @file comdef.h

  Declarations for portability.

  Copyright (c) 2014 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary


when       who          what, where, why
--------   ---          -------------------------------------------------- 
08/26/14   vm           Created.
============================================================================*/ 

#ifndef COMDEF_H
#define COMDEF_H

#include "com_dtypes.h"

#define PACKED_POST __attribute__((__packed__))
#define wchar CHAR16

#endif /* BOOT_COMDEF_H */
