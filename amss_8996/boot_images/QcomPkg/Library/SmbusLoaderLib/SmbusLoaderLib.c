/** @file SmbusLoaderLib.c

  Smbus Loader Lib code

  Copyright (c) 2014 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential

**/
#include <SmbusSharedLib.h>

EFI_STATUS
EFIAPI
Execute (
  IN              i2c_instance                  Instance,
  IN              EFI_SMBUS_DEVICE_ADDRESS      SlaveAddress,
  IN              EFI_SMBUS_DEVICE_COMMAND      Command,
  IN              EFI_SMBUS_OPERATION           Operation,
  IN              BOOLEAN                       PecCheck,
  IN OUT  UINTN                                 *Length,
  IN OUT  VOID                                  *Buffer
){
  i2c_status              I2cStatus;
  EFI_STATUS              Status;
  EFI_I2C_REQUEST_PACKET  *rp = NULL;
  i2c_transfer_list       Transfers[2];
  void**                  i2c_handle = NULL;
  uint32                  written;
  uint32                  read;
  i2c_config              I2cConfig;

  // Create a request packet
  Status = GetRequestPacket(Command, Operation, PecCheck, SlaveAddress, Length, Buffer, &rp);
  if(EFI_ERROR(Status)){
    goto Done;
  }

  // Create a transfer packet from the request packet
  Status = ConvertRequestPacketToTransferPacket(rp, (i2c_transfer_list*)Transfers);
  if(EFI_ERROR(Status)){
    goto Done;
  }

  // Open a handle
  I2cStatus = i2c_open(Instance, i2c_handle);
  Status = I2cStatusToEfiStatus(I2cStatus);
  if(EFI_ERROR(Status)){
    goto Done;
  }

  //
  // Set the default controller frequency, slave address, and slave type
  //
  I2cConfig.bus_frequency_khz    = QUPE_DEFAULT_BUS_FREQ_KHZ;
  I2cConfig.slave_address        = 0;
  I2cConfig.slave_address_type   = I2C_07_BIT_SLAVE_ADDRESS;

  // Perform the transfer
  I2cStatus = i2c_transfer(
                  i2c_handle,
                  &I2cConfig,
                  Transfers,
                  rp->OperationCount,
                  &written,
                  &read,
                  2000);
  Status = I2cStatusToEfiStatus(I2cStatus);
  if(EFI_ERROR(Status)){
    goto Done;
  }

  // Populate the return buffer
  Status = ProcessResultsFromTransferList(
                                Operation,
                                Transfers,
                                PecCheck,
                                Length,
                                Buffer);

  // Close the opened instance
  i2c_close(i2c_handle);

Done:
  return Status;
}


EFI_STATUS
EFIAPI
ArpDevice(
  IN CONST    EFI_SMBUS_HC_PROTOCOL     *This,
  IN          BOOLEAN                   ArpAll,
  IN          EFI_SMBUS_UDID            *SmbusUdid, OPTIONAL
  IN OUT      EFI_SMBUS_DEVICE_ADDRESS  *SlaveAddress OPTIONAL
){
  return EFI_UNSUPPORTED;
}


EFI_STATUS
EFIAPI
GetArpMap (
  IN CONST  EFI_SMBUS_HC_PROTOCOL   *This,
  IN OUT    UINTN                   *Length,
  IN OUT    EFI_SMBUS_DEVICE_MAP    **SmbusDeviceMap
){
  return EFI_UNSUPPORTED;
}


EFI_STATUS
EFIAPI
Notify (
  IN  CONST EFI_SMBUS_HC_PROTOCOL       *This,
  IN        EFI_SMBUS_DEVICE_ADDRESS    SlaveAddress,
  IN        UINTN                       Data,
  IN        EFI_SMBUS_NOTIFY_FUNCTION   NotifyFunction
){
  return EFI_UNSUPPORTED;
}

