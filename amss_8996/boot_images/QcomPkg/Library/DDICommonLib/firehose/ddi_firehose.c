/*==================================================================
 *
 * FILE:        ddi_firehose.c
 *
 * DESCRIPTION:
 *
 *
 *        Copyright � 2008-2015 Qualcomm Technologies Incorporated.
 *               All Rights Reserved.
 *               QUALCOMM Proprietary
 *==================================================================*/

/*===================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *
 * YYYY-MM-DD   who     what, where, why
 * ----------   ---     ----------------------------------------------
 * 2015-04-15   bn      Added GetStorageInfo support
 * 2015-03-26   wek     Test DDR if fails use Lite buffers to communicate.
 * 2015-02-18   wek     Zero initalize different set of buffers.
 * 2015-01-20   wek     Avoid NULL dereference.
 * 2014-12-02   wek     8996/XBL Compilation.
 * 2014-10-02   ah      Major code clean up
 * 2014-03-03   dks     Added MMC FFU Support
 * 2013-06-03   ah      Added legal header
 * 2013-05-31   ab      Initial checkin
 */

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "crc.h"
#include "stringl/stringl.h"
#include "ddi_firehose.h"
#include "ddr_drivers.h"

#include "usb_wrapper.h"
#include "boot_sahara.h"
#include "ClockBoot.h"
#include "busywait.h"



#define MAX_PARAM_TYPE_NAME_SIZE 16
#define MAX_CMD_NAME_SIZE 32
#define MAX_RSP_NAME_SIZE 32
#define MAX_PARAM_NAME_SIZE 32
char str[100] = {0};





struct ddiCmdsStruct
{
        char    cmdName[MAX_CMD_NAME_SIZE];
        SIZE_T  version;
        SIZE_T  paremetersNum;
        SIZE_T  responseNum;
};

struct ddiParamStruct
{
        char    cmdName[MAX_CMD_NAME_SIZE];
        char    paramName[MAX_PARAM_NAME_SIZE];
        char    pType[MAX_PARAM_TYPE_NAME_SIZE];
        int     minVal;
        int     maxVal;
        int     defaultVal;

};


char Pattern[3500];

struct QduttParamStruct
{
        char    cmdName[MAX_CMD_NAME_SIZE];
        char    paramName[MAX_PARAM_NAME_SIZE];
        char    val[256];
};

struct ddiResponseStruct
{
        char            cmdName[MAX_CMD_NAME_SIZE];
        char            RespName[MAX_RSP_NAME_SIZE];
        char            rType[MAX_PARAM_TYPE_NAME_SIZE];
};

uint32 dataBuffer[256] __attribute__ ((section (".bss.DDITRAINING_BUFFER_SECTION"))) = {0};
uint16 patternElemCnt;

//21 commands
struct ddiCmdsStruct SupportedCmds[] = {
        {"OwnAddressRWTest",        1,  2,  1},
        {"AddressLinesTest",        1,  2,  1},
        {"DataLinesTest",           1,  2,  1},
        {"ReadTest",                1,  5,  1},
        {"WriteTest",               1,  5,  1},
        {"ReadWriteTest",           1,  5,  1},
        {"Retrain",                 1,  0,  1},
        {"ChangeBIMCPhySettings",   1,  7,  1},
        //{"ReadMRReg",               1,  3,  1},
        {"ReadPowerSetting",        1,  0,  1},
        {"ReadPhySetting",          1,  2,  4},
        {"ReadTemperature",         1,  0,  1},
        {"ChangeBIMCClock",         1,  1,  1},
        {"ChangeVDDQ",              1,  1,  1},
        {"ChangeVDD1",              1,  1,  1},
        {"DisableDBI",              1,  0,  1},
        {"ChangeMR3",               1,  2,  1},
        {"ChangeMR11",              1,  2,  1},
        //{"ChangeMR22",              1,  2,  1},
        {"MRWrite",                 1,  4,  1},
        };


struct ddiResponseStruct SupportedResp[] = {
        {"OwnAddressRWTest",        "Pass",                 "bool"},
        {"AddressLinesTest",        "Pass",                 "bool"},
        {"DataLinesTest",           "Pass",                 "bool"},
        {"ReadTest",                "Pass",                 "bool"},
        {"WriteTest",               "Pass",                 "bool"},
        {"ReadWriteTest",           "Pass",                 "bool"},
        {"Retrain",                 "Pass",                 "bool"},
        {"ChangeBIMCPhySettings",   "Pass",                 "bool"},
       // {"ReadMRReg",               "MRRegVal",             "uint32"},
        {"ReadPowerSetting",        "Pass",                 "bool"},
        {"ReadPhySetting",          "OverDrive",            "uint32"},
        {"ReadPhySetting",          "ODT",                  "uint32"},
        {"ReadPhySetting",          "PullUp",               "uint32"},
        {"ReadPhySetting",          "VoH",                  "uint32"},
        {"ReadTemperature",         "Pass",                  "bool"},
        {"ChangeBIMCClock",         "Pass",                 "bool"},
        {"ChangeVDDQ",              "Pass",                 "bool"},
        {"ChangeVDD1",              "Pass",                 "bool"},
        {"DisableDBI",              "Pass",                 "bool"},
        {"ChangeMR3",               "Pass",                 "bool"},
        {"ChangeMR11",              "Pass",                 "bool"},
       // {"ChangeMR22",              "Pass",                 "bool"},
        {"MRWrite",                 "Pass",                 "bool"},
        };


struct ddiParamStruct SupportedParams[] = {
        {"OwnAddressRWTest",        "SDRamInterface",           "uint8",          0,          1,          0},
        {"OwnAddressRWTest",        "SDRamChipSelect",          "uint8",          0,          1,          0},
        {"AddressLinesTest",        "SDRamInterface",           "uint8",          0,          1,          0},
        {"AddressLinesTest",        "SDRamChipSelect",          "uint8",          0,          1,          0},
        {"DataLinesTest",           "SDRamInterface",           "uint8",          0,          1,          0},
        {"DataLinesTest",           "SDRamChipSelect",          "uint8",          0,          1,          0},
        {"ReadTest",                "SDRamInterface",           "uint8",          0,          2,          0},
        {"ReadTest",                "SDRamChipSelect",          "uint8",          0,          2,          0},
        {"ReadTest",                "LoopCount",                "uint32",          1,          100000000,  1},
        {"ReadTest",                "InfinityFlag",             "bool",       -1,          -1,         0}, //"true/false"
        {"ReadTest",                "Pattern",                  "array_uint32",  16,       256,        -1},
        {"WriteTest",               "SDRamInterface",           "uint8",          0,          2,          0},
        {"WriteTest",               "SDRamChipSelect",          "uint8",          0,          2,          0},
        {"WriteTest",               "LoopCount",                "uint32",          1,          100000000,  1},
        {"WriteTest",               "InfinityFlag",             "bool",       -1,          -1,         0},
        {"WriteTest",               "Pattern",                  "array_uint32",  16,       256,        -1},
        {"ReadWriteTest",           "SDRamInterface",           "uint8",          0,          2,          0},
        {"ReadWriteTest",           "SDRamChipSelect",          "uint8",          0,          2,          0},
        {"ReadWriteTest",           "LoopCount",                "uint32",         1,          100000000,  1},
        {"ReadWriteTest",           "InfinityFlag",             "bool",       -1,          -1,         0},
        {"ReadWriteTest",           "Pattern",                  "array_uint32",  16,       256,        -1},
        {"ChangeBIMCPhySettings",   "TestLine",                 "uint8",           0,          3,          0},
        //##TesLine could be DQ(0), DQs(1), CA(2), CK(3)
        {"ChangeBIMCPhySettings",   "PRFSLevel",                "uint8",           0,          7,          0},
        {"ChangeBIMCPhySettings",   "OverDrive",                "uint8",           0,          7,          0},
        {"ChangeBIMCPhySettings",   "PullUp",                   "uint8",           0,          7,          0},
        {"ChangeBIMCPhySettings",   "ODT",                      "uint8",           0,          7,          0},
        {"ChangeBIMCPhySettings",   "VoH",                      "uint8",           0,          3,          0},
        {"ChangeBIMCPhySettings",   "EN_Term",                  "bool",        -1,          -1,         0},
       // {"ReadMRReg",               "SDRamInterface",           "uint8",            0,          1,          0},
       // {"ReadMRReg",               "SDRamChipSelect",          "uint8",            0,          1,          0},
      //  {"ReadMRReg",               "RegNum",                   "uint8",            0,          40,         5},
        {"ReadPhySetting",          "TestLine",                 "uint8",           0,          3,           0},
        {"ReadPhySetting",          "PRFSLevel",                "uint8",          0,          7,           0},
        {"ChangeBIMCClock",         "ClockValue",               "uint8",          100,         1800,        770},
        //##100,150,200,300,400,550,680,770,1000,1500,1800
        {"ChangeVDDQ",              "VDDQValue",                "uint32",          1060,         1160,        1060},
       //## 0 means increase , 1 means decrease
        {"ChangeVDD1",              "VDD1Value",                "uint32",          1700,         1900,        0},
        {"ChangeMR3",              "Freq",                      "uint8",          0,              3,          0},
        //##770, 1000,  1500, 1800
        {"ChangeMR3",              "mr3Val",                    "uint8",          0,              256,          -1},

        {"ChangeMR11",              "Freq",                     "uint8",          0,              3,          0},
        {"ChangeMR11",              "mr11Val",                   "uint8",          0,             256,          -1},

        //{"ChangeMR22",              "Freq",                     "uint8",          0,              3,          0},
        //{"ChangeMR22",              "CA_D",                     "uint8",          0,              1,          0},
        //{"ChangeMR22",              "CS_E",                      "uint8",          0,              1,          0},
        //{"ChangeMR22",              "CK_ODTD",                  "uint8",          0,              1,          0},
        //{"ChangeMR22",              "CODT",                     "uint8",          0,              7,          3},
        {"MRWrite",                 "SDRamInterface",           "uint8",           0,             1,          0},
        {"MRWrite",                 "SDRamChipSelect",          "uint8",            0,            1,          0},
        {"MRWrite",                 "RegNum",                   "uint8",            0,            40,          5},
        {"MRWrite",                 "RegValue",                 "uint8",           0,            255,          -1},

        };



struct QduttParamStruct inComingParams[] = {
        {"OwnAddressRWTest",        "SDRamInterface",                           "" },
        {"OwnAddressRWTest",        "SDRamChipSelect",                          "" },
        {"AddressLinesTest",        "SDRamInterface",                           "" },
        {"AddressLinesTest",        "SDRamChipSelect",                          "" },
        {"DataLinesTest",           "SDRamInterface",                           "" },
        {"DataLinesTest",           "SDRamChipSelect",                          "" },
        {"ReadTest",                "SDRamInterface",                           "" },
        {"ReadTest",                "SDRamChipSelect",                          "" },
        {"ReadTest",                "LoopCount",                                "" },
        {"ReadTest",                "InfinityFlag",                             "" },
        {"ReadTest",                "Pattern",                                  "" },
        {"WriteTest",               "SDRamInterface",                           "" },
        {"WriteTest",               "SDRamChipSelect",                          "" },
        {"WriteTest",               "LoopCount",                                "" },
        {"WriteTest",               "InfinityFlag",                             "" },
        {"WriteTest",               "Pattern",                                  "" },
        {"ReadWriteTest",           "SDRamInterface",                           "" },
        {"ReadWriteTest",           "SDRamChipSelect",                          "" },
        {"ReadWriteTest",           "LoopCount",                                "" },
        {"ReadWriteTest",           "InfinityFlag",                             "" },
        {"ReadWriteTest",           "Pattern",                                  "" },
        {"ChangeBIMCPhySettings",   "TestLine",                                 "" },
        //##TesLine could be DQ(0), DQs(1), CA(2), CK(3)
        {"ChangeBIMCPhySettings",   "PRFSLevel",                                "" },
        {"ChangeBIMCPhySettings",   "OverDrive",                                "" },
        {"ChangeBIMCPhySettings",   "PullUp",                                   "" },
        {"ChangeBIMCPhySettings",   "ODT",                                      "" },
        {"ChangeBIMCPhySettings",   "VoH",                                      "" },
        {"ChangeBIMCPhySettings",   "EN_Term",                               "" },
      //  {"ReadMRReg",               "SDRamInterface",                           "" },
       // {"ReadMRReg",               "SDRamChipSelect",                          "" },
       // {"ReadMRReg",               "RegNum",                                   "" },
        {"ReadPhySetting",          "TestLine",                                 "" },
        {"ReadPhySetting",          "PRFSLevel",                                "" },
        {"ChangeBIMCClock",         "ClockValue",                               "" },
        //##100,150,200,300,400,550,680,770,1000,1500,1800
        //## 0 means increase , 1 means decrease
        {"ChangeVDDQ",              "VDDQValue",                                "" },
        //## 0 means increase , 1 means decrease
        {"ChangeVDD1",              "VDD1Value",                                "" },
        {"ChangeMR3",              "Freq",                                      "" },
        //##770, 1000,  1500, 1800
        {"ChangeMR3",              "mr3Val",                                      "" },
        {"ChangeMR11",              "Freq",                                     "" },
        {"ChangeMR11",              "mr11Val",                                    "" },

        //{"ChangeMR22",              "Freq",                                     "" },
        //{"ChangeMR22",              "CA-ODTD",                                  "" },
        //{"ChangeMR22",              "CS-ODTE",                                  "" },
        //{"ChangeMR22",              "CK-ODTE",                                  "" },
        //{"ChangeMR22",              "CODT",                                     "" },

        {"MRWrite",                 "SDRamInterface",                           "" },
        {"MRWrite",                 "SDRamChipSelect",                          "" },
        {"MRWrite",                 "RegNum",                                   "" },
        {"MRWrite",                 "RegValue",                                 "" },

 };

//#include "CeTL_sha2_sw.h"
//void InitTXBufferWithXMLHeader(void);
void InitBufferWithXMLHeader(byte *MyBuffer, SIZE_T Length);
void CloseTXBuffer(void);
//void AppendToTXBuffer(char *buf);
void AppendToBuffer(byte *MyBuffer, char *buf, SIZE_T MaxBufferSize);
//char * CleanStringForPC(char *p, SIZE_T  Length);
char * RemoveBannedChars(char *p);

#define MAX_TAG_NAME_LENGTH 30
#define MAX_TAG_ATTR_LENGTH 80



char NewTagName[MAX_TAG_NAME_LENGTH+1];
char CurrentTagName[MAX_TAG_NAME_LENGTH+1];
char ParseAttributes = 1; // this changes from 0 to 1 during the program






// These buffers are being allocated separately so as to make them easy to relocate
// because these are currently accessed directly by the hardware


/* Buffers that are allocated in DDR */
byte ddr_main_channel_buffer_A[FIREHOSE_DDR_CHANNEL_BUFFER_SIZE]  __attribute__ ((section (".bss.FIREHOSE_CHANNEL_A_BUFFER"))) = {0};
byte ddr_main_channel_buffer_B[FIREHOSE_DDR_CHANNEL_BUFFER_SIZE]  __attribute__ ((section (".bss.FIREHOSE_CHANNEL_B_BUFFER"))) = {0};

/* Buffers that are allocated in On chip memory. */
byte lite_main_channel_buffer_A[FIREHOSE_LITE_CHANNEL_BUFFER_SIZE]  __attribute__ ((section (".bss.LITE_CHANNEL_BUFFER_A_SECTION"))) = {0};
byte lite_main_channel_buffer_B[FIREHOSE_LITE_CHANNEL_BUFFER_SIZE]  __attribute__ ((section (".bss.LITE_CHANNEL_BUFFER_B_SECTION"))) = {0};


byte transmit_buffer[FIREHOSE_TX_BUFFER_SIZE]  __attribute__ ((section (".bss.TRANSMIT_BUFFER_SECTION"))) = {0};
byte* packet;


char temp_buffer[FIREHOSE_TEMP_BUFFER_SIZE];

//static byte storage_raw_data_buff[STORAGE_DEVICE_INFO_BUFFER_SIZE];

//firehose_protocol_t fh;   // this can be a big structure
firehose_protocol_t fh __attribute__ ((section (".bss.FIREHOSE_PROTOCOL_STRUCT")));

#ifdef USE_FIRMWARE_BUFFER
#define BIG_FW_BUFFER_SIZE (1024*512)
byte BigFWBuffer[BIG_FW_BUFFER_SIZE] __attribute__ ((section (".bss.DDR_CHANNEL_BUFFER_A_SECTION"))) = {0};
SIZE_T BigFWBufferCount = 0, BigFWBufferExpected = 0;
#endif


SIZE_T ConfigureCommandNotReceivedYet = 1;  // allow VERBOSE until user turns it off

struct Error_Struct
{
    unsigned int ErrorNum;
    char *pErrorMessage;
};

struct Error_Struct AllErrorsStruct[] =
{
    { FAILURE_CHIP_SERIAL_NUMBER,       "FAILURE_CHIP_SERIAL_NUMBER"},
    { AUTHENTICATE_IMAGE_FAILURE,       "AUTHENTICATE_IMAGE_FAILURE"},
    { UFS_COMMIT_EXTRAS_FAILURE,        "UFS_COMMIT_EXTRAS_FAILURE"},
    { SET_BOOTABLE_PARTITION_FAILURE,   "SET_BOOTABLE_PARTITION_FAILURE"},
    { STORAGE_READ_FAILURE,             "STORAGE_READ_FAILURE"},
    { STORAGE_WRITE_FAILURE,            "STORAGE_WRITE_FAILURE"},
    { STORAGE_DEVICE_ERASE_FAILURE,     "STORAGE_DEVICE_ERASE_FAILURE"},
    { EMMC_COMMIT_EXTRAS_FAILURE,       "EMMC_COMMIT_EXTRAS_FAILURE"},
    { EMMC_FW_UPDATE_FAILURE,           "EMMC_FW_UPDATE_FAILURE"},
    { USB_READ_FAILURE,                 "USB_READ_FAILURE"},
    { USB_WRITE_FAILURE,                "USB_WRITE_FAILURE"},
    { DIGITAL_SIGNATURE_DID_NOT_PASS,   "DIGITAL_SIGNATURE_DID_NOT_PASS"},
    { PACKET_HASH_MISMATCH,             "PACKET_HASH_MISMATCH"},
    { HANDLE_CONFIGURE_FAILURE,         "HANDLE_CONFIGURE_FAILURE"},
    { HANDLE_STORAGE_INFO_FAILURE,      "HANDLE_STORAGE_INFO_FAILURE"},
    { HANDLE_PEEK_FAILURE,              "HANDLE_PEEK_FAILURE"},
    { HANDLE_POKE_FAILURE,              "HANDLE_POKE_FAILURE"},
    { HANDLE_ERASE_FAILURE,             "HANDLE_ERASE_FAILURE"},
    { HANDLE_FIRMWARE_PROGRAM_FAILURE,  "HANDLE_FIRMWARE_PROGRAM_FAILURE"},
    { HANDLE_PATCH_FAILURE,             "HANDLE_PATCH_FAILURE"},
    { HANDLE_GET_DIGEST_FAILURE,        "HANDLE_GET_DIGEST_FAILURE"},
    { HANDLE_PROGRAM_FAILURE,           "HANDLE_PROGRAM_FAILURE"},
    { XML_PACKET_TOO_LARGE,             "XML_PACKET_TOO_LARGE"},
    { SERIAL_NUM_OF_DEVICE_INCORRECT,   "SERIAL_NUM_OF_DEVICE_INCORRECT"},
};


#define MAX_ATTR_NAME_SIZE 32
#define MAX_ATTR_RAW_SIZE 64
struct Attributes_Struct
{
        char    Name[MAX_ATTR_NAME_SIZE];               // i.e. "MaxPayloadSizeToTargetInBytes"
        char    Raw[MAX_ATTR_RAW_SIZE];   // i.e. "8192"
        void   *pValue;
        SIZE_T  Min;
        SIZE_T  Max;
        SIZE_T  MultipleOf;
    SIZE_T  SizeOfpStr;
        char    *pStr;
        char    Type;           // i.e. "i" for integer, "s" for string, "x" for special, "t" for SIZE_T
        char    Assigned;       // i.e. Flag indicating if it was just assigned
};


// Declare the variables we care about
storage_device_extras_t storage_extras;

struct UFS_LUN_Var
{
    SIZE_T LUNum;
    SIZE_T bLUEnable;
    SIZE_T bBootLunID;
    SIZE_T bLUWriteProtect;
    SIZE_T bMemoryType;
    SIZE_T size_in_KB;
    SIZE_T bDataReliability;
    SIZE_T bLogicalBlockSize;
    SIZE_T bProvisioningType;
    SIZE_T wContextCapabilities;
};

struct UFS_LUN_Var UFS_LUN_Var_Struct;


struct Attributes_Struct AllAttributes[] = {
        {"Verbose",                             "", (SIZE_T *)&fh.attrs.Verbose,                       0,0,1,0, NULL, 'i', 0 },
        {"MaxPayloadSizeToTargetInBytes",       "", (SIZE_T *)&fh.attrs.MaxPayloadSizeToTargetInBytes, 0,0,512,0, NULL, 'i', 0 },
        {"MaxDigestTableSizeInBytes",           "", (SIZE_T *)&fh.attrs.MaxDigestTableSizeInBytes,     0,0,512,0, NULL, 'i', 0 },
        {"AckRawDataEveryNumPackets",           "", (SIZE_T *)&fh.attrs.AckRawDataEveryNumPackets,     0,0,1,0, NULL, 'i', 0 },
        {"delayinseconds",                      "", (SIZE_T *)&fh.attrs.delayinseconds,                0,0,1,0, NULL, 'i', 0 },
        {"address64",                           "", (SIZE_T *)&fh.attrs.address64,                     0,0,1,0, NULL, 'i', 0 },
        {"value64",                             "", (SIZE_T *)&fh.attrs.value64,                       0,0,1,0, NULL, 'i', 0 },
        //{"storagedrive",                        "", (SIZE_T *)&fh.attrs.storagedrive,                  0,0,1,0, NULL, 'i', 0 },
        {"storagedrive",                        "", (SIZE_T *)&fh.attrs.physical_partition_number,     0,0,1,0, NULL, 'i', 0 },
        {"SECTOR_SIZE_IN_BYTES",                "", (SIZE_T *)&fh.attrs.SECTOR_SIZE_IN_BYTES,          0,0,512,0, NULL, 'i', 0 },
        {"byte_offset",                         "", (SIZE_T *)&fh.attrs.byte_offset,                   0,0,1,0, NULL, 'i', 0 },
        {"physical_partition_number",           "", (SIZE_T *)&fh.attrs.physical_partition_number,     0,0,1,0, NULL, 'i', 0 },
        {"size_in_bytes",                       "", (SIZE_T *)&fh.attrs.size_in_bytes,                 0,0,1,0, NULL, 'i', 0 },
        {"num_partition_sectors",               "", (SIZE_T *)&fh.attrs.num_partition_sectors,         0,0,1,0, NULL, 'i', 0 },
        {"trials",                              "", (SIZE_T *)&fh.attrs.trials,                        0,0,1,0, NULL, 'i', 0 },
        {"file_sector_offset",                  "", (SIZE_T *)&fh.attrs.file_sector_offset,            0,0,1,0, NULL, 'i', 0 },   // 13

        {"ZlpAwareHost",                        "", (SIZE_T *)&fh.attrs.ZlpAwareHost,                  0,0,1,0, NULL, 'i', 0 },
        {"SkipWrite",                           "", (SIZE_T *)&fh.attrs.SkipWrite,                     0,0,1,0, NULL, 'i', 0 },
        {"BufferWrites",                        "", (SIZE_T *)&fh.attrs.BufferWrites,                     0,0,1,0, NULL, 'i', 0 },
        //{"AckRawData",                          "", (SIZE_T *)&fh.attrs.AckRawData,                    0,0,1,0, NULL, 'i', 0 },
        {"AlwaysValidate",                      "", (SIZE_T *)&fh.attrs.AlwaysValidate,                0,0,1,0, NULL, 'i', 0 },
        {"commit",                              "", NULL,                                       0,0,1,sizeof(fh.attrs.Commit), (char *)fh.attrs.Commit, 'x', 0 }, // string convert to fh.attrs.commit
        //{"display",                             "", (SIZE_T *)&fh.attrs.display,                       0,0,1,0, NULL, 'i', 0 },
        //{"read_back_verify",                    "", (SIZE_T *)&fh.attrs.read_back_verify,              0,0,1,0, NULL, 'i', 0 },
        {"TestWritePerformance",                "", (SIZE_T *)&fh.attrs.TestWritePerformance,          0,0,1,0, NULL, 'i', 0 },
        {"TestReadPerformance",                 "", (SIZE_T *)&fh.attrs.TestReadPerformance,           0,0,1,0, NULL, 'i', 0 },
        {"TestDigestPerformance",               "", (SIZE_T *)&fh.attrs.TestDigestPerformance,         0,0,1,0, NULL, 'i', 0 },   // 20
        {"TestDDRValidity",                     "", (SIZE_T *)&fh.attrs.TestDDRValidity,          0,0,1,0, NULL, 'i', 0 },
        {"SkipStorageInit",                     "", (SIZE_T *)&fh.attrs.SkipStorageInit,               0,0,1,0, NULL, 'i', 0 }, // <configure SkipStorageInit="1"
        {"SkipSettingMinClocks",                "", (SIZE_T *)&fh.attrs.SkipSettingMinClocks,          0,0,1,0, NULL, 'i', 0 }, // <configure SkipSettingMinClocks="1"
        {"SkipSettingMaxClocks",                "", (SIZE_T *)&fh.attrs.SkipSettingMaxClocks,          0,0,1,0, NULL, 'i', 0 }, // <configure SkipSettingMaxClocks="1"
        {"actual_size_in_bytes",                "", (SIZE_T *)&fh.attrs.actual_size_in_bytes,          0,0,1,0, NULL, 'i', 0 }, // <configure actual_size_in_bytes="1234"


        // eMMC GPP creation
        {"DRIVE4_SIZE_IN_KB",                   "", (SIZE_T *)&storage_extras.emmc_extras.GPP_size[0],  0,0,512,0, NULL, 'w', 0 },    // 24
        {"DRIVE5_SIZE_IN_KB",                   "", (SIZE_T *)&storage_extras.emmc_extras.GPP_size[1],  0,0,512,0, NULL, 'w', 0 },
        {"DRIVE6_SIZE_IN_KB",                   "", (SIZE_T *)&storage_extras.emmc_extras.GPP_size[2],  0,0,512,0, NULL, 'w', 0 },
        {"DRIVE7_SIZE_IN_KB",                   "", (SIZE_T *)&storage_extras.emmc_extras.GPP_size[3],  0,0,512,0, NULL, 'w', 0 },
        {"ENH_SIZE",                            "", (SIZE_T *)&storage_extras.emmc_extras.ENH_size,     0,0,1,0,   NULL, 'w', 0 },
        {"ENH_START_ADDR",                      "", (SIZE_T *)&storage_extras.emmc_extras.ENH_start_addr, 0,0,1,0,   NULL, 'w', 0 },
        {"GPP_ENH_FLAG",                        "", (SIZE_T *)&storage_extras.emmc_extras.GPP_enh_flag,     0,0,1,0,   NULL, 'b', 0 },    // 30

        // UFS LUN creation
        {"bNumberLU",                           "", (byte *)&storage_extras.ufs_extras.bNumberLU,       0,0,1,0,   NULL, 'b', 0 },
        {"bBootEnable",                         "", (byte *)&storage_extras.ufs_extras.bBootEnable,     0,0,1,0,   NULL, 'b', 0 },
        {"bDescrAccessEn",                      "", (byte *)&storage_extras.ufs_extras.bDescrAccessEn,  0,0,1,0,   NULL, 'b', 0 },
        {"bInitPowerMode",                      "", (byte *)&storage_extras.ufs_extras.bInitPowerMode,  0,0,1,0,   NULL, 'b', 0 },
        {"bHighPriorityLUN",                    "", (byte *)&storage_extras.ufs_extras.bHighPriorityLUN,    0,0,1,0,   NULL, 'b', 0 },
        {"bSecureRemovalType",                  "", (byte *)&storage_extras.ufs_extras.bSecureRemovalType,  0,0,1,0,   NULL, 'b', 0 },
        {"bInitActiveICCLevel",                 "", (byte *)&storage_extras.ufs_extras.bInitActiveICCLevel, 0,0,1,0,   NULL, 'b', 0 },
        {"wPeriodicRTCUpdate",                  "", (short *)&storage_extras.ufs_extras.wPeriodicRTCUpdate,     0,0,1,0,   NULL, 'n', 0 },
        {"bConfigDescrLock",                    "", (byte *)&storage_extras.ufs_extras.bConfigDescrLock,    0,0,1,0,   NULL, 'b', 0 },    // 39

        // These vars are handled in ParseComplicatedAttributes when (CurrentHandlerFunction==handleStorageExtras)
        {"LUNum",                               "", (byte *)&UFS_LUN_Var_Struct.LUNum,                  0,7,1,0,   NULL, 'i', 0 },
        {"bLUEnable",                           "", (byte *)&UFS_LUN_Var_Struct.bLUEnable,              0,0,1,0,   NULL, 'b', 0 },
        {"bBootLunID",                          "", (byte *)&UFS_LUN_Var_Struct.bBootLunID,             0,0,1,0,   NULL, 'b', 0 },
        {"bLUWriteProtect",                     "", (byte *)&UFS_LUN_Var_Struct.bLUWriteProtect,        0,0,1,0,   NULL, 'b', 0 },
        {"bMemoryType",                         "", (byte *)&UFS_LUN_Var_Struct.bMemoryType,            0,0,1,0,   NULL, 'b', 0 },
        {"size_in_KB",                          "", (SIZE_T *)&UFS_LUN_Var_Struct.size_in_KB,           0,0,1,0,   NULL, 'i', 0 },
        {"bDataReliability",                    "", (byte *)&UFS_LUN_Var_Struct.bDataReliability,       0,0,1,0,   NULL, 'b', 0 },
        {"bLogicalBlockSize",                   "", (byte *)&UFS_LUN_Var_Struct.bLogicalBlockSize,      0,0,1,0,   NULL, 'b', 0 },
        {"bProvisioningType",                   "", (byte *)&UFS_LUN_Var_Struct.bProvisioningType,      0,0,1,0,   NULL, 'b', 0 },
        {"wContextCapabilities",                "", (short *)&UFS_LUN_Var_Struct.wContextCapabilities,  0,0,1,0,   NULL, 'n', 0 },    // 49

        {"MemoryName",                          "", NULL,                                       0,0,1,sizeof(fh.attrs.MemoryName), (char *)fh.attrs.MemoryName, 's', 0 },
        {"TargetName",                          "", NULL,                                       0,0,1,sizeof(fh.attrs.TargetName), (char *)fh.attrs.TargetName, 's', 0 },
        {"filename",                            "", NULL,                                       0,0,1,sizeof(fh.attrs.filename), (char *)fh.attrs.filename,   's', 0 },
        {"value",                               "", NULL,                                       0,0,1,sizeof(fh.attrs.value), (char *)fh.attrs.value,      's', 0 },   // 53

        // x means it needs special processing later, as in start_sector="NUM_DISK_SECTORS-33."
        {"start_sector",                        "", (SIZE_T *)&fh.attrs.start_sector,                  0,0,1,0, NULL, 'x', 0 },   // 54
};




//#define XML_HEADER_LENGTH 47  // 47 does NOT include null
#define XML_HEADER_LENGTH 40    // 40 does NOT include null
#define XML_TAIL_LENGTH 9               // 9 DOES include null

static const char xml_header[XML_HEADER_LENGTH] = "<\?xml version=\"1.0\" encoding=\"UTF-8\" \?>\n";

extern uint32 Image$$SBL1_DDI_USB_ZI$$Base;
extern uint32 Image$$SBL1_DDI_USB_ZI$$ZI$$Length;

extern uint32 Image$$SBL1_DDI_FH_ZI$$Base;
extern uint32 Image$$SBL1_DDI_FH_ZI$$ZI$$Length;

int ReturnIncomingParam(void)
{
    return (int)(sizeof(inComingParams)/sizeof(struct QduttParamStruct));
}


int ReturnNumCmds(void)
{
    return (int)(sizeof(SupportedCmds)/sizeof(struct ddiCmdsStruct));
}

int ReturnNumParam(void)
{
    return (int)(sizeof(SupportedParams)/sizeof(struct ddiParamStruct));
}

int ReturnNumResp(void)
{
    return (int)(sizeof(SupportedResp)/sizeof(struct ddiResponseStruct));
}



/* Temporary bringup function to clear the USB Buffers. */
void deviceprogrammer_zi_buffers (void) {

  memset (&fh, 0, sizeof(fh));
  memset (&storage_extras, 0, sizeof(storage_extras));

#ifdef FEATURE_DEVICEPROGRAMMER_DDR
  memset (ddr_main_channel_buffer_A, 0, sizeof(ddr_main_channel_buffer_A));
  memset (ddr_main_channel_buffer_B, 0, sizeof(ddr_main_channel_buffer_B));
#endif

  memset (lite_main_channel_buffer_A, 0, sizeof(lite_main_channel_buffer_A));
  memset (lite_main_channel_buffer_B, 0, sizeof(lite_main_channel_buffer_B));
}

static void firehoseResetValidationVariables() {
    fh.num_packets_received = 0;

    fh.validation_hash_buffer_num_hashes = 0;
    fh.validation_hash_buffer_capacity = sizeof(fh.validation_hash_buffer) /
                        sizeof(fh.validation_hash_value);
    fh.validation_hash_buffer_new_capacity = fh.validation_hash_buffer_capacity;

    fh.validation_hash_buffer_index = 0;
    fh.validation_table_count = 0;
    fh.validation_signed_hash_received = FALSE;
}

int memset_verify (byte *buffer, byte val, int size)
{
  int i;
  for (i = 0; i < size; i++)
  {
    if (buffer[i] != val)
      return -1;
  }
  return 0;
}

int ddr_set_and_test (byte val)
{
  byte *channelA;
  byte *channelB;
  uint32 channel_size;
  int result;

  channelA = ddr_main_channel_buffer_A;
  channelB = ddr_main_channel_buffer_B;
  /* It takes a long time to memset the whole buffers, do just half to save time. */
  channel_size = FIREHOSE_DDR_CHANNEL_BUFFER_SIZE / 2;

  memset (channelA, val, channel_size);
  memset (channelB, val, channel_size);

  result = memset_verify (channelA, val, channel_size);
  if (result != 0)
    return -1;
  result = memset_verify (channelB, val, channel_size);
  if (result != 0)
    return -1;

  return 0;
}

int ddr_fill_and_check_test (void)
{
  byte *channelA;
  byte *channelB;
  uint32 channel_size;
  int i;
  byte val;

  // Rename the variables for easy typing.
  channelA = ddr_main_channel_buffer_A;
  channelB = ddr_main_channel_buffer_B;
  /* It takes a long time to memset the whole buffers, do just half to save time. */
  channel_size = FIREHOSE_DDR_CHANNEL_BUFFER_SIZE / 2;

  val = 0;
  // Fill the buffers with some pattern.
  for (i = 0; i < channel_size; i++)
  {
    channelA[i] = val;
    channelB[i] = val;
    val++;
    // Make the counter wrap around at an odd value. This causes that same
    // value won't fall in the same "column" in memory. If we use a multiple
    // of 2, then the same value will fall in the same colum every time.
    if (val >= 253)
      val = 0;
  }
  val = 0;
  // Verify the pattern that we just filled.
  for (i = 0; i < channel_size; i++)
  {
    /* The buffers should be filled with an specific pattern. Verify that pattern. */
    if (channelA[i] != val || channelB[i] != val)
      return -1;

    val++;
    if (val >= 253)
      val = 0;
  }

  return 0;
}


/* This feature will run some tests on DDR. If the test fail it will fall back
 * and use the Lite buffers and return a NAK for any command sent. */
#ifdef FEATURE_DEVICEPROGRAMMER_INIT_DDR_TEST
static int basic_test_ddr(void)
{
  int result;

  /* Set DDR to some values and read back the pattern to make sure it is there. */
  result = ddr_set_and_test (0x55);
  if (result != 0)
    return -1;
  result = ddr_set_and_test (0xAA);
  if (result != 0)
    return -1;

  /* Fill the buffers with a non repetitive pattern and verify it */
  result = ddr_fill_and_check_test ();
  if (result != 0)
    return -1;

  /* Last do the test filling the memory to zero, esentially initializing it. */
  result = ddr_set_and_test (0x00);
  if (result != 0)
    return -1;

  return 0;
}
#else
static int basic_test_ddr(void)
{
  return 0;
}
#endif





void initFirehoseProtocol()
{
  byte *channelA;
  byte *channelB;
  uint32 channel_size;

  /* Set the default values for the buffers as Lite buffers. */
  channelA = lite_main_channel_buffer_A;
  channelB = lite_main_channel_buffer_B;
  channel_size = FIREHOSE_LITE_CHANNEL_BUFFER_SIZE;
  fh.nak_all_packets = 0;

  /* If the DDR Buffers are defined, verify DDR is good.*/
  if (FIREHOSE_LITE_CHANNEL_BUFFER_SIZE <= FIREHOSE_DDR_CHANNEL_BUFFER_SIZE)
  {
    int result;
    result = basic_test_ddr ();
    if (result == 0)
    {
      // DDR Is good. Use the DDR Buffers instead.
      channelA = ddr_main_channel_buffer_A;
      channelB = ddr_main_channel_buffer_B;
      channel_size = FIREHOSE_DDR_CHANNEL_BUFFER_SIZE;
    }
    else
    {
      fh.nak_all_packets = 1;
    }
  }

    fh.tx_buffer = transmit_buffer;
    // Channel variables
    fh.channel_error_code = 0;
    fh.channel_bytes_read = 0;
    fh.channel_read_completed = FALSE;
    //fh.channel_total_bytes_read = 0;
    fh.channel_write_completed = FALSE;
    //fh.channel_bytes_written = 0;
    //fh.channel_total_bytes_written = 0;

    fh.channel_buffer[0] = channelA;
    fh.channel_buffer[1] = channelB;
    fh.channel_buffer_capacity = channel_size;
    //fh.channel_num_errors = 0;
    fh.channel_buffer_queued = FALSE;
    fh.channel_buffer_to_use = 0;

    fh.debug_log_position = 0;


    //xmlInitReader(&(fh.xml_reader), NULL, 0);
    //xmlInitWriter(&(fh.xml_writer),
    //             fh.tx_buffer,
    //             FIREHOSE_TX_BUFFER_SIZE);
    storage_device_init_struct(&fh.store_dev);

    //fh.skip_storage_writes = FALSE;
    //fh.hash_in_non_secure = FALSE;
    //fh.verbose = FALSE;
    //fh.ack_raw_data_every = 0;

    firehoseResetValidationVariables();
}

void debugMessage(const char* format, ...) {
    char *temp_log_buffer = (char *) fh.debug_log_buffer[fh.debug_log_position];
    SIZE_T buffer_space = FIREHOSE_DEBUG_LOG_MESSAGE_SIZE;

    va_list args;
    va_start (args, format);

    vsnprintf(temp_log_buffer,
              buffer_space,
              format,
              args);
    va_end (args);
    fh.debug_log_position = (fh.debug_log_position + 1) % FIREHOSE_NUM_DEBUG_LOG_MESSAGES;
}

void shutDownUSB()
{

    qusb_al_bulk_shutdown();
    busywait(3000000);

}


static void packet_handle_incoming_buf (uint32 bytes_read, uint32 err_code) {
    debugMessage("cback bytes_read=%u err_code=%u", bytes_read, err_code);
    fh.channel_error_code = err_code;
    if (err_code != BULK_SUCCESS) {
        fh.channel_bytes_read = 0;
        //fh.channel_num_errors += 1;
        //usb_al_bulk_cancel_rx();
    }
    else {
        fh.channel_bytes_read = bytes_read;
        fh.num_packets_received += 1;
    }
    //fh.channel_total_bytes_read += bytes_read;
    fh.channel_read_completed = TRUE;
    return;
}

firehose_error_t readChannel(byte** packet_ptr, SIZE_T bytes_to_read, SIZE_T *bytes_read, boolean queue_only) {
    // Since the 'channel' variables are directly manipulated here, they must not be used by clients such
    // as handleProgram directly. Only the copies made by this function (packet_ptr, bytes_read) must
    // be used by those client functions

    if (fh.channel_buffer_queued == FALSE)
    {
        uint32 frame_size = usb_al_bulk_get_max_packet_size ();
        uint32 usb_pad;
        debugMessage("In readChannel queue %lld", bytes_to_read);

        fh.channel_read_completed = FALSE;
        fh.channel_bytes_read = 0;
        fh.channel_buffer_queued = TRUE;

        // Check if the number of bytes is a multiple of the USB frame size.
        // The frame size depends if it is USB3.0 (Super Seed) or USB2.0
        // (High Speed) and it has to be a multiple of the frame size for the
        // usb_al_bulk_receive to return success.
        usb_pad = bytes_to_read % frame_size;
        if (usb_pad != 0)
        {
            // round up to the USB PAD boundary.
            bytes_to_read += frame_size - usb_pad;
        }

        if (bytes_to_read > fh.channel_buffer_capacity)
        {
            logMessage("readChannel bytes to read larger than buffer capacity %d>%d", bytes_to_read, fh.channel_buffer_capacity);
            return FIREHOSE_ERROR;
        }

        usb_al_bulk_receive(fh.channel_buffer[fh.channel_buffer_to_use], bytes_to_read, packet_handle_incoming_buf, &fh.channel_error_code);

        if (fh.channel_error_code != BULK_SUCCESS)
        {
            logMessage("USB read failed - usb_al_bulk_receive() gave channel error code %u",fh.channel_error_code);
            MyErrorMessage(USB_READ_FAILURE);
            return FIREHOSE_TRANSPORT_ERROR;
        }
    }

    if (queue_only == TRUE)
        return FIREHOSE_SUCCESS;

    while (fh.channel_read_completed == FALSE)
    {
        usb_al_bulk_poll();
    }

    if (fh.channel_bytes_read > fh.channel_buffer_capacity)
    {
        debugMessage("read fh.channel_bytes_read=%i fh.channel_buffer_capacity=%i", fh.channel_bytes_read, fh.channel_buffer_capacity);
        return FIREHOSE_TRANSPORT_ERROR;
    }

    *bytes_read = fh.channel_bytes_read;
    *packet_ptr = fh.channel_buffer[fh.channel_buffer_to_use];
    fh.channel_buffer_queued = FALSE;
    fh.channel_buffer_to_use = 1 - fh.channel_buffer_to_use;

    debugMessage("Leaving readChannel bytes_read %lld", fh.channel_bytes_read);

    if (fh.channel_error_code != BULK_SUCCESS)
    {
        logMessage("USB read failed - usb_al_bulk_receive() gave channel error code %u",fh.channel_error_code);
        MyErrorMessage(USB_READ_FAILURE);
        return FIREHOSE_TRANSPORT_ERROR;
    }

    return FIREHOSE_SUCCESS;
}

static void packet_handle_outgoing_buf(uint32 bytes_written, uint32 err_code) {
    //fh.channel_bytes_written = bytes_written;
    //fh.channel_total_bytes_written += bytes_written;
    fh.channel_error_code = err_code;
    fh.channel_write_completed = TRUE;
    return;
}

static firehose_error_t writeChannel(byte* buffer, SIZE_T length)
{
    fh.channel_write_completed = FALSE;

    debugMessage("bulk_tx() buffer at 0x%X", buffer);
    debugMessage("bulk_tx() length %lld", length);

    usb_al_bulk_transmit(buffer, length, packet_handle_outgoing_buf, &fh.channel_error_code);

    if (fh.channel_error_code != BULK_SUCCESS)
    {
        MyErrorMessage(USB_WRITE_FAILURE);
        return FIREHOSE_TRANSPORT_ERROR;
    }

    while (fh.channel_write_completed == FALSE)
    {
        usb_al_bulk_poll();
    }

    return FIREHOSE_SUCCESS;
}

static void DDITxCB(uint32 bytes_written, uint32 err_code)
{
}



firehose_error_t writeTrainingData(byte* buffer, SIZE_T length)
{

    usb_al_bulk_transmit(buffer, length, DDITxCB, &fh.channel_error_code);

    if (fh.channel_error_code != BULK_SUCCESS)
    {
        logMessage("USB write failed - usb_al_bulk_transmit() gave channel error code %u",fh.channel_error_code);
        MyErrorMessage(USB_WRITE_FAILURE);
        return FIREHOSE_TRANSPORT_ERROR;
    }



    return FIREHOSE_SUCCESS;
}

static firehose_error_t sendTrainingBuffer()
{
    debugMessage("sendTrainingBuffer() fh.tx_buffer at 0x%X", fh.tx_buffer);
    firehose_error_t retval = writeTrainingData(fh.tx_buffer, strlen((const char *)fh.tx_buffer));
    return retval;
}

static firehose_error_t sendTransmitBuffer()
{
    debugMessage("sendTransmitBuffer() fh.tx_buffer at 0x%X", fh.tx_buffer);
    firehose_error_t retval = writeChannel(fh.tx_buffer, strlen((const char *)fh.tx_buffer));
    return retval;
}
// format: C string that contains a format string that follows the same specifications as format in printf

#ifdef _MSC_VER
#define dbg(log_level, fmt, ...) MyLog(log_level, __FUNCTION__, __LINE__, fmt, __VA_ARGS__)
#else
#define dbg(log_level, fmt ...) logMessage(fmt)
#endif


void logMessage(const char* format, ...) {
    int retval;
    int written;
    char temp_log_buffer[FIREHOSE_TEMP_LOG_BUFFER_SIZE] = {0};  // no single message > 256 bytes
    SIZE_T buffer_space = sizeof(temp_log_buffer);

    va_list args;
    va_start (args, format);

    retval = vsnprintf(temp_log_buffer,
                       buffer_space,
                       format,
                       args);
    va_end (args);
    written = MIN(retval, buffer_space - 1); // vsnprintf retval doesn't include null char


    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<log value=\"", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,temp_log_buffer, FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"\"/>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>", FIREHOSE_TX_BUFFER_SIZE);
    sendTransmitBuffer();

}


void sendProtocolInfo(const char* format, ...) {
    int retval;
    int written;
    char temp_log_buffer[FIREHOSE_TEMP_LOG_BUFFER_SIZE] = {0};  // no single message > 256 bytes
    SIZE_T buffer_space = sizeof(temp_log_buffer);

    va_list args;
    va_start (args, format);

    retval = vsnprintf(temp_log_buffer,
                       buffer_space,
                       format,
                       args);
    va_end (args);
    written = MIN(retval, buffer_space - 1); // vsnprintf retval doesn't include null char


    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<Info value=\"", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,temp_log_buffer, FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"\"/>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>", FIREHOSE_TX_BUFFER_SIZE);
    sendTransmitBuffer();

}

void ErrorMessage(unsigned int LineNumber, firehose_error_code_t ErrorCode)
{
    unsigned int i = 0, found = 0;

    for(i=0;i<FH_ERR_MAX;i++)
    {
        if(ErrorCode == AllErrorsStruct[i].ErrorNum)
        {
            found = 1;
            break;
        }
    }

    if(found)
        logMessage("ERROR %d: Line %d: %s", ErrorCode, LineNumber, AllErrorsStruct[i].pErrorMessage );
    else
        logMessage("ERROR %d: Line %d: Unknown error is *not* in AllErrorsStruct", ErrorCode, LineNumber);

}

firehose_error_t sendResponse(response_t response)
{
        if (fh.nak_all_packets != 0)
        {
          logMessage("ERROR: DDR Test failed, pease file a CR for this. Fall back to Lite version while this gets fixed.");
        }

        InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
        AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
        if(response==ACK && fh.nak_all_packets == 0)
        {
                AppendToBuffer(fh.tx_buffer,"<response value=\"ACK\" />\n</data>", FIREHOSE_TX_BUFFER_SIZE);
        }
        else
        {
                AppendToBuffer(fh.tx_buffer,"<response value=\"NAK\" />\n</data>", FIREHOSE_TX_BUFFER_SIZE);
        }

        sendTrainingBuffer();

        if(response==ACK)
                return FIREHOSE_SUCCESS;
        else
                return FIREHOSE_ERROR;


    //return sendResponseFields(response, 0);
}


firehose_error_t sendResponseWithTag(response_t response, char* str)
{
        char tempStr[128];
        if (fh.nak_all_packets != 0)
        {
          logMessage("ERROR: DDR Test failed, pease file a CR for this. Fall back to Lite version while this gets fixed.");
        }

        InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
        AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
        if(response==ACK && fh.nak_all_packets == 0)
        {
            snprintf(tempStr, BUFFER_LENGTH,"<");
            AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE );
            AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
            snprintf(tempStr, BUFFER_LENGTH," value=\"ACK\" />\n</data>");
            AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE);
        }
        else
        {
            snprintf(tempStr, BUFFER_LENGTH,"<");
            AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE );
            AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
            snprintf(tempStr, BUFFER_LENGTH," value=\"NAK\" />\n</data>");
            AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE);
        }

        sendTrainingBuffer();

        if(response==ACK)
                return FIREHOSE_SUCCESS;
        else
                return FIREHOSE_ERROR;


    //return sendResponseFields(response, 0);
}




firehose_error_t sendDDIResponse(response_t response)
{
        if (fh.nak_all_packets != 0)
        {
          logMessage("ERROR: DDR Test failed, pease file a CR for this. Fall back to Lite version while this gets fixed.");
        }

        InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
        AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
        if(response==ACK && fh.nak_all_packets == 0)
        {
                AppendToBuffer(fh.tx_buffer,"<response value=\"ACK\" />\n</data>", FIREHOSE_TX_BUFFER_SIZE);
        }
        else
        {
                AppendToBuffer(fh.tx_buffer,"<response value=\"NAK\" />\n</data>", FIREHOSE_TX_BUFFER_SIZE);
        }

        sendTrainingBuffer();

        if(response==ACK)
                return FIREHOSE_SUCCESS;
        else
                return FIREHOSE_ERROR;


    //return sendResponseFields(response, 0);
}





firehose_error_t sendPHYData(uint8 data, char* dataName)
{
    char tempStr[128];
    char* responseTag = "responseReadPhySetting ";
    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,responseTag, FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,dataName, FIREHOSE_TX_BUFFER_SIZE);

    snprintf(tempStr, BUFFER_LENGTH,"=\"%d\"",data);
    AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE );
    AppendToBuffer(fh.tx_buffer," />\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );





    sendTrainingBuffer();
    return FIREHOSE_SUCCESS;

}




firehose_error_t sendTrainingData(unsigned char* buffer)
{
    busywait(10000);
    char* responseTag = "log value=\"";
    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,responseTag, FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,(char*)buffer, FIREHOSE_TX_BUFFER_SIZE );
    AppendToBuffer(fh.tx_buffer,"\" />\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );

    sendTrainingBuffer();


    return FIREHOSE_SUCCESS;


}
firehose_error_t sendlogWithInfo(char* tagName, uint32 data)
{
    char tempStr[128];
    char* responseTag = "log";
    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,responseTag, FIREHOSE_TX_BUFFER_SIZE);

    snprintf(tempStr, BUFFER_LENGTH," value=\"");
    AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE );
    AppendToBuffer(fh.tx_buffer,tagName, FIREHOSE_TX_BUFFER_SIZE );
    snprintf(tempStr, BUFFER_LENGTH,":%d\"",data);
    AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE );
    AppendToBuffer(fh.tx_buffer," />\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );

    sendTrainingBuffer();

    return FIREHOSE_SUCCESS;

}




firehose_error_t sendTrainingArgs(ddr_training_args* tArgs)
{
    char tempStr[128];
    char* responseTag = "log";

    memset(tempStr,0, 128);
    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,responseTag, FIREHOSE_TX_BUFFER_SIZE);

    snprintf(tempStr, BUFFER_LENGTH," value=\"paramFreq:0x%X\"",tArgs->FreqInKhz);
    AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE );
    AppendToBuffer(fh.tx_buffer," />\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );

    sendTrainingBuffer();

     memset(tempStr,0, 128);
    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,responseTag, FIREHOSE_TX_BUFFER_SIZE);

    snprintf(tempStr, BUFFER_LENGTH," value=\"paramChannel:%d\"",tArgs->currentChannel);
    AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE );
    AppendToBuffer(fh.tx_buffer," />\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );

    sendTrainingBuffer();


     memset(tempStr,0, 128);
    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,responseTag, FIREHOSE_TX_BUFFER_SIZE);

    snprintf(tempStr, BUFFER_LENGTH," value=\"paramChipSelect:%d\"",tArgs->currentChipSelect);
    AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE );
    AppendToBuffer(fh.tx_buffer," />\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );

    sendTrainingBuffer();

     memset(tempStr,0, 128);
    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,responseTag, FIREHOSE_TX_BUFFER_SIZE);

    snprintf(tempStr, BUFFER_LENGTH," value=\"paramTrainingType:%d\"",tArgs->trainingType);
    AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE );
    AppendToBuffer(fh.tx_buffer," />\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );

    sendTrainingBuffer();


    return FIREHOSE_SUCCESS;
}

firehose_error_t sendRunningResponse(char* responseTag)
{
        InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
        AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"<", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,responseTag, FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer," Running=\"true\"/>\n</data> ", FIREHOSE_TX_BUFFER_SIZE);

        sendTrainingBuffer();
        sendDDIResponse(ACK);
        return FIREHOSE_SUCCESS;



    //return sendResponseFields(response, 0);
}

firehose_error_t sendPassResponse(char* responseTag)
{
        InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
        AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"<", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,responseTag, FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer," Pass=\"true\"/>\n</data> ", FIREHOSE_TX_BUFFER_SIZE);

        sendTrainingBuffer();
        sendDDIResponse(ACK);
        return FIREHOSE_SUCCESS;



    //return sendResponseFields(response, 0);
}

firehose_error_t sendFailResponse(char* responseTag)
{
        InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
        AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"<", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,responseTag, FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer," Pass=\"fail\"/>\n</data> ", FIREHOSE_TX_BUFFER_SIZE);
        sendTrainingBuffer();
        sendDDIResponse(ACK);
        return FIREHOSE_SUCCESS;


    //return sendResponseFields(response, 0);
}





void sendCommandInfo(int ix)
{

    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<commandefinition", FIREHOSE_TX_BUFFER_SIZE );
    //AppendToBuffer(fh.tx_buffer,SupportedCmds[ix]., FIREHOSE_TX_BUFFER_SIZE );
    snprintf(str, BUFFER_LENGTH," Name=\"%s\"",SupportedCmds[ix].cmdName);
    AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
    snprintf(str, BUFFER_LENGTH," Version=\"%d\"",SupportedCmds[ix].version);
    AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
    snprintf(str, BUFFER_LENGTH," ParameterNum=\"%d\"",SupportedCmds[ix].paremetersNum);
    AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
    snprintf(str, BUFFER_LENGTH," ResponseNum=\"%d\"",SupportedCmds[ix].responseNum);
    AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );

    AppendToBuffer(fh.tx_buffer," />\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );
    sendTransmitBuffer();
    busywait(10000);


}


void transmitParamInfo(int ix)
{
    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<parameterdefiniton", FIREHOSE_TX_BUFFER_SIZE );
    snprintf(str, BUFFER_LENGTH," Name=\"%s\"",SupportedParams[ix].paramName);
    AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
    snprintf(str, BUFFER_LENGTH," Type=\"%s\"",SupportedParams[ix].pType);
    AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
    snprintf(str, BUFFER_LENGTH," Min=\"%d\"",SupportedParams[ix].minVal);
    AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
    snprintf(str, BUFFER_LENGTH," Max=\"%d\"",SupportedParams[ix].maxVal);
    AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
    snprintf(str, BUFFER_LENGTH," Default=\"%d\"",SupportedParams[ix].defaultVal);
    AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );

    AppendToBuffer(fh.tx_buffer," />\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );
    sendTransmitBuffer();
    busywait(10000);


}

void sendParamInfo(int ix)
{
    for (int i = 0 ; i < ReturnNumParam() ; i++)
    {
        if (strncasecmp(SupportedParams[i].cmdName, SupportedCmds[ix].cmdName, strlen(SupportedCmds[ix].cmdName))==0)
        {
            transmitParamInfo(i);
        }
    }
}

void transmitRspInfo(int ix)
{
    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<responsedefinition ", FIREHOSE_TX_BUFFER_SIZE );
    snprintf(str, BUFFER_LENGTH," Name=\"%s\"",SupportedResp[ix].RespName);
    AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
    snprintf(str, BUFFER_LENGTH," Type=\"%s\"",SupportedResp[ix].rType);
    AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );



    AppendToBuffer(fh.tx_buffer," />\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );
    sendTransmitBuffer();
    busywait(10000);
}

void sendResponseInfo(int ix)
{
    for (int i = 0 ; i < ReturnNumResp() ; i++)
    {
        if (strncasecmp(SupportedResp[i].cmdName, SupportedCmds[ix].cmdName, strlen(SupportedCmds[ix].cmdName))==0)
        {
            transmitRspInfo(i);
        }
    }

}

void updateParamStruct()
{

    for (int i = 0 ; i < ReturnNumParam(); i++)
    {
        if (strncasecmp(SupportedParams[i].paramName, "ClockValue", strlen("ChangeBIMCClock"))==0)
        {
               SupportedParams[i].minVal = 0;
               SupportedParams[i].maxVal = gSupportedClockLevel-1;
               SupportedParams[i].defaultVal = (gSupportedClockLevel/2);
        }

        if (strncasecmp(SupportedParams[i].paramName, "Freq", strlen("Freq"))==0)
        {
               SupportedParams[i].minVal = 0;
               SupportedParams[i].maxVal = gSupportedClockLevel-1;
               SupportedParams[i].defaultVal = (gSupportedClockLevel/2);
        }


    }





}

void initDDIBuffer()
{
    uint32 cookie1 = share_data->ddi_buf[0];
    uint32 cookie2 = share_data->ddi_buf[1];
    memset(share_data->ddi_buf, 0x0, sizeof(share_data->ddi_buf));
    share_data->ddi_buf[0] = cookie1;
    share_data->ddi_buf[1] = cookie2;
}



firehose_error_t handleDDIInfo()
{
    //<ddidefinition Version=�1� Platform=�8909� CommandNum=�1�/>



        InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
        AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"<ddidefinition ", FIREHOSE_TX_BUFFER_SIZE );
        snprintf(str, BUFFER_LENGTH," Version=\"%d.%d\"", DDI_VERSION_MAJOR, DDI_VERSION_MINOR);
        AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
        snprintf(str, BUFFER_LENGTH," Platform=\"%d\"", DDI_PLATFORM);
        AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
        snprintf(str, BUFFER_LENGTH," BuildDate=\"%s\"",__DATE__);
        AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
        snprintf(str, BUFFER_LENGTH," BuildTime=\"%s\"",__TIME__);
        AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
        snprintf(str, BUFFER_LENGTH," DSFVersion=\"%d\"",DDI_share_data->version);
        AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );
        snprintf(str, BUFFER_LENGTH," ClkPlan=\"\r\n");
        AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE);
        for (int i = 0; i < gSupportedClockLevel ; i++)
        {
            //logMessage("supported function: '%s'", firehose_tag_data[i].tag_name);
            snprintf(str, BUFFER_LENGTH,"%d - %d,\r\n", i, bimc_clk_plan[i]);
            AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE);
        }
        AppendToBuffer(fh.tx_buffer,"\r\n\"", FIREHOSE_TX_BUFFER_SIZE);


        snprintf(str, BUFFER_LENGTH," CommandNum=\"%d\r\n\"",ReturnNumCmds());
        AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE );



        snprintf(str, BUFFER_LENGTH," TrainingType=\"\r\n");
        AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"0 - READ,\r\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"1 - WRITE,\r\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"2 - CA\r\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"\r\n\"", FIREHOSE_TX_BUFFER_SIZE);

        snprintf(str, BUFFER_LENGTH," Testline=\"\r\n");
        AppendToBuffer(fh.tx_buffer,str, FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"0 - DQ,\r\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"1 - DQS,\r\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"2 - CA,\r\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"3 - CK\r\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"\r\n\"", FIREHOSE_TX_BUFFER_SIZE);









        AppendToBuffer(fh.tx_buffer," />\n", FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );



        sendTransmitBuffer();

        busywait(10000);

        // prepare command Packets..
        // for each command
        for(int i=0;i<(SIZE_T)ReturnNumCmds();i++)
        {
            sendCommandInfo(i);
            sendParamInfo(i);
            sendResponseInfo(i);
        }

        sendResponse(ACK);// acknowledge that we are done !!

        return FIREHOSE_SUCCESS;

}

int pow(int base, int exp)
{
    int result = 1;
    while (exp) {
        if (exp & 1)
            result *= base;

        exp >>= 1;
        base *= base;
    }

    return result;
}



uint32 convertStrtoUint(char* str)
{
    int num = 0;
    int c;

    if (str == NULL)
    {
        return 0xDEAD;
    }

    int len = strlen(str);

    if (str[0] == '0' && str[1] == 'x' )
    {

        // hex value
         for (int i = 2; i < len; i++)
         {
            c = (int)str[i];

            if (c >= 97 && c <= 102)
            {
                c = (int)str[i] - 87;
                num = num + c*(pow(16, len-(i+1))) ;

            }
            else if (c >= 65 && c <= 70)
            {
                c = (int)str[i] - 65;
                num = num + c*(pow(16, len-(i+1))) ;
            }
            else
            {
                c = (int)str[i] - 48;
                num = num + c*(pow(16, len-(i+1))) ;
            }

         }
    }
    else
    {
        for (int i = 0; i < len ; i++)
        {

            c = (int)str[i] - 48;
            num = num + c*(pow(10, len-(i+1))) ;

        }

    }



        return num;

}


int convertStrtoInt(char* str)
{
    int num = 0;
    int c;
    boolean neg = 0;
    if (str == NULL)
    {
        return 0xDEAD;
    }

    int len = strlen(str);

    if (str[0] == '0' && str[1] == 'x' )
    {

        // hex value
         for (int i = 2; i < len; i++)
         {
            c = (int)str[i];

            if (c >= 65 && c <= 70)
            {
                c = (int)str[i] - 55;
                num = num + c*(pow(16, len-(i+1))) ;

            }
            else
            {
                c = (int)str[i] - 48;
                num = num + c*(pow(16, len-(i+1))) ;
            }

         }
    }
    else
    {
        for (int i = 0; i < len; i++)
        {
            if (str[i] == '-')
            {
                // negative number
                neg = 1;
            }
            else
            {
                c = (int)str[i] - 48;
                num = num + c*(pow(10, len-(i+1))) ;

            }


        }

    }


    if(neg)
        return num*(-1);
    else
        return num;

}
firehose_error_t sendlog(char* msg)
{
    char tempStr[128];
    char* responseTag = "log";
    InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);            // adds XML header only
    AppendToBuffer(fh.tx_buffer,"<data>\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"<", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,responseTag, FIREHOSE_TX_BUFFER_SIZE);

    snprintf(tempStr, BUFFER_LENGTH," value=\"");
    AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE );
    AppendToBuffer(fh.tx_buffer,msg, FIREHOSE_TX_BUFFER_SIZE );
    snprintf(tempStr, BUFFER_LENGTH,"\"");
    AppendToBuffer(fh.tx_buffer,tempStr, FIREHOSE_TX_BUFFER_SIZE );
    AppendToBuffer(fh.tx_buffer," />\n", FIREHOSE_TX_BUFFER_SIZE);
    AppendToBuffer(fh.tx_buffer,"</data>\r\n", FIREHOSE_TX_BUFFER_SIZE );

    sendTrainingBuffer();

    return FIREHOSE_SUCCESS;

}




firehose_error_t handleChangeBIMCClock()
{
    int clockPlanIx = 0;
    int val = 0;
    int i = 0;


    if (share_data->ddi_buf[0] != 0xE12A3456)
    {
       sendlog("Please Retrain before using this feature");
       sendResponse(NAK);
       return FIREHOSE_SUCCESS;

    }

    for (i = 0 ; i < ReturnIncomingParam(); i++)
    {


        if (strncasecmp(inComingParams[i].cmdName, NewTagName, strlen(NewTagName)) == 0)
        {
            if (strncasecmp(inComingParams[i].paramName, "ClockValue", strlen("ClockValue"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        clockPlanIx = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                         clockPlanIx =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     clockPlanIx =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }
            }
        }

    }


    sendResponse(ACK);
    if (ddi_ChangeBIMCClock(clockPlanIx) == TRUE)
    {
        sendPassResponse("responseChangeBIMCClock");
    }
    else
    {
        sendFailResponse("responseChangeBIMCClock");

    }

     return FIREHOSE_SUCCESS;


}

firehose_error_t handleReset()
{

     resetDDI();
     return FIREHOSE_SUCCESS;
}

firehose_error_t handleReadPhySetting()
{
    int testline = 0;
    int prfs = 0;
    int val = 0;

    for (int i = 0 ; i < ReturnIncomingParam(); i++)
    {


        if (strncasecmp(inComingParams[i].cmdName, NewTagName, strlen(NewTagName)) == 0)
        {
            if (strncasecmp(inComingParams[i].paramName, "TestLine", strlen("TestLine"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        testline = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          testline =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                      testline =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }
            }
            else  if (strncasecmp(inComingParams[i].paramName, "PRFSLevel", strlen("PRFSLevel"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        prfs = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          prfs =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                      prfs =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }
        }
    }

    sendResponse(ACK);


    ddi_ReadPHYSettings(testline,prfs);



    return FIREHOSE_SUCCESS;

}


firehose_error_t handleChangeBIMCPhySettings()
{
    int testline = 0;
    int prfs = 0;
    int ovrdrv = 0;
    int PU = 0;
    int ODT = 0;
    int VOH = 0;
    int ODTE = 0;
    int val = 0;
    int i =0;


    for (i = 0 ; i < ReturnIncomingParam(); i++)
    {


        if (strncasecmp(inComingParams[i].cmdName, NewTagName, strlen(NewTagName)) == 0)
        {
            if (strncasecmp(inComingParams[i].paramName, "TestLine", strlen("TestLine"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        testline = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          testline =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                      testline =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }
            }
            else  if (strncasecmp(inComingParams[i].paramName, "PRFSLevel", strlen("PRFSLevel"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        prfs = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          prfs =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                      prfs =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }
            else  if (strncasecmp(inComingParams[i].paramName, "OverDrive", strlen("OverDrive"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        ovrdrv = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          ovrdrv =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     ovrdrv =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }
            else  if (strncasecmp(inComingParams[i].paramName, "PullUp", strlen("PullUp"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        PU = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          PU =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     PU =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }
            else  if (strncasecmp(inComingParams[i].paramName, "ODT", strlen("ODT"))==0)
            {
                 val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        ODT = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          ODT =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     ODT =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }
            else  if (strncasecmp(inComingParams[i].paramName, "VoH", strlen("VoH"))==0)
            {
                 val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        VOH = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          VOH =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     VOH =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }
            else  if (strncasecmp(inComingParams[i].paramName, "En_Term", strlen("En_Term"))==0)
            {
                if ((inComingParams[i].val[0] == 't'||inComingParams[i].val[0] == 'T')  && (inComingParams[i].val[1] == 'r'||inComingParams[i].val[1] == 'R'))
                {
                    ODTE = 1;
                }
                else if ((inComingParams[i].val[0] == 'f'||inComingParams[i].val[0] == 'F')  && (inComingParams[i].val[1] == 'a'||inComingParams[i].val[1] == 'A'))
                {
                    ODTE = 0;
                }
                else
                {
                    sendResponse(NAK);
                    ODTE = SupportedParams[i].defaultVal;
                    return FIREHOSE_SUCCESS;
                }

            }


         }
     }

      sendResponse(ACK);




    if (ddi_ChangePHYSettings(testline,prfs,ovrdrv,PU,ODT,VOH,ODTE ) == TRUE)
    {
        sendPassResponse("responseChangeBIMCPhySettings");
    }
    else
    {
        sendFailResponse("responseChangeBIMCPhySettings");

    }





    return FIREHOSE_SUCCESS;
}


firehose_error_t handleRWtest()
{
    int ch = 0;
    int cs = 0;
    int val = 0;

    int loopcnt = 0;
    int infinity = 0;



    for (int i = 0 ; i < ReturnIncomingParam(); i++)
    {


        if (strncasecmp(inComingParams[i].cmdName, NewTagName, strlen(NewTagName)) == 0)
        {
            if (strncasecmp(inComingParams[i].paramName, "SDRamInterface", strlen("SDRamInterface"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        ch = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          ch =0;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                      ch =0;
                     return FIREHOSE_SUCCESS;

                }

            }
            else if (strncasecmp(inComingParams[i].paramName, "SDRamChipSelect", strlen("SDRamChipSelect"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        cs = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          cs =0;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                      cs =0;
                     return FIREHOSE_SUCCESS;

                }

            }
            else if (strncasecmp(inComingParams[i].paramName, "LoopCount", strlen("LoopCount"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        loopcnt = val;
                    }
                    else
                    {
                        sendResponse(NAK);
                        loopcnt = 1;
                        return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                      loopcnt = 1;
                     return FIREHOSE_SUCCESS;

                }
            }
            else if (strncasecmp(inComingParams[i].paramName, "InfinityFlag", strlen("InfinityFlag"))==0)
            {

                if ((inComingParams[i].val[0] == 't'||inComingParams[i].val[0] == 'T')  && (inComingParams[i].val[1] == 'r'||inComingParams[i].val[1] == 'R'))
                {
                    infinity = 1;
                }
                else if ((inComingParams[i].val[0] == 'f'||inComingParams[i].val[0] == 'F')  && (inComingParams[i].val[1] == 'a'||inComingParams[i].val[1] == 'A'))
                {
                    infinity = 0;
                }
                else
                {
                    sendResponse(NAK);
                    infinity = 0;
                    return FIREHOSE_SUCCESS;
                }


            }



        }

    }

    //send ack
    sendResponse(ACK);



    if (strncasecmp(NewTagName, "WriteTest", strlen("WriteTest")) == 0)
    {
        if (ddi_WriteTest(ch, cs, loopcnt, infinity ) == TRUE)
        {
            sendPassResponse("responseWriteTest");
        }
        else
        {
            sendFailResponse("responseWriteTest");

        }
    }
    else if (strncasecmp(NewTagName, "ReadTest", strlen("ReadTest")) == 0)
    {
        if (ddi_ReadTest(ch, cs, loopcnt, infinity ) == TRUE)
        {
            sendPassResponse("responseReadTest");
        }
        else
        {
            sendFailResponse("responseReadTest");

        }


    }
    else if (strncasecmp(NewTagName, "ReadWriteTest", strlen("ReadWriteTest")) == 0)
    {
        if (ddi_ReadWriteTest(ch, cs, loopcnt, infinity ) == TRUE)
        {
            sendPassResponse("responseReadWriteTest");
        }
        else
        {
            sendFailResponse("responseReadWriteTest");

        }

    }





    return FIREHOSE_SUCCESS;

}

firehose_error_t handleDisableDBI()
{


    sendResponse(ACK);

    //run the test
    if ( disableDBI() == TRUE)
    {
        sendPassResponse("responseDisableDBI");
    }
    else
    {
        sendFailResponse("responseDisableDBI");

    }


    return FIREHOSE_SUCCESS;



}

firehose_error_t handleReadPowerSetting()
{
    ddi_readPowerSetting();

    return FIREHOSE_SUCCESS;
}



firehose_error_t handleReadTemperature()
{

    ddi_readTemperature();

    return FIREHOSE_SUCCESS;

}



firehose_error_t handleChangeVDDQ()
{
    int vddVal = 0;
    int val = 0;
     for (int i = 0 ; i < ReturnIncomingParam(); i++)
    {
        if (strncasecmp(inComingParams[i].cmdName, NewTagName, strlen(NewTagName))==0)
        {
            if (strncasecmp(inComingParams[i].paramName, "VDDQValue", strlen("VDDQValue"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        vddVal = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                         vddVal  =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     vddVal  =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;
                }


            }

        }

     }

     sendResponse(ACK);



        if (ddi_ChangeVDDQ(vddVal) == TRUE)
        {
            sendPassResponse("responseChangeVDDQ");
        }
        else
        {
            sendFailResponse("responseChangeVDDQ");

        }





    return FIREHOSE_SUCCESS;

}

firehose_error_t handleChangeVDD1()
{

    int vddVal = 0;
    int val = 0;

    for (int i = 0 ; i < ReturnIncomingParam(); i++)
    {
        if (strncasecmp(inComingParams[i].cmdName, NewTagName, strlen(NewTagName))==0)
        {
            if (strncasecmp(inComingParams[i].paramName, "VDD1Value", strlen("VDD1Value"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        vddVal = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                         vddVal  =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     vddVal  =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;
                }
            }




        }


    }


    sendResponse(ACK);



        if (ddi_ChangeVDD1(vddVal) == TRUE)
        {
            sendPassResponse("responseChangeVDD1");
        }
        else
        {
            sendFailResponse("responseChangeVDD1");

        }




    return FIREHOSE_SUCCESS;
}


firehose_error_t handleDRAMSettings()
{

    int freq = 0;
    int mr3Val = 0;
    int mr11Val = 0;

    int val = 0;

     for (int i = 0 ; i < ReturnIncomingParam(); i++)
    {
        if (strncasecmp(inComingParams[i].cmdName, NewTagName, strlen(NewTagName))==0)
        {
            if (strncasecmp(inComingParams[i].paramName, "Freq", strlen("Freq"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        freq = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                         freq  =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     freq  =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }


            if (strncasecmp(inComingParams[i].paramName, "mr3Val", strlen("mr3Val"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        mr3Val = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                         mr3Val  =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     mr3Val  =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }


            if (strncasecmp(inComingParams[i].paramName, "mr11Val", strlen("mr11Val"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        mr11Val = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                         mr11Val  =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     mr11Val  =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }




        }

     }





     //send ack
    sendResponse(ACK);


    if (strncasecmp(NewTagName, "ChangeMR3", strlen("ChangeMR3")) == 0)
    {
        if (ddi_ChangeMR3(freq, mr3Val ) == TRUE)
        {
            sendPassResponse("responseChangeMR3");
        }
        else
        {
            sendFailResponse("responseChangeMR3");

        }

    }
    else if (strncasecmp(NewTagName, "ChangeMR11", strlen("ChangeMR11")) == 0)
    {
        if (ddi_ChangeMR11(freq, mr11Val) == TRUE)
        {
            sendPassResponse("responseChangeMR11");
        }
        else
        {
            sendFailResponse("responseChangeMR11");

        }

    }
//  else if (strncasecmp(NewTagName, "ChangeMR22", strlen("ChangeMR22")) == 0)
//  {
//      if (ddi_ChangeMR22(freq, CA_ODTD, CS_ODTE,CK_ODTD,CODT ) == TRUE)
//      {
//          sendPassResponse("responseChangeMR22");
//      }
//      else
//      {
//          sendFailResponse("responseChangeMR22");
//
//      }
//
//  }


    return FIREHOSE_SUCCESS;
}


firehose_error_t handleDDRTest()
{
    int ch = 0;
    int cs = 0;
    int val = 0;

    //
    //get the Parameters...
    for (int i = 0 ; i < ReturnIncomingParam(); i++)
    {
        if (strncasecmp(inComingParams[i].cmdName, NewTagName, strlen(NewTagName))==0)
        {
            if (strncasecmp(inComingParams[i].paramName, "SDRamInterface", strlen("SDRamInterface"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        ch = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          ch =0;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                      ch =0;
                     return FIREHOSE_SUCCESS;

                }

            }
            else if (strncasecmp(inComingParams[i].paramName, "SDRamChipSelect", strlen("SDRamChipSelect"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        cs = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          cs =0;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     cs =0;
                     return FIREHOSE_SUCCESS;

                }
            }

        }
    }

    //send ack
    sendResponse(ACK);


    if (strncasecmp(NewTagName, "OwnAddressRWTest", strlen("OwnAddressRWTest")) == 0)
    {
        if (ddi_DDRTestOwnAddr(ch, cs, "responseOwnAddressRWTest" ) == TRUE)
        {
            sendPassResponse("responseOwnAddressRWTest");
        }
        else
        {
            sendFailResponse("responseOwnAddressRWTest");

        }

    }
    else if (strncasecmp(NewTagName, "AddressLinesTest", strlen("AddressLinesTest")) == 0)
    {
        if (ddi_DDRAddressLinesTest(ch, cs, "responseAddressLinesTest" ) == TRUE)
        {
            sendPassResponse("responseAddressLinesTest");
        }
        else
        {
            sendFailResponse("responseAddressLinesTest");

        }


    }
    else if (strncasecmp(NewTagName, "DataLinesTest", strlen("DataLinesTest")) == 0)
    {
        if (ddi_DDRDataLinesTest(ch, cs, "responseDataLinesTest" ) == TRUE)
        {
            sendPassResponse("responseDataLinesTest");
        }
        else
        {
            sendFailResponse("responseDataLinesTest");

        }

    }

    //run the test


    //send test response
    return FIREHOSE_SUCCESS;
}


firehose_error_t handleRetrain()
{
    retrainDDR();

    return FIREHOSE_SUCCESS;
}



firehose_error_t handleReadWriteMRReg()
{
    int ch = 0;
    int cs = 0;
    int reg = 0;
    int val = 0;
    int regVal = 0;
    int i = 0;


    if (share_data->ddi_buf[0] != 0xE12A3456)
    {
       sendlog("Please Retrain before using this feature");
       sendResponse(NAK);
       return FIREHOSE_SUCCESS;

    }

    //
    //get the Parameters...
    for ( i = 0 ; i < ReturnIncomingParam(); i++)
    {
        if (strncasecmp(inComingParams[i].cmdName, NewTagName, strlen(NewTagName))==0)
        {
            if (strncasecmp(inComingParams[i].paramName, "SDRamInterface", strlen("SDRamInterface"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        ch = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          ch =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                      ch =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }
            else if (strncasecmp(inComingParams[i].paramName, "SDRamChipSelect", strlen("SDRamChipSelect"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        cs = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          cs =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     cs =SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }
            }
            else if (strncasecmp(inComingParams[i].paramName, "RegNum", strlen("RegNum"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        reg = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                          reg =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     reg = SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }else if (strncasecmp(inComingParams[i].paramName, "RegValue", strlen("RegValue"))==0)
            {
                val = convertStrtoInt(inComingParams[i].val);
                if (val != 0xDEAD)
                {
                    if ((val <= SupportedParams[i].maxVal) && (val >= SupportedParams[i].minVal))
                    {
                        regVal = val;
                    }
                    else
                    {
                         sendResponse(NAK);
                         regVal =SupportedParams[i].defaultVal;
                         return FIREHOSE_SUCCESS;

                    }
                }
                else
                {
                     sendResponse(NAK);
                     regVal = SupportedParams[i].defaultVal;
                     return FIREHOSE_SUCCESS;

                }

            }
        }
    }

    if (strncasecmp(NewTagName, "ReadMRReg", strlen("ReadMRReg")) == 0)
    {
        ddi_readMRReg(ch, cs, reg);

    }
    else if (strncasecmp(NewTagName, "MRWrite", strlen("MRWrite")) == 0)
    {
        if (ddi_writeMRReg(ch, cs, reg, regVal)== TRUE)
        {
            sendPassResponse("responseMRWrite");
        }
        else
        {
            sendFailResponse("responseMRWrite");

        }


    }


    return FIREHOSE_SUCCESS;
}




static void logDigest(char* prefix, byte* digest, SIZE_T length) {
    int i = 0;
    int written;
    char log_string[FIREHOSE_TEMP_BUFFER_SIZE] = { 0 };

    if (sizeof(log_string) < ((length * 2) + strlen(prefix) + 1)) {
        return;
    }

    written = snprintf(log_string,
                       sizeof(log_string),
                       "%s",
                       prefix);
    while (written < sizeof(log_string) && i < length) {
        written += snprintf(log_string + written,
                            sizeof(log_string) - written,
                            "%02X",
                            digest[i]);
        i++;
    }

    logMessage(log_string);
}


static void firehoseResetValidation() {
    firehoseResetValidationVariables();
    logMessage("Reset validation state. Expecting Digital signature");
}


static firehose_error_t recvNextPacket(byte** packet_ptr, SIZE_T bytes_to_read,SIZE_T *bytes_read, boolean queue_only)
{

    firehose_error_t read_retval = FIREHOSE_SUCCESS;
    read_retval = readChannel(packet_ptr, bytes_to_read, bytes_read, queue_only);
    if (read_retval != FIREHOSE_SUCCESS)
    {
        return read_retval;
    }

    if (queue_only == TRUE)
        return FIREHOSE_SUCCESS;



    return FIREHOSE_SUCCESS;
}

static firehose_error_t queueNextPacket(SIZE_T bytes_to_read) {
    return recvNextPacket(NULL, bytes_to_read, NULL, TRUE);
}

static firehose_error_t getNextPacket(byte** packet_ptr, SIZE_T bytes_to_read, SIZE_T *bytes_read) {
    return recvNextPacket(packet_ptr, bytes_to_read, bytes_read, FALSE);
}

// Parse a string sector value in valueArray and store in parsedValue.
// attrName is name of XML attribute with this value, used in error message.
//
// Returns:
//      FALSE if sector value is non-numerical, if it exceeds the number of
//      sectors in specified partition, or if partition is invalid
//      TRUE if sector value is valid
static boolean parseSectorValue(char* valueArray,
                          SIZE_T value_array_size,
                          SIZE_T *parsedValue,
                          SIZE_T partition,
                          char *attrName) {
    //byte temp_buffer[FIREHOSE_TEMP_BUFFER_SIZE] = {0};
    boolean num_conversion;
    SIZE_T num_partition_sectors;

    memset(temp_buffer,0,sizeof(temp_buffer));  // clear this out

    if (fh.attrs.SkipStorageInit == FALSE)
    {

        if (FALSE == storage_device_get_num_partition_sectors(&fh.store_dev, partition, &num_partition_sectors))
      {
        logMessage("storage_device_get_num_partition_sectors FAILED!");    // conversion to number failed
            return FALSE;
      }

    }

    simpleStrReplace(valueArray,
                         ".",
                         "");

    if (strstr(valueArray, "NUM_DISK_SECTORS") != NULL) {
        snprintf((char *)temp_buffer, sizeof(temp_buffer), SIZE_T_FORMAT, num_partition_sectors);
        simpleStrReplace(valueArray,
                         "NUM_DISK_SECTORS",
                         (const char *) temp_buffer);
        //num_conversion = parseExpression(valueArray, parsedValue);
    num_conversion = MyParseExpression(valueArray, strlen(valueArray), parsedValue);
    }
    else
    {
        //num_conversion = parseExpression(valueArray, parsedValue);
    num_conversion = MyParseExpression(valueArray, strlen(valueArray), parsedValue);
    }
    if (FALSE == num_conversion)
    {
        logMessage("Can't convert attribute '%s'='%s' to number", attrName, valueArray);    // conversion to number failed
        return FALSE;
    }

    if (fh.attrs.SkipStorageInit == FALSE)
    {

       if (*parsedValue > num_partition_sectors)
       {
           logMessage("XML attribute '%s'=%lld exceeds total size of partition %lld in sectors: %lld", attrName, *parsedValue, partition, num_partition_sectors);
           return FALSE;
       }
    }
    return TRUE;
}





static firehose_error_t handleNop()
{
    //logMessage("Binary build date: %s @ %s\n", __DATE__, __TIME__);

    //fh.attrs.Verbose ^= 1;
    //logMessage("fh.attrs.Verbose is set to %i", fh.attrs.Verbose);

    //sendAndAuthSerialNum();
    //sendSupportedFunctions();
    sendResponse(ACK);
    return FIREHOSE_SUCCESS;
}

int ReturnNumAttributes(void)
{
    return (int)(sizeof(AllAttributes)/sizeof(struct Attributes_Struct));
}

int ReturnAttributeLocation(char *NewAttrName)
{
    volatile SIZE_T i;
    SIZE_T StringLength1=0,StringLength2=0;

    StringLength1 = strlen((const char *)NewAttrName);

    for(i=0;i<(SIZE_T)ReturnNumAttributes();i++)
    {
        StringLength2 = strlen((const char *)AllAttributes[i].Name);

        if(StringLength1<StringLength2)
            StringLength1 = StringLength2;  // want the biggest number

        if ( strncasecmp(AllAttributes[i].Name, NewAttrName, StringLength1) == 0)
        {
            return i;
        }
    }

    return -1;
}

void Display_storage_extras_UFS(void)
{
    int i;
    // test if attribute was set here
    i = ReturnAttributeLocation("bNumberLU");
    if(i==-1) { return; }

    if(AllAttributes[i].Assigned == 1)
    {
        InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"<data>\n",FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"<log value=\"",FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bNumberLU=%i ",storage_extras.ufs_extras.bNumberLU);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bBootEnable=%i ",storage_extras.ufs_extras.bBootEnable);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bDescrAccessEn=%i ",storage_extras.ufs_extras.bDescrAccessEn);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bInitPowerMode=%i ",storage_extras.ufs_extras.bInitPowerMode);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bHighPriorityLUN=%i ",storage_extras.ufs_extras.bHighPriorityLUN);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bSecureRemovalType=%i ",storage_extras.ufs_extras.bSecureRemovalType);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bInitActiveICCLevel=%i ",storage_extras.ufs_extras.bInitActiveICCLevel);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"wPeriodicRTCUpdate=%i ",storage_extras.ufs_extras.wPeriodicRTCUpdate);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bConfigDescrLock=%i ",storage_extras.ufs_extras.bConfigDescrLock);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"\" />\n</data>",FIREHOSE_TX_BUFFER_SIZE);
        sendTransmitBuffer();
    }

    i = ReturnAttributeLocation("LUNum");
    if(i==-1) { return; }

    if(AllAttributes[i].Assigned == 1)
    {
        // show variables now. NOTE, can't use    i = UFS_LUN_Var_Struct.LUNum    since I memset this struct
             if(AllAttributes[i].Raw[0] == '1') { i = 1; }
        else if(AllAttributes[i].Raw[0] == '2') { i = 2; }
        else if(AllAttributes[i].Raw[0] == '3') { i = 3; }
        else if(AllAttributes[i].Raw[0] == '4') { i = 4; }
        else if(AllAttributes[i].Raw[0] == '5') { i = 5; }
        else if(AllAttributes[i].Raw[0] == '6') { i = 6; }
        else if(AllAttributes[i].Raw[0] == '7') { i = 7; }
        else                                    { i = 0; }

        InitBufferWithXMLHeader(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"<data>\n",FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"<log value=\"",FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"LUNum=%i ",i);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bLUEnable=%i ",storage_extras.ufs_extras.unit[i].bLUEnable);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bBootLunID=%i ",storage_extras.ufs_extras.unit[i].bBootLunID);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bLUWriteProtect=%i ",storage_extras.ufs_extras.unit[i].bLUWriteProtect);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bMemoryType=%i ",storage_extras.ufs_extras.unit[i].bMemoryType);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"size_in_KB=%i ",storage_extras.ufs_extras.unit[i].dNumAllocUnits);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bDataReliability=%i ",storage_extras.ufs_extras.unit[i].bDataReliability);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bLogicalBlockSize=%i ",storage_extras.ufs_extras.unit[i].bLogicalBlockSize);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"bProvisioningType=%i ",storage_extras.ufs_extras.unit[i].bProvisioningType);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        snprintf(temp_buffer,sizeof(temp_buffer),"wContextCapabilities=%i ",storage_extras.ufs_extras.unit[i].wContextCapabilities);
        AppendToBuffer(fh.tx_buffer,temp_buffer,FIREHOSE_TX_BUFFER_SIZE);
        AppendToBuffer(fh.tx_buffer,"\" />\n</data>",FIREHOSE_TX_BUFFER_SIZE);
        sendTransmitBuffer();
    }

}



static firehose_error_t handleStorageExtras()
{
#ifndef FEATURE_FIREHOSE_ENABLE_STORAGE_EXTRAS
    logMessage("handleStorageExtras() is not supported in this image");
    return sendResponse(NAK);
#endif

    if (fh.attrs.commit == FALSE)
    {
        if(fh.store_dev.current_storage_type== HOTPLUG_TYPE_UFS)
            Display_storage_extras_UFS();

        //logMessage("In handleStorageExtras() but doing nothing since commit==FALSE");
        return sendResponse(ACK);
    }

    // To be this far means we're going through with committing these changes

    if (FALSE == storage_device_commit_extras(&fh.store_dev, &storage_extras))
    {
        logMessage("Failed to set storage extra parameters - storage_device_commit_extras() returned FALSE");  // Failed to set storage extra parameters
        return sendResponse(NAK);
    }


    logMessage("Set storage parameters successfully");

    // Clear these for next run
    fh.attrs.commit = 0;
    memset(&storage_extras.emmc_extras,0,sizeof(sdcc_emmc_gpp_enh_desc_type));  // clear this out for next run
    memset(&storage_extras.ufs_extras, 0,sizeof(struct ufs_config_descr));      // clear this out for next run

    return sendResponse(ACK);
}












firehose_tag_data_t firehose_tag_data[] = {
    {"ddinfo",                  handleDDIInfo},
    {"OwnAddressRWTest",        handleDDRTest},
    {"AddressLinesTest",        handleDDRTest},
    {"DataLinesTest",           handleDDRTest},
    {"ReadTest",                handleRWtest},
    {"WriteTest",               handleRWtest},
    {"ReadWriteTest",           handleRWtest},
    {"Retrain",                 handleRetrain},
    {"ChangeBIMCPhySettings",   handleChangeBIMCPhySettings},
    {"ReadMRReg",               handleReadWriteMRReg},
    {"ReadPowerSetting",        handleReadPowerSetting},
    {"ReadPhySetting",          handleReadPhySetting},
    {"ReadTemperature",         handleReadTemperature},
    {"ChangeBIMCClock",         handleChangeBIMCClock},
    {"ChangeVDDQ",              handleChangeVDDQ},
    {"ChangeVDD1",              handleChangeVDD1},
    {"DisableDBI",              handleDisableDBI},
    {"ChangeMR3",               handleDRAMSettings},
    {"ChangeMR11",              handleDRAMSettings},
    {"ChangeMR22",              handleDRAMSettings},
    {"MRWrite",                 handleReadWriteMRReg},
    {"Reset",                   handleReset},
    {"nop",                     handleNop},             // Use this with Verbose="1", i.e. <NOP value="ping" Verbose="1" /> to turn on verbose logging for debug

};

// This reads the next packet sent over USB and initializes the XML
// reader fh.xml_reader with it.
//
// Returns: FIREHOSE_SUCCESS if valid XML was received
//          FIREHOSE_ERROR on read error, invalid XML, or XML too large
//
// NOTE: How 'we' choose to validate the XML right now is not very generic.
// Hence this should not be put under the xml parser code
firehose_error_t getXMLPacket() {
    SIZE_T bytes_read = 0;
    firehose_error_t read_retval;
    volatile int i = 0;

    while (bytes_read == 0)
        read_retval = getNextPacket(&packet, MIN(sizeof(fh.xml_buffer), fh.channel_buffer_capacity), &bytes_read);


    i = 1; // for breakpoint

    if (FIREHOSE_SUCCESS != read_retval)
        return read_retval;

    i = 2; // for breakpoint

    if (sizeof(fh.xml_buffer) < bytes_read)
    {
        i = bytes_read; // for breakpoint

        logMessage("XML (%lld bytes) is too big for buffer (max %lld bytes)", bytes_read, sizeof(fh.xml_buffer));
        MyErrorMessage(XML_PACKET_TOO_LARGE);
        return FIREHOSE_ERROR;
    }

    i = 3; // for breakpoint
//#define memscpy(dest,destsize,src,srcsize) memcpy(dest,src,srcsize)
    //memscpy(fh.xml_buffer, sizeof(fh.xml_buffer), packet, bytes_read);
    memset(fh.xml_buffer, 0, sizeof(fh.xml_buffer));
    memscpy(fh.xml_buffer, sizeof(fh.xml_buffer), packet, bytes_read);
    //xmlInitReader(&fh.xml_reader, fh.xml_buffer, bytes_read);

    i = 4; // for breakpoint

    return FIREHOSE_SUCCESS;
}

void initFirehoseTransport()
{
    usb_al_bulk_init();
}




char OPENING_DATA_TAG[] = "<data>";
char CLOSING_DATA_TAG[] = "</data>";
char OPENING_PATCHES_TAG[] = "<patches>";
char CLOSING_PATCHES_TAG[] = "</patches>";

tag_handler_t CurrentHandlerFunction;   // this will always point to latest TAG function, i.e. handleConfigure()

SIZE_T CopyString(char *Dest, char *Source, SIZE_T  Dstart, SIZE_T  Sstart, SIZE_T  length, SIZE_T DestSize, SIZE_T SourceSize)
{
        SIZE_T  i = 0;
        char Temp;

    if(Dest=='\0')               { return 0; }  // Dest is null
    if(Source=='\0')             { return 0; }  // Source is null
    if(Dstart+length-1>DestSize) { return 0; }  // string to copy over is too big for destination
    if(Sstart+length>SourceSize) { return 0; }  // range to copy is beyond source string
    if(length==0)                { return 0; }  // nothing to copy

    //logMessage("CopyString(0x%X, 0x%X, %ld, %ld, %ld, %ld, %ld)",Dest,Source,Dstart,Sstart,length,DestSize,SourceSize);

        for(i=0;i<length;i++)
        {
           Temp = *(Source+Sstart+i);

            *(Dest+Dstart+i) = Temp;
        } // end i

        *(Dest+Dstart+i) = '\0';    // NULL

        return length;
}

void ResetAllAttributeAssignFlags(void)
{
        SIZE_T  i;

        for(i=0;i<(SIZE_T)ReturnNumAttributes();i++)
            AllAttributes[i].Assigned = 0;
}

#define BITOP(a,b,op) \
 ((a)[(size_t)(b)/(8*sizeof *(a))] op (size_t)1<<((size_t)(b)%(8*sizeof *(a))))



#define ALIGNY (sizeof(size_t))
#define ONES ((size_t)-1/UCHAR_MAX)
#define HIGHS (ONES * (UCHAR_MAX/2+1))
#define HASZERO(x) ((x)-ONES & ~(x) & HIGHS)

char *__strchrnul(const char *s, int c)
{
    size_t *w, k;

    c = (unsigned char)c;
    if (!c) return (char *)s + strlen(s);

    for (; (uintptr_t)s % ALIGNY; s++)
        if (!*s || *(unsigned char *)s == c) return (char *)s;
    k = ONES * c;
    for (w = (void *)s; !HASZERO(*w) && !HASZERO(*w^k); w++);
    for (s = (void *)w; *s && *(unsigned char *)s != c; s++);
    return (char *)s;
}

size_t strcspn(const char *s, const char *c)
{
    const char *a = s;
    size_t byteset[32/sizeof(size_t)];

    if (!c[0] || !c[1]) return __strchrnul(s, *c)-a;

    memset(byteset, 0, sizeof byteset);
    for (; *c && BITOP(byteset, *(unsigned char *)c, |=); c++);
    for (; *s && !BITOP(byteset, *(unsigned char *)s, &); s++);
    return s-a;
}

size_t strspn(const char *s, const char *c)
{
    const char *a = s;
    size_t byteset[32/sizeof(size_t)] = { 0 };

    if (!c[0]) return 0;
    if (!c[1]) {
        for (; *s == *c; s++);
        return s-a;
    }

    for (; *c && BITOP(byteset, *(unsigned char *)c, |=); c++);
    for (; *s && BITOP(byteset, *(unsigned char *)s, &); s++);
    return s-a;
}
char *ddi_strtok(char *restrict s, const char *restrict sep)
{
    static char *p;
    if (!s && !(s = p)) return NULL;
    s += strspn(s, sep);
    if (!*s) return p = 0;
    p = s + strcspn(s, sep);
    if (*p) *p++ = 0;
    else p = 0;
    return s;
}


int AssignAttribute(char *NewAttrName, char *NewAttrValue, SIZE_T NewAttrValueLen)
{
        volatile SIZE_T i,Matched=0;


        //printf("\n\nIn AssignAttribute(%s,%s)\n",NewAttrName,NewAttrValue);

        //logMessage("In AssignAttribute(%s,%s) and fh.attrs.Verbose=0x%X",NewAttrName,NewAttrValue,fh.attrs.Verbose);

        for(i=0;i<sizeof(inComingParams)/sizeof(struct QduttParamStruct);i++)
        {
            if (strncasecmp(NewTagName, inComingParams[i].cmdName, strlen((const char *)inComingParams[i].cmdName)) == 0)
            {
                if (strncasecmp(NewAttrName, inComingParams[i].paramName, strlen((const char *)inComingParams[i].paramName)) == 0)
                {
                    //if (strncasecmp(NewAttrName, "Pattern", strlen((const char *)NewAttrName)) == 0)



                        Matched = 1;
                        if( CopyString(inComingParams[i].val, NewAttrValue, 0, 0, strlen(NewAttrValue), sizeof(inComingParams[i].val), NewAttrValueLen) == 0)
                        {
                            dbg(LOG_ERROR, "1 Failed to copy string '%s' of length %ld bytes into AllAttributes[%ld].Raw of size %ld bytes for '%s'",NewAttrValue,strlen(NewAttrValue),i,sizeof(AllAttributes[i].Raw),NewAttrName);
                            return 1;
                        }





                }
            }
         }


        if(Matched==0)
        {
            if(fh.attrs.Verbose == TRUE)
                dbg(LOG_INFO,"IGNORING UNRECOGNIZED Attribute '%s' with value '%s'",NewAttrName,NewAttrValue);
        }

        return 0;
}


SIZE_T IsEmptySpace(char c)
{
    if( c == 0xA ) { return 1; }
    if( c == 0xD ) { return 1; }
    if( c == 0x9 ) { return 1; }
    if( c == 0x20) { return 1; }

    return 0;
}



char * RemoveEverythingButTags(char *Packet, SIZE_T  CurrentPacketLoc, SIZE_T MaxPacketSize)
{
    SIZE_T  PacketLength,PacketLoc=0,i;
    char *pch;
    long FoundLeftBrace=-1,FoundRightBrace=-1,FoundForwardSlash=-1,Found=0;

    if(CurrentPacketLoc>=MaxPacketSize)
        return Packet; // nothing to do

    PacketLength= strlen((const char *)Packet);

    if(PacketLength>=MaxPacketSize)
        return Packet; // should be impossible

    for(i=0;i<PacketLength;i++)
    {
        if(Found==0)
        {
            if(i+3<PacketLength)
            {
                if(Packet[i]=='<' && Packet[i+1]=='!' && Packet[i+2]=='-' && Packet[i+3]=='-')  // protected by if(i+3<PacketLength) above
                {
                    Found = 1;
                    Packet[i] = ' '; Packet[i+1] = ' '; Packet[i+2] = ' '; Packet[i+3] = ' ';
                    i+=3;
                }
            }
        } // end of if(Found==0)
        else
        {
            // we are inside of a comment
            if(i+2<PacketLength)
            {
                if(Packet[i]=='-' && Packet[i+1]=='-' && Packet[i+2]=='>')  // protected by if(i+2<PacketLength) above
                {
                    Found = 0;
                    Packet[i] = ' '; Packet[i+1] = ' '; Packet[i+2] = ' ';
                    i+=2;
                    continue;
                }
            } // end if(i+2<PacketLength)
            Packet[i] = ' '; // remove since this is a comment
        }

    }

    // At this point, XML file has all <!-- comments --> replaced by spaces


    // Now nuke the first <?xml version="1.0" ?>

    do
    {
        pch = strstr(Packet, "xml version");
        if(pch==NULL) { break; }

        // to be this far we found "xml version"
        // Now look slightly beyond that

        pch = strstr(pch, ">");             // is now pointing to the beginning of the file
        if(pch!=NULL) //'\0')                   // pch = "<configuration> <options> DOWNLOAD_PROTOCOL       = FIREHOSE etc etc
        {
            memset(Packet,0x20,pch-&Packet[PacketLoc]+1);   // this removes <?xml version="1.0" ?>
        }

        // We could have more than 1 XML file here
        pch = strstr(Packet, "xml version");
        if(pch!=NULL)
        {
            PacketLength = pch-Packet; // now length updated to start of next XML
        }


        // find last tag name
        // find last tag name
        for(i=PacketLength-1;i>0;i--)
        {
            if( Packet[i] == '>' ) { FoundRightBrace = i; }

            if(FoundRightBrace>0)
                if( Packet[i] == '/' ) { FoundForwardSlash = i; }

            if(FoundRightBrace>0 && FoundForwardSlash>0)
            {
                if( Packet[i] == '<' )
                {
                    FoundLeftBrace = i;
                    break;
                }
            } // end if(FoundRightBrace>0)
        } // end for

        if( (FoundRightBrace-FoundForwardSlash+2) > MaxPacketSize-1 )
        {
            logMessage("Either closing tag is bigger than %d bytes, or the XML file is not formed correctly",MaxPacketSize);
            return Packet; // something is wrong
        }

        memset(CurrentTagName,0,sizeof(CurrentTagName));

        if( CopyString(CurrentTagName, Packet, 0, FoundForwardSlash, FoundRightBrace-FoundForwardSlash+1, sizeof(CurrentTagName), MaxPacketSize)  == 0)
        {
            logMessage("3 Failed to copy string of length %ld bytes into CurrentTagName",FoundRightBrace-FoundForwardSlash+1);
            return Packet; // something is wrong
        }
        //strncpy(CurrentTagName,&Packet[FoundForwardSlash],FoundRightBrace-FoundForwardSlash+1);   // CurrentTagName = "/data>" OR "/configuration>" etc etc

        //CurrentTagName[FoundRightBrace-FoundForwardSlash+1] = '\0';

        memset(&Packet[FoundForwardSlash-1],0x20,strlen(CurrentTagName)+1); // this takes out </configuration> OR </data> at the end of the file

        CurrentTagName[0] = '<';                            // CurrentTagName = "<data>" OR "<configuration>" etc etc

        pch = strstr(Packet, CurrentTagName);               // is now pointing to the beginning of the file
        if(pch!=NULL) //'\0')                   // pch = "<configuration> <options> DOWNLOAD_PROTOCOL       = FIREHOSE etc etc
        {
            memset(Packet,0x20,strlen(CurrentTagName)+(pch-Packet)); // nukes everything up to <data>
        }
        else
        {
            logMessage("ERROR: Either closing tag is bigger than %d bytes, or the XML file is not formed correctly",MaxPacketSize);
            logMessage("ERROR: Could not find '%s' tag",CurrentTagName);
            return Packet; // something is wrong
        }

        break; // breaking the do loop

    }while(1); // end of looking for "xml version"

    return Packet;
}

SIZE_T  DetermineTag(char *Packet, SIZE_T  CurrentPacketLoc, SIZE_T MaxPacketSize)
{
    volatile SIZE_T  i=0;
    SIZE_T  PacketLength;
    int CurrentTag = -1;
    char *pch;
    long FoundRightBrace=-1;

    if(CurrentPacketLoc>=MaxPacketSize)
        return 0;   // out of packet

    Packet = RemoveEverythingButTags(Packet,CurrentPacketLoc, MaxPacketSize);
    // Step 1. String away header
    // Step 2. Strip away <data> tags
    // step 3. Get tag_name
    // step 4. Get attributes
    PacketLength= strlen((const char *)Packet);

    if(PacketLength>=MaxPacketSize)
        return 0;   // should be impossible


    ResetAllAttributeAssignFlags();

    // This chomps up empty space till we hit a tag
    while( IsEmptySpace( Packet[CurrentPacketLoc] ) )
    {
        CurrentPacketLoc++;

        if(CurrentPacketLoc>=PacketLength)
        {
             logMessage("ERROR: XML packet not formed correctly. Ran out of room looking for TAG");

             return 0;
        }
    }

    if(Packet[CurrentPacketLoc]!='<')
    {
        logMessage("ERROR: XML not formed correctly. Expected a &lt; character at loc %d",CurrentPacketLoc);
        return 0;   // not formed correctly
    }

    CurrentPacketLoc++;    // move past the above opening <

    // Could be one solid tag like <options>, test by looking for spaces *before* seeing a '>'
    FoundRightBrace = -1;
    for(i=CurrentPacketLoc;i<PacketLength;i++)
    {
        if( Packet[i] == '>' ) { FoundRightBrace = i; break;}
        if( Packet[i] == ' ' ) { break;}
    }

    if(FoundRightBrace>0)
    {
        // this is something like <options> some text </options>
        ParseAttributes = 0;    // need to parse the strings

        CopyString(NewTagName, Packet, 0, CurrentPacketLoc,   FoundRightBrace-CurrentPacketLoc, sizeof(NewTagName), MaxPacketSize );   // NewTagName has the TAG now
        RemoveBannedChars(NewTagName); // chars like " < > are replaced by _

        CurrentPacketLoc = FoundRightBrace + 1; // The +1 to get past the >

    }
    else
    {
        // or could be tag with attributes like <program filename
        ParseAttributes = 1;

        pch = strstr(&Packet[CurrentPacketLoc], " ");          // Find a space after the TAG name

        if(pch==NULL) //'\0')   // if null, XML is not formed correctly
        {
            logMessage("ERROR: XML not formed correctly. Expected one SPACE character at loc %d",CurrentPacketLoc);
            return 0;   // not formed correctly
        }

        if( pch-&Packet[CurrentPacketLoc] > MAX_TAG_NAME_LENGTH )
        {
            logMessage("ERROR: The XML TAG is too long (%d chars) only %d chars allowed",(pch-&Packet[CurrentPacketLoc]),MAX_TAG_NAME_LENGTH);
            return 0;   // not formed correctly
        }


        CopyString(NewTagName, Packet, 0, CurrentPacketLoc,   pch-&Packet[CurrentPacketLoc], sizeof(NewTagName), MaxPacketSize );   // NewTagName has the TAG now
        RemoveBannedChars(NewTagName); // chars like " < > are replaced by _

        CurrentPacketLoc += strlen((const char *)NewTagName);
        CurrentPacketLoc++;    // move past the above opening space we confirmed exists above

    }

    for (i = 0; i < sizeof(firehose_tag_data) / sizeof(firehose_tag_data_t); i++)
    {
        if (strncasecmp(firehose_tag_data[i].tag_name, NewTagName,strlen((const char *)NewTagName)) == 0)
        {
                CurrentTag             = i;
                CurrentHandlerFunction = firehose_tag_data[i].handler;
                break;
        }
    } // end i

    if(CurrentTag==-1)
    {
        logMessage("**WARNING Ignoring Unrecognized tag '%s'", NewTagName);
        return 0;
    }

    // The above got the <TAG>, accessible like this firehose_tag_data[CurrentTag].handler and firehose_tag_data[CurrentTag].tag_name

    return CurrentPacketLoc;

} // end DetermineTag(char *Packet, SIZE_T  PacketLoc)


char NewAttrValue[3072] __attribute__ ((section (".bss.DDITRAINING_BUFFER_SECTION")));



SIZE_T getlength(char* str)
{
    SIZE_T len = 0;
    while (*str != '}')
    {
        len++;
        str++;


    }

    return len+1;
}


SIZE_T glen ;
SIZE_T  DetermineAttributes(char *Packet, SIZE_T  CurrentPacketLoc, SIZE_T MaxPacketSize)
{
    SIZE_T  AttributesFound=0;
    SIZE_T  PacketLength;

    char NewAttrName[MAX_TAG_ATTR_LENGTH];

    char *pch;

    if(CurrentPacketLoc>=MaxPacketSize)
        return 0;   // ran out of packet

    // step 4. Get attributes
    PacketLength    = strlen((const char *)Packet);
    // Packet[CurrentPacketLoc] should be at the first letter of the attribute now
    // They all look like this attribute1="value1" attribute2="value2"
    // Meaning only spaces seperate them

    if(PacketLength>=MaxPacketSize)
        return 0;   // should be impossible

    AttributesFound = 0;

    while(CurrentPacketLoc < PacketLength)
    {
        pch = strstr(&Packet[CurrentPacketLoc], "=");          // Find an equals sign
        if(pch==NULL) //'\0')   // if null, we didn't find it
        {
            dbg(LOG_ERROR,"XML not formed correctly. Could not find '=' character");
            return 0;   // not formed correctly
        }

        if( pch-&Packet[CurrentPacketLoc] > 3072 )
        {
            dbg(LOG_ERROR,"The value for XML attribute is too long (%d chars) only %d chars allowed",(pch-&Packet[CurrentPacketLoc]),MAX_TAG_ATTR_LENGTH);
            return 0;   // not formed correctly
        }

        CopyString(NewAttrName, Packet, 0, CurrentPacketLoc,   pch-&Packet[CurrentPacketLoc], sizeof(NewAttrName), MaxPacketSize );  // NewAttrName has the ATTR now

        RemoveBannedChars(NewAttrName); // chars like " < > are replaced by _

        CurrentPacketLoc += strlen((const char *)NewAttrName);
        CurrentPacketLoc++;    // move past the = sign

        if(Packet[CurrentPacketLoc]!='"')
        {
            dbg(LOG_ERROR,"XML not formed correctly. Expected one &quot; character at loc %d",CurrentPacketLoc);
            return 0;   // not formed correctly
        }

        CurrentPacketLoc++;    // Before "value" and now we move past the " sign, all that is left now is the actual value"

        pch = strstr(&Packet[CurrentPacketLoc], "\"");         // Find closing "

        if(pch==NULL) //'\0')   // if null, we didn't find it
        {
            dbg(LOG_ERROR,"XML not formed correctly. Expected one &quot; character at loc %d",CurrentPacketLoc);
            return 0;   // not formed correctly
        }

        if( pch-&Packet[CurrentPacketLoc] > 3072 )
        {
            dbg(LOG_ERROR,"The value for XML attribute '%s' is too long (%d chars) only %d chars allowed",NewAttrName,(pch-&Packet[CurrentPacketLoc]),MAX_TAG_ATTR_LENGTH);
            return 0;   // not formed correctly
        }

        CopyString(NewAttrValue, Packet, 0, CurrentPacketLoc,   pch-&Packet[CurrentPacketLoc], sizeof(NewAttrValue), MaxPacketSize ); // NewAttrValue has the ATTR value now
        RemoveBannedChars(NewAttrValue); // chars like " < > are replaced by _

        AttributesFound++;

        // FIGURE OUT WHICH ATTRIBUTE AND ASSIGN VALUE



        if (strncasecmp(NewAttrName, "Pattern", strlen((const char *)NewAttrName)) == 0)
        {
            int patternCount = 0;
            uint32 dataBufferSize = sizeof(dataBuffer)/sizeof(uint32);
            uint16 count = 0;
            uint16 patternIx = 0;



            patternElemCnt = 0;
            // tokenize the pattern string
            // get intvalue..
            //save to pattern container

            if (NewAttrValue[0] == '-' && NewAttrValue[1] == '1')
            {
                glen = strlen((const char *)NewAttrValue);
                // use default pattern..
                for (int i = 0 ;i < 256 ; i++)// potential bug...we dont have size of write_in_buffer so we use hard coded data here.TO DO
                {
                    patternDataBuf[i] = write_in_pattern[i];
                    patternElemCnt++;
                }

                if(patternElemCnt%16!=0)
                 {
                     return 0;
                 }
                 else
                 {
                     while(patternCount < (dataBufferSize/patternElemCnt))
                    {

                        while(count < patternElemCnt)
                        {


                            dataBuffer[patternIx + count] = patternDataBuf[count];
                            count = count+1;


                        }

                        patternIx = patternIx + count;
                        count = 0;
                        patternCount = patternCount+1;

                    }
                 }


            }
            else // assuminng we are getting c style array!!
            {
                glen = getlength(NewAttrValue);
                char *strIx;
                strIx = ddi_strtok(NewAttrValue, "{,}");
                while (strIx != NULL)
                {

                    patternDataBuf[patternElemCnt] = convertStrtoUint(strIx);
                    //sscanf(strIx, "%x", &patternDataBuf[patternElemCnt]);
                    patternElemCnt++;
                    if (patternElemCnt == dataBufferSize)
                    {
                        break;
                    }
                    strIx = ddi_strtok(NULL, "{,}");
                }

                 if(patternElemCnt%16!=0)
                 {
                     return 0;
                 }
                 else
                 {
                     while(patternCount < (dataBufferSize/patternElemCnt))
                    {

                        while(count < patternElemCnt)
                        {


                            dataBuffer[patternIx + count] = patternDataBuf[count];
                            count = count+1;


                        }

                        patternIx = patternIx + count;
                        count = 0;
                        patternCount = patternCount+1;

                    }
                 }

            }






            CurrentPacketLoc += glen;

            CurrentPacketLoc++;    // move past the closing quote " sign

        }
        else
        {
            if( AssignAttribute(NewAttrName, NewAttrValue, sizeof(NewAttrValue)) != 0 )
            {
                dbg(LOG_ERROR,"AssignAttribute failed");
                return 0;   // not formed correctly
            }
              CurrentPacketLoc += strlen((const char *)NewAttrValue);

              CurrentPacketLoc++;    // move past the closing quote " sign


        }


        while( IsEmptySpace( Packet[CurrentPacketLoc] ) )
        {
            CurrentPacketLoc++;

            if(CurrentPacketLoc>=PacketLength)
            {
                dbg(LOG_ERROR,"XML packet not formed correctly. Ran out of room looking for ATTRIBUTES");

                return 0;
            }
        }


        if(Packet[CurrentPacketLoc]=='/')
        {
           // This is the END of the packet
           return CurrentPacketLoc+2; // +2 gets us past the />
        }
    } // end of while(CurrentPacketLoc < PacketLength)

    return 0;       // to be here means we ran out of packet

} // end DetermineAttributes(char *Packet, SIZE_T CurrentPacketLoc)

#define BYTES_PER_KB 1024

SIZE_T  ParseComplicatedAttributes(void)
{
        volatile SIZE_T  i,j;

        for(i=0;i<(SIZE_T)ReturnNumAttributes();i++)
        {
            j = i;    // for breakpoint

            if(AllAttributes[i].Assigned==0)
                continue;   // variable wasn't even assigned, so no point checking


            if(CurrentHandlerFunction==handleStorageExtras)
            {
                // This is a special case, where the <ufs> or <emmc> tags are used and variables are sent
                // over multiple packets. In this case the struct UFS_LUN_Var is used as for temporary holding
                // and these values need to be moved to storage_extras.ufs_extras.unit[ 0 ].bLUEnable
                j = i; // for breakpoint
            }
            else if(AllAttributes[i].Type == 'i' && AllAttributes[i].Type == 'b' && AllAttributes[i].Type == 'w')
                continue;   // integer attributes are not complicated, Only want 'x' or 's'


            j = i+1;    // for breakpoint

            if (strncasecmp(AllAttributes[i].Name, "start_sector",strlen("start_sector")) == 0)
            {
                //printf("MATCHED - '%s'\n",AllAttributes[i].Name);

                j = i;    // for breakpoint

                if (parseSectorValue(AllAttributes[i].Raw, sizeof(AllAttributes[i].Raw), &fh.attrs.start_sector, fh.attrs.physical_partition_number, "start_sector") == FALSE)
                {
                    logMessage("parseSectorValue could not handle start_sector value");
                    return FIREHOSE_ERROR;
                }

                j = i;    // for breakpoint

            }  // end of looking for start_sector
// =================================================================

            if (strncasecmp(AllAttributes[i].Name, "MemoryName", strlen("MemoryName")) == 0)
            {
                //printf("MATCHED - '%s'\n",AllAttributes[i].Name);

                j = i;    // for breakpoint


               // convert memory name to enum value
               if (strncasecmp( AllAttributes[i].Raw, "emmc", strlen("eMMC") ) == 0)
               {
                   fh.attrs.storage_type = HOTPLUG_TYPE_MMC;
                   fh.store_dev.sector_size = 512; // for eMMC

                   i = j;
               }
               else if (strncasecmp( AllAttributes[i].Raw, "ufs", strlen("UFS") ) == 0)
               {
                   fh.attrs.storage_type = HOTPLUG_TYPE_UFS;
                   fh.store_dev.sector_size = 4096; // for UFS
                   i = j;
               }
               else
               {
                   logMessage("Invalid storage type: %s", (char *)AllAttributes[i].Raw);
                   return FIREHOSE_ERROR;
               }

                j = i;    // for breakpoint
            }  // end of looking for MemoryName
// ==================================================================
            if (strncasecmp(AllAttributes[i].Name, "LUNum",strlen("LUNum")) == 0)
            {
                volatile SIZE_T  Temp1 = 0;
                volatile SIZE_T  Temp2 = 0;

                if(CurrentHandlerFunction!=handleStorageExtras) { continue; }

                j = i;    // for breakpoint

                Temp1 = UFS_LUN_Var_Struct.LUNum;

                // All attributes for this LUN would have also been assigned
                Temp2 = UFS_LUN_Var_Struct.bLUEnable;               storage_extras.ufs_extras.unit[ Temp1 ].bLUEnable           = Temp2;
                Temp2 = UFS_LUN_Var_Struct.bBootLunID;              storage_extras.ufs_extras.unit[ Temp1 ].bBootLunID          = Temp2;
                Temp2 = UFS_LUN_Var_Struct.bLUWriteProtect;         storage_extras.ufs_extras.unit[ Temp1 ].bLUWriteProtect     = Temp2;
                Temp2 = UFS_LUN_Var_Struct.bMemoryType;             storage_extras.ufs_extras.unit[ Temp1 ].bMemoryType         = Temp2;
                Temp2 = UFS_LUN_Var_Struct.bDataReliability;        storage_extras.ufs_extras.unit[ Temp1 ].bDataReliability    = Temp2;
                Temp2 = UFS_LUN_Var_Struct.bLogicalBlockSize;       storage_extras.ufs_extras.unit[ Temp1 ].bLogicalBlockSize   = Temp2;
                Temp2 = UFS_LUN_Var_Struct.bProvisioningType;       storage_extras.ufs_extras.unit[ Temp1 ].bProvisioningType   = Temp2;
                Temp2 = UFS_LUN_Var_Struct.wContextCapabilities;    storage_extras.ufs_extras.unit[ Temp1 ].wContextCapabilities= Temp2;

                // We allow user to enter size_in_KB but the real attribute is dNumAllocUnits
                Temp2 = UFS_LUN_Var_Struct.size_in_KB;

                storage_extras.ufs_extras.unit[ Temp1 ].dNumAllocUnits = Temp2; // for now, dNumAllocUnits *is* size_in_KB until we call ufs_commit_extras()

                memset(&UFS_LUN_Var_Struct,0,sizeof(UFS_LUN_Var_Struct));   // clear this out for next run

                j = i;    // for breakpoint

            }  // end of looking for start_sector
// =================================================================
// ==================================================================

            if (strncasecmp(AllAttributes[i].Name, "commit",strlen("commit")) == 0)
            {
                //printf("MATCHED - '%s'\n",AllAttributes[i].Name);

                // Need to update this value

                j = i;    // for breakpoint

                if (strncasecmp(AllAttributes[i].Raw, "true",strlen("true")) == 0)
                    fh.attrs.commit = 1;
                else if (strncasecmp(AllAttributes[i].Raw, "1",strlen("1")) == 0)
                    fh.attrs.commit = 1;
                else
                    fh.attrs.commit = 0;

                j = i;    // for breakpoint

            }  // end of looking for start_sector
// =================================================================

        } // end for i

        return FIREHOSE_SUCCESS;

} // end SIZE_T  ParseComplicatedAttributes(void)

void handleResettingAttributesBasedOnTag(void)
{




} // end of void handleResettingAttributesBasedOnTag(void);

/*
 * The main entry point after initialization in deviceprogrammer_initialize.c.
 * It is called by deviceprogrammer_entry().
 *
 * To add a new command to Firehose, define it above and add it to the function
 * table: firehose_tag_data.
 */

//#include "CeTL_Comdef.h"
//#include "CeML.h"
//#include "CeTL_Hash.h"
//#include "CeTL_Env.h"
void InitAttributes(void);

void InitAttributes(void)
{
    fh.attrs.MaxPayloadSizeToTargetInBytes          = 131072; //ONE_MEGABYTE;
    fh.attrs.MaxPayloadSizeToTargetInBytesSupported = 32768; //ONE_MEGABYTE;
    fh.attrs.MaxPayloadSizeFromTargetInBytes        = 8192; //ONE_MEGABYTE;
    fh.attrs.Verbose                                = 0;
    fh.attrs.MaxDigestTableSizeInBytes              = 8192;
    fh.attrs.AckRawDataEveryNumPackets              = 0;
    fh.attrs.delayinseconds                         = 1;
    fh.attrs.address64                              = 0;
    fh.attrs.value64                                = 0;
    fh.attrs.storagedrive                           = 0;
    fh.attrs.physical_partition_number              = 0;
    fh.attrs.SECTOR_SIZE_IN_BYTES                   = 512;
    fh.attrs.byte_offset                            = 0;
    fh.attrs.physical_partition_number              = 0;
    fh.attrs.size_in_bytes                          = 0;
    fh.attrs.num_partition_sectors                  = 0;
    fh.attrs.file_sector_offset                     = 0;

    fh.attrs.ZlpAwareHost                           = 1;
    fh.attrs.SkipWrite                              = 0;
    fh.attrs.BufferWrites                           = 0;
    //fh.attrs.AckRawData                               = 0;
    fh.attrs.AlwaysValidate                         = 0;

    //fh.attrs.display                              = 0;
    //fh.attrs.read_back_verify                     = 0;
    fh.attrs.TestWritePerformance                   = 0;
    fh.attrs.TestReadPerformance                    = 0;
    fh.attrs.TestDigestPerformance                  = 0;
    fh.attrs.TestDDRValidity                        = 0;
    fh.attrs.SkipStorageInit                        = 0;
    fh.attrs.SkipSettingMinClocks                   = 0;
    fh.attrs.SkipSettingMaxClocks                   = 0;
    fh.attrs.actual_size_in_bytes                   = 0;

    fh.attrs.start_sector                           = 0;

    if( CopyString(fh.attrs.MemoryName, "eMMC", 0, 0, sizeof("eMMC"),sizeof(fh.attrs.MemoryName),sizeof("eMMC")) == 0)
    {
        dbg(LOG_ERROR, "Failed to copy '%s' of length %"SIZE_T_FORMAT" bytes into fh.attrs.MemoryName","eMMC",strlen("eMMC"));
        //ExitAndShowLog(1);
    }

}

void main_firehose(void)
{
    volatile SIZE_T  PacketLoc = 0, BreakPoint = 0;

    //dbg(LOG_INFO,"Binary build date: %s @ %s", __DATE__, __TIME__);



    InitAttributes();

    update_ddr_clock_table();
    updateParamStruct();
    initDDIBuffer();
    initMRStruct();



//
//    CeTLHashTests();
//
  //  sendAndAuthSerialNum();     // sent again in handleNop
  //  sendSupportedFunctions();   // sent again in handleNop

    for (;;)
    {
        PacketLoc = 0;

        // Get XML packet over USB and initialize XML reader.
        if (getXMLPacket() != FIREHOSE_SUCCESS)
        {
            // on any errors send NAK and get next packet, getXMLPacket() would have sent the error
            sendResponse(NAK);
            continue;
        }

        // To be this far fh.xml_buffer has the data

        PacketLoc = DetermineTag((char *)fh.xml_buffer, PacketLoc, FIREHOSE_XML_BUFFER_SIZE);       // This sets CurrentHandlerFunction()

        // PacketLoc should be past the xml and data tags, pointing at the 1st attribute
        // So if PacketLoc is still it 0, something went wrong

        if(PacketLoc==0) {  sendResponse(NAK);          continue;        }


        // Based on the TAG above, some variables need to be reset
        handleResettingAttributesBasedOnTag();



        // Singh TO DO: determine Attributes should not be executed for operations like ddiinfo and nop


        if (!(CurrentHandlerFunction == handleNop ||
              CurrentHandlerFunction == handleDDIInfo ||
              CurrentHandlerFunction == handleRetrain ||
              CurrentHandlerFunction == handleReset ||
              CurrentHandlerFunction == handleDisableDBI ||
              CurrentHandlerFunction == handleReadPowerSetting ||
              CurrentHandlerFunction == handleReadTemperature ))
        {
            // To be this far fh.xml_buffer[PacketLoc] is pointing to the first char of the first attribute
            PacketLoc = DetermineAttributes((char *)fh.xml_buffer, PacketLoc, FIREHOSE_XML_BUFFER_SIZE);
        }



        // PacketLoc should now be past all attributes and pointing at the />
        // So if PacketLoc is still it 0, something went wrong

        if(PacketLoc==0)
        {
            sendResponse(NAK);
            continue;
        }

        // At this point some attributes will need additional processing before
        // we call the Tag Handler function
        if(ParseComplicatedAttributes()==FIREHOSE_ERROR)    // i.e. start_sector="NUM_DISKSECTORS-33."
        {
            logMessage("ParseComplicatedAttributes returned error");
            sendResponse(NAK);
            continue;
        }

        BreakPoint = 2;

        // Below is the function pointer, i.e. tag_handler_t CurrentHandlerFunction;
        CurrentHandlerFunction();
    }
}

void AppendToBuffer(byte *MyBuffer, char *buf, SIZE_T MaxBufferSize)
{
   size_t SpaceLeft, CharsToAdd = 0;
   size_t Length;

   Length       = strlen((const char *)MyBuffer);
   CharsToAdd   = strlen((const char *)buf);    // size of string to append

   //SpaceLeft = FIREHOSE_TX_BUFFER_SIZE - Length - XML_TAIL_LENGTH;      // tx_buffer is transmit_buffer of size FIREHOSE_TX_BUFFER_SIZE
   SpaceLeft = MaxBufferSize - Length - XML_TAIL_LENGTH;      // tx_buffer is transmit_buffer of size FIREHOSE_TX_BUFFER_SIZE


   if(CharsToAdd > SpaceLeft) { return; } // too big to add this

   // NOTE: If you're *not* seeing your messages, increase the size of transmit_buffer[FIREHOSE_TX_BUFFER_SIZE]
   //       or break up your logging

   memscpy((MyBuffer+Length), MaxBufferSize, buf, CharsToAdd);
   MyBuffer[Length+CharsToAdd] = '\0';

} // end void AppendToBuffer(byte *MyBuffer, char *buf)


/*
void InitTXBufferWithXMLHeader(void)
{
   memset(fh.tx_buffer, 0x0, FIREHOSE_TX_BUFFER_SIZE);
   memscpy(fh.tx_buffer, FIREHOSE_TX_BUFFER_SIZE, xml_header, XML_HEADER_LENGTH);
} // end void InitTXBufferWithXMLHeader(void)
*/
void InitBufferWithXMLHeader(byte *MyBuffer, SIZE_T Length)
{
   memset(MyBuffer, 0x0, Length);
   memscpy(MyBuffer, Length, xml_header, XML_HEADER_LENGTH);
   //MyBuffer[XML_HEADER_LENGTH] = '\0';
} // end void InitBufferWithXMLHeader(void)

/*
void AppendToTXBuffer(char *buf)
{
   size_t SpaceLeft, CharsToAdd = 0;
   size_t Length;

   Length       = strlen((const char *)fh.tx_buffer);
   CharsToAdd   = strlen((const char *)buf);    // size of string to append

   SpaceLeft = FIREHOSE_TX_BUFFER_SIZE - Length - XML_TAIL_LENGTH;      // fh.tx_buffer is transmit_buffer of size FIREHOSE_TX_BUFFER_SIZE

   if(CharsToAdd > SpaceLeft) { return; } // too big to add this

   // NOTE: If you're *not* seeing your messages, increase the size of transmit_buffer[FIREHOSE_TX_BUFFER_SIZE]
   //       or break up your logging

   memscpy((fh.tx_buffer+Length), SpaceLeft, buf, CharsToAdd);

} // end void AppendToTXBuffer(char *buf)
*/

char * RemoveBannedChars(char *p)
{
    char *pOrig = p;

    while(*p!='\0')
    {
        if( *p=='"' || *p=='<' || *p=='>')
            *p = '_';

         p++;
    }

    return pOrig;
}

