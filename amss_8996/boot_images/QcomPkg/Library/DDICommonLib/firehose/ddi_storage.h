/*==================================================================
 *
 * FILE:        ddi_storage.h
 *
 * DESCRIPTION:
 *
 *
 *        Copyright � 2015 Qualcomm Technologies Incorporated.
 *               All Rights Reserved.
 *               QUALCOMM Proprietary
 *==================================================================*/

/*===================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *
 * YYYY-MM-DD   who     what, where, why
 * ----------   ---     ----------------------------------------------
 * 2015-04-15   bn      Added GetStorageInfo support
 * 2014-10-02   ah      Major code clean up
 * 2014-07-23   kh      Initial checkin
 */
#ifndef __DDI_STORAGE_H__
#define __DDI_STORAGE_H__

#include "fs_hotplug.h"
#include "fs_hotplug_config.h"
#include "ddi_utils.h"

#define MAX_PARTITIONS_PER_DEVICE 8 // max number of partitions in a storage device

#ifndef _MSC_VER
#include "sdcc_api.h"
#include "ufs_api.h"
#endif


typedef struct {
    hotplug_dev_type current_storage_type; 

    struct hotplug_device *hwhole_dev;  // whole device handle
    struct hotplug_device *hparts[MAX_PARTITIONS_PER_DEVICE];    // all available partitions

    SIZE_T num_partitions;
    SIZE_T sector_size;
    SIZE_T partition_sizes[MAX_PARTITIONS_PER_DEVICE];
    // Needed for UFS
    SIZE_T blocks_per_alloc_unit;
} storage_device_t;

void storage_device_init_hotplug();  // init hotplug without polling
void storage_device_init_struct(storage_device_t *dev);

boolean init_ufs_params(storage_device_t *dev);

// poll for specified type, and open first device and all its hard partitions
boolean storage_device_open(storage_device_t *dev, hotplug_dev_type type);
boolean storage_device_close(storage_device_t *dev);

boolean storage_device_read(storage_device_t *dev, SIZE_T partition, byte *buffer, uint32 start_sector, uint16 num_sectors);
boolean storage_device_write(storage_device_t *dev, SIZE_T partition, byte *buffer, uint32 start_sector, uint16 num_sectors);
boolean storage_device_erase(storage_device_t *dev, SIZE_T partition);

boolean storage_device_set_bootable_partition(storage_device_t *dev, SIZE_T partition);

boolean storage_device_is_valid_partition(storage_device_t *dev, SIZE_T partition);
boolean storage_device_get_num_partition_sectors(storage_device_t *dev, SIZE_T partition, SIZE_T *num_partition_sectors);
boolean storage_device_get_info(storage_device_t *dev, SIZE_T partition);

// FW update and device configure not supported for VS testing
#ifndef _MSC_VER

typedef struct {
    //union {
        sdcc_emmc_gpp_enh_desc_type emmc_extras;
        struct ufs_config_descr 	ufs_extras;
    //};

    int lun_to_grow;	// Extra UFS parameters
} storage_device_extras_t;

boolean storage_device_set_extras(storage_device_t *dev,
                            storage_device_extras_t *extras,    // <- stores into this variable
                            const char* attribute_name,
                            const char* attribute_value);
void storage_device_get_extras(storage_device_t *dev, storage_device_extras_t *extras);
boolean storage_device_commit_extras(storage_device_t *dev, storage_device_extras_t *extras);

boolean storage_device_fw_update(storage_device_t *dev, byte *fw_bin, SIZE_T num_sectors);
boolean storage_get_device_info_raw (storage_device_t *dev, void *storage_info_raw, uint32 *buff_len);

#endif // #ifndef _MSC_VER

#endif
