#ifndef _BOOT_DDI_TOOL_H
#define _BOOT_DDI_TOOL_H
/*===========================================================================

                    BOOT EXTERN QUSB DRIVER DEFINITIONS

DESCRIPTION
  Contains wrapper definition for external QUSB drivers

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when        who      what, where, why
--------    ---      ----------------------------------------------------------
05/07/2015  Singh   Support for DDI
===========================================================================*/

/*

DDI BUFF Structure. Available DWORDS : 100, Used : 44

DDI BUFF STRUCTURE.....
|__________________|DWORD 0-> cookie 1
|__________________|DWORD 1-> cookie 2
|__________________|DWORD 2-> DQ_LO 0 PRFS values
|__________________|DWORD 3-> DQ_LO 1 PRFS values
|__________________|DWORD 4-> DQ_LO 2 PRFS values
|__________________|DWORD 5-> DQ_LO 3 PRFS values
|__________________|DWORD 6-> DQ_LO 4 PRFS values
|__________________|DWORD 7-> DQ_LO 5 PRFS values
|__________________|DWORD 8-> DQ_LO 6 PRFS values
|__________________|DWORD 9-> DQ_LO 7 PRFS values
|__________________|DWORD 10-> DQ_HI 0 PRFS values
|__________________|DWORD 11-> DQ_HI 1 PRFS values
|__________________|DWORD 12-> DQ_HI 2 PRFS values
|__________________|DWORD 13-> DQ_HI 3 PRFS values
|__________________|DWORD 14-> DQ_HI 4 PRFS values
|__________________|DWORD 15-> DQ_HI 5 PRFS values
|__________________|DWORD 16-> DQ_HI 6 PRFS values
|__________________|DWORD 17-> DQ_HI 7 PRFS values
|__________________|DWORD 18-> CA_LO 0 PRFS values
|__________________|DWORD 19-> CA_LO 1 PRFS values
|__________________|DWORD 20-> CA_LO 2 PRFS values
|__________________|DWORD 21-> CA_LO 3 PRFS values
|__________________|DWORD 22-> CA_LO 4 PRFS values
|__________________|DWORD 23-> CA_LO 5 PRFS values
|__________________|DWORD 24-> CA_LO 6 PRFS values
|__________________|DWORD 25-> CA_LO 7 PRFS values
|__________________|DWORD 26-> CA_HI 0 PRFS values
|__________________|DWORD 27-> CA_HI 1 PRFS values
|__________________|DWORD 28-> CA_HI 2 PRFS values
|__________________|DWORD 29-> CA_HI 3 PRFS values
|__________________|DWORD 30-> CA_HI 4 PRFS values
|__________________|DWORD 31-> CA_HI 5 PRFS values
|__________________|DWORD 32-> CA_HI 6 PRFS values
|__________________|DWORD 33-> CA_HI 7 PRFS values
|__________________|DWORD 34-> 32 bits for possible frequencies for which MR registers are changed.
|__________________|DWORD 35-> 32 bits for Print Mask. This will enable the data that will be displayed for training
|__________________|DWORD 36->
_______________________________________________________________________________________________________________________
|8 bits for MR3 for freq 4  |   8 bits for MR3 for freq 3   |   8 bits for MR3 for freq 2   |8 bits for MR3 for freq 1|
-----------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 37->
_____________________________________________________________________________________________________________________
|8 bits for MR3 for freq 8  |   8 bits for MR3 for freq 7   |   8 bits for MR3 for freq 6| 8 bits for MR3 for freq 5|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 38->
_____________________________________________________________________________________________________________________
|8 bits for MR11 for freq 4 |   8 bits for MR11 for freq 3  |   8 bits for MR11 for freq 2| 8 bits for MR11 for freq 1|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 39->
_____________________________________________________________________________________________________________________
|8 bits for MR11 for freq 8 |   8 bits for MR11 for freq 7  |   8 bits for MR11 for freq 6| 8 bits for MR11 for freq 5|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 40->
_____________________________________________________________________________________________________________________
|8 bits for MR22_CS0 for freq 4 |8 bits for MR22_CS0 for freq 3|8 bits for MR22_CS0 for freq 2| 8 bits for MR22_CS0 for freq 1|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 41->
_____________________________________________________________________________________________________________________
|8 bits for MR22_CS0 for freq 8 |8 bits for MR22_CS0 for freq 7|8 bits for MR22_CS0 for freq 6| 8 bits for MR22_CS0 for freq 5|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 42->
_____________________________________________________________________________________________________________________
|8 bits for MR22_CS1 for freq 4 |8 bits for MR22_CS1 for freq 3|8 bits for MR22_CS1 for freq 2| 8 bits for MR22_CS1 for freq 1|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 43->
_____________________________________________________________________________________________________________________
|8 bits for MR22_CS1 for freq 8 |8 bits for MR22_CS1 for freq 7|8 bits for MR22_CS1 for freq 6| 8 bits for MR22_CS1 for freq 5|
---------------------------------------------------------------------------------------------------------------------
|__________________|DWORD 44->     Training Data Display or not
*/


/*==========================================================================

                               INCLUDE FILES

===========================================================================*/

/*

DDI BUFF Structure. Available DWORDS : 100, Used : 44

DDI BUFF STRUCTURE.....
|__________________|DWORD 0-> cookie 1
|__________________|DWORD 1-> cookie 2
|__________________|DWORD 2-> DQ_LO 0 PRFS values
|__________________|DWORD 3-> DQ_LO 1 PRFS values
|__________________|DWORD 4-> DQ_LO 2 PRFS values
|__________________|DWORD 5-> DQ_LO 3 PRFS values
|__________________|DWORD 6-> DQ_LO 4 PRFS values
|__________________|DWORD 7-> DQ_LO 5 PRFS values
|__________________|DWORD 8-> DQ_LO 6 PRFS values
|__________________|DWORD 9-> DQ_LO 7 PRFS values
|__________________|DWORD 10-> DQ_HI 0 PRFS values
|__________________|DWORD 11-> DQ_HI 1 PRFS values
|__________________|DWORD 12-> DQ_HI 2 PRFS values
|__________________|DWORD 13-> DQ_HI 3 PRFS values
|__________________|DWORD 14-> DQ_HI 4 PRFS values
|__________________|DWORD 15-> DQ_HI 5 PRFS values
|__________________|DWORD 16-> DQ_HI 6 PRFS values
|__________________|DWORD 17-> DQ_HI 7 PRFS values
|__________________|DWORD 18-> CA_LO 0 PRFS values
|__________________|DWORD 19-> CA_LO 1 PRFS values
|__________________|DWORD 20-> CA_LO 2 PRFS values
|__________________|DWORD 21-> CA_LO 3 PRFS values
|__________________|DWORD 22-> CA_LO 4 PRFS values
|__________________|DWORD 23-> CA_LO 5 PRFS values
|__________________|DWORD 24-> CA_LO 6 PRFS values
|__________________|DWORD 25-> CA_LO 7 PRFS values
|__________________|DWORD 26-> CA_HI 0 PRFS values
|__________________|DWORD 27-> CA_HI 1 PRFS values
|__________________|DWORD 28-> CA_HI 2 PRFS values
|__________________|DWORD 29-> CA_HI 3 PRFS values
|__________________|DWORD 30-> CA_HI 4 PRFS values
|__________________|DWORD 31-> CA_HI 5 PRFS values
|__________________|DWORD 32-> CA_HI 6 PRFS values
|__________________|DWORD 33-> CA_HI 7 PRFS values
|__________________|DWORD 34-> 32 bits for possible frequencies for which MR registers are changed.
|__________________|DWORD 35-> 32 bits for Print Mask. This will enable the data that will be displayed for training
|__________________|DWORD 36->
_______________________________________________________________________________________________________________________
|8 bits for MR3 for freq 4  |   8 bits for MR3 for freq 3   |   8 bits for MR3 for freq 2   |8 bits for MR3 for freq 1|
-----------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 37->
_____________________________________________________________________________________________________________________
|8 bits for MR3 for freq 8  |   8 bits for MR3 for freq 7   |   8 bits for MR3 for freq 6| 8 bits for MR3 for freq 5|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 38->
_____________________________________________________________________________________________________________________
|8 bits for MR11 for freq 4 |   8 bits for MR11 for freq 3  |   8 bits for MR11 for freq 2| 8 bits for MR11 for freq 1|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 39->
_____________________________________________________________________________________________________________________
|8 bits for MR11 for freq 8 |   8 bits for MR11 for freq 7  |   8 bits for MR11 for freq 6| 8 bits for MR11 for freq 5|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 40->
_____________________________________________________________________________________________________________________
|8 bits for MR22_CS0 for freq 4 |8 bits for MR22_CS0 for freq 3|8 bits for MR22_CS0 for freq 2| 8 bits for MR22_CS0 for freq 1|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 41->
_____________________________________________________________________________________________________________________
|8 bits for MR22_CS0 for freq 8 |8 bits for MR22_CS0 for freq 7|8 bits for MR22_CS0 for freq 6| 8 bits for MR22_CS0 for freq 5|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 42->
_____________________________________________________________________________________________________________________
|8 bits for MR22_CS1 for freq 4 |8 bits for MR22_CS1 for freq 3|8 bits for MR22_CS1 for freq 2| 8 bits for MR22_CS1 for freq 1|
---------------------------------------------------------------------------------------------------------------------

|__________________|DWORD 43->
_____________________________________________________________________________________________________________________
|8 bits for MR22_CS1 for freq 8 |8 bits for MR22_CS1 for freq 7|8 bits for MR22_CS1 for freq 6| 8 bits for MR22_CS1 for freq 5|
---------------------------------------------------------------------------------------------------------------------
|__________________|DWORD 44->     Training Data Display or not
*/




#include "ddr_drivers.h"
#include "ddr_common.h"
#include "boot_logger.h"
#include <string.h>
#include <stdio.h>
#include "../../../Msm8996Pkg/Library/DSFTargetLib/sns_api/HALhwio.h"
#include "../../../Include/api/systemdrivers/pmic/pm_smps.h"
#include "../../../Include/Library/TsensLib.h"
#include "../../../Msm8996Pkg/Library/DSFTargetLib/sns_api/ddr_external.h"


#define DDI_VERSION_MAJOR 1
#define DDI_VERSION_MINOR 9
#define DDI_PLATFORM 8996
#define BUFFER_LENGTH 128



#define MAX_PRFS_LEVELS 8
static unsigned char ddiTrainingBuffer[3072] __attribute__ ((section (".bss.DDITRAINING_BUFFER_SECTION"))) = {0};
static char srcBuffer[1024] __attribute__ ((section (".bss.DDITRAINING_BUFFER_SECTION"))) = {0};
static uint32 patternDataBuf[256]   __attribute__ ((section (".bss.DDITRAINING_BUFFER_SECTION"))) = {0};
static uint32 read_back_pattern[256]  __attribute__ ((section (".bss.DDITRAINING_BUFFER_SECTION"))) = {0};
static  uint8 Data[4][MAX_PRFS_LEVELS][4][2][64]  __attribute__ ((section (".bss.DDITRAINING_BUFFER_SECTION")));
static    uint8 xAxisTitle[50]  __attribute__ ((section (".bss.DDITRAINING_BUFFER_SECTION"))) = {0};
static    uint8 yAxisTitle[100]  __attribute__ ((section (".bss.DDITRAINING_BUFFER_SECTION"))) = {0};
static uint8 perfLevel = 0;

static uint8 regLo = 0;
static uint8 ixDQ = 0;
static uint8 ixDQs = 1;
static uint8 ixCA  = 2;
static uint8 ixCK = 3;
static uint8 ovrDrv = 0;
static uint8 oDt = 2;
static uint8 pU = 1;
static uint8 vOh = 3;
static uint32 maskDQCAOvrDrv  = 0x38;
static uint32 maskDQCAhiOdt  = 0x3;
static uint32 maskDQCAloOdt  = 0x80000000;
static uint32 maskDQCAPu  = 0xE00;
static uint32 maskDQDQsCACKVoh  = 0x18000 ;

static uint32 shiftDQCAOvrDrv  =  3;
static uint32 shiftDQCAloOdt =  31;
static uint32 shiftDQCAhiOdt =  1;
static uint32 shiftDQCAPu =  9 ;
static uint32 shiftDQDQsCACKVoh  =  15;

static uint32 maskDQsCKOvrDrv  = 0x1C0;
static uint32 maskDQsCKOdt =  0x1C ;
static uint32 maskDQsCKPu  = 0x7000;
static uint32 shiftDQsCKOvrDrv   = 6;
static uint32 shiftDQsCKOdt   = 2;
static uint32 shiftDQsCKPu = 12;



extern uint32 write_in_pattern[];



#define MAX_CLOCK_FREQ 15

//static  ddiMRStruct ddiMRS[15];




boolean ddi_DDRTestOwnAddr(int ch , int cs, char* tag);
boolean ddi_DDRDataLinesTest(int ch , int cs, char* tag);
boolean ddi_DDRAddressLinesTest(int ch , int cs, char* tag);
boolean ddi_ChangePHYSettings(int testline,int prfs,int ovrdrv,int PU,int ODT,int VOH,int ODTE );
void ddi_ReadPHYSettings(int testline,int prfs );

void retrainDDR(void);
void sayHello(char* tag);
void displayData(void * buffer, uint32 size,void* tArgs, void *training_params_ptr);
void clearFlashParams();
void clearDDIBuff();
void InitDDITransport();
void getBaseSize(int ch, int cs);
boolean ddi_WriteTest(uint8 ch, uint8 cs, uint32 loopcnt, boolean infitinity);
void resetDDI();
boolean  disableDBI();
void changeDRAMSettings(uint32 clk_in_khz);
void sendTrainingRsp();
char *ddi_strcpy(char *restrict dest, const char *restrict src);
void update_soc_drive_strength(void);
void deInitTransport();
boolean ddi_ChangeMR11(int freq, int mr11Val);
boolean ddi_ChangeMR3(int freq, int mr3Val );
void update_ddr_clock_table();
boolean ddi_ChangeBIMCClock(int Ix);
void ddi_readPowerSetting();
void ddi_readTemperature();
boolean ddi_ChangeVDDQ(int vddVal);
boolean ddi_ChangeVDD1(int vddVal);
boolean ddi_ReadWriteTest(uint8 ch, uint8 cs, uint32 loopcnt, boolean infitinity);
boolean ddi_ReadTest(uint8 ch, uint8 cs, uint32 loopcnt, boolean infitinity);
void ddi_readMRReg(uint8 ch, uint8 cs, uint8 reg);
boolean ddi_writeMRReg(uint8 ch, uint8 cs, uint8 reg, uint8 regVal);
void initMRStruct();
void applyMRVal();




static uint64 gBase = 0;
static uint64 gSize = 0;
#define DDR_TEST_OFFSET 0x7000000
#define BIMC_S_DDR0_DPE_CONFIG_9 0x00434034
#define BIMC_S_DDR1_DPE_CONFIG_9 0x00440034

#define BIMC_S_DDR0_DPE_CONFIG_4 0x00434020
#define BIMC_S_DDR1_DPE_CONFIG_4 0x00440020

#define DPE_CONFIG_9_VAL_DISABLE 0x101000
#define DPE_CONFIG_4_VAL_DISABLE 0x1




#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_0_LO_CFG_ADDR(x)         (x+0x00000368)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_0_LO_CFG_ADDR(x)         (x+0x00000378)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_0_LO_CFG_ADDR(x)         (x+0x00000388)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_0_LO_CFG_ADDR(x)         (x+0x0000039c)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_0_LO_CFG_ADDR(x)         (x+0x000003ac)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_0_LO_CFG_ADDR(x)         (x+0x000003bc)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_0_LO_CFG_ADDR(x)         (x+0x0000043c)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_0_LO_CFG_ADDR(x)         (x+0x0000044c)

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_0_HI_CFG_ADDR(x)         (x+0x0000036c)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_0_HI_CFG_ADDR(x)         (x+0x0000037c)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_0_HI_CFG_ADDR(x)         (x+0x0000038c)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_0_HI_CFG_ADDR(x)         (x+0x000003a0)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_0_HI_CFG_ADDR(x)         (x+0x000003b0)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_0_HI_CFG_ADDR(x)         (x+0x000003c0)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_0_HI_CFG_ADDR(x)         (x+0x00000440)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_0_HI_CFG_ADDR(x)         (x+0x00000450)

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ADDR(x)         (x+0x00000370)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_ADDR(x)         (x+0x00000380)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_ADDR(x)         (x+0x00000390)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ADDR(x)         (x+0x000003a4)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ADDR(x)         (x+0x000003b4)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ADDR(x)         (x+0x000003c4)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ADDR(x)         (x+0x00000444)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ADDR(x)         (x+0x00000458)

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ADDR(x)         (x+0x00000374)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG_ADDR(x)         (x+0x00000384)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG_ADDR(x)         (x+0x00000394)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG_ADDR(x)         (x+0x000003a8)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG_ADDR(x)         (x+0x000003b8)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG_ADDR(x)         (x+0x000003c8)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG_ADDR(x)         (x+0x00000448)
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG_ADDR(x)         (x+0x0000045c)

static uint32 ddr_phy_dq_config_tool[16]={

    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),
};


static uint32 ddr_phy_ca_config_tool[16]={
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),


    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
    HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),
};


static uint32 ddr_phy_ca_config_lo_tool[] =
{
    //CH0 CA0 //CH0_CA0_  DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

   //CH0 CA1
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

   //CH1 CA0
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

   //CH1 CA1
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

};


static uint32 ddr_phy_ca_config_hi_tool[] =
{
    //CH0 CA0
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),

   //CH0 CA1
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000480800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),


   //CH1 CA0
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000483800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),

   //CH1 CA1
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
   HWIO_ADDRX(0x0000000000484000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),

};

// register access definitions for DQ/DQS drive strength from SOC side
static uint32 ddr_phy_dq_config_lo_tool[] =
{
    //CH0 DQ0
   HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

   //CH0 DQ1
   HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

   //CH0 DQ2
   HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

   //CH0 DQ3
   HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

   //CH1 DQ0
   HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),


   //CH1 DQ1
   HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

   //CH1 DQ2
   HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

   //CH1 DQ3
   HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG),
   HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG),

};
static uint32 ddr_phy_dq_config_hi_tool[] =
{
    //CH0 DQ0
    HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),

    //CH0 DQ1
    HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000481800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),

    //CH0 DQ2
    HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),

    //CH0 DQ3
    HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000482800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),

    //CH1 DQ0
    HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000484800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),

    //CH1 DQ1
    HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),

    //CH1 DQ2
    HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000485800, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),

    //CH1 DQ3
    HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG),
    HWIO_ADDRX(0x0000000000486000, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG),

};


#endif /* _BOOT_DDI_TOOL_H */
