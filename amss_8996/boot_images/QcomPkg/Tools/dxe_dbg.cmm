;============================================================================
;  Name:
;    dxe_dbg.cmm
;
;  Description:
;     Debug Dxe runtime loading and dispatching
;
; Copyright (c) 2010-2015 Qualcomm Technologies Incorporated.
; All Rights Reserved.
; Qualcomm Technologies Confidential and Proprietary
;
;----------------------------------------------------------------------------
;============================================================================
;
;                        EDIT HISTORY FOR MODULE
;
;
;
;when         who     what, where, why
;----------   ---     ----------------------------------------------------------
;2015-06-22   vk      Add target-specific win.cmm
;2015-03-19   bh      Robust debug script
;2015-03-10   jb      Add additional valid area for dxe sym workaround
;2015-02-20   vk      Do not reset sym
;2015-02-12   bh      Change RD: to A:
;2015-01-27   bh      Unifying 32/64-bit scripts
;2014-10-24   vk      Add MdmBasePkg
;2014-10-08   vk      Call t32_options.cmm
;2014-08-21   wg      Updated argument order received by dxe_dbg.cmm
;2014-01-08   wg      Added 50ms wait if 8994, to ensure breaks are hit correct
;2014-31-07   wg      Updated UEFI Debug setup for 8994
;2014-14-07   wg      Added UEFI Debug Mode check and clear UefiDebugCookie
;2014-25-06   aus     Fixed for 8916
;2014-06-03   vk      Cleanup
;2014-05-08   vk      Look for uefi.mbn in Bin32
;2014-04-15   vk      Do not exit if chip revision is not available - for virtio
;2014-04-09   vk      Remove A,B family changes, cleanup
;2014-04-04   vk      Load first four bytes for file type identification
;2014-02-11   vk      Update read macro
;2013-11-18   vk      Workaround for virtio not returning correct chip id
;2013-11-15   vk      Add EfiHeaderOffset parameter
;2013-11-10   vk      Add RVCT6 and GCC path
;2013-11-01   aus     Added support to generate elf images
;2013-10-31   aus     Added support to read chipset information from the HW
;2013-10-10   vk      Add Sec64 support
;2013-05-01   vk      Virtio / RUMI support
;2013-04-13   plc     Add support for 8x10
;2013-03-19   jz      Cleanup CpuArch, fix for 8x26
;2013-03-13   nk      Swapd input param to fix it for V1.
;2013-03-13   vk      Swap lines for TZ update fix
;2013-03-06   vk      Add change for TZ update, and CpuArch param
;2013-03-03   yg      Fix RAM Debug option
;2013-01-23   yg      Move to use ARMCT501
;2013-01-08   yg      Fix debug script to work, add go param
;2012-12-11   yg      Configure Program bkpts to be only Onchip
;2012-11-22   vk      Update for 8974 debug_uefi.cmm
;2012-10-18   nk      Added suport for std_dbg for 8974
;2012-05-02   vishalo Check RVCT 4.1 first
;2012-05-01   vishalo Added RVCT 4.1 support
;2011-11-16   yg      Load sybols from correct location in release build
;2011-09-26   yg      Added Dxe debug capability
;2011-08-25   jz      Added support for release build and bp at CEntryPoint for sec
;2011-05-18   niting  Changed y.spath inclusions
;2011-05-06   MK      Added image to debug option
;2011-03-16   niting  Added manual load option
;2010-12-03   yg      Initial Version
;============================================================================;

area.clear
area.reset
area.create UEFI_Logs 1000. 200.
area.view UEFI_Logs


global &ChipsetFamily
global &ChipsetName
global &ChipsetID
global &IgnoreSymPathParts
global &PathSep
local &BuildOutDir
local &SecLoadAddr
local &SecSymFile
local &SecPath
local &DxeAddr
local &TargetPkg
local &Target
local &RamEntryAddr
local &SecXtn
local &DebugDxe
local &TestPath
local &UefiMbn
local &UefiDebugCookieAddr

;
;  [ImageName] can be one of the following options:
;  SEC : Debug Sec initialization code
;  DXE : Debug Dxe initialization code
;  <str> : Where str is free string that can match to any part of the image name
;          that we want to debug. eg: timer will let debug RealTimeClock and TimerDxe
;
entry &TargetPkg &RamEntryAddr &UefiDebugCookieAddr &ImageName &ImageName1
  
  ;do QcomPkg/Tools/cmm/t32_options.cmm
 
  &Target=string.cut("&TargetPkg",-3)

  ; Assumes you are at root of build
  &CwDir=os.pwd()

  ; Initialize variables
  &DebugDxe=0

  ; Break at UEFI Sec entry point
  ;b.d
  b.sel program onchip


  do QcomPkg/Tools/InitOffsets.cmm

  if ("&ImageName"=="ram"||"&ImageName"=="RAM")
  (
    &ImageName="&ImageName1"
    &UefiMbn="&CwDir/QcomPkg/&TargetPkg/&BinFolder/uefi.mbn"

    if (!os.file("&UefiMbn"))
    (
      print %String %ERROR "Unable to find &UefiMbn exiting"
      enter
      END
    )

    ; Default supported Image is elf now
    data.load.elf &UefiMbn
    r.s PC &RamEntryAddr
  )

  ; Sec Executable image is located after FD header offset (0x158)
  &SecLoadAddr=&RamEntryAddr+&SecLoadAddrOffset

  ; Get the symbol file path
  gosub GetSymFilePath &RamEntryAddr
  entry &SecSymFile

  if (!os.file("&SecSymFile"))
  (
    print %ERROR "Unable to find &SecSymFile !"
    END
  )

  print "Sec dll file: &SecSymFile"

  data.load.elf &SecSymFile &SecLoadAddr /nocode /strippart &IgnoreSymPathParts

  print "Loaded Sec symbols at &SecLoadAddr from &SecSymFile"

  Print "Loaded image, target setup complete."

  ; Stop at UefiDebugCookie() entry point before Main() in UEFI

  ; Clear UEFI Debug Cookie
  D.S A:&UefiDebugCookieAddr %LE %LONG 0x0

  print
  print "You are stopped in CENTRY() before Main() in UEFI"

  go Main
  print "go Main ..."
  wait !run()
  print "Break at Main, ok ..."

  ; Setup windows
  if (os.file("&CwDir/QcomPkg/&TargetPkg/Tools/win.cmm"))
  (    
    do QcomPkg/&TargetPkg/Tools/win
  )
  else 
  (
    do QcomPkg/Tools/win
  )

  if ("&ImageName"=="sec"||"&ImageName"=="SEC")
  (
    ; Stop at SEC entry point to allow user to do some stuff before continuing
    print
    print "Press enter in area window to continue debugging Dxe..."
    print "Execute ""continue"" to get focus in area window"
    &DebugDxe=1
    area
    enter
  )

  ; if name says go then we should just execute without loading any symbols
  if ("&ImageName"=="go"||"&ImageName"=="GO")
  (
    go
    enddo
  )

  ; If Dxe is the param make sure it doesnt stop on all drivers having dxe in string
  if ("&ImageName"=="dxe"||"&ImageName"=="DXE")
  (
    &ImageName=""
    &DebugDxe=1
  )

  ; Execute until the Dxe core is loaded into memory
  ; This could change with EDK II sync changes code
  go LoadDxeCoreFromFfsFile\24
  wait !run()

  ; Now if CPU has executed and copied the Dxe core into memory, the variable
  ; ImageAddress will have the value of where is it.
  &DxeAddr=v.value(ImageAddress)

  ; TODO: Fix me remove this block after the fix
  ; Workaround for symbol problem until its fixed
  if (&DxeAddr>0xFFFFFFFF)&&((&DxeAddr<0x4000000000)||(&DxeAddr>0x4010000000))
  (
    local &ImgAddrPtr
    &ImgAddrPtr=v.value(&ImageAddress)
    &ImgAddrPtr=&ImgAddrPtr-0x20
    &DxeAddr=DATA.QUAD(A:&ImgAddrPtr)
  )

  ; Dxe efi file loaded into memory, executable code is after efi header offset (from InitOffsets.cmm)
  &DxeAddr=&DxeAddr+&EfiHeaderOffset

  ; Load DxeCore symbols at that address
  data.load.elf &BuildOutDir/MdeModulePkg/Core/Dxe/DxeMain/DEBUG/DxeCore.dll &DxeAddr /nocode /noclear /strippart &IgnoreSymPathParts

  print "Loaded DXE symbols at &DxeAddr from &BuildOutDir/MdeModulePkg/Core/Dxe/DxeMain/DEBUG/DxeCore.dll"

  ; Execute until DxeMain
  go DxeMain
  wait !run()

  ; Set Breakpoint at which the Dxe drivers would be loaded into RAM.
  ; The script will load appropriate image symbols at that location and set
  ; Break point into the entrypoint so that the driver could be debugged.
  B.S   CoreLoadPeImage\249 /P /CMD "do QcomPkg/Tools/load_drvr_sym.cmm &ImageName"

  ; Execute to start loading the Dxe drivers
  if (&DebugDxe!=1)
  (
    go
  )

enddo

; Read the symbol file path from the elf image loaded in memory
GetSymFilePath:
  entry &UefiBaseAddr

  local &ImgBase
  local &Rva
  local &ElfFilePath
  local &LoadFilePathPos
  local &LoadFilePath
  local &DxeAddr
  local &ScanLocation

  ; Load Sec symbols first
  &Rva=data.long(A:&UefiBaseAddr+&RvaOffset)
  &Rva=&Rva-&RvaOffsetSub
  &ElfFilePath=data.string(A:&UefiBaseAddr+&Rva)
  &ImgBase=&UefiBaseAddr+&SecLoadAddrOffset

  ; Search for Build folder in the file path
  &LoadFilePathPos=string.scan("&ElfFilePath","Build",0)

  &PathSep=string.mid("&ElfFilePath",&LoadFilePathPos-1,1)

  ; Add Symbols paths
  ;y.spath.reset
  y.spath.SETBASEDIR .

  &IgnoreSymPathParts=0
  &ScanLocation=string.scan("&ElfFilePath","&PathSep",0)
  while &ScanLocation<&LoadFilePathPos
  (
    &IgnoreSymPathParts=&IgnoreSymPathParts+1
    &ScanLocation=string.scan("&ElfFilePath","&PathSep",&ScanLocation+1)
  )

  ; Extract only the elf file path relative to current location
  &LoadFilePath=string.cut("&ElfFilePath",&LoadFilePathPos)

  ; Check if the DxeCore Symbol file is existing, attempt to load only if its present
  if (!os.file("&LoadFilePath"))
  (
    print "COULD NOT LOCATE &LoadFilePath"
    cd &CwDir
    END
  )
  &LoadFilePathPos=string.scan("&LoadFilePath","QcomPkg",0)
  &BuildOutDir=string.mid("&LoadFilePath",0,&LoadFilePathPos-1)
return &LoadFilePath


