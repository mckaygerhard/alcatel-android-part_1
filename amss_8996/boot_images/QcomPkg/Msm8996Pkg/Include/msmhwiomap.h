/*
===========================================================================
*/
/**
  @file msmhwiomap.h 
  @brief Auto-generated HWIO base map include file.
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================


  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * Declare supported bases using user-defined HWIO_MAP_ENTRY macro.
 *--------------------------------------------------------------------------*/

HWIO_MAP_ENTRY(CNOC_0_BUS_TIMEOUT)
HWIO_MAP_ENTRY(CNOC_1_BUS_TIMEOUT)
HWIO_MAP_ENTRY(CNOC_2_BUS_TIMEOUT)
HWIO_MAP_ENTRY(CNOC_3_BUS_TIMEOUT)
HWIO_MAP_ENTRY(CNOC_4_BUS_TIMEOUT)
HWIO_MAP_ENTRY(CNOC_5_BUS_TIMEOUT)
HWIO_MAP_ENTRY(CNOC_6_BUS_TIMEOUT)
HWIO_MAP_ENTRY(CNOC_7_BUS_TIMEOUT)
HWIO_MAP_ENTRY(CNOC_8_BUS_TIMEOUT)
HWIO_MAP_ENTRY(CNOC_9_BUS_TIMEOUT)
HWIO_MAP_ENTRY(XPU_CFG_CPR_APU1132_2)
HWIO_MAP_ENTRY(XPU_CFG_SNOC_CFG_MPU1032_4_M16L12_AHB)
HWIO_MAP_ENTRY(XPU_CFG_ANOC0_CFG_MPU1032_4_M16L12_AHB)
HWIO_MAP_ENTRY(XPU_CFG_ANOC1_CFG_MPU1032_4_M16L12_AHB)
HWIO_MAP_ENTRY(XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB)
HWIO_MAP_ENTRY(XPU_CFG_QDSS_MPU1132_3_M23L12_AHB)
HWIO_MAP_ENTRY(XPU_CFG_SSC_MPU1032_10_M22L11_AHB)
HWIO_MAP_ENTRY(PCIE20_AHB2PHY)
HWIO_MAP_ENTRY(RAMBLUR_PIMEM)
HWIO_MAP_ENTRY(OCIMEM_WRAPPER_CSR)
HWIO_MAP_ENTRY(SPDM_WRAPPER_TOP)
HWIO_MAP_ENTRY(CPR3)
HWIO_MAP_ENTRY(MX_CPR3)
HWIO_MAP_ENTRY(ISTARI_SKL_WRAPPER)
HWIO_MAP_ENTRY(MSS_MPU_MPU1032A_16_M35L12_AXI_36)
HWIO_MAP_ENTRY(MSS_Q6_MPU_MPU1032A_16_M35L12_AXI_36)
HWIO_MAP_ENTRY(ANOC1_MPU_MPU1032A_16_M35L12_AXI_36)
HWIO_MAP_ENTRY(ANOC2_MPU_MPU1032A_16_M35L12_AXI_36)
HWIO_MAP_ENTRY(RPM_SS_MSG_RAM_START_ADDRESS)
HWIO_MAP_ENTRY(RPM_MSG_RAM)
HWIO_MAP_ENTRY(SECURITY_CONTROL)
HWIO_MAP_ENTRY(PRNG_CFG_PRNG_TOP)
HWIO_MAP_ENTRY(BOOT_ROM_START_ADDRESS)
HWIO_MAP_ENTRY(BOOT_ROM)
HWIO_MAP_ENTRY(RPM_CODE_RAM_START_ADDRESS)
HWIO_MAP_ENTRY(RPM)
HWIO_MAP_ENTRY(RPM_DATA_RAM_START_ADDRESS)
HWIO_MAP_ENTRY(CLK_CTL)
HWIO_MAP_ENTRY(BIMC)
HWIO_MAP_ENTRY(DDR_SS)
HWIO_MAP_ENTRY(MPM2_MPM)
HWIO_MAP_ENTRY(DCC_WRAPPER)
HWIO_MAP_ENTRY(DCC_RAM_START_ADDRESS)
HWIO_MAP_ENTRY(CONFIG_NOC)
HWIO_MAP_ENTRY(SYSTEM_NOC)
HWIO_MAP_ENTRY(A0_NOC_AGGRE0_NOC)
HWIO_MAP_ENTRY(A1_NOC_AGGRE1_NOC)
HWIO_MAP_ENTRY(A2_NOC_AGGRE2_NOC)
HWIO_MAP_ENTRY(MMSS_NOC)
HWIO_MAP_ENTRY(PERIPH_NOC)
HWIO_MAP_ENTRY(PCIE_0_PCIE20_WRAPPER_AHB)
HWIO_MAP_ENTRY(PCIE_1_PCIE20_WRAPPER_AHB)
HWIO_MAP_ENTRY(PCIE_2_PCIE20_WRAPPER_AHB)
HWIO_MAP_ENTRY(UFS_UFS_REGS)
HWIO_MAP_ENTRY(CRYPTO0_CRYPTO_TOP)
HWIO_MAP_ENTRY(IPA_0_IPA_WRAPPER)
HWIO_MAP_ENTRY(CORE_TOP_CSR)
HWIO_MAP_ENTRY(MMSS)
HWIO_MAP_ENTRY(TLMM)
HWIO_MAP_ENTRY(LPASS_Q6_SMMU_QSMMUV2_WRAP_LPASS_Q6_ISTARI)
HWIO_MAP_ENTRY(LPASS_CORE_SMMU_QSMMUV2_WRAP_LPASS_CORE_ISTARI)
HWIO_MAP_ENTRY(ANOC0_SMMU_QSMMUV2_WRAP_PCIE_ISTARI)
HWIO_MAP_ENTRY(ANOC1_SMMU_QSMMUV2_WRAP_ANOC1_ISTARI)
HWIO_MAP_ENTRY(ANOC2_SMMU_QSMMUV2_WRAP_ANOC2_ISTARI)
HWIO_MAP_ENTRY(MDM_Q6_SMMU_QSMMUV2_WRAP_MDM_Q6_ISTARI)
HWIO_MAP_ENTRY(SSC)
HWIO_MAP_ENTRY(SSC_RAM_START_ADDRESS)
HWIO_MAP_ENTRY(MODEM_TOP)
HWIO_MAP_ENTRY(QDSP6SS_TCM)
HWIO_MAP_ENTRY(QDSS_QDSS_APB)
HWIO_MAP_ENTRY(HMSS_DBG_HMSS_DEBUG)
HWIO_MAP_ENTRY(PMIC_ARB)
HWIO_MAP_ENTRY(L3_TCM)
HWIO_MAP_ENTRY(HMSS_QLL)
HWIO_MAP_ENTRY(SNOC_1_BUS_TIMEOUT)
HWIO_MAP_ENTRY(SNOC_2_BUS_TIMEOUT)
HWIO_MAP_ENTRY(SYSTEM_IMEM)
HWIO_MAP_ENTRY(MMSS_VMEM)
HWIO_MAP_ENTRY(USB30_PRIM)
HWIO_MAP_ENTRY(PERIPH_SS)
HWIO_MAP_ENTRY(QDSS_STM)
HWIO_MAP_ENTRY(LPASS)
HWIO_MAP_ENTRY(LPASS_LPM)
HWIO_MAP_ENTRY(LPASS_TCM)
HWIO_MAP_ENTRY(HMSS)
HWIO_MAP_ENTRY(PIMEM)
HWIO_MAP_ENTRY(PCIE_0_PCIE20_WRAPPER_AXI)
HWIO_MAP_ENTRY(PCIE_1_PCIE20_WRAPPER_AXI)
HWIO_MAP_ENTRY(PCIE_2_PCIE20_WRAPPER_AXI)
HWIO_MAP_ENTRY(MODEM)
HWIO_MAP_ENTRY(DDR_MEM)

