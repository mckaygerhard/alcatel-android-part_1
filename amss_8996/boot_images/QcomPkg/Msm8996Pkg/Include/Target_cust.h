#ifndef _TARGET_CUST_H
#define _TARGET_CUST_H

/*===========================================================================

                                Target_cust
                                Header File

GENERAL DESCRIPTION
  This header file contains target specific declarations and definitions

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright 2014, 2015, 2016 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who          what, where, why
--------   --------     ----------------------------------------------------------
06/20/16   sk           Added 6GB size mapping tables to DDR_MEM_SIZE
12/18/15   kedara       Fix XBL Buffer base address and resize SDI buffer size
11/17/15   kedara       Relocate pre ddr init hash buffer to RPM code ram
10/14/15   kedara       Relocate Stack to reclaim bootrom L2 TCM buffer
10/05/15   kedara       Adjust buffer sizes for code, ZI
10/05/15   kedara       Code Cleanup, remove commented out code
10/01/15   kedara       Increased SCL_SBL1_OCIMEM_DATA_SIZE by 4kb to relocate L2 TCM ZI
09/25/15   rparekh      Changed sbl1_code size to be 4K aligned, adjusted sbl1_data and sbl1_data_zi sizes
08/04/15   kedara       Reorganize ZI buffers for loader, deviceprogrammer.
07/28/15   rparekh      Move PMIC image, reduce RPM coderam size, move deviceprogrammer ZI base
07/21/15   kedara       Reorganize XBL ZI buffer, increase code size
07/20/15   kedara       Resize SDI debug segment size.
07/18/15   kedara       Added SIZE_1GB_MASK.
07/09/15   kedara       Increase XBL rpm code ram dump size to 160kb, enable IPA dumps
07/08/15   ck           Increased SCL_SBL1_BOOT_LOG_BUF_SIZE to support enhanced boot logging
06/24/15   ck           Added defines to support enhanced boot logging
06/08/15   rparekh      Updated SCL_DDR_TRAINING_DATA_BUF_BASE and SCL_DDR_TRAINING_DATA_BUF_SIZE
05/21/15   kedara       Reorganize XBL rpm code ram allocations
05/20/15   kedara       Increase boot log buffer.
05/05/15   kedara       resize XBL Loader L2 TCM buffer allocations.
04/23/15   plc          Add infrastructure for boot-profiling
04/19/15   kedara       Increase XBL loader code size.
04/13/15   kedara       Increase Pmic elf buffer size to 70KB.
04/13/15   ck           Adjusted SCL_SBL1_CODE_SIZE to be 4KB aligned
04/01/15   kedara       Added SCL_pIMEM_MINIMUM_SIZE.
02/20/15   kedara       Update RPM code ram buffer sizes
02/19/15   kedara       Added SCL_SYSTEM_DEBUG_QSEE_INFO_BASE
02/18/15   wkimberl     Move device programmer buffers to OCIMEM.
02/12/15   kedara       Increase XBL region RPM code ram buffer size.
02/09/15   kedara       Added IPA region defines.
02/03/15   wkimberl     Add section for DeviceProgrammer FH Buffers.
02/02/15   wkimberl     Add section for DeviceProgrammer USB Buffers.
01/29/15   kedara       update memory map for sec.dat image base update.
01/21/15   kedara       Reduce pre-DDR init Page table size, increase code size
01/14/15   kedara       update SCL_RAMDUMP_DLOAD_BUFFERS_SIZE
01/08/15   bhsriniv     Add Deviceprogrammer variables
01/08/15   vphan        Increase Deviceprogrammer data size to 32KB
01/08/15   kedara       Increase Deviceprogrammer image size, to accomodate secImg lib
01/05/15   kedara       Increase Loader image size to 402KB to accomodate security libs.
12/11/14   ck           Added FEATURE_EMPTY_L1_PAGE_TABLE for targets that cannot use PBL's
12/04/14   kedara       Increase Loader image size to 386KB.
12/04/14   kedara       Increase DDR size.
11/26/14   ck           Added FEATURE_USE_PBL_SECBOOT_API
11/20/14   ck           Increased RamDump size.
10/24/14   kedara       Increase XBL Loader size.
10/03/14   kedara       Add base address, size defines for loading PMIC image.
05/02/14   kedara       Initial version
============================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/*===========================================================================
              DEFINES FOR TARGET (equivalent to <target>.builds src)
===========================================================================*/


/*===========================================================================
                      DEFINES FOR 8996 MEMORY MAP
===========================================================================*/
#define  SIZE_256B  0x00000100
#define  SIZE_1KB   0x00000400
#define  SIZE_2KB   0x00000800
#define  SIZE_4KB   0x00001000
#define  SIZE_6KB   0x00001800
#define  SIZE_8KB   0x00002000
#define  SIZE_10KB  0x00002800
#define  SIZE_12KB  0x00003000
#define  SIZE_16KB  0x00004000
#define  SIZE_18KB  0x00004800
#define  SIZE_20KB  0x00005000
#define  SIZE_22KB	0x00005800
#define  SIZE_24KB  0x00006000
#define  SIZE_28KB  0x00007000
#define  SIZE_30KB  0x00007800
#define  SIZE_32KB  0x00008000
#define  SIZE_34KB  0x00008800
#define  SIZE_38KB  0x00009800
#define  SIZE_36KB  0x00009000
#define  SIZE_40KB  0x0000A000
#define  SIZE_42KB  0x0000A800
#define  SIZE_44KB  0x0000B000
#define  SIZE_48KB  0x0000C000
#define  SIZE_52KB  0x0000D000
#define  SIZE_56KB  0x0000E000
#define  SIZE_60KB  0x0000F000
#define  SIZE_64KB  0x00010000
#define  SIZE_66KB	0x00010800
#define  SIZE_68KB  0x00011000
#define  SIZE_70KB  0x00011800
#define  SIZE_76KB  0x00013000
#define  SIZE_80KB  0x00014000
#define  SIZE_84KB  0x00015000
#define  SIZE_88KB  0x00016000
#define  SIZE_92KB  0x00017000
#define SIZE_112KB  0x0001C000
#define SIZE_120KB  0x0001E000
#define SIZE_128KB  0x00020000
#define SIZE_136KB  0x00022000
#define SIZE_140KB  0x00023000
#define SIZE_144KB  0x00024000
#define SIZE_160KB  0x00028000
#define SIZE_192KB  0x00030000
#define SIZE_216KB  0x00036000
#define SIZE_232KB  0x0003A000
#define SIZE_256KB  0x00040000
#define SIZE_294KB  0x00049800
#define SIZE_300KB  0x0004B000
#define SIZE_312KB  0x0004E000
#define SIZE_320KB  0x00050000
#define SIZE_324KB  0x00051000
#define SIZE_328KB  0x00052000
#define SIZE_332KB  0x00053000
#define SIZE_336KB  0x00054000
#define SIZE_338KB  0x00054800
#define SIZE_340KB  0x00055000
#define SIZE_344KB  0x00056000
#define SIZE_348KB  0x00057000
#define SIZE_350KB  0x00057800
#define SIZE_372KB  0x0005D000 // VERTU - qiuwei - BUG-2012951
#define SIZE_400KB  0x00064000
#define SIZE_402KB  0x00064800
#define SIZE_416KB  0x00068000
#define SIZE_456KB  0x00072000
#define SIZE_512KB  0x00080000
#define   SIZE_1MB  0x00100000
#define   SIZE_2MB  0x00200000
#define   SIZE_6MB  0x00600000
#define  SIZE_16MB  0x01000000
#define  SIZE_17MB  0x01100000
#define   SIZE_1GB  0x40000000
#define SIZE_1_5GB  0x60000000
#define   SIZE_2GB  0x80000000
#define   SIZE_6GB  0x180000000

#define SIZE_1GB_MASK  (SIZE_1GB -1)

#define SCL_L2_TCM_BASE_ADDR 0x06200000
#define SCL_L3_TCM_BASE_ADDR 0x06000000
#define SCL_RPM_CODE_RAM_BASE 0x00200000
#define SCL_RPM_CODE_RAM_SIZE SIZE_160KB

/* XBL rpm code ram buffer to span across [0x0021E000 to 0x00224000] */
#define SCL_RPM_CODE_RAM_XBL_BASE (SCL_RPM_CODE_RAM_BASE + SIZE_120KB)
#define SCL_RPM_CODE_RAM_XBL_SIZE SIZE_24KB

#define SCL_RPM_DATA_RAM_BASE 0x00290000
#define SCL_RPM_DATA_RAM_SIZE SIZE_80KB
#define SCL_RPM_MSG_RAM_BASE 0x00068000
#define SCL_RPM_MSG_RAM_SIZE SIZE_24KB
#define SCL_IMEM_BASE 0x06680000
#define SCL_IMEM_SIZE SIZE_256KB
#define SCL_SBL1_TOTAL_SIZE SIZE_400KB

#define SCL_SBL1_IMAGE_BASE 0x06208000

#define SCL_SBL1_IMAGE_SIZE SIZE_416KB
#define SCL_SBL1_CODE_BASE SCL_SBL1_IMAGE_BASE
#define SCL_SBL1_CODE_SIZE SIZE_372KB // VERTU - qiuwei - BUG-2012951

#define SCL_SBL1_VECTOR_BASE SCL_SBL1_CODE_BASE
#define SCL_SBL1_VECTOR_SIZE 0x00000020
#define SCL_SBL1_STACK_BASE SCL_L2_TCM_BASE_ADDR
#define SCL_SBL1_STACK_SIZE SIZE_12KB
//Base address of stack is higher end-address of stack region
#define SBL1_STACK_BASE (SCL_SBL1_STACK_BASE + SCL_SBL1_STACK_SIZE)
#define SCL_SBL1_DATA_BASE (SCL_SBL1_CODE_BASE + SCL_SBL1_CODE_SIZE)
#define SCL_SBL1_DATA_SIZE SIZE_44KB
#define SCL_SBL1_DATA_ZI_BASE  (SCL_SBL1_SHARED_FUNCTIONS_TABLE_BASE + SCL_SBL1_SHARED_FUNCTIONS_TABLE_SIZE) // VERTU - qiuwei - BUG-2012951
#define SCL_SBL1_DATA_ZI_SIZE SIZE_24KB

#define SCL_DDR_TRAINING_DATA_BUF_BASE (SCL_SBL1_OCIMEM_DATA_BASE + SCL_SBL1_OCIMEM_DATA_SIZE)

/*RPM Code Ram memory map defines */
#define SCL_SBL1_RPM_CODE_BASE  SCL_RPM_CODE_RAM_XBL_BASE
#define SCL_SBL1_RPM_CODE_SIZE  SIZE_24KB

/* RPM Code RAM memory map defines for DDR training image */
 #define SCL_DDR_TRAINING_CODE_BASE      0x00207000
#define SCL_DDR_TRAINING_CODE_SIZE       SIZE_66KB
#define SCL_DDR_TRAINING_DATA_BASE      (SCL_DDR_TRAINING_CODE_BASE + SCL_DDR_TRAINING_CODE_SIZE)
#define SCL_DDR_TRAINING_DATA_SIZE       SIZE_8KB
#define SCL_DDR_TRAINING_DATA_ZI_BASE   (SCL_DDR_TRAINING_DATA_BASE + SCL_DDR_TRAINING_DATA_SIZE)
#define SCL_DDR_TRAINING_DATA_ZI_SIZE    SIZE_6KB
#define SCL_DDR_TRAINING_SIZE            SIZE_80KB

#define SCL_SYSTEM_DEBUG_BASE (SCL_IMEM_BASE)
#define SCL_SYSTEM_DEBUG_SIZE SIZE_24KB
#define SCL_SYSTEM_DEBUG_CO_RO_BASE (SCL_SYSTEM_DEBUG_BASE)
#define SCL_SYSTEM_DEBUG_CO_RO_SIZE SIZE_12KB
#define SCL_SYSTEM_DEBUG_DATA_BASE (SCL_SYSTEM_DEBUG_BASE + SCL_SYSTEM_DEBUG_CO_RO_SIZE)
#define SCL_SYSTEM_DEBUG_QSEE_INFO_SIZE SIZE_1KB
#define SCL_SYSTEM_DEBUG_DATA_SIZE (SCL_SYSTEM_DEBUG_SIZE - SCL_SYSTEM_DEBUG_CO_RO_SIZE - SCL_SYSTEM_DEBUG_QSEE_INFO_SIZE )
#define SCL_SYSTEM_DEBUG_QSEE_INFO_BASE (SCL_SYSTEM_DEBUG_DATA_BASE + SCL_SYSTEM_DEBUG_DATA_SIZE)

#define SCL_TZ_OCIMEM_BUFFER SIZE_112KB
/* Ocimem Xbl Buffer start is after SDI (24 KB) and TZ buffers (112 KB). Using explicit offset
  so as to not affect its start due to sdi/tz buffer size reductions */
#define SCL_SBL1_OCIMEM_DATA_OFFSET SIZE_136KB
#define SCL_SBL1_OCIMEM_DATA_BASE (SCL_IMEM_BASE + SCL_SBL1_OCIMEM_DATA_OFFSET)
#define SCL_SBL1_OCIMEM_DATA_SIZE SIZE_48KB
#define SCL_SBL1_PAGE_TABLE_BASE (SCL_SBL1_OCIMEM_DATA_BASE + SCL_SBL1_OCIMEM_DATA_SIZE + SCL_DDR_TRAINING_DATA_BUF_SIZE)
#define SCL_SBL1_PAGE_TABLE_SIZE SIZE_20KB
#define SCL_SBL1_SHARED_FUNCTIONS_TABLE_BASE (SCL_SBL1_PAGE_TABLE_BASE + SCL_SBL1_PAGE_TABLE_SIZE)
#define SCL_SBL1_SHARED_FUNCTIONS_TABLE_SIZE  SIZE_1KB

#define SCL_DEVICEPROG_ZI_BASE (SCL_SYSTEM_DEBUG_BASE + SCL_SYSTEM_DEBUG_SIZE)
#define SCL_DEVICEPROG_ZI_SIZE SIZE_84KB

#define SCL_DDI_ZI_BASE (SCL_SYSTEM_DEBUG_BASE + SCL_SYSTEM_DEBUG_SIZE)
#define SCL_DDI_ZI_SIZE SIZE_84KB

#define MMU_POST_RPM_MEM_REGION_BASE (SCL_SYSTEM_DEBUG_BASE + SCL_SYSTEM_DEBUG_SIZE)
#define MMU_POST_RPM_MEM_REGION_SIZE (0x0100000 - SCL_RPM_CODE_RAM_SIZE - SCL_SYSTEM_DEBUG_SIZE)
#define SHARED_IMEM_SIZE SIZE_4KB
#define SHARED_IMEM_BASE (SCL_IMEM_BASE + SCL_IMEM_SIZE - SHARED_IMEM_SIZE)
#define SHARED_IMEM_BOOT_BASE SHARED_IMEM_BASE
#define SHARED_IMEM_BOOT_SIZE 200
#define SHARED_IMEM_USB_BASE (SHARED_IMEM_BOOT_BASE + SHARED_IMEM_BOOT_SIZE)
#define SHARED_IMEM_USB_SIZE 200
#define SHARED_IMEM_BOOT_CDT_BASE (SHARED_IMEM_USB_BASE + SHARED_IMEM_USB_SIZE)
#define SHARED_IMEM_BOOT_CDT_SIZE 0x0000400
#define SHARED_IMEM_DDR_BASE (SHARED_IMEM_BOOT_CDT_BASE + SHARED_IMEM_BOOT_CDT_SIZE)
#define SHARED_IMEM_DDR_SIZE 200
#define SHARED_IMEM_HLOS_BASE (SHARED_IMEM_DDR_BASE + SHARED_IMEM_DDR_SIZE)
#define SHARED_IMEM_HLOS_SIZE 200
#define SHARED_IMEM_RPM_BASE (SHARED_IMEM_HLOS_BASE + SHARED_IMEM_HLOS_SIZE)
#define SHARED_IMEM_RPM_SIZE 8
#define SHARED_IMEM_QDSS_BASE (SHARED_IMEM_RPM_BASE + SHARED_IMEM_RPM_SIZE)
#define SHARED_IMEM_QDSS_SIZE 12
#define SHARED_IMEM_SECUREMSM_BASE (SHARED_IMEM_QDSS_BASE + SHARED_IMEM_QDSS_SIZE)
#define SHARED_IMEM_SECUREMSM_SIZE 512
#define SHARED_IMEM_PERIPHERAL_DEBUG_BASE (SHARED_IMEM_SECUREMSM_BASE + SHARED_IMEM_SECUREMSM_SIZE)
#define SHARED_IMEM_PERIPHERAL_DEBUG_SIZE 24
#define SHARED_IMEM_PIL_BASE (SHARED_IMEM_PERIPHERAL_DEBUG_BASE + SHARED_IMEM_PERIPHERAL_DEBUG_SIZE)
#define SHARED_IMEM_PIL_SIZE 200
#define SHARED_IMEM_TZ_BASE (SHARED_IMEM_PIL_BASE + SHARED_IMEM_PIL_SIZE)
#define SHARED_IMEM_TZ_SIZE 128
#define SHARED_IMEM_OEM_BASE (SHARED_IMEM_TZ_BASE + SHARED_IMEM_TZ_SIZE)
#define SHARED_IMEM_OEM_SIZE 100
#define SHARED_IMEM_SDI_BASE (SHARED_IMEM_OEM_BASE + SHARED_IMEM_OEM_SIZE)
#define SHARED_IMEM_SDI_SIZE 32
#define SHARED_IMEM_MODEM_BASE (SHARED_IMEM_SDI_BASE + SHARED_IMEM_SDI_SIZE)
#define SHARED_IMEM_MODEM_SIZE 4
#define SHARED_IMEM_UNUSED_SPACE_BASE (SHARED_IMEM_MODEM_BASE + SHARED_IMEM_MODEM_SIZE)
#define SHARED_IMEM_UNUSED_SPACE_SIZE 1152
#define SHARED_IMEM_BOOTROM_BASE (SHARED_IMEM_UNUSED_SPACE_BASE + SHARED_IMEM_UNUSED_SPACE_SIZE)
#define SHARED_IMEM_BOOTROM_SIZE 100
#define DDR_MEM_BASE 0x80000000
#define DDR_MEM_SIZE SIZE_6GB
#define DDR_MEM_LOWER_BASE 0x40000000
#define DDR_MEM_LOWER_SIZE SIZE_1GB
#define SCL_DDR_pIMEM_BASE 0x86300000
#define SCL_DDR_pIMEM_SIZE SIZE_17MB
#define SCL_pIMEM_BASE 0x0B000000
#define SCL_pIMEM_SIZE SIZE_16MB
#define SCL_pIMEM_MINIMUM_SIZE SIZE_2MB

/* SBL's DDR section is shared by Loader DDR Data and Ramdump */
#define SCL_SBL1_DDR_BASE 0x85E00000
#define SCL_SBL1_DDR_SIZE SIZE_1MB

#define SCL_SBL1_DDR_DATA_BASE SCL_SBL1_DDR_BASE
#define SCL_SBL1_DDR_DATA_SIZE SIZE_512KB
#define SCL_SBL1_DDR_PAGE_TABLE_BASE SCL_SBL1_DDR_DATA_BASE
#define SCL_SBL1_DDR_PAGE_TABLE_SIZE SIZE_64KB
#define SCL_SBL1_DDR_UNCACHED_ZI_BASE (SCL_SBL1_DDR_PAGE_TABLE_BASE + SCL_SBL1_DDR_PAGE_TABLE_SIZE)
#define SCL_SBL1_DDR_UNCACHED_ZI_SIZE SIZE_4KB
#define SCL_SBL1_DDR_ZI_BASE (SCL_SBL1_DDR_UNCACHED_ZI_BASE + SCL_SBL1_DDR_UNCACHED_ZI_SIZE)
#define SCL_SBL1_DDR_ZI_SIZE (SCL_SBL1_DDR_DATA_SIZE - SCL_SBL1_DDR_PAGE_TABLE_SIZE - SCL_SBL1_DDR_UNCACHED_ZI_SIZE)

#define SCL_RAMDUMP_TOTAL_SIZE (SCL_SBL1_DDR_SIZE - SCL_SBL1_DDR_DATA_SIZE)
#define SCL_RAMDUMP_CODE_BASE (SCL_SBL1_DDR_BASE + SCL_SBL1_DDR_DATA_SIZE)
#define SCL_RAMDUMP_CODE_SIZE SIZE_192KB
#define SCL_RAMDUMP_DLOAD_BUFFERS_BASE (SCL_RAMDUMP_CODE_BASE + SCL_RAMDUMP_CODE_SIZE)
#define SCL_RAMDUMP_DLOAD_BUFFERS_SIZE SIZE_64KB
#define SCL_RAMDUMP_DATA_BASE (SCL_RAMDUMP_DLOAD_BUFFERS_BASE + SCL_RAMDUMP_DLOAD_BUFFERS_SIZE)
#define SCL_RAMDUMP_DATA_SIZE (SCL_RAMDUMP_TOTAL_SIZE - SCL_RAMDUMP_CODE_SIZE - SCL_RAMDUMP_DLOAD_BUFFERS_SIZE)
#define SCL_LOADERTEST_TOTAL_SIZE  (SCL_LOADERTEST_CODE_SIZE + SCL_LOADERTEST_DATA_SIZE)
#define SCL_LOADERTEST_CODE_BASE   SCL_XBL_CORE_CODE_BASE
#define SCL_LOADERTEST_CODE_SIZE   SIZE_2MB
#define SCL_LOADERTEST_DATA_BASE   (SCL_XBL_CORE_CODE_BASE + SCL_LOADERTEST_CODE_SIZE)
#define SCL_LOADERTEST_DATA_SIZE   SIZE_2MB
/* Only boot log size is needed as base addresses are automatically placed
   in DDR_ZI */
#define SCL_SBL1_DDR_BOOT_LOG_META_INFO_SIZE SCL_SBL1_BOOT_LOG_META_INFO_SIZE
#define SCL_SBL1_DDR_BOOT_LOG_BUF_SIZE SIZE_4KB

#define SCL_SHARED_RAM_BASE 0x86000000
#define SCL_SHARED_RAM_SIZE SIZE_2MB
#define SCL_QSEE_CODE_BASE 0x6686000
#define SCL_QSEE_TOTAL_SIZE SIZE_112KB
#define SCL_TZ_STAT_BASE  0x86200000
#define SCL_TZ_STAT_BASE_SIZE SIZE_1MB

#define SCL_QHEE_CODE_BASE 0x85800000
#define SCL_QHEE_TOTAL_SIZE SIZE_6MB
#define SCL_APPSBL_CODE_BASE 0x8EC00000
#define SCL_APPSBL_TOTAL_SIZE 0x0F00000
#define SCL_XBL_CORE_CODE_BASE 0x80200000
#define SCL_XBL_CORE_TOTAL_SIZE 0x0F00000
#define SCL_SBL1_BOOT_LOG_META_INFO_SIZE 64
#define SCL_SBL1_BOOT_LOG_BUF_SIZE 0x00000A00
#define PBL_APPS_ROM_BASE 0x00100000
#define PBL_APPS_ROM_SIZE SIZE_144KB
#define SBL1_DEBUG_COOKIE_ADDR (SHARED_IMEM_PERIPHERAL_DEBUG_BASE + 0x10)
#define SBL1_DEBUG_COOKIE_VAL 0x53444247
#define SCL_DDR_TRAINING_DATA_BUF_SIZE 4096
#define SCL_IRAM_BASE SCL_RPM_CODE_RAM_BASE
#define SCL_IRAM_SIZE SCL_RPM_CODE_RAM_SIZE
#define SCL_MODEM_EFS_RAM_SIZE 0x0300000

#define IMAGE_KEY_EMMCBLD_IMG_DEST_ADDR SCL_SBL1_CODE_BASE+80
#define IMAGE_KEY_DEVICEPROGRAMMER_IMG_DEST_ADDR SCL_SBL1_CODE_BASE+80

/* IPA Ramdump memory regions */
#define SCL_IPA_IRAM_BASE  0x006D0000
#define SCL_IPA_IRAM_SIZE  SIZE_16KB
#define SCL_IPA_DRAM_BASE  0x006D4000
#define SCL_IPA_DRAM_SIZE  0x00003F00
#define SCL_IPA_SRAM_BASE  0x006C5000
#define SCL_IPA_SRAM_SIZE  SIZE_8KB
#define SCL_IPA_HRAM_BASE  0x006C9000
#define SCL_IPA_HRAM_SIZE  0x00002F00
#define SCL_IPA_MBOX_BASE  0x006E2000
#define SCL_IPA_MBOX_SIZE  SIZE_256B


/*===========================================================================
                Defines to support PMIC image loading
===========================================================================*/


/*  PMIC config base (0x207000) and size */
#define SCL_PMIC_CONFIG_BUF_BASE  (SCL_RPM_CODE_RAM_BASE + SIZE_28KB)
#define SCL_PMIC_CONFIG_BUF_BASE_DDI  0x6800000
#define SCL_PMIC_CONFIG_BUF_SIZE  SIZE_64KB
#define SCL_VMEM_DDI_BASE_ADDR SCL_PMIC_CONFIG_BUF_BASE_DDI+SCL_PMIC_CONFIG_BUF_SIZE
#define SCL_VMEM_DDI_TCM_SIZE SIZE_160KB
#define SCL_VMEM_DDI_ZI_BASE_ADDR SCL_PMIC_CONFIG_BUF_BASE_DDI+SCL_PMIC_CONFIG_BUF_SIZE+SCL_VMEM_DDI_TCM_SIZE
#define SCL_VMEM_DDI_ZI_SIZE SIZE_128KB


/*  Below is temp buffer base for hash segment used by ELF loader in loading images
    before DDR init
    Location: 92KB from 0x207000 are reclaimed from bootrom for use by XblLoader. Pmic elf
    has max size of 64KB. Out of remaining space 10KB is used for hash buffer.
    If DDR training segment gets loaded it will occupy 80KB. hash table segment
    not needed for DDR training load but just a note that it will get overwritten.
*/
#define HASH_SEGMENT_TEMP_BASE (SCL_PMIC_CONFIG_BUF_BASE + SCL_PMIC_CONFIG_BUF_SIZE)
#define HASH_SEGMENT_SIZE SIZE_10KB

/*===========================================================================
                 Defines to support enhanced boot logging
============================================================================*/
#define BOOT_LOGGER_BOOT_CONFIG_FUSE_ADDRESS           HWIO_BOOT_CONFIG_ADDR
#define BOOT_LOGGER_JTAG_ID_FUSE_ADDRESS               HWIO_JTAG_ID_ADDR
#define BOOT_LOGGER_OEM_ID_FUSE_ADDRESS                HWIO_OEM_ID_ADDR
#define BOOT_LOGGER_SERIAL_NUM_FUSE_ADDRESS            HWIO_QFPROM_CORR_PTE_ROW1_LSB_ADDR
#define BOOT_LOGGER_OEM_CONFIG_ROW_0_FUSE_ADDRESS      HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR
#define BOOT_LOGGER_OEM_CONFIG_ROW_1_FUSE_ADDRESS      HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR
#define BOOT_LOGGER_FEATURE_CONFIG_ROW_0_FUSE_ADDRESS  HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR
#define BOOT_LOGGER_FEATURE_CONFIG_ROW_1_FUSE_ADDRESS  HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR


/*===========================================================================
                      FEATURE DEFINES USED ACROSS DRIVERS
===========================================================================*/
#define FEATURE_EFS_EFS2_ON_RMTS
#define DDR_XO_SPEED_IN_KHZ 19200
#define BOOT_TEMP_CHECK_THRESHOLD_DEGC
#undef  FEATURE_USE_PBL_SECBOOT_API
#undef  FEATURE_EMPTY_L1_PAGE_TABLE

/* Enablement flags for boot profiler feature below */
#define BOOT_PROFILER_FEATURE  /* Enable boot profiler overall feature can
                                  remove/comment out to completely disable */

/* Set current profiler level
   0 - Profiling messges only
   1 - High level debug messages and level 0
   2 - Low level debug messages and levels 1 and 0 */
#define BOOT_PROFILER_LEVEL 0

/* Enable boot profiler for boot code
   Uncomment the following to enable */
//#define SBL_PROFILER_FEATURE

/* Driver team must define macros which map to boot_profiler* macros.
   These driver-defined macros to be defined in "[driver]_profile.h".
   After driver has defined and placed these, uncomment the following
   lines to enable for any particular driver(commented out by default) */
//#define EFS_PROFILER_FEATURE

//#define DAL_PROFILER_FEATURE

//#define TLMM_PROFILER_FEATURE

//#define APT_PROFILER_FEATURE

//#define HOTPLUG_PROFILER_FEATURE

//#define SMEM_PROFILER_FEATURE

//#define PLATFORMINFO_PROFILER_FEATURE

//#define PMIC_PROFILER_FEATURE

//#define CLK_PROFILER_FEATURE

//#define VSENSE_PROFILER_FEATURE

//#define HSUSB_PROFILER_FEATURE

//#define BUSYWAIT_PROFILER_FEATURE

//#define BUS_PROFILER_FEATURE

//#define DDR_PROFILER_FEATURE

//#define FLASH_PROFILER_FEATURE

//#define CRYPTO_PROFILER_FEATURE

//#define SECBOOT_PROFILER_FEATURE

//#define UART_PROFILER_FEATURE

//#define POWER_PROFILER_FEATURE

//#define LIMITS_PROFILER_FEATURE

//#define SECIMG_AUTH_PROFILER_FEATURE


#endif  /* _TARGET_CUST_H */
