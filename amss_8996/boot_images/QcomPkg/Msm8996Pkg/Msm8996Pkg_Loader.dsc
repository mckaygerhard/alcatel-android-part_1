#==============================================================================
# @file Msm8996Pkg_Loader.dsc
# Msm8996 Loader package.
#
# Copyright (c) 2013-2015 Qualcomm Technologies, Inc. All rights reserved.
# Portions Copyright (c) 2009 - 2010, Apple Inc. All rights reserved.
# This program and the accompanying materials
# are licensed and made available under the terms and conditions of the BSD License
# which accompanies this distribution.  The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php
#
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#
#==============================================================================
#                              EDIT HISTORY
#
# when       who     what, where, why
# --------   ---     ----------------------------------------------------------
# 11/02/15   fz      Removed STI for release build 
# 09/03/15   bn      Added SdccLoaderTargetLib
# 08/06/16   bn      Enabled SDHCi, ADMA2, HS400
# 08/04/15   wg      Added PcdBootLogAddr & Size
# 08/04/15   ck      Enabled ImageVersionLib
# 08/01/15   kpa     Added LoaderCacheLib
# 08/01/15   sng     DDRDebugImage Framework Added
# 07/28/15   rp      Added DDRTrngLib
# 07/16/15   fz      Replace Null lib by actual for LoaderAptLib.
# 07/10/15   kpa     Added LoaderAptLib.
# 06/11/15   kpa     enable RNGLib.
# 06/05/15   unr     Add I2CLib to XBL Loader
# 06/04/15   dg      Add Null ReportStatusCodeLib
# 06/02/15   vk      Update download cookie address
# 06/02/15   vk      Remove limiting to 1.5 GB
# 05/18/15   bh      Remove LoaderUtilsLib
# 05/15/15   vk      Support seperate dsc for 8996 loader and core
# 05/07/15   dks     Added RFSLib
# 05/05/15   vk      Enable perf
# 04/24/15   kpa     Added XBLLoaderDevProgLib
# 05/01/15   aab     Added PmicLoaderNoChgrLib.inf for deviceprogrammer
# 04/30/15   vk      Move to v3 memory map
# 04/30/15   vk      Remove limiting memory available
# 04/29/15   rm      Use SDCC DDR50 speed mode for V2 bringup
# 04/23/15   unr     Add SPIApp and updated EEPROM core
# 04/21/15   rm      Added RpmbCgLib HmacLib HashLib, edited SDCCRpmbKey
# 04/19/15   bn      Added BaseDebugLibNull to RAM Dump
# 04/18/15   vk      Remove unsupported LLVM flag
# 04/18/15   vk      Update PcdPrePiCpuMemorySize for 64 bit address space support
# 04/17/15   rm      Added BootConfigLib
# 04/16/15   jz      Added DALTFDxe, DALTFTest and CoreTestsLib, removed 
#                    deprecated DALTFLib and CoreTests Apps
# 04/14/15   vk      Remove BaseTargetPkg dependency
# 04/09/15   vk      Added BaseDebugLibNull to RAM Dump
# 04/02/15   vk      DisableTzRngTest
# 03/30/15   eo      Added SpiNorCommonLib
# 03/30/15   ap      Added DALConfigToolsLib
# 03/27/15   unr     Added SPILibLoader
# 03/24/15   jb      Updates for tianocore 16623
# 03/19/15   rli     Add SmdLib to NpaDxe
# 03/17/15   vk      Enable security
# 03/12/15   jb      Add needed libraries for capsule processing changes
# 03/11/15   sm      Added TzRngLib and TzRngTest
# 03/11/15   jb      Set default entry to EL1
# 03/11/15   vk      Move to qualcomm RTCLib
# 03/10/15   bn      Disable SDCC's SDHCi, ADMA in JtagProgrammer
# 03/02/15   vk      Enable VarTest, reduce VariableDxe log
# 03/02/15   nc      Added APT test and DALTF applications, Added PmicCoreLib to 
#					 Target libraries
# 02/27/15   rli     Enable MpPark
# 02/27/15   rm      Added SDCCEmmcSpeedMode, enabled DMA in core
# 02/27/15   sk      Added measurebootapp
# 02/26/15   ck      Disabled SDCC SDHCi as LK currently fails
# 02/26/15   ck      Added global include of Target_cust.h file during compilation
# 02/25/15   bn      Enable SDCC SDHCi
# 02/25/15   vk      Add test apps to fv1
# 02/23/15   kpa     Added BootDebugLib
# 02/22/15   qtong   Enable display driver for the panel
# 02/20/15   vk      timer freq value, add NV erase app, massstorage cokkie addr
# 02/15/15   vk      Fix reset, add RPMB apps
# 02/17/15   bh      Enable Timer test
# 02/12/15   vk      Fix Dload cookie address
# 02/12/15   kpa     Added BootDdrTrainingLib
# 02/12/15   vk      Enable menu
# 02/10/15   kpa     Remove PRE_SIL Flag.
# 02/05/15   rli     Remove SmdDxe, make SmdLib and GLinkLib private
# 02/02/15   bm      Added GLinkDxe driver
# 02/04/15   ab      Add TimerSleepLib
# 01/28/15   rm      Added RPMB related libs.
# 01/26/15   tselvam Added PcieTest.inf
# 01/21/15   pbitra  Added chipinfo core and loader libs.
# 01/15/15   bh      Add MuslLib as NULL
# 01/01/15   bh      Add MuslLib
# 12/30/14   bm      Smem PCD values are now 64 bit.
# 12/15/14   wek     Add deviceprogrammer DDR and lite.
# 12/12/14   bh      Move to QcomPkg CapsuleRuntimeDxe
# 12/11/14   bh      Add ExtractGuidedSectionLib to UEFI_APPLICATION and DXE_RUNTIME_DRIVER section
# 12/06/14   al      Adding PmicApiLib
# 12/05/14   qtong   Add BaseSynchronization Library
# 12/03/14   bn      Added SdccCoreClockLib
# 11/25/14   sj      Enabling NPA/RPM/Utils and related test apps.
# 12/01/14   bm      Add a pcd for upprt 32 bit of smem base address
# 11/24/14   qtong   Remove eDP code due to no HW support
# 11/20/14   kpa     Temporarily switch to SecImgAuthLibNull.
# 11/12/14   al      Adding PmicCore and PmicDxe
# 11/19/14   pbitra  Replaced BusywaitLibNull with BusywaitLib.
# 11/14/14   vk      Fix Shared IMEM Cookie location
# 11/13/14   jb      Add ARCHPP_FLAGS
# 11/13/14   vk      Use BaseMemoryLib from ArmPkg for SEC
# 11/11/14   bh      Add new QcomBds, point GenericBds to Intel, Disable SetupBrowser
# 11/06/14   bm      Enable GLink and SMD compilation
# 11/06/14   bn      Add Jtag Programmer. Temporarily disabled SDCC's SDHCI mode
# 11/05/14   dcf     Added TlmmLib and removed TlmmLibNull.
# 11/04/14   vk      Add RamPartitionTableLib
# 11/04/14   unr     Added SPI XBL support
# 10/29/14   jb      Build ULogApi/NpaLib to enable ClockDxe
# 10/18/14   jb      Add PCD to start shell in nomap nostartup mode
# 10/14/14   jb      Switched to newer ArmLibs due to tianocore upgrade
# 10/21/14   tomwang  enabled ddr driver and icb driver
# 10/23/14   aab     Enable PMIC XBL loader Driver
# 10/13/14   kedara  Updates to generate Pmic Image.
# 10/13/14   kedara  DeviceProgrammerCommonLib to be stubbed for XBL image.
# 10/09/14   ck      Removed DeviceProgrammerCommonLibNull as the real one now exists
# 10/07/14   vk      Use Qualcomm DiskIo
# 10/06/14   ah      Added Pcie
# 10/07/14   dcf     Added TLMM for XBLCore.
# 10/06/14   kedara  Add QcomPkg/Msm8996Pkg/Include search path
#                    remove SmemlibNull for Loader
# 10/03/14   ck      Pointed all BaseTargetPkg null libs to proper target
# 10/03/14   al      Added PmicLib, PmicLoaderLib, PmicCoreLib
# 10/03/14   bmongia Enable SmemDxe
# 10/03/14   qtong   Enable DisplayDxe
# 10/01/14   vk      Rename _64 dsc
# 10/01/14   bn      Disabled SDCC's ADMA2
# 09/30/14   ck      Added EfsCookieBootLib, ServicesUtilsLib, HotPlugEfsLib
# 09/30/14   vk      Add SmemlibNull for Loader, enable for core
# 09/30/14   ck      Disabled SDCC SDHCi mode
# 09/30/14   ck      Modified location of ImageVersionLibNull
# 09/26/14   ck      Added TlmmLibNull
# 09/24/14   bn      Enabled SDCC's SDHCi mode
# 09/24/14   ck      Added BusyWaitLibNull
# 09/24/14   ck      Added AptLibNull
# 09/24/14   ck      Added PowerLibNull
# 09/24/14   ck      Added CryptoDriversLibNull
# 09/19/14   sho     Remove DALModEmptyDirLib which is now obsolete
# 09/03/14   Wayne   Added ACPIPlatformDxe
# 09/03/14   kedara  Added DeviceProgrammerCommonLib
# 08/27/14   cpaulo  Added InterruptController and Timer
# 08/26/14   unr     Added I2C
# 08/26/14   vm      Added ROMFS and HFAT libraries.
# 08/20/14   jjo     Added TSENS
# 08/18/14   bn      Added SdccDxe support
# 08/12/14   aa      Added DALConfig Loader Library
# 08/12/14   sho     Added DAL common library
# 07/29/14   pbitra  Added ChipInfo driver support.
# 07/25/14   ck      Added XBLRamDump to compilation
# 07/23/14   aa      Added DALModEmptyDirLib Libraries
# 07/22/14   aa      Added DAL Libraries
# 07/21/14   bmongia Add 8996 target info for SMEM Lib
# 07/18/14   kedara  Include HotPlugLib, EfsUtilsLib, Sdcc, LoaderUtils libs
# 07/17/14   vk      Update to memory map v1
# 06/09/14   kedara  Added Utils and Null libs
# 05/07/14   kedara  Added XBL common lib
# 04/25/14   kedara  Added DDR drivers
# 04/25/14   niting  Added XBL Core/Debug framework
# 04/23/14   niting  Initial Revision
#
#==============================================================================
################################################################################
#
# Defines Section - statements that will be processed to create a Makefile.
#
################################################################################
[Defines]
  PLATFORM_NAME                  = Msm8996Pkg
  PLATFORM_GUID                  = 4476742F-4C2D-BA9D-992A-CB82914F5E58
  PLATFORM_VERSION               = 0.1
  DSC_SPECIFICATION              = 0x00010005
  SUPPORTED_ARCHITECTURES        = AARCH64
  BUILD_TARGETS                  = DEBUG|RELEASE
  SKUID_IDENTIFIER               = DEFAULT

  OUTPUT_DIRECTORY               = Build/Msm8996_Loader

[LibraryClasses.common]
  DebugLib|QcomPkg/Library/DebugLib/DebugLib.inf
  UncachedMemoryAllocationLib|ArmPkg/Library/UncachedMemoryAllocationLib/UncachedMemoryAllocationLib.inf
  MemoryAllocationLib|MdePkg/Library/UefiMemoryAllocationLib/UefiMemoryAllocationLib.inf
  BaseLib|MdePkg/Library/BaseLib/BaseLib.inf
  BaseMemoryLib|ArmPkg/Library/BaseMemoryLibStm/BaseMemoryLibStm.inf
  RamPartitionTableLib|QcomPkg/Library/RamPartitionTableLib/RamPartitionTableLib.inf

  PciLib|MdePkg/Library/BasePciLibCf8/BasePciLibCf8.inf
  PerformanceLib|MdePkg/Library/BasePerformanceLibNull/BasePerformanceLibNull.inf
  PrintLib|MdePkg/Library/BasePrintLib/BasePrintLib.inf
  EfiFileLib|EmbeddedPkg/Library/EfiFileLib/EfiFileLib.inf
  PeCoffGetEntryPointLib|MdePkg/Library/BasePeCoffGetEntryPointLib/BasePeCoffGetEntryPointLib.inf

  #
  # Uncomment (and comment out the next line) For RealView Debugger. The Standard IO window
  # in the debugger will show load and unload commands for symbols. You can cut and paste this
  # into the command window to load symbols. We should be able to use a script to do this, but
  # the version of RVD I have does not support scipts accessing system memory.
  #
#  PeCoffExtraActionLib|ArmPkg/Library/RvdPeCoffExtraActionLib/RvdPeCoffExtraActionLib.inf
  PeCoffExtraActionLib|ArmPkg/Library/DebugPeCoffExtraActionLib/DebugPeCoffExtraActionLib.inf
#  PeCoffExtraActionLib|MdePkg/Library/BasePeCoffExtraActionLibNull/BasePeCoffExtraActionLibNull.inf

  CacheMaintenanceLib|ArmPkg/Library/ArmCacheMaintenanceLib/ArmCacheMaintenanceLib.inf
  DefaultExceptioHandlerLib|ArmPkg/Library/DefaultExceptionHandlerLib/DefaultExceptionHandlerLib.inf
  PrePiLib|EmbeddedPkg/Library/PrePiLib/PrePiLib.inf
  PrePiMemoryAllocationLib|EmbeddedPkg/Library/PrePiMemoryAllocationLib/PrePiMemoryAllocationLib.inf
  SemihostLib|ArmPkg/Library/SemihostLib/SemihostLib.inf
  IoLib|MdePkg/Library/BaseIoLibIntrinsic/BaseIoLibIntrinsic.inf

  MemoryAllocationLib|MdePkg/Library/UefiMemoryAllocationLib/UefiMemoryAllocationLib.inf
  UefiLib|MdePkg/Library/UefiLib/UefiLib.inf
  HobLib|MdePkg/Library/DxeHobLib/DxeHobLib.inf
  UefiRuntimeServicesTableLib|MdePkg/Library/UefiRuntimeServicesTableLib/UefiRuntimeServicesTableLib.inf
  DevicePathLib|MdePkg/Library/UefiDevicePathLib/UefiDevicePathLib.inf
  UefiBootServicesTableLib|MdePkg/Library/UefiBootServicesTableLib/UefiBootServicesTableLib.inf
  BaseSynchronizationLib|MdePkg/Library/BaseSynchronizationLib/BaseSynchronizationLib.inf

  DxeServicesTableLib|MdePkg/Library/DxeServicesTableLib/DxeServicesTableLib.inf
  UefiDriverEntryPoint|MdePkg/Library/UefiDriverEntryPoint/UefiDriverEntryPoint.inf
  UefiApplicationEntryPoint|MdePkg/Library/UefiApplicationEntryPoint/UefiApplicationEntryPoint.inf


#
# Assume everything is fixed at build
#
  PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
  UefiRuntimeLib|MdePkg/Library/UefiRuntimeLib/UefiRuntimeLib.inf
  UefiUsbLib|MdePkg/Library/UefiUsbLib/UefiUsbLib.inf
  EblAddExternalCommandLib|EmbeddedPkg/Library/EblAddExternalCommandLib/EblAddExternalCommandLib.inf
  CpuLib|MdePkg/Library/BaseCpuLib/BaseCpuLib.inf
  EblNetworkLib|EmbeddedPkg/Library/EblNetworkLib/EblNetworkLib.inf
  ArmDisassemblerLib|ArmPkg/Library/ArmDisassemblerLib/ArmDisassemblerLib.inf
  DebugAgentLib|MdeModulePkg/Library/DebugAgentLibNull/DebugAgentLibNull.inf
  DmaLib|ArmPkg/Library/ArmDmaLib/ArmDmaLib.inf
  NetLib|MdeModulePkg/Library/DxeNetLib/DxeNetLib.inf
  DpcLib|MdeModulePkg/Library/DxeDpcLib/DxeDpcLib.inf
  UdpIoLib|MdeModulePkg/Library/DxeUdpIoLib/DxeUdpIoLib.inf
  IpIoLib|MdeModulePkg/Library/DxeIpIoLib/DxeIpIoLib.inf

  # Qcom Generic Libraries
  QcomLib|QcomPkg/Library/QcomLib/QcomLib.inf
  QcomUtilsLib|QcomPkg/Library/QcomUtilsLib/QcomUtilsLib.inf
  TimetickRunTimeLib|QcomPkg/Library/TimetickLibB/TimetickRunTimeLib.inf
  TimetickLib|QcomPkg/Library/TimetickLibB/TimetickLib.inf
  EblCmdLib|QcomPkg/Library/EblCmdLib/EblCmdLib.inf
  TimerLib|ArmPkg/Library/ArmArchTimerLib/ArmArchTimerLib.inf
  TimerSleepLib|QcomPkg/Library/TimerSleepLib/TimerSleepLib.inf
  ArmGenericTimerCounterLib|ArmPkg/Library/ArmGenericTimerPhyCounterLib/ArmGenericTimerPhyCounterLib.inf
  SmemLib|QcomPkg/Library/SmemLib/SmemLib.inf
  RealTimeClockLib|QcomPkg/Library/RealTimeClockLibLegacy/RealTimeClockLib.inf
  SerialPortLib|QcomPkg/Library/SerialPortLib/SerialPortShLib.inf
  SecBootSigLib|QcomPkg/Library/SecBootSigLib/SecBootSigLib.inf
  QcomBaseLib|QcomPkg/Library/QcomBaseLib/QcomBaseLib.inf
  NULL|QcomPkg/Library/QcomBaseLib/QcomBaseLib.inf
  ParserLib|QcomPkg/Library/ParserLib/ParserLib.inf
  FwCommonLib|QcomPkg/Msm8996Pkg/Library/FwCommonLibNull/FwCommonLibNull.inf
  FwUpdateLib|QcomPkg/Msm8996Pkg/Library/FwCommonLibNull/FwCommonLibNull.inf
  FwProvisionLib|QcomPkg/Msm8996Pkg/Library/FwCommonLibNull/FwCommonLibNull.inf
  ProcLib|QcomPkg/Library/ProcLib/ProcLib.inf
  FBPTLib|QcomPkg/Library/FBPTLib/FBPTLib.inf
  UefiCfgLib|QcomPkg/Library/UefiConfigLib/UefiCfgLib.inf
  SmBiosLib|QcomPkg/Library/SmBiosLib/SmBiosLib.inf
  EfiResetSystemLib|QcomPkg/Library/ResetSystemLib/ResetSystemLib.inf
  ScmCmdLib|QcomPkg/Library/ScmCmdLib/ScmCmdLib.inf
  XBLSharedMemLib|QcomPkg/Library/XBLSharedMemLib/XBLSharedMemLib.inf
  TzRuntimeLib|QcomPkg/Library/TzRuntimeLib/TzRuntimeLib.inf
  TzRngLib|QcomPkg/Library/TzRngLib/TzRngLib.inf
  SdccCommonLib|QcomPkg/Library/SdccCommonLib/SdccCommonLib.inf
  SdccHalLib|QcomPkg/Library/SdccCommonLib/SdccHalLib/SdccHalLib.inf
  UfsCommonLib|QcomPkg/Library/UfsCommonLib/UfsCommonLib.inf
  UfsLoaderLib|QcomPkg/Library/UfsCommonLib/UfsLoaderLib.inf
  UfsJtagLib|QcomPkg/Library/UfsCommonLib/UfsJtagLib.inf
  QusbLib|QcomPkg/Library/QusbLib/QusbLib.inf
  SecCfgLib|QcomPkg/Library/SecCfgLib/SecCfgLib.inf
  HotPlugLib|QcomPkg/Library/HotplugLib/HotPlugLib.inf
  HotPlugEfsLib|QcomPkg/Library/HotplugLib/HotPlugEfsLib.inf
  HFATLib|QcomPkg/Library/HFATLib/HFATLib.inf
  ROMFSLib|QcomPkg/Library/EfsLib/ROMFSLib.inf
  EfsUtilsLib|QcomPkg/Library/EfsLib/EfsUtilsLib.inf
  EfsCookieBootLib|QcomPkg/Library/EfsLib/EfsCookieBootLib.inf
  ServicesUtilsLib|QcomPkg/Library/ServicesLib/ServicesUtilsLib.inf
  PlatformInfoCoreLib|QcomPkg/Library/PlatformInfoLib/PlatformInfoCore.inf
  PlatformInfoLoaderLib|QcomPkg/Library/PlatformInfoLib/PlatformInfoLoader.inf
  ChipInfoCoreLib|QcomPkg/Library/ChipInfoLib/ChipInfoCore.inf
  ChipInfoLoaderLib|QcomPkg/Library/ChipInfoLib/ChipInfoLoader.inf
  RpmbLib|QcomPkg/Library/RpmbLib/RpmbLib.inf
  RpmbListenerLib|QcomPkg/Library/RpmbLib/RpmbListenerLib.inf    
  HashLib|QcomPkg/Msm8996Pkg/Library/HashLib/HashLibHw.inf
  HmacLib|QcomPkg/Library/HmacLib/HmacLib.inf  
  RpmbCgLib|QcomPkg/Library/RpmbCgLib/RpmbCgLib.inf
  ImageVersionLib|QcomPkg/Library/ImageVersionLib/ImageVersionLib.inf
  SecBootLib|QcomPkg/Library/SecBootLib/SecBootLib.inf
  ToolSupportLib|QcomPkg/Library/ToolSupportLib/ToolSupportLib.inf
  FuseControlLib|QcomPkg/Library/FuseControlLib/FuseControlLib.inf
  SecBootBaseLib|QcomPkg/Library/SecBootBaseLib/SecBootBaseLib.inf
  PeHashLib|QcomPkg/Library/PeHashLib/PeHashLib.inf

  # Target Libraries
  TargetResetLib|QcomPkg/Msm8996Pkg/Library/TargetResetLibNull/TargetResetLib.inf
  XBLCommonLib|QcomPkg/Msm8996Pkg/Library/XBLLoaderLib/XBLCommonLib.inf
  DDILIB|QcomPkg/Msm8996Pkg/Library/DDILib/DDILib.inf
  DDRTargetLib|QcomPkg/Msm8996Pkg/Library/DDRTargetLib/DDRTargetLib.inf
  DSFTargetLib|QcomPkg/Msm8996Pkg/Library/DSFTargetLib/DSFTargetLib.inf
  PIMEMTargetLib|QcomPkg/Msm8996Pkg/Library/PIMEMTargetLib/PIMEMTargetLib.inf
  ClockTargetLib|QcomPkg/Msm8996Pkg/Library/ClockTargetLib/ClockTargetLib.inf
  TLMMTargetLib|QcomPkg/Msm8996Pkg/Library/TLMMTargetLib/TLMMTargetLib.inf
  HotPlugLib|QcomPkg/Library/HotplugLib/HotPlugLib.inf
  HFATLib|QcomPkg/Library/HFATLib/HFATLib.inf
  PmicLib|QcomPkg/Msm8996Pkg/Library/PmicLib/PmicLib.inf
  PmicLoaderLib|QcomPkg/Msm8996Pkg/Library/PmicLib/PmicLoaderLib.inf
  PmicCoreLib|QcomPkg/Msm8996Pkg/Library/PmicLib/PmicCoreLib.inf
  PmicApiLib|QcomPkg/Library/PmicApiLib/PmicApiLib.inf
  PmicConfigLib|QcomPkg/Msm8996Pkg/Library/PmicLib/PmicConfigLib.inf
  HotPlugLib|QcomPkg/Library/HotplugLib/HotPlugLib.inf
  HFATLib|QcomPkg/Library/HFATLib/HFATLib.inf
  SdccClockLib|QcomPkg/Msm8996Pkg/Library/SdccClockLib/SdccCoreClockLib.inf
  UfsTargetBootLib|QcomPkg/Msm8996Pkg/Library/UfsTargetLib/UfsTargetBootLib.inf
  UfsTargetJtagLib|QcomPkg/Msm8996Pkg/Library/UfsTargetLib/UfsTargetJtagLib.inf
  UfsTargetUefiLib|QcomPkg/Msm8996Pkg/Library/UfsTargetLib/UfsTargetUefiLib.inf
  QusbTargetLib|QcomPkg/Msm8996Pkg/Library/QusbTargetLib/QusbTargetLib.inf
  QusbPblDloadCheckLib|QcomPkg/Msm8996Pkg/Library/QusbPblDloadCheckLib/QusbPblDloadCheckLib.inf
  UIELib|QcomPkg/Msm8996Pkg/Library/UIELib/UIELib.inf
  SecBootAuthLib|QcomPkg/Msm8996Pkg/Library/SecBootAuthLib/SecBootAuthLib.inf
  SecImgAuthTargetLib|QcomPkg/Msm8996Pkg/Library/SecImgAuthTargetLib/SecImgAuthTargetLib.inf
  PrngLib|QcomPkg/Msm8996Pkg/Library/RNGLib/RNGLibSw.inf
  HashLib|QcomPkg/Msm8996Pkg/Library/HashLib/HashLibSw.inf
  RNGLib|QcomPkg/Msm8996Pkg/Library/RNGLib/RNGLibSw.inf
  IcbCfgLib|QcomPkg/Library/ICBLib/ICBLib.inf
  IcbTargetLib|QcomPkg/Msm8996Pkg/Library/ICBTargetLib/ICBTargetLib.inf
  AptLib|QcomPkg/Msm8996Pkg/Library/AptLibNull/AptLibNull.inf
  UsbWrapperLayerLib|QcomPkg/Msm8996Pkg/Library/UsbWrapperLayerLib/UsbWrapperLayerLib.inf
  BusyWaitLib|QcomPkg/Library/BusyWaitLib/BusyWaitLib.inf
  TlmmLib|QcomPkg/Library/TlmmLib/TlmmLib.inf
  HALclkLib|QcomPkg/Msm8996Pkg/Library/HALclkLib/HALclkLibXBL.inf
  HALclkSharedLib|QcomPkg/Library/HALclkSharedLib/HALclkSharedLibUEFI.inf
  PmicShutdownLibBoot|QcomPkg/Library/PmicShutdownLib/PmicShutdownBoottimeLib.inf
  PmicShutdownRuntimeLib|QcomPkg/Library/PmicShutdownLib/PmicShutdownRuntimeLib.inf
  HALqgicLib|QcomPkg/Library/HalqgicLib/HALqgicLib.inf
  SpiNorCommonLib|QcomPkg/Library/SpiNorCommonLib/SpiNorCommonLib.inf
  BootConfigLib|QcomPkg/Msm8996Pkg/Library/BootConfigLib/BootConfigLib.inf
  LoaderCacheLib|QcomPkg/XBLLoader/LoaderCacheLib.inf

  # DAL Libraries
  DALModEnvLib|QcomPkg/Library/DALModEnvLib/DALModEnvLib.inf
  DALCommonDxeLib|QcomPkg/Library/DALCommonLib/DALCommonDxeLib.inf
  DALCommonLoaderLib|QcomPkg/Library/DALCommonLib/DALCommonLoaderLib.inf
  DALFwkLib|QcomPkg/Library/DALFwkLib/DALFwkLib.inf

  DALGenLibCore|QcomPkg/Msm8996Pkg/Library/DALConfigLib/DALGenLibCore.inf
  DALGenLibLoader|QcomPkg/Msm8996Pkg/Library/DALConfigLib/DALGenLibLoader.inf
  DALGenLibTools|QcomPkg/Msm8996Pkg/Library/DALConfigLib/DALGenLibTools.inf

  DALConfigLib|QcomPkg/Msm8996Pkg/Library/DALConfigLib/DALConfigCoreLib.inf
  DALConfigLoaderLib|QcomPkg/Msm8996Pkg/Library/DALConfigLib/DALConfigLoaderLib.inf
  DALConfigToolsLib | QcomPkg/Msm8996Pkg/Library/DALConfigLib/DALConfigToolsLib.inf

  # Test Interface Libraries
  TestInterfaceLib|QcomPkg/Library/TestInterfaceLib/TestInterfaceLib.inf

  # HAL Libraries
  # Display HAL Libraries
  ArmGicLib|ArmPkg/Drivers/ArmGic/ArmGicSecLib.inf
  ArmPlatformLib|ArmPlatformPkg/Library/ArmPlatformLibNull/ArmPlatformLibNull.inf
  HALMDPLib|QcomPkg/Library/HALMDPLib/HALMDPLib.inf
  HALDSILib|QcomPkg/Library/HALDSILib/HALDSILib.inf
  HALHDMILib|QcomPkg/Library/HALHDMILib/HALHDMILib.inf


  # MDP Libraries
  MDPLib|QcomPkg/Library/MDPLib/MDPLib.inf
  MDPPlatformLib|QcomPkg/Msm8996Pkg/Library/MDPPlatformLib/MDPPlatformLib.inf

  # BAM Lib Library
  BamLib|QcomPkg/Library/BamLib/BamLoaderLib.inf
  BamLoaderLib|QcomPkg/Library/BamLib/BamLoaderLib.inf
  BamTargetLib|QcomPkg/Msm8996Pkg/Library/BamTargetLib/BamTargetLib.inf

  # RFS Lib Library
  RFSLib|QcomPkg/Library/RFSLib/RFSLib.inf

  # ULog Libraries
  ULogApiLib|QcomPkg/Library/ULogApiLib/ULogApiLib.inf
  CoreUtilsLib|QcomPkg/Library/CoreUtilsLib/CoreUtilsLib.inf

  # Clock Libraries - These are internal chipset specific clock libraries.
  #                   They should only be used by the clock DXE driver and test application.
  ClockLib|QcomPkg/Msm8996Pkg/Library/ClockLib/ClockLibXBL.inf

  # Railway Libraries - These are internal chipset specific clock libraries.
  #                   They should only be used by the Railway DXE driver.
  RailwayLib|QcomPkg/Library/RailwayLib/RailwayLib.inf
  RailwayTargetLib|QcomPkg/Msm8996Pkg/Library/RailwayTargetLib/RailwayTargetLib.inf

  # Railway Libraries - These are internal chipset specific clock libraries.
  #                   They should only be used by the Railway DXE driver.
  CPRTargetLib|QcomPkg/Msm8996Pkg/Library/CPRTargetLib/CPRTargetLib.inf

  
  SecurityManagementLib|MdeModulePkg/Library/DxeSecurityManagementLib/DxeSecurityManagementLib.inf
  DxeServicesLib|MdePkg/Library/DxeServicesLib/DxeServicesLib.inf

  # MpPark Library
  MpParkLib|QcomPkg/Msm8996Pkg/Library/MpParkLib/MpParkLib.inf

  # ChargerHW Library

  # BatteryGauge Library

  # SPMI Library
  SPMILib|QcomPkg/Msm8996Pkg/Library/SPMILib/SPMILib.inf
  SPMIApiLib|QcomPkg/Library/SPMIApiLib/SPMIApiLib.inf
  SPMIRuntimeLib|QcomPkg/Library/SPMIRuntimeLib/SPMIRuntime.inf
  
  #Fastboot  
  BdsLib|ArmPkg/Library/BdsLib/BdsLib.inf
  FdtLib|EmbeddedPkg/Library/FdtLib/FdtLib.inf  
  ArmSmcLib|ArmPkg/Library/ArmSmcLib/ArmSmcLib.inf
  
  # I2C Library
  I2CLib|QcomPkg/Library/I2CLib/I2CLib.inf
  I2CApiLib|QcomPkg/Library/I2CApiLib/I2CApiLib.inf
  SPILib|QcomPkg/Library/SPILib/SPILib.inf
  SPIApiLib|QcomPkg/Library/SPIApiLib/SPIApiLib.inf

  # PCIe Library
  PcieConfigLib|QcomPkg/Msm8996Pkg/Library/PcieConfigLib/PcieConfigLib.inf

  # Shared library instace for clients usage
  ShLib|QcomPkg/Library/ShLib/ShLib.inf

  # NPA API Library
  NpaApiLib|QcomPkg/Library/NpaApiLib/NpaApiLib.inf

  # ADC Libraries
  AdcLoaderLib|QcomPkg/Library/AdcLib/build/AdcLoaderLib.inf
  AdcDxeWrapperLib|QcomPkg/Library/AdcLib/build/AdcDxeWrapperLib.inf

  # TSENS Libraries
  TsensLoaderLib|QcomPkg/Library/TsensLib/TsensLoaderLib.inf
  TsensTargetLib|QcomPkg/Msm8996Pkg/Library/TsensTargetLib/TsensTargetLib.inf
  BootTempCheckLib|QcomPkg/Library/BootTempCheckLib/BootTempCheckLib.inf
  # MuslLib: POSIX string APIs
  MuslLib|QcomPkg/Library/MuslLib/MuslLib.inf
  NULL|QcomPkg/Library/MuslLib/MuslLib.inf

  #SortLib
  SortLib|MdeModulePkg/Library/UefiSortLib/UefiSortLib.inf

[LibraryClasses.Arm]
  ArmLib|ArmPkg/Library/ArmLib/ArmV7/ArmV7Lib.inf

[LibraryClasses.AARCH64]
  ArmLib|ArmPkg/Library/ArmLib/AArch64/AArch64Lib.inf

[LibraryClasses.common.SEC]
  ProcLib|QcomPkg/Library/ProcLib/ProcLib.inf
  FBPTLib|QcomPkg/Library/FBPTLib/FBPTLib.inf
  PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
  ReportStatusCodeLib|QcomPkg/Library/PeiReportStatusCodeLibFrameworkNull/PeiReportStatusCodeLibNull.inf
  UefiDecompressLib|MdePkg/Library/BaseUefiDecompressLib/BaseUefiDecompressLib.inf
  ExtractGuidedSectionLib|EmbeddedPkg/Library/PrePiExtractGuidedSectionLib/PrePiExtractGuidedSectionLib.inf
  LzmaDecompressLib|IntelFrameworkModulePkg/Library/LzmaCustomDecompressLib/LzmaCustomDecompressLib.inf
  PeCoffLib|MdePkg/Library/BasePeCoffLib/BasePeCoffLib.inf
  HobLib|EmbeddedPkg/Library/PrePiHobLib/PrePiHobLib.inf
  PrePiHobListPointerLib|EmbeddedPkg/Library/PrePiHobListPointerLib/PrePiHobListPointerLib.inf
  PerformanceLib|MdeModulePkg/Library/PeiPerformanceLib/PeiPerformanceLib.inf
  ShLib|QcomPkg/Library/ShLib/ShLibMgr.inf
  SerialPortLib|QcomPkg/Library/SerialPortLib/SerialPortLib.inf
  UefiCfgLib|QcomPkg/Library/UefiConfigLib/UefiCfgLib.inf
  OfflineCrashDumpLib|QcomPkg/Library/OfflineCrashDumpLib/OfflineCrashDumpLib.inf

  # Target SEC Libraries
  TargetInitLib|QcomPkg/Msm8996Pkg/Library/TargetInitLib/TargetInitLib.inf
  SdccClockLib|QcomPkg/Msm8996Pkg/Library/SdccClockLib/SdccLoaderClockLib.inf
  SdccTargetLib|QcomPkg/Msm8996Pkg/Library/SdccTargetLib/SdccLoaderTargetLib.inf

  DebugLib|QcomPkg/Library/DebugLib/DebugLib.inf

[LibraryClasses.ARM.SEC]
  ArmLib|ArmPkg/Library/ArmLib/ArmV7/ArmV7LibPrePi.inf

[LibraryClasses.AARCH64.SEC]
  ArmLib|ArmPkg/Library/ArmLib/AArch64/AArch64LibPrePi.inf

[LibraryClasses.common.PEI_CORE]
  PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
  ReportStatusCodeLib|QcomPkg/Library/PeiReportStatusCodeLibFrameworkNull/PeiReportStatusCodeLibNull.inf

[LibraryClasses.common.DXE_CORE]
  HobLib|MdePkg/Library/DxeCoreHobLib/DxeCoreHobLib.inf
  MemoryAllocationLib|MdeModulePkg/Library/DxeCoreMemoryAllocationLib/DxeCoreMemoryAllocationLib.inf
  DxeCoreEntryPoint|MdePkg/Library/DxeCoreEntryPoint/DxeCoreEntryPoint.inf
  ReportStatusCodeLib|IntelFrameworkModulePkg/Library/DxeReportStatusCodeLibFramework/DxeReportStatusCodeLib.inf
  ExtractGuidedSectionLib|MdePkg/Library/DxeExtractGuidedSectionLib/DxeExtractGuidedSectionLib.inf
  UefiDecompressLib|MdePkg/Library/BaseUefiDecompressLib/BaseUefiDecompressLib.inf
  #PeCoffLib|MdePkg/Library/BasePeCoffLib/BasePeCoffLib.inf
  PeCoffLib|EmbeddedPkg/Library/DxeHobPeCoffLib/DxeHobPeCoffLib.inf
  CpuExceptionHandlerLib|MdeModulePkg/Library/CpuExceptionHandlerLibNull/CpuExceptionHandlerLibNull.inf

  PerformanceLib|MdeModulePkg/Library/DxeCorePerformanceLib/DxeCorePerformanceLib.inf

  DebugLib|QcomPkg/Library/DebugLib/DebugLib.inf

[LibraryClasses.common.DXE_DRIVER]
  ReportStatusCodeLib|IntelFrameworkModulePkg/Library/DxeReportStatusCodeLibFramework/DxeReportStatusCodeLib.inf
  DxeServicesLib|MdePkg/Library/DxeServicesLib/DxeServicesLib.inf
  PerformanceLib|MdeModulePkg/Library/DxePerformanceLib/DxePerformanceLib.inf
  HiiLib|MdeModulePkg/Library/UefiHiiLib/UefiHiiLib.inf
  UefiHiiServicesLib|MdeModulePkg/Library/UefiHiiServicesLib/UefiHiiServicesLib.inf
  GenericBdsLib|IntelFrameworkModulePkg/Library/GenericBdsLib/GenericBdsLib.inf
  PlatformBdsLib|QcomPkg/Library/PlatformBdsLib/PlatformBdsLib.inf
  QcomBdsLib|QcomPkg/Library/QcomBdsLib/QcomBdsLib.inf
  CapsuleLib|MdeModulePkg/Library/DxeCapsuleLibNull/DxeCapsuleLibNull.inf
  ExtractGuidedSectionLib|MdePkg/Library/DxeExtractGuidedSectionLib/DxeExtractGuidedSectionLib.inf
  LzmaDecompressLib|IntelFrameworkModulePkg/Library/LzmaCustomDecompressLib/LzmaCustomDecompressLib.inf

  # Target Libraries
  OfflineCrashDumpLib|QcomPkg/Library/OfflineCrashDumpLib/OfflineCrashDumpDxeLib.inf

  #Arm Architectural Libraries
  DefaultExceptionHandlerLib|ArmPkg/Library/DefaultExceptionHandlerLib/DefaultExceptionHandlerLib.inf
  ArmDisassemblerLib|ArmPkg/Library/ArmDisassemblerLib/ArmDisassemblerLib.inf
  DmaLib|ArmPkg/Library/ArmDmaLib/ArmDmaLib.inf

[LibraryClasses.common.UEFI_APPLICATION]
  ReportStatusCodeLib|IntelFrameworkModulePkg/Library/DxeReportStatusCodeLibFramework/DxeReportStatusCodeLib.inf
  UefiDecompressLib|IntelFrameworkModulePkg/Library/BaseUefiTianoCustomDecompressLib/BaseUefiTianoCustomDecompressLib.inf
  PerformanceLib|MdeModulePkg/Library/DxePerformanceLib/DxePerformanceLib.inf
  UefiHiiServicesLib|MdeModulePkg/Library/UefiHiiServicesLib/UefiHiiServicesLib.inf
  ExtractGuidedSectionLib|MdePkg/Library/DxeExtractGuidedSectionLib/DxeExtractGuidedSectionLib.inf
  HiiLib|MdeModulePkg/Library/UefiHiiLib/UefiHiiLib.inf
  OfflineCrashDumpLib|QcomPkg/Library/OfflineCrashDumpLib/OfflineCrashDumpDxeLib.inf

[LibraryClasses.common.UEFI_DRIVER]
  ReportStatusCodeLib|IntelFrameworkModulePkg/Library/DxeReportStatusCodeLibFramework/DxeReportStatusCodeLib.inf
  UefiDecompressLib|IntelFrameworkModulePkg/Library/BaseUefiTianoCustomDecompressLib/BaseUefiTianoCustomDecompressLib.inf
  ExtractGuidedSectionLib|MdePkg/Library/DxeExtractGuidedSectionLib/DxeExtractGuidedSectionLib.inf
  PerformanceLib|MdeModulePkg/Library/DxePerformanceLib/DxePerformanceLib.inf
  HiiLib|MdeModulePkg/Library/UefiHiiLib/UefiHiiLib.inf
  UefiHiiServicesLib|MdeModulePkg/Library/UefiHiiServicesLib/UefiHiiServicesLib.inf

[LibraryClasses.common.DXE_RUNTIME_DRIVER]
  HobLib|MdePkg/Library/DxeHobLib/DxeHobLib.inf
  MemoryAllocationLib|MdePkg/Library/UefiMemoryAllocationLib/UefiMemoryAllocationLib.inf
  ExtractGuidedSectionLib|MdePkg/Library/DxeExtractGuidedSectionLib/DxeExtractGuidedSectionLib.inf
  ReportStatusCodeLib|IntelFrameworkModulePkg/Library/DxeReportStatusCodeLibFramework/DxeReportStatusCodeLib.inf
  CapsuleLib|MdeModulePkg/Library/DxeCapsuleLibNull/DxeCapsuleLibNull.inf
#  PeCoffLib|MdePkg/Library/BasePeCoffLib/BasePeCoffLib.inf
  PeCoffLib|EmbeddedPkg/Library/DxeHobPeCoffLib/DxeHobPeCoffLib.inf
  OfflineCrashDumpLib|QcomPkg/Library/OfflineCrashDumpLib/OfflineCrashDumpDxeLib.inf
  DebugLib|QcomPkg/Library/DebugLib/DebugLibRuntime.inf
  GenericBdsLib|IntelFrameworkModulePkg/Library/GenericBdsLib/GenericBdsLib.inf
  HiiLib|MdeModulePkg/Library/UefiHiiLib/UefiHiiLib.inf
  UefiHiiServicesLib|MdeModulePkg/Library/UefiHiiServicesLib/UefiHiiServicesLib.inf
  DxeServicesLib|MdePkg/Library/DxeServicesLib/DxeServicesLib.inf


[LibraryClasses.ARM]
  #
  # It is not possible to prevent the ARM compiler for generic intrinsic functions.
  # This library provides the instrinsic functions generate by a given compiler.
  # [LibraryClasses.ARM] and NULL mean link this library into all ARM images.
  #
  NULL|ArmPkg/Library/CompilerIntrinsicsLib/CompilerIntrinsicsLib.inf

[LibraryClasses.AARCH64]
  NULL|ArmPkg/Library/CompilerIntrinsicsLib/CompilerIntrinsicsLib.inf


[BuildOptions.AARCH64]
  RVCT:*_*_ARM_ARCHASM_FLAGS == --cpu 8-A.64 -I$(WORKSPACE)/QcomPkg/Msm8996Pkg/Include -include $(WORKSPACE)/QcomPkg/Msm8996Pkg/Include/Target_cust.h
  GCC:*_*_AARCH64_ARCHCC_FLAGS  == -mtune=cortex-a53 -I $(WORKSPACE)/QcomPkg/Msm8996Pkg/Include -include $(WORKSPACE)/QcomPkg/Msm8996Pkg/Include/Target_cust.h -DMDEPKG_NDEBUG
  GCC:*_*_AARCH64_ARCHPP_FLAGS  == -I $(WORKSPACE)/QcomPkg/Msm8996Pkg/Include -include $(WORKSPACE)/QcomPkg/Msm8996Pkg/Include/Target_cust.h -DMDEPKG_NDEBUG

!if $(PRODMODE) == "PRODMODE"
  RVCT:*_*_*_CC_FLAGS = -DPRODMODE
  GCC:*_*_*_CC_FLAGS = -DPRODMODE
!endif

#  RVCT:RELEASE_*_*_CC_FLAGS  = -DMDEPKG_NDEBUG

################################################################################
#
# Pcd Section - list of all EDK II PCD Entries defined by this Platform
#
################################################################################

[PcdsFeatureFlag.common]
  gEfiMdePkgTokenSpaceGuid.PcdComponentNameDisable|TRUE
  gEfiMdePkgTokenSpaceGuid.PcdDriverDiagnosticsDisable|TRUE
  gEfiMdePkgTokenSpaceGuid.PcdComponentName2Disable|TRUE
  gEfiMdePkgTokenSpaceGuid.PcdDriverDiagnostics2Disable|TRUE

  #
  # Control what commands are supported from the UI
  # Turn these on and off to add features or save size
  #
  gEmbeddedTokenSpaceGuid.PcdEmbeddedDirCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedHobCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedHwDebugCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedPciDebugCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedIoEnable|FALSE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedScriptCmd|FALSE

  gEmbeddedTokenSpaceGuid.PcdCacheEnable|TRUE

  gEmbeddedTokenSpaceGuid.PcdPrePiProduceMemoryTypeInformationHob|TRUE
  gArmTokenSpaceGuid.PcdCpuDxeProduceDebugSupport|FALSE

  gEfiMdeModulePkgTokenSpaceGuid.PcdTurnOffUsbLegacySupport|TRUE

  gEfiMdeModulePkgTokenSpaceGuid.PcdConOutGopSupport|TRUE

  gEfiMdeModulePkgTokenSpaceGuid.PcdSupportUpdateCapsuleReset|TRUE


  #
  # Target board Specific PCDs
  #

  gArmTokenSpaceGuid.PcdArmGicV3WithV2Legacy|TRUE

[PcdsFixedAtBuild.common]
  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVendor|L"Qualcomm Technologies, Inc."
  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVersionString|L"4.00"

  gEmbeddedTokenSpaceGuid.PcdEmbeddedPrompt|"EBL"
  gEmbeddedTokenSpaceGuid.PcdPrePiCpuMemorySize|48
  gEmbeddedTokenSpaceGuid.PcdPrePiCpuIoSize|0
  gEfiMdePkgTokenSpaceGuid.PcdMaximumUnicodeStringLength|1000000
  gEfiMdePkgTokenSpaceGuid.PcdMaximumAsciiStringLength|1000000
  gEfiMdePkgTokenSpaceGuid.PcdMaximumLinkedListLength|1000000
  gEfiMdePkgTokenSpaceGuid.PcdSpinLockTimeout|10000000
  gEfiMdePkgTokenSpaceGuid.PcdDebugClearMemoryValue|0xAF

  # Disable perf
  gEfiMdePkgTokenSpaceGuid.PcdPerformanceLibraryPropertyMask|1
  gEfiMdePkgTokenSpaceGuid.PcdPostCodePropertyMask|0
  gEfiMdePkgTokenSpaceGuid.PcdUefiLibMaxPrintBufferSize|320

# DEBUG_ASSERT_ENABLED       0x01
# DEBUG_PRINT_ENABLED        0x02
# DEBUG_CODE_ENABLED         0x04
# CLEAR_MEMORY_ENABLED       0x08
# ASSERT_BREAKPOINT_ENABLED  0x10
# ASSERT_DEADLOOP_ENABLED    0x20

!if $(PRODMODE) == "PRODMODE"
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x2b
!else
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x2f
!endif

#  DEBUG_INIT      0x00000001  // Initialization
#  DEBUG_WARN      0x00000002  // Warnings
#  DEBUG_LOAD      0x00000004  // Load events
#  DEBUG_FS        0x00000008  // EFI File system
#  DEBUG_POOL      0x00000010  // Alloc & Free's
#  DEBUG_PAGE      0x00000020  // Alloc & Free's
#  DEBUG_INFO      0x00000040  // Verbose
#  DEBUG_DISPATCH  0x00000080  // PEI/DXE Dispatchers
#  DEBUG_VARIABLE  0x00000100  // Variable
#  DEBUG_BM        0x00000400  // Boot Manager
#  DEBUG_BLKIO     0x00001000  // BlkIo Driver
#  DEBUG_NET       0x00004000  // SNI Driver
#  DEBUG_UNDI      0x00010000  // UNDI Driver
#  DEBUG_LOADFILE  0x00020000  // UNDI Driver
#  DEBUG_EVENT     0x00080000  // Event messages
#  DEBUG_GCD       0x00100000  // Global Coherency Database changes
#  DEBUG_CACHE     0x00200000  // Memory range cachability changes
#  DEBUG_VERBOSE   0x00400000  // Detailed debug messages that may significantly impact boot performance
#  DEBUG_ERROR     0x80000000  // Error

  # Production Mode Flag
!if $(PRODMODE) == "PRODMODE"
  # Only enable errors, warnings, and Boot Manager
  gEfiMdePkgTokenSpaceGuid.PcdDebugPrintErrorLevel|0x80000400
!else
  gEfiMdePkgTokenSpaceGuid.PcdDebugPrintErrorLevel|0x800fee0f
!endif

  gEfiMdePkgTokenSpaceGuid.PcdReportStatusCodePropertyMask|0x07

  gEmbeddedTokenSpaceGuid.PcdEmbeddedAutomaticBootCommand|""
  gEmbeddedTokenSpaceGuid.PcdEmbeddedDefaultTextColor|0x07
  gEmbeddedTokenSpaceGuid.PcdEmbeddedMemVariableStoreSize|0x10000

  gEmbeddedTokenSpaceGuid.PcdFlashFvMainBase|0
  gEmbeddedTokenSpaceGuid.PcdFlashFvMainSize|0

#
# Optional feature to help prevent EFI memory map fragments
# Turned on and off via: PcdPrePiProduceMemoryTypeInformationHob
# Values are in EFI Pages (4K). DXE Core will make sure that
# at least this much of each type of memory can be allocated
# from a single memory range. This way you only end up with
# maximum of two fragements for each type in the memory map
# (the memory used, and the free memory that was prereserved
# but not used).
#
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiACPIReclaimMemory|0
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiACPIMemoryNVS|0
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiReservedMemoryType|0
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiRuntimeServicesData|80
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiRuntimeServicesCode|40
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiBootServicesCode|400
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiBootServicesData|800
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiLoaderCode|10
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiLoaderData|0

#
# SMBIOS related
#
  # OEM's can set their product info here
  gQcomTokenSpaceGuid.PcdSystemMfrStr|"Qualcomm"
  gQcomTokenSpaceGuid.PcdSystemProductNameStr|""
  gQcomTokenSpaceGuid.PcdSystemProductFamilyStr|""

#
# ACPI related
#
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemId|"QCOM  "
  #Signature_64('Q','C','O','M','E','D','K','2') = 0x324B44454D4F4351 = '2','K','D','E','M','O','C','Q'
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemTableId|0x324B44454D4F4351
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemRevision|0x00000001
  #Signature_32('Q', 'C', 'O', 'M') = 0x4D4F4351
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultCreatorId|0x4D4F4351
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultCreatorRevision|0x00000001
  gQcomTokenSpaceGuid.DisableWriteProtect|TRUE

#
# Target Specific PCDs
#

  #########################################################################
  #                               Memory Map
  #########################################################################

  #################################
  ## Presilicon definitions.
  #################################
  gQcomTokenSpaceGuid.PcdMemoryBase|0x80000000    # Starting address
  gQcomTokenSpaceGuid.PcdMemorySize|0x20000000    # .5 GB

  #################################
  ## HLOS Base Offset and size.
  #################################
  gQcomTokenSpaceGuid.PcdHLOSMemoryBaseOffset|0x90900000 # remaining memory after this is HLOS

  #################################
  ## UEFI Memory Base Offset and Size.
  #################################
  gQcomTokenSpaceGuid.PcdUefiMemPoolBaseOffset|0x80D00000
  gQcomTokenSpaceGuid.PcdUefiMemPoolSize|0x03300000     # 51 MB

  # The HOB list will start here.
  # NOTE: Hob.c is still referencing PcdPrePiHobBase
  gQcomTokenSpaceGuid.PcdPrePiHobBase|0x80D00000

  # Offset to UEFI memory
  gQcomTokenSpaceGuid.PcdPreAllocatedMemorySize|0x00D00000

  #################################
  ## Stack Base Address and size.
  ## TODO: PcdPrePiStackBase is currently used by ModuleEntryPoint.asm
  ##################################
  gEmbeddedTokenSpaceGuid.PcdPrePiStackBase|0x80C00000
  gEmbeddedTokenSpaceGuid.PcdPrePiStackSize|0x00040000  # 256K stack

  #################################
  ## Exception Vector Base.
  ## TODO: PcdCpuVectorBaseAddress is currently used by ModuleEntryPoint.asm
  #################################
  gArmTokenSpaceGuid.PcdCpuVectorBaseAddress|0x80C40000

  #################################
  ## UEFI FBPT Base offset and size.
  #################################
  gQcomTokenSpaceGuid.PcdFBPTPayloadBaseOffset|0x80380000
  gQcomTokenSpaceGuid.PcdFBPTPayloadSize|0x00001000    # 4KB

  #################################
  ## MpParkCode Base offset and size.
  #################################
  gQcomTokenSpaceGuid.PcdMpParkCodeBaseOffset|0x00300000
  gQcomTokenSpaceGuid.PcdMpParkCodeSize|0x00080000    # 64KB per Core * up to 8 cores = 512KB

  #################################
  ## FD Base offset (refer to .fdf for FD size)
  #################################
  gQcomTokenSpaceGuid.PcdEmbeddedFdBaseAddress|0x80200000

  #################################
  ## Shared Memory Base offset and size.
  ## TODO: PcdSmemBaseAddress is currently used by smem library
  #################################
  gQcomTokenSpaceGuid.PcdSmemBaseAddress|0x86000000  #64-bit smem addr
  gQcomTokenSpaceGuid.PcdSmemSize|0x00200000     # 2MB
  ## Generic SMEM information that SMEM lib might need
  ## LSB represents chip family, other bytes are unused for now.
  gQcomTokenSpaceGuid.PcdSmemInformation|0x00000003  # 3 = honeybadger-family

  # Maximum number of memory regions defined in the above memory map
  gQcomTokenSpaceGuid.PcdMaxMemRegions|64

  #################################
  ## IMem Base Address and size.
  #################################

  gQcomTokenSpaceGuid.PcdIMemCookiesBase|0x066BF000
  gQcomTokenSpaceGuid.PcdIMemCookiesSize|0x00001000

  # Boot Log Address and Size
  gQcomTokenSpaceGuid.PcdBootLogAddrPtr|0x066BF028
  gQcomTokenSpaceGuid.PcdBootLogSizePtr|0x066BF030
  
  # Cookie (memory capture mode) Address for crash dump support
  gQcomTokenSpaceGuid.PcdHLOSCrashCookieAddr|0x066BF018

  # Uefi Debug Cookie Address and value
  gQcomTokenSpaceGuid.PcdUefiDebugCookieOffset|0x944
  gQcomTokenSpaceGuid.PcdUefiDebugCookie|0x55444247

  gArmTokenSpaceGuid.PcdCpuResetAddress|0x08000000    # warm reset should use this

  # Secure boot efuse register address
  gQcomTokenSpaceGuid.PcdSecbootRegAddr|0x0005E030
  gQcomTokenSpaceGuid.PcdSecbootAuthEnMask|0x20


  # Timer Debug
  gQcomTokenSpaceGuid.PcdTimerDxeDebug|FALSE

  # Timers
  gEmbeddedTokenSpaceGuid.PcdInterruptBaseAddress|0x9BC0000

  # Timers

  gArmTokenSpaceGuid.PcdArmArchTimerFreqInHz|19200000
  gArmTokenSpaceGuid.PcdArmArchTimerSecIntrNum|29
  gArmTokenSpaceGuid.PcdArmArchTimerIntrNum|30


  gArmTokenSpaceGuid.PcdGicDistributorBase|0x9BC0000
  gArmTokenSpaceGuid.PcdGicInterruptInterfaceBase|0x65C0000

  gQcomTokenSpaceGuid.PcdInterruptTimerIndex|3
  gQcomTokenSpaceGuid.PcdInterruptTimerPeriod|100000
  gQcomTokenSpaceGuid.PcdTimer1IRQ|0x28
  gQcomTokenSpaceGuid.PcdTimerTestClockFreq|918000000 # Pcycles runs at 918 MHz
  gQcomTokenSpaceGuid.PcdTimerTestClockFreqDiv|64 # Pcycles divided by value
  gQcomTokenSpaceGuid.PcdQGICCBaseAddress|0x65C0000
  gQcomTokenSpaceGuid.PcdQTimerAC_BasePhys|0x09840000
  gQcomTokenSpaceGuid.PcdQTimerV1BasePhys|0x09850000
  gQcomTokenSpaceGuid.PcdQTimerV2BasePhys|0x09860000
  gQcomTokenSpaceGuid.PcdQTimerFrame|0
  gQcomTokenSpaceGuid.PcdQTimerEnabled|1
  gQcomTokenSpaceGuid.PcdPerfomanceCounterEnd|0xFFFFFFFF
  gQcomTokenSpaceGuid.PcdPerfomanceCounterStart|0x00000000
  gQcomTokenSpaceGuid.PcdInterruptVectorDGT|17
  gQcomTokenSpaceGuid.PcdInterruptVectorGPT0|18
  gQcomTokenSpaceGuid.PcdInterruptVectorGPT1|19
  gQcomTokenSpaceGuid.PcdPerformanceCounterIndex|3
  gQcomTokenSpaceGuid.PcdRealTimeClockResolution|1
  gQcomTokenSpaceGuid.PcdRealTimeClockAccuracy|200000000
  gQcomTokenSpaceGuid.PcdRealTimeClockSetToZero|1

  #TODO: Need to correct based on frequency plan
  gQcomTokenSpaceGuid.PcdKraitFrequencyMhz|960

  #MPM_SLEEP_TIMETICK_COUNT_VAL for time from power on
  gQcomTokenSpaceGuid.PcdTimeTickClkAddress|0x004A3000

  gQcomTokenSpaceGuid.PcdPsHoldAddress|0x4AB000

  #I2C
  gQcomTokenSpaceGuid.EEPROMI2CCore|3
  gQcomTokenSpaceGuid.EEPROMSlaveAddress|0x50

  # SPMI
  gQcomTokenSpaceGuid.SPMIBasePhysical0|0x04000000
  gQcomTokenSpaceGuid.SPMIBasePhysical1|0
  gQcomTokenSpaceGuid.SPMIMemoryMapSize|0x1C00 
  gQcomTokenSpaceGuid.SPMIOwner|0
  gQcomTokenSpaceGuid.SPMIDebugChannel|1

  # Display
  gQcomTokenSpaceGuid.PcdPanelType|0
  gQcomTokenSpaceGuid.PcdDisableHDMI|0
  gQcomTokenSpaceGuid.PcdHPDPollCount|200
  gQcomTokenSpaceGuid.PcdBacklightLevel|50
  gQcomTokenSpaceGuid.PcdDisplayForceSwRenderer|FALSE


  # SDCC
  gQcomTokenSpaceGuid.SDCCExtSlotNumber|1
  gQcomTokenSpaceGuid.SDCCEmmcSlotNumber|0
  gQcomTokenSpaceGuid.SDCCRpmbKey|{0x20, 0xAA, 0x9C, 0xF4, 0x99, 0x4D, 0xD4, 0xFE, 0xA5, 0x85, 0xBE, 0x96, 0x6, 0x1, 0xD1, 0xA9, 0xC3, 0x3, 0x4F, 0x91, 0x62, 0x7C, 0x64, 0x53, 0x38, 0xC1, 0x1F, 0xF2, 0x76, 0x4D, 0x2E, 0xC4}
  gQcomTokenSpaceGuid.SDCCEventBasedSDDetectEnabled|FALSE
  gQcomTokenSpaceGuid.SDCCDmaEnabled|TRUE
  gQcomTokenSpaceGuid.SDCCEmmcHwResetEnabled|TRUE
  gQcomTokenSpaceGuid.SDCCMaxSlotNumber|2
  gQcomTokenSpaceGuid.SDCCSdhciEnable|TRUE
  ## SDCC eMMC speed mode - DDR50: 0, HS200: 1, HS400: 2 SDR: 3 ##
  gQcomTokenSpaceGuid.SDCCEmmcSpeedMode|1

  #
  # ARM Pcds
  #
  gArmTokenSpaceGuid.PcdArmUncachedMemoryMask|0x0000000040000000

  # Cookies
  gQcomTokenSpaceGuid.PcdMassStorageCookie0|0x10
  gQcomTokenSpaceGuid.PcdMassStorageCookie1|0x0
  gQcomTokenSpaceGuid.PcdMassStorageCookieAddr|0x007B3000
  gQcomTokenSpaceGuid.PcdMassStorageCookieSize|4
  gQcomTokenSpaceGuid.PcdMassStorageEnabled|TRUE

  # Timer sync not required
  gQcomTokenSpaceGuid.PcdSyncTimerToMPP|FALSE

  # UART
  gQcomTokenSpaceGuid.UartPlatform|"MSM8996"

!if $(PRODMODE) == "PRODMODE"
  # Version info needed on LCD Display
  gQcomTokenSpaceGuid.VersionDisplay|FALSE
!else
  # Version info needed on debug port
  gQcomTokenSpaceGuid.VersionDisplay|TRUE
!endif

  # offset to 4KB shared memory in System IMEM             : 0x8603F000
  # offset to 200 bytes HLOS region within this 4KB memory : 0x658
  # offset 0  : 4 byte physical address for DebugLog Override
  gQcomTokenSpaceGuid.PcdDebugLogOverrideAddress|0x08642658

  # Magic signature value that is used to indicate DebugLog Override
  # capability in DebugLib of DXE
  gQcomTokenSpaceGuid.PcdDebugLogOverrideMagic|0x534B4950

  # Hard/Warm Reset
  gQcomTokenSpaceGuid.PcdUefiHardResetConfigure|TRUE
  gQcomTokenSpaceGuid.PcdUefiWarmResetConfigure|TRUE
  gQcomTokenSpaceGuid.PcdUefiHardResetEnabled|TRUE
  gQcomTokenSpaceGuid.PcdUefiWarmResetEnabled|TRUE

  # PON Debounce
  gQcomTokenSpaceGuid.PcdPONDebounce|0x00000001 # max value 0x07


  # ChargerTaskSupported
  gQcomTokenSpaceGuid.PcdChargerTaskSupported|FALSE

  # Default to EL1
  gQcomTokenSpaceGuid.PcdSwitchToEL1|TRUE

[PcdsPatchableInModule]
  ## This PCD defines the Console output column and the default value is 25 according to UEFI spec
  #gEfiMdeModulePkgTokenSpaceGuid.PcdConOutRow|53

  ## This PCD defines the Console output row and the default value is 80 according to UEFI spec
  #gEfiMdeModulePkgTokenSpaceGuid.PcdConOutColumn|75

  # This PCD should be set to 0 then video resolution could be at highest resolution.
  gEfiMdeModulePkgTokenSpaceGuid.PcdVideoHorizontalResolution|0
  gEfiMdeModulePkgTokenSpaceGuid.PcdVideoVerticalResolution|0


################################################################################
#
# Components Section - list of all EDK II Modules needed by this Platform
#
################################################################################
[Components.common]

#
# SEC
#
  
  QcomPkg/Msm8996Pkg/Library/XBLRamDumpLib/XBLRamDumpLib.inf {
    <LibraryClasses>
      DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf  
      <PcdsFixedAtBuild>
        gQcomTokenSpaceGuid.SDCCDmaEnabled|TRUE
        gQcomTokenSpaceGuid.SDCCSdhciEnable|TRUE
        gQcomTokenSpaceGuid.SDCCEmmcSpeedMode|1
  }
  
  QcomPkg/Msm8996Pkg/Library/DDRTrngLib/DDRTrngLib.inf {
	<LibraryClasses>
	  DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
	  DDILib|QcomPkg/Msm8996Pkg/Library/DDILibNull/DDILibNull.inf
	  <PcdsFixedAtBuild>
        gQcomTokenSpaceGuid.SDCCDmaEnabled|TRUE
        gQcomTokenSpaceGuid.SDCCSdhciEnable|TRUE
        gQcomTokenSpaceGuid.SDCCEmmcSpeedMode|1
  }


# DeviceProgrammerCommonLib to be stubbed out for XBL image. To be enabled only in actual
# device programmer image
  QcomPkg/XBLLoader/XBLLoader.inf {
    <LibraryClasses>
      XBLLoaderLib|QcomPkg/Msm8996Pkg/Library/XBLLoaderLib/XBLLoaderLib.inf
      DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
      DevPrgLLib|QcomPkg/Msm8996Pkg/Library/DevPrgLLibNull/DevPrgLLibNull.inf
      DevPrgDLib|QcomPkg/Msm8996Pkg/Library/DevPrgDLibNull/DevPrgDLibNull.inf
      DDILib|QcomPkg/Msm8996Pkg/Library/DDILibNull/DDILibNull.inf
      SecImgAuthLib|QcomPkg/Library/SecImgAuthLib/SecImgAuthLib.inf
      SecDbgLib|QcomPkg/Msm8996Pkg/Library/SecDbgLib/SecDbgLib.inf
      BootDdrTrainingLib|QcomPkg/XBLLoader/BootDdrTrainingLib.inf
      BootDebugLib|QcomPkg/XBLLoader/BootDebugLib.inf
      SPILib|QcomPkg/Library/SPILib/SPILibLoader.inf
      I2CLib|QcomPkg/Library/I2CLib/I2CLibLoader.inf
      HashLib|QcomPkg/Msm8996Pkg/Library/HashLib/HashLibHw.inf
      VSenseTargetLib|QcomPkg/Msm8996Pkg/Library/VSenseTargetLib/VSenseTargetLib.inf
      PmicLoaderLib|QcomPkg/Msm8996Pkg/Library/PmicLib/PmicLoaderLib.inf
      LoaderAptLib|QcomPkg/Msm8996Pkg/Library/LoaderAptLib/LoaderAptLib.inf

      #This PCD values should match with the ones defined in boot_sbl_shared.h for sbl_build_type.
      <PcdsFixedAtBuild>
        gQcomTokenSpaceGuid.PcdBuildType|0x0
        gQcomTokenSpaceGuid.SDCCDmaEnabled|TRUE
        gQcomTokenSpaceGuid.SDCCSdhciEnable|TRUE
        gQcomTokenSpaceGuid.SDCCEmmcSpeedMode|1
  }

  QcomPkg/XBLLoader/DevPrgD.inf {
     <LibraryClasses>
      XBLLoaderLib|QcomPkg/Msm8996Pkg/Library/XBLLoaderLib/XBLLoaderDevProgLib.inf
      SmemLib|QcomPkg/Msm8996Pkg/Library/SmemLibNull/SmemLibNull.inf
      SecImgAuthLib|QcomPkg/Library/SecImgAuthLib/SecImgAuthLib.inf
      SecDbgLib|QcomPkg/Msm8996Pkg/Library/SecDbgLib/SecDbgLib.inf
      SecBootAuthLib|QcomPkg/Msm8996Pkg/Library/SecBootAuthLib/SecBootAuthLib.inf
      DevPrgLLib|QcomPkg/Msm8996Pkg/Library/DevPrgLLibNull/DevPrgLLibNull.inf
      DevPrgDLib|QcomPkg/Msm8996Pkg/Library/DevPrgDLib/DevPrgDLib.inf
      DDILib|QcomPkg/Msm8996Pkg/Library/DDILibNull/DDILibNull.inf
      BootDdrTrainingLib|QcomPkg/XBLLoader/BootDdrTrainingLibNull.inf
      BootDebugLib|QcomPkg/XBLLoader/BootDebugLibNull.inf
      HashLib|QcomPkg/Msm8996Pkg/Library/HashLib/HashLibHw.inf
      VSenseTargetLib|QcomPkg/Msm8996Pkg/Library/VSenseTargetLib/VSenseTargetLib.inf
      PmicLoaderLib|QcomPkg/Msm8996Pkg/Library/PmicLib/PmicLoaderNoChgrLib.inf
      LoaderAptLib|QcomPkg/Msm8996Pkg/Library/LoaderAptLibNull/LoaderAptLibNull.inf
      QusbPblDloadCheckLib|QcomPkg/Msm8996Pkg/Library/QusbFedlLibNull/QusbFedlLibNull.inf

      <PcdsFixedAtBuild>
        gQcomTokenSpaceGuid.PcdBuildType|0x2
  }

  QcomPkg/XBLLoader/DevPrgL.inf {
     <LibraryClasses>
      XBLLoaderLib|QcomPkg/Msm8996Pkg/Library/XBLLoaderLib/XBLLoaderDevProgLib.inf
      SmemLib|QcomPkg/Msm8996Pkg/Library/SmemLibNull/SmemLibNull.inf
      SecImgAuthLib|QcomPkg/Library/SecImgAuthLib/SecImgAuthLib.inf
      SecDbgLib|QcomPkg/Msm8996Pkg/Library/SecDbgLib/SecDbgLib.inf
      SecBootAuthLib|QcomPkg/Msm8996Pkg/Library/SecBootAuthLib/SecBootAuthLib.inf
      DevPrgLLib|QcomPkg/Msm8996Pkg/Library/DevPrgLLib/DevPrgLLib.inf
      DevPrgDLib|QcomPkg/Msm8996Pkg/Library/DevPrgDLibNull/DevPrgDLibNull.inf
      DDILib|QcomPkg/Msm8996Pkg/Library/DDILibNull/DDILibNull.inf
      BootDdrTrainingLib|QcomPkg/XBLLoader/BootDdrTrainingLibNull.inf
      BootDebugLib|QcomPkg/XBLLoader/BootDebugLibNull.inf
      HashLib|QcomPkg/Msm8996Pkg/Library/HashLib/HashLibHw.inf      
      VSenseTargetLib|QcomPkg/Msm8996Pkg/Library/VSenseTargetLib/VSenseTargetLib.inf
      PmicLoaderLib|QcomPkg/Msm8996Pkg/Library/PmicLib/PmicLoaderNoChgrLib.inf
      LoaderAptLib|QcomPkg/Msm8996Pkg/Library/LoaderAptLibNull/LoaderAptLibNull.inf
      QusbPblDloadCheckLib|QcomPkg/Msm8996Pkg/Library/QusbFedlLibNull/QusbFedlLibNull.inf

      <PcdsFixedAtBuild>
        gQcomTokenSpaceGuid.PcdBuildType|0x1
  }

  QcomPkg/XBLLoader/DevPrgDV.inf {
     <LibraryClasses>
      XBLLoaderLib|QcomPkg/Msm8996Pkg/Library/XBLLoaderLib/XBLLoaderDevProgLib.inf
      SmemLib|QcomPkg/Msm8996Pkg/Library/SmemLibNull/SmemLibNull.inf
      SecImgAuthLib|QcomPkg/Library/SecImgAuthLib/SecImgAuthLib.inf
      SecDbgLib|QcomPkg/Msm8996Pkg/Library/SecDbgLib/SecDbgLib.inf
      SecBootAuthLib|QcomPkg/Msm8996Pkg/Library/SecBootAuthLib/SecBootAuthLib.inf
      DevPrgLLib|QcomPkg/Msm8996Pkg/Library/DevPrgLLibNull/DevPrgLLibNull.inf
      DevPrgDLib|QcomPkg/Msm8996Pkg/Library/DevPrgDVLib/DevPrgDVLib.inf
      DDILib|QcomPkg/Msm8996Pkg/Library/DDILibNull/DDILibNull.inf
      BootDdrTrainingLib|QcomPkg/XBLLoader/BootDdrTrainingLibNull.inf
      BootDebugLib|QcomPkg/XBLLoader/BootDebugLibNull.inf
      HashLib|QcomPkg/Msm8996Pkg/Library/HashLib/HashLibHw.inf
      VSenseTargetLib|QcomPkg/Msm8996Pkg/Library/VSenseTargetLib/VSenseTargetLib.inf
      PmicLoaderLib|QcomPkg/Msm8996Pkg/Library/PmicLib/PmicLoaderNoChgrLib.inf
      LoaderAptLib|QcomPkg/Msm8996Pkg/Library/LoaderAptLibNull/LoaderAptLibNull.inf
      QusbPblDloadCheckLib|QcomPkg/Msm8996Pkg/Library/QusbFedlLibNull/QusbFedlLibNull.inf

      <PcdsFixedAtBuild>
        gQcomTokenSpaceGuid.PcdBuildType|0x2
  }
QcomPkg/XBLLoader/DDI.inf {
    <LibraryClasses>
      XBLLoaderLib|QcomPkg/Msm8996Pkg/Library/XBLLoaderLib/XBLLoaderDDILib.inf
      SmemLib|QcomPkg/Msm8996Pkg/Library/SmemLibNull/SmemLibNull.inf
      DevPrgLLib|QcomPkg/Msm8996Pkg/Library/DevPrgLLibNull/DevPrgLLibNull.inf
      DevPrgDLib|QcomPkg/Msm8996Pkg/Library/DevPrgDLibNull/DevPrgDLibNull.inf
      DDILib|QcomPkg/Msm8996Pkg/Library/DDILib/DDILib.inf
      PmicConfigLib|QcomPkg/Msm8996Pkg/Library/PmicLib/PmicConfigLib.inf
      SecImgAuthLib|QcomPkg/Library/SecImgAuthLib/SecImgAuthLib.inf
      SecDbgLib|QcomPkg/Msm8996Pkg/Library/SecDbgLib/SecDbgLib.inf
      BootDdrTrainingLib|QcomPkg/XBLLoader/BootDdrTrainingLib.inf
      BootDebugLib|QcomPkg/XBLLoader/BootDebugLib.inf     
      HashLib|QcomPkg/Msm8996Pkg/Library/HashLib/HashLibHw.inf
      VSenseTargetLib|QcomPkg/Msm8996Pkg/Library/VSenseTargetLib/VSenseTargetLib.inf
      PmicLoaderLib|QcomPkg/Msm8996Pkg/Library/PmicLib/PmicLoaderLib.inf
      LoaderAptLib|QcomPkg/Msm8996Pkg/Library/LoaderAptLib/LoaderAptLib.inf
      MuslLib|QcomPkg/Library/MuslLib/MuslLib.inf
      QusbPblDloadCheckLib|QcomPkg/Msm8996Pkg/Library/QusbFedlLibNull/QusbFedlLibNull.inf

      
      
      #This PCD values should match with the ones defined in boot_sbl_shared.h for sbl_build_type.
      <PcdsFixedAtBuild>
        gQcomTokenSpaceGuid.PcdBuildType|0x5
        
  }

  QcomPkg/XBLLoader/DevPrgLV.inf {
     <LibraryClasses>
      XBLLoaderLib|QcomPkg/Msm8996Pkg/Library/XBLLoaderLib/XBLLoaderDevProgLib.inf
      SmemLib|QcomPkg/Msm8996Pkg/Library/SmemLibNull/SmemLibNull.inf
      SecImgAuthLib|QcomPkg/Library/SecImgAuthLib/SecImgAuthLib.inf
      SecDbgLib|QcomPkg/Msm8996Pkg/Library/SecDbgLib/SecDbgLib.inf
      SecBootAuthLib|QcomPkg/Msm8996Pkg/Library/SecBootAuthLib/SecBootAuthLib.inf
      DevPrgLLib|QcomPkg/Msm8996Pkg/Library/DevPrgLVLib/DevPrgLVLib.inf
      DevPrgDLib|QcomPkg/Msm8996Pkg/Library/DevPrgDLibNull/DevPrgDLibNull.inf
      DDILib|QcomPkg/Msm8996Pkg/Library/DDILibNull/DDILibNull.inf
      BootDdrTrainingLib|QcomPkg/XBLLoader/BootDdrTrainingLibNull.inf
      BootDebugLib|QcomPkg/XBLLoader/BootDebugLibNull.inf
      HashLib|QcomPkg/Msm8996Pkg/Library/HashLib/HashLibHw.inf      
      VSenseTargetLib|QcomPkg/Msm8996Pkg/Library/VSenseTargetLib/VSenseTargetLib.inf
      PmicLoaderLib|QcomPkg/Msm8996Pkg/Library/PmicLib/PmicLoaderNoChgrLib.inf
      LoaderAptLib|QcomPkg/Msm8996Pkg/Library/LoaderAptLibNull/LoaderAptLibNull.inf
      QusbPblDloadCheckLib|QcomPkg/Msm8996Pkg/Library/QusbFedlLibNull/QusbFedlLibNull.inf

      <PcdsFixedAtBuild>
        gQcomTokenSpaceGuid.PcdBuildType|0x1
  }

  QcomPkg/Msm8996Pkg/Library/PmicLib/Pmic.inf {
  <LibraryClasses>
      DDILib|QcomPkg/Msm8996Pkg/Library/DDILibNull/DDILibNull.inf
      <PcdsFixedAtBuild>
        gQcomTokenSpaceGuid.PcdBuildType|0x3
  }

  QcomPkg/Library/JtagProgrammerLib/JtagProgrammer.inf {
      <PcdsFixedAtBuild>
        gQcomTokenSpaceGuid.PcdBuildType|0x4
        gQcomTokenSpaceGuid.SDCCDmaEnabled|FALSE
        gQcomTokenSpaceGuid.SDCCSdhciEnable|FALSE
        gQcomTokenSpaceGuid.SDCCEmmcSpeedMode|0        
  }
 
