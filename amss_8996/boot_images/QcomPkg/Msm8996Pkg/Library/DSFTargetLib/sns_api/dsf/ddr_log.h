#ifndef __DDR_LOG_H__
#define __DDR_LOG_H__

/**
 * @file ddr_log.h
 * @brief
 * Header file for DDR logging.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DSFTargetLib/sns_api/dsf/ddr_log.h#4 $
$DateTime: 2015/12/07 13:32:35 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
04/11/14   sr      supported DDR logging for SNS DDR driver .
09/12/12   sl      Use macro-style DDR logging.
07/19/12   tw      Implemented qdss swevent wrapper around DDR logging.
03/02/12   tw      Initial version.
================================================================================
                      Copyright 2012 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/

#include <stdio.h>
#include <stdarg.h>

/*==============================================================================
                                  MACROS
==============================================================================*/

#define DDR_LOG_BUF_LEN_MAX		256

#if defined( DDI_BUILD  ) || defined( BUILD_BOOT_CHAIN )
  #include "boot_logger.h"
  #define DDR_LOG(...)
#endif


/*==============================================================================
                                  TYPES
==============================================================================*/
enum ddr_log_level
{
  DDR_STATUS  = 0, /* no provided for DDRSNS , existing for legacy targets support */
  DDR_WARNING = 1, /* no provided for DDRSNS , existing for legacy targets support */
  DDR_ERROR   = 2, /* provided for flagging Error conditions in DDRSNS */
  DDR_NORMAL  = 3, /* provided for printing interesting/debug  information along with BEGIN/END of each DDRSNS function */
  DDR_DETAIL  = 4, /* provided for detailed logging information i.e; coarse/fine delay training & other training(CA/WRLRL/RCW) results with DDRSNS */
};



/*==============================================================================
                                  DATA
==============================================================================*/
extern enum ddr_log_level ddr_log_level;

/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
/* ============================================================================
**  Function : ddr_logf
** ============================================================================
*/
/*!
*   @brief
*
*
*   @details
*
*   @param
*   None
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

static inline void ddr_printf(uint32 severity, const char* fmt, ...)
{
#if defined( DDI_BUILD  ) || defined( BUILD_BOOT_CHAIN )
    va_list vargs;
    va_start(vargs, fmt);
    vsnprintf(ddr_printf_buf, DDR_LOG_BUF_LEN_MAX-1, fmt, vargs);
    va_end(vargs); 
	 boot_log_message(ddr_printf_buf);
#else
    #ifdef SVE_PRINTF_ENABLED
    va_list vargs;
    va_start(vargs, fmt);
    vprintf(fmt, vargs);
    va_end(vargs);
    #endif
#endif
} /* ddr_printf */

#endif /* __DDR_LOG_H__ */
