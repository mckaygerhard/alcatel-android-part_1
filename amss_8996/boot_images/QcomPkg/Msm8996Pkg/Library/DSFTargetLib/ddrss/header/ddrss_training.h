/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DSFTargetLib/ddrss/header/ddrss_training.h#7 $
$DateTime: 2015/12/04 17:21:20 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/08/14   jeasley      Update DDRSS_wrlvl prototype to use zero based channel and cs.
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#ifndef __DDRSS_TRAINING_H__
#define __DDRSS_TRAINING_H__

#include "ddr_phy_technology.h"

typedef enum
{
    TRAINING_TYPE_CA_VREF   = 0,
    TRAINING_TYPE_WR_DQDQS  = 1,
    TRAINING_TYPE_RD_DQDQS  = 2,
    TRAINING_TYPE_MAX       = 0x7f
} TRAINING_TYPE;

#define NUM_PRFS_BANDS            8
#define TRAINING_THRESHOLD        533000 

#define TRAINING_START_PRFS       3
#define FSP_THRESHOLD_PRFS_BAND   3
#define MAX_TRAINING_FREQ_NUM     5

#define DTTS_ACQUIRE_MODE        0x1
#define DTTS_TRACKING_MODE       0x2

#define DTTS_WAKEUP_DISABLE_ALL        0x70000
#define DTTS_WAKEUP_ENABLE_ALL         0x00000
#define DTTS_WAKEUP_ENABLE_SHKE_ONLY   0x60000

#define SHKE_TRIGGER_ENABLE         0x1
#define SHKE_TRIGGER_DISABLE        0x2
#define DTTS_FW_IDLE                0

#define RX_DCC                      10
#define PHASE                       2
#define CLK_DCC_OFFSET_RANGE        7     //2*3(range) + 1

#define ODT_FSP_EN_FREQ_THRESHOLD 1000000 //Match with settings in bimc_data.c

#define FEATURE_DISABLE            0
#define FEATURE_ENABLE             1
#define DCC_TRAINING_WRLVL_WR90_IO 7
#define DCC_TRAINING_WR90          2
#define DCC_TRAINING_WRLVL         1
#define DCC_TRAINING_WR90_IO       6

#define FSP_OP_BOOT               0x0
#define FSP_OP_HIGH               0x1

#define MPRX                      0
#define HPRX                      1
#define MR23_data                 0x3F

#define IO_QUAL_OFFSET_DOUBLE_STEP_SIZE 0x5
#define LOCAL_VREF_MID_VALUE            0x08
#define LOCAL_VREF_MAX_VALUE            0x0F

#define LP_MODE                   0
#define HP_MODE                   1
#define SWEEP_LEFT                0
#define SWEEP_RIGHT               1

#define DCC_TRAINING_SEL          7         // Selecting all three DCC training flows
#define MR12_VREF_RANGE_BIT       0         // MR12 OP6 -> 0: Vref Range 0; 1: MR12 Vref Range 1
#define CA_VREF_RANGE_THRESHOLD   30

// PHY Power Collapse Broadcast for dual channel devices
#define PC_PHY_CA_DQ_BROADCAST    0x1FBF
#define PC_CC_BROADCAST           0x2040

#define PINS_PER_PHY                    10
#define PINS_PER_PHY_CONNECTED_NO_DBI    8
#define PINS_PER_PHY_CONNECTED_WITH_DBI  9

//RCW related
#define   RCW_COARSE_DELAY        1
#define   RCW_FINE_DELAY          0

// Defines for Periodic Training
#define LOG2(i) (( (i)>=8u)? ( (i)>=32u)?5 :  ( (i)>=16u)?4 : 3 :  ( (i)>=4u)?2 :  ( (i)>=2u)?1 : 0 )
#define PT_SLAB_THRESHOLD_PS      10
#define NUM_DIT_COUNT 12 // NOTE: NUM_DIT_COUN must be an even number

extern uint32 freq_range[6]; //for 1353MHz expansion. 
extern uint8 connected_bit_mapping_no_DBI_A  [PINS_PER_PHY_CONNECTED_NO_DBI];
extern uint8 connected_bit_mapping_no_DBI_B  [PINS_PER_PHY_CONNECTED_NO_DBI];
extern uint8 connected_bit_mapping_with_DBI_A [PINS_PER_PHY_CONNECTED_WITH_DBI];
extern uint8 connected_bit_mapping_with_DBI_B [PINS_PER_PHY_CONNECTED_WITH_DBI];
//extern uint8 connected_bit_mapping_CA [PINS_PER_PHY_CONNECTED_CA];


/***************************************************************************************************
 Training Params Struct
 ***************************************************************************************************/
typedef struct
{
   struct
   {
      uint8 max_loopcnt;
   } dcc;

   struct
   {
      uint8 max_loopcnt;
   } dit;

   struct
   {
      uint8 max_loopcnt;
      uint8 cdc_training_enable;
      uint8 coarse_vref_start_value;
      uint8 coarse_vref_max_value;
      uint8 coarse_vref_step;
      uint8 coarse_cdc_start_value;
      uint8 coarse_cdc_max_value;
      uint8 coarse_cdc_step;
      uint8 fine_training_enable;
      uint8 fine_cdc_max_value;
      uint8 fine_cdc_step;
      uint8 fine_vref_enable;
      uint8 fine_vref_step;
      uint8 fine_perbit_start_value;
   } ca_vref;

   struct
   {
      uint8 max_loopcnt;
      uint8 max_coarse_cdc;
      uint8 max_fine_cdc;
      uint8 coarse_cdc_step;
      uint8 fine_cdc_step;
      uint8 feedback_percent;
   } wrlvl;

   struct
   {
       uint8  max_loopcnt;
       uint8  max_coarse_cdc;
       uint8  max_fine_cdc;
       uint8  coarse_cdc_step;
       uint8  fine_cdc_step;
   } rcw;

   struct
   {
      uint8 max_loopcnt;
      uint8 coarse_vref_start_mprx;
      uint8 coarse_vref_min_mprx;
      uint8 coarse_vref_max_mprx;
      uint8 coarse_vref_start_hprx;
      uint8 coarse_vref_min_hprx;
      uint8 coarse_vref_max_hprx;
      uint8 coarse_vref_start_value;
      uint8 coarse_vref_max_value;
      uint8 coarse_vref_step;
      uint8 coarse_cdc_start_value;
      uint8 coarse_cdc_max_value;
      uint8 coarse_cdc_step;
      uint8 coarse_hp_vref_mask_min_delta;
      uint8 coarse_hp_vref_mask_max_delta;
      uint8 coarse_mp_vref_mask_min_delta;
      uint8 coarse_mp_vref_mask_max_delta;
      uint8 coarse_cdc_min_width_value;
      uint8 fine_training_enable;
      uint8 fine_vref_start_value;
      uint8 fine_vref_max_value;
      uint8 fine_cdc_start_value;
      uint8 fine_cdc_max_value;
      uint8 fine_cdc_step;
      uint8 fine_vref_enable;
      uint8 fine_vref_step;
      uint8 pbit_start_value;
      uint8 coarse_vref_low_limit_hprx;
   } rd_dqdqs;

   struct
   {
      uint8 max_loopcnt;
      uint8 coarse_vref_start_value;
      uint8 coarse_vref_max_value;
      uint8 coarse_vref_step;
      uint8 coarse_cdc_start_value;
      uint8 coarse_cdc_max_value;
      uint8 coarse_cdc_step;
      uint8 coarse_cdc_min_width_value;
      uint8 coarse_MR14_1_vref_mask_min_delta;
      uint8 coarse_MR14_1_vref_mask_max_delta;
      uint8 coarse_MR14_0_vref_mask_min_delta;
      uint8 coarse_MR14_0_vref_mask_max_delta;
      uint8 fine_training_enable;
      uint8 fine_cdc_top_freq_start_value;
      uint8 fine_cdc_start_value;
      uint8 fine_cdc_top_freq_max_value;
      uint8 fine_cdc_max_value;
      uint8 fine_cdc_step;
      uint8 fine_vref_enable;
      uint8 fine_vref_step;
      uint8 pbit_start_value;
   } wr_dqdqs;

} training_params_t;


/***************************************************************************************************
 Training data struct
 This is the organization of ddr->flash_params.training_data struct
 ***************************************************************************************************/
struct training_results
{
   struct
   {
     uint16 dit_count[NUM_CH][NUM_CS][NUM_DIE_PCH]; // 8996: 2 * (2 * 2 * 2) = 16.
     uint32 dit_training_freq;                      // 8996: 4. 
     uint16 dit_runtime_count;                      // 8996: 2. 
     uint8  dit_fine_step_limit;                    // 8996: 1. 
     uint16 dit_interval_per_sec;                   // 8996: 2. 
     uint8  dit_loop_count;                         // 8996: 1. 
     uint16  dit_acq_ndx;                           // 8996: 2. 
     uint16  dit_trac_ndx;                          // 8996: 2. 
   } dit; // 8996: Total = 30.

   struct
   {
      uint16 wrlvl_stat_dq [NUM_CH][NUM_DQ_PCH];    // 8996: 2 * (2 * 4) = 16.
      uint16 t90_stat_dq   [NUM_CH][NUM_DQ_PCH];    // 8996: 2 * (2 * 4) = 16.
      uint16 iodqs_stat_dq [NUM_CH][NUM_DQ_PCH];    // 8996: 2 * (2 * 4) = 16.
      uint16 iodq_stat_dq  [NUM_CH][NUM_DQ_PCH];    // 8996: 2 * (2 * 4) = 16.

      uint16 wrlvl_stat_ca [NUM_CH][NUM_CA_PCH];    // 8996: 2 * (2 * 2) = 8.
      uint16 t90_stat_ca   [NUM_CH][NUM_CA_PCH];    // 8996: 2 * (2 * 2) = 8.
      uint16 iodqs_stat_ca [NUM_CH][NUM_CA_PCH];    // 8996: 2 * (2 * 2) = 8.
      uint16 iodq_stat_ca  [NUM_CH][NUM_CA_PCH];    // 8996: 2 * (2 * 2) = 8.
   } dcc; // 8996: Total = 96.

   struct
   {
      uint8 coarse_vref    [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];       // 8996: 8 * 2 * 2 * 2 = 64.
      uint8 fine_vref      [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];       // 8996: 8 * 2 * 2 * 2 = 64.
      uint8 coarse_cdc     [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];       // 8996: 8 * 2 * 2 * 2 = 64.
      uint8 fine_cdc       [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];       // 8996: 8 * 2 * 2 * 2 = 64.
      uint8 dq_retmr       [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];       // 8996: 8 * 2 * 2 * 2 = 64.
      uint8 dq_half_cycle  [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];       // 8996: 8 * 2 * 2 * 2 = 64.
      uint8 dq_full_cycle  [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];       // 8996: 8 * 2 * 2 * 2 = 64.
      uint8 perbit_delay   [NUM_CH][NUM_CS][NUM_CA_PCH][PINS_PER_PHY];         // 8996: 2 * 2 * 2 * 10 = 80.
   } ca_vref; // 8996: Total = 528.


   struct
   {
      uint8  dq_dqs_retmr       [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];  //8996: 8 * 2 * 2 * 4 = 128.
      uint8  dq_coarse_dqs_delay[NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];  //8996: 8 * 2 * 2 * 4 = 128.
      uint8  dq_fine_dqs_delay  [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];  //8996: 8 * 2 * 2 * 4 = 128.
      uint8  dq_dqs_half_cycle  [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];  //8996: 8 * 2 * 2 * 4 = 128.
      uint8  dq_dqs_full_cycle  [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];  //8996: 8 * 2 * 2 * 4 = 128.
                                                                                   
      uint8  ca_dqs_retmr       [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];  //8996: 8 * 2 * 2 * 2 = 64.
      uint8  ca_coarse_dqs_delay[NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];  //8996: 8 * 2 * 2 * 2 = 64.
      uint8  ca_dqs_half_cycle  [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];  //8996: 8 * 2 * 2 * 2 = 64.
      uint8  ca_dqs_full_cycle  [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];  //8996: 8 * 2 * 2 * 2 = 64.
   } wrlvl; // 8996: Total = 896.

   struct
   {
      uint8 bimc_tDQSCK [MAX_NUM_CLOCK_PLAN][NUM_CH][NUM_CS][NUM_DQ_PCH];   // 8996: 15 * 2 * 2 * 4 = 240.
   } rcw; // 8996: Total = 240.

   struct
   {
      uint8 coarse_vref [VREF_MP_HP][NUM_CH][NUM_CS][NUM_DQ_PCH];           // 8996: 2 * 2 * 2 * 4 = 32.
      uint8 coarse_cdc  [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];       // 8996: 8 * 2 * 2 * 4 = 128.
      uint8 coarse_dcc  [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];       // 8996: 8 * 2 * 2 * 4 = 128.
      uint8 fine_cdc    [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];       // 8996: 8 * 2 * 2 * 4 = 128.
      uint8 perbit_delay[NUM_CH][NUM_CS][NUM_DQ_PCH][PINS_PER_PHY];         // 8996: 2 * 2 * 4 * 10 = 160.
      uint8 perbit_vref [NUM_CH][NUM_CS][NUM_DQ_PCH][PINS_PER_PHY];         // 8996: 2 * 2 * 4 * 10 = 160.
      uint8 rd_dcc      [NUM_CH][NUM_DQ_PCH];                               // 8996: 2 * 4 = 8.
      uint8 clk_dcc_mag  [NUM_CH][NUM_CA_PCH];                              // 8996: 2 * 2 = 4.      
      uint8 clk_dcc_sign [NUM_CH][NUM_CA_PCH];                              // 8996: 2 * 2 = 4.      
   } rd_dqdqs; // 8996: Total = 748 + 8 = 756.

   struct
   {
      uint8 coarse_vref[NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];                                // 8996: 8 * 2 * 2 * 4 = 128.
      uint8 coarse_cdc [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];                                // 8996: 8 * 2 * 2 * 4 = 128.
      uint8 fine_cdc   [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];                                // 8996: 8 * 2 * 2 * 4 = 128.
      uint8 coarse_best_eye_vref_value[NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_CA_PCH];                 // 8996: 8 * 2 * 2 * 2 = 64.
      uint8 coarse_max_eye_left_boundary_cdc_value[NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];     // 8996: 8 * 2 * 2 * 4 = 128.
      uint8 coarse_max_eye_right_boundary_cdc_value[NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];    // 8996: 8 * 2 * 2 * 4 = 128.
      uint8 dq_retmr      [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];                             // 8996: 8 * 2 * 2 * 4 = 128.
      uint8 dq_half_cycle [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];                             // 8996: 8 * 2 * 2 * 4 = 128.
      uint8 dq_full_cycle [NUM_PRFS_BANDS][NUM_CH][NUM_CS][NUM_DQ_PCH];                             // 8996: 8 * 2 * 2 * 4 = 128.
      uint8 perbit_delay[NUM_CH][NUM_CS][NUM_DQ_PCH][PINS_PER_PHY];                                 // 8996: 2 * 2 * 4 * 10 = 160.
   } wr_dqdqs; // 8996: Total = 1248.

};

typedef struct
{
   struct training_results results; // 8996 : Total = 3794 uint8s.
} training_data;

void DDRSS_set_training_params (training_params_t *training_params_ptr);


/***************************************************************************************************
 CA Vref training
 ***************************************************************************************************/

extern uint8 ca_training_pattern_lpddr3[CA_PATTERN_NUM][6];
extern uint8 ca_training_pattern_lpddr4[CA_PATTERN_NUM][3];


typedef struct
{
  uint8 best_cdc_value;
  uint8 best_fine_cdc_value;
  uint8 best_vref_value;
  uint16 best_dcc_value; 
  uint8 max_eye_right_boundary_cdc_value;
  uint8 max_eye_left_boundary_cdc_value;
  uint8 bottom_max_eye_vref_value;
  uint8 top_max_eye_vref_value;
  uint8 vref_all_fail_flag;
} best_eye_struct;

typedef struct
{
  uint8 vertical_best_cdc_value;
  uint8 vertical_best_vref_value;
  uint8 max_eye_top_boundary_vref_value;
  uint8 max_eye_bottom_boundary_vref_value;
  uint8 vertical_cdc_all_fail_flag;
} vertical_best_eye_struct;

typedef struct
{
  uint8 ca_coarse_vref;  // Coarse vref of SMALLER vref CA
  uint8 ca_vref_diff;    // Difference value of two CA vref
  uint8 ca_vref_flag;    // Flag to identify the CA with a SMALLER vref
} ca_vref_diff_struct;

typedef struct {
       uint8 coarse_fail_count_table       [NUM_CA_PCH][COARSE_VREF][COARSE_CDC];  //8996: 2 * 56 * 50 = 5600B.
       uint8 left_fine_fail_count_table    [NUM_CA_PCH][PINS_PER_PHY][FINE_VREF][FINE_CDC];  //8996: 2 * 10 * 9 * 9 = 1620B.
       uint8 right_fine_fail_count_table   [NUM_CA_PCH][PINS_PER_PHY][FINE_VREF][FINE_CDC];  //8996: 1620B.
       uint8 left_boundary_fine_cdc_value  [NUM_CA_PCH][PINS_PER_PHY][FINE_CDC];  //8996: 2 * 10 * 9 = 180B.
       uint8 right_boundary_fine_cdc_value [NUM_CA_PCH][PINS_PER_PHY][FINE_CDC];  //8996: 180B.

} ddrss_ca_vref_local_vars; //8996 total size: 9200B = 9.2KB.


/***************************************************************************************************
 Write Leveling
 ***************************************************************************************************/
// Structure for wrlvl convert routine
typedef struct {
  uint32 dqs_retmr;
  uint32 coarse_dqs_delay;
  uint32 fine_dqs_delay;
  uint32 dqs_full_cycle;
  uint32 dqs_half_cycle;
} wrlvl_params_struct;


/***************************************************************************************************
 Write and Read training
 ***************************************************************************************************/
// Structure containing what would otherwise be local variables used by the ddrss_rd_dqdqs(), ddrss_wr_dqdqs() functions and its callees.
typedef struct {
    struct {
        uint8 fine_left_boundary_cdc_value[NUM_DQ_PCH][PINS_PER_PHY][FINE_VREF];  //8996: 4 * 10 * 9 = 360B.
        uint8 fine_right_boundary_cdc_value[NUM_DQ_PCH][PINS_PER_PHY][FINE_VREF]; //8996: 360B.
    } ddrss_rdwr_fine_cdc_boundary;

    struct {
        uint8 coarse_dq_passband_info[NUM_DQ_PCH][COARSE_VREF][COARSE_CDC];                     //8996: 4 * 56 * 25 = 5600B.
        uint8 coarse_dq_half_cycle[NUM_DQ_PCH][COARSE_VREF];                                    //8996: 4 * 56  = 224B.
        uint8 coarse_dq_full_cycle[NUM_DQ_PCH][COARSE_VREF];                                    //8996: 4 * 56  = 224B.
        uint8 coarse_dq_dcc_info[NUM_DQ_PCH][NUM_CS][RX_DCC];                                   //8996: 4 * 2 * 10 = 80B. 
        uint8 coarse_dq_clk_dcc_info[NUM_DQ_PCH][NUM_CS][CLK_DCC_OFFSET_RANGE][2][COARSE_CDC * FINE_STEPS_PER_COARSE];   //8996: 4 * 2 * 9 * 2 * (25 * 7) = 25200B.
        uint8 dcc_width[NUM_DQ_PCH][NUM_CS][RX_DCC];                                            //8996: 4 * 2 * 10 = 80B.
        uint8 best_dcc[NUM_DQ_PCH][NUM_CS];                                                     //8996: 4 * 2 = 8B.
        uint8 rd_dcc_ctl[NUM_DQ_PCH][NUM_CS][RX_DCC];                                           //8996: 4 * 2 * 10 = 80B.
        uint8 clk_dcc_width[NUM_DQ_PCH][NUM_CS][CLK_DCC_OFFSET_RANGE][PHASE];                   //8996: 4 * 2 * 9 * 2 = 144B.
        uint8 clk_dq_dcd[NUM_DQ_PCH][NUM_CS][CLK_DCC_OFFSET_RANGE];                             //8996: 4 * 2 * 9 = 72B.
        uint8 coarse_fail_count_table[NUM_DQ_PCH][COARSE_VREF];                                 //8996: 4 * 56 = 224B.
        uint8 coarse_dq_1D_passband_info[NUM_DQ_PCH][COARSE_CDC];                               //8996: 4 * 25 = 100B.
    } coarse_schmoo;

    struct {
        uint8 fine_dq_passband_info_left[NUM_DQ_PCH][PINS_PER_PHY][FINE_VREF][FINE_CDC];        //8996: 4 * 10 * 9 * 9 = 3240B.
        uint8 fine_dq_passband_info_right[NUM_DQ_PCH][PINS_PER_PHY][FINE_VREF][FINE_CDC];       //8996: 3240B.
        uint8 fine_rd_dq_passband_info_left[NUM_DQ_PCH][PINS_PER_PHY][FINE_VREF][FINE_RD_CDC];  //8996: 4 * 10 * 9 * 9 = 3240B.
        uint8 fine_rd_dq_passband_info_right[NUM_DQ_PCH][PINS_PER_PHY][FINE_VREF][FINE_RD_CDC]; //8996: 3240B.
        uint8 fine_dq_1D_passband_info[NUM_DQ_PCH][2 * FINE_CDC];                               //8996: 4 * 2 * 9 = 72B.
    } fine_schmoo;

} ddrss_rdwr_dqdqs_local_vars; // 8996 total size: ~45KB.

typedef struct
{
  uint8 dbi_flag;  // Coarse vref of SMALLER vref CA
  uint8 dq_pin_num_max;    // Difference value of two CA vref
  uint8 *connected_bit_mapping_A;
} dbi_struct;

/***************************************************************************************************
 Periodic Training
 ***************************************************************************************************/
#define REG_OFFSET_DTTS_SRAM(uint8)     ((uint8 == 0) ? \
                                        BIMC_BASE + SEQ_BIMC_BIMC_S_DDR0_DTTS_SRAM_OFFSET  : \
                                        BIMC_BASE + SEQ_BIMC_BIMC_S_DDR1_DTTS_SRAM_OFFSET)


#endif /* __DDRSS_TRAINING_H__ */
