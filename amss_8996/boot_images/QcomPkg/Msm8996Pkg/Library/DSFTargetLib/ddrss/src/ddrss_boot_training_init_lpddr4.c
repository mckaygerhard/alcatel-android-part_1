/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DSFTargetLib/ddrss/src/ddrss_boot_training_init_lpddr4.c#10 $
$DateTime: 2016/01/27 11:17:44 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/08/14   jeasley      Change wrlvl to zero based channel and chip select.
                        Moved DRAM routines from wrlvl.
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/
#include "ddrss.h"
#include "target_config.h"

// Needed for the perbit Restore sequence.
extern uint8 dq_dbi_bit;
extern uint8 dq_spare_bit;
extern uint8 connected_bit_mapping_CA [PINS_PER_PHY_CONNECTED_CA];


boolean HAL_DDR_Boot_Training_Init(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{

    // Executes training and returns TRUE if called with DDR_TRAINING_MODE_INIT
    DDRSS_boot_training_lpddr4 (ddr, channel, chip_select);
    ddr->flash_params.version = TARGET_DDR_SYSTEM_FIRMWARE_VERSION;
    
    // Post Boot Training setup.
    DDRSS_Post_Boot_Training(ddr, channel, chip_select);
    
    return TRUE; 
}


//================================================================================================//
// DDRSS Boot Training
//================================================================================================//

boolean DDRSS_boot_training_lpddr4 (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
    uint8  byte_lane         = 0;
    uint8  MR13_value        = 0;
    uint8  fsp_op            = 0;
    uint8  fsp_wr            = 0;
    uint8  high_current_mode = 1;
    uint8  ch                = 0;
    uint8  cs                = 0;
    uint32 dq0_ddr_phy_base  = 0;
    uint32 reg_offset_dpe    = 0;

    uint32 temp_MPVref_bit[NUM_DQ_PCH] = {0};
    ddr_training_args tArgs;
    
    DDR_CHIPSELECT qualified_cs = (DDR_CHIPSELECT)(chip_select & ddr->cdt_params[0].common.populated_chipselect);
    
#if  DSF_WRLVL_TRAINING_EN
    uint8   RL_WL_idx  = 0;
    uint32  MR2_wrlvl_opcode    = 0;
    uint8   wrlvl_clk_freq_idx  = 0;
    uint32  wrlvl_clk_freq_khz  = 0;
    uint32  training_freq_to_prfs_table_map[MAX_TRAINING_FREQ_NUM] = {0};
#endif

#if DSF_RCW_TRAINING_EN
    uint32 rcw_training_ddr_freq_index = 0;
    uint8 rcw_start_clk_idx = 0;
    uint8 rcw_clk_idx = 0;
#endif

#if DSF_CA_VREF_TRAINING_EN
    uint8   ca_training_MR12_vref [NUM_CA_PCH][NUM_CS];
    boolean ca_vref_fail_flag = FALSE;
    
    uint32 reg_offset_ddr_phy = 0; 
#endif

#if DSF_PERIODIC_TRAINING_EN    
    uint8   dit_ave                                =  0;
    uint8   die                                    =  0;
#endif

    uint8 low_speed_clk_idx = 0;
    uint8  training_ddr_freq_indx = 0;
    uint8  ddr_freq_indx = 0;
    uint32 PRFS_BAND_THRESHOLD[NUM_PRFS_BANDS]  = {F_RANGE_0, F_RANGE_1, F_RANGE_2, F_RANGE_3, F_RANGE_4, F_RANGE_5, F_RANGE_6, F_RANGE_7};
    uint32 training_freq_table[MAX_TRAINING_FREQ_NUM] = {0};
    uint8  prfs_index = TRAINING_START_PRFS; 
    uint8  training_table_index = 0; 
    uint8  min_training_prfs_index = 0xFF; 
    static training_params_t training_params;
    training_params_t *training_params_ptr;
    
#if DSF_WR_DQDQS_TRAINING_TDQS2DQ_CAL_EN
    uint32 temp_rd_cdc_value[NUM_DQ_PCH] = {0};
#endif

#if (DSF_WR_DQDQS_TRAINING_TDQS2DQ_CAL_EN || DSF_PERIODIC_TRAINING_EN)
    uint32 dit_count[NUM_CH][NUM_CS][NUM_DIE_PCH] = {{{0}}};
#endif
    // Set aside an area to be used by training algorithm for computation local_vars area.
    // This area is declared as a static local array so as to get it into the ZI data section
    // of the executable. Training algorithms will receive a pointer to this area, and can
    // use it as a storage for what would otherwise have been local variables, consuming
    // huge amounts of stack space.
    static   uint8   *local_vars;
    boolean bResult;
    uint32 local_var_size = LOCAL_VARS_AREA_SIZE;
    
#if (DSF_CA_VREF_TRAINING_EN || DSF_PERIODIC_TRAINING_EN)
     // Training data structure pointer
    training_data *training_data_ptr;   
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);   
#endif    

    bResult = ddr_external_get_buffer((void **)&local_vars, &local_var_size);
    
    if (bResult != TRUE)
    {
         return bResult;
    }
    // Training params structure pointer
    training_params_ptr = &training_params;
    
//================================================================================================//
// Pre training setup
//================================================================================================//
    // Set all training parameters to recommended values
    DDRSS_set_training_params(training_params_ptr);
    DDRSS_Get_Training_Address(ddr);
    
#if DSF_PERIODIC_TRAINING_EN    
    training_data_ptr->results.dit.dit_fine_step_limit  = DIT_FINE_STEP_LIMIT;
    training_data_ptr->results.dit.dit_interval_per_sec = DIT_INTERVAL_PER_SEC;
#endif

    //search through frequency table to automatically identify training frequency
    for (training_ddr_freq_indx = 0; training_ddr_freq_indx < ddr->misc.ddr_num_clock_levels; training_ddr_freq_indx++)
    { 
        if ((ddr->misc.clock_plan[training_ddr_freq_indx].clk_freq_in_khz) > PRFS_BAND_THRESHOLD[prfs_index])
        {
            if ((ddr->misc.clock_plan[training_ddr_freq_indx-1].clk_freq_in_khz) > PRFS_BAND_THRESHOLD[prfs_index - 1]) //make sure we have a frequency in this band
            {
                training_freq_table[training_table_index] = ddr->misc.clock_plan[training_ddr_freq_indx -1].clk_freq_in_khz;
                
                if (min_training_prfs_index > prfs_index)
                {
                    min_training_prfs_index = prfs_index;
                }
#if DSF_RCW_TRAINING_EN
                if (prfs_index == TRAINING_START_PRFS) 
                {
                    rcw_training_ddr_freq_index = training_table_index + 1;   //1056000
                }
#endif
#if  DSF_WRLVL_TRAINING_EN
                training_freq_to_prfs_table_map[training_table_index] = prfs_index;
#endif
                training_table_index++;
                prfs_index++;
            }
            else
            {
                prfs_index++;  //don't advance training table index
            }
        }
        if (training_ddr_freq_indx == ddr->misc.ddr_num_clock_levels-1) 
        {
            training_freq_table[training_table_index] = ddr->misc.clock_plan[training_ddr_freq_indx].clk_freq_in_khz;
#if  DSF_WRLVL_TRAINING_EN
                training_freq_to_prfs_table_map[training_table_index] = prfs_index;
#endif
        }
    }


#if DSF_PERIODIC_TRAINING_EN
    if( prfs_index >= MIN_DTTS_TRACKING_PRFS)  //> 1555Mhz
    {
        training_data_ptr->results.dit.dit_acq_ndx  = (training_ddr_freq_indx -2) ;  //for now, acquiring index is ONE less than tracking index
        training_data_ptr->results.dit.dit_trac_ndx = training_ddr_freq_indx-1;
    }
    else //no periodic training required
    {
        training_data_ptr->results.dit.dit_acq_ndx  = 255 ;  //for now, acquiring index is ONE less than tracking index
        training_data_ptr->results.dit.dit_trac_ndx = 255;
    }
#endif

    // Disable all periodic functions: auto refresh, hw self refresh, periodic ZQCAL, periodic SRR
    BIMC_All_Periodic_Ctrl (ddr, channel, DDR_CS_BOTH, FEATURE_DISABLE/*0 for disable*/);

    for (ch = 0; ch < NUM_CH; ch++)
    {
        reg_offset_dpe = REG_OFFSET_DPE(ch);
        if ((channel >> ch) & 0x1)
        {
            // Disable Power Down
            HWIO_OUTXF2 (reg_offset_dpe, DPE_PWR_CTRL_0, PWR_DN_EN, CLK_STOP_PWR_DN_EN, 0x0, 0x0);
            
            HWIO_OUTXF  (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);
        }
    }
     
    //Determine highest band0 frequency as low speed frequency
    for (low_speed_clk_idx = 0; low_speed_clk_idx < ddr->misc.ddr_num_clock_levels; low_speed_clk_idx++)
    { 
        if ( F_RANGE_0 < ddr->misc.clock_plan[low_speed_clk_idx].clk_freq_in_khz)
        {
            low_speed_clk_idx--;
            break;
        }
    }
        
    //Start RCW training at beginning of training sequence

    //turn off io_qualifer before RCW training
    for (ch = 0; ch < 2; ch++)
    {
        dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;       
        if ((channel >> ch) & 0x1)
        {
            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
            {
                HWIO_OUTXF  ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_4_CFG, LPDDR4_MODE, 0);
            }                   
        }      
    }
        
#if DSF_RCW_TRAINING_EN
        // Switch to rcw_training_ddr_freq to do RCW training only
        // PLL0 -> PLL1.
        ddr_external_set_clk_speed (training_freq_table[rcw_training_ddr_freq_index]);
        // PLL1 -> PLL0.
        ddr_external_set_clk_speed (training_freq_table[rcw_training_ddr_freq_index]);

        // Get first band1 clk index in the clock plan
        for (rcw_start_clk_idx = 0; rcw_start_clk_idx < ddr->misc.ddr_num_clock_levels; rcw_start_clk_idx++)
        { 
            if ( F_RANGE_3 < ddr->misc.clock_plan[rcw_start_clk_idx].clk_freq_in_khz)
            break;            
        }
        
        // Get 1056MHz clk index in the clock plan
        for (rcw_clk_idx = 0; rcw_clk_idx < ddr->misc.ddr_num_clock_levels; rcw_clk_idx++)
        { 
            if (training_freq_table[rcw_training_ddr_freq_index] == ddr->misc.clock_plan[rcw_clk_idx].clk_freq_in_khz)
            break;
        }
     
        for (ch = 0; ch < NUM_CH; ch++)
        {
            if ((channel >> ch) & 0x1)
            {
                for(cs = 0; cs < NUM_CS; cs++)
                {
                    if((qualified_cs >> cs) & 0x1)
                    {  
                        ddr_printf(DDR_NORMAL, "START: RCW training on Ch: %u Rank: %u\n", ch, cs);
                        DDRSS_rcw (ddr, ch, cs, training_params_ptr, rcw_start_clk_idx, rcw_clk_idx);
                        ddr_printf(DDR_NORMAL, "END: RCW training on Ch: %u Rank: %u\n", ch, cs);
                    }
                }
            }
        }
#endif  // DSF_RCW_TRAINING_EN

    //turn on io_qualifer after RCW training
    for (ch = 0; ch < 2; ch++)
    {
        dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
       
        if ((channel >> ch) & 0x1)
        {
            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
            {
                HWIO_OUTXF  ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_4_CFG, LPDDR4_MODE, 1);
            }                   
        }      
    }
    
    // Train at each training frequency in the training_freq_table
    training_ddr_freq_indx = training_table_index + 1;
    for (ddr_freq_indx = 0;ddr_freq_indx < training_table_index +1; ddr_freq_indx++) 
    {
        training_ddr_freq_indx--;
        ddr_printf(DDR_NORMAL, "START: DDR training at freq: %u\n", training_freq_table[training_ddr_freq_indx]);

        // Switch to PLL 1 for all training below 1804 MHz
        if (ddr_freq_indx == 1)
        {
            ddr_external_set_clk_speed (training_freq_table[training_table_index]);
        }

        // Train single rank only for 1066MHz and 800MHz frequencies
        if ((training_freq_table[training_ddr_freq_indx] == training_freq_table[0]) || (training_freq_table[training_ddr_freq_indx] == training_freq_table[1]))
        {
            chip_select = DDR_CS0;
        }
        else
        {
            chip_select = qualified_cs;
        }
        
        // Turn on MP_VREF.
        for (ch = 0; ch < NUM_CH; ch++)
        {
            dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
            if((channel >> ch) & 0x1)
            {
                for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
                {
                    temp_MPVref_bit[byte_lane] = 0;
                    if(training_ddr_freq_indx == 1)  //1017MHz.
                    { 
                        temp_MPVref_bit[byte_lane] = HWIO_INX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG);
                        temp_MPVref_bit[byte_lane] = (temp_MPVref_bit[byte_lane] | (1 << 21));
                        HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG, temp_MPVref_bit[byte_lane]);
                    }
                    if(training_ddr_freq_indx == 2)  //1353MHz.
                    { 
                        temp_MPVref_bit[byte_lane] = HWIO_INX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG);
                        temp_MPVref_bit[byte_lane] = (temp_MPVref_bit[byte_lane] | (1 << 21));
                        HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG, temp_MPVref_bit[byte_lane]);
                    }
                    if(training_ddr_freq_indx == 3)  //1555MHz.
                    { 
                        temp_MPVref_bit[byte_lane] = HWIO_INX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG);
                        temp_MPVref_bit[byte_lane] = (temp_MPVref_bit[byte_lane] | (1 << 21));
                        HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG, temp_MPVref_bit[byte_lane]);
                    }
                    if(training_ddr_freq_indx == 4)  //1824MHz.
                    { 
                        temp_MPVref_bit[byte_lane] = HWIO_INX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG);
                        temp_MPVref_bit[byte_lane] = (temp_MPVref_bit[byte_lane] | (1 << 21));
                        HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG, temp_MPVref_bit[byte_lane]);
                    }
                }
            }
        }
        

#if DSF_PASS_1_DCC_TRAINING_EN 
//================================================================================================//
// DCC
//================================================================================================//   
        if (training_ddr_freq_indx == training_table_index)  //do DCC training at 1866MHz first
        {
            ddr_printf(DDR_NORMAL, "START: DCC training PASS 1.  \n");
            // PLL0 -> PLL1.
            ddr_external_set_clk_speed (training_freq_table[training_table_index]);
            // PLL1 -> PLL0. 
            ddr_external_set_clk_speed (training_freq_table[training_table_index]);

            // DCC Training for WRLVL CDC, WR90 CDC and IO DCC at max freq
            DDRSS_dcc_boot (ddr, channel, DCC_TRAINING_WRLVL_WR90_IO, training_freq_table[training_table_index] );  // do DCC training only at highest freq.
            
        }
#endif// DSF_PASS_1_DCC_TRAINING_EN

   
#if DSF_CA_VREF_TRAINING_EN
//================================================================================================//
// CA-Vref
//================================================================================================//
        if (training_ddr_freq_indx == training_table_index)
        {
        ddr_printf(DDR_NORMAL, "START: CA training.  \n");
        // Switch to boot freq to do CA Vref training 
        // PLL0 -> PLL1.        
        ddr_external_set_clk_speed (ddr->misc.clock_plan[low_speed_clk_idx].clk_freq_in_khz);
    
        // Command Bus Training - CBT (CA, CS and Vref)
        for (ch = 0; ch < NUM_CH; ch++)
        {
            reg_offset_ddr_phy = REG_OFFSET_DDR_PHY_CH (ch);
            
                if ((channel >> ch) & 0x1)
                {             
                    // PHY CA0 training entry
                    DDR_PHY_hal_cfg_ca_vref_dq_out (reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET);
                    DDR_PHY_hal_cfg_ca_vref_dq_in  (reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET);
                    //DDR_PHY_hal_cfg_ca_vref_ca     (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET); 
                
                    // PHY CA1 training entry
                    DDR_PHY_hal_cfg_ca_vref_dq_out (reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET);
                    DDR_PHY_hal_cfg_ca_vref_dq_in  (reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET);
                    //DDR_PHY_hal_cfg_ca_vref_ca     (reg_offset_ddr_phy + CA1_DDR_PHY_OFFSET);
                
                    // Only train Rank0
                    cs = 0;                       
                    // MC Set the DRAM to CA training mode. CA training mode enter with MR13[7:0]=000x10x1
                    // MC set the CKE to low to enter into CBT mode, CKE high to Low: FSP automatically change to 1
                    BIMC_CA_Training_Entry (ch, CS_1HOT(cs), FSP_OP_BOOT/*FSP_OP*/);
                           
                    // Disable DPE and SHKE so that in freq switch, MR write will not happen
                    HWIO_OUTXI (REG_OFFSET_GLOBAL0, BIMC_DDR_CHN_HFSC_STAGE1_HANDSHAKE_CTRL, ch, 0xE);
                    HWIO_OUTXI (REG_OFFSET_GLOBAL0, BIMC_DDR_CHN_HFSC_STAGE2_HANDSHAKE_CTRL, ch, 0x6);
                                          
                    // Change to target high frequency after tFC (200ns)
                    // PLL0 -> PLL1.
                    ddr_external_set_clk_speed (training_freq_table[training_ddr_freq_indx]);
                                    
                    ca_vref_fail_flag = DDRSS_ca_vref_lpddr4 (ddr,
                                                       ch,
                                                       cs,
                                                       prfs_index,
                                                       training_params_ptr,
                                                       (ddrss_ca_vref_local_vars *)local_vars,
                                                       training_freq_table[training_ddr_freq_indx]
                                                      );
                    
                    BIMC_CA_Training_Exit_CKE_ON (ch, CS_1HOT(cs));
                    
                    // PLL0 -> PLL1.
                    ddr_external_set_clk_speed (ddr->misc.clock_plan[low_speed_clk_idx].clk_freq_in_khz);
                    // Disable DPE and SHKE so that in freq switch, MR write will not happen
                    HWIO_OUTXI (REG_OFFSET_GLOBAL0, BIMC_DDR_CHN_HFSC_STAGE1_HANDSHAKE_CTRL, ch, 0xF);
                    HWIO_OUTXI (REG_OFFSET_GLOBAL0, BIMC_DDR_CHN_HFSC_STAGE2_HANDSHAKE_CTRL, ch, 0x7);
                
                    BIMC_CA_Training_Exit_MR (ch, CS_1HOT(cs), FSP_OP_BOOT/*FSP_OP*/);
                    tArgs.currentChannel = ch;
                    tArgs.currentChipSelect = cs;
                    tArgs.trainingType = DDR_TRAINING_CACK;
                    tArgs.currentFreqIndex = training_ddr_freq_indx+TRAINING_START_PRFS;
                    tArgs.FreqInKhz = training_freq_table[training_ddr_freq_indx];

                    ddr_send_data_to_ddi(local_vars, LOCAL_VARS_AREA_SIZE, &tArgs, (void*)training_params_ptr);
                    // Store local vars in flash and zero-initialize for use by the next training function.
                    ddr_external_page_to_storage(local_vars, LOCAL_VARS_AREA_SIZE);
                    
                    if (ca_vref_fail_flag)
                    {
                       // CA Vref Fine training is enabled, write fine results to MR12
                       if ( training_params_ptr->ca_vref.fine_training_enable == 1 )
                       {
                            if (training_data_ptr->results.ca_vref.fine_vref[prfs_index][ch][cs][0]<=50)
                            {
                                ca_training_MR12_vref[0][cs] = (MR12_VREF_RANGE_BIT << 6) | 
                                                                         training_data_ptr->results.ca_vref.fine_vref[prfs_index][ch][cs][0];
                            }
                            else 
                            {
                                ca_training_MR12_vref[0][cs] = ((MR12_VREF_RANGE_BIT+1) << 6) | 
                                                                (training_data_ptr->results.ca_vref.fine_vref[prfs_index][ch][cs][0]-CA_VREF_RANGE_THRESHOLD);
                            }
                            if (training_data_ptr->results.ca_vref.fine_vref[prfs_index][ch][cs][1]<=50)
                            {
                                ca_training_MR12_vref[1][cs] = (MR12_VREF_RANGE_BIT << 6) | 
                                                                training_data_ptr->results.ca_vref.fine_vref[prfs_index][ch][cs][1];
                            }
                            else 
                            {
                                ca_training_MR12_vref[1][cs] = ((MR12_VREF_RANGE_BIT+1) << 6) | 
                                                               (training_data_ptr->results.ca_vref.fine_vref[prfs_index][ch][cs][1]-CA_VREF_RANGE_THRESHOLD);
                            }
                       }
                       // CA Vref Fine training is disabled, write coarse results to MR12
                       else
                       {
                            if (training_data_ptr->results.ca_vref.coarse_vref[prfs_index][ch][cs][0]<=50)
                            {
                                ca_training_MR12_vref[0][cs] = (MR12_VREF_RANGE_BIT << 6) | 
                                                                training_data_ptr->results.ca_vref.coarse_vref[prfs_index][ch][cs][0];
                            }
                            else 
                            {
                                ca_training_MR12_vref[0][cs] = ((MR12_VREF_RANGE_BIT+1) << 6) | 
                                                                (training_data_ptr->results.ca_vref.coarse_vref[prfs_index][ch][cs][0]-CA_VREF_RANGE_THRESHOLD);
                            }
                            if (training_data_ptr->results.ca_vref.coarse_vref[prfs_index][ch][cs][1]<=50)
                            {
                                ca_training_MR12_vref[1][cs] = (MR12_VREF_RANGE_BIT << 6) | 
                                                               training_data_ptr->results.ca_vref.coarse_vref[prfs_index][ch][cs][1];
                            }
                            else 
                            {
                                ca_training_MR12_vref[1][cs] = ((MR12_VREF_RANGE_BIT+1) << 6) | 
                                                                (training_data_ptr->results.ca_vref.coarse_vref[prfs_index][ch][cs][1]-CA_VREF_RANGE_THRESHOLD);
                            }
                       }
                    }
                
                    HWIO_OUTX   ((reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, 0);
                    HWIO_OUTXF2 ((reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_PAD_CNTL_0_CFG, PULL_N_DQS, PULL_DQS, 3 ,2);
                    HWIO_OUTX   ((reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, 0);
                    
                    HWIO_OUTX   ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, 0);
                    HWIO_OUTXF2 ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_PAD_CNTL_0_CFG, PULL_N_DQS, PULL_DQS, 3 ,2);
                    HWIO_OUTXF  ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_FPM_CNTRL_CFG, FPM_BYPASS, 0x1);
                    HWIO_OUTXF  ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, CA_TRAINING_MODE, 0x0);
                    HWIO_OUTXF  ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_FPM_CNTRL_CFG, FPM_BYPASS, 0x0);
                    HWIO_OUTX   ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, 0);
                    
                    HWIO_OUTX   ((reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, 0);
                    HWIO_OUTXF2 ((reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_PAD_CNTL_0_CFG, PULL_N_DQS, PULL_DQS, 3 ,2);
                    HWIO_OUTX   ((reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, 0);
                    
                    HWIO_OUTX   ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, 0);
                    HWIO_OUTXF2 ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_PAD_CNTL_0_CFG, PULL_N_DQS, PULL_DQS, 3 ,2);
                    HWIO_OUTXF  ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_FPM_CNTRL_CFG, FPM_BYPASS, 0x1);
                    HWIO_OUTXF  ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, CA_TRAINING_MODE, 0x0);
                    HWIO_OUTXF  ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_FPM_CNTRL_CFG, FPM_BYPASS, 0x0);
                    HWIO_OUTX   ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, 0);
             
                    // Enable DPE and SHKE so that in freq switch, MR write will not happen
                    HWIO_OUTXI (REG_OFFSET_GLOBAL0, BIMC_DDR_CHN_HFSC_STAGE1_HANDSHAKE_CTRL, ch, 0xF);
                    HWIO_OUTXI (REG_OFFSET_GLOBAL0, BIMC_DDR_CHN_HFSC_STAGE2_HANDSHAKE_CTRL, ch, 0x7);
                    
                    // Current R1 BIMC doesn't allow setting CA0 and CA1 to different Vref values, so disable PHY to work around
                    // But with the future R2 BIMC, we will set separate Vref values for CA0 and CA1 by BIMC CSR
                    DDRSS_MR_Write_per_die (ddr, ch, cs, JEDEC_MR_12, ca_training_MR12_vref[0][cs], ca_training_MR12_vref[1][cs]);
                    
                    // Write Rank0 Vref values to FSP1 of rank1 
                    MR13_value = ((0 << 7) | (1 << 6) | (0 << 5) | (0 << 3) | (0 << 2) | (0 << 0));	
                    BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(1), JEDEC_MR_13, MR13_value);
                    
                    DDRSS_MR_Write_per_die (ddr, ch, 1, JEDEC_MR_12, ca_training_MR12_vref[0][cs], ca_training_MR12_vref[1][cs]);
                    
                    MR13_value = ((0 << 7) | (0 << 6) | (0 << 5) | (0 << 3) | (0 << 2) | (0 << 0));
                    BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(cs), JEDEC_MR_13, MR13_value);
                    BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(1), JEDEC_MR_13, MR13_value);                
                }
            }  // End of ch loop
        
        // don't need to move DQS and DQ any more since we want to keep the same relationship
        // If we did CA training before, since we moved CLK by an additional half cycle, we need to move DQS and DQ a half cycle accordingly

        
        ddr_external_set_clk_speed (training_freq_table[training_ddr_freq_indx]);
        
        ddr_printf(DDR_NORMAL, "END: CA training.  \n");
        
#if DSF_PASS_2_DCC_TRAINING_EN    
            ddr_printf(DDR_NORMAL, "START: DCC training PASS 2.  \n");
            // Go to max freq to do DCC training
            // PLL0 -> PLL1.
            ddr_external_set_clk_speed (training_freq_table[training_ddr_freq_indx]);

            // DCC Training for WR90 CDC since CA training involves WR90 CDC adjustment
            DDRSS_dcc_boot (ddr, channel, DCC_TRAINING_WRLVL_WR90_IO, training_freq_table[training_ddr_freq_indx] );  

#endif // DSF_PASS_2_DCC_TRAINING_EN
   } 

#endif  // DSF_CA_VREF_TRAINING_EN


#if DSF_WR_DQDQS_TRAINING_TDQS2DQ_CAL_EN 
//================================================================================================//
// WR DQDQS Training for band0/1.
//================================================================================================//
        // Need to do only once at the beginning before any WR is done.
        if (training_ddr_freq_indx == training_table_index)
        {
            for (ch = 0; ch < NUM_CH; ch++)
            {
                dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
                
                if((channel >> ch) & 0x1)
                {
                    for(cs = 0; cs < NUM_CS; cs++)
                    {
                        if((chip_select >> cs) & 0x1)
                        {
                            // Ensure more setup margin.
                            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
                            {
                                // Store the original cdc value.
                                temp_rd_cdc_value[byte_lane] = HWIO_INX((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_CMCDCRD_CTL_CFG);
                                
                                DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                                            LOW_SPEED_RD_COARSE_CDC_VALUE, 
                                                            0x1,  /*coarse_cdc */ 
                                                            0x1,  /*hp_mode */
                                                            cs);   
                            }
                            
                            if(cs == 0)
                            {                            
                                ddr_printf(DDR_NORMAL, "START: DIT runtime calculation on Ch: %u Rank: %u\n", ch, cs);
                                DDRSS_DIT_Runtime(ddr, ch, cs, training_params_ptr,  dit_count,training_freq_table[training_ddr_freq_indx]);
                                ddr_printf(DDR_NORMAL, "END: DIT runtime calculation  Ch: %u Rank: %u\n", ch, cs);
                            
                                ddr_printf(DDR_NORMAL, "START: WR training for band0/1 on Ch: %u Rank: %u\n", ch, cs);
                                DDRSS_wr_dqdqs_lpddr4_tdqs2dq_cal (ddr, 
                                                                ch, 
                                                                cs, 
                                                                training_params_ptr, 
                                                                training_freq_table[training_table_index]);
                                ddr_printf(DDR_NORMAL, "END: WR training for band0/1 on Ch: %u Rank: %u\n", ch, cs);
                            }
                            
                            // Restore the original CDC value.
                            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
                            {
                                HWIO_OUTX((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_CMCDCRD_CTL_CFG, temp_rd_cdc_value[byte_lane]);
                            }
                        }
                    } // cs
                }
            } // ch
        }
#endif  // End of DSF_WR_DQDQS_TRAINING_TDQS2DQ_CAL_EN   


#if DSF_CLK_DCC_RD_DQDQS
//================================================================================================//
// CLK DCC training.
//================================================================================================//
        if (training_ddr_freq_indx == training_table_index)
        {
            ddr_printf(DDR_NORMAL, "START: CLK RD DCC training.  \n");
            // Switch to boot freq to do a low speed memory write to be used for read back during training
            ddr_external_set_clk_speed (ddr->misc.clock_plan[low_speed_clk_idx].clk_freq_in_khz);
        
            // Enable refresh so that memory contents are retained after low speed write
            BIMC_Auto_Refresh_Ctrl (channel, chip_select, FEATURE_ENABLE/*1 for enable*/);
            
            // Memory write at low speed
            for (ch = 0; ch < NUM_CH; ch++)
            {
                if ((channel >> ch) & 0x1)
                {
                    for (cs = 0; cs < NUM_CS; cs++)
                    {
                        if ((chip_select >> cs) & 0x1)
                        {
                            DDRSS_mem_write (ddr, ch, cs);
                        }
                    }
                }
            }

        // Switch to max freq to do dq-dqs training
        ddr_external_set_clk_speed (training_freq_table[training_ddr_freq_indx]);
        
            for (ch = 0; ch < NUM_CH; ch++)
            {
                if ((channel >> ch) & 0x1)
                {
                    for (cs = 0; cs < NUM_CS;cs++)
                    {
                        if ((chip_select >> cs) & 0x1)
                        {
                            ddr_printf(DDR_NORMAL, "  CLK DCC training on Ch: %u Rank: %u\n", ch, cs);
                            DDRSS_clk_dcc_rd_dqdqs_schmoo (ddr, ch, cs, training_params_ptr, (ddrss_rdwr_dqdqs_local_vars *)local_vars, training_freq_table[training_ddr_freq_indx]);	
                        }
                    }
                }
            }
        }
        
        BIMC_Auto_Refresh_Ctrl (channel, chip_select, FEATURE_DISABLE/*0 for disable*/);  
        
#endif // DSF_RD_DQDQS_DCC


#if DSF_WRLVL_TRAINING_EN
 //================================================================================================//
 // Write Leveling
 // This training needs to be done for NUM_TRAINING_FREQS starting from the max freq.
 //================================================================================================//
        if (training_ddr_freq_indx == training_table_index)
        {
            ddr_printf(DDR_NORMAL, "START: WRLVL training.  \n");
            
            wrlvl_clk_freq_khz = training_freq_table[training_ddr_freq_indx];
            wrlvl_clk_freq_idx = training_freq_to_prfs_table_map[training_ddr_freq_indx];
        
            RL_WL_idx  = BIMC_RL_WL_Freq_Index (ddr, wrlvl_clk_freq_khz);

            // Enable write leveling in the DRAM using MR2 write
            MR2_wrlvl_opcode = ((1 << 7) | ddr->extended_cdt_runtime.dram_latency[RL_WL_idx].MR2);
            // Train rank 0 and copy results to rank 1
//            BIMC_MR_Write (channel, DDR_CS0, JEDEC_MR_2, MR2_wrlvl_opcode);
        
            for (ch = 0; ch < NUM_CH; ch++)
            {
              if ((channel >> ch) & 0x1)
              {
                  for (cs = 0; cs <= 1; cs++)
                  {
                      if ((chip_select >> cs) & 0x1)
                      {
                          BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(cs), JEDEC_MR_2, MR2_wrlvl_opcode);
                      //  ddr_printf(DDR_NORMAL, "START: WRLVL training on Ch: %u Rank: %u\n", ch, cs);                        
                          DDRSS_wrlvl_ca (ddr, ch, cs, wrlvl_clk_freq_khz, training_params_ptr, wrlvl_clk_freq_idx);
                      //  ddr_printf(DDR_NORMAL, "END: WRLVL training on Ch: %u Rank: %u\n", ch, cs);
                          BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(cs), JEDEC_MR_2, (ddr->extended_cdt_runtime.dram_latency[RL_WL_idx].MR2));
                      }
                  }  //End of cs loop            
              
                  for (cs = 0; cs <= 1; cs++)
                  {
                      if ((chip_select >> cs) & 0x1)
                      {
                          BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(cs), JEDEC_MR_2, MR2_wrlvl_opcode);    
                      //  ddr_printf(DDR_NORMAL, "START: WRLVL training on Ch: %u Rank: %u\n", ch, cs);                        
                          DDRSS_wrlvl (ddr, ch, cs, wrlvl_clk_freq_khz, wrlvl_clk_freq_idx, training_params_ptr);
                          BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(cs), JEDEC_MR_2, (ddr->extended_cdt_runtime.dram_latency[RL_WL_idx].MR2));
                      //  ddr_printf(DDR_NORMAL, "END: WRLVL training on Ch: %u Rank: %u\n", ch, cs);
                      }
                  }  //End of cs loop
              }
            }
        
            // Disable write leveling in the DRAM using MR2 write
            //BIMC_MR_Write (channel,DDR_CS_BOTH,JEDEC_MR_2,BIMC_RL_WL_Table_Sel(ddr, MR2_WR_VAL, wrlvl_clk_freq_khz));
            ddr_printf(DDR_NORMAL, "END: WRLVL training.  \n");
            
    #if DSF_PASS_3_DCC_TRAINING_EN    
            ddr_printf(DDR_NORMAL, "START: DCC training PASS 3.  \n");

            // DCC Training for WRLVL CDC since write leveling involves WRLVL CDC adjustment
            DDRSS_dcc_boot (ddr, channel, DCC_TRAINING_WRLVL_WR90_IO, training_freq_table[training_ddr_freq_indx] );  
    
    #endif // DSF_PASS_3_DCC_TRAINING_EN
        }
#endif  // DSF_WRLVL_TRAINING_EN


//================================================================================================//
// Read Training (DQ-DQS and RCW)
//================================================================================================//
        // Switch to boot freq to do a low speed memory write to be used for read back during training
        // PLL0 -> PLL1. 
        ddr_external_set_clk_speed (ddr->misc.clock_plan[low_speed_clk_idx].clk_freq_in_khz);
        // PLL1 -> PLL0.
        ddr_external_set_clk_speed (ddr->misc.clock_plan[low_speed_clk_idx].clk_freq_in_khz);
    
        // Enable refresh so that memory contents are retained after low speed write
        BIMC_Auto_Refresh_Ctrl (channel, chip_select, FEATURE_ENABLE/*1 for enable*/);
         
        // Memory write at low speed
        for (ch = 0; ch < NUM_CH; ch++)
        {
            if ((channel >> ch) & 0x1)
            {
                for (cs = 0; cs < NUM_CS; cs++)
                {
                    if ((chip_select >> cs) & 0x1)
                    {
                        DDRSS_mem_write (ddr, ch, cs);
                    }
                }
            }
        }

        // Switch to max freq to do dq-dqs training
        // PLL0 -> PLL1. 
        ddr_external_set_clk_speed (training_freq_table[training_ddr_freq_indx]);
        // PLL1 -> PLL0.
        ddr_external_set_clk_speed (training_freq_table[training_ddr_freq_indx]);
           
#if DSF_RD_DQDQS_TRAINING_EN
        for (ch = 0; ch < NUM_CH; ch++)
        {
            if ((channel >> ch) & 0x1)
            {
#if DSF_RD_DQDQS_DCC 
//================================================================================================//
// Read_DCC at 1866MHz.
//================================================================================================//
                if (training_ddr_freq_indx == training_table_index)
                {   
                  for (cs = 0; cs < NUM_CS;cs++)
                  {
                      if ((chip_select >> cs) & 0x1)
                      {
                          ddr_printf(DDR_NORMAL, "  RD_DCC training on Ch: %u Rank: %u\n", ch, cs);
                          DDRSS_rd_dqdqs_dcc_schmoo (ddr, 
                                                     ch, 
                                                     cs, 
                                                     training_params_ptr, 
                                                     (ddrss_rdwr_dqdqs_local_vars *)local_vars, 
                                                     training_freq_table[training_ddr_freq_indx]);      
                      }
                  }
                }
#endif // DSF_RD_DQDQS_DCC

                for (cs = 0; cs < NUM_CS; cs++)
                {
                    if ((chip_select >> cs) & 0x1)
                    {
                        ddr_printf(DDR_NORMAL, "START: Read training on Ch: %u Rank: %u\n", ch, cs);
                        tArgs.currentChannel = ch;
                        tArgs.currentChipSelect = cs;
                        tArgs.trainingType = DDR_TRAINING_READ;
                        tArgs.currentFreqIndex = training_ddr_freq_indx+TRAINING_START_PRFS;
                        tArgs.FreqInKhz = training_freq_table[training_ddr_freq_indx];

                        DDRSS_rd_dqdqs_lpddr4  (ddr, 
                                                ch, 
                                                cs, 
                                                training_params_ptr, 
                                                (ddrss_rdwr_dqdqs_local_vars *)local_vars, 
                                                training_freq_table[training_ddr_freq_indx],
                                                prfs_index);
                        ddr_printf(DDR_NORMAL, "END: Read training on Ch: %u Rank: %u\n\n", ch, cs);
                        ddr_send_data_to_ddi(local_vars, LOCAL_VARS_AREA_SIZE, &tArgs, (void*)training_params_ptr);
        
                        // Store local vars in flash and zero-initialize for use by the next training function.
                        ddr_external_page_to_storage(local_vars, LOCAL_VARS_AREA_SIZE);
                    }
                }
            }
        }

#endif  // DSF_RD_DQDQS_TRAINING_EN

#if DSF_PERIODIC_TRAINING_EN
//================================================================================================//
// DIT Training
//================================================================================================//
       if ((training_ddr_freq_indx == training_table_index) && ( prfs_index >= MIN_DTTS_TRACKING_PRFS))
       {
            for (ch = 0; ch < NUM_CH; ch++)
            {
                if((channel >> ch) & 0x1)
                {
                    for(cs = 0; cs < NUM_CS; cs++)
                    {
                        if((chip_select >> cs) & 0x1)
                        {
                            ddr_printf(DDR_NORMAL, "START: DIT pre_wr_dqdqs training on Ch: %u Rank: %u\n", ch, cs);
                            DDRSS_DIT_Read (ddr, ch, cs, training_params_ptr, dit_ave, dit_count,training_freq_table[training_ddr_freq_indx]);
                            ddr_printf(DDR_NORMAL, "END: DIT pre_wr_dqdqs training on Ch: %u Rank: %u\n", ch, cs);
                        }
                    }
                }
            }
            dit_ave = 1;
            
        }
#endif  // End of DSF_PERIODIC_TRAINING_EN     

#if DSF_WR_DQDQS_TRAINING_EN
//================================================================================================//
// Write DQ-DQS Training
//================================================================================================//
    
        fsp_op = (training_freq_table[training_ddr_freq_indx] > ODT_FSP_EN_FREQ_THRESHOLD) ? 1 : 0 ;
        fsp_wr = (training_freq_table[training_ddr_freq_indx] > ODT_FSP_EN_FREQ_THRESHOLD) ? 1 : 0 ;
        
        for (ch = 0; ch < NUM_CH; ch++)
        {
            if((channel >> ch) & 0x1)
            {
                for(cs = 0; cs < NUM_CS; cs++)
                {
                    if((chip_select >> cs) & 0x1)
                    {
                        ddr_printf(DDR_NORMAL, "START: Write training on Ch: %u Rank: %u\n", ch, cs);
                        // Enter WR_Training. MR13, VRCG (bit 3) set to high-current mode for WR training.
                        high_current_mode = 1; 
                        MR13_value = (fsp_op << 7) | (fsp_wr << 6) | (0 << 5) | (high_current_mode << 3) | (0 << 2) | (0 << 0);
                        
                        // Satisfy tVRCG_ENABLE=200ns. ROUNDUP(200ns/52ns)=0x4
                        BIMC_Wait_Timer_Setup ((DDR_CHANNEL)CH_1HOT(ch), WAIT_XO_CLOCK, 0x04);
                        
                        BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(cs), JEDEC_MR_13, MR13_value);
                        
                        // Reset manual_1 timer
                        BIMC_Wait_Timer_Setup ((DDR_CHANNEL)CH_1HOT(ch), WAIT_XO_CLOCK, 0x00);
                        tArgs.currentChannel = ch;
                        tArgs.currentChipSelect = cs;
                        tArgs.trainingType = DDR_TRAINING_WRITE;
                        tArgs.currentFreqIndex = training_ddr_freq_indx+TRAINING_START_PRFS;
                        tArgs.FreqInKhz = training_freq_table[training_ddr_freq_indx];
                        
                        DDRSS_wr_dqdqs_lpddr4 (ddr, ch, cs, training_params_ptr, (ddrss_rdwr_dqdqs_local_vars *)local_vars, training_freq_table[training_ddr_freq_indx], prfs_index);
                        
                        // Exit WR Training. MR13. (VRCG, bit 3 = 0, normal current mode).
                        high_current_mode = 0;
                        MR13_value = (fsp_op << 7) | (fsp_wr << 6) | (0 << 5) | (high_current_mode << 3) | (0 << 2) | (0 << 0);
                        
                        // Satisfy tVRCG_DISABLE=100ns. ROUNDUP(100ns/52ns)=0x2
                        BIMC_Wait_Timer_Setup ((DDR_CHANNEL)CH_1HOT(ch), WAIT_XO_CLOCK, div_ceil( ( ddr->cdt_params[ch].lpddr.tVRCG_DISABLE * 100), XO_PERIOD_IN_PS));
    
                        BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(cs), JEDEC_MR_13, MR13_value);
                        
                        // Reset manual_1 timer
                        BIMC_Wait_Timer_Setup ((DDR_CHANNEL)CH_1HOT(ch), WAIT_XO_CLOCK, 0x00);
                        
                        ddr_printf(DDR_NORMAL, "END: Write training on Ch: %u Rank: %u\n", ch, cs);
                        ddr_send_data_to_ddi(local_vars, LOCAL_VARS_AREA_SIZE, &tArgs, (void*)training_params_ptr);
                        
                        // Store local vars in flash and zero-initialize for use by the next training function.
                        ddr_external_page_to_storage(local_vars, LOCAL_VARS_AREA_SIZE);
                    }
                }
            }
        }
        
#endif  // DSF_WR_DQDQS_TRAINING_EN


#if DSF_PERIODIC_TRAINING_EN
//================================================================================================//
// DIT Training
//================================================================================================//

        // Capture DIT for the highest two training frequencies
        if ((training_ddr_freq_indx == training_table_index) && ( prfs_index >= MIN_DTTS_TRACKING_PRFS))
        {
            // Write the DIT capture frequency in to the data structure
            training_data_ptr->results.dit.dit_training_freq = training_freq_table[training_ddr_freq_indx] ;
            
            for (ch = 0; ch < NUM_CH; ch++)
            {
                if((channel >> ch) & 0x1)
                {
                    for(cs = 0; cs < NUM_CS; cs++)
                    {
                        if((chip_select >> cs) & 0x1)
                        {                                                                    
                            ddr_printf(DDR_NORMAL, "START: DIT post_wr_dqdqs training on Ch: %u Rank: %u\n", ch, cs);
                            DDRSS_DIT_Read (ddr, ch, cs, training_params_ptr, dit_ave, dit_count,training_freq_table[training_ddr_freq_indx]);
                
                            // Store the DIT count in the data structure
                            for (die=0;die<=1;die++) 
                            {
                                training_data_ptr->results.dit.dit_count[ch][cs][die] = dit_count[ch][cs][die];
                            }
                
                            ddr_printf(DDR_NORMAL, "END: DIT post_wr_dqdqs training on Ch: %u Rank: %u\n", ch, cs);
                        }
                    }
                }
            }
        }
#endif  // End of DSF_PERIODIC_TRAINING_EN    

        // Disable Auto refresh at the beginning of training of each frequency
        BIMC_Auto_Refresh_Ctrl (channel, chip_select, FEATURE_DISABLE/*0 for disable*/);        

#if DSF_PASS_4_DCC_TRAINING_EN    
        ddr_printf(DDR_NORMAL, "START: DCC training PASS 4.  \n");
        // DCC Training for WR90 CDC and IO DCC since DQ-DQS training involves WR90 CDC and Pbit adjustment
        DDRSS_dcc_boot (ddr, channel, DCC_TRAINING_WRLVL_WR90_IO, training_freq_table[training_ddr_freq_indx] );  

#endif // DSF_PASS_4_DCC_TRAINING_EN


        // Put back the passed in chip_select saved at the beginning    
        chip_select = qualified_cs;   
        ddr_printf(DDR_NORMAL, " END:   DDR training at freq: %u\n", training_freq_table[training_ddr_freq_indx]);
        
//================================================================================================//        
        
        for (ch = 0; ch < NUM_CH; ch++)
        {
            dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
            
            if((channel >> ch) & 0x1)
            {
                for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
                {
                    // Restore the MPVref bit.
                    if(training_ddr_freq_indx == 1)  //1017MHz.
                    {            
                        temp_MPVref_bit[byte_lane] = HWIO_INX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG);
                        temp_MPVref_bit[byte_lane] = (temp_MPVref_bit[byte_lane] & 0xFFDFFFFF); // zero out bit 21.
                        HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG, temp_MPVref_bit[byte_lane]);
                    }
                    if(training_ddr_freq_indx == 2)  //1353MHz.
                    {            
                        temp_MPVref_bit[byte_lane] = HWIO_INX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG);
                        temp_MPVref_bit[byte_lane] = (temp_MPVref_bit[byte_lane] & 0xFFDFFFFF);
                        HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG, temp_MPVref_bit[byte_lane]);
                    }
                    if(training_ddr_freq_indx == 3)  //1555MHz.
                    {            
                        temp_MPVref_bit[byte_lane] = HWIO_INX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG);
                        temp_MPVref_bit[byte_lane] = (temp_MPVref_bit[byte_lane] & 0xFFDFFFFF);
                        HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG, temp_MPVref_bit[byte_lane]);
                    }
                    if(training_ddr_freq_indx == 4)  //1804MHz.
                    {            
                        temp_MPVref_bit[byte_lane] = HWIO_INX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG);
                        temp_MPVref_bit[byte_lane] = (temp_MPVref_bit[byte_lane] & 0xFFDFFFFF);
                        HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG, temp_MPVref_bit[byte_lane]);
                    }
                }
            }
        }
        // PLL0 -> PLL1. 
        ddr_external_set_clk_speed(training_freq_table[training_ddr_freq_indx]);
        // PLL1 -> PLL0.
        ddr_external_set_clk_speed(training_freq_table[training_ddr_freq_indx]);
        
    } // end of freq loop.
    
//================================================================================================//
// Post training cleanup
//================================================================================================//
    for (ch = 0; ch < NUM_CH; ch++)
    {
        reg_offset_dpe = REG_OFFSET_DPE(ch);
        dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;        
        
        if ((channel >> ch) & 0x1)
        {
            // Enable all periodic functions: auto refresh, hw self refresh, periodic ZQCAL, periodic SRR
            BIMC_All_Periodic_Ctrl (ddr, CH_1HOT(ch), (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect, FEATURE_ENABLE/*1 for enable*/);

            // Enable Power Down
            HWIO_OUTXF2 (reg_offset_dpe, DPE_PWR_CTRL_0, PWR_DN_EN, CLK_STOP_PWR_DN_EN, 0x1, 0x1);
            
            HWIO_OUTXF  (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);

            //Disable DM RX to save power       
            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
            {
                HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, 0x220);
                HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, 0x200);
                HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, 0x0);
            }    
        }
    }
    
    return TRUE;
    
}  // END of DDRSS_boot_training.


