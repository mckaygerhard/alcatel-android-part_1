/* Copyright (C) 2016 Tcl Corporation Limited */
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

GENERAL DESCRIPTION
  This module contains the authentication and some calls to the hashing
  functions used by device programmer.

Copyright (c) 2013-2016 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE


when         who   what, where, why
----------   ---   ----------------------------------------------------------
2016-03-09   wek   Fix feature define for Hashing.
2015-10-02   wek   Move SHA functions from security to a new file.
2014-10-02   ah    Major code clean up
2014-05-23   wek   Compile for 64 bit.
2013-06-03   ah    Added legal header
2013-05-31   ab    Initial checkin

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include <stdio.h>
#include <string.h>
//#include <basetypes.h>
//#include <basetypes_qcom.h>
//#include <memory.h>
#include <comdef.h>
#include "deviceprogrammer_security.h"
//#include "deviceprogrammer_firehose.h"
#include "stringl/stringl.h"

#include "boot_util.h"
//#include "boot_sbl_if.h"
#include "secboot_hw.h"
#include "secboot.h"
#include BOOT_PBL_H

#ifdef FEATURE_DEVPRG_SOFTWARE_HASH
static void PerformSHA256_i(uint8* inputPtr, uint32 inputLen, uint8* outputPtr)
{
  struct __sechsh_ctx_s   context;

  sechsharm_sha256_init  (&context);
  sechsharm_sha256_update(&context,
                        context.leftover,
                        &(context.leftover_size),
                        inputPtr,
                        inputLen);

  sechsharm_sha256_final (&context,
                        context.leftover,
                        &(context.leftover_size),
                        outputPtr);

}

#else /* No hardware SHA, use software. */
static void PerformSHA256_i(uint8* inputPtr, uint32 inputLen, uint8* outputPtr)
{
    CeMLCntxHandle*     cntx = NULL;
    CEMLIovecListType   ioVecIn;
    CEMLIovecListType   ioVecOut;
    CEMLIovecType       IovecIn;
    CEMLIovecType       IovecOut;
    uint32              digest_len = CEML_HASH_DIGEST_SIZE_SHA256;

    /* Input IOVEC */
    ioVecIn.size = 1;
    ioVecIn.iov = &IovecIn;
    ioVecIn.iov[0].dwLen = inputLen; //msg_len;
    ioVecIn.iov[0].pvBase = inputPtr; //msg;

    /* Output IOVEC */
    ioVecOut.size = 1;
    ioVecOut.iov = &IovecOut;
    ioVecOut.iov[0].dwLen = digest_len;
    ioVecOut.iov[0].pvBase = outputPtr; //result_digest;


    CeMLInit();
    CeMLHashInit(&cntx, CEML_HASH_ALGO_SHA256);
    CeMLHashUpdate(cntx, ioVecIn);

    CeMLHashFinal(cntx, &ioVecOut);

    CeMLHashDeInit(&cntx);

    CeMLDeInit();
}
#endif

void sbl1_PerformSHA256(uint8* inputPtr, uint32 inputLen, uint8* outputPtr)
{
  PerformSHA256_i(inputPtr, inputLen, outputPtr);
}


