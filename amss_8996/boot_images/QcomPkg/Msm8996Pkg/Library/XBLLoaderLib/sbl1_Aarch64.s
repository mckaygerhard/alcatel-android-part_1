//*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
//
//                              SBL1 Aarch64
//
// GENERAL DESCRIPTION
//   This file bootstraps the processor. The Start-up Primary Bootloader
//   (SBL1) performs the following functions:
//
//      - Set up the hardware to continue boot process.
//      - Initialize DDR memory
//      - Load Trust-Zone OS
//      - Load RPM firmware
//      - Load APPSBL and continue boot process
//
//   The SBL1 is written to perform the above functions with optimal speed.
//   It also attempts to minimize the execution time and hence reduce boot time.
//
// Copyright 2013 - 2016 by Qualcomm Technologies, Inc.
// All Rights Reserved.
//*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

//*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
//
//                           EDIT HISTORY FOR FILE
//
// This section contains comments describing changes made to the module.
// Notice that changes are listed in reverse chronological order.
//
//
// when       who     what, where, why
// --------   ---     --------------------------------------------------------
// 10/14/15   kedara  Update chip version detection logic
// 10/14/15   kedara  Mark stack region read-write for 8996 v2.
// 10/14/15   kedara  Zero init stack region.
// 07/09/15   kedara  Update sbl1_enable_serror.
// 02/12/15   ck      Removing sbl1_recover_from_reset as this was for CCI workaround
// 11/14/14   kedara  Unmask pending aborts.
// 08/20/14   kedara  Update Stack base address.
// 07/18/14   kedara  Update for llvm compilation, remove sysini for 8996.
// 05/22/14   plc     Add sbl1_recover_from_reset call before sysini
// 05/20/14   plc     Add a53_aarch64_sysini delivered from HW team
// 04/03/14   plc     Fix PBL shared-data pointer to reference 32-bit register
// 01/23/14   plc     Add Setup for VBAR, and update SP logic
// 01/07/14   plc     Adding initial bootstrap and loop_forever
// 11/01/13   plc     Initial revision for 64-bit Aarch64 sbl1
//*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


//============================================================================
//
//                            MODULE INCLUDES
//
//============================================================================
#include "boot_msm.h"
#include "boot_target.h"


//============================================================================
//
//                             MODULE DEFINES
//
//============================================================================
//
.set  Mode_SVC,  0x13
.set  Mode_ABT,  0x17
.set  Mode_UND,  0x1b
.set  Mode_USR,  0x10
.set  Mode_FIQ,  0x11
.set  Mode_IRQ,  0x12
.set  Mode_SYS,  0x1F

.set  I_Bit,  0x80
.set  F_Bit,  0x40
.set  SCR_EL3_EA_Bit,  0x08
.set  ISR_EL1_A_BIT,   0x100
.set  L2ESR1_CLEAR,    0x80000080
.set  L2ESR1_INDEX,    0x206

.set  L3_PAGE_ENTRY_AP_BIT_1,  0x80

#define L2CPUSRSELR_EL1 S3_3_c15_c0_6
#define L2CPUSRDR_EL1 S3_3_c15_c0_7

//============================================================================
//
//                             MODULE IMPORTS
//
//============================================================================

    // Import the external symbols that are referenced in this module.
  .extern a53_aarch64_sysini
  .extern sbl1_vector_table
  .extern sbl1_main_ctl
  .extern mmu_invalidate_tlb_el3


//============================================================================
//
//                             MODULE EXPORTS
//
//============================================================================

  // Export the external symbols that are referenced in this module.
  .global sbl_loop_here
 
  // Export the symbols __main and _main to prevent the linker from
  // including the standard runtime library and startup routine.
  .global   __main
  .global   _main
  .global sbl1_entry
  .global sbl1_disable_serror
  .global sbl1_enable_serror  
	
//============================================================================
//
//                             MODULE DATA AREA
//
//============================================================================


//============================================================================
// Qualcomm SECONDARY BOOT LOADER 1 ENTRY POINT
//============================================================================

    //AREA  SBL1_ENTRY, CODE, READONLY, ALIGN=4
    .section  SBL1_ENTRY, "ax", %progbits
    .p2align 4
    
    //CODE
    //ENTRY
    
__main:
_main:

//============================================================================
//   sbl1_entry
//============================================================================
sbl1_entry:
  // Save the passing parameter from PBL
  MOV w7, w0
  
  // Configure SBL1 vector table base for EL3 and EL1 VBARs (SBL1 contexts)
  // -------------------------------
  LDR x0, =sbl1_vector_table
  MSR VBAR_EL3, x0
  MSR VBAR_EL1, x0

  // Check for pending aborts
  // ------------------------------- 
  MRS X0, ISR_EL1
  AND X0, X0, #ISR_EL1_A_BIT
  CBNZ X0, boot_loop_here

  // Enable EA bit. Take external aborts, sError interrupt
  // -------------------------------  
  MRS X0, SCR_EL3
  ORR X0, X0, #SCR_EL3_EA_Bit
  MSR SCR_EL3, X0

  // Mark L2 TCM base read-write for chips < v3
  // -------------------------------
  // Change needed only for chips with version less than v3
  // as bootrom marks the region readonly till v2.x 
  // Workaround is to locate L2 TCM page table entry in mmu 
  // and modify its access permissions to read-write from
  // readonly ie change AP[1:0] bits to 0x00, 
  
  //if chip > v2 jump to skip_v2_stack_rw_update
  //check TCSR_SOC_HW_VERSION for revision
  MOV  x0, #HWIO_TCSR_SOC_HW_VERSION_ADDR & 0xffff
  MOVK  x0, #HWIO_TCSR_SOC_HW_VERSION_ADDR >> 16, LSL #16
  LDR  x0, [x0]
  AND x2, x0, #HWIO_TCSR_SOC_HW_VERSION_MAJOR_VERSION_BMSK
  LSR x2, x2, #HWIO_TCSR_SOC_HW_VERSION_MAJOR_VERSION_SHFT
  CMP x2, #3
  BGE skip_v2_stack_rw_update
  
  //Also check TCSR_SOC_HW_VERSION for device number
  AND x2, x0, #HWIO_TCSR_SOC_HW_VERSION_DEVICE_NUMBER_BMSK
  LSR x2, x2, #HWIO_TCSR_SOC_HW_VERSION_DEVICE_NUMBER_SHFT
  CMP x2, #4
  BGE skip_v2_stack_rw_update  
  
  // point to first 4k L3 page table entry (0x00201000). 
  // Mark total 4 entries ie 16kb as read-writeable
  MOV  x0, #0x1000
  MOVK x0, #0x0020 , LSL #16
  MOV  x2, #0                      // used as counter
  
page_table_entry_updt:  
  LDR  x1, [x0]
  BIC  x1, x1, #L3_PAGE_ENTRY_AP_BIT_1
  STR  x1, [x0]
  
  ADD  x0, x0, #0x08
  ADD  x2, x2, #0x01
  CMP  x2, #0x4
  BNE  page_table_entry_updt  
  
  BL mmu_invalidate_tlb_el3
skip_v2_stack_rw_update:
  
  // retrieve stack-base
  // -------------------------------
  MOV x0,  SBL1_STACK_BASE & 0xffff
  MOVK x0, SBL1_STACK_BASE >> 16, LSL #16

  // configure stack for SBL (EL3)
  // -------------------------------
  MOV SP, x0

  // Zero init stack region
  // -------------------------------    
  MOV  x0,  SCL_SBL1_STACK_BASE & 0xffff
  MOVK x0, SCL_SBL1_STACK_BASE >> 16, LSL #16 

  MOV  x1,  SCL_SBL1_STACK_SIZE & 0xffff
  MOVK x1, SCL_SBL1_STACK_SIZE >> 16, LSL #16  

  // size expected to be 16 (0x10) byte aligned
  BIC  x1, x1, #0xF  
  MOV  x2, #0     // used as counter
  
write_16bytes_loop:
  stp xzr,xzr,[x0],  #0x10
  add     x2, x2, #0x10 // increment by 16 bytes
  cmp     x1, x2
  b.ne write_16bytes_loop  
  
      
  // Configure EL1 as AARCH64 because SBL1 in EL1 after QSEE returns is AARCH64
  // -------------------------------
  MRS x0, SCR_EL3
  ORR w0, w0, #(1 << 10)  // Set RW bit (SBL1 in EL1 is AArch64)
  MSR SCR_EL3, x0

  // -------------------------------
  // add more assembly init code here for entering sbl1_main_ctl	
  // 
	
  // restore PBL parameter and enter sbl1_main_ctl
  // -------------------------------
  MOV w0, w7
  BL sbl1_main_ctl
	
  // For safety
  BL boot_loop_here  // never returns, keep lr in r14 for debug


//======================================================================
// Called by sbl1_error_handler only. We clean up the registers and loop
// here until JTAG is connected.
//======================================================================
sbl_loop_here:

boot_loop_here:
  b boot_loop_here

  
sbl1_disable_serror:  
  // Disable EA bit. Do Not Take external aborts, sError interrupt
  // -------------------------------  
  MRS X0, SCR_EL3
  BIC X0, X0, #SCR_EL3_EA_Bit
  MSR SCR_EL3, X0
  RET    

sbl1_enable_serror:  

  // Clear M4M ERROR by writing 1 to APCS_CBF_M4M_ERR_CLR (0x09A50008)
  MOVZ X0, #0x09A5, LSL #16
  ADD  X0, X0, #0x08
  MOV  W1, #0x1
  STR  W1, [X0]

  //Clear L2ESR1
  MOVZ X0, #L2ESR1_INDEX
  MSR  L2CPUSRSELR_EL1, X0
  ISB
  //Write 0x80000080 to L2ESR1
  MOVZ X0, #0x8000, LSL #16
  ADD  X0, X0, #0x80
  MSR L2CPUSRDR_EL1, X0
  ISB

  // Enable EA bit. Take external aborts, sError interrupt
  // -------------------------------  
  MRS X0, SCR_EL3
  ORR X0, X0, #SCR_EL3_EA_Bit
  MSR SCR_EL3, X0
  RET

  .end
