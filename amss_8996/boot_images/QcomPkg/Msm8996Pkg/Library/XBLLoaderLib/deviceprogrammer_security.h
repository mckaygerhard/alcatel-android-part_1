/* Copyright (C) 2016 Tcl Corporation Limited */
/*==================================================================

GENERAL DESCRIPTION
  This module contains the authentication and some calls to the hashing
  functions used by device programmer.

Copyright (c) 2013-2015 by Qualcomm Technologies, Incorporated.
All Rights Reserved.
=========================================================================*/

/*=========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the
  module. Notice that changes are listed in reverse chronological
  order.

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Library/DeviceProgrammerCommonLib/firehose/deviceprogrammer_security.h#4 $
  $DateTime: 2016/08/08 00:01:05 $ $Author: pwbldsvc $

YYYY-MM-DD   who   what, where, why
----------   ---   ----------------------------------------------
2015-08-04   wek   Move Sha functions to a separate file.
2014-10-02   ah    Major code clean up
2013-06-03   ah    Added legal header
2013-05-31   ab    Initial checkin

=========================================================================*/

#ifndef DEVICEPROGRAMMER_SECURITY_H
#define DEVICEPROGRAMMER_SECURITY_H

typedef unsigned long long int SIZE_T;
//#include "deviceprogrammer_utils.h"
//#include "deviceprogrammer_sha.h"

void sbl1_PerformSHA256(uint8* inputPtr, uint32 inputLen,uint8* outputPtr);



#endif
