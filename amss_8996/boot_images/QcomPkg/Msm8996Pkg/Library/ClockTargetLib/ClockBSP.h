#ifndef CLOCKBSP_H
#define CLOCKBSP_H
/*
===========================================================================
*/
/**
  @file ClockBSP.h

  Internal header file for the BSP data structures.
*/
/*
  ====================================================================

  Copyright (c) 2014 QUALCOMM Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================

  when       who     what, where, why
  --------   ---     -------------------------------------------------
  07/07/14   vph     Initial code
  ====================================================================
*/


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBoot.h"
#include "ClockCommon.h"
#include "railway.h"

/*=========================================================================
      References
==========================================================================*/

/*=========================================================================
      Definitions
==========================================================================*/

/* CPU LF Mux */
#define CPU_LF_XO       0x0
#define CPU_LF_PLL2_DIV 0x1
#define CPU_LF_PLL_DIV  0x2
#define CPU_LF_CBF      0x3

/* CPU HF Mux */
#define CPU_HF_LFMUX    0x0
#define CPU_HF_PLL2     0x1
#define CPU_HF_GPLL0    0x2
#define CPU_HF_PLL      0x3

/* CBF MUX A */
#define CBF_MUXA_MUXB      0x0
#define CBF_MUXA_PLL_EAR   0x1
#define CBF_MUXA_PLL_MAIN  0x2
#define CBF_MUXA_PLL_AUX   0x3

/* CBF MUX B */
#define CBF_MUXB_XO      0x0
#define CBF_MUXB_JTAG    0x1
#define CBF_MUXB_MSTR    0x2
#define CBF_MUXB_SLV     0x3

/* Types of APCS PLL */
#define PLL_HUAYRA       0x0
#define PLL_SPARK        0x1

#define SPARK_CONFIG_CTL  0x4001051B
#define BRAMO_CONFIG_CTL  0x00004289
#define PEEL_CONFIG_CTL   0x88488CE3
#define HUAYRA_CONFIG_CTL 0x02200D4AA8
#define HUAYRA_CONFIG_CTL_V3 0x06200D4828

#define PLL_TEST_CTL_SPARK1 0x06000000
#define PLL_TEST_CTL_SPARK2 0x02000000
#define PLL_TEST_CTL_BRAMO  0x08000000
#define PLL_TEST_CTL_HUAYRA 0x400004000000
#define PLL_TEST_CTL_HUAYRA_V3 0x40001C000000


/*=========================================================================
      Type Definitions
==========================================================================*/

/*
 * ClockConfigApcsPLLType
 *
 * Parameters used for configuring Huayra or Sparks PLLS for APCS 
 *
 */
typedef struct sbl_clock_apcs_pll_cfg
{
  ClockConfigPLLType PLL_Cfg;
  uint32 nFrequency;
} ClockConfigApcsPLLType;

/*
 * ClockConfigCPUL2Type
 *
 * Parameters used for configuring a standard clock multiplexer.
 *
 *  nSourceHF   - The HF mux source to use.
 *  nSourceLF   - The LF mux source to use. 
 *  nL          - The L value of the HFPLL, 0 indicates not a PLL 
 */
typedef struct sbl_clock_l2cpu_cfg
{
  uint32 nSourceHF;
  uint32 nSourceLF;
  uint32 nGFMDiv;
  uint32 nL;
  uint32 nPLLPostDiv;
  uint32 nFrequency;
} ClockConfigCPUL2Type;

/* Data structure for configuration data */
typedef struct
{
  /* PLL configurations */
  ClockConfigPLLType PLL0_Cfg;
  ClockConfigPLLType PLL1_Cfg;
  ClockConfigPLLType PLL2_Cfg;
  ClockConfigPLLType PLL3_Cfg;
  ClockConfigPLLType PLL4_Cfg;
  ClockConfigPLLType MMPLL8_Cfg_V1;
  ClockConfigPLLType MMPLL8_Cfg_V2;

  /* PLL Configurations for APCS */
  ClockConfigApcsPLLType CPU_Cfg_Huayra[CLOCK_BOOT_PERF_NUM];
  ClockConfigApcsPLLType CPU_Cfg_Huayra_V3[CLOCK_BOOT_PERF_NUM];

  /* Configurations for CBF */
  ClockConfigCPUL2Type CBF_Cfg[CLOCK_BOOT_PERF_NUM];
  
  /* Configurations for RPM */
  ClockConfigMuxType RPM_Cfg[CLOCK_BOOT_PERF_NUM];

  /* System NOC config data */
  ClockConfigMuxType SNOC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* PIMEM config data */
  ClockConfigMuxType PIMEM_Cfg[CLOCK_BOOT_PERF_NUM];

  /* HS System NOC config data */
  ClockConfigMuxType HS_SNOC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* Config NOC config data */
  ClockConfigMuxType CNOC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* Periph NOC config data */
  ClockConfigMuxType PNOC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* SDC table (for backwards compatibility) */
  uint32 SDC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* SDC extended configurations */
  ClockFreqMuxCfgType SDC_Ext_Cfg[6];

  /* Crypto clock config */
  ClockConfigMuxType CE_Cfg;

  /* USB clock config */
  ClockConfigMuxType USB_Cfg;

  /* UART clock config */
  ClockConfigMuxType UART_Cfg[CLOCK_BOOT_PERF_NUM];

  /* Krait AHB config data */
  ClockConfigMuxType HMSS_AHB_Cfg;

  /* I2C clock config */
  ClockConfigMuxType I2C_Cfg[CLOCK_BOOT_PERF_NUM];

  /* SPI configurations */
  ClockFreqMuxCfgType SPI_Cfg[3];

  /* SPI Serial configurations */
  ClockFreqMuxCfgType QSPISer_Cfg[5];

  /* UFS clock config */
  ClockConfigMuxType UFS_Cfg[CLOCK_BOOT_PERF_NUM];

  /* UFS ICE CORE clock config */
  ClockConfigMuxType UFS_Ice_Cfg[CLOCK_BOOT_PERF_NUM];

} Clock_ConfigType;

/* Data structure for Railway data */
typedef struct
{
  char*             CxRail;
  int               nCxRailId;
  railway_voter_t   CxVoter;
  char*             EBIRail;
  int               nEBIRailId;
  railway_voter_t   EBIVoter;
}Clock_RailwayType;

extern Clock_ConfigType *Clock_Config( void );
extern Clock_RailwayType *Clock_RailwayConfig( void );
extern boolean Clock_EnableSource( ClockSourceType eSource );
extern boolean Clock_DisableSource( ClockSourceType eSource );
extern boolean Clock_ConfigureSource( ClockSourceType eSource );

boolean Clock_SourceMapToMux
(
  const ClockConfigMuxType *pConfig,
  uint32 *nMuxValue
);

boolean Clock_MuxMapToSource
(
  ClockConfigMuxType *pConfig,
  uint32 nSource
);

#endif /* !CLOCKBSP_H */

