/*
===========================================================================
  @file ClockCPU.c

  This file provides clock initialization for the Apps CPU.
===========================================================================

  Copyright (c) 2014-2015 QUALCOMM Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  =========================================================================


  when       who     what, where, why
  --------   ---     ------------------------------------------------------
  04/17/15   vph     Add support for V2.
  07/07/14   vph     Initial revision

  =========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/
#include "ClockHWIO.h"
#include "ClockBSP.h"
#include <busywait.h>

/*=========================================================================
      Macro Definitions
==========================================================================*/
#define PLL_BIAS_CNT 6
#define PLL_LOCK_CNT 0
#define MODE_CTL_MSK 0x7
#define BIAS_CNT_5_0_SHFT 14
#define LOCK_CNT_5_0_SHFT 8

#define HUAYRA_POSTDIV_SHFT 8
#define HUAYRA_POSTDIV_MASK 0x300
#define HUAYRA_PLLOUT_MASK 0x1F

#define POSTDIV_SHFT 8
#define SPARK_VCO_SHFT 20
#define SPARK_POSTDIV_MASK 0x700
#define SPARK_PLLOUT_MASK 0x1F

#define HWIO_MODE_OFFS                 (HWIO_ADDR(APC0_QLL_PLL_MODE)            - HWIO_ADDR(APC0_QLL_PLL_MODE))
#define HWIO_L_VAL_OFFS                (HWIO_ADDR(APC0_QLL_PLL_L_VAL)           - HWIO_ADDR(APC0_QLL_PLL_MODE))
#define HWIO_ALPHA_VAL_HI_OFFS         (HWIO_ADDR(APC0_QLL_ALT_PLL_MODE)        - HWIO_ADDR(APC0_QLL_ALT_PLL_ALPHA_VAL_HI))
#define HWIO_ALPHA_VAL_OFFS            (HWIO_ADDR(APC0_QLL_PLL_ALPHA_VAL)       - HWIO_ADDR(APC0_QLL_PLL_MODE))
#define HWIO_CONFIG_CTL_HI_OFFS        (HWIO_ADDR(APC0_QLL_PLL_CONFIG_CTL_HI)   - HWIO_ADDR(APC0_QLL_PLL_MODE))
#define HWIO_CONFIG_CTL_OFFS           (HWIO_ADDR(APC0_QLL_PLL_CONFIG_CTL_LO)   - HWIO_ADDR(APC0_QLL_PLL_MODE))
#define HWIO_TEST_CTL_OFFS             (HWIO_ADDR(APC0_QLL_TEST_CTL)            - HWIO_ADDR(APC0_QLL_PLL_MODE))
#define HWIO_USER_CTL_HI_OFFS          (HWIO_ADDR(APC0_QLL_ALT_PLL_USER_CTL_HI) - HWIO_ADDR(APC0_QLL_ALT_PLL_MODE))
#define HWIO_USER_CTL_OFFS             (HWIO_ADDR(APC0_QLL_PLL_USER_CTL)        - HWIO_ADDR(APC0_QLL_PLL_MODE))

#define APCS_CLKSEL_MASK  0xF
#define APCS_CLKSEL_GPLL0 0xC
#define APCS_CLKSEL_MAIN  0x4
#define APCS_CLKSEL_ALT   0x3

#define SBL_DLOAD_MODE_BIT_MASK_V1       0x00100000
#define SBL_DLOAD_MODE_BIT_MASK_V1_SHFT  20

/* Configure and Enable Soft Start;
     FSSM=2�b10;
     FSSP=3�b011;
     SSSP=3�b011;
     B2SSSTP=2�b10;
     BNSSSTP=2�b10;
     BSSMODE=0;
     FSSE=1;
     SSSE=1;
     SSGE=1
*/
#define SOFT_START 0x0337A700

/*=========================================================================
     Externs
==========================================================================*/

/*=========================================================================
      Function Prototypes
==========================================================================*/
static boolean Clock_EnableAppsPLL(const ClockConfigPLLType *pConfig, uint32 PLL_Type);

/*=========================================================================
      Data
==========================================================================*/

/* The current rate of the APPS cluster 0 clock in KHz */
static uint32 Clock_APPSCL0SpeedKHz = 0;

/*=========================================================================
      Function Definitions
==========================================================================*/

/* ========================================================================
**  Function : Clock_GetAPSSCL0SpeedKHz
** ======================================================================*/
/*
    Description: Configure CPU clock source

    @param 
      None.
      
    @return
      Frequency of APPS cluster zero in KHZ.

    @dependencies
      None.

    @sa
      None.
*/

uint32 Clock_GetAPSSCL0SpeedKHz(void)
{
  uint32 apps_pbl_boot_speed;
  if(!Clock_APPSCL0SpeedKHz)
  { 
    apps_pbl_boot_speed = HWIO_INF(CAPT_SEC_GPIO, BOOT_CONFIG_GPIO_APPS_PBL_BOOT_SPEED);
    switch(apps_pbl_boot_speed)
    { 
      case 0: 
        Clock_APPSCL0SpeedKHz = 19200;
        break;
      case 1: 
        Clock_APPSCL0SpeedKHz = 600000;
        break;
      case 2: 
        Clock_APPSCL0SpeedKHz = 796800;
        break;
      case 3: 
        Clock_APPSCL0SpeedKHz = 998400;
        break;
    }
  }
  return Clock_APPSCL0SpeedKHz;
}

/* ========================================================================
**  Function : Clock_CPUMuxSelect
** ======================================================================*/
/*
    Description: Configure CPU clock source

    @param 
      hf_mux_select -  High frequency mux source to be selected.
      lf_mux_select -  Low frequency mux source to be selected.
      pll_div       -  PLL main divider (1-2)
      
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/

static boolean Clock_CPUMuxSelect( uint32 hf_mux_select, uint32 lf_mux_select,
                                   uint32 pll_div )
{
  return TRUE;
} /* END Clock_CPUMuxSelect */


/* ============================================================================
**  Function : Clock_SetCBFPerfLevel
** ============================================================================
*/
/**
  Configure CBF perf level.

  @param eCBFPerfLevel [in] - CBF performance level.

  @return
  TRUE -- CPU configuration to perf level was successful.
  FALSE -- Configuration failed.

  @dependencies
  None.

  @sa
  Clock_Init.
*/

boolean Clock_SetCBFPerfLevel(ClockBootPerfLevelType eCBFPerfLevel)
{
  const Clock_ConfigType *cfg = Clock_Config();
  const ClockConfigCPUL2Type *CBF_cfg;

  if( eCBFPerfLevel >= CLOCK_BOOT_PERF_NUM ) return FALSE;

  CBF_cfg = &cfg->CBF_Cfg[eCBFPerfLevel];

  /* Configure CBF if it is not at the same performance level */
  if ( (HWIO_INF(APCS_CBF_GFMUX_CTL, MUXB_SEL_STS) != CBF_cfg->nSourceHF) ||
       (HWIO_INF(APCS_CBF_GFMUX_CTL, MUXA_SEL_STS) != CBF_cfg->nSourceLF) )
  {
    /* Retune PLL */
    if( CBF_cfg->nL > 0) 
    {
      /* TO DO */
    }
  
    /* Switch CBF source */
    HWIO_OUTF(APCS_CBF_GFMUX_CTL, MUXB_SEL, CBF_cfg->nSourceHF);
    HWIO_OUTF(APCS_CBF_GFMUX_CTL, MUXA_SEL, CBF_cfg->nSourceLF);
  }

  return TRUE;
}


/* ============================================================================
**  Function : Clock_SetCPUPerfLevel
** ============================================================================
*/
/**
  Configure A53 CPU to a specific perf level.

  @param eCPUPerfLevel [in] - CPU performance level.

  @return
  TRUE -- CPU configuration to perf level was successful.
  FALSE -- Configuration failed.

  @dependencies
  None.

  @sa
  Clock_Init.
*/

boolean Clock_SetCPUPerfLevel(ClockBootPerfLevelType eCPUPerfLevel)
{
  static ClockBootPerfLevelType eCurrLevel = CLOCK_BOOT_PERF_NONE;
  const Clock_ConfigType *cfg = Clock_Config();
  const ClockConfigApcsPLLType *CPU_Cfg;
  uint32 dload_flag =0x0;
  ClockConfigPLLType *AppsPLL_cfg;

  /* Don't switch performance level if it has been set previously */
  if ( eCurrLevel == eCPUPerfLevel )
  {
    return TRUE;
  }
  eCurrLevel = eCPUPerfLevel;

  // Enable soft start to reduce in-rush current
  if( HWIO_INF(TCSR_SOC_HW_VERSION, MAJOR_VERSION) < 3 && HWIO_INF(TCSR_SOC_HW_VERSION, DEVICE_NUMBER) == 0 )
  {
    HWIO_OUT (APC0_QLL_SSCTL, SOFT_START);
    HWIO_OUT (APC1_QLL_SSCTL, SOFT_START);
  }

  if( eCPUPerfLevel >= CLOCK_BOOT_PERF_NUM ) return FALSE;

  if( HWIO_INF(TCSR_SOC_HW_VERSION, MAJOR_VERSION) == 3 || HWIO_INF(TCSR_SOC_HW_VERSION, DEVICE_NUMBER) == 4 )
    CPU_Cfg = &cfg->CPU_Cfg_Huayra_V3[eCPUPerfLevel];
  else
    CPU_Cfg = &cfg->CPU_Cfg_Huayra[eCPUPerfLevel];

  AppsPLL_cfg = (ClockConfigPLLType*)&CPU_Cfg->PLL_Cfg;

  /* Retune Apps PLL to the required frequency */
  if (HWIO_INF(APC0_QLL_PLL_MODE, LOCK_DET) == 1)
  {
    /* AppsPLL must be retuned, switch CPU0 to GPLL0, and turn off AppsPLL */
    if( !Clock_EnableSource(SRC_GPLL0) ) return FALSE;

    /* Configure GPLL0 (APCS_AUX_CLK) to div 1 */
    HWIO_OUTF(APCS_AUX_CBCR, CLK_DIV, 0);
    HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_AUX, 1);
    HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_AUX2, 1);

    /* Enable APCS_AUX_CLK for source from GPLL0 */
    if ( HWIO_INF(APCS_AUX_CBCR, CLK_OFF) == 1)
    {
      HWIO_OUTF(APCS_AUX_CBCR, SW_CTL, 1);
    }

    /* Switch to APCS_GPLL0 */
    HWIO_OUTM(APC0_QLL_CLKSEL, APCS_CLKSEL_MASK, APCS_CLKSEL_GPLL0 );

    /* Disable AppsPLL */
    if( ! Clock_DisablePLL(AppsPLL_cfg) ) return FALSE;
  }

  //Temporary change to bypass enabling l2 auto clock gating in download mode
  dload_flag = HWIO_GCC_WIND_DOWN_TIMER_INM(SBL_DLOAD_MODE_BIT_MASK_V1)>>SBL_DLOAD_MODE_BIT_MASK_V1_SHFT;   
  
  if (!dload_flag)
  {
    if( HWIO_INF(TCSR_SOC_HW_VERSION, MAJOR_VERSION) == 3 || HWIO_INF(TCSR_SOC_HW_VERSION, DEVICE_NUMBER) == 4)
    {
      HWIO_OUTM(APC0_QLL_CLKCTL, 0xFF, 0x71);
      HWIO_OUTM(APC1_QLL_CLKCTL, 0xFF, 0x71);
    }
    else
    {
      HWIO_OUTM(APC0_QLL_CLKCTL, 0xFF, 0x41);
      HWIO_OUTM(APC1_QLL_CLKCTL, 0xFF, 0x41);
    }
  }

  if( AppsPLL_cfg->nL > 0) 
  {
    /* Switch back to APCS PLL main source */
    if( ! Clock_EnableAppsPLL(AppsPLL_cfg, PLL_HUAYRA) ) return FALSE;
  HWIO_OUTM(APC0_QLL_CLKSEL, APCS_CLKSEL_MASK, APCS_CLKSEL_MAIN );
  }
  
  /* Save new CPU state */
  Clock_APPSCL0SpeedKHz = CPU_Cfg->nFrequency;

  /* Set CBF Performance level */
  return Clock_SetCBFPerfLevel( eCPUPerfLevel );
}


/* Stubbed out for backwards compatibility, L2 clock is sync to CPU */
boolean Clock_SetL2PerfLevel(ClockBootPerfLevelType eL2PerfLevel)
{
  return TRUE;
}

/* ============================================================================
**  Function : Clock_EnableAppsPLL
** ============================================================================
    Configure and enable a high frequency Apps PLL.

    @param 
      pConfig -  [IN] PLL configuration structure

    @return
      TRUE -- Initialization was successful.
      FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
static boolean Clock_EnableAppsPLL(const ClockConfigPLLType *pConfig, uint32 PLL_Type)
{
  
  uintnt nModeAddr;
  uint32 nUserVal;
  volatile uint32 nModeVal;

  nModeAddr = pConfig->nPLLModeAddr;
  
  if ( (inp32(nModeAddr) & HWIO_FMSK(APC0_QLL_PLL_MODE, LOCK_DET)) == 1)
  {
    /* The PLL is already enabled, it cannot be reconfigured */
    return FALSE;
  }

  /*
   * Get the CONFIG value.
   */
  nUserVal = inp32(nModeAddr + HWIO_OFFS(USER_CTL));

  /*
  * Program the VCO. Spark Only.
  */
  if( PLL_Type == PLL_SPARK )
    nUserVal |= (pConfig->nVCO << SPARK_VCO_SHFT);

  /*
   * Program the post-div value 
   */
  nUserVal |= ((pConfig->nPostDiv-1)  << POSTDIV_SHFT);

  /*
   * Enable MAIN and EARLY Output bits. 
   */
  nUserVal |= HWIO_FMSK(GCC_GPLL0_USER_CTL, PLLOUT_LV_MAIN);
  nUserVal |= HWIO_FMSK(GCC_GPLL0_USER_CTL, PLLOUT_LV_EARLY);
  // Set post-div
  /*
   * Program the USER_CTL register.
   */
  outp32(nModeAddr + HWIO_OFFS(USER_CTL), nUserVal);


  /* Configure the PLL. */
  if( PLL_Type == PLL_HUAYRA )
    outp32(nModeAddr + HWIO_OFFS(CONFIG_CTL_HI), (uint32)(pConfig->nConfigCtl >> 32)); // Huayra Only
  
  outp32(nModeAddr + HWIO_OFFS(CONFIG_CTL), (uint32)(pConfig->nConfigCtl));
  
  /*
   * Program L, Alpha.
   */
  outp32(nModeAddr + HWIO_OFFS(L_VAL), pConfig->nL);
  if( pConfig->nAlpha )
  {
    if( PLL_Type == PLL_SPARK )
      outp32(nModeAddr + HWIO_OFFS(ALPHA_VAL_HI), (uint32)(pConfig->nAlpha >> 32)); // Spark Only
    
    outp32(nModeAddr + HWIO_OFFS(ALPHA_VAL), (uint32)pConfig->nAlpha);
  }

  nModeVal = inp32(nModeAddr);
  nModeVal |= HWIO_FMSK(APC0_QLL_PLL_MODE, BIAS_CNT_5_0) & (PLL_BIAS_CNT << BIAS_CNT_5_0_SHFT);
  nModeVal |= HWIO_FMSK(APC0_QLL_PLL_MODE, LOCK_CNT_5_0) & (PLL_LOCK_CNT << LOCK_CNT_5_0_SHFT);
  
  outp32(nModeAddr, nModeVal);
  
  busywait( 50 );

  /*
   * Enable the PLL.
   */
  nModeVal = inp32(nModeAddr);
  nModeVal = (nModeVal & (~MODE_CTL_MSK)) | (MODE_CTL_MSK & 0x2);
  outp32(nModeAddr, nModeVal);

  busywait( 50 );

  nModeVal = inp32(nModeAddr);
  nModeVal = (nModeVal & (~MODE_CTL_MSK)) | (MODE_CTL_MSK & 0x6);
  outp32(nModeAddr, nModeVal);
  
  busywait( 200 );

  while ((inp32(nModeAddr) & HWIO_FMSK(APC0_QLL_PLL_MODE, LOCK_DET)) == 0)
  {
    busywait(1);
  }

  nModeVal = inp32(nModeAddr);
  nModeVal = (nModeVal & (~MODE_CTL_MSK)) | (MODE_CTL_MSK & 0x7);
  outp32(nModeAddr, nModeVal);

  return TRUE;

} /* END Clock_EnableSparkPLL */

