/** @file

  QCOM_USB_CONFIG_PROTOCOL 8996 implementation.

  Copyright (c) 2011 - 2015, QUALCOMM Technologies Inc. All rights reserved.

**/


/*=============================================================================
                              EDIT HISTORY


 when       who      what, where, why
 --------   ---      ----------------------------------------------------------
 03/25/15   sbryksin USB LPM, HS/SS PHY config changes, USB mode toggle, clamp UsbFn to HS if SS PHY failed
 09/23/14   ar       Cleanup debug messages
 08/13/14   amitg    Clean Up Code, Add CI core init in exit boot services only
 06/18/14   amitg    Added KDNET Port Selection Variable and defaulted KDNET over primary port
 06/04/14   amitg    MSM 8994 BU Updates
 06/03/14   vk       Disable init temp.
 05/20/14   amitg    CleanUp
 04/30/14   ar       Cleanup for NDEBUG build  
 03/14/14   amitg    Updates supporting SNPS Host mode and device mode
 10/29/13   amitg    Initial Revision for MSM 8994
=============================================================================*/

//
// Includes
//
#include "UsbConfigPrivate.h"


// External vars from other drivers
extern EFI_PLATFORMINFO_PLATFORM_INFO_TYPE    PlatformInfo;
extern DalChipInfoVersionType                 chipVersion;

/*
 * Global core configuration instance
 */
QCOM_USB_CORE_CONFIGURATION  gUsbCoreConfig;

extern USB_DEV gUsbConfigDevice[];                          //structure holding controller instances
extern QCOM_USB_CONFIG_PROTOCOL gUsbConfigTemplate;         //template of UsbConfigProtocol struct

extern EFI_GUID                 gQcomTokenSpaceGuid;        // used for getting the BCD variable "UsbUseSynopsys"


/**
  Returns core number for a given core type

  @param [in]  CoreType           USB core type.
  @param [out] CoreNum            Core number

  @retval EFI_SUCCESS             CoreNum is valid
  @retval EFI_INVALID_PARAMETER   CoreNum is NULL
  @retval EFI_UNSUPPORTED         Unsupported CoreType.
**/
EFI_STATUS
EFIAPI
GetCoreNum (
  IN  QCOM_USB_CONFIG_CORE_TYPE   CoreType,
  OUT  QCOM_USB_CORE_NUM          *CoreNum
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();

  if (NULL == CoreNum) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  //vsb:TODO this is ugly. Need to have clean solution to map coreNum/coreType
  switch (CoreType)
  {
    case USB_CONFIG_SSUSB1:
      *CoreNum = USB_CORE_0;
    break;
    case USB_CONFIG_SSUSB2:
      *CoreNum = USB_CORE_1;
    break;
    default:
      DBG(EFI_D_ERROR, "unsupported USB core 0x%x", CoreType);
      Status = EFI_UNSUPPORTED;
      *CoreNum = USB_CORE_MAX_NUM;
    break;
  }

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}



/**
  Returns base address of the specifed USB core.

  @param [in]  CoreType           USB core type.
  @param [out] BaseAddr           Register set base address output.

  @retval EFI_SUCCESS             BaseAddr is valid
  @retval EFI_INVALID_PARAMETER   BaseAddr is NULL
  @retval EFI_UNSUPPORTED         Unsupported CoreType.
**/
EFI_STATUS
EFIAPI
GetCoreBaseAddr (
  IN  QCOM_USB_CONFIG_CORE_TYPE   CoreType,
  OUT UINTN                       *BaseAddr
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  QCOM_USB_CORE_NUM   coreNum;

  FNC_ENTER_MSG ();

  if (NULL == BaseAddr) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  if (CoreType >= USB_CONFIG_CORE_MAX) {
    Status = EFI_UNSUPPORTED;
    DBG(EFI_D_ERROR, "unsupported USB core 0x%x", CoreType);
    goto ON_EXIT;
  }

  Status = GetCoreNum(CoreType, &coreNum);
  ERR_CHK ("failed to get USB core number");
  
  *BaseAddr = gUsbConfigDevice[coreNum].usbCore.BaseAddr;

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}



/**
  Initializes the specified USB core for in host or device mode.

  @param  Mode                  Host or device mode
  @param  CoreNum               Index of core supporting Mode to initialize

  @retval EFI_SUCCESS           USB initialized successfully.
  @retval EFI_UNSUPPORTED       Unsupported ConfigType.
**/
EFI_STATUS
EFIAPI
ConfigUsb (
  IN QCOM_USB_MODE_TYPE         Mode,
  IN QCOM_USB_CORE_NUM          CoreNum
  )
{
  EFI_STATUS                  Status   = EFI_SUCCESS;
  QCOM_USB_CORE               *UsbCore;

  FNC_ENTER_MSG ();

  if (CoreNum > USB_CORE_1)
  {
    DEBUG(( EFI_D_ERROR,
          "ConfigUsb: Invalid core passed: %d\r\n", CoreNum));
    return EFI_INVALID_PARAMETER;
  }

  UsbCore = &gUsbConfigDevice[CoreNum].usbCore;

  USB_ASSERT_RETURN ((NULL != UsbCore), EFI_DEVICE_ERROR);
  
  // initialize the core
  USB_ASSERT_RETURN ((NULL != UsbCore->InitCommon), EFI_DEVICE_ERROR);
  Status = UsbCore->InitCommon(UsbCore);
  ERR_CHK ("failed to perform common initialization for USB core %d: %r",
      CoreNum, Status);

  if ( (Mode == USB_DEVICE_MODE_HS) || (Mode == USB_DEVICE_MODE_SS) ) {
    USB_ASSERT_RETURN ((NULL != UsbCore->InitDevice), EFI_DEVICE_ERROR);
    Status = UsbCore->InitDevice(UsbCore);
    ERR_CHK ("failed to perform device initializaition for USB core %d: %r",
        CoreNum, Status);
  } else {
    USB_ASSERT_RETURN ((NULL != UsbCore->InitHost), EFI_DEVICE_ERROR);
    Status = UsbCore->InitHost(UsbCore);
    ERR_CHK ("failed to perform host initializaition for USB core %d: %r",
        CoreNum, Status);
  }

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}



/**
  Resets the USB core and reinitializes in the appropriate mode (host or device).

  @param  ConfigType            Configuration type
  @param  DeviceMode            Reinitialize in host or device mode

  @retval EFI_SUCCESS           USB reset successfully
  @retval Others                Error resetting core

**/
EFI_STATUS
EFIAPI
ResetUsb (
  IN QCOM_USB_CONFIG_CORE_TYPE  CoreType,
  IN BOOLEAN                    DeviceMode
  )
{
  EFI_STATUS          Status = EFI_SUCCESS;
  QCOM_USB_CORE       *UsbCore;
  QCOM_USB_CORE_NUM   coreNum;

  FNC_ENTER_MSG ();

  if (CoreType >= USB_CONFIG_CORE_MAX) {
    Status = EFI_UNSUPPORTED;
    DBG(EFI_D_ERROR, "unsupported USB core 0x%x", CoreType);
    goto ON_EXIT;
  }

  Status = GetCoreNum(CoreType, &coreNum);
  ERR_CHK ("failed to get USB core number");
  
  UsbCore = &gUsbConfigDevice[coreNum].usbCore;
  
  // reset the core
  USB_ASSERT_GOTO_SETSTS ((NULL != UsbCore->Reset), ON_EXIT,
                          EFI_DEVICE_ERROR, Status);
  Status = UsbCore->Reset(UsbCore);
  ERR_CHK ("failed to reset USB core %d: %r", CoreType, Status);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/**
  Enter low power mode for USB Core

  @param  Mode                  Host or device mode
  @param  CoreNum               Index of core to be entered in low power mode

  @retval EFI_SUCCESS           USB core entered LPM successfully.
  @retval EFI_INVALID_PARAMETER Unsupported Core
  @retval Other                 Operation failed
**/

EFI_STATUS
EFIAPI
EnterLPM (
  IN QCOM_USB_MODE_TYPE         Mode,
  IN QCOM_USB_CORE_NUM          CoreNum
  )
{
  QCOM_USB_CORE       *UsbCore;
  EFI_STATUS          Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();

  UsbCore = &gUsbConfigDevice[CoreNum].usbCore;

  if (CoreNum > USB_CORE_1)
  {
    DEBUG(( EFI_D_ERROR,
          "EnterLPM: Invalid core passed: %d\r\n", CoreNum));
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }

  Status = UsbCore->CoreEnterLPM(UsbCore);
  ERR_CHK ("failed to enter low power mode for USB coreNum %d: %r", CoreNum, Status);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/**
  Exits low power mode for USB Core

  @param  Mode                  Host or device mode
  @param  CoreNum               Index of core supporting Mode to initialize

  @retval EFI_SUCCESS                 USB core exited LPM successfully.
  @retval EFI_INVALID_PARAMETER       Unsupported Core
  @retval Other                       Operation failed
**/

EFI_STATUS
EFIAPI
ExitLPM (
  IN QCOM_USB_MODE_TYPE         Mode,
  IN QCOM_USB_CORE_NUM          CoreNum
  )
{
  QCOM_USB_CORE       *UsbCore;
  EFI_STATUS          Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();

  UsbCore = &gUsbConfigDevice[CoreNum].usbCore;

  if (CoreNum > USB_CORE_1)
  {
    DEBUG(( EFI_D_ERROR,
          "ConfigUsb: Invalid core passed: %d\r\n", CoreNum));
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }

  Status = UsbCore->CoreExitLPM(UsbCore);
  ERR_CHK ("failed to exit low power mode for USB coreNum %d: %r", CoreNum, Status);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/**
  Return appropriate core type describing the ChipIdea core to be used in
  device mode. The Synopsys Usbfn driver will use a separate function.

  @param  CoreType               Configuration type.

  @retval EFI_SUCCESS            Success
  @retval EFI_UNSUPPORTED        Unsupported ConfigType.

**/
EFI_STATUS
EFIAPI
GetUsbFnConfig (
  OUT QCOM_USB_CONFIG_CORE_TYPE *CoreType
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();
  ARG_CHK (CoreType);

  *CoreType = USB_CONFIG_CORE_MAX;

ON_EXIT:
  FNC_LEAVE_MSG ();
  return EFI_UNSUPPORTED;
}


/**
  Return appropriate core type for each platform

  @param  CoreType               Configuration type.

  @retval EFI_SUCCESS            Success
  @retval EFI_UNSUPPORTED        Unsupported ConfigType.

**/
EFI_STATUS
EFIAPI
GetSSUsbFnConfig (
  OUT QCOM_USB_CONFIG_CORE_TYPE *CoreType
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();
  ARG_CHK (CoreType);

  *CoreType = USB_CONFIG_SSUSB1;

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/**
  Return appropriate core type to use for the specified host mode and index.

  @param  Mode                  USB_HOST_MODE_EHCI or USB_HOST_MODE_XHCI
  @param  CoreNum               Core number supporting Mode
  @param  CoreType              Core type fitting above parameters

  @retval EFI_SUCCESS           CoreType contains a valid core
  @retval EFI_UNSUPPORTED       Unsupported combination of parameters

**/
EFI_STATUS
EFIAPI
GetUsbHostConfig (
  IN  QCOM_USB_MODE_TYPE        Mode,
  IN  QCOM_USB_CORE_NUM         CoreNum,
  OUT QCOM_USB_CONFIG_CORE_TYPE *CoreType
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();
  ARG_CHK (CoreType);

  *CoreType = USB_CONFIG_SSUSB1;

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}



/**
  UsbConfigInit does HW/driver initialization that's not dependent on other driver requests.

  @retval EFI_SUCCESS            Success.
  @retval EFI_DEVICE_ERROR       Failure

**/
EFI_STATUS
EFIAPI
UsbConfigInit (
  VOID
  )
{
  EFI_STATUS                Status       = EFI_SUCCESS;
  UINT8                     uefiVar;
  UINTN                     uefiVarSize;
  FNC_ENTER_MSG ();

  // Open all the protocols needed by the core initialization functions
  Status = UsbConfigLibOpenProtocols();
  ERR_CHK("Failed to open all required protocols");

  //Allocate a handle w/ invalid core config to be used by clients to start a controller in case where no valid USB controller handles exist
  gUsbConfigTemplate.coreNum = (QCOM_USB_CORE_NUM)USB_CORE_MAX_NUM;
  gUsbConfigTemplate.modeType = USB_INVALID_MODE;
  gUsbConfigDevice[USB_CORE_MAX_NUM_8996].usbConfig = gUsbConfigTemplate;

  Status = gBS->InstallMultipleProtocolInterfaces(&gUsbConfigDevice[USB_CORE_MAX_NUM_8996].clientHandle ,              //if handle is NULL, new one is allocated
                                &gQcomUsbConfigProtocolGuid,  &gUsbConfigDevice[USB_CORE_MAX_NUM_8996].usbConfig,
                                NULL);
  ERR_CHK ( "Failed to install USB_CONFIG protocol");

  // Store the SoC version
  switch (chipVersion >> 16)
  {
    case QCOM_MSM8996_V1:
      DBG (EFI_D_ERROR, "8996 v1 Chip found");
      gUsbCoreConfig.MSM8996Version = QCOM_MSM8996_V1;
      break;

    case QCOM_MSM8996_V2:
      DBG (EFI_D_INFO, "8996 v2 Chip found");
      gUsbCoreConfig.MSM8996Version = QCOM_MSM8996_V2;
      break;

    case QCOM_MSM8996_V3:
      DBG (EFI_D_INFO, "8996 v3 Chip found");
      gUsbCoreConfig.MSM8996Version = QCOM_MSM8996_V3;
      break;

    default:
      DBG (EFI_D_INFO, "8996 Unknown Chip found");
      gUsbCoreConfig.MSM8996Version = QCOM_MSM8996_UNKNOWN;
      break;
  }

  DBG (EFI_D_INFO, "Platform = 0x%x",gUsbCoreConfig.MSM8996Version );

  //Bootstrap controllers & install ifaces for UEFI drivers in upper stacks  
  uefiVarSize = sizeof(uefiVar);
  //if UsbHostPrimaryPort is set to host override, force host mode
  Status = gRT->GetVariable(L"UsbHostPrimaryPort", &gQcomTokenSpaceGuid, NULL, &uefiVarSize, &uefiVar);
  if (!EFI_ERROR(Status) && (uefiVar != 0) ) 
  {
    DBG (EFI_D_WARN, "Host mode override set: starting XHCI stack");
    UsbStartController (USB_CORE_0, USB_HOST_MODE_XHCI);
  }
  else
  {
    UsbStartController (USB_CORE_0, USB_DEVICE_MODE_SS);
  }

  //USB debugger on 2ndary core: if set, configure the core instead of LPM
  Status = gRT->GetVariable(L"UsbUseDebuggerOnSecPort", &gQcomTokenSpaceGuid, NULL, &uefiVarSize, &uefiVar);
  if (!EFI_ERROR(Status) && (uefiVar != 0) ) 
  {
    DBG (EFI_D_WARN, "USB debug on 2ndary core override set: configuring the core");
    ConfigUsb(USB_DEVICE_MODE_SS, USB_CORE_1);
  }
  else
  {
    //Put secondary controller in LPM
    EnterLPM(USB_DEVICE_MODE_SS, USB_CORE_1);
  }

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/**
  Placeholder function

  @param  ConfigType             Configuration type.

  @retval EFI_SUCCESS            Success
  @retval EFI_UNSUPPORTED        Unsupported ConfigType.

**/
EFI_STATUS
EFIAPI
ExitUsbLibServices (
  VOID
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;
  
  FNC_ENTER_MSG ();

  //vsb: TODO: use primary core as default for debugger?
  //Configure debugger on 2ndary core
  //DEBUG(( EFI_D_ERROR, "ExitUsbLibServices: Configuring USB for KDNET on 2ndary port \r\n"));
  //ConfigUsb (USB_DEVICE_MODE_SS, USB_CORE_1);

  FNC_LEAVE_MSG ();
  return Status;
}


EFI_STATUS
EFIAPI UsbStopController(
  IN   QCOM_USB_CORE_NUM          CoreNum,
  IN   QCOM_USB_MODE_TYPE         ModeType
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  
  FNC_ENTER_MSG ();
  
  if (CoreNum > USB_CORE_1)
  {
    DEBUG(( EFI_D_ERROR,
          "UsbConfigStopController: Invalid core passed: %d\r\n", CoreNum));
    return EFI_INVALID_PARAMETER;
  }
  
  //check if it's a valid request to stop
  if (gUsbConfigDevice[CoreNum].clientHandle == NULL)
  {
    DEBUG(( EFI_D_ERROR,
          "UsbConfigStopController: No stack running on core: %d\r\n", CoreNum));
    return EFI_INVALID_PARAMETER;
  }
  
   //check if current running stack is running in mode that's requested to be stopped
  if (gUsbConfigDevice[CoreNum].usbConfig.modeType != ModeType)
  {
    DEBUG(( EFI_D_ERROR,
          "UsbConfigStopController: Invalid mode (%d) passed to core %d\r\n", ModeType, CoreNum));
    return EFI_INVALID_PARAMETER;
  }

  Status = gBS->UninstallProtocolInterface (
                gUsbConfigDevice[CoreNum].clientHandle,
                &gQcomUsbConfigProtocolGuid, 
                &gUsbConfigDevice[CoreNum].usbConfig
                );
  ERR_CHK ( "UsbConfigStopController: Failed to uninstall QcomUsbConfig protocol"); 
  
  //clear out device struct
  gUsbConfigTemplate.coreNum = (QCOM_USB_CORE_NUM)USB_CORE_MAX_NUM;
  gUsbConfigTemplate.modeType = USB_INVALID_MODE;
  gUsbConfigDevice[CoreNum].clientHandle = NULL;
  gUsbConfigDevice[CoreNum].usbConfig = gUsbConfigTemplate;
  
ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}



EFI_STATUS
EFIAPI UsbStartController(
  IN   QCOM_USB_CORE_NUM          CoreNum,
  IN   QCOM_USB_MODE_TYPE         ModeType
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  
  FNC_ENTER_MSG ();
  
  if ((CoreNum > USB_CORE_1) || (ModeType >= USB_INVALID_MODE))
  {
    return EFI_INVALID_PARAMETER;
  }
  
  //USB3.0 primary supports XHCI and device mode only (HS/SS)
  if ((CoreNum == USB_CORE_0) && (ModeType != USB_HOST_MODE_XHCI) && (ModeType != USB_DEVICE_MODE_SS))
  {
    return EFI_INVALID_PARAMETER;
  }

  //USB3.0 2ndary supports device mode only
  if ((CoreNum == USB_CORE_1) && (ModeType != USB_DEVICE_MODE_SS))
  {
    return EFI_INVALID_PARAMETER;
  }
  
  //check if there's already a stack running on the core. If so, stop it, then start new stack
  if (gUsbConfigDevice[CoreNum].clientHandle != NULL)
  {
    //if core configured in same mode, ignore duplicate request
    if (gUsbConfigDevice[CoreNum].usbConfig.modeType == ModeType)
    {
      DEBUG(( EFI_D_ERROR,
          "UsbConfigStopController: Core %d already running in current mode: \r\n", CoreNum));
      //vsb: TODO: return STATUS_SUCCESS instead?
      return EFI_ALREADY_STARTED;
    }
    else
    {
      Status = UsbStopController( CoreNum, gUsbConfigDevice[CoreNum].usbConfig.modeType);
      if (EFI_ERROR (Status)) {
        DEBUG(( EFI_D_ERROR,
          "UsbConfigStartController: Unable to stop controller on core %d, Status =  (0x%x)\r\n", CoreNum, Status));
        return Status;
      }
    }
  }
  
  gUsbConfigTemplate.coreNum = CoreNum;
  gUsbConfigTemplate.modeType = ModeType;
  gUsbConfigDevice[CoreNum].clientHandle = NULL;
  gUsbConfigDevice[CoreNum].usbConfig = gUsbConfigTemplate;

  //vsb: do not install device path since separate client handles are used per each HW controller
  //no need for device path in this case.
  Status = gBS->InstallMultipleProtocolInterfaces(&gUsbConfigDevice[CoreNum].clientHandle ,              //if handle is NULL, new one is allocated
                                &gQcomUsbConfigProtocolGuid,  &gUsbConfigDevice[CoreNum].usbConfig,
                                NULL);
  ERR_CHK ( "Failed to Install QcomUsbConfig protocol"); 

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}





/**
  This routine toggles the USB mode configuration on the core specified by CoreNum  

  @param  CoreNum             USB core number  

  @retval EFI_SUCCESS         Operation succeeded. 
  @retval Others              Operation failed.

**/
EFI_STATUS
EFIAPI UsbToggleControllerMode(
  IN   QCOM_USB_CORE_NUM          CoreNum
  )
{
  EFI_STATUS             Status       = EFI_SUCCESS;
  QCOM_USB_MODE_TYPE     CurrModeType = USB_INVALID_MODE;
  QCOM_USB_MODE_TYPE     NextModeType = USB_INVALID_MODE;

  FNC_ENTER_MSG ();

  if (CoreNum > USB_CORE_1) {
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }

  if (gUsbConfigTemplate.modeType == USB_INVALID_MODE) {
    Status = EFI_NOT_READY;
    goto ON_EXIT;
  }

  // store the current mode type. UsbStopController will set the 
  // gUsbConfigTemplate.modeType to USB_INVALID_MODE
  CurrModeType = gUsbConfigTemplate.modeType; 

  // The new mode on the core:
  // For 8996, only switch from USB_DEVICE_MODE_SS to USB_HOST_MODE_XHCI
  switch (CurrModeType) {
    case USB_DEVICE_MODE_SS:
    NextModeType = USB_HOST_MODE_XHCI;
    break;
    case USB_DEVICE_MODE_HS:    
    case USB_HOST_MODE_XHCI:
    case USB_HOST_MODE_EHCI:
    NextModeType = USB_INVALID_MODE;
    break;
    default:
    NextModeType = USB_INVALID_MODE;
  }

  if (NextModeType == USB_INVALID_MODE) {
    Status = EFI_UNSUPPORTED;
    goto ON_EXIT;
  }

  Status = UsbStopController(CoreNum, CurrModeType);
  if (EFI_ERROR(Status)) {
    DEBUG((EFI_D_ERROR,
      "UsbToggleControllerMode: Unable to stop controller on core %d, Status =  (%r)\r\n", CoreNum, Status));
    goto ON_EXIT;
  }

  Status = UsbStartController(CoreNum, NextModeType);
  if (EFI_ERROR (Status)) {
    DEBUG(( EFI_D_ERROR,
      "UsbToggleControllerMode: Unable to start controller on core %d, Status =  (%r)\r\n", CoreNum, Status));
    goto ON_EXIT;
  }
  
ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}
