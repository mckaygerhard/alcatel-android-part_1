/*
==============================================================================

FILE:         HALclkVIDEOSUBCORE0.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   VIDEOSUBCORE0 clocks.

   List of clock domains:
     - HAL_clk_mMMSSVIDEOSUBCORE0ClkDomain


   List of power domains:
     - HAL_clk_mMMSSVIDEOSUBCORE0PowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl_3;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO_3;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mVIDEOSUBCORE0ClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mVIDEOSUBCORE0ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "video_subcore0_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_VIDEO_SUBCORE0_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_VIDEO_SUBCORE0_CLK
  },
};


/*
 * HAL_clk_mMMSSVIDEOSUBCORE0ClkDomain
 *
 * VIDEOSUBCORE0 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSVIDEOSUBCORE0ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_VIDEO_SUBCORE0_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mVIDEOSUBCORE0ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mVIDEOSUBCORE0ClkDomainClks)/sizeof(HAL_clk_mVIDEOSUBCORE0ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_3,
  /* .pmNextClockDomain    = */ NULL
};


/*
 * HAL_clk_mMMSSVIDEOSUBCORE0PowerDomain
 *
 * VIDEO_SUBCORE0 power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mMMSSVIDEOSUBCORE0PowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_VIDEO_SUBCORE0",
  /* .nGDSCRAddr              = */ HWIO_OFFS(MMSS_VIDEO_SUBCORE0_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

