/*
==============================================================================

FILE:         HALclkMMAGICVIDEO.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MMAGICVIDEO clocks.

   List of clock domains:


   List of power domains:
     - HAL_clk_mMMSSMMAGICVIDEOPowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/



/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_clk_mMMSSMMAGICVIDEOPowerDomain
 *
 * MMAGIC_VIDEO power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mMMSSMMAGICVIDEOPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_MMAGIC_VIDEO",
  /* .nGDSCRAddr              = */ HWIO_OFFS(MMSS_MMAGIC_VIDEO_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

