/*
==============================================================================

FILE:         HALclkUFS.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the
   UFS clocks.

   List of clock domains:
     - HAL_clk_mGCCUFSAXIClkDomain
     - HAL_clk_mGCCUFSICECOREClkDomain
     - HAL_clk_mGCCUFSRXSYMBOL0ClkDomain
     - HAL_clk_mGCCUFSRXSYMBOL1ClkDomain
     - HAL_clk_mGCCUFSTXSYMBOL0ClkDomain


   List of power domains:
     - HAL_clk_mGCCUFSPowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 *  HAL_clk_mUFSAXIClkDomainClks
 *
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mUFSAXIClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_aggre2_ufs_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_AGGRE2_UFS_AXI_CBCR), HWIO_OFFS(GCC_AGGRE2_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_AGGRE2_UFS_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_sys_noc_ufs_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_SYS_NOC_UFS_AXI_CBCR), HWIO_OFFS(GCC_SYSTEM_NOC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_SYS_NOC_UFS_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_ufs_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_UFS_AXI_CBCR), HWIO_OFFS(GCC_UFS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_UFS_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_ufs_rx_cfg_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_UFS_RX_CFG_CBCR), HWIO_OFFS(GCC_UFS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_UFS_RX_CFG_CLK
  },
  {
    /* .szClockName      = */ "gcc_ufs_tx_cfg_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_UFS_TX_CFG_CBCR), HWIO_OFFS(GCC_UFS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_UFS_TX_CFG_CLK
  },
};


/*
 * HAL_clk_mGCCUFSAXIClkDomain
 *
 * UFSAXI clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCUFSAXIClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_UFS_AXI_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mUFSAXIClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mUFSAXIClkDomainClks)/sizeof(HAL_clk_mUFSAXIClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*
 *  HAL_clk_mUFSICECOREClkDomainClks
 *
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mUFSICECOREClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_ufs_ice_core_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_UFS_ICE_CORE_CBCR), HWIO_OFFS(GCC_UFS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_UFS_ICE_CORE_CLK
  },
  {
    /* .szClockName      = */ "gcc_ufs_unipro_core_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_UFS_UNIPRO_CORE_CBCR), HWIO_OFFS(GCC_UFS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_UFS_UNIPRO_CORE_CLK
  },
};


/*
 * HAL_clk_mGCCUFSICECOREClkDomain
 *
 * UFSICECORE clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCUFSICECOREClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_UFS_ICE_CORE_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mUFSICECOREClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mUFSICECOREClkDomainClks)/sizeof(HAL_clk_mUFSICECOREClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*
 *  HAL_clk_mUFSRXSYMBOL0ClkDomainClks
 *
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mUFSRXSYMBOL0ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_ufs_rx_symbol_0_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_UFS_RX_SYMBOL_0_CBCR), HWIO_OFFS(GCC_UFS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_UFS_RX_SYMBOL_0_CLK
  },
};


/*
 * HAL_clk_mGCCUFSRXSYMBOL0ClkDomain
 *
 * UFSRXSYMBOL0 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCUFSRXSYMBOL0ClkDomain =
{
  /* .nCGRAddr             = */ 0, /* this domain does not have a cmd rcgr */
  /* .pmClocks             = */ HAL_clk_mUFSRXSYMBOL0ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mUFSRXSYMBOL0ClkDomainClks)/sizeof(HAL_clk_mUFSRXSYMBOL0ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*
 *  HAL_clk_mUFSRXSYMBOL1ClkDomainClks
 *
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mUFSRXSYMBOL1ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_ufs_rx_symbol_1_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_UFS_RX_SYMBOL_1_CBCR), HWIO_OFFS(GCC_UFS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_UFS_RX_SYMBOL_1_CLK
  },
};


/*
 * HAL_clk_mGCCUFSRXSYMBOL1ClkDomain
 *
 * UFSRXSYMBOL1 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCUFSRXSYMBOL1ClkDomain =
{
  /* .nCGRAddr             = */ 0, /* this domain does not have a cmd rcgr */
  /* .pmClocks             = */ HAL_clk_mUFSRXSYMBOL1ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mUFSRXSYMBOL1ClkDomainClks)/sizeof(HAL_clk_mUFSRXSYMBOL1ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*
 *  HAL_clk_mUFSTXSYMBOL0ClkDomainClks
 *
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mUFSTXSYMBOL0ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_ufs_tx_symbol_0_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_UFS_TX_SYMBOL_0_CBCR), HWIO_OFFS(GCC_UFS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_UFS_TX_SYMBOL_0_CLK
  },
};


/*
 * HAL_clk_mGCCUFSTXSYMBOL0ClkDomain
 *
 * UFSTXSYMBOL0 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCUFSTXSYMBOL0ClkDomain =
{
  /* .nCGRAddr             = */ 0, /* this domain does not have a cmd rcgr */
  /* .pmClocks             = */ HAL_clk_mUFSTXSYMBOL0ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mUFSTXSYMBOL0ClkDomainClks)/sizeof(HAL_clk_mUFSTXSYMBOL0ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*
 * HAL_clk_mGCCUFSPowerDomain
 *
 * UFS power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mGCCUFSPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_UFS",
  /* .nGDSCRAddr              = */ HWIO_OFFS(GCC_UFS_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

