/*
==============================================================================

FILE:         HALclkGPUAON.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   GPUAON clocks.

   List of clock domains:
     - HAL_clk_mMMSSISENSEClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl_4;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO_4;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mISENSEClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mISENSEClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gpu_aon_isense_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_GPU_AON_ISENSE_CBCR), HWIO_OFFS(MMSS_GPU_AON_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_GPU_AON_ISENSE_CLK
  },
};


/*
 * HAL_clk_mMMSSISENSEClkDomain
 *
 * ISENSE clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSISENSEClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_ISENSE_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mISENSEClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mISENSEClkDomainClks)/sizeof(HAL_clk_mISENSEClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_4,
  /* .pmNextClockDomain    = */ NULL
};

