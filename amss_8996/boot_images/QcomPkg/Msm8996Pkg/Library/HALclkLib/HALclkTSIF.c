/*
==============================================================================

FILE:         HALclkTSIF.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   TSIF clocks.

   List of clock domains:
     - HAL_clk_mGCCTSIFREFClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl_1;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControlRO_1;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mTSIFREFClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mTSIFREFClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_tsif_ref_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_TSIF_REF_CBCR), HWIO_OFFS(GCC_TSIF_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_TSIF_REF_CLK
  },
};


/*
 * HAL_clk_mGCCTSIFREFClkDomain
 *
 * TSIFREF clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCTSIFREFClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_TSIF_REF_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mTSIFREFClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mTSIFREFClkDomainClks)/sizeof(HAL_clk_mTSIFREFClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl_1,
  /* .pmNextClockDomain    = */ NULL
};

