/*
==============================================================================

FILE:         HALclkMMAGICMAXI.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MMAGICMAXI clocks.

   List of clock domains:
     - HAL_clk_mMMSSMAXIClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mMAXIClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mMAXIClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mmss_mmagic_maxi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_MMAGIC_MAXI_CBCR), HWIO_OFFS(MMSS_MMAGICMAXI_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_MMAGIC_MAXI_CLK
  },
  {
    /* .szClockName      = */ "mmss_spdm_rm_maxi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_RM_MAXI_CBCR), HWIO_OFFS(MMSS_MMSS_SPDM_RM_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_SPDM_RM_MAXI_CLK
  },
  {
    /* .szClockName      = */ "video_maxi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_VIDEO_MAXI_CBCR), HWIO_OFFS(MMSS_VIDEO_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_VIDEO_MAXI_CLK
  },
  {
    /* .szClockName      = */ "vmem_maxi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_VMEM_MAXI_CBCR), HWIO_OFFS(MMSS_VMEM_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_VMEM_MAXI_CLK
  },
};


/*
 * HAL_clk_mMMSSMAXIClkDomain
 *
 * MAXI clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSMAXIClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_MAXI_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mMAXIClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mMAXIClkDomainClks)/sizeof(HAL_clk_mMAXIClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

