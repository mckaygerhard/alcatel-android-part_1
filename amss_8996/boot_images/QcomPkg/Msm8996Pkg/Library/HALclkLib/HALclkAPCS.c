/*
==============================================================================

FILE:         HALclkAPCS.c

DESCRIPTION:
  This file contains the clock HAL code to control the APCS clocks.


==============================================================================

                             Edit History


==============================================================================
   Copyright (c) 2014-2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"
#include "HALclkAPCS.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/

/*
 * Macros to store/retrieve multiple mux select values in single variable.
 * If the secondary mux isn't selected, then use '0' as a "don't care".
 */
#define APCS_MUX_SEL(pri, sec)      (((pri) << 16) | (sec))
#define APCS_MUX_SEL_PRIMARY(sel)   ((sel) >> 16)
#define APCS_MUX_SEL_SECONDARY(sel) ((sel) & 0xFFFF)


/* ============================================================================
**    Prototypes
** ==========================================================================*/

static void HAL_clk_APCSConfigMux       (HAL_clk_ClockDomainDescType *pmClockDomainDesc, const HAL_clk_ClockMuxConfigType *pmConfig);
static void HAL_clk_APCSDetectMuxConfig (HAL_clk_ClockDomainDescType *pmClockDomainDesc, HAL_clk_ClockMuxConfigType  *pmConfig);


/* ============================================================================
**    Typedefs
** ==========================================================================*/

/*
 * HAL_clk_APCSClusterDomainDescType
 *
 */
typedef struct HAL_clk_APCSClusterDomainDescType
{
  HAL_clk_ClockDomainDescType mDomain; /* Must be first element. */
  const HAL_clk_APCSCPUType   eCPU;
} HAL_clk_APCSClusterDomainDescType;


/* ============================================================================
**    Data
** ==========================================================================*/

/*
 * aClusterSourceMap
 *
 * APCS cluster HW source mapping.
 */
static HAL_clk_SourceMapType aClusterSourceMap[] =
{
  {HAL_CLK_SOURCE_XO,                 APCS_MUX_SEL(0, 0)},
  {HAL_CLK_SOURCE_GPLL0,              APCS_MUX_SEL(0, 3)},
  {HAL_CLK_SOURCE_APCSPRIPLL0,        APCS_MUX_SEL(0, 1)},
  {HAL_CLK_SOURCE_APCSPRIPLL0_EARLY,  APCS_MUX_SEL(1, 0)},
  {HAL_CLK_SOURCE_APCSSECPLL0,        APCS_MUX_SEL(3, 0)},
  {HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID}
};


/*
 * HAL_clk_mAPCSClusterDomainControl
 *
 * Functions for controlling APCS clock domains
 */
static HAL_clk_ClockDomainControlType HAL_clk_mAPCSClusterDomainControl =
{
   /* .ConfigMux          = */ HAL_clk_APCSConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_APCSDetectMuxConfig,
   /* .pmSourceMap        = */ aClusterSourceMap,
};


/*
 * aCBFSourceMap
 *
 * APCS CBF HW source mapping
 */
static HAL_clk_SourceMapType aCBFSourceMap[] =
{
  {HAL_CLK_SOURCE_XO,               APCS_MUX_SEL(0, 0)},
  {HAL_CLK_SOURCE_GPLL0,            APCS_MUX_SEL(3, 0)},
  {HAL_CLK_SOURCE_APCSCBFPLL,       APCS_MUX_SEL(2, 0)},
  {HAL_CLK_SOURCE_APCSCBFPLL_EARLY, APCS_MUX_SEL(1, 0)},
  {HAL_CLK_SOURCE_NULL,             HAL_CLK_SOURCE_INDEX_INVALID}
};


/*
 * HAL_clk_mAPCSCBFDomainControl
 *
 * Functions for controlling APCS clock domains
 */
static HAL_clk_ClockDomainControlType HAL_clk_mAPCSCBFDomainControl =
{
   /* .ConfigMux          = */ HAL_clk_APCSConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_APCSDetectMuxConfig,
   /* .pmSourceMap        = */ aCBFSourceMap,
};


/*
 *  HAL_clk_mAPCSCluster0DomainClocks
 *
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mAPCSCluster0DomainClks[] =
{
  {
    /* .szClockName      = */ "apcs_cluster0_clk",
    /* .mRegisters       = */ { 0 },
    /* .pmControl        = */ NULL,
    /* .nTestClock       = */ HAL_CLK_APCS_TEST_CLUSTER0_CLK,
  },
};


/*
 * HAL_clk_mAPCSCluster0ClkDomain
 *
 * APCS cluster 0 clock domain.
 */
HAL_clk_APCSClusterDomainDescType HAL_clk_mAPCSCluster0ClkDomain =
{
  /* .mDomain = */
  {
    /* .nCGRAddr             = */ HWIO_OFFS(APC0_QLL_CLKSEL),
    /* .pmClocks             = */ HAL_clk_mAPCSCluster0DomainClks,
    /* .nClockCount          = */ sizeof(HAL_clk_mAPCSCluster0DomainClks)/sizeof(HAL_clk_mAPCSCluster0DomainClks[0]),
    /* .pmControl            = */ &HAL_clk_mAPCSClusterDomainControl,
    /* .pmNextClockDomain    = */ NULL
  },

  /* .eCPU  = */ HAL_CLK_APCS_CPU_CLUSTER0
};


/*
 *  HAL_clk_mAPCSCluster1DomainClocks
 *
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mAPCSCluster1DomainClks[] =
{
  {
    /* .szClockName      = */ "apcs_cluster1_clk",
    /* .mRegisters       = */ { 0 },
    /* .pmControl        = */ NULL,
    /* .nTestClock       = */ HAL_CLK_APCS_TEST_CLUSTER1_CLK,
  },
};


/*
 * HAL_clk_mAPCSCluster1ClkDomain
 *
 * APCS cluster 1 clock domain.
 */
static HAL_clk_APCSClusterDomainDescType HAL_clk_mAPCSCluster1ClkDomain =
{
  /* .mDomain = */
  {
    /* .nCGRAddr             = */ HWIO_OFFS(APC1_QLL_CLKSEL),
    /* .pmClocks             = */ HAL_clk_mAPCSCluster1DomainClks,
    /* .nClockCount          = */ sizeof(HAL_clk_mAPCSCluster1DomainClks)/sizeof(HAL_clk_mAPCSCluster1DomainClks[0]),
    /* .pmControl            = */ &HAL_clk_mAPCSClusterDomainControl,
    /* .pmNextClockDomain    = */ NULL
  },

  /* .eCPU  = */ HAL_CLK_APCS_CPU_CLUSTER1
};


/*
 *  HAL_clk_mAPCSCBFDomainClocks
 *
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mAPCSCBFDomainClks[] =
{
  {
    /* .szClockName      = */ "apcs_cbf_clk",
    /* .mRegisters       = */ { 0 },
    /* .pmControl        = */ NULL,
    /* .nTestClock       = */ HAL_CLK_APCS_TEST_CBF_CLK,
  },
};


/*
 * HAL_clk_mAPCSCBFClkDomain
 *
 * APCS CBF clock domain.
 */
static HAL_clk_APCSClusterDomainDescType HAL_clk_mAPCSCBFClkDomain =
{
  /* .mDomain = */
  {
    /* .nCGRAddr             = */ HWIO_OFFS(APCS_CBF_GFMUX_CTL),
    /* .pmClocks             = */ HAL_clk_mAPCSCBFDomainClks,
    /* .nClockCount          = */ sizeof(HAL_clk_mAPCSCBFDomainClks)/sizeof(HAL_clk_mAPCSCBFDomainClks[0]),
    /* .pmControl            = */ &HAL_clk_mAPCSCBFDomainControl,
    /* .pmNextClockDomain    = */ NULL
  },

  /* .eCPU  = */ HAL_CLK_APCS_CPU_CBF
};


/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/

/* ===========================================================================
**  HAL_clk_IsClusterSource
**
** ======================================================================== */

static boolean HAL_clk_IsClusterSource
(
  HAL_clk_SourceType eSource
)
{
  return eSource >= HAL_CLK_SOURCE_BEGIN_CLUSTER_SOURCES &&
         eSource <= HAL_CLK_SOURCE_END_CLUSTER_SOURCES;

} /* END HAL_clk_IsClusterSource */


/* ===========================================================================
**  HAL_clk_APCSDetectMuxConfig
**
** ======================================================================== */

static void HAL_clk_APCSDetectMuxConfig
(
  HAL_clk_ClockDomainDescType *pmClockDomainDesc,
  HAL_clk_ClockMuxConfigType  *pmConfig
)
{
  HAL_clk_APCSClusterDomainDescType *pmAPCSClusterDomainDesc;
  HAL_clk_SourceMapType *pmSourceMap;
  uint32 nSecondary, nPrimary;
  size_t nAddr;
  uint32 nVal;

  /*
   * Sanity check
   */
  if((pmConfig                                    == NULL) ||
     (pmClockDomainDesc                           == NULL) ||
     (pmClockDomainDesc->nCGRAddr                 == 0   ) ||
     (pmClockDomainDesc->pmControl                == NULL) ||
     (pmClockDomainDesc->pmControl->pmSourceMap   == NULL) )
  {
    return;
  }

  pmAPCSClusterDomainDesc = (HAL_clk_APCSClusterDomainDescType *)pmClockDomainDesc;
  nAddr = pmClockDomainDesc->nCGRAddr;
  nVal = inpdw(nAddr);

  nPrimary = (nVal & HWIO_FMSK(APC0_QLL_CLKSEL, PRISRCSEL_1_0)) >>
    HWIO_SHFT(APC0_QLL_CLKSEL, PRISRCSEL_1_0);

  if (nPrimary == 0)
  {
    nSecondary = (nVal & HWIO_FMSK(APC0_QLL_CLKSEL, SECSRCSEL_1_0)) >>
      HWIO_SHFT(APC0_QLL_CLKSEL, SECSRCSEL_1_0);
  }
  else
  {
    /*
     * The secondary source isn't selected, so it's a "don't care".
     */
    nSecondary = 0;
  }

  pmSourceMap = pmClockDomainDesc->pmControl->pmSourceMap;
  pmConfig->eSource =
    HAL_clk_GenericSourceMapFromHW(pmSourceMap, APCS_MUX_SEL(nPrimary, nSecondary));

  /*
   * Map to cluster-specific PLL.
   */
  if (HAL_clk_IsClusterSource(pmConfig->eSource))
  {
    pmConfig->eSource += pmAPCSClusterDomainDesc->eCPU;
  }

  pmConfig->nDiv2x = 0;
  pmConfig->nM = 0;
  pmConfig->nN = 0;
  pmConfig->n2D = 0;

} /* END HAL_clk_APCSDetectMuxConfig */


/* ===========================================================================
**  HAL_clk_APCSConfigMux
**
** ======================================================================== */

static void HAL_clk_APCSConfigMux
(
  HAL_clk_ClockDomainDescType      *pmClockDomainDesc,
  const HAL_clk_ClockMuxConfigType *pmConfig
)
{
  HAL_clk_APCSClusterDomainDescType *pmAPCSClusterDomainDesc;
  HAL_clk_SourceMapType *pmSourceMap;
  HAL_clk_SourceType eSource;
  size_t nAddr;
  uint32 nVal, nSel;

  /*
   * Sanity check
   */
  if((pmConfig                                    == NULL) ||
     (pmClockDomainDesc                           == NULL) ||
     (pmClockDomainDesc->nCGRAddr                 == 0   ) ||
     (pmClockDomainDesc->pmControl                == NULL) ||
     (pmClockDomainDesc->pmControl->pmSourceMap   == NULL) )
  {
    return;
  }

  pmAPCSClusterDomainDesc = (HAL_clk_APCSClusterDomainDescType *)pmClockDomainDesc;
  nAddr = pmClockDomainDesc->nCGRAddr;
  nVal = inpdw(nAddr);

  /*
   * Map from cluster-specfic PLL to cluster 0 PLL for the mapping lookup.
   */
  eSource = pmConfig->eSource;
  if (HAL_clk_IsClusterSource(eSource))
  {
    eSource -= pmAPCSClusterDomainDesc->eCPU;
  }

  pmSourceMap = pmClockDomainDesc->pmControl->pmSourceMap;
  nSel = HAL_clk_GenericSourceMapToHW(pmSourceMap, eSource);

  nVal &= ~HWIO_FMSK(APC0_QLL_CLKSEL, PRISRCSEL_1_0);
  nVal &= ~HWIO_FMSK(APC0_QLL_CLKSEL, SECSRCSEL_1_0);
  nVal |= HWIO_FVAL(APC0_QLL_CLKSEL, PRISRCSEL_1_0, APCS_MUX_SEL_PRIMARY(nSel));
  nVal |= HWIO_FVAL(APC0_QLL_CLKSEL, SECSRCSEL_1_0, APCS_MUX_SEL_SECONDARY(nSel));
  outpdw(nAddr, nVal);

} /* END HAL_clk_APCSConfigMux */


/* ===========================================================================
**  Function : HAL_clk_KPSSAXISleepEnable
**
** ======================================================================== */

void HAL_clk_KPSSAXISleepEnable(boolean bEnable)
{
  if (bEnable)
  {
    HWIO_OUTF(GCC_APCS_CLOCK_SLEEP_ENA_VOTE, HMSS_MSTR_AXI_CLK_SLEEP_ENA, 1);
  }
  else
  {
    HWIO_OUTF(GCC_APCS_CLOCK_SLEEP_ENA_VOTE, HMSS_MSTR_AXI_CLK_SLEEP_ENA, 0);
  }
} /* End HAL_clk_KPSSAXISleepEnable */


/* ===========================================================================
**  Function : HAL_clk_ApplyMemAccValue
**
** ======================================================================== */

void HAL_clk_ApplyMemAccValue
(
  HAL_clk_APCSCPUType eCore,
  HAL_clk_VRegLevelType eVRegLevelPrev,
  HAL_clk_VRegLevelType eVRegLevelNext
)
{
} /* End HAL_clk_ApplyMemAccValue */


/* ===========================================================================
**  HAL_clk_PlatformInitAPCSMain
**
** ======================================================================== */

void HAL_clk_PlatformInitAPCSMain (void)
{
  HAL_clk_InstallClockDomain(&HAL_clk_mAPCSCluster0ClkDomain.mDomain, HMSS_QLL_BASE);
  HAL_clk_InstallClockDomain(&HAL_clk_mAPCSCluster1ClkDomain.mDomain, HMSS_QLL_BASE);
  HAL_clk_InstallClockDomain(&HAL_clk_mAPCSCBFClkDomain.mDomain, HMSS_BASE);

} /* END HAL_clkPlatformInitAPCSMain */

