/*
==============================================================================

FILE:         HALclkGCCMain.c

DESCRIPTION:
   The main auto-generated file for GCC.


==============================================================================


==============================================================================
            Copyright (c) 2014-2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"
#include "DDIChipInfo.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/



/*
 * Clock domains
 */
//extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBIMCClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBIMCHMSSAXIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP1I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP1SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP2I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP2SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP3I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP3SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP4I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP4SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP5I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP5SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP6I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1QUP6SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1UART1APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1UART2APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1UART3APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1UART4APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1UART5APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP1UART6APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP1I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP1SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP2I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP2SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP3I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP3SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP4I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP4SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP5I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP5SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP6I2CAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP6SPIAPPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2UART1APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2UART2APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2UART3APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2UART4APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2UART5APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2UART6APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCCE1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCCONFIGNOCClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCGCCSLEEPClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCGCCXOClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCGP1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCGP2ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCGP3ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCHMSSAHBClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCHMSSRBCPRClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCMMSSBIMCGFXClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCPCIE0PIPEClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCPCIE1PIPEClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCPCIE2PIPEClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCPCIEAUXClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCPDM2ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCPERIPHNOCClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCQDSSATClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCQSPISERClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSDCC1APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSDCC1ICECOREClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSDCC2APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSDCC3APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSDCC4APPSClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSPMISERClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCSYSTEMNOCClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCTSIFREFClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCUFSAXIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCUFSICECOREClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCUFSRXSYMBOL0ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCUFSRXSYMBOL1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCUFSTXSYMBOL0ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCUSB20MASTERClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCUSB20MOCKUTMIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCUSB30MASTERClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCUSB30MOCKUTMIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCUSB3PHYAUXClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mGCCUSB3PHYPIPEClkDomain;

/*
 * Power domains
 */
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCAGGRE0NOCPowerDomain;
#if 0 /* not present on apcs */
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCBIMCPowerDomain;
#endif
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCPCIE0PowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCPCIE1PowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCPCIE2PowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCUFSPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mGCCUSB30PowerDomain;


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * aGCCSourceMap
 *
 * GCC HW source mapping
 *
 */
static HAL_clk_SourceMapType aGCCSourceMap[] =
{
  { HAL_CLK_SOURCE_XO,                 0 },
  { HAL_CLK_SOURCE_GPLL0,              1 },
  { HAL_CLK_SOURCE_GPLL2,              2 },
  { HAL_CLK_SOURCE_GPLL3,              3 },
  { HAL_CLK_SOURCE_GPLL1,              4 },
  { HAL_CLK_SOURCE_GPLL4,              5 },
  { HAL_CLK_SOURCE_GPLL0_DIV2,         6 },
  { HAL_CLK_SOURCE_PLLTEST,            7 },
  { HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID }
};


/*
 * HAL_clk_mGCCClockDomainControl
 *
 * Functions for controlling GCC clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mGCCClockDomainControl =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aGCCSourceMap
};


/*
 * HAL_clk_mGCCClockDomainControlRO
 *
 * Read-only functions for GCC clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mGCCClockDomainControlRO =
{
   /* .ConfigMux          = */ NULL,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aGCCSourceMap
};


/*
 * aGCCSourceMap_1
 *
 * GCC HW source mapping
 *
 */
static HAL_clk_SourceMapType aGCCSourceMap_1[] =
{
  { HAL_CLK_SOURCE_XO,                 0 },
  { HAL_CLK_SOURCE_GPLL0,              1 },
  { HAL_CLK_SOURCE_EXTERNAL2,          2 },
  { HAL_CLK_SOURCE_GROUND,             3 },
  { HAL_CLK_SOURCE_GROUND,             4 },
  { HAL_CLK_SOURCE_SLEEPCLK,           5 },
  { HAL_CLK_SOURCE_GPLL0_DIV2,         6 },
  { HAL_CLK_SOURCE_PLLTEST,            7 },
  { HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID }
};


/*
 * HAL_clk_mGCCClockDomainControl_1
 *
 * Functions for controlling GCC clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mGCCClockDomainControl_1 =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aGCCSourceMap_1
};


/*
 * HAL_clk_mGCCClockDomainControlRO_1
 *
 * Read-only functions for GCC clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mGCCClockDomainControlRO_1 =
{
   /* .ConfigMux          = */ NULL,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aGCCSourceMap_1
};


/*
 * aGCCSourceMap_2
 *
 * GCC HW source mapping
 *
 */
static HAL_clk_SourceMapType aGCCSourceMap_2[] =
{
  { HAL_CLK_SOURCE_XO,                 0 },
  { HAL_CLK_SOURCE_GPLL0,              1 },
  { HAL_CLK_SOURCE_GPLL2,              2 },
  { HAL_CLK_SOURCE_GPLL3,              3 },
  { HAL_CLK_SOURCE_GPLL1,              4 },
  { HAL_CLK_SOURCE_GPLL2_EARLY,        5 },
  { HAL_CLK_SOURCE_GPLL0_DIV2,         6 },
  { HAL_CLK_SOURCE_PLLTEST,            7 },
  { HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID }
};


/*
 * HAL_clk_mGCCClockDomainControl_2
 *
 * Functions for controlling GCC clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mGCCClockDomainControl_2 =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aGCCSourceMap_2
};


/*
 * HAL_clk_mGCCClockDomainControlRO_2
 *
 * Read-only functions for GCC clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mGCCClockDomainControlRO_2 =
{
   /* .ConfigMux          = */ NULL,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aGCCSourceMap_2
};


/*
 * aGCCSourceMap_3
 *
 * GCC HW source mapping
 *
 */
static HAL_clk_SourceMapType aGCCSourceMap_3[] =
{
  { HAL_CLK_SOURCE_XO,                 0 },
  { HAL_CLK_SOURCE_GPLL0,              1 },
  { HAL_CLK_SOURCE_GROUND,             2 },
  { HAL_CLK_SOURCE_GPLL1_DIV2,         3 },
  { HAL_CLK_SOURCE_GPLL1,              4 },
  { HAL_CLK_SOURCE_GPLL4,              5 },
  { HAL_CLK_SOURCE_GPLL0_DIV2,         6 },
  { HAL_CLK_SOURCE_PLLTEST,            7 },
  { HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID }
};


/*
 * HAL_clk_mGCCClockDomainControl_3
 *
 * Functions for controlling GCC clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mGCCClockDomainControl_3 =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aGCCSourceMap_3
};


/*
 * HAL_clk_mGCCClockDomainControlRO_3
 *
 * Read-only functions for GCC clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mGCCClockDomainControlRO_3 =
{
   /* .ConfigMux          = */ NULL,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aGCCSourceMap_3
};

/*
 * HAL_clk_aGCCClockDomainDesc_V1
 *
 * List of GCC clock domains V1
*/
static HAL_clk_ClockDomainDescType * HAL_clk_aGCCClockDomainDesc_V1 [] =
{
  &HAL_clk_mGCCBIMCHMSSAXIClkDomain,
  &HAL_clk_mGCCBLSP1QUP1I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP1SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP2I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP2SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP3I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP3SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP4I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP4SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP5I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP5SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP6I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP6SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1UART1APPSClkDomain,
  &HAL_clk_mGCCBLSP1UART2APPSClkDomain,
  &HAL_clk_mGCCBLSP1UART3APPSClkDomain,
  &HAL_clk_mGCCBLSP1UART4APPSClkDomain,
  &HAL_clk_mGCCBLSP1UART5APPSClkDomain,
  &HAL_clk_mGCCBLSP1UART6APPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP1I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP1SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP2I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP2SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP3I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP3SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP4I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP4SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP5I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP5SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP6I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP6SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2UART1APPSClkDomain,
  &HAL_clk_mGCCBLSP2UART2APPSClkDomain,
  &HAL_clk_mGCCBLSP2UART3APPSClkDomain,
  &HAL_clk_mGCCBLSP2UART4APPSClkDomain,
  &HAL_clk_mGCCBLSP2UART5APPSClkDomain,
  &HAL_clk_mGCCBLSP2UART6APPSClkDomain,
  &HAL_clk_mGCCMMSSBIMCGFXClkDomain,
  &HAL_clk_mGCCCE1ClkDomain,
  &HAL_clk_mGCCCONFIGNOCClkDomain,
  &HAL_clk_mGCCGCCSLEEPClkDomain,
  &HAL_clk_mGCCGP1ClkDomain,
  &HAL_clk_mGCCGP2ClkDomain,
  &HAL_clk_mGCCGP3ClkDomain,
  &HAL_clk_mGCCHMSSAHBClkDomain,
  &HAL_clk_mGCCHMSSRBCPRClkDomain,
  &HAL_clk_mGCCPCIE0PIPEClkDomain,
  &HAL_clk_mGCCPCIE1PIPEClkDomain,
  &HAL_clk_mGCCPCIE2PIPEClkDomain,
  &HAL_clk_mGCCPCIEAUXClkDomain,
  &HAL_clk_mGCCPDM2ClkDomain,
  &HAL_clk_mGCCPERIPHNOCClkDomain,
  &HAL_clk_mGCCSDCC1APPSClkDomain,
  &HAL_clk_mGCCSDCC2APPSClkDomain,
  &HAL_clk_mGCCSDCC3APPSClkDomain,
  &HAL_clk_mGCCSDCC4APPSClkDomain,
  &HAL_clk_mGCCSYSTEMNOCClkDomain,
  &HAL_clk_mGCCTSIFREFClkDomain,
  &HAL_clk_mGCCUFSAXIClkDomain,
  &HAL_clk_mGCCUFSICECOREClkDomain,
  &HAL_clk_mGCCUFSRXSYMBOL0ClkDomain,
  &HAL_clk_mGCCUFSRXSYMBOL1ClkDomain,
  &HAL_clk_mGCCUFSTXSYMBOL0ClkDomain,
  &HAL_clk_mGCCUSB20MASTERClkDomain,
  &HAL_clk_mGCCUSB20MOCKUTMIClkDomain,
  &HAL_clk_mGCCUSB30MASTERClkDomain,
  &HAL_clk_mGCCUSB30MOCKUTMIClkDomain,
  &HAL_clk_mGCCUSB3PHYAUXClkDomain,
  &HAL_clk_mGCCUSB3PHYPIPEClkDomain,
  NULL
};

/*
 * HAL_clk_aGCCClockDomainDesc
 *
 * List of GCC clock domains V2
*/
static HAL_clk_ClockDomainDescType * HAL_clk_aGCCClockDomainDesc_V2 [] =
{
  &HAL_clk_mGCCBIMCHMSSAXIClkDomain,
  &HAL_clk_mGCCBLSP1QUP1I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP1SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP2I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP2SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP3I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP3SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP4I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP4SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP5I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP5SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP6I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP1QUP6SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP1UART1APPSClkDomain,
  &HAL_clk_mGCCBLSP1UART2APPSClkDomain,
  &HAL_clk_mGCCBLSP1UART3APPSClkDomain,
  &HAL_clk_mGCCBLSP1UART4APPSClkDomain,
  &HAL_clk_mGCCBLSP1UART5APPSClkDomain,
  &HAL_clk_mGCCBLSP1UART6APPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP1I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP1SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP2I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP2SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP3I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP3SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP4I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP4SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP5I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP5SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP6I2CAPPSClkDomain,
  &HAL_clk_mGCCBLSP2QUP6SPIAPPSClkDomain,
  &HAL_clk_mGCCBLSP2UART1APPSClkDomain,
  &HAL_clk_mGCCBLSP2UART2APPSClkDomain,
  &HAL_clk_mGCCBLSP2UART3APPSClkDomain,
  &HAL_clk_mGCCBLSP2UART4APPSClkDomain,
  &HAL_clk_mGCCBLSP2UART5APPSClkDomain,
  &HAL_clk_mGCCBLSP2UART6APPSClkDomain,
  &HAL_clk_mGCCCE1ClkDomain,
  &HAL_clk_mGCCCONFIGNOCClkDomain,
  &HAL_clk_mGCCGCCSLEEPClkDomain,
  &HAL_clk_mGCCGCCXOClkDomain,
  &HAL_clk_mGCCGP1ClkDomain,
  &HAL_clk_mGCCGP2ClkDomain,
  &HAL_clk_mGCCGP3ClkDomain,
  &HAL_clk_mGCCHMSSAHBClkDomain,
  &HAL_clk_mGCCHMSSRBCPRClkDomain,
  &HAL_clk_mGCCMMSSBIMCGFXClkDomain,
  &HAL_clk_mGCCPCIE0PIPEClkDomain,
  &HAL_clk_mGCCPCIE1PIPEClkDomain,
  &HAL_clk_mGCCPCIE2PIPEClkDomain,
  &HAL_clk_mGCCPCIEAUXClkDomain,
  &HAL_clk_mGCCPDM2ClkDomain,
  &HAL_clk_mGCCPERIPHNOCClkDomain,
  &HAL_clk_mGCCQDSSATClkDomain,
  &HAL_clk_mGCCQSPISERClkDomain,
  &HAL_clk_mGCCSDCC1APPSClkDomain,
  &HAL_clk_mGCCSDCC1ICECOREClkDomain,
  &HAL_clk_mGCCSDCC2APPSClkDomain,
  &HAL_clk_mGCCSDCC3APPSClkDomain,
  &HAL_clk_mGCCSDCC4APPSClkDomain,
  &HAL_clk_mGCCSPMISERClkDomain,
  &HAL_clk_mGCCSYSTEMNOCClkDomain,
  &HAL_clk_mGCCTSIFREFClkDomain,
  &HAL_clk_mGCCUFSAXIClkDomain,
  &HAL_clk_mGCCUFSICECOREClkDomain,
  &HAL_clk_mGCCUFSRXSYMBOL0ClkDomain,
  &HAL_clk_mGCCUFSRXSYMBOL1ClkDomain,
  &HAL_clk_mGCCUFSTXSYMBOL0ClkDomain,
  &HAL_clk_mGCCUSB20MASTERClkDomain,
  &HAL_clk_mGCCUSB20MOCKUTMIClkDomain,
  &HAL_clk_mGCCUSB30MASTERClkDomain,
  &HAL_clk_mGCCUSB30MOCKUTMIClkDomain,
  &HAL_clk_mGCCUSB3PHYAUXClkDomain,
  &HAL_clk_mGCCUSB3PHYPIPEClkDomain,
  NULL
};


/*
 * HAL_clk_aGCCPowerDomainDesc
 *
 * List of GCC power domains
 */
static HAL_clk_PowerDomainDescType * HAL_clk_aGCCPowerDomainDesc [] =
{
  &HAL_clk_mGCCAGGRE0NOCPowerDomain,
#if 0 /* not present on apcs */
  &HAL_clk_mGCCBIMCPowerDomain,
#endif
  &HAL_clk_mGCCPCIE0PowerDomain,
  &HAL_clk_mGCCPCIE1PowerDomain,
  &HAL_clk_mGCCPCIE2PowerDomain,
  &HAL_clk_mGCCUFSPowerDomain,
  &HAL_clk_mGCCUSB30PowerDomain,
  NULL
};



/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/


/* ===========================================================================
**  HAL_clk_PlatformInitGCCMain
**
** ======================================================================== */

void HAL_clk_PlatformInitGCCMain (void)
{
  DalChipInfoVersionType  nChipVersion;


  nChipVersion = DalChipInfo_ChipVersion();

  /*
   * Install all clock domains
   */
  if ((uint32)nChipVersion >= HAL_CLK_CHIP_VERSION(2,0))
  {
    HAL_clk_InstallClockDomains(HAL_clk_aGCCClockDomainDesc_V2, CLK_CTL_BASE);
  }
  else
  {
    HAL_clk_InstallClockDomains(HAL_clk_aGCCClockDomainDesc_V1, CLK_CTL_BASE);
  }

  /*
   * Install all power domains
   */
  HAL_clk_InstallPowerDomains(HAL_clk_aGCCPowerDomainDesc, CLK_CTL_BASE);

} /* END HAL_clk_PlatformInitGCCMain */

