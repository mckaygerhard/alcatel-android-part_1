/*
==============================================================================

FILE:         HALclkCAMSSJPEG.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   CAMSSJPEG clocks.

   List of clock domains:
     - HAL_clk_mMMSSJPEG0ClkDomain
     - HAL_clk_mMMSSJPEG2ClkDomain
     - HAL_clk_mMMSSJPEGDMAClkDomain


   List of power domains:
     - HAL_clk_mMMSSCAMSSJPEGPowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014-2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl_1;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO_1;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mJPEG0ClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mJPEG0ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "camss_jpeg0_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_JPEG0_CBCR), HWIO_OFFS(MMSS_CAMSS_JPEG_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_JPEG0_CLK
  },
  {
    /* .szClockName      = */ "mmss_spdm_jpeg0_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_JPEG0_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_SPDM_JPEG0_CLK
  },
};


/*
 * HAL_clk_mMMSSJPEG0ClkDomain
 *
 * JPEG0 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSJPEG0ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_JPEG0_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mJPEG0ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mJPEG0ClkDomainClks)/sizeof(HAL_clk_mJPEG0ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_1,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mJPEG2ClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mJPEG2ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "camss_jpeg2_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_JPEG2_CBCR), HWIO_OFFS(MMSS_CAMSS_JPEG_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_JPEG2_CLK
  },
  {
    /* .szClockName      = */ "mmss_spdm_jpeg2_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_JPEG2_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_SPDM_JPEG2_CLK
  },
};


/*
 * HAL_clk_mMMSSJPEG2ClkDomain
 *
 * JPEG2 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSJPEG2ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_JPEG2_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mJPEG2ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mJPEG2ClkDomainClks)/sizeof(HAL_clk_mJPEG2ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_1,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mJPEGDMAClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mJPEGDMAClkDomainClks[] =
{
  {
    /* .szClockName      = */ "camss_jpeg_dma_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_JPEG_DMA_CBCR), HWIO_OFFS(MMSS_CAMSS_JPEG_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_JPEG_DMA_CLK
  },
  {
    /* .szClockName      = */ "mmss_spdm_jpeg_dma_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_JPEG_DMA_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_SPDM_JPEG_DMA_CLK
  },
};


/*
 * HAL_clk_mMMSSJPEGDMAClkDomain
 *
 * JPEGDMA clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSJPEGDMAClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_JPEG_DMA_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mJPEGDMAClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mJPEGDMAClkDomainClks)/sizeof(HAL_clk_mJPEGDMAClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_1,
  /* .pmNextClockDomain    = */ NULL
};


/*
 * HAL_clk_mMMSSCAMSSJPEGPowerDomain
 *
 * CAMSS_JPEG power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mMMSSCAMSSJPEGPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_CAMSS_JPEG",
  /* .nGDSCRAddr              = */ HWIO_OFFS(MMSS_CAMSS_JPEG_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

