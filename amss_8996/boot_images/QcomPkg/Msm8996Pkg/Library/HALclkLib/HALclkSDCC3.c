/*
==============================================================================

FILE:         HALclkSDCC3.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   SDCC3 clocks.

   List of clock domains:
     - HAL_clk_mGCCSDCC3APPSClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mSDCC3APPSClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mSDCC3APPSClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_sdcc3_apps_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_SDCC3_APPS_CBCR), HWIO_OFFS(GCC_SDCC3_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_SDCC3_APPS_CLK
  },
};


/*
 * HAL_clk_mGCCSDCC3APPSClkDomain
 *
 * SDCC3APPS clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCSDCC3APPSClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_SDCC3_APPS_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mSDCC3APPSClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mSDCC3APPSClkDomainClks)/sizeof(HAL_clk_mSDCC3APPSClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

