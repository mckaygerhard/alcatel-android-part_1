/*
==============================================================================

FILE:         HALclkMMAGICMDSS.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MMAGICMDSS clocks.

   List of clock domains:


   List of power domains:
     - HAL_clk_mMMSSMMAGICMDSSPowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/



/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_clk_mMMSSMMAGICMDSSPowerDomain
 *
 * MMAGIC_MDSS power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mMMSSMMAGICMDSSPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_MMAGIC_MDSS",
  /* .nGDSCRAddr              = */ HWIO_OFFS(MMSS_MMAGIC_MDSS_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

