/*
==============================================================================

FILE:         HALclkPCIE0.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the
   PCIE0 clocks.

   List of clock domains:
     - HAL_clk_mGCCPCIE0PIPEClkDomain
     - HAL_clk_mGCCPCIEAUXClkDomain


   List of power domains:
     - HAL_clk_mGCCPCIE0PowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControlRO;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl_1;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControlRO_1;


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 *  HAL_clk_mPCIE0PIPEClkDomainClks
 *
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mPCIE0PIPEClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_pcie_0_pipe_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_PCIE_0_PIPE_CBCR), HWIO_OFFS(GCC_PCIE_0_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_PCIE_0_PIPE_CLK
  },
};


/*
 * HAL_clk_mGCCPCIE0PIPEClkDomain
 *
 * PCIE0PIPE clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCPCIE0PIPEClkDomain =
{
  /* .nCGRAddr             = */ 0, /* this domain does not have a cmd rcgr */
  /* .pmClocks             = */ HAL_clk_mPCIE0PIPEClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mPCIE0PIPEClkDomainClks)/sizeof(HAL_clk_mPCIE0PIPEClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*
 *  HAL_clk_mPCIEAUXClkDomainClks
 *
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mPCIEAUXClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_pcie_0_aux_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_PCIE_0_AUX_CBCR), HWIO_OFFS(GCC_PCIE_0_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_PCIE_0_AUX_CLK
  },
  {
    /* .szClockName      = */ "gcc_pcie_1_aux_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_PCIE_1_AUX_CBCR), HWIO_OFFS(GCC_PCIE_1_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_PCIE_1_AUX_CLK
  },
  {
    /* .szClockName      = */ "gcc_pcie_2_aux_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_PCIE_2_AUX_CBCR), HWIO_OFFS(GCC_PCIE_2_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_PCIE_2_AUX_CLK
  },
  {
    /* .szClockName      = */ "gcc_pcie_phy_aux_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_PCIE_PHY_AUX_CBCR), HWIO_OFFS(GCC_PCIE_PHY_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_PCIE_PHY_AUX_CLK
  },
};


/*
 * HAL_clk_mGCCPCIEAUXClkDomain
 *
 * PCIEAUX clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCPCIEAUXClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_PCIE_AUX_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mPCIEAUXClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mPCIEAUXClkDomainClks)/sizeof(HAL_clk_mPCIEAUXClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl_1,
  /* .pmNextClockDomain    = */ NULL
};


/*
 * HAL_clk_mGCCPCIE0PowerDomain
 *
 * PCIE_0 power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mGCCPCIE0PowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_PCIE_0",
  /* .nGDSCRAddr              = */ HWIO_OFFS(GCC_PCIE_0_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

