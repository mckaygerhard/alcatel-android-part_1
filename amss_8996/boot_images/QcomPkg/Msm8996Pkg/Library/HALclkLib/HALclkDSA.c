/*
==============================================================================

FILE:         HALclkDSA.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   DSA clocks.

   List of clock domains:
     - HAL_clk_mMMSSDSACOREClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mDSACOREClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mDSACOREClkDomainClks[] =
{
  {
    /* .szClockName      = */ "dsa_core_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_DSA_CORE_CBCR), HWIO_OFFS(MMSS_DSA_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_DSA_CORE_CLK
  },
};


/*
 * HAL_clk_mMMSSDSACOREClkDomain
 *
 * DSACORE clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSDSACOREClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_DSA_CORE_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mDSACOREClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mDSACOREClkDomainClks)/sizeof(HAL_clk_mDSACOREClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

