/*
==============================================================================

FILE:         HALclkGCCCLOCKS.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   GCCCLOCKS clocks.

   List of clock domains:
     - HAL_clk_mMMSSGCCMMSSNOCCFGAHBCLKClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mGCCMMSSNOCCFGAHBCLKClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mGCCMMSSNOCCFGAHBCLKClkDomainClks[] =
{
  {
    /* .szClockName      = */ "dsa_noc_cfg_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_DSA_NOC_CFG_AHB_CBCR), HWIO_OFFS(MMSS_DSA_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_DSA_NOC_CFG_AHB_CLK
  },
  {
    /* .szClockName      = */ "mmagic_bimc_noc_cfg_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMAGIC_BIMC_NOC_CFG_AHB_CBCR), HWIO_OFFS(MMSS_MMAGIC_BIMC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMAGIC_BIMC_NOC_CFG_AHB_CLK
  },
  {
    /* .szClockName      = */ "mmagic_camss_noc_cfg_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMAGIC_CAMSS_NOC_CFG_AHB_CBCR), HWIO_OFFS(MMSS_MMAGIC_CAMSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMAGIC_CAMSS_NOC_CFG_AHB_CLK
  },
  {
    /* .szClockName      = */ "mmagic_mdss_noc_cfg_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMAGIC_MDSS_NOC_CFG_AHB_CBCR), HWIO_OFFS(MMSS_MMAGIC_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMAGIC_MDSS_NOC_CFG_AHB_CLK
  },
  {
    /* .szClockName      = */ "mmagic_video_noc_cfg_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMAGIC_VIDEO_NOC_CFG_AHB_CBCR), HWIO_OFFS(MMSS_MMAGIC_VIDEO_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMAGIC_VIDEO_NOC_CFG_AHB_CLK
  },
};


/*
 * HAL_clk_mMMSSGCCMMSSNOCCFGAHBCLKClkDomain
 *
 * GCCMMSSNOCCFGAHBCLK clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSGCCMMSSNOCCFGAHBCLKClkDomain =
{
  /* .nCGRAddr             = */ 0, /* this domain does not have a cmd rcgr */
  /* .pmClocks             = */ HAL_clk_mGCCMMSSNOCCFGAHBCLKClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mGCCMMSSNOCCFGAHBCLKClkDomainClks)/sizeof(HAL_clk_mGCCMMSSNOCCFGAHBCLKClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

