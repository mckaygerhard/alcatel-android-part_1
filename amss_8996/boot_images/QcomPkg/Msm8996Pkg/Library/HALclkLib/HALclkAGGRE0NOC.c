/*
==============================================================================

FILE:         HALclkAGGRE0NOC.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   AGGRE0NOC clocks.

   List of clock domains:


   List of power domains:
     - HAL_clk_mGCCAGGRE0NOCPowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014-2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/



/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_clk_mGCCAGGRE0NOCPowerDomain
 *
 * AGGRE0_NOC power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mGCCAGGRE0NOCPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_AGGRE0_NOC",
  /* .nGDSCRAddr              = */ HWIO_OFFS(GCC_AGGRE0_NOC_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

