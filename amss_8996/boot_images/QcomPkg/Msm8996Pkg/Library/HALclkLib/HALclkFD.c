/*
==============================================================================

FILE:         HALclkFD.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   FD clocks.

   List of clock domains:
     - HAL_clk_mMMSSFDCOREClkDomain


   List of power domains:
     - HAL_clk_mMMSSFDPowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mFDCOREClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mFDCOREClkDomainClks[] =
{
  {
    /* .szClockName      = */ "fd_core_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_FD_CORE_CBCR), HWIO_OFFS(MMSS_FD_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_FD_CORE_CLK
  },
  {
    /* .szClockName      = */ "fd_core_uar_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_FD_CORE_UAR_CBCR), HWIO_OFFS(MMSS_FD_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_FD_CORE_UAR_CLK
  },
};


/*
 * HAL_clk_mMMSSFDCOREClkDomain
 *
 * FDCORE clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSFDCOREClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_FD_CORE_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mFDCOREClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mFDCOREClkDomainClks)/sizeof(HAL_clk_mFDCOREClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*
 * HAL_clk_mMMSSFDPowerDomain
 *
 * FD power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mMMSSFDPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_FD",
  /* .nGDSCRAddr              = */ HWIO_OFFS(MMSS_FD_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

