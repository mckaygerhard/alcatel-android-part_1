/*
==============================================================================

FILE:         HALclkMDSS.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MDSS clocks.

   List of clock domains:
     - HAL_clk_mMMSSBYTE0ClkDomain
     - HAL_clk_mMMSSBYTE1ClkDomain
     - HAL_clk_mMMSSEDPAUXClkDomain
     - HAL_clk_mMMSSEDPGTCClkDomain
     - HAL_clk_mMMSSEDPLINKClkDomain
     - HAL_clk_mMMSSEDPPIXELClkDomain
     - HAL_clk_mMMSSESC0ClkDomain
     - HAL_clk_mMMSSESC1ClkDomain
     - HAL_clk_mMMSSEXTPCLKClkDomain
     - HAL_clk_mMMSSHDMIClkDomain
     - HAL_clk_mMMSSMDPClkDomain
     - HAL_clk_mMMSSPCLK0ClkDomain
     - HAL_clk_mMMSSPCLK1ClkDomain
     - HAL_clk_mMMSSVSYNCClkDomain


   List of power domains:
     - HAL_clk_mMMSSMDSSPowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014-2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl_2;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO_2;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl_3;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO_3;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mBYTE0ClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mBYTE0ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_byte0_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_BYTE0_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_BYTE0_CLK
  },
};


/*
 * HAL_clk_mMMSSBYTE0ClkDomain
 *
 * BYTE0 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSBYTE0ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_BYTE0_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mBYTE0ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mBYTE0ClkDomainClks)/sizeof(HAL_clk_mBYTE0ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_2,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mBYTE1ClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mBYTE1ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_byte1_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_BYTE1_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_BYTE1_CLK
  },
};


/*
 * HAL_clk_mMMSSBYTE1ClkDomain
 *
 * BYTE1 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSBYTE1ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_BYTE1_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mBYTE1ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mBYTE1ClkDomainClks)/sizeof(HAL_clk_mBYTE1ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_2,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mEDPAUXClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mEDPAUXClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_edpaux_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_EDPAUX_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_EDPAUX_CLK
  },
};


/*
 * HAL_clk_mMMSSEDPAUXClkDomain
 *
 * EDPAUX clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSEDPAUXClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_EDPAUX_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mEDPAUXClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mEDPAUXClkDomainClks)/sizeof(HAL_clk_mEDPAUXClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mEDPGTCClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mEDPGTCClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_edpgtc_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_EDPGTC_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_EDPGTC_CLK
  },
};


/*
 * HAL_clk_mMMSSEDPGTCClkDomain
 *
 * EDPGTC clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSEDPGTCClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_EDPGTC_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mEDPGTCClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mEDPGTCClkDomainClks)/sizeof(HAL_clk_mEDPGTCClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mEDPLINKClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mEDPLINKClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_edplink_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_EDPLINK_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_EDPLINK_CLK
  },
};


/*
 * HAL_clk_mMMSSEDPLINKClkDomain
 *
 * EDPLINK clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSEDPLINKClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_EDPLINK_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mEDPLINKClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mEDPLINKClkDomainClks)/sizeof(HAL_clk_mEDPLINKClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_2,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mEDPPIXELClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mEDPPIXELClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_edppixel_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_EDPPIXEL_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_EDPPIXEL_CLK
  },
};


/*
 * HAL_clk_mMMSSEDPPIXELClkDomain
 *
 * EDPPIXEL clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSEDPPIXELClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_EDPPIXEL_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mEDPPIXELClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mEDPPIXELClkDomainClks)/sizeof(HAL_clk_mEDPPIXELClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_2,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mESC0ClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mESC0ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_esc0_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_ESC0_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_ESC0_CLK
  },
};


/*
 * HAL_clk_mMMSSESC0ClkDomain
 *
 * ESC0 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSESC0ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_ESC0_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mESC0ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mESC0ClkDomainClks)/sizeof(HAL_clk_mESC0ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_2,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mESC1ClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mESC1ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_esc1_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_ESC1_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_ESC1_CLK
  },
};


/*
 * HAL_clk_mMMSSESC1ClkDomain
 *
 * ESC1 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSESC1ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_ESC1_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mESC1ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mESC1ClkDomainClks)/sizeof(HAL_clk_mESC1ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_2,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mEXTPCLKClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mEXTPCLKClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_extpclk_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_EXTPCLK_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_EXTPCLK_CLK
  },
};


/*
 * HAL_clk_mMMSSEXTPCLKClkDomain
 *
 * EXTPCLK clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSEXTPCLKClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_EXTPCLK_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mEXTPCLKClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mEXTPCLKClkDomainClks)/sizeof(HAL_clk_mEXTPCLKClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_2,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mHDMIClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mHDMIClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_hdmi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_HDMI_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_HDMI_CLK
  },
};


/*
 * HAL_clk_mMMSSHDMIClkDomain
 *
 * HDMI clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSHDMIClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_HDMI_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mHDMIClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mHDMIClkDomainClks)/sizeof(HAL_clk_mHDMIClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mMDPClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mMDPClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_mdp_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_MDP_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_MDP_CLK
  },
  {
    /* .szClockName      = */ "mmss_spdm_mdp_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_MDP_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_SPDM_MDP_CLK
  },
};


/*
 * HAL_clk_mMMSSMDPClkDomain
 *
 * MDP clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSMDPClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_MDP_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mMDPClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mMDPClkDomainClks)/sizeof(HAL_clk_mMDPClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_3,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mPCLK0ClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mPCLK0ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_pclk0_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_PCLK0_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_PCLK0_CLK
  },
  {
    /* .szClockName      = */ "mmss_spdm_pclk0_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_PCLK0_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_SPDM_PCLK0_CLK
  },
};


/*
 * HAL_clk_mMMSSPCLK0ClkDomain
 *
 * PCLK0 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSPCLK0ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_PCLK0_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mPCLK0ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mPCLK0ClkDomainClks)/sizeof(HAL_clk_mPCLK0ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_2,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mPCLK1ClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mPCLK1ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_pclk1_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_PCLK1_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_PCLK1_CLK
  },
  {
    /* .szClockName      = */ "mmss_spdm_pclk1_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_PCLK1_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_SPDM_PCLK1_CLK
  },
};


/*
 * HAL_clk_mMMSSPCLK1ClkDomain
 *
 * PCLK1 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSPCLK1ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_PCLK1_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mPCLK1ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mPCLK1ClkDomainClks)/sizeof(HAL_clk_mPCLK1ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_2,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mVSYNCClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mVSYNCClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mdss_vsync_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_VSYNC_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_VSYNC_CLK
  },
};


/*
 * HAL_clk_mMMSSVSYNCClkDomain
 *
 * VSYNC clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSVSYNCClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_VSYNC_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mVSYNCClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mVSYNCClkDomainClks)/sizeof(HAL_clk_mVSYNCClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*
 * HAL_clk_mMMSSMDSSPowerDomain
 *
 * MDSS power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mMMSSMDSSPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_MDSS",
  /* .nGDSCRAddr              = */ HWIO_OFFS(MMSS_MDSS_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

