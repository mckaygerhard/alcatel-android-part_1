#ifndef VSENSE_INTERNAL_H
#define VSENSE_INTERNAL_H
/*
===========================================================================
*/
/**
  @file vsense_internal.h

  Common internal data structures for vsense init and start calibration routine
*/
/*
  ====================================================================

  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies, Inc Proprietary and Confidential.

  ====================================================================


  when        who     what, where, why
  --------    ---     -------------------------------------------------
  2015/4/14  aks      Initial revision.
  ====================================================================
*/
#include "com_dtypes.h"


#define VSENSE_FIFO_ARRAY_SIZE  64
#define VSENSE_MAX_INSTANCES_PER_RAIL 5  //num of sensors per rail
#define VSENSE_MAX_SENSORS  12   //total num of voltage sensors on the chip


typedef enum
{
  VSENSE_TYPE_RAIL_MX      ,
  VSENSE_TYPE_RAIL_CX      ,
 //  VSENSE_TYPE_RAIL_GFX     , 
  VSENSE_TYPE_RAIL_EBI     , 
  VSENSE_TYPE_RAIL_MSS    , 
  VSENSE_TYPE_RAIL_GFX     , 
  VSENSE_TYPE_RAIL_MAX     ,
  VSENSE_TYPE_RAIL_INVALID = 0x7FFFFFFF,
}vsense_type_rail;

typedef enum
{
  VSENSE_TYPE_PMIC_RESOURCE_LDO,
  VSENSE_TYPE_PMIC_RESOURCE_SMPS
}vsense_type_pmic_resource;

typedef enum
{
  VSENSE_TRIG_PRE,
  VSENSE_TRIG_MID,
  VSENSE_TRIG_POST,  
}vsense_type_trig_pos;


typedef struct
{
  vsense_type_rail           rail;
  boolean                    is_supported;
  uint8                      peripheral_index;  
  uint8                      pmic_index;
  vsense_type_pmic_resource  pmic_resource_type;
  uint8                      vsense_instances_on_rail;
  uint8                      sensor_id_arr[VSENSE_MAX_INSTANCES_PER_RAIL];
}vsense_type_rail_info;

typedef struct 
{
  uint32                    fuse1_voltage_code;
  uint32                    fuse2_voltage_code;
}vsense_type_voltage_code;

typedef struct
{
  vsense_type_rail          rail;
  uint8                     vsense_instances_on_rail;
  uint32                    not_calibrated;
  uint32                    fuse1_voltage_uv;
  uint32                    fuse2_voltage_uv;
  vsense_type_voltage_code  voltage_code_arr[VSENSE_MAX_INSTANCES_PER_RAIL];   
}vsense_type_fuse_info;

typedef struct
{
  uint32                    version;
  uint32                    num_of_vsense_rails; //no. of rails vsense is monitoring
  vsense_type_fuse_info     rails_fuse_info[VSENSE_MAX_SENSORS]; //array size is num_of_rails_supported value 
}vsense_type_smem_info;



typedef struct 
{ 
  vsense_type_smem_info      smem_info;
  vsense_type_rail_info      rail_info[VSENSE_TYPE_RAIL_MAX];         
}vsense_type_rail_and_fuse_info;

typedef struct 
{
  uint8 is_vsense_enabled; //vsense enable/disable
}vsense_type_dal_cfg;


#endif

