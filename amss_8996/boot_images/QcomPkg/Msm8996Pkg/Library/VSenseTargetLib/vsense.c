/*
===========================================================================
  @file vsense.c

  This file provides functions to calibrate the voltage sensor data and store in shared memory.
===========================================================================

  Copyright (c) 2012 - 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  =========================================================================



  =========================================================================
*/

#include "HALhwio.h"
#include "msmhwiobase.h"
#include "DALSys.h"
#include "smem_type.h"
#include "vsense.h"
#include "smem.h"
#include "vsense_internal.h"
#include "pm_smps.h"
#include "pm_ldo.h"
#include "DALSys.h"
#include "vsense_dal_prop_ids.h"
#include "CoreVerify.h"
#include "LoaderUtils.h"
#include "DALFramework.h"
#include "hwio_qdss_vsense_controller.h"
#include "qdss_fuse_regs.h"
#include "tcsr_hwio.h"
#include "mpss_perph_hwio.h"
#include "ClockBoot.h"
#include "pm_version.h"

static DALSYS_PROPERTY_HANDLE_DECLARE(hProp_vsense);
static DALSYSPropertyVar prop_vsense; 

static vsense_type_dal_cfg*  vsense_dal_data;

uint8 vsense_calibrate_sensor(vsense_type_rail rail, uint8 sensor_id);

void vsense_read_fifo(vsense_type_rail rail,  uint32* fifo_sum,
	                                     uint8 sensor_id);
uint32 vsense_set_voltage( vsense_type_rail rail, 
                                         uint32 fuse_voltage );
void vsense_get_rail_status( vsense_type_rail rail, uint32* voltage, 
                                 uint32* sw_enable );
void vsense_enable_rail(vsense_type_rail rail, uint32 sw_enable);
void vsense_power_down(vsense_type_rail rail, uint8 sensor_id);

unsigned int qdss_fuse_trace_access(void);
void vsense_controller_en( boolean en_vsense);



#define VSENSE_FIFO_ARRAY_SIZE  64
#define VSENSE_FIFO_READ_COUNT  1
#define VSENSE_FIFO_READ_DELAY  1 //10 XO clk cycles .. 10 * (1000000/19200000)
static  void *vsense_smem_ptr = NULL;
#define VSENSE_MIN_FUSE_VOLTAGE_DIFF 100*1000 //100 mV
#define UNLOCK_REG_VAL 0xC5ACCE55
#define VSENSE_WAIT_FOR_WR_STATUS(sensor_id)  DALSYS_BusyWait(20)
//while(inpdw(HWIO_ADDR( QDSS_VSENSE_SENSOR_CONFIG_WR_STATUS)) != (1 << sensor_id))


static vsense_type_rail_and_fuse_info vsense_rail_and_fuse_info =
{
  .rail_info = 
  {
    {
      .rail  = VSENSE_TYPE_RAIL_MX,
      .is_supported = TRUE,
      .peripheral_index = 1,
      .pmic_index = 0,
      .pmic_resource_type = VSENSE_TYPE_PMIC_RESOURCE_SMPS,
      .vsense_instances_on_rail = 3,
      .sensor_id_arr =
    	 {2,5,7}        
    },                
    {               
      .rail = VSENSE_TYPE_RAIL_CX,
      .is_supported = TRUE,
      .peripheral_index = 0,
      .pmic_index = 0,
      .pmic_resource_type = VSENSE_TYPE_PMIC_RESOURCE_SMPS,
      .vsense_instances_on_rail = 1,
      .sensor_id_arr =
    	 {0}
    },
   /*  {               
      .rail = VSENSE_TYPE_RAIL_GFX,
      .is_supported = TRUE, 
      .peripheral_index = 1,
      .pmic_index = 2,
      .pmic_resource_type = VSENSE_TYPE_PMIC_RESOURCE_SMPS,
      .vsense_instances_on_rail = 1,
      .sensor_id_arr =
    	 {1}
    },  */
    {               
      .rail = VSENSE_TYPE_RAIL_EBI,
      .is_supported = TRUE,
      .peripheral_index = 2,
      .pmic_index = 0,
      .pmic_resource_type = VSENSE_TYPE_PMIC_RESOURCE_LDO,
      .vsense_instances_on_rail = 2,
      .sensor_id_arr =
    	 {3,6}
    },
    {               
      .rail = VSENSE_TYPE_RAIL_MSS,
      .is_supported = TRUE, 
      .peripheral_index = 6,
      .pmic_index = 0,
      .pmic_resource_type = VSENSE_TYPE_PMIC_RESOURCE_SMPS,
      .vsense_instances_on_rail = 1,
      .sensor_id_arr =
    	 {4}
    },
	{               
      .rail = VSENSE_TYPE_RAIL_GFX,
      .is_supported = TRUE, 
      .peripheral_index = 1,
      .pmic_index = 2,
      .pmic_resource_type = VSENSE_TYPE_PMIC_RESOURCE_SMPS,
      .vsense_instances_on_rail = 1,
      .sensor_id_arr =
    	 {1}
    }, 
	
  
  },
  
  .smem_info =
  {  
    .version = 1,
    .num_of_vsense_rails = VSENSE_TYPE_RAIL_MAX,
    .rails_fuse_info = 
    {
     {
        .rail  = VSENSE_TYPE_RAIL_MX,
        .fuse1_voltage_uv =  900000, 
        .fuse2_voltage_uv = 1000000,	
      
      },                
      {               
        .rail  = VSENSE_TYPE_RAIL_CX,
        .fuse1_voltage_uv =  900000, 
        .fuse2_voltage_uv = 1000000,	
        
      },
	/*    {               
        .rail = VSENSE_TYPE_RAIL_GFX,
        .fuse1_voltage_uv =  900000, 
        .fuse2_voltage_uv = 1000000,       
      
      }, */
      {               
        .rail = VSENSE_TYPE_RAIL_EBI,
        .fuse1_voltage_uv =  900000, 
        .fuse2_voltage_uv =  1000000,
     
      },
      {               
        .rail = VSENSE_TYPE_RAIL_MSS,
        .fuse1_voltage_uv =  900000, 
        .fuse2_voltage_uv = 1000000,       
      
      },
       {               
        .rail = VSENSE_TYPE_RAIL_GFX,
        .fuse1_voltage_uv =  900000, 
        .fuse2_voltage_uv = 1000000,       
      
      },
     
    }
  }
  
};

const vsense_type_rail_and_fuse_info* const vsense_rail_ref = &vsense_rail_and_fuse_info;


//configure common settings for any voltage sensor before
//it can be used
void vsense_config_common(vsense_type_rail rail, uint8 sensor_id)
{
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  HWIO_OUTI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, 0);
  HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, POWER_EN, 1 );

  HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_0, sensor_id, TRIG_POS, VSENSE_TRIG_POST);
  //set the write enable to push the data through the daisy chain
  HWIO_OUT(QDSS_VSENSE_SENSOR_CONFIG_WR_EN, 0);
  HWIO_OUT(QDSS_VSENSE_SENSOR_CONFIG_WR_EN,(1 << sensor_id));
  
  VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
 
}

//get vsense config data 
void* vsense_get_config_info(uint32 prop_id)
{
  void* cfg_ptr = NULL;

  if(DAL_SUCCESS == DALSYS_GetPropertyValue(hProp_vsense, NULL, prop_id, &prop_vsense))
  {
      cfg_ptr = (void*) prop_vsense.Val.pStruct;
  }

  return cfg_ptr;
}

//main function called to init vsense and perform calibration
boolean vsense_init()
{

  if(FALSE != qdss_fuse_trace_access())
  {
     vsense_controller_en(TRUE);
  }

  DALSYS_GetDALPropertyHandleStr("/vsense",hProp_vsense);


  vsense_dal_data       = (vsense_type_dal_cfg*)
                         vsense_get_config_info(VSENSE_PROP_CONFIG);

  if( vsense_dal_data == NULL || 
       vsense_dal_data->is_vsense_enabled == FALSE)
  {
     return TRUE;
  }
  //Enable clocks 
  if(Clock_InitVSense())
  {
    HWIO_OUT(QDSS_TPDM_LAR, UNLOCK_REG_VAL);
    vsense_start_calibration();

    Clock_ShutdownVSense();

  }
   
  return TRUE;
}

unsigned int qdss_fuse_trace_access(void)
{
   unsigned int status=FALSE;
   
   status =           !(HWIO_INF(OEM_CONFIG2,DAP_SPNIDEN_DISABLE));
   status = status && !(HWIO_INF(OEM_CONFIG2,DAP_SPIDEN_DISABLE));
   status = status && !(HWIO_INF(OEM_CONFIG1,DAP_NIDEN_DISABLE));
   status = status && !(HWIO_INF(OEM_CONFIG1,DAP_DBGEN_DISABLE));
   status = status && !(HWIO_INF(OEM_CONFIG1,DAP_DEVICEEN_DISABLE));
   status = status && !(HWIO_INF(OEM_CONFIG0,ALL_DEBUG_DISABLE));
  
   return status;
}


void vsense_controller_en( boolean en_vsense)
{

  if(en_vsense == TRUE)
  {
    HWIO_OUT(TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER, 1); //enable controller block
  }
    
}


//store the calibrated data into the shared memory 
boolean vsense_copy_to_smem()
{
  
  if( vsense_dal_data == NULL || 
       vsense_dal_data->is_vsense_enabled == FALSE)
  {
     return TRUE;
  }
  
  vsense_smem_ptr = (void *)smem_alloc(SMEM_VSENSE_DATA, 
                          sizeof(vsense_rail_and_fuse_info.smem_info));
  if(vsense_smem_ptr == NULL) 
  {  
    return FALSE;
  }
  memscpy(vsense_smem_ptr, sizeof(vsense_rail_and_fuse_info.smem_info),
              &vsense_rail_and_fuse_info.smem_info, 
              sizeof(vsense_rail_and_fuse_info.smem_info));

  return TRUE;
}

//calibrate the vsense for the supported rails
boolean vsense_start_calibration()
{
  uint32 rail_count;
  uint32 voltage = 0;
  uint32 mx_voltage = 0;
  uint32 sw_enable = 0;
  uint8  sensor_count = 0; 
  uint8  num_of_sensors = 0; 
  vsense_type_rail_info  *rail_info = 
  	                      vsense_rail_and_fuse_info.rail_info;
  vsense_type_fuse_info  *fuse_info = 
  	                      vsense_rail_and_fuse_info.smem_info.rails_fuse_info;

  if ( pm_get_pmic_model(2) != PMIC_IS_PM8004)
  {
     rail_info[VSENSE_TYPE_RAIL_GFX].pmic_index = 1;
  }

  for( rail_count = 0; rail_count < VSENSE_TYPE_RAIL_MAX; rail_count++ )
  {
    voltage = 0;
    sw_enable = 0;
    if(rail_info[rail_count].is_supported)
    {
      
      num_of_sensors = 
  	          rail_info[rail_count].vsense_instances_on_rail;
      //read the current rail status - voltage, sw en 
      vsense_get_rail_status( rail_info[rail_count].rail,
                             &voltage, &sw_enable);

      //set the fuse 1 voltage
      fuse_info[rail_count].fuse1_voltage_uv = 
                    vsense_set_voltage( rail_info[rail_count].rail , 
                                        fuse_info[rail_count].fuse1_voltage_uv);

      if( fuse_info[rail_count].fuse1_voltage_uv == 0)
      {
         fuse_info[rail_count].not_calibrated = TRUE;
         continue;
      }

      // turn rail on for calibration
      if(sw_enable == PM_OFF)
      {
        vsense_enable_rail(rail_info[rail_count].rail, PM_ON);
        DALSYS_BusyWait(500); //let it settle   
      }
      
      //workaround for GPU block disabled when confguring 
      //MX sensors . The sensors are in daisy chain and if the 
      //the block prior to the sensor being configured is down 
      //the sensor cannot be configured. Bypassing that sensor
      //makes sense for now.
      if(rail_count == VSENSE_TYPE_RAIL_GFX)
      {
         HWIO_OUT(QDSS_VSENSE_SENSOR_APPS_BYPASS_EN,0x0);  //disable the bypass
      }
      else
      {
         HWIO_OUT(QDSS_VSENSE_SENSOR_APPS_BYPASS_EN,0x2);  //enable bypass
      }
      if ( rail_count == VSENSE_TYPE_RAIL_MSS )
      {
          HWIO_OUT(MSS_CLAMP_IO, 0x8); //unclamp MSS for vsense calibration
      }
	 

      //calibrate the rail
      for(sensor_count= 0; 
          sensor_count < num_of_sensors; 
          sensor_count++)
      {
         fuse_info[rail_count].voltage_code_arr[sensor_count].fuse1_voltage_code =
         	vsense_calibrate_sensor(rail_info[rail_count].rail, 
         	                 rail_info[rail_count].sensor_id_arr[sensor_count]);
      } 
      //set fuse 2 voltage
      fuse_info[rail_count].fuse2_voltage_uv = 
                    vsense_set_voltage( rail_info[rail_count].rail , 
                                        fuse_info[rail_count].fuse2_voltage_uv);


      if(fuse_info[rail_count].fuse2_voltage_uv == 0)
      {
        fuse_info[rail_count].not_calibrated = TRUE;
        continue;
      }
      
      //ensure 100mV gap between fuse1 and fuse2 voltage
      CORE_VERIFY(fuse_info[rail_count].fuse2_voltage_uv -
      	                    fuse_info[rail_count].fuse1_voltage_uv >= 
                           VSENSE_MIN_FUSE_VOLTAGE_DIFF);
      
      //calibrate the rail
      for(sensor_count= 0; 
          sensor_count < num_of_sensors; 
          sensor_count++)
      {
         fuse_info[rail_count].voltage_code_arr[sensor_count].fuse2_voltage_code =
         	vsense_calibrate_sensor(rail_info[rail_count].rail, 
         	                 rail_info[rail_count].sensor_id_arr[sensor_count]);
      
        //fuse 1 code (lower voltage) cannot be greater than/equal to fuse 2 code (higher voltage)
        if(fuse_info[rail_count].voltage_code_arr[sensor_count].fuse1_voltage_code >= 
      	    fuse_info[rail_count].voltage_code_arr[sensor_count].fuse2_voltage_code )
        {
          fuse_info[rail_count].not_calibrated = TRUE;
         
        }
      
        //power down the sensor
        vsense_power_down( rail_info[rail_count].rail,
                           rail_info[rail_count].sensor_id_arr[sensor_count]);   
      }
	  
	  if ( rail_count == VSENSE_TYPE_RAIL_MSS )
	  {
	         //This bit controls the bypass mux also for the MSS sensor
			 //So if unclapmed puts MSS out of BYPASS . If you turn off MSS
			 //rail with MSS sensor not in YBPASS then ....... 
			 HWIO_OUT(MSS_CLAMP_IO, 0x0); 
	  }
      //turn off the rail if it was off previously
      if(sw_enable == PM_OFF)
      {
        vsense_enable_rail(rail_info[rail_count].rail, PM_OFF);
      }
      
     
     
      if(rail_info[rail_count].rail != VSENSE_TYPE_RAIL_MX)
      {
      //restore the rail voltage to before calibration state
      vsense_set_voltage( rail_info[rail_count].rail , voltage);    
    }
      else
      {
          mx_voltage = voltage; //save mx voltage to restore later
      }
    }
   
 }
  //restore MX voltage now 
  if(mx_voltage != 0)
  {
     //restore the rail voltage to before calibration state
     vsense_set_voltage( VSENSE_TYPE_RAIL_MX , mx_voltage);    
  }
  return TRUE;
}
 
void vsense_enable_rail(vsense_type_rail rail, uint32 sw_enable)
{
  vsense_type_rail_info  *rail_info = 
  	                      vsense_rail_and_fuse_info.rail_info;
  
  
  switch( rail_info[rail].pmic_resource_type)
  {
    case VSENSE_TYPE_PMIC_RESOURCE_LDO:
       
      pm_ldo_sw_enable( rail_info[rail].pmic_index,
                        rail_info[rail].peripheral_index , 
                        sw_enable);
      break;
    case VSENSE_TYPE_PMIC_RESOURCE_SMPS:
      pm_smps_sw_enable( rail_info[rail].pmic_index,
                         rail_info[rail].peripheral_index , 
                         sw_enable);
      break;
  }

}
//set the fuse voltage based on corner -for railway
//else set the default fuse voltage
uint32 vsense_set_voltage( vsense_type_rail rail, uint32 fuse_voltage )
{   
  uint32 uv = fuse_voltage;
  uint32 mx_voltage = 0;
  vsense_type_rail_info  *rail_info = 
  	                      vsense_rail_and_fuse_info.rail_info;
  

  pm_smps_volt_level_status( rail_info[VSENSE_TYPE_RAIL_MX].pmic_index,
                             rail_info[VSENSE_TYPE_RAIL_MX].peripheral_index ,
                             (pm_volt_level_type*) &mx_voltage);


    //check if voltage is <= Memory domain 
    if((rail != VSENSE_TYPE_RAIL_MX) && (fuse_voltage > mx_voltage))
    {
      return 0;        
    }
    
    switch( rail_info[rail].pmic_resource_type)
    {
      case VSENSE_TYPE_PMIC_RESOURCE_LDO:
        pm_ldo_volt_level( rail_info[rail].pmic_index,
                           rail_info[rail].peripheral_index , 
                           fuse_voltage);
        break;
        case VSENSE_TYPE_PMIC_RESOURCE_SMPS:
        pm_smps_volt_level( rail_info[rail].pmic_index,
                            rail_info[rail].peripheral_index , 
                            fuse_voltage);
        break;
    }
    DALSYS_BusyWait(500);  
 
  return uv;
}

void vsense_get_rail_status( vsense_type_rail rail, uint32* voltage, 
                                 uint32* sw_enable )
{   
  vsense_type_rail_info  *rail_info = 
  	                      vsense_rail_and_fuse_info.rail_info;
 
  
  switch( rail_info[rail].pmic_resource_type)
  {
    case VSENSE_TYPE_PMIC_RESOURCE_LDO:
      pm_ldo_volt_level_status( rail_info[rail].pmic_index,
                                rail_info[rail].peripheral_index , 
                                voltage);
      pm_ldo_sw_enable_status( rail_info[rail].pmic_index,
                               rail_info[rail].peripheral_index , 
                               sw_enable);
      break;
    case VSENSE_TYPE_PMIC_RESOURCE_SMPS:
      pm_smps_volt_level_status( rail_info[rail].pmic_index,
                                 rail_info[rail].peripheral_index , 
                                 voltage);
      pm_smps_sw_enable_status( rail_info[rail].pmic_index,
                                rail_info[rail].peripheral_index , 
                                sw_enable);
     break;
  }
  
}

void vsense_func_en(vsense_type_rail rail, boolean en_vsense, uint8 sensor_id)
{
  
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
   
  if(en_vsense == FALSE)
  {
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, FUNC_EN , 0);
  }
  else
  {
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, FUNC_EN, 1);
  }
  
   //set the write enable to push the data through the daisy chain
  HWIO_OUT(QDSS_VSENSE_SENSOR_CONFIG_WR_EN, 0);
  HWIO_OUT(QDSS_VSENSE_SENSOR_CONFIG_WR_EN,(1 << sensor_id));
  
  VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
  
}


//enable and set the start capture 
void vsense_start_capture(vsense_type_rail rail, boolean en_capture, uint8 sensor_id)
{
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
   
  if(en_capture == FALSE)
  {
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, SW_CAPTURE , 0);
  }
  else
  {
    HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, SW_CAPTURE, 1);
  }
  
   //set the write enable to push the data through the daisy chain
  HWIO_OUT(QDSS_VSENSE_SENSOR_CONFIG_WR_EN, 0);
  HWIO_OUT(QDSS_VSENSE_SENSOR_CONFIG_WR_EN,(1 << sensor_id));
  
  VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
}



//disable and reset the vsense
void vsense_power_down(vsense_type_rail rail, uint8 sensor_id)
{
  vsense_type_rail_info  *rail_info = 
  	                      vsense_rail_and_fuse_info.rail_info;
 
  if(rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  HWIO_OUTFI(QDSS_VSENSE_SENSORn_CONFIG_1, sensor_id, POWER_EN, 0);

  //set the write enable to push the data through the daisy chain
  HWIO_OUT(QDSS_VSENSE_SENSOR_CONFIG_WR_EN, 0);
  HWIO_OUT(QDSS_VSENSE_SENSOR_CONFIG_WR_EN,(1 << sensor_id));
  VSENSE_WAIT_FOR_WR_STATUS(sensor_id);
 
  
}



//start the capture, read fifio and return the fifo average -> voltage code
uint8 vsense_calibrate_sensor(vsense_type_rail rail, uint8 sensor_id)
{
  uint32 fifo_sum;
  uint32 fifo_cumulative_sum = 0;
  uint8 read_fifo_count ;
  
  
  
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   return 0;
  }

  
  //configure the common settings
  vsense_config_common(rail, sensor_id);

  for(read_fifo_count = 0; read_fifo_count < VSENSE_FIFO_READ_COUNT; read_fifo_count++)
  {
    vsense_func_en(rail, TRUE, sensor_id); 
    //set the en and start capture 
    vsense_start_capture(rail, TRUE, sensor_id);
    //read the fifo buffer
    vsense_read_fifo(rail, &fifo_sum, sensor_id);
    //sum the fifo read
    fifo_cumulative_sum += fifo_sum;
    vsense_func_en(rail,FALSE, sensor_id);
    vsense_start_capture(rail, FALSE, sensor_id);
    //unmask the FIFO Complete
    HWIO_OUT(QDSS_VSENSE_SENSOR_FC_MASK_EN, 0 << sensor_id);
   
  }
  vsense_power_down(rail, sensor_id);
  return        (fifo_cumulative_sum + 
                (VSENSE_FIFO_ARRAY_SIZE * VSENSE_FIFO_READ_COUNT)/2 ) /
                (VSENSE_FIFO_ARRAY_SIZE * VSENSE_FIFO_READ_COUNT) ;
   
  
}

//read the 64 bytes fifo from the status register
void vsense_read_fifo(vsense_type_rail rail, uint32* fifo_sum, uint8 sensor_id)
{
 
  uint32 fifo_array_sum = 0;
  uint8  read_byte_count = 0;
  uint8  busy_loop = 0;
  static uint8  fifo_data[VSENSE_FIFO_ARRAY_SIZE] = {0}; //ToDo: remove the static later 
  uint32* fifo_ptr = (uint32*)fifo_data; 
   
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   return;
  }
  
 
  DALSYS_memset(fifo_data,VSENSE_FIFO_ARRAY_SIZE,0);
  
 #define FC_STATUS_MASK ((1<< 16)<< sensor_id)

  while((HWIO_IN(QDSS_VSENSE_SENSOR_ALARM_FC_STATUS) & FC_STATUS_MASK) 
  	                      != FC_STATUS_MASK)
  {

    if(busy_loop > 5)
    {
      vsense_rail_and_fuse_info.rail_info[rail].is_supported = FALSE;
      return;
    }
    DALSYS_BusyWait(5);
    busy_loop++;
  }
     //mask the FIFO Complete
    HWIO_OUT(QDSS_VSENSE_SENSOR_FC_MASK_EN, (1 << sensor_id));
    DALSYS_BusyWait(50); //per HPG
    for(read_byte_count= 0; read_byte_count < 8 ; read_byte_count++)
    {
      HWIO_OUT(QDSS_VSENSE_CNTRL_FIFO_DATA_MUX_SEL,read_byte_count);
      *fifo_ptr = HWIO_IN(QDSS_VSENSE_SENSOR_FIFO_DATA0);
      fifo_ptr++;
      *fifo_ptr = HWIO_IN(QDSS_VSENSE_SENSOR_FIFO_DATA1);
      fifo_ptr++;
      
    }
  
  for(read_byte_count= 0; read_byte_count < 64 ; read_byte_count++)
  {
    fifo_array_sum += fifo_data[read_byte_count];
  }
  *fifo_sum = fifo_array_sum;
}
   
