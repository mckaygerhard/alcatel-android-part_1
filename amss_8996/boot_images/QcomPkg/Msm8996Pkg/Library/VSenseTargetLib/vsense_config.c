#include "vsense_internal.h"
/*
==============================================================================

FILE:         vsense_cfg.c

DESCRIPTION:
  This file contains vsense driver data for DAL based driver.

==============================================================================

            Copyright (c) 2013-2015 QUALCOMM Technologies Inc.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR

==============================================================================

                             Edit History



when       who     what, where, why
--------   ---     -----------------------------------------------------------
04/20/15   aks     Initial vsense driver 
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/




/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 * vsense configuration data.
 */


 vsense_type_dal_cfg vsense_config_data = {0}; // Enable/Disable VSense feature 
 

