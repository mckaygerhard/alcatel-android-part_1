/** @file DeviceProgrammerLiteLib.c
  
  Stub functions for DeviceProgrammerLiteLib

  Copyright (c) 2014, 2015 Qualcomm Technologies, Inc. All rights reserved.
**/

/*=============================================================================
                              EDIT HISTORY


when       who      what, where, why
--------   ---      -----------------------------------------------------------
10/22/15   plc      Replaced tlb invalidate with el-agnostic mmu_invalidate_tlb
12/02/14   wkimberl  Initial revision.


=============================================================================*/
#include "boot_sbl_if.h"
#include "boot_cache_mmu.h"
#include "boot_page_table_armv8.h"
#include "deviceprogrammer_initialize.h"

/*!
 * Stub for boot_hand_control_to_deviceprogrammer_lite_main
 *
 * @param bl_shared_data
 *    The shared bootloader information.
 *
*****************************************************************************/
void
boot_hand_control_to_deviceprogrammer_lite_main (boot_pbl_shared_data_type *pbl_shared_data)
{
  /* Temporary: mark page table as executable
     This is not needed if we could remove unused funcitons in SBL for device programmer. */
  boot_boolean result = FALSE;
  struct mem_block devprog_mem_block;
  devprog_mem_block.p_base = SCL_DEVICEPROG_ZI_BASE;
  devprog_mem_block.v_base = SCL_DEVICEPROG_ZI_BASE;
  devprog_mem_block.size_in_kbytes = (SCL_DEVICEPROG_ZI_SIZE) >> 10;
  devprog_mem_block.memory_mapping = MMU_L3_SECTION_MAPPING;
  devprog_mem_block.access = MMU_PAGETABLE_MEM_READ_WRITE;
  devprog_mem_block.cachetype = MMU_PAGETABLE_MEM_NON_CACHEABLE;
  devprog_mem_block.executable = MMU_PAGETABLE_NON_EXECUTABLE_REGION;

  result =
    boot_mmu_page_table_map_single_mem_block((uint64*)mmu_get_page_table_base(),
                                             &devprog_mem_block);
  BL_VERIFY(result, BL_ERR_SBL);
  mmu_invalidate_tlb_el3();

  /* Done setting up the MMU, run device programmer.  */
  deviceprogrammer_entry(pbl_shared_data);
}
