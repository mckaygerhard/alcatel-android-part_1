/** @file CprLibNull.c
  
  Stub functions for CprLib

  Copyright (c) 2014, Qualcomm Technologies, Inc. All rights reserved.
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 09/24/14   ck      Initial revision

=============================================================================*/


/*==========================================================================

                               INCLUDE FILES

===========================================================================*/



/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/ 

//Function to be called to initialize the CPR driver.
//Must be called after railway_init.
//Will do the necessary settling of voltages as required for any DDR-related
//training on the target.
void rbcpr_init(void)
{
}


//Function to be called after SMEM is initialized to push out the CPR settings
//to SMEM. These settings are to be picked up by the RPM CPR driver during boot.
//Must be done before the RPM FW execution begins.
void rbcpr_externalize_state(void)
{
}
